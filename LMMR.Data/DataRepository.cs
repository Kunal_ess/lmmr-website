﻿#region Using directives

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Configuration.Provider;
using System.Web.Configuration;
using System.Web;
using LMMR.Entities;
using LMMR.Data;
using LMMR.Data.Bases;

#endregion

namespace LMMR.Data
{
	/// <summary>
	/// This class represents the Data source repository and gives access to all the underlying providers.
	/// </summary>
	[CLSCompliant(true)]
	public sealed class DataRepository 
	{
		private static volatile NetTiersProvider _provider = null;
        private static volatile NetTiersProviderCollection _providers = null;
		private static volatile NetTiersServiceSection _section = null;
		private static volatile Configuration _config = null;
        
        private static object SyncRoot = new object();
				
		private DataRepository()
		{
		}
		
		#region Public LoadProvider
		/// <summary>
        /// Enables the DataRepository to programatically create and 
        /// pass in a <c>NetTiersProvider</c> during runtime.
        /// </summary>
        /// <param name="provider">An instatiated NetTiersProvider.</param>
        public static void LoadProvider(NetTiersProvider provider)
        {
			LoadProvider(provider, false);
        }
		
		/// <summary>
        /// Enables the DataRepository to programatically create and 
        /// pass in a <c>NetTiersProvider</c> during runtime.
        /// </summary>
        /// <param name="provider">An instatiated NetTiersProvider.</param>
        /// <param name="setAsDefault">ability to set any valid provider as the default provider for the DataRepository.</param>
		public static void LoadProvider(NetTiersProvider provider, bool setAsDefault)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");

            if (_providers == null)
			{
				lock(SyncRoot)
				{
            		if (_providers == null)
						_providers = new NetTiersProviderCollection();
				}
			}
			
            if (_providers[provider.Name] == null)
            {
                lock (_providers.SyncRoot)
                {
                    _providers.Add(provider);
                }
            }

            if (_provider == null || setAsDefault)
            {
                lock (SyncRoot)
                {
                    if(_provider == null || setAsDefault)
                         _provider = provider;
                }
            }
        }
		#endregion 
		
		///<summary>
		/// Configuration based provider loading, will load the providers on first call.
		///</summary>
		private static void LoadProviders()
        {
            // Avoid claiming lock if providers are already loaded
            if (_provider == null)
            {
                lock (SyncRoot)
                {
                    // Do this again to make sure _provider is still null
                    if (_provider == null)
                    {
                        // Load registered providers and point _provider to the default provider
                        _providers = new NetTiersProviderCollection();

                        ProvidersHelper.InstantiateProviders(NetTiersSection.Providers, _providers, typeof(NetTiersProvider));
						_provider = _providers[NetTiersSection.DefaultProvider];

                        if (_provider == null)
                        {
                            throw new ProviderException("Unable to load default NetTiersProvider");
                        }
                    }
                }
            }
        }

		/// <summary>
        /// Gets the provider.
        /// </summary>
        /// <value>The provider.</value>
        public static NetTiersProvider Provider
        {
            get { LoadProviders(); return _provider; }
        }

		/// <summary>
        /// Gets the provider collection.
        /// </summary>
        /// <value>The providers.</value>
        public static NetTiersProviderCollection Providers
        {
            get { LoadProviders(); return _providers; }
        }
		
		/// <summary>
		/// Creates a new <see cref="TransactionManager"/> instance from the current datasource.
		/// </summary>
		/// <returns></returns>
		public TransactionManager CreateTransaction()
		{
			return _provider.CreateTransaction();
		}

		#region Configuration

		/// <summary>
		/// Gets a reference to the configured NetTiersServiceSection object.
		/// </summary>
		public static NetTiersServiceSection NetTiersSection
		{
			get
			{
				// Try to get a reference to the default <netTiersService> section
				_section = WebConfigurationManager.GetSection("netTiersService") as NetTiersServiceSection;

				if ( _section == null )
				{
					// otherwise look for section based on the assembly name
					_section = WebConfigurationManager.GetSection("LMMR.Data") as NetTiersServiceSection;
				}

				#region Design-Time Support

				if ( _section == null )
				{
					// lastly, try to find the specific NetTiersServiceSection for this assembly
					foreach ( ConfigurationSection temp in Configuration.Sections )
					{
						if ( temp is NetTiersServiceSection )
						{
							_section = temp as NetTiersServiceSection;
							break;
						}
					}
				}

				#endregion Design-Time Support
				
				if ( _section == null )
				{
					throw new ProviderException("Unable to load NetTiersServiceSection");
				}

				return _section;
			}
		}

		#region Design-Time Support

		/// <summary>
		/// Gets a reference to the application configuration object.
		/// </summary>
		public static Configuration Configuration
		{
			get
			{
				if ( _config == null )
				{
					// load specific config file
					if ( HttpContext.Current != null )
					{
						_config = WebConfigurationManager.OpenWebConfiguration("~");
					}
					else
					{
						String configFile = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile.Replace(".config", "").Replace(".temp", "");

						// check for design mode
						if ( configFile.ToLower().Contains("devenv.exe") )
						{
							_config = GetDesignTimeConfig();
						}
						else
						{
							_config = ConfigurationManager.OpenExeConfiguration(configFile);
						}
					}
				}

				return _config;
			}
		}

		private static Configuration GetDesignTimeConfig()
		{
			ExeConfigurationFileMap configMap = null;
			Configuration config = null;
			String path = null;

			// Get an instance of the currently running Visual Studio IDE.
			EnvDTE80.DTE2 dte = (EnvDTE80.DTE2) System.Runtime.InteropServices.Marshal.GetActiveObject("VisualStudio.DTE.10.0");
			
			if ( dte != null )
			{
				dte.SuppressUI = true;

				EnvDTE.ProjectItem item = dte.Solution.FindProjectItem("web.config");
				if ( item != null )
				{
					if (!item.ContainingProject.FullName.ToLower().StartsWith("http:"))
               {
                  System.IO.FileInfo info = new System.IO.FileInfo(item.ContainingProject.FullName);
                  path = String.Format("{0}\\{1}", info.Directory.FullName, item.Name);
                  configMap = new ExeConfigurationFileMap();
                  configMap.ExeConfigFilename = path;
               }
               else
               {
                  configMap = new ExeConfigurationFileMap();
                  configMap.ExeConfigFilename = item.get_FileNames(0);
               }}

				/*
				Array projects = (Array) dte2.ActiveSolutionProjects;
				EnvDTE.Project project = (EnvDTE.Project) projects.GetValue(0);
				System.IO.FileInfo info;

				foreach ( EnvDTE.ProjectItem item in project.ProjectItems )
				{
					if ( String.Compare(item.Name, "web.config", true) == 0 )
					{
						info = new System.IO.FileInfo(project.FullName);
						path = String.Format("{0}\\{1}", info.Directory.FullName, item.Name);
						configMap = new ExeConfigurationFileMap();
						configMap.ExeConfigFilename = path;
						break;
					}
				}
				*/
			}

			config = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
			return config;
		}

		#endregion Design-Time Support

		#endregion Configuration

		#region Connections

		/// <summary>
		/// Gets a reference to the ConnectionStringSettings collection.
		/// </summary>
		public static ConnectionStringSettingsCollection ConnectionStrings
		{
			get
			{
					// use default ConnectionStrings if _section has already been discovered
					if ( _config == null && _section != null )
					{
						return WebConfigurationManager.ConnectionStrings;
					}
					
					return Configuration.ConnectionStrings.ConnectionStrings;
			}
		}

		// dictionary of connection providers
		private static Dictionary<String, ConnectionProvider> _connections;

		/// <summary>
		/// Gets the dictionary of connection providers.
		/// </summary>
		public static Dictionary<String, ConnectionProvider> Connections
		{
			get
			{
				if ( _connections == null )
				{
					lock (SyncRoot)
                	{
						if (_connections == null)
						{
							_connections = new Dictionary<String, ConnectionProvider>();
		
							// add a connection provider for each configured connection string
							foreach ( ConnectionStringSettings conn in ConnectionStrings )
							{
								_connections.Add(conn.Name, new ConnectionProvider(conn.Name, conn.ConnectionString));
							}
						}
					}
				}

				return _connections;
			}
		}

		/// <summary>
		/// Adds the specified connection string to the map of connection strings.
		/// </summary>
		/// <param name="connectionStringName">The connection string name.</param>
		/// <param name="connectionString">The provider specific connection information.</param>
		public static void AddConnection(String connectionStringName, String connectionString)
		{
			lock (SyncRoot)
            {
				Connections.Remove(connectionStringName);
				ConnectionProvider connection = new ConnectionProvider(connectionStringName, connectionString);
				Connections.Add(connectionStringName, connection);
			}
		}

		/// <summary>
		/// Provides ability to switch connection string at runtime.
		/// </summary>
		public sealed class ConnectionProvider
		{
			private NetTiersProvider _provider;
			private NetTiersProviderCollection _providers;
			private String _connectionStringName;
			private String _connectionString;


			/// <summary>
			/// Initializes a new instance of the ConnectionProvider class.
			/// </summary>
			/// <param name="connectionStringName">The connection string name.</param>
			/// <param name="connectionString">The provider specific connection information.</param>
			public ConnectionProvider(String connectionStringName, String connectionString)
			{
				_connectionString = connectionString;
				_connectionStringName = connectionStringName;
			}

			/// <summary>
			/// Gets the provider.
			/// </summary>
			public NetTiersProvider Provider
			{
				get { LoadProviders(); return _provider; }
			}

			/// <summary>
			/// Gets the provider collection.
			/// </summary>
			public NetTiersProviderCollection Providers
			{
				get { LoadProviders(); return _providers; }
			}

			/// <summary>
			/// Instantiates the configured providers based on the supplied connection string.
			/// </summary>
			private void LoadProviders()
			{
				DataRepository.LoadProviders();

				// Avoid claiming lock if providers are already loaded
				if ( _providers == null )
				{
					lock ( SyncRoot )
					{
						// Do this again to make sure _provider is still null
						if ( _providers == null )
						{
							// apply connection information to each provider
							for ( int i = 0; i < NetTiersSection.Providers.Count; i++ )
							{
								NetTiersSection.Providers[i].Parameters["connectionStringName"] = _connectionStringName;
								// remove previous connection string, if any
								NetTiersSection.Providers[i].Parameters.Remove("connectionString");

								if ( !String.IsNullOrEmpty(_connectionString) )
								{
									NetTiersSection.Providers[i].Parameters["connectionString"] = _connectionString;
								}
							}

							// Load registered providers and point _provider to the default provider
							_providers = new NetTiersProviderCollection();

							ProvidersHelper.InstantiateProviders(NetTiersSection.Providers, _providers, typeof(NetTiersProvider));
							_provider = _providers[NetTiersSection.DefaultProvider];
						}
					}
				}
			}
		}

		#endregion Connections

		#region Static properties
		
		#region PackageMasterProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="PackageMaster"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static PackageMasterProviderBase PackageMasterProvider
		{
			get 
			{
				LoadProviders();
				return _provider.PackageMasterProvider;
			}
		}
		
		#endregion
		
		#region PackageByHotelProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="PackageByHotel"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static PackageByHotelProviderBase PackageByHotelProvider
		{
			get 
			{
				LoadProviders();
				return _provider.PackageByHotelProvider;
			}
		}
		
		#endregion
		
		#region PackageItemsProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="PackageItems"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static PackageItemsProviderBase PackageItemsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.PackageItemsProvider;
			}
		}
		
		#endregion
		
		#region ActualPackagePriceProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ActualPackagePrice"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ActualPackagePriceProviderBase ActualPackagePriceProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ActualPackagePriceProvider;
			}
		}
		
		#endregion
		
		#region PackageByHotelTrailProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="PackageByHotelTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static PackageByHotelTrailProviderBase PackageByHotelTrailProvider
		{
			get 
			{
				LoadProviders();
				return _provider.PackageByHotelTrailProvider;
			}
		}
		
		#endregion
		
		#region OthersProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Others"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static OthersProviderBase OthersProvider
		{
			get 
			{
				LoadProviders();
				return _provider.OthersProvider;
			}
		}
		
		#endregion
		
		#region OtherItemsPricingbyHotelProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="OtherItemsPricingbyHotel"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static OtherItemsPricingbyHotelProviderBase OtherItemsPricingbyHotelProvider
		{
			get 
			{
				LoadProviders();
				return _provider.OtherItemsPricingbyHotelProvider;
			}
		}
		
		#endregion
		
		#region PackageDescriptionProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="PackageDescription"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static PackageDescriptionProviderBase PackageDescriptionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.PackageDescriptionProvider;
			}
		}
		
		#endregion
		
		#region PolicyHotelMappingProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="PolicyHotelMapping"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static PolicyHotelMappingProviderBase PolicyHotelMappingProvider
		{
			get 
			{
				LoadProviders();
				return _provider.PolicyHotelMappingProvider;
			}
		}
		
		#endregion
		
		#region PackageItemMappingProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="PackageItemMapping"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static PackageItemMappingProviderBase PackageItemMappingProvider
		{
			get 
			{
				LoadProviders();
				return _provider.PackageItemMappingProvider;
			}
		}
		
		#endregion
		
		#region PermissionsProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Permissions"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static PermissionsProviderBase PermissionsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.PermissionsProvider;
			}
		}
		
		#endregion
		
		#region OtherDescriptionLanguageProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="OtherDescriptionLanguage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static OtherDescriptionLanguageProviderBase OtherDescriptionLanguageProvider
		{
			get 
			{
				LoadProviders();
				return _provider.OtherDescriptionLanguageProvider;
			}
		}
		
		#endregion
		
		#region PackageMasterDescriptionProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="PackageMasterDescription"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static PackageMasterDescriptionProviderBase PackageMasterDescriptionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.PackageMasterDescriptionProvider;
			}
		}
		
		#endregion
		
		#region NewsLetterSubscriberProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="NewsLetterSubscriber"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static NewsLetterSubscriberProviderBase NewsLetterSubscriberProvider
		{
			get 
			{
				LoadProviders();
				return _provider.NewsLetterSubscriberProvider;
			}
		}
		
		#endregion
		
		#region MeetingRoomProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="MeetingRoom"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static MeetingRoomProviderBase MeetingRoomProvider
		{
			get 
			{
				LoadProviders();
				return _provider.MeetingRoomProvider;
			}
		}
		
		#endregion
		
		#region MainPointProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="MainPoint"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static MainPointProviderBase MainPointProvider
		{
			get 
			{
				LoadProviders();
				return _provider.MainPointProvider;
			}
		}
		
		#endregion
		
		#region LeftMenuLinkProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="LeftMenuLink"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static LeftMenuLinkProviderBase LeftMenuLinkProvider
		{
			get 
			{
				LoadProviders();
				return _provider.LeftMenuLinkProvider;
			}
		}
		
		#endregion
		
		#region LanguageProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Language"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static LanguageProviderBase LanguageProvider
		{
			get 
			{
				LoadProviders();
				return _provider.LanguageProvider;
			}
		}
		
		#endregion
		
		#region MeetingRoomTrailProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="MeetingRoomTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static MeetingRoomTrailProviderBase MeetingRoomTrailProvider
		{
			get 
			{
				LoadProviders();
				return _provider.MeetingRoomTrailProvider;
			}
		}
		
		#endregion
		
		#region MeetingRoomConfigProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="MeetingRoomConfig"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static MeetingRoomConfigProviderBase MeetingRoomConfigProvider
		{
			get 
			{
				LoadProviders();
				return _provider.MeetingRoomConfigProvider;
			}
		}
		
		#endregion
		
		#region MeetingRoomConfigTrailProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="MeetingRoomConfigTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static MeetingRoomConfigTrailProviderBase MeetingRoomConfigTrailProvider
		{
			get 
			{
				LoadProviders();
				return _provider.MeetingRoomConfigTrailProvider;
			}
		}
		
		#endregion
		
		#region MeetingRoomDescProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="MeetingRoomDesc"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static MeetingRoomDescProviderBase MeetingRoomDescProvider
		{
			get 
			{
				LoadProviders();
				return _provider.MeetingRoomDescProvider;
			}
		}
		
		#endregion
		
		#region MeetingRoomPictureVideoTrailProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="MeetingRoomPictureVideoTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static MeetingRoomPictureVideoTrailProviderBase MeetingRoomPictureVideoTrailProvider
		{
			get 
			{
				LoadProviders();
				return _provider.MeetingRoomPictureVideoTrailProvider;
			}
		}
		
		#endregion
		
		#region MeetingRoomPictureVideoProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="MeetingRoomPictureVideo"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static MeetingRoomPictureVideoProviderBase MeetingRoomPictureVideoProvider
		{
			get 
			{
				LoadProviders();
				return _provider.MeetingRoomPictureVideoProvider;
			}
		}
		
		#endregion
		
		#region PriorityBasketProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="PriorityBasket"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static PriorityBasketProviderBase PriorityBasketProvider
		{
			get 
			{
				LoadProviders();
				return _provider.PriorityBasketProvider;
			}
		}
		
		#endregion
		
		#region MeetingRoomDescTrailProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="MeetingRoomDescTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static MeetingRoomDescTrailProviderBase MeetingRoomDescTrailProvider
		{
			get 
			{
				LoadProviders();
				return _provider.MeetingRoomDescTrailProvider;
			}
		}
		
		#endregion
		
		#region RankingAlgoMasterProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="RankingAlgoMaster"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static RankingAlgoMasterProviderBase RankingAlgoMasterProvider
		{
			get 
			{
				LoadProviders();
				return _provider.RankingAlgoMasterProvider;
			}
		}
		
		#endregion
		
		#region RankingAlgoConditionProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="RankingAlgoCondition"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static RankingAlgoConditionProviderBase RankingAlgoConditionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.RankingAlgoConditionProvider;
			}
		}
		
		#endregion
		
		#region UsersProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Users"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static UsersProviderBase UsersProvider
		{
			get 
			{
				LoadProviders();
				return _provider.UsersProvider;
			}
		}
		
		#endregion
		
		#region StaticLebelProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="StaticLebel"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static StaticLebelProviderBase StaticLebelProvider
		{
			get 
			{
				LoadProviders();
				return _provider.StaticLebelProvider;
			}
		}
		
		#endregion
		
		#region StaticLebelDescriptionProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="StaticLebelDescription"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static StaticLebelDescriptionProviderBase StaticLebelDescriptionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.StaticLebelDescriptionProvider;
			}
		}
		
		#endregion
		
		#region StaffAccountProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="StaffAccount"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static StaffAccountProviderBase StaffAccountProvider
		{
			get 
			{
				LoadProviders();
				return _provider.StaffAccountProvider;
			}
		}
		
		#endregion
		
		#region StatisticsProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Statistics"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static StatisticsProviderBase StatisticsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.StatisticsProvider;
			}
		}
		
		#endregion
		
		#region SpecialPriceForBedroomProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SpecialPriceForBedroom"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SpecialPriceForBedroomProviderBase SpecialPriceForBedroomProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SpecialPriceForBedroomProvider;
			}
		}
		
		#endregion
		
		#region ZoneProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Zone"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ZoneProviderBase ZoneProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ZoneProvider;
			}
		}
		
		#endregion
		
		#region TransferProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Transfer"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TransferProviderBase TransferProvider
		{
			get 
			{
				LoadProviders();
				return _provider.TransferProvider;
			}
		}
		
		#endregion
		
		#region WhiteLabelProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="WhiteLabel"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static WhiteLabelProviderBase WhiteLabelProvider
		{
			get 
			{
				LoadProviders();
				return _provider.WhiteLabelProvider;
			}
		}
		
		#endregion
		
		#region WhiteLabelMappingWithHotelProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="WhiteLabelMappingWithHotel"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static WhiteLabelMappingWithHotelProviderBase WhiteLabelMappingWithHotelProvider
		{
			get 
			{
				LoadProviders();
				return _provider.WhiteLabelMappingWithHotelProvider;
			}
		}
		
		#endregion
		
		#region UserDetailsProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="UserDetails"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static UserDetailsProviderBase UserDetailsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.UserDetailsProvider;
			}
		}
		
		#endregion
		
		#region SpecialPriceAndPromoTrailProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SpecialPriceAndPromoTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SpecialPriceAndPromoTrailProviderBase SpecialPriceAndPromoTrailProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SpecialPriceAndPromoTrailProvider;
			}
		}
		
		#endregion
		
		#region SpecialPriceAndPromoProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SpecialPriceAndPromo"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SpecialPriceAndPromoProviderBase SpecialPriceAndPromoProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SpecialPriceAndPromoProvider;
			}
		}
		
		#endregion
		
		#region SpandPpackageProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SpandPpackage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SpandPpackageProviderBase SpandPpackageProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SpandPpackageProvider;
			}
		}
		
		#endregion
		
		#region ServayDescriptionProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ServayDescription"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ServayDescriptionProviderBase ServayDescriptionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ServayDescriptionProvider;
			}
		}
		
		#endregion
		
		#region SearchTracerProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SearchTracer"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SearchTracerProviderBase SearchTracerProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SearchTracerProvider;
			}
		}
		
		#endregion
		
		#region RpfProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Rpf"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static RpfProviderBase RpfProvider
		{
			get 
			{
				LoadProviders();
				return _provider.RpfProvider;
			}
		}
		
		#endregion
		
		#region RolesProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Roles"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static RolesProviderBase RolesProvider
		{
			get 
			{
				LoadProviders();
				return _provider.RolesProvider;
			}
		}
		
		#endregion
		
		#region ServeyAnswerProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ServeyAnswer"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ServeyAnswerProviderBase ServeyAnswerProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ServeyAnswerProvider;
			}
		}
		
		#endregion
		
		#region ServeyAnswerDescriptionProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ServeyAnswerDescription"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ServeyAnswerDescriptionProviderBase ServeyAnswerDescriptionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ServeyAnswerDescriptionProvider;
			}
		}
		
		#endregion
		
		#region SpandPbedroomProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SpandPbedroom"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SpandPbedroomProviderBase SpandPbedroomProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SpandPbedroomProvider;
			}
		}
		
		#endregion
		
		#region SpandPmeetingRoomProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SpandPmeetingRoom"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SpandPmeetingRoomProviderBase SpandPmeetingRoomProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SpandPmeetingRoomProvider;
			}
		}
		
		#endregion
		
		#region ServeyQuestionProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ServeyQuestion"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ServeyQuestionProviderBase ServeyQuestionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ServeyQuestionProvider;
			}
		}
		
		#endregion
		
		#region ServeyResponseProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ServeyResponse"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ServeyResponseProviderBase ServeyResponseProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ServeyResponseProvider;
			}
		}
		
		#endregion
		
		#region InvoiceCommissionProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="InvoiceCommission"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static InvoiceCommissionProviderBase InvoiceCommissionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.InvoiceCommissionProvider;
			}
		}
		
		#endregion
		
		#region ServeyresultProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Serveyresult"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ServeyresultProviderBase ServeyresultProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ServeyresultProvider;
			}
		}
		
		#endregion
		
		#region CurrencyProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Currency"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CurrencyProviderBase CurrencyProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CurrencyProvider;
			}
		}
		
		#endregion
		
		#region BedRoomProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="BedRoom"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static BedRoomProviderBase BedRoomProvider
		{
			get 
			{
				LoadProviders();
				return _provider.BedRoomProvider;
			}
		}
		
		#endregion
		
		#region BookingProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Booking"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static BookingProviderBase BookingProvider
		{
			get 
			{
				LoadProviders();
				return _provider.BookingProvider;
			}
		}
		
		#endregion
		
		#region BedRoomPictureImageProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="BedRoomPictureImage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static BedRoomPictureImageProviderBase BedRoomPictureImageProvider
		{
			get 
			{
				LoadProviders();
				return _provider.BedRoomPictureImageProvider;
			}
		}
		
		#endregion
		
		#region CountryProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Country"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CountryProviderBase CountryProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CountryProvider;
			}
		}
		
		#endregion
		
		#region BookingLogsProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="BookingLogs"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static BookingLogsProviderBase BookingLogsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.BookingLogsProvider;
			}
		}
		
		#endregion
		
		#region CmsProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Cms"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CmsProviderBase CmsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CmsProvider;
			}
		}
		
		#endregion
		
		#region CancelationPolicyProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CancelationPolicy"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CancelationPolicyProviderBase CancelationPolicyProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CancelationPolicyProvider;
			}
		}
		
		#endregion
		
		#region CityLanguageProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CityLanguage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CityLanguageProviderBase CityLanguageProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CityLanguageProvider;
			}
		}
		
		#endregion
		
		#region BookedMeetingRoomProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="BookedMeetingRoom"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static BookedMeetingRoomProviderBase BookedMeetingRoomProvider
		{
			get 
			{
				LoadProviders();
				return _provider.BookedMeetingRoomProvider;
			}
		}
		
		#endregion
		
		#region BuildMeetingConfigureProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="BuildMeetingConfigure"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static BuildMeetingConfigureProviderBase BuildMeetingConfigureProvider
		{
			get 
			{
				LoadProviders();
				return _provider.BuildMeetingConfigureProvider;
			}
		}
		
		#endregion
		
		#region CancelationPolicyLanguageProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CancelationPolicyLanguage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CancelationPolicyLanguageProviderBase CancelationPolicyLanguageProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CancelationPolicyLanguageProvider;
			}
		}
		
		#endregion
		
		#region BuildPackageConfigureProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="BuildPackageConfigure"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static BuildPackageConfigureProviderBase BuildPackageConfigureProvider
		{
			get 
			{
				LoadProviders();
				return _provider.BuildPackageConfigureProvider;
			}
		}
		
		#endregion
		
		#region BuildPackageConfigureDescProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="BuildPackageConfigureDesc"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static BuildPackageConfigureDescProviderBase BuildPackageConfigureDescProvider
		{
			get 
			{
				LoadProviders();
				return _provider.BuildPackageConfigureDescProvider;
			}
		}
		
		#endregion
		
		#region CityProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="City"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CityProviderBase CityProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CityProvider;
			}
		}
		
		#endregion
		
		#region AgentUserProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AgentUser"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AgentUserProviderBase AgentUserProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AgentUserProvider;
			}
		}
		
		#endregion
		
		#region BookedBedRoomProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="BookedBedRoom"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static BookedBedRoomProviderBase BookedBedRoomProvider
		{
			get 
			{
				LoadProviders();
				return _provider.BookedBedRoomProvider;
			}
		}
		
		#endregion
		
		#region AgencyClientProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AgencyClient"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AgencyClientProviderBase AgencyClientProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AgencyClientProvider;
			}
		}
		
		#endregion
		
		#region AgencyInvoiceProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AgencyInvoice"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AgencyInvoiceProviderBase AgencyInvoiceProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AgencyInvoiceProvider;
			}
		}
		
		#endregion
		
		#region InvoiceProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Invoice"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static InvoiceProviderBase InvoiceProvider
		{
			get 
			{
				LoadProviders();
				return _provider.InvoiceProvider;
			}
		}
		
		#endregion
		
		#region AvailabilityProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Availability"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AvailabilityProviderBase AvailabilityProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AvailabilityProvider;
			}
		}
		
		#endregion
		
		#region AvailabilityTrailProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AvailabilityTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AvailabilityTrailProviderBase AvailabilityTrailProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AvailabilityTrailProvider;
			}
		}
		
		#endregion
		
		#region BedRoomPictureImageTrailProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="BedRoomPictureImageTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static BedRoomPictureImageTrailProviderBase BedRoomPictureImageTrailProvider
		{
			get 
			{
				LoadProviders();
				return _provider.BedRoomPictureImageTrailProvider;
			}
		}
		
		#endregion
		
		#region AvailibilityOfRoomsProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AvailibilityOfRooms"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AvailibilityOfRoomsProviderBase AvailibilityOfRoomsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AvailibilityOfRoomsProvider;
			}
		}
		
		#endregion
		
		#region BedRoomDescProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="BedRoomDesc"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static BedRoomDescProviderBase BedRoomDescProvider
		{
			get 
			{
				LoadProviders();
				return _provider.BedRoomDescProvider;
			}
		}
		
		#endregion
		
		#region BedRoomDescTrailProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="BedRoomDescTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static BedRoomDescTrailProviderBase BedRoomDescTrailProvider
		{
			get 
			{
				LoadProviders();
				return _provider.BedRoomDescTrailProvider;
			}
		}
		
		#endregion
		
		#region BedRoomTrailProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="BedRoomTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static BedRoomTrailProviderBase BedRoomTrailProvider
		{
			get 
			{
				LoadProviders();
				return _provider.BedRoomTrailProvider;
			}
		}
		
		#endregion
		
		#region CmsDescriptionProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CmsDescription"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CmsDescriptionProviderBase CmsDescriptionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CmsDescriptionProvider;
			}
		}
		
		#endregion
		
		#region CommissionPercentageProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CommissionPercentage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CommissionPercentageProviderBase CommissionPercentageProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CommissionPercentageProvider;
			}
		}
		
		#endregion
		
		#region HotelProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Hotel"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static HotelProviderBase HotelProvider
		{
			get 
			{
				LoadProviders();
				return _provider.HotelProvider;
			}
		}
		
		#endregion
		
		#region ZoneLanguageProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ZoneLanguage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ZoneLanguageProviderBase ZoneLanguageProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ZoneLanguageProvider;
			}
		}
		
		#endregion
		
		#region HotelContactProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="HotelContact"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static HotelContactProviderBase HotelContactProvider
		{
			get 
			{
				LoadProviders();
				return _provider.HotelContactProvider;
			}
		}
		
		#endregion
		
		#region HotelTrailProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="HotelTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static HotelTrailProviderBase HotelTrailProvider
		{
			get 
			{
				LoadProviders();
				return _provider.HotelTrailProvider;
			}
		}
		
		#endregion
		
		#region FrontEndBottomProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="FrontEndBottom"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static FrontEndBottomProviderBase FrontEndBottomProvider
		{
			get 
			{
				LoadProviders();
				return _provider.FrontEndBottomProvider;
			}
		}
		
		#endregion
		
		#region HotelContactTrailProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="HotelContactTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static HotelContactTrailProviderBase HotelContactTrailProvider
		{
			get 
			{
				LoadProviders();
				return _provider.HotelContactTrailProvider;
			}
		}
		
		#endregion
		
		#region HotelDescProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="HotelDesc"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static HotelDescProviderBase HotelDescProvider
		{
			get 
			{
				LoadProviders();
				return _provider.HotelDescProvider;
			}
		}
		
		#endregion
		
		#region HoteldescTrailProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="HoteldescTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static HoteldescTrailProviderBase HoteldescTrailProvider
		{
			get 
			{
				LoadProviders();
				return _provider.HoteldescTrailProvider;
			}
		}
		
		#endregion
		
		#region HotelPhotoVideoGallaryProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="HotelPhotoVideoGallary"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static HotelPhotoVideoGallaryProviderBase HotelPhotoVideoGallaryProvider
		{
			get 
			{
				LoadProviders();
				return _provider.HotelPhotoVideoGallaryProvider;
			}
		}
		
		#endregion
		
		#region HotelOwnerLinkProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="HotelOwnerLink"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static HotelOwnerLinkProviderBase HotelOwnerLinkProvider
		{
			get 
			{
				LoadProviders();
				return _provider.HotelOwnerLinkProvider;
			}
		}
		
		#endregion
		
		#region HotelFacilitiesTrailProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="HotelFacilitiesTrail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static HotelFacilitiesTrailProviderBase HotelFacilitiesTrailProvider
		{
			get 
			{
				LoadProviders();
				return _provider.HotelFacilitiesTrailProvider;
			}
		}
		
		#endregion
		
		#region HotelFacilitiesProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="HotelFacilities"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static HotelFacilitiesProviderBase HotelFacilitiesProvider
		{
			get 
			{
				LoadProviders();
				return _provider.HotelFacilitiesProvider;
			}
		}
		
		#endregion
		
		#region HearUsQuestionProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="HearUsQuestion"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static HearUsQuestionProviderBase HearUsQuestionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.HearUsQuestionProvider;
			}
		}
		
		#endregion
		
		#region GroupProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Group"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static GroupProviderBase GroupProvider
		{
			get 
			{
				LoadProviders();
				return _provider.GroupProvider;
			}
		}
		
		#endregion
		
		#region FrontEndBottomDescriptionProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="FrontEndBottomDescription"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static FrontEndBottomDescriptionProviderBase FrontEndBottomDescriptionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.FrontEndBottomDescriptionProvider;
			}
		}
		
		#endregion
		
		#region DashboardLinkProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="DashboardLink"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static DashboardLinkProviderBase DashboardLinkProvider
		{
			get 
			{
				LoadProviders();
				return _provider.DashboardLinkProvider;
			}
		}
		
		#endregion
		
		#region CurrencyExchangeLogsProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CurrencyExchangeLogs"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CurrencyExchangeLogsProviderBase CurrencyExchangeLogsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CurrencyExchangeLogsProvider;
			}
		}
		
		#endregion
		
		#region CreditCardDetailProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CreditCardDetail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CreditCardDetailProviderBase CreditCardDetailProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CreditCardDetailProvider;
			}
		}
		
		#endregion
		
		#region CountryLanguageProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CountryLanguage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CountryLanguageProviderBase CountryLanguageProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CountryLanguageProvider;
			}
		}
		
		#endregion
		
		#region DbAuditProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="DbAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static DbAuditProviderBase DbAuditProvider
		{
			get 
			{
				LoadProviders();
				return _provider.DbAuditProvider;
			}
		}
		
		#endregion
		
		#region EmailConfigProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="EmailConfig"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static EmailConfigProviderBase EmailConfigProvider
		{
			get 
			{
				LoadProviders();
				return _provider.EmailConfigProvider;
			}
		}
		
		#endregion
		
		#region FinancialInfoProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="FinancialInfo"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static FinancialInfoProviderBase FinancialInfoProvider
		{
			get 
			{
				LoadProviders();
				return _provider.FinancialInfoProvider;
			}
		}
		
		#endregion
		
		#region FacilityTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="FacilityType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static FacilityTypeProviderBase FacilityTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.FacilityTypeProvider;
			}
		}
		
		#endregion
		
		#region FacilityProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Facility"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static FacilityProviderBase FacilityProvider
		{
			get 
			{
				LoadProviders();
				return _provider.FacilityProvider;
			}
		}
		
		#endregion
		
		#region EmailConfigMappingProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="EmailConfigMapping"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static EmailConfigMappingProviderBase EmailConfigMappingProvider
		{
			get 
			{
				LoadProviders();
				return _provider.EmailConfigMappingProvider;
			}
		}
		
		#endregion
		
		#region InvoiceCommPercentageProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="InvoiceCommPercentage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static InvoiceCommPercentageProviderBase InvoiceCommPercentageProvider
		{
			get 
			{
				LoadProviders();
				return _provider.InvoiceCommPercentageProvider;
			}
		}
		
		#endregion
		
		
		#region AvailabilityWithSpandpProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AvailabilityWithSpandp"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AvailabilityWithSpandpProviderBase AvailabilityWithSpandpProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AvailabilityWithSpandpProvider;
			}
		}
		
		#endregion
		
		#region BookingRequestViewListProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="BookingRequestViewList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static BookingRequestViewListProviderBase BookingRequestViewListProvider
		{
			get 
			{
				LoadProviders();
				return _provider.BookingRequestViewListProvider;
			}
		}
		
		#endregion
		
		#region ViewAvailabilityAndSpecialManagerProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewAvailabilityAndSpecialManager"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewAvailabilityAndSpecialManagerProviderBase ViewAvailabilityAndSpecialManagerProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewAvailabilityAndSpecialManagerProvider;
			}
		}
		
		#endregion
		
		#region ViewAvailabilityOfBedRoomProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewAvailabilityOfBedRoom"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewAvailabilityOfBedRoomProviderBase ViewAvailabilityOfBedRoomProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewAvailabilityOfBedRoomProvider;
			}
		}
		
		#endregion
		
		#region ViewAvailabilityOfRoomsProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewAvailabilityOfRooms"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewAvailabilityOfRoomsProviderBase ViewAvailabilityOfRoomsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewAvailabilityOfRoomsProvider;
			}
		}
		
		#endregion
		
		#region ViewBookingHotelsProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewBookingHotels"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewBookingHotelsProviderBase ViewBookingHotelsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewBookingHotelsProvider;
			}
		}
		
		#endregion
		
		#region ViewbookingrequestProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Viewbookingrequest"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewbookingrequestProviderBase ViewbookingrequestProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewbookingrequestProvider;
			}
		}
		
		#endregion
		
		#region ViewClientContractProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewClientContract"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewClientContractProviderBase ViewClientContractProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewClientContractProvider;
			}
		}
		
		#endregion
		
		#region ViewFacilitiesProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewFacilities"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewFacilitiesProviderBase ViewFacilitiesProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewFacilitiesProvider;
			}
		}
		
		#endregion
		
		#region ViewFacilityIconProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewFacilityIcon"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewFacilityIconProviderBase ViewFacilityIconProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewFacilityIconProvider;
			}
		}
		
		#endregion
		
		#region ViewFindAvailableBedroomProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewFindAvailableBedroom"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewFindAvailableBedroomProviderBase ViewFindAvailableBedroomProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewFindAvailableBedroomProvider;
			}
		}
		
		#endregion
		
		#region ViewForAvailabilityCheckProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewForAvailabilityCheck"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewForAvailabilityCheckProviderBase ViewForAvailabilityCheckProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewForAvailabilityCheckProvider;
			}
		}
		
		#endregion
		
		#region ViewForCommissionFieldsProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewForCommissionFields"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewForCommissionFieldsProviderBase ViewForCommissionFieldsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewForCommissionFieldsProvider;
			}
		}
		
		#endregion
		
		#region ViewForRequestSearchProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewForRequestSearch"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewForRequestSearchProviderBase ViewForRequestSearchProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewForRequestSearchProvider;
			}
		}
		
		#endregion
		
		#region ViewForSetInvoiceProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewForSetInvoice"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewForSetInvoiceProviderBase ViewForSetInvoiceProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewForSetInvoiceProvider;
			}
		}
		
		#endregion
		
		#region ViewMeetingRoomAvailabilityProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewMeetingRoomAvailability"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewMeetingRoomAvailabilityProviderBase ViewMeetingRoomAvailabilityProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewMeetingRoomAvailabilityProvider;
			}
		}
		
		#endregion
		
		#region ViewMeetingRoomForRequestProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewMeetingRoomForRequest"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewMeetingRoomForRequestProviderBase ViewMeetingRoomForRequestProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewMeetingRoomForRequestProvider;
			}
		}
		
		#endregion
		
		#region ViewReportAvailabilityProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewReportAvailability"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewReportAvailabilityProviderBase ViewReportAvailabilityProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewReportAvailabilityProvider;
			}
		}
		
		#endregion
		
		#region ViewReportBookingRequestHistoryProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewReportBookingRequestHistory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewReportBookingRequestHistoryProviderBase ViewReportBookingRequestHistoryProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewReportBookingRequestHistoryProvider;
			}
		}
		
		#endregion
		
		#region ViewReportCancelationProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewReportCancelation"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewReportCancelationProviderBase ViewReportCancelationProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewReportCancelationProvider;
			}
		}
		
		#endregion
		
		#region ViewReportCommissionControlProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewReportCommissionControl"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewReportCommissionControlProviderBase ViewReportCommissionControlProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewReportCommissionControlProvider;
			}
		}
		
		#endregion
		
		#region ViewReportforProfileCompanyAgencyProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewReportforProfileCompanyAgency"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewReportforProfileCompanyAgencyProviderBase ViewReportforProfileCompanyAgencyProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewReportforProfileCompanyAgencyProvider;
			}
		}
		
		#endregion
		
		#region ViewReportInventoryHotelProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewReportInventoryHotel"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewReportInventoryHotelProviderBase ViewReportInventoryHotelProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewReportInventoryHotelProvider;
			}
		}
		
		#endregion
		
		#region ViewreportInventoryReportofExistingHotelProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewreportInventoryReportofExistingHotel"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewreportInventoryReportofExistingHotelProviderBase ViewreportInventoryReportofExistingHotelProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewreportInventoryReportofExistingHotelProvider;
			}
		}
		
		#endregion
		
		#region ViewReportLeadTimeProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewReportLeadTime"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewReportLeadTimeProviderBase ViewReportLeadTimeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewReportLeadTimeProvider;
			}
		}
		
		#endregion
		
		#region ViewReportOnLineInventoryReportProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewReportOnLineInventoryReport"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewReportOnLineInventoryReportProviderBase ViewReportOnLineInventoryReportProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewReportOnLineInventoryReportProvider;
			}
		}
		
		#endregion
		
		#region ViewReportProductionCompanyAgencyProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewReportProductionCompanyAgency"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewReportProductionCompanyAgencyProviderBase ViewReportProductionCompanyAgencyProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewReportProductionCompanyAgencyProvider;
			}
		}
		
		#endregion
		
		#region ViewReportProductionReportHotelProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewReportProductionReportHotel"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewReportProductionReportHotelProviderBase ViewReportProductionReportHotelProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewReportProductionReportHotelProvider;
			}
		}
		
		#endregion
		
		#region ViewReportRankingProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewReportRanking"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewReportRankingProviderBase ViewReportRankingProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewReportRankingProvider;
			}
		}
		
		#endregion
		
		#region ViewRequestProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewRequest"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewRequestProviderBase ViewRequestProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewRequestProvider;
			}
		}
		
		#endregion
		
		#region ViewSearchForDay2Provider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewSearchForDay2"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewSearchForDay2ProviderBase ViewSearchForDay2Provider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewSearchForDay2Provider;
			}
		}
		
		#endregion
		
		#region ViewSelectAllDetailsByPackageandMrProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewSelectAllDetailsByPackageandMr"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewSelectAllDetailsByPackageandMrProviderBase ViewSelectAllDetailsByPackageandMrProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewSelectAllDetailsByPackageandMrProvider;
			}
		}
		
		#endregion
		
		#region ViewSelectAvailabilityProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewSelectAvailability"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewSelectAvailabilityProviderBase ViewSelectAvailabilityProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewSelectAvailabilityProvider;
			}
		}
		
		#endregion
		
		#region ViewSpandPpercentageOfMeetingRoomProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewSpandPpercentageOfMeetingRoom"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewSpandPpercentageOfMeetingRoomProviderBase ViewSpandPpercentageOfMeetingRoomProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewSpandPpercentageOfMeetingRoomProvider;
			}
		}
		
		#endregion
		
		#region ViewSpandPpercentageOfPackageProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewSpandPpercentageOfPackage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewSpandPpercentageOfPackageProviderBase ViewSpandPpercentageOfPackageProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewSpandPpercentageOfPackageProvider;
			}
		}
		
		#endregion
		
		#region ViewWsCheckAvailabilityProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ViewWsCheckAvailability"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ViewWsCheckAvailabilityProviderBase ViewWsCheckAvailabilityProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ViewWsCheckAvailabilityProvider;
			}
		}
		
		#endregion
		
		#endregion
	}
	
	#region Query/Filters
		
	#region PackageMasterFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PackageMaster"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PackageMasterFilters : PackageMasterFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageMasterFilters class.
		/// </summary>
		public PackageMasterFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the PackageMasterFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PackageMasterFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PackageMasterFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PackageMasterFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PackageMasterFilters
	
	#region PackageMasterQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="PackageMasterParameterBuilder"/> class
	/// that is used exclusively with a <see cref="PackageMaster"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PackageMasterQuery : PackageMasterParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageMasterQuery class.
		/// </summary>
		public PackageMasterQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the PackageMasterQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PackageMasterQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PackageMasterQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PackageMasterQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PackageMasterQuery
		
	#region PackageByHotelFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PackageByHotel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PackageByHotelFilters : PackageByHotelFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageByHotelFilters class.
		/// </summary>
		public PackageByHotelFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the PackageByHotelFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PackageByHotelFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PackageByHotelFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PackageByHotelFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PackageByHotelFilters
	
	#region PackageByHotelQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="PackageByHotelParameterBuilder"/> class
	/// that is used exclusively with a <see cref="PackageByHotel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PackageByHotelQuery : PackageByHotelParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageByHotelQuery class.
		/// </summary>
		public PackageByHotelQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the PackageByHotelQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PackageByHotelQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PackageByHotelQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PackageByHotelQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PackageByHotelQuery
		
	#region PackageItemsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PackageItems"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PackageItemsFilters : PackageItemsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageItemsFilters class.
		/// </summary>
		public PackageItemsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the PackageItemsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PackageItemsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PackageItemsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PackageItemsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PackageItemsFilters
	
	#region PackageItemsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="PackageItemsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="PackageItems"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PackageItemsQuery : PackageItemsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageItemsQuery class.
		/// </summary>
		public PackageItemsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the PackageItemsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PackageItemsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PackageItemsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PackageItemsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PackageItemsQuery
		
	#region ActualPackagePriceFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ActualPackagePrice"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ActualPackagePriceFilters : ActualPackagePriceFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ActualPackagePriceFilters class.
		/// </summary>
		public ActualPackagePriceFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ActualPackagePriceFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ActualPackagePriceFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ActualPackagePriceFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ActualPackagePriceFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ActualPackagePriceFilters
	
	#region ActualPackagePriceQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ActualPackagePriceParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ActualPackagePrice"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ActualPackagePriceQuery : ActualPackagePriceParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ActualPackagePriceQuery class.
		/// </summary>
		public ActualPackagePriceQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ActualPackagePriceQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ActualPackagePriceQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ActualPackagePriceQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ActualPackagePriceQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ActualPackagePriceQuery
		
	#region PackageByHotelTrailFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PackageByHotelTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PackageByHotelTrailFilters : PackageByHotelTrailFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageByHotelTrailFilters class.
		/// </summary>
		public PackageByHotelTrailFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the PackageByHotelTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PackageByHotelTrailFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PackageByHotelTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PackageByHotelTrailFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PackageByHotelTrailFilters
	
	#region PackageByHotelTrailQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="PackageByHotelTrailParameterBuilder"/> class
	/// that is used exclusively with a <see cref="PackageByHotelTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PackageByHotelTrailQuery : PackageByHotelTrailParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageByHotelTrailQuery class.
		/// </summary>
		public PackageByHotelTrailQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the PackageByHotelTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PackageByHotelTrailQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PackageByHotelTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PackageByHotelTrailQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PackageByHotelTrailQuery
		
	#region OthersFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Others"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OthersFilters : OthersFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OthersFilters class.
		/// </summary>
		public OthersFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the OthersFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OthersFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OthersFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OthersFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OthersFilters
	
	#region OthersQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="OthersParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Others"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OthersQuery : OthersParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OthersQuery class.
		/// </summary>
		public OthersQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the OthersQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OthersQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OthersQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OthersQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OthersQuery
		
	#region OtherItemsPricingbyHotelFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OtherItemsPricingbyHotel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OtherItemsPricingbyHotelFilters : OtherItemsPricingbyHotelFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OtherItemsPricingbyHotelFilters class.
		/// </summary>
		public OtherItemsPricingbyHotelFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the OtherItemsPricingbyHotelFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OtherItemsPricingbyHotelFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OtherItemsPricingbyHotelFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OtherItemsPricingbyHotelFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OtherItemsPricingbyHotelFilters
	
	#region OtherItemsPricingbyHotelQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="OtherItemsPricingbyHotelParameterBuilder"/> class
	/// that is used exclusively with a <see cref="OtherItemsPricingbyHotel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OtherItemsPricingbyHotelQuery : OtherItemsPricingbyHotelParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OtherItemsPricingbyHotelQuery class.
		/// </summary>
		public OtherItemsPricingbyHotelQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the OtherItemsPricingbyHotelQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OtherItemsPricingbyHotelQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OtherItemsPricingbyHotelQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OtherItemsPricingbyHotelQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OtherItemsPricingbyHotelQuery
		
	#region PackageDescriptionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PackageDescription"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PackageDescriptionFilters : PackageDescriptionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageDescriptionFilters class.
		/// </summary>
		public PackageDescriptionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the PackageDescriptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PackageDescriptionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PackageDescriptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PackageDescriptionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PackageDescriptionFilters
	
	#region PackageDescriptionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="PackageDescriptionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="PackageDescription"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PackageDescriptionQuery : PackageDescriptionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageDescriptionQuery class.
		/// </summary>
		public PackageDescriptionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the PackageDescriptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PackageDescriptionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PackageDescriptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PackageDescriptionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PackageDescriptionQuery
		
	#region PolicyHotelMappingFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PolicyHotelMapping"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PolicyHotelMappingFilters : PolicyHotelMappingFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PolicyHotelMappingFilters class.
		/// </summary>
		public PolicyHotelMappingFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the PolicyHotelMappingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PolicyHotelMappingFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PolicyHotelMappingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PolicyHotelMappingFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PolicyHotelMappingFilters
	
	#region PolicyHotelMappingQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="PolicyHotelMappingParameterBuilder"/> class
	/// that is used exclusively with a <see cref="PolicyHotelMapping"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PolicyHotelMappingQuery : PolicyHotelMappingParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PolicyHotelMappingQuery class.
		/// </summary>
		public PolicyHotelMappingQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the PolicyHotelMappingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PolicyHotelMappingQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PolicyHotelMappingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PolicyHotelMappingQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PolicyHotelMappingQuery
		
	#region PackageItemMappingFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PackageItemMapping"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PackageItemMappingFilters : PackageItemMappingFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageItemMappingFilters class.
		/// </summary>
		public PackageItemMappingFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the PackageItemMappingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PackageItemMappingFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PackageItemMappingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PackageItemMappingFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PackageItemMappingFilters
	
	#region PackageItemMappingQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="PackageItemMappingParameterBuilder"/> class
	/// that is used exclusively with a <see cref="PackageItemMapping"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PackageItemMappingQuery : PackageItemMappingParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageItemMappingQuery class.
		/// </summary>
		public PackageItemMappingQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the PackageItemMappingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PackageItemMappingQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PackageItemMappingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PackageItemMappingQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PackageItemMappingQuery
		
	#region PermissionsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Permissions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PermissionsFilters : PermissionsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PermissionsFilters class.
		/// </summary>
		public PermissionsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the PermissionsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PermissionsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PermissionsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PermissionsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PermissionsFilters
	
	#region PermissionsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="PermissionsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Permissions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PermissionsQuery : PermissionsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PermissionsQuery class.
		/// </summary>
		public PermissionsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the PermissionsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PermissionsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PermissionsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PermissionsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PermissionsQuery
		
	#region OtherDescriptionLanguageFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OtherDescriptionLanguage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OtherDescriptionLanguageFilters : OtherDescriptionLanguageFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OtherDescriptionLanguageFilters class.
		/// </summary>
		public OtherDescriptionLanguageFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the OtherDescriptionLanguageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OtherDescriptionLanguageFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OtherDescriptionLanguageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OtherDescriptionLanguageFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OtherDescriptionLanguageFilters
	
	#region OtherDescriptionLanguageQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="OtherDescriptionLanguageParameterBuilder"/> class
	/// that is used exclusively with a <see cref="OtherDescriptionLanguage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OtherDescriptionLanguageQuery : OtherDescriptionLanguageParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OtherDescriptionLanguageQuery class.
		/// </summary>
		public OtherDescriptionLanguageQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the OtherDescriptionLanguageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OtherDescriptionLanguageQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OtherDescriptionLanguageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OtherDescriptionLanguageQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OtherDescriptionLanguageQuery
		
	#region PackageMasterDescriptionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PackageMasterDescription"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PackageMasterDescriptionFilters : PackageMasterDescriptionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageMasterDescriptionFilters class.
		/// </summary>
		public PackageMasterDescriptionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the PackageMasterDescriptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PackageMasterDescriptionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PackageMasterDescriptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PackageMasterDescriptionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PackageMasterDescriptionFilters
	
	#region PackageMasterDescriptionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="PackageMasterDescriptionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="PackageMasterDescription"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PackageMasterDescriptionQuery : PackageMasterDescriptionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageMasterDescriptionQuery class.
		/// </summary>
		public PackageMasterDescriptionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the PackageMasterDescriptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PackageMasterDescriptionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PackageMasterDescriptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PackageMasterDescriptionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PackageMasterDescriptionQuery
		
	#region NewsLetterSubscriberFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="NewsLetterSubscriber"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class NewsLetterSubscriberFilters : NewsLetterSubscriberFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the NewsLetterSubscriberFilters class.
		/// </summary>
		public NewsLetterSubscriberFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the NewsLetterSubscriberFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public NewsLetterSubscriberFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the NewsLetterSubscriberFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public NewsLetterSubscriberFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion NewsLetterSubscriberFilters
	
	#region NewsLetterSubscriberQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="NewsLetterSubscriberParameterBuilder"/> class
	/// that is used exclusively with a <see cref="NewsLetterSubscriber"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class NewsLetterSubscriberQuery : NewsLetterSubscriberParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the NewsLetterSubscriberQuery class.
		/// </summary>
		public NewsLetterSubscriberQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the NewsLetterSubscriberQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public NewsLetterSubscriberQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the NewsLetterSubscriberQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public NewsLetterSubscriberQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion NewsLetterSubscriberQuery
		
	#region MeetingRoomFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomFilters : MeetingRoomFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomFilters class.
		/// </summary>
		public MeetingRoomFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomFilters
	
	#region MeetingRoomQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="MeetingRoomParameterBuilder"/> class
	/// that is used exclusively with a <see cref="MeetingRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomQuery : MeetingRoomParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomQuery class.
		/// </summary>
		public MeetingRoomQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomQuery
		
	#region MainPointFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MainPoint"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MainPointFilters : MainPointFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MainPointFilters class.
		/// </summary>
		public MainPointFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the MainPointFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MainPointFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MainPointFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MainPointFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MainPointFilters
	
	#region MainPointQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="MainPointParameterBuilder"/> class
	/// that is used exclusively with a <see cref="MainPoint"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MainPointQuery : MainPointParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MainPointQuery class.
		/// </summary>
		public MainPointQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the MainPointQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MainPointQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MainPointQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MainPointQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MainPointQuery
		
	#region LeftMenuLinkFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LeftMenuLink"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LeftMenuLinkFilters : LeftMenuLinkFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LeftMenuLinkFilters class.
		/// </summary>
		public LeftMenuLinkFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the LeftMenuLinkFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LeftMenuLinkFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LeftMenuLinkFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LeftMenuLinkFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LeftMenuLinkFilters
	
	#region LeftMenuLinkQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="LeftMenuLinkParameterBuilder"/> class
	/// that is used exclusively with a <see cref="LeftMenuLink"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LeftMenuLinkQuery : LeftMenuLinkParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LeftMenuLinkQuery class.
		/// </summary>
		public LeftMenuLinkQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the LeftMenuLinkQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LeftMenuLinkQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LeftMenuLinkQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LeftMenuLinkQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LeftMenuLinkQuery
		
	#region LanguageFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Language"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LanguageFilters : LanguageFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LanguageFilters class.
		/// </summary>
		public LanguageFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the LanguageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LanguageFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LanguageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LanguageFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LanguageFilters
	
	#region LanguageQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="LanguageParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Language"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LanguageQuery : LanguageParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LanguageQuery class.
		/// </summary>
		public LanguageQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the LanguageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LanguageQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LanguageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LanguageQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LanguageQuery
		
	#region MeetingRoomTrailFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoomTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomTrailFilters : MeetingRoomTrailFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomTrailFilters class.
		/// </summary>
		public MeetingRoomTrailFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomTrailFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomTrailFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomTrailFilters
	
	#region MeetingRoomTrailQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="MeetingRoomTrailParameterBuilder"/> class
	/// that is used exclusively with a <see cref="MeetingRoomTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomTrailQuery : MeetingRoomTrailParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomTrailQuery class.
		/// </summary>
		public MeetingRoomTrailQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomTrailQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomTrailQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomTrailQuery
		
	#region MeetingRoomConfigFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoomConfig"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomConfigFilters : MeetingRoomConfigFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigFilters class.
		/// </summary>
		public MeetingRoomConfigFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomConfigFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomConfigFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomConfigFilters
	
	#region MeetingRoomConfigQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="MeetingRoomConfigParameterBuilder"/> class
	/// that is used exclusively with a <see cref="MeetingRoomConfig"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomConfigQuery : MeetingRoomConfigParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigQuery class.
		/// </summary>
		public MeetingRoomConfigQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomConfigQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomConfigQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomConfigQuery
		
	#region MeetingRoomConfigTrailFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoomConfigTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomConfigTrailFilters : MeetingRoomConfigTrailFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigTrailFilters class.
		/// </summary>
		public MeetingRoomConfigTrailFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomConfigTrailFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomConfigTrailFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomConfigTrailFilters
	
	#region MeetingRoomConfigTrailQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="MeetingRoomConfigTrailParameterBuilder"/> class
	/// that is used exclusively with a <see cref="MeetingRoomConfigTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomConfigTrailQuery : MeetingRoomConfigTrailParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigTrailQuery class.
		/// </summary>
		public MeetingRoomConfigTrailQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomConfigTrailQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomConfigTrailQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomConfigTrailQuery
		
	#region MeetingRoomDescFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoomDesc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomDescFilters : MeetingRoomDescFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescFilters class.
		/// </summary>
		public MeetingRoomDescFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomDescFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomDescFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomDescFilters
	
	#region MeetingRoomDescQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="MeetingRoomDescParameterBuilder"/> class
	/// that is used exclusively with a <see cref="MeetingRoomDesc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomDescQuery : MeetingRoomDescParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescQuery class.
		/// </summary>
		public MeetingRoomDescQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomDescQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomDescQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomDescQuery
		
	#region MeetingRoomPictureVideoTrailFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoomPictureVideoTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomPictureVideoTrailFilters : MeetingRoomPictureVideoTrailFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomPictureVideoTrailFilters class.
		/// </summary>
		public MeetingRoomPictureVideoTrailFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomPictureVideoTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomPictureVideoTrailFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomPictureVideoTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomPictureVideoTrailFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomPictureVideoTrailFilters
	
	#region MeetingRoomPictureVideoTrailQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="MeetingRoomPictureVideoTrailParameterBuilder"/> class
	/// that is used exclusively with a <see cref="MeetingRoomPictureVideoTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomPictureVideoTrailQuery : MeetingRoomPictureVideoTrailParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomPictureVideoTrailQuery class.
		/// </summary>
		public MeetingRoomPictureVideoTrailQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomPictureVideoTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomPictureVideoTrailQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomPictureVideoTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomPictureVideoTrailQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomPictureVideoTrailQuery
		
	#region MeetingRoomPictureVideoFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoomPictureVideo"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomPictureVideoFilters : MeetingRoomPictureVideoFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomPictureVideoFilters class.
		/// </summary>
		public MeetingRoomPictureVideoFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomPictureVideoFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomPictureVideoFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomPictureVideoFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomPictureVideoFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomPictureVideoFilters
	
	#region MeetingRoomPictureVideoQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="MeetingRoomPictureVideoParameterBuilder"/> class
	/// that is used exclusively with a <see cref="MeetingRoomPictureVideo"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomPictureVideoQuery : MeetingRoomPictureVideoParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomPictureVideoQuery class.
		/// </summary>
		public MeetingRoomPictureVideoQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomPictureVideoQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomPictureVideoQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomPictureVideoQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomPictureVideoQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomPictureVideoQuery
		
	#region PriorityBasketFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PriorityBasket"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PriorityBasketFilters : PriorityBasketFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PriorityBasketFilters class.
		/// </summary>
		public PriorityBasketFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the PriorityBasketFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PriorityBasketFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PriorityBasketFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PriorityBasketFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PriorityBasketFilters
	
	#region PriorityBasketQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="PriorityBasketParameterBuilder"/> class
	/// that is used exclusively with a <see cref="PriorityBasket"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PriorityBasketQuery : PriorityBasketParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PriorityBasketQuery class.
		/// </summary>
		public PriorityBasketQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the PriorityBasketQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PriorityBasketQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PriorityBasketQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PriorityBasketQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PriorityBasketQuery
		
	#region MeetingRoomDescTrailFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoomDescTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomDescTrailFilters : MeetingRoomDescTrailFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescTrailFilters class.
		/// </summary>
		public MeetingRoomDescTrailFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomDescTrailFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomDescTrailFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomDescTrailFilters
	
	#region MeetingRoomDescTrailQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="MeetingRoomDescTrailParameterBuilder"/> class
	/// that is used exclusively with a <see cref="MeetingRoomDescTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomDescTrailQuery : MeetingRoomDescTrailParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescTrailQuery class.
		/// </summary>
		public MeetingRoomDescTrailQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomDescTrailQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomDescTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomDescTrailQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomDescTrailQuery
		
	#region RankingAlgoMasterFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RankingAlgoMaster"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RankingAlgoMasterFilters : RankingAlgoMasterFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RankingAlgoMasterFilters class.
		/// </summary>
		public RankingAlgoMasterFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the RankingAlgoMasterFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RankingAlgoMasterFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RankingAlgoMasterFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RankingAlgoMasterFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RankingAlgoMasterFilters
	
	#region RankingAlgoMasterQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="RankingAlgoMasterParameterBuilder"/> class
	/// that is used exclusively with a <see cref="RankingAlgoMaster"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RankingAlgoMasterQuery : RankingAlgoMasterParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RankingAlgoMasterQuery class.
		/// </summary>
		public RankingAlgoMasterQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the RankingAlgoMasterQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RankingAlgoMasterQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RankingAlgoMasterQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RankingAlgoMasterQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RankingAlgoMasterQuery
		
	#region RankingAlgoConditionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RankingAlgoCondition"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RankingAlgoConditionFilters : RankingAlgoConditionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RankingAlgoConditionFilters class.
		/// </summary>
		public RankingAlgoConditionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the RankingAlgoConditionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RankingAlgoConditionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RankingAlgoConditionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RankingAlgoConditionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RankingAlgoConditionFilters
	
	#region RankingAlgoConditionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="RankingAlgoConditionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="RankingAlgoCondition"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RankingAlgoConditionQuery : RankingAlgoConditionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RankingAlgoConditionQuery class.
		/// </summary>
		public RankingAlgoConditionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the RankingAlgoConditionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RankingAlgoConditionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RankingAlgoConditionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RankingAlgoConditionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RankingAlgoConditionQuery
		
	#region UsersFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Users"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersFilters : UsersFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersFilters class.
		/// </summary>
		public UsersFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersFilters
	
	#region UsersQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="UsersParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Users"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersQuery : UsersParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersQuery class.
		/// </summary>
		public UsersQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersQuery
		
	#region StaticLebelFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="StaticLebel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class StaticLebelFilters : StaticLebelFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the StaticLebelFilters class.
		/// </summary>
		public StaticLebelFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the StaticLebelFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public StaticLebelFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the StaticLebelFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public StaticLebelFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion StaticLebelFilters
	
	#region StaticLebelQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="StaticLebelParameterBuilder"/> class
	/// that is used exclusively with a <see cref="StaticLebel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class StaticLebelQuery : StaticLebelParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the StaticLebelQuery class.
		/// </summary>
		public StaticLebelQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the StaticLebelQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public StaticLebelQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the StaticLebelQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public StaticLebelQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion StaticLebelQuery
		
	#region StaticLebelDescriptionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="StaticLebelDescription"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class StaticLebelDescriptionFilters : StaticLebelDescriptionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the StaticLebelDescriptionFilters class.
		/// </summary>
		public StaticLebelDescriptionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the StaticLebelDescriptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public StaticLebelDescriptionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the StaticLebelDescriptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public StaticLebelDescriptionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion StaticLebelDescriptionFilters
	
	#region StaticLebelDescriptionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="StaticLebelDescriptionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="StaticLebelDescription"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class StaticLebelDescriptionQuery : StaticLebelDescriptionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the StaticLebelDescriptionQuery class.
		/// </summary>
		public StaticLebelDescriptionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the StaticLebelDescriptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public StaticLebelDescriptionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the StaticLebelDescriptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public StaticLebelDescriptionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion StaticLebelDescriptionQuery
		
	#region StaffAccountFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="StaffAccount"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class StaffAccountFilters : StaffAccountFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the StaffAccountFilters class.
		/// </summary>
		public StaffAccountFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the StaffAccountFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public StaffAccountFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the StaffAccountFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public StaffAccountFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion StaffAccountFilters
	
	#region StaffAccountQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="StaffAccountParameterBuilder"/> class
	/// that is used exclusively with a <see cref="StaffAccount"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class StaffAccountQuery : StaffAccountParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the StaffAccountQuery class.
		/// </summary>
		public StaffAccountQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the StaffAccountQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public StaffAccountQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the StaffAccountQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public StaffAccountQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion StaffAccountQuery
		
	#region StatisticsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Statistics"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class StatisticsFilters : StatisticsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the StatisticsFilters class.
		/// </summary>
		public StatisticsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the StatisticsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public StatisticsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the StatisticsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public StatisticsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion StatisticsFilters
	
	#region StatisticsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="StatisticsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Statistics"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class StatisticsQuery : StatisticsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the StatisticsQuery class.
		/// </summary>
		public StatisticsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the StatisticsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public StatisticsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the StatisticsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public StatisticsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion StatisticsQuery
		
	#region SpecialPriceForBedroomFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SpecialPriceForBedroom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SpecialPriceForBedroomFilters : SpecialPriceForBedroomFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SpecialPriceForBedroomFilters class.
		/// </summary>
		public SpecialPriceForBedroomFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SpecialPriceForBedroomFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SpecialPriceForBedroomFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SpecialPriceForBedroomFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SpecialPriceForBedroomFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SpecialPriceForBedroomFilters
	
	#region SpecialPriceForBedroomQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SpecialPriceForBedroomParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SpecialPriceForBedroom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SpecialPriceForBedroomQuery : SpecialPriceForBedroomParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SpecialPriceForBedroomQuery class.
		/// </summary>
		public SpecialPriceForBedroomQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SpecialPriceForBedroomQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SpecialPriceForBedroomQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SpecialPriceForBedroomQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SpecialPriceForBedroomQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SpecialPriceForBedroomQuery
		
	#region ZoneFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Zone"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ZoneFilters : ZoneFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ZoneFilters class.
		/// </summary>
		public ZoneFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ZoneFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ZoneFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ZoneFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ZoneFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ZoneFilters
	
	#region ZoneQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ZoneParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Zone"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ZoneQuery : ZoneParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ZoneQuery class.
		/// </summary>
		public ZoneQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ZoneQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ZoneQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ZoneQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ZoneQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ZoneQuery
		
	#region TransferFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Transfer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TransferFilters : TransferFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TransferFilters class.
		/// </summary>
		public TransferFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TransferFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TransferFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TransferFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TransferFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TransferFilters
	
	#region TransferQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TransferParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Transfer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TransferQuery : TransferParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TransferQuery class.
		/// </summary>
		public TransferQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the TransferQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TransferQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TransferQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TransferQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TransferQuery
		
	#region WhiteLabelFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="WhiteLabel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class WhiteLabelFilters : WhiteLabelFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the WhiteLabelFilters class.
		/// </summary>
		public WhiteLabelFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the WhiteLabelFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public WhiteLabelFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the WhiteLabelFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public WhiteLabelFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion WhiteLabelFilters
	
	#region WhiteLabelQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="WhiteLabelParameterBuilder"/> class
	/// that is used exclusively with a <see cref="WhiteLabel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class WhiteLabelQuery : WhiteLabelParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the WhiteLabelQuery class.
		/// </summary>
		public WhiteLabelQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the WhiteLabelQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public WhiteLabelQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the WhiteLabelQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public WhiteLabelQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion WhiteLabelQuery
		
	#region WhiteLabelMappingWithHotelFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="WhiteLabelMappingWithHotel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class WhiteLabelMappingWithHotelFilters : WhiteLabelMappingWithHotelFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the WhiteLabelMappingWithHotelFilters class.
		/// </summary>
		public WhiteLabelMappingWithHotelFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the WhiteLabelMappingWithHotelFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public WhiteLabelMappingWithHotelFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the WhiteLabelMappingWithHotelFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public WhiteLabelMappingWithHotelFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion WhiteLabelMappingWithHotelFilters
	
	#region WhiteLabelMappingWithHotelQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="WhiteLabelMappingWithHotelParameterBuilder"/> class
	/// that is used exclusively with a <see cref="WhiteLabelMappingWithHotel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class WhiteLabelMappingWithHotelQuery : WhiteLabelMappingWithHotelParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the WhiteLabelMappingWithHotelQuery class.
		/// </summary>
		public WhiteLabelMappingWithHotelQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the WhiteLabelMappingWithHotelQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public WhiteLabelMappingWithHotelQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the WhiteLabelMappingWithHotelQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public WhiteLabelMappingWithHotelQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion WhiteLabelMappingWithHotelQuery
		
	#region UserDetailsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UserDetails"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UserDetailsFilters : UserDetailsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UserDetailsFilters class.
		/// </summary>
		public UserDetailsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the UserDetailsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UserDetailsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UserDetailsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UserDetailsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UserDetailsFilters
	
	#region UserDetailsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="UserDetailsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="UserDetails"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UserDetailsQuery : UserDetailsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UserDetailsQuery class.
		/// </summary>
		public UserDetailsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the UserDetailsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UserDetailsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UserDetailsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UserDetailsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UserDetailsQuery
		
	#region SpecialPriceAndPromoTrailFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SpecialPriceAndPromoTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SpecialPriceAndPromoTrailFilters : SpecialPriceAndPromoTrailFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SpecialPriceAndPromoTrailFilters class.
		/// </summary>
		public SpecialPriceAndPromoTrailFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SpecialPriceAndPromoTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SpecialPriceAndPromoTrailFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SpecialPriceAndPromoTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SpecialPriceAndPromoTrailFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SpecialPriceAndPromoTrailFilters
	
	#region SpecialPriceAndPromoTrailQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SpecialPriceAndPromoTrailParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SpecialPriceAndPromoTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SpecialPriceAndPromoTrailQuery : SpecialPriceAndPromoTrailParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SpecialPriceAndPromoTrailQuery class.
		/// </summary>
		public SpecialPriceAndPromoTrailQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SpecialPriceAndPromoTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SpecialPriceAndPromoTrailQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SpecialPriceAndPromoTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SpecialPriceAndPromoTrailQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SpecialPriceAndPromoTrailQuery
		
	#region SpecialPriceAndPromoFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SpecialPriceAndPromo"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SpecialPriceAndPromoFilters : SpecialPriceAndPromoFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SpecialPriceAndPromoFilters class.
		/// </summary>
		public SpecialPriceAndPromoFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SpecialPriceAndPromoFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SpecialPriceAndPromoFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SpecialPriceAndPromoFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SpecialPriceAndPromoFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SpecialPriceAndPromoFilters
	
	#region SpecialPriceAndPromoQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SpecialPriceAndPromoParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SpecialPriceAndPromo"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SpecialPriceAndPromoQuery : SpecialPriceAndPromoParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SpecialPriceAndPromoQuery class.
		/// </summary>
		public SpecialPriceAndPromoQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SpecialPriceAndPromoQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SpecialPriceAndPromoQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SpecialPriceAndPromoQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SpecialPriceAndPromoQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SpecialPriceAndPromoQuery
		
	#region SpandPpackageFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SpandPpackage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SpandPpackageFilters : SpandPpackageFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SpandPpackageFilters class.
		/// </summary>
		public SpandPpackageFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SpandPpackageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SpandPpackageFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SpandPpackageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SpandPpackageFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SpandPpackageFilters
	
	#region SpandPpackageQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SpandPpackageParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SpandPpackage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SpandPpackageQuery : SpandPpackageParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SpandPpackageQuery class.
		/// </summary>
		public SpandPpackageQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SpandPpackageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SpandPpackageQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SpandPpackageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SpandPpackageQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SpandPpackageQuery
		
	#region ServayDescriptionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ServayDescription"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ServayDescriptionFilters : ServayDescriptionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ServayDescriptionFilters class.
		/// </summary>
		public ServayDescriptionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ServayDescriptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ServayDescriptionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ServayDescriptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ServayDescriptionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ServayDescriptionFilters
	
	#region ServayDescriptionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ServayDescriptionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ServayDescription"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ServayDescriptionQuery : ServayDescriptionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ServayDescriptionQuery class.
		/// </summary>
		public ServayDescriptionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ServayDescriptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ServayDescriptionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ServayDescriptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ServayDescriptionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ServayDescriptionQuery
		
	#region SearchTracerFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SearchTracer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SearchTracerFilters : SearchTracerFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SearchTracerFilters class.
		/// </summary>
		public SearchTracerFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SearchTracerFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SearchTracerFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SearchTracerFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SearchTracerFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SearchTracerFilters
	
	#region SearchTracerQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SearchTracerParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SearchTracer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SearchTracerQuery : SearchTracerParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SearchTracerQuery class.
		/// </summary>
		public SearchTracerQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SearchTracerQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SearchTracerQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SearchTracerQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SearchTracerQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SearchTracerQuery
		
	#region RpfFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Rpf"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RpfFilters : RpfFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RpfFilters class.
		/// </summary>
		public RpfFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the RpfFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RpfFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RpfFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RpfFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RpfFilters
	
	#region RpfQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="RpfParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Rpf"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RpfQuery : RpfParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RpfQuery class.
		/// </summary>
		public RpfQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the RpfQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RpfQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RpfQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RpfQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RpfQuery
		
	#region RolesFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Roles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RolesFilters : RolesFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RolesFilters class.
		/// </summary>
		public RolesFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the RolesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RolesFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RolesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RolesFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RolesFilters
	
	#region RolesQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="RolesParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Roles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RolesQuery : RolesParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RolesQuery class.
		/// </summary>
		public RolesQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the RolesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RolesQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RolesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RolesQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RolesQuery
		
	#region ServeyAnswerFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ServeyAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ServeyAnswerFilters : ServeyAnswerFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ServeyAnswerFilters class.
		/// </summary>
		public ServeyAnswerFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ServeyAnswerFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ServeyAnswerFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ServeyAnswerFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ServeyAnswerFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ServeyAnswerFilters
	
	#region ServeyAnswerQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ServeyAnswerParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ServeyAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ServeyAnswerQuery : ServeyAnswerParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ServeyAnswerQuery class.
		/// </summary>
		public ServeyAnswerQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ServeyAnswerQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ServeyAnswerQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ServeyAnswerQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ServeyAnswerQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ServeyAnswerQuery
		
	#region ServeyAnswerDescriptionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ServeyAnswerDescription"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ServeyAnswerDescriptionFilters : ServeyAnswerDescriptionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ServeyAnswerDescriptionFilters class.
		/// </summary>
		public ServeyAnswerDescriptionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ServeyAnswerDescriptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ServeyAnswerDescriptionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ServeyAnswerDescriptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ServeyAnswerDescriptionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ServeyAnswerDescriptionFilters
	
	#region ServeyAnswerDescriptionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ServeyAnswerDescriptionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ServeyAnswerDescription"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ServeyAnswerDescriptionQuery : ServeyAnswerDescriptionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ServeyAnswerDescriptionQuery class.
		/// </summary>
		public ServeyAnswerDescriptionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ServeyAnswerDescriptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ServeyAnswerDescriptionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ServeyAnswerDescriptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ServeyAnswerDescriptionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ServeyAnswerDescriptionQuery
		
	#region SpandPbedroomFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SpandPbedroom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SpandPbedroomFilters : SpandPbedroomFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SpandPbedroomFilters class.
		/// </summary>
		public SpandPbedroomFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SpandPbedroomFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SpandPbedroomFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SpandPbedroomFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SpandPbedroomFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SpandPbedroomFilters
	
	#region SpandPbedroomQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SpandPbedroomParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SpandPbedroom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SpandPbedroomQuery : SpandPbedroomParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SpandPbedroomQuery class.
		/// </summary>
		public SpandPbedroomQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SpandPbedroomQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SpandPbedroomQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SpandPbedroomQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SpandPbedroomQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SpandPbedroomQuery
		
	#region SpandPmeetingRoomFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SpandPmeetingRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SpandPmeetingRoomFilters : SpandPmeetingRoomFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SpandPmeetingRoomFilters class.
		/// </summary>
		public SpandPmeetingRoomFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SpandPmeetingRoomFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SpandPmeetingRoomFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SpandPmeetingRoomFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SpandPmeetingRoomFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SpandPmeetingRoomFilters
	
	#region SpandPmeetingRoomQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SpandPmeetingRoomParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SpandPmeetingRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SpandPmeetingRoomQuery : SpandPmeetingRoomParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SpandPmeetingRoomQuery class.
		/// </summary>
		public SpandPmeetingRoomQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SpandPmeetingRoomQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SpandPmeetingRoomQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SpandPmeetingRoomQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SpandPmeetingRoomQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SpandPmeetingRoomQuery
		
	#region ServeyQuestionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ServeyQuestion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ServeyQuestionFilters : ServeyQuestionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ServeyQuestionFilters class.
		/// </summary>
		public ServeyQuestionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ServeyQuestionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ServeyQuestionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ServeyQuestionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ServeyQuestionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ServeyQuestionFilters
	
	#region ServeyQuestionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ServeyQuestionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ServeyQuestion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ServeyQuestionQuery : ServeyQuestionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ServeyQuestionQuery class.
		/// </summary>
		public ServeyQuestionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ServeyQuestionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ServeyQuestionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ServeyQuestionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ServeyQuestionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ServeyQuestionQuery
		
	#region ServeyResponseFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ServeyResponse"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ServeyResponseFilters : ServeyResponseFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ServeyResponseFilters class.
		/// </summary>
		public ServeyResponseFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ServeyResponseFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ServeyResponseFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ServeyResponseFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ServeyResponseFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ServeyResponseFilters
	
	#region ServeyResponseQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ServeyResponseParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ServeyResponse"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ServeyResponseQuery : ServeyResponseParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ServeyResponseQuery class.
		/// </summary>
		public ServeyResponseQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ServeyResponseQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ServeyResponseQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ServeyResponseQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ServeyResponseQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ServeyResponseQuery
		
	#region InvoiceCommissionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="InvoiceCommission"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class InvoiceCommissionFilters : InvoiceCommissionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the InvoiceCommissionFilters class.
		/// </summary>
		public InvoiceCommissionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the InvoiceCommissionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public InvoiceCommissionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the InvoiceCommissionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public InvoiceCommissionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion InvoiceCommissionFilters
	
	#region InvoiceCommissionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="InvoiceCommissionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="InvoiceCommission"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class InvoiceCommissionQuery : InvoiceCommissionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the InvoiceCommissionQuery class.
		/// </summary>
		public InvoiceCommissionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the InvoiceCommissionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public InvoiceCommissionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the InvoiceCommissionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public InvoiceCommissionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion InvoiceCommissionQuery
		
	#region ServeyresultFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Serveyresult"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ServeyresultFilters : ServeyresultFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ServeyresultFilters class.
		/// </summary>
		public ServeyresultFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ServeyresultFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ServeyresultFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ServeyresultFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ServeyresultFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ServeyresultFilters
	
	#region ServeyresultQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ServeyresultParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Serveyresult"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ServeyresultQuery : ServeyresultParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ServeyresultQuery class.
		/// </summary>
		public ServeyresultQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ServeyresultQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ServeyresultQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ServeyresultQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ServeyresultQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ServeyresultQuery
		
	#region CurrencyFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Currency"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrencyFilters : CurrencyFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrencyFilters class.
		/// </summary>
		public CurrencyFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrencyFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrencyFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrencyFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrencyFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrencyFilters
	
	#region CurrencyQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CurrencyParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Currency"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrencyQuery : CurrencyParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrencyQuery class.
		/// </summary>
		public CurrencyQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrencyQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrencyQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrencyQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrencyQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrencyQuery
		
	#region BedRoomFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BedRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BedRoomFilters : BedRoomFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomFilters class.
		/// </summary>
		public BedRoomFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the BedRoomFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BedRoomFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BedRoomFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BedRoomFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BedRoomFilters
	
	#region BedRoomQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="BedRoomParameterBuilder"/> class
	/// that is used exclusively with a <see cref="BedRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BedRoomQuery : BedRoomParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomQuery class.
		/// </summary>
		public BedRoomQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the BedRoomQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BedRoomQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BedRoomQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BedRoomQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BedRoomQuery
		
	#region BookingFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Booking"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BookingFilters : BookingFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BookingFilters class.
		/// </summary>
		public BookingFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the BookingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BookingFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BookingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BookingFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BookingFilters
	
	#region BookingQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="BookingParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Booking"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BookingQuery : BookingParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BookingQuery class.
		/// </summary>
		public BookingQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the BookingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BookingQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BookingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BookingQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BookingQuery
		
	#region BedRoomPictureImageFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BedRoomPictureImage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BedRoomPictureImageFilters : BedRoomPictureImageFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageFilters class.
		/// </summary>
		public BedRoomPictureImageFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BedRoomPictureImageFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BedRoomPictureImageFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BedRoomPictureImageFilters
	
	#region BedRoomPictureImageQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="BedRoomPictureImageParameterBuilder"/> class
	/// that is used exclusively with a <see cref="BedRoomPictureImage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BedRoomPictureImageQuery : BedRoomPictureImageParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageQuery class.
		/// </summary>
		public BedRoomPictureImageQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BedRoomPictureImageQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BedRoomPictureImageQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BedRoomPictureImageQuery
		
	#region CountryFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Country"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CountryFilters : CountryFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CountryFilters class.
		/// </summary>
		public CountryFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CountryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CountryFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CountryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CountryFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CountryFilters
	
	#region CountryQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CountryParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Country"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CountryQuery : CountryParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CountryQuery class.
		/// </summary>
		public CountryQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CountryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CountryQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CountryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CountryQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CountryQuery
		
	#region BookingLogsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BookingLogs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BookingLogsFilters : BookingLogsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BookingLogsFilters class.
		/// </summary>
		public BookingLogsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the BookingLogsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BookingLogsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BookingLogsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BookingLogsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BookingLogsFilters
	
	#region BookingLogsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="BookingLogsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="BookingLogs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BookingLogsQuery : BookingLogsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BookingLogsQuery class.
		/// </summary>
		public BookingLogsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the BookingLogsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BookingLogsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BookingLogsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BookingLogsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BookingLogsQuery
		
	#region CmsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Cms"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CmsFilters : CmsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CmsFilters class.
		/// </summary>
		public CmsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CmsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CmsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CmsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CmsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CmsFilters
	
	#region CmsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CmsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Cms"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CmsQuery : CmsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CmsQuery class.
		/// </summary>
		public CmsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CmsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CmsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CmsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CmsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CmsQuery
		
	#region CancelationPolicyFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CancelationPolicy"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CancelationPolicyFilters : CancelationPolicyFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CancelationPolicyFilters class.
		/// </summary>
		public CancelationPolicyFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CancelationPolicyFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CancelationPolicyFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CancelationPolicyFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CancelationPolicyFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CancelationPolicyFilters
	
	#region CancelationPolicyQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CancelationPolicyParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CancelationPolicy"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CancelationPolicyQuery : CancelationPolicyParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CancelationPolicyQuery class.
		/// </summary>
		public CancelationPolicyQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CancelationPolicyQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CancelationPolicyQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CancelationPolicyQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CancelationPolicyQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CancelationPolicyQuery
		
	#region CityLanguageFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CityLanguage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CityLanguageFilters : CityLanguageFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CityLanguageFilters class.
		/// </summary>
		public CityLanguageFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CityLanguageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CityLanguageFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CityLanguageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CityLanguageFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CityLanguageFilters
	
	#region CityLanguageQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CityLanguageParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CityLanguage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CityLanguageQuery : CityLanguageParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CityLanguageQuery class.
		/// </summary>
		public CityLanguageQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CityLanguageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CityLanguageQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CityLanguageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CityLanguageQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CityLanguageQuery
		
	#region BookedMeetingRoomFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BookedMeetingRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BookedMeetingRoomFilters : BookedMeetingRoomFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BookedMeetingRoomFilters class.
		/// </summary>
		public BookedMeetingRoomFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the BookedMeetingRoomFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BookedMeetingRoomFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BookedMeetingRoomFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BookedMeetingRoomFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BookedMeetingRoomFilters
	
	#region BookedMeetingRoomQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="BookedMeetingRoomParameterBuilder"/> class
	/// that is used exclusively with a <see cref="BookedMeetingRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BookedMeetingRoomQuery : BookedMeetingRoomParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BookedMeetingRoomQuery class.
		/// </summary>
		public BookedMeetingRoomQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the BookedMeetingRoomQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BookedMeetingRoomQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BookedMeetingRoomQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BookedMeetingRoomQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BookedMeetingRoomQuery
		
	#region BuildMeetingConfigureFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BuildMeetingConfigure"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BuildMeetingConfigureFilters : BuildMeetingConfigureFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BuildMeetingConfigureFilters class.
		/// </summary>
		public BuildMeetingConfigureFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the BuildMeetingConfigureFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BuildMeetingConfigureFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BuildMeetingConfigureFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BuildMeetingConfigureFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BuildMeetingConfigureFilters
	
	#region BuildMeetingConfigureQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="BuildMeetingConfigureParameterBuilder"/> class
	/// that is used exclusively with a <see cref="BuildMeetingConfigure"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BuildMeetingConfigureQuery : BuildMeetingConfigureParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BuildMeetingConfigureQuery class.
		/// </summary>
		public BuildMeetingConfigureQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the BuildMeetingConfigureQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BuildMeetingConfigureQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BuildMeetingConfigureQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BuildMeetingConfigureQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BuildMeetingConfigureQuery
		
	#region CancelationPolicyLanguageFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CancelationPolicyLanguage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CancelationPolicyLanguageFilters : CancelationPolicyLanguageFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CancelationPolicyLanguageFilters class.
		/// </summary>
		public CancelationPolicyLanguageFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CancelationPolicyLanguageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CancelationPolicyLanguageFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CancelationPolicyLanguageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CancelationPolicyLanguageFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CancelationPolicyLanguageFilters
	
	#region CancelationPolicyLanguageQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CancelationPolicyLanguageParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CancelationPolicyLanguage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CancelationPolicyLanguageQuery : CancelationPolicyLanguageParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CancelationPolicyLanguageQuery class.
		/// </summary>
		public CancelationPolicyLanguageQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CancelationPolicyLanguageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CancelationPolicyLanguageQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CancelationPolicyLanguageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CancelationPolicyLanguageQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CancelationPolicyLanguageQuery
		
	#region BuildPackageConfigureFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BuildPackageConfigure"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BuildPackageConfigureFilters : BuildPackageConfigureFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureFilters class.
		/// </summary>
		public BuildPackageConfigureFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BuildPackageConfigureFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BuildPackageConfigureFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BuildPackageConfigureFilters
	
	#region BuildPackageConfigureQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="BuildPackageConfigureParameterBuilder"/> class
	/// that is used exclusively with a <see cref="BuildPackageConfigure"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BuildPackageConfigureQuery : BuildPackageConfigureParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureQuery class.
		/// </summary>
		public BuildPackageConfigureQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BuildPackageConfigureQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BuildPackageConfigureQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BuildPackageConfigureQuery
		
	#region BuildPackageConfigureDescFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BuildPackageConfigureDesc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BuildPackageConfigureDescFilters : BuildPackageConfigureDescFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureDescFilters class.
		/// </summary>
		public BuildPackageConfigureDescFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureDescFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BuildPackageConfigureDescFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureDescFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BuildPackageConfigureDescFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BuildPackageConfigureDescFilters
	
	#region BuildPackageConfigureDescQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="BuildPackageConfigureDescParameterBuilder"/> class
	/// that is used exclusively with a <see cref="BuildPackageConfigureDesc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BuildPackageConfigureDescQuery : BuildPackageConfigureDescParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureDescQuery class.
		/// </summary>
		public BuildPackageConfigureDescQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureDescQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BuildPackageConfigureDescQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BuildPackageConfigureDescQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BuildPackageConfigureDescQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BuildPackageConfigureDescQuery
		
	#region CityFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="City"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CityFilters : CityFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CityFilters class.
		/// </summary>
		public CityFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CityFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CityFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CityFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CityFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CityFilters
	
	#region CityQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CityParameterBuilder"/> class
	/// that is used exclusively with a <see cref="City"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CityQuery : CityParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CityQuery class.
		/// </summary>
		public CityQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CityQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CityQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CityQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CityQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CityQuery
		
	#region AgentUserFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AgentUser"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AgentUserFilters : AgentUserFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AgentUserFilters class.
		/// </summary>
		public AgentUserFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AgentUserFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AgentUserFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AgentUserFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AgentUserFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AgentUserFilters
	
	#region AgentUserQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AgentUserParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AgentUser"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AgentUserQuery : AgentUserParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AgentUserQuery class.
		/// </summary>
		public AgentUserQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AgentUserQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AgentUserQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AgentUserQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AgentUserQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AgentUserQuery
		
	#region BookedBedRoomFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BookedBedRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BookedBedRoomFilters : BookedBedRoomFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BookedBedRoomFilters class.
		/// </summary>
		public BookedBedRoomFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the BookedBedRoomFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BookedBedRoomFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BookedBedRoomFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BookedBedRoomFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BookedBedRoomFilters
	
	#region BookedBedRoomQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="BookedBedRoomParameterBuilder"/> class
	/// that is used exclusively with a <see cref="BookedBedRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BookedBedRoomQuery : BookedBedRoomParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BookedBedRoomQuery class.
		/// </summary>
		public BookedBedRoomQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the BookedBedRoomQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BookedBedRoomQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BookedBedRoomQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BookedBedRoomQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BookedBedRoomQuery
		
	#region AgencyClientFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AgencyClient"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AgencyClientFilters : AgencyClientFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AgencyClientFilters class.
		/// </summary>
		public AgencyClientFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AgencyClientFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AgencyClientFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AgencyClientFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AgencyClientFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AgencyClientFilters
	
	#region AgencyClientQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AgencyClientParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AgencyClient"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AgencyClientQuery : AgencyClientParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AgencyClientQuery class.
		/// </summary>
		public AgencyClientQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AgencyClientQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AgencyClientQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AgencyClientQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AgencyClientQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AgencyClientQuery
		
	#region AgencyInvoiceFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AgencyInvoice"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AgencyInvoiceFilters : AgencyInvoiceFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AgencyInvoiceFilters class.
		/// </summary>
		public AgencyInvoiceFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AgencyInvoiceFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AgencyInvoiceFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AgencyInvoiceFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AgencyInvoiceFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AgencyInvoiceFilters
	
	#region AgencyInvoiceQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AgencyInvoiceParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AgencyInvoice"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AgencyInvoiceQuery : AgencyInvoiceParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AgencyInvoiceQuery class.
		/// </summary>
		public AgencyInvoiceQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AgencyInvoiceQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AgencyInvoiceQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AgencyInvoiceQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AgencyInvoiceQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AgencyInvoiceQuery
		
	#region InvoiceFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Invoice"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class InvoiceFilters : InvoiceFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the InvoiceFilters class.
		/// </summary>
		public InvoiceFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the InvoiceFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public InvoiceFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the InvoiceFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public InvoiceFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion InvoiceFilters
	
	#region InvoiceQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="InvoiceParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Invoice"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class InvoiceQuery : InvoiceParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the InvoiceQuery class.
		/// </summary>
		public InvoiceQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the InvoiceQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public InvoiceQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the InvoiceQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public InvoiceQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion InvoiceQuery
		
	#region AvailabilityFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Availability"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AvailabilityFilters : AvailabilityFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AvailabilityFilters class.
		/// </summary>
		public AvailabilityFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AvailabilityFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AvailabilityFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AvailabilityFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AvailabilityFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AvailabilityFilters
	
	#region AvailabilityQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AvailabilityParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Availability"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AvailabilityQuery : AvailabilityParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AvailabilityQuery class.
		/// </summary>
		public AvailabilityQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AvailabilityQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AvailabilityQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AvailabilityQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AvailabilityQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AvailabilityQuery
		
	#region AvailabilityTrailFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AvailabilityTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AvailabilityTrailFilters : AvailabilityTrailFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AvailabilityTrailFilters class.
		/// </summary>
		public AvailabilityTrailFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AvailabilityTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AvailabilityTrailFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AvailabilityTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AvailabilityTrailFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AvailabilityTrailFilters
	
	#region AvailabilityTrailQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AvailabilityTrailParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AvailabilityTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AvailabilityTrailQuery : AvailabilityTrailParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AvailabilityTrailQuery class.
		/// </summary>
		public AvailabilityTrailQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AvailabilityTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AvailabilityTrailQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AvailabilityTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AvailabilityTrailQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AvailabilityTrailQuery
		
	#region BedRoomPictureImageTrailFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BedRoomPictureImageTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BedRoomPictureImageTrailFilters : BedRoomPictureImageTrailFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageTrailFilters class.
		/// </summary>
		public BedRoomPictureImageTrailFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BedRoomPictureImageTrailFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BedRoomPictureImageTrailFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BedRoomPictureImageTrailFilters
	
	#region BedRoomPictureImageTrailQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="BedRoomPictureImageTrailParameterBuilder"/> class
	/// that is used exclusively with a <see cref="BedRoomPictureImageTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BedRoomPictureImageTrailQuery : BedRoomPictureImageTrailParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageTrailQuery class.
		/// </summary>
		public BedRoomPictureImageTrailQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BedRoomPictureImageTrailQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BedRoomPictureImageTrailQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BedRoomPictureImageTrailQuery
		
	#region AvailibilityOfRoomsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AvailibilityOfRooms"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AvailibilityOfRoomsFilters : AvailibilityOfRoomsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AvailibilityOfRoomsFilters class.
		/// </summary>
		public AvailibilityOfRoomsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AvailibilityOfRoomsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AvailibilityOfRoomsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AvailibilityOfRoomsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AvailibilityOfRoomsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AvailibilityOfRoomsFilters
	
	#region AvailibilityOfRoomsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AvailibilityOfRoomsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AvailibilityOfRooms"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AvailibilityOfRoomsQuery : AvailibilityOfRoomsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AvailibilityOfRoomsQuery class.
		/// </summary>
		public AvailibilityOfRoomsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AvailibilityOfRoomsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AvailibilityOfRoomsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AvailibilityOfRoomsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AvailibilityOfRoomsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AvailibilityOfRoomsQuery
		
	#region BedRoomDescFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BedRoomDesc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BedRoomDescFilters : BedRoomDescFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomDescFilters class.
		/// </summary>
		public BedRoomDescFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the BedRoomDescFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BedRoomDescFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BedRoomDescFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BedRoomDescFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BedRoomDescFilters
	
	#region BedRoomDescQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="BedRoomDescParameterBuilder"/> class
	/// that is used exclusively with a <see cref="BedRoomDesc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BedRoomDescQuery : BedRoomDescParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomDescQuery class.
		/// </summary>
		public BedRoomDescQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the BedRoomDescQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BedRoomDescQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BedRoomDescQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BedRoomDescQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BedRoomDescQuery
		
	#region BedRoomDescTrailFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BedRoomDescTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BedRoomDescTrailFilters : BedRoomDescTrailFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomDescTrailFilters class.
		/// </summary>
		public BedRoomDescTrailFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the BedRoomDescTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BedRoomDescTrailFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BedRoomDescTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BedRoomDescTrailFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BedRoomDescTrailFilters
	
	#region BedRoomDescTrailQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="BedRoomDescTrailParameterBuilder"/> class
	/// that is used exclusively with a <see cref="BedRoomDescTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BedRoomDescTrailQuery : BedRoomDescTrailParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomDescTrailQuery class.
		/// </summary>
		public BedRoomDescTrailQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the BedRoomDescTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BedRoomDescTrailQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BedRoomDescTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BedRoomDescTrailQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BedRoomDescTrailQuery
		
	#region BedRoomTrailFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BedRoomTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BedRoomTrailFilters : BedRoomTrailFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomTrailFilters class.
		/// </summary>
		public BedRoomTrailFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the BedRoomTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BedRoomTrailFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BedRoomTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BedRoomTrailFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BedRoomTrailFilters
	
	#region BedRoomTrailQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="BedRoomTrailParameterBuilder"/> class
	/// that is used exclusively with a <see cref="BedRoomTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BedRoomTrailQuery : BedRoomTrailParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomTrailQuery class.
		/// </summary>
		public BedRoomTrailQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the BedRoomTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BedRoomTrailQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BedRoomTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BedRoomTrailQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BedRoomTrailQuery
		
	#region CmsDescriptionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CmsDescription"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CmsDescriptionFilters : CmsDescriptionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CmsDescriptionFilters class.
		/// </summary>
		public CmsDescriptionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CmsDescriptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CmsDescriptionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CmsDescriptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CmsDescriptionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CmsDescriptionFilters
	
	#region CmsDescriptionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CmsDescriptionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CmsDescription"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CmsDescriptionQuery : CmsDescriptionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CmsDescriptionQuery class.
		/// </summary>
		public CmsDescriptionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CmsDescriptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CmsDescriptionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CmsDescriptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CmsDescriptionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CmsDescriptionQuery
		
	#region CommissionPercentageFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CommissionPercentage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CommissionPercentageFilters : CommissionPercentageFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CommissionPercentageFilters class.
		/// </summary>
		public CommissionPercentageFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CommissionPercentageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CommissionPercentageFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CommissionPercentageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CommissionPercentageFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CommissionPercentageFilters
	
	#region CommissionPercentageQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CommissionPercentageParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CommissionPercentage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CommissionPercentageQuery : CommissionPercentageParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CommissionPercentageQuery class.
		/// </summary>
		public CommissionPercentageQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CommissionPercentageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CommissionPercentageQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CommissionPercentageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CommissionPercentageQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CommissionPercentageQuery
		
	#region HotelFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Hotel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelFilters : HotelFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelFilters class.
		/// </summary>
		public HotelFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelFilters
	
	#region HotelQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="HotelParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Hotel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelQuery : HotelParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelQuery class.
		/// </summary>
		public HotelQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelQuery
		
	#region ZoneLanguageFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ZoneLanguage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ZoneLanguageFilters : ZoneLanguageFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ZoneLanguageFilters class.
		/// </summary>
		public ZoneLanguageFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ZoneLanguageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ZoneLanguageFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ZoneLanguageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ZoneLanguageFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ZoneLanguageFilters
	
	#region ZoneLanguageQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ZoneLanguageParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ZoneLanguage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ZoneLanguageQuery : ZoneLanguageParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ZoneLanguageQuery class.
		/// </summary>
		public ZoneLanguageQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ZoneLanguageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ZoneLanguageQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ZoneLanguageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ZoneLanguageQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ZoneLanguageQuery
		
	#region HotelContactFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HotelContact"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelContactFilters : HotelContactFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelContactFilters class.
		/// </summary>
		public HotelContactFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelContactFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelContactFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelContactFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelContactFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelContactFilters
	
	#region HotelContactQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="HotelContactParameterBuilder"/> class
	/// that is used exclusively with a <see cref="HotelContact"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelContactQuery : HotelContactParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelContactQuery class.
		/// </summary>
		public HotelContactQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelContactQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelContactQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelContactQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelContactQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelContactQuery
		
	#region HotelTrailFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HotelTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelTrailFilters : HotelTrailFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelTrailFilters class.
		/// </summary>
		public HotelTrailFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelTrailFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelTrailFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelTrailFilters
	
	#region HotelTrailQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="HotelTrailParameterBuilder"/> class
	/// that is used exclusively with a <see cref="HotelTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelTrailQuery : HotelTrailParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelTrailQuery class.
		/// </summary>
		public HotelTrailQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelTrailQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelTrailQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelTrailQuery
		
	#region FrontEndBottomFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FrontEndBottom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FrontEndBottomFilters : FrontEndBottomFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomFilters class.
		/// </summary>
		public FrontEndBottomFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FrontEndBottomFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FrontEndBottomFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FrontEndBottomFilters
	
	#region FrontEndBottomQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="FrontEndBottomParameterBuilder"/> class
	/// that is used exclusively with a <see cref="FrontEndBottom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FrontEndBottomQuery : FrontEndBottomParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomQuery class.
		/// </summary>
		public FrontEndBottomQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FrontEndBottomQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FrontEndBottomQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FrontEndBottomQuery
		
	#region HotelContactTrailFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HotelContactTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelContactTrailFilters : HotelContactTrailFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelContactTrailFilters class.
		/// </summary>
		public HotelContactTrailFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelContactTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelContactTrailFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelContactTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelContactTrailFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelContactTrailFilters
	
	#region HotelContactTrailQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="HotelContactTrailParameterBuilder"/> class
	/// that is used exclusively with a <see cref="HotelContactTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelContactTrailQuery : HotelContactTrailParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelContactTrailQuery class.
		/// </summary>
		public HotelContactTrailQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelContactTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelContactTrailQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelContactTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelContactTrailQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelContactTrailQuery
		
	#region HotelDescFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HotelDesc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelDescFilters : HotelDescFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelDescFilters class.
		/// </summary>
		public HotelDescFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelDescFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelDescFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelDescFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelDescFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelDescFilters
	
	#region HotelDescQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="HotelDescParameterBuilder"/> class
	/// that is used exclusively with a <see cref="HotelDesc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelDescQuery : HotelDescParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelDescQuery class.
		/// </summary>
		public HotelDescQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelDescQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelDescQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelDescQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelDescQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelDescQuery
		
	#region HoteldescTrailFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HoteldescTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HoteldescTrailFilters : HoteldescTrailFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HoteldescTrailFilters class.
		/// </summary>
		public HoteldescTrailFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the HoteldescTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HoteldescTrailFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HoteldescTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HoteldescTrailFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HoteldescTrailFilters
	
	#region HoteldescTrailQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="HoteldescTrailParameterBuilder"/> class
	/// that is used exclusively with a <see cref="HoteldescTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HoteldescTrailQuery : HoteldescTrailParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HoteldescTrailQuery class.
		/// </summary>
		public HoteldescTrailQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the HoteldescTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HoteldescTrailQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HoteldescTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HoteldescTrailQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HoteldescTrailQuery
		
	#region HotelPhotoVideoGallaryFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HotelPhotoVideoGallary"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelPhotoVideoGallaryFilters : HotelPhotoVideoGallaryFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelPhotoVideoGallaryFilters class.
		/// </summary>
		public HotelPhotoVideoGallaryFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelPhotoVideoGallaryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelPhotoVideoGallaryFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelPhotoVideoGallaryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelPhotoVideoGallaryFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelPhotoVideoGallaryFilters
	
	#region HotelPhotoVideoGallaryQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="HotelPhotoVideoGallaryParameterBuilder"/> class
	/// that is used exclusively with a <see cref="HotelPhotoVideoGallary"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelPhotoVideoGallaryQuery : HotelPhotoVideoGallaryParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelPhotoVideoGallaryQuery class.
		/// </summary>
		public HotelPhotoVideoGallaryQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelPhotoVideoGallaryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelPhotoVideoGallaryQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelPhotoVideoGallaryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelPhotoVideoGallaryQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelPhotoVideoGallaryQuery
		
	#region HotelOwnerLinkFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HotelOwnerLink"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelOwnerLinkFilters : HotelOwnerLinkFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelOwnerLinkFilters class.
		/// </summary>
		public HotelOwnerLinkFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelOwnerLinkFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelOwnerLinkFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelOwnerLinkFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelOwnerLinkFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelOwnerLinkFilters
	
	#region HotelOwnerLinkQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="HotelOwnerLinkParameterBuilder"/> class
	/// that is used exclusively with a <see cref="HotelOwnerLink"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelOwnerLinkQuery : HotelOwnerLinkParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelOwnerLinkQuery class.
		/// </summary>
		public HotelOwnerLinkQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelOwnerLinkQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelOwnerLinkQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelOwnerLinkQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelOwnerLinkQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelOwnerLinkQuery
		
	#region HotelFacilitiesTrailFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HotelFacilitiesTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelFacilitiesTrailFilters : HotelFacilitiesTrailFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelFacilitiesTrailFilters class.
		/// </summary>
		public HotelFacilitiesTrailFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelFacilitiesTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelFacilitiesTrailFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelFacilitiesTrailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelFacilitiesTrailFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelFacilitiesTrailFilters
	
	#region HotelFacilitiesTrailQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="HotelFacilitiesTrailParameterBuilder"/> class
	/// that is used exclusively with a <see cref="HotelFacilitiesTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelFacilitiesTrailQuery : HotelFacilitiesTrailParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelFacilitiesTrailQuery class.
		/// </summary>
		public HotelFacilitiesTrailQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelFacilitiesTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelFacilitiesTrailQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelFacilitiesTrailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelFacilitiesTrailQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelFacilitiesTrailQuery
		
	#region HotelFacilitiesFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HotelFacilities"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelFacilitiesFilters : HotelFacilitiesFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelFacilitiesFilters class.
		/// </summary>
		public HotelFacilitiesFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelFacilitiesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelFacilitiesFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelFacilitiesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelFacilitiesFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelFacilitiesFilters
	
	#region HotelFacilitiesQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="HotelFacilitiesParameterBuilder"/> class
	/// that is used exclusively with a <see cref="HotelFacilities"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelFacilitiesQuery : HotelFacilitiesParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelFacilitiesQuery class.
		/// </summary>
		public HotelFacilitiesQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelFacilitiesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelFacilitiesQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelFacilitiesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelFacilitiesQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelFacilitiesQuery
		
	#region HearUsQuestionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HearUsQuestion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HearUsQuestionFilters : HearUsQuestionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HearUsQuestionFilters class.
		/// </summary>
		public HearUsQuestionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the HearUsQuestionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HearUsQuestionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HearUsQuestionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HearUsQuestionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HearUsQuestionFilters
	
	#region HearUsQuestionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="HearUsQuestionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="HearUsQuestion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HearUsQuestionQuery : HearUsQuestionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HearUsQuestionQuery class.
		/// </summary>
		public HearUsQuestionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the HearUsQuestionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HearUsQuestionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HearUsQuestionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HearUsQuestionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HearUsQuestionQuery
		
	#region GroupFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Group"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class GroupFilters : GroupFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the GroupFilters class.
		/// </summary>
		public GroupFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the GroupFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public GroupFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the GroupFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public GroupFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion GroupFilters
	
	#region GroupQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="GroupParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Group"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class GroupQuery : GroupParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the GroupQuery class.
		/// </summary>
		public GroupQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the GroupQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public GroupQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the GroupQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public GroupQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion GroupQuery
		
	#region FrontEndBottomDescriptionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FrontEndBottomDescription"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FrontEndBottomDescriptionFilters : FrontEndBottomDescriptionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomDescriptionFilters class.
		/// </summary>
		public FrontEndBottomDescriptionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomDescriptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FrontEndBottomDescriptionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomDescriptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FrontEndBottomDescriptionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FrontEndBottomDescriptionFilters
	
	#region FrontEndBottomDescriptionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="FrontEndBottomDescriptionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="FrontEndBottomDescription"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FrontEndBottomDescriptionQuery : FrontEndBottomDescriptionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomDescriptionQuery class.
		/// </summary>
		public FrontEndBottomDescriptionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomDescriptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FrontEndBottomDescriptionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomDescriptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FrontEndBottomDescriptionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FrontEndBottomDescriptionQuery
		
	#region DashboardLinkFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="DashboardLink"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DashboardLinkFilters : DashboardLinkFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DashboardLinkFilters class.
		/// </summary>
		public DashboardLinkFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the DashboardLinkFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public DashboardLinkFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the DashboardLinkFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public DashboardLinkFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion DashboardLinkFilters
	
	#region DashboardLinkQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="DashboardLinkParameterBuilder"/> class
	/// that is used exclusively with a <see cref="DashboardLink"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DashboardLinkQuery : DashboardLinkParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DashboardLinkQuery class.
		/// </summary>
		public DashboardLinkQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the DashboardLinkQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public DashboardLinkQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the DashboardLinkQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public DashboardLinkQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion DashboardLinkQuery
		
	#region CurrencyExchangeLogsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrencyExchangeLogs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrencyExchangeLogsFilters : CurrencyExchangeLogsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrencyExchangeLogsFilters class.
		/// </summary>
		public CurrencyExchangeLogsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrencyExchangeLogsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrencyExchangeLogsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrencyExchangeLogsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrencyExchangeLogsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrencyExchangeLogsFilters
	
	#region CurrencyExchangeLogsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CurrencyExchangeLogsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CurrencyExchangeLogs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrencyExchangeLogsQuery : CurrencyExchangeLogsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrencyExchangeLogsQuery class.
		/// </summary>
		public CurrencyExchangeLogsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrencyExchangeLogsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrencyExchangeLogsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrencyExchangeLogsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrencyExchangeLogsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrencyExchangeLogsQuery
		
	#region CreditCardDetailFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CreditCardDetail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CreditCardDetailFilters : CreditCardDetailFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CreditCardDetailFilters class.
		/// </summary>
		public CreditCardDetailFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CreditCardDetailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CreditCardDetailFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CreditCardDetailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CreditCardDetailFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CreditCardDetailFilters
	
	#region CreditCardDetailQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CreditCardDetailParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CreditCardDetail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CreditCardDetailQuery : CreditCardDetailParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CreditCardDetailQuery class.
		/// </summary>
		public CreditCardDetailQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CreditCardDetailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CreditCardDetailQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CreditCardDetailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CreditCardDetailQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CreditCardDetailQuery
		
	#region CountryLanguageFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CountryLanguage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CountryLanguageFilters : CountryLanguageFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CountryLanguageFilters class.
		/// </summary>
		public CountryLanguageFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CountryLanguageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CountryLanguageFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CountryLanguageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CountryLanguageFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CountryLanguageFilters
	
	#region CountryLanguageQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CountryLanguageParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CountryLanguage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CountryLanguageQuery : CountryLanguageParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CountryLanguageQuery class.
		/// </summary>
		public CountryLanguageQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CountryLanguageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CountryLanguageQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CountryLanguageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CountryLanguageQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CountryLanguageQuery
		
	#region DbAuditFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="DbAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DbAuditFilters : DbAuditFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DbAuditFilters class.
		/// </summary>
		public DbAuditFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the DbAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public DbAuditFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the DbAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public DbAuditFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion DbAuditFilters
	
	#region DbAuditQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="DbAuditParameterBuilder"/> class
	/// that is used exclusively with a <see cref="DbAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DbAuditQuery : DbAuditParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DbAuditQuery class.
		/// </summary>
		public DbAuditQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the DbAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public DbAuditQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the DbAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public DbAuditQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion DbAuditQuery
		
	#region EmailConfigFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailConfig"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailConfigFilters : EmailConfigFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailConfigFilters class.
		/// </summary>
		public EmailConfigFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the EmailConfigFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EmailConfigFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EmailConfigFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EmailConfigFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EmailConfigFilters
	
	#region EmailConfigQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="EmailConfigParameterBuilder"/> class
	/// that is used exclusively with a <see cref="EmailConfig"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailConfigQuery : EmailConfigParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailConfigQuery class.
		/// </summary>
		public EmailConfigQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the EmailConfigQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EmailConfigQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EmailConfigQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EmailConfigQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EmailConfigQuery
		
	#region FinancialInfoFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FinancialInfo"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FinancialInfoFilters : FinancialInfoFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FinancialInfoFilters class.
		/// </summary>
		public FinancialInfoFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the FinancialInfoFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FinancialInfoFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FinancialInfoFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FinancialInfoFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FinancialInfoFilters
	
	#region FinancialInfoQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="FinancialInfoParameterBuilder"/> class
	/// that is used exclusively with a <see cref="FinancialInfo"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FinancialInfoQuery : FinancialInfoParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FinancialInfoQuery class.
		/// </summary>
		public FinancialInfoQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the FinancialInfoQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FinancialInfoQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FinancialInfoQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FinancialInfoQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FinancialInfoQuery
		
	#region FacilityTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FacilityType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FacilityTypeFilters : FacilityTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacilityTypeFilters class.
		/// </summary>
		public FacilityTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the FacilityTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FacilityTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FacilityTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FacilityTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FacilityTypeFilters
	
	#region FacilityTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="FacilityTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="FacilityType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FacilityTypeQuery : FacilityTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacilityTypeQuery class.
		/// </summary>
		public FacilityTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the FacilityTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FacilityTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FacilityTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FacilityTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FacilityTypeQuery
		
	#region FacilityFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Facility"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FacilityFilters : FacilityFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacilityFilters class.
		/// </summary>
		public FacilityFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the FacilityFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FacilityFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FacilityFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FacilityFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FacilityFilters
	
	#region FacilityQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="FacilityParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Facility"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FacilityQuery : FacilityParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacilityQuery class.
		/// </summary>
		public FacilityQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the FacilityQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FacilityQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FacilityQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FacilityQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FacilityQuery
		
	#region EmailConfigMappingFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailConfigMapping"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailConfigMappingFilters : EmailConfigMappingFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailConfigMappingFilters class.
		/// </summary>
		public EmailConfigMappingFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the EmailConfigMappingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EmailConfigMappingFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EmailConfigMappingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EmailConfigMappingFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EmailConfigMappingFilters
	
	#region EmailConfigMappingQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="EmailConfigMappingParameterBuilder"/> class
	/// that is used exclusively with a <see cref="EmailConfigMapping"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailConfigMappingQuery : EmailConfigMappingParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailConfigMappingQuery class.
		/// </summary>
		public EmailConfigMappingQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the EmailConfigMappingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EmailConfigMappingQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EmailConfigMappingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EmailConfigMappingQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EmailConfigMappingQuery
		
	#region InvoiceCommPercentageFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="InvoiceCommPercentage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class InvoiceCommPercentageFilters : InvoiceCommPercentageFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the InvoiceCommPercentageFilters class.
		/// </summary>
		public InvoiceCommPercentageFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the InvoiceCommPercentageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public InvoiceCommPercentageFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the InvoiceCommPercentageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public InvoiceCommPercentageFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion InvoiceCommPercentageFilters
	
	#region InvoiceCommPercentageQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="InvoiceCommPercentageParameterBuilder"/> class
	/// that is used exclusively with a <see cref="InvoiceCommPercentage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class InvoiceCommPercentageQuery : InvoiceCommPercentageParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the InvoiceCommPercentageQuery class.
		/// </summary>
		public InvoiceCommPercentageQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the InvoiceCommPercentageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public InvoiceCommPercentageQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the InvoiceCommPercentageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public InvoiceCommPercentageQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion InvoiceCommPercentageQuery
		
	#region AvailabilityWithSpandpFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AvailabilityWithSpandp"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AvailabilityWithSpandpFilters : AvailabilityWithSpandpFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AvailabilityWithSpandpFilters class.
		/// </summary>
		public AvailabilityWithSpandpFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AvailabilityWithSpandpFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AvailabilityWithSpandpFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AvailabilityWithSpandpFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AvailabilityWithSpandpFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AvailabilityWithSpandpFilters
	
	#region AvailabilityWithSpandpQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AvailabilityWithSpandpParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AvailabilityWithSpandp"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AvailabilityWithSpandpQuery : AvailabilityWithSpandpParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AvailabilityWithSpandpQuery class.
		/// </summary>
		public AvailabilityWithSpandpQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AvailabilityWithSpandpQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AvailabilityWithSpandpQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AvailabilityWithSpandpQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AvailabilityWithSpandpQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AvailabilityWithSpandpQuery
		
	#region BookingRequestViewListFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BookingRequestViewList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BookingRequestViewListFilters : BookingRequestViewListFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BookingRequestViewListFilters class.
		/// </summary>
		public BookingRequestViewListFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the BookingRequestViewListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BookingRequestViewListFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BookingRequestViewListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BookingRequestViewListFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BookingRequestViewListFilters
	
	#region BookingRequestViewListQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="BookingRequestViewListParameterBuilder"/> class
	/// that is used exclusively with a <see cref="BookingRequestViewList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BookingRequestViewListQuery : BookingRequestViewListParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BookingRequestViewListQuery class.
		/// </summary>
		public BookingRequestViewListQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the BookingRequestViewListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BookingRequestViewListQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BookingRequestViewListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BookingRequestViewListQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BookingRequestViewListQuery
		
	#region ViewAvailabilityAndSpecialManagerFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewAvailabilityAndSpecialManager"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewAvailabilityAndSpecialManagerFilters : ViewAvailabilityAndSpecialManagerFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityAndSpecialManagerFilters class.
		/// </summary>
		public ViewAvailabilityAndSpecialManagerFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityAndSpecialManagerFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewAvailabilityAndSpecialManagerFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityAndSpecialManagerFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewAvailabilityAndSpecialManagerFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewAvailabilityAndSpecialManagerFilters
	
	#region ViewAvailabilityAndSpecialManagerQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewAvailabilityAndSpecialManagerParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewAvailabilityAndSpecialManager"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewAvailabilityAndSpecialManagerQuery : ViewAvailabilityAndSpecialManagerParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityAndSpecialManagerQuery class.
		/// </summary>
		public ViewAvailabilityAndSpecialManagerQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityAndSpecialManagerQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewAvailabilityAndSpecialManagerQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityAndSpecialManagerQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewAvailabilityAndSpecialManagerQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewAvailabilityAndSpecialManagerQuery
		
	#region ViewAvailabilityOfBedRoomFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewAvailabilityOfBedRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewAvailabilityOfBedRoomFilters : ViewAvailabilityOfBedRoomFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfBedRoomFilters class.
		/// </summary>
		public ViewAvailabilityOfBedRoomFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfBedRoomFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewAvailabilityOfBedRoomFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfBedRoomFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewAvailabilityOfBedRoomFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewAvailabilityOfBedRoomFilters
	
	#region ViewAvailabilityOfBedRoomQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewAvailabilityOfBedRoomParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewAvailabilityOfBedRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewAvailabilityOfBedRoomQuery : ViewAvailabilityOfBedRoomParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfBedRoomQuery class.
		/// </summary>
		public ViewAvailabilityOfBedRoomQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfBedRoomQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewAvailabilityOfBedRoomQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfBedRoomQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewAvailabilityOfBedRoomQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewAvailabilityOfBedRoomQuery
		
	#region ViewAvailabilityOfRoomsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewAvailabilityOfRooms"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewAvailabilityOfRoomsFilters : ViewAvailabilityOfRoomsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfRoomsFilters class.
		/// </summary>
		public ViewAvailabilityOfRoomsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfRoomsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewAvailabilityOfRoomsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfRoomsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewAvailabilityOfRoomsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewAvailabilityOfRoomsFilters
	
	#region ViewAvailabilityOfRoomsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewAvailabilityOfRoomsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewAvailabilityOfRooms"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewAvailabilityOfRoomsQuery : ViewAvailabilityOfRoomsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfRoomsQuery class.
		/// </summary>
		public ViewAvailabilityOfRoomsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfRoomsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewAvailabilityOfRoomsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfRoomsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewAvailabilityOfRoomsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewAvailabilityOfRoomsQuery
		
	#region ViewBookingHotelsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewBookingHotels"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewBookingHotelsFilters : ViewBookingHotelsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewBookingHotelsFilters class.
		/// </summary>
		public ViewBookingHotelsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewBookingHotelsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewBookingHotelsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewBookingHotelsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewBookingHotelsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewBookingHotelsFilters
	
	#region ViewBookingHotelsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewBookingHotelsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewBookingHotels"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewBookingHotelsQuery : ViewBookingHotelsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewBookingHotelsQuery class.
		/// </summary>
		public ViewBookingHotelsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewBookingHotelsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewBookingHotelsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewBookingHotelsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewBookingHotelsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewBookingHotelsQuery
		
	#region ViewbookingrequestFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Viewbookingrequest"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewbookingrequestFilters : ViewbookingrequestFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewbookingrequestFilters class.
		/// </summary>
		public ViewbookingrequestFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewbookingrequestFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewbookingrequestFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewbookingrequestFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewbookingrequestFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewbookingrequestFilters
	
	#region ViewbookingrequestQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewbookingrequestParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Viewbookingrequest"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewbookingrequestQuery : ViewbookingrequestParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewbookingrequestQuery class.
		/// </summary>
		public ViewbookingrequestQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewbookingrequestQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewbookingrequestQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewbookingrequestQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewbookingrequestQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewbookingrequestQuery
		
	#region ViewClientContractFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewClientContract"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewClientContractFilters : ViewClientContractFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewClientContractFilters class.
		/// </summary>
		public ViewClientContractFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewClientContractFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewClientContractFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewClientContractFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewClientContractFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewClientContractFilters
	
	#region ViewClientContractQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewClientContractParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewClientContract"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewClientContractQuery : ViewClientContractParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewClientContractQuery class.
		/// </summary>
		public ViewClientContractQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewClientContractQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewClientContractQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewClientContractQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewClientContractQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewClientContractQuery
		
	#region ViewFacilitiesFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewFacilities"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewFacilitiesFilters : ViewFacilitiesFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewFacilitiesFilters class.
		/// </summary>
		public ViewFacilitiesFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewFacilitiesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewFacilitiesFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewFacilitiesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewFacilitiesFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewFacilitiesFilters
	
	#region ViewFacilitiesQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewFacilitiesParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewFacilities"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewFacilitiesQuery : ViewFacilitiesParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewFacilitiesQuery class.
		/// </summary>
		public ViewFacilitiesQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewFacilitiesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewFacilitiesQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewFacilitiesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewFacilitiesQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewFacilitiesQuery
		
	#region ViewFacilityIconFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewFacilityIcon"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewFacilityIconFilters : ViewFacilityIconFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewFacilityIconFilters class.
		/// </summary>
		public ViewFacilityIconFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewFacilityIconFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewFacilityIconFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewFacilityIconFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewFacilityIconFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewFacilityIconFilters
	
	#region ViewFacilityIconQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewFacilityIconParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewFacilityIcon"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewFacilityIconQuery : ViewFacilityIconParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewFacilityIconQuery class.
		/// </summary>
		public ViewFacilityIconQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewFacilityIconQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewFacilityIconQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewFacilityIconQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewFacilityIconQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewFacilityIconQuery
		
	#region ViewFindAvailableBedroomFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewFindAvailableBedroom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewFindAvailableBedroomFilters : ViewFindAvailableBedroomFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewFindAvailableBedroomFilters class.
		/// </summary>
		public ViewFindAvailableBedroomFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewFindAvailableBedroomFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewFindAvailableBedroomFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewFindAvailableBedroomFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewFindAvailableBedroomFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewFindAvailableBedroomFilters
	
	#region ViewFindAvailableBedroomQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewFindAvailableBedroomParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewFindAvailableBedroom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewFindAvailableBedroomQuery : ViewFindAvailableBedroomParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewFindAvailableBedroomQuery class.
		/// </summary>
		public ViewFindAvailableBedroomQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewFindAvailableBedroomQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewFindAvailableBedroomQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewFindAvailableBedroomQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewFindAvailableBedroomQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewFindAvailableBedroomQuery
		
	#region ViewForAvailabilityCheckFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewForAvailabilityCheck"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewForAvailabilityCheckFilters : ViewForAvailabilityCheckFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewForAvailabilityCheckFilters class.
		/// </summary>
		public ViewForAvailabilityCheckFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewForAvailabilityCheckFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewForAvailabilityCheckFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewForAvailabilityCheckFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewForAvailabilityCheckFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewForAvailabilityCheckFilters
	
	#region ViewForAvailabilityCheckQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewForAvailabilityCheckParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewForAvailabilityCheck"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewForAvailabilityCheckQuery : ViewForAvailabilityCheckParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewForAvailabilityCheckQuery class.
		/// </summary>
		public ViewForAvailabilityCheckQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewForAvailabilityCheckQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewForAvailabilityCheckQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewForAvailabilityCheckQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewForAvailabilityCheckQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewForAvailabilityCheckQuery
		
	#region ViewForCommissionFieldsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewForCommissionFields"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewForCommissionFieldsFilters : ViewForCommissionFieldsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewForCommissionFieldsFilters class.
		/// </summary>
		public ViewForCommissionFieldsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewForCommissionFieldsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewForCommissionFieldsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewForCommissionFieldsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewForCommissionFieldsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewForCommissionFieldsFilters
	
	#region ViewForCommissionFieldsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewForCommissionFieldsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewForCommissionFields"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewForCommissionFieldsQuery : ViewForCommissionFieldsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewForCommissionFieldsQuery class.
		/// </summary>
		public ViewForCommissionFieldsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewForCommissionFieldsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewForCommissionFieldsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewForCommissionFieldsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewForCommissionFieldsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewForCommissionFieldsQuery
		
	#region ViewForRequestSearchFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewForRequestSearch"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewForRequestSearchFilters : ViewForRequestSearchFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewForRequestSearchFilters class.
		/// </summary>
		public ViewForRequestSearchFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewForRequestSearchFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewForRequestSearchFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewForRequestSearchFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewForRequestSearchFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewForRequestSearchFilters
	
	#region ViewForRequestSearchQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewForRequestSearchParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewForRequestSearch"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewForRequestSearchQuery : ViewForRequestSearchParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewForRequestSearchQuery class.
		/// </summary>
		public ViewForRequestSearchQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewForRequestSearchQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewForRequestSearchQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewForRequestSearchQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewForRequestSearchQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewForRequestSearchQuery
		
	#region ViewForSetInvoiceFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewForSetInvoice"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewForSetInvoiceFilters : ViewForSetInvoiceFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewForSetInvoiceFilters class.
		/// </summary>
		public ViewForSetInvoiceFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewForSetInvoiceFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewForSetInvoiceFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewForSetInvoiceFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewForSetInvoiceFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewForSetInvoiceFilters
	
	#region ViewForSetInvoiceQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewForSetInvoiceParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewForSetInvoice"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewForSetInvoiceQuery : ViewForSetInvoiceParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewForSetInvoiceQuery class.
		/// </summary>
		public ViewForSetInvoiceQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewForSetInvoiceQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewForSetInvoiceQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewForSetInvoiceQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewForSetInvoiceQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewForSetInvoiceQuery
		
	#region ViewMeetingRoomAvailabilityFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewMeetingRoomAvailability"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewMeetingRoomAvailabilityFilters : ViewMeetingRoomAvailabilityFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewMeetingRoomAvailabilityFilters class.
		/// </summary>
		public ViewMeetingRoomAvailabilityFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewMeetingRoomAvailabilityFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewMeetingRoomAvailabilityFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewMeetingRoomAvailabilityFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewMeetingRoomAvailabilityFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewMeetingRoomAvailabilityFilters
	
	#region ViewMeetingRoomAvailabilityQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewMeetingRoomAvailabilityParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewMeetingRoomAvailability"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewMeetingRoomAvailabilityQuery : ViewMeetingRoomAvailabilityParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewMeetingRoomAvailabilityQuery class.
		/// </summary>
		public ViewMeetingRoomAvailabilityQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewMeetingRoomAvailabilityQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewMeetingRoomAvailabilityQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewMeetingRoomAvailabilityQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewMeetingRoomAvailabilityQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewMeetingRoomAvailabilityQuery
		
	#region ViewMeetingRoomForRequestFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewMeetingRoomForRequest"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewMeetingRoomForRequestFilters : ViewMeetingRoomForRequestFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewMeetingRoomForRequestFilters class.
		/// </summary>
		public ViewMeetingRoomForRequestFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewMeetingRoomForRequestFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewMeetingRoomForRequestFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewMeetingRoomForRequestFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewMeetingRoomForRequestFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewMeetingRoomForRequestFilters
	
	#region ViewMeetingRoomForRequestQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewMeetingRoomForRequestParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewMeetingRoomForRequest"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewMeetingRoomForRequestQuery : ViewMeetingRoomForRequestParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewMeetingRoomForRequestQuery class.
		/// </summary>
		public ViewMeetingRoomForRequestQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewMeetingRoomForRequestQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewMeetingRoomForRequestQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewMeetingRoomForRequestQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewMeetingRoomForRequestQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewMeetingRoomForRequestQuery
		
	#region ViewReportAvailabilityFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportAvailability"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportAvailabilityFilters : ViewReportAvailabilityFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportAvailabilityFilters class.
		/// </summary>
		public ViewReportAvailabilityFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportAvailabilityFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportAvailabilityFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportAvailabilityFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportAvailabilityFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportAvailabilityFilters
	
	#region ViewReportAvailabilityQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewReportAvailabilityParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewReportAvailability"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportAvailabilityQuery : ViewReportAvailabilityParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportAvailabilityQuery class.
		/// </summary>
		public ViewReportAvailabilityQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportAvailabilityQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportAvailabilityQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportAvailabilityQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportAvailabilityQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportAvailabilityQuery
		
	#region ViewReportBookingRequestHistoryFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportBookingRequestHistory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportBookingRequestHistoryFilters : ViewReportBookingRequestHistoryFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportBookingRequestHistoryFilters class.
		/// </summary>
		public ViewReportBookingRequestHistoryFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportBookingRequestHistoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportBookingRequestHistoryFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportBookingRequestHistoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportBookingRequestHistoryFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportBookingRequestHistoryFilters
	
	#region ViewReportBookingRequestHistoryQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewReportBookingRequestHistoryParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewReportBookingRequestHistory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportBookingRequestHistoryQuery : ViewReportBookingRequestHistoryParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportBookingRequestHistoryQuery class.
		/// </summary>
		public ViewReportBookingRequestHistoryQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportBookingRequestHistoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportBookingRequestHistoryQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportBookingRequestHistoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportBookingRequestHistoryQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportBookingRequestHistoryQuery
		
	#region ViewReportCancelationFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportCancelation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportCancelationFilters : ViewReportCancelationFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportCancelationFilters class.
		/// </summary>
		public ViewReportCancelationFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportCancelationFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportCancelationFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportCancelationFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportCancelationFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportCancelationFilters
	
	#region ViewReportCancelationQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewReportCancelationParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewReportCancelation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportCancelationQuery : ViewReportCancelationParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportCancelationQuery class.
		/// </summary>
		public ViewReportCancelationQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportCancelationQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportCancelationQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportCancelationQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportCancelationQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportCancelationQuery
		
	#region ViewReportCommissionControlFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportCommissionControl"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportCommissionControlFilters : ViewReportCommissionControlFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportCommissionControlFilters class.
		/// </summary>
		public ViewReportCommissionControlFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportCommissionControlFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportCommissionControlFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportCommissionControlFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportCommissionControlFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportCommissionControlFilters
	
	#region ViewReportCommissionControlQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewReportCommissionControlParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewReportCommissionControl"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportCommissionControlQuery : ViewReportCommissionControlParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportCommissionControlQuery class.
		/// </summary>
		public ViewReportCommissionControlQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportCommissionControlQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportCommissionControlQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportCommissionControlQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportCommissionControlQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportCommissionControlQuery
		
	#region ViewReportforProfileCompanyAgencyFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportforProfileCompanyAgency"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportforProfileCompanyAgencyFilters : ViewReportforProfileCompanyAgencyFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportforProfileCompanyAgencyFilters class.
		/// </summary>
		public ViewReportforProfileCompanyAgencyFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportforProfileCompanyAgencyFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportforProfileCompanyAgencyFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportforProfileCompanyAgencyFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportforProfileCompanyAgencyFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportforProfileCompanyAgencyFilters
	
	#region ViewReportforProfileCompanyAgencyQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewReportforProfileCompanyAgencyParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewReportforProfileCompanyAgency"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportforProfileCompanyAgencyQuery : ViewReportforProfileCompanyAgencyParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportforProfileCompanyAgencyQuery class.
		/// </summary>
		public ViewReportforProfileCompanyAgencyQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportforProfileCompanyAgencyQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportforProfileCompanyAgencyQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportforProfileCompanyAgencyQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportforProfileCompanyAgencyQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportforProfileCompanyAgencyQuery
		
	#region ViewReportInventoryHotelFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportInventoryHotel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportInventoryHotelFilters : ViewReportInventoryHotelFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportInventoryHotelFilters class.
		/// </summary>
		public ViewReportInventoryHotelFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportInventoryHotelFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportInventoryHotelFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportInventoryHotelFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportInventoryHotelFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportInventoryHotelFilters
	
	#region ViewReportInventoryHotelQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewReportInventoryHotelParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewReportInventoryHotel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportInventoryHotelQuery : ViewReportInventoryHotelParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportInventoryHotelQuery class.
		/// </summary>
		public ViewReportInventoryHotelQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportInventoryHotelQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportInventoryHotelQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportInventoryHotelQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportInventoryHotelQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportInventoryHotelQuery
		
	#region ViewreportInventoryReportofExistingHotelFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewreportInventoryReportofExistingHotel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewreportInventoryReportofExistingHotelFilters : ViewreportInventoryReportofExistingHotelFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewreportInventoryReportofExistingHotelFilters class.
		/// </summary>
		public ViewreportInventoryReportofExistingHotelFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewreportInventoryReportofExistingHotelFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewreportInventoryReportofExistingHotelFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewreportInventoryReportofExistingHotelFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewreportInventoryReportofExistingHotelFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewreportInventoryReportofExistingHotelFilters
	
	#region ViewreportInventoryReportofExistingHotelQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewreportInventoryReportofExistingHotelParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewreportInventoryReportofExistingHotel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewreportInventoryReportofExistingHotelQuery : ViewreportInventoryReportofExistingHotelParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewreportInventoryReportofExistingHotelQuery class.
		/// </summary>
		public ViewreportInventoryReportofExistingHotelQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewreportInventoryReportofExistingHotelQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewreportInventoryReportofExistingHotelQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewreportInventoryReportofExistingHotelQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewreportInventoryReportofExistingHotelQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewreportInventoryReportofExistingHotelQuery
		
	#region ViewReportLeadTimeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportLeadTime"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportLeadTimeFilters : ViewReportLeadTimeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportLeadTimeFilters class.
		/// </summary>
		public ViewReportLeadTimeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportLeadTimeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportLeadTimeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportLeadTimeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportLeadTimeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportLeadTimeFilters
	
	#region ViewReportLeadTimeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewReportLeadTimeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewReportLeadTime"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportLeadTimeQuery : ViewReportLeadTimeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportLeadTimeQuery class.
		/// </summary>
		public ViewReportLeadTimeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportLeadTimeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportLeadTimeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportLeadTimeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportLeadTimeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportLeadTimeQuery
		
	#region ViewReportOnLineInventoryReportFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportOnLineInventoryReport"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportOnLineInventoryReportFilters : ViewReportOnLineInventoryReportFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportOnLineInventoryReportFilters class.
		/// </summary>
		public ViewReportOnLineInventoryReportFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportOnLineInventoryReportFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportOnLineInventoryReportFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportOnLineInventoryReportFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportOnLineInventoryReportFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportOnLineInventoryReportFilters
	
	#region ViewReportOnLineInventoryReportQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewReportOnLineInventoryReportParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewReportOnLineInventoryReport"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportOnLineInventoryReportQuery : ViewReportOnLineInventoryReportParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportOnLineInventoryReportQuery class.
		/// </summary>
		public ViewReportOnLineInventoryReportQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportOnLineInventoryReportQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportOnLineInventoryReportQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportOnLineInventoryReportQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportOnLineInventoryReportQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportOnLineInventoryReportQuery
		
	#region ViewReportProductionCompanyAgencyFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportProductionCompanyAgency"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportProductionCompanyAgencyFilters : ViewReportProductionCompanyAgencyFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionCompanyAgencyFilters class.
		/// </summary>
		public ViewReportProductionCompanyAgencyFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionCompanyAgencyFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportProductionCompanyAgencyFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionCompanyAgencyFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportProductionCompanyAgencyFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportProductionCompanyAgencyFilters
	
	#region ViewReportProductionCompanyAgencyQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewReportProductionCompanyAgencyParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewReportProductionCompanyAgency"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportProductionCompanyAgencyQuery : ViewReportProductionCompanyAgencyParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionCompanyAgencyQuery class.
		/// </summary>
		public ViewReportProductionCompanyAgencyQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionCompanyAgencyQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportProductionCompanyAgencyQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionCompanyAgencyQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportProductionCompanyAgencyQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportProductionCompanyAgencyQuery
		
	#region ViewReportProductionReportHotelFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportProductionReportHotel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportProductionReportHotelFilters : ViewReportProductionReportHotelFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionReportHotelFilters class.
		/// </summary>
		public ViewReportProductionReportHotelFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionReportHotelFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportProductionReportHotelFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionReportHotelFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportProductionReportHotelFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportProductionReportHotelFilters
	
	#region ViewReportProductionReportHotelQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewReportProductionReportHotelParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewReportProductionReportHotel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportProductionReportHotelQuery : ViewReportProductionReportHotelParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionReportHotelQuery class.
		/// </summary>
		public ViewReportProductionReportHotelQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionReportHotelQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportProductionReportHotelQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionReportHotelQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportProductionReportHotelQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportProductionReportHotelQuery
		
	#region ViewReportRankingFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportRanking"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportRankingFilters : ViewReportRankingFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportRankingFilters class.
		/// </summary>
		public ViewReportRankingFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportRankingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportRankingFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportRankingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportRankingFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportRankingFilters
	
	#region ViewReportRankingQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewReportRankingParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewReportRanking"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportRankingQuery : ViewReportRankingParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportRankingQuery class.
		/// </summary>
		public ViewReportRankingQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportRankingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportRankingQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportRankingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportRankingQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportRankingQuery
		
	#region ViewRequestFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewRequest"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewRequestFilters : ViewRequestFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewRequestFilters class.
		/// </summary>
		public ViewRequestFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewRequestFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewRequestFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewRequestFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewRequestFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewRequestFilters
	
	#region ViewRequestQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewRequestParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewRequest"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewRequestQuery : ViewRequestParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewRequestQuery class.
		/// </summary>
		public ViewRequestQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewRequestQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewRequestQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewRequestQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewRequestQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewRequestQuery
		
	#region ViewSearchForDay2Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewSearchForDay2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewSearchForDay2Filters : ViewSearchForDay2FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewSearchForDay2Filters class.
		/// </summary>
		public ViewSearchForDay2Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewSearchForDay2Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewSearchForDay2Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewSearchForDay2Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewSearchForDay2Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewSearchForDay2Filters
	
	#region ViewSearchForDay2Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewSearchForDay2ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewSearchForDay2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewSearchForDay2Query : ViewSearchForDay2ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewSearchForDay2Query class.
		/// </summary>
		public ViewSearchForDay2Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewSearchForDay2Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewSearchForDay2Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewSearchForDay2Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewSearchForDay2Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewSearchForDay2Query
		
	#region ViewSelectAllDetailsByPackageandMrFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewSelectAllDetailsByPackageandMr"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewSelectAllDetailsByPackageandMrFilters : ViewSelectAllDetailsByPackageandMrFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewSelectAllDetailsByPackageandMrFilters class.
		/// </summary>
		public ViewSelectAllDetailsByPackageandMrFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewSelectAllDetailsByPackageandMrFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewSelectAllDetailsByPackageandMrFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewSelectAllDetailsByPackageandMrFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewSelectAllDetailsByPackageandMrFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewSelectAllDetailsByPackageandMrFilters
	
	#region ViewSelectAllDetailsByPackageandMrQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewSelectAllDetailsByPackageandMrParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewSelectAllDetailsByPackageandMr"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewSelectAllDetailsByPackageandMrQuery : ViewSelectAllDetailsByPackageandMrParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewSelectAllDetailsByPackageandMrQuery class.
		/// </summary>
		public ViewSelectAllDetailsByPackageandMrQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewSelectAllDetailsByPackageandMrQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewSelectAllDetailsByPackageandMrQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewSelectAllDetailsByPackageandMrQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewSelectAllDetailsByPackageandMrQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewSelectAllDetailsByPackageandMrQuery
		
	#region ViewSelectAvailabilityFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewSelectAvailability"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewSelectAvailabilityFilters : ViewSelectAvailabilityFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewSelectAvailabilityFilters class.
		/// </summary>
		public ViewSelectAvailabilityFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewSelectAvailabilityFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewSelectAvailabilityFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewSelectAvailabilityFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewSelectAvailabilityFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewSelectAvailabilityFilters
	
	#region ViewSelectAvailabilityQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewSelectAvailabilityParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewSelectAvailability"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewSelectAvailabilityQuery : ViewSelectAvailabilityParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewSelectAvailabilityQuery class.
		/// </summary>
		public ViewSelectAvailabilityQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewSelectAvailabilityQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewSelectAvailabilityQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewSelectAvailabilityQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewSelectAvailabilityQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewSelectAvailabilityQuery
		
	#region ViewSpandPpercentageOfMeetingRoomFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewSpandPpercentageOfMeetingRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewSpandPpercentageOfMeetingRoomFilters : ViewSpandPpercentageOfMeetingRoomFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfMeetingRoomFilters class.
		/// </summary>
		public ViewSpandPpercentageOfMeetingRoomFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfMeetingRoomFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewSpandPpercentageOfMeetingRoomFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfMeetingRoomFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewSpandPpercentageOfMeetingRoomFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewSpandPpercentageOfMeetingRoomFilters
	
	#region ViewSpandPpercentageOfMeetingRoomQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewSpandPpercentageOfMeetingRoomParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewSpandPpercentageOfMeetingRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewSpandPpercentageOfMeetingRoomQuery : ViewSpandPpercentageOfMeetingRoomParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfMeetingRoomQuery class.
		/// </summary>
		public ViewSpandPpercentageOfMeetingRoomQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfMeetingRoomQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewSpandPpercentageOfMeetingRoomQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfMeetingRoomQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewSpandPpercentageOfMeetingRoomQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewSpandPpercentageOfMeetingRoomQuery
		
	#region ViewSpandPpercentageOfPackageFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewSpandPpercentageOfPackage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewSpandPpercentageOfPackageFilters : ViewSpandPpercentageOfPackageFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfPackageFilters class.
		/// </summary>
		public ViewSpandPpercentageOfPackageFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfPackageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewSpandPpercentageOfPackageFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfPackageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewSpandPpercentageOfPackageFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewSpandPpercentageOfPackageFilters
	
	#region ViewSpandPpercentageOfPackageQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewSpandPpercentageOfPackageParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewSpandPpercentageOfPackage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewSpandPpercentageOfPackageQuery : ViewSpandPpercentageOfPackageParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfPackageQuery class.
		/// </summary>
		public ViewSpandPpercentageOfPackageQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfPackageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewSpandPpercentageOfPackageQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfPackageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewSpandPpercentageOfPackageQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewSpandPpercentageOfPackageQuery
		
	#region ViewWsCheckAvailabilityFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewWsCheckAvailability"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewWsCheckAvailabilityFilters : ViewWsCheckAvailabilityFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewWsCheckAvailabilityFilters class.
		/// </summary>
		public ViewWsCheckAvailabilityFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewWsCheckAvailabilityFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewWsCheckAvailabilityFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewWsCheckAvailabilityFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewWsCheckAvailabilityFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewWsCheckAvailabilityFilters
	
	#region ViewWsCheckAvailabilityQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ViewWsCheckAvailabilityParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ViewWsCheckAvailability"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewWsCheckAvailabilityQuery : ViewWsCheckAvailabilityParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewWsCheckAvailabilityQuery class.
		/// </summary>
		public ViewWsCheckAvailabilityQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewWsCheckAvailabilityQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewWsCheckAvailabilityQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewWsCheckAvailabilityQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewWsCheckAvailabilityQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewWsCheckAvailabilityQuery
	#endregion

	
}
