﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="BedRoomPictureImageProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class BedRoomPictureImageProviderBaseCore : EntityProviderBase<LMMR.Entities.BedRoomPictureImage, LMMR.Entities.BedRoomPictureImageKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.BedRoomPictureImageKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomPictureImage_BedRoom key.
		///		FK_BedRoomPictureImage_BedRoom Description: 
		/// </summary>
		/// <param name="_bedRoomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomPictureImage objects.</returns>
		public TList<BedRoomPictureImage> GetByBedRoomId(System.Int64 _bedRoomId)
		{
			int count = -1;
			return GetByBedRoomId(_bedRoomId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomPictureImage_BedRoom key.
		///		FK_BedRoomPictureImage_BedRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bedRoomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomPictureImage objects.</returns>
		/// <remarks></remarks>
		public TList<BedRoomPictureImage> GetByBedRoomId(TransactionManager transactionManager, System.Int64 _bedRoomId)
		{
			int count = -1;
			return GetByBedRoomId(transactionManager, _bedRoomId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomPictureImage_BedRoom key.
		///		FK_BedRoomPictureImage_BedRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bedRoomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomPictureImage objects.</returns>
		public TList<BedRoomPictureImage> GetByBedRoomId(TransactionManager transactionManager, System.Int64 _bedRoomId, int start, int pageLength)
		{
			int count = -1;
			return GetByBedRoomId(transactionManager, _bedRoomId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomPictureImage_BedRoom key.
		///		fkBedRoomPictureImageBedRoom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bedRoomId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomPictureImage objects.</returns>
		public TList<BedRoomPictureImage> GetByBedRoomId(System.Int64 _bedRoomId, int start, int pageLength)
		{
			int count =  -1;
			return GetByBedRoomId(null, _bedRoomId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomPictureImage_BedRoom key.
		///		fkBedRoomPictureImageBedRoom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bedRoomId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomPictureImage objects.</returns>
		public TList<BedRoomPictureImage> GetByBedRoomId(System.Int64 _bedRoomId, int start, int pageLength,out int count)
		{
			return GetByBedRoomId(null, _bedRoomId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomPictureImage_BedRoom key.
		///		FK_BedRoomPictureImage_BedRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bedRoomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomPictureImage objects.</returns>
		public abstract TList<BedRoomPictureImage> GetByBedRoomId(TransactionManager transactionManager, System.Int64 _bedRoomId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomPictureImage_Users key.
		///		FK_BedRoomPictureImage_Users Description: 
		/// </summary>
		/// <param name="_updatedBy"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomPictureImage objects.</returns>
		public TList<BedRoomPictureImage> GetByUpdatedBy(System.Int64? _updatedBy)
		{
			int count = -1;
			return GetByUpdatedBy(_updatedBy, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomPictureImage_Users key.
		///		FK_BedRoomPictureImage_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_updatedBy"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomPictureImage objects.</returns>
		/// <remarks></remarks>
		public TList<BedRoomPictureImage> GetByUpdatedBy(TransactionManager transactionManager, System.Int64? _updatedBy)
		{
			int count = -1;
			return GetByUpdatedBy(transactionManager, _updatedBy, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomPictureImage_Users key.
		///		FK_BedRoomPictureImage_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_updatedBy"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomPictureImage objects.</returns>
		public TList<BedRoomPictureImage> GetByUpdatedBy(TransactionManager transactionManager, System.Int64? _updatedBy, int start, int pageLength)
		{
			int count = -1;
			return GetByUpdatedBy(transactionManager, _updatedBy, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomPictureImage_Users key.
		///		fkBedRoomPictureImageUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_updatedBy"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomPictureImage objects.</returns>
		public TList<BedRoomPictureImage> GetByUpdatedBy(System.Int64? _updatedBy, int start, int pageLength)
		{
			int count =  -1;
			return GetByUpdatedBy(null, _updatedBy, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomPictureImage_Users key.
		///		fkBedRoomPictureImageUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_updatedBy"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomPictureImage objects.</returns>
		public TList<BedRoomPictureImage> GetByUpdatedBy(System.Int64? _updatedBy, int start, int pageLength,out int count)
		{
			return GetByUpdatedBy(null, _updatedBy, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomPictureImage_Users key.
		///		FK_BedRoomPictureImage_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_updatedBy"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomPictureImage objects.</returns>
		public abstract TList<BedRoomPictureImage> GetByUpdatedBy(TransactionManager transactionManager, System.Int64? _updatedBy, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.BedRoomPictureImage Get(TransactionManager transactionManager, LMMR.Entities.BedRoomPictureImageKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_BedRoomPictureImage index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomPictureImage"/> class.</returns>
		public LMMR.Entities.BedRoomPictureImage GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoomPictureImage index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomPictureImage"/> class.</returns>
		public LMMR.Entities.BedRoomPictureImage GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoomPictureImage index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomPictureImage"/> class.</returns>
		public LMMR.Entities.BedRoomPictureImage GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoomPictureImage index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomPictureImage"/> class.</returns>
		public LMMR.Entities.BedRoomPictureImage GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoomPictureImage index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomPictureImage"/> class.</returns>
		public LMMR.Entities.BedRoomPictureImage GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoomPictureImage index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomPictureImage"/> class.</returns>
		public abstract LMMR.Entities.BedRoomPictureImage GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;BedRoomPictureImage&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;BedRoomPictureImage&gt;"/></returns>
		public static TList<BedRoomPictureImage> Fill(IDataReader reader, TList<BedRoomPictureImage> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.BedRoomPictureImage c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("BedRoomPictureImage")
					.Append("|").Append((System.Int64)reader[((int)BedRoomPictureImageColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<BedRoomPictureImage>(
					key.ToString(), // EntityTrackingKey
					"BedRoomPictureImage",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.BedRoomPictureImage();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)BedRoomPictureImageColumn.Id - 1)];
					c.BedRoomId = (System.Int64)reader[((int)BedRoomPictureImageColumn.BedRoomId - 1)];
					c.FileType = (reader.IsDBNull(((int)BedRoomPictureImageColumn.FileType - 1)))?null:(System.Int32?)reader[((int)BedRoomPictureImageColumn.FileType - 1)];
					c.ImageName = (reader.IsDBNull(((int)BedRoomPictureImageColumn.ImageName - 1)))?null:(System.String)reader[((int)BedRoomPictureImageColumn.ImageName - 1)];
					c.VideoName = (reader.IsDBNull(((int)BedRoomPictureImageColumn.VideoName - 1)))?null:(System.String)reader[((int)BedRoomPictureImageColumn.VideoName - 1)];
					c.FileName = (reader.IsDBNull(((int)BedRoomPictureImageColumn.FileName - 1)))?null:(System.String)reader[((int)BedRoomPictureImageColumn.FileName - 1)];
					c.AlterText = (reader.IsDBNull(((int)BedRoomPictureImageColumn.AlterText - 1)))?null:(System.String)reader[((int)BedRoomPictureImageColumn.AlterText - 1)];
					c.IsMain = (System.Boolean)reader[((int)BedRoomPictureImageColumn.IsMain - 1)];
					c.UpdatedBy = (reader.IsDBNull(((int)BedRoomPictureImageColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)BedRoomPictureImageColumn.UpdatedBy - 1)];
					c.OrderNumber = (reader.IsDBNull(((int)BedRoomPictureImageColumn.OrderNumber - 1)))?null:(System.Int32?)reader[((int)BedRoomPictureImageColumn.OrderNumber - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.BedRoomPictureImage"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BedRoomPictureImage"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.BedRoomPictureImage entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)BedRoomPictureImageColumn.Id - 1)];
			entity.BedRoomId = (System.Int64)reader[((int)BedRoomPictureImageColumn.BedRoomId - 1)];
			entity.FileType = (reader.IsDBNull(((int)BedRoomPictureImageColumn.FileType - 1)))?null:(System.Int32?)reader[((int)BedRoomPictureImageColumn.FileType - 1)];
			entity.ImageName = (reader.IsDBNull(((int)BedRoomPictureImageColumn.ImageName - 1)))?null:(System.String)reader[((int)BedRoomPictureImageColumn.ImageName - 1)];
			entity.VideoName = (reader.IsDBNull(((int)BedRoomPictureImageColumn.VideoName - 1)))?null:(System.String)reader[((int)BedRoomPictureImageColumn.VideoName - 1)];
			entity.FileName = (reader.IsDBNull(((int)BedRoomPictureImageColumn.FileName - 1)))?null:(System.String)reader[((int)BedRoomPictureImageColumn.FileName - 1)];
			entity.AlterText = (reader.IsDBNull(((int)BedRoomPictureImageColumn.AlterText - 1)))?null:(System.String)reader[((int)BedRoomPictureImageColumn.AlterText - 1)];
			entity.IsMain = (System.Boolean)reader[((int)BedRoomPictureImageColumn.IsMain - 1)];
			entity.UpdatedBy = (reader.IsDBNull(((int)BedRoomPictureImageColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)BedRoomPictureImageColumn.UpdatedBy - 1)];
			entity.OrderNumber = (reader.IsDBNull(((int)BedRoomPictureImageColumn.OrderNumber - 1)))?null:(System.Int32?)reader[((int)BedRoomPictureImageColumn.OrderNumber - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.BedRoomPictureImage"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BedRoomPictureImage"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.BedRoomPictureImage entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.BedRoomId = (System.Int64)dataRow["BedRoomId"];
			entity.FileType = Convert.IsDBNull(dataRow["FileType"]) ? null : (System.Int32?)dataRow["FileType"];
			entity.ImageName = Convert.IsDBNull(dataRow["ImageName"]) ? null : (System.String)dataRow["ImageName"];
			entity.VideoName = Convert.IsDBNull(dataRow["VideoName"]) ? null : (System.String)dataRow["VideoName"];
			entity.FileName = Convert.IsDBNull(dataRow["FileName"]) ? null : (System.String)dataRow["FileName"];
			entity.AlterText = Convert.IsDBNull(dataRow["AlterText"]) ? null : (System.String)dataRow["AlterText"];
			entity.IsMain = (System.Boolean)dataRow["IsMain"];
			entity.UpdatedBy = Convert.IsDBNull(dataRow["UpdatedBy"]) ? null : (System.Int64?)dataRow["UpdatedBy"];
			entity.OrderNumber = Convert.IsDBNull(dataRow["OrderNumber"]) ? null : (System.Int32?)dataRow["OrderNumber"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BedRoomPictureImage"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.BedRoomPictureImage Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.BedRoomPictureImage entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region BedRoomIdSource	
			if (CanDeepLoad(entity, "BedRoom|BedRoomIdSource", deepLoadType, innerList) 
				&& entity.BedRoomIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.BedRoomId;
				BedRoom tmpEntity = EntityManager.LocateEntity<BedRoom>(EntityLocator.ConstructKeyFromPkItems(typeof(BedRoom), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.BedRoomIdSource = tmpEntity;
				else
					entity.BedRoomIdSource = DataRepository.BedRoomProvider.GetById(transactionManager, entity.BedRoomId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BedRoomIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.BedRoomIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.BedRoomProvider.DeepLoad(transactionManager, entity.BedRoomIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion BedRoomIdSource

			#region UpdatedBySource	
			if (CanDeepLoad(entity, "Users|UpdatedBySource", deepLoadType, innerList) 
				&& entity.UpdatedBySource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.UpdatedBy ?? (long)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.UpdatedBySource = tmpEntity;
				else
					entity.UpdatedBySource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.UpdatedBy ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UpdatedBySource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.UpdatedBySource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.UpdatedBySource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion UpdatedBySource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region BedRoomPictureImageTrailCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<BedRoomPictureImageTrail>|BedRoomPictureImageTrailCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BedRoomPictureImageTrailCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BedRoomPictureImageTrailCollection = DataRepository.BedRoomPictureImageTrailProvider.GetByBedRoomPicimgId(transactionManager, entity.Id);

				if (deep && entity.BedRoomPictureImageTrailCollection.Count > 0)
				{
					deepHandles.Add("BedRoomPictureImageTrailCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<BedRoomPictureImageTrail>) DataRepository.BedRoomPictureImageTrailProvider.DeepLoad,
						new object[] { transactionManager, entity.BedRoomPictureImageTrailCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.BedRoomPictureImage object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.BedRoomPictureImage instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.BedRoomPictureImage Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.BedRoomPictureImage entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region BedRoomIdSource
			if (CanDeepSave(entity, "BedRoom|BedRoomIdSource", deepSaveType, innerList) 
				&& entity.BedRoomIdSource != null)
			{
				DataRepository.BedRoomProvider.Save(transactionManager, entity.BedRoomIdSource);
				entity.BedRoomId = entity.BedRoomIdSource.Id;
			}
			#endregion 
			
			#region UpdatedBySource
			if (CanDeepSave(entity, "Users|UpdatedBySource", deepSaveType, innerList) 
				&& entity.UpdatedBySource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.UpdatedBySource);
				entity.UpdatedBy = entity.UpdatedBySource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<BedRoomPictureImageTrail>
				if (CanDeepSave(entity.BedRoomPictureImageTrailCollection, "List<BedRoomPictureImageTrail>|BedRoomPictureImageTrailCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(BedRoomPictureImageTrail child in entity.BedRoomPictureImageTrailCollection)
					{
						if(child.BedRoomPicimgIdSource != null)
						{
							child.BedRoomPicimgId = child.BedRoomPicimgIdSource.Id;
						}
						else
						{
							child.BedRoomPicimgId = entity.Id;
						}

					}

					if (entity.BedRoomPictureImageTrailCollection.Count > 0 || entity.BedRoomPictureImageTrailCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BedRoomPictureImageTrailProvider.Save(transactionManager, entity.BedRoomPictureImageTrailCollection);
						
						deepHandles.Add("BedRoomPictureImageTrailCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< BedRoomPictureImageTrail >) DataRepository.BedRoomPictureImageTrailProvider.DeepSave,
							new object[] { transactionManager, entity.BedRoomPictureImageTrailCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region BedRoomPictureImageChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.BedRoomPictureImage</c>
	///</summary>
	public enum BedRoomPictureImageChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>BedRoom</c> at BedRoomIdSource
		///</summary>
		[ChildEntityType(typeof(BedRoom))]
		BedRoom,
			
		///<summary>
		/// Composite Property for <c>Users</c> at UpdatedBySource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
	
		///<summary>
		/// Collection of <c>BedRoomPictureImage</c> as OneToMany for BedRoomPictureImageTrailCollection
		///</summary>
		[ChildEntityType(typeof(TList<BedRoomPictureImageTrail>))]
		BedRoomPictureImageTrailCollection,
	}
	
	#endregion BedRoomPictureImageChildEntityTypes
	
	#region BedRoomPictureImageFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;BedRoomPictureImageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BedRoomPictureImage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BedRoomPictureImageFilterBuilder : SqlFilterBuilder<BedRoomPictureImageColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageFilterBuilder class.
		/// </summary>
		public BedRoomPictureImageFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BedRoomPictureImageFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BedRoomPictureImageFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BedRoomPictureImageFilterBuilder
	
	#region BedRoomPictureImageParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;BedRoomPictureImageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BedRoomPictureImage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BedRoomPictureImageParameterBuilder : ParameterizedSqlFilterBuilder<BedRoomPictureImageColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageParameterBuilder class.
		/// </summary>
		public BedRoomPictureImageParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BedRoomPictureImageParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BedRoomPictureImageParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BedRoomPictureImageParameterBuilder
	
	#region BedRoomPictureImageSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;BedRoomPictureImageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BedRoomPictureImage"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class BedRoomPictureImageSortBuilder : SqlSortBuilder<BedRoomPictureImageColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageSqlSortBuilder class.
		/// </summary>
		public BedRoomPictureImageSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion BedRoomPictureImageSortBuilder
	
} // end namespace
