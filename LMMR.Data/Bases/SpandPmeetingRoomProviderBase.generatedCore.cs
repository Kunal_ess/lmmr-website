﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SpandPmeetingRoomProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SpandPmeetingRoomProviderBaseCore : EntityProviderBase<LMMR.Entities.SpandPmeetingRoom, LMMR.Entities.SpandPmeetingRoomKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.SpandPmeetingRoomKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SPandPmeetingRoom_MeetingRoom key.
		///		FK_SPandPmeetingRoom_MeetingRoom Description: 
		/// </summary>
		/// <param name="_meetingRoomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SpandPmeetingRoom objects.</returns>
		public TList<SpandPmeetingRoom> GetByMeetingRoomId(System.Int64 _meetingRoomId)
		{
			int count = -1;
			return GetByMeetingRoomId(_meetingRoomId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SPandPmeetingRoom_MeetingRoom key.
		///		FK_SPandPmeetingRoom_MeetingRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_meetingRoomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SpandPmeetingRoom objects.</returns>
		/// <remarks></remarks>
		public TList<SpandPmeetingRoom> GetByMeetingRoomId(TransactionManager transactionManager, System.Int64 _meetingRoomId)
		{
			int count = -1;
			return GetByMeetingRoomId(transactionManager, _meetingRoomId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_SPandPmeetingRoom_MeetingRoom key.
		///		FK_SPandPmeetingRoom_MeetingRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_meetingRoomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SpandPmeetingRoom objects.</returns>
		public TList<SpandPmeetingRoom> GetByMeetingRoomId(TransactionManager transactionManager, System.Int64 _meetingRoomId, int start, int pageLength)
		{
			int count = -1;
			return GetByMeetingRoomId(transactionManager, _meetingRoomId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SPandPmeetingRoom_MeetingRoom key.
		///		fkSpandPmeetingRoomMeetingRoom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_meetingRoomId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SpandPmeetingRoom objects.</returns>
		public TList<SpandPmeetingRoom> GetByMeetingRoomId(System.Int64 _meetingRoomId, int start, int pageLength)
		{
			int count =  -1;
			return GetByMeetingRoomId(null, _meetingRoomId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SPandPmeetingRoom_MeetingRoom key.
		///		fkSpandPmeetingRoomMeetingRoom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_meetingRoomId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SpandPmeetingRoom objects.</returns>
		public TList<SpandPmeetingRoom> GetByMeetingRoomId(System.Int64 _meetingRoomId, int start, int pageLength,out int count)
		{
			return GetByMeetingRoomId(null, _meetingRoomId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SPandPmeetingRoom_MeetingRoom key.
		///		FK_SPandPmeetingRoom_MeetingRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_meetingRoomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.SpandPmeetingRoom objects.</returns>
		public abstract TList<SpandPmeetingRoom> GetByMeetingRoomId(TransactionManager transactionManager, System.Int64 _meetingRoomId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.SpandPmeetingRoom Get(TransactionManager transactionManager, LMMR.Entities.SpandPmeetingRoomKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SPandPmeetingRoom index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SpandPmeetingRoom"/> class.</returns>
		public LMMR.Entities.SpandPmeetingRoom GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SPandPmeetingRoom index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SpandPmeetingRoom"/> class.</returns>
		public LMMR.Entities.SpandPmeetingRoom GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SPandPmeetingRoom index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SpandPmeetingRoom"/> class.</returns>
		public LMMR.Entities.SpandPmeetingRoom GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SPandPmeetingRoom index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SpandPmeetingRoom"/> class.</returns>
		public LMMR.Entities.SpandPmeetingRoom GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SPandPmeetingRoom index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SpandPmeetingRoom"/> class.</returns>
		public LMMR.Entities.SpandPmeetingRoom GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SPandPmeetingRoom index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SpandPmeetingRoom"/> class.</returns>
		public abstract LMMR.Entities.SpandPmeetingRoom GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SpandPmeetingRoom&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SpandPmeetingRoom&gt;"/></returns>
		public static TList<SpandPmeetingRoom> Fill(IDataReader reader, TList<SpandPmeetingRoom> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.SpandPmeetingRoom c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SpandPmeetingRoom")
					.Append("|").Append((System.Int64)reader[((int)SpandPmeetingRoomColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SpandPmeetingRoom>(
					key.ToString(), // EntityTrackingKey
					"SpandPmeetingRoom",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.SpandPmeetingRoom();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)SpandPmeetingRoomColumn.Id - 1)];
					c.SpandPid = (System.Int64)reader[((int)SpandPmeetingRoomColumn.SpandPid - 1)];
					c.MeetingRoomId = (System.Int64)reader[((int)SpandPmeetingRoomColumn.MeetingRoomId - 1)];
					c.AvailabilityHalfDay = (reader.IsDBNull(((int)SpandPmeetingRoomColumn.AvailabilityHalfDay - 1)))?null:(System.Int32?)reader[((int)SpandPmeetingRoomColumn.AvailabilityHalfDay - 1)];
					c.AvailabilityFullDay = (reader.IsDBNull(((int)SpandPmeetingRoomColumn.AvailabilityFullDay - 1)))?null:(System.Int32?)reader[((int)SpandPmeetingRoomColumn.AvailabilityFullDay - 1)];
					c.TotalAvailability = (reader.IsDBNull(((int)SpandPmeetingRoomColumn.TotalAvailability - 1)))?null:(System.Int32?)reader[((int)SpandPmeetingRoomColumn.TotalAvailability - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.SpandPmeetingRoom"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SpandPmeetingRoom"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.SpandPmeetingRoom entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)SpandPmeetingRoomColumn.Id - 1)];
			entity.SpandPid = (System.Int64)reader[((int)SpandPmeetingRoomColumn.SpandPid - 1)];
			entity.MeetingRoomId = (System.Int64)reader[((int)SpandPmeetingRoomColumn.MeetingRoomId - 1)];
			entity.AvailabilityHalfDay = (reader.IsDBNull(((int)SpandPmeetingRoomColumn.AvailabilityHalfDay - 1)))?null:(System.Int32?)reader[((int)SpandPmeetingRoomColumn.AvailabilityHalfDay - 1)];
			entity.AvailabilityFullDay = (reader.IsDBNull(((int)SpandPmeetingRoomColumn.AvailabilityFullDay - 1)))?null:(System.Int32?)reader[((int)SpandPmeetingRoomColumn.AvailabilityFullDay - 1)];
			entity.TotalAvailability = (reader.IsDBNull(((int)SpandPmeetingRoomColumn.TotalAvailability - 1)))?null:(System.Int32?)reader[((int)SpandPmeetingRoomColumn.TotalAvailability - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.SpandPmeetingRoom"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SpandPmeetingRoom"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.SpandPmeetingRoom entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.SpandPid = (System.Int64)dataRow["SPandPId"];
			entity.MeetingRoomId = (System.Int64)dataRow["MeetingRoomId"];
			entity.AvailabilityHalfDay = Convert.IsDBNull(dataRow["AvailabilityHalfDay"]) ? null : (System.Int32?)dataRow["AvailabilityHalfDay"];
			entity.AvailabilityFullDay = Convert.IsDBNull(dataRow["AvailabilityFullDay"]) ? null : (System.Int32?)dataRow["AvailabilityFullDay"];
			entity.TotalAvailability = Convert.IsDBNull(dataRow["TotalAvailability"]) ? null : (System.Int32?)dataRow["TotalAvailability"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SpandPmeetingRoom"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.SpandPmeetingRoom Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.SpandPmeetingRoom entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region MeetingRoomIdSource	
			if (CanDeepLoad(entity, "MeetingRoom|MeetingRoomIdSource", deepLoadType, innerList) 
				&& entity.MeetingRoomIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.MeetingRoomId;
				MeetingRoom tmpEntity = EntityManager.LocateEntity<MeetingRoom>(EntityLocator.ConstructKeyFromPkItems(typeof(MeetingRoom), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.MeetingRoomIdSource = tmpEntity;
				else
					entity.MeetingRoomIdSource = DataRepository.MeetingRoomProvider.GetById(transactionManager, entity.MeetingRoomId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MeetingRoomIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.MeetingRoomIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.MeetingRoomProvider.DeepLoad(transactionManager, entity.MeetingRoomIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion MeetingRoomIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.SpandPmeetingRoom object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.SpandPmeetingRoom instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.SpandPmeetingRoom Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.SpandPmeetingRoom entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region MeetingRoomIdSource
			if (CanDeepSave(entity, "MeetingRoom|MeetingRoomIdSource", deepSaveType, innerList) 
				&& entity.MeetingRoomIdSource != null)
			{
				DataRepository.MeetingRoomProvider.Save(transactionManager, entity.MeetingRoomIdSource);
				entity.MeetingRoomId = entity.MeetingRoomIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SpandPmeetingRoomChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.SpandPmeetingRoom</c>
	///</summary>
	public enum SpandPmeetingRoomChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>MeetingRoom</c> at MeetingRoomIdSource
		///</summary>
		[ChildEntityType(typeof(MeetingRoom))]
		MeetingRoom,
		}
	
	#endregion SpandPmeetingRoomChildEntityTypes
	
	#region SpandPmeetingRoomFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SpandPmeetingRoomColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SpandPmeetingRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SpandPmeetingRoomFilterBuilder : SqlFilterBuilder<SpandPmeetingRoomColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SpandPmeetingRoomFilterBuilder class.
		/// </summary>
		public SpandPmeetingRoomFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SpandPmeetingRoomFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SpandPmeetingRoomFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SpandPmeetingRoomFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SpandPmeetingRoomFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SpandPmeetingRoomFilterBuilder
	
	#region SpandPmeetingRoomParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SpandPmeetingRoomColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SpandPmeetingRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SpandPmeetingRoomParameterBuilder : ParameterizedSqlFilterBuilder<SpandPmeetingRoomColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SpandPmeetingRoomParameterBuilder class.
		/// </summary>
		public SpandPmeetingRoomParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SpandPmeetingRoomParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SpandPmeetingRoomParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SpandPmeetingRoomParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SpandPmeetingRoomParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SpandPmeetingRoomParameterBuilder
	
	#region SpandPmeetingRoomSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SpandPmeetingRoomColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SpandPmeetingRoom"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SpandPmeetingRoomSortBuilder : SqlSortBuilder<SpandPmeetingRoomColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SpandPmeetingRoomSqlSortBuilder class.
		/// </summary>
		public SpandPmeetingRoomSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SpandPmeetingRoomSortBuilder
	
} // end namespace
