﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="PackageByHotelProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class PackageByHotelProviderBaseCore : EntityProviderBase<LMMR.Entities.PackageByHotel, LMMR.Entities.PackageByHotelKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.PackageByHotelKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_Hotel key.
		///		FK_PackageByHotel_Hotel Description: 
		/// </summary>
		/// <param name="_hotelId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotel objects.</returns>
		public TList<PackageByHotel> GetByHotelId(System.Int64 _hotelId)
		{
			int count = -1;
			return GetByHotelId(_hotelId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_Hotel key.
		///		FK_PackageByHotel_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotel objects.</returns>
		/// <remarks></remarks>
		public TList<PackageByHotel> GetByHotelId(TransactionManager transactionManager, System.Int64 _hotelId)
		{
			int count = -1;
			return GetByHotelId(transactionManager, _hotelId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_Hotel key.
		///		FK_PackageByHotel_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotel objects.</returns>
		public TList<PackageByHotel> GetByHotelId(TransactionManager transactionManager, System.Int64 _hotelId, int start, int pageLength)
		{
			int count = -1;
			return GetByHotelId(transactionManager, _hotelId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_Hotel key.
		///		fkPackageByHotelHotel Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_hotelId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotel objects.</returns>
		public TList<PackageByHotel> GetByHotelId(System.Int64 _hotelId, int start, int pageLength)
		{
			int count =  -1;
			return GetByHotelId(null, _hotelId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_Hotel key.
		///		fkPackageByHotelHotel Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_hotelId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotel objects.</returns>
		public TList<PackageByHotel> GetByHotelId(System.Int64 _hotelId, int start, int pageLength,out int count)
		{
			return GetByHotelId(null, _hotelId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_Hotel key.
		///		FK_PackageByHotel_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotel objects.</returns>
		public abstract TList<PackageByHotel> GetByHotelId(TransactionManager transactionManager, System.Int64 _hotelId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_PackageItems key.
		///		FK_PackageByHotel_PackageItems Description: 
		/// </summary>
		/// <param name="_itemId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotel objects.</returns>
		public TList<PackageByHotel> GetByItemId(System.Int64 _itemId)
		{
			int count = -1;
			return GetByItemId(_itemId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_PackageItems key.
		///		FK_PackageByHotel_PackageItems Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_itemId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotel objects.</returns>
		/// <remarks></remarks>
		public TList<PackageByHotel> GetByItemId(TransactionManager transactionManager, System.Int64 _itemId)
		{
			int count = -1;
			return GetByItemId(transactionManager, _itemId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_PackageItems key.
		///		FK_PackageByHotel_PackageItems Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_itemId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotel objects.</returns>
		public TList<PackageByHotel> GetByItemId(TransactionManager transactionManager, System.Int64 _itemId, int start, int pageLength)
		{
			int count = -1;
			return GetByItemId(transactionManager, _itemId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_PackageItems key.
		///		fkPackageByHotelPackageItems Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_itemId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotel objects.</returns>
		public TList<PackageByHotel> GetByItemId(System.Int64 _itemId, int start, int pageLength)
		{
			int count =  -1;
			return GetByItemId(null, _itemId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_PackageItems key.
		///		fkPackageByHotelPackageItems Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_itemId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotel objects.</returns>
		public TList<PackageByHotel> GetByItemId(System.Int64 _itemId, int start, int pageLength,out int count)
		{
			return GetByItemId(null, _itemId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_PackageItems key.
		///		FK_PackageByHotel_PackageItems Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_itemId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotel objects.</returns>
		public abstract TList<PackageByHotel> GetByItemId(TransactionManager transactionManager, System.Int64 _itemId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_PackageMaster key.
		///		FK_PackageByHotel_PackageMaster Description: 
		/// </summary>
		/// <param name="_packageId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotel objects.</returns>
		public TList<PackageByHotel> GetByPackageId(System.Int64? _packageId)
		{
			int count = -1;
			return GetByPackageId(_packageId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_PackageMaster key.
		///		FK_PackageByHotel_PackageMaster Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_packageId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotel objects.</returns>
		/// <remarks></remarks>
		public TList<PackageByHotel> GetByPackageId(TransactionManager transactionManager, System.Int64? _packageId)
		{
			int count = -1;
			return GetByPackageId(transactionManager, _packageId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_PackageMaster key.
		///		FK_PackageByHotel_PackageMaster Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_packageId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotel objects.</returns>
		public TList<PackageByHotel> GetByPackageId(TransactionManager transactionManager, System.Int64? _packageId, int start, int pageLength)
		{
			int count = -1;
			return GetByPackageId(transactionManager, _packageId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_PackageMaster key.
		///		fkPackageByHotelPackageMaster Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_packageId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotel objects.</returns>
		public TList<PackageByHotel> GetByPackageId(System.Int64? _packageId, int start, int pageLength)
		{
			int count =  -1;
			return GetByPackageId(null, _packageId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_PackageMaster key.
		///		fkPackageByHotelPackageMaster Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_packageId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotel objects.</returns>
		public TList<PackageByHotel> GetByPackageId(System.Int64? _packageId, int start, int pageLength,out int count)
		{
			return GetByPackageId(null, _packageId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_PackageMaster key.
		///		FK_PackageByHotel_PackageMaster Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_packageId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotel objects.</returns>
		public abstract TList<PackageByHotel> GetByPackageId(TransactionManager transactionManager, System.Int64? _packageId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_Users key.
		///		FK_PackageByHotel_Users Description: 
		/// </summary>
		/// <param name="_updatedBy"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotel objects.</returns>
		public TList<PackageByHotel> GetByUpdatedBy(System.Int64? _updatedBy)
		{
			int count = -1;
			return GetByUpdatedBy(_updatedBy, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_Users key.
		///		FK_PackageByHotel_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_updatedBy"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotel objects.</returns>
		/// <remarks></remarks>
		public TList<PackageByHotel> GetByUpdatedBy(TransactionManager transactionManager, System.Int64? _updatedBy)
		{
			int count = -1;
			return GetByUpdatedBy(transactionManager, _updatedBy, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_Users key.
		///		FK_PackageByHotel_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_updatedBy"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotel objects.</returns>
		public TList<PackageByHotel> GetByUpdatedBy(TransactionManager transactionManager, System.Int64? _updatedBy, int start, int pageLength)
		{
			int count = -1;
			return GetByUpdatedBy(transactionManager, _updatedBy, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_Users key.
		///		fkPackageByHotelUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_updatedBy"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotel objects.</returns>
		public TList<PackageByHotel> GetByUpdatedBy(System.Int64? _updatedBy, int start, int pageLength)
		{
			int count =  -1;
			return GetByUpdatedBy(null, _updatedBy, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_Users key.
		///		fkPackageByHotelUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_updatedBy"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotel objects.</returns>
		public TList<PackageByHotel> GetByUpdatedBy(System.Int64? _updatedBy, int start, int pageLength,out int count)
		{
			return GetByUpdatedBy(null, _updatedBy, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_Users key.
		///		FK_PackageByHotel_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_updatedBy"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotel objects.</returns>
		public abstract TList<PackageByHotel> GetByUpdatedBy(TransactionManager transactionManager, System.Int64? _updatedBy, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.PackageByHotel Get(TransactionManager transactionManager, LMMR.Entities.PackageByHotelKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_PackageByHotel index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageByHotel"/> class.</returns>
		public LMMR.Entities.PackageByHotel GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_PackageByHotel index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageByHotel"/> class.</returns>
		public LMMR.Entities.PackageByHotel GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_PackageByHotel index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageByHotel"/> class.</returns>
		public LMMR.Entities.PackageByHotel GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_PackageByHotel index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageByHotel"/> class.</returns>
		public LMMR.Entities.PackageByHotel GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_PackageByHotel index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageByHotel"/> class.</returns>
		public LMMR.Entities.PackageByHotel GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_PackageByHotel index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageByHotel"/> class.</returns>
		public abstract LMMR.Entities.PackageByHotel GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;PackageByHotel&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;PackageByHotel&gt;"/></returns>
		public static TList<PackageByHotel> Fill(IDataReader reader, TList<PackageByHotel> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.PackageByHotel c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("PackageByHotel")
					.Append("|").Append((System.Int64)reader[((int)PackageByHotelColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<PackageByHotel>(
					key.ToString(), // EntityTrackingKey
					"PackageByHotel",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.PackageByHotel();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)PackageByHotelColumn.Id - 1)];
					c.PackageId = (reader.IsDBNull(((int)PackageByHotelColumn.PackageId - 1)))?null:(System.Int64?)reader[((int)PackageByHotelColumn.PackageId - 1)];
					c.HotelId = (System.Int64)reader[((int)PackageByHotelColumn.HotelId - 1)];
					c.HalfdayPrice = (reader.IsDBNull(((int)PackageByHotelColumn.HalfdayPrice - 1)))?null:(System.Decimal?)reader[((int)PackageByHotelColumn.HalfdayPrice - 1)];
					c.FulldayPrice = (reader.IsDBNull(((int)PackageByHotelColumn.FulldayPrice - 1)))?null:(System.Decimal?)reader[((int)PackageByHotelColumn.FulldayPrice - 1)];
					c.IsOnline = (System.Boolean)reader[((int)PackageByHotelColumn.IsOnline - 1)];
					c.ItemId = (System.Int64)reader[((int)PackageByHotelColumn.ItemId - 1)];
					c.IsComplementary = (System.Boolean)reader[((int)PackageByHotelColumn.IsComplementary - 1)];
					c.UpdatedBy = (reader.IsDBNull(((int)PackageByHotelColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)PackageByHotelColumn.UpdatedBy - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.PackageByHotel"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.PackageByHotel"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.PackageByHotel entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)PackageByHotelColumn.Id - 1)];
			entity.PackageId = (reader.IsDBNull(((int)PackageByHotelColumn.PackageId - 1)))?null:(System.Int64?)reader[((int)PackageByHotelColumn.PackageId - 1)];
			entity.HotelId = (System.Int64)reader[((int)PackageByHotelColumn.HotelId - 1)];
			entity.HalfdayPrice = (reader.IsDBNull(((int)PackageByHotelColumn.HalfdayPrice - 1)))?null:(System.Decimal?)reader[((int)PackageByHotelColumn.HalfdayPrice - 1)];
			entity.FulldayPrice = (reader.IsDBNull(((int)PackageByHotelColumn.FulldayPrice - 1)))?null:(System.Decimal?)reader[((int)PackageByHotelColumn.FulldayPrice - 1)];
			entity.IsOnline = (System.Boolean)reader[((int)PackageByHotelColumn.IsOnline - 1)];
			entity.ItemId = (System.Int64)reader[((int)PackageByHotelColumn.ItemId - 1)];
			entity.IsComplementary = (System.Boolean)reader[((int)PackageByHotelColumn.IsComplementary - 1)];
			entity.UpdatedBy = (reader.IsDBNull(((int)PackageByHotelColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)PackageByHotelColumn.UpdatedBy - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.PackageByHotel"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.PackageByHotel"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.PackageByHotel entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.PackageId = Convert.IsDBNull(dataRow["PackageID"]) ? null : (System.Int64?)dataRow["PackageID"];
			entity.HotelId = (System.Int64)dataRow["HotelId"];
			entity.HalfdayPrice = Convert.IsDBNull(dataRow["HalfdayPrice"]) ? null : (System.Decimal?)dataRow["HalfdayPrice"];
			entity.FulldayPrice = Convert.IsDBNull(dataRow["FulldayPrice"]) ? null : (System.Decimal?)dataRow["FulldayPrice"];
			entity.IsOnline = (System.Boolean)dataRow["IsOnline"];
			entity.ItemId = (System.Int64)dataRow["ItemId"];
			entity.IsComplementary = (System.Boolean)dataRow["IsComplementary"];
			entity.UpdatedBy = Convert.IsDBNull(dataRow["UpdatedBy"]) ? null : (System.Int64?)dataRow["UpdatedBy"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.PackageByHotel"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.PackageByHotel Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.PackageByHotel entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region HotelIdSource	
			if (CanDeepLoad(entity, "Hotel|HotelIdSource", deepLoadType, innerList) 
				&& entity.HotelIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.HotelId;
				Hotel tmpEntity = EntityManager.LocateEntity<Hotel>(EntityLocator.ConstructKeyFromPkItems(typeof(Hotel), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.HotelIdSource = tmpEntity;
				else
					entity.HotelIdSource = DataRepository.HotelProvider.GetById(transactionManager, entity.HotelId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HotelIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.HotelIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.HotelProvider.DeepLoad(transactionManager, entity.HotelIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion HotelIdSource

			#region ItemIdSource	
			if (CanDeepLoad(entity, "PackageItems|ItemIdSource", deepLoadType, innerList) 
				&& entity.ItemIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ItemId;
				PackageItems tmpEntity = EntityManager.LocateEntity<PackageItems>(EntityLocator.ConstructKeyFromPkItems(typeof(PackageItems), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ItemIdSource = tmpEntity;
				else
					entity.ItemIdSource = DataRepository.PackageItemsProvider.GetById(transactionManager, entity.ItemId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ItemIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ItemIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PackageItemsProvider.DeepLoad(transactionManager, entity.ItemIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ItemIdSource

			#region PackageIdSource	
			if (CanDeepLoad(entity, "PackageMaster|PackageIdSource", deepLoadType, innerList) 
				&& entity.PackageIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.PackageId ?? (long)0);
				PackageMaster tmpEntity = EntityManager.LocateEntity<PackageMaster>(EntityLocator.ConstructKeyFromPkItems(typeof(PackageMaster), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PackageIdSource = tmpEntity;
				else
					entity.PackageIdSource = DataRepository.PackageMasterProvider.GetById(transactionManager, (entity.PackageId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PackageIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PackageIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PackageMasterProvider.DeepLoad(transactionManager, entity.PackageIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PackageIdSource

			#region UpdatedBySource	
			if (CanDeepLoad(entity, "Users|UpdatedBySource", deepLoadType, innerList) 
				&& entity.UpdatedBySource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.UpdatedBy ?? (long)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.UpdatedBySource = tmpEntity;
				else
					entity.UpdatedBySource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.UpdatedBy ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UpdatedBySource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.UpdatedBySource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.UpdatedBySource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion UpdatedBySource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region PackageByHotelTrailCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PackageByHotelTrail>|PackageByHotelTrailCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PackageByHotelTrailCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PackageByHotelTrailCollection = DataRepository.PackageByHotelTrailProvider.GetByPackagebyHotelId(transactionManager, entity.Id);

				if (deep && entity.PackageByHotelTrailCollection.Count > 0)
				{
					deepHandles.Add("PackageByHotelTrailCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PackageByHotelTrail>) DataRepository.PackageByHotelTrailProvider.DeepLoad,
						new object[] { transactionManager, entity.PackageByHotelTrailCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.PackageByHotel object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.PackageByHotel instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.PackageByHotel Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.PackageByHotel entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region HotelIdSource
			if (CanDeepSave(entity, "Hotel|HotelIdSource", deepSaveType, innerList) 
				&& entity.HotelIdSource != null)
			{
				DataRepository.HotelProvider.Save(transactionManager, entity.HotelIdSource);
				entity.HotelId = entity.HotelIdSource.Id;
			}
			#endregion 
			
			#region ItemIdSource
			if (CanDeepSave(entity, "PackageItems|ItemIdSource", deepSaveType, innerList) 
				&& entity.ItemIdSource != null)
			{
				DataRepository.PackageItemsProvider.Save(transactionManager, entity.ItemIdSource);
				entity.ItemId = entity.ItemIdSource.Id;
			}
			#endregion 
			
			#region PackageIdSource
			if (CanDeepSave(entity, "PackageMaster|PackageIdSource", deepSaveType, innerList) 
				&& entity.PackageIdSource != null)
			{
				DataRepository.PackageMasterProvider.Save(transactionManager, entity.PackageIdSource);
				entity.PackageId = entity.PackageIdSource.Id;
			}
			#endregion 
			
			#region UpdatedBySource
			if (CanDeepSave(entity, "Users|UpdatedBySource", deepSaveType, innerList) 
				&& entity.UpdatedBySource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.UpdatedBySource);
				entity.UpdatedBy = entity.UpdatedBySource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<PackageByHotelTrail>
				if (CanDeepSave(entity.PackageByHotelTrailCollection, "List<PackageByHotelTrail>|PackageByHotelTrailCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PackageByHotelTrail child in entity.PackageByHotelTrailCollection)
					{
						if(child.PackagebyHotelIdSource != null)
						{
							child.PackagebyHotelId = child.PackagebyHotelIdSource.Id;
						}
						else
						{
							child.PackagebyHotelId = entity.Id;
						}

					}

					if (entity.PackageByHotelTrailCollection.Count > 0 || entity.PackageByHotelTrailCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PackageByHotelTrailProvider.Save(transactionManager, entity.PackageByHotelTrailCollection);
						
						deepHandles.Add("PackageByHotelTrailCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PackageByHotelTrail >) DataRepository.PackageByHotelTrailProvider.DeepSave,
							new object[] { transactionManager, entity.PackageByHotelTrailCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region PackageByHotelChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.PackageByHotel</c>
	///</summary>
	public enum PackageByHotelChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Hotel</c> at HotelIdSource
		///</summary>
		[ChildEntityType(typeof(Hotel))]
		Hotel,
			
		///<summary>
		/// Composite Property for <c>PackageItems</c> at ItemIdSource
		///</summary>
		[ChildEntityType(typeof(PackageItems))]
		PackageItems,
			
		///<summary>
		/// Composite Property for <c>PackageMaster</c> at PackageIdSource
		///</summary>
		[ChildEntityType(typeof(PackageMaster))]
		PackageMaster,
			
		///<summary>
		/// Composite Property for <c>Users</c> at UpdatedBySource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
	
		///<summary>
		/// Collection of <c>PackageByHotel</c> as OneToMany for PackageByHotelTrailCollection
		///</summary>
		[ChildEntityType(typeof(TList<PackageByHotelTrail>))]
		PackageByHotelTrailCollection,
	}
	
	#endregion PackageByHotelChildEntityTypes
	
	#region PackageByHotelFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;PackageByHotelColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PackageByHotel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PackageByHotelFilterBuilder : SqlFilterBuilder<PackageByHotelColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageByHotelFilterBuilder class.
		/// </summary>
		public PackageByHotelFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PackageByHotelFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PackageByHotelFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PackageByHotelFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PackageByHotelFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PackageByHotelFilterBuilder
	
	#region PackageByHotelParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;PackageByHotelColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PackageByHotel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PackageByHotelParameterBuilder : ParameterizedSqlFilterBuilder<PackageByHotelColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageByHotelParameterBuilder class.
		/// </summary>
		public PackageByHotelParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PackageByHotelParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PackageByHotelParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PackageByHotelParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PackageByHotelParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PackageByHotelParameterBuilder
	
	#region PackageByHotelSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;PackageByHotelColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PackageByHotel"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class PackageByHotelSortBuilder : SqlSortBuilder<PackageByHotelColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageByHotelSqlSortBuilder class.
		/// </summary>
		public PackageByHotelSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion PackageByHotelSortBuilder
	
} // end namespace
