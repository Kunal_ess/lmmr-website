﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="PackageDescriptionProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class PackageDescriptionProviderBaseCore : EntityProviderBase<LMMR.Entities.PackageDescription, LMMR.Entities.PackageDescriptionKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.PackageDescriptionKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageDescription_PackageItems key.
		///		FK_PackageDescription_PackageItems Description: 
		/// </summary>
		/// <param name="_itemId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageDescription objects.</returns>
		public TList<PackageDescription> GetByItemId(System.Int64 _itemId)
		{
			int count = -1;
			return GetByItemId(_itemId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageDescription_PackageItems key.
		///		FK_PackageDescription_PackageItems Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_itemId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageDescription objects.</returns>
		/// <remarks></remarks>
		public TList<PackageDescription> GetByItemId(TransactionManager transactionManager, System.Int64 _itemId)
		{
			int count = -1;
			return GetByItemId(transactionManager, _itemId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageDescription_PackageItems key.
		///		FK_PackageDescription_PackageItems Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_itemId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageDescription objects.</returns>
		public TList<PackageDescription> GetByItemId(TransactionManager transactionManager, System.Int64 _itemId, int start, int pageLength)
		{
			int count = -1;
			return GetByItemId(transactionManager, _itemId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageDescription_PackageItems key.
		///		fkPackageDescriptionPackageItems Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_itemId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageDescription objects.</returns>
		public TList<PackageDescription> GetByItemId(System.Int64 _itemId, int start, int pageLength)
		{
			int count =  -1;
			return GetByItemId(null, _itemId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageDescription_PackageItems key.
		///		fkPackageDescriptionPackageItems Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_itemId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageDescription objects.</returns>
		public TList<PackageDescription> GetByItemId(System.Int64 _itemId, int start, int pageLength,out int count)
		{
			return GetByItemId(null, _itemId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageDescription_PackageItems key.
		///		FK_PackageDescription_PackageItems Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_itemId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageDescription objects.</returns>
		public abstract TList<PackageDescription> GetByItemId(TransactionManager transactionManager, System.Int64 _itemId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.PackageDescription Get(TransactionManager transactionManager, LMMR.Entities.PackageDescriptionKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SectionDescription index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageDescription"/> class.</returns>
		public LMMR.Entities.PackageDescription GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SectionDescription index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageDescription"/> class.</returns>
		public LMMR.Entities.PackageDescription GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SectionDescription index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageDescription"/> class.</returns>
		public LMMR.Entities.PackageDescription GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SectionDescription index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageDescription"/> class.</returns>
		public LMMR.Entities.PackageDescription GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SectionDescription index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageDescription"/> class.</returns>
		public LMMR.Entities.PackageDescription GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SectionDescription index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageDescription"/> class.</returns>
		public abstract LMMR.Entities.PackageDescription GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;PackageDescription&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;PackageDescription&gt;"/></returns>
		public static TList<PackageDescription> Fill(IDataReader reader, TList<PackageDescription> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.PackageDescription c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("PackageDescription")
					.Append("|").Append((System.Int64)reader[((int)PackageDescriptionColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<PackageDescription>(
					key.ToString(), // EntityTrackingKey
					"PackageDescription",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.PackageDescription();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)PackageDescriptionColumn.Id - 1)];
					c.ItemId = (System.Int64)reader[((int)PackageDescriptionColumn.ItemId - 1)];
					c.ItemName = (reader.IsDBNull(((int)PackageDescriptionColumn.ItemName - 1)))?null:(System.String)reader[((int)PackageDescriptionColumn.ItemName - 1)];
					c.ItemDescription = (reader.IsDBNull(((int)PackageDescriptionColumn.ItemDescription - 1)))?null:(System.String)reader[((int)PackageDescriptionColumn.ItemDescription - 1)];
					c.LanguageId = (System.Int64)reader[((int)PackageDescriptionColumn.LanguageId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.PackageDescription"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.PackageDescription"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.PackageDescription entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)PackageDescriptionColumn.Id - 1)];
			entity.ItemId = (System.Int64)reader[((int)PackageDescriptionColumn.ItemId - 1)];
			entity.ItemName = (reader.IsDBNull(((int)PackageDescriptionColumn.ItemName - 1)))?null:(System.String)reader[((int)PackageDescriptionColumn.ItemName - 1)];
			entity.ItemDescription = (reader.IsDBNull(((int)PackageDescriptionColumn.ItemDescription - 1)))?null:(System.String)reader[((int)PackageDescriptionColumn.ItemDescription - 1)];
			entity.LanguageId = (System.Int64)reader[((int)PackageDescriptionColumn.LanguageId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.PackageDescription"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.PackageDescription"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.PackageDescription entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.ItemId = (System.Int64)dataRow["ItemId"];
			entity.ItemName = Convert.IsDBNull(dataRow["ItemName"]) ? null : (System.String)dataRow["ItemName"];
			entity.ItemDescription = Convert.IsDBNull(dataRow["ItemDescription"]) ? null : (System.String)dataRow["ItemDescription"];
			entity.LanguageId = (System.Int64)dataRow["LanguageId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.PackageDescription"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.PackageDescription Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.PackageDescription entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ItemIdSource	
			if (CanDeepLoad(entity, "PackageItems|ItemIdSource", deepLoadType, innerList) 
				&& entity.ItemIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ItemId;
				PackageItems tmpEntity = EntityManager.LocateEntity<PackageItems>(EntityLocator.ConstructKeyFromPkItems(typeof(PackageItems), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ItemIdSource = tmpEntity;
				else
					entity.ItemIdSource = DataRepository.PackageItemsProvider.GetById(transactionManager, entity.ItemId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ItemIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ItemIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PackageItemsProvider.DeepLoad(transactionManager, entity.ItemIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ItemIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.PackageDescription object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.PackageDescription instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.PackageDescription Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.PackageDescription entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ItemIdSource
			if (CanDeepSave(entity, "PackageItems|ItemIdSource", deepSaveType, innerList) 
				&& entity.ItemIdSource != null)
			{
				DataRepository.PackageItemsProvider.Save(transactionManager, entity.ItemIdSource);
				entity.ItemId = entity.ItemIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region PackageDescriptionChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.PackageDescription</c>
	///</summary>
	public enum PackageDescriptionChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>PackageItems</c> at ItemIdSource
		///</summary>
		[ChildEntityType(typeof(PackageItems))]
		PackageItems,
		}
	
	#endregion PackageDescriptionChildEntityTypes
	
	#region PackageDescriptionFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;PackageDescriptionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PackageDescription"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PackageDescriptionFilterBuilder : SqlFilterBuilder<PackageDescriptionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageDescriptionFilterBuilder class.
		/// </summary>
		public PackageDescriptionFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PackageDescriptionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PackageDescriptionFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PackageDescriptionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PackageDescriptionFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PackageDescriptionFilterBuilder
	
	#region PackageDescriptionParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;PackageDescriptionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PackageDescription"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PackageDescriptionParameterBuilder : ParameterizedSqlFilterBuilder<PackageDescriptionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageDescriptionParameterBuilder class.
		/// </summary>
		public PackageDescriptionParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PackageDescriptionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PackageDescriptionParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PackageDescriptionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PackageDescriptionParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PackageDescriptionParameterBuilder
	
	#region PackageDescriptionSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;PackageDescriptionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PackageDescription"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class PackageDescriptionSortBuilder : SqlSortBuilder<PackageDescriptionColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageDescriptionSqlSortBuilder class.
		/// </summary>
		public PackageDescriptionSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion PackageDescriptionSortBuilder
	
} // end namespace
