﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="BedRoomPictureImageTrailProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class BedRoomPictureImageTrailProviderBaseCore : EntityProviderBase<LMMR.Entities.BedRoomPictureImageTrail, LMMR.Entities.BedRoomPictureImageTrailKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.BedRoomPictureImageTrailKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomPictureImage_Trail_BedRoomPictureImage key.
		///		FK_BedRoomPictureImage_Trail_BedRoomPictureImage Description: 
		/// </summary>
		/// <param name="_bedRoomPicimgId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomPictureImageTrail objects.</returns>
		public TList<BedRoomPictureImageTrail> GetByBedRoomPicimgId(System.Int64? _bedRoomPicimgId)
		{
			int count = -1;
			return GetByBedRoomPicimgId(_bedRoomPicimgId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomPictureImage_Trail_BedRoomPictureImage key.
		///		FK_BedRoomPictureImage_Trail_BedRoomPictureImage Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bedRoomPicimgId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomPictureImageTrail objects.</returns>
		/// <remarks></remarks>
		public TList<BedRoomPictureImageTrail> GetByBedRoomPicimgId(TransactionManager transactionManager, System.Int64? _bedRoomPicimgId)
		{
			int count = -1;
			return GetByBedRoomPicimgId(transactionManager, _bedRoomPicimgId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomPictureImage_Trail_BedRoomPictureImage key.
		///		FK_BedRoomPictureImage_Trail_BedRoomPictureImage Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bedRoomPicimgId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomPictureImageTrail objects.</returns>
		public TList<BedRoomPictureImageTrail> GetByBedRoomPicimgId(TransactionManager transactionManager, System.Int64? _bedRoomPicimgId, int start, int pageLength)
		{
			int count = -1;
			return GetByBedRoomPicimgId(transactionManager, _bedRoomPicimgId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomPictureImage_Trail_BedRoomPictureImage key.
		///		fkBedRoomPictureImageTrailBedRoomPictureImage Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bedRoomPicimgId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomPictureImageTrail objects.</returns>
		public TList<BedRoomPictureImageTrail> GetByBedRoomPicimgId(System.Int64? _bedRoomPicimgId, int start, int pageLength)
		{
			int count =  -1;
			return GetByBedRoomPicimgId(null, _bedRoomPicimgId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomPictureImage_Trail_BedRoomPictureImage key.
		///		fkBedRoomPictureImageTrailBedRoomPictureImage Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bedRoomPicimgId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomPictureImageTrail objects.</returns>
		public TList<BedRoomPictureImageTrail> GetByBedRoomPicimgId(System.Int64? _bedRoomPicimgId, int start, int pageLength,out int count)
		{
			return GetByBedRoomPicimgId(null, _bedRoomPicimgId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomPictureImage_Trail_BedRoomPictureImage key.
		///		FK_BedRoomPictureImage_Trail_BedRoomPictureImage Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bedRoomPicimgId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomPictureImageTrail objects.</returns>
		public abstract TList<BedRoomPictureImageTrail> GetByBedRoomPicimgId(TransactionManager transactionManager, System.Int64? _bedRoomPicimgId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.BedRoomPictureImageTrail Get(TransactionManager transactionManager, LMMR.Entities.BedRoomPictureImageTrailKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_BedRoomPictureImage_Trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomPictureImageTrail"/> class.</returns>
		public LMMR.Entities.BedRoomPictureImageTrail GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoomPictureImage_Trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomPictureImageTrail"/> class.</returns>
		public LMMR.Entities.BedRoomPictureImageTrail GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoomPictureImage_Trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomPictureImageTrail"/> class.</returns>
		public LMMR.Entities.BedRoomPictureImageTrail GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoomPictureImage_Trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomPictureImageTrail"/> class.</returns>
		public LMMR.Entities.BedRoomPictureImageTrail GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoomPictureImage_Trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomPictureImageTrail"/> class.</returns>
		public LMMR.Entities.BedRoomPictureImageTrail GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoomPictureImage_Trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomPictureImageTrail"/> class.</returns>
		public abstract LMMR.Entities.BedRoomPictureImageTrail GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;BedRoomPictureImageTrail&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;BedRoomPictureImageTrail&gt;"/></returns>
		public static TList<BedRoomPictureImageTrail> Fill(IDataReader reader, TList<BedRoomPictureImageTrail> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.BedRoomPictureImageTrail c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("BedRoomPictureImageTrail")
					.Append("|").Append((System.Int64)reader[((int)BedRoomPictureImageTrailColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<BedRoomPictureImageTrail>(
					key.ToString(), // EntityTrackingKey
					"BedRoomPictureImageTrail",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.BedRoomPictureImageTrail();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)BedRoomPictureImageTrailColumn.Id - 1)];
					c.BedRoomPicimgId = (reader.IsDBNull(((int)BedRoomPictureImageTrailColumn.BedRoomPicimgId - 1)))?null:(System.Int64?)reader[((int)BedRoomPictureImageTrailColumn.BedRoomPicimgId - 1)];
					c.BedRoomId = (System.Int64)reader[((int)BedRoomPictureImageTrailColumn.BedRoomId - 1)];
					c.FileType = (reader.IsDBNull(((int)BedRoomPictureImageTrailColumn.FileType - 1)))?null:(System.Int32?)reader[((int)BedRoomPictureImageTrailColumn.FileType - 1)];
					c.ImageName = (reader.IsDBNull(((int)BedRoomPictureImageTrailColumn.ImageName - 1)))?null:(System.String)reader[((int)BedRoomPictureImageTrailColumn.ImageName - 1)];
					c.VideoName = (reader.IsDBNull(((int)BedRoomPictureImageTrailColumn.VideoName - 1)))?null:(System.String)reader[((int)BedRoomPictureImageTrailColumn.VideoName - 1)];
					c.FileName = (reader.IsDBNull(((int)BedRoomPictureImageTrailColumn.FileName - 1)))?null:(System.String)reader[((int)BedRoomPictureImageTrailColumn.FileName - 1)];
					c.AlterText = (reader.IsDBNull(((int)BedRoomPictureImageTrailColumn.AlterText - 1)))?null:(System.String)reader[((int)BedRoomPictureImageTrailColumn.AlterText - 1)];
					c.IsMain = (System.Boolean)reader[((int)BedRoomPictureImageTrailColumn.IsMain - 1)];
					c.UpdatedBy = (reader.IsDBNull(((int)BedRoomPictureImageTrailColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)BedRoomPictureImageTrailColumn.UpdatedBy - 1)];
					c.UpdateDate = (reader.IsDBNull(((int)BedRoomPictureImageTrailColumn.UpdateDate - 1)))?null:(System.DateTime?)reader[((int)BedRoomPictureImageTrailColumn.UpdateDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.BedRoomPictureImageTrail"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BedRoomPictureImageTrail"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.BedRoomPictureImageTrail entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)BedRoomPictureImageTrailColumn.Id - 1)];
			entity.BedRoomPicimgId = (reader.IsDBNull(((int)BedRoomPictureImageTrailColumn.BedRoomPicimgId - 1)))?null:(System.Int64?)reader[((int)BedRoomPictureImageTrailColumn.BedRoomPicimgId - 1)];
			entity.BedRoomId = (System.Int64)reader[((int)BedRoomPictureImageTrailColumn.BedRoomId - 1)];
			entity.FileType = (reader.IsDBNull(((int)BedRoomPictureImageTrailColumn.FileType - 1)))?null:(System.Int32?)reader[((int)BedRoomPictureImageTrailColumn.FileType - 1)];
			entity.ImageName = (reader.IsDBNull(((int)BedRoomPictureImageTrailColumn.ImageName - 1)))?null:(System.String)reader[((int)BedRoomPictureImageTrailColumn.ImageName - 1)];
			entity.VideoName = (reader.IsDBNull(((int)BedRoomPictureImageTrailColumn.VideoName - 1)))?null:(System.String)reader[((int)BedRoomPictureImageTrailColumn.VideoName - 1)];
			entity.FileName = (reader.IsDBNull(((int)BedRoomPictureImageTrailColumn.FileName - 1)))?null:(System.String)reader[((int)BedRoomPictureImageTrailColumn.FileName - 1)];
			entity.AlterText = (reader.IsDBNull(((int)BedRoomPictureImageTrailColumn.AlterText - 1)))?null:(System.String)reader[((int)BedRoomPictureImageTrailColumn.AlterText - 1)];
			entity.IsMain = (System.Boolean)reader[((int)BedRoomPictureImageTrailColumn.IsMain - 1)];
			entity.UpdatedBy = (reader.IsDBNull(((int)BedRoomPictureImageTrailColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)BedRoomPictureImageTrailColumn.UpdatedBy - 1)];
			entity.UpdateDate = (reader.IsDBNull(((int)BedRoomPictureImageTrailColumn.UpdateDate - 1)))?null:(System.DateTime?)reader[((int)BedRoomPictureImageTrailColumn.UpdateDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.BedRoomPictureImageTrail"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BedRoomPictureImageTrail"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.BedRoomPictureImageTrail entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.BedRoomPicimgId = Convert.IsDBNull(dataRow["BedRoomPicimgId"]) ? null : (System.Int64?)dataRow["BedRoomPicimgId"];
			entity.BedRoomId = (System.Int64)dataRow["BedRoomId"];
			entity.FileType = Convert.IsDBNull(dataRow["FileType"]) ? null : (System.Int32?)dataRow["FileType"];
			entity.ImageName = Convert.IsDBNull(dataRow["ImageName"]) ? null : (System.String)dataRow["ImageName"];
			entity.VideoName = Convert.IsDBNull(dataRow["VideoName"]) ? null : (System.String)dataRow["VideoName"];
			entity.FileName = Convert.IsDBNull(dataRow["FileName"]) ? null : (System.String)dataRow["FileName"];
			entity.AlterText = Convert.IsDBNull(dataRow["AlterText"]) ? null : (System.String)dataRow["AlterText"];
			entity.IsMain = (System.Boolean)dataRow["IsMain"];
			entity.UpdatedBy = Convert.IsDBNull(dataRow["UpdatedBy"]) ? null : (System.Int64?)dataRow["UpdatedBy"];
			entity.UpdateDate = Convert.IsDBNull(dataRow["UpdateDate"]) ? null : (System.DateTime?)dataRow["UpdateDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BedRoomPictureImageTrail"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.BedRoomPictureImageTrail Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.BedRoomPictureImageTrail entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region BedRoomPicimgIdSource	
			if (CanDeepLoad(entity, "BedRoomPictureImage|BedRoomPicimgIdSource", deepLoadType, innerList) 
				&& entity.BedRoomPicimgIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.BedRoomPicimgId ?? (long)0);
				BedRoomPictureImage tmpEntity = EntityManager.LocateEntity<BedRoomPictureImage>(EntityLocator.ConstructKeyFromPkItems(typeof(BedRoomPictureImage), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.BedRoomPicimgIdSource = tmpEntity;
				else
					entity.BedRoomPicimgIdSource = DataRepository.BedRoomPictureImageProvider.GetById(transactionManager, (entity.BedRoomPicimgId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BedRoomPicimgIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.BedRoomPicimgIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.BedRoomPictureImageProvider.DeepLoad(transactionManager, entity.BedRoomPicimgIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion BedRoomPicimgIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.BedRoomPictureImageTrail object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.BedRoomPictureImageTrail instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.BedRoomPictureImageTrail Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.BedRoomPictureImageTrail entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region BedRoomPicimgIdSource
			if (CanDeepSave(entity, "BedRoomPictureImage|BedRoomPicimgIdSource", deepSaveType, innerList) 
				&& entity.BedRoomPicimgIdSource != null)
			{
				DataRepository.BedRoomPictureImageProvider.Save(transactionManager, entity.BedRoomPicimgIdSource);
				entity.BedRoomPicimgId = entity.BedRoomPicimgIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region BedRoomPictureImageTrailChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.BedRoomPictureImageTrail</c>
	///</summary>
	public enum BedRoomPictureImageTrailChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>BedRoomPictureImage</c> at BedRoomPicimgIdSource
		///</summary>
		[ChildEntityType(typeof(BedRoomPictureImage))]
		BedRoomPictureImage,
		}
	
	#endregion BedRoomPictureImageTrailChildEntityTypes
	
	#region BedRoomPictureImageTrailFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;BedRoomPictureImageTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BedRoomPictureImageTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BedRoomPictureImageTrailFilterBuilder : SqlFilterBuilder<BedRoomPictureImageTrailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageTrailFilterBuilder class.
		/// </summary>
		public BedRoomPictureImageTrailFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageTrailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BedRoomPictureImageTrailFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageTrailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BedRoomPictureImageTrailFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BedRoomPictureImageTrailFilterBuilder
	
	#region BedRoomPictureImageTrailParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;BedRoomPictureImageTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BedRoomPictureImageTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BedRoomPictureImageTrailParameterBuilder : ParameterizedSqlFilterBuilder<BedRoomPictureImageTrailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageTrailParameterBuilder class.
		/// </summary>
		public BedRoomPictureImageTrailParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageTrailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BedRoomPictureImageTrailParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageTrailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BedRoomPictureImageTrailParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BedRoomPictureImageTrailParameterBuilder
	
	#region BedRoomPictureImageTrailSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;BedRoomPictureImageTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BedRoomPictureImageTrail"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class BedRoomPictureImageTrailSortBuilder : SqlSortBuilder<BedRoomPictureImageTrailColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomPictureImageTrailSqlSortBuilder class.
		/// </summary>
		public BedRoomPictureImageTrailSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion BedRoomPictureImageTrailSortBuilder
	
} // end namespace
