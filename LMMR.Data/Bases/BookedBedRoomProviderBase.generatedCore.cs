﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="BookedBedRoomProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class BookedBedRoomProviderBaseCore : EntityProviderBase<LMMR.Entities.BookedBedRoom, LMMR.Entities.BookedBedRoomKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.BookedBedRoomKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedBedRoom_BedRoom key.
		///		FK_BookedBedRoom_BedRoom Description: 
		/// </summary>
		/// <param name="_bedRoomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedBedRoom objects.</returns>
		public TList<BookedBedRoom> GetByBedRoomId(System.Int64 _bedRoomId)
		{
			int count = -1;
			return GetByBedRoomId(_bedRoomId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedBedRoom_BedRoom key.
		///		FK_BookedBedRoom_BedRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bedRoomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedBedRoom objects.</returns>
		/// <remarks></remarks>
		public TList<BookedBedRoom> GetByBedRoomId(TransactionManager transactionManager, System.Int64 _bedRoomId)
		{
			int count = -1;
			return GetByBedRoomId(transactionManager, _bedRoomId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedBedRoom_BedRoom key.
		///		FK_BookedBedRoom_BedRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bedRoomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedBedRoom objects.</returns>
		public TList<BookedBedRoom> GetByBedRoomId(TransactionManager transactionManager, System.Int64 _bedRoomId, int start, int pageLength)
		{
			int count = -1;
			return GetByBedRoomId(transactionManager, _bedRoomId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedBedRoom_BedRoom key.
		///		fkBookedBedRoomBedRoom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bedRoomId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedBedRoom objects.</returns>
		public TList<BookedBedRoom> GetByBedRoomId(System.Int64 _bedRoomId, int start, int pageLength)
		{
			int count =  -1;
			return GetByBedRoomId(null, _bedRoomId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedBedRoom_BedRoom key.
		///		fkBookedBedRoomBedRoom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bedRoomId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedBedRoom objects.</returns>
		public TList<BookedBedRoom> GetByBedRoomId(System.Int64 _bedRoomId, int start, int pageLength,out int count)
		{
			return GetByBedRoomId(null, _bedRoomId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedBedRoom_BedRoom key.
		///		FK_BookedBedRoom_BedRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bedRoomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedBedRoom objects.</returns>
		public abstract TList<BookedBedRoom> GetByBedRoomId(TransactionManager transactionManager, System.Int64 _bedRoomId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedBedRoom_Booking key.
		///		FK_BookedBedRoom_Booking Description: 
		/// </summary>
		/// <param name="_bookingId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedBedRoom objects.</returns>
		public TList<BookedBedRoom> GetByBookingId(System.Int64 _bookingId)
		{
			int count = -1;
			return GetByBookingId(_bookingId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedBedRoom_Booking key.
		///		FK_BookedBedRoom_Booking Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bookingId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedBedRoom objects.</returns>
		/// <remarks></remarks>
		public TList<BookedBedRoom> GetByBookingId(TransactionManager transactionManager, System.Int64 _bookingId)
		{
			int count = -1;
			return GetByBookingId(transactionManager, _bookingId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedBedRoom_Booking key.
		///		FK_BookedBedRoom_Booking Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bookingId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedBedRoom objects.</returns>
		public TList<BookedBedRoom> GetByBookingId(TransactionManager transactionManager, System.Int64 _bookingId, int start, int pageLength)
		{
			int count = -1;
			return GetByBookingId(transactionManager, _bookingId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedBedRoom_Booking key.
		///		fkBookedBedRoomBooking Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bookingId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedBedRoom objects.</returns>
		public TList<BookedBedRoom> GetByBookingId(System.Int64 _bookingId, int start, int pageLength)
		{
			int count =  -1;
			return GetByBookingId(null, _bookingId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedBedRoom_Booking key.
		///		fkBookedBedRoomBooking Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bookingId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedBedRoom objects.</returns>
		public TList<BookedBedRoom> GetByBookingId(System.Int64 _bookingId, int start, int pageLength,out int count)
		{
			return GetByBookingId(null, _bookingId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BookedBedRoom_Booking key.
		///		FK_BookedBedRoom_Booking Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bookingId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.BookedBedRoom objects.</returns>
		public abstract TList<BookedBedRoom> GetByBookingId(TransactionManager transactionManager, System.Int64 _bookingId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.BookedBedRoom Get(TransactionManager transactionManager, LMMR.Entities.BookedBedRoomKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_BookedBedRoom index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BookedBedRoom"/> class.</returns>
		public LMMR.Entities.BookedBedRoom GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BookedBedRoom index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BookedBedRoom"/> class.</returns>
		public LMMR.Entities.BookedBedRoom GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BookedBedRoom index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BookedBedRoom"/> class.</returns>
		public LMMR.Entities.BookedBedRoom GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BookedBedRoom index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BookedBedRoom"/> class.</returns>
		public LMMR.Entities.BookedBedRoom GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BookedBedRoom index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BookedBedRoom"/> class.</returns>
		public LMMR.Entities.BookedBedRoom GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BookedBedRoom index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BookedBedRoom"/> class.</returns>
		public abstract LMMR.Entities.BookedBedRoom GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;BookedBedRoom&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;BookedBedRoom&gt;"/></returns>
		public static TList<BookedBedRoom> Fill(IDataReader reader, TList<BookedBedRoom> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.BookedBedRoom c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("BookedBedRoom")
					.Append("|").Append((System.Int64)reader[((int)BookedBedRoomColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<BookedBedRoom>(
					key.ToString(), // EntityTrackingKey
					"BookedBedRoom",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.BookedBedRoom();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)BookedBedRoomColumn.Id - 1)];
					c.BookingId = (System.Int64)reader[((int)BookedBedRoomColumn.BookingId - 1)];
					c.BedRoomId = (System.Int64)reader[((int)BookedBedRoomColumn.BedRoomId - 1)];
					c.PersonName = (reader.IsDBNull(((int)BookedBedRoomColumn.PersonName - 1)))?null:(System.String)reader[((int)BookedBedRoomColumn.PersonName - 1)];
					c.CheckIn = (reader.IsDBNull(((int)BookedBedRoomColumn.CheckIn - 1)))?null:(System.DateTime?)reader[((int)BookedBedRoomColumn.CheckIn - 1)];
					c.CheckOut = (reader.IsDBNull(((int)BookedBedRoomColumn.CheckOut - 1)))?null:(System.DateTime?)reader[((int)BookedBedRoomColumn.CheckOut - 1)];
					c.Note = (reader.IsDBNull(((int)BookedBedRoomColumn.Note - 1)))?null:(System.String)reader[((int)BookedBedRoomColumn.Note - 1)];
					c.BedType = (reader.IsDBNull(((int)BookedBedRoomColumn.BedType - 1)))?null:(System.Int32?)reader[((int)BookedBedRoomColumn.BedType - 1)];
					c.Total = (reader.IsDBNull(((int)BookedBedRoomColumn.Total - 1)))?null:(System.Decimal?)reader[((int)BookedBedRoomColumn.Total - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.BookedBedRoom"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BookedBedRoom"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.BookedBedRoom entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)BookedBedRoomColumn.Id - 1)];
			entity.BookingId = (System.Int64)reader[((int)BookedBedRoomColumn.BookingId - 1)];
			entity.BedRoomId = (System.Int64)reader[((int)BookedBedRoomColumn.BedRoomId - 1)];
			entity.PersonName = (reader.IsDBNull(((int)BookedBedRoomColumn.PersonName - 1)))?null:(System.String)reader[((int)BookedBedRoomColumn.PersonName - 1)];
			entity.CheckIn = (reader.IsDBNull(((int)BookedBedRoomColumn.CheckIn - 1)))?null:(System.DateTime?)reader[((int)BookedBedRoomColumn.CheckIn - 1)];
			entity.CheckOut = (reader.IsDBNull(((int)BookedBedRoomColumn.CheckOut - 1)))?null:(System.DateTime?)reader[((int)BookedBedRoomColumn.CheckOut - 1)];
			entity.Note = (reader.IsDBNull(((int)BookedBedRoomColumn.Note - 1)))?null:(System.String)reader[((int)BookedBedRoomColumn.Note - 1)];
			entity.BedType = (reader.IsDBNull(((int)BookedBedRoomColumn.BedType - 1)))?null:(System.Int32?)reader[((int)BookedBedRoomColumn.BedType - 1)];
			entity.Total = (reader.IsDBNull(((int)BookedBedRoomColumn.Total - 1)))?null:(System.Decimal?)reader[((int)BookedBedRoomColumn.Total - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.BookedBedRoom"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BookedBedRoom"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.BookedBedRoom entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.BookingId = (System.Int64)dataRow["BookingId"];
			entity.BedRoomId = (System.Int64)dataRow["BedRoomId"];
			entity.PersonName = Convert.IsDBNull(dataRow["PersonName"]) ? null : (System.String)dataRow["PersonName"];
			entity.CheckIn = Convert.IsDBNull(dataRow["CheckIn"]) ? null : (System.DateTime?)dataRow["CheckIn"];
			entity.CheckOut = Convert.IsDBNull(dataRow["CheckOut"]) ? null : (System.DateTime?)dataRow["CheckOut"];
			entity.Note = Convert.IsDBNull(dataRow["Note"]) ? null : (System.String)dataRow["Note"];
			entity.BedType = Convert.IsDBNull(dataRow["BedType"]) ? null : (System.Int32?)dataRow["BedType"];
			entity.Total = Convert.IsDBNull(dataRow["Total"]) ? null : (System.Decimal?)dataRow["Total"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BookedBedRoom"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.BookedBedRoom Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.BookedBedRoom entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region BedRoomIdSource	
			if (CanDeepLoad(entity, "BedRoom|BedRoomIdSource", deepLoadType, innerList) 
				&& entity.BedRoomIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.BedRoomId;
				BedRoom tmpEntity = EntityManager.LocateEntity<BedRoom>(EntityLocator.ConstructKeyFromPkItems(typeof(BedRoom), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.BedRoomIdSource = tmpEntity;
				else
					entity.BedRoomIdSource = DataRepository.BedRoomProvider.GetById(transactionManager, entity.BedRoomId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BedRoomIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.BedRoomIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.BedRoomProvider.DeepLoad(transactionManager, entity.BedRoomIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion BedRoomIdSource

			#region BookingIdSource	
			if (CanDeepLoad(entity, "Booking|BookingIdSource", deepLoadType, innerList) 
				&& entity.BookingIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.BookingId;
				Booking tmpEntity = EntityManager.LocateEntity<Booking>(EntityLocator.ConstructKeyFromPkItems(typeof(Booking), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.BookingIdSource = tmpEntity;
				else
					entity.BookingIdSource = DataRepository.BookingProvider.GetById(transactionManager, entity.BookingId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BookingIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.BookingIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.BookingProvider.DeepLoad(transactionManager, entity.BookingIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion BookingIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.BookedBedRoom object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.BookedBedRoom instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.BookedBedRoom Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.BookedBedRoom entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region BedRoomIdSource
			if (CanDeepSave(entity, "BedRoom|BedRoomIdSource", deepSaveType, innerList) 
				&& entity.BedRoomIdSource != null)
			{
				DataRepository.BedRoomProvider.Save(transactionManager, entity.BedRoomIdSource);
				entity.BedRoomId = entity.BedRoomIdSource.Id;
			}
			#endregion 
			
			#region BookingIdSource
			if (CanDeepSave(entity, "Booking|BookingIdSource", deepSaveType, innerList) 
				&& entity.BookingIdSource != null)
			{
				DataRepository.BookingProvider.Save(transactionManager, entity.BookingIdSource);
				entity.BookingId = entity.BookingIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region BookedBedRoomChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.BookedBedRoom</c>
	///</summary>
	public enum BookedBedRoomChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>BedRoom</c> at BedRoomIdSource
		///</summary>
		[ChildEntityType(typeof(BedRoom))]
		BedRoom,
			
		///<summary>
		/// Composite Property for <c>Booking</c> at BookingIdSource
		///</summary>
		[ChildEntityType(typeof(Booking))]
		Booking,
		}
	
	#endregion BookedBedRoomChildEntityTypes
	
	#region BookedBedRoomFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;BookedBedRoomColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BookedBedRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BookedBedRoomFilterBuilder : SqlFilterBuilder<BookedBedRoomColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BookedBedRoomFilterBuilder class.
		/// </summary>
		public BookedBedRoomFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BookedBedRoomFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BookedBedRoomFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BookedBedRoomFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BookedBedRoomFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BookedBedRoomFilterBuilder
	
	#region BookedBedRoomParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;BookedBedRoomColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BookedBedRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BookedBedRoomParameterBuilder : ParameterizedSqlFilterBuilder<BookedBedRoomColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BookedBedRoomParameterBuilder class.
		/// </summary>
		public BookedBedRoomParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BookedBedRoomParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BookedBedRoomParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BookedBedRoomParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BookedBedRoomParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BookedBedRoomParameterBuilder
	
	#region BookedBedRoomSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;BookedBedRoomColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BookedBedRoom"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class BookedBedRoomSortBuilder : SqlSortBuilder<BookedBedRoomColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BookedBedRoomSqlSortBuilder class.
		/// </summary>
		public BookedBedRoomSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion BookedBedRoomSortBuilder
	
} // end namespace
