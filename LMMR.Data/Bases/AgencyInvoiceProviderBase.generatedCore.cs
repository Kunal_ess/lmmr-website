﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AgencyInvoiceProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AgencyInvoiceProviderBaseCore : EntityProviderBase<LMMR.Entities.AgencyInvoice, LMMR.Entities.AgencyInvoiceKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.AgencyInvoiceKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AgencyInvoice_AgencyClient key.
		///		FK_AgencyInvoice_AgencyClient Description: 
		/// </summary>
		/// <param name="_agencyClientId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.AgencyInvoice objects.</returns>
		public TList<AgencyInvoice> GetByAgencyClientId(System.Int64 _agencyClientId)
		{
			int count = -1;
			return GetByAgencyClientId(_agencyClientId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AgencyInvoice_AgencyClient key.
		///		FK_AgencyInvoice_AgencyClient Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_agencyClientId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.AgencyInvoice objects.</returns>
		/// <remarks></remarks>
		public TList<AgencyInvoice> GetByAgencyClientId(TransactionManager transactionManager, System.Int64 _agencyClientId)
		{
			int count = -1;
			return GetByAgencyClientId(transactionManager, _agencyClientId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_AgencyInvoice_AgencyClient key.
		///		FK_AgencyInvoice_AgencyClient Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_agencyClientId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.AgencyInvoice objects.</returns>
		public TList<AgencyInvoice> GetByAgencyClientId(TransactionManager transactionManager, System.Int64 _agencyClientId, int start, int pageLength)
		{
			int count = -1;
			return GetByAgencyClientId(transactionManager, _agencyClientId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AgencyInvoice_AgencyClient key.
		///		fkAgencyInvoiceAgencyClient Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_agencyClientId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.AgencyInvoice objects.</returns>
		public TList<AgencyInvoice> GetByAgencyClientId(System.Int64 _agencyClientId, int start, int pageLength)
		{
			int count =  -1;
			return GetByAgencyClientId(null, _agencyClientId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AgencyInvoice_AgencyClient key.
		///		fkAgencyInvoiceAgencyClient Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_agencyClientId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.AgencyInvoice objects.</returns>
		public TList<AgencyInvoice> GetByAgencyClientId(System.Int64 _agencyClientId, int start, int pageLength,out int count)
		{
			return GetByAgencyClientId(null, _agencyClientId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AgencyInvoice_AgencyClient key.
		///		FK_AgencyInvoice_AgencyClient Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_agencyClientId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.AgencyInvoice objects.</returns>
		public abstract TList<AgencyInvoice> GetByAgencyClientId(TransactionManager transactionManager, System.Int64 _agencyClientId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AgencyInvoice_Hotel key.
		///		FK_AgencyInvoice_Hotel Description: 
		/// </summary>
		/// <param name="_hotelId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.AgencyInvoice objects.</returns>
		public TList<AgencyInvoice> GetByHotelId(System.Int64 _hotelId)
		{
			int count = -1;
			return GetByHotelId(_hotelId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AgencyInvoice_Hotel key.
		///		FK_AgencyInvoice_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.AgencyInvoice objects.</returns>
		/// <remarks></remarks>
		public TList<AgencyInvoice> GetByHotelId(TransactionManager transactionManager, System.Int64 _hotelId)
		{
			int count = -1;
			return GetByHotelId(transactionManager, _hotelId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_AgencyInvoice_Hotel key.
		///		FK_AgencyInvoice_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.AgencyInvoice objects.</returns>
		public TList<AgencyInvoice> GetByHotelId(TransactionManager transactionManager, System.Int64 _hotelId, int start, int pageLength)
		{
			int count = -1;
			return GetByHotelId(transactionManager, _hotelId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AgencyInvoice_Hotel key.
		///		fkAgencyInvoiceHotel Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_hotelId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.AgencyInvoice objects.</returns>
		public TList<AgencyInvoice> GetByHotelId(System.Int64 _hotelId, int start, int pageLength)
		{
			int count =  -1;
			return GetByHotelId(null, _hotelId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AgencyInvoice_Hotel key.
		///		fkAgencyInvoiceHotel Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_hotelId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.AgencyInvoice objects.</returns>
		public TList<AgencyInvoice> GetByHotelId(System.Int64 _hotelId, int start, int pageLength,out int count)
		{
			return GetByHotelId(null, _hotelId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AgencyInvoice_Hotel key.
		///		FK_AgencyInvoice_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.AgencyInvoice objects.</returns>
		public abstract TList<AgencyInvoice> GetByHotelId(TransactionManager transactionManager, System.Int64 _hotelId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.AgencyInvoice Get(TransactionManager transactionManager, LMMR.Entities.AgencyInvoiceKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_AgencyInvoice index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.AgencyInvoice"/> class.</returns>
		public LMMR.Entities.AgencyInvoice GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AgencyInvoice index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.AgencyInvoice"/> class.</returns>
		public LMMR.Entities.AgencyInvoice GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AgencyInvoice index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.AgencyInvoice"/> class.</returns>
		public LMMR.Entities.AgencyInvoice GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AgencyInvoice index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.AgencyInvoice"/> class.</returns>
		public LMMR.Entities.AgencyInvoice GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AgencyInvoice index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.AgencyInvoice"/> class.</returns>
		public LMMR.Entities.AgencyInvoice GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AgencyInvoice index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.AgencyInvoice"/> class.</returns>
		public abstract LMMR.Entities.AgencyInvoice GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;AgencyInvoice&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;AgencyInvoice&gt;"/></returns>
		public static TList<AgencyInvoice> Fill(IDataReader reader, TList<AgencyInvoice> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.AgencyInvoice c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("AgencyInvoice")
					.Append("|").Append((System.Int64)reader[((int)AgencyInvoiceColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<AgencyInvoice>(
					key.ToString(), // EntityTrackingKey
					"AgencyInvoice",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.AgencyInvoice();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)AgencyInvoiceColumn.Id - 1)];
					c.HotelId = (System.Int64)reader[((int)AgencyInvoiceColumn.HotelId - 1)];
					c.AgencyUserId = (System.Int64)reader[((int)AgencyInvoiceColumn.AgencyUserId - 1)];
					c.AgencyClientId = (System.Int64)reader[((int)AgencyInvoiceColumn.AgencyClientId - 1)];
					c.BookingId = (reader.IsDBNull(((int)AgencyInvoiceColumn.BookingId - 1)))?null:(System.Int64?)reader[((int)AgencyInvoiceColumn.BookingId - 1)];
					c.CommInvoiceAmount = (System.Decimal)reader[((int)AgencyInvoiceColumn.CommInvoiceAmount - 1)];
					c.IsInvoiceSent = (System.Boolean)reader[((int)AgencyInvoiceColumn.IsInvoiceSent - 1)];
					c.InvoiceSentDate = (reader.IsDBNull(((int)AgencyInvoiceColumn.InvoiceSentDate - 1)))?null:(System.DateTime?)reader[((int)AgencyInvoiceColumn.InvoiceSentDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.AgencyInvoice"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.AgencyInvoice"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.AgencyInvoice entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)AgencyInvoiceColumn.Id - 1)];
			entity.HotelId = (System.Int64)reader[((int)AgencyInvoiceColumn.HotelId - 1)];
			entity.AgencyUserId = (System.Int64)reader[((int)AgencyInvoiceColumn.AgencyUserId - 1)];
			entity.AgencyClientId = (System.Int64)reader[((int)AgencyInvoiceColumn.AgencyClientId - 1)];
			entity.BookingId = (reader.IsDBNull(((int)AgencyInvoiceColumn.BookingId - 1)))?null:(System.Int64?)reader[((int)AgencyInvoiceColumn.BookingId - 1)];
			entity.CommInvoiceAmount = (System.Decimal)reader[((int)AgencyInvoiceColumn.CommInvoiceAmount - 1)];
			entity.IsInvoiceSent = (System.Boolean)reader[((int)AgencyInvoiceColumn.IsInvoiceSent - 1)];
			entity.InvoiceSentDate = (reader.IsDBNull(((int)AgencyInvoiceColumn.InvoiceSentDate - 1)))?null:(System.DateTime?)reader[((int)AgencyInvoiceColumn.InvoiceSentDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.AgencyInvoice"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.AgencyInvoice"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.AgencyInvoice entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.HotelId = (System.Int64)dataRow["HotelId"];
			entity.AgencyUserId = (System.Int64)dataRow["AgencyUserId"];
			entity.AgencyClientId = (System.Int64)dataRow["AgencyClientId"];
			entity.BookingId = Convert.IsDBNull(dataRow["BookingId"]) ? null : (System.Int64?)dataRow["BookingId"];
			entity.CommInvoiceAmount = (System.Decimal)dataRow["CommInvoiceAmount"];
			entity.IsInvoiceSent = (System.Boolean)dataRow["IsInvoiceSent"];
			entity.InvoiceSentDate = Convert.IsDBNull(dataRow["InvoiceSentDate"]) ? null : (System.DateTime?)dataRow["InvoiceSentDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.AgencyInvoice"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.AgencyInvoice Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.AgencyInvoice entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region AgencyClientIdSource	
			if (CanDeepLoad(entity, "AgencyClient|AgencyClientIdSource", deepLoadType, innerList) 
				&& entity.AgencyClientIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.AgencyClientId;
				AgencyClient tmpEntity = EntityManager.LocateEntity<AgencyClient>(EntityLocator.ConstructKeyFromPkItems(typeof(AgencyClient), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AgencyClientIdSource = tmpEntity;
				else
					entity.AgencyClientIdSource = DataRepository.AgencyClientProvider.GetById(transactionManager, entity.AgencyClientId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AgencyClientIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AgencyClientIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AgencyClientProvider.DeepLoad(transactionManager, entity.AgencyClientIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AgencyClientIdSource

			#region HotelIdSource	
			if (CanDeepLoad(entity, "Hotel|HotelIdSource", deepLoadType, innerList) 
				&& entity.HotelIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.HotelId;
				Hotel tmpEntity = EntityManager.LocateEntity<Hotel>(EntityLocator.ConstructKeyFromPkItems(typeof(Hotel), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.HotelIdSource = tmpEntity;
				else
					entity.HotelIdSource = DataRepository.HotelProvider.GetById(transactionManager, entity.HotelId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HotelIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.HotelIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.HotelProvider.DeepLoad(transactionManager, entity.HotelIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion HotelIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.AgencyInvoice object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.AgencyInvoice instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.AgencyInvoice Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.AgencyInvoice entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region AgencyClientIdSource
			if (CanDeepSave(entity, "AgencyClient|AgencyClientIdSource", deepSaveType, innerList) 
				&& entity.AgencyClientIdSource != null)
			{
				DataRepository.AgencyClientProvider.Save(transactionManager, entity.AgencyClientIdSource);
				entity.AgencyClientId = entity.AgencyClientIdSource.Id;
			}
			#endregion 
			
			#region HotelIdSource
			if (CanDeepSave(entity, "Hotel|HotelIdSource", deepSaveType, innerList) 
				&& entity.HotelIdSource != null)
			{
				DataRepository.HotelProvider.Save(transactionManager, entity.HotelIdSource);
				entity.HotelId = entity.HotelIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AgencyInvoiceChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.AgencyInvoice</c>
	///</summary>
	public enum AgencyInvoiceChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>AgencyClient</c> at AgencyClientIdSource
		///</summary>
		[ChildEntityType(typeof(AgencyClient))]
		AgencyClient,
			
		///<summary>
		/// Composite Property for <c>Hotel</c> at HotelIdSource
		///</summary>
		[ChildEntityType(typeof(Hotel))]
		Hotel,
		}
	
	#endregion AgencyInvoiceChildEntityTypes
	
	#region AgencyInvoiceFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AgencyInvoiceColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AgencyInvoice"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AgencyInvoiceFilterBuilder : SqlFilterBuilder<AgencyInvoiceColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AgencyInvoiceFilterBuilder class.
		/// </summary>
		public AgencyInvoiceFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AgencyInvoiceFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AgencyInvoiceFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AgencyInvoiceFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AgencyInvoiceFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AgencyInvoiceFilterBuilder
	
	#region AgencyInvoiceParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AgencyInvoiceColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AgencyInvoice"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AgencyInvoiceParameterBuilder : ParameterizedSqlFilterBuilder<AgencyInvoiceColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AgencyInvoiceParameterBuilder class.
		/// </summary>
		public AgencyInvoiceParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AgencyInvoiceParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AgencyInvoiceParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AgencyInvoiceParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AgencyInvoiceParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AgencyInvoiceParameterBuilder
	
	#region AgencyInvoiceSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AgencyInvoiceColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AgencyInvoice"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AgencyInvoiceSortBuilder : SqlSortBuilder<AgencyInvoiceColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AgencyInvoiceSqlSortBuilder class.
		/// </summary>
		public AgencyInvoiceSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AgencyInvoiceSortBuilder
	
} // end namespace
