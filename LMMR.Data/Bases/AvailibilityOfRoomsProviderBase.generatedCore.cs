﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AvailibilityOfRoomsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AvailibilityOfRoomsProviderBaseCore : EntityProviderBase<LMMR.Entities.AvailibilityOfRooms, LMMR.Entities.AvailibilityOfRoomsKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.AvailibilityOfRoomsKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AvailibilityBedRoom_Availability key.
		///		FK_AvailibilityBedRoom_Availability Description: 
		/// </summary>
		/// <param name="_availibilityIdForBedRoom"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.AvailibilityOfRooms objects.</returns>
		public TList<AvailibilityOfRooms> GetByAvailibilityIdForBedRoom(System.Int64 _availibilityIdForBedRoom)
		{
			int count = -1;
			return GetByAvailibilityIdForBedRoom(_availibilityIdForBedRoom, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AvailibilityBedRoom_Availability key.
		///		FK_AvailibilityBedRoom_Availability Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_availibilityIdForBedRoom"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.AvailibilityOfRooms objects.</returns>
		/// <remarks></remarks>
		public TList<AvailibilityOfRooms> GetByAvailibilityIdForBedRoom(TransactionManager transactionManager, System.Int64 _availibilityIdForBedRoom)
		{
			int count = -1;
			return GetByAvailibilityIdForBedRoom(transactionManager, _availibilityIdForBedRoom, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_AvailibilityBedRoom_Availability key.
		///		FK_AvailibilityBedRoom_Availability Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_availibilityIdForBedRoom"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.AvailibilityOfRooms objects.</returns>
		public TList<AvailibilityOfRooms> GetByAvailibilityIdForBedRoom(TransactionManager transactionManager, System.Int64 _availibilityIdForBedRoom, int start, int pageLength)
		{
			int count = -1;
			return GetByAvailibilityIdForBedRoom(transactionManager, _availibilityIdForBedRoom, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AvailibilityBedRoom_Availability key.
		///		fkAvailibilityBedRoomAvailability Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_availibilityIdForBedRoom"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.AvailibilityOfRooms objects.</returns>
		public TList<AvailibilityOfRooms> GetByAvailibilityIdForBedRoom(System.Int64 _availibilityIdForBedRoom, int start, int pageLength)
		{
			int count =  -1;
			return GetByAvailibilityIdForBedRoom(null, _availibilityIdForBedRoom, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AvailibilityBedRoom_Availability key.
		///		fkAvailibilityBedRoomAvailability Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_availibilityIdForBedRoom"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.AvailibilityOfRooms objects.</returns>
		public TList<AvailibilityOfRooms> GetByAvailibilityIdForBedRoom(System.Int64 _availibilityIdForBedRoom, int start, int pageLength,out int count)
		{
			return GetByAvailibilityIdForBedRoom(null, _availibilityIdForBedRoom, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AvailibilityBedRoom_Availability key.
		///		FK_AvailibilityBedRoom_Availability Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_availibilityIdForBedRoom"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.AvailibilityOfRooms objects.</returns>
		public abstract TList<AvailibilityOfRooms> GetByAvailibilityIdForBedRoom(TransactionManager transactionManager, System.Int64 _availibilityIdForBedRoom, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.AvailibilityOfRooms Get(TransactionManager transactionManager, LMMR.Entities.AvailibilityOfRoomsKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_AvailibilityBedRoom index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.AvailibilityOfRooms"/> class.</returns>
		public LMMR.Entities.AvailibilityOfRooms GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AvailibilityBedRoom index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.AvailibilityOfRooms"/> class.</returns>
		public LMMR.Entities.AvailibilityOfRooms GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AvailibilityBedRoom index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.AvailibilityOfRooms"/> class.</returns>
		public LMMR.Entities.AvailibilityOfRooms GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AvailibilityBedRoom index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.AvailibilityOfRooms"/> class.</returns>
		public LMMR.Entities.AvailibilityOfRooms GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AvailibilityBedRoom index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.AvailibilityOfRooms"/> class.</returns>
		public LMMR.Entities.AvailibilityOfRooms GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AvailibilityBedRoom index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.AvailibilityOfRooms"/> class.</returns>
		public abstract LMMR.Entities.AvailibilityOfRooms GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;AvailibilityOfRooms&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;AvailibilityOfRooms&gt;"/></returns>
		public static TList<AvailibilityOfRooms> Fill(IDataReader reader, TList<AvailibilityOfRooms> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.AvailibilityOfRooms c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("AvailibilityOfRooms")
					.Append("|").Append((System.Int64)reader[((int)AvailibilityOfRoomsColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<AvailibilityOfRooms>(
					key.ToString(), // EntityTrackingKey
					"AvailibilityOfRooms",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.AvailibilityOfRooms();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)AvailibilityOfRoomsColumn.Id - 1)];
					c.AvailibilityIdForBedRoom = (System.Int64)reader[((int)AvailibilityOfRoomsColumn.AvailibilityIdForBedRoom - 1)];
					c.RoomId = (System.Int64)reader[((int)AvailibilityOfRoomsColumn.RoomId - 1)];
					c.MorningStatus = (reader.IsDBNull(((int)AvailibilityOfRoomsColumn.MorningStatus - 1)))?null:(System.Int32?)reader[((int)AvailibilityOfRoomsColumn.MorningStatus - 1)];
					c.AfternoonStatus = (reader.IsDBNull(((int)AvailibilityOfRoomsColumn.AfternoonStatus - 1)))?null:(System.Int32?)reader[((int)AvailibilityOfRoomsColumn.AfternoonStatus - 1)];
					c.NumberOfRoomsAvailable = (reader.IsDBNull(((int)AvailibilityOfRoomsColumn.NumberOfRoomsAvailable - 1)))?null:(System.Int64?)reader[((int)AvailibilityOfRoomsColumn.NumberOfRoomsAvailable - 1)];
					c.RoomType = (reader.IsDBNull(((int)AvailibilityOfRoomsColumn.RoomType - 1)))?null:(System.Int32?)reader[((int)AvailibilityOfRoomsColumn.RoomType - 1)];
					c.IsReserved = (System.Boolean)reader[((int)AvailibilityOfRoomsColumn.IsReserved - 1)];
					c.LastModifyTime = (reader.IsDBNull(((int)AvailibilityOfRoomsColumn.LastModifyTime - 1)))?null:(System.DateTime?)reader[((int)AvailibilityOfRoomsColumn.LastModifyTime - 1)];
					c.NumberOfRoomBooked = (reader.IsDBNull(((int)AvailibilityOfRoomsColumn.NumberOfRoomBooked - 1)))?null:(System.Int64?)reader[((int)AvailibilityOfRoomsColumn.NumberOfRoomBooked - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.AvailibilityOfRooms"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.AvailibilityOfRooms"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.AvailibilityOfRooms entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)AvailibilityOfRoomsColumn.Id - 1)];
			entity.AvailibilityIdForBedRoom = (System.Int64)reader[((int)AvailibilityOfRoomsColumn.AvailibilityIdForBedRoom - 1)];
			entity.RoomId = (System.Int64)reader[((int)AvailibilityOfRoomsColumn.RoomId - 1)];
			entity.MorningStatus = (reader.IsDBNull(((int)AvailibilityOfRoomsColumn.MorningStatus - 1)))?null:(System.Int32?)reader[((int)AvailibilityOfRoomsColumn.MorningStatus - 1)];
			entity.AfternoonStatus = (reader.IsDBNull(((int)AvailibilityOfRoomsColumn.AfternoonStatus - 1)))?null:(System.Int32?)reader[((int)AvailibilityOfRoomsColumn.AfternoonStatus - 1)];
			entity.NumberOfRoomsAvailable = (reader.IsDBNull(((int)AvailibilityOfRoomsColumn.NumberOfRoomsAvailable - 1)))?null:(System.Int64?)reader[((int)AvailibilityOfRoomsColumn.NumberOfRoomsAvailable - 1)];
			entity.RoomType = (reader.IsDBNull(((int)AvailibilityOfRoomsColumn.RoomType - 1)))?null:(System.Int32?)reader[((int)AvailibilityOfRoomsColumn.RoomType - 1)];
			entity.IsReserved = (System.Boolean)reader[((int)AvailibilityOfRoomsColumn.IsReserved - 1)];
			entity.LastModifyTime = (reader.IsDBNull(((int)AvailibilityOfRoomsColumn.LastModifyTime - 1)))?null:(System.DateTime?)reader[((int)AvailibilityOfRoomsColumn.LastModifyTime - 1)];
			entity.NumberOfRoomBooked = (reader.IsDBNull(((int)AvailibilityOfRoomsColumn.NumberOfRoomBooked - 1)))?null:(System.Int64?)reader[((int)AvailibilityOfRoomsColumn.NumberOfRoomBooked - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.AvailibilityOfRooms"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.AvailibilityOfRooms"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.AvailibilityOfRooms entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.AvailibilityIdForBedRoom = (System.Int64)dataRow["AvailibilityIdForBedRoom"];
			entity.RoomId = (System.Int64)dataRow["RoomId"];
			entity.MorningStatus = Convert.IsDBNull(dataRow["MorningStatus"]) ? null : (System.Int32?)dataRow["MorningStatus"];
			entity.AfternoonStatus = Convert.IsDBNull(dataRow["AfternoonStatus"]) ? null : (System.Int32?)dataRow["AfternoonStatus"];
			entity.NumberOfRoomsAvailable = Convert.IsDBNull(dataRow["NumberOfRoomsAvailable"]) ? null : (System.Int64?)dataRow["NumberOfRoomsAvailable"];
			entity.RoomType = Convert.IsDBNull(dataRow["RoomType"]) ? null : (System.Int32?)dataRow["RoomType"];
			entity.IsReserved = (System.Boolean)dataRow["IsReserved"];
			entity.LastModifyTime = Convert.IsDBNull(dataRow["LastModifyTime"]) ? null : (System.DateTime?)dataRow["LastModifyTime"];
			entity.NumberOfRoomBooked = Convert.IsDBNull(dataRow["NumberOfRoomBooked"]) ? null : (System.Int64?)dataRow["NumberOfRoomBooked"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.AvailibilityOfRooms"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.AvailibilityOfRooms Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.AvailibilityOfRooms entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region AvailibilityIdForBedRoomSource	
			if (CanDeepLoad(entity, "Availability|AvailibilityIdForBedRoomSource", deepLoadType, innerList) 
				&& entity.AvailibilityIdForBedRoomSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.AvailibilityIdForBedRoom;
				Availability tmpEntity = EntityManager.LocateEntity<Availability>(EntityLocator.ConstructKeyFromPkItems(typeof(Availability), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AvailibilityIdForBedRoomSource = tmpEntity;
				else
					entity.AvailibilityIdForBedRoomSource = DataRepository.AvailabilityProvider.GetById(transactionManager, entity.AvailibilityIdForBedRoom);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AvailibilityIdForBedRoomSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AvailibilityIdForBedRoomSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AvailabilityProvider.DeepLoad(transactionManager, entity.AvailibilityIdForBedRoomSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AvailibilityIdForBedRoomSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.AvailibilityOfRooms object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.AvailibilityOfRooms instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.AvailibilityOfRooms Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.AvailibilityOfRooms entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region AvailibilityIdForBedRoomSource
			if (CanDeepSave(entity, "Availability|AvailibilityIdForBedRoomSource", deepSaveType, innerList) 
				&& entity.AvailibilityIdForBedRoomSource != null)
			{
				DataRepository.AvailabilityProvider.DeepSave(transactionManager, entity.AvailibilityIdForBedRoomSource);
				entity.AvailibilityIdForBedRoom = entity.AvailibilityIdForBedRoomSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AvailibilityOfRoomsChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.AvailibilityOfRooms</c>
	///</summary>
	public enum AvailibilityOfRoomsChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Availability</c> at AvailibilityIdForBedRoomSource
		///</summary>
		[ChildEntityType(typeof(Availability))]
		Availability,
		}
	
	#endregion AvailibilityOfRoomsChildEntityTypes
	
	#region AvailibilityOfRoomsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AvailibilityOfRoomsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AvailibilityOfRooms"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AvailibilityOfRoomsFilterBuilder : SqlFilterBuilder<AvailibilityOfRoomsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AvailibilityOfRoomsFilterBuilder class.
		/// </summary>
		public AvailibilityOfRoomsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AvailibilityOfRoomsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AvailibilityOfRoomsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AvailibilityOfRoomsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AvailibilityOfRoomsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AvailibilityOfRoomsFilterBuilder
	
	#region AvailibilityOfRoomsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AvailibilityOfRoomsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AvailibilityOfRooms"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AvailibilityOfRoomsParameterBuilder : ParameterizedSqlFilterBuilder<AvailibilityOfRoomsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AvailibilityOfRoomsParameterBuilder class.
		/// </summary>
		public AvailibilityOfRoomsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AvailibilityOfRoomsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AvailibilityOfRoomsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AvailibilityOfRoomsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AvailibilityOfRoomsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AvailibilityOfRoomsParameterBuilder
	
	#region AvailibilityOfRoomsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AvailibilityOfRoomsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AvailibilityOfRooms"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AvailibilityOfRoomsSortBuilder : SqlSortBuilder<AvailibilityOfRoomsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AvailibilityOfRoomsSqlSortBuilder class.
		/// </summary>
		public AvailibilityOfRoomsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AvailibilityOfRoomsSortBuilder
	
} // end namespace
