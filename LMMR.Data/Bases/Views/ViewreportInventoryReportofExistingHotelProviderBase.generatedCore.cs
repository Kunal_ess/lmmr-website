﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewreportInventoryReportofExistingHotelProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewreportInventoryReportofExistingHotelProviderBaseCore : EntityViewProviderBase<ViewreportInventoryReportofExistingHotel>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewreportInventoryReportofExistingHotel&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewreportInventoryReportofExistingHotel&gt;"/></returns>
		protected static VList&lt;ViewreportInventoryReportofExistingHotel&gt; Fill(DataSet dataSet, VList<ViewreportInventoryReportofExistingHotel> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewreportInventoryReportofExistingHotel>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewreportInventoryReportofExistingHotel&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewreportInventoryReportofExistingHotel>"/></returns>
		protected static VList&lt;ViewreportInventoryReportofExistingHotel&gt; Fill(DataTable dataTable, VList<ViewreportInventoryReportofExistingHotel> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewreportInventoryReportofExistingHotel c = new ViewreportInventoryReportofExistingHotel();
					c.NoOfMeetingRoom = (Convert.IsDBNull(row["NoOfMeetingRoom"]))?(int)0:(System.Int32?)row["NoOfMeetingRoom"];
					c.NoofMeetingRoomOnline = (Convert.IsDBNull(row["NOofMeetingRoomOnline"]))?(int)0:(System.Int32?)row["NOofMeetingRoomOnline"];
					c.HotelId = (Convert.IsDBNull(row["HotelId"]))?(long)0:(System.Int64)row["HotelId"];
					c.HotelName = (Convert.IsDBNull(row["HotelName"]))?string.Empty:(System.String)row["HotelName"];
					c.City = (Convert.IsDBNull(row["city"]))?string.Empty:(System.String)row["city"];
					c.Countryname = (Convert.IsDBNull(row["Countryname"]))?string.Empty:(System.String)row["Countryname"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewreportInventoryReportofExistingHotel&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewreportInventoryReportofExistingHotel&gt;"/></returns>
		protected VList<ViewreportInventoryReportofExistingHotel> Fill(IDataReader reader, VList<ViewreportInventoryReportofExistingHotel> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewreportInventoryReportofExistingHotel entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewreportInventoryReportofExistingHotel>("ViewreportInventoryReportofExistingHotel",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewreportInventoryReportofExistingHotel();
					}
					
					entity.SuppressEntityEvents = true;

					entity.NoOfMeetingRoom = (reader.IsDBNull(((int)ViewreportInventoryReportofExistingHotelColumn.NoOfMeetingRoom)))?null:(System.Int32?)reader[((int)ViewreportInventoryReportofExistingHotelColumn.NoOfMeetingRoom)];
					//entity.NoOfMeetingRoom = (Convert.IsDBNull(reader["NoOfMeetingRoom"]))?(int)0:(System.Int32?)reader["NoOfMeetingRoom"];
					entity.NoofMeetingRoomOnline = (reader.IsDBNull(((int)ViewreportInventoryReportofExistingHotelColumn.NoofMeetingRoomOnline)))?null:(System.Int32?)reader[((int)ViewreportInventoryReportofExistingHotelColumn.NoofMeetingRoomOnline)];
					//entity.NoofMeetingRoomOnline = (Convert.IsDBNull(reader["NOofMeetingRoomOnline"]))?(int)0:(System.Int32?)reader["NOofMeetingRoomOnline"];
					entity.HotelId = (System.Int64)reader[((int)ViewreportInventoryReportofExistingHotelColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
					entity.HotelName = (reader.IsDBNull(((int)ViewreportInventoryReportofExistingHotelColumn.HotelName)))?null:(System.String)reader[((int)ViewreportInventoryReportofExistingHotelColumn.HotelName)];
					//entity.HotelName = (Convert.IsDBNull(reader["HotelName"]))?string.Empty:(System.String)reader["HotelName"];
					entity.City = (reader.IsDBNull(((int)ViewreportInventoryReportofExistingHotelColumn.City)))?null:(System.String)reader[((int)ViewreportInventoryReportofExistingHotelColumn.City)];
					//entity.City = (Convert.IsDBNull(reader["city"]))?string.Empty:(System.String)reader["city"];
					entity.Countryname = (reader.IsDBNull(((int)ViewreportInventoryReportofExistingHotelColumn.Countryname)))?null:(System.String)reader[((int)ViewreportInventoryReportofExistingHotelColumn.Countryname)];
					//entity.Countryname = (Convert.IsDBNull(reader["Countryname"]))?string.Empty:(System.String)reader["Countryname"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewreportInventoryReportofExistingHotel"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewreportInventoryReportofExistingHotel"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewreportInventoryReportofExistingHotel entity)
		{
			reader.Read();
			entity.NoOfMeetingRoom = (reader.IsDBNull(((int)ViewreportInventoryReportofExistingHotelColumn.NoOfMeetingRoom)))?null:(System.Int32?)reader[((int)ViewreportInventoryReportofExistingHotelColumn.NoOfMeetingRoom)];
			//entity.NoOfMeetingRoom = (Convert.IsDBNull(reader["NoOfMeetingRoom"]))?(int)0:(System.Int32?)reader["NoOfMeetingRoom"];
			entity.NoofMeetingRoomOnline = (reader.IsDBNull(((int)ViewreportInventoryReportofExistingHotelColumn.NoofMeetingRoomOnline)))?null:(System.Int32?)reader[((int)ViewreportInventoryReportofExistingHotelColumn.NoofMeetingRoomOnline)];
			//entity.NoofMeetingRoomOnline = (Convert.IsDBNull(reader["NOofMeetingRoomOnline"]))?(int)0:(System.Int32?)reader["NOofMeetingRoomOnline"];
			entity.HotelId = (System.Int64)reader[((int)ViewreportInventoryReportofExistingHotelColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
			entity.HotelName = (reader.IsDBNull(((int)ViewreportInventoryReportofExistingHotelColumn.HotelName)))?null:(System.String)reader[((int)ViewreportInventoryReportofExistingHotelColumn.HotelName)];
			//entity.HotelName = (Convert.IsDBNull(reader["HotelName"]))?string.Empty:(System.String)reader["HotelName"];
			entity.City = (reader.IsDBNull(((int)ViewreportInventoryReportofExistingHotelColumn.City)))?null:(System.String)reader[((int)ViewreportInventoryReportofExistingHotelColumn.City)];
			//entity.City = (Convert.IsDBNull(reader["city"]))?string.Empty:(System.String)reader["city"];
			entity.Countryname = (reader.IsDBNull(((int)ViewreportInventoryReportofExistingHotelColumn.Countryname)))?null:(System.String)reader[((int)ViewreportInventoryReportofExistingHotelColumn.Countryname)];
			//entity.Countryname = (Convert.IsDBNull(reader["Countryname"]))?string.Empty:(System.String)reader["Countryname"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewreportInventoryReportofExistingHotel"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewreportInventoryReportofExistingHotel"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewreportInventoryReportofExistingHotel entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.NoOfMeetingRoom = (Convert.IsDBNull(dataRow["NoOfMeetingRoom"]))?(int)0:(System.Int32?)dataRow["NoOfMeetingRoom"];
			entity.NoofMeetingRoomOnline = (Convert.IsDBNull(dataRow["NOofMeetingRoomOnline"]))?(int)0:(System.Int32?)dataRow["NOofMeetingRoomOnline"];
			entity.HotelId = (Convert.IsDBNull(dataRow["HotelId"]))?(long)0:(System.Int64)dataRow["HotelId"];
			entity.HotelName = (Convert.IsDBNull(dataRow["HotelName"]))?string.Empty:(System.String)dataRow["HotelName"];
			entity.City = (Convert.IsDBNull(dataRow["city"]))?string.Empty:(System.String)dataRow["city"];
			entity.Countryname = (Convert.IsDBNull(dataRow["Countryname"]))?string.Empty:(System.String)dataRow["Countryname"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewreportInventoryReportofExistingHotelFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewreportInventoryReportofExistingHotel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewreportInventoryReportofExistingHotelFilterBuilder : SqlFilterBuilder<ViewreportInventoryReportofExistingHotelColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewreportInventoryReportofExistingHotelFilterBuilder class.
		/// </summary>
		public ViewreportInventoryReportofExistingHotelFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewreportInventoryReportofExistingHotelFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewreportInventoryReportofExistingHotelFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewreportInventoryReportofExistingHotelFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewreportInventoryReportofExistingHotelFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewreportInventoryReportofExistingHotelFilterBuilder

	#region ViewreportInventoryReportofExistingHotelParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewreportInventoryReportofExistingHotel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewreportInventoryReportofExistingHotelParameterBuilder : ParameterizedSqlFilterBuilder<ViewreportInventoryReportofExistingHotelColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewreportInventoryReportofExistingHotelParameterBuilder class.
		/// </summary>
		public ViewreportInventoryReportofExistingHotelParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewreportInventoryReportofExistingHotelParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewreportInventoryReportofExistingHotelParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewreportInventoryReportofExistingHotelParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewreportInventoryReportofExistingHotelParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewreportInventoryReportofExistingHotelParameterBuilder
	
	#region ViewreportInventoryReportofExistingHotelSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewreportInventoryReportofExistingHotel"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewreportInventoryReportofExistingHotelSortBuilder : SqlSortBuilder<ViewreportInventoryReportofExistingHotelColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewreportInventoryReportofExistingHotelSqlSortBuilder class.
		/// </summary>
		public ViewreportInventoryReportofExistingHotelSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewreportInventoryReportofExistingHotelSortBuilder

} // end namespace
