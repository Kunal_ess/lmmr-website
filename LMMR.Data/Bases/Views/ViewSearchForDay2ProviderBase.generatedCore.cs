﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewSearchForDay2ProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewSearchForDay2ProviderBaseCore : EntityViewProviderBase<ViewSearchForDay2>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewSearchForDay2&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewSearchForDay2&gt;"/></returns>
		protected static VList&lt;ViewSearchForDay2&gt; Fill(DataSet dataSet, VList<ViewSearchForDay2> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewSearchForDay2>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewSearchForDay2&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewSearchForDay2>"/></returns>
		protected static VList&lt;ViewSearchForDay2&gt; Fill(DataTable dataTable, VList<ViewSearchForDay2> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewSearchForDay2 c = new ViewSearchForDay2();
					c.HotelId = (Convert.IsDBNull(row["HotelId"]))?(long)0:(System.Int64)row["HotelId"];
					c.HotelName = (Convert.IsDBNull(row["HotelName"]))?string.Empty:(System.String)row["HotelName"];
					c.CountryId = (Convert.IsDBNull(row["CountryId"]))?(long)0:(System.Int64)row["CountryId"];
					c.CityId = (Convert.IsDBNull(row["CityId"]))?(long)0:(System.Int64)row["CityId"];
					c.ZoneId = (Convert.IsDBNull(row["ZoneId"]))?(long)0:(System.Int64)row["ZoneId"];
					c.Stars = (Convert.IsDBNull(row["Stars"]))?(int)0:(System.Int32?)row["Stars"];
					c.Longitude = (Convert.IsDBNull(row["Longitude"]))?string.Empty:(System.String)row["Longitude"];
					c.Latitude = (Convert.IsDBNull(row["Latitude"]))?string.Empty:(System.String)row["Latitude"];
					c.Logo = (Convert.IsDBNull(row["Logo"]))?string.Empty:(System.String)row["Logo"];
					c.IsActive = (Convert.IsDBNull(row["IsActive"]))?false:(System.Boolean)row["IsActive"];
					c.CancelationPolicyId = (Convert.IsDBNull(row["CancelationPolicyId"]))?(long)0:(System.Int64)row["CancelationPolicyId"];
					c.GroupId = (Convert.IsDBNull(row["GroupId"]))?(long)0:(System.Int64?)row["GroupId"];
					c.OperatorChoice = (Convert.IsDBNull(row["OperatorChoice"]))?string.Empty:(System.String)row["OperatorChoice"];
					c.HotelPlan = (Convert.IsDBNull(row["HotelPlan"]))?string.Empty:(System.String)row["HotelPlan"];
					c.ClientId = (Convert.IsDBNull(row["ClientId"]))?(long)0:(System.Int64?)row["ClientId"];
					c.ContractId = (Convert.IsDBNull(row["ContractId"]))?string.Empty:(System.String)row["ContractId"];
					c.StaffId = (Convert.IsDBNull(row["StaffId"]))?(long)0:(System.Int64)row["StaffId"];
					c.CurrencyId = (Convert.IsDBNull(row["CurrencyId"]))?(long)0:(System.Int64)row["CurrencyId"];
					c.ContractValue = (Convert.IsDBNull(row["ContractValue"]))?0.0m:(System.Decimal?)row["ContractValue"];
					c.TimeZone = (Convert.IsDBNull(row["TimeZone"]))?string.Empty:(System.String)row["TimeZone"];
					c.Phone = (Convert.IsDBNull(row["Phone"]))?string.Empty:(System.String)row["Phone"];
					c.PhoneExt = (Convert.IsDBNull(row["PhoneExt"]))?string.Empty:(System.String)row["PhoneExt"];
					c.RtMFrom = (Convert.IsDBNull(row["RT_M_From"]))?string.Empty:(System.String)row["RT_M_From"];
					c.RtMTo = (Convert.IsDBNull(row["RT_M_To"]))?string.Empty:(System.String)row["RT_M_To"];
					c.RtAFrom = (Convert.IsDBNull(row["RT_A_From"]))?string.Empty:(System.String)row["RT_A_From"];
					c.RtATo = (Convert.IsDBNull(row["RT_A_To"]))?string.Empty:(System.String)row["RT_A_To"];
					c.RtFFrom = (Convert.IsDBNull(row["RT_F_From"]))?string.Empty:(System.String)row["RT_F_From"];
					c.RtFTo = (Convert.IsDBNull(row["RT_F_To"]))?string.Empty:(System.String)row["RT_F_To"];
					c.IsCreditCardExcepted = (Convert.IsDBNull(row["IsCreditCardExcepted"]))?false:(System.Boolean)row["IsCreditCardExcepted"];
					c.CreditCardType = (Convert.IsDBNull(row["CreditCardType"]))?string.Empty:(System.String)row["CreditCardType"];
					c.Theme = (Convert.IsDBNull(row["Theme"]))?string.Empty:(System.String)row["Theme"];
					c.IsBedroomAvailable = (Convert.IsDBNull(row["IsBedroomAvailable"]))?false:(System.Boolean)row["IsBedroomAvailable"];
					c.NumberOfBedroom = (Convert.IsDBNull(row["NumberOfBedroom"]))?(int)0:(System.Int32?)row["NumberOfBedroom"];
					c.Email = (Convert.IsDBNull(row["Email"]))?string.Empty:(System.String)row["Email"];
					c.ContactPerson = (Convert.IsDBNull(row["ContactPerson"]))?string.Empty:(System.String)row["ContactPerson"];
					c.PhoneNumber = (Convert.IsDBNull(row["PhoneNumber"]))?string.Empty:(System.String)row["PhoneNumber"];
					c.GoOnline = (Convert.IsDBNull(row["GoOnline"]))?false:(System.Boolean?)row["GoOnline"];
					c.ZipCode = (Convert.IsDBNull(row["ZipCode"]))?string.Empty:(System.String)row["ZipCode"];
					c.UpdatedBy = (Convert.IsDBNull(row["UpdatedBy"]))?(long)0:(System.Int64?)row["UpdatedBy"];
					c.HotelAddress = (Convert.IsDBNull(row["HotelAddress"]))?string.Empty:(System.String)row["HotelAddress"];
					c.CreationDate = (Convert.IsDBNull(row["CreationDate"]))?DateTime.MinValue:(System.DateTime)row["CreationDate"];
					c.CountryCodePhone = (Convert.IsDBNull(row["CountryCodePhone"]))?string.Empty:(System.String)row["CountryCodePhone"];
					c.CountryCodeFax = (Convert.IsDBNull(row["CountryCodeFax"]))?string.Empty:(System.String)row["CountryCodeFax"];
					c.FaxNumber = (Convert.IsDBNull(row["FaxNumber"]))?string.Empty:(System.String)row["FaxNumber"];
					c.IsRemoved = (Convert.IsDBNull(row["IsRemoved"]))?false:(System.Boolean)row["IsRemoved"];
					c.RequestGoOnline = (Convert.IsDBNull(row["RequestGoOnline"]))?false:(System.Boolean?)row["RequestGoOnline"];
					c.HotelUserId = (Convert.IsDBNull(row["HotelUserID"]))?(long)0:(System.Int64?)row["HotelUserID"];
					c.AccountingOfficer = (Convert.IsDBNull(row["AccountingOfficer"]))?string.Empty:(System.String)row["AccountingOfficer"];
					c.MeetingRoomId = (Convert.IsDBNull(row["MeetingRoomId"]))?(long)0:(System.Int64)row["MeetingRoomId"];
					c.MeetingRoomName = (Convert.IsDBNull(row["MeetingRoomName"]))?string.Empty:(System.String)row["MeetingRoomName"];
					c.Picture = (Convert.IsDBNull(row["Picture"]))?string.Empty:(System.String)row["Picture"];
					c.DayLight = (Convert.IsDBNull(row["DayLight"]))?(int)0:(System.Int32)row["DayLight"];
					c.Surface = (Convert.IsDBNull(row["Surface"]))?(long)0:(System.Int64?)row["Surface"];
					c.Height = (Convert.IsDBNull(row["Height"]))?0.0m:(System.Decimal?)row["Height"];
					c.MeetingRoomActive = (Convert.IsDBNull(row["MeetingRoomActive"]))?false:(System.Boolean)row["MeetingRoomActive"];
					c.MrPlan = (Convert.IsDBNull(row["MR_Plan"]))?string.Empty:(System.String)row["MR_Plan"];
					c.OrderNumber = (Convert.IsDBNull(row["OrderNumber"]))?(int)0:(System.Int32?)row["OrderNumber"];
					c.HalfdayPrice = (Convert.IsDBNull(row["HalfdayPrice"]))?0.0m:(System.Decimal?)row["HalfdayPrice"];
					c.FulldayPrice = (Convert.IsDBNull(row["FulldayPrice"]))?0.0m:(System.Decimal?)row["FulldayPrice"];
					c.IsOnline = (Convert.IsDBNull(row["IsOnline"]))?false:(System.Boolean)row["IsOnline"];
					c.IsDeleted = (Convert.IsDBNull(row["IsDeleted"]))?false:(System.Boolean)row["IsDeleted"];
					c.RoomType = (Convert.IsDBNull(row["RoomType"]))?(int)0:(System.Int32?)row["RoomType"];
					c.MeetingRoomConfigId = (Convert.IsDBNull(row["MeetingRoomConfigId"]))?(long)0:(System.Int64)row["MeetingRoomConfigId"];
					c.RoomShapeId = (Convert.IsDBNull(row["RoomShapeId"]))?(int)0:(System.Int32?)row["RoomShapeId"];
					c.MinCapacity = (Convert.IsDBNull(row["MinCapacity"]))?(int)0:(System.Int32?)row["MinCapacity"];
					c.MaxCapicity = (Convert.IsDBNull(row["MaxCapicity"]))?(int)0:(System.Int32?)row["MaxCapicity"];
					c.BookingAlgo = (Convert.IsDBNull(row["BookingAlgo"]))?0.0m:(System.Decimal?)row["BookingAlgo"];
					c.RequestAlgo = (Convert.IsDBNull(row["RequestAlgo"]))?0.0m:(System.Decimal?)row["RequestAlgo"];
					c.IsPriority = (Convert.IsDBNull(row["IsPriority"]))?string.Empty:(System.String)row["IsPriority"];
					c.ActualPkgPrice = (Convert.IsDBNull(row["ActualPkgPrice"]))?0.0m:(System.Decimal?)row["ActualPkgPrice"];
					c.ActualPkgPriceHalfDay = (Convert.IsDBNull(row["ActualPkgPriceHalfDay"]))?0.0m:(System.Decimal?)row["ActualPkgPriceHalfDay"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewSearchForDay2&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewSearchForDay2&gt;"/></returns>
		protected VList<ViewSearchForDay2> Fill(IDataReader reader, VList<ViewSearchForDay2> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewSearchForDay2 entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewSearchForDay2>("ViewSearchForDay2",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewSearchForDay2();
					}
					
					entity.SuppressEntityEvents = true;

					entity.HotelId = (System.Int64)reader[((int)ViewSearchForDay2Column.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
					entity.HotelName = (reader.IsDBNull(((int)ViewSearchForDay2Column.HotelName)))?null:(System.String)reader[((int)ViewSearchForDay2Column.HotelName)];
					//entity.HotelName = (Convert.IsDBNull(reader["HotelName"]))?string.Empty:(System.String)reader["HotelName"];
					entity.CountryId = (System.Int64)reader[((int)ViewSearchForDay2Column.CountryId)];
					//entity.CountryId = (Convert.IsDBNull(reader["CountryId"]))?(long)0:(System.Int64)reader["CountryId"];
					entity.CityId = (System.Int64)reader[((int)ViewSearchForDay2Column.CityId)];
					//entity.CityId = (Convert.IsDBNull(reader["CityId"]))?(long)0:(System.Int64)reader["CityId"];
					entity.ZoneId = (System.Int64)reader[((int)ViewSearchForDay2Column.ZoneId)];
					//entity.ZoneId = (Convert.IsDBNull(reader["ZoneId"]))?(long)0:(System.Int64)reader["ZoneId"];
					entity.Stars = (reader.IsDBNull(((int)ViewSearchForDay2Column.Stars)))?null:(System.Int32?)reader[((int)ViewSearchForDay2Column.Stars)];
					//entity.Stars = (Convert.IsDBNull(reader["Stars"]))?(int)0:(System.Int32?)reader["Stars"];
					entity.Longitude = (reader.IsDBNull(((int)ViewSearchForDay2Column.Longitude)))?null:(System.String)reader[((int)ViewSearchForDay2Column.Longitude)];
					//entity.Longitude = (Convert.IsDBNull(reader["Longitude"]))?string.Empty:(System.String)reader["Longitude"];
					entity.Latitude = (reader.IsDBNull(((int)ViewSearchForDay2Column.Latitude)))?null:(System.String)reader[((int)ViewSearchForDay2Column.Latitude)];
					//entity.Latitude = (Convert.IsDBNull(reader["Latitude"]))?string.Empty:(System.String)reader["Latitude"];
					entity.Logo = (reader.IsDBNull(((int)ViewSearchForDay2Column.Logo)))?null:(System.String)reader[((int)ViewSearchForDay2Column.Logo)];
					//entity.Logo = (Convert.IsDBNull(reader["Logo"]))?string.Empty:(System.String)reader["Logo"];
					entity.IsActive = (System.Boolean)reader[((int)ViewSearchForDay2Column.IsActive)];
					//entity.IsActive = (Convert.IsDBNull(reader["IsActive"]))?false:(System.Boolean)reader["IsActive"];
					entity.CancelationPolicyId = (System.Int64)reader[((int)ViewSearchForDay2Column.CancelationPolicyId)];
					//entity.CancelationPolicyId = (Convert.IsDBNull(reader["CancelationPolicyId"]))?(long)0:(System.Int64)reader["CancelationPolicyId"];
					entity.GroupId = (reader.IsDBNull(((int)ViewSearchForDay2Column.GroupId)))?null:(System.Int64?)reader[((int)ViewSearchForDay2Column.GroupId)];
					//entity.GroupId = (Convert.IsDBNull(reader["GroupId"]))?(long)0:(System.Int64?)reader["GroupId"];
					entity.OperatorChoice = (reader.IsDBNull(((int)ViewSearchForDay2Column.OperatorChoice)))?null:(System.String)reader[((int)ViewSearchForDay2Column.OperatorChoice)];
					//entity.OperatorChoice = (Convert.IsDBNull(reader["OperatorChoice"]))?string.Empty:(System.String)reader["OperatorChoice"];
					entity.HotelPlan = (reader.IsDBNull(((int)ViewSearchForDay2Column.HotelPlan)))?null:(System.String)reader[((int)ViewSearchForDay2Column.HotelPlan)];
					//entity.HotelPlan = (Convert.IsDBNull(reader["HotelPlan"]))?string.Empty:(System.String)reader["HotelPlan"];
					entity.ClientId = (reader.IsDBNull(((int)ViewSearchForDay2Column.ClientId)))?null:(System.Int64?)reader[((int)ViewSearchForDay2Column.ClientId)];
					//entity.ClientId = (Convert.IsDBNull(reader["ClientId"]))?(long)0:(System.Int64?)reader["ClientId"];
					entity.ContractId = (reader.IsDBNull(((int)ViewSearchForDay2Column.ContractId)))?null:(System.String)reader[((int)ViewSearchForDay2Column.ContractId)];
					//entity.ContractId = (Convert.IsDBNull(reader["ContractId"]))?string.Empty:(System.String)reader["ContractId"];
					entity.StaffId = (System.Int64)reader[((int)ViewSearchForDay2Column.StaffId)];
					//entity.StaffId = (Convert.IsDBNull(reader["StaffId"]))?(long)0:(System.Int64)reader["StaffId"];
					entity.CurrencyId = (System.Int64)reader[((int)ViewSearchForDay2Column.CurrencyId)];
					//entity.CurrencyId = (Convert.IsDBNull(reader["CurrencyId"]))?(long)0:(System.Int64)reader["CurrencyId"];
					entity.ContractValue = (reader.IsDBNull(((int)ViewSearchForDay2Column.ContractValue)))?null:(System.Decimal?)reader[((int)ViewSearchForDay2Column.ContractValue)];
					//entity.ContractValue = (Convert.IsDBNull(reader["ContractValue"]))?0.0m:(System.Decimal?)reader["ContractValue"];
					entity.TimeZone = (reader.IsDBNull(((int)ViewSearchForDay2Column.TimeZone)))?null:(System.String)reader[((int)ViewSearchForDay2Column.TimeZone)];
					//entity.TimeZone = (Convert.IsDBNull(reader["TimeZone"]))?string.Empty:(System.String)reader["TimeZone"];
					entity.Phone = (reader.IsDBNull(((int)ViewSearchForDay2Column.Phone)))?null:(System.String)reader[((int)ViewSearchForDay2Column.Phone)];
					//entity.Phone = (Convert.IsDBNull(reader["Phone"]))?string.Empty:(System.String)reader["Phone"];
					entity.PhoneExt = (reader.IsDBNull(((int)ViewSearchForDay2Column.PhoneExt)))?null:(System.String)reader[((int)ViewSearchForDay2Column.PhoneExt)];
					//entity.PhoneExt = (Convert.IsDBNull(reader["PhoneExt"]))?string.Empty:(System.String)reader["PhoneExt"];
					entity.RtMFrom = (reader.IsDBNull(((int)ViewSearchForDay2Column.RtMFrom)))?null:(System.String)reader[((int)ViewSearchForDay2Column.RtMFrom)];
					//entity.RtMFrom = (Convert.IsDBNull(reader["RT_M_From"]))?string.Empty:(System.String)reader["RT_M_From"];
					entity.RtMTo = (reader.IsDBNull(((int)ViewSearchForDay2Column.RtMTo)))?null:(System.String)reader[((int)ViewSearchForDay2Column.RtMTo)];
					//entity.RtMTo = (Convert.IsDBNull(reader["RT_M_To"]))?string.Empty:(System.String)reader["RT_M_To"];
					entity.RtAFrom = (reader.IsDBNull(((int)ViewSearchForDay2Column.RtAFrom)))?null:(System.String)reader[((int)ViewSearchForDay2Column.RtAFrom)];
					//entity.RtAFrom = (Convert.IsDBNull(reader["RT_A_From"]))?string.Empty:(System.String)reader["RT_A_From"];
					entity.RtATo = (reader.IsDBNull(((int)ViewSearchForDay2Column.RtATo)))?null:(System.String)reader[((int)ViewSearchForDay2Column.RtATo)];
					//entity.RtATo = (Convert.IsDBNull(reader["RT_A_To"]))?string.Empty:(System.String)reader["RT_A_To"];
					entity.RtFFrom = (reader.IsDBNull(((int)ViewSearchForDay2Column.RtFFrom)))?null:(System.String)reader[((int)ViewSearchForDay2Column.RtFFrom)];
					//entity.RtFFrom = (Convert.IsDBNull(reader["RT_F_From"]))?string.Empty:(System.String)reader["RT_F_From"];
					entity.RtFTo = (reader.IsDBNull(((int)ViewSearchForDay2Column.RtFTo)))?null:(System.String)reader[((int)ViewSearchForDay2Column.RtFTo)];
					//entity.RtFTo = (Convert.IsDBNull(reader["RT_F_To"]))?string.Empty:(System.String)reader["RT_F_To"];
					entity.IsCreditCardExcepted = (System.Boolean)reader[((int)ViewSearchForDay2Column.IsCreditCardExcepted)];
					//entity.IsCreditCardExcepted = (Convert.IsDBNull(reader["IsCreditCardExcepted"]))?false:(System.Boolean)reader["IsCreditCardExcepted"];
					entity.CreditCardType = (reader.IsDBNull(((int)ViewSearchForDay2Column.CreditCardType)))?null:(System.String)reader[((int)ViewSearchForDay2Column.CreditCardType)];
					//entity.CreditCardType = (Convert.IsDBNull(reader["CreditCardType"]))?string.Empty:(System.String)reader["CreditCardType"];
					entity.Theme = (reader.IsDBNull(((int)ViewSearchForDay2Column.Theme)))?null:(System.String)reader[((int)ViewSearchForDay2Column.Theme)];
					//entity.Theme = (Convert.IsDBNull(reader["Theme"]))?string.Empty:(System.String)reader["Theme"];
					entity.IsBedroomAvailable = (System.Boolean)reader[((int)ViewSearchForDay2Column.IsBedroomAvailable)];
					//entity.IsBedroomAvailable = (Convert.IsDBNull(reader["IsBedroomAvailable"]))?false:(System.Boolean)reader["IsBedroomAvailable"];
					entity.NumberOfBedroom = (reader.IsDBNull(((int)ViewSearchForDay2Column.NumberOfBedroom)))?null:(System.Int32?)reader[((int)ViewSearchForDay2Column.NumberOfBedroom)];
					//entity.NumberOfBedroom = (Convert.IsDBNull(reader["NumberOfBedroom"]))?(int)0:(System.Int32?)reader["NumberOfBedroom"];
					entity.Email = (reader.IsDBNull(((int)ViewSearchForDay2Column.Email)))?null:(System.String)reader[((int)ViewSearchForDay2Column.Email)];
					//entity.Email = (Convert.IsDBNull(reader["Email"]))?string.Empty:(System.String)reader["Email"];
					entity.ContactPerson = (reader.IsDBNull(((int)ViewSearchForDay2Column.ContactPerson)))?null:(System.String)reader[((int)ViewSearchForDay2Column.ContactPerson)];
					//entity.ContactPerson = (Convert.IsDBNull(reader["ContactPerson"]))?string.Empty:(System.String)reader["ContactPerson"];
					entity.PhoneNumber = (reader.IsDBNull(((int)ViewSearchForDay2Column.PhoneNumber)))?null:(System.String)reader[((int)ViewSearchForDay2Column.PhoneNumber)];
					//entity.PhoneNumber = (Convert.IsDBNull(reader["PhoneNumber"]))?string.Empty:(System.String)reader["PhoneNumber"];
					entity.GoOnline = (reader.IsDBNull(((int)ViewSearchForDay2Column.GoOnline)))?null:(System.Boolean?)reader[((int)ViewSearchForDay2Column.GoOnline)];
					//entity.GoOnline = (Convert.IsDBNull(reader["GoOnline"]))?false:(System.Boolean?)reader["GoOnline"];
					entity.ZipCode = (reader.IsDBNull(((int)ViewSearchForDay2Column.ZipCode)))?null:(System.String)reader[((int)ViewSearchForDay2Column.ZipCode)];
					//entity.ZipCode = (Convert.IsDBNull(reader["ZipCode"]))?string.Empty:(System.String)reader["ZipCode"];
					entity.UpdatedBy = (reader.IsDBNull(((int)ViewSearchForDay2Column.UpdatedBy)))?null:(System.Int64?)reader[((int)ViewSearchForDay2Column.UpdatedBy)];
					//entity.UpdatedBy = (Convert.IsDBNull(reader["UpdatedBy"]))?(long)0:(System.Int64?)reader["UpdatedBy"];
					entity.HotelAddress = (reader.IsDBNull(((int)ViewSearchForDay2Column.HotelAddress)))?null:(System.String)reader[((int)ViewSearchForDay2Column.HotelAddress)];
					//entity.HotelAddress = (Convert.IsDBNull(reader["HotelAddress"]))?string.Empty:(System.String)reader["HotelAddress"];
					entity.CreationDate = (System.DateTime)reader[((int)ViewSearchForDay2Column.CreationDate)];
					//entity.CreationDate = (Convert.IsDBNull(reader["CreationDate"]))?DateTime.MinValue:(System.DateTime)reader["CreationDate"];
					entity.CountryCodePhone = (reader.IsDBNull(((int)ViewSearchForDay2Column.CountryCodePhone)))?null:(System.String)reader[((int)ViewSearchForDay2Column.CountryCodePhone)];
					//entity.CountryCodePhone = (Convert.IsDBNull(reader["CountryCodePhone"]))?string.Empty:(System.String)reader["CountryCodePhone"];
					entity.CountryCodeFax = (reader.IsDBNull(((int)ViewSearchForDay2Column.CountryCodeFax)))?null:(System.String)reader[((int)ViewSearchForDay2Column.CountryCodeFax)];
					//entity.CountryCodeFax = (Convert.IsDBNull(reader["CountryCodeFax"]))?string.Empty:(System.String)reader["CountryCodeFax"];
					entity.FaxNumber = (reader.IsDBNull(((int)ViewSearchForDay2Column.FaxNumber)))?null:(System.String)reader[((int)ViewSearchForDay2Column.FaxNumber)];
					//entity.FaxNumber = (Convert.IsDBNull(reader["FaxNumber"]))?string.Empty:(System.String)reader["FaxNumber"];
					entity.IsRemoved = (System.Boolean)reader[((int)ViewSearchForDay2Column.IsRemoved)];
					//entity.IsRemoved = (Convert.IsDBNull(reader["IsRemoved"]))?false:(System.Boolean)reader["IsRemoved"];
					entity.RequestGoOnline = (reader.IsDBNull(((int)ViewSearchForDay2Column.RequestGoOnline)))?null:(System.Boolean?)reader[((int)ViewSearchForDay2Column.RequestGoOnline)];
					//entity.RequestGoOnline = (Convert.IsDBNull(reader["RequestGoOnline"]))?false:(System.Boolean?)reader["RequestGoOnline"];
					entity.HotelUserId = (reader.IsDBNull(((int)ViewSearchForDay2Column.HotelUserId)))?null:(System.Int64?)reader[((int)ViewSearchForDay2Column.HotelUserId)];
					//entity.HotelUserId = (Convert.IsDBNull(reader["HotelUserID"]))?(long)0:(System.Int64?)reader["HotelUserID"];
					entity.AccountingOfficer = (reader.IsDBNull(((int)ViewSearchForDay2Column.AccountingOfficer)))?null:(System.String)reader[((int)ViewSearchForDay2Column.AccountingOfficer)];
					//entity.AccountingOfficer = (Convert.IsDBNull(reader["AccountingOfficer"]))?string.Empty:(System.String)reader["AccountingOfficer"];
					entity.MeetingRoomId = (System.Int64)reader[((int)ViewSearchForDay2Column.MeetingRoomId)];
					//entity.MeetingRoomId = (Convert.IsDBNull(reader["MeetingRoomId"]))?(long)0:(System.Int64)reader["MeetingRoomId"];
					entity.MeetingRoomName = (reader.IsDBNull(((int)ViewSearchForDay2Column.MeetingRoomName)))?null:(System.String)reader[((int)ViewSearchForDay2Column.MeetingRoomName)];
					//entity.MeetingRoomName = (Convert.IsDBNull(reader["MeetingRoomName"]))?string.Empty:(System.String)reader["MeetingRoomName"];
					entity.Picture = (reader.IsDBNull(((int)ViewSearchForDay2Column.Picture)))?null:(System.String)reader[((int)ViewSearchForDay2Column.Picture)];
					//entity.Picture = (Convert.IsDBNull(reader["Picture"]))?string.Empty:(System.String)reader["Picture"];
					entity.DayLight = (System.Int32)reader[((int)ViewSearchForDay2Column.DayLight)];
					//entity.DayLight = (Convert.IsDBNull(reader["DayLight"]))?(int)0:(System.Int32)reader["DayLight"];
					entity.Surface = (reader.IsDBNull(((int)ViewSearchForDay2Column.Surface)))?null:(System.Int64?)reader[((int)ViewSearchForDay2Column.Surface)];
					//entity.Surface = (Convert.IsDBNull(reader["Surface"]))?(long)0:(System.Int64?)reader["Surface"];
					entity.Height = (reader.IsDBNull(((int)ViewSearchForDay2Column.Height)))?null:(System.Decimal?)reader[((int)ViewSearchForDay2Column.Height)];
					//entity.Height = (Convert.IsDBNull(reader["Height"]))?0.0m:(System.Decimal?)reader["Height"];
					entity.MeetingRoomActive = (System.Boolean)reader[((int)ViewSearchForDay2Column.MeetingRoomActive)];
					//entity.MeetingRoomActive = (Convert.IsDBNull(reader["MeetingRoomActive"]))?false:(System.Boolean)reader["MeetingRoomActive"];
					entity.MrPlan = (reader.IsDBNull(((int)ViewSearchForDay2Column.MrPlan)))?null:(System.String)reader[((int)ViewSearchForDay2Column.MrPlan)];
					//entity.MrPlan = (Convert.IsDBNull(reader["MR_Plan"]))?string.Empty:(System.String)reader["MR_Plan"];
					entity.OrderNumber = (reader.IsDBNull(((int)ViewSearchForDay2Column.OrderNumber)))?null:(System.Int32?)reader[((int)ViewSearchForDay2Column.OrderNumber)];
					//entity.OrderNumber = (Convert.IsDBNull(reader["OrderNumber"]))?(int)0:(System.Int32?)reader["OrderNumber"];
					entity.HalfdayPrice = (reader.IsDBNull(((int)ViewSearchForDay2Column.HalfdayPrice)))?null:(System.Decimal?)reader[((int)ViewSearchForDay2Column.HalfdayPrice)];
					//entity.HalfdayPrice = (Convert.IsDBNull(reader["HalfdayPrice"]))?0.0m:(System.Decimal?)reader["HalfdayPrice"];
					entity.FulldayPrice = (reader.IsDBNull(((int)ViewSearchForDay2Column.FulldayPrice)))?null:(System.Decimal?)reader[((int)ViewSearchForDay2Column.FulldayPrice)];
					//entity.FulldayPrice = (Convert.IsDBNull(reader["FulldayPrice"]))?0.0m:(System.Decimal?)reader["FulldayPrice"];
					entity.IsOnline = (System.Boolean)reader[((int)ViewSearchForDay2Column.IsOnline)];
					//entity.IsOnline = (Convert.IsDBNull(reader["IsOnline"]))?false:(System.Boolean)reader["IsOnline"];
					entity.IsDeleted = (System.Boolean)reader[((int)ViewSearchForDay2Column.IsDeleted)];
					//entity.IsDeleted = (Convert.IsDBNull(reader["IsDeleted"]))?false:(System.Boolean)reader["IsDeleted"];
					entity.RoomType = (reader.IsDBNull(((int)ViewSearchForDay2Column.RoomType)))?null:(System.Int32?)reader[((int)ViewSearchForDay2Column.RoomType)];
					//entity.RoomType = (Convert.IsDBNull(reader["RoomType"]))?(int)0:(System.Int32?)reader["RoomType"];
					entity.MeetingRoomConfigId = (System.Int64)reader[((int)ViewSearchForDay2Column.MeetingRoomConfigId)];
					//entity.MeetingRoomConfigId = (Convert.IsDBNull(reader["MeetingRoomConfigId"]))?(long)0:(System.Int64)reader["MeetingRoomConfigId"];
					entity.RoomShapeId = (reader.IsDBNull(((int)ViewSearchForDay2Column.RoomShapeId)))?null:(System.Int32?)reader[((int)ViewSearchForDay2Column.RoomShapeId)];
					//entity.RoomShapeId = (Convert.IsDBNull(reader["RoomShapeId"]))?(int)0:(System.Int32?)reader["RoomShapeId"];
					entity.MinCapacity = (reader.IsDBNull(((int)ViewSearchForDay2Column.MinCapacity)))?null:(System.Int32?)reader[((int)ViewSearchForDay2Column.MinCapacity)];
					//entity.MinCapacity = (Convert.IsDBNull(reader["MinCapacity"]))?(int)0:(System.Int32?)reader["MinCapacity"];
					entity.MaxCapicity = (reader.IsDBNull(((int)ViewSearchForDay2Column.MaxCapicity)))?null:(System.Int32?)reader[((int)ViewSearchForDay2Column.MaxCapicity)];
					//entity.MaxCapicity = (Convert.IsDBNull(reader["MaxCapicity"]))?(int)0:(System.Int32?)reader["MaxCapicity"];
					entity.BookingAlgo = (reader.IsDBNull(((int)ViewSearchForDay2Column.BookingAlgo)))?null:(System.Decimal?)reader[((int)ViewSearchForDay2Column.BookingAlgo)];
					//entity.BookingAlgo = (Convert.IsDBNull(reader["BookingAlgo"]))?0.0m:(System.Decimal?)reader["BookingAlgo"];
					entity.RequestAlgo = (reader.IsDBNull(((int)ViewSearchForDay2Column.RequestAlgo)))?null:(System.Decimal?)reader[((int)ViewSearchForDay2Column.RequestAlgo)];
					//entity.RequestAlgo = (Convert.IsDBNull(reader["RequestAlgo"]))?0.0m:(System.Decimal?)reader["RequestAlgo"];
					entity.IsPriority = (System.String)reader[((int)ViewSearchForDay2Column.IsPriority)];
					//entity.IsPriority = (Convert.IsDBNull(reader["IsPriority"]))?string.Empty:(System.String)reader["IsPriority"];
					entity.ActualPkgPrice = (reader.IsDBNull(((int)ViewSearchForDay2Column.ActualPkgPrice)))?null:(System.Decimal?)reader[((int)ViewSearchForDay2Column.ActualPkgPrice)];
					//entity.ActualPkgPrice = (Convert.IsDBNull(reader["ActualPkgPrice"]))?0.0m:(System.Decimal?)reader["ActualPkgPrice"];
					entity.ActualPkgPriceHalfDay = (reader.IsDBNull(((int)ViewSearchForDay2Column.ActualPkgPriceHalfDay)))?null:(System.Decimal?)reader[((int)ViewSearchForDay2Column.ActualPkgPriceHalfDay)];
					//entity.ActualPkgPriceHalfDay = (Convert.IsDBNull(reader["ActualPkgPriceHalfDay"]))?0.0m:(System.Decimal?)reader["ActualPkgPriceHalfDay"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewSearchForDay2"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewSearchForDay2"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewSearchForDay2 entity)
		{
			reader.Read();
			entity.HotelId = (System.Int64)reader[((int)ViewSearchForDay2Column.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
			entity.HotelName = (reader.IsDBNull(((int)ViewSearchForDay2Column.HotelName)))?null:(System.String)reader[((int)ViewSearchForDay2Column.HotelName)];
			//entity.HotelName = (Convert.IsDBNull(reader["HotelName"]))?string.Empty:(System.String)reader["HotelName"];
			entity.CountryId = (System.Int64)reader[((int)ViewSearchForDay2Column.CountryId)];
			//entity.CountryId = (Convert.IsDBNull(reader["CountryId"]))?(long)0:(System.Int64)reader["CountryId"];
			entity.CityId = (System.Int64)reader[((int)ViewSearchForDay2Column.CityId)];
			//entity.CityId = (Convert.IsDBNull(reader["CityId"]))?(long)0:(System.Int64)reader["CityId"];
			entity.ZoneId = (System.Int64)reader[((int)ViewSearchForDay2Column.ZoneId)];
			//entity.ZoneId = (Convert.IsDBNull(reader["ZoneId"]))?(long)0:(System.Int64)reader["ZoneId"];
			entity.Stars = (reader.IsDBNull(((int)ViewSearchForDay2Column.Stars)))?null:(System.Int32?)reader[((int)ViewSearchForDay2Column.Stars)];
			//entity.Stars = (Convert.IsDBNull(reader["Stars"]))?(int)0:(System.Int32?)reader["Stars"];
			entity.Longitude = (reader.IsDBNull(((int)ViewSearchForDay2Column.Longitude)))?null:(System.String)reader[((int)ViewSearchForDay2Column.Longitude)];
			//entity.Longitude = (Convert.IsDBNull(reader["Longitude"]))?string.Empty:(System.String)reader["Longitude"];
			entity.Latitude = (reader.IsDBNull(((int)ViewSearchForDay2Column.Latitude)))?null:(System.String)reader[((int)ViewSearchForDay2Column.Latitude)];
			//entity.Latitude = (Convert.IsDBNull(reader["Latitude"]))?string.Empty:(System.String)reader["Latitude"];
			entity.Logo = (reader.IsDBNull(((int)ViewSearchForDay2Column.Logo)))?null:(System.String)reader[((int)ViewSearchForDay2Column.Logo)];
			//entity.Logo = (Convert.IsDBNull(reader["Logo"]))?string.Empty:(System.String)reader["Logo"];
			entity.IsActive = (System.Boolean)reader[((int)ViewSearchForDay2Column.IsActive)];
			//entity.IsActive = (Convert.IsDBNull(reader["IsActive"]))?false:(System.Boolean)reader["IsActive"];
			entity.CancelationPolicyId = (System.Int64)reader[((int)ViewSearchForDay2Column.CancelationPolicyId)];
			//entity.CancelationPolicyId = (Convert.IsDBNull(reader["CancelationPolicyId"]))?(long)0:(System.Int64)reader["CancelationPolicyId"];
			entity.GroupId = (reader.IsDBNull(((int)ViewSearchForDay2Column.GroupId)))?null:(System.Int64?)reader[((int)ViewSearchForDay2Column.GroupId)];
			//entity.GroupId = (Convert.IsDBNull(reader["GroupId"]))?(long)0:(System.Int64?)reader["GroupId"];
			entity.OperatorChoice = (reader.IsDBNull(((int)ViewSearchForDay2Column.OperatorChoice)))?null:(System.String)reader[((int)ViewSearchForDay2Column.OperatorChoice)];
			//entity.OperatorChoice = (Convert.IsDBNull(reader["OperatorChoice"]))?string.Empty:(System.String)reader["OperatorChoice"];
			entity.HotelPlan = (reader.IsDBNull(((int)ViewSearchForDay2Column.HotelPlan)))?null:(System.String)reader[((int)ViewSearchForDay2Column.HotelPlan)];
			//entity.HotelPlan = (Convert.IsDBNull(reader["HotelPlan"]))?string.Empty:(System.String)reader["HotelPlan"];
			entity.ClientId = (reader.IsDBNull(((int)ViewSearchForDay2Column.ClientId)))?null:(System.Int64?)reader[((int)ViewSearchForDay2Column.ClientId)];
			//entity.ClientId = (Convert.IsDBNull(reader["ClientId"]))?(long)0:(System.Int64?)reader["ClientId"];
			entity.ContractId = (reader.IsDBNull(((int)ViewSearchForDay2Column.ContractId)))?null:(System.String)reader[((int)ViewSearchForDay2Column.ContractId)];
			//entity.ContractId = (Convert.IsDBNull(reader["ContractId"]))?string.Empty:(System.String)reader["ContractId"];
			entity.StaffId = (System.Int64)reader[((int)ViewSearchForDay2Column.StaffId)];
			//entity.StaffId = (Convert.IsDBNull(reader["StaffId"]))?(long)0:(System.Int64)reader["StaffId"];
			entity.CurrencyId = (System.Int64)reader[((int)ViewSearchForDay2Column.CurrencyId)];
			//entity.CurrencyId = (Convert.IsDBNull(reader["CurrencyId"]))?(long)0:(System.Int64)reader["CurrencyId"];
			entity.ContractValue = (reader.IsDBNull(((int)ViewSearchForDay2Column.ContractValue)))?null:(System.Decimal?)reader[((int)ViewSearchForDay2Column.ContractValue)];
			//entity.ContractValue = (Convert.IsDBNull(reader["ContractValue"]))?0.0m:(System.Decimal?)reader["ContractValue"];
			entity.TimeZone = (reader.IsDBNull(((int)ViewSearchForDay2Column.TimeZone)))?null:(System.String)reader[((int)ViewSearchForDay2Column.TimeZone)];
			//entity.TimeZone = (Convert.IsDBNull(reader["TimeZone"]))?string.Empty:(System.String)reader["TimeZone"];
			entity.Phone = (reader.IsDBNull(((int)ViewSearchForDay2Column.Phone)))?null:(System.String)reader[((int)ViewSearchForDay2Column.Phone)];
			//entity.Phone = (Convert.IsDBNull(reader["Phone"]))?string.Empty:(System.String)reader["Phone"];
			entity.PhoneExt = (reader.IsDBNull(((int)ViewSearchForDay2Column.PhoneExt)))?null:(System.String)reader[((int)ViewSearchForDay2Column.PhoneExt)];
			//entity.PhoneExt = (Convert.IsDBNull(reader["PhoneExt"]))?string.Empty:(System.String)reader["PhoneExt"];
			entity.RtMFrom = (reader.IsDBNull(((int)ViewSearchForDay2Column.RtMFrom)))?null:(System.String)reader[((int)ViewSearchForDay2Column.RtMFrom)];
			//entity.RtMFrom = (Convert.IsDBNull(reader["RT_M_From"]))?string.Empty:(System.String)reader["RT_M_From"];
			entity.RtMTo = (reader.IsDBNull(((int)ViewSearchForDay2Column.RtMTo)))?null:(System.String)reader[((int)ViewSearchForDay2Column.RtMTo)];
			//entity.RtMTo = (Convert.IsDBNull(reader["RT_M_To"]))?string.Empty:(System.String)reader["RT_M_To"];
			entity.RtAFrom = (reader.IsDBNull(((int)ViewSearchForDay2Column.RtAFrom)))?null:(System.String)reader[((int)ViewSearchForDay2Column.RtAFrom)];
			//entity.RtAFrom = (Convert.IsDBNull(reader["RT_A_From"]))?string.Empty:(System.String)reader["RT_A_From"];
			entity.RtATo = (reader.IsDBNull(((int)ViewSearchForDay2Column.RtATo)))?null:(System.String)reader[((int)ViewSearchForDay2Column.RtATo)];
			//entity.RtATo = (Convert.IsDBNull(reader["RT_A_To"]))?string.Empty:(System.String)reader["RT_A_To"];
			entity.RtFFrom = (reader.IsDBNull(((int)ViewSearchForDay2Column.RtFFrom)))?null:(System.String)reader[((int)ViewSearchForDay2Column.RtFFrom)];
			//entity.RtFFrom = (Convert.IsDBNull(reader["RT_F_From"]))?string.Empty:(System.String)reader["RT_F_From"];
			entity.RtFTo = (reader.IsDBNull(((int)ViewSearchForDay2Column.RtFTo)))?null:(System.String)reader[((int)ViewSearchForDay2Column.RtFTo)];
			//entity.RtFTo = (Convert.IsDBNull(reader["RT_F_To"]))?string.Empty:(System.String)reader["RT_F_To"];
			entity.IsCreditCardExcepted = (System.Boolean)reader[((int)ViewSearchForDay2Column.IsCreditCardExcepted)];
			//entity.IsCreditCardExcepted = (Convert.IsDBNull(reader["IsCreditCardExcepted"]))?false:(System.Boolean)reader["IsCreditCardExcepted"];
			entity.CreditCardType = (reader.IsDBNull(((int)ViewSearchForDay2Column.CreditCardType)))?null:(System.String)reader[((int)ViewSearchForDay2Column.CreditCardType)];
			//entity.CreditCardType = (Convert.IsDBNull(reader["CreditCardType"]))?string.Empty:(System.String)reader["CreditCardType"];
			entity.Theme = (reader.IsDBNull(((int)ViewSearchForDay2Column.Theme)))?null:(System.String)reader[((int)ViewSearchForDay2Column.Theme)];
			//entity.Theme = (Convert.IsDBNull(reader["Theme"]))?string.Empty:(System.String)reader["Theme"];
			entity.IsBedroomAvailable = (System.Boolean)reader[((int)ViewSearchForDay2Column.IsBedroomAvailable)];
			//entity.IsBedroomAvailable = (Convert.IsDBNull(reader["IsBedroomAvailable"]))?false:(System.Boolean)reader["IsBedroomAvailable"];
			entity.NumberOfBedroom = (reader.IsDBNull(((int)ViewSearchForDay2Column.NumberOfBedroom)))?null:(System.Int32?)reader[((int)ViewSearchForDay2Column.NumberOfBedroom)];
			//entity.NumberOfBedroom = (Convert.IsDBNull(reader["NumberOfBedroom"]))?(int)0:(System.Int32?)reader["NumberOfBedroom"];
			entity.Email = (reader.IsDBNull(((int)ViewSearchForDay2Column.Email)))?null:(System.String)reader[((int)ViewSearchForDay2Column.Email)];
			//entity.Email = (Convert.IsDBNull(reader["Email"]))?string.Empty:(System.String)reader["Email"];
			entity.ContactPerson = (reader.IsDBNull(((int)ViewSearchForDay2Column.ContactPerson)))?null:(System.String)reader[((int)ViewSearchForDay2Column.ContactPerson)];
			//entity.ContactPerson = (Convert.IsDBNull(reader["ContactPerson"]))?string.Empty:(System.String)reader["ContactPerson"];
			entity.PhoneNumber = (reader.IsDBNull(((int)ViewSearchForDay2Column.PhoneNumber)))?null:(System.String)reader[((int)ViewSearchForDay2Column.PhoneNumber)];
			//entity.PhoneNumber = (Convert.IsDBNull(reader["PhoneNumber"]))?string.Empty:(System.String)reader["PhoneNumber"];
			entity.GoOnline = (reader.IsDBNull(((int)ViewSearchForDay2Column.GoOnline)))?null:(System.Boolean?)reader[((int)ViewSearchForDay2Column.GoOnline)];
			//entity.GoOnline = (Convert.IsDBNull(reader["GoOnline"]))?false:(System.Boolean?)reader["GoOnline"];
			entity.ZipCode = (reader.IsDBNull(((int)ViewSearchForDay2Column.ZipCode)))?null:(System.String)reader[((int)ViewSearchForDay2Column.ZipCode)];
			//entity.ZipCode = (Convert.IsDBNull(reader["ZipCode"]))?string.Empty:(System.String)reader["ZipCode"];
			entity.UpdatedBy = (reader.IsDBNull(((int)ViewSearchForDay2Column.UpdatedBy)))?null:(System.Int64?)reader[((int)ViewSearchForDay2Column.UpdatedBy)];
			//entity.UpdatedBy = (Convert.IsDBNull(reader["UpdatedBy"]))?(long)0:(System.Int64?)reader["UpdatedBy"];
			entity.HotelAddress = (reader.IsDBNull(((int)ViewSearchForDay2Column.HotelAddress)))?null:(System.String)reader[((int)ViewSearchForDay2Column.HotelAddress)];
			//entity.HotelAddress = (Convert.IsDBNull(reader["HotelAddress"]))?string.Empty:(System.String)reader["HotelAddress"];
			entity.CreationDate = (System.DateTime)reader[((int)ViewSearchForDay2Column.CreationDate)];
			//entity.CreationDate = (Convert.IsDBNull(reader["CreationDate"]))?DateTime.MinValue:(System.DateTime)reader["CreationDate"];
			entity.CountryCodePhone = (reader.IsDBNull(((int)ViewSearchForDay2Column.CountryCodePhone)))?null:(System.String)reader[((int)ViewSearchForDay2Column.CountryCodePhone)];
			//entity.CountryCodePhone = (Convert.IsDBNull(reader["CountryCodePhone"]))?string.Empty:(System.String)reader["CountryCodePhone"];
			entity.CountryCodeFax = (reader.IsDBNull(((int)ViewSearchForDay2Column.CountryCodeFax)))?null:(System.String)reader[((int)ViewSearchForDay2Column.CountryCodeFax)];
			//entity.CountryCodeFax = (Convert.IsDBNull(reader["CountryCodeFax"]))?string.Empty:(System.String)reader["CountryCodeFax"];
			entity.FaxNumber = (reader.IsDBNull(((int)ViewSearchForDay2Column.FaxNumber)))?null:(System.String)reader[((int)ViewSearchForDay2Column.FaxNumber)];
			//entity.FaxNumber = (Convert.IsDBNull(reader["FaxNumber"]))?string.Empty:(System.String)reader["FaxNumber"];
			entity.IsRemoved = (System.Boolean)reader[((int)ViewSearchForDay2Column.IsRemoved)];
			//entity.IsRemoved = (Convert.IsDBNull(reader["IsRemoved"]))?false:(System.Boolean)reader["IsRemoved"];
			entity.RequestGoOnline = (reader.IsDBNull(((int)ViewSearchForDay2Column.RequestGoOnline)))?null:(System.Boolean?)reader[((int)ViewSearchForDay2Column.RequestGoOnline)];
			//entity.RequestGoOnline = (Convert.IsDBNull(reader["RequestGoOnline"]))?false:(System.Boolean?)reader["RequestGoOnline"];
			entity.HotelUserId = (reader.IsDBNull(((int)ViewSearchForDay2Column.HotelUserId)))?null:(System.Int64?)reader[((int)ViewSearchForDay2Column.HotelUserId)];
			//entity.HotelUserId = (Convert.IsDBNull(reader["HotelUserID"]))?(long)0:(System.Int64?)reader["HotelUserID"];
			entity.AccountingOfficer = (reader.IsDBNull(((int)ViewSearchForDay2Column.AccountingOfficer)))?null:(System.String)reader[((int)ViewSearchForDay2Column.AccountingOfficer)];
			//entity.AccountingOfficer = (Convert.IsDBNull(reader["AccountingOfficer"]))?string.Empty:(System.String)reader["AccountingOfficer"];
			entity.MeetingRoomId = (System.Int64)reader[((int)ViewSearchForDay2Column.MeetingRoomId)];
			//entity.MeetingRoomId = (Convert.IsDBNull(reader["MeetingRoomId"]))?(long)0:(System.Int64)reader["MeetingRoomId"];
			entity.MeetingRoomName = (reader.IsDBNull(((int)ViewSearchForDay2Column.MeetingRoomName)))?null:(System.String)reader[((int)ViewSearchForDay2Column.MeetingRoomName)];
			//entity.MeetingRoomName = (Convert.IsDBNull(reader["MeetingRoomName"]))?string.Empty:(System.String)reader["MeetingRoomName"];
			entity.Picture = (reader.IsDBNull(((int)ViewSearchForDay2Column.Picture)))?null:(System.String)reader[((int)ViewSearchForDay2Column.Picture)];
			//entity.Picture = (Convert.IsDBNull(reader["Picture"]))?string.Empty:(System.String)reader["Picture"];
			entity.DayLight = (System.Int32)reader[((int)ViewSearchForDay2Column.DayLight)];
			//entity.DayLight = (Convert.IsDBNull(reader["DayLight"]))?(int)0:(System.Int32)reader["DayLight"];
			entity.Surface = (reader.IsDBNull(((int)ViewSearchForDay2Column.Surface)))?null:(System.Int64?)reader[((int)ViewSearchForDay2Column.Surface)];
			//entity.Surface = (Convert.IsDBNull(reader["Surface"]))?(long)0:(System.Int64?)reader["Surface"];
			entity.Height = (reader.IsDBNull(((int)ViewSearchForDay2Column.Height)))?null:(System.Decimal?)reader[((int)ViewSearchForDay2Column.Height)];
			//entity.Height = (Convert.IsDBNull(reader["Height"]))?0.0m:(System.Decimal?)reader["Height"];
			entity.MeetingRoomActive = (System.Boolean)reader[((int)ViewSearchForDay2Column.MeetingRoomActive)];
			//entity.MeetingRoomActive = (Convert.IsDBNull(reader["MeetingRoomActive"]))?false:(System.Boolean)reader["MeetingRoomActive"];
			entity.MrPlan = (reader.IsDBNull(((int)ViewSearchForDay2Column.MrPlan)))?null:(System.String)reader[((int)ViewSearchForDay2Column.MrPlan)];
			//entity.MrPlan = (Convert.IsDBNull(reader["MR_Plan"]))?string.Empty:(System.String)reader["MR_Plan"];
			entity.OrderNumber = (reader.IsDBNull(((int)ViewSearchForDay2Column.OrderNumber)))?null:(System.Int32?)reader[((int)ViewSearchForDay2Column.OrderNumber)];
			//entity.OrderNumber = (Convert.IsDBNull(reader["OrderNumber"]))?(int)0:(System.Int32?)reader["OrderNumber"];
			entity.HalfdayPrice = (reader.IsDBNull(((int)ViewSearchForDay2Column.HalfdayPrice)))?null:(System.Decimal?)reader[((int)ViewSearchForDay2Column.HalfdayPrice)];
			//entity.HalfdayPrice = (Convert.IsDBNull(reader["HalfdayPrice"]))?0.0m:(System.Decimal?)reader["HalfdayPrice"];
			entity.FulldayPrice = (reader.IsDBNull(((int)ViewSearchForDay2Column.FulldayPrice)))?null:(System.Decimal?)reader[((int)ViewSearchForDay2Column.FulldayPrice)];
			//entity.FulldayPrice = (Convert.IsDBNull(reader["FulldayPrice"]))?0.0m:(System.Decimal?)reader["FulldayPrice"];
			entity.IsOnline = (System.Boolean)reader[((int)ViewSearchForDay2Column.IsOnline)];
			//entity.IsOnline = (Convert.IsDBNull(reader["IsOnline"]))?false:(System.Boolean)reader["IsOnline"];
			entity.IsDeleted = (System.Boolean)reader[((int)ViewSearchForDay2Column.IsDeleted)];
			//entity.IsDeleted = (Convert.IsDBNull(reader["IsDeleted"]))?false:(System.Boolean)reader["IsDeleted"];
			entity.RoomType = (reader.IsDBNull(((int)ViewSearchForDay2Column.RoomType)))?null:(System.Int32?)reader[((int)ViewSearchForDay2Column.RoomType)];
			//entity.RoomType = (Convert.IsDBNull(reader["RoomType"]))?(int)0:(System.Int32?)reader["RoomType"];
			entity.MeetingRoomConfigId = (System.Int64)reader[((int)ViewSearchForDay2Column.MeetingRoomConfigId)];
			//entity.MeetingRoomConfigId = (Convert.IsDBNull(reader["MeetingRoomConfigId"]))?(long)0:(System.Int64)reader["MeetingRoomConfigId"];
			entity.RoomShapeId = (reader.IsDBNull(((int)ViewSearchForDay2Column.RoomShapeId)))?null:(System.Int32?)reader[((int)ViewSearchForDay2Column.RoomShapeId)];
			//entity.RoomShapeId = (Convert.IsDBNull(reader["RoomShapeId"]))?(int)0:(System.Int32?)reader["RoomShapeId"];
			entity.MinCapacity = (reader.IsDBNull(((int)ViewSearchForDay2Column.MinCapacity)))?null:(System.Int32?)reader[((int)ViewSearchForDay2Column.MinCapacity)];
			//entity.MinCapacity = (Convert.IsDBNull(reader["MinCapacity"]))?(int)0:(System.Int32?)reader["MinCapacity"];
			entity.MaxCapicity = (reader.IsDBNull(((int)ViewSearchForDay2Column.MaxCapicity)))?null:(System.Int32?)reader[((int)ViewSearchForDay2Column.MaxCapicity)];
			//entity.MaxCapicity = (Convert.IsDBNull(reader["MaxCapicity"]))?(int)0:(System.Int32?)reader["MaxCapicity"];
			entity.BookingAlgo = (reader.IsDBNull(((int)ViewSearchForDay2Column.BookingAlgo)))?null:(System.Decimal?)reader[((int)ViewSearchForDay2Column.BookingAlgo)];
			//entity.BookingAlgo = (Convert.IsDBNull(reader["BookingAlgo"]))?0.0m:(System.Decimal?)reader["BookingAlgo"];
			entity.RequestAlgo = (reader.IsDBNull(((int)ViewSearchForDay2Column.RequestAlgo)))?null:(System.Decimal?)reader[((int)ViewSearchForDay2Column.RequestAlgo)];
			//entity.RequestAlgo = (Convert.IsDBNull(reader["RequestAlgo"]))?0.0m:(System.Decimal?)reader["RequestAlgo"];
			entity.IsPriority = (System.String)reader[((int)ViewSearchForDay2Column.IsPriority)];
			//entity.IsPriority = (Convert.IsDBNull(reader["IsPriority"]))?string.Empty:(System.String)reader["IsPriority"];
			entity.ActualPkgPrice = (reader.IsDBNull(((int)ViewSearchForDay2Column.ActualPkgPrice)))?null:(System.Decimal?)reader[((int)ViewSearchForDay2Column.ActualPkgPrice)];
			//entity.ActualPkgPrice = (Convert.IsDBNull(reader["ActualPkgPrice"]))?0.0m:(System.Decimal?)reader["ActualPkgPrice"];
			entity.ActualPkgPriceHalfDay = (reader.IsDBNull(((int)ViewSearchForDay2Column.ActualPkgPriceHalfDay)))?null:(System.Decimal?)reader[((int)ViewSearchForDay2Column.ActualPkgPriceHalfDay)];
			//entity.ActualPkgPriceHalfDay = (Convert.IsDBNull(reader["ActualPkgPriceHalfDay"]))?0.0m:(System.Decimal?)reader["ActualPkgPriceHalfDay"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewSearchForDay2"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewSearchForDay2"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewSearchForDay2 entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.HotelId = (Convert.IsDBNull(dataRow["HotelId"]))?(long)0:(System.Int64)dataRow["HotelId"];
			entity.HotelName = (Convert.IsDBNull(dataRow["HotelName"]))?string.Empty:(System.String)dataRow["HotelName"];
			entity.CountryId = (Convert.IsDBNull(dataRow["CountryId"]))?(long)0:(System.Int64)dataRow["CountryId"];
			entity.CityId = (Convert.IsDBNull(dataRow["CityId"]))?(long)0:(System.Int64)dataRow["CityId"];
			entity.ZoneId = (Convert.IsDBNull(dataRow["ZoneId"]))?(long)0:(System.Int64)dataRow["ZoneId"];
			entity.Stars = (Convert.IsDBNull(dataRow["Stars"]))?(int)0:(System.Int32?)dataRow["Stars"];
			entity.Longitude = (Convert.IsDBNull(dataRow["Longitude"]))?string.Empty:(System.String)dataRow["Longitude"];
			entity.Latitude = (Convert.IsDBNull(dataRow["Latitude"]))?string.Empty:(System.String)dataRow["Latitude"];
			entity.Logo = (Convert.IsDBNull(dataRow["Logo"]))?string.Empty:(System.String)dataRow["Logo"];
			entity.IsActive = (Convert.IsDBNull(dataRow["IsActive"]))?false:(System.Boolean)dataRow["IsActive"];
			entity.CancelationPolicyId = (Convert.IsDBNull(dataRow["CancelationPolicyId"]))?(long)0:(System.Int64)dataRow["CancelationPolicyId"];
			entity.GroupId = (Convert.IsDBNull(dataRow["GroupId"]))?(long)0:(System.Int64?)dataRow["GroupId"];
			entity.OperatorChoice = (Convert.IsDBNull(dataRow["OperatorChoice"]))?string.Empty:(System.String)dataRow["OperatorChoice"];
			entity.HotelPlan = (Convert.IsDBNull(dataRow["HotelPlan"]))?string.Empty:(System.String)dataRow["HotelPlan"];
			entity.ClientId = (Convert.IsDBNull(dataRow["ClientId"]))?(long)0:(System.Int64?)dataRow["ClientId"];
			entity.ContractId = (Convert.IsDBNull(dataRow["ContractId"]))?string.Empty:(System.String)dataRow["ContractId"];
			entity.StaffId = (Convert.IsDBNull(dataRow["StaffId"]))?(long)0:(System.Int64)dataRow["StaffId"];
			entity.CurrencyId = (Convert.IsDBNull(dataRow["CurrencyId"]))?(long)0:(System.Int64)dataRow["CurrencyId"];
			entity.ContractValue = (Convert.IsDBNull(dataRow["ContractValue"]))?0.0m:(System.Decimal?)dataRow["ContractValue"];
			entity.TimeZone = (Convert.IsDBNull(dataRow["TimeZone"]))?string.Empty:(System.String)dataRow["TimeZone"];
			entity.Phone = (Convert.IsDBNull(dataRow["Phone"]))?string.Empty:(System.String)dataRow["Phone"];
			entity.PhoneExt = (Convert.IsDBNull(dataRow["PhoneExt"]))?string.Empty:(System.String)dataRow["PhoneExt"];
			entity.RtMFrom = (Convert.IsDBNull(dataRow["RT_M_From"]))?string.Empty:(System.String)dataRow["RT_M_From"];
			entity.RtMTo = (Convert.IsDBNull(dataRow["RT_M_To"]))?string.Empty:(System.String)dataRow["RT_M_To"];
			entity.RtAFrom = (Convert.IsDBNull(dataRow["RT_A_From"]))?string.Empty:(System.String)dataRow["RT_A_From"];
			entity.RtATo = (Convert.IsDBNull(dataRow["RT_A_To"]))?string.Empty:(System.String)dataRow["RT_A_To"];
			entity.RtFFrom = (Convert.IsDBNull(dataRow["RT_F_From"]))?string.Empty:(System.String)dataRow["RT_F_From"];
			entity.RtFTo = (Convert.IsDBNull(dataRow["RT_F_To"]))?string.Empty:(System.String)dataRow["RT_F_To"];
			entity.IsCreditCardExcepted = (Convert.IsDBNull(dataRow["IsCreditCardExcepted"]))?false:(System.Boolean)dataRow["IsCreditCardExcepted"];
			entity.CreditCardType = (Convert.IsDBNull(dataRow["CreditCardType"]))?string.Empty:(System.String)dataRow["CreditCardType"];
			entity.Theme = (Convert.IsDBNull(dataRow["Theme"]))?string.Empty:(System.String)dataRow["Theme"];
			entity.IsBedroomAvailable = (Convert.IsDBNull(dataRow["IsBedroomAvailable"]))?false:(System.Boolean)dataRow["IsBedroomAvailable"];
			entity.NumberOfBedroom = (Convert.IsDBNull(dataRow["NumberOfBedroom"]))?(int)0:(System.Int32?)dataRow["NumberOfBedroom"];
			entity.Email = (Convert.IsDBNull(dataRow["Email"]))?string.Empty:(System.String)dataRow["Email"];
			entity.ContactPerson = (Convert.IsDBNull(dataRow["ContactPerson"]))?string.Empty:(System.String)dataRow["ContactPerson"];
			entity.PhoneNumber = (Convert.IsDBNull(dataRow["PhoneNumber"]))?string.Empty:(System.String)dataRow["PhoneNumber"];
			entity.GoOnline = (Convert.IsDBNull(dataRow["GoOnline"]))?false:(System.Boolean?)dataRow["GoOnline"];
			entity.ZipCode = (Convert.IsDBNull(dataRow["ZipCode"]))?string.Empty:(System.String)dataRow["ZipCode"];
			entity.UpdatedBy = (Convert.IsDBNull(dataRow["UpdatedBy"]))?(long)0:(System.Int64?)dataRow["UpdatedBy"];
			entity.HotelAddress = (Convert.IsDBNull(dataRow["HotelAddress"]))?string.Empty:(System.String)dataRow["HotelAddress"];
			entity.CreationDate = (Convert.IsDBNull(dataRow["CreationDate"]))?DateTime.MinValue:(System.DateTime)dataRow["CreationDate"];
			entity.CountryCodePhone = (Convert.IsDBNull(dataRow["CountryCodePhone"]))?string.Empty:(System.String)dataRow["CountryCodePhone"];
			entity.CountryCodeFax = (Convert.IsDBNull(dataRow["CountryCodeFax"]))?string.Empty:(System.String)dataRow["CountryCodeFax"];
			entity.FaxNumber = (Convert.IsDBNull(dataRow["FaxNumber"]))?string.Empty:(System.String)dataRow["FaxNumber"];
			entity.IsRemoved = (Convert.IsDBNull(dataRow["IsRemoved"]))?false:(System.Boolean)dataRow["IsRemoved"];
			entity.RequestGoOnline = (Convert.IsDBNull(dataRow["RequestGoOnline"]))?false:(System.Boolean?)dataRow["RequestGoOnline"];
			entity.HotelUserId = (Convert.IsDBNull(dataRow["HotelUserID"]))?(long)0:(System.Int64?)dataRow["HotelUserID"];
			entity.AccountingOfficer = (Convert.IsDBNull(dataRow["AccountingOfficer"]))?string.Empty:(System.String)dataRow["AccountingOfficer"];
			entity.MeetingRoomId = (Convert.IsDBNull(dataRow["MeetingRoomId"]))?(long)0:(System.Int64)dataRow["MeetingRoomId"];
			entity.MeetingRoomName = (Convert.IsDBNull(dataRow["MeetingRoomName"]))?string.Empty:(System.String)dataRow["MeetingRoomName"];
			entity.Picture = (Convert.IsDBNull(dataRow["Picture"]))?string.Empty:(System.String)dataRow["Picture"];
			entity.DayLight = (Convert.IsDBNull(dataRow["DayLight"]))?(int)0:(System.Int32)dataRow["DayLight"];
			entity.Surface = (Convert.IsDBNull(dataRow["Surface"]))?(long)0:(System.Int64?)dataRow["Surface"];
			entity.Height = (Convert.IsDBNull(dataRow["Height"]))?0.0m:(System.Decimal?)dataRow["Height"];
			entity.MeetingRoomActive = (Convert.IsDBNull(dataRow["MeetingRoomActive"]))?false:(System.Boolean)dataRow["MeetingRoomActive"];
			entity.MrPlan = (Convert.IsDBNull(dataRow["MR_Plan"]))?string.Empty:(System.String)dataRow["MR_Plan"];
			entity.OrderNumber = (Convert.IsDBNull(dataRow["OrderNumber"]))?(int)0:(System.Int32?)dataRow["OrderNumber"];
			entity.HalfdayPrice = (Convert.IsDBNull(dataRow["HalfdayPrice"]))?0.0m:(System.Decimal?)dataRow["HalfdayPrice"];
			entity.FulldayPrice = (Convert.IsDBNull(dataRow["FulldayPrice"]))?0.0m:(System.Decimal?)dataRow["FulldayPrice"];
			entity.IsOnline = (Convert.IsDBNull(dataRow["IsOnline"]))?false:(System.Boolean)dataRow["IsOnline"];
			entity.IsDeleted = (Convert.IsDBNull(dataRow["IsDeleted"]))?false:(System.Boolean)dataRow["IsDeleted"];
			entity.RoomType = (Convert.IsDBNull(dataRow["RoomType"]))?(int)0:(System.Int32?)dataRow["RoomType"];
			entity.MeetingRoomConfigId = (Convert.IsDBNull(dataRow["MeetingRoomConfigId"]))?(long)0:(System.Int64)dataRow["MeetingRoomConfigId"];
			entity.RoomShapeId = (Convert.IsDBNull(dataRow["RoomShapeId"]))?(int)0:(System.Int32?)dataRow["RoomShapeId"];
			entity.MinCapacity = (Convert.IsDBNull(dataRow["MinCapacity"]))?(int)0:(System.Int32?)dataRow["MinCapacity"];
			entity.MaxCapicity = (Convert.IsDBNull(dataRow["MaxCapicity"]))?(int)0:(System.Int32?)dataRow["MaxCapicity"];
			entity.BookingAlgo = (Convert.IsDBNull(dataRow["BookingAlgo"]))?0.0m:(System.Decimal?)dataRow["BookingAlgo"];
			entity.RequestAlgo = (Convert.IsDBNull(dataRow["RequestAlgo"]))?0.0m:(System.Decimal?)dataRow["RequestAlgo"];
			entity.IsPriority = (Convert.IsDBNull(dataRow["IsPriority"]))?string.Empty:(System.String)dataRow["IsPriority"];
			entity.ActualPkgPrice = (Convert.IsDBNull(dataRow["ActualPkgPrice"]))?0.0m:(System.Decimal?)dataRow["ActualPkgPrice"];
			entity.ActualPkgPriceHalfDay = (Convert.IsDBNull(dataRow["ActualPkgPriceHalfDay"]))?0.0m:(System.Decimal?)dataRow["ActualPkgPriceHalfDay"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewSearchForDay2FilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewSearchForDay2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewSearchForDay2FilterBuilder : SqlFilterBuilder<ViewSearchForDay2Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewSearchForDay2FilterBuilder class.
		/// </summary>
		public ViewSearchForDay2FilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewSearchForDay2FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewSearchForDay2FilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewSearchForDay2FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewSearchForDay2FilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewSearchForDay2FilterBuilder

	#region ViewSearchForDay2ParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewSearchForDay2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewSearchForDay2ParameterBuilder : ParameterizedSqlFilterBuilder<ViewSearchForDay2Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewSearchForDay2ParameterBuilder class.
		/// </summary>
		public ViewSearchForDay2ParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewSearchForDay2ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewSearchForDay2ParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewSearchForDay2ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewSearchForDay2ParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewSearchForDay2ParameterBuilder
	
	#region ViewSearchForDay2SortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewSearchForDay2"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewSearchForDay2SortBuilder : SqlSortBuilder<ViewSearchForDay2Column>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewSearchForDay2SqlSortBuilder class.
		/// </summary>
		public ViewSearchForDay2SortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewSearchForDay2SortBuilder

} // end namespace
