﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewFindAvailableBedroomProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewFindAvailableBedroomProviderBaseCore : EntityViewProviderBase<ViewFindAvailableBedroom>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewFindAvailableBedroom&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewFindAvailableBedroom&gt;"/></returns>
		protected static VList&lt;ViewFindAvailableBedroom&gt; Fill(DataSet dataSet, VList<ViewFindAvailableBedroom> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewFindAvailableBedroom>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewFindAvailableBedroom&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewFindAvailableBedroom>"/></returns>
		protected static VList&lt;ViewFindAvailableBedroom&gt; Fill(DataTable dataTable, VList<ViewFindAvailableBedroom> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewFindAvailableBedroom c = new ViewFindAvailableBedroom();
					c.Id = (Convert.IsDBNull(row["Id"]))?(long)0:(System.Int64)row["Id"];
					c.HotelId = (Convert.IsDBNull(row["HotelId"]))?(long)0:(System.Int64)row["HotelId"];
					c.Name = (Convert.IsDBNull(row["Name"]))?string.Empty:(System.String)row["Name"];
					c.Types = (Convert.IsDBNull(row["Types"]))?(int)0:(System.Int32?)row["Types"];
					c.Allotment = (Convert.IsDBNull(row["Allotment"]))?(int)0:(System.Int32?)row["Allotment"];
					c.Picture = (Convert.IsDBNull(row["Picture"]))?string.Empty:(System.String)row["Picture"];
					c.PriceDouble = (Convert.IsDBNull(row["PriceDouble"]))?0.0m:(System.Decimal?)row["PriceDouble"];
					c.PriceSingle = (Convert.IsDBNull(row["PriceSingle"]))?0.0m:(System.Decimal?)row["PriceSingle"];
					c.IsBreakFastInclude = (Convert.IsDBNull(row["IsBreakFastInclude"]))?false:(System.Boolean)row["IsBreakFastInclude"];
					c.BreakfastPrice = (Convert.IsDBNull(row["BreakfastPrice"]))?0.0m:(System.Decimal?)row["BreakfastPrice"];
					c.AvailabilityId = (Convert.IsDBNull(row["AvailabilityID"]))?(long)0:(System.Int64)row["AvailabilityID"];
					c.AvailabilityDate = (Convert.IsDBNull(row["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)row["AvailabilityDate"];
					c.FullPropertyStatus = (Convert.IsDBNull(row["FullPropertyStatus"]))?(int)0:(System.Int32?)row["FullPropertyStatus"];
					c.LeadTimeForMeetingRoom = (Convert.IsDBNull(row["LeadTimeForMeetingRoom"]))?(long)0:(System.Int64?)row["LeadTimeForMeetingRoom"];
					c.FullProrertyStatusBr = (Convert.IsDBNull(row["FullProrertyStatusBR"]))?(int)0:(System.Int32?)row["FullProrertyStatusBR"];
					c.AvailabilityRoomId = (Convert.IsDBNull(row["AvailabilityRoomID"]))?(long)0:(System.Int64)row["AvailabilityRoomID"];
					c.RoomId = (Convert.IsDBNull(row["RoomId"]))?(long)0:(System.Int64)row["RoomId"];
					c.MorningStatus = (Convert.IsDBNull(row["MorningStatus"]))?(int)0:(System.Int32?)row["MorningStatus"];
					c.AfternoonStatus = (Convert.IsDBNull(row["AfternoonStatus"]))?(int)0:(System.Int32?)row["AfternoonStatus"];
					c.NumberOfRoomsAvailable = (Convert.IsDBNull(row["NumberOfRoomsAvailable"]))?(long)0:(System.Int64?)row["NumberOfRoomsAvailable"];
					c.RoomType = (Convert.IsDBNull(row["RoomType"]))?(int)0:(System.Int32?)row["RoomType"];
					c.PriceOfTheDayDouble = (Convert.IsDBNull(row["PriceOfTheDayDouble"]))?0.0m:(System.Decimal?)row["PriceOfTheDayDouble"];
					c.PriceOfTheDaySingle = (Convert.IsDBNull(row["PriceOfTheDaySingle"]))?0.0m:(System.Decimal?)row["PriceOfTheDaySingle"];
					c.NumberOfRoomBooked = (Convert.IsDBNull(row["NumberOfRoomBooked"]))?(long)0:(System.Int64?)row["NumberOfRoomBooked"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewFindAvailableBedroom&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewFindAvailableBedroom&gt;"/></returns>
		protected VList<ViewFindAvailableBedroom> Fill(IDataReader reader, VList<ViewFindAvailableBedroom> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewFindAvailableBedroom entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewFindAvailableBedroom>("ViewFindAvailableBedroom",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewFindAvailableBedroom();
					}
					
					entity.SuppressEntityEvents = true;

					entity.Id = (System.Int64)reader[((int)ViewFindAvailableBedroomColumn.Id)];
					//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
					entity.HotelId = (System.Int64)reader[((int)ViewFindAvailableBedroomColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
					entity.Name = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.Name)))?null:(System.String)reader[((int)ViewFindAvailableBedroomColumn.Name)];
					//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
					entity.Types = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.Types)))?null:(System.Int32?)reader[((int)ViewFindAvailableBedroomColumn.Types)];
					//entity.Types = (Convert.IsDBNull(reader["Types"]))?(int)0:(System.Int32?)reader["Types"];
					entity.Allotment = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.Allotment)))?null:(System.Int32?)reader[((int)ViewFindAvailableBedroomColumn.Allotment)];
					//entity.Allotment = (Convert.IsDBNull(reader["Allotment"]))?(int)0:(System.Int32?)reader["Allotment"];
					entity.Picture = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.Picture)))?null:(System.String)reader[((int)ViewFindAvailableBedroomColumn.Picture)];
					//entity.Picture = (Convert.IsDBNull(reader["Picture"]))?string.Empty:(System.String)reader["Picture"];
					entity.PriceDouble = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.PriceDouble)))?null:(System.Decimal?)reader[((int)ViewFindAvailableBedroomColumn.PriceDouble)];
					//entity.PriceDouble = (Convert.IsDBNull(reader["PriceDouble"]))?0.0m:(System.Decimal?)reader["PriceDouble"];
					entity.PriceSingle = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.PriceSingle)))?null:(System.Decimal?)reader[((int)ViewFindAvailableBedroomColumn.PriceSingle)];
					//entity.PriceSingle = (Convert.IsDBNull(reader["PriceSingle"]))?0.0m:(System.Decimal?)reader["PriceSingle"];
					entity.IsBreakFastInclude = (System.Boolean)reader[((int)ViewFindAvailableBedroomColumn.IsBreakFastInclude)];
					//entity.IsBreakFastInclude = (Convert.IsDBNull(reader["IsBreakFastInclude"]))?false:(System.Boolean)reader["IsBreakFastInclude"];
					entity.BreakfastPrice = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.BreakfastPrice)))?null:(System.Decimal?)reader[((int)ViewFindAvailableBedroomColumn.BreakfastPrice)];
					//entity.BreakfastPrice = (Convert.IsDBNull(reader["BreakfastPrice"]))?0.0m:(System.Decimal?)reader["BreakfastPrice"];
					entity.AvailabilityId = (System.Int64)reader[((int)ViewFindAvailableBedroomColumn.AvailabilityId)];
					//entity.AvailabilityId = (Convert.IsDBNull(reader["AvailabilityID"]))?(long)0:(System.Int64)reader["AvailabilityID"];
					entity.AvailabilityDate = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.AvailabilityDate)))?null:(System.DateTime?)reader[((int)ViewFindAvailableBedroomColumn.AvailabilityDate)];
					//entity.AvailabilityDate = (Convert.IsDBNull(reader["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)reader["AvailabilityDate"];
					entity.FullPropertyStatus = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.FullPropertyStatus)))?null:(System.Int32?)reader[((int)ViewFindAvailableBedroomColumn.FullPropertyStatus)];
					//entity.FullPropertyStatus = (Convert.IsDBNull(reader["FullPropertyStatus"]))?(int)0:(System.Int32?)reader["FullPropertyStatus"];
					entity.LeadTimeForMeetingRoom = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.LeadTimeForMeetingRoom)))?null:(System.Int64?)reader[((int)ViewFindAvailableBedroomColumn.LeadTimeForMeetingRoom)];
					//entity.LeadTimeForMeetingRoom = (Convert.IsDBNull(reader["LeadTimeForMeetingRoom"]))?(long)0:(System.Int64?)reader["LeadTimeForMeetingRoom"];
					entity.FullProrertyStatusBr = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.FullProrertyStatusBr)))?null:(System.Int32?)reader[((int)ViewFindAvailableBedroomColumn.FullProrertyStatusBr)];
					//entity.FullProrertyStatusBr = (Convert.IsDBNull(reader["FullProrertyStatusBR"]))?(int)0:(System.Int32?)reader["FullProrertyStatusBR"];
					entity.AvailabilityRoomId = (System.Int64)reader[((int)ViewFindAvailableBedroomColumn.AvailabilityRoomId)];
					//entity.AvailabilityRoomId = (Convert.IsDBNull(reader["AvailabilityRoomID"]))?(long)0:(System.Int64)reader["AvailabilityRoomID"];
					entity.RoomId = (System.Int64)reader[((int)ViewFindAvailableBedroomColumn.RoomId)];
					//entity.RoomId = (Convert.IsDBNull(reader["RoomId"]))?(long)0:(System.Int64)reader["RoomId"];
					entity.MorningStatus = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.MorningStatus)))?null:(System.Int32?)reader[((int)ViewFindAvailableBedroomColumn.MorningStatus)];
					//entity.MorningStatus = (Convert.IsDBNull(reader["MorningStatus"]))?(int)0:(System.Int32?)reader["MorningStatus"];
					entity.AfternoonStatus = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.AfternoonStatus)))?null:(System.Int32?)reader[((int)ViewFindAvailableBedroomColumn.AfternoonStatus)];
					//entity.AfternoonStatus = (Convert.IsDBNull(reader["AfternoonStatus"]))?(int)0:(System.Int32?)reader["AfternoonStatus"];
					entity.NumberOfRoomsAvailable = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.NumberOfRoomsAvailable)))?null:(System.Int64?)reader[((int)ViewFindAvailableBedroomColumn.NumberOfRoomsAvailable)];
					//entity.NumberOfRoomsAvailable = (Convert.IsDBNull(reader["NumberOfRoomsAvailable"]))?(long)0:(System.Int64?)reader["NumberOfRoomsAvailable"];
					entity.RoomType = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.RoomType)))?null:(System.Int32?)reader[((int)ViewFindAvailableBedroomColumn.RoomType)];
					//entity.RoomType = (Convert.IsDBNull(reader["RoomType"]))?(int)0:(System.Int32?)reader["RoomType"];
					entity.PriceOfTheDayDouble = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.PriceOfTheDayDouble)))?null:(System.Decimal?)reader[((int)ViewFindAvailableBedroomColumn.PriceOfTheDayDouble)];
					//entity.PriceOfTheDayDouble = (Convert.IsDBNull(reader["PriceOfTheDayDouble"]))?0.0m:(System.Decimal?)reader["PriceOfTheDayDouble"];
					entity.PriceOfTheDaySingle = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.PriceOfTheDaySingle)))?null:(System.Decimal?)reader[((int)ViewFindAvailableBedroomColumn.PriceOfTheDaySingle)];
					//entity.PriceOfTheDaySingle = (Convert.IsDBNull(reader["PriceOfTheDaySingle"]))?0.0m:(System.Decimal?)reader["PriceOfTheDaySingle"];
					entity.NumberOfRoomBooked = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.NumberOfRoomBooked)))?null:(System.Int64?)reader[((int)ViewFindAvailableBedroomColumn.NumberOfRoomBooked)];
					//entity.NumberOfRoomBooked = (Convert.IsDBNull(reader["NumberOfRoomBooked"]))?(long)0:(System.Int64?)reader["NumberOfRoomBooked"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewFindAvailableBedroom"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewFindAvailableBedroom"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewFindAvailableBedroom entity)
		{
			reader.Read();
			entity.Id = (System.Int64)reader[((int)ViewFindAvailableBedroomColumn.Id)];
			//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
			entity.HotelId = (System.Int64)reader[((int)ViewFindAvailableBedroomColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
			entity.Name = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.Name)))?null:(System.String)reader[((int)ViewFindAvailableBedroomColumn.Name)];
			//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
			entity.Types = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.Types)))?null:(System.Int32?)reader[((int)ViewFindAvailableBedroomColumn.Types)];
			//entity.Types = (Convert.IsDBNull(reader["Types"]))?(int)0:(System.Int32?)reader["Types"];
			entity.Allotment = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.Allotment)))?null:(System.Int32?)reader[((int)ViewFindAvailableBedroomColumn.Allotment)];
			//entity.Allotment = (Convert.IsDBNull(reader["Allotment"]))?(int)0:(System.Int32?)reader["Allotment"];
			entity.Picture = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.Picture)))?null:(System.String)reader[((int)ViewFindAvailableBedroomColumn.Picture)];
			//entity.Picture = (Convert.IsDBNull(reader["Picture"]))?string.Empty:(System.String)reader["Picture"];
			entity.PriceDouble = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.PriceDouble)))?null:(System.Decimal?)reader[((int)ViewFindAvailableBedroomColumn.PriceDouble)];
			//entity.PriceDouble = (Convert.IsDBNull(reader["PriceDouble"]))?0.0m:(System.Decimal?)reader["PriceDouble"];
			entity.PriceSingle = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.PriceSingle)))?null:(System.Decimal?)reader[((int)ViewFindAvailableBedroomColumn.PriceSingle)];
			//entity.PriceSingle = (Convert.IsDBNull(reader["PriceSingle"]))?0.0m:(System.Decimal?)reader["PriceSingle"];
			entity.IsBreakFastInclude = (System.Boolean)reader[((int)ViewFindAvailableBedroomColumn.IsBreakFastInclude)];
			//entity.IsBreakFastInclude = (Convert.IsDBNull(reader["IsBreakFastInclude"]))?false:(System.Boolean)reader["IsBreakFastInclude"];
			entity.BreakfastPrice = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.BreakfastPrice)))?null:(System.Decimal?)reader[((int)ViewFindAvailableBedroomColumn.BreakfastPrice)];
			//entity.BreakfastPrice = (Convert.IsDBNull(reader["BreakfastPrice"]))?0.0m:(System.Decimal?)reader["BreakfastPrice"];
			entity.AvailabilityId = (System.Int64)reader[((int)ViewFindAvailableBedroomColumn.AvailabilityId)];
			//entity.AvailabilityId = (Convert.IsDBNull(reader["AvailabilityID"]))?(long)0:(System.Int64)reader["AvailabilityID"];
			entity.AvailabilityDate = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.AvailabilityDate)))?null:(System.DateTime?)reader[((int)ViewFindAvailableBedroomColumn.AvailabilityDate)];
			//entity.AvailabilityDate = (Convert.IsDBNull(reader["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)reader["AvailabilityDate"];
			entity.FullPropertyStatus = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.FullPropertyStatus)))?null:(System.Int32?)reader[((int)ViewFindAvailableBedroomColumn.FullPropertyStatus)];
			//entity.FullPropertyStatus = (Convert.IsDBNull(reader["FullPropertyStatus"]))?(int)0:(System.Int32?)reader["FullPropertyStatus"];
			entity.LeadTimeForMeetingRoom = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.LeadTimeForMeetingRoom)))?null:(System.Int64?)reader[((int)ViewFindAvailableBedroomColumn.LeadTimeForMeetingRoom)];
			//entity.LeadTimeForMeetingRoom = (Convert.IsDBNull(reader["LeadTimeForMeetingRoom"]))?(long)0:(System.Int64?)reader["LeadTimeForMeetingRoom"];
			entity.FullProrertyStatusBr = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.FullProrertyStatusBr)))?null:(System.Int32?)reader[((int)ViewFindAvailableBedroomColumn.FullProrertyStatusBr)];
			//entity.FullProrertyStatusBr = (Convert.IsDBNull(reader["FullProrertyStatusBR"]))?(int)0:(System.Int32?)reader["FullProrertyStatusBR"];
			entity.AvailabilityRoomId = (System.Int64)reader[((int)ViewFindAvailableBedroomColumn.AvailabilityRoomId)];
			//entity.AvailabilityRoomId = (Convert.IsDBNull(reader["AvailabilityRoomID"]))?(long)0:(System.Int64)reader["AvailabilityRoomID"];
			entity.RoomId = (System.Int64)reader[((int)ViewFindAvailableBedroomColumn.RoomId)];
			//entity.RoomId = (Convert.IsDBNull(reader["RoomId"]))?(long)0:(System.Int64)reader["RoomId"];
			entity.MorningStatus = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.MorningStatus)))?null:(System.Int32?)reader[((int)ViewFindAvailableBedroomColumn.MorningStatus)];
			//entity.MorningStatus = (Convert.IsDBNull(reader["MorningStatus"]))?(int)0:(System.Int32?)reader["MorningStatus"];
			entity.AfternoonStatus = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.AfternoonStatus)))?null:(System.Int32?)reader[((int)ViewFindAvailableBedroomColumn.AfternoonStatus)];
			//entity.AfternoonStatus = (Convert.IsDBNull(reader["AfternoonStatus"]))?(int)0:(System.Int32?)reader["AfternoonStatus"];
			entity.NumberOfRoomsAvailable = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.NumberOfRoomsAvailable)))?null:(System.Int64?)reader[((int)ViewFindAvailableBedroomColumn.NumberOfRoomsAvailable)];
			//entity.NumberOfRoomsAvailable = (Convert.IsDBNull(reader["NumberOfRoomsAvailable"]))?(long)0:(System.Int64?)reader["NumberOfRoomsAvailable"];
			entity.RoomType = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.RoomType)))?null:(System.Int32?)reader[((int)ViewFindAvailableBedroomColumn.RoomType)];
			//entity.RoomType = (Convert.IsDBNull(reader["RoomType"]))?(int)0:(System.Int32?)reader["RoomType"];
			entity.PriceOfTheDayDouble = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.PriceOfTheDayDouble)))?null:(System.Decimal?)reader[((int)ViewFindAvailableBedroomColumn.PriceOfTheDayDouble)];
			//entity.PriceOfTheDayDouble = (Convert.IsDBNull(reader["PriceOfTheDayDouble"]))?0.0m:(System.Decimal?)reader["PriceOfTheDayDouble"];
			entity.PriceOfTheDaySingle = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.PriceOfTheDaySingle)))?null:(System.Decimal?)reader[((int)ViewFindAvailableBedroomColumn.PriceOfTheDaySingle)];
			//entity.PriceOfTheDaySingle = (Convert.IsDBNull(reader["PriceOfTheDaySingle"]))?0.0m:(System.Decimal?)reader["PriceOfTheDaySingle"];
			entity.NumberOfRoomBooked = (reader.IsDBNull(((int)ViewFindAvailableBedroomColumn.NumberOfRoomBooked)))?null:(System.Int64?)reader[((int)ViewFindAvailableBedroomColumn.NumberOfRoomBooked)];
			//entity.NumberOfRoomBooked = (Convert.IsDBNull(reader["NumberOfRoomBooked"]))?(long)0:(System.Int64?)reader["NumberOfRoomBooked"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewFindAvailableBedroom"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewFindAvailableBedroom"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewFindAvailableBedroom entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (Convert.IsDBNull(dataRow["Id"]))?(long)0:(System.Int64)dataRow["Id"];
			entity.HotelId = (Convert.IsDBNull(dataRow["HotelId"]))?(long)0:(System.Int64)dataRow["HotelId"];
			entity.Name = (Convert.IsDBNull(dataRow["Name"]))?string.Empty:(System.String)dataRow["Name"];
			entity.Types = (Convert.IsDBNull(dataRow["Types"]))?(int)0:(System.Int32?)dataRow["Types"];
			entity.Allotment = (Convert.IsDBNull(dataRow["Allotment"]))?(int)0:(System.Int32?)dataRow["Allotment"];
			entity.Picture = (Convert.IsDBNull(dataRow["Picture"]))?string.Empty:(System.String)dataRow["Picture"];
			entity.PriceDouble = (Convert.IsDBNull(dataRow["PriceDouble"]))?0.0m:(System.Decimal?)dataRow["PriceDouble"];
			entity.PriceSingle = (Convert.IsDBNull(dataRow["PriceSingle"]))?0.0m:(System.Decimal?)dataRow["PriceSingle"];
			entity.IsBreakFastInclude = (Convert.IsDBNull(dataRow["IsBreakFastInclude"]))?false:(System.Boolean)dataRow["IsBreakFastInclude"];
			entity.BreakfastPrice = (Convert.IsDBNull(dataRow["BreakfastPrice"]))?0.0m:(System.Decimal?)dataRow["BreakfastPrice"];
			entity.AvailabilityId = (Convert.IsDBNull(dataRow["AvailabilityID"]))?(long)0:(System.Int64)dataRow["AvailabilityID"];
			entity.AvailabilityDate = (Convert.IsDBNull(dataRow["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["AvailabilityDate"];
			entity.FullPropertyStatus = (Convert.IsDBNull(dataRow["FullPropertyStatus"]))?(int)0:(System.Int32?)dataRow["FullPropertyStatus"];
			entity.LeadTimeForMeetingRoom = (Convert.IsDBNull(dataRow["LeadTimeForMeetingRoom"]))?(long)0:(System.Int64?)dataRow["LeadTimeForMeetingRoom"];
			entity.FullProrertyStatusBr = (Convert.IsDBNull(dataRow["FullProrertyStatusBR"]))?(int)0:(System.Int32?)dataRow["FullProrertyStatusBR"];
			entity.AvailabilityRoomId = (Convert.IsDBNull(dataRow["AvailabilityRoomID"]))?(long)0:(System.Int64)dataRow["AvailabilityRoomID"];
			entity.RoomId = (Convert.IsDBNull(dataRow["RoomId"]))?(long)0:(System.Int64)dataRow["RoomId"];
			entity.MorningStatus = (Convert.IsDBNull(dataRow["MorningStatus"]))?(int)0:(System.Int32?)dataRow["MorningStatus"];
			entity.AfternoonStatus = (Convert.IsDBNull(dataRow["AfternoonStatus"]))?(int)0:(System.Int32?)dataRow["AfternoonStatus"];
			entity.NumberOfRoomsAvailable = (Convert.IsDBNull(dataRow["NumberOfRoomsAvailable"]))?(long)0:(System.Int64?)dataRow["NumberOfRoomsAvailable"];
			entity.RoomType = (Convert.IsDBNull(dataRow["RoomType"]))?(int)0:(System.Int32?)dataRow["RoomType"];
			entity.PriceOfTheDayDouble = (Convert.IsDBNull(dataRow["PriceOfTheDayDouble"]))?0.0m:(System.Decimal?)dataRow["PriceOfTheDayDouble"];
			entity.PriceOfTheDaySingle = (Convert.IsDBNull(dataRow["PriceOfTheDaySingle"]))?0.0m:(System.Decimal?)dataRow["PriceOfTheDaySingle"];
			entity.NumberOfRoomBooked = (Convert.IsDBNull(dataRow["NumberOfRoomBooked"]))?(long)0:(System.Int64?)dataRow["NumberOfRoomBooked"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewFindAvailableBedroomFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewFindAvailableBedroom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewFindAvailableBedroomFilterBuilder : SqlFilterBuilder<ViewFindAvailableBedroomColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewFindAvailableBedroomFilterBuilder class.
		/// </summary>
		public ViewFindAvailableBedroomFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewFindAvailableBedroomFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewFindAvailableBedroomFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewFindAvailableBedroomFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewFindAvailableBedroomFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewFindAvailableBedroomFilterBuilder

	#region ViewFindAvailableBedroomParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewFindAvailableBedroom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewFindAvailableBedroomParameterBuilder : ParameterizedSqlFilterBuilder<ViewFindAvailableBedroomColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewFindAvailableBedroomParameterBuilder class.
		/// </summary>
		public ViewFindAvailableBedroomParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewFindAvailableBedroomParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewFindAvailableBedroomParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewFindAvailableBedroomParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewFindAvailableBedroomParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewFindAvailableBedroomParameterBuilder
	
	#region ViewFindAvailableBedroomSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewFindAvailableBedroom"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewFindAvailableBedroomSortBuilder : SqlSortBuilder<ViewFindAvailableBedroomColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewFindAvailableBedroomSqlSortBuilder class.
		/// </summary>
		public ViewFindAvailableBedroomSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewFindAvailableBedroomSortBuilder

} // end namespace
