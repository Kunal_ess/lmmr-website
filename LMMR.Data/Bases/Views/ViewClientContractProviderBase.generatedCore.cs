﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewClientContractProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewClientContractProviderBaseCore : EntityViewProviderBase<ViewClientContract>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewClientContract&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewClientContract&gt;"/></returns>
		protected static VList&lt;ViewClientContract&gt; Fill(DataSet dataSet, VList<ViewClientContract> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewClientContract>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewClientContract&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewClientContract>"/></returns>
		protected static VList&lt;ViewClientContract&gt; Fill(DataTable dataTable, VList<ViewClientContract> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewClientContract c = new ViewClientContract();
					c.Id = (Convert.IsDBNull(row["Id"]))?(long)0:(System.Int64)row["Id"];
					c.City = (Convert.IsDBNull(row["City"]))?string.Empty:(System.String)row["City"];
					c.CounytryId = (Convert.IsDBNull(row["CounytryId"]))?(long)0:(System.Int64)row["CounytryId"];
					c.CountryName = (Convert.IsDBNull(row["CountryName"]))?string.Empty:(System.String)row["CountryName"];
					c.HotelId = (Convert.IsDBNull(row["HotelId"]))?(long)0:(System.Int64)row["HotelId"];
					c.Name = (Convert.IsDBNull(row["Name"]))?string.Empty:(System.String)row["Name"];
					c.CountryId = (Convert.IsDBNull(row["CountryId"]))?(long)0:(System.Int64)row["CountryId"];
					c.CityId = (Convert.IsDBNull(row["CityId"]))?(long)0:(System.Int64)row["CityId"];
					c.ZoneId = (Convert.IsDBNull(row["ZoneId"]))?(long)0:(System.Int64)row["ZoneId"];
					c.Stars = (Convert.IsDBNull(row["Stars"]))?(int)0:(System.Int32?)row["Stars"];
					c.Longitude = (Convert.IsDBNull(row["Longitude"]))?string.Empty:(System.String)row["Longitude"];
					c.Latitude = (Convert.IsDBNull(row["Latitude"]))?string.Empty:(System.String)row["Latitude"];
					c.OperatorChoice = (Convert.IsDBNull(row["OperatorChoice"]))?string.Empty:(System.String)row["OperatorChoice"];
					c.HotelPlan = (Convert.IsDBNull(row["HotelPlan"]))?string.Empty:(System.String)row["HotelPlan"];
					c.ClientId = (Convert.IsDBNull(row["ClientId"]))?(long)0:(System.Int64?)row["ClientId"];
					c.ContractId = (Convert.IsDBNull(row["ContractId"]))?string.Empty:(System.String)row["ContractId"];
					c.StaffId = (Convert.IsDBNull(row["StaffId"]))?(long)0:(System.Int64)row["StaffId"];
					c.CurrencyId = (Convert.IsDBNull(row["CurrencyId"]))?(long)0:(System.Int64)row["CurrencyId"];
					c.ContractValue = (Convert.IsDBNull(row["ContractValue"]))?0.0m:(System.Decimal?)row["ContractValue"];
					c.Phone = (Convert.IsDBNull(row["Phone"]))?string.Empty:(System.String)row["Phone"];
					c.HotelAddress = (Convert.IsDBNull(row["HotelAddress"]))?string.Empty:(System.String)row["HotelAddress"];
					c.UserId = (Convert.IsDBNull(row["UserId"]))?(long)0:(System.Int64)row["UserId"];
					c.FirstName = (Convert.IsDBNull(row["FirstName"]))?string.Empty:(System.String)row["FirstName"];
					c.EmailId = (Convert.IsDBNull(row["EmailId"]))?string.Empty:(System.String)row["EmailId"];
					c.IsActive = (Convert.IsDBNull(row["IsActive"]))?false:(System.Boolean)row["IsActive"];
					c.StaffexActId = (Convert.IsDBNull(row["StaffexActId"]))?(long)0:(System.Int64)row["StaffexActId"];
					c.StaffName = (Convert.IsDBNull(row["StaffName"]))?string.Empty:(System.String)row["StaffName"];
					c.ZonId = (Convert.IsDBNull(row["ZonId"]))?(long)0:(System.Int64)row["ZonId"];
					c.Zone = (Convert.IsDBNull(row["Zone"]))?string.Empty:(System.String)row["Zone"];
					c.Currency = (Convert.IsDBNull(row["Currency"]))?string.Empty:(System.String)row["Currency"];
					c.CurrencySignature = (Convert.IsDBNull(row["CurrencySignature"]))?string.Empty:(System.String)row["CurrencySignature"];
					c.Password = (Convert.IsDBNull(row["Password"]))?string.Empty:(System.String)row["Password"];
					c.Usertype = (Convert.IsDBNull(row["Usertype"]))?(int)0:(System.Int32?)row["Usertype"];
					c.LastLogin = (Convert.IsDBNull(row["LastLogin"]))?DateTime.MinValue:(System.DateTime?)row["LastLogin"];
					c.CurId = (Convert.IsDBNull(row["CurId"]))?(long)0:(System.Int64)row["CurId"];
					c.PhoneNumber = (Convert.IsDBNull(row["PhoneNumber"]))?string.Empty:(System.String)row["PhoneNumber"];
					c.ContactPerson = (Convert.IsDBNull(row["ContactPerson"]))?string.Empty:(System.String)row["ContactPerson"];
					c.CreationDate = (Convert.IsDBNull(row["CreationDate"]))?DateTime.MinValue:(System.DateTime)row["CreationDate"];
					c.Hotelisactive = (Convert.IsDBNull(row["hotelisactive"]))?false:(System.Boolean)row["hotelisactive"];
					c.Logo = (Convert.IsDBNull(row["Logo"]))?string.Empty:(System.String)row["Logo"];
					c.PhoneExt = (Convert.IsDBNull(row["PhoneExt"]))?string.Empty:(System.String)row["PhoneExt"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewClientContract&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewClientContract&gt;"/></returns>
		protected VList<ViewClientContract> Fill(IDataReader reader, VList<ViewClientContract> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewClientContract entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewClientContract>("ViewClientContract",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewClientContract();
					}
					
					entity.SuppressEntityEvents = true;

					entity.Id = (System.Int64)reader[((int)ViewClientContractColumn.Id)];
					//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
					entity.City = (reader.IsDBNull(((int)ViewClientContractColumn.City)))?null:(System.String)reader[((int)ViewClientContractColumn.City)];
					//entity.City = (Convert.IsDBNull(reader["City"]))?string.Empty:(System.String)reader["City"];
					entity.CounytryId = (System.Int64)reader[((int)ViewClientContractColumn.CounytryId)];
					//entity.CounytryId = (Convert.IsDBNull(reader["CounytryId"]))?(long)0:(System.Int64)reader["CounytryId"];
					entity.CountryName = (reader.IsDBNull(((int)ViewClientContractColumn.CountryName)))?null:(System.String)reader[((int)ViewClientContractColumn.CountryName)];
					//entity.CountryName = (Convert.IsDBNull(reader["CountryName"]))?string.Empty:(System.String)reader["CountryName"];
					entity.HotelId = (System.Int64)reader[((int)ViewClientContractColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
					entity.Name = (reader.IsDBNull(((int)ViewClientContractColumn.Name)))?null:(System.String)reader[((int)ViewClientContractColumn.Name)];
					//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
					entity.CountryId = (System.Int64)reader[((int)ViewClientContractColumn.CountryId)];
					//entity.CountryId = (Convert.IsDBNull(reader["CountryId"]))?(long)0:(System.Int64)reader["CountryId"];
					entity.CityId = (System.Int64)reader[((int)ViewClientContractColumn.CityId)];
					//entity.CityId = (Convert.IsDBNull(reader["CityId"]))?(long)0:(System.Int64)reader["CityId"];
					entity.ZoneId = (System.Int64)reader[((int)ViewClientContractColumn.ZoneId)];
					//entity.ZoneId = (Convert.IsDBNull(reader["ZoneId"]))?(long)0:(System.Int64)reader["ZoneId"];
					entity.Stars = (reader.IsDBNull(((int)ViewClientContractColumn.Stars)))?null:(System.Int32?)reader[((int)ViewClientContractColumn.Stars)];
					//entity.Stars = (Convert.IsDBNull(reader["Stars"]))?(int)0:(System.Int32?)reader["Stars"];
					entity.Longitude = (reader.IsDBNull(((int)ViewClientContractColumn.Longitude)))?null:(System.String)reader[((int)ViewClientContractColumn.Longitude)];
					//entity.Longitude = (Convert.IsDBNull(reader["Longitude"]))?string.Empty:(System.String)reader["Longitude"];
					entity.Latitude = (reader.IsDBNull(((int)ViewClientContractColumn.Latitude)))?null:(System.String)reader[((int)ViewClientContractColumn.Latitude)];
					//entity.Latitude = (Convert.IsDBNull(reader["Latitude"]))?string.Empty:(System.String)reader["Latitude"];
					entity.OperatorChoice = (reader.IsDBNull(((int)ViewClientContractColumn.OperatorChoice)))?null:(System.String)reader[((int)ViewClientContractColumn.OperatorChoice)];
					//entity.OperatorChoice = (Convert.IsDBNull(reader["OperatorChoice"]))?string.Empty:(System.String)reader["OperatorChoice"];
					entity.HotelPlan = (reader.IsDBNull(((int)ViewClientContractColumn.HotelPlan)))?null:(System.String)reader[((int)ViewClientContractColumn.HotelPlan)];
					//entity.HotelPlan = (Convert.IsDBNull(reader["HotelPlan"]))?string.Empty:(System.String)reader["HotelPlan"];
					entity.ClientId = (reader.IsDBNull(((int)ViewClientContractColumn.ClientId)))?null:(System.Int64?)reader[((int)ViewClientContractColumn.ClientId)];
					//entity.ClientId = (Convert.IsDBNull(reader["ClientId"]))?(long)0:(System.Int64?)reader["ClientId"];
					entity.ContractId = (reader.IsDBNull(((int)ViewClientContractColumn.ContractId)))?null:(System.String)reader[((int)ViewClientContractColumn.ContractId)];
					//entity.ContractId = (Convert.IsDBNull(reader["ContractId"]))?string.Empty:(System.String)reader["ContractId"];
					entity.StaffId = (System.Int64)reader[((int)ViewClientContractColumn.StaffId)];
					//entity.StaffId = (Convert.IsDBNull(reader["StaffId"]))?(long)0:(System.Int64)reader["StaffId"];
					entity.CurrencyId = (System.Int64)reader[((int)ViewClientContractColumn.CurrencyId)];
					//entity.CurrencyId = (Convert.IsDBNull(reader["CurrencyId"]))?(long)0:(System.Int64)reader["CurrencyId"];
					entity.ContractValue = (reader.IsDBNull(((int)ViewClientContractColumn.ContractValue)))?null:(System.Decimal?)reader[((int)ViewClientContractColumn.ContractValue)];
					//entity.ContractValue = (Convert.IsDBNull(reader["ContractValue"]))?0.0m:(System.Decimal?)reader["ContractValue"];
					entity.Phone = (reader.IsDBNull(((int)ViewClientContractColumn.Phone)))?null:(System.String)reader[((int)ViewClientContractColumn.Phone)];
					//entity.Phone = (Convert.IsDBNull(reader["Phone"]))?string.Empty:(System.String)reader["Phone"];
					entity.HotelAddress = (reader.IsDBNull(((int)ViewClientContractColumn.HotelAddress)))?null:(System.String)reader[((int)ViewClientContractColumn.HotelAddress)];
					//entity.HotelAddress = (Convert.IsDBNull(reader["HotelAddress"]))?string.Empty:(System.String)reader["HotelAddress"];
					entity.UserId = (System.Int64)reader[((int)ViewClientContractColumn.UserId)];
					//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(long)0:(System.Int64)reader["UserId"];
					entity.FirstName = (reader.IsDBNull(((int)ViewClientContractColumn.FirstName)))?null:(System.String)reader[((int)ViewClientContractColumn.FirstName)];
					//entity.FirstName = (Convert.IsDBNull(reader["FirstName"]))?string.Empty:(System.String)reader["FirstName"];
					entity.EmailId = (reader.IsDBNull(((int)ViewClientContractColumn.EmailId)))?null:(System.String)reader[((int)ViewClientContractColumn.EmailId)];
					//entity.EmailId = (Convert.IsDBNull(reader["EmailId"]))?string.Empty:(System.String)reader["EmailId"];
					entity.IsActive = (System.Boolean)reader[((int)ViewClientContractColumn.IsActive)];
					//entity.IsActive = (Convert.IsDBNull(reader["IsActive"]))?false:(System.Boolean)reader["IsActive"];
					entity.StaffexActId = (System.Int64)reader[((int)ViewClientContractColumn.StaffexActId)];
					//entity.StaffexActId = (Convert.IsDBNull(reader["StaffexActId"]))?(long)0:(System.Int64)reader["StaffexActId"];
					entity.StaffName = (reader.IsDBNull(((int)ViewClientContractColumn.StaffName)))?null:(System.String)reader[((int)ViewClientContractColumn.StaffName)];
					//entity.StaffName = (Convert.IsDBNull(reader["StaffName"]))?string.Empty:(System.String)reader["StaffName"];
					entity.ZonId = (System.Int64)reader[((int)ViewClientContractColumn.ZonId)];
					//entity.ZonId = (Convert.IsDBNull(reader["ZonId"]))?(long)0:(System.Int64)reader["ZonId"];
					entity.Zone = (reader.IsDBNull(((int)ViewClientContractColumn.Zone)))?null:(System.String)reader[((int)ViewClientContractColumn.Zone)];
					//entity.Zone = (Convert.IsDBNull(reader["Zone"]))?string.Empty:(System.String)reader["Zone"];
					entity.Currency = (reader.IsDBNull(((int)ViewClientContractColumn.Currency)))?null:(System.String)reader[((int)ViewClientContractColumn.Currency)];
					//entity.Currency = (Convert.IsDBNull(reader["Currency"]))?string.Empty:(System.String)reader["Currency"];
					entity.CurrencySignature = (reader.IsDBNull(((int)ViewClientContractColumn.CurrencySignature)))?null:(System.String)reader[((int)ViewClientContractColumn.CurrencySignature)];
					//entity.CurrencySignature = (Convert.IsDBNull(reader["CurrencySignature"]))?string.Empty:(System.String)reader["CurrencySignature"];
					entity.Password = (reader.IsDBNull(((int)ViewClientContractColumn.Password)))?null:(System.String)reader[((int)ViewClientContractColumn.Password)];
					//entity.Password = (Convert.IsDBNull(reader["Password"]))?string.Empty:(System.String)reader["Password"];
					entity.Usertype = (reader.IsDBNull(((int)ViewClientContractColumn.Usertype)))?null:(System.Int32?)reader[((int)ViewClientContractColumn.Usertype)];
					//entity.Usertype = (Convert.IsDBNull(reader["Usertype"]))?(int)0:(System.Int32?)reader["Usertype"];
					entity.LastLogin = (reader.IsDBNull(((int)ViewClientContractColumn.LastLogin)))?null:(System.DateTime?)reader[((int)ViewClientContractColumn.LastLogin)];
					//entity.LastLogin = (Convert.IsDBNull(reader["LastLogin"]))?DateTime.MinValue:(System.DateTime?)reader["LastLogin"];
					entity.CurId = (System.Int64)reader[((int)ViewClientContractColumn.CurId)];
					//entity.CurId = (Convert.IsDBNull(reader["CurId"]))?(long)0:(System.Int64)reader["CurId"];
					entity.PhoneNumber = (reader.IsDBNull(((int)ViewClientContractColumn.PhoneNumber)))?null:(System.String)reader[((int)ViewClientContractColumn.PhoneNumber)];
					//entity.PhoneNumber = (Convert.IsDBNull(reader["PhoneNumber"]))?string.Empty:(System.String)reader["PhoneNumber"];
					entity.ContactPerson = (reader.IsDBNull(((int)ViewClientContractColumn.ContactPerson)))?null:(System.String)reader[((int)ViewClientContractColumn.ContactPerson)];
					//entity.ContactPerson = (Convert.IsDBNull(reader["ContactPerson"]))?string.Empty:(System.String)reader["ContactPerson"];
					entity.CreationDate = (System.DateTime)reader[((int)ViewClientContractColumn.CreationDate)];
					//entity.CreationDate = (Convert.IsDBNull(reader["CreationDate"]))?DateTime.MinValue:(System.DateTime)reader["CreationDate"];
					entity.Hotelisactive = (System.Boolean)reader[((int)ViewClientContractColumn.Hotelisactive)];
					//entity.Hotelisactive = (Convert.IsDBNull(reader["hotelisactive"]))?false:(System.Boolean)reader["hotelisactive"];
					entity.Logo = (reader.IsDBNull(((int)ViewClientContractColumn.Logo)))?null:(System.String)reader[((int)ViewClientContractColumn.Logo)];
					//entity.Logo = (Convert.IsDBNull(reader["Logo"]))?string.Empty:(System.String)reader["Logo"];
					entity.PhoneExt = (reader.IsDBNull(((int)ViewClientContractColumn.PhoneExt)))?null:(System.String)reader[((int)ViewClientContractColumn.PhoneExt)];
					//entity.PhoneExt = (Convert.IsDBNull(reader["PhoneExt"]))?string.Empty:(System.String)reader["PhoneExt"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewClientContract"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewClientContract"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewClientContract entity)
		{
			reader.Read();
			entity.Id = (System.Int64)reader[((int)ViewClientContractColumn.Id)];
			//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
			entity.City = (reader.IsDBNull(((int)ViewClientContractColumn.City)))?null:(System.String)reader[((int)ViewClientContractColumn.City)];
			//entity.City = (Convert.IsDBNull(reader["City"]))?string.Empty:(System.String)reader["City"];
			entity.CounytryId = (System.Int64)reader[((int)ViewClientContractColumn.CounytryId)];
			//entity.CounytryId = (Convert.IsDBNull(reader["CounytryId"]))?(long)0:(System.Int64)reader["CounytryId"];
			entity.CountryName = (reader.IsDBNull(((int)ViewClientContractColumn.CountryName)))?null:(System.String)reader[((int)ViewClientContractColumn.CountryName)];
			//entity.CountryName = (Convert.IsDBNull(reader["CountryName"]))?string.Empty:(System.String)reader["CountryName"];
			entity.HotelId = (System.Int64)reader[((int)ViewClientContractColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
			entity.Name = (reader.IsDBNull(((int)ViewClientContractColumn.Name)))?null:(System.String)reader[((int)ViewClientContractColumn.Name)];
			//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
			entity.CountryId = (System.Int64)reader[((int)ViewClientContractColumn.CountryId)];
			//entity.CountryId = (Convert.IsDBNull(reader["CountryId"]))?(long)0:(System.Int64)reader["CountryId"];
			entity.CityId = (System.Int64)reader[((int)ViewClientContractColumn.CityId)];
			//entity.CityId = (Convert.IsDBNull(reader["CityId"]))?(long)0:(System.Int64)reader["CityId"];
			entity.ZoneId = (System.Int64)reader[((int)ViewClientContractColumn.ZoneId)];
			//entity.ZoneId = (Convert.IsDBNull(reader["ZoneId"]))?(long)0:(System.Int64)reader["ZoneId"];
			entity.Stars = (reader.IsDBNull(((int)ViewClientContractColumn.Stars)))?null:(System.Int32?)reader[((int)ViewClientContractColumn.Stars)];
			//entity.Stars = (Convert.IsDBNull(reader["Stars"]))?(int)0:(System.Int32?)reader["Stars"];
			entity.Longitude = (reader.IsDBNull(((int)ViewClientContractColumn.Longitude)))?null:(System.String)reader[((int)ViewClientContractColumn.Longitude)];
			//entity.Longitude = (Convert.IsDBNull(reader["Longitude"]))?string.Empty:(System.String)reader["Longitude"];
			entity.Latitude = (reader.IsDBNull(((int)ViewClientContractColumn.Latitude)))?null:(System.String)reader[((int)ViewClientContractColumn.Latitude)];
			//entity.Latitude = (Convert.IsDBNull(reader["Latitude"]))?string.Empty:(System.String)reader["Latitude"];
			entity.OperatorChoice = (reader.IsDBNull(((int)ViewClientContractColumn.OperatorChoice)))?null:(System.String)reader[((int)ViewClientContractColumn.OperatorChoice)];
			//entity.OperatorChoice = (Convert.IsDBNull(reader["OperatorChoice"]))?string.Empty:(System.String)reader["OperatorChoice"];
			entity.HotelPlan = (reader.IsDBNull(((int)ViewClientContractColumn.HotelPlan)))?null:(System.String)reader[((int)ViewClientContractColumn.HotelPlan)];
			//entity.HotelPlan = (Convert.IsDBNull(reader["HotelPlan"]))?string.Empty:(System.String)reader["HotelPlan"];
			entity.ClientId = (reader.IsDBNull(((int)ViewClientContractColumn.ClientId)))?null:(System.Int64?)reader[((int)ViewClientContractColumn.ClientId)];
			//entity.ClientId = (Convert.IsDBNull(reader["ClientId"]))?(long)0:(System.Int64?)reader["ClientId"];
			entity.ContractId = (reader.IsDBNull(((int)ViewClientContractColumn.ContractId)))?null:(System.String)reader[((int)ViewClientContractColumn.ContractId)];
			//entity.ContractId = (Convert.IsDBNull(reader["ContractId"]))?string.Empty:(System.String)reader["ContractId"];
			entity.StaffId = (System.Int64)reader[((int)ViewClientContractColumn.StaffId)];
			//entity.StaffId = (Convert.IsDBNull(reader["StaffId"]))?(long)0:(System.Int64)reader["StaffId"];
			entity.CurrencyId = (System.Int64)reader[((int)ViewClientContractColumn.CurrencyId)];
			//entity.CurrencyId = (Convert.IsDBNull(reader["CurrencyId"]))?(long)0:(System.Int64)reader["CurrencyId"];
			entity.ContractValue = (reader.IsDBNull(((int)ViewClientContractColumn.ContractValue)))?null:(System.Decimal?)reader[((int)ViewClientContractColumn.ContractValue)];
			//entity.ContractValue = (Convert.IsDBNull(reader["ContractValue"]))?0.0m:(System.Decimal?)reader["ContractValue"];
			entity.Phone = (reader.IsDBNull(((int)ViewClientContractColumn.Phone)))?null:(System.String)reader[((int)ViewClientContractColumn.Phone)];
			//entity.Phone = (Convert.IsDBNull(reader["Phone"]))?string.Empty:(System.String)reader["Phone"];
			entity.HotelAddress = (reader.IsDBNull(((int)ViewClientContractColumn.HotelAddress)))?null:(System.String)reader[((int)ViewClientContractColumn.HotelAddress)];
			//entity.HotelAddress = (Convert.IsDBNull(reader["HotelAddress"]))?string.Empty:(System.String)reader["HotelAddress"];
			entity.UserId = (System.Int64)reader[((int)ViewClientContractColumn.UserId)];
			//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(long)0:(System.Int64)reader["UserId"];
			entity.FirstName = (reader.IsDBNull(((int)ViewClientContractColumn.FirstName)))?null:(System.String)reader[((int)ViewClientContractColumn.FirstName)];
			//entity.FirstName = (Convert.IsDBNull(reader["FirstName"]))?string.Empty:(System.String)reader["FirstName"];
			entity.EmailId = (reader.IsDBNull(((int)ViewClientContractColumn.EmailId)))?null:(System.String)reader[((int)ViewClientContractColumn.EmailId)];
			//entity.EmailId = (Convert.IsDBNull(reader["EmailId"]))?string.Empty:(System.String)reader["EmailId"];
			entity.IsActive = (System.Boolean)reader[((int)ViewClientContractColumn.IsActive)];
			//entity.IsActive = (Convert.IsDBNull(reader["IsActive"]))?false:(System.Boolean)reader["IsActive"];
			entity.StaffexActId = (System.Int64)reader[((int)ViewClientContractColumn.StaffexActId)];
			//entity.StaffexActId = (Convert.IsDBNull(reader["StaffexActId"]))?(long)0:(System.Int64)reader["StaffexActId"];
			entity.StaffName = (reader.IsDBNull(((int)ViewClientContractColumn.StaffName)))?null:(System.String)reader[((int)ViewClientContractColumn.StaffName)];
			//entity.StaffName = (Convert.IsDBNull(reader["StaffName"]))?string.Empty:(System.String)reader["StaffName"];
			entity.ZonId = (System.Int64)reader[((int)ViewClientContractColumn.ZonId)];
			//entity.ZonId = (Convert.IsDBNull(reader["ZonId"]))?(long)0:(System.Int64)reader["ZonId"];
			entity.Zone = (reader.IsDBNull(((int)ViewClientContractColumn.Zone)))?null:(System.String)reader[((int)ViewClientContractColumn.Zone)];
			//entity.Zone = (Convert.IsDBNull(reader["Zone"]))?string.Empty:(System.String)reader["Zone"];
			entity.Currency = (reader.IsDBNull(((int)ViewClientContractColumn.Currency)))?null:(System.String)reader[((int)ViewClientContractColumn.Currency)];
			//entity.Currency = (Convert.IsDBNull(reader["Currency"]))?string.Empty:(System.String)reader["Currency"];
			entity.CurrencySignature = (reader.IsDBNull(((int)ViewClientContractColumn.CurrencySignature)))?null:(System.String)reader[((int)ViewClientContractColumn.CurrencySignature)];
			//entity.CurrencySignature = (Convert.IsDBNull(reader["CurrencySignature"]))?string.Empty:(System.String)reader["CurrencySignature"];
			entity.Password = (reader.IsDBNull(((int)ViewClientContractColumn.Password)))?null:(System.String)reader[((int)ViewClientContractColumn.Password)];
			//entity.Password = (Convert.IsDBNull(reader["Password"]))?string.Empty:(System.String)reader["Password"];
			entity.Usertype = (reader.IsDBNull(((int)ViewClientContractColumn.Usertype)))?null:(System.Int32?)reader[((int)ViewClientContractColumn.Usertype)];
			//entity.Usertype = (Convert.IsDBNull(reader["Usertype"]))?(int)0:(System.Int32?)reader["Usertype"];
			entity.LastLogin = (reader.IsDBNull(((int)ViewClientContractColumn.LastLogin)))?null:(System.DateTime?)reader[((int)ViewClientContractColumn.LastLogin)];
			//entity.LastLogin = (Convert.IsDBNull(reader["LastLogin"]))?DateTime.MinValue:(System.DateTime?)reader["LastLogin"];
			entity.CurId = (System.Int64)reader[((int)ViewClientContractColumn.CurId)];
			//entity.CurId = (Convert.IsDBNull(reader["CurId"]))?(long)0:(System.Int64)reader["CurId"];
			entity.PhoneNumber = (reader.IsDBNull(((int)ViewClientContractColumn.PhoneNumber)))?null:(System.String)reader[((int)ViewClientContractColumn.PhoneNumber)];
			//entity.PhoneNumber = (Convert.IsDBNull(reader["PhoneNumber"]))?string.Empty:(System.String)reader["PhoneNumber"];
			entity.ContactPerson = (reader.IsDBNull(((int)ViewClientContractColumn.ContactPerson)))?null:(System.String)reader[((int)ViewClientContractColumn.ContactPerson)];
			//entity.ContactPerson = (Convert.IsDBNull(reader["ContactPerson"]))?string.Empty:(System.String)reader["ContactPerson"];
			entity.CreationDate = (System.DateTime)reader[((int)ViewClientContractColumn.CreationDate)];
			//entity.CreationDate = (Convert.IsDBNull(reader["CreationDate"]))?DateTime.MinValue:(System.DateTime)reader["CreationDate"];
			entity.Hotelisactive = (System.Boolean)reader[((int)ViewClientContractColumn.Hotelisactive)];
			//entity.Hotelisactive = (Convert.IsDBNull(reader["hotelisactive"]))?false:(System.Boolean)reader["hotelisactive"];
			entity.Logo = (reader.IsDBNull(((int)ViewClientContractColumn.Logo)))?null:(System.String)reader[((int)ViewClientContractColumn.Logo)];
			//entity.Logo = (Convert.IsDBNull(reader["Logo"]))?string.Empty:(System.String)reader["Logo"];
			entity.PhoneExt = (reader.IsDBNull(((int)ViewClientContractColumn.PhoneExt)))?null:(System.String)reader[((int)ViewClientContractColumn.PhoneExt)];
			//entity.PhoneExt = (Convert.IsDBNull(reader["PhoneExt"]))?string.Empty:(System.String)reader["PhoneExt"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewClientContract"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewClientContract"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewClientContract entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (Convert.IsDBNull(dataRow["Id"]))?(long)0:(System.Int64)dataRow["Id"];
			entity.City = (Convert.IsDBNull(dataRow["City"]))?string.Empty:(System.String)dataRow["City"];
			entity.CounytryId = (Convert.IsDBNull(dataRow["CounytryId"]))?(long)0:(System.Int64)dataRow["CounytryId"];
			entity.CountryName = (Convert.IsDBNull(dataRow["CountryName"]))?string.Empty:(System.String)dataRow["CountryName"];
			entity.HotelId = (Convert.IsDBNull(dataRow["HotelId"]))?(long)0:(System.Int64)dataRow["HotelId"];
			entity.Name = (Convert.IsDBNull(dataRow["Name"]))?string.Empty:(System.String)dataRow["Name"];
			entity.CountryId = (Convert.IsDBNull(dataRow["CountryId"]))?(long)0:(System.Int64)dataRow["CountryId"];
			entity.CityId = (Convert.IsDBNull(dataRow["CityId"]))?(long)0:(System.Int64)dataRow["CityId"];
			entity.ZoneId = (Convert.IsDBNull(dataRow["ZoneId"]))?(long)0:(System.Int64)dataRow["ZoneId"];
			entity.Stars = (Convert.IsDBNull(dataRow["Stars"]))?(int)0:(System.Int32?)dataRow["Stars"];
			entity.Longitude = (Convert.IsDBNull(dataRow["Longitude"]))?string.Empty:(System.String)dataRow["Longitude"];
			entity.Latitude = (Convert.IsDBNull(dataRow["Latitude"]))?string.Empty:(System.String)dataRow["Latitude"];
			entity.OperatorChoice = (Convert.IsDBNull(dataRow["OperatorChoice"]))?string.Empty:(System.String)dataRow["OperatorChoice"];
			entity.HotelPlan = (Convert.IsDBNull(dataRow["HotelPlan"]))?string.Empty:(System.String)dataRow["HotelPlan"];
			entity.ClientId = (Convert.IsDBNull(dataRow["ClientId"]))?(long)0:(System.Int64?)dataRow["ClientId"];
			entity.ContractId = (Convert.IsDBNull(dataRow["ContractId"]))?string.Empty:(System.String)dataRow["ContractId"];
			entity.StaffId = (Convert.IsDBNull(dataRow["StaffId"]))?(long)0:(System.Int64)dataRow["StaffId"];
			entity.CurrencyId = (Convert.IsDBNull(dataRow["CurrencyId"]))?(long)0:(System.Int64)dataRow["CurrencyId"];
			entity.ContractValue = (Convert.IsDBNull(dataRow["ContractValue"]))?0.0m:(System.Decimal?)dataRow["ContractValue"];
			entity.Phone = (Convert.IsDBNull(dataRow["Phone"]))?string.Empty:(System.String)dataRow["Phone"];
			entity.HotelAddress = (Convert.IsDBNull(dataRow["HotelAddress"]))?string.Empty:(System.String)dataRow["HotelAddress"];
			entity.UserId = (Convert.IsDBNull(dataRow["UserId"]))?(long)0:(System.Int64)dataRow["UserId"];
			entity.FirstName = (Convert.IsDBNull(dataRow["FirstName"]))?string.Empty:(System.String)dataRow["FirstName"];
			entity.EmailId = (Convert.IsDBNull(dataRow["EmailId"]))?string.Empty:(System.String)dataRow["EmailId"];
			entity.IsActive = (Convert.IsDBNull(dataRow["IsActive"]))?false:(System.Boolean)dataRow["IsActive"];
			entity.StaffexActId = (Convert.IsDBNull(dataRow["StaffexActId"]))?(long)0:(System.Int64)dataRow["StaffexActId"];
			entity.StaffName = (Convert.IsDBNull(dataRow["StaffName"]))?string.Empty:(System.String)dataRow["StaffName"];
			entity.ZonId = (Convert.IsDBNull(dataRow["ZonId"]))?(long)0:(System.Int64)dataRow["ZonId"];
			entity.Zone = (Convert.IsDBNull(dataRow["Zone"]))?string.Empty:(System.String)dataRow["Zone"];
			entity.Currency = (Convert.IsDBNull(dataRow["Currency"]))?string.Empty:(System.String)dataRow["Currency"];
			entity.CurrencySignature = (Convert.IsDBNull(dataRow["CurrencySignature"]))?string.Empty:(System.String)dataRow["CurrencySignature"];
			entity.Password = (Convert.IsDBNull(dataRow["Password"]))?string.Empty:(System.String)dataRow["Password"];
			entity.Usertype = (Convert.IsDBNull(dataRow["Usertype"]))?(int)0:(System.Int32?)dataRow["Usertype"];
			entity.LastLogin = (Convert.IsDBNull(dataRow["LastLogin"]))?DateTime.MinValue:(System.DateTime?)dataRow["LastLogin"];
			entity.CurId = (Convert.IsDBNull(dataRow["CurId"]))?(long)0:(System.Int64)dataRow["CurId"];
			entity.PhoneNumber = (Convert.IsDBNull(dataRow["PhoneNumber"]))?string.Empty:(System.String)dataRow["PhoneNumber"];
			entity.ContactPerson = (Convert.IsDBNull(dataRow["ContactPerson"]))?string.Empty:(System.String)dataRow["ContactPerson"];
			entity.CreationDate = (Convert.IsDBNull(dataRow["CreationDate"]))?DateTime.MinValue:(System.DateTime)dataRow["CreationDate"];
			entity.Hotelisactive = (Convert.IsDBNull(dataRow["hotelisactive"]))?false:(System.Boolean)dataRow["hotelisactive"];
			entity.Logo = (Convert.IsDBNull(dataRow["Logo"]))?string.Empty:(System.String)dataRow["Logo"];
			entity.PhoneExt = (Convert.IsDBNull(dataRow["PhoneExt"]))?string.Empty:(System.String)dataRow["PhoneExt"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewClientContractFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewClientContract"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewClientContractFilterBuilder : SqlFilterBuilder<ViewClientContractColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewClientContractFilterBuilder class.
		/// </summary>
		public ViewClientContractFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewClientContractFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewClientContractFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewClientContractFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewClientContractFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewClientContractFilterBuilder

	#region ViewClientContractParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewClientContract"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewClientContractParameterBuilder : ParameterizedSqlFilterBuilder<ViewClientContractColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewClientContractParameterBuilder class.
		/// </summary>
		public ViewClientContractParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewClientContractParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewClientContractParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewClientContractParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewClientContractParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewClientContractParameterBuilder
	
	#region ViewClientContractSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewClientContract"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewClientContractSortBuilder : SqlSortBuilder<ViewClientContractColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewClientContractSqlSortBuilder class.
		/// </summary>
		public ViewClientContractSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewClientContractSortBuilder

} // end namespace
