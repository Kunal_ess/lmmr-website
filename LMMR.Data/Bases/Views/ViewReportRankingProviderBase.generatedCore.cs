﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewReportRankingProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewReportRankingProviderBaseCore : EntityViewProviderBase<ViewReportRanking>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewReportRanking&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewReportRanking&gt;"/></returns>
		protected static VList&lt;ViewReportRanking&gt; Fill(DataSet dataSet, VList<ViewReportRanking> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewReportRanking>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewReportRanking&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewReportRanking>"/></returns>
		protected static VList&lt;ViewReportRanking&gt; Fill(DataTable dataTable, VList<ViewReportRanking> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewReportRanking c = new ViewReportRanking();
					c.Countryid = (Convert.IsDBNull(row["countryid"]))?(long)0:(System.Int64)row["countryid"];
					c.Countryname = (Convert.IsDBNull(row["Countryname"]))?string.Empty:(System.String)row["Countryname"];
					c.CityId = (Convert.IsDBNull(row["cityID"]))?(long)0:(System.Int64)row["cityID"];
					c.City = (Convert.IsDBNull(row["city"]))?string.Empty:(System.String)row["city"];
					c.HotelId = (Convert.IsDBNull(row["HotelId"]))?(long)0:(System.Int64)row["HotelId"];
					c.HotelName = (Convert.IsDBNull(row["HotelName"]))?string.Empty:(System.String)row["HotelName"];
					c.BookingAlgo = (Convert.IsDBNull(row["BookingAlgo"]))?0.0m:(System.Decimal?)row["BookingAlgo"];
					c.RequestAlgo = (Convert.IsDBNull(row["RequestAlgo"]))?0.0m:(System.Decimal?)row["RequestAlgo"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewReportRanking&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewReportRanking&gt;"/></returns>
		protected VList<ViewReportRanking> Fill(IDataReader reader, VList<ViewReportRanking> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewReportRanking entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewReportRanking>("ViewReportRanking",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewReportRanking();
					}
					
					entity.SuppressEntityEvents = true;

					entity.Countryid = (System.Int64)reader[((int)ViewReportRankingColumn.Countryid)];
					//entity.Countryid = (Convert.IsDBNull(reader["countryid"]))?(long)0:(System.Int64)reader["countryid"];
					entity.Countryname = (reader.IsDBNull(((int)ViewReportRankingColumn.Countryname)))?null:(System.String)reader[((int)ViewReportRankingColumn.Countryname)];
					//entity.Countryname = (Convert.IsDBNull(reader["Countryname"]))?string.Empty:(System.String)reader["Countryname"];
					entity.CityId = (System.Int64)reader[((int)ViewReportRankingColumn.CityId)];
					//entity.CityId = (Convert.IsDBNull(reader["cityID"]))?(long)0:(System.Int64)reader["cityID"];
					entity.City = (reader.IsDBNull(((int)ViewReportRankingColumn.City)))?null:(System.String)reader[((int)ViewReportRankingColumn.City)];
					//entity.City = (Convert.IsDBNull(reader["city"]))?string.Empty:(System.String)reader["city"];
					entity.HotelId = (System.Int64)reader[((int)ViewReportRankingColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
					entity.HotelName = (reader.IsDBNull(((int)ViewReportRankingColumn.HotelName)))?null:(System.String)reader[((int)ViewReportRankingColumn.HotelName)];
					//entity.HotelName = (Convert.IsDBNull(reader["HotelName"]))?string.Empty:(System.String)reader["HotelName"];
					entity.BookingAlgo = (reader.IsDBNull(((int)ViewReportRankingColumn.BookingAlgo)))?null:(System.Decimal?)reader[((int)ViewReportRankingColumn.BookingAlgo)];
					//entity.BookingAlgo = (Convert.IsDBNull(reader["BookingAlgo"]))?0.0m:(System.Decimal?)reader["BookingAlgo"];
					entity.RequestAlgo = (reader.IsDBNull(((int)ViewReportRankingColumn.RequestAlgo)))?null:(System.Decimal?)reader[((int)ViewReportRankingColumn.RequestAlgo)];
					//entity.RequestAlgo = (Convert.IsDBNull(reader["RequestAlgo"]))?0.0m:(System.Decimal?)reader["RequestAlgo"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewReportRanking"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewReportRanking"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewReportRanking entity)
		{
			reader.Read();
			entity.Countryid = (System.Int64)reader[((int)ViewReportRankingColumn.Countryid)];
			//entity.Countryid = (Convert.IsDBNull(reader["countryid"]))?(long)0:(System.Int64)reader["countryid"];
			entity.Countryname = (reader.IsDBNull(((int)ViewReportRankingColumn.Countryname)))?null:(System.String)reader[((int)ViewReportRankingColumn.Countryname)];
			//entity.Countryname = (Convert.IsDBNull(reader["Countryname"]))?string.Empty:(System.String)reader["Countryname"];
			entity.CityId = (System.Int64)reader[((int)ViewReportRankingColumn.CityId)];
			//entity.CityId = (Convert.IsDBNull(reader["cityID"]))?(long)0:(System.Int64)reader["cityID"];
			entity.City = (reader.IsDBNull(((int)ViewReportRankingColumn.City)))?null:(System.String)reader[((int)ViewReportRankingColumn.City)];
			//entity.City = (Convert.IsDBNull(reader["city"]))?string.Empty:(System.String)reader["city"];
			entity.HotelId = (System.Int64)reader[((int)ViewReportRankingColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
			entity.HotelName = (reader.IsDBNull(((int)ViewReportRankingColumn.HotelName)))?null:(System.String)reader[((int)ViewReportRankingColumn.HotelName)];
			//entity.HotelName = (Convert.IsDBNull(reader["HotelName"]))?string.Empty:(System.String)reader["HotelName"];
			entity.BookingAlgo = (reader.IsDBNull(((int)ViewReportRankingColumn.BookingAlgo)))?null:(System.Decimal?)reader[((int)ViewReportRankingColumn.BookingAlgo)];
			//entity.BookingAlgo = (Convert.IsDBNull(reader["BookingAlgo"]))?0.0m:(System.Decimal?)reader["BookingAlgo"];
			entity.RequestAlgo = (reader.IsDBNull(((int)ViewReportRankingColumn.RequestAlgo)))?null:(System.Decimal?)reader[((int)ViewReportRankingColumn.RequestAlgo)];
			//entity.RequestAlgo = (Convert.IsDBNull(reader["RequestAlgo"]))?0.0m:(System.Decimal?)reader["RequestAlgo"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewReportRanking"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewReportRanking"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewReportRanking entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Countryid = (Convert.IsDBNull(dataRow["countryid"]))?(long)0:(System.Int64)dataRow["countryid"];
			entity.Countryname = (Convert.IsDBNull(dataRow["Countryname"]))?string.Empty:(System.String)dataRow["Countryname"];
			entity.CityId = (Convert.IsDBNull(dataRow["cityID"]))?(long)0:(System.Int64)dataRow["cityID"];
			entity.City = (Convert.IsDBNull(dataRow["city"]))?string.Empty:(System.String)dataRow["city"];
			entity.HotelId = (Convert.IsDBNull(dataRow["HotelId"]))?(long)0:(System.Int64)dataRow["HotelId"];
			entity.HotelName = (Convert.IsDBNull(dataRow["HotelName"]))?string.Empty:(System.String)dataRow["HotelName"];
			entity.BookingAlgo = (Convert.IsDBNull(dataRow["BookingAlgo"]))?0.0m:(System.Decimal?)dataRow["BookingAlgo"];
			entity.RequestAlgo = (Convert.IsDBNull(dataRow["RequestAlgo"]))?0.0m:(System.Decimal?)dataRow["RequestAlgo"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewReportRankingFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportRanking"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportRankingFilterBuilder : SqlFilterBuilder<ViewReportRankingColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportRankingFilterBuilder class.
		/// </summary>
		public ViewReportRankingFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportRankingFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportRankingFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportRankingFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportRankingFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportRankingFilterBuilder

	#region ViewReportRankingParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportRanking"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportRankingParameterBuilder : ParameterizedSqlFilterBuilder<ViewReportRankingColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportRankingParameterBuilder class.
		/// </summary>
		public ViewReportRankingParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportRankingParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportRankingParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportRankingParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportRankingParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportRankingParameterBuilder
	
	#region ViewReportRankingSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportRanking"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewReportRankingSortBuilder : SqlSortBuilder<ViewReportRankingColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportRankingSqlSortBuilder class.
		/// </summary>
		public ViewReportRankingSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewReportRankingSortBuilder

} // end namespace
