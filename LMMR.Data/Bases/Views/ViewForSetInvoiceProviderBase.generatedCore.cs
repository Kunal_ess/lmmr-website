﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewForSetInvoiceProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewForSetInvoiceProviderBaseCore : EntityViewProviderBase<ViewForSetInvoice>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewForSetInvoice&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewForSetInvoice&gt;"/></returns>
		protected static VList&lt;ViewForSetInvoice&gt; Fill(DataSet dataSet, VList<ViewForSetInvoice> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewForSetInvoice>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewForSetInvoice&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewForSetInvoice>"/></returns>
		protected static VList&lt;ViewForSetInvoice&gt; Fill(DataTable dataTable, VList<ViewForSetInvoice> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewForSetInvoice c = new ViewForSetInvoice();
					c.CommId = (Convert.IsDBNull(row["CommId"]))?(int)0:(System.Int32)row["CommId"];
					c.FromDay = (Convert.IsDBNull(row["FromDay"]))?(int)0:(System.Int32?)row["FromDay"];
					c.ToDay = (Convert.IsDBNull(row["ToDay"]))?(int)0:(System.Int32?)row["ToDay"];
					c.DefaultCommission = (Convert.IsDBNull(row["DefaultCommission"]))?(int)0:(System.Int32?)row["DefaultCommission"];
					c.CommPernId = (Convert.IsDBNull(row["CommPernId"]))?(int)0:(System.Int32)row["CommPernId"];
					c.ProfileName = (Convert.IsDBNull(row["ProfileName"]))?string.Empty:(System.String)row["ProfileName"];
					c.CommissionDayId = (Convert.IsDBNull(row["CommissionDayId"]))?(int)0:(System.Int32?)row["CommissionDayId"];
					c.CommissionPercentage = (Convert.IsDBNull(row["CommissionPercentage"]))?(int)0:(System.Int32?)row["CommissionPercentage"];
					c.HotelId = (Convert.IsDBNull(row["HotelId"]))?(long)0:(System.Int64?)row["HotelId"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewForSetInvoice&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewForSetInvoice&gt;"/></returns>
		protected VList<ViewForSetInvoice> Fill(IDataReader reader, VList<ViewForSetInvoice> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewForSetInvoice entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewForSetInvoice>("ViewForSetInvoice",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewForSetInvoice();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CommId = (System.Int32)reader[((int)ViewForSetInvoiceColumn.CommId)];
					//entity.CommId = (Convert.IsDBNull(reader["CommId"]))?(int)0:(System.Int32)reader["CommId"];
					entity.FromDay = (reader.IsDBNull(((int)ViewForSetInvoiceColumn.FromDay)))?null:(System.Int32?)reader[((int)ViewForSetInvoiceColumn.FromDay)];
					//entity.FromDay = (Convert.IsDBNull(reader["FromDay"]))?(int)0:(System.Int32?)reader["FromDay"];
					entity.ToDay = (reader.IsDBNull(((int)ViewForSetInvoiceColumn.ToDay)))?null:(System.Int32?)reader[((int)ViewForSetInvoiceColumn.ToDay)];
					//entity.ToDay = (Convert.IsDBNull(reader["ToDay"]))?(int)0:(System.Int32?)reader["ToDay"];
					entity.DefaultCommission = (reader.IsDBNull(((int)ViewForSetInvoiceColumn.DefaultCommission)))?null:(System.Int32?)reader[((int)ViewForSetInvoiceColumn.DefaultCommission)];
					//entity.DefaultCommission = (Convert.IsDBNull(reader["DefaultCommission"]))?(int)0:(System.Int32?)reader["DefaultCommission"];
					entity.CommPernId = (System.Int32)reader[((int)ViewForSetInvoiceColumn.CommPernId)];
					//entity.CommPernId = (Convert.IsDBNull(reader["CommPernId"]))?(int)0:(System.Int32)reader["CommPernId"];
					entity.ProfileName = (reader.IsDBNull(((int)ViewForSetInvoiceColumn.ProfileName)))?null:(System.String)reader[((int)ViewForSetInvoiceColumn.ProfileName)];
					//entity.ProfileName = (Convert.IsDBNull(reader["ProfileName"]))?string.Empty:(System.String)reader["ProfileName"];
					entity.CommissionDayId = (reader.IsDBNull(((int)ViewForSetInvoiceColumn.CommissionDayId)))?null:(System.Int32?)reader[((int)ViewForSetInvoiceColumn.CommissionDayId)];
					//entity.CommissionDayId = (Convert.IsDBNull(reader["CommissionDayId"]))?(int)0:(System.Int32?)reader["CommissionDayId"];
					entity.CommissionPercentage = (reader.IsDBNull(((int)ViewForSetInvoiceColumn.CommissionPercentage)))?null:(System.Int32?)reader[((int)ViewForSetInvoiceColumn.CommissionPercentage)];
					//entity.CommissionPercentage = (Convert.IsDBNull(reader["CommissionPercentage"]))?(int)0:(System.Int32?)reader["CommissionPercentage"];
					entity.HotelId = (reader.IsDBNull(((int)ViewForSetInvoiceColumn.HotelId)))?null:(System.Int64?)reader[((int)ViewForSetInvoiceColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64?)reader["HotelId"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewForSetInvoice"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewForSetInvoice"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewForSetInvoice entity)
		{
			reader.Read();
			entity.CommId = (System.Int32)reader[((int)ViewForSetInvoiceColumn.CommId)];
			//entity.CommId = (Convert.IsDBNull(reader["CommId"]))?(int)0:(System.Int32)reader["CommId"];
			entity.FromDay = (reader.IsDBNull(((int)ViewForSetInvoiceColumn.FromDay)))?null:(System.Int32?)reader[((int)ViewForSetInvoiceColumn.FromDay)];
			//entity.FromDay = (Convert.IsDBNull(reader["FromDay"]))?(int)0:(System.Int32?)reader["FromDay"];
			entity.ToDay = (reader.IsDBNull(((int)ViewForSetInvoiceColumn.ToDay)))?null:(System.Int32?)reader[((int)ViewForSetInvoiceColumn.ToDay)];
			//entity.ToDay = (Convert.IsDBNull(reader["ToDay"]))?(int)0:(System.Int32?)reader["ToDay"];
			entity.DefaultCommission = (reader.IsDBNull(((int)ViewForSetInvoiceColumn.DefaultCommission)))?null:(System.Int32?)reader[((int)ViewForSetInvoiceColumn.DefaultCommission)];
			//entity.DefaultCommission = (Convert.IsDBNull(reader["DefaultCommission"]))?(int)0:(System.Int32?)reader["DefaultCommission"];
			entity.CommPernId = (System.Int32)reader[((int)ViewForSetInvoiceColumn.CommPernId)];
			//entity.CommPernId = (Convert.IsDBNull(reader["CommPernId"]))?(int)0:(System.Int32)reader["CommPernId"];
			entity.ProfileName = (reader.IsDBNull(((int)ViewForSetInvoiceColumn.ProfileName)))?null:(System.String)reader[((int)ViewForSetInvoiceColumn.ProfileName)];
			//entity.ProfileName = (Convert.IsDBNull(reader["ProfileName"]))?string.Empty:(System.String)reader["ProfileName"];
			entity.CommissionDayId = (reader.IsDBNull(((int)ViewForSetInvoiceColumn.CommissionDayId)))?null:(System.Int32?)reader[((int)ViewForSetInvoiceColumn.CommissionDayId)];
			//entity.CommissionDayId = (Convert.IsDBNull(reader["CommissionDayId"]))?(int)0:(System.Int32?)reader["CommissionDayId"];
			entity.CommissionPercentage = (reader.IsDBNull(((int)ViewForSetInvoiceColumn.CommissionPercentage)))?null:(System.Int32?)reader[((int)ViewForSetInvoiceColumn.CommissionPercentage)];
			//entity.CommissionPercentage = (Convert.IsDBNull(reader["CommissionPercentage"]))?(int)0:(System.Int32?)reader["CommissionPercentage"];
			entity.HotelId = (reader.IsDBNull(((int)ViewForSetInvoiceColumn.HotelId)))?null:(System.Int64?)reader[((int)ViewForSetInvoiceColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64?)reader["HotelId"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewForSetInvoice"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewForSetInvoice"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewForSetInvoice entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CommId = (Convert.IsDBNull(dataRow["CommId"]))?(int)0:(System.Int32)dataRow["CommId"];
			entity.FromDay = (Convert.IsDBNull(dataRow["FromDay"]))?(int)0:(System.Int32?)dataRow["FromDay"];
			entity.ToDay = (Convert.IsDBNull(dataRow["ToDay"]))?(int)0:(System.Int32?)dataRow["ToDay"];
			entity.DefaultCommission = (Convert.IsDBNull(dataRow["DefaultCommission"]))?(int)0:(System.Int32?)dataRow["DefaultCommission"];
			entity.CommPernId = (Convert.IsDBNull(dataRow["CommPernId"]))?(int)0:(System.Int32)dataRow["CommPernId"];
			entity.ProfileName = (Convert.IsDBNull(dataRow["ProfileName"]))?string.Empty:(System.String)dataRow["ProfileName"];
			entity.CommissionDayId = (Convert.IsDBNull(dataRow["CommissionDayId"]))?(int)0:(System.Int32?)dataRow["CommissionDayId"];
			entity.CommissionPercentage = (Convert.IsDBNull(dataRow["CommissionPercentage"]))?(int)0:(System.Int32?)dataRow["CommissionPercentage"];
			entity.HotelId = (Convert.IsDBNull(dataRow["HotelId"]))?(long)0:(System.Int64?)dataRow["HotelId"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewForSetInvoiceFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewForSetInvoice"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewForSetInvoiceFilterBuilder : SqlFilterBuilder<ViewForSetInvoiceColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewForSetInvoiceFilterBuilder class.
		/// </summary>
		public ViewForSetInvoiceFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewForSetInvoiceFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewForSetInvoiceFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewForSetInvoiceFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewForSetInvoiceFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewForSetInvoiceFilterBuilder

	#region ViewForSetInvoiceParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewForSetInvoice"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewForSetInvoiceParameterBuilder : ParameterizedSqlFilterBuilder<ViewForSetInvoiceColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewForSetInvoiceParameterBuilder class.
		/// </summary>
		public ViewForSetInvoiceParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewForSetInvoiceParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewForSetInvoiceParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewForSetInvoiceParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewForSetInvoiceParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewForSetInvoiceParameterBuilder
	
	#region ViewForSetInvoiceSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewForSetInvoice"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewForSetInvoiceSortBuilder : SqlSortBuilder<ViewForSetInvoiceColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewForSetInvoiceSqlSortBuilder class.
		/// </summary>
		public ViewForSetInvoiceSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewForSetInvoiceSortBuilder

} // end namespace
