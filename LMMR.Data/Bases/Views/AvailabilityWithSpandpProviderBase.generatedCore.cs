﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AvailabilityWithSpandpProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class AvailabilityWithSpandpProviderBaseCore : EntityViewProviderBase<AvailabilityWithSpandp>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;AvailabilityWithSpandp&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;AvailabilityWithSpandp&gt;"/></returns>
		protected static VList&lt;AvailabilityWithSpandp&gt; Fill(DataSet dataSet, VList<AvailabilityWithSpandp> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<AvailabilityWithSpandp>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;AvailabilityWithSpandp&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<AvailabilityWithSpandp>"/></returns>
		protected static VList&lt;AvailabilityWithSpandp&gt; Fill(DataTable dataTable, VList<AvailabilityWithSpandp> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					AvailabilityWithSpandp c = new AvailabilityWithSpandp();
					c.Id = (Convert.IsDBNull(row["Id"]))?(long)0:(System.Int64)row["Id"];
					c.HotelId = (Convert.IsDBNull(row["HotelID"]))?(long)0:(System.Int64)row["HotelID"];
					c.AvailabilityDate = (Convert.IsDBNull(row["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)row["AvailabilityDate"];
					c.FullPropertyStatus = (Convert.IsDBNull(row["FullPropertyStatus"]))?(int)0:(System.Int32?)row["FullPropertyStatus"];
					c.FullPropStatus = (Convert.IsDBNull(row["FullPropStatus"]))?string.Empty:(System.String)row["FullPropStatus"];
					c.LeadTimeForMeetingRoom = (Convert.IsDBNull(row["LeadTimeForMeetingRoom"]))?(long)0:(System.Int64?)row["LeadTimeForMeetingRoom"];
					c.MeetingRoomPercent = (Convert.IsDBNull(row["MeetingRoomPercent"]))?0.0m:(System.Decimal?)row["MeetingRoomPercent"];
					c.DdrPercent = (Convert.IsDBNull(row["DDRPercent"]))?0.0m:(System.Decimal?)row["DDRPercent"];
					c.FullProrertyStatusBr = (Convert.IsDBNull(row["FullProrertyStatusBR"]))?(int)0:(System.Int32?)row["FullProrertyStatusBR"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;AvailabilityWithSpandp&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;AvailabilityWithSpandp&gt;"/></returns>
		protected VList<AvailabilityWithSpandp> Fill(IDataReader reader, VList<AvailabilityWithSpandp> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					AvailabilityWithSpandp entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<AvailabilityWithSpandp>("AvailabilityWithSpandp",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new AvailabilityWithSpandp();
					}
					
					entity.SuppressEntityEvents = true;

					entity.Id = (System.Int64)reader[((int)AvailabilityWithSpandpColumn.Id)];
					//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
					entity.HotelId = (System.Int64)reader[((int)AvailabilityWithSpandpColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["HotelID"]))?(long)0:(System.Int64)reader["HotelID"];
					entity.AvailabilityDate = (reader.IsDBNull(((int)AvailabilityWithSpandpColumn.AvailabilityDate)))?null:(System.DateTime?)reader[((int)AvailabilityWithSpandpColumn.AvailabilityDate)];
					//entity.AvailabilityDate = (Convert.IsDBNull(reader["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)reader["AvailabilityDate"];
					entity.FullPropertyStatus = (reader.IsDBNull(((int)AvailabilityWithSpandpColumn.FullPropertyStatus)))?null:(System.Int32?)reader[((int)AvailabilityWithSpandpColumn.FullPropertyStatus)];
					//entity.FullPropertyStatus = (Convert.IsDBNull(reader["FullPropertyStatus"]))?(int)0:(System.Int32?)reader["FullPropertyStatus"];
					entity.FullPropStatus = (System.String)reader[((int)AvailabilityWithSpandpColumn.FullPropStatus)];
					//entity.FullPropStatus = (Convert.IsDBNull(reader["FullPropStatus"]))?string.Empty:(System.String)reader["FullPropStatus"];
					entity.LeadTimeForMeetingRoom = (reader.IsDBNull(((int)AvailabilityWithSpandpColumn.LeadTimeForMeetingRoom)))?null:(System.Int64?)reader[((int)AvailabilityWithSpandpColumn.LeadTimeForMeetingRoom)];
					//entity.LeadTimeForMeetingRoom = (Convert.IsDBNull(reader["LeadTimeForMeetingRoom"]))?(long)0:(System.Int64?)reader["LeadTimeForMeetingRoom"];
					entity.MeetingRoomPercent = (reader.IsDBNull(((int)AvailabilityWithSpandpColumn.MeetingRoomPercent)))?null:(System.Decimal?)reader[((int)AvailabilityWithSpandpColumn.MeetingRoomPercent)];
					//entity.MeetingRoomPercent = (Convert.IsDBNull(reader["MeetingRoomPercent"]))?0.0m:(System.Decimal?)reader["MeetingRoomPercent"];
					entity.DdrPercent = (reader.IsDBNull(((int)AvailabilityWithSpandpColumn.DdrPercent)))?null:(System.Decimal?)reader[((int)AvailabilityWithSpandpColumn.DdrPercent)];
					//entity.DdrPercent = (Convert.IsDBNull(reader["DDRPercent"]))?0.0m:(System.Decimal?)reader["DDRPercent"];
					entity.FullProrertyStatusBr = (reader.IsDBNull(((int)AvailabilityWithSpandpColumn.FullProrertyStatusBr)))?null:(System.Int32?)reader[((int)AvailabilityWithSpandpColumn.FullProrertyStatusBr)];
					//entity.FullProrertyStatusBr = (Convert.IsDBNull(reader["FullProrertyStatusBR"]))?(int)0:(System.Int32?)reader["FullProrertyStatusBR"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="AvailabilityWithSpandp"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="AvailabilityWithSpandp"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, AvailabilityWithSpandp entity)
		{
			reader.Read();
			entity.Id = (System.Int64)reader[((int)AvailabilityWithSpandpColumn.Id)];
			//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
			entity.HotelId = (System.Int64)reader[((int)AvailabilityWithSpandpColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["HotelID"]))?(long)0:(System.Int64)reader["HotelID"];
			entity.AvailabilityDate = (reader.IsDBNull(((int)AvailabilityWithSpandpColumn.AvailabilityDate)))?null:(System.DateTime?)reader[((int)AvailabilityWithSpandpColumn.AvailabilityDate)];
			//entity.AvailabilityDate = (Convert.IsDBNull(reader["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)reader["AvailabilityDate"];
			entity.FullPropertyStatus = (reader.IsDBNull(((int)AvailabilityWithSpandpColumn.FullPropertyStatus)))?null:(System.Int32?)reader[((int)AvailabilityWithSpandpColumn.FullPropertyStatus)];
			//entity.FullPropertyStatus = (Convert.IsDBNull(reader["FullPropertyStatus"]))?(int)0:(System.Int32?)reader["FullPropertyStatus"];
			entity.FullPropStatus = (System.String)reader[((int)AvailabilityWithSpandpColumn.FullPropStatus)];
			//entity.FullPropStatus = (Convert.IsDBNull(reader["FullPropStatus"]))?string.Empty:(System.String)reader["FullPropStatus"];
			entity.LeadTimeForMeetingRoom = (reader.IsDBNull(((int)AvailabilityWithSpandpColumn.LeadTimeForMeetingRoom)))?null:(System.Int64?)reader[((int)AvailabilityWithSpandpColumn.LeadTimeForMeetingRoom)];
			//entity.LeadTimeForMeetingRoom = (Convert.IsDBNull(reader["LeadTimeForMeetingRoom"]))?(long)0:(System.Int64?)reader["LeadTimeForMeetingRoom"];
			entity.MeetingRoomPercent = (reader.IsDBNull(((int)AvailabilityWithSpandpColumn.MeetingRoomPercent)))?null:(System.Decimal?)reader[((int)AvailabilityWithSpandpColumn.MeetingRoomPercent)];
			//entity.MeetingRoomPercent = (Convert.IsDBNull(reader["MeetingRoomPercent"]))?0.0m:(System.Decimal?)reader["MeetingRoomPercent"];
			entity.DdrPercent = (reader.IsDBNull(((int)AvailabilityWithSpandpColumn.DdrPercent)))?null:(System.Decimal?)reader[((int)AvailabilityWithSpandpColumn.DdrPercent)];
			//entity.DdrPercent = (Convert.IsDBNull(reader["DDRPercent"]))?0.0m:(System.Decimal?)reader["DDRPercent"];
			entity.FullProrertyStatusBr = (reader.IsDBNull(((int)AvailabilityWithSpandpColumn.FullProrertyStatusBr)))?null:(System.Int32?)reader[((int)AvailabilityWithSpandpColumn.FullProrertyStatusBr)];
			//entity.FullProrertyStatusBr = (Convert.IsDBNull(reader["FullProrertyStatusBR"]))?(int)0:(System.Int32?)reader["FullProrertyStatusBR"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="AvailabilityWithSpandp"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="AvailabilityWithSpandp"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, AvailabilityWithSpandp entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (Convert.IsDBNull(dataRow["Id"]))?(long)0:(System.Int64)dataRow["Id"];
			entity.HotelId = (Convert.IsDBNull(dataRow["HotelID"]))?(long)0:(System.Int64)dataRow["HotelID"];
			entity.AvailabilityDate = (Convert.IsDBNull(dataRow["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["AvailabilityDate"];
			entity.FullPropertyStatus = (Convert.IsDBNull(dataRow["FullPropertyStatus"]))?(int)0:(System.Int32?)dataRow["FullPropertyStatus"];
			entity.FullPropStatus = (Convert.IsDBNull(dataRow["FullPropStatus"]))?string.Empty:(System.String)dataRow["FullPropStatus"];
			entity.LeadTimeForMeetingRoom = (Convert.IsDBNull(dataRow["LeadTimeForMeetingRoom"]))?(long)0:(System.Int64?)dataRow["LeadTimeForMeetingRoom"];
			entity.MeetingRoomPercent = (Convert.IsDBNull(dataRow["MeetingRoomPercent"]))?0.0m:(System.Decimal?)dataRow["MeetingRoomPercent"];
			entity.DdrPercent = (Convert.IsDBNull(dataRow["DDRPercent"]))?0.0m:(System.Decimal?)dataRow["DDRPercent"];
			entity.FullProrertyStatusBr = (Convert.IsDBNull(dataRow["FullProrertyStatusBR"]))?(int)0:(System.Int32?)dataRow["FullProrertyStatusBR"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region AvailabilityWithSpandpFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AvailabilityWithSpandp"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AvailabilityWithSpandpFilterBuilder : SqlFilterBuilder<AvailabilityWithSpandpColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AvailabilityWithSpandpFilterBuilder class.
		/// </summary>
		public AvailabilityWithSpandpFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AvailabilityWithSpandpFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AvailabilityWithSpandpFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AvailabilityWithSpandpFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AvailabilityWithSpandpFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AvailabilityWithSpandpFilterBuilder

	#region AvailabilityWithSpandpParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AvailabilityWithSpandp"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AvailabilityWithSpandpParameterBuilder : ParameterizedSqlFilterBuilder<AvailabilityWithSpandpColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AvailabilityWithSpandpParameterBuilder class.
		/// </summary>
		public AvailabilityWithSpandpParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AvailabilityWithSpandpParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AvailabilityWithSpandpParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AvailabilityWithSpandpParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AvailabilityWithSpandpParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AvailabilityWithSpandpParameterBuilder
	
	#region AvailabilityWithSpandpSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AvailabilityWithSpandp"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AvailabilityWithSpandpSortBuilder : SqlSortBuilder<AvailabilityWithSpandpColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AvailabilityWithSpandpSqlSortBuilder class.
		/// </summary>
		public AvailabilityWithSpandpSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AvailabilityWithSpandpSortBuilder

} // end namespace
