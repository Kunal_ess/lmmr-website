﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewReportOnLineInventoryReportProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewReportOnLineInventoryReportProviderBaseCore : EntityViewProviderBase<ViewReportOnLineInventoryReport>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewReportOnLineInventoryReport&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewReportOnLineInventoryReport&gt;"/></returns>
		protected static VList&lt;ViewReportOnLineInventoryReport&gt; Fill(DataSet dataSet, VList<ViewReportOnLineInventoryReport> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewReportOnLineInventoryReport>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewReportOnLineInventoryReport&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewReportOnLineInventoryReport>"/></returns>
		protected static VList&lt;ViewReportOnLineInventoryReport&gt; Fill(DataTable dataTable, VList<ViewReportOnLineInventoryReport> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewReportOnLineInventoryReport c = new ViewReportOnLineInventoryReport();
					c.CountryId = (Convert.IsDBNull(row["CountryID"]))?(long)0:(System.Int64)row["CountryID"];
					c.CountryName = (Convert.IsDBNull(row["CountryName"]))?string.Empty:(System.String)row["CountryName"];
					c.CityId = (Convert.IsDBNull(row["CityID"]))?(long)0:(System.Int64)row["CityID"];
					c.City = (Convert.IsDBNull(row["City"]))?string.Empty:(System.String)row["City"];
					c.Id = (Convert.IsDBNull(row["Id"]))?(long)0:(System.Int64)row["Id"];
					c.Name = (Convert.IsDBNull(row["Name"]))?string.Empty:(System.String)row["Name"];
					c.AvailabilityDate = (Convert.IsDBNull(row["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)row["AvailabilityDate"];
					c.MeetingroomCount = (Convert.IsDBNull(row["MeetingroomCount"]))?(int)0:(System.Int32?)row["MeetingroomCount"];
					c.BookedMeetingRoom = (Convert.IsDBNull(row["BookedMeetingRoom"]))?0.0m:(System.Decimal?)row["BookedMeetingRoom"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewReportOnLineInventoryReport&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewReportOnLineInventoryReport&gt;"/></returns>
		protected VList<ViewReportOnLineInventoryReport> Fill(IDataReader reader, VList<ViewReportOnLineInventoryReport> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewReportOnLineInventoryReport entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewReportOnLineInventoryReport>("ViewReportOnLineInventoryReport",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewReportOnLineInventoryReport();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CountryId = (System.Int64)reader[((int)ViewReportOnLineInventoryReportColumn.CountryId)];
					//entity.CountryId = (Convert.IsDBNull(reader["CountryID"]))?(long)0:(System.Int64)reader["CountryID"];
					entity.CountryName = (reader.IsDBNull(((int)ViewReportOnLineInventoryReportColumn.CountryName)))?null:(System.String)reader[((int)ViewReportOnLineInventoryReportColumn.CountryName)];
					//entity.CountryName = (Convert.IsDBNull(reader["CountryName"]))?string.Empty:(System.String)reader["CountryName"];
					entity.CityId = (System.Int64)reader[((int)ViewReportOnLineInventoryReportColumn.CityId)];
					//entity.CityId = (Convert.IsDBNull(reader["CityID"]))?(long)0:(System.Int64)reader["CityID"];
					entity.City = (reader.IsDBNull(((int)ViewReportOnLineInventoryReportColumn.City)))?null:(System.String)reader[((int)ViewReportOnLineInventoryReportColumn.City)];
					//entity.City = (Convert.IsDBNull(reader["City"]))?string.Empty:(System.String)reader["City"];
					entity.Id = (System.Int64)reader[((int)ViewReportOnLineInventoryReportColumn.Id)];
					//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
					entity.Name = (reader.IsDBNull(((int)ViewReportOnLineInventoryReportColumn.Name)))?null:(System.String)reader[((int)ViewReportOnLineInventoryReportColumn.Name)];
					//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
					entity.AvailabilityDate = (reader.IsDBNull(((int)ViewReportOnLineInventoryReportColumn.AvailabilityDate)))?null:(System.DateTime?)reader[((int)ViewReportOnLineInventoryReportColumn.AvailabilityDate)];
					//entity.AvailabilityDate = (Convert.IsDBNull(reader["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)reader["AvailabilityDate"];
					entity.MeetingroomCount = (reader.IsDBNull(((int)ViewReportOnLineInventoryReportColumn.MeetingroomCount)))?null:(System.Int32?)reader[((int)ViewReportOnLineInventoryReportColumn.MeetingroomCount)];
					//entity.MeetingroomCount = (Convert.IsDBNull(reader["MeetingroomCount"]))?(int)0:(System.Int32?)reader["MeetingroomCount"];
					entity.BookedMeetingRoom = (reader.IsDBNull(((int)ViewReportOnLineInventoryReportColumn.BookedMeetingRoom)))?null:(System.Decimal?)reader[((int)ViewReportOnLineInventoryReportColumn.BookedMeetingRoom)];
					//entity.BookedMeetingRoom = (Convert.IsDBNull(reader["BookedMeetingRoom"]))?0.0m:(System.Decimal?)reader["BookedMeetingRoom"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewReportOnLineInventoryReport"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewReportOnLineInventoryReport"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewReportOnLineInventoryReport entity)
		{
			reader.Read();
			entity.CountryId = (System.Int64)reader[((int)ViewReportOnLineInventoryReportColumn.CountryId)];
			//entity.CountryId = (Convert.IsDBNull(reader["CountryID"]))?(long)0:(System.Int64)reader["CountryID"];
			entity.CountryName = (reader.IsDBNull(((int)ViewReportOnLineInventoryReportColumn.CountryName)))?null:(System.String)reader[((int)ViewReportOnLineInventoryReportColumn.CountryName)];
			//entity.CountryName = (Convert.IsDBNull(reader["CountryName"]))?string.Empty:(System.String)reader["CountryName"];
			entity.CityId = (System.Int64)reader[((int)ViewReportOnLineInventoryReportColumn.CityId)];
			//entity.CityId = (Convert.IsDBNull(reader["CityID"]))?(long)0:(System.Int64)reader["CityID"];
			entity.City = (reader.IsDBNull(((int)ViewReportOnLineInventoryReportColumn.City)))?null:(System.String)reader[((int)ViewReportOnLineInventoryReportColumn.City)];
			//entity.City = (Convert.IsDBNull(reader["City"]))?string.Empty:(System.String)reader["City"];
			entity.Id = (System.Int64)reader[((int)ViewReportOnLineInventoryReportColumn.Id)];
			//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
			entity.Name = (reader.IsDBNull(((int)ViewReportOnLineInventoryReportColumn.Name)))?null:(System.String)reader[((int)ViewReportOnLineInventoryReportColumn.Name)];
			//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
			entity.AvailabilityDate = (reader.IsDBNull(((int)ViewReportOnLineInventoryReportColumn.AvailabilityDate)))?null:(System.DateTime?)reader[((int)ViewReportOnLineInventoryReportColumn.AvailabilityDate)];
			//entity.AvailabilityDate = (Convert.IsDBNull(reader["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)reader["AvailabilityDate"];
			entity.MeetingroomCount = (reader.IsDBNull(((int)ViewReportOnLineInventoryReportColumn.MeetingroomCount)))?null:(System.Int32?)reader[((int)ViewReportOnLineInventoryReportColumn.MeetingroomCount)];
			//entity.MeetingroomCount = (Convert.IsDBNull(reader["MeetingroomCount"]))?(int)0:(System.Int32?)reader["MeetingroomCount"];
			entity.BookedMeetingRoom = (reader.IsDBNull(((int)ViewReportOnLineInventoryReportColumn.BookedMeetingRoom)))?null:(System.Decimal?)reader[((int)ViewReportOnLineInventoryReportColumn.BookedMeetingRoom)];
			//entity.BookedMeetingRoom = (Convert.IsDBNull(reader["BookedMeetingRoom"]))?0.0m:(System.Decimal?)reader["BookedMeetingRoom"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewReportOnLineInventoryReport"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewReportOnLineInventoryReport"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewReportOnLineInventoryReport entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CountryId = (Convert.IsDBNull(dataRow["CountryID"]))?(long)0:(System.Int64)dataRow["CountryID"];
			entity.CountryName = (Convert.IsDBNull(dataRow["CountryName"]))?string.Empty:(System.String)dataRow["CountryName"];
			entity.CityId = (Convert.IsDBNull(dataRow["CityID"]))?(long)0:(System.Int64)dataRow["CityID"];
			entity.City = (Convert.IsDBNull(dataRow["City"]))?string.Empty:(System.String)dataRow["City"];
			entity.Id = (Convert.IsDBNull(dataRow["Id"]))?(long)0:(System.Int64)dataRow["Id"];
			entity.Name = (Convert.IsDBNull(dataRow["Name"]))?string.Empty:(System.String)dataRow["Name"];
			entity.AvailabilityDate = (Convert.IsDBNull(dataRow["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["AvailabilityDate"];
			entity.MeetingroomCount = (Convert.IsDBNull(dataRow["MeetingroomCount"]))?(int)0:(System.Int32?)dataRow["MeetingroomCount"];
			entity.BookedMeetingRoom = (Convert.IsDBNull(dataRow["BookedMeetingRoom"]))?0.0m:(System.Decimal?)dataRow["BookedMeetingRoom"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewReportOnLineInventoryReportFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportOnLineInventoryReport"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportOnLineInventoryReportFilterBuilder : SqlFilterBuilder<ViewReportOnLineInventoryReportColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportOnLineInventoryReportFilterBuilder class.
		/// </summary>
		public ViewReportOnLineInventoryReportFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportOnLineInventoryReportFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportOnLineInventoryReportFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportOnLineInventoryReportFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportOnLineInventoryReportFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportOnLineInventoryReportFilterBuilder

	#region ViewReportOnLineInventoryReportParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportOnLineInventoryReport"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportOnLineInventoryReportParameterBuilder : ParameterizedSqlFilterBuilder<ViewReportOnLineInventoryReportColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportOnLineInventoryReportParameterBuilder class.
		/// </summary>
		public ViewReportOnLineInventoryReportParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportOnLineInventoryReportParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportOnLineInventoryReportParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportOnLineInventoryReportParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportOnLineInventoryReportParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportOnLineInventoryReportParameterBuilder
	
	#region ViewReportOnLineInventoryReportSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportOnLineInventoryReport"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewReportOnLineInventoryReportSortBuilder : SqlSortBuilder<ViewReportOnLineInventoryReportColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportOnLineInventoryReportSqlSortBuilder class.
		/// </summary>
		public ViewReportOnLineInventoryReportSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewReportOnLineInventoryReportSortBuilder

} // end namespace
