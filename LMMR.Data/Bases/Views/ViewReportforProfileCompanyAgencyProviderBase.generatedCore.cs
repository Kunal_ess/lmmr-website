﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewReportforProfileCompanyAgencyProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewReportforProfileCompanyAgencyProviderBaseCore : EntityViewProviderBase<ViewReportforProfileCompanyAgency>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewReportforProfileCompanyAgency&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewReportforProfileCompanyAgency&gt;"/></returns>
		protected static VList&lt;ViewReportforProfileCompanyAgency&gt; Fill(DataSet dataSet, VList<ViewReportforProfileCompanyAgency> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewReportforProfileCompanyAgency>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewReportforProfileCompanyAgency&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewReportforProfileCompanyAgency>"/></returns>
		protected static VList&lt;ViewReportforProfileCompanyAgency&gt; Fill(DataTable dataTable, VList<ViewReportforProfileCompanyAgency> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewReportforProfileCompanyAgency c = new ViewReportforProfileCompanyAgency();
					c.UserId = (Convert.IsDBNull(row["UserId"]))?(long)0:(System.Int64)row["UserId"];
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.FirstName = (Convert.IsDBNull(row["FirstName"]))?string.Empty:(System.String)row["FirstName"];
					c.LastName = (Convert.IsDBNull(row["LastName"]))?string.Empty:(System.String)row["LastName"];
					c.Usertype = (Convert.IsDBNull(row["Usertype"]))?(int)0:(System.Int32?)row["Usertype"];
					c.CountryId = (Convert.IsDBNull(row["CountryId"]))?(long)0:(System.Int64?)row["CountryId"];
					c.CityId = (Convert.IsDBNull(row["CityId"]))?(long)0:(System.Int64?)row["CityId"];
					c.CityName = (Convert.IsDBNull(row["CityName"]))?string.Empty:(System.String)row["CityName"];
					c.PostalCode = (Convert.IsDBNull(row["PostalCode"]))?string.Empty:(System.String)row["PostalCode"];
					c.Address = (Convert.IsDBNull(row["Address"]))?string.Empty:(System.String)row["Address"];
					c.EmailId = (Convert.IsDBNull(row["EmailId"]))?string.Empty:(System.String)row["EmailId"];
					c.Phone = (Convert.IsDBNull(row["Phone"]))?string.Empty:(System.String)row["Phone"];
					c.CreatedDate = (Convert.IsDBNull(row["CreatedDate"]))?DateTime.MinValue:(System.DateTime?)row["CreatedDate"];
					c.LastLogin = (Convert.IsDBNull(row["LastLogin"]))?DateTime.MinValue:(System.DateTime?)row["LastLogin"];
					c.ActivationDate = (Convert.IsDBNull(row["ActivationDate"]))?DateTime.MinValue:(System.DateTime?)row["ActivationDate"];
					c.Name = (Convert.IsDBNull(row["Name"]))?string.Empty:(System.String)row["Name"];
					c.ParentId = (Convert.IsDBNull(row["ParentID"]))?(long)0:(System.Int64?)row["ParentID"];
					c.HotelId = (Convert.IsDBNull(row["HotelId"]))?(long)0:(System.Int64)row["HotelId"];
					c.BookType = (Convert.IsDBNull(row["BookType"]))?(int)0:(System.Int32?)row["BookType"];
					c.Id = (Convert.IsDBNull(row["Id"]))?(long)0:(System.Int64)row["Id"];
					c.BookingDate = (Convert.IsDBNull(row["BookingDate"]))?DateTime.MinValue:(System.DateTime?)row["BookingDate"];
					c.ArrivalDate = (Convert.IsDBNull(row["ArrivalDate"]))?DateTime.MinValue:(System.DateTime?)row["ArrivalDate"];
					c.DepartureDate = (Convert.IsDBNull(row["DepartureDate"]))?DateTime.MinValue:(System.DateTime?)row["DepartureDate"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewReportforProfileCompanyAgency&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewReportforProfileCompanyAgency&gt;"/></returns>
		protected VList<ViewReportforProfileCompanyAgency> Fill(IDataReader reader, VList<ViewReportforProfileCompanyAgency> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewReportforProfileCompanyAgency entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewReportforProfileCompanyAgency>("ViewReportforProfileCompanyAgency",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewReportforProfileCompanyAgency();
					}
					
					entity.SuppressEntityEvents = true;

					entity.UserId = (System.Int64)reader[((int)ViewReportforProfileCompanyAgencyColumn.UserId)];
					//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(long)0:(System.Int64)reader["UserId"];
					entity.CompanyName = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.CompanyName)))?null:(System.String)reader[((int)ViewReportforProfileCompanyAgencyColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.FirstName = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.FirstName)))?null:(System.String)reader[((int)ViewReportforProfileCompanyAgencyColumn.FirstName)];
					//entity.FirstName = (Convert.IsDBNull(reader["FirstName"]))?string.Empty:(System.String)reader["FirstName"];
					entity.LastName = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.LastName)))?null:(System.String)reader[((int)ViewReportforProfileCompanyAgencyColumn.LastName)];
					//entity.LastName = (Convert.IsDBNull(reader["LastName"]))?string.Empty:(System.String)reader["LastName"];
					entity.Usertype = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.Usertype)))?null:(System.Int32?)reader[((int)ViewReportforProfileCompanyAgencyColumn.Usertype)];
					//entity.Usertype = (Convert.IsDBNull(reader["Usertype"]))?(int)0:(System.Int32?)reader["Usertype"];
					entity.CountryId = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.CountryId)))?null:(System.Int64?)reader[((int)ViewReportforProfileCompanyAgencyColumn.CountryId)];
					//entity.CountryId = (Convert.IsDBNull(reader["CountryId"]))?(long)0:(System.Int64?)reader["CountryId"];
					entity.CityId = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.CityId)))?null:(System.Int64?)reader[((int)ViewReportforProfileCompanyAgencyColumn.CityId)];
					//entity.CityId = (Convert.IsDBNull(reader["CityId"]))?(long)0:(System.Int64?)reader["CityId"];
					entity.CityName = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.CityName)))?null:(System.String)reader[((int)ViewReportforProfileCompanyAgencyColumn.CityName)];
					//entity.CityName = (Convert.IsDBNull(reader["CityName"]))?string.Empty:(System.String)reader["CityName"];
					entity.PostalCode = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.PostalCode)))?null:(System.String)reader[((int)ViewReportforProfileCompanyAgencyColumn.PostalCode)];
					//entity.PostalCode = (Convert.IsDBNull(reader["PostalCode"]))?string.Empty:(System.String)reader["PostalCode"];
					entity.Address = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.Address)))?null:(System.String)reader[((int)ViewReportforProfileCompanyAgencyColumn.Address)];
					//entity.Address = (Convert.IsDBNull(reader["Address"]))?string.Empty:(System.String)reader["Address"];
					entity.EmailId = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.EmailId)))?null:(System.String)reader[((int)ViewReportforProfileCompanyAgencyColumn.EmailId)];
					//entity.EmailId = (Convert.IsDBNull(reader["EmailId"]))?string.Empty:(System.String)reader["EmailId"];
					entity.Phone = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.Phone)))?null:(System.String)reader[((int)ViewReportforProfileCompanyAgencyColumn.Phone)];
					//entity.Phone = (Convert.IsDBNull(reader["Phone"]))?string.Empty:(System.String)reader["Phone"];
					entity.CreatedDate = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.CreatedDate)))?null:(System.DateTime?)reader[((int)ViewReportforProfileCompanyAgencyColumn.CreatedDate)];
					//entity.CreatedDate = (Convert.IsDBNull(reader["CreatedDate"]))?DateTime.MinValue:(System.DateTime?)reader["CreatedDate"];
					entity.LastLogin = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.LastLogin)))?null:(System.DateTime?)reader[((int)ViewReportforProfileCompanyAgencyColumn.LastLogin)];
					//entity.LastLogin = (Convert.IsDBNull(reader["LastLogin"]))?DateTime.MinValue:(System.DateTime?)reader["LastLogin"];
					entity.ActivationDate = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.ActivationDate)))?null:(System.DateTime?)reader[((int)ViewReportforProfileCompanyAgencyColumn.ActivationDate)];
					//entity.ActivationDate = (Convert.IsDBNull(reader["ActivationDate"]))?DateTime.MinValue:(System.DateTime?)reader["ActivationDate"];
					entity.Name = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.Name)))?null:(System.String)reader[((int)ViewReportforProfileCompanyAgencyColumn.Name)];
					//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
					entity.ParentId = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.ParentId)))?null:(System.Int64?)reader[((int)ViewReportforProfileCompanyAgencyColumn.ParentId)];
					//entity.ParentId = (Convert.IsDBNull(reader["ParentID"]))?(long)0:(System.Int64?)reader["ParentID"];
					entity.HotelId = (System.Int64)reader[((int)ViewReportforProfileCompanyAgencyColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
					entity.BookType = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.BookType)))?null:(System.Int32?)reader[((int)ViewReportforProfileCompanyAgencyColumn.BookType)];
					//entity.BookType = (Convert.IsDBNull(reader["BookType"]))?(int)0:(System.Int32?)reader["BookType"];
					entity.Id = (System.Int64)reader[((int)ViewReportforProfileCompanyAgencyColumn.Id)];
					//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
					entity.BookingDate = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.BookingDate)))?null:(System.DateTime?)reader[((int)ViewReportforProfileCompanyAgencyColumn.BookingDate)];
					//entity.BookingDate = (Convert.IsDBNull(reader["BookingDate"]))?DateTime.MinValue:(System.DateTime?)reader["BookingDate"];
					entity.ArrivalDate = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.ArrivalDate)))?null:(System.DateTime?)reader[((int)ViewReportforProfileCompanyAgencyColumn.ArrivalDate)];
					//entity.ArrivalDate = (Convert.IsDBNull(reader["ArrivalDate"]))?DateTime.MinValue:(System.DateTime?)reader["ArrivalDate"];
					entity.DepartureDate = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.DepartureDate)))?null:(System.DateTime?)reader[((int)ViewReportforProfileCompanyAgencyColumn.DepartureDate)];
					//entity.DepartureDate = (Convert.IsDBNull(reader["DepartureDate"]))?DateTime.MinValue:(System.DateTime?)reader["DepartureDate"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewReportforProfileCompanyAgency"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewReportforProfileCompanyAgency"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewReportforProfileCompanyAgency entity)
		{
			reader.Read();
			entity.UserId = (System.Int64)reader[((int)ViewReportforProfileCompanyAgencyColumn.UserId)];
			//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(long)0:(System.Int64)reader["UserId"];
			entity.CompanyName = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.CompanyName)))?null:(System.String)reader[((int)ViewReportforProfileCompanyAgencyColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.FirstName = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.FirstName)))?null:(System.String)reader[((int)ViewReportforProfileCompanyAgencyColumn.FirstName)];
			//entity.FirstName = (Convert.IsDBNull(reader["FirstName"]))?string.Empty:(System.String)reader["FirstName"];
			entity.LastName = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.LastName)))?null:(System.String)reader[((int)ViewReportforProfileCompanyAgencyColumn.LastName)];
			//entity.LastName = (Convert.IsDBNull(reader["LastName"]))?string.Empty:(System.String)reader["LastName"];
			entity.Usertype = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.Usertype)))?null:(System.Int32?)reader[((int)ViewReportforProfileCompanyAgencyColumn.Usertype)];
			//entity.Usertype = (Convert.IsDBNull(reader["Usertype"]))?(int)0:(System.Int32?)reader["Usertype"];
			entity.CountryId = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.CountryId)))?null:(System.Int64?)reader[((int)ViewReportforProfileCompanyAgencyColumn.CountryId)];
			//entity.CountryId = (Convert.IsDBNull(reader["CountryId"]))?(long)0:(System.Int64?)reader["CountryId"];
			entity.CityId = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.CityId)))?null:(System.Int64?)reader[((int)ViewReportforProfileCompanyAgencyColumn.CityId)];
			//entity.CityId = (Convert.IsDBNull(reader["CityId"]))?(long)0:(System.Int64?)reader["CityId"];
			entity.CityName = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.CityName)))?null:(System.String)reader[((int)ViewReportforProfileCompanyAgencyColumn.CityName)];
			//entity.CityName = (Convert.IsDBNull(reader["CityName"]))?string.Empty:(System.String)reader["CityName"];
			entity.PostalCode = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.PostalCode)))?null:(System.String)reader[((int)ViewReportforProfileCompanyAgencyColumn.PostalCode)];
			//entity.PostalCode = (Convert.IsDBNull(reader["PostalCode"]))?string.Empty:(System.String)reader["PostalCode"];
			entity.Address = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.Address)))?null:(System.String)reader[((int)ViewReportforProfileCompanyAgencyColumn.Address)];
			//entity.Address = (Convert.IsDBNull(reader["Address"]))?string.Empty:(System.String)reader["Address"];
			entity.EmailId = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.EmailId)))?null:(System.String)reader[((int)ViewReportforProfileCompanyAgencyColumn.EmailId)];
			//entity.EmailId = (Convert.IsDBNull(reader["EmailId"]))?string.Empty:(System.String)reader["EmailId"];
			entity.Phone = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.Phone)))?null:(System.String)reader[((int)ViewReportforProfileCompanyAgencyColumn.Phone)];
			//entity.Phone = (Convert.IsDBNull(reader["Phone"]))?string.Empty:(System.String)reader["Phone"];
			entity.CreatedDate = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.CreatedDate)))?null:(System.DateTime?)reader[((int)ViewReportforProfileCompanyAgencyColumn.CreatedDate)];
			//entity.CreatedDate = (Convert.IsDBNull(reader["CreatedDate"]))?DateTime.MinValue:(System.DateTime?)reader["CreatedDate"];
			entity.LastLogin = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.LastLogin)))?null:(System.DateTime?)reader[((int)ViewReportforProfileCompanyAgencyColumn.LastLogin)];
			//entity.LastLogin = (Convert.IsDBNull(reader["LastLogin"]))?DateTime.MinValue:(System.DateTime?)reader["LastLogin"];
			entity.ActivationDate = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.ActivationDate)))?null:(System.DateTime?)reader[((int)ViewReportforProfileCompanyAgencyColumn.ActivationDate)];
			//entity.ActivationDate = (Convert.IsDBNull(reader["ActivationDate"]))?DateTime.MinValue:(System.DateTime?)reader["ActivationDate"];
			entity.Name = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.Name)))?null:(System.String)reader[((int)ViewReportforProfileCompanyAgencyColumn.Name)];
			//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
			entity.ParentId = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.ParentId)))?null:(System.Int64?)reader[((int)ViewReportforProfileCompanyAgencyColumn.ParentId)];
			//entity.ParentId = (Convert.IsDBNull(reader["ParentID"]))?(long)0:(System.Int64?)reader["ParentID"];
			entity.HotelId = (System.Int64)reader[((int)ViewReportforProfileCompanyAgencyColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
			entity.BookType = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.BookType)))?null:(System.Int32?)reader[((int)ViewReportforProfileCompanyAgencyColumn.BookType)];
			//entity.BookType = (Convert.IsDBNull(reader["BookType"]))?(int)0:(System.Int32?)reader["BookType"];
			entity.Id = (System.Int64)reader[((int)ViewReportforProfileCompanyAgencyColumn.Id)];
			//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
			entity.BookingDate = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.BookingDate)))?null:(System.DateTime?)reader[((int)ViewReportforProfileCompanyAgencyColumn.BookingDate)];
			//entity.BookingDate = (Convert.IsDBNull(reader["BookingDate"]))?DateTime.MinValue:(System.DateTime?)reader["BookingDate"];
			entity.ArrivalDate = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.ArrivalDate)))?null:(System.DateTime?)reader[((int)ViewReportforProfileCompanyAgencyColumn.ArrivalDate)];
			//entity.ArrivalDate = (Convert.IsDBNull(reader["ArrivalDate"]))?DateTime.MinValue:(System.DateTime?)reader["ArrivalDate"];
			entity.DepartureDate = (reader.IsDBNull(((int)ViewReportforProfileCompanyAgencyColumn.DepartureDate)))?null:(System.DateTime?)reader[((int)ViewReportforProfileCompanyAgencyColumn.DepartureDate)];
			//entity.DepartureDate = (Convert.IsDBNull(reader["DepartureDate"]))?DateTime.MinValue:(System.DateTime?)reader["DepartureDate"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewReportforProfileCompanyAgency"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewReportforProfileCompanyAgency"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewReportforProfileCompanyAgency entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.UserId = (Convert.IsDBNull(dataRow["UserId"]))?(long)0:(System.Int64)dataRow["UserId"];
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.FirstName = (Convert.IsDBNull(dataRow["FirstName"]))?string.Empty:(System.String)dataRow["FirstName"];
			entity.LastName = (Convert.IsDBNull(dataRow["LastName"]))?string.Empty:(System.String)dataRow["LastName"];
			entity.Usertype = (Convert.IsDBNull(dataRow["Usertype"]))?(int)0:(System.Int32?)dataRow["Usertype"];
			entity.CountryId = (Convert.IsDBNull(dataRow["CountryId"]))?(long)0:(System.Int64?)dataRow["CountryId"];
			entity.CityId = (Convert.IsDBNull(dataRow["CityId"]))?(long)0:(System.Int64?)dataRow["CityId"];
			entity.CityName = (Convert.IsDBNull(dataRow["CityName"]))?string.Empty:(System.String)dataRow["CityName"];
			entity.PostalCode = (Convert.IsDBNull(dataRow["PostalCode"]))?string.Empty:(System.String)dataRow["PostalCode"];
			entity.Address = (Convert.IsDBNull(dataRow["Address"]))?string.Empty:(System.String)dataRow["Address"];
			entity.EmailId = (Convert.IsDBNull(dataRow["EmailId"]))?string.Empty:(System.String)dataRow["EmailId"];
			entity.Phone = (Convert.IsDBNull(dataRow["Phone"]))?string.Empty:(System.String)dataRow["Phone"];
			entity.CreatedDate = (Convert.IsDBNull(dataRow["CreatedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["CreatedDate"];
			entity.LastLogin = (Convert.IsDBNull(dataRow["LastLogin"]))?DateTime.MinValue:(System.DateTime?)dataRow["LastLogin"];
			entity.ActivationDate = (Convert.IsDBNull(dataRow["ActivationDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["ActivationDate"];
			entity.Name = (Convert.IsDBNull(dataRow["Name"]))?string.Empty:(System.String)dataRow["Name"];
			entity.ParentId = (Convert.IsDBNull(dataRow["ParentID"]))?(long)0:(System.Int64?)dataRow["ParentID"];
			entity.HotelId = (Convert.IsDBNull(dataRow["HotelId"]))?(long)0:(System.Int64)dataRow["HotelId"];
			entity.BookType = (Convert.IsDBNull(dataRow["BookType"]))?(int)0:(System.Int32?)dataRow["BookType"];
			entity.Id = (Convert.IsDBNull(dataRow["Id"]))?(long)0:(System.Int64)dataRow["Id"];
			entity.BookingDate = (Convert.IsDBNull(dataRow["BookingDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["BookingDate"];
			entity.ArrivalDate = (Convert.IsDBNull(dataRow["ArrivalDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["ArrivalDate"];
			entity.DepartureDate = (Convert.IsDBNull(dataRow["DepartureDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["DepartureDate"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewReportforProfileCompanyAgencyFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportforProfileCompanyAgency"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportforProfileCompanyAgencyFilterBuilder : SqlFilterBuilder<ViewReportforProfileCompanyAgencyColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportforProfileCompanyAgencyFilterBuilder class.
		/// </summary>
		public ViewReportforProfileCompanyAgencyFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportforProfileCompanyAgencyFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportforProfileCompanyAgencyFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportforProfileCompanyAgencyFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportforProfileCompanyAgencyFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportforProfileCompanyAgencyFilterBuilder

	#region ViewReportforProfileCompanyAgencyParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportforProfileCompanyAgency"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportforProfileCompanyAgencyParameterBuilder : ParameterizedSqlFilterBuilder<ViewReportforProfileCompanyAgencyColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportforProfileCompanyAgencyParameterBuilder class.
		/// </summary>
		public ViewReportforProfileCompanyAgencyParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportforProfileCompanyAgencyParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportforProfileCompanyAgencyParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportforProfileCompanyAgencyParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportforProfileCompanyAgencyParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportforProfileCompanyAgencyParameterBuilder
	
	#region ViewReportforProfileCompanyAgencySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportforProfileCompanyAgency"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewReportforProfileCompanyAgencySortBuilder : SqlSortBuilder<ViewReportforProfileCompanyAgencyColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportforProfileCompanyAgencySqlSortBuilder class.
		/// </summary>
		public ViewReportforProfileCompanyAgencySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewReportforProfileCompanyAgencySortBuilder

} // end namespace
