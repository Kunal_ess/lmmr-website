﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewAvailabilityOfRoomsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewAvailabilityOfRoomsProviderBaseCore : EntityViewProviderBase<ViewAvailabilityOfRooms>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewAvailabilityOfRooms&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewAvailabilityOfRooms&gt;"/></returns>
		protected static VList&lt;ViewAvailabilityOfRooms&gt; Fill(DataSet dataSet, VList<ViewAvailabilityOfRooms> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewAvailabilityOfRooms>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewAvailabilityOfRooms&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewAvailabilityOfRooms>"/></returns>
		protected static VList&lt;ViewAvailabilityOfRooms&gt; Fill(DataTable dataTable, VList<ViewAvailabilityOfRooms> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewAvailabilityOfRooms c = new ViewAvailabilityOfRooms();
					c.Id = (Convert.IsDBNull(row["Id"]))?(long)0:(System.Int64)row["Id"];
					c.AvailibilityIdForBedRoom = (Convert.IsDBNull(row["AvailibilityIdForBedRoom"]))?(long)0:(System.Int64)row["AvailibilityIdForBedRoom"];
					c.RoomId = (Convert.IsDBNull(row["RoomId"]))?(long)0:(System.Int64)row["RoomId"];
					c.RoomType = (Convert.IsDBNull(row["RoomType"]))?(int)0:(System.Int32?)row["RoomType"];
					c.MorningStatus = (Convert.IsDBNull(row["MorningStatus"]))?(int)0:(System.Int32?)row["MorningStatus"];
					c.AfternoonStatus = (Convert.IsDBNull(row["AfternoonStatus"]))?(int)0:(System.Int32?)row["AfternoonStatus"];
					c.NumberOfRoomsAvailable = (Convert.IsDBNull(row["NumberOfRoomsAvailable"]))?(long)0:(System.Int64?)row["NumberOfRoomsAvailable"];
					c.HotelId = (Convert.IsDBNull(row["Hotel_Id"]))?(long)0:(System.Int64)row["Hotel_Id"];
					c.Name = (Convert.IsDBNull(row["Name"]))?string.Empty:(System.String)row["Name"];
					c.AvailabilityDate = (Convert.IsDBNull(row["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)row["AvailabilityDate"];
					c.NumberOfRoomBooked = (Convert.IsDBNull(row["NumberOfRoomBooked"]))?(long)0:(System.Int64?)row["NumberOfRoomBooked"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewAvailabilityOfRooms&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewAvailabilityOfRooms&gt;"/></returns>
		protected VList<ViewAvailabilityOfRooms> Fill(IDataReader reader, VList<ViewAvailabilityOfRooms> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewAvailabilityOfRooms entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewAvailabilityOfRooms>("ViewAvailabilityOfRooms",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewAvailabilityOfRooms();
					}
					
					entity.SuppressEntityEvents = true;

					entity.Id = (System.Int64)reader[((int)ViewAvailabilityOfRoomsColumn.Id)];
					//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
					entity.AvailibilityIdForBedRoom = (System.Int64)reader[((int)ViewAvailabilityOfRoomsColumn.AvailibilityIdForBedRoom)];
					//entity.AvailibilityIdForBedRoom = (Convert.IsDBNull(reader["AvailibilityIdForBedRoom"]))?(long)0:(System.Int64)reader["AvailibilityIdForBedRoom"];
					entity.RoomId = (System.Int64)reader[((int)ViewAvailabilityOfRoomsColumn.RoomId)];
					//entity.RoomId = (Convert.IsDBNull(reader["RoomId"]))?(long)0:(System.Int64)reader["RoomId"];
					entity.RoomType = (reader.IsDBNull(((int)ViewAvailabilityOfRoomsColumn.RoomType)))?null:(System.Int32?)reader[((int)ViewAvailabilityOfRoomsColumn.RoomType)];
					//entity.RoomType = (Convert.IsDBNull(reader["RoomType"]))?(int)0:(System.Int32?)reader["RoomType"];
					entity.MorningStatus = (reader.IsDBNull(((int)ViewAvailabilityOfRoomsColumn.MorningStatus)))?null:(System.Int32?)reader[((int)ViewAvailabilityOfRoomsColumn.MorningStatus)];
					//entity.MorningStatus = (Convert.IsDBNull(reader["MorningStatus"]))?(int)0:(System.Int32?)reader["MorningStatus"];
					entity.AfternoonStatus = (reader.IsDBNull(((int)ViewAvailabilityOfRoomsColumn.AfternoonStatus)))?null:(System.Int32?)reader[((int)ViewAvailabilityOfRoomsColumn.AfternoonStatus)];
					//entity.AfternoonStatus = (Convert.IsDBNull(reader["AfternoonStatus"]))?(int)0:(System.Int32?)reader["AfternoonStatus"];
					entity.NumberOfRoomsAvailable = (reader.IsDBNull(((int)ViewAvailabilityOfRoomsColumn.NumberOfRoomsAvailable)))?null:(System.Int64?)reader[((int)ViewAvailabilityOfRoomsColumn.NumberOfRoomsAvailable)];
					//entity.NumberOfRoomsAvailable = (Convert.IsDBNull(reader["NumberOfRoomsAvailable"]))?(long)0:(System.Int64?)reader["NumberOfRoomsAvailable"];
					entity.HotelId = (System.Int64)reader[((int)ViewAvailabilityOfRoomsColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["Hotel_Id"]))?(long)0:(System.Int64)reader["Hotel_Id"];
					entity.Name = (reader.IsDBNull(((int)ViewAvailabilityOfRoomsColumn.Name)))?null:(System.String)reader[((int)ViewAvailabilityOfRoomsColumn.Name)];
					//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
					entity.AvailabilityDate = (reader.IsDBNull(((int)ViewAvailabilityOfRoomsColumn.AvailabilityDate)))?null:(System.DateTime?)reader[((int)ViewAvailabilityOfRoomsColumn.AvailabilityDate)];
					//entity.AvailabilityDate = (Convert.IsDBNull(reader["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)reader["AvailabilityDate"];
					entity.NumberOfRoomBooked = (reader.IsDBNull(((int)ViewAvailabilityOfRoomsColumn.NumberOfRoomBooked)))?null:(System.Int64?)reader[((int)ViewAvailabilityOfRoomsColumn.NumberOfRoomBooked)];
					//entity.NumberOfRoomBooked = (Convert.IsDBNull(reader["NumberOfRoomBooked"]))?(long)0:(System.Int64?)reader["NumberOfRoomBooked"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewAvailabilityOfRooms"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewAvailabilityOfRooms"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewAvailabilityOfRooms entity)
		{
			reader.Read();
			entity.Id = (System.Int64)reader[((int)ViewAvailabilityOfRoomsColumn.Id)];
			//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
			entity.AvailibilityIdForBedRoom = (System.Int64)reader[((int)ViewAvailabilityOfRoomsColumn.AvailibilityIdForBedRoom)];
			//entity.AvailibilityIdForBedRoom = (Convert.IsDBNull(reader["AvailibilityIdForBedRoom"]))?(long)0:(System.Int64)reader["AvailibilityIdForBedRoom"];
			entity.RoomId = (System.Int64)reader[((int)ViewAvailabilityOfRoomsColumn.RoomId)];
			//entity.RoomId = (Convert.IsDBNull(reader["RoomId"]))?(long)0:(System.Int64)reader["RoomId"];
			entity.RoomType = (reader.IsDBNull(((int)ViewAvailabilityOfRoomsColumn.RoomType)))?null:(System.Int32?)reader[((int)ViewAvailabilityOfRoomsColumn.RoomType)];
			//entity.RoomType = (Convert.IsDBNull(reader["RoomType"]))?(int)0:(System.Int32?)reader["RoomType"];
			entity.MorningStatus = (reader.IsDBNull(((int)ViewAvailabilityOfRoomsColumn.MorningStatus)))?null:(System.Int32?)reader[((int)ViewAvailabilityOfRoomsColumn.MorningStatus)];
			//entity.MorningStatus = (Convert.IsDBNull(reader["MorningStatus"]))?(int)0:(System.Int32?)reader["MorningStatus"];
			entity.AfternoonStatus = (reader.IsDBNull(((int)ViewAvailabilityOfRoomsColumn.AfternoonStatus)))?null:(System.Int32?)reader[((int)ViewAvailabilityOfRoomsColumn.AfternoonStatus)];
			//entity.AfternoonStatus = (Convert.IsDBNull(reader["AfternoonStatus"]))?(int)0:(System.Int32?)reader["AfternoonStatus"];
			entity.NumberOfRoomsAvailable = (reader.IsDBNull(((int)ViewAvailabilityOfRoomsColumn.NumberOfRoomsAvailable)))?null:(System.Int64?)reader[((int)ViewAvailabilityOfRoomsColumn.NumberOfRoomsAvailable)];
			//entity.NumberOfRoomsAvailable = (Convert.IsDBNull(reader["NumberOfRoomsAvailable"]))?(long)0:(System.Int64?)reader["NumberOfRoomsAvailable"];
			entity.HotelId = (System.Int64)reader[((int)ViewAvailabilityOfRoomsColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["Hotel_Id"]))?(long)0:(System.Int64)reader["Hotel_Id"];
			entity.Name = (reader.IsDBNull(((int)ViewAvailabilityOfRoomsColumn.Name)))?null:(System.String)reader[((int)ViewAvailabilityOfRoomsColumn.Name)];
			//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
			entity.AvailabilityDate = (reader.IsDBNull(((int)ViewAvailabilityOfRoomsColumn.AvailabilityDate)))?null:(System.DateTime?)reader[((int)ViewAvailabilityOfRoomsColumn.AvailabilityDate)];
			//entity.AvailabilityDate = (Convert.IsDBNull(reader["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)reader["AvailabilityDate"];
			entity.NumberOfRoomBooked = (reader.IsDBNull(((int)ViewAvailabilityOfRoomsColumn.NumberOfRoomBooked)))?null:(System.Int64?)reader[((int)ViewAvailabilityOfRoomsColumn.NumberOfRoomBooked)];
			//entity.NumberOfRoomBooked = (Convert.IsDBNull(reader["NumberOfRoomBooked"]))?(long)0:(System.Int64?)reader["NumberOfRoomBooked"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewAvailabilityOfRooms"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewAvailabilityOfRooms"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewAvailabilityOfRooms entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (Convert.IsDBNull(dataRow["Id"]))?(long)0:(System.Int64)dataRow["Id"];
			entity.AvailibilityIdForBedRoom = (Convert.IsDBNull(dataRow["AvailibilityIdForBedRoom"]))?(long)0:(System.Int64)dataRow["AvailibilityIdForBedRoom"];
			entity.RoomId = (Convert.IsDBNull(dataRow["RoomId"]))?(long)0:(System.Int64)dataRow["RoomId"];
			entity.RoomType = (Convert.IsDBNull(dataRow["RoomType"]))?(int)0:(System.Int32?)dataRow["RoomType"];
			entity.MorningStatus = (Convert.IsDBNull(dataRow["MorningStatus"]))?(int)0:(System.Int32?)dataRow["MorningStatus"];
			entity.AfternoonStatus = (Convert.IsDBNull(dataRow["AfternoonStatus"]))?(int)0:(System.Int32?)dataRow["AfternoonStatus"];
			entity.NumberOfRoomsAvailable = (Convert.IsDBNull(dataRow["NumberOfRoomsAvailable"]))?(long)0:(System.Int64?)dataRow["NumberOfRoomsAvailable"];
			entity.HotelId = (Convert.IsDBNull(dataRow["Hotel_Id"]))?(long)0:(System.Int64)dataRow["Hotel_Id"];
			entity.Name = (Convert.IsDBNull(dataRow["Name"]))?string.Empty:(System.String)dataRow["Name"];
			entity.AvailabilityDate = (Convert.IsDBNull(dataRow["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["AvailabilityDate"];
			entity.NumberOfRoomBooked = (Convert.IsDBNull(dataRow["NumberOfRoomBooked"]))?(long)0:(System.Int64?)dataRow["NumberOfRoomBooked"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewAvailabilityOfRoomsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewAvailabilityOfRooms"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewAvailabilityOfRoomsFilterBuilder : SqlFilterBuilder<ViewAvailabilityOfRoomsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfRoomsFilterBuilder class.
		/// </summary>
		public ViewAvailabilityOfRoomsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfRoomsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewAvailabilityOfRoomsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfRoomsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewAvailabilityOfRoomsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewAvailabilityOfRoomsFilterBuilder

	#region ViewAvailabilityOfRoomsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewAvailabilityOfRooms"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewAvailabilityOfRoomsParameterBuilder : ParameterizedSqlFilterBuilder<ViewAvailabilityOfRoomsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfRoomsParameterBuilder class.
		/// </summary>
		public ViewAvailabilityOfRoomsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfRoomsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewAvailabilityOfRoomsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfRoomsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewAvailabilityOfRoomsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewAvailabilityOfRoomsParameterBuilder
	
	#region ViewAvailabilityOfRoomsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewAvailabilityOfRooms"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewAvailabilityOfRoomsSortBuilder : SqlSortBuilder<ViewAvailabilityOfRoomsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfRoomsSqlSortBuilder class.
		/// </summary>
		public ViewAvailabilityOfRoomsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewAvailabilityOfRoomsSortBuilder

} // end namespace
