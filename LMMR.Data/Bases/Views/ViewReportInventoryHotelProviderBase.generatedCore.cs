﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewReportInventoryHotelProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewReportInventoryHotelProviderBaseCore : EntityViewProviderBase<ViewReportInventoryHotel>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewReportInventoryHotel&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewReportInventoryHotel&gt;"/></returns>
		protected static VList&lt;ViewReportInventoryHotel&gt; Fill(DataSet dataSet, VList<ViewReportInventoryHotel> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewReportInventoryHotel>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewReportInventoryHotel&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewReportInventoryHotel>"/></returns>
		protected static VList&lt;ViewReportInventoryHotel&gt; Fill(DataTable dataTable, VList<ViewReportInventoryHotel> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewReportInventoryHotel c = new ViewReportInventoryHotel();
					c.Id = (Convert.IsDBNull(row["Id"]))?(long)0:(System.Int64)row["Id"];
					c.Name = (Convert.IsDBNull(row["Name"]))?string.Empty:(System.String)row["Name"];
					c.TotalMeetintRoom = (Convert.IsDBNull(row["TotalMeetintRoom"]))?(int)0:(System.Int32?)row["TotalMeetintRoom"];
					c.OnlineMeetingRoom = (Convert.IsDBNull(row["OnlineMeetingRoom"]))?(int)0:(System.Int32?)row["OnlineMeetingRoom"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewReportInventoryHotel&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewReportInventoryHotel&gt;"/></returns>
		protected VList<ViewReportInventoryHotel> Fill(IDataReader reader, VList<ViewReportInventoryHotel> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewReportInventoryHotel entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewReportInventoryHotel>("ViewReportInventoryHotel",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewReportInventoryHotel();
					}
					
					entity.SuppressEntityEvents = true;

					entity.Id = (System.Int64)reader[((int)ViewReportInventoryHotelColumn.Id)];
					//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
					entity.Name = (reader.IsDBNull(((int)ViewReportInventoryHotelColumn.Name)))?null:(System.String)reader[((int)ViewReportInventoryHotelColumn.Name)];
					//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
					entity.TotalMeetintRoom = (reader.IsDBNull(((int)ViewReportInventoryHotelColumn.TotalMeetintRoom)))?null:(System.Int32?)reader[((int)ViewReportInventoryHotelColumn.TotalMeetintRoom)];
					//entity.TotalMeetintRoom = (Convert.IsDBNull(reader["TotalMeetintRoom"]))?(int)0:(System.Int32?)reader["TotalMeetintRoom"];
					entity.OnlineMeetingRoom = (reader.IsDBNull(((int)ViewReportInventoryHotelColumn.OnlineMeetingRoom)))?null:(System.Int32?)reader[((int)ViewReportInventoryHotelColumn.OnlineMeetingRoom)];
					//entity.OnlineMeetingRoom = (Convert.IsDBNull(reader["OnlineMeetingRoom"]))?(int)0:(System.Int32?)reader["OnlineMeetingRoom"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewReportInventoryHotel"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewReportInventoryHotel"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewReportInventoryHotel entity)
		{
			reader.Read();
			entity.Id = (System.Int64)reader[((int)ViewReportInventoryHotelColumn.Id)];
			//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
			entity.Name = (reader.IsDBNull(((int)ViewReportInventoryHotelColumn.Name)))?null:(System.String)reader[((int)ViewReportInventoryHotelColumn.Name)];
			//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
			entity.TotalMeetintRoom = (reader.IsDBNull(((int)ViewReportInventoryHotelColumn.TotalMeetintRoom)))?null:(System.Int32?)reader[((int)ViewReportInventoryHotelColumn.TotalMeetintRoom)];
			//entity.TotalMeetintRoom = (Convert.IsDBNull(reader["TotalMeetintRoom"]))?(int)0:(System.Int32?)reader["TotalMeetintRoom"];
			entity.OnlineMeetingRoom = (reader.IsDBNull(((int)ViewReportInventoryHotelColumn.OnlineMeetingRoom)))?null:(System.Int32?)reader[((int)ViewReportInventoryHotelColumn.OnlineMeetingRoom)];
			//entity.OnlineMeetingRoom = (Convert.IsDBNull(reader["OnlineMeetingRoom"]))?(int)0:(System.Int32?)reader["OnlineMeetingRoom"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewReportInventoryHotel"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewReportInventoryHotel"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewReportInventoryHotel entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (Convert.IsDBNull(dataRow["Id"]))?(long)0:(System.Int64)dataRow["Id"];
			entity.Name = (Convert.IsDBNull(dataRow["Name"]))?string.Empty:(System.String)dataRow["Name"];
			entity.TotalMeetintRoom = (Convert.IsDBNull(dataRow["TotalMeetintRoom"]))?(int)0:(System.Int32?)dataRow["TotalMeetintRoom"];
			entity.OnlineMeetingRoom = (Convert.IsDBNull(dataRow["OnlineMeetingRoom"]))?(int)0:(System.Int32?)dataRow["OnlineMeetingRoom"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewReportInventoryHotelFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportInventoryHotel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportInventoryHotelFilterBuilder : SqlFilterBuilder<ViewReportInventoryHotelColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportInventoryHotelFilterBuilder class.
		/// </summary>
		public ViewReportInventoryHotelFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportInventoryHotelFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportInventoryHotelFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportInventoryHotelFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportInventoryHotelFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportInventoryHotelFilterBuilder

	#region ViewReportInventoryHotelParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportInventoryHotel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportInventoryHotelParameterBuilder : ParameterizedSqlFilterBuilder<ViewReportInventoryHotelColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportInventoryHotelParameterBuilder class.
		/// </summary>
		public ViewReportInventoryHotelParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportInventoryHotelParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportInventoryHotelParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportInventoryHotelParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportInventoryHotelParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportInventoryHotelParameterBuilder
	
	#region ViewReportInventoryHotelSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportInventoryHotel"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewReportInventoryHotelSortBuilder : SqlSortBuilder<ViewReportInventoryHotelColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportInventoryHotelSqlSortBuilder class.
		/// </summary>
		public ViewReportInventoryHotelSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewReportInventoryHotelSortBuilder

} // end namespace
