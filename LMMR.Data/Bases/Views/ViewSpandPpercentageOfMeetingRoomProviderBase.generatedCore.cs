﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewSpandPpercentageOfMeetingRoomProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewSpandPpercentageOfMeetingRoomProviderBaseCore : EntityViewProviderBase<ViewSpandPpercentageOfMeetingRoom>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewSpandPpercentageOfMeetingRoom&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewSpandPpercentageOfMeetingRoom&gt;"/></returns>
		protected static VList&lt;ViewSpandPpercentageOfMeetingRoom&gt; Fill(DataSet dataSet, VList<ViewSpandPpercentageOfMeetingRoom> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewSpandPpercentageOfMeetingRoom>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewSpandPpercentageOfMeetingRoom&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewSpandPpercentageOfMeetingRoom>"/></returns>
		protected static VList&lt;ViewSpandPpercentageOfMeetingRoom&gt; Fill(DataTable dataTable, VList<ViewSpandPpercentageOfMeetingRoom> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewSpandPpercentageOfMeetingRoom c = new ViewSpandPpercentageOfMeetingRoom();
					c.Id = (Convert.IsDBNull(row["Id"]))?(long)0:(System.Int64)row["Id"];
					c.HotelId = (Convert.IsDBNull(row["HotelId"]))?(long)0:(System.Int64)row["HotelId"];
					c.SpecialPriceDate = (Convert.IsDBNull(row["SpecialPriceDate"]))?DateTime.MinValue:(System.DateTime?)row["SpecialPriceDate"];
					c.MeetingRoomPercent = (Convert.IsDBNull(row["MeetingRoomPercent"]))?0.0m:(System.Decimal?)row["MeetingRoomPercent"];
					c.MeetingRoomId = (Convert.IsDBNull(row["MeetingRoomID"]))?(long)0:(System.Int64)row["MeetingRoomID"];
					c.Name = (Convert.IsDBNull(row["Name"]))?string.Empty:(System.String)row["Name"];
					c.IsActive = (Convert.IsDBNull(row["IsActive"]))?false:(System.Boolean)row["IsActive"];
					c.MeetingroomHalfdayPrice = (Convert.IsDBNull(row["MeetingroomHalfdayPrice"]))?0.0m:(System.Decimal?)row["MeetingroomHalfdayPrice"];
					c.MeetingroomFullDayPrice = (Convert.IsDBNull(row["MeetingroomFullDayPrice"]))?0.0m:(System.Decimal?)row["MeetingroomFullDayPrice"];
					c.MeetingroomIsOnline = (Convert.IsDBNull(row["MeetingroomIsOnline"]))?false:(System.Boolean)row["MeetingroomIsOnline"];
					c.IsDeleted = (Convert.IsDBNull(row["IsDeleted"]))?false:(System.Boolean)row["IsDeleted"];
					c.MrHalfDayPricePercent = (Convert.IsDBNull(row["MRHalfDayPricePercent"]))?0.0m:(System.Decimal?)row["MRHalfDayPricePercent"];
					c.MrFullDayPricePercent = (Convert.IsDBNull(row["MRFullDayPricePercent"]))?0.0m:(System.Decimal?)row["MRFullDayPricePercent"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewSpandPpercentageOfMeetingRoom&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewSpandPpercentageOfMeetingRoom&gt;"/></returns>
		protected VList<ViewSpandPpercentageOfMeetingRoom> Fill(IDataReader reader, VList<ViewSpandPpercentageOfMeetingRoom> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewSpandPpercentageOfMeetingRoom entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewSpandPpercentageOfMeetingRoom>("ViewSpandPpercentageOfMeetingRoom",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewSpandPpercentageOfMeetingRoom();
					}
					
					entity.SuppressEntityEvents = true;

					entity.Id = (System.Int64)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.Id)];
					//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
					entity.HotelId = (System.Int64)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
					entity.SpecialPriceDate = (reader.IsDBNull(((int)ViewSpandPpercentageOfMeetingRoomColumn.SpecialPriceDate)))?null:(System.DateTime?)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.SpecialPriceDate)];
					//entity.SpecialPriceDate = (Convert.IsDBNull(reader["SpecialPriceDate"]))?DateTime.MinValue:(System.DateTime?)reader["SpecialPriceDate"];
					entity.MeetingRoomPercent = (reader.IsDBNull(((int)ViewSpandPpercentageOfMeetingRoomColumn.MeetingRoomPercent)))?null:(System.Decimal?)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.MeetingRoomPercent)];
					//entity.MeetingRoomPercent = (Convert.IsDBNull(reader["MeetingRoomPercent"]))?0.0m:(System.Decimal?)reader["MeetingRoomPercent"];
					entity.MeetingRoomId = (System.Int64)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.MeetingRoomId)];
					//entity.MeetingRoomId = (Convert.IsDBNull(reader["MeetingRoomID"]))?(long)0:(System.Int64)reader["MeetingRoomID"];
					entity.Name = (reader.IsDBNull(((int)ViewSpandPpercentageOfMeetingRoomColumn.Name)))?null:(System.String)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.Name)];
					//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
					entity.IsActive = (System.Boolean)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.IsActive)];
					//entity.IsActive = (Convert.IsDBNull(reader["IsActive"]))?false:(System.Boolean)reader["IsActive"];
					entity.MeetingroomHalfdayPrice = (reader.IsDBNull(((int)ViewSpandPpercentageOfMeetingRoomColumn.MeetingroomHalfdayPrice)))?null:(System.Decimal?)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.MeetingroomHalfdayPrice)];
					//entity.MeetingroomHalfdayPrice = (Convert.IsDBNull(reader["MeetingroomHalfdayPrice"]))?0.0m:(System.Decimal?)reader["MeetingroomHalfdayPrice"];
					entity.MeetingroomFullDayPrice = (reader.IsDBNull(((int)ViewSpandPpercentageOfMeetingRoomColumn.MeetingroomFullDayPrice)))?null:(System.Decimal?)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.MeetingroomFullDayPrice)];
					//entity.MeetingroomFullDayPrice = (Convert.IsDBNull(reader["MeetingroomFullDayPrice"]))?0.0m:(System.Decimal?)reader["MeetingroomFullDayPrice"];
					entity.MeetingroomIsOnline = (System.Boolean)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.MeetingroomIsOnline)];
					//entity.MeetingroomIsOnline = (Convert.IsDBNull(reader["MeetingroomIsOnline"]))?false:(System.Boolean)reader["MeetingroomIsOnline"];
					entity.IsDeleted = (System.Boolean)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.IsDeleted)];
					//entity.IsDeleted = (Convert.IsDBNull(reader["IsDeleted"]))?false:(System.Boolean)reader["IsDeleted"];
					entity.MrHalfDayPricePercent = (reader.IsDBNull(((int)ViewSpandPpercentageOfMeetingRoomColumn.MrHalfDayPricePercent)))?null:(System.Decimal?)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.MrHalfDayPricePercent)];
					//entity.MrHalfDayPricePercent = (Convert.IsDBNull(reader["MRHalfDayPricePercent"]))?0.0m:(System.Decimal?)reader["MRHalfDayPricePercent"];
					entity.MrFullDayPricePercent = (reader.IsDBNull(((int)ViewSpandPpercentageOfMeetingRoomColumn.MrFullDayPricePercent)))?null:(System.Decimal?)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.MrFullDayPricePercent)];
					//entity.MrFullDayPricePercent = (Convert.IsDBNull(reader["MRFullDayPricePercent"]))?0.0m:(System.Decimal?)reader["MRFullDayPricePercent"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewSpandPpercentageOfMeetingRoom"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewSpandPpercentageOfMeetingRoom"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewSpandPpercentageOfMeetingRoom entity)
		{
			reader.Read();
			entity.Id = (System.Int64)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.Id)];
			//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
			entity.HotelId = (System.Int64)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
			entity.SpecialPriceDate = (reader.IsDBNull(((int)ViewSpandPpercentageOfMeetingRoomColumn.SpecialPriceDate)))?null:(System.DateTime?)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.SpecialPriceDate)];
			//entity.SpecialPriceDate = (Convert.IsDBNull(reader["SpecialPriceDate"]))?DateTime.MinValue:(System.DateTime?)reader["SpecialPriceDate"];
			entity.MeetingRoomPercent = (reader.IsDBNull(((int)ViewSpandPpercentageOfMeetingRoomColumn.MeetingRoomPercent)))?null:(System.Decimal?)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.MeetingRoomPercent)];
			//entity.MeetingRoomPercent = (Convert.IsDBNull(reader["MeetingRoomPercent"]))?0.0m:(System.Decimal?)reader["MeetingRoomPercent"];
			entity.MeetingRoomId = (System.Int64)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.MeetingRoomId)];
			//entity.MeetingRoomId = (Convert.IsDBNull(reader["MeetingRoomID"]))?(long)0:(System.Int64)reader["MeetingRoomID"];
			entity.Name = (reader.IsDBNull(((int)ViewSpandPpercentageOfMeetingRoomColumn.Name)))?null:(System.String)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.Name)];
			//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
			entity.IsActive = (System.Boolean)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.IsActive)];
			//entity.IsActive = (Convert.IsDBNull(reader["IsActive"]))?false:(System.Boolean)reader["IsActive"];
			entity.MeetingroomHalfdayPrice = (reader.IsDBNull(((int)ViewSpandPpercentageOfMeetingRoomColumn.MeetingroomHalfdayPrice)))?null:(System.Decimal?)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.MeetingroomHalfdayPrice)];
			//entity.MeetingroomHalfdayPrice = (Convert.IsDBNull(reader["MeetingroomHalfdayPrice"]))?0.0m:(System.Decimal?)reader["MeetingroomHalfdayPrice"];
			entity.MeetingroomFullDayPrice = (reader.IsDBNull(((int)ViewSpandPpercentageOfMeetingRoomColumn.MeetingroomFullDayPrice)))?null:(System.Decimal?)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.MeetingroomFullDayPrice)];
			//entity.MeetingroomFullDayPrice = (Convert.IsDBNull(reader["MeetingroomFullDayPrice"]))?0.0m:(System.Decimal?)reader["MeetingroomFullDayPrice"];
			entity.MeetingroomIsOnline = (System.Boolean)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.MeetingroomIsOnline)];
			//entity.MeetingroomIsOnline = (Convert.IsDBNull(reader["MeetingroomIsOnline"]))?false:(System.Boolean)reader["MeetingroomIsOnline"];
			entity.IsDeleted = (System.Boolean)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.IsDeleted)];
			//entity.IsDeleted = (Convert.IsDBNull(reader["IsDeleted"]))?false:(System.Boolean)reader["IsDeleted"];
			entity.MrHalfDayPricePercent = (reader.IsDBNull(((int)ViewSpandPpercentageOfMeetingRoomColumn.MrHalfDayPricePercent)))?null:(System.Decimal?)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.MrHalfDayPricePercent)];
			//entity.MrHalfDayPricePercent = (Convert.IsDBNull(reader["MRHalfDayPricePercent"]))?0.0m:(System.Decimal?)reader["MRHalfDayPricePercent"];
			entity.MrFullDayPricePercent = (reader.IsDBNull(((int)ViewSpandPpercentageOfMeetingRoomColumn.MrFullDayPricePercent)))?null:(System.Decimal?)reader[((int)ViewSpandPpercentageOfMeetingRoomColumn.MrFullDayPricePercent)];
			//entity.MrFullDayPricePercent = (Convert.IsDBNull(reader["MRFullDayPricePercent"]))?0.0m:(System.Decimal?)reader["MRFullDayPricePercent"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewSpandPpercentageOfMeetingRoom"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewSpandPpercentageOfMeetingRoom"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewSpandPpercentageOfMeetingRoom entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (Convert.IsDBNull(dataRow["Id"]))?(long)0:(System.Int64)dataRow["Id"];
			entity.HotelId = (Convert.IsDBNull(dataRow["HotelId"]))?(long)0:(System.Int64)dataRow["HotelId"];
			entity.SpecialPriceDate = (Convert.IsDBNull(dataRow["SpecialPriceDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["SpecialPriceDate"];
			entity.MeetingRoomPercent = (Convert.IsDBNull(dataRow["MeetingRoomPercent"]))?0.0m:(System.Decimal?)dataRow["MeetingRoomPercent"];
			entity.MeetingRoomId = (Convert.IsDBNull(dataRow["MeetingRoomID"]))?(long)0:(System.Int64)dataRow["MeetingRoomID"];
			entity.Name = (Convert.IsDBNull(dataRow["Name"]))?string.Empty:(System.String)dataRow["Name"];
			entity.IsActive = (Convert.IsDBNull(dataRow["IsActive"]))?false:(System.Boolean)dataRow["IsActive"];
			entity.MeetingroomHalfdayPrice = (Convert.IsDBNull(dataRow["MeetingroomHalfdayPrice"]))?0.0m:(System.Decimal?)dataRow["MeetingroomHalfdayPrice"];
			entity.MeetingroomFullDayPrice = (Convert.IsDBNull(dataRow["MeetingroomFullDayPrice"]))?0.0m:(System.Decimal?)dataRow["MeetingroomFullDayPrice"];
			entity.MeetingroomIsOnline = (Convert.IsDBNull(dataRow["MeetingroomIsOnline"]))?false:(System.Boolean)dataRow["MeetingroomIsOnline"];
			entity.IsDeleted = (Convert.IsDBNull(dataRow["IsDeleted"]))?false:(System.Boolean)dataRow["IsDeleted"];
			entity.MrHalfDayPricePercent = (Convert.IsDBNull(dataRow["MRHalfDayPricePercent"]))?0.0m:(System.Decimal?)dataRow["MRHalfDayPricePercent"];
			entity.MrFullDayPricePercent = (Convert.IsDBNull(dataRow["MRFullDayPricePercent"]))?0.0m:(System.Decimal?)dataRow["MRFullDayPricePercent"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewSpandPpercentageOfMeetingRoomFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewSpandPpercentageOfMeetingRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewSpandPpercentageOfMeetingRoomFilterBuilder : SqlFilterBuilder<ViewSpandPpercentageOfMeetingRoomColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfMeetingRoomFilterBuilder class.
		/// </summary>
		public ViewSpandPpercentageOfMeetingRoomFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfMeetingRoomFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewSpandPpercentageOfMeetingRoomFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfMeetingRoomFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewSpandPpercentageOfMeetingRoomFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewSpandPpercentageOfMeetingRoomFilterBuilder

	#region ViewSpandPpercentageOfMeetingRoomParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewSpandPpercentageOfMeetingRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewSpandPpercentageOfMeetingRoomParameterBuilder : ParameterizedSqlFilterBuilder<ViewSpandPpercentageOfMeetingRoomColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfMeetingRoomParameterBuilder class.
		/// </summary>
		public ViewSpandPpercentageOfMeetingRoomParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfMeetingRoomParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewSpandPpercentageOfMeetingRoomParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfMeetingRoomParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewSpandPpercentageOfMeetingRoomParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewSpandPpercentageOfMeetingRoomParameterBuilder
	
	#region ViewSpandPpercentageOfMeetingRoomSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewSpandPpercentageOfMeetingRoom"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewSpandPpercentageOfMeetingRoomSortBuilder : SqlSortBuilder<ViewSpandPpercentageOfMeetingRoomColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewSpandPpercentageOfMeetingRoomSqlSortBuilder class.
		/// </summary>
		public ViewSpandPpercentageOfMeetingRoomSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewSpandPpercentageOfMeetingRoomSortBuilder

} // end namespace
