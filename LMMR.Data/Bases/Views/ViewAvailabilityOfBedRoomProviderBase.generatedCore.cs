﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewAvailabilityOfBedRoomProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewAvailabilityOfBedRoomProviderBaseCore : EntityViewProviderBase<ViewAvailabilityOfBedRoom>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewAvailabilityOfBedRoom&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewAvailabilityOfBedRoom&gt;"/></returns>
		protected static VList&lt;ViewAvailabilityOfBedRoom&gt; Fill(DataSet dataSet, VList<ViewAvailabilityOfBedRoom> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewAvailabilityOfBedRoom>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewAvailabilityOfBedRoom&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewAvailabilityOfBedRoom>"/></returns>
		protected static VList&lt;ViewAvailabilityOfBedRoom&gt; Fill(DataTable dataTable, VList<ViewAvailabilityOfBedRoom> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewAvailabilityOfBedRoom c = new ViewAvailabilityOfBedRoom();
					c.Id = (Convert.IsDBNull(row["Id"]))?(long)0:(System.Int64)row["Id"];
					c.AvailibilityIdForBedRoom = (Convert.IsDBNull(row["AvailibilityIdForBedRoom"]))?(long)0:(System.Int64)row["AvailibilityIdForBedRoom"];
					c.RoomType = (Convert.IsDBNull(row["RoomType"]))?(int)0:(System.Int32?)row["RoomType"];
					c.MorningStatus = (Convert.IsDBNull(row["MorningStatus"]))?(int)0:(System.Int32?)row["MorningStatus"];
					c.AfternoonStatus = (Convert.IsDBNull(row["AfternoonStatus"]))?(int)0:(System.Int32?)row["AfternoonStatus"];
					c.NumberOfRoomsAvailable = (Convert.IsDBNull(row["NumberOfRoomsAvailable"]))?(long)0:(System.Int64?)row["NumberOfRoomsAvailable"];
					c.AvailabilityDate = (Convert.IsDBNull(row["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)row["AvailabilityDate"];
					c.RoomId = (Convert.IsDBNull(row["RoomId"]))?(long)0:(System.Int64)row["RoomId"];
					c.HotelId = (Convert.IsDBNull(row["HotelId"]))?(long)0:(System.Int64)row["HotelId"];
					c.Name = (Convert.IsDBNull(row["Name"]))?string.Empty:(System.String)row["Name"];
					c.FullProrertyStatusBr = (Convert.IsDBNull(row["FullProrertyStatusBR"]))?(int)0:(System.Int32?)row["FullProrertyStatusBR"];
					c.NumberOfRoomBooked = (Convert.IsDBNull(row["NumberOfRoomBooked"]))?(long)0:(System.Int64?)row["NumberOfRoomBooked"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewAvailabilityOfBedRoom&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewAvailabilityOfBedRoom&gt;"/></returns>
		protected VList<ViewAvailabilityOfBedRoom> Fill(IDataReader reader, VList<ViewAvailabilityOfBedRoom> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewAvailabilityOfBedRoom entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewAvailabilityOfBedRoom>("ViewAvailabilityOfBedRoom",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewAvailabilityOfBedRoom();
					}
					
					entity.SuppressEntityEvents = true;

					entity.Id = (System.Int64)reader[((int)ViewAvailabilityOfBedRoomColumn.Id)];
					//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
					entity.AvailibilityIdForBedRoom = (System.Int64)reader[((int)ViewAvailabilityOfBedRoomColumn.AvailibilityIdForBedRoom)];
					//entity.AvailibilityIdForBedRoom = (Convert.IsDBNull(reader["AvailibilityIdForBedRoom"]))?(long)0:(System.Int64)reader["AvailibilityIdForBedRoom"];
					entity.RoomType = (reader.IsDBNull(((int)ViewAvailabilityOfBedRoomColumn.RoomType)))?null:(System.Int32?)reader[((int)ViewAvailabilityOfBedRoomColumn.RoomType)];
					//entity.RoomType = (Convert.IsDBNull(reader["RoomType"]))?(int)0:(System.Int32?)reader["RoomType"];
					entity.MorningStatus = (reader.IsDBNull(((int)ViewAvailabilityOfBedRoomColumn.MorningStatus)))?null:(System.Int32?)reader[((int)ViewAvailabilityOfBedRoomColumn.MorningStatus)];
					//entity.MorningStatus = (Convert.IsDBNull(reader["MorningStatus"]))?(int)0:(System.Int32?)reader["MorningStatus"];
					entity.AfternoonStatus = (reader.IsDBNull(((int)ViewAvailabilityOfBedRoomColumn.AfternoonStatus)))?null:(System.Int32?)reader[((int)ViewAvailabilityOfBedRoomColumn.AfternoonStatus)];
					//entity.AfternoonStatus = (Convert.IsDBNull(reader["AfternoonStatus"]))?(int)0:(System.Int32?)reader["AfternoonStatus"];
					entity.NumberOfRoomsAvailable = (reader.IsDBNull(((int)ViewAvailabilityOfBedRoomColumn.NumberOfRoomsAvailable)))?null:(System.Int64?)reader[((int)ViewAvailabilityOfBedRoomColumn.NumberOfRoomsAvailable)];
					//entity.NumberOfRoomsAvailable = (Convert.IsDBNull(reader["NumberOfRoomsAvailable"]))?(long)0:(System.Int64?)reader["NumberOfRoomsAvailable"];
					entity.AvailabilityDate = (reader.IsDBNull(((int)ViewAvailabilityOfBedRoomColumn.AvailabilityDate)))?null:(System.DateTime?)reader[((int)ViewAvailabilityOfBedRoomColumn.AvailabilityDate)];
					//entity.AvailabilityDate = (Convert.IsDBNull(reader["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)reader["AvailabilityDate"];
					entity.RoomId = (System.Int64)reader[((int)ViewAvailabilityOfBedRoomColumn.RoomId)];
					//entity.RoomId = (Convert.IsDBNull(reader["RoomId"]))?(long)0:(System.Int64)reader["RoomId"];
					entity.HotelId = (System.Int64)reader[((int)ViewAvailabilityOfBedRoomColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
					entity.Name = (reader.IsDBNull(((int)ViewAvailabilityOfBedRoomColumn.Name)))?null:(System.String)reader[((int)ViewAvailabilityOfBedRoomColumn.Name)];
					//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
					entity.FullProrertyStatusBr = (reader.IsDBNull(((int)ViewAvailabilityOfBedRoomColumn.FullProrertyStatusBr)))?null:(System.Int32?)reader[((int)ViewAvailabilityOfBedRoomColumn.FullProrertyStatusBr)];
					//entity.FullProrertyStatusBr = (Convert.IsDBNull(reader["FullProrertyStatusBR"]))?(int)0:(System.Int32?)reader["FullProrertyStatusBR"];
					entity.NumberOfRoomBooked = (reader.IsDBNull(((int)ViewAvailabilityOfBedRoomColumn.NumberOfRoomBooked)))?null:(System.Int64?)reader[((int)ViewAvailabilityOfBedRoomColumn.NumberOfRoomBooked)];
					//entity.NumberOfRoomBooked = (Convert.IsDBNull(reader["NumberOfRoomBooked"]))?(long)0:(System.Int64?)reader["NumberOfRoomBooked"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewAvailabilityOfBedRoom"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewAvailabilityOfBedRoom"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewAvailabilityOfBedRoom entity)
		{
			reader.Read();
			entity.Id = (System.Int64)reader[((int)ViewAvailabilityOfBedRoomColumn.Id)];
			//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
			entity.AvailibilityIdForBedRoom = (System.Int64)reader[((int)ViewAvailabilityOfBedRoomColumn.AvailibilityIdForBedRoom)];
			//entity.AvailibilityIdForBedRoom = (Convert.IsDBNull(reader["AvailibilityIdForBedRoom"]))?(long)0:(System.Int64)reader["AvailibilityIdForBedRoom"];
			entity.RoomType = (reader.IsDBNull(((int)ViewAvailabilityOfBedRoomColumn.RoomType)))?null:(System.Int32?)reader[((int)ViewAvailabilityOfBedRoomColumn.RoomType)];
			//entity.RoomType = (Convert.IsDBNull(reader["RoomType"]))?(int)0:(System.Int32?)reader["RoomType"];
			entity.MorningStatus = (reader.IsDBNull(((int)ViewAvailabilityOfBedRoomColumn.MorningStatus)))?null:(System.Int32?)reader[((int)ViewAvailabilityOfBedRoomColumn.MorningStatus)];
			//entity.MorningStatus = (Convert.IsDBNull(reader["MorningStatus"]))?(int)0:(System.Int32?)reader["MorningStatus"];
			entity.AfternoonStatus = (reader.IsDBNull(((int)ViewAvailabilityOfBedRoomColumn.AfternoonStatus)))?null:(System.Int32?)reader[((int)ViewAvailabilityOfBedRoomColumn.AfternoonStatus)];
			//entity.AfternoonStatus = (Convert.IsDBNull(reader["AfternoonStatus"]))?(int)0:(System.Int32?)reader["AfternoonStatus"];
			entity.NumberOfRoomsAvailable = (reader.IsDBNull(((int)ViewAvailabilityOfBedRoomColumn.NumberOfRoomsAvailable)))?null:(System.Int64?)reader[((int)ViewAvailabilityOfBedRoomColumn.NumberOfRoomsAvailable)];
			//entity.NumberOfRoomsAvailable = (Convert.IsDBNull(reader["NumberOfRoomsAvailable"]))?(long)0:(System.Int64?)reader["NumberOfRoomsAvailable"];
			entity.AvailabilityDate = (reader.IsDBNull(((int)ViewAvailabilityOfBedRoomColumn.AvailabilityDate)))?null:(System.DateTime?)reader[((int)ViewAvailabilityOfBedRoomColumn.AvailabilityDate)];
			//entity.AvailabilityDate = (Convert.IsDBNull(reader["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)reader["AvailabilityDate"];
			entity.RoomId = (System.Int64)reader[((int)ViewAvailabilityOfBedRoomColumn.RoomId)];
			//entity.RoomId = (Convert.IsDBNull(reader["RoomId"]))?(long)0:(System.Int64)reader["RoomId"];
			entity.HotelId = (System.Int64)reader[((int)ViewAvailabilityOfBedRoomColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
			entity.Name = (reader.IsDBNull(((int)ViewAvailabilityOfBedRoomColumn.Name)))?null:(System.String)reader[((int)ViewAvailabilityOfBedRoomColumn.Name)];
			//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
			entity.FullProrertyStatusBr = (reader.IsDBNull(((int)ViewAvailabilityOfBedRoomColumn.FullProrertyStatusBr)))?null:(System.Int32?)reader[((int)ViewAvailabilityOfBedRoomColumn.FullProrertyStatusBr)];
			//entity.FullProrertyStatusBr = (Convert.IsDBNull(reader["FullProrertyStatusBR"]))?(int)0:(System.Int32?)reader["FullProrertyStatusBR"];
			entity.NumberOfRoomBooked = (reader.IsDBNull(((int)ViewAvailabilityOfBedRoomColumn.NumberOfRoomBooked)))?null:(System.Int64?)reader[((int)ViewAvailabilityOfBedRoomColumn.NumberOfRoomBooked)];
			//entity.NumberOfRoomBooked = (Convert.IsDBNull(reader["NumberOfRoomBooked"]))?(long)0:(System.Int64?)reader["NumberOfRoomBooked"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewAvailabilityOfBedRoom"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewAvailabilityOfBedRoom"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewAvailabilityOfBedRoom entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (Convert.IsDBNull(dataRow["Id"]))?(long)0:(System.Int64)dataRow["Id"];
			entity.AvailibilityIdForBedRoom = (Convert.IsDBNull(dataRow["AvailibilityIdForBedRoom"]))?(long)0:(System.Int64)dataRow["AvailibilityIdForBedRoom"];
			entity.RoomType = (Convert.IsDBNull(dataRow["RoomType"]))?(int)0:(System.Int32?)dataRow["RoomType"];
			entity.MorningStatus = (Convert.IsDBNull(dataRow["MorningStatus"]))?(int)0:(System.Int32?)dataRow["MorningStatus"];
			entity.AfternoonStatus = (Convert.IsDBNull(dataRow["AfternoonStatus"]))?(int)0:(System.Int32?)dataRow["AfternoonStatus"];
			entity.NumberOfRoomsAvailable = (Convert.IsDBNull(dataRow["NumberOfRoomsAvailable"]))?(long)0:(System.Int64?)dataRow["NumberOfRoomsAvailable"];
			entity.AvailabilityDate = (Convert.IsDBNull(dataRow["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["AvailabilityDate"];
			entity.RoomId = (Convert.IsDBNull(dataRow["RoomId"]))?(long)0:(System.Int64)dataRow["RoomId"];
			entity.HotelId = (Convert.IsDBNull(dataRow["HotelId"]))?(long)0:(System.Int64)dataRow["HotelId"];
			entity.Name = (Convert.IsDBNull(dataRow["Name"]))?string.Empty:(System.String)dataRow["Name"];
			entity.FullProrertyStatusBr = (Convert.IsDBNull(dataRow["FullProrertyStatusBR"]))?(int)0:(System.Int32?)dataRow["FullProrertyStatusBR"];
			entity.NumberOfRoomBooked = (Convert.IsDBNull(dataRow["NumberOfRoomBooked"]))?(long)0:(System.Int64?)dataRow["NumberOfRoomBooked"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewAvailabilityOfBedRoomFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewAvailabilityOfBedRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewAvailabilityOfBedRoomFilterBuilder : SqlFilterBuilder<ViewAvailabilityOfBedRoomColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfBedRoomFilterBuilder class.
		/// </summary>
		public ViewAvailabilityOfBedRoomFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfBedRoomFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewAvailabilityOfBedRoomFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfBedRoomFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewAvailabilityOfBedRoomFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewAvailabilityOfBedRoomFilterBuilder

	#region ViewAvailabilityOfBedRoomParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewAvailabilityOfBedRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewAvailabilityOfBedRoomParameterBuilder : ParameterizedSqlFilterBuilder<ViewAvailabilityOfBedRoomColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfBedRoomParameterBuilder class.
		/// </summary>
		public ViewAvailabilityOfBedRoomParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfBedRoomParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewAvailabilityOfBedRoomParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfBedRoomParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewAvailabilityOfBedRoomParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewAvailabilityOfBedRoomParameterBuilder
	
	#region ViewAvailabilityOfBedRoomSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewAvailabilityOfBedRoom"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewAvailabilityOfBedRoomSortBuilder : SqlSortBuilder<ViewAvailabilityOfBedRoomColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityOfBedRoomSqlSortBuilder class.
		/// </summary>
		public ViewAvailabilityOfBedRoomSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewAvailabilityOfBedRoomSortBuilder

} // end namespace
