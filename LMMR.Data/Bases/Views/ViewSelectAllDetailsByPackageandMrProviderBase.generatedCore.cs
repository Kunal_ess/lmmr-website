﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewSelectAllDetailsByPackageandMrProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewSelectAllDetailsByPackageandMrProviderBaseCore : EntityViewProviderBase<ViewSelectAllDetailsByPackageandMr>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewSelectAllDetailsByPackageandMr&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewSelectAllDetailsByPackageandMr&gt;"/></returns>
		protected static VList&lt;ViewSelectAllDetailsByPackageandMr&gt; Fill(DataSet dataSet, VList<ViewSelectAllDetailsByPackageandMr> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewSelectAllDetailsByPackageandMr>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewSelectAllDetailsByPackageandMr&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewSelectAllDetailsByPackageandMr>"/></returns>
		protected static VList&lt;ViewSelectAllDetailsByPackageandMr&gt; Fill(DataTable dataTable, VList<ViewSelectAllDetailsByPackageandMr> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewSelectAllDetailsByPackageandMr c = new ViewSelectAllDetailsByPackageandMr();
					c.Id = (Convert.IsDBNull(row["Id"]))?(long)0:(System.Int64)row["Id"];
					c.HotelId = (Convert.IsDBNull(row["HotelId"]))?(long)0:(System.Int64)row["HotelId"];
					c.SpecialPriceDate = (Convert.IsDBNull(row["SpecialPriceDate"]))?DateTime.MinValue:(System.DateTime?)row["SpecialPriceDate"];
					c.DdrPercent = (Convert.IsDBNull(row["DDRPercent"]))?0.0m:(System.Decimal?)row["DDRPercent"];
					c.MeetingRoomPercent = (Convert.IsDBNull(row["MeetingRoomPercent"]))?0.0m:(System.Decimal?)row["MeetingRoomPercent"];
					c.PackageId = (Convert.IsDBNull(row["PackageID"]))?(long)0:(System.Int64?)row["PackageID"];
					c.HalfdayPrice = (Convert.IsDBNull(row["HalfdayPrice"]))?0.0m:(System.Decimal?)row["HalfdayPrice"];
					c.FulldayPrice = (Convert.IsDBNull(row["FulldayPrice"]))?0.0m:(System.Decimal?)row["FulldayPrice"];
					c.IsOnline = (Convert.IsDBNull(row["IsOnline"]))?false:(System.Boolean)row["IsOnline"];
					c.ItemId = (Convert.IsDBNull(row["ItemId"]))?(long)0:(System.Int64)row["ItemId"];
					c.IsComplementary = (Convert.IsDBNull(row["IsComplementary"]))?false:(System.Boolean)row["IsComplementary"];
					c.UpdatedBy = (Convert.IsDBNull(row["UpdatedBy"]))?(long)0:(System.Int64?)row["UpdatedBy"];
					c.HalfDayPricePercent = (Convert.IsDBNull(row["HalfDayPricePercent"]))?0.0m:(System.Decimal?)row["HalfDayPricePercent"];
					c.FullDayPricePercent = (Convert.IsDBNull(row["FullDayPricePercent"]))?0.0m:(System.Decimal?)row["FullDayPricePercent"];
					c.MeetingRoomId = (Convert.IsDBNull(row["MeetingRoomID"]))?(long)0:(System.Int64)row["MeetingRoomID"];
					c.Name = (Convert.IsDBNull(row["Name"]))?string.Empty:(System.String)row["Name"];
					c.Surface = (Convert.IsDBNull(row["Surface"]))?(long)0:(System.Int64?)row["Surface"];
					c.Height = (Convert.IsDBNull(row["Height"]))?0.0m:(System.Decimal?)row["Height"];
					c.IsActive = (Convert.IsDBNull(row["IsActive"]))?false:(System.Boolean)row["IsActive"];
					c.OrderNumber = (Convert.IsDBNull(row["OrderNumber"]))?(int)0:(System.Int32?)row["OrderNumber"];
					c.MeetingroomHalfdayPrice = (Convert.IsDBNull(row["MeetingroomHalfdayPrice"]))?0.0m:(System.Decimal?)row["MeetingroomHalfdayPrice"];
					c.MeetingroomFullDayPrice = (Convert.IsDBNull(row["MeetingroomFullDayPrice"]))?0.0m:(System.Decimal?)row["MeetingroomFullDayPrice"];
					c.MeetingroomIsOnline = (Convert.IsDBNull(row["MeetingroomIsOnline"]))?false:(System.Boolean)row["MeetingroomIsOnline"];
					c.IsDeleted = (Convert.IsDBNull(row["IsDeleted"]))?false:(System.Boolean)row["IsDeleted"];
					c.MrHalfDayPricePercent = (Convert.IsDBNull(row["MRHalfDayPricePercent"]))?0.0m:(System.Decimal?)row["MRHalfDayPricePercent"];
					c.MrFullDayPricePercent = (Convert.IsDBNull(row["MRFullDayPricePercent"]))?0.0m:(System.Decimal?)row["MRFullDayPricePercent"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewSelectAllDetailsByPackageandMr&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewSelectAllDetailsByPackageandMr&gt;"/></returns>
		protected VList<ViewSelectAllDetailsByPackageandMr> Fill(IDataReader reader, VList<ViewSelectAllDetailsByPackageandMr> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewSelectAllDetailsByPackageandMr entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewSelectAllDetailsByPackageandMr>("ViewSelectAllDetailsByPackageandMr",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewSelectAllDetailsByPackageandMr();
					}
					
					entity.SuppressEntityEvents = true;

					entity.Id = (System.Int64)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.Id)];
					//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
					entity.HotelId = (System.Int64)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
					entity.SpecialPriceDate = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.SpecialPriceDate)))?null:(System.DateTime?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.SpecialPriceDate)];
					//entity.SpecialPriceDate = (Convert.IsDBNull(reader["SpecialPriceDate"]))?DateTime.MinValue:(System.DateTime?)reader["SpecialPriceDate"];
					entity.DdrPercent = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.DdrPercent)))?null:(System.Decimal?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.DdrPercent)];
					//entity.DdrPercent = (Convert.IsDBNull(reader["DDRPercent"]))?0.0m:(System.Decimal?)reader["DDRPercent"];
					entity.MeetingRoomPercent = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.MeetingRoomPercent)))?null:(System.Decimal?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.MeetingRoomPercent)];
					//entity.MeetingRoomPercent = (Convert.IsDBNull(reader["MeetingRoomPercent"]))?0.0m:(System.Decimal?)reader["MeetingRoomPercent"];
					entity.PackageId = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.PackageId)))?null:(System.Int64?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.PackageId)];
					//entity.PackageId = (Convert.IsDBNull(reader["PackageID"]))?(long)0:(System.Int64?)reader["PackageID"];
					entity.HalfdayPrice = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.HalfdayPrice)))?null:(System.Decimal?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.HalfdayPrice)];
					//entity.HalfdayPrice = (Convert.IsDBNull(reader["HalfdayPrice"]))?0.0m:(System.Decimal?)reader["HalfdayPrice"];
					entity.FulldayPrice = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.FulldayPrice)))?null:(System.Decimal?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.FulldayPrice)];
					//entity.FulldayPrice = (Convert.IsDBNull(reader["FulldayPrice"]))?0.0m:(System.Decimal?)reader["FulldayPrice"];
					entity.IsOnline = (System.Boolean)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.IsOnline)];
					//entity.IsOnline = (Convert.IsDBNull(reader["IsOnline"]))?false:(System.Boolean)reader["IsOnline"];
					entity.ItemId = (System.Int64)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.ItemId)];
					//entity.ItemId = (Convert.IsDBNull(reader["ItemId"]))?(long)0:(System.Int64)reader["ItemId"];
					entity.IsComplementary = (System.Boolean)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.IsComplementary)];
					//entity.IsComplementary = (Convert.IsDBNull(reader["IsComplementary"]))?false:(System.Boolean)reader["IsComplementary"];
					entity.UpdatedBy = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.UpdatedBy)))?null:(System.Int64?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.UpdatedBy)];
					//entity.UpdatedBy = (Convert.IsDBNull(reader["UpdatedBy"]))?(long)0:(System.Int64?)reader["UpdatedBy"];
					entity.HalfDayPricePercent = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.HalfDayPricePercent)))?null:(System.Decimal?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.HalfDayPricePercent)];
					//entity.HalfDayPricePercent = (Convert.IsDBNull(reader["HalfDayPricePercent"]))?0.0m:(System.Decimal?)reader["HalfDayPricePercent"];
					entity.FullDayPricePercent = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.FullDayPricePercent)))?null:(System.Decimal?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.FullDayPricePercent)];
					//entity.FullDayPricePercent = (Convert.IsDBNull(reader["FullDayPricePercent"]))?0.0m:(System.Decimal?)reader["FullDayPricePercent"];
					entity.MeetingRoomId = (System.Int64)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.MeetingRoomId)];
					//entity.MeetingRoomId = (Convert.IsDBNull(reader["MeetingRoomID"]))?(long)0:(System.Int64)reader["MeetingRoomID"];
					entity.Name = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.Name)))?null:(System.String)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.Name)];
					//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
					entity.Surface = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.Surface)))?null:(System.Int64?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.Surface)];
					//entity.Surface = (Convert.IsDBNull(reader["Surface"]))?(long)0:(System.Int64?)reader["Surface"];
					entity.Height = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.Height)))?null:(System.Decimal?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.Height)];
					//entity.Height = (Convert.IsDBNull(reader["Height"]))?0.0m:(System.Decimal?)reader["Height"];
					entity.IsActive = (System.Boolean)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.IsActive)];
					//entity.IsActive = (Convert.IsDBNull(reader["IsActive"]))?false:(System.Boolean)reader["IsActive"];
					entity.OrderNumber = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.OrderNumber)))?null:(System.Int32?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.OrderNumber)];
					//entity.OrderNumber = (Convert.IsDBNull(reader["OrderNumber"]))?(int)0:(System.Int32?)reader["OrderNumber"];
					entity.MeetingroomHalfdayPrice = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.MeetingroomHalfdayPrice)))?null:(System.Decimal?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.MeetingroomHalfdayPrice)];
					//entity.MeetingroomHalfdayPrice = (Convert.IsDBNull(reader["MeetingroomHalfdayPrice"]))?0.0m:(System.Decimal?)reader["MeetingroomHalfdayPrice"];
					entity.MeetingroomFullDayPrice = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.MeetingroomFullDayPrice)))?null:(System.Decimal?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.MeetingroomFullDayPrice)];
					//entity.MeetingroomFullDayPrice = (Convert.IsDBNull(reader["MeetingroomFullDayPrice"]))?0.0m:(System.Decimal?)reader["MeetingroomFullDayPrice"];
					entity.MeetingroomIsOnline = (System.Boolean)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.MeetingroomIsOnline)];
					//entity.MeetingroomIsOnline = (Convert.IsDBNull(reader["MeetingroomIsOnline"]))?false:(System.Boolean)reader["MeetingroomIsOnline"];
					entity.IsDeleted = (System.Boolean)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.IsDeleted)];
					//entity.IsDeleted = (Convert.IsDBNull(reader["IsDeleted"]))?false:(System.Boolean)reader["IsDeleted"];
					entity.MrHalfDayPricePercent = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.MrHalfDayPricePercent)))?null:(System.Decimal?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.MrHalfDayPricePercent)];
					//entity.MrHalfDayPricePercent = (Convert.IsDBNull(reader["MRHalfDayPricePercent"]))?0.0m:(System.Decimal?)reader["MRHalfDayPricePercent"];
					entity.MrFullDayPricePercent = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.MrFullDayPricePercent)))?null:(System.Decimal?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.MrFullDayPricePercent)];
					//entity.MrFullDayPricePercent = (Convert.IsDBNull(reader["MRFullDayPricePercent"]))?0.0m:(System.Decimal?)reader["MRFullDayPricePercent"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewSelectAllDetailsByPackageandMr"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewSelectAllDetailsByPackageandMr"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewSelectAllDetailsByPackageandMr entity)
		{
			reader.Read();
			entity.Id = (System.Int64)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.Id)];
			//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
			entity.HotelId = (System.Int64)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
			entity.SpecialPriceDate = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.SpecialPriceDate)))?null:(System.DateTime?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.SpecialPriceDate)];
			//entity.SpecialPriceDate = (Convert.IsDBNull(reader["SpecialPriceDate"]))?DateTime.MinValue:(System.DateTime?)reader["SpecialPriceDate"];
			entity.DdrPercent = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.DdrPercent)))?null:(System.Decimal?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.DdrPercent)];
			//entity.DdrPercent = (Convert.IsDBNull(reader["DDRPercent"]))?0.0m:(System.Decimal?)reader["DDRPercent"];
			entity.MeetingRoomPercent = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.MeetingRoomPercent)))?null:(System.Decimal?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.MeetingRoomPercent)];
			//entity.MeetingRoomPercent = (Convert.IsDBNull(reader["MeetingRoomPercent"]))?0.0m:(System.Decimal?)reader["MeetingRoomPercent"];
			entity.PackageId = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.PackageId)))?null:(System.Int64?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.PackageId)];
			//entity.PackageId = (Convert.IsDBNull(reader["PackageID"]))?(long)0:(System.Int64?)reader["PackageID"];
			entity.HalfdayPrice = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.HalfdayPrice)))?null:(System.Decimal?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.HalfdayPrice)];
			//entity.HalfdayPrice = (Convert.IsDBNull(reader["HalfdayPrice"]))?0.0m:(System.Decimal?)reader["HalfdayPrice"];
			entity.FulldayPrice = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.FulldayPrice)))?null:(System.Decimal?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.FulldayPrice)];
			//entity.FulldayPrice = (Convert.IsDBNull(reader["FulldayPrice"]))?0.0m:(System.Decimal?)reader["FulldayPrice"];
			entity.IsOnline = (System.Boolean)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.IsOnline)];
			//entity.IsOnline = (Convert.IsDBNull(reader["IsOnline"]))?false:(System.Boolean)reader["IsOnline"];
			entity.ItemId = (System.Int64)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.ItemId)];
			//entity.ItemId = (Convert.IsDBNull(reader["ItemId"]))?(long)0:(System.Int64)reader["ItemId"];
			entity.IsComplementary = (System.Boolean)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.IsComplementary)];
			//entity.IsComplementary = (Convert.IsDBNull(reader["IsComplementary"]))?false:(System.Boolean)reader["IsComplementary"];
			entity.UpdatedBy = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.UpdatedBy)))?null:(System.Int64?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.UpdatedBy)];
			//entity.UpdatedBy = (Convert.IsDBNull(reader["UpdatedBy"]))?(long)0:(System.Int64?)reader["UpdatedBy"];
			entity.HalfDayPricePercent = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.HalfDayPricePercent)))?null:(System.Decimal?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.HalfDayPricePercent)];
			//entity.HalfDayPricePercent = (Convert.IsDBNull(reader["HalfDayPricePercent"]))?0.0m:(System.Decimal?)reader["HalfDayPricePercent"];
			entity.FullDayPricePercent = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.FullDayPricePercent)))?null:(System.Decimal?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.FullDayPricePercent)];
			//entity.FullDayPricePercent = (Convert.IsDBNull(reader["FullDayPricePercent"]))?0.0m:(System.Decimal?)reader["FullDayPricePercent"];
			entity.MeetingRoomId = (System.Int64)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.MeetingRoomId)];
			//entity.MeetingRoomId = (Convert.IsDBNull(reader["MeetingRoomID"]))?(long)0:(System.Int64)reader["MeetingRoomID"];
			entity.Name = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.Name)))?null:(System.String)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.Name)];
			//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
			entity.Surface = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.Surface)))?null:(System.Int64?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.Surface)];
			//entity.Surface = (Convert.IsDBNull(reader["Surface"]))?(long)0:(System.Int64?)reader["Surface"];
			entity.Height = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.Height)))?null:(System.Decimal?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.Height)];
			//entity.Height = (Convert.IsDBNull(reader["Height"]))?0.0m:(System.Decimal?)reader["Height"];
			entity.IsActive = (System.Boolean)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.IsActive)];
			//entity.IsActive = (Convert.IsDBNull(reader["IsActive"]))?false:(System.Boolean)reader["IsActive"];
			entity.OrderNumber = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.OrderNumber)))?null:(System.Int32?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.OrderNumber)];
			//entity.OrderNumber = (Convert.IsDBNull(reader["OrderNumber"]))?(int)0:(System.Int32?)reader["OrderNumber"];
			entity.MeetingroomHalfdayPrice = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.MeetingroomHalfdayPrice)))?null:(System.Decimal?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.MeetingroomHalfdayPrice)];
			//entity.MeetingroomHalfdayPrice = (Convert.IsDBNull(reader["MeetingroomHalfdayPrice"]))?0.0m:(System.Decimal?)reader["MeetingroomHalfdayPrice"];
			entity.MeetingroomFullDayPrice = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.MeetingroomFullDayPrice)))?null:(System.Decimal?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.MeetingroomFullDayPrice)];
			//entity.MeetingroomFullDayPrice = (Convert.IsDBNull(reader["MeetingroomFullDayPrice"]))?0.0m:(System.Decimal?)reader["MeetingroomFullDayPrice"];
			entity.MeetingroomIsOnline = (System.Boolean)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.MeetingroomIsOnline)];
			//entity.MeetingroomIsOnline = (Convert.IsDBNull(reader["MeetingroomIsOnline"]))?false:(System.Boolean)reader["MeetingroomIsOnline"];
			entity.IsDeleted = (System.Boolean)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.IsDeleted)];
			//entity.IsDeleted = (Convert.IsDBNull(reader["IsDeleted"]))?false:(System.Boolean)reader["IsDeleted"];
			entity.MrHalfDayPricePercent = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.MrHalfDayPricePercent)))?null:(System.Decimal?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.MrHalfDayPricePercent)];
			//entity.MrHalfDayPricePercent = (Convert.IsDBNull(reader["MRHalfDayPricePercent"]))?0.0m:(System.Decimal?)reader["MRHalfDayPricePercent"];
			entity.MrFullDayPricePercent = (reader.IsDBNull(((int)ViewSelectAllDetailsByPackageandMrColumn.MrFullDayPricePercent)))?null:(System.Decimal?)reader[((int)ViewSelectAllDetailsByPackageandMrColumn.MrFullDayPricePercent)];
			//entity.MrFullDayPricePercent = (Convert.IsDBNull(reader["MRFullDayPricePercent"]))?0.0m:(System.Decimal?)reader["MRFullDayPricePercent"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewSelectAllDetailsByPackageandMr"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewSelectAllDetailsByPackageandMr"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewSelectAllDetailsByPackageandMr entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (Convert.IsDBNull(dataRow["Id"]))?(long)0:(System.Int64)dataRow["Id"];
			entity.HotelId = (Convert.IsDBNull(dataRow["HotelId"]))?(long)0:(System.Int64)dataRow["HotelId"];
			entity.SpecialPriceDate = (Convert.IsDBNull(dataRow["SpecialPriceDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["SpecialPriceDate"];
			entity.DdrPercent = (Convert.IsDBNull(dataRow["DDRPercent"]))?0.0m:(System.Decimal?)dataRow["DDRPercent"];
			entity.MeetingRoomPercent = (Convert.IsDBNull(dataRow["MeetingRoomPercent"]))?0.0m:(System.Decimal?)dataRow["MeetingRoomPercent"];
			entity.PackageId = (Convert.IsDBNull(dataRow["PackageID"]))?(long)0:(System.Int64?)dataRow["PackageID"];
			entity.HalfdayPrice = (Convert.IsDBNull(dataRow["HalfdayPrice"]))?0.0m:(System.Decimal?)dataRow["HalfdayPrice"];
			entity.FulldayPrice = (Convert.IsDBNull(dataRow["FulldayPrice"]))?0.0m:(System.Decimal?)dataRow["FulldayPrice"];
			entity.IsOnline = (Convert.IsDBNull(dataRow["IsOnline"]))?false:(System.Boolean)dataRow["IsOnline"];
			entity.ItemId = (Convert.IsDBNull(dataRow["ItemId"]))?(long)0:(System.Int64)dataRow["ItemId"];
			entity.IsComplementary = (Convert.IsDBNull(dataRow["IsComplementary"]))?false:(System.Boolean)dataRow["IsComplementary"];
			entity.UpdatedBy = (Convert.IsDBNull(dataRow["UpdatedBy"]))?(long)0:(System.Int64?)dataRow["UpdatedBy"];
			entity.HalfDayPricePercent = (Convert.IsDBNull(dataRow["HalfDayPricePercent"]))?0.0m:(System.Decimal?)dataRow["HalfDayPricePercent"];
			entity.FullDayPricePercent = (Convert.IsDBNull(dataRow["FullDayPricePercent"]))?0.0m:(System.Decimal?)dataRow["FullDayPricePercent"];
			entity.MeetingRoomId = (Convert.IsDBNull(dataRow["MeetingRoomID"]))?(long)0:(System.Int64)dataRow["MeetingRoomID"];
			entity.Name = (Convert.IsDBNull(dataRow["Name"]))?string.Empty:(System.String)dataRow["Name"];
			entity.Surface = (Convert.IsDBNull(dataRow["Surface"]))?(long)0:(System.Int64?)dataRow["Surface"];
			entity.Height = (Convert.IsDBNull(dataRow["Height"]))?0.0m:(System.Decimal?)dataRow["Height"];
			entity.IsActive = (Convert.IsDBNull(dataRow["IsActive"]))?false:(System.Boolean)dataRow["IsActive"];
			entity.OrderNumber = (Convert.IsDBNull(dataRow["OrderNumber"]))?(int)0:(System.Int32?)dataRow["OrderNumber"];
			entity.MeetingroomHalfdayPrice = (Convert.IsDBNull(dataRow["MeetingroomHalfdayPrice"]))?0.0m:(System.Decimal?)dataRow["MeetingroomHalfdayPrice"];
			entity.MeetingroomFullDayPrice = (Convert.IsDBNull(dataRow["MeetingroomFullDayPrice"]))?0.0m:(System.Decimal?)dataRow["MeetingroomFullDayPrice"];
			entity.MeetingroomIsOnline = (Convert.IsDBNull(dataRow["MeetingroomIsOnline"]))?false:(System.Boolean)dataRow["MeetingroomIsOnline"];
			entity.IsDeleted = (Convert.IsDBNull(dataRow["IsDeleted"]))?false:(System.Boolean)dataRow["IsDeleted"];
			entity.MrHalfDayPricePercent = (Convert.IsDBNull(dataRow["MRHalfDayPricePercent"]))?0.0m:(System.Decimal?)dataRow["MRHalfDayPricePercent"];
			entity.MrFullDayPricePercent = (Convert.IsDBNull(dataRow["MRFullDayPricePercent"]))?0.0m:(System.Decimal?)dataRow["MRFullDayPricePercent"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewSelectAllDetailsByPackageandMrFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewSelectAllDetailsByPackageandMr"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewSelectAllDetailsByPackageandMrFilterBuilder : SqlFilterBuilder<ViewSelectAllDetailsByPackageandMrColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewSelectAllDetailsByPackageandMrFilterBuilder class.
		/// </summary>
		public ViewSelectAllDetailsByPackageandMrFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewSelectAllDetailsByPackageandMrFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewSelectAllDetailsByPackageandMrFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewSelectAllDetailsByPackageandMrFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewSelectAllDetailsByPackageandMrFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewSelectAllDetailsByPackageandMrFilterBuilder

	#region ViewSelectAllDetailsByPackageandMrParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewSelectAllDetailsByPackageandMr"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewSelectAllDetailsByPackageandMrParameterBuilder : ParameterizedSqlFilterBuilder<ViewSelectAllDetailsByPackageandMrColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewSelectAllDetailsByPackageandMrParameterBuilder class.
		/// </summary>
		public ViewSelectAllDetailsByPackageandMrParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewSelectAllDetailsByPackageandMrParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewSelectAllDetailsByPackageandMrParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewSelectAllDetailsByPackageandMrParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewSelectAllDetailsByPackageandMrParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewSelectAllDetailsByPackageandMrParameterBuilder
	
	#region ViewSelectAllDetailsByPackageandMrSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewSelectAllDetailsByPackageandMr"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewSelectAllDetailsByPackageandMrSortBuilder : SqlSortBuilder<ViewSelectAllDetailsByPackageandMrColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewSelectAllDetailsByPackageandMrSqlSortBuilder class.
		/// </summary>
		public ViewSelectAllDetailsByPackageandMrSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewSelectAllDetailsByPackageandMrSortBuilder

} // end namespace
