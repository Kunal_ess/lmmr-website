﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewReportProductionCompanyAgencyProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewReportProductionCompanyAgencyProviderBaseCore : EntityViewProviderBase<ViewReportProductionCompanyAgency>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewReportProductionCompanyAgency&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewReportProductionCompanyAgency&gt;"/></returns>
		protected static VList&lt;ViewReportProductionCompanyAgency&gt; Fill(DataSet dataSet, VList<ViewReportProductionCompanyAgency> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewReportProductionCompanyAgency>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewReportProductionCompanyAgency&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewReportProductionCompanyAgency>"/></returns>
		protected static VList&lt;ViewReportProductionCompanyAgency&gt; Fill(DataTable dataTable, VList<ViewReportProductionCompanyAgency> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewReportProductionCompanyAgency c = new ViewReportProductionCompanyAgency();
					c.CountryId = (Convert.IsDBNull(row["CountryID"]))?(long)0:(System.Int64)row["CountryID"];
					c.CountryName = (Convert.IsDBNull(row["CountryName"]))?string.Empty:(System.String)row["CountryName"];
					c.CityName = (Convert.IsDBNull(row["CityName"]))?string.Empty:(System.String)row["CityName"];
					c.Userid = (Convert.IsDBNull(row["Userid"]))?(long)0:(System.Int64)row["Userid"];
					c.Usertype = (Convert.IsDBNull(row["Usertype"]))?(int)0:(System.Int32?)row["Usertype"];
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.NoOfBooking = (Convert.IsDBNull(row["NoOfBooking"]))?(int)0:(System.Int32?)row["NoOfBooking"];
					c.NoOfRequest = (Convert.IsDBNull(row["NoOfRequest"]))?(int)0:(System.Int32?)row["NoOfRequest"];
					c.NoOfBookingCancled = (Convert.IsDBNull(row["NoOfBookingCancled"]))?(int)0:(System.Int32?)row["NoOfBookingCancled"];
					c.NoOfRequestCancled = (Convert.IsDBNull(row["NoOfRequestCancled"]))?(int)0:(System.Int32?)row["NoOfRequestCancled"];
					c.TotalRevenueAmountFromBooking = (Convert.IsDBNull(row["TotalRevenueAmountFromBooking"]))?0.0m:(System.Decimal?)row["TotalRevenueAmountFromBooking"];
					c.Modifyrevenuefromrequest = (Convert.IsDBNull(row["Modifyrevenuefromrequest"]))?0.0m:(System.Decimal?)row["Modifyrevenuefromrequest"];
					c.Months = (Convert.IsDBNull(row["Months"]))?(int)0:(System.Int32?)row["Months"];
					c.Years = (Convert.IsDBNull(row["Years"]))?(int)0:(System.Int32?)row["Years"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewReportProductionCompanyAgency&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewReportProductionCompanyAgency&gt;"/></returns>
		protected VList<ViewReportProductionCompanyAgency> Fill(IDataReader reader, VList<ViewReportProductionCompanyAgency> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewReportProductionCompanyAgency entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewReportProductionCompanyAgency>("ViewReportProductionCompanyAgency",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewReportProductionCompanyAgency();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CountryId = (System.Int64)reader[((int)ViewReportProductionCompanyAgencyColumn.CountryId)];
					//entity.CountryId = (Convert.IsDBNull(reader["CountryID"]))?(long)0:(System.Int64)reader["CountryID"];
					entity.CountryName = (reader.IsDBNull(((int)ViewReportProductionCompanyAgencyColumn.CountryName)))?null:(System.String)reader[((int)ViewReportProductionCompanyAgencyColumn.CountryName)];
					//entity.CountryName = (Convert.IsDBNull(reader["CountryName"]))?string.Empty:(System.String)reader["CountryName"];
					entity.CityName = (reader.IsDBNull(((int)ViewReportProductionCompanyAgencyColumn.CityName)))?null:(System.String)reader[((int)ViewReportProductionCompanyAgencyColumn.CityName)];
					//entity.CityName = (Convert.IsDBNull(reader["CityName"]))?string.Empty:(System.String)reader["CityName"];
					entity.Userid = (System.Int64)reader[((int)ViewReportProductionCompanyAgencyColumn.Userid)];
					//entity.Userid = (Convert.IsDBNull(reader["Userid"]))?(long)0:(System.Int64)reader["Userid"];
					entity.Usertype = (reader.IsDBNull(((int)ViewReportProductionCompanyAgencyColumn.Usertype)))?null:(System.Int32?)reader[((int)ViewReportProductionCompanyAgencyColumn.Usertype)];
					//entity.Usertype = (Convert.IsDBNull(reader["Usertype"]))?(int)0:(System.Int32?)reader["Usertype"];
					entity.CompanyName = (reader.IsDBNull(((int)ViewReportProductionCompanyAgencyColumn.CompanyName)))?null:(System.String)reader[((int)ViewReportProductionCompanyAgencyColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.NoOfBooking = (reader.IsDBNull(((int)ViewReportProductionCompanyAgencyColumn.NoOfBooking)))?null:(System.Int32?)reader[((int)ViewReportProductionCompanyAgencyColumn.NoOfBooking)];
					//entity.NoOfBooking = (Convert.IsDBNull(reader["NoOfBooking"]))?(int)0:(System.Int32?)reader["NoOfBooking"];
					entity.NoOfRequest = (reader.IsDBNull(((int)ViewReportProductionCompanyAgencyColumn.NoOfRequest)))?null:(System.Int32?)reader[((int)ViewReportProductionCompanyAgencyColumn.NoOfRequest)];
					//entity.NoOfRequest = (Convert.IsDBNull(reader["NoOfRequest"]))?(int)0:(System.Int32?)reader["NoOfRequest"];
					entity.NoOfBookingCancled = (reader.IsDBNull(((int)ViewReportProductionCompanyAgencyColumn.NoOfBookingCancled)))?null:(System.Int32?)reader[((int)ViewReportProductionCompanyAgencyColumn.NoOfBookingCancled)];
					//entity.NoOfBookingCancled = (Convert.IsDBNull(reader["NoOfBookingCancled"]))?(int)0:(System.Int32?)reader["NoOfBookingCancled"];
					entity.NoOfRequestCancled = (reader.IsDBNull(((int)ViewReportProductionCompanyAgencyColumn.NoOfRequestCancled)))?null:(System.Int32?)reader[((int)ViewReportProductionCompanyAgencyColumn.NoOfRequestCancled)];
					//entity.NoOfRequestCancled = (Convert.IsDBNull(reader["NoOfRequestCancled"]))?(int)0:(System.Int32?)reader["NoOfRequestCancled"];
					entity.TotalRevenueAmountFromBooking = (reader.IsDBNull(((int)ViewReportProductionCompanyAgencyColumn.TotalRevenueAmountFromBooking)))?null:(System.Decimal?)reader[((int)ViewReportProductionCompanyAgencyColumn.TotalRevenueAmountFromBooking)];
					//entity.TotalRevenueAmountFromBooking = (Convert.IsDBNull(reader["TotalRevenueAmountFromBooking"]))?0.0m:(System.Decimal?)reader["TotalRevenueAmountFromBooking"];
					entity.Modifyrevenuefromrequest = (reader.IsDBNull(((int)ViewReportProductionCompanyAgencyColumn.Modifyrevenuefromrequest)))?null:(System.Decimal?)reader[((int)ViewReportProductionCompanyAgencyColumn.Modifyrevenuefromrequest)];
					//entity.Modifyrevenuefromrequest = (Convert.IsDBNull(reader["Modifyrevenuefromrequest"]))?0.0m:(System.Decimal?)reader["Modifyrevenuefromrequest"];
					entity.Months = (reader.IsDBNull(((int)ViewReportProductionCompanyAgencyColumn.Months)))?null:(System.Int32?)reader[((int)ViewReportProductionCompanyAgencyColumn.Months)];
					//entity.Months = (Convert.IsDBNull(reader["Months"]))?(int)0:(System.Int32?)reader["Months"];
					entity.Years = (reader.IsDBNull(((int)ViewReportProductionCompanyAgencyColumn.Years)))?null:(System.Int32?)reader[((int)ViewReportProductionCompanyAgencyColumn.Years)];
					//entity.Years = (Convert.IsDBNull(reader["Years"]))?(int)0:(System.Int32?)reader["Years"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewReportProductionCompanyAgency"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewReportProductionCompanyAgency"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewReportProductionCompanyAgency entity)
		{
			reader.Read();
			entity.CountryId = (System.Int64)reader[((int)ViewReportProductionCompanyAgencyColumn.CountryId)];
			//entity.CountryId = (Convert.IsDBNull(reader["CountryID"]))?(long)0:(System.Int64)reader["CountryID"];
			entity.CountryName = (reader.IsDBNull(((int)ViewReportProductionCompanyAgencyColumn.CountryName)))?null:(System.String)reader[((int)ViewReportProductionCompanyAgencyColumn.CountryName)];
			//entity.CountryName = (Convert.IsDBNull(reader["CountryName"]))?string.Empty:(System.String)reader["CountryName"];
			entity.CityName = (reader.IsDBNull(((int)ViewReportProductionCompanyAgencyColumn.CityName)))?null:(System.String)reader[((int)ViewReportProductionCompanyAgencyColumn.CityName)];
			//entity.CityName = (Convert.IsDBNull(reader["CityName"]))?string.Empty:(System.String)reader["CityName"];
			entity.Userid = (System.Int64)reader[((int)ViewReportProductionCompanyAgencyColumn.Userid)];
			//entity.Userid = (Convert.IsDBNull(reader["Userid"]))?(long)0:(System.Int64)reader["Userid"];
			entity.Usertype = (reader.IsDBNull(((int)ViewReportProductionCompanyAgencyColumn.Usertype)))?null:(System.Int32?)reader[((int)ViewReportProductionCompanyAgencyColumn.Usertype)];
			//entity.Usertype = (Convert.IsDBNull(reader["Usertype"]))?(int)0:(System.Int32?)reader["Usertype"];
			entity.CompanyName = (reader.IsDBNull(((int)ViewReportProductionCompanyAgencyColumn.CompanyName)))?null:(System.String)reader[((int)ViewReportProductionCompanyAgencyColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.NoOfBooking = (reader.IsDBNull(((int)ViewReportProductionCompanyAgencyColumn.NoOfBooking)))?null:(System.Int32?)reader[((int)ViewReportProductionCompanyAgencyColumn.NoOfBooking)];
			//entity.NoOfBooking = (Convert.IsDBNull(reader["NoOfBooking"]))?(int)0:(System.Int32?)reader["NoOfBooking"];
			entity.NoOfRequest = (reader.IsDBNull(((int)ViewReportProductionCompanyAgencyColumn.NoOfRequest)))?null:(System.Int32?)reader[((int)ViewReportProductionCompanyAgencyColumn.NoOfRequest)];
			//entity.NoOfRequest = (Convert.IsDBNull(reader["NoOfRequest"]))?(int)0:(System.Int32?)reader["NoOfRequest"];
			entity.NoOfBookingCancled = (reader.IsDBNull(((int)ViewReportProductionCompanyAgencyColumn.NoOfBookingCancled)))?null:(System.Int32?)reader[((int)ViewReportProductionCompanyAgencyColumn.NoOfBookingCancled)];
			//entity.NoOfBookingCancled = (Convert.IsDBNull(reader["NoOfBookingCancled"]))?(int)0:(System.Int32?)reader["NoOfBookingCancled"];
			entity.NoOfRequestCancled = (reader.IsDBNull(((int)ViewReportProductionCompanyAgencyColumn.NoOfRequestCancled)))?null:(System.Int32?)reader[((int)ViewReportProductionCompanyAgencyColumn.NoOfRequestCancled)];
			//entity.NoOfRequestCancled = (Convert.IsDBNull(reader["NoOfRequestCancled"]))?(int)0:(System.Int32?)reader["NoOfRequestCancled"];
			entity.TotalRevenueAmountFromBooking = (reader.IsDBNull(((int)ViewReportProductionCompanyAgencyColumn.TotalRevenueAmountFromBooking)))?null:(System.Decimal?)reader[((int)ViewReportProductionCompanyAgencyColumn.TotalRevenueAmountFromBooking)];
			//entity.TotalRevenueAmountFromBooking = (Convert.IsDBNull(reader["TotalRevenueAmountFromBooking"]))?0.0m:(System.Decimal?)reader["TotalRevenueAmountFromBooking"];
			entity.Modifyrevenuefromrequest = (reader.IsDBNull(((int)ViewReportProductionCompanyAgencyColumn.Modifyrevenuefromrequest)))?null:(System.Decimal?)reader[((int)ViewReportProductionCompanyAgencyColumn.Modifyrevenuefromrequest)];
			//entity.Modifyrevenuefromrequest = (Convert.IsDBNull(reader["Modifyrevenuefromrequest"]))?0.0m:(System.Decimal?)reader["Modifyrevenuefromrequest"];
			entity.Months = (reader.IsDBNull(((int)ViewReportProductionCompanyAgencyColumn.Months)))?null:(System.Int32?)reader[((int)ViewReportProductionCompanyAgencyColumn.Months)];
			//entity.Months = (Convert.IsDBNull(reader["Months"]))?(int)0:(System.Int32?)reader["Months"];
			entity.Years = (reader.IsDBNull(((int)ViewReportProductionCompanyAgencyColumn.Years)))?null:(System.Int32?)reader[((int)ViewReportProductionCompanyAgencyColumn.Years)];
			//entity.Years = (Convert.IsDBNull(reader["Years"]))?(int)0:(System.Int32?)reader["Years"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewReportProductionCompanyAgency"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewReportProductionCompanyAgency"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewReportProductionCompanyAgency entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CountryId = (Convert.IsDBNull(dataRow["CountryID"]))?(long)0:(System.Int64)dataRow["CountryID"];
			entity.CountryName = (Convert.IsDBNull(dataRow["CountryName"]))?string.Empty:(System.String)dataRow["CountryName"];
			entity.CityName = (Convert.IsDBNull(dataRow["CityName"]))?string.Empty:(System.String)dataRow["CityName"];
			entity.Userid = (Convert.IsDBNull(dataRow["Userid"]))?(long)0:(System.Int64)dataRow["Userid"];
			entity.Usertype = (Convert.IsDBNull(dataRow["Usertype"]))?(int)0:(System.Int32?)dataRow["Usertype"];
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.NoOfBooking = (Convert.IsDBNull(dataRow["NoOfBooking"]))?(int)0:(System.Int32?)dataRow["NoOfBooking"];
			entity.NoOfRequest = (Convert.IsDBNull(dataRow["NoOfRequest"]))?(int)0:(System.Int32?)dataRow["NoOfRequest"];
			entity.NoOfBookingCancled = (Convert.IsDBNull(dataRow["NoOfBookingCancled"]))?(int)0:(System.Int32?)dataRow["NoOfBookingCancled"];
			entity.NoOfRequestCancled = (Convert.IsDBNull(dataRow["NoOfRequestCancled"]))?(int)0:(System.Int32?)dataRow["NoOfRequestCancled"];
			entity.TotalRevenueAmountFromBooking = (Convert.IsDBNull(dataRow["TotalRevenueAmountFromBooking"]))?0.0m:(System.Decimal?)dataRow["TotalRevenueAmountFromBooking"];
			entity.Modifyrevenuefromrequest = (Convert.IsDBNull(dataRow["Modifyrevenuefromrequest"]))?0.0m:(System.Decimal?)dataRow["Modifyrevenuefromrequest"];
			entity.Months = (Convert.IsDBNull(dataRow["Months"]))?(int)0:(System.Int32?)dataRow["Months"];
			entity.Years = (Convert.IsDBNull(dataRow["Years"]))?(int)0:(System.Int32?)dataRow["Years"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewReportProductionCompanyAgencyFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportProductionCompanyAgency"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportProductionCompanyAgencyFilterBuilder : SqlFilterBuilder<ViewReportProductionCompanyAgencyColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionCompanyAgencyFilterBuilder class.
		/// </summary>
		public ViewReportProductionCompanyAgencyFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionCompanyAgencyFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportProductionCompanyAgencyFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionCompanyAgencyFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportProductionCompanyAgencyFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportProductionCompanyAgencyFilterBuilder

	#region ViewReportProductionCompanyAgencyParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportProductionCompanyAgency"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportProductionCompanyAgencyParameterBuilder : ParameterizedSqlFilterBuilder<ViewReportProductionCompanyAgencyColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionCompanyAgencyParameterBuilder class.
		/// </summary>
		public ViewReportProductionCompanyAgencyParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionCompanyAgencyParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportProductionCompanyAgencyParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionCompanyAgencyParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportProductionCompanyAgencyParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportProductionCompanyAgencyParameterBuilder
	
	#region ViewReportProductionCompanyAgencySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportProductionCompanyAgency"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewReportProductionCompanyAgencySortBuilder : SqlSortBuilder<ViewReportProductionCompanyAgencyColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionCompanyAgencySqlSortBuilder class.
		/// </summary>
		public ViewReportProductionCompanyAgencySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewReportProductionCompanyAgencySortBuilder

} // end namespace
