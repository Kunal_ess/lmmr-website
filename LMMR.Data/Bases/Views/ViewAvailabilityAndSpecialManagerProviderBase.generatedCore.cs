﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewAvailabilityAndSpecialManagerProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewAvailabilityAndSpecialManagerProviderBaseCore : EntityViewProviderBase<ViewAvailabilityAndSpecialManager>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewAvailabilityAndSpecialManager&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewAvailabilityAndSpecialManager&gt;"/></returns>
		protected static VList&lt;ViewAvailabilityAndSpecialManager&gt; Fill(DataSet dataSet, VList<ViewAvailabilityAndSpecialManager> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewAvailabilityAndSpecialManager>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewAvailabilityAndSpecialManager&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewAvailabilityAndSpecialManager>"/></returns>
		protected static VList&lt;ViewAvailabilityAndSpecialManager&gt; Fill(DataTable dataTable, VList<ViewAvailabilityAndSpecialManager> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewAvailabilityAndSpecialManager c = new ViewAvailabilityAndSpecialManager();
					c.Id = (Convert.IsDBNull(row["Id"]))?(long)0:(System.Int64)row["Id"];
					c.SpecialPriceDate = (Convert.IsDBNull(row["SpecialPriceDate"]))?DateTime.MinValue:(System.DateTime?)row["SpecialPriceDate"];
					c.DdrPercent = (Convert.IsDBNull(row["DDRPercent"]))?0.0m:(System.Decimal?)row["DDRPercent"];
					c.MeetingroomCount = (Convert.IsDBNull(row["MeetingroomCount"]))?(int)0:(System.Int32?)row["MeetingroomCount"];
					c.BedroomCount = (Convert.IsDBNull(row["BedroomCount"]))?(long)0:(System.Int64?)row["BedroomCount"];
					c.BookedMeetingRoom = (Convert.IsDBNull(row["BookedMeetingRoom"]))?0.0m:(System.Decimal?)row["BookedMeetingRoom"];
					c.BookedBedroom = (Convert.IsDBNull(row["BookedBedroom"]))?(long)0:(System.Int64?)row["BookedBedroom"];
					c.Name = (Convert.IsDBNull(row["Name"]))?string.Empty:(System.String)row["Name"];
					c.CountryId = (Convert.IsDBNull(row["CountryId"]))?(long)0:(System.Int64)row["CountryId"];
					c.CityId = (Convert.IsDBNull(row["CityId"]))?(long)0:(System.Int64)row["CityId"];
					c.ZoneId = (Convert.IsDBNull(row["ZoneId"]))?(long)0:(System.Int64)row["ZoneId"];
					c.Stars = (Convert.IsDBNull(row["Stars"]))?(int)0:(System.Int32?)row["Stars"];
					c.IsActive = (Convert.IsDBNull(row["IsActive"]))?false:(System.Boolean)row["IsActive"];
					c.GoOnline = (Convert.IsDBNull(row["GoOnline"]))?false:(System.Boolean?)row["GoOnline"];
					c.IsRemoved = (Convert.IsDBNull(row["IsRemoved"]))?false:(System.Boolean)row["IsRemoved"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewAvailabilityAndSpecialManager&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewAvailabilityAndSpecialManager&gt;"/></returns>
		protected VList<ViewAvailabilityAndSpecialManager> Fill(IDataReader reader, VList<ViewAvailabilityAndSpecialManager> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewAvailabilityAndSpecialManager entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewAvailabilityAndSpecialManager>("ViewAvailabilityAndSpecialManager",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewAvailabilityAndSpecialManager();
					}
					
					entity.SuppressEntityEvents = true;

					entity.Id = (System.Int64)reader[((int)ViewAvailabilityAndSpecialManagerColumn.Id)];
					//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
					entity.SpecialPriceDate = (reader.IsDBNull(((int)ViewAvailabilityAndSpecialManagerColumn.SpecialPriceDate)))?null:(System.DateTime?)reader[((int)ViewAvailabilityAndSpecialManagerColumn.SpecialPriceDate)];
					//entity.SpecialPriceDate = (Convert.IsDBNull(reader["SpecialPriceDate"]))?DateTime.MinValue:(System.DateTime?)reader["SpecialPriceDate"];
					entity.DdrPercent = (reader.IsDBNull(((int)ViewAvailabilityAndSpecialManagerColumn.DdrPercent)))?null:(System.Decimal?)reader[((int)ViewAvailabilityAndSpecialManagerColumn.DdrPercent)];
					//entity.DdrPercent = (Convert.IsDBNull(reader["DDRPercent"]))?0.0m:(System.Decimal?)reader["DDRPercent"];
					entity.MeetingroomCount = (reader.IsDBNull(((int)ViewAvailabilityAndSpecialManagerColumn.MeetingroomCount)))?null:(System.Int32?)reader[((int)ViewAvailabilityAndSpecialManagerColumn.MeetingroomCount)];
					//entity.MeetingroomCount = (Convert.IsDBNull(reader["MeetingroomCount"]))?(int)0:(System.Int32?)reader["MeetingroomCount"];
					entity.BedroomCount = (reader.IsDBNull(((int)ViewAvailabilityAndSpecialManagerColumn.BedroomCount)))?null:(System.Int64?)reader[((int)ViewAvailabilityAndSpecialManagerColumn.BedroomCount)];
					//entity.BedroomCount = (Convert.IsDBNull(reader["BedroomCount"]))?(long)0:(System.Int64?)reader["BedroomCount"];
					entity.BookedMeetingRoom = (reader.IsDBNull(((int)ViewAvailabilityAndSpecialManagerColumn.BookedMeetingRoom)))?null:(System.Decimal?)reader[((int)ViewAvailabilityAndSpecialManagerColumn.BookedMeetingRoom)];
					//entity.BookedMeetingRoom = (Convert.IsDBNull(reader["BookedMeetingRoom"]))?0.0m:(System.Decimal?)reader["BookedMeetingRoom"];
					entity.BookedBedroom = (reader.IsDBNull(((int)ViewAvailabilityAndSpecialManagerColumn.BookedBedroom)))?null:(System.Int64?)reader[((int)ViewAvailabilityAndSpecialManagerColumn.BookedBedroom)];
					//entity.BookedBedroom = (Convert.IsDBNull(reader["BookedBedroom"]))?(long)0:(System.Int64?)reader["BookedBedroom"];
					entity.Name = (reader.IsDBNull(((int)ViewAvailabilityAndSpecialManagerColumn.Name)))?null:(System.String)reader[((int)ViewAvailabilityAndSpecialManagerColumn.Name)];
					//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
					entity.CountryId = (System.Int64)reader[((int)ViewAvailabilityAndSpecialManagerColumn.CountryId)];
					//entity.CountryId = (Convert.IsDBNull(reader["CountryId"]))?(long)0:(System.Int64)reader["CountryId"];
					entity.CityId = (System.Int64)reader[((int)ViewAvailabilityAndSpecialManagerColumn.CityId)];
					//entity.CityId = (Convert.IsDBNull(reader["CityId"]))?(long)0:(System.Int64)reader["CityId"];
					entity.ZoneId = (System.Int64)reader[((int)ViewAvailabilityAndSpecialManagerColumn.ZoneId)];
					//entity.ZoneId = (Convert.IsDBNull(reader["ZoneId"]))?(long)0:(System.Int64)reader["ZoneId"];
					entity.Stars = (reader.IsDBNull(((int)ViewAvailabilityAndSpecialManagerColumn.Stars)))?null:(System.Int32?)reader[((int)ViewAvailabilityAndSpecialManagerColumn.Stars)];
					//entity.Stars = (Convert.IsDBNull(reader["Stars"]))?(int)0:(System.Int32?)reader["Stars"];
					entity.IsActive = (System.Boolean)reader[((int)ViewAvailabilityAndSpecialManagerColumn.IsActive)];
					//entity.IsActive = (Convert.IsDBNull(reader["IsActive"]))?false:(System.Boolean)reader["IsActive"];
					entity.GoOnline = (reader.IsDBNull(((int)ViewAvailabilityAndSpecialManagerColumn.GoOnline)))?null:(System.Boolean?)reader[((int)ViewAvailabilityAndSpecialManagerColumn.GoOnline)];
					//entity.GoOnline = (Convert.IsDBNull(reader["GoOnline"]))?false:(System.Boolean?)reader["GoOnline"];
					entity.IsRemoved = (System.Boolean)reader[((int)ViewAvailabilityAndSpecialManagerColumn.IsRemoved)];
					//entity.IsRemoved = (Convert.IsDBNull(reader["IsRemoved"]))?false:(System.Boolean)reader["IsRemoved"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewAvailabilityAndSpecialManager"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewAvailabilityAndSpecialManager"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewAvailabilityAndSpecialManager entity)
		{
			reader.Read();
			entity.Id = (System.Int64)reader[((int)ViewAvailabilityAndSpecialManagerColumn.Id)];
			//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
			entity.SpecialPriceDate = (reader.IsDBNull(((int)ViewAvailabilityAndSpecialManagerColumn.SpecialPriceDate)))?null:(System.DateTime?)reader[((int)ViewAvailabilityAndSpecialManagerColumn.SpecialPriceDate)];
			//entity.SpecialPriceDate = (Convert.IsDBNull(reader["SpecialPriceDate"]))?DateTime.MinValue:(System.DateTime?)reader["SpecialPriceDate"];
			entity.DdrPercent = (reader.IsDBNull(((int)ViewAvailabilityAndSpecialManagerColumn.DdrPercent)))?null:(System.Decimal?)reader[((int)ViewAvailabilityAndSpecialManagerColumn.DdrPercent)];
			//entity.DdrPercent = (Convert.IsDBNull(reader["DDRPercent"]))?0.0m:(System.Decimal?)reader["DDRPercent"];
			entity.MeetingroomCount = (reader.IsDBNull(((int)ViewAvailabilityAndSpecialManagerColumn.MeetingroomCount)))?null:(System.Int32?)reader[((int)ViewAvailabilityAndSpecialManagerColumn.MeetingroomCount)];
			//entity.MeetingroomCount = (Convert.IsDBNull(reader["MeetingroomCount"]))?(int)0:(System.Int32?)reader["MeetingroomCount"];
			entity.BedroomCount = (reader.IsDBNull(((int)ViewAvailabilityAndSpecialManagerColumn.BedroomCount)))?null:(System.Int64?)reader[((int)ViewAvailabilityAndSpecialManagerColumn.BedroomCount)];
			//entity.BedroomCount = (Convert.IsDBNull(reader["BedroomCount"]))?(long)0:(System.Int64?)reader["BedroomCount"];
			entity.BookedMeetingRoom = (reader.IsDBNull(((int)ViewAvailabilityAndSpecialManagerColumn.BookedMeetingRoom)))?null:(System.Decimal?)reader[((int)ViewAvailabilityAndSpecialManagerColumn.BookedMeetingRoom)];
			//entity.BookedMeetingRoom = (Convert.IsDBNull(reader["BookedMeetingRoom"]))?0.0m:(System.Decimal?)reader["BookedMeetingRoom"];
			entity.BookedBedroom = (reader.IsDBNull(((int)ViewAvailabilityAndSpecialManagerColumn.BookedBedroom)))?null:(System.Int64?)reader[((int)ViewAvailabilityAndSpecialManagerColumn.BookedBedroom)];
			//entity.BookedBedroom = (Convert.IsDBNull(reader["BookedBedroom"]))?(long)0:(System.Int64?)reader["BookedBedroom"];
			entity.Name = (reader.IsDBNull(((int)ViewAvailabilityAndSpecialManagerColumn.Name)))?null:(System.String)reader[((int)ViewAvailabilityAndSpecialManagerColumn.Name)];
			//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
			entity.CountryId = (System.Int64)reader[((int)ViewAvailabilityAndSpecialManagerColumn.CountryId)];
			//entity.CountryId = (Convert.IsDBNull(reader["CountryId"]))?(long)0:(System.Int64)reader["CountryId"];
			entity.CityId = (System.Int64)reader[((int)ViewAvailabilityAndSpecialManagerColumn.CityId)];
			//entity.CityId = (Convert.IsDBNull(reader["CityId"]))?(long)0:(System.Int64)reader["CityId"];
			entity.ZoneId = (System.Int64)reader[((int)ViewAvailabilityAndSpecialManagerColumn.ZoneId)];
			//entity.ZoneId = (Convert.IsDBNull(reader["ZoneId"]))?(long)0:(System.Int64)reader["ZoneId"];
			entity.Stars = (reader.IsDBNull(((int)ViewAvailabilityAndSpecialManagerColumn.Stars)))?null:(System.Int32?)reader[((int)ViewAvailabilityAndSpecialManagerColumn.Stars)];
			//entity.Stars = (Convert.IsDBNull(reader["Stars"]))?(int)0:(System.Int32?)reader["Stars"];
			entity.IsActive = (System.Boolean)reader[((int)ViewAvailabilityAndSpecialManagerColumn.IsActive)];
			//entity.IsActive = (Convert.IsDBNull(reader["IsActive"]))?false:(System.Boolean)reader["IsActive"];
			entity.GoOnline = (reader.IsDBNull(((int)ViewAvailabilityAndSpecialManagerColumn.GoOnline)))?null:(System.Boolean?)reader[((int)ViewAvailabilityAndSpecialManagerColumn.GoOnline)];
			//entity.GoOnline = (Convert.IsDBNull(reader["GoOnline"]))?false:(System.Boolean?)reader["GoOnline"];
			entity.IsRemoved = (System.Boolean)reader[((int)ViewAvailabilityAndSpecialManagerColumn.IsRemoved)];
			//entity.IsRemoved = (Convert.IsDBNull(reader["IsRemoved"]))?false:(System.Boolean)reader["IsRemoved"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewAvailabilityAndSpecialManager"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewAvailabilityAndSpecialManager"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewAvailabilityAndSpecialManager entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (Convert.IsDBNull(dataRow["Id"]))?(long)0:(System.Int64)dataRow["Id"];
			entity.SpecialPriceDate = (Convert.IsDBNull(dataRow["SpecialPriceDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["SpecialPriceDate"];
			entity.DdrPercent = (Convert.IsDBNull(dataRow["DDRPercent"]))?0.0m:(System.Decimal?)dataRow["DDRPercent"];
			entity.MeetingroomCount = (Convert.IsDBNull(dataRow["MeetingroomCount"]))?(int)0:(System.Int32?)dataRow["MeetingroomCount"];
			entity.BedroomCount = (Convert.IsDBNull(dataRow["BedroomCount"]))?(long)0:(System.Int64?)dataRow["BedroomCount"];
			entity.BookedMeetingRoom = (Convert.IsDBNull(dataRow["BookedMeetingRoom"]))?0.0m:(System.Decimal?)dataRow["BookedMeetingRoom"];
			entity.BookedBedroom = (Convert.IsDBNull(dataRow["BookedBedroom"]))?(long)0:(System.Int64?)dataRow["BookedBedroom"];
			entity.Name = (Convert.IsDBNull(dataRow["Name"]))?string.Empty:(System.String)dataRow["Name"];
			entity.CountryId = (Convert.IsDBNull(dataRow["CountryId"]))?(long)0:(System.Int64)dataRow["CountryId"];
			entity.CityId = (Convert.IsDBNull(dataRow["CityId"]))?(long)0:(System.Int64)dataRow["CityId"];
			entity.ZoneId = (Convert.IsDBNull(dataRow["ZoneId"]))?(long)0:(System.Int64)dataRow["ZoneId"];
			entity.Stars = (Convert.IsDBNull(dataRow["Stars"]))?(int)0:(System.Int32?)dataRow["Stars"];
			entity.IsActive = (Convert.IsDBNull(dataRow["IsActive"]))?false:(System.Boolean)dataRow["IsActive"];
			entity.GoOnline = (Convert.IsDBNull(dataRow["GoOnline"]))?false:(System.Boolean?)dataRow["GoOnline"];
			entity.IsRemoved = (Convert.IsDBNull(dataRow["IsRemoved"]))?false:(System.Boolean)dataRow["IsRemoved"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewAvailabilityAndSpecialManagerFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewAvailabilityAndSpecialManager"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewAvailabilityAndSpecialManagerFilterBuilder : SqlFilterBuilder<ViewAvailabilityAndSpecialManagerColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityAndSpecialManagerFilterBuilder class.
		/// </summary>
		public ViewAvailabilityAndSpecialManagerFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityAndSpecialManagerFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewAvailabilityAndSpecialManagerFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityAndSpecialManagerFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewAvailabilityAndSpecialManagerFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewAvailabilityAndSpecialManagerFilterBuilder

	#region ViewAvailabilityAndSpecialManagerParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewAvailabilityAndSpecialManager"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewAvailabilityAndSpecialManagerParameterBuilder : ParameterizedSqlFilterBuilder<ViewAvailabilityAndSpecialManagerColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityAndSpecialManagerParameterBuilder class.
		/// </summary>
		public ViewAvailabilityAndSpecialManagerParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityAndSpecialManagerParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewAvailabilityAndSpecialManagerParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityAndSpecialManagerParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewAvailabilityAndSpecialManagerParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewAvailabilityAndSpecialManagerParameterBuilder
	
	#region ViewAvailabilityAndSpecialManagerSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewAvailabilityAndSpecialManager"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewAvailabilityAndSpecialManagerSortBuilder : SqlSortBuilder<ViewAvailabilityAndSpecialManagerColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewAvailabilityAndSpecialManagerSqlSortBuilder class.
		/// </summary>
		public ViewAvailabilityAndSpecialManagerSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewAvailabilityAndSpecialManagerSortBuilder

} // end namespace
