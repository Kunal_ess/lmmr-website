﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewWsCheckAvailabilityProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewWsCheckAvailabilityProviderBaseCore : EntityViewProviderBase<ViewWsCheckAvailability>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewWsCheckAvailability&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewWsCheckAvailability&gt;"/></returns>
		protected static VList&lt;ViewWsCheckAvailability&gt; Fill(DataSet dataSet, VList<ViewWsCheckAvailability> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewWsCheckAvailability>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewWsCheckAvailability&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewWsCheckAvailability>"/></returns>
		protected static VList&lt;ViewWsCheckAvailability&gt; Fill(DataTable dataTable, VList<ViewWsCheckAvailability> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewWsCheckAvailability c = new ViewWsCheckAvailability();
					c.Id = (Convert.IsDBNull(row["Id"]))?(long)0:(System.Int64)row["Id"];
					c.Name = (Convert.IsDBNull(row["Name"]))?string.Empty:(System.String)row["Name"];
					c.CountryId = (Convert.IsDBNull(row["CountryId"]))?(long)0:(System.Int64)row["CountryId"];
					c.CountryName = (Convert.IsDBNull(row["CountryName"]))?string.Empty:(System.String)row["CountryName"];
					c.CityId = (Convert.IsDBNull(row["CityId"]))?(long)0:(System.Int64)row["CityId"];
					c.CityName = (Convert.IsDBNull(row["CityName"]))?string.Empty:(System.String)row["CityName"];
					c.AvailabilityDate = (Convert.IsDBNull(row["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)row["AvailabilityDate"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewWsCheckAvailability&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewWsCheckAvailability&gt;"/></returns>
		protected VList<ViewWsCheckAvailability> Fill(IDataReader reader, VList<ViewWsCheckAvailability> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewWsCheckAvailability entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewWsCheckAvailability>("ViewWsCheckAvailability",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewWsCheckAvailability();
					}
					
					entity.SuppressEntityEvents = true;

					entity.Id = (System.Int64)reader[((int)ViewWsCheckAvailabilityColumn.Id)];
					//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
					entity.Name = (reader.IsDBNull(((int)ViewWsCheckAvailabilityColumn.Name)))?null:(System.String)reader[((int)ViewWsCheckAvailabilityColumn.Name)];
					//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
					entity.CountryId = (System.Int64)reader[((int)ViewWsCheckAvailabilityColumn.CountryId)];
					//entity.CountryId = (Convert.IsDBNull(reader["CountryId"]))?(long)0:(System.Int64)reader["CountryId"];
					entity.CountryName = (reader.IsDBNull(((int)ViewWsCheckAvailabilityColumn.CountryName)))?null:(System.String)reader[((int)ViewWsCheckAvailabilityColumn.CountryName)];
					//entity.CountryName = (Convert.IsDBNull(reader["CountryName"]))?string.Empty:(System.String)reader["CountryName"];
					entity.CityId = (System.Int64)reader[((int)ViewWsCheckAvailabilityColumn.CityId)];
					//entity.CityId = (Convert.IsDBNull(reader["CityId"]))?(long)0:(System.Int64)reader["CityId"];
					entity.CityName = (reader.IsDBNull(((int)ViewWsCheckAvailabilityColumn.CityName)))?null:(System.String)reader[((int)ViewWsCheckAvailabilityColumn.CityName)];
					//entity.CityName = (Convert.IsDBNull(reader["CityName"]))?string.Empty:(System.String)reader["CityName"];
					entity.AvailabilityDate = (reader.IsDBNull(((int)ViewWsCheckAvailabilityColumn.AvailabilityDate)))?null:(System.DateTime?)reader[((int)ViewWsCheckAvailabilityColumn.AvailabilityDate)];
					//entity.AvailabilityDate = (Convert.IsDBNull(reader["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)reader["AvailabilityDate"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewWsCheckAvailability"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewWsCheckAvailability"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewWsCheckAvailability entity)
		{
			reader.Read();
			entity.Id = (System.Int64)reader[((int)ViewWsCheckAvailabilityColumn.Id)];
			//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
			entity.Name = (reader.IsDBNull(((int)ViewWsCheckAvailabilityColumn.Name)))?null:(System.String)reader[((int)ViewWsCheckAvailabilityColumn.Name)];
			//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
			entity.CountryId = (System.Int64)reader[((int)ViewWsCheckAvailabilityColumn.CountryId)];
			//entity.CountryId = (Convert.IsDBNull(reader["CountryId"]))?(long)0:(System.Int64)reader["CountryId"];
			entity.CountryName = (reader.IsDBNull(((int)ViewWsCheckAvailabilityColumn.CountryName)))?null:(System.String)reader[((int)ViewWsCheckAvailabilityColumn.CountryName)];
			//entity.CountryName = (Convert.IsDBNull(reader["CountryName"]))?string.Empty:(System.String)reader["CountryName"];
			entity.CityId = (System.Int64)reader[((int)ViewWsCheckAvailabilityColumn.CityId)];
			//entity.CityId = (Convert.IsDBNull(reader["CityId"]))?(long)0:(System.Int64)reader["CityId"];
			entity.CityName = (reader.IsDBNull(((int)ViewWsCheckAvailabilityColumn.CityName)))?null:(System.String)reader[((int)ViewWsCheckAvailabilityColumn.CityName)];
			//entity.CityName = (Convert.IsDBNull(reader["CityName"]))?string.Empty:(System.String)reader["CityName"];
			entity.AvailabilityDate = (reader.IsDBNull(((int)ViewWsCheckAvailabilityColumn.AvailabilityDate)))?null:(System.DateTime?)reader[((int)ViewWsCheckAvailabilityColumn.AvailabilityDate)];
			//entity.AvailabilityDate = (Convert.IsDBNull(reader["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)reader["AvailabilityDate"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewWsCheckAvailability"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewWsCheckAvailability"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewWsCheckAvailability entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (Convert.IsDBNull(dataRow["Id"]))?(long)0:(System.Int64)dataRow["Id"];
			entity.Name = (Convert.IsDBNull(dataRow["Name"]))?string.Empty:(System.String)dataRow["Name"];
			entity.CountryId = (Convert.IsDBNull(dataRow["CountryId"]))?(long)0:(System.Int64)dataRow["CountryId"];
			entity.CountryName = (Convert.IsDBNull(dataRow["CountryName"]))?string.Empty:(System.String)dataRow["CountryName"];
			entity.CityId = (Convert.IsDBNull(dataRow["CityId"]))?(long)0:(System.Int64)dataRow["CityId"];
			entity.CityName = (Convert.IsDBNull(dataRow["CityName"]))?string.Empty:(System.String)dataRow["CityName"];
			entity.AvailabilityDate = (Convert.IsDBNull(dataRow["AvailabilityDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["AvailabilityDate"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewWsCheckAvailabilityFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewWsCheckAvailability"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewWsCheckAvailabilityFilterBuilder : SqlFilterBuilder<ViewWsCheckAvailabilityColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewWsCheckAvailabilityFilterBuilder class.
		/// </summary>
		public ViewWsCheckAvailabilityFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewWsCheckAvailabilityFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewWsCheckAvailabilityFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewWsCheckAvailabilityFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewWsCheckAvailabilityFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewWsCheckAvailabilityFilterBuilder

	#region ViewWsCheckAvailabilityParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewWsCheckAvailability"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewWsCheckAvailabilityParameterBuilder : ParameterizedSqlFilterBuilder<ViewWsCheckAvailabilityColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewWsCheckAvailabilityParameterBuilder class.
		/// </summary>
		public ViewWsCheckAvailabilityParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewWsCheckAvailabilityParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewWsCheckAvailabilityParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewWsCheckAvailabilityParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewWsCheckAvailabilityParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewWsCheckAvailabilityParameterBuilder
	
	#region ViewWsCheckAvailabilitySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewWsCheckAvailability"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewWsCheckAvailabilitySortBuilder : SqlSortBuilder<ViewWsCheckAvailabilityColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewWsCheckAvailabilitySqlSortBuilder class.
		/// </summary>
		public ViewWsCheckAvailabilitySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewWsCheckAvailabilitySortBuilder

} // end namespace
