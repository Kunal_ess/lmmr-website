﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewReportProductionReportHotelProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewReportProductionReportHotelProviderBaseCore : EntityViewProviderBase<ViewReportProductionReportHotel>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewReportProductionReportHotel&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewReportProductionReportHotel&gt;"/></returns>
		protected static VList&lt;ViewReportProductionReportHotel&gt; Fill(DataSet dataSet, VList<ViewReportProductionReportHotel> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewReportProductionReportHotel>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewReportProductionReportHotel&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewReportProductionReportHotel>"/></returns>
		protected static VList&lt;ViewReportProductionReportHotel&gt; Fill(DataTable dataTable, VList<ViewReportProductionReportHotel> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewReportProductionReportHotel c = new ViewReportProductionReportHotel();
					c.CountryId = (Convert.IsDBNull(row["CountryID"]))?(long)0:(System.Int64)row["CountryID"];
					c.CountryName = (Convert.IsDBNull(row["CountryName"]))?string.Empty:(System.String)row["CountryName"];
					c.CityId = (Convert.IsDBNull(row["CityID"]))?(long)0:(System.Int64)row["CityID"];
					c.City = (Convert.IsDBNull(row["City"]))?string.Empty:(System.String)row["City"];
					c.Id = (Convert.IsDBNull(row["Id"]))?(long)0:(System.Int64)row["Id"];
					c.HotelName = (Convert.IsDBNull(row["HotelName"]))?string.Empty:(System.String)row["HotelName"];
					c.NoOfBooking = (Convert.IsDBNull(row["NoOfBooking"]))?(int)0:(System.Int32?)row["NoOfBooking"];
					c.NoOfRequest = (Convert.IsDBNull(row["NoOfRequest"]))?(int)0:(System.Int32?)row["NoOfRequest"];
					c.NoOfBookingCancled = (Convert.IsDBNull(row["NoOfBookingCancled"]))?(int)0:(System.Int32?)row["NoOfBookingCancled"];
					c.NoOfRequestCancled = (Convert.IsDBNull(row["NoOfRequestCancled"]))?(int)0:(System.Int32?)row["NoOfRequestCancled"];
					c.TotalRevenueAmountFromBooking = (Convert.IsDBNull(row["TotalRevenueAmountFromBooking"]))?0.0m:(System.Decimal?)row["TotalRevenueAmountFromBooking"];
					c.Modifyrevenuefromrequest = (Convert.IsDBNull(row["Modifyrevenuefromrequest"]))?0.0m:(System.Decimal?)row["Modifyrevenuefromrequest"];
					c.TotalCommission = (Convert.IsDBNull(row["TotalCommission"]))?(int)0:(System.Int32?)row["TotalCommission"];
					c.TotalCommissionRequest = (Convert.IsDBNull(row["TotalCommissionRequest"]))?(int)0:(System.Int32?)row["TotalCommissionRequest"];
					c.StatisticsBookingTimeout = (Convert.IsDBNull(row["StatisticsBookingTimeout"]))?(int)0:(System.Int32?)row["StatisticsBookingTimeout"];
					c.StatisticsRequestTimeout = (Convert.IsDBNull(row["StatisticsRequestTimeout"]))?(int)0:(System.Int32?)row["StatisticsRequestTimeout"];
					c.Months = (Convert.IsDBNull(row["Months"]))?(int)0:(System.Int32?)row["Months"];
					c.Years = (Convert.IsDBNull(row["Years"]))?(int)0:(System.Int32?)row["Years"];
					c.Convper = (Convert.IsDBNull(row["convper"]))?0.0m:(System.Decimal?)row["convper"];
					c.Conv = (Convert.IsDBNull(row["conv"]))?string.Empty:(System.String)row["conv"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewReportProductionReportHotel&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewReportProductionReportHotel&gt;"/></returns>
		protected VList<ViewReportProductionReportHotel> Fill(IDataReader reader, VList<ViewReportProductionReportHotel> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewReportProductionReportHotel entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewReportProductionReportHotel>("ViewReportProductionReportHotel",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewReportProductionReportHotel();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CountryId = (System.Int64)reader[((int)ViewReportProductionReportHotelColumn.CountryId)];
					//entity.CountryId = (Convert.IsDBNull(reader["CountryID"]))?(long)0:(System.Int64)reader["CountryID"];
					entity.CountryName = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.CountryName)))?null:(System.String)reader[((int)ViewReportProductionReportHotelColumn.CountryName)];
					//entity.CountryName = (Convert.IsDBNull(reader["CountryName"]))?string.Empty:(System.String)reader["CountryName"];
					entity.CityId = (System.Int64)reader[((int)ViewReportProductionReportHotelColumn.CityId)];
					//entity.CityId = (Convert.IsDBNull(reader["CityID"]))?(long)0:(System.Int64)reader["CityID"];
					entity.City = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.City)))?null:(System.String)reader[((int)ViewReportProductionReportHotelColumn.City)];
					//entity.City = (Convert.IsDBNull(reader["City"]))?string.Empty:(System.String)reader["City"];
					entity.Id = (System.Int64)reader[((int)ViewReportProductionReportHotelColumn.Id)];
					//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
					entity.HotelName = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.HotelName)))?null:(System.String)reader[((int)ViewReportProductionReportHotelColumn.HotelName)];
					//entity.HotelName = (Convert.IsDBNull(reader["HotelName"]))?string.Empty:(System.String)reader["HotelName"];
					entity.NoOfBooking = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.NoOfBooking)))?null:(System.Int32?)reader[((int)ViewReportProductionReportHotelColumn.NoOfBooking)];
					//entity.NoOfBooking = (Convert.IsDBNull(reader["NoOfBooking"]))?(int)0:(System.Int32?)reader["NoOfBooking"];
					entity.NoOfRequest = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.NoOfRequest)))?null:(System.Int32?)reader[((int)ViewReportProductionReportHotelColumn.NoOfRequest)];
					//entity.NoOfRequest = (Convert.IsDBNull(reader["NoOfRequest"]))?(int)0:(System.Int32?)reader["NoOfRequest"];
					entity.NoOfBookingCancled = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.NoOfBookingCancled)))?null:(System.Int32?)reader[((int)ViewReportProductionReportHotelColumn.NoOfBookingCancled)];
					//entity.NoOfBookingCancled = (Convert.IsDBNull(reader["NoOfBookingCancled"]))?(int)0:(System.Int32?)reader["NoOfBookingCancled"];
					entity.NoOfRequestCancled = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.NoOfRequestCancled)))?null:(System.Int32?)reader[((int)ViewReportProductionReportHotelColumn.NoOfRequestCancled)];
					//entity.NoOfRequestCancled = (Convert.IsDBNull(reader["NoOfRequestCancled"]))?(int)0:(System.Int32?)reader["NoOfRequestCancled"];
					entity.TotalRevenueAmountFromBooking = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.TotalRevenueAmountFromBooking)))?null:(System.Decimal?)reader[((int)ViewReportProductionReportHotelColumn.TotalRevenueAmountFromBooking)];
					//entity.TotalRevenueAmountFromBooking = (Convert.IsDBNull(reader["TotalRevenueAmountFromBooking"]))?0.0m:(System.Decimal?)reader["TotalRevenueAmountFromBooking"];
					entity.Modifyrevenuefromrequest = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.Modifyrevenuefromrequest)))?null:(System.Decimal?)reader[((int)ViewReportProductionReportHotelColumn.Modifyrevenuefromrequest)];
					//entity.Modifyrevenuefromrequest = (Convert.IsDBNull(reader["Modifyrevenuefromrequest"]))?0.0m:(System.Decimal?)reader["Modifyrevenuefromrequest"];
					entity.TotalCommission = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.TotalCommission)))?null:(System.Int32?)reader[((int)ViewReportProductionReportHotelColumn.TotalCommission)];
					//entity.TotalCommission = (Convert.IsDBNull(reader["TotalCommission"]))?(int)0:(System.Int32?)reader["TotalCommission"];
					entity.TotalCommissionRequest = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.TotalCommissionRequest)))?null:(System.Int32?)reader[((int)ViewReportProductionReportHotelColumn.TotalCommissionRequest)];
					//entity.TotalCommissionRequest = (Convert.IsDBNull(reader["TotalCommissionRequest"]))?(int)0:(System.Int32?)reader["TotalCommissionRequest"];
					entity.StatisticsBookingTimeout = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.StatisticsBookingTimeout)))?null:(System.Int32?)reader[((int)ViewReportProductionReportHotelColumn.StatisticsBookingTimeout)];
					//entity.StatisticsBookingTimeout = (Convert.IsDBNull(reader["StatisticsBookingTimeout"]))?(int)0:(System.Int32?)reader["StatisticsBookingTimeout"];
					entity.StatisticsRequestTimeout = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.StatisticsRequestTimeout)))?null:(System.Int32?)reader[((int)ViewReportProductionReportHotelColumn.StatisticsRequestTimeout)];
					//entity.StatisticsRequestTimeout = (Convert.IsDBNull(reader["StatisticsRequestTimeout"]))?(int)0:(System.Int32?)reader["StatisticsRequestTimeout"];
					entity.Months = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.Months)))?null:(System.Int32?)reader[((int)ViewReportProductionReportHotelColumn.Months)];
					//entity.Months = (Convert.IsDBNull(reader["Months"]))?(int)0:(System.Int32?)reader["Months"];
					entity.Years = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.Years)))?null:(System.Int32?)reader[((int)ViewReportProductionReportHotelColumn.Years)];
					//entity.Years = (Convert.IsDBNull(reader["Years"]))?(int)0:(System.Int32?)reader["Years"];
					entity.Convper = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.Convper)))?null:(System.Decimal?)reader[((int)ViewReportProductionReportHotelColumn.Convper)];
					//entity.Convper = (Convert.IsDBNull(reader["convper"]))?0.0m:(System.Decimal?)reader["convper"];
					entity.Conv = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.Conv)))?null:(System.String)reader[((int)ViewReportProductionReportHotelColumn.Conv)];
					//entity.Conv = (Convert.IsDBNull(reader["conv"]))?string.Empty:(System.String)reader["conv"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewReportProductionReportHotel"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewReportProductionReportHotel"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewReportProductionReportHotel entity)
		{
			reader.Read();
			entity.CountryId = (System.Int64)reader[((int)ViewReportProductionReportHotelColumn.CountryId)];
			//entity.CountryId = (Convert.IsDBNull(reader["CountryID"]))?(long)0:(System.Int64)reader["CountryID"];
			entity.CountryName = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.CountryName)))?null:(System.String)reader[((int)ViewReportProductionReportHotelColumn.CountryName)];
			//entity.CountryName = (Convert.IsDBNull(reader["CountryName"]))?string.Empty:(System.String)reader["CountryName"];
			entity.CityId = (System.Int64)reader[((int)ViewReportProductionReportHotelColumn.CityId)];
			//entity.CityId = (Convert.IsDBNull(reader["CityID"]))?(long)0:(System.Int64)reader["CityID"];
			entity.City = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.City)))?null:(System.String)reader[((int)ViewReportProductionReportHotelColumn.City)];
			//entity.City = (Convert.IsDBNull(reader["City"]))?string.Empty:(System.String)reader["City"];
			entity.Id = (System.Int64)reader[((int)ViewReportProductionReportHotelColumn.Id)];
			//entity.Id = (Convert.IsDBNull(reader["Id"]))?(long)0:(System.Int64)reader["Id"];
			entity.HotelName = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.HotelName)))?null:(System.String)reader[((int)ViewReportProductionReportHotelColumn.HotelName)];
			//entity.HotelName = (Convert.IsDBNull(reader["HotelName"]))?string.Empty:(System.String)reader["HotelName"];
			entity.NoOfBooking = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.NoOfBooking)))?null:(System.Int32?)reader[((int)ViewReportProductionReportHotelColumn.NoOfBooking)];
			//entity.NoOfBooking = (Convert.IsDBNull(reader["NoOfBooking"]))?(int)0:(System.Int32?)reader["NoOfBooking"];
			entity.NoOfRequest = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.NoOfRequest)))?null:(System.Int32?)reader[((int)ViewReportProductionReportHotelColumn.NoOfRequest)];
			//entity.NoOfRequest = (Convert.IsDBNull(reader["NoOfRequest"]))?(int)0:(System.Int32?)reader["NoOfRequest"];
			entity.NoOfBookingCancled = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.NoOfBookingCancled)))?null:(System.Int32?)reader[((int)ViewReportProductionReportHotelColumn.NoOfBookingCancled)];
			//entity.NoOfBookingCancled = (Convert.IsDBNull(reader["NoOfBookingCancled"]))?(int)0:(System.Int32?)reader["NoOfBookingCancled"];
			entity.NoOfRequestCancled = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.NoOfRequestCancled)))?null:(System.Int32?)reader[((int)ViewReportProductionReportHotelColumn.NoOfRequestCancled)];
			//entity.NoOfRequestCancled = (Convert.IsDBNull(reader["NoOfRequestCancled"]))?(int)0:(System.Int32?)reader["NoOfRequestCancled"];
			entity.TotalRevenueAmountFromBooking = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.TotalRevenueAmountFromBooking)))?null:(System.Decimal?)reader[((int)ViewReportProductionReportHotelColumn.TotalRevenueAmountFromBooking)];
			//entity.TotalRevenueAmountFromBooking = (Convert.IsDBNull(reader["TotalRevenueAmountFromBooking"]))?0.0m:(System.Decimal?)reader["TotalRevenueAmountFromBooking"];
			entity.Modifyrevenuefromrequest = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.Modifyrevenuefromrequest)))?null:(System.Decimal?)reader[((int)ViewReportProductionReportHotelColumn.Modifyrevenuefromrequest)];
			//entity.Modifyrevenuefromrequest = (Convert.IsDBNull(reader["Modifyrevenuefromrequest"]))?0.0m:(System.Decimal?)reader["Modifyrevenuefromrequest"];
			entity.TotalCommission = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.TotalCommission)))?null:(System.Int32?)reader[((int)ViewReportProductionReportHotelColumn.TotalCommission)];
			//entity.TotalCommission = (Convert.IsDBNull(reader["TotalCommission"]))?(int)0:(System.Int32?)reader["TotalCommission"];
			entity.TotalCommissionRequest = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.TotalCommissionRequest)))?null:(System.Int32?)reader[((int)ViewReportProductionReportHotelColumn.TotalCommissionRequest)];
			//entity.TotalCommissionRequest = (Convert.IsDBNull(reader["TotalCommissionRequest"]))?(int)0:(System.Int32?)reader["TotalCommissionRequest"];
			entity.StatisticsBookingTimeout = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.StatisticsBookingTimeout)))?null:(System.Int32?)reader[((int)ViewReportProductionReportHotelColumn.StatisticsBookingTimeout)];
			//entity.StatisticsBookingTimeout = (Convert.IsDBNull(reader["StatisticsBookingTimeout"]))?(int)0:(System.Int32?)reader["StatisticsBookingTimeout"];
			entity.StatisticsRequestTimeout = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.StatisticsRequestTimeout)))?null:(System.Int32?)reader[((int)ViewReportProductionReportHotelColumn.StatisticsRequestTimeout)];
			//entity.StatisticsRequestTimeout = (Convert.IsDBNull(reader["StatisticsRequestTimeout"]))?(int)0:(System.Int32?)reader["StatisticsRequestTimeout"];
			entity.Months = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.Months)))?null:(System.Int32?)reader[((int)ViewReportProductionReportHotelColumn.Months)];
			//entity.Months = (Convert.IsDBNull(reader["Months"]))?(int)0:(System.Int32?)reader["Months"];
			entity.Years = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.Years)))?null:(System.Int32?)reader[((int)ViewReportProductionReportHotelColumn.Years)];
			//entity.Years = (Convert.IsDBNull(reader["Years"]))?(int)0:(System.Int32?)reader["Years"];
			entity.Convper = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.Convper)))?null:(System.Decimal?)reader[((int)ViewReportProductionReportHotelColumn.Convper)];
			//entity.Convper = (Convert.IsDBNull(reader["convper"]))?0.0m:(System.Decimal?)reader["convper"];
			entity.Conv = (reader.IsDBNull(((int)ViewReportProductionReportHotelColumn.Conv)))?null:(System.String)reader[((int)ViewReportProductionReportHotelColumn.Conv)];
			//entity.Conv = (Convert.IsDBNull(reader["conv"]))?string.Empty:(System.String)reader["conv"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewReportProductionReportHotel"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewReportProductionReportHotel"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewReportProductionReportHotel entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CountryId = (Convert.IsDBNull(dataRow["CountryID"]))?(long)0:(System.Int64)dataRow["CountryID"];
			entity.CountryName = (Convert.IsDBNull(dataRow["CountryName"]))?string.Empty:(System.String)dataRow["CountryName"];
			entity.CityId = (Convert.IsDBNull(dataRow["CityID"]))?(long)0:(System.Int64)dataRow["CityID"];
			entity.City = (Convert.IsDBNull(dataRow["City"]))?string.Empty:(System.String)dataRow["City"];
			entity.Id = (Convert.IsDBNull(dataRow["Id"]))?(long)0:(System.Int64)dataRow["Id"];
			entity.HotelName = (Convert.IsDBNull(dataRow["HotelName"]))?string.Empty:(System.String)dataRow["HotelName"];
			entity.NoOfBooking = (Convert.IsDBNull(dataRow["NoOfBooking"]))?(int)0:(System.Int32?)dataRow["NoOfBooking"];
			entity.NoOfRequest = (Convert.IsDBNull(dataRow["NoOfRequest"]))?(int)0:(System.Int32?)dataRow["NoOfRequest"];
			entity.NoOfBookingCancled = (Convert.IsDBNull(dataRow["NoOfBookingCancled"]))?(int)0:(System.Int32?)dataRow["NoOfBookingCancled"];
			entity.NoOfRequestCancled = (Convert.IsDBNull(dataRow["NoOfRequestCancled"]))?(int)0:(System.Int32?)dataRow["NoOfRequestCancled"];
			entity.TotalRevenueAmountFromBooking = (Convert.IsDBNull(dataRow["TotalRevenueAmountFromBooking"]))?0.0m:(System.Decimal?)dataRow["TotalRevenueAmountFromBooking"];
			entity.Modifyrevenuefromrequest = (Convert.IsDBNull(dataRow["Modifyrevenuefromrequest"]))?0.0m:(System.Decimal?)dataRow["Modifyrevenuefromrequest"];
			entity.TotalCommission = (Convert.IsDBNull(dataRow["TotalCommission"]))?(int)0:(System.Int32?)dataRow["TotalCommission"];
			entity.TotalCommissionRequest = (Convert.IsDBNull(dataRow["TotalCommissionRequest"]))?(int)0:(System.Int32?)dataRow["TotalCommissionRequest"];
			entity.StatisticsBookingTimeout = (Convert.IsDBNull(dataRow["StatisticsBookingTimeout"]))?(int)0:(System.Int32?)dataRow["StatisticsBookingTimeout"];
			entity.StatisticsRequestTimeout = (Convert.IsDBNull(dataRow["StatisticsRequestTimeout"]))?(int)0:(System.Int32?)dataRow["StatisticsRequestTimeout"];
			entity.Months = (Convert.IsDBNull(dataRow["Months"]))?(int)0:(System.Int32?)dataRow["Months"];
			entity.Years = (Convert.IsDBNull(dataRow["Years"]))?(int)0:(System.Int32?)dataRow["Years"];
			entity.Convper = (Convert.IsDBNull(dataRow["convper"]))?0.0m:(System.Decimal?)dataRow["convper"];
			entity.Conv = (Convert.IsDBNull(dataRow["conv"]))?string.Empty:(System.String)dataRow["conv"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewReportProductionReportHotelFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportProductionReportHotel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportProductionReportHotelFilterBuilder : SqlFilterBuilder<ViewReportProductionReportHotelColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionReportHotelFilterBuilder class.
		/// </summary>
		public ViewReportProductionReportHotelFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionReportHotelFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportProductionReportHotelFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionReportHotelFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportProductionReportHotelFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportProductionReportHotelFilterBuilder

	#region ViewReportProductionReportHotelParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportProductionReportHotel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportProductionReportHotelParameterBuilder : ParameterizedSqlFilterBuilder<ViewReportProductionReportHotelColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionReportHotelParameterBuilder class.
		/// </summary>
		public ViewReportProductionReportHotelParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionReportHotelParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportProductionReportHotelParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionReportHotelParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportProductionReportHotelParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportProductionReportHotelParameterBuilder
	
	#region ViewReportProductionReportHotelSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportProductionReportHotel"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewReportProductionReportHotelSortBuilder : SqlSortBuilder<ViewReportProductionReportHotelColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportProductionReportHotelSqlSortBuilder class.
		/// </summary>
		public ViewReportProductionReportHotelSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewReportProductionReportHotelSortBuilder

} // end namespace
