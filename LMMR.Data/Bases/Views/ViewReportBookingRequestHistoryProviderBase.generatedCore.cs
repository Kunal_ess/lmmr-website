﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewReportBookingRequestHistoryProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewReportBookingRequestHistoryProviderBaseCore : EntityViewProviderBase<ViewReportBookingRequestHistory>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewReportBookingRequestHistory&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewReportBookingRequestHistory&gt;"/></returns>
		protected static VList&lt;ViewReportBookingRequestHistory&gt; Fill(DataSet dataSet, VList<ViewReportBookingRequestHistory> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewReportBookingRequestHistory>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewReportBookingRequestHistory&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewReportBookingRequestHistory>"/></returns>
		protected static VList&lt;ViewReportBookingRequestHistory&gt; Fill(DataTable dataTable, VList<ViewReportBookingRequestHistory> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewReportBookingRequestHistory c = new ViewReportBookingRequestHistory();
					c.BookType = (Convert.IsDBNull(row["BookType"]))?(int)0:(System.Int32?)row["BookType"];
					c.CountryId = (Convert.IsDBNull(row["CountryID"]))?(long)0:(System.Int64)row["CountryID"];
					c.CountryName = (Convert.IsDBNull(row["CountryName"]))?string.Empty:(System.String)row["CountryName"];
					c.CityId = (Convert.IsDBNull(row["CityID"]))?(long)0:(System.Int64)row["CityID"];
					c.City = (Convert.IsDBNull(row["City"]))?string.Empty:(System.String)row["City"];
					c.BookingDate = (Convert.IsDBNull(row["BookingDate"]))?DateTime.MinValue:(System.DateTime?)row["BookingDate"];
					c.CountBooking = (Convert.IsDBNull(row["CountBooking"]))?(int)0:(System.Int32?)row["CountBooking"];
					c.HotelId = (Convert.IsDBNull(row["HotelID"]))?(long)0:(System.Int64)row["HotelID"];
					c.HotelName = (Convert.IsDBNull(row["HotelName"]))?string.Empty:(System.String)row["HotelName"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewReportBookingRequestHistory&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewReportBookingRequestHistory&gt;"/></returns>
		protected VList<ViewReportBookingRequestHistory> Fill(IDataReader reader, VList<ViewReportBookingRequestHistory> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewReportBookingRequestHistory entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewReportBookingRequestHistory>("ViewReportBookingRequestHistory",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewReportBookingRequestHistory();
					}
					
					entity.SuppressEntityEvents = true;

					entity.BookType = (reader.IsDBNull(((int)ViewReportBookingRequestHistoryColumn.BookType)))?null:(System.Int32?)reader[((int)ViewReportBookingRequestHistoryColumn.BookType)];
					//entity.BookType = (Convert.IsDBNull(reader["BookType"]))?(int)0:(System.Int32?)reader["BookType"];
					entity.CountryId = (System.Int64)reader[((int)ViewReportBookingRequestHistoryColumn.CountryId)];
					//entity.CountryId = (Convert.IsDBNull(reader["CountryID"]))?(long)0:(System.Int64)reader["CountryID"];
					entity.CountryName = (reader.IsDBNull(((int)ViewReportBookingRequestHistoryColumn.CountryName)))?null:(System.String)reader[((int)ViewReportBookingRequestHistoryColumn.CountryName)];
					//entity.CountryName = (Convert.IsDBNull(reader["CountryName"]))?string.Empty:(System.String)reader["CountryName"];
					entity.CityId = (System.Int64)reader[((int)ViewReportBookingRequestHistoryColumn.CityId)];
					//entity.CityId = (Convert.IsDBNull(reader["CityID"]))?(long)0:(System.Int64)reader["CityID"];
					entity.City = (reader.IsDBNull(((int)ViewReportBookingRequestHistoryColumn.City)))?null:(System.String)reader[((int)ViewReportBookingRequestHistoryColumn.City)];
					//entity.City = (Convert.IsDBNull(reader["City"]))?string.Empty:(System.String)reader["City"];
					entity.BookingDate = (reader.IsDBNull(((int)ViewReportBookingRequestHistoryColumn.BookingDate)))?null:(System.DateTime?)reader[((int)ViewReportBookingRequestHistoryColumn.BookingDate)];
					//entity.BookingDate = (Convert.IsDBNull(reader["BookingDate"]))?DateTime.MinValue:(System.DateTime?)reader["BookingDate"];
					entity.CountBooking = (reader.IsDBNull(((int)ViewReportBookingRequestHistoryColumn.CountBooking)))?null:(System.Int32?)reader[((int)ViewReportBookingRequestHistoryColumn.CountBooking)];
					//entity.CountBooking = (Convert.IsDBNull(reader["CountBooking"]))?(int)0:(System.Int32?)reader["CountBooking"];
					entity.HotelId = (System.Int64)reader[((int)ViewReportBookingRequestHistoryColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["HotelID"]))?(long)0:(System.Int64)reader["HotelID"];
					entity.HotelName = (reader.IsDBNull(((int)ViewReportBookingRequestHistoryColumn.HotelName)))?null:(System.String)reader[((int)ViewReportBookingRequestHistoryColumn.HotelName)];
					//entity.HotelName = (Convert.IsDBNull(reader["HotelName"]))?string.Empty:(System.String)reader["HotelName"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewReportBookingRequestHistory"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewReportBookingRequestHistory"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewReportBookingRequestHistory entity)
		{
			reader.Read();
			entity.BookType = (reader.IsDBNull(((int)ViewReportBookingRequestHistoryColumn.BookType)))?null:(System.Int32?)reader[((int)ViewReportBookingRequestHistoryColumn.BookType)];
			//entity.BookType = (Convert.IsDBNull(reader["BookType"]))?(int)0:(System.Int32?)reader["BookType"];
			entity.CountryId = (System.Int64)reader[((int)ViewReportBookingRequestHistoryColumn.CountryId)];
			//entity.CountryId = (Convert.IsDBNull(reader["CountryID"]))?(long)0:(System.Int64)reader["CountryID"];
			entity.CountryName = (reader.IsDBNull(((int)ViewReportBookingRequestHistoryColumn.CountryName)))?null:(System.String)reader[((int)ViewReportBookingRequestHistoryColumn.CountryName)];
			//entity.CountryName = (Convert.IsDBNull(reader["CountryName"]))?string.Empty:(System.String)reader["CountryName"];
			entity.CityId = (System.Int64)reader[((int)ViewReportBookingRequestHistoryColumn.CityId)];
			//entity.CityId = (Convert.IsDBNull(reader["CityID"]))?(long)0:(System.Int64)reader["CityID"];
			entity.City = (reader.IsDBNull(((int)ViewReportBookingRequestHistoryColumn.City)))?null:(System.String)reader[((int)ViewReportBookingRequestHistoryColumn.City)];
			//entity.City = (Convert.IsDBNull(reader["City"]))?string.Empty:(System.String)reader["City"];
			entity.BookingDate = (reader.IsDBNull(((int)ViewReportBookingRequestHistoryColumn.BookingDate)))?null:(System.DateTime?)reader[((int)ViewReportBookingRequestHistoryColumn.BookingDate)];
			//entity.BookingDate = (Convert.IsDBNull(reader["BookingDate"]))?DateTime.MinValue:(System.DateTime?)reader["BookingDate"];
			entity.CountBooking = (reader.IsDBNull(((int)ViewReportBookingRequestHistoryColumn.CountBooking)))?null:(System.Int32?)reader[((int)ViewReportBookingRequestHistoryColumn.CountBooking)];
			//entity.CountBooking = (Convert.IsDBNull(reader["CountBooking"]))?(int)0:(System.Int32?)reader["CountBooking"];
			entity.HotelId = (System.Int64)reader[((int)ViewReportBookingRequestHistoryColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["HotelID"]))?(long)0:(System.Int64)reader["HotelID"];
			entity.HotelName = (reader.IsDBNull(((int)ViewReportBookingRequestHistoryColumn.HotelName)))?null:(System.String)reader[((int)ViewReportBookingRequestHistoryColumn.HotelName)];
			//entity.HotelName = (Convert.IsDBNull(reader["HotelName"]))?string.Empty:(System.String)reader["HotelName"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewReportBookingRequestHistory"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewReportBookingRequestHistory"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewReportBookingRequestHistory entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.BookType = (Convert.IsDBNull(dataRow["BookType"]))?(int)0:(System.Int32?)dataRow["BookType"];
			entity.CountryId = (Convert.IsDBNull(dataRow["CountryID"]))?(long)0:(System.Int64)dataRow["CountryID"];
			entity.CountryName = (Convert.IsDBNull(dataRow["CountryName"]))?string.Empty:(System.String)dataRow["CountryName"];
			entity.CityId = (Convert.IsDBNull(dataRow["CityID"]))?(long)0:(System.Int64)dataRow["CityID"];
			entity.City = (Convert.IsDBNull(dataRow["City"]))?string.Empty:(System.String)dataRow["City"];
			entity.BookingDate = (Convert.IsDBNull(dataRow["BookingDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["BookingDate"];
			entity.CountBooking = (Convert.IsDBNull(dataRow["CountBooking"]))?(int)0:(System.Int32?)dataRow["CountBooking"];
			entity.HotelId = (Convert.IsDBNull(dataRow["HotelID"]))?(long)0:(System.Int64)dataRow["HotelID"];
			entity.HotelName = (Convert.IsDBNull(dataRow["HotelName"]))?string.Empty:(System.String)dataRow["HotelName"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewReportBookingRequestHistoryFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportBookingRequestHistory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportBookingRequestHistoryFilterBuilder : SqlFilterBuilder<ViewReportBookingRequestHistoryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportBookingRequestHistoryFilterBuilder class.
		/// </summary>
		public ViewReportBookingRequestHistoryFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportBookingRequestHistoryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportBookingRequestHistoryFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportBookingRequestHistoryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportBookingRequestHistoryFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportBookingRequestHistoryFilterBuilder

	#region ViewReportBookingRequestHistoryParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportBookingRequestHistory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportBookingRequestHistoryParameterBuilder : ParameterizedSqlFilterBuilder<ViewReportBookingRequestHistoryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportBookingRequestHistoryParameterBuilder class.
		/// </summary>
		public ViewReportBookingRequestHistoryParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportBookingRequestHistoryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportBookingRequestHistoryParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportBookingRequestHistoryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportBookingRequestHistoryParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportBookingRequestHistoryParameterBuilder
	
	#region ViewReportBookingRequestHistorySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportBookingRequestHistory"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewReportBookingRequestHistorySortBuilder : SqlSortBuilder<ViewReportBookingRequestHistoryColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportBookingRequestHistorySqlSortBuilder class.
		/// </summary>
		public ViewReportBookingRequestHistorySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewReportBookingRequestHistorySortBuilder

} // end namespace
