﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewForCommissionFieldsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewForCommissionFieldsProviderBaseCore : EntityViewProviderBase<ViewForCommissionFields>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewForCommissionFields&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewForCommissionFields&gt;"/></returns>
		protected static VList&lt;ViewForCommissionFields&gt; Fill(DataSet dataSet, VList<ViewForCommissionFields> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewForCommissionFields>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewForCommissionFields&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewForCommissionFields>"/></returns>
		protected static VList&lt;ViewForCommissionFields&gt; Fill(DataTable dataTable, VList<ViewForCommissionFields> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewForCommissionFields c = new ViewForCommissionFields();
					c.CountryId = (Convert.IsDBNull(row["CountryID"]))?(long)0:(System.Int64)row["CountryID"];
					c.CountryName = (Convert.IsDBNull(row["CountryName"]))?string.Empty:(System.String)row["CountryName"];
					c.CityId = (Convert.IsDBNull(row["CityID"]))?(long)0:(System.Int64)row["CityID"];
					c.City = (Convert.IsDBNull(row["City"]))?string.Empty:(System.String)row["City"];
					c.HotelId = (Convert.IsDBNull(row["HotelID"]))?(long)0:(System.Int64)row["HotelID"];
					c.HotelName = (Convert.IsDBNull(row["HotelName"]))?string.Empty:(System.String)row["HotelName"];
					c.BookingDate = (Convert.IsDBNull(row["BookingDate"]))?DateTime.MinValue:(System.DateTime?)row["BookingDate"];
					c.Accomodation = (Convert.IsDBNull(row["Accomodation"]))?(int)0:(System.Int32)row["Accomodation"];
					c.InitialValue = (Convert.IsDBNull(row["InitialValue"]))?0.0m:(System.Decimal?)row["InitialValue"];
					c.CommissionPercentage = (Convert.IsDBNull(row["CommissionPercentage"]))?(int)0:(System.Int32?)row["CommissionPercentage"];
					c.DefaultCommission = (Convert.IsDBNull(row["defaultCommission"]))?(int)0:(System.Int32?)row["defaultCommission"];
					c.ActualValue = (Convert.IsDBNull(row["ActualValue"]))?0.0m:(System.Decimal?)row["ActualValue"];
					c.RequestStatus = (Convert.IsDBNull(row["RequestStatus"]))?(int)0:(System.Int32?)row["RequestStatus"];
					c.BookType = (Convert.IsDBNull(row["BookType"]))?(int)0:(System.Int32?)row["BookType"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewForCommissionFields&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewForCommissionFields&gt;"/></returns>
		protected VList<ViewForCommissionFields> Fill(IDataReader reader, VList<ViewForCommissionFields> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewForCommissionFields entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewForCommissionFields>("ViewForCommissionFields",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewForCommissionFields();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CountryId = (System.Int64)reader[((int)ViewForCommissionFieldsColumn.CountryId)];
					//entity.CountryId = (Convert.IsDBNull(reader["CountryID"]))?(long)0:(System.Int64)reader["CountryID"];
					entity.CountryName = (reader.IsDBNull(((int)ViewForCommissionFieldsColumn.CountryName)))?null:(System.String)reader[((int)ViewForCommissionFieldsColumn.CountryName)];
					//entity.CountryName = (Convert.IsDBNull(reader["CountryName"]))?string.Empty:(System.String)reader["CountryName"];
					entity.CityId = (System.Int64)reader[((int)ViewForCommissionFieldsColumn.CityId)];
					//entity.CityId = (Convert.IsDBNull(reader["CityID"]))?(long)0:(System.Int64)reader["CityID"];
					entity.City = (reader.IsDBNull(((int)ViewForCommissionFieldsColumn.City)))?null:(System.String)reader[((int)ViewForCommissionFieldsColumn.City)];
					//entity.City = (Convert.IsDBNull(reader["City"]))?string.Empty:(System.String)reader["City"];
					entity.HotelId = (System.Int64)reader[((int)ViewForCommissionFieldsColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["HotelID"]))?(long)0:(System.Int64)reader["HotelID"];
					entity.HotelName = (reader.IsDBNull(((int)ViewForCommissionFieldsColumn.HotelName)))?null:(System.String)reader[((int)ViewForCommissionFieldsColumn.HotelName)];
					//entity.HotelName = (Convert.IsDBNull(reader["HotelName"]))?string.Empty:(System.String)reader["HotelName"];
					entity.BookingDate = (reader.IsDBNull(((int)ViewForCommissionFieldsColumn.BookingDate)))?null:(System.DateTime?)reader[((int)ViewForCommissionFieldsColumn.BookingDate)];
					//entity.BookingDate = (Convert.IsDBNull(reader["BookingDate"]))?DateTime.MinValue:(System.DateTime?)reader["BookingDate"];
					entity.Accomodation = (System.Int32)reader[((int)ViewForCommissionFieldsColumn.Accomodation)];
					//entity.Accomodation = (Convert.IsDBNull(reader["Accomodation"]))?(int)0:(System.Int32)reader["Accomodation"];
					entity.InitialValue = (reader.IsDBNull(((int)ViewForCommissionFieldsColumn.InitialValue)))?null:(System.Decimal?)reader[((int)ViewForCommissionFieldsColumn.InitialValue)];
					//entity.InitialValue = (Convert.IsDBNull(reader["InitialValue"]))?0.0m:(System.Decimal?)reader["InitialValue"];
					entity.CommissionPercentage = (reader.IsDBNull(((int)ViewForCommissionFieldsColumn.CommissionPercentage)))?null:(System.Int32?)reader[((int)ViewForCommissionFieldsColumn.CommissionPercentage)];
					//entity.CommissionPercentage = (Convert.IsDBNull(reader["CommissionPercentage"]))?(int)0:(System.Int32?)reader["CommissionPercentage"];
					entity.DefaultCommission = (reader.IsDBNull(((int)ViewForCommissionFieldsColumn.DefaultCommission)))?null:(System.Int32?)reader[((int)ViewForCommissionFieldsColumn.DefaultCommission)];
					//entity.DefaultCommission = (Convert.IsDBNull(reader["defaultCommission"]))?(int)0:(System.Int32?)reader["defaultCommission"];
					entity.ActualValue = (reader.IsDBNull(((int)ViewForCommissionFieldsColumn.ActualValue)))?null:(System.Decimal?)reader[((int)ViewForCommissionFieldsColumn.ActualValue)];
					//entity.ActualValue = (Convert.IsDBNull(reader["ActualValue"]))?0.0m:(System.Decimal?)reader["ActualValue"];
					entity.RequestStatus = (reader.IsDBNull(((int)ViewForCommissionFieldsColumn.RequestStatus)))?null:(System.Int32?)reader[((int)ViewForCommissionFieldsColumn.RequestStatus)];
					//entity.RequestStatus = (Convert.IsDBNull(reader["RequestStatus"]))?(int)0:(System.Int32?)reader["RequestStatus"];
					entity.BookType = (reader.IsDBNull(((int)ViewForCommissionFieldsColumn.BookType)))?null:(System.Int32?)reader[((int)ViewForCommissionFieldsColumn.BookType)];
					//entity.BookType = (Convert.IsDBNull(reader["BookType"]))?(int)0:(System.Int32?)reader["BookType"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewForCommissionFields"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewForCommissionFields"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewForCommissionFields entity)
		{
			reader.Read();
			entity.CountryId = (System.Int64)reader[((int)ViewForCommissionFieldsColumn.CountryId)];
			//entity.CountryId = (Convert.IsDBNull(reader["CountryID"]))?(long)0:(System.Int64)reader["CountryID"];
			entity.CountryName = (reader.IsDBNull(((int)ViewForCommissionFieldsColumn.CountryName)))?null:(System.String)reader[((int)ViewForCommissionFieldsColumn.CountryName)];
			//entity.CountryName = (Convert.IsDBNull(reader["CountryName"]))?string.Empty:(System.String)reader["CountryName"];
			entity.CityId = (System.Int64)reader[((int)ViewForCommissionFieldsColumn.CityId)];
			//entity.CityId = (Convert.IsDBNull(reader["CityID"]))?(long)0:(System.Int64)reader["CityID"];
			entity.City = (reader.IsDBNull(((int)ViewForCommissionFieldsColumn.City)))?null:(System.String)reader[((int)ViewForCommissionFieldsColumn.City)];
			//entity.City = (Convert.IsDBNull(reader["City"]))?string.Empty:(System.String)reader["City"];
			entity.HotelId = (System.Int64)reader[((int)ViewForCommissionFieldsColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["HotelID"]))?(long)0:(System.Int64)reader["HotelID"];
			entity.HotelName = (reader.IsDBNull(((int)ViewForCommissionFieldsColumn.HotelName)))?null:(System.String)reader[((int)ViewForCommissionFieldsColumn.HotelName)];
			//entity.HotelName = (Convert.IsDBNull(reader["HotelName"]))?string.Empty:(System.String)reader["HotelName"];
			entity.BookingDate = (reader.IsDBNull(((int)ViewForCommissionFieldsColumn.BookingDate)))?null:(System.DateTime?)reader[((int)ViewForCommissionFieldsColumn.BookingDate)];
			//entity.BookingDate = (Convert.IsDBNull(reader["BookingDate"]))?DateTime.MinValue:(System.DateTime?)reader["BookingDate"];
			entity.Accomodation = (System.Int32)reader[((int)ViewForCommissionFieldsColumn.Accomodation)];
			//entity.Accomodation = (Convert.IsDBNull(reader["Accomodation"]))?(int)0:(System.Int32)reader["Accomodation"];
			entity.InitialValue = (reader.IsDBNull(((int)ViewForCommissionFieldsColumn.InitialValue)))?null:(System.Decimal?)reader[((int)ViewForCommissionFieldsColumn.InitialValue)];
			//entity.InitialValue = (Convert.IsDBNull(reader["InitialValue"]))?0.0m:(System.Decimal?)reader["InitialValue"];
			entity.CommissionPercentage = (reader.IsDBNull(((int)ViewForCommissionFieldsColumn.CommissionPercentage)))?null:(System.Int32?)reader[((int)ViewForCommissionFieldsColumn.CommissionPercentage)];
			//entity.CommissionPercentage = (Convert.IsDBNull(reader["CommissionPercentage"]))?(int)0:(System.Int32?)reader["CommissionPercentage"];
			entity.DefaultCommission = (reader.IsDBNull(((int)ViewForCommissionFieldsColumn.DefaultCommission)))?null:(System.Int32?)reader[((int)ViewForCommissionFieldsColumn.DefaultCommission)];
			//entity.DefaultCommission = (Convert.IsDBNull(reader["defaultCommission"]))?(int)0:(System.Int32?)reader["defaultCommission"];
			entity.ActualValue = (reader.IsDBNull(((int)ViewForCommissionFieldsColumn.ActualValue)))?null:(System.Decimal?)reader[((int)ViewForCommissionFieldsColumn.ActualValue)];
			//entity.ActualValue = (Convert.IsDBNull(reader["ActualValue"]))?0.0m:(System.Decimal?)reader["ActualValue"];
			entity.RequestStatus = (reader.IsDBNull(((int)ViewForCommissionFieldsColumn.RequestStatus)))?null:(System.Int32?)reader[((int)ViewForCommissionFieldsColumn.RequestStatus)];
			//entity.RequestStatus = (Convert.IsDBNull(reader["RequestStatus"]))?(int)0:(System.Int32?)reader["RequestStatus"];
			entity.BookType = (reader.IsDBNull(((int)ViewForCommissionFieldsColumn.BookType)))?null:(System.Int32?)reader[((int)ViewForCommissionFieldsColumn.BookType)];
			//entity.BookType = (Convert.IsDBNull(reader["BookType"]))?(int)0:(System.Int32?)reader["BookType"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewForCommissionFields"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewForCommissionFields"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewForCommissionFields entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CountryId = (Convert.IsDBNull(dataRow["CountryID"]))?(long)0:(System.Int64)dataRow["CountryID"];
			entity.CountryName = (Convert.IsDBNull(dataRow["CountryName"]))?string.Empty:(System.String)dataRow["CountryName"];
			entity.CityId = (Convert.IsDBNull(dataRow["CityID"]))?(long)0:(System.Int64)dataRow["CityID"];
			entity.City = (Convert.IsDBNull(dataRow["City"]))?string.Empty:(System.String)dataRow["City"];
			entity.HotelId = (Convert.IsDBNull(dataRow["HotelID"]))?(long)0:(System.Int64)dataRow["HotelID"];
			entity.HotelName = (Convert.IsDBNull(dataRow["HotelName"]))?string.Empty:(System.String)dataRow["HotelName"];
			entity.BookingDate = (Convert.IsDBNull(dataRow["BookingDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["BookingDate"];
			entity.Accomodation = (Convert.IsDBNull(dataRow["Accomodation"]))?(int)0:(System.Int32)dataRow["Accomodation"];
			entity.InitialValue = (Convert.IsDBNull(dataRow["InitialValue"]))?0.0m:(System.Decimal?)dataRow["InitialValue"];
			entity.CommissionPercentage = (Convert.IsDBNull(dataRow["CommissionPercentage"]))?(int)0:(System.Int32?)dataRow["CommissionPercentage"];
			entity.DefaultCommission = (Convert.IsDBNull(dataRow["defaultCommission"]))?(int)0:(System.Int32?)dataRow["defaultCommission"];
			entity.ActualValue = (Convert.IsDBNull(dataRow["ActualValue"]))?0.0m:(System.Decimal?)dataRow["ActualValue"];
			entity.RequestStatus = (Convert.IsDBNull(dataRow["RequestStatus"]))?(int)0:(System.Int32?)dataRow["RequestStatus"];
			entity.BookType = (Convert.IsDBNull(dataRow["BookType"]))?(int)0:(System.Int32?)dataRow["BookType"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewForCommissionFieldsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewForCommissionFields"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewForCommissionFieldsFilterBuilder : SqlFilterBuilder<ViewForCommissionFieldsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewForCommissionFieldsFilterBuilder class.
		/// </summary>
		public ViewForCommissionFieldsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewForCommissionFieldsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewForCommissionFieldsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewForCommissionFieldsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewForCommissionFieldsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewForCommissionFieldsFilterBuilder

	#region ViewForCommissionFieldsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewForCommissionFields"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewForCommissionFieldsParameterBuilder : ParameterizedSqlFilterBuilder<ViewForCommissionFieldsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewForCommissionFieldsParameterBuilder class.
		/// </summary>
		public ViewForCommissionFieldsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewForCommissionFieldsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewForCommissionFieldsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewForCommissionFieldsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewForCommissionFieldsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewForCommissionFieldsParameterBuilder
	
	#region ViewForCommissionFieldsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewForCommissionFields"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewForCommissionFieldsSortBuilder : SqlSortBuilder<ViewForCommissionFieldsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewForCommissionFieldsSqlSortBuilder class.
		/// </summary>
		public ViewForCommissionFieldsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewForCommissionFieldsSortBuilder

} // end namespace
