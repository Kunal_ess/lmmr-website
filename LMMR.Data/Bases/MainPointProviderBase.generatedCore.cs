﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="MainPointProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class MainPointProviderBaseCore : EntityProviderBase<LMMR.Entities.MainPoint, LMMR.Entities.MainPointKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.MainPointKey key)
		{
			return Delete(transactionManager, key.MainPointId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_mainPointId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _mainPointId)
		{
			return Delete(null, _mainPointId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_mainPointId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _mainPointId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MainPoint_City key.
		///		FK_MainPoint_City Description: 
		/// </summary>
		/// <param name="_cityId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.MainPoint objects.</returns>
		public TList<MainPoint> GetByCityId(System.Int64 _cityId)
		{
			int count = -1;
			return GetByCityId(_cityId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MainPoint_City key.
		///		FK_MainPoint_City Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cityId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.MainPoint objects.</returns>
		/// <remarks></remarks>
		public TList<MainPoint> GetByCityId(TransactionManager transactionManager, System.Int64 _cityId)
		{
			int count = -1;
			return GetByCityId(transactionManager, _cityId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_MainPoint_City key.
		///		FK_MainPoint_City Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cityId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MainPoint objects.</returns>
		public TList<MainPoint> GetByCityId(TransactionManager transactionManager, System.Int64 _cityId, int start, int pageLength)
		{
			int count = -1;
			return GetByCityId(transactionManager, _cityId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MainPoint_City key.
		///		fkMainPointCity Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_cityId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MainPoint objects.</returns>
		public TList<MainPoint> GetByCityId(System.Int64 _cityId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCityId(null, _cityId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MainPoint_City key.
		///		fkMainPointCity Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_cityId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MainPoint objects.</returns>
		public TList<MainPoint> GetByCityId(System.Int64 _cityId, int start, int pageLength,out int count)
		{
			return GetByCityId(null, _cityId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MainPoint_City key.
		///		FK_MainPoint_City Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cityId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.MainPoint objects.</returns>
		public abstract TList<MainPoint> GetByCityId(TransactionManager transactionManager, System.Int64 _cityId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.MainPoint Get(TransactionManager transactionManager, LMMR.Entities.MainPointKey key, int start, int pageLength)
		{
			return GetByMainPointId(transactionManager, key.MainPointId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_MainPoint index.
		/// </summary>
		/// <param name="_mainPointId"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MainPoint"/> class.</returns>
		public LMMR.Entities.MainPoint GetByMainPointId(System.Int64 _mainPointId)
		{
			int count = -1;
			return GetByMainPointId(null,_mainPointId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MainPoint index.
		/// </summary>
		/// <param name="_mainPointId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MainPoint"/> class.</returns>
		public LMMR.Entities.MainPoint GetByMainPointId(System.Int64 _mainPointId, int start, int pageLength)
		{
			int count = -1;
			return GetByMainPointId(null, _mainPointId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MainPoint index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_mainPointId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MainPoint"/> class.</returns>
		public LMMR.Entities.MainPoint GetByMainPointId(TransactionManager transactionManager, System.Int64 _mainPointId)
		{
			int count = -1;
			return GetByMainPointId(transactionManager, _mainPointId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MainPoint index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_mainPointId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MainPoint"/> class.</returns>
		public LMMR.Entities.MainPoint GetByMainPointId(TransactionManager transactionManager, System.Int64 _mainPointId, int start, int pageLength)
		{
			int count = -1;
			return GetByMainPointId(transactionManager, _mainPointId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MainPoint index.
		/// </summary>
		/// <param name="_mainPointId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MainPoint"/> class.</returns>
		public LMMR.Entities.MainPoint GetByMainPointId(System.Int64 _mainPointId, int start, int pageLength, out int count)
		{
			return GetByMainPointId(null, _mainPointId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MainPoint index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_mainPointId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MainPoint"/> class.</returns>
		public abstract LMMR.Entities.MainPoint GetByMainPointId(TransactionManager transactionManager, System.Int64 _mainPointId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;MainPoint&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;MainPoint&gt;"/></returns>
		public static TList<MainPoint> Fill(IDataReader reader, TList<MainPoint> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.MainPoint c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("MainPoint")
					.Append("|").Append((System.Int64)reader[((int)MainPointColumn.MainPointId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<MainPoint>(
					key.ToString(), // EntityTrackingKey
					"MainPoint",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.MainPoint();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.MainPointId = (System.Int64)reader[((int)MainPointColumn.MainPointId - 1)];
					c.MainPointName = (System.String)reader[((int)MainPointColumn.MainPointName - 1)];
					c.CityId = (System.Int64)reader[((int)MainPointColumn.CityId - 1)];
					c.Longitude = (System.Decimal)reader[((int)MainPointColumn.Longitude - 1)];
					c.Latitude = (System.Decimal)reader[((int)MainPointColumn.Latitude - 1)];
					c.IsCenter = (reader.IsDBNull(((int)MainPointColumn.IsCenter - 1)))?null:(System.Boolean?)reader[((int)MainPointColumn.IsCenter - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.MainPoint"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.MainPoint"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.MainPoint entity)
		{
			if (!reader.Read()) return;
			
			entity.MainPointId = (System.Int64)reader[((int)MainPointColumn.MainPointId - 1)];
			entity.MainPointName = (System.String)reader[((int)MainPointColumn.MainPointName - 1)];
			entity.CityId = (System.Int64)reader[((int)MainPointColumn.CityId - 1)];
			entity.Longitude = (System.Decimal)reader[((int)MainPointColumn.Longitude - 1)];
			entity.Latitude = (System.Decimal)reader[((int)MainPointColumn.Latitude - 1)];
			entity.IsCenter = (reader.IsDBNull(((int)MainPointColumn.IsCenter - 1)))?null:(System.Boolean?)reader[((int)MainPointColumn.IsCenter - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.MainPoint"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.MainPoint"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.MainPoint entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.MainPointId = (System.Int64)dataRow["MainPointID"];
			entity.MainPointName = (System.String)dataRow["MainPointName"];
			entity.CityId = (System.Int64)dataRow["CityID"];
			entity.Longitude = (System.Decimal)dataRow["Longitude"];
			entity.Latitude = (System.Decimal)dataRow["Latitude"];
			entity.IsCenter = Convert.IsDBNull(dataRow["IsCenter"]) ? null : (System.Boolean?)dataRow["IsCenter"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.MainPoint"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.MainPoint Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.MainPoint entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CityIdSource	
			if (CanDeepLoad(entity, "City|CityIdSource", deepLoadType, innerList) 
				&& entity.CityIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CityId;
				City tmpEntity = EntityManager.LocateEntity<City>(EntityLocator.ConstructKeyFromPkItems(typeof(City), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CityIdSource = tmpEntity;
				else
					entity.CityIdSource = DataRepository.CityProvider.GetById(transactionManager, entity.CityId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CityIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CityIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CityProvider.DeepLoad(transactionManager, entity.CityIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CityIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.MainPoint object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.MainPoint instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.MainPoint Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.MainPoint entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CityIdSource
			if (CanDeepSave(entity, "City|CityIdSource", deepSaveType, innerList) 
				&& entity.CityIdSource != null)
			{
				DataRepository.CityProvider.Save(transactionManager, entity.CityIdSource);
				entity.CityId = entity.CityIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region MainPointChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.MainPoint</c>
	///</summary>
	public enum MainPointChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>City</c> at CityIdSource
		///</summary>
		[ChildEntityType(typeof(City))]
		City,
		}
	
	#endregion MainPointChildEntityTypes
	
	#region MainPointFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;MainPointColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MainPoint"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MainPointFilterBuilder : SqlFilterBuilder<MainPointColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MainPointFilterBuilder class.
		/// </summary>
		public MainPointFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the MainPointFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MainPointFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MainPointFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MainPointFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MainPointFilterBuilder
	
	#region MainPointParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;MainPointColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MainPoint"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MainPointParameterBuilder : ParameterizedSqlFilterBuilder<MainPointColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MainPointParameterBuilder class.
		/// </summary>
		public MainPointParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the MainPointParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MainPointParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MainPointParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MainPointParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MainPointParameterBuilder
	
	#region MainPointSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;MainPointColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MainPoint"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class MainPointSortBuilder : SqlSortBuilder<MainPointColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MainPointSqlSortBuilder class.
		/// </summary>
		public MainPointSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion MainPointSortBuilder
	
} // end namespace
