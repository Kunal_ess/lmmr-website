﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SpandPpackageProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SpandPpackageProviderBaseCore : EntityProviderBase<LMMR.Entities.SpandPpackage, LMMR.Entities.SpandPpackageKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.SpandPpackageKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SPandPpackage_SpecialPriceAndPromo key.
		///		FK_SPandPpackage_SpecialPriceAndPromo Description: 
		/// </summary>
		/// <param name="_spandPid"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SpandPpackage objects.</returns>
		public TList<SpandPpackage> GetBySpandPid(System.Int64 _spandPid)
		{
			int count = -1;
			return GetBySpandPid(_spandPid, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SPandPpackage_SpecialPriceAndPromo key.
		///		FK_SPandPpackage_SpecialPriceAndPromo Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_spandPid"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SpandPpackage objects.</returns>
		/// <remarks></remarks>
		public TList<SpandPpackage> GetBySpandPid(TransactionManager transactionManager, System.Int64 _spandPid)
		{
			int count = -1;
			return GetBySpandPid(transactionManager, _spandPid, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_SPandPpackage_SpecialPriceAndPromo key.
		///		FK_SPandPpackage_SpecialPriceAndPromo Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_spandPid"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SpandPpackage objects.</returns>
		public TList<SpandPpackage> GetBySpandPid(TransactionManager transactionManager, System.Int64 _spandPid, int start, int pageLength)
		{
			int count = -1;
			return GetBySpandPid(transactionManager, _spandPid, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SPandPpackage_SpecialPriceAndPromo key.
		///		fkSpandPpackageSpecialPriceAndPromo Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_spandPid"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SpandPpackage objects.</returns>
		public TList<SpandPpackage> GetBySpandPid(System.Int64 _spandPid, int start, int pageLength)
		{
			int count =  -1;
			return GetBySpandPid(null, _spandPid, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SPandPpackage_SpecialPriceAndPromo key.
		///		fkSpandPpackageSpecialPriceAndPromo Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_spandPid"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SpandPpackage objects.</returns>
		public TList<SpandPpackage> GetBySpandPid(System.Int64 _spandPid, int start, int pageLength,out int count)
		{
			return GetBySpandPid(null, _spandPid, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SPandPpackage_SpecialPriceAndPromo key.
		///		FK_SPandPpackage_SpecialPriceAndPromo Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_spandPid"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.SpandPpackage objects.</returns>
		public abstract TList<SpandPpackage> GetBySpandPid(TransactionManager transactionManager, System.Int64 _spandPid, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.SpandPpackage Get(TransactionManager transactionManager, LMMR.Entities.SpandPpackageKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SPandPpackage index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SpandPpackage"/> class.</returns>
		public LMMR.Entities.SpandPpackage GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SPandPpackage index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SpandPpackage"/> class.</returns>
		public LMMR.Entities.SpandPpackage GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SPandPpackage index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SpandPpackage"/> class.</returns>
		public LMMR.Entities.SpandPpackage GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SPandPpackage index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SpandPpackage"/> class.</returns>
		public LMMR.Entities.SpandPpackage GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SPandPpackage index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SpandPpackage"/> class.</returns>
		public LMMR.Entities.SpandPpackage GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SPandPpackage index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SpandPpackage"/> class.</returns>
		public abstract LMMR.Entities.SpandPpackage GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SpandPpackage&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SpandPpackage&gt;"/></returns>
		public static TList<SpandPpackage> Fill(IDataReader reader, TList<SpandPpackage> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.SpandPpackage c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SpandPpackage")
					.Append("|").Append((System.Int64)reader[((int)SpandPpackageColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SpandPpackage>(
					key.ToString(), // EntityTrackingKey
					"SpandPpackage",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.SpandPpackage();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)SpandPpackageColumn.Id - 1)];
					c.SpandPid = (System.Int64)reader[((int)SpandPpackageColumn.SpandPid - 1)];
					c.DdrId = (System.Int64)reader[((int)SpandPpackageColumn.DdrId - 1)];
					c.AvailabilityHalfDay = (reader.IsDBNull(((int)SpandPpackageColumn.AvailabilityHalfDay - 1)))?null:(System.Int32?)reader[((int)SpandPpackageColumn.AvailabilityHalfDay - 1)];
					c.AvailabilityFullDay = (reader.IsDBNull(((int)SpandPpackageColumn.AvailabilityFullDay - 1)))?null:(System.Int32?)reader[((int)SpandPpackageColumn.AvailabilityFullDay - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.SpandPpackage"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SpandPpackage"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.SpandPpackage entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)SpandPpackageColumn.Id - 1)];
			entity.SpandPid = (System.Int64)reader[((int)SpandPpackageColumn.SpandPid - 1)];
			entity.DdrId = (System.Int64)reader[((int)SpandPpackageColumn.DdrId - 1)];
			entity.AvailabilityHalfDay = (reader.IsDBNull(((int)SpandPpackageColumn.AvailabilityHalfDay - 1)))?null:(System.Int32?)reader[((int)SpandPpackageColumn.AvailabilityHalfDay - 1)];
			entity.AvailabilityFullDay = (reader.IsDBNull(((int)SpandPpackageColumn.AvailabilityFullDay - 1)))?null:(System.Int32?)reader[((int)SpandPpackageColumn.AvailabilityFullDay - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.SpandPpackage"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SpandPpackage"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.SpandPpackage entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.SpandPid = (System.Int64)dataRow["SPandPId"];
			entity.DdrId = (System.Int64)dataRow["DDRId"];
			entity.AvailabilityHalfDay = Convert.IsDBNull(dataRow["AvailabilityHalfDay"]) ? null : (System.Int32?)dataRow["AvailabilityHalfDay"];
			entity.AvailabilityFullDay = Convert.IsDBNull(dataRow["AvailabilityFullDay"]) ? null : (System.Int32?)dataRow["AvailabilityFullDay"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SpandPpackage"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.SpandPpackage Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.SpandPpackage entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region SpandPidSource	
			if (CanDeepLoad(entity, "SpecialPriceAndPromo|SpandPidSource", deepLoadType, innerList) 
				&& entity.SpandPidSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SpandPid;
				SpecialPriceAndPromo tmpEntity = EntityManager.LocateEntity<SpecialPriceAndPromo>(EntityLocator.ConstructKeyFromPkItems(typeof(SpecialPriceAndPromo), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SpandPidSource = tmpEntity;
				else
					entity.SpandPidSource = DataRepository.SpecialPriceAndPromoProvider.GetById(transactionManager, entity.SpandPid);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SpandPidSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SpandPidSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SpecialPriceAndPromoProvider.DeepLoad(transactionManager, entity.SpandPidSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SpandPidSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.SpandPpackage object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.SpandPpackage instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.SpandPpackage Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.SpandPpackage entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region SpandPidSource
			if (CanDeepSave(entity, "SpecialPriceAndPromo|SpandPidSource", deepSaveType, innerList) 
				&& entity.SpandPidSource != null)
			{
				DataRepository.SpecialPriceAndPromoProvider.Save(transactionManager, entity.SpandPidSource);
				entity.SpandPid = entity.SpandPidSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SpandPpackageChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.SpandPpackage</c>
	///</summary>
	public enum SpandPpackageChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>SpecialPriceAndPromo</c> at SpandPidSource
		///</summary>
		[ChildEntityType(typeof(SpecialPriceAndPromo))]
		SpecialPriceAndPromo,
		}
	
	#endregion SpandPpackageChildEntityTypes
	
	#region SpandPpackageFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SpandPpackageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SpandPpackage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SpandPpackageFilterBuilder : SqlFilterBuilder<SpandPpackageColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SpandPpackageFilterBuilder class.
		/// </summary>
		public SpandPpackageFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SpandPpackageFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SpandPpackageFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SpandPpackageFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SpandPpackageFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SpandPpackageFilterBuilder
	
	#region SpandPpackageParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SpandPpackageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SpandPpackage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SpandPpackageParameterBuilder : ParameterizedSqlFilterBuilder<SpandPpackageColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SpandPpackageParameterBuilder class.
		/// </summary>
		public SpandPpackageParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SpandPpackageParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SpandPpackageParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SpandPpackageParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SpandPpackageParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SpandPpackageParameterBuilder
	
	#region SpandPpackageSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SpandPpackageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SpandPpackage"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SpandPpackageSortBuilder : SqlSortBuilder<SpandPpackageColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SpandPpackageSqlSortBuilder class.
		/// </summary>
		public SpandPpackageSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SpandPpackageSortBuilder
	
} // end namespace
