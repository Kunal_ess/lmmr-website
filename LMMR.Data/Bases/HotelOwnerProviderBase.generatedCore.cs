﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="HotelOwnerProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class HotelOwnerProviderBaseCore : EntityProviderBase<LMMR.Entities.HotelOwner, LMMR.Entities.HotelOwnerKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.HotelOwnerKey key)
		{
			return Delete(transactionManager, key.ClientId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_clientId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(long _clientId)
		{
			return Delete(null, _clientId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_clientId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, long _clientId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.HotelOwner Get(TransactionManager transactionManager, LMMR.Entities.HotelOwnerKey key, int start, int pageLength)
		{
			return GetByClientId(transactionManager, key.ClientId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Client index.
		/// </summary>
		/// <param name="_clientId"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HotelOwner"/> class.</returns>
		public LMMR.Entities.HotelOwner GetByClientId(long _clientId)
		{
			int count = -1;
			return GetByClientId(null,_clientId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Client index.
		/// </summary>
		/// <param name="_clientId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HotelOwner"/> class.</returns>
		public LMMR.Entities.HotelOwner GetByClientId(long _clientId, int start, int pageLength)
		{
			int count = -1;
			return GetByClientId(null, _clientId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Client index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_clientId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HotelOwner"/> class.</returns>
		public LMMR.Entities.HotelOwner GetByClientId(TransactionManager transactionManager, long _clientId)
		{
			int count = -1;
			return GetByClientId(transactionManager, _clientId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Client index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_clientId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HotelOwner"/> class.</returns>
		public LMMR.Entities.HotelOwner GetByClientId(TransactionManager transactionManager, long _clientId, int start, int pageLength)
		{
			int count = -1;
			return GetByClientId(transactionManager, _clientId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Client index.
		/// </summary>
		/// <param name="_clientId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HotelOwner"/> class.</returns>
		public LMMR.Entities.HotelOwner GetByClientId(long _clientId, int start, int pageLength, out int count)
		{
			return GetByClientId(null, _clientId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Client index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_clientId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HotelOwner"/> class.</returns>
		public abstract LMMR.Entities.HotelOwner GetByClientId(TransactionManager transactionManager, long _clientId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;HotelOwner&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;HotelOwner&gt;"/></returns>
		public static TList<HotelOwner> Fill(IDataReader reader, TList<HotelOwner> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.HotelOwner c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("HotelOwner")
					.Append("|").Append((long)reader[((int)HotelOwnerColumn.ClientId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<HotelOwner>(
					key.ToString(), // EntityTrackingKey
					"HotelOwner",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.HotelOwner();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ClientId = (long)reader[((int)HotelOwnerColumn.ClientId - 1)];
					c.ClientName = (string)reader[((int)HotelOwnerColumn.ClientName - 1)];
					c.VatNumber = (string)reader[((int)HotelOwnerColumn.VatNumber - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.HotelOwner"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.HotelOwner"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.HotelOwner entity)
		{
			if (!reader.Read()) return;
			
			entity.ClientId = (long)reader[((int)HotelOwnerColumn.ClientId - 1)];
			entity.ClientName = (string)reader[((int)HotelOwnerColumn.ClientName - 1)];
			entity.VatNumber = (string)reader[((int)HotelOwnerColumn.VatNumber - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.HotelOwner"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.HotelOwner"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.HotelOwner entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ClientId = (long)dataRow["ClientID"];
			entity.ClientName = (string)dataRow["ClientName"];
			entity.VatNumber = (string)dataRow["VatNumber"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.HotelOwner"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.HotelOwner Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.HotelOwner entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByClientId methods when available
			
			#region HotelCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Hotel>|HotelCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HotelCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.HotelCollection = DataRepository.HotelProvider.GetByClientId(transactionManager, entity.ClientId);

				if (deep && entity.HotelCollection.Count > 0)
				{
					deepHandles.Add("HotelCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Hotel>) DataRepository.HotelProvider.DeepLoad,
						new object[] { transactionManager, entity.HotelCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.HotelOwner object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.HotelOwner instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.HotelOwner Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.HotelOwner entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<Hotel>
				if (CanDeepSave(entity.HotelCollection, "List<Hotel>|HotelCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Hotel child in entity.HotelCollection)
					{
						if(child.ClientIdSource != null)
						{
							child.ClientId = child.ClientIdSource.ClientId;
						}
						else
						{
							child.ClientId = entity.ClientId;
						}

					}

					if (entity.HotelCollection.Count > 0 || entity.HotelCollection.DeletedItems.Count > 0)
					{
						//DataRepository.HotelProvider.Save(transactionManager, entity.HotelCollection);
						
						deepHandles.Add("HotelCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Hotel >) DataRepository.HotelProvider.DeepSave,
							new object[] { transactionManager, entity.HotelCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region HotelOwnerChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.HotelOwner</c>
	///</summary>
	public enum HotelOwnerChildEntityTypes
	{

		///<summary>
		/// Collection of <c>HotelOwner</c> as OneToMany for HotelCollection
		///</summary>
		[ChildEntityType(typeof(TList<Hotel>))]
		HotelCollection,
	}
	
	#endregion HotelOwnerChildEntityTypes
	
	#region HotelOwnerFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;HotelOwnerColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HotelOwner"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelOwnerFilterBuilder : SqlFilterBuilder<HotelOwnerColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelOwnerFilterBuilder class.
		/// </summary>
		public HotelOwnerFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelOwnerFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelOwnerFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelOwnerFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelOwnerFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelOwnerFilterBuilder
	
	#region HotelOwnerParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;HotelOwnerColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HotelOwner"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelOwnerParameterBuilder : ParameterizedSqlFilterBuilder<HotelOwnerColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelOwnerParameterBuilder class.
		/// </summary>
		public HotelOwnerParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelOwnerParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelOwnerParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelOwnerParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelOwnerParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelOwnerParameterBuilder
	
	#region HotelOwnerSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;HotelOwnerColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HotelOwner"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class HotelOwnerSortBuilder : SqlSortBuilder<HotelOwnerColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelOwnerSqlSortBuilder class.
		/// </summary>
		public HotelOwnerSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion HotelOwnerSortBuilder
	
} // end namespace
