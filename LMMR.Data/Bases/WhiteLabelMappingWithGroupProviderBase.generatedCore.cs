﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="WhiteLabelMappingWithGroupProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class WhiteLabelMappingWithGroupProviderBaseCore : EntityProviderBase<LMMR.Entities.WhiteLabelMappingWithGroup, LMMR.Entities.WhiteLabelMappingWithGroupKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.WhiteLabelMappingWithGroupKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_WhiteLabelMappingWithGroup_Users key.
		///		FK_WhiteLabelMappingWithGroup_Users Description: 
		/// </summary>
		/// <param name="_groupId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.WhiteLabelMappingWithGroup objects.</returns>
		public TList<WhiteLabelMappingWithGroup> GetByGroupId(System.Int64 _groupId)
		{
			int count = -1;
			return GetByGroupId(_groupId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_WhiteLabelMappingWithGroup_Users key.
		///		FK_WhiteLabelMappingWithGroup_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_groupId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.WhiteLabelMappingWithGroup objects.</returns>
		/// <remarks></remarks>
		public TList<WhiteLabelMappingWithGroup> GetByGroupId(TransactionManager transactionManager, System.Int64 _groupId)
		{
			int count = -1;
			return GetByGroupId(transactionManager, _groupId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_WhiteLabelMappingWithGroup_Users key.
		///		FK_WhiteLabelMappingWithGroup_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_groupId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.WhiteLabelMappingWithGroup objects.</returns>
		public TList<WhiteLabelMappingWithGroup> GetByGroupId(TransactionManager transactionManager, System.Int64 _groupId, int start, int pageLength)
		{
			int count = -1;
			return GetByGroupId(transactionManager, _groupId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_WhiteLabelMappingWithGroup_Users key.
		///		fkWhiteLabelMappingWithGroupUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_groupId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.WhiteLabelMappingWithGroup objects.</returns>
		public TList<WhiteLabelMappingWithGroup> GetByGroupId(System.Int64 _groupId, int start, int pageLength)
		{
			int count =  -1;
			return GetByGroupId(null, _groupId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_WhiteLabelMappingWithGroup_Users key.
		///		fkWhiteLabelMappingWithGroupUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_groupId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.WhiteLabelMappingWithGroup objects.</returns>
		public TList<WhiteLabelMappingWithGroup> GetByGroupId(System.Int64 _groupId, int start, int pageLength,out int count)
		{
			return GetByGroupId(null, _groupId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_WhiteLabelMappingWithGroup_Users key.
		///		FK_WhiteLabelMappingWithGroup_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_groupId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.WhiteLabelMappingWithGroup objects.</returns>
		public abstract TList<WhiteLabelMappingWithGroup> GetByGroupId(TransactionManager transactionManager, System.Int64 _groupId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_WhiteLabelMappingWithGroup_WhiteLabel key.
		///		FK_WhiteLabelMappingWithGroup_WhiteLabel Description: 
		/// </summary>
		/// <param name="_whiteLabelId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.WhiteLabelMappingWithGroup objects.</returns>
		public TList<WhiteLabelMappingWithGroup> GetByWhiteLabelId(System.Int64 _whiteLabelId)
		{
			int count = -1;
			return GetByWhiteLabelId(_whiteLabelId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_WhiteLabelMappingWithGroup_WhiteLabel key.
		///		FK_WhiteLabelMappingWithGroup_WhiteLabel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_whiteLabelId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.WhiteLabelMappingWithGroup objects.</returns>
		/// <remarks></remarks>
		public TList<WhiteLabelMappingWithGroup> GetByWhiteLabelId(TransactionManager transactionManager, System.Int64 _whiteLabelId)
		{
			int count = -1;
			return GetByWhiteLabelId(transactionManager, _whiteLabelId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_WhiteLabelMappingWithGroup_WhiteLabel key.
		///		FK_WhiteLabelMappingWithGroup_WhiteLabel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_whiteLabelId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.WhiteLabelMappingWithGroup objects.</returns>
		public TList<WhiteLabelMappingWithGroup> GetByWhiteLabelId(TransactionManager transactionManager, System.Int64 _whiteLabelId, int start, int pageLength)
		{
			int count = -1;
			return GetByWhiteLabelId(transactionManager, _whiteLabelId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_WhiteLabelMappingWithGroup_WhiteLabel key.
		///		fkWhiteLabelMappingWithGroupWhiteLabel Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_whiteLabelId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.WhiteLabelMappingWithGroup objects.</returns>
		public TList<WhiteLabelMappingWithGroup> GetByWhiteLabelId(System.Int64 _whiteLabelId, int start, int pageLength)
		{
			int count =  -1;
			return GetByWhiteLabelId(null, _whiteLabelId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_WhiteLabelMappingWithGroup_WhiteLabel key.
		///		fkWhiteLabelMappingWithGroupWhiteLabel Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_whiteLabelId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.WhiteLabelMappingWithGroup objects.</returns>
		public TList<WhiteLabelMappingWithGroup> GetByWhiteLabelId(System.Int64 _whiteLabelId, int start, int pageLength,out int count)
		{
			return GetByWhiteLabelId(null, _whiteLabelId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_WhiteLabelMappingWithGroup_WhiteLabel key.
		///		FK_WhiteLabelMappingWithGroup_WhiteLabel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_whiteLabelId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.WhiteLabelMappingWithGroup objects.</returns>
		public abstract TList<WhiteLabelMappingWithGroup> GetByWhiteLabelId(TransactionManager transactionManager, System.Int64 _whiteLabelId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.WhiteLabelMappingWithGroup Get(TransactionManager transactionManager, LMMR.Entities.WhiteLabelMappingWithGroupKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_WhiteLabelMappingWithGroup index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.WhiteLabelMappingWithGroup"/> class.</returns>
		public LMMR.Entities.WhiteLabelMappingWithGroup GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_WhiteLabelMappingWithGroup index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.WhiteLabelMappingWithGroup"/> class.</returns>
		public LMMR.Entities.WhiteLabelMappingWithGroup GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_WhiteLabelMappingWithGroup index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.WhiteLabelMappingWithGroup"/> class.</returns>
		public LMMR.Entities.WhiteLabelMappingWithGroup GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_WhiteLabelMappingWithGroup index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.WhiteLabelMappingWithGroup"/> class.</returns>
		public LMMR.Entities.WhiteLabelMappingWithGroup GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_WhiteLabelMappingWithGroup index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.WhiteLabelMappingWithGroup"/> class.</returns>
		public LMMR.Entities.WhiteLabelMappingWithGroup GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_WhiteLabelMappingWithGroup index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.WhiteLabelMappingWithGroup"/> class.</returns>
		public abstract LMMR.Entities.WhiteLabelMappingWithGroup GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;WhiteLabelMappingWithGroup&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;WhiteLabelMappingWithGroup&gt;"/></returns>
		public static TList<WhiteLabelMappingWithGroup> Fill(IDataReader reader, TList<WhiteLabelMappingWithGroup> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.WhiteLabelMappingWithGroup c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("WhiteLabelMappingWithGroup")
					.Append("|").Append((System.Int64)reader[((int)WhiteLabelMappingWithGroupColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<WhiteLabelMappingWithGroup>(
					key.ToString(), // EntityTrackingKey
					"WhiteLabelMappingWithGroup",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.WhiteLabelMappingWithGroup();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)WhiteLabelMappingWithGroupColumn.Id - 1)];
					c.WhiteLabelId = (System.Int64)reader[((int)WhiteLabelMappingWithGroupColumn.WhiteLabelId - 1)];
					c.GroupId = (System.Int64)reader[((int)WhiteLabelMappingWithGroupColumn.GroupId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.WhiteLabelMappingWithGroup"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.WhiteLabelMappingWithGroup"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.WhiteLabelMappingWithGroup entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)WhiteLabelMappingWithGroupColumn.Id - 1)];
			entity.WhiteLabelId = (System.Int64)reader[((int)WhiteLabelMappingWithGroupColumn.WhiteLabelId - 1)];
			entity.GroupId = (System.Int64)reader[((int)WhiteLabelMappingWithGroupColumn.GroupId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.WhiteLabelMappingWithGroup"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.WhiteLabelMappingWithGroup"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.WhiteLabelMappingWithGroup entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.WhiteLabelId = (System.Int64)dataRow["WhiteLabelId"];
			entity.GroupId = (System.Int64)dataRow["GroupId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.WhiteLabelMappingWithGroup"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.WhiteLabelMappingWithGroup Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.WhiteLabelMappingWithGroup entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region GroupIdSource	
			if (CanDeepLoad(entity, "Users|GroupIdSource", deepLoadType, innerList) 
				&& entity.GroupIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.GroupId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.GroupIdSource = tmpEntity;
				else
					entity.GroupIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.GroupId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'GroupIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.GroupIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.GroupIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion GroupIdSource

			#region WhiteLabelIdSource	
			if (CanDeepLoad(entity, "WhiteLabel|WhiteLabelIdSource", deepLoadType, innerList) 
				&& entity.WhiteLabelIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.WhiteLabelId;
				WhiteLabel tmpEntity = EntityManager.LocateEntity<WhiteLabel>(EntityLocator.ConstructKeyFromPkItems(typeof(WhiteLabel), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.WhiteLabelIdSource = tmpEntity;
				else
					entity.WhiteLabelIdSource = DataRepository.WhiteLabelProvider.GetById(transactionManager, entity.WhiteLabelId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'WhiteLabelIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.WhiteLabelIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.WhiteLabelProvider.DeepLoad(transactionManager, entity.WhiteLabelIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion WhiteLabelIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.WhiteLabelMappingWithGroup object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.WhiteLabelMappingWithGroup instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.WhiteLabelMappingWithGroup Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.WhiteLabelMappingWithGroup entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region GroupIdSource
			if (CanDeepSave(entity, "Users|GroupIdSource", deepSaveType, innerList) 
				&& entity.GroupIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.GroupIdSource);
				entity.GroupId = entity.GroupIdSource.UserId;
			}
			#endregion 
			
			#region WhiteLabelIdSource
			if (CanDeepSave(entity, "WhiteLabel|WhiteLabelIdSource", deepSaveType, innerList) 
				&& entity.WhiteLabelIdSource != null)
			{
				DataRepository.WhiteLabelProvider.Save(transactionManager, entity.WhiteLabelIdSource);
				entity.WhiteLabelId = entity.WhiteLabelIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region WhiteLabelMappingWithGroupChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.WhiteLabelMappingWithGroup</c>
	///</summary>
	public enum WhiteLabelMappingWithGroupChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Users</c> at GroupIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
			
		///<summary>
		/// Composite Property for <c>WhiteLabel</c> at WhiteLabelIdSource
		///</summary>
		[ChildEntityType(typeof(WhiteLabel))]
		WhiteLabel,
		}
	
	#endregion WhiteLabelMappingWithGroupChildEntityTypes
	
	#region WhiteLabelMappingWithGroupFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;WhiteLabelMappingWithGroupColumn&gt;"/> class
	/// that is used exclusively with a <see cref="WhiteLabelMappingWithGroup"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class WhiteLabelMappingWithGroupFilterBuilder : SqlFilterBuilder<WhiteLabelMappingWithGroupColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the WhiteLabelMappingWithGroupFilterBuilder class.
		/// </summary>
		public WhiteLabelMappingWithGroupFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the WhiteLabelMappingWithGroupFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public WhiteLabelMappingWithGroupFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the WhiteLabelMappingWithGroupFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public WhiteLabelMappingWithGroupFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion WhiteLabelMappingWithGroupFilterBuilder
	
	#region WhiteLabelMappingWithGroupParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;WhiteLabelMappingWithGroupColumn&gt;"/> class
	/// that is used exclusively with a <see cref="WhiteLabelMappingWithGroup"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class WhiteLabelMappingWithGroupParameterBuilder : ParameterizedSqlFilterBuilder<WhiteLabelMappingWithGroupColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the WhiteLabelMappingWithGroupParameterBuilder class.
		/// </summary>
		public WhiteLabelMappingWithGroupParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the WhiteLabelMappingWithGroupParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public WhiteLabelMappingWithGroupParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the WhiteLabelMappingWithGroupParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public WhiteLabelMappingWithGroupParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion WhiteLabelMappingWithGroupParameterBuilder
	
	#region WhiteLabelMappingWithGroupSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;WhiteLabelMappingWithGroupColumn&gt;"/> class
	/// that is used exclusively with a <see cref="WhiteLabelMappingWithGroup"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class WhiteLabelMappingWithGroupSortBuilder : SqlSortBuilder<WhiteLabelMappingWithGroupColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the WhiteLabelMappingWithGroupSqlSortBuilder class.
		/// </summary>
		public WhiteLabelMappingWithGroupSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion WhiteLabelMappingWithGroupSortBuilder
	
} // end namespace
