﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ServeyQuestionProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ServeyQuestionProviderBaseCore : EntityProviderBase<LMMR.Entities.ServeyQuestion, LMMR.Entities.ServeyQuestionKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.ServeyQuestionKey key)
		{
			return Delete(transactionManager, key.QuestionId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_questionId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _questionId)
		{
			return Delete(null, _questionId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _questionId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.ServeyQuestion Get(TransactionManager transactionManager, LMMR.Entities.ServeyQuestionKey key, int start, int pageLength)
		{
			return GetByQuestionId(transactionManager, key.QuestionId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ServeyQuestion index.
		/// </summary>
		/// <param name="_questionId"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.ServeyQuestion"/> class.</returns>
		public LMMR.Entities.ServeyQuestion GetByQuestionId(System.Int32 _questionId)
		{
			int count = -1;
			return GetByQuestionId(null,_questionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ServeyQuestion index.
		/// </summary>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.ServeyQuestion"/> class.</returns>
		public LMMR.Entities.ServeyQuestion GetByQuestionId(System.Int32 _questionId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionId(null, _questionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ServeyQuestion index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.ServeyQuestion"/> class.</returns>
		public LMMR.Entities.ServeyQuestion GetByQuestionId(TransactionManager transactionManager, System.Int32 _questionId)
		{
			int count = -1;
			return GetByQuestionId(transactionManager, _questionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ServeyQuestion index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.ServeyQuestion"/> class.</returns>
		public LMMR.Entities.ServeyQuestion GetByQuestionId(TransactionManager transactionManager, System.Int32 _questionId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionId(transactionManager, _questionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ServeyQuestion index.
		/// </summary>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.ServeyQuestion"/> class.</returns>
		public LMMR.Entities.ServeyQuestion GetByQuestionId(System.Int32 _questionId, int start, int pageLength, out int count)
		{
			return GetByQuestionId(null, _questionId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ServeyQuestion index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.ServeyQuestion"/> class.</returns>
		public abstract LMMR.Entities.ServeyQuestion GetByQuestionId(TransactionManager transactionManager, System.Int32 _questionId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ServeyQuestion&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ServeyQuestion&gt;"/></returns>
		public static TList<ServeyQuestion> Fill(IDataReader reader, TList<ServeyQuestion> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.ServeyQuestion c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ServeyQuestion")
					.Append("|").Append((System.Int32)reader[((int)ServeyQuestionColumn.QuestionId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ServeyQuestion>(
					key.ToString(), // EntityTrackingKey
					"ServeyQuestion",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.ServeyQuestion();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.QuestionId = (System.Int32)reader[((int)ServeyQuestionColumn.QuestionId - 1)];
					c.QuestionName = (reader.IsDBNull(((int)ServeyQuestionColumn.QuestionName - 1)))?null:(System.String)reader[((int)ServeyQuestionColumn.QuestionName - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.ServeyQuestion"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.ServeyQuestion"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.ServeyQuestion entity)
		{
			if (!reader.Read()) return;
			
			entity.QuestionId = (System.Int32)reader[((int)ServeyQuestionColumn.QuestionId - 1)];
			entity.QuestionName = (reader.IsDBNull(((int)ServeyQuestionColumn.QuestionName - 1)))?null:(System.String)reader[((int)ServeyQuestionColumn.QuestionName - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.ServeyQuestion"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.ServeyQuestion"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.ServeyQuestion entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionId = (System.Int32)dataRow["QuestionID"];
			entity.QuestionName = Convert.IsDBNull(dataRow["QuestionName"]) ? null : (System.String)dataRow["QuestionName"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.ServeyQuestion"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.ServeyQuestion Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.ServeyQuestion entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region QuestionIdSource	
			if (CanDeepLoad(entity, "ServeyQuestion|QuestionIdSource", deepLoadType, innerList) 
				&& entity.QuestionIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.QuestionId;
				ServeyQuestion tmpEntity = EntityManager.LocateEntity<ServeyQuestion>(EntityLocator.ConstructKeyFromPkItems(typeof(ServeyQuestion), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.QuestionIdSource = tmpEntity;
				else
					entity.QuestionIdSource = DataRepository.ServeyQuestionProvider.GetByQuestionId(transactionManager, entity.QuestionId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.QuestionIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ServeyQuestionProvider.DeepLoad(transactionManager, entity.QuestionIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion QuestionIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByQuestionId methods when available
			
			#region ServeyQuestion
			// RelationshipType.OneToOne
			if (CanDeepLoad(entity, "ServeyQuestion|ServeyQuestion", deepLoadType, innerList))
			{
				entity.ServeyQuestion = DataRepository.ServeyQuestionProvider.GetByQuestionId(transactionManager, entity.QuestionId);
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ServeyQuestion' loaded. key " + entity.EntityTrackingKey);
				#endif 

				if (deep && entity.ServeyQuestion != null)
				{
					deepHandles.Add("ServeyQuestion",
						new KeyValuePair<Delegate, object>((DeepLoadSingleHandle< ServeyQuestion >) DataRepository.ServeyQuestionProvider.DeepLoad,
						new object[] { transactionManager, entity.ServeyQuestion, deep, deepLoadType, childTypes, innerList }
					));
				}
			}
			#endregion 
			
			
			
			#region ServeyAnswerCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ServeyAnswer>|ServeyAnswerCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ServeyAnswerCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ServeyAnswerCollection = DataRepository.ServeyAnswerProvider.GetByQuestionId(transactionManager, entity.QuestionId);

				if (deep && entity.ServeyAnswerCollection.Count > 0)
				{
					deepHandles.Add("ServeyAnswerCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ServeyAnswer>) DataRepository.ServeyAnswerProvider.DeepLoad,
						new object[] { transactionManager, entity.ServeyAnswerCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ServeyresultCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Serveyresult>|ServeyresultCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ServeyresultCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ServeyresultCollection = DataRepository.ServeyresultProvider.GetByQuestionId(transactionManager, entity.QuestionId);

				if (deep && entity.ServeyresultCollection.Count > 0)
				{
					deepHandles.Add("ServeyresultCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Serveyresult>) DataRepository.ServeyresultProvider.DeepLoad,
						new object[] { transactionManager, entity.ServeyresultCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.ServeyQuestion object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.ServeyQuestion instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.ServeyQuestion Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.ServeyQuestion entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region QuestionIdSource
			if (CanDeepSave(entity, "ServeyQuestion|QuestionIdSource", deepSaveType, innerList) 
				&& entity.QuestionIdSource != null)
			{
				DataRepository.ServeyQuestionProvider.Save(transactionManager, entity.QuestionIdSource);
				entity.QuestionId = entity.QuestionIdSource.QuestionId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();

			#region ServeyQuestion
			if (CanDeepSave(entity.ServeyQuestion, "ServeyQuestion|ServeyQuestion", deepSaveType, innerList))
			{

				if (entity.ServeyQuestion != null)
				{
					// update each child parent id with the real parent id (mostly used on insert)

					entity.ServeyQuestion.QuestionId = entity.QuestionId;
					//DataRepository.ServeyQuestionProvider.Save(transactionManager, entity.ServeyQuestion);
					deepHandles.Add("ServeyQuestion",
						new KeyValuePair<Delegate, object>((DeepSaveSingleHandle< ServeyQuestion >) DataRepository.ServeyQuestionProvider.DeepSave,
						new object[] { transactionManager, entity.ServeyQuestion, deepSaveType, childTypes, innerList }
					));
				}
			} 
			#endregion 
	
			#region List<ServeyAnswer>
				if (CanDeepSave(entity.ServeyAnswerCollection, "List<ServeyAnswer>|ServeyAnswerCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ServeyAnswer child in entity.ServeyAnswerCollection)
					{
						if(child.QuestionIdSource != null)
						{
							child.QuestionId = child.QuestionIdSource.QuestionId;
						}
						else
						{
							child.QuestionId = entity.QuestionId;
						}

					}

					if (entity.ServeyAnswerCollection.Count > 0 || entity.ServeyAnswerCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ServeyAnswerProvider.Save(transactionManager, entity.ServeyAnswerCollection);
						
						deepHandles.Add("ServeyAnswerCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ServeyAnswer >) DataRepository.ServeyAnswerProvider.DeepSave,
							new object[] { transactionManager, entity.ServeyAnswerCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Serveyresult>
				if (CanDeepSave(entity.ServeyresultCollection, "List<Serveyresult>|ServeyresultCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Serveyresult child in entity.ServeyresultCollection)
					{
						if(child.QuestionIdSource != null)
						{
							child.QuestionId = child.QuestionIdSource.QuestionId;
						}
						else
						{
							child.QuestionId = entity.QuestionId;
						}

					}

					if (entity.ServeyresultCollection.Count > 0 || entity.ServeyresultCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ServeyresultProvider.Save(transactionManager, entity.ServeyresultCollection);
						
						deepHandles.Add("ServeyresultCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Serveyresult >) DataRepository.ServeyresultProvider.DeepSave,
							new object[] { transactionManager, entity.ServeyresultCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ServeyQuestionChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.ServeyQuestion</c>
	///</summary>
	public enum ServeyQuestionChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>ServeyQuestion</c> at QuestionIdSource
		///</summary>
		[ChildEntityType(typeof(ServeyQuestion))]
		ServeyQuestion,
	
		///<summary>
		/// Collection of <c>ServeyQuestion</c> as OneToMany for ServeyAnswerCollection
		///</summary>
		[ChildEntityType(typeof(TList<ServeyAnswer>))]
		ServeyAnswerCollection,

		///<summary>
		/// Collection of <c>ServeyQuestion</c> as OneToMany for ServeyresultCollection
		///</summary>
		[ChildEntityType(typeof(TList<Serveyresult>))]
		ServeyresultCollection,
	}
	
	#endregion ServeyQuestionChildEntityTypes
	
	#region ServeyQuestionFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ServeyQuestionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ServeyQuestion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ServeyQuestionFilterBuilder : SqlFilterBuilder<ServeyQuestionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ServeyQuestionFilterBuilder class.
		/// </summary>
		public ServeyQuestionFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ServeyQuestionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ServeyQuestionFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ServeyQuestionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ServeyQuestionFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ServeyQuestionFilterBuilder
	
	#region ServeyQuestionParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ServeyQuestionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ServeyQuestion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ServeyQuestionParameterBuilder : ParameterizedSqlFilterBuilder<ServeyQuestionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ServeyQuestionParameterBuilder class.
		/// </summary>
		public ServeyQuestionParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ServeyQuestionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ServeyQuestionParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ServeyQuestionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ServeyQuestionParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ServeyQuestionParameterBuilder
	
	#region ServeyQuestionSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ServeyQuestionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ServeyQuestion"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ServeyQuestionSortBuilder : SqlSortBuilder<ServeyQuestionColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ServeyQuestionSqlSortBuilder class.
		/// </summary>
		public ServeyQuestionSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ServeyQuestionSortBuilder
	
} // end namespace
