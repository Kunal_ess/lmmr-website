﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="MeetingRoomConfigProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class MeetingRoomConfigProviderBaseCore : EntityProviderBase<LMMR.Entities.MeetingRoomConfig, LMMR.Entities.MeetingRoomConfigKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.MeetingRoomConfigKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomConfig_Hotel key.
		///		FK_MeetingRoomConfig_Hotel Description: 
		/// </summary>
		/// <param name="_hotelId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomConfig objects.</returns>
		public TList<MeetingRoomConfig> GetByHotelId(System.Int64? _hotelId)
		{
			int count = -1;
			return GetByHotelId(_hotelId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomConfig_Hotel key.
		///		FK_MeetingRoomConfig_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomConfig objects.</returns>
		/// <remarks></remarks>
		public TList<MeetingRoomConfig> GetByHotelId(TransactionManager transactionManager, System.Int64? _hotelId)
		{
			int count = -1;
			return GetByHotelId(transactionManager, _hotelId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomConfig_Hotel key.
		///		FK_MeetingRoomConfig_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomConfig objects.</returns>
		public TList<MeetingRoomConfig> GetByHotelId(TransactionManager transactionManager, System.Int64? _hotelId, int start, int pageLength)
		{
			int count = -1;
			return GetByHotelId(transactionManager, _hotelId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomConfig_Hotel key.
		///		fkMeetingRoomConfigHotel Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_hotelId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomConfig objects.</returns>
		public TList<MeetingRoomConfig> GetByHotelId(System.Int64? _hotelId, int start, int pageLength)
		{
			int count =  -1;
			return GetByHotelId(null, _hotelId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomConfig_Hotel key.
		///		fkMeetingRoomConfigHotel Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_hotelId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomConfig objects.</returns>
		public TList<MeetingRoomConfig> GetByHotelId(System.Int64? _hotelId, int start, int pageLength,out int count)
		{
			return GetByHotelId(null, _hotelId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomConfig_Hotel key.
		///		FK_MeetingRoomConfig_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomConfig objects.</returns>
		public abstract TList<MeetingRoomConfig> GetByHotelId(TransactionManager transactionManager, System.Int64? _hotelId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomConfig_MeetingRoom key.
		///		FK_MeetingRoomConfig_MeetingRoom Description: 
		/// </summary>
		/// <param name="_meetingRoomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomConfig objects.</returns>
		public TList<MeetingRoomConfig> GetByMeetingRoomId(System.Int64 _meetingRoomId)
		{
			int count = -1;
			return GetByMeetingRoomId(_meetingRoomId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomConfig_MeetingRoom key.
		///		FK_MeetingRoomConfig_MeetingRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_meetingRoomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomConfig objects.</returns>
		/// <remarks></remarks>
		public TList<MeetingRoomConfig> GetByMeetingRoomId(TransactionManager transactionManager, System.Int64 _meetingRoomId)
		{
			int count = -1;
			return GetByMeetingRoomId(transactionManager, _meetingRoomId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomConfig_MeetingRoom key.
		///		FK_MeetingRoomConfig_MeetingRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_meetingRoomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomConfig objects.</returns>
		public TList<MeetingRoomConfig> GetByMeetingRoomId(TransactionManager transactionManager, System.Int64 _meetingRoomId, int start, int pageLength)
		{
			int count = -1;
			return GetByMeetingRoomId(transactionManager, _meetingRoomId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomConfig_MeetingRoom key.
		///		fkMeetingRoomConfigMeetingRoom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_meetingRoomId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomConfig objects.</returns>
		public TList<MeetingRoomConfig> GetByMeetingRoomId(System.Int64 _meetingRoomId, int start, int pageLength)
		{
			int count =  -1;
			return GetByMeetingRoomId(null, _meetingRoomId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomConfig_MeetingRoom key.
		///		fkMeetingRoomConfigMeetingRoom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_meetingRoomId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomConfig objects.</returns>
		public TList<MeetingRoomConfig> GetByMeetingRoomId(System.Int64 _meetingRoomId, int start, int pageLength,out int count)
		{
			return GetByMeetingRoomId(null, _meetingRoomId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomConfig_MeetingRoom key.
		///		FK_MeetingRoomConfig_MeetingRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_meetingRoomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomConfig objects.</returns>
		public abstract TList<MeetingRoomConfig> GetByMeetingRoomId(TransactionManager transactionManager, System.Int64 _meetingRoomId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomConfig_Users key.
		///		FK_MeetingRoomConfig_Users Description: 
		/// </summary>
		/// <param name="_updatedBy"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomConfig objects.</returns>
		public TList<MeetingRoomConfig> GetByUpdatedBy(System.Int64? _updatedBy)
		{
			int count = -1;
			return GetByUpdatedBy(_updatedBy, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomConfig_Users key.
		///		FK_MeetingRoomConfig_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_updatedBy"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomConfig objects.</returns>
		/// <remarks></remarks>
		public TList<MeetingRoomConfig> GetByUpdatedBy(TransactionManager transactionManager, System.Int64? _updatedBy)
		{
			int count = -1;
			return GetByUpdatedBy(transactionManager, _updatedBy, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomConfig_Users key.
		///		FK_MeetingRoomConfig_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_updatedBy"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomConfig objects.</returns>
		public TList<MeetingRoomConfig> GetByUpdatedBy(TransactionManager transactionManager, System.Int64? _updatedBy, int start, int pageLength)
		{
			int count = -1;
			return GetByUpdatedBy(transactionManager, _updatedBy, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomConfig_Users key.
		///		fkMeetingRoomConfigUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_updatedBy"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomConfig objects.</returns>
		public TList<MeetingRoomConfig> GetByUpdatedBy(System.Int64? _updatedBy, int start, int pageLength)
		{
			int count =  -1;
			return GetByUpdatedBy(null, _updatedBy, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomConfig_Users key.
		///		fkMeetingRoomConfigUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_updatedBy"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomConfig objects.</returns>
		public TList<MeetingRoomConfig> GetByUpdatedBy(System.Int64? _updatedBy, int start, int pageLength,out int count)
		{
			return GetByUpdatedBy(null, _updatedBy, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomConfig_Users key.
		///		FK_MeetingRoomConfig_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_updatedBy"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomConfig objects.</returns>
		public abstract TList<MeetingRoomConfig> GetByUpdatedBy(TransactionManager transactionManager, System.Int64? _updatedBy, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.MeetingRoomConfig Get(TransactionManager transactionManager, LMMR.Entities.MeetingRoomConfigKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_MeetingRoomConfig index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomConfig"/> class.</returns>
		public LMMR.Entities.MeetingRoomConfig GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoomConfig index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomConfig"/> class.</returns>
		public LMMR.Entities.MeetingRoomConfig GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoomConfig index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomConfig"/> class.</returns>
		public LMMR.Entities.MeetingRoomConfig GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoomConfig index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomConfig"/> class.</returns>
		public LMMR.Entities.MeetingRoomConfig GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoomConfig index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomConfig"/> class.</returns>
		public LMMR.Entities.MeetingRoomConfig GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoomConfig index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomConfig"/> class.</returns>
		public abstract LMMR.Entities.MeetingRoomConfig GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;MeetingRoomConfig&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;MeetingRoomConfig&gt;"/></returns>
		public static TList<MeetingRoomConfig> Fill(IDataReader reader, TList<MeetingRoomConfig> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.MeetingRoomConfig c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("MeetingRoomConfig")
					.Append("|").Append((System.Int64)reader[((int)MeetingRoomConfigColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<MeetingRoomConfig>(
					key.ToString(), // EntityTrackingKey
					"MeetingRoomConfig",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.MeetingRoomConfig();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)MeetingRoomConfigColumn.Id - 1)];
					c.MeetingRoomId = (System.Int64)reader[((int)MeetingRoomConfigColumn.MeetingRoomId - 1)];
					c.RoomShapeId = (reader.IsDBNull(((int)MeetingRoomConfigColumn.RoomShapeId - 1)))?null:(System.Int32?)reader[((int)MeetingRoomConfigColumn.RoomShapeId - 1)];
					c.MinCapacity = (reader.IsDBNull(((int)MeetingRoomConfigColumn.MinCapacity - 1)))?null:(System.Int32?)reader[((int)MeetingRoomConfigColumn.MinCapacity - 1)];
					c.MaxCapicity = (reader.IsDBNull(((int)MeetingRoomConfigColumn.MaxCapicity - 1)))?null:(System.Int32?)reader[((int)MeetingRoomConfigColumn.MaxCapicity - 1)];
					c.HotelId = (reader.IsDBNull(((int)MeetingRoomConfigColumn.HotelId - 1)))?null:(System.Int64?)reader[((int)MeetingRoomConfigColumn.HotelId - 1)];
					c.UpdatedBy = (reader.IsDBNull(((int)MeetingRoomConfigColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)MeetingRoomConfigColumn.UpdatedBy - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.MeetingRoomConfig"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.MeetingRoomConfig"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.MeetingRoomConfig entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)MeetingRoomConfigColumn.Id - 1)];
			entity.MeetingRoomId = (System.Int64)reader[((int)MeetingRoomConfigColumn.MeetingRoomId - 1)];
			entity.RoomShapeId = (reader.IsDBNull(((int)MeetingRoomConfigColumn.RoomShapeId - 1)))?null:(System.Int32?)reader[((int)MeetingRoomConfigColumn.RoomShapeId - 1)];
			entity.MinCapacity = (reader.IsDBNull(((int)MeetingRoomConfigColumn.MinCapacity - 1)))?null:(System.Int32?)reader[((int)MeetingRoomConfigColumn.MinCapacity - 1)];
			entity.MaxCapicity = (reader.IsDBNull(((int)MeetingRoomConfigColumn.MaxCapicity - 1)))?null:(System.Int32?)reader[((int)MeetingRoomConfigColumn.MaxCapicity - 1)];
			entity.HotelId = (reader.IsDBNull(((int)MeetingRoomConfigColumn.HotelId - 1)))?null:(System.Int64?)reader[((int)MeetingRoomConfigColumn.HotelId - 1)];
			entity.UpdatedBy = (reader.IsDBNull(((int)MeetingRoomConfigColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)MeetingRoomConfigColumn.UpdatedBy - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.MeetingRoomConfig"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.MeetingRoomConfig"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.MeetingRoomConfig entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.MeetingRoomId = (System.Int64)dataRow["MeetingRoom_Id"];
			entity.RoomShapeId = Convert.IsDBNull(dataRow["RoomShapeId"]) ? null : (System.Int32?)dataRow["RoomShapeId"];
			entity.MinCapacity = Convert.IsDBNull(dataRow["MinCapacity"]) ? null : (System.Int32?)dataRow["MinCapacity"];
			entity.MaxCapicity = Convert.IsDBNull(dataRow["MaxCapicity"]) ? null : (System.Int32?)dataRow["MaxCapicity"];
			entity.HotelId = Convert.IsDBNull(dataRow["HotelID"]) ? null : (System.Int64?)dataRow["HotelID"];
			entity.UpdatedBy = Convert.IsDBNull(dataRow["UpdatedBy"]) ? null : (System.Int64?)dataRow["UpdatedBy"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.MeetingRoomConfig"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.MeetingRoomConfig Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.MeetingRoomConfig entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region HotelIdSource	
			if (CanDeepLoad(entity, "Hotel|HotelIdSource", deepLoadType, innerList) 
				&& entity.HotelIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.HotelId ?? (long)0);
				Hotel tmpEntity = EntityManager.LocateEntity<Hotel>(EntityLocator.ConstructKeyFromPkItems(typeof(Hotel), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.HotelIdSource = tmpEntity;
				else
					entity.HotelIdSource = DataRepository.HotelProvider.GetById(transactionManager, (entity.HotelId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HotelIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.HotelIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.HotelProvider.DeepLoad(transactionManager, entity.HotelIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion HotelIdSource

			#region MeetingRoomIdSource	
			if (CanDeepLoad(entity, "MeetingRoom|MeetingRoomIdSource", deepLoadType, innerList) 
				&& entity.MeetingRoomIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.MeetingRoomId;
				MeetingRoom tmpEntity = EntityManager.LocateEntity<MeetingRoom>(EntityLocator.ConstructKeyFromPkItems(typeof(MeetingRoom), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.MeetingRoomIdSource = tmpEntity;
				else
					entity.MeetingRoomIdSource = DataRepository.MeetingRoomProvider.GetById(transactionManager, entity.MeetingRoomId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MeetingRoomIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.MeetingRoomIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.MeetingRoomProvider.DeepLoad(transactionManager, entity.MeetingRoomIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion MeetingRoomIdSource

			#region UpdatedBySource	
			if (CanDeepLoad(entity, "Users|UpdatedBySource", deepLoadType, innerList) 
				&& entity.UpdatedBySource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.UpdatedBy ?? (long)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.UpdatedBySource = tmpEntity;
				else
					entity.UpdatedBySource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.UpdatedBy ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UpdatedBySource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.UpdatedBySource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.UpdatedBySource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion UpdatedBySource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region BookedMeetingRoomCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<BookedMeetingRoom>|BookedMeetingRoomCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BookedMeetingRoomCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BookedMeetingRoomCollection = DataRepository.BookedMeetingRoomProvider.GetByMeetingRoomConfigId(transactionManager, entity.Id);

				if (deep && entity.BookedMeetingRoomCollection.Count > 0)
				{
					deepHandles.Add("BookedMeetingRoomCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<BookedMeetingRoom>) DataRepository.BookedMeetingRoomProvider.DeepLoad,
						new object[] { transactionManager, entity.BookedMeetingRoomCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region MeetingRoomConfigTrailCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<MeetingRoomConfigTrail>|MeetingRoomConfigTrailCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MeetingRoomConfigTrailCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.MeetingRoomConfigTrailCollection = DataRepository.MeetingRoomConfigTrailProvider.GetByMeetingRoomId(transactionManager, entity.Id);

				if (deep && entity.MeetingRoomConfigTrailCollection.Count > 0)
				{
					deepHandles.Add("MeetingRoomConfigTrailCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<MeetingRoomConfigTrail>) DataRepository.MeetingRoomConfigTrailProvider.DeepLoad,
						new object[] { transactionManager, entity.MeetingRoomConfigTrailCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.MeetingRoomConfig object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.MeetingRoomConfig instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.MeetingRoomConfig Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.MeetingRoomConfig entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region HotelIdSource
			if (CanDeepSave(entity, "Hotel|HotelIdSource", deepSaveType, innerList) 
				&& entity.HotelIdSource != null)
			{
				DataRepository.HotelProvider.Save(transactionManager, entity.HotelIdSource);
				entity.HotelId = entity.HotelIdSource.Id;
			}
			#endregion 
			
			#region MeetingRoomIdSource
			if (CanDeepSave(entity, "MeetingRoom|MeetingRoomIdSource", deepSaveType, innerList) 
				&& entity.MeetingRoomIdSource != null)
			{
				DataRepository.MeetingRoomProvider.Save(transactionManager, entity.MeetingRoomIdSource);
				entity.MeetingRoomId = entity.MeetingRoomIdSource.Id;
			}
			#endregion 
			
			#region UpdatedBySource
			if (CanDeepSave(entity, "Users|UpdatedBySource", deepSaveType, innerList) 
				&& entity.UpdatedBySource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.UpdatedBySource);
				entity.UpdatedBy = entity.UpdatedBySource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<BookedMeetingRoom>
				if (CanDeepSave(entity.BookedMeetingRoomCollection, "List<BookedMeetingRoom>|BookedMeetingRoomCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(BookedMeetingRoom child in entity.BookedMeetingRoomCollection)
					{
						if(child.MeetingRoomConfigIdSource != null)
						{
							child.MeetingRoomConfigId = child.MeetingRoomConfigIdSource.Id;
						}
						else
						{
							child.MeetingRoomConfigId = entity.Id;
						}

					}

					if (entity.BookedMeetingRoomCollection.Count > 0 || entity.BookedMeetingRoomCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BookedMeetingRoomProvider.Save(transactionManager, entity.BookedMeetingRoomCollection);
						
						deepHandles.Add("BookedMeetingRoomCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< BookedMeetingRoom >) DataRepository.BookedMeetingRoomProvider.DeepSave,
							new object[] { transactionManager, entity.BookedMeetingRoomCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<MeetingRoomConfigTrail>
				if (CanDeepSave(entity.MeetingRoomConfigTrailCollection, "List<MeetingRoomConfigTrail>|MeetingRoomConfigTrailCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(MeetingRoomConfigTrail child in entity.MeetingRoomConfigTrailCollection)
					{
						if(child.MeetingRoomIdSource != null)
						{
							child.MeetingRoomId = child.MeetingRoomIdSource.Id;
						}
						else
						{
							child.MeetingRoomId = entity.Id;
						}

					}

					if (entity.MeetingRoomConfigTrailCollection.Count > 0 || entity.MeetingRoomConfigTrailCollection.DeletedItems.Count > 0)
					{
						//DataRepository.MeetingRoomConfigTrailProvider.Save(transactionManager, entity.MeetingRoomConfigTrailCollection);
						
						deepHandles.Add("MeetingRoomConfigTrailCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< MeetingRoomConfigTrail >) DataRepository.MeetingRoomConfigTrailProvider.DeepSave,
							new object[] { transactionManager, entity.MeetingRoomConfigTrailCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region MeetingRoomConfigChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.MeetingRoomConfig</c>
	///</summary>
	public enum MeetingRoomConfigChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Hotel</c> at HotelIdSource
		///</summary>
		[ChildEntityType(typeof(Hotel))]
		Hotel,
			
		///<summary>
		/// Composite Property for <c>MeetingRoom</c> at MeetingRoomIdSource
		///</summary>
		[ChildEntityType(typeof(MeetingRoom))]
		MeetingRoom,
			
		///<summary>
		/// Composite Property for <c>Users</c> at UpdatedBySource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
	
		///<summary>
		/// Collection of <c>MeetingRoomConfig</c> as OneToMany for BookedMeetingRoomCollection
		///</summary>
		[ChildEntityType(typeof(TList<BookedMeetingRoom>))]
		BookedMeetingRoomCollection,

		///<summary>
		/// Collection of <c>MeetingRoomConfig</c> as OneToMany for MeetingRoomConfigTrailCollection
		///</summary>
		[ChildEntityType(typeof(TList<MeetingRoomConfigTrail>))]
		MeetingRoomConfigTrailCollection,
	}
	
	#endregion MeetingRoomConfigChildEntityTypes
	
	#region MeetingRoomConfigFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;MeetingRoomConfigColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoomConfig"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomConfigFilterBuilder : SqlFilterBuilder<MeetingRoomConfigColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigFilterBuilder class.
		/// </summary>
		public MeetingRoomConfigFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomConfigFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomConfigFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomConfigFilterBuilder
	
	#region MeetingRoomConfigParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;MeetingRoomConfigColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoomConfig"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomConfigParameterBuilder : ParameterizedSqlFilterBuilder<MeetingRoomConfigColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigParameterBuilder class.
		/// </summary>
		public MeetingRoomConfigParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomConfigParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomConfigParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomConfigParameterBuilder
	
	#region MeetingRoomConfigSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;MeetingRoomConfigColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoomConfig"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class MeetingRoomConfigSortBuilder : SqlSortBuilder<MeetingRoomConfigColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomConfigSqlSortBuilder class.
		/// </summary>
		public MeetingRoomConfigSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion MeetingRoomConfigSortBuilder
	
} // end namespace
