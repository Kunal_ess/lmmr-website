﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="FrontEndBottomProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class FrontEndBottomProviderBaseCore : EntityProviderBase<LMMR.Entities.FrontEndBottom, LMMR.Entities.FrontEndBottomKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.FrontEndBottomKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottom_City key.
		///		FK_FrontEndBottom_City Description: 
		/// </summary>
		/// <param name="_cityId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottom objects.</returns>
		public TList<FrontEndBottom> GetByCityId(System.Int64 _cityId)
		{
			int count = -1;
			return GetByCityId(_cityId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottom_City key.
		///		FK_FrontEndBottom_City Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cityId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottom objects.</returns>
		/// <remarks></remarks>
		public TList<FrontEndBottom> GetByCityId(TransactionManager transactionManager, System.Int64 _cityId)
		{
			int count = -1;
			return GetByCityId(transactionManager, _cityId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottom_City key.
		///		FK_FrontEndBottom_City Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cityId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottom objects.</returns>
		public TList<FrontEndBottom> GetByCityId(TransactionManager transactionManager, System.Int64 _cityId, int start, int pageLength)
		{
			int count = -1;
			return GetByCityId(transactionManager, _cityId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottom_City key.
		///		fkFrontEndBottomCity Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_cityId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottom objects.</returns>
		public TList<FrontEndBottom> GetByCityId(System.Int64 _cityId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCityId(null, _cityId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottom_City key.
		///		fkFrontEndBottomCity Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_cityId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottom objects.</returns>
		public TList<FrontEndBottom> GetByCityId(System.Int64 _cityId, int start, int pageLength,out int count)
		{
			return GetByCityId(null, _cityId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottom_City key.
		///		FK_FrontEndBottom_City Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cityId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottom objects.</returns>
		public abstract TList<FrontEndBottom> GetByCityId(TransactionManager transactionManager, System.Int64 _cityId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottom_Country key.
		///		FK_FrontEndBottom_Country Description: 
		/// </summary>
		/// <param name="_countryId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottom objects.</returns>
		public TList<FrontEndBottom> GetByCountryId(System.Int64 _countryId)
		{
			int count = -1;
			return GetByCountryId(_countryId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottom_Country key.
		///		FK_FrontEndBottom_Country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottom objects.</returns>
		/// <remarks></remarks>
		public TList<FrontEndBottom> GetByCountryId(TransactionManager transactionManager, System.Int64 _countryId)
		{
			int count = -1;
			return GetByCountryId(transactionManager, _countryId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottom_Country key.
		///		FK_FrontEndBottom_Country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottom objects.</returns>
		public TList<FrontEndBottom> GetByCountryId(TransactionManager transactionManager, System.Int64 _countryId, int start, int pageLength)
		{
			int count = -1;
			return GetByCountryId(transactionManager, _countryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottom_Country key.
		///		fkFrontEndBottomCountry Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_countryId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottom objects.</returns>
		public TList<FrontEndBottom> GetByCountryId(System.Int64 _countryId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCountryId(null, _countryId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottom_Country key.
		///		fkFrontEndBottomCountry Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_countryId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottom objects.</returns>
		public TList<FrontEndBottom> GetByCountryId(System.Int64 _countryId, int start, int pageLength,out int count)
		{
			return GetByCountryId(null, _countryId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottom_Country key.
		///		FK_FrontEndBottom_Country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottom objects.</returns>
		public abstract TList<FrontEndBottom> GetByCountryId(TransactionManager transactionManager, System.Int64 _countryId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottom_Zone key.
		///		FK_FrontEndBottom_Zone Description: 
		/// </summary>
		/// <param name="_zoneId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottom objects.</returns>
		public TList<FrontEndBottom> GetByZoneId(System.Int64 _zoneId)
		{
			int count = -1;
			return GetByZoneId(_zoneId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottom_Zone key.
		///		FK_FrontEndBottom_Zone Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_zoneId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottom objects.</returns>
		/// <remarks></remarks>
		public TList<FrontEndBottom> GetByZoneId(TransactionManager transactionManager, System.Int64 _zoneId)
		{
			int count = -1;
			return GetByZoneId(transactionManager, _zoneId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottom_Zone key.
		///		FK_FrontEndBottom_Zone Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_zoneId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottom objects.</returns>
		public TList<FrontEndBottom> GetByZoneId(TransactionManager transactionManager, System.Int64 _zoneId, int start, int pageLength)
		{
			int count = -1;
			return GetByZoneId(transactionManager, _zoneId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottom_Zone key.
		///		fkFrontEndBottomZone Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_zoneId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottom objects.</returns>
		public TList<FrontEndBottom> GetByZoneId(System.Int64 _zoneId, int start, int pageLength)
		{
			int count =  -1;
			return GetByZoneId(null, _zoneId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottom_Zone key.
		///		fkFrontEndBottomZone Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_zoneId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottom objects.</returns>
		public TList<FrontEndBottom> GetByZoneId(System.Int64 _zoneId, int start, int pageLength,out int count)
		{
			return GetByZoneId(null, _zoneId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottom_Zone key.
		///		FK_FrontEndBottom_Zone Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_zoneId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottom objects.</returns>
		public abstract TList<FrontEndBottom> GetByZoneId(TransactionManager transactionManager, System.Int64 _zoneId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.FrontEndBottom Get(TransactionManager transactionManager, LMMR.Entities.FrontEndBottomKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_FrontEndBottom index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.FrontEndBottom"/> class.</returns>
		public LMMR.Entities.FrontEndBottom GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FrontEndBottom index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.FrontEndBottom"/> class.</returns>
		public LMMR.Entities.FrontEndBottom GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FrontEndBottom index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.FrontEndBottom"/> class.</returns>
		public LMMR.Entities.FrontEndBottom GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FrontEndBottom index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.FrontEndBottom"/> class.</returns>
		public LMMR.Entities.FrontEndBottom GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FrontEndBottom index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.FrontEndBottom"/> class.</returns>
		public LMMR.Entities.FrontEndBottom GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FrontEndBottom index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.FrontEndBottom"/> class.</returns>
		public abstract LMMR.Entities.FrontEndBottom GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;FrontEndBottom&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;FrontEndBottom&gt;"/></returns>
		public static TList<FrontEndBottom> Fill(IDataReader reader, TList<FrontEndBottom> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.FrontEndBottom c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("FrontEndBottom")
					.Append("|").Append((System.Int64)reader[((int)FrontEndBottomColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<FrontEndBottom>(
					key.ToString(), // EntityTrackingKey
					"FrontEndBottom",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.FrontEndBottom();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)FrontEndBottomColumn.Id - 1)];
					c.CountryId = (System.Int64)reader[((int)FrontEndBottomColumn.CountryId - 1)];
					c.CityId = (System.Int64)reader[((int)FrontEndBottomColumn.CityId - 1)];
					c.ZoneId = (System.Int64)reader[((int)FrontEndBottomColumn.ZoneId - 1)];
					c.IsActive = (System.Boolean)reader[((int)FrontEndBottomColumn.IsActive - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.FrontEndBottom"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.FrontEndBottom"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.FrontEndBottom entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)FrontEndBottomColumn.Id - 1)];
			entity.CountryId = (System.Int64)reader[((int)FrontEndBottomColumn.CountryId - 1)];
			entity.CityId = (System.Int64)reader[((int)FrontEndBottomColumn.CityId - 1)];
			entity.ZoneId = (System.Int64)reader[((int)FrontEndBottomColumn.ZoneId - 1)];
			entity.IsActive = (System.Boolean)reader[((int)FrontEndBottomColumn.IsActive - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.FrontEndBottom"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.FrontEndBottom"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.FrontEndBottom entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.CountryId = (System.Int64)dataRow["CountryId"];
			entity.CityId = (System.Int64)dataRow["CityId"];
			entity.ZoneId = (System.Int64)dataRow["ZoneId"];
			entity.IsActive = (System.Boolean)dataRow["IsActive"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.FrontEndBottom"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.FrontEndBottom Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.FrontEndBottom entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CityIdSource	
			if (CanDeepLoad(entity, "City|CityIdSource", deepLoadType, innerList) 
				&& entity.CityIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CityId;
				City tmpEntity = EntityManager.LocateEntity<City>(EntityLocator.ConstructKeyFromPkItems(typeof(City), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CityIdSource = tmpEntity;
				else
					entity.CityIdSource = DataRepository.CityProvider.GetById(transactionManager, entity.CityId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CityIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CityIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CityProvider.DeepLoad(transactionManager, entity.CityIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CityIdSource

			#region CountryIdSource	
			if (CanDeepLoad(entity, "Country|CountryIdSource", deepLoadType, innerList) 
				&& entity.CountryIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CountryId;
				Country tmpEntity = EntityManager.LocateEntity<Country>(EntityLocator.ConstructKeyFromPkItems(typeof(Country), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CountryIdSource = tmpEntity;
				else
					entity.CountryIdSource = DataRepository.CountryProvider.GetById(transactionManager, entity.CountryId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CountryIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CountryIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CountryProvider.DeepLoad(transactionManager, entity.CountryIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CountryIdSource

			#region ZoneIdSource	
			if (CanDeepLoad(entity, "Zone|ZoneIdSource", deepLoadType, innerList) 
				&& entity.ZoneIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ZoneId;
				Zone tmpEntity = EntityManager.LocateEntity<Zone>(EntityLocator.ConstructKeyFromPkItems(typeof(Zone), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ZoneIdSource = tmpEntity;
				else
					entity.ZoneIdSource = DataRepository.ZoneProvider.GetById(transactionManager, entity.ZoneId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ZoneIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ZoneIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ZoneProvider.DeepLoad(transactionManager, entity.ZoneIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ZoneIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region FrontEndBottomDescriptionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<FrontEndBottomDescription>|FrontEndBottomDescriptionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FrontEndBottomDescriptionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.FrontEndBottomDescriptionCollection = DataRepository.FrontEndBottomDescriptionProvider.GetByFrontEndId(transactionManager, entity.Id);

				if (deep && entity.FrontEndBottomDescriptionCollection.Count > 0)
				{
					deepHandles.Add("FrontEndBottomDescriptionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<FrontEndBottomDescription>) DataRepository.FrontEndBottomDescriptionProvider.DeepLoad,
						new object[] { transactionManager, entity.FrontEndBottomDescriptionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.FrontEndBottom object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.FrontEndBottom instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.FrontEndBottom Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.FrontEndBottom entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CityIdSource
			if (CanDeepSave(entity, "City|CityIdSource", deepSaveType, innerList) 
				&& entity.CityIdSource != null)
			{
				DataRepository.CityProvider.Save(transactionManager, entity.CityIdSource);
				entity.CityId = entity.CityIdSource.Id;
			}
			#endregion 
			
			#region CountryIdSource
			if (CanDeepSave(entity, "Country|CountryIdSource", deepSaveType, innerList) 
				&& entity.CountryIdSource != null)
			{
				DataRepository.CountryProvider.Save(transactionManager, entity.CountryIdSource);
				entity.CountryId = entity.CountryIdSource.Id;
			}
			#endregion 
			
			#region ZoneIdSource
			if (CanDeepSave(entity, "Zone|ZoneIdSource", deepSaveType, innerList) 
				&& entity.ZoneIdSource != null)
			{
				DataRepository.ZoneProvider.Save(transactionManager, entity.ZoneIdSource);
				entity.ZoneId = entity.ZoneIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<FrontEndBottomDescription>
				if (CanDeepSave(entity.FrontEndBottomDescriptionCollection, "List<FrontEndBottomDescription>|FrontEndBottomDescriptionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(FrontEndBottomDescription child in entity.FrontEndBottomDescriptionCollection)
					{
						if(child.FrontEndIdSource != null)
						{
							child.FrontEndId = child.FrontEndIdSource.Id;
						}
						else
						{
							child.FrontEndId = entity.Id;
						}

					}

					if (entity.FrontEndBottomDescriptionCollection.Count > 0 || entity.FrontEndBottomDescriptionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.FrontEndBottomDescriptionProvider.Save(transactionManager, entity.FrontEndBottomDescriptionCollection);
						
						deepHandles.Add("FrontEndBottomDescriptionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< FrontEndBottomDescription >) DataRepository.FrontEndBottomDescriptionProvider.DeepSave,
							new object[] { transactionManager, entity.FrontEndBottomDescriptionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region FrontEndBottomChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.FrontEndBottom</c>
	///</summary>
	public enum FrontEndBottomChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>City</c> at CityIdSource
		///</summary>
		[ChildEntityType(typeof(City))]
		City,
			
		///<summary>
		/// Composite Property for <c>Country</c> at CountryIdSource
		///</summary>
		[ChildEntityType(typeof(Country))]
		Country,
			
		///<summary>
		/// Composite Property for <c>Zone</c> at ZoneIdSource
		///</summary>
		[ChildEntityType(typeof(Zone))]
		Zone,
	
		///<summary>
		/// Collection of <c>FrontEndBottom</c> as OneToMany for FrontEndBottomDescriptionCollection
		///</summary>
		[ChildEntityType(typeof(TList<FrontEndBottomDescription>))]
		FrontEndBottomDescriptionCollection,
	}
	
	#endregion FrontEndBottomChildEntityTypes
	
	#region FrontEndBottomFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;FrontEndBottomColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FrontEndBottom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FrontEndBottomFilterBuilder : SqlFilterBuilder<FrontEndBottomColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomFilterBuilder class.
		/// </summary>
		public FrontEndBottomFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FrontEndBottomFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FrontEndBottomFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FrontEndBottomFilterBuilder
	
	#region FrontEndBottomParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;FrontEndBottomColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FrontEndBottom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FrontEndBottomParameterBuilder : ParameterizedSqlFilterBuilder<FrontEndBottomColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomParameterBuilder class.
		/// </summary>
		public FrontEndBottomParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FrontEndBottomParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FrontEndBottomParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FrontEndBottomParameterBuilder
	
	#region FrontEndBottomSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;FrontEndBottomColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FrontEndBottom"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class FrontEndBottomSortBuilder : SqlSortBuilder<FrontEndBottomColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomSqlSortBuilder class.
		/// </summary>
		public FrontEndBottomSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion FrontEndBottomSortBuilder
	
} // end namespace
