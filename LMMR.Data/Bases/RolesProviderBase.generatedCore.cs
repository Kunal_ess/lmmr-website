﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="RolesProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class RolesProviderBaseCore : EntityProviderBase<LMMR.Entities.Roles, LMMR.Entities.RolesKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.RolesKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.Roles Get(TransactionManager transactionManager, LMMR.Entities.RolesKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Roles index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Roles"/> class.</returns>
		public LMMR.Entities.Roles GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Roles index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Roles"/> class.</returns>
		public LMMR.Entities.Roles GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Roles index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Roles"/> class.</returns>
		public LMMR.Entities.Roles GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Roles index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Roles"/> class.</returns>
		public LMMR.Entities.Roles GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Roles index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Roles"/> class.</returns>
		public LMMR.Entities.Roles GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Roles index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Roles"/> class.</returns>
		public abstract LMMR.Entities.Roles GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Roles&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Roles&gt;"/></returns>
		public static TList<Roles> Fill(IDataReader reader, TList<Roles> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.Roles c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Roles")
					.Append("|").Append((System.Int64)reader[((int)RolesColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Roles>(
					key.ToString(), // EntityTrackingKey
					"Roles",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.Roles();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)RolesColumn.Id - 1)];
					c.RoleName = (reader.IsDBNull(((int)RolesColumn.RoleName - 1)))?null:(System.String)reader[((int)RolesColumn.RoleName - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Roles"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Roles"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.Roles entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)RolesColumn.Id - 1)];
			entity.RoleName = (reader.IsDBNull(((int)RolesColumn.RoleName - 1)))?null:(System.String)reader[((int)RolesColumn.RoleName - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Roles"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Roles"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.Roles entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.RoleName = Convert.IsDBNull(dataRow["RoleName"]) ? null : (System.String)dataRow["RoleName"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Roles"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.Roles Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.Roles entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region StaffAccountCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<StaffAccount>|StaffAccountCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'StaffAccountCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.StaffAccountCollection = DataRepository.StaffAccountProvider.GetByRole(transactionManager, entity.Id);

				if (deep && entity.StaffAccountCollection.Count > 0)
				{
					deepHandles.Add("StaffAccountCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<StaffAccount>) DataRepository.StaffAccountProvider.DeepLoad,
						new object[] { transactionManager, entity.StaffAccountCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PermissionsCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Permissions>|PermissionsCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PermissionsCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PermissionsCollection = DataRepository.PermissionsProvider.GetByRoleId(transactionManager, entity.Id);

				if (deep && entity.PermissionsCollection.Count > 0)
				{
					deepHandles.Add("PermissionsCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Permissions>) DataRepository.PermissionsProvider.DeepLoad,
						new object[] { transactionManager, entity.PermissionsCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.Roles object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.Roles instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.Roles Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.Roles entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<StaffAccount>
				if (CanDeepSave(entity.StaffAccountCollection, "List<StaffAccount>|StaffAccountCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(StaffAccount child in entity.StaffAccountCollection)
					{
						if(child.RoleSource != null)
						{
							child.Role = child.RoleSource.Id;
						}
						else
						{
							child.Role = entity.Id;
						}

					}

					if (entity.StaffAccountCollection.Count > 0 || entity.StaffAccountCollection.DeletedItems.Count > 0)
					{
						//DataRepository.StaffAccountProvider.Save(transactionManager, entity.StaffAccountCollection);
						
						deepHandles.Add("StaffAccountCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< StaffAccount >) DataRepository.StaffAccountProvider.DeepSave,
							new object[] { transactionManager, entity.StaffAccountCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Permissions>
				if (CanDeepSave(entity.PermissionsCollection, "List<Permissions>|PermissionsCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Permissions child in entity.PermissionsCollection)
					{
						if(child.RoleIdSource != null)
						{
							child.RoleId = child.RoleIdSource.Id;
						}
						else
						{
							child.RoleId = entity.Id;
						}

					}

					if (entity.PermissionsCollection.Count > 0 || entity.PermissionsCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PermissionsProvider.Save(transactionManager, entity.PermissionsCollection);
						
						deepHandles.Add("PermissionsCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Permissions >) DataRepository.PermissionsProvider.DeepSave,
							new object[] { transactionManager, entity.PermissionsCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region RolesChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.Roles</c>
	///</summary>
	public enum RolesChildEntityTypes
	{

		///<summary>
		/// Collection of <c>Roles</c> as OneToMany for StaffAccountCollection
		///</summary>
		[ChildEntityType(typeof(TList<StaffAccount>))]
		StaffAccountCollection,

		///<summary>
		/// Collection of <c>Roles</c> as OneToMany for PermissionsCollection
		///</summary>
		[ChildEntityType(typeof(TList<Permissions>))]
		PermissionsCollection,
	}
	
	#endregion RolesChildEntityTypes
	
	#region RolesFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;RolesColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Roles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RolesFilterBuilder : SqlFilterBuilder<RolesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RolesFilterBuilder class.
		/// </summary>
		public RolesFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the RolesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RolesFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RolesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RolesFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RolesFilterBuilder
	
	#region RolesParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;RolesColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Roles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RolesParameterBuilder : ParameterizedSqlFilterBuilder<RolesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RolesParameterBuilder class.
		/// </summary>
		public RolesParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the RolesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RolesParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RolesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RolesParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RolesParameterBuilder
	
	#region RolesSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;RolesColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Roles"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class RolesSortBuilder : SqlSortBuilder<RolesColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RolesSqlSortBuilder class.
		/// </summary>
		public RolesSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion RolesSortBuilder
	
} // end namespace
