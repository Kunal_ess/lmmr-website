﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="MeetingRoomPictureVideoTrailProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class MeetingRoomPictureVideoTrailProviderBaseCore : EntityProviderBase<LMMR.Entities.MeetingRoomPictureVideoTrail, LMMR.Entities.MeetingRoomPictureVideoTrailKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.MeetingRoomPictureVideoTrailKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomPictureVideo_Trail_MeetingRoomPictureVideo key.
		///		FK_MeetingRoomPictureVideo_Trail_MeetingRoomPictureVideo Description: 
		/// </summary>
		/// <param name="_meetingRoomPicId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomPictureVideoTrail objects.</returns>
		public TList<MeetingRoomPictureVideoTrail> GetByMeetingRoomPicId(System.Int64? _meetingRoomPicId)
		{
			int count = -1;
			return GetByMeetingRoomPicId(_meetingRoomPicId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomPictureVideo_Trail_MeetingRoomPictureVideo key.
		///		FK_MeetingRoomPictureVideo_Trail_MeetingRoomPictureVideo Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_meetingRoomPicId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomPictureVideoTrail objects.</returns>
		/// <remarks></remarks>
		public TList<MeetingRoomPictureVideoTrail> GetByMeetingRoomPicId(TransactionManager transactionManager, System.Int64? _meetingRoomPicId)
		{
			int count = -1;
			return GetByMeetingRoomPicId(transactionManager, _meetingRoomPicId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomPictureVideo_Trail_MeetingRoomPictureVideo key.
		///		FK_MeetingRoomPictureVideo_Trail_MeetingRoomPictureVideo Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_meetingRoomPicId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomPictureVideoTrail objects.</returns>
		public TList<MeetingRoomPictureVideoTrail> GetByMeetingRoomPicId(TransactionManager transactionManager, System.Int64? _meetingRoomPicId, int start, int pageLength)
		{
			int count = -1;
			return GetByMeetingRoomPicId(transactionManager, _meetingRoomPicId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomPictureVideo_Trail_MeetingRoomPictureVideo key.
		///		fkMeetingRoomPictureVideoTrailMeetingRoomPictureVideo Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_meetingRoomPicId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomPictureVideoTrail objects.</returns>
		public TList<MeetingRoomPictureVideoTrail> GetByMeetingRoomPicId(System.Int64? _meetingRoomPicId, int start, int pageLength)
		{
			int count =  -1;
			return GetByMeetingRoomPicId(null, _meetingRoomPicId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomPictureVideo_Trail_MeetingRoomPictureVideo key.
		///		fkMeetingRoomPictureVideoTrailMeetingRoomPictureVideo Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_meetingRoomPicId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomPictureVideoTrail objects.</returns>
		public TList<MeetingRoomPictureVideoTrail> GetByMeetingRoomPicId(System.Int64? _meetingRoomPicId, int start, int pageLength,out int count)
		{
			return GetByMeetingRoomPicId(null, _meetingRoomPicId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoomPictureVideo_Trail_MeetingRoomPictureVideo key.
		///		FK_MeetingRoomPictureVideo_Trail_MeetingRoomPictureVideo Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_meetingRoomPicId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoomPictureVideoTrail objects.</returns>
		public abstract TList<MeetingRoomPictureVideoTrail> GetByMeetingRoomPicId(TransactionManager transactionManager, System.Int64? _meetingRoomPicId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.MeetingRoomPictureVideoTrail Get(TransactionManager transactionManager, LMMR.Entities.MeetingRoomPictureVideoTrailKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_MeetingRoomPictureVideo_Trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomPictureVideoTrail"/> class.</returns>
		public LMMR.Entities.MeetingRoomPictureVideoTrail GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoomPictureVideo_Trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomPictureVideoTrail"/> class.</returns>
		public LMMR.Entities.MeetingRoomPictureVideoTrail GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoomPictureVideo_Trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomPictureVideoTrail"/> class.</returns>
		public LMMR.Entities.MeetingRoomPictureVideoTrail GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoomPictureVideo_Trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomPictureVideoTrail"/> class.</returns>
		public LMMR.Entities.MeetingRoomPictureVideoTrail GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoomPictureVideo_Trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomPictureVideoTrail"/> class.</returns>
		public LMMR.Entities.MeetingRoomPictureVideoTrail GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoomPictureVideo_Trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoomPictureVideoTrail"/> class.</returns>
		public abstract LMMR.Entities.MeetingRoomPictureVideoTrail GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;MeetingRoomPictureVideoTrail&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;MeetingRoomPictureVideoTrail&gt;"/></returns>
		public static TList<MeetingRoomPictureVideoTrail> Fill(IDataReader reader, TList<MeetingRoomPictureVideoTrail> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.MeetingRoomPictureVideoTrail c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("MeetingRoomPictureVideoTrail")
					.Append("|").Append((System.Int64)reader[((int)MeetingRoomPictureVideoTrailColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<MeetingRoomPictureVideoTrail>(
					key.ToString(), // EntityTrackingKey
					"MeetingRoomPictureVideoTrail",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.MeetingRoomPictureVideoTrail();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)MeetingRoomPictureVideoTrailColumn.Id - 1)];
					c.MeetingRoomPicId = (reader.IsDBNull(((int)MeetingRoomPictureVideoTrailColumn.MeetingRoomPicId - 1)))?null:(System.Int64?)reader[((int)MeetingRoomPictureVideoTrailColumn.MeetingRoomPicId - 1)];
					c.MeetingRoomId = (System.Int64)reader[((int)MeetingRoomPictureVideoTrailColumn.MeetingRoomId - 1)];
					c.FileType = (reader.IsDBNull(((int)MeetingRoomPictureVideoTrailColumn.FileType - 1)))?null:(System.Int32?)reader[((int)MeetingRoomPictureVideoTrailColumn.FileType - 1)];
					c.ImageName = (reader.IsDBNull(((int)MeetingRoomPictureVideoTrailColumn.ImageName - 1)))?null:(System.String)reader[((int)MeetingRoomPictureVideoTrailColumn.ImageName - 1)];
					c.VideoName = (reader.IsDBNull(((int)MeetingRoomPictureVideoTrailColumn.VideoName - 1)))?null:(System.String)reader[((int)MeetingRoomPictureVideoTrailColumn.VideoName - 1)];
					c.FileName = (reader.IsDBNull(((int)MeetingRoomPictureVideoTrailColumn.FileName - 1)))?null:(System.String)reader[((int)MeetingRoomPictureVideoTrailColumn.FileName - 1)];
					c.AlterText = (reader.IsDBNull(((int)MeetingRoomPictureVideoTrailColumn.AlterText - 1)))?null:(System.String)reader[((int)MeetingRoomPictureVideoTrailColumn.AlterText - 1)];
					c.IsMain = (System.Boolean)reader[((int)MeetingRoomPictureVideoTrailColumn.IsMain - 1)];
					c.UpdatedBy = (reader.IsDBNull(((int)MeetingRoomPictureVideoTrailColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)MeetingRoomPictureVideoTrailColumn.UpdatedBy - 1)];
					c.UpdateDate = (reader.IsDBNull(((int)MeetingRoomPictureVideoTrailColumn.UpdateDate - 1)))?null:(System.DateTime?)reader[((int)MeetingRoomPictureVideoTrailColumn.UpdateDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.MeetingRoomPictureVideoTrail"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.MeetingRoomPictureVideoTrail"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.MeetingRoomPictureVideoTrail entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)MeetingRoomPictureVideoTrailColumn.Id - 1)];
			entity.MeetingRoomPicId = (reader.IsDBNull(((int)MeetingRoomPictureVideoTrailColumn.MeetingRoomPicId - 1)))?null:(System.Int64?)reader[((int)MeetingRoomPictureVideoTrailColumn.MeetingRoomPicId - 1)];
			entity.MeetingRoomId = (System.Int64)reader[((int)MeetingRoomPictureVideoTrailColumn.MeetingRoomId - 1)];
			entity.FileType = (reader.IsDBNull(((int)MeetingRoomPictureVideoTrailColumn.FileType - 1)))?null:(System.Int32?)reader[((int)MeetingRoomPictureVideoTrailColumn.FileType - 1)];
			entity.ImageName = (reader.IsDBNull(((int)MeetingRoomPictureVideoTrailColumn.ImageName - 1)))?null:(System.String)reader[((int)MeetingRoomPictureVideoTrailColumn.ImageName - 1)];
			entity.VideoName = (reader.IsDBNull(((int)MeetingRoomPictureVideoTrailColumn.VideoName - 1)))?null:(System.String)reader[((int)MeetingRoomPictureVideoTrailColumn.VideoName - 1)];
			entity.FileName = (reader.IsDBNull(((int)MeetingRoomPictureVideoTrailColumn.FileName - 1)))?null:(System.String)reader[((int)MeetingRoomPictureVideoTrailColumn.FileName - 1)];
			entity.AlterText = (reader.IsDBNull(((int)MeetingRoomPictureVideoTrailColumn.AlterText - 1)))?null:(System.String)reader[((int)MeetingRoomPictureVideoTrailColumn.AlterText - 1)];
			entity.IsMain = (System.Boolean)reader[((int)MeetingRoomPictureVideoTrailColumn.IsMain - 1)];
			entity.UpdatedBy = (reader.IsDBNull(((int)MeetingRoomPictureVideoTrailColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)MeetingRoomPictureVideoTrailColumn.UpdatedBy - 1)];
			entity.UpdateDate = (reader.IsDBNull(((int)MeetingRoomPictureVideoTrailColumn.UpdateDate - 1)))?null:(System.DateTime?)reader[((int)MeetingRoomPictureVideoTrailColumn.UpdateDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.MeetingRoomPictureVideoTrail"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.MeetingRoomPictureVideoTrail"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.MeetingRoomPictureVideoTrail entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.MeetingRoomPicId = Convert.IsDBNull(dataRow["MeetingRoomPicId"]) ? null : (System.Int64?)dataRow["MeetingRoomPicId"];
			entity.MeetingRoomId = (System.Int64)dataRow["MeetingRoom_Id"];
			entity.FileType = Convert.IsDBNull(dataRow["FileType"]) ? null : (System.Int32?)dataRow["FileType"];
			entity.ImageName = Convert.IsDBNull(dataRow["ImageName"]) ? null : (System.String)dataRow["ImageName"];
			entity.VideoName = Convert.IsDBNull(dataRow["VideoName"]) ? null : (System.String)dataRow["VideoName"];
			entity.FileName = Convert.IsDBNull(dataRow["FileName"]) ? null : (System.String)dataRow["FileName"];
			entity.AlterText = Convert.IsDBNull(dataRow["AlterText"]) ? null : (System.String)dataRow["AlterText"];
			entity.IsMain = (System.Boolean)dataRow["IsMain"];
			entity.UpdatedBy = Convert.IsDBNull(dataRow["UpdatedBy"]) ? null : (System.Int64?)dataRow["UpdatedBy"];
			entity.UpdateDate = Convert.IsDBNull(dataRow["UpdateDate"]) ? null : (System.DateTime?)dataRow["UpdateDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.MeetingRoomPictureVideoTrail"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.MeetingRoomPictureVideoTrail Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.MeetingRoomPictureVideoTrail entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region MeetingRoomPicIdSource	
			if (CanDeepLoad(entity, "MeetingRoomPictureVideo|MeetingRoomPicIdSource", deepLoadType, innerList) 
				&& entity.MeetingRoomPicIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.MeetingRoomPicId ?? (long)0);
				MeetingRoomPictureVideo tmpEntity = EntityManager.LocateEntity<MeetingRoomPictureVideo>(EntityLocator.ConstructKeyFromPkItems(typeof(MeetingRoomPictureVideo), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.MeetingRoomPicIdSource = tmpEntity;
				else
					entity.MeetingRoomPicIdSource = DataRepository.MeetingRoomPictureVideoProvider.GetById(transactionManager, (entity.MeetingRoomPicId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MeetingRoomPicIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.MeetingRoomPicIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.MeetingRoomPictureVideoProvider.DeepLoad(transactionManager, entity.MeetingRoomPicIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion MeetingRoomPicIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.MeetingRoomPictureVideoTrail object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.MeetingRoomPictureVideoTrail instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.MeetingRoomPictureVideoTrail Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.MeetingRoomPictureVideoTrail entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region MeetingRoomPicIdSource
			if (CanDeepSave(entity, "MeetingRoomPictureVideo|MeetingRoomPicIdSource", deepSaveType, innerList) 
				&& entity.MeetingRoomPicIdSource != null)
			{
				DataRepository.MeetingRoomPictureVideoProvider.Save(transactionManager, entity.MeetingRoomPicIdSource);
				entity.MeetingRoomPicId = entity.MeetingRoomPicIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region MeetingRoomPictureVideoTrailChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.MeetingRoomPictureVideoTrail</c>
	///</summary>
	public enum MeetingRoomPictureVideoTrailChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>MeetingRoomPictureVideo</c> at MeetingRoomPicIdSource
		///</summary>
		[ChildEntityType(typeof(MeetingRoomPictureVideo))]
		MeetingRoomPictureVideo,
		}
	
	#endregion MeetingRoomPictureVideoTrailChildEntityTypes
	
	#region MeetingRoomPictureVideoTrailFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;MeetingRoomPictureVideoTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoomPictureVideoTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomPictureVideoTrailFilterBuilder : SqlFilterBuilder<MeetingRoomPictureVideoTrailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomPictureVideoTrailFilterBuilder class.
		/// </summary>
		public MeetingRoomPictureVideoTrailFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomPictureVideoTrailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomPictureVideoTrailFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomPictureVideoTrailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomPictureVideoTrailFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomPictureVideoTrailFilterBuilder
	
	#region MeetingRoomPictureVideoTrailParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;MeetingRoomPictureVideoTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoomPictureVideoTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomPictureVideoTrailParameterBuilder : ParameterizedSqlFilterBuilder<MeetingRoomPictureVideoTrailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomPictureVideoTrailParameterBuilder class.
		/// </summary>
		public MeetingRoomPictureVideoTrailParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomPictureVideoTrailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomPictureVideoTrailParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomPictureVideoTrailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomPictureVideoTrailParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomPictureVideoTrailParameterBuilder
	
	#region MeetingRoomPictureVideoTrailSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;MeetingRoomPictureVideoTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoomPictureVideoTrail"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class MeetingRoomPictureVideoTrailSortBuilder : SqlSortBuilder<MeetingRoomPictureVideoTrailColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomPictureVideoTrailSqlSortBuilder class.
		/// </summary>
		public MeetingRoomPictureVideoTrailSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion MeetingRoomPictureVideoTrailSortBuilder
	
} // end namespace
