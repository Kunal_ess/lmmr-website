﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="HotelProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class HotelProviderBaseCore : EntityProviderBase<LMMR.Entities.Hotel, LMMR.Entities.HotelKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.HotelKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Hotel_Group key.
		///		FK_Hotel_Group Description: 
		/// </summary>
		/// <param name="_groupId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Hotel objects.</returns>
		public TList<Hotel> GetByGroupId(System.Int64? _groupId)
		{
			int count = -1;
			return GetByGroupId(_groupId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Hotel_Group key.
		///		FK_Hotel_Group Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_groupId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Hotel objects.</returns>
		/// <remarks></remarks>
		public TList<Hotel> GetByGroupId(TransactionManager transactionManager, System.Int64? _groupId)
		{
			int count = -1;
			return GetByGroupId(transactionManager, _groupId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Hotel_Group key.
		///		FK_Hotel_Group Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_groupId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Hotel objects.</returns>
		public TList<Hotel> GetByGroupId(TransactionManager transactionManager, System.Int64? _groupId, int start, int pageLength)
		{
			int count = -1;
			return GetByGroupId(transactionManager, _groupId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Hotel_Group key.
		///		fkHotelGroup Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_groupId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Hotel objects.</returns>
		public TList<Hotel> GetByGroupId(System.Int64? _groupId, int start, int pageLength)
		{
			int count =  -1;
			return GetByGroupId(null, _groupId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Hotel_Group key.
		///		fkHotelGroup Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_groupId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Hotel objects.</returns>
		public TList<Hotel> GetByGroupId(System.Int64? _groupId, int start, int pageLength,out int count)
		{
			return GetByGroupId(null, _groupId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Hotel_Group key.
		///		FK_Hotel_Group Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_groupId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.Hotel objects.</returns>
		public abstract TList<Hotel> GetByGroupId(TransactionManager transactionManager, System.Int64? _groupId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Hotel_UpdatedBy key.
		///		FK_Hotel_UpdatedBy Description: 
		/// </summary>
		/// <param name="_updatedBy"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Hotel objects.</returns>
		public TList<Hotel> GetByUpdatedBy(System.Int64? _updatedBy)
		{
			int count = -1;
			return GetByUpdatedBy(_updatedBy, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Hotel_UpdatedBy key.
		///		FK_Hotel_UpdatedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_updatedBy"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Hotel objects.</returns>
		/// <remarks></remarks>
		public TList<Hotel> GetByUpdatedBy(TransactionManager transactionManager, System.Int64? _updatedBy)
		{
			int count = -1;
			return GetByUpdatedBy(transactionManager, _updatedBy, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Hotel_UpdatedBy key.
		///		FK_Hotel_UpdatedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_updatedBy"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Hotel objects.</returns>
		public TList<Hotel> GetByUpdatedBy(TransactionManager transactionManager, System.Int64? _updatedBy, int start, int pageLength)
		{
			int count = -1;
			return GetByUpdatedBy(transactionManager, _updatedBy, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Hotel_UpdatedBy key.
		///		fkHotelUpdatedBy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_updatedBy"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Hotel objects.</returns>
		public TList<Hotel> GetByUpdatedBy(System.Int64? _updatedBy, int start, int pageLength)
		{
			int count =  -1;
			return GetByUpdatedBy(null, _updatedBy, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Hotel_UpdatedBy key.
		///		fkHotelUpdatedBy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_updatedBy"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Hotel objects.</returns>
		public TList<Hotel> GetByUpdatedBy(System.Int64? _updatedBy, int start, int pageLength,out int count)
		{
			return GetByUpdatedBy(null, _updatedBy, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Hotel_UpdatedBy key.
		///		FK_Hotel_UpdatedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_updatedBy"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.Hotel objects.</returns>
		public abstract TList<Hotel> GetByUpdatedBy(TransactionManager transactionManager, System.Int64? _updatedBy, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Hotel_Users key.
		///		FK_Hotel_Users Description: 
		/// </summary>
		/// <param name="_clientId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Hotel objects.</returns>
		public TList<Hotel> GetByClientId(System.Int64? _clientId)
		{
			int count = -1;
			return GetByClientId(_clientId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Hotel_Users key.
		///		FK_Hotel_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_clientId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Hotel objects.</returns>
		/// <remarks></remarks>
		public TList<Hotel> GetByClientId(TransactionManager transactionManager, System.Int64? _clientId)
		{
			int count = -1;
			return GetByClientId(transactionManager, _clientId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Hotel_Users key.
		///		FK_Hotel_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_clientId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Hotel objects.</returns>
		public TList<Hotel> GetByClientId(TransactionManager transactionManager, System.Int64? _clientId, int start, int pageLength)
		{
			int count = -1;
			return GetByClientId(transactionManager, _clientId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Hotel_Users key.
		///		fkHotelUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_clientId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Hotel objects.</returns>
		public TList<Hotel> GetByClientId(System.Int64? _clientId, int start, int pageLength)
		{
			int count =  -1;
			return GetByClientId(null, _clientId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Hotel_Users key.
		///		fkHotelUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_clientId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Hotel objects.</returns>
		public TList<Hotel> GetByClientId(System.Int64? _clientId, int start, int pageLength,out int count)
		{
			return GetByClientId(null, _clientId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Hotel_Users key.
		///		FK_Hotel_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_clientId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.Hotel objects.</returns>
		public abstract TList<Hotel> GetByClientId(TransactionManager transactionManager, System.Int64? _clientId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.Hotel Get(TransactionManager transactionManager, LMMR.Entities.HotelKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_tblHotel index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Hotel"/> class.</returns>
		public LMMR.Entities.Hotel GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_tblHotel index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Hotel"/> class.</returns>
		public LMMR.Entities.Hotel GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_tblHotel index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Hotel"/> class.</returns>
		public LMMR.Entities.Hotel GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_tblHotel index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Hotel"/> class.</returns>
		public LMMR.Entities.Hotel GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_tblHotel index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Hotel"/> class.</returns>
		public LMMR.Entities.Hotel GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_tblHotel index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Hotel"/> class.</returns>
		public abstract LMMR.Entities.Hotel GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Hotel&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Hotel&gt;"/></returns>
		public static TList<Hotel> Fill(IDataReader reader, TList<Hotel> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.Hotel c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Hotel")
					.Append("|").Append((System.Int64)reader[((int)HotelColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Hotel>(
					key.ToString(), // EntityTrackingKey
					"Hotel",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.Hotel();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)HotelColumn.Id - 1)];
					c.Name = (reader.IsDBNull(((int)HotelColumn.Name - 1)))?null:(System.String)reader[((int)HotelColumn.Name - 1)];
					c.CountryId = (System.Int64)reader[((int)HotelColumn.CountryId - 1)];
					c.CityId = (System.Int64)reader[((int)HotelColumn.CityId - 1)];
					c.ZoneId = (System.Int64)reader[((int)HotelColumn.ZoneId - 1)];
					c.Stars = (reader.IsDBNull(((int)HotelColumn.Stars - 1)))?null:(System.Int32?)reader[((int)HotelColumn.Stars - 1)];
					c.Longitude = (reader.IsDBNull(((int)HotelColumn.Longitude - 1)))?null:(System.String)reader[((int)HotelColumn.Longitude - 1)];
					c.Latitude = (reader.IsDBNull(((int)HotelColumn.Latitude - 1)))?null:(System.String)reader[((int)HotelColumn.Latitude - 1)];
					c.Logo = (reader.IsDBNull(((int)HotelColumn.Logo - 1)))?null:(System.String)reader[((int)HotelColumn.Logo - 1)];
					c.IsActive = (System.Boolean)reader[((int)HotelColumn.IsActive - 1)];
					c.CancelationPolicyId = (System.Int64)reader[((int)HotelColumn.CancelationPolicyId - 1)];
					c.GroupId = (reader.IsDBNull(((int)HotelColumn.GroupId - 1)))?null:(System.Int64?)reader[((int)HotelColumn.GroupId - 1)];
					c.OperatorChoice = (reader.IsDBNull(((int)HotelColumn.OperatorChoice - 1)))?null:(System.String)reader[((int)HotelColumn.OperatorChoice - 1)];
					c.HotelPlan = (reader.IsDBNull(((int)HotelColumn.HotelPlan - 1)))?null:(System.String)reader[((int)HotelColumn.HotelPlan - 1)];
					c.ClientId = (reader.IsDBNull(((int)HotelColumn.ClientId - 1)))?null:(System.Int64?)reader[((int)HotelColumn.ClientId - 1)];
					c.ContractId = (reader.IsDBNull(((int)HotelColumn.ContractId - 1)))?null:(System.String)reader[((int)HotelColumn.ContractId - 1)];
					c.StaffId = (System.Int64)reader[((int)HotelColumn.StaffId - 1)];
					c.CurrencyId = (System.Int64)reader[((int)HotelColumn.CurrencyId - 1)];
					c.ContractValue = (reader.IsDBNull(((int)HotelColumn.ContractValue - 1)))?null:(System.Decimal?)reader[((int)HotelColumn.ContractValue - 1)];
					c.TimeZone = (reader.IsDBNull(((int)HotelColumn.TimeZone - 1)))?null:(System.String)reader[((int)HotelColumn.TimeZone - 1)];
					c.Phone = (reader.IsDBNull(((int)HotelColumn.Phone - 1)))?null:(System.String)reader[((int)HotelColumn.Phone - 1)];
					c.PhoneExt = (reader.IsDBNull(((int)HotelColumn.PhoneExt - 1)))?null:(System.String)reader[((int)HotelColumn.PhoneExt - 1)];
					c.RtMFrom = (reader.IsDBNull(((int)HotelColumn.RtMFrom - 1)))?null:(System.String)reader[((int)HotelColumn.RtMFrom - 1)];
					c.RtMTo = (reader.IsDBNull(((int)HotelColumn.RtMTo - 1)))?null:(System.String)reader[((int)HotelColumn.RtMTo - 1)];
					c.RtAFrom = (reader.IsDBNull(((int)HotelColumn.RtAFrom - 1)))?null:(System.String)reader[((int)HotelColumn.RtAFrom - 1)];
					c.RtATo = (reader.IsDBNull(((int)HotelColumn.RtATo - 1)))?null:(System.String)reader[((int)HotelColumn.RtATo - 1)];
					c.RtFFrom = (reader.IsDBNull(((int)HotelColumn.RtFFrom - 1)))?null:(System.String)reader[((int)HotelColumn.RtFFrom - 1)];
					c.RtFTo = (reader.IsDBNull(((int)HotelColumn.RtFTo - 1)))?null:(System.String)reader[((int)HotelColumn.RtFTo - 1)];
					c.IsCreditCardExcepted = (System.Boolean)reader[((int)HotelColumn.IsCreditCardExcepted - 1)];
					c.CreditCardType = (reader.IsDBNull(((int)HotelColumn.CreditCardType - 1)))?null:(System.String)reader[((int)HotelColumn.CreditCardType - 1)];
					c.Theme = (reader.IsDBNull(((int)HotelColumn.Theme - 1)))?null:(System.String)reader[((int)HotelColumn.Theme - 1)];
					c.IsBedroomAvailable = (System.Boolean)reader[((int)HotelColumn.IsBedroomAvailable - 1)];
					c.NumberOfBedroom = (reader.IsDBNull(((int)HotelColumn.NumberOfBedroom - 1)))?null:(System.Int32?)reader[((int)HotelColumn.NumberOfBedroom - 1)];
					c.Email = (reader.IsDBNull(((int)HotelColumn.Email - 1)))?null:(System.String)reader[((int)HotelColumn.Email - 1)];
					c.Password = (reader.IsDBNull(((int)HotelColumn.Password - 1)))?null:(System.String)reader[((int)HotelColumn.Password - 1)];
					c.ContactPerson = (reader.IsDBNull(((int)HotelColumn.ContactPerson - 1)))?null:(System.String)reader[((int)HotelColumn.ContactPerson - 1)];
					c.PhoneNumber = (reader.IsDBNull(((int)HotelColumn.PhoneNumber - 1)))?null:(System.String)reader[((int)HotelColumn.PhoneNumber - 1)];
					c.GoOnline = (reader.IsDBNull(((int)HotelColumn.GoOnline - 1)))?null:(System.Boolean?)reader[((int)HotelColumn.GoOnline - 1)];
					c.ZipCode = (reader.IsDBNull(((int)HotelColumn.ZipCode - 1)))?null:(System.String)reader[((int)HotelColumn.ZipCode - 1)];
					c.UpdatedBy = (reader.IsDBNull(((int)HotelColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)HotelColumn.UpdatedBy - 1)];
					c.HotelAddress = (reader.IsDBNull(((int)HotelColumn.HotelAddress - 1)))?null:(System.String)reader[((int)HotelColumn.HotelAddress - 1)];
					c.CreationDate = (System.DateTime)reader[((int)HotelColumn.CreationDate - 1)];
					c.CountryCodePhone = (reader.IsDBNull(((int)HotelColumn.CountryCodePhone - 1)))?null:(System.String)reader[((int)HotelColumn.CountryCodePhone - 1)];
					c.CountryCodeFax = (reader.IsDBNull(((int)HotelColumn.CountryCodeFax - 1)))?null:(System.String)reader[((int)HotelColumn.CountryCodeFax - 1)];
					c.FaxNumber = (reader.IsDBNull(((int)HotelColumn.FaxNumber - 1)))?null:(System.String)reader[((int)HotelColumn.FaxNumber - 1)];
					c.IsRemoved = (System.Boolean)reader[((int)HotelColumn.IsRemoved - 1)];
					c.RequestGoOnline = (reader.IsDBNull(((int)HotelColumn.RequestGoOnline - 1)))?null:(System.Boolean?)reader[((int)HotelColumn.RequestGoOnline - 1)];
					c.HotelUserId = (reader.IsDBNull(((int)HotelColumn.HotelUserId - 1)))?null:(System.Int64?)reader[((int)HotelColumn.HotelUserId - 1)];
					c.AccountingOfficer = (reader.IsDBNull(((int)HotelColumn.AccountingOfficer - 1)))?null:(System.String)reader[((int)HotelColumn.AccountingOfficer - 1)];
					c.BookingAlgo = (reader.IsDBNull(((int)HotelColumn.BookingAlgo - 1)))?null:(System.Decimal?)reader[((int)HotelColumn.BookingAlgo - 1)];
					c.RequestAlgo = (reader.IsDBNull(((int)HotelColumn.RequestAlgo - 1)))?null:(System.Decimal?)reader[((int)HotelColumn.RequestAlgo - 1)];
					c.GoOnlineDate = (reader.IsDBNull(((int)HotelColumn.GoOnlineDate - 1)))?null:(System.DateTime?)reader[((int)HotelColumn.GoOnlineDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Hotel"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Hotel"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.Hotel entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)HotelColumn.Id - 1)];
			entity.Name = (reader.IsDBNull(((int)HotelColumn.Name - 1)))?null:(System.String)reader[((int)HotelColumn.Name - 1)];
			entity.CountryId = (System.Int64)reader[((int)HotelColumn.CountryId - 1)];
			entity.CityId = (System.Int64)reader[((int)HotelColumn.CityId - 1)];
			entity.ZoneId = (System.Int64)reader[((int)HotelColumn.ZoneId - 1)];
			entity.Stars = (reader.IsDBNull(((int)HotelColumn.Stars - 1)))?null:(System.Int32?)reader[((int)HotelColumn.Stars - 1)];
			entity.Longitude = (reader.IsDBNull(((int)HotelColumn.Longitude - 1)))?null:(System.String)reader[((int)HotelColumn.Longitude - 1)];
			entity.Latitude = (reader.IsDBNull(((int)HotelColumn.Latitude - 1)))?null:(System.String)reader[((int)HotelColumn.Latitude - 1)];
			entity.Logo = (reader.IsDBNull(((int)HotelColumn.Logo - 1)))?null:(System.String)reader[((int)HotelColumn.Logo - 1)];
			entity.IsActive = (System.Boolean)reader[((int)HotelColumn.IsActive - 1)];
			entity.CancelationPolicyId = (System.Int64)reader[((int)HotelColumn.CancelationPolicyId - 1)];
			entity.GroupId = (reader.IsDBNull(((int)HotelColumn.GroupId - 1)))?null:(System.Int64?)reader[((int)HotelColumn.GroupId - 1)];
			entity.OperatorChoice = (reader.IsDBNull(((int)HotelColumn.OperatorChoice - 1)))?null:(System.String)reader[((int)HotelColumn.OperatorChoice - 1)];
			entity.HotelPlan = (reader.IsDBNull(((int)HotelColumn.HotelPlan - 1)))?null:(System.String)reader[((int)HotelColumn.HotelPlan - 1)];
			entity.ClientId = (reader.IsDBNull(((int)HotelColumn.ClientId - 1)))?null:(System.Int64?)reader[((int)HotelColumn.ClientId - 1)];
			entity.ContractId = (reader.IsDBNull(((int)HotelColumn.ContractId - 1)))?null:(System.String)reader[((int)HotelColumn.ContractId - 1)];
			entity.StaffId = (System.Int64)reader[((int)HotelColumn.StaffId - 1)];
			entity.CurrencyId = (System.Int64)reader[((int)HotelColumn.CurrencyId - 1)];
			entity.ContractValue = (reader.IsDBNull(((int)HotelColumn.ContractValue - 1)))?null:(System.Decimal?)reader[((int)HotelColumn.ContractValue - 1)];
			entity.TimeZone = (reader.IsDBNull(((int)HotelColumn.TimeZone - 1)))?null:(System.String)reader[((int)HotelColumn.TimeZone - 1)];
			entity.Phone = (reader.IsDBNull(((int)HotelColumn.Phone - 1)))?null:(System.String)reader[((int)HotelColumn.Phone - 1)];
			entity.PhoneExt = (reader.IsDBNull(((int)HotelColumn.PhoneExt - 1)))?null:(System.String)reader[((int)HotelColumn.PhoneExt - 1)];
			entity.RtMFrom = (reader.IsDBNull(((int)HotelColumn.RtMFrom - 1)))?null:(System.String)reader[((int)HotelColumn.RtMFrom - 1)];
			entity.RtMTo = (reader.IsDBNull(((int)HotelColumn.RtMTo - 1)))?null:(System.String)reader[((int)HotelColumn.RtMTo - 1)];
			entity.RtAFrom = (reader.IsDBNull(((int)HotelColumn.RtAFrom - 1)))?null:(System.String)reader[((int)HotelColumn.RtAFrom - 1)];
			entity.RtATo = (reader.IsDBNull(((int)HotelColumn.RtATo - 1)))?null:(System.String)reader[((int)HotelColumn.RtATo - 1)];
			entity.RtFFrom = (reader.IsDBNull(((int)HotelColumn.RtFFrom - 1)))?null:(System.String)reader[((int)HotelColumn.RtFFrom - 1)];
			entity.RtFTo = (reader.IsDBNull(((int)HotelColumn.RtFTo - 1)))?null:(System.String)reader[((int)HotelColumn.RtFTo - 1)];
			entity.IsCreditCardExcepted = (System.Boolean)reader[((int)HotelColumn.IsCreditCardExcepted - 1)];
			entity.CreditCardType = (reader.IsDBNull(((int)HotelColumn.CreditCardType - 1)))?null:(System.String)reader[((int)HotelColumn.CreditCardType - 1)];
			entity.Theme = (reader.IsDBNull(((int)HotelColumn.Theme - 1)))?null:(System.String)reader[((int)HotelColumn.Theme - 1)];
			entity.IsBedroomAvailable = (System.Boolean)reader[((int)HotelColumn.IsBedroomAvailable - 1)];
			entity.NumberOfBedroom = (reader.IsDBNull(((int)HotelColumn.NumberOfBedroom - 1)))?null:(System.Int32?)reader[((int)HotelColumn.NumberOfBedroom - 1)];
			entity.Email = (reader.IsDBNull(((int)HotelColumn.Email - 1)))?null:(System.String)reader[((int)HotelColumn.Email - 1)];
			entity.Password = (reader.IsDBNull(((int)HotelColumn.Password - 1)))?null:(System.String)reader[((int)HotelColumn.Password - 1)];
			entity.ContactPerson = (reader.IsDBNull(((int)HotelColumn.ContactPerson - 1)))?null:(System.String)reader[((int)HotelColumn.ContactPerson - 1)];
			entity.PhoneNumber = (reader.IsDBNull(((int)HotelColumn.PhoneNumber - 1)))?null:(System.String)reader[((int)HotelColumn.PhoneNumber - 1)];
			entity.GoOnline = (reader.IsDBNull(((int)HotelColumn.GoOnline - 1)))?null:(System.Boolean?)reader[((int)HotelColumn.GoOnline - 1)];
			entity.ZipCode = (reader.IsDBNull(((int)HotelColumn.ZipCode - 1)))?null:(System.String)reader[((int)HotelColumn.ZipCode - 1)];
			entity.UpdatedBy = (reader.IsDBNull(((int)HotelColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)HotelColumn.UpdatedBy - 1)];
			entity.HotelAddress = (reader.IsDBNull(((int)HotelColumn.HotelAddress - 1)))?null:(System.String)reader[((int)HotelColumn.HotelAddress - 1)];
			entity.CreationDate = (System.DateTime)reader[((int)HotelColumn.CreationDate - 1)];
			entity.CountryCodePhone = (reader.IsDBNull(((int)HotelColumn.CountryCodePhone - 1)))?null:(System.String)reader[((int)HotelColumn.CountryCodePhone - 1)];
			entity.CountryCodeFax = (reader.IsDBNull(((int)HotelColumn.CountryCodeFax - 1)))?null:(System.String)reader[((int)HotelColumn.CountryCodeFax - 1)];
			entity.FaxNumber = (reader.IsDBNull(((int)HotelColumn.FaxNumber - 1)))?null:(System.String)reader[((int)HotelColumn.FaxNumber - 1)];
			entity.IsRemoved = (System.Boolean)reader[((int)HotelColumn.IsRemoved - 1)];
			entity.RequestGoOnline = (reader.IsDBNull(((int)HotelColumn.RequestGoOnline - 1)))?null:(System.Boolean?)reader[((int)HotelColumn.RequestGoOnline - 1)];
			entity.HotelUserId = (reader.IsDBNull(((int)HotelColumn.HotelUserId - 1)))?null:(System.Int64?)reader[((int)HotelColumn.HotelUserId - 1)];
			entity.AccountingOfficer = (reader.IsDBNull(((int)HotelColumn.AccountingOfficer - 1)))?null:(System.String)reader[((int)HotelColumn.AccountingOfficer - 1)];
			entity.BookingAlgo = (reader.IsDBNull(((int)HotelColumn.BookingAlgo - 1)))?null:(System.Decimal?)reader[((int)HotelColumn.BookingAlgo - 1)];
			entity.RequestAlgo = (reader.IsDBNull(((int)HotelColumn.RequestAlgo - 1)))?null:(System.Decimal?)reader[((int)HotelColumn.RequestAlgo - 1)];
			entity.GoOnlineDate = (reader.IsDBNull(((int)HotelColumn.GoOnlineDate - 1)))?null:(System.DateTime?)reader[((int)HotelColumn.GoOnlineDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Hotel"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Hotel"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.Hotel entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.Name = Convert.IsDBNull(dataRow["Name"]) ? null : (System.String)dataRow["Name"];
			entity.CountryId = (System.Int64)dataRow["CountryId"];
			entity.CityId = (System.Int64)dataRow["CityId"];
			entity.ZoneId = (System.Int64)dataRow["ZoneId"];
			entity.Stars = Convert.IsDBNull(dataRow["Stars"]) ? null : (System.Int32?)dataRow["Stars"];
			entity.Longitude = Convert.IsDBNull(dataRow["Longitude"]) ? null : (System.String)dataRow["Longitude"];
			entity.Latitude = Convert.IsDBNull(dataRow["Latitude"]) ? null : (System.String)dataRow["Latitude"];
			entity.Logo = Convert.IsDBNull(dataRow["Logo"]) ? null : (System.String)dataRow["Logo"];
			entity.IsActive = (System.Boolean)dataRow["IsActive"];
			entity.CancelationPolicyId = (System.Int64)dataRow["CancelationPolicyId"];
			entity.GroupId = Convert.IsDBNull(dataRow["GroupId"]) ? null : (System.Int64?)dataRow["GroupId"];
			entity.OperatorChoice = Convert.IsDBNull(dataRow["OperatorChoice"]) ? null : (System.String)dataRow["OperatorChoice"];
			entity.HotelPlan = Convert.IsDBNull(dataRow["HotelPlan"]) ? null : (System.String)dataRow["HotelPlan"];
			entity.ClientId = Convert.IsDBNull(dataRow["ClientId"]) ? null : (System.Int64?)dataRow["ClientId"];
			entity.ContractId = Convert.IsDBNull(dataRow["ContractId"]) ? null : (System.String)dataRow["ContractId"];
			entity.StaffId = (System.Int64)dataRow["StaffId"];
			entity.CurrencyId = (System.Int64)dataRow["CurrencyId"];
			entity.ContractValue = Convert.IsDBNull(dataRow["ContractValue"]) ? null : (System.Decimal?)dataRow["ContractValue"];
			entity.TimeZone = Convert.IsDBNull(dataRow["TimeZone"]) ? null : (System.String)dataRow["TimeZone"];
			entity.Phone = Convert.IsDBNull(dataRow["Phone"]) ? null : (System.String)dataRow["Phone"];
			entity.PhoneExt = Convert.IsDBNull(dataRow["PhoneExt"]) ? null : (System.String)dataRow["PhoneExt"];
			entity.RtMFrom = Convert.IsDBNull(dataRow["RT_M_From"]) ? null : (System.String)dataRow["RT_M_From"];
			entity.RtMTo = Convert.IsDBNull(dataRow["RT_M_To"]) ? null : (System.String)dataRow["RT_M_To"];
			entity.RtAFrom = Convert.IsDBNull(dataRow["RT_A_From"]) ? null : (System.String)dataRow["RT_A_From"];
			entity.RtATo = Convert.IsDBNull(dataRow["RT_A_To"]) ? null : (System.String)dataRow["RT_A_To"];
			entity.RtFFrom = Convert.IsDBNull(dataRow["RT_F_From"]) ? null : (System.String)dataRow["RT_F_From"];
			entity.RtFTo = Convert.IsDBNull(dataRow["RT_F_To"]) ? null : (System.String)dataRow["RT_F_To"];
			entity.IsCreditCardExcepted = (System.Boolean)dataRow["IsCreditCardExcepted"];
			entity.CreditCardType = Convert.IsDBNull(dataRow["CreditCardType"]) ? null : (System.String)dataRow["CreditCardType"];
			entity.Theme = Convert.IsDBNull(dataRow["Theme"]) ? null : (System.String)dataRow["Theme"];
			entity.IsBedroomAvailable = (System.Boolean)dataRow["IsBedroomAvailable"];
			entity.NumberOfBedroom = Convert.IsDBNull(dataRow["NumberOfBedroom"]) ? null : (System.Int32?)dataRow["NumberOfBedroom"];
			entity.Email = Convert.IsDBNull(dataRow["Email"]) ? null : (System.String)dataRow["Email"];
			entity.Password = Convert.IsDBNull(dataRow["Password"]) ? null : (System.String)dataRow["Password"];
			entity.ContactPerson = Convert.IsDBNull(dataRow["ContactPerson"]) ? null : (System.String)dataRow["ContactPerson"];
			entity.PhoneNumber = Convert.IsDBNull(dataRow["PhoneNumber"]) ? null : (System.String)dataRow["PhoneNumber"];
			entity.GoOnline = Convert.IsDBNull(dataRow["GoOnline"]) ? null : (System.Boolean?)dataRow["GoOnline"];
			entity.ZipCode = Convert.IsDBNull(dataRow["ZipCode"]) ? null : (System.String)dataRow["ZipCode"];
			entity.UpdatedBy = Convert.IsDBNull(dataRow["UpdatedBy"]) ? null : (System.Int64?)dataRow["UpdatedBy"];
			entity.HotelAddress = Convert.IsDBNull(dataRow["HotelAddress"]) ? null : (System.String)dataRow["HotelAddress"];
			entity.CreationDate = (System.DateTime)dataRow["CreationDate"];
			entity.CountryCodePhone = Convert.IsDBNull(dataRow["CountryCodePhone"]) ? null : (System.String)dataRow["CountryCodePhone"];
			entity.CountryCodeFax = Convert.IsDBNull(dataRow["CountryCodeFax"]) ? null : (System.String)dataRow["CountryCodeFax"];
			entity.FaxNumber = Convert.IsDBNull(dataRow["FaxNumber"]) ? null : (System.String)dataRow["FaxNumber"];
			entity.IsRemoved = (System.Boolean)dataRow["IsRemoved"];
			entity.RequestGoOnline = Convert.IsDBNull(dataRow["RequestGoOnline"]) ? null : (System.Boolean?)dataRow["RequestGoOnline"];
			entity.HotelUserId = Convert.IsDBNull(dataRow["HotelUserID"]) ? null : (System.Int64?)dataRow["HotelUserID"];
			entity.AccountingOfficer = Convert.IsDBNull(dataRow["AccountingOfficer"]) ? null : (System.String)dataRow["AccountingOfficer"];
			entity.BookingAlgo = Convert.IsDBNull(dataRow["BookingAlgo"]) ? null : (System.Decimal?)dataRow["BookingAlgo"];
			entity.RequestAlgo = Convert.IsDBNull(dataRow["RequestAlgo"]) ? null : (System.Decimal?)dataRow["RequestAlgo"];
			entity.GoOnlineDate = Convert.IsDBNull(dataRow["GoOnlineDate"]) ? null : (System.DateTime?)dataRow["GoOnlineDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Hotel"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.Hotel Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.Hotel entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region GroupIdSource	
			if (CanDeepLoad(entity, "Group|GroupIdSource", deepLoadType, innerList) 
				&& entity.GroupIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.GroupId ?? (long)0);
				Group tmpEntity = EntityManager.LocateEntity<Group>(EntityLocator.ConstructKeyFromPkItems(typeof(Group), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.GroupIdSource = tmpEntity;
				else
					entity.GroupIdSource = DataRepository.GroupProvider.GetById(transactionManager, (entity.GroupId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'GroupIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.GroupIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.GroupProvider.DeepLoad(transactionManager, entity.GroupIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion GroupIdSource

			#region UpdatedBySource	
			if (CanDeepLoad(entity, "Users|UpdatedBySource", deepLoadType, innerList) 
				&& entity.UpdatedBySource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.UpdatedBy ?? (long)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.UpdatedBySource = tmpEntity;
				else
					entity.UpdatedBySource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.UpdatedBy ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UpdatedBySource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.UpdatedBySource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.UpdatedBySource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion UpdatedBySource

			#region ClientIdSource	
			if (CanDeepLoad(entity, "Users|ClientIdSource", deepLoadType, innerList) 
				&& entity.ClientIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ClientId ?? (long)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ClientIdSource = tmpEntity;
				else
					entity.ClientIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.ClientId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ClientIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ClientIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ClientIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ClientIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region SpecialPriceAndPromoCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SpecialPriceAndPromo>|SpecialPriceAndPromoCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SpecialPriceAndPromoCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SpecialPriceAndPromoCollection = DataRepository.SpecialPriceAndPromoProvider.GetByHotelId(transactionManager, entity.Id);

				if (deep && entity.SpecialPriceAndPromoCollection.Count > 0)
				{
					deepHandles.Add("SpecialPriceAndPromoCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SpecialPriceAndPromo>) DataRepository.SpecialPriceAndPromoProvider.DeepLoad,
						new object[] { transactionManager, entity.SpecialPriceAndPromoCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region StatisticsCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Statistics>|StatisticsCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'StatisticsCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.StatisticsCollection = DataRepository.StatisticsProvider.GetByHotelId(transactionManager, entity.Id);

				if (deep && entity.StatisticsCollection.Count > 0)
				{
					deepHandles.Add("StatisticsCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Statistics>) DataRepository.StatisticsProvider.DeepLoad,
						new object[] { transactionManager, entity.StatisticsCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region MeetingRoomCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<MeetingRoom>|MeetingRoomCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MeetingRoomCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.MeetingRoomCollection = DataRepository.MeetingRoomProvider.GetByHotelId(transactionManager, entity.Id);

				if (deep && entity.MeetingRoomCollection.Count > 0)
				{
					deepHandles.Add("MeetingRoomCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<MeetingRoom>) DataRepository.MeetingRoomProvider.DeepLoad,
						new object[] { transactionManager, entity.MeetingRoomCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AvailabilityCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Availability>|AvailabilityCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AvailabilityCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AvailabilityCollection = DataRepository.AvailabilityProvider.GetByHotelId(transactionManager, entity.Id);

				if (deep && entity.AvailabilityCollection.Count > 0)
				{
					deepHandles.Add("AvailabilityCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Availability>) DataRepository.AvailabilityProvider.DeepLoad,
						new object[] { transactionManager, entity.AvailabilityCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AgencyInvoiceCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AgencyInvoice>|AgencyInvoiceCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AgencyInvoiceCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AgencyInvoiceCollection = DataRepository.AgencyInvoiceProvider.GetByHotelId(transactionManager, entity.Id);

				if (deep && entity.AgencyInvoiceCollection.Count > 0)
				{
					deepHandles.Add("AgencyInvoiceCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AgencyInvoice>) DataRepository.AgencyInvoiceProvider.DeepLoad,
						new object[] { transactionManager, entity.AgencyInvoiceCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region OtherItemsPricingbyHotelCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<OtherItemsPricingbyHotel>|OtherItemsPricingbyHotelCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OtherItemsPricingbyHotelCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.OtherItemsPricingbyHotelCollection = DataRepository.OtherItemsPricingbyHotelProvider.GetByHotelId(transactionManager, entity.Id);

				if (deep && entity.OtherItemsPricingbyHotelCollection.Count > 0)
				{
					deepHandles.Add("OtherItemsPricingbyHotelCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<OtherItemsPricingbyHotel>) DataRepository.OtherItemsPricingbyHotelProvider.DeepLoad,
						new object[] { transactionManager, entity.OtherItemsPricingbyHotelCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region InvoiceCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Invoice>|InvoiceCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'InvoiceCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.InvoiceCollection = DataRepository.InvoiceProvider.GetByHotelId(transactionManager, entity.Id);

				if (deep && entity.InvoiceCollection.Count > 0)
				{
					deepHandles.Add("InvoiceCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Invoice>) DataRepository.InvoiceProvider.DeepLoad,
						new object[] { transactionManager, entity.InvoiceCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region HotelPhotoVideoGallaryCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<HotelPhotoVideoGallary>|HotelPhotoVideoGallaryCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HotelPhotoVideoGallaryCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.HotelPhotoVideoGallaryCollection = DataRepository.HotelPhotoVideoGallaryProvider.GetByHotelId(transactionManager, entity.Id);

				if (deep && entity.HotelPhotoVideoGallaryCollection.Count > 0)
				{
					deepHandles.Add("HotelPhotoVideoGallaryCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<HotelPhotoVideoGallary>) DataRepository.HotelPhotoVideoGallaryProvider.DeepLoad,
						new object[] { transactionManager, entity.HotelPhotoVideoGallaryCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ActualPackagePriceCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ActualPackagePrice>|ActualPackagePriceCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ActualPackagePriceCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ActualPackagePriceCollection = DataRepository.ActualPackagePriceProvider.GetByHotelId(transactionManager, entity.Id);

				if (deep && entity.ActualPackagePriceCollection.Count > 0)
				{
					deepHandles.Add("ActualPackagePriceCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ActualPackagePrice>) DataRepository.ActualPackagePriceProvider.DeepLoad,
						new object[] { transactionManager, entity.ActualPackagePriceCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PackageByHotelCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PackageByHotel>|PackageByHotelCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PackageByHotelCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PackageByHotelCollection = DataRepository.PackageByHotelProvider.GetByHotelId(transactionManager, entity.Id);

				if (deep && entity.PackageByHotelCollection.Count > 0)
				{
					deepHandles.Add("PackageByHotelCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PackageByHotel>) DataRepository.PackageByHotelProvider.DeepLoad,
						new object[] { transactionManager, entity.PackageByHotelCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region HotelDescCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<HotelDesc>|HotelDescCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HotelDescCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.HotelDescCollection = DataRepository.HotelDescProvider.GetByHotelId(transactionManager, entity.Id);

				if (deep && entity.HotelDescCollection.Count > 0)
				{
					deepHandles.Add("HotelDescCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<HotelDesc>) DataRepository.HotelDescProvider.DeepLoad,
						new object[] { transactionManager, entity.HotelDescCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region BedRoomCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<BedRoom>|BedRoomCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BedRoomCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BedRoomCollection = DataRepository.BedRoomProvider.GetByHotelId(transactionManager, entity.Id);

				if (deep && entity.BedRoomCollection.Count > 0)
				{
					deepHandles.Add("BedRoomCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<BedRoom>) DataRepository.BedRoomProvider.DeepLoad,
						new object[] { transactionManager, entity.BedRoomCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region HotelContactCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<HotelContact>|HotelContactCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HotelContactCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.HotelContactCollection = DataRepository.HotelContactProvider.GetByHotelId(transactionManager, entity.Id);

				if (deep && entity.HotelContactCollection.Count > 0)
				{
					deepHandles.Add("HotelContactCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<HotelContact>) DataRepository.HotelContactProvider.DeepLoad,
						new object[] { transactionManager, entity.HotelContactCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region SpecialPriceForBedroomCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SpecialPriceForBedroom>|SpecialPriceForBedroomCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SpecialPriceForBedroomCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SpecialPriceForBedroomCollection = DataRepository.SpecialPriceForBedroomProvider.GetByHotelId(transactionManager, entity.Id);

				if (deep && entity.SpecialPriceForBedroomCollection.Count > 0)
				{
					deepHandles.Add("SpecialPriceForBedroomCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SpecialPriceForBedroom>) DataRepository.SpecialPriceForBedroomProvider.DeepLoad,
						new object[] { transactionManager, entity.SpecialPriceForBedroomCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PolicyHotelMappingCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PolicyHotelMapping>|PolicyHotelMappingCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PolicyHotelMappingCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PolicyHotelMappingCollection = DataRepository.PolicyHotelMappingProvider.GetByHotelId(transactionManager, entity.Id);

				if (deep && entity.PolicyHotelMappingCollection.Count > 0)
				{
					deepHandles.Add("PolicyHotelMappingCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PolicyHotelMapping>) DataRepository.PolicyHotelMappingProvider.DeepLoad,
						new object[] { transactionManager, entity.PolicyHotelMappingCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region HotelTrailCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<HotelTrail>|HotelTrailCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HotelTrailCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.HotelTrailCollection = DataRepository.HotelTrailProvider.GetByHotelId(transactionManager, entity.Id);

				if (deep && entity.HotelTrailCollection.Count > 0)
				{
					deepHandles.Add("HotelTrailCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<HotelTrail>) DataRepository.HotelTrailProvider.DeepLoad,
						new object[] { transactionManager, entity.HotelTrailCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region MeetingRoomConfigCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<MeetingRoomConfig>|MeetingRoomConfigCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MeetingRoomConfigCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.MeetingRoomConfigCollection = DataRepository.MeetingRoomConfigProvider.GetByHotelId(transactionManager, entity.Id);

				if (deep && entity.MeetingRoomConfigCollection.Count > 0)
				{
					deepHandles.Add("MeetingRoomConfigCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<MeetingRoomConfig>) DataRepository.MeetingRoomConfigProvider.DeepLoad,
						new object[] { transactionManager, entity.MeetingRoomConfigCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ServeyResponseCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ServeyResponse>|ServeyResponseCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ServeyResponseCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ServeyResponseCollection = DataRepository.ServeyResponseProvider.GetByVanueId(transactionManager, entity.Id);

				if (deep && entity.ServeyResponseCollection.Count > 0)
				{
					deepHandles.Add("ServeyResponseCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ServeyResponse>) DataRepository.ServeyResponseProvider.DeepLoad,
						new object[] { transactionManager, entity.ServeyResponseCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region HotelOwnerLinkCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<HotelOwnerLink>|HotelOwnerLinkCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HotelOwnerLinkCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.HotelOwnerLinkCollection = DataRepository.HotelOwnerLinkProvider.GetByHotelId(transactionManager, entity.Id);

				if (deep && entity.HotelOwnerLinkCollection.Count > 0)
				{
					deepHandles.Add("HotelOwnerLinkCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<HotelOwnerLink>) DataRepository.HotelOwnerLinkProvider.DeepLoad,
						new object[] { transactionManager, entity.HotelOwnerLinkCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region LeftMenuLinkCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<LeftMenuLink>|LeftMenuLinkCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'LeftMenuLinkCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.LeftMenuLinkCollection = DataRepository.LeftMenuLinkProvider.GetByHotelId(transactionManager, entity.Id);

				if (deep && entity.LeftMenuLinkCollection.Count > 0)
				{
					deepHandles.Add("LeftMenuLinkCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<LeftMenuLink>) DataRepository.LeftMenuLinkProvider.DeepLoad,
						new object[] { transactionManager, entity.LeftMenuLinkCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region BookingCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Booking>|BookingCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BookingCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BookingCollection = DataRepository.BookingProvider.GetByHotelId(transactionManager, entity.Id);

				if (deep && entity.BookingCollection.Count > 0)
				{
					deepHandles.Add("BookingCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Booking>) DataRepository.BookingProvider.DeepLoad,
						new object[] { transactionManager, entity.BookingCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region DashboardLinkCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<DashboardLink>|DashboardLinkCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'DashboardLinkCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.DashboardLinkCollection = DataRepository.DashboardLinkProvider.GetByHotelId(transactionManager, entity.Id);

				if (deep && entity.DashboardLinkCollection.Count > 0)
				{
					deepHandles.Add("DashboardLinkCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<DashboardLink>) DataRepository.DashboardLinkProvider.DeepLoad,
						new object[] { transactionManager, entity.DashboardLinkCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PriorityBasketCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PriorityBasket>|PriorityBasketCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PriorityBasketCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PriorityBasketCollection = DataRepository.PriorityBasketProvider.GetByHotelId(transactionManager, entity.Id);

				if (deep && entity.PriorityBasketCollection.Count > 0)
				{
					deepHandles.Add("PriorityBasketCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PriorityBasket>) DataRepository.PriorityBasketProvider.DeepLoad,
						new object[] { transactionManager, entity.PriorityBasketCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region HotelFacilitiesCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<HotelFacilities>|HotelFacilitiesCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HotelFacilitiesCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.HotelFacilitiesCollection = DataRepository.HotelFacilitiesProvider.GetByHotelId(transactionManager, entity.Id);

				if (deep && entity.HotelFacilitiesCollection.Count > 0)
				{
					deepHandles.Add("HotelFacilitiesCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<HotelFacilities>) DataRepository.HotelFacilitiesProvider.DeepLoad,
						new object[] { transactionManager, entity.HotelFacilitiesCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region BookingLogsCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<BookingLogs>|BookingLogsCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BookingLogsCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BookingLogsCollection = DataRepository.BookingLogsProvider.GetByHotelId(transactionManager, entity.Id);

				if (deep && entity.BookingLogsCollection.Count > 0)
				{
					deepHandles.Add("BookingLogsCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<BookingLogs>) DataRepository.BookingLogsProvider.DeepLoad,
						new object[] { transactionManager, entity.BookingLogsCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region WhiteLabelMappingWithHotelCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<WhiteLabelMappingWithHotel>|WhiteLabelMappingWithHotelCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'WhiteLabelMappingWithHotelCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.WhiteLabelMappingWithHotelCollection = DataRepository.WhiteLabelMappingWithHotelProvider.GetByHotelId(transactionManager, entity.Id);

				if (deep && entity.WhiteLabelMappingWithHotelCollection.Count > 0)
				{
					deepHandles.Add("WhiteLabelMappingWithHotelCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<WhiteLabelMappingWithHotel>) DataRepository.WhiteLabelMappingWithHotelProvider.DeepLoad,
						new object[] { transactionManager, entity.WhiteLabelMappingWithHotelCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.Hotel object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.Hotel instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.Hotel Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.Hotel entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region GroupIdSource
			if (CanDeepSave(entity, "Group|GroupIdSource", deepSaveType, innerList) 
				&& entity.GroupIdSource != null)
			{
				DataRepository.GroupProvider.Save(transactionManager, entity.GroupIdSource);
				entity.GroupId = entity.GroupIdSource.Id;
			}
			#endregion 
			
			#region UpdatedBySource
			if (CanDeepSave(entity, "Users|UpdatedBySource", deepSaveType, innerList) 
				&& entity.UpdatedBySource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.UpdatedBySource);
				entity.UpdatedBy = entity.UpdatedBySource.UserId;
			}
			#endregion 
			
			#region ClientIdSource
			if (CanDeepSave(entity, "Users|ClientIdSource", deepSaveType, innerList) 
				&& entity.ClientIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ClientIdSource);
				entity.ClientId = entity.ClientIdSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<SpecialPriceAndPromo>
				if (CanDeepSave(entity.SpecialPriceAndPromoCollection, "List<SpecialPriceAndPromo>|SpecialPriceAndPromoCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SpecialPriceAndPromo child in entity.SpecialPriceAndPromoCollection)
					{
						if(child.HotelIdSource != null)
						{
							child.HotelId = child.HotelIdSource.Id;
						}
						else
						{
							child.HotelId = entity.Id;
						}

					}

					if (entity.SpecialPriceAndPromoCollection.Count > 0 || entity.SpecialPriceAndPromoCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SpecialPriceAndPromoProvider.Save(transactionManager, entity.SpecialPriceAndPromoCollection);
						
						deepHandles.Add("SpecialPriceAndPromoCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SpecialPriceAndPromo >) DataRepository.SpecialPriceAndPromoProvider.DeepSave,
							new object[] { transactionManager, entity.SpecialPriceAndPromoCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Statistics>
				if (CanDeepSave(entity.StatisticsCollection, "List<Statistics>|StatisticsCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Statistics child in entity.StatisticsCollection)
					{
						if(child.HotelIdSource != null)
						{
							child.HotelId = child.HotelIdSource.Id;
						}
						else
						{
							child.HotelId = entity.Id;
						}

					}

					if (entity.StatisticsCollection.Count > 0 || entity.StatisticsCollection.DeletedItems.Count > 0)
					{
						//DataRepository.StatisticsProvider.Save(transactionManager, entity.StatisticsCollection);
						
						deepHandles.Add("StatisticsCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Statistics >) DataRepository.StatisticsProvider.DeepSave,
							new object[] { transactionManager, entity.StatisticsCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<MeetingRoom>
				if (CanDeepSave(entity.MeetingRoomCollection, "List<MeetingRoom>|MeetingRoomCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(MeetingRoom child in entity.MeetingRoomCollection)
					{
						if(child.HotelIdSource != null)
						{
							child.HotelId = child.HotelIdSource.Id;
						}
						else
						{
							child.HotelId = entity.Id;
						}

					}

					if (entity.MeetingRoomCollection.Count > 0 || entity.MeetingRoomCollection.DeletedItems.Count > 0)
					{
						//DataRepository.MeetingRoomProvider.Save(transactionManager, entity.MeetingRoomCollection);
						
						deepHandles.Add("MeetingRoomCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< MeetingRoom >) DataRepository.MeetingRoomProvider.DeepSave,
							new object[] { transactionManager, entity.MeetingRoomCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Availability>
				if (CanDeepSave(entity.AvailabilityCollection, "List<Availability>|AvailabilityCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Availability child in entity.AvailabilityCollection)
					{
						if(child.HotelIdSource != null)
						{
							child.HotelId = child.HotelIdSource.Id;
						}
						else
						{
							child.HotelId = entity.Id;
						}

					}

					if (entity.AvailabilityCollection.Count > 0 || entity.AvailabilityCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AvailabilityProvider.Save(transactionManager, entity.AvailabilityCollection);
						
						deepHandles.Add("AvailabilityCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Availability >) DataRepository.AvailabilityProvider.DeepSave,
							new object[] { transactionManager, entity.AvailabilityCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<AgencyInvoice>
				if (CanDeepSave(entity.AgencyInvoiceCollection, "List<AgencyInvoice>|AgencyInvoiceCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AgencyInvoice child in entity.AgencyInvoiceCollection)
					{
						if(child.HotelIdSource != null)
						{
							child.HotelId = child.HotelIdSource.Id;
						}
						else
						{
							child.HotelId = entity.Id;
						}

					}

					if (entity.AgencyInvoiceCollection.Count > 0 || entity.AgencyInvoiceCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AgencyInvoiceProvider.Save(transactionManager, entity.AgencyInvoiceCollection);
						
						deepHandles.Add("AgencyInvoiceCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AgencyInvoice >) DataRepository.AgencyInvoiceProvider.DeepSave,
							new object[] { transactionManager, entity.AgencyInvoiceCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<OtherItemsPricingbyHotel>
				if (CanDeepSave(entity.OtherItemsPricingbyHotelCollection, "List<OtherItemsPricingbyHotel>|OtherItemsPricingbyHotelCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(OtherItemsPricingbyHotel child in entity.OtherItemsPricingbyHotelCollection)
					{
						if(child.HotelIdSource != null)
						{
							child.HotelId = child.HotelIdSource.Id;
						}
						else
						{
							child.HotelId = entity.Id;
						}

					}

					if (entity.OtherItemsPricingbyHotelCollection.Count > 0 || entity.OtherItemsPricingbyHotelCollection.DeletedItems.Count > 0)
					{
						//DataRepository.OtherItemsPricingbyHotelProvider.Save(transactionManager, entity.OtherItemsPricingbyHotelCollection);
						
						deepHandles.Add("OtherItemsPricingbyHotelCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< OtherItemsPricingbyHotel >) DataRepository.OtherItemsPricingbyHotelProvider.DeepSave,
							new object[] { transactionManager, entity.OtherItemsPricingbyHotelCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Invoice>
				if (CanDeepSave(entity.InvoiceCollection, "List<Invoice>|InvoiceCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Invoice child in entity.InvoiceCollection)
					{
						if(child.HotelIdSource != null)
						{
							child.HotelId = child.HotelIdSource.Id;
						}
						else
						{
							child.HotelId = entity.Id;
						}

					}

					if (entity.InvoiceCollection.Count > 0 || entity.InvoiceCollection.DeletedItems.Count > 0)
					{
						//DataRepository.InvoiceProvider.Save(transactionManager, entity.InvoiceCollection);
						
						deepHandles.Add("InvoiceCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Invoice >) DataRepository.InvoiceProvider.DeepSave,
							new object[] { transactionManager, entity.InvoiceCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<HotelPhotoVideoGallary>
				if (CanDeepSave(entity.HotelPhotoVideoGallaryCollection, "List<HotelPhotoVideoGallary>|HotelPhotoVideoGallaryCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(HotelPhotoVideoGallary child in entity.HotelPhotoVideoGallaryCollection)
					{
						if(child.HotelIdSource != null)
						{
							child.HotelId = child.HotelIdSource.Id;
						}
						else
						{
							child.HotelId = entity.Id;
						}

					}

					if (entity.HotelPhotoVideoGallaryCollection.Count > 0 || entity.HotelPhotoVideoGallaryCollection.DeletedItems.Count > 0)
					{
						//DataRepository.HotelPhotoVideoGallaryProvider.Save(transactionManager, entity.HotelPhotoVideoGallaryCollection);
						
						deepHandles.Add("HotelPhotoVideoGallaryCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< HotelPhotoVideoGallary >) DataRepository.HotelPhotoVideoGallaryProvider.DeepSave,
							new object[] { transactionManager, entity.HotelPhotoVideoGallaryCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ActualPackagePrice>
				if (CanDeepSave(entity.ActualPackagePriceCollection, "List<ActualPackagePrice>|ActualPackagePriceCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ActualPackagePrice child in entity.ActualPackagePriceCollection)
					{
						if(child.HotelIdSource != null)
						{
							child.HotelId = child.HotelIdSource.Id;
						}
						else
						{
							child.HotelId = entity.Id;
						}

					}

					if (entity.ActualPackagePriceCollection.Count > 0 || entity.ActualPackagePriceCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ActualPackagePriceProvider.Save(transactionManager, entity.ActualPackagePriceCollection);
						
						deepHandles.Add("ActualPackagePriceCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ActualPackagePrice >) DataRepository.ActualPackagePriceProvider.DeepSave,
							new object[] { transactionManager, entity.ActualPackagePriceCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<PackageByHotel>
				if (CanDeepSave(entity.PackageByHotelCollection, "List<PackageByHotel>|PackageByHotelCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PackageByHotel child in entity.PackageByHotelCollection)
					{
						if(child.HotelIdSource != null)
						{
							child.HotelId = child.HotelIdSource.Id;
						}
						else
						{
							child.HotelId = entity.Id;
						}

					}

					if (entity.PackageByHotelCollection.Count > 0 || entity.PackageByHotelCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PackageByHotelProvider.Save(transactionManager, entity.PackageByHotelCollection);
						
						deepHandles.Add("PackageByHotelCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PackageByHotel >) DataRepository.PackageByHotelProvider.DeepSave,
							new object[] { transactionManager, entity.PackageByHotelCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<HotelDesc>
				if (CanDeepSave(entity.HotelDescCollection, "List<HotelDesc>|HotelDescCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(HotelDesc child in entity.HotelDescCollection)
					{
						if(child.HotelIdSource != null)
						{
							child.HotelId = child.HotelIdSource.Id;
						}
						else
						{
							child.HotelId = entity.Id;
						}

					}

					if (entity.HotelDescCollection.Count > 0 || entity.HotelDescCollection.DeletedItems.Count > 0)
					{
						//DataRepository.HotelDescProvider.Save(transactionManager, entity.HotelDescCollection);
						
						deepHandles.Add("HotelDescCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< HotelDesc >) DataRepository.HotelDescProvider.DeepSave,
							new object[] { transactionManager, entity.HotelDescCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<BedRoom>
				if (CanDeepSave(entity.BedRoomCollection, "List<BedRoom>|BedRoomCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(BedRoom child in entity.BedRoomCollection)
					{
						if(child.HotelIdSource != null)
						{
							child.HotelId = child.HotelIdSource.Id;
						}
						else
						{
							child.HotelId = entity.Id;
						}

					}

					if (entity.BedRoomCollection.Count > 0 || entity.BedRoomCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BedRoomProvider.Save(transactionManager, entity.BedRoomCollection);
						
						deepHandles.Add("BedRoomCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< BedRoom >) DataRepository.BedRoomProvider.DeepSave,
							new object[] { transactionManager, entity.BedRoomCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<HotelContact>
				if (CanDeepSave(entity.HotelContactCollection, "List<HotelContact>|HotelContactCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(HotelContact child in entity.HotelContactCollection)
					{
						if(child.HotelIdSource != null)
						{
							child.HotelId = child.HotelIdSource.Id;
						}
						else
						{
							child.HotelId = entity.Id;
						}

					}

					if (entity.HotelContactCollection.Count > 0 || entity.HotelContactCollection.DeletedItems.Count > 0)
					{
						//DataRepository.HotelContactProvider.Save(transactionManager, entity.HotelContactCollection);
						
						deepHandles.Add("HotelContactCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< HotelContact >) DataRepository.HotelContactProvider.DeepSave,
							new object[] { transactionManager, entity.HotelContactCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<SpecialPriceForBedroom>
				if (CanDeepSave(entity.SpecialPriceForBedroomCollection, "List<SpecialPriceForBedroom>|SpecialPriceForBedroomCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SpecialPriceForBedroom child in entity.SpecialPriceForBedroomCollection)
					{
						if(child.HotelIdSource != null)
						{
							child.HotelId = child.HotelIdSource.Id;
						}
						else
						{
							child.HotelId = entity.Id;
						}

					}

					if (entity.SpecialPriceForBedroomCollection.Count > 0 || entity.SpecialPriceForBedroomCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SpecialPriceForBedroomProvider.Save(transactionManager, entity.SpecialPriceForBedroomCollection);
						
						deepHandles.Add("SpecialPriceForBedroomCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SpecialPriceForBedroom >) DataRepository.SpecialPriceForBedroomProvider.DeepSave,
							new object[] { transactionManager, entity.SpecialPriceForBedroomCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<PolicyHotelMapping>
				if (CanDeepSave(entity.PolicyHotelMappingCollection, "List<PolicyHotelMapping>|PolicyHotelMappingCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PolicyHotelMapping child in entity.PolicyHotelMappingCollection)
					{
						if(child.HotelIdSource != null)
						{
							child.HotelId = child.HotelIdSource.Id;
						}
						else
						{
							child.HotelId = entity.Id;
						}

					}

					if (entity.PolicyHotelMappingCollection.Count > 0 || entity.PolicyHotelMappingCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PolicyHotelMappingProvider.Save(transactionManager, entity.PolicyHotelMappingCollection);
						
						deepHandles.Add("PolicyHotelMappingCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PolicyHotelMapping >) DataRepository.PolicyHotelMappingProvider.DeepSave,
							new object[] { transactionManager, entity.PolicyHotelMappingCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<HotelTrail>
				if (CanDeepSave(entity.HotelTrailCollection, "List<HotelTrail>|HotelTrailCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(HotelTrail child in entity.HotelTrailCollection)
					{
						if(child.HotelIdSource != null)
						{
							child.HotelId = child.HotelIdSource.Id;
						}
						else
						{
							child.HotelId = entity.Id;
						}

					}

					if (entity.HotelTrailCollection.Count > 0 || entity.HotelTrailCollection.DeletedItems.Count > 0)
					{
						//DataRepository.HotelTrailProvider.Save(transactionManager, entity.HotelTrailCollection);
						
						deepHandles.Add("HotelTrailCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< HotelTrail >) DataRepository.HotelTrailProvider.DeepSave,
							new object[] { transactionManager, entity.HotelTrailCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<MeetingRoomConfig>
				if (CanDeepSave(entity.MeetingRoomConfigCollection, "List<MeetingRoomConfig>|MeetingRoomConfigCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(MeetingRoomConfig child in entity.MeetingRoomConfigCollection)
					{
						if(child.HotelIdSource != null)
						{
							child.HotelId = child.HotelIdSource.Id;
						}
						else
						{
							child.HotelId = entity.Id;
						}

					}

					if (entity.MeetingRoomConfigCollection.Count > 0 || entity.MeetingRoomConfigCollection.DeletedItems.Count > 0)
					{
						//DataRepository.MeetingRoomConfigProvider.Save(transactionManager, entity.MeetingRoomConfigCollection);
						
						deepHandles.Add("MeetingRoomConfigCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< MeetingRoomConfig >) DataRepository.MeetingRoomConfigProvider.DeepSave,
							new object[] { transactionManager, entity.MeetingRoomConfigCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ServeyResponse>
				if (CanDeepSave(entity.ServeyResponseCollection, "List<ServeyResponse>|ServeyResponseCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ServeyResponse child in entity.ServeyResponseCollection)
					{
						if(child.VanueIdSource != null)
						{
							child.VanueId = child.VanueIdSource.Id;
						}
						else
						{
							child.VanueId = entity.Id;
						}

					}

					if (entity.ServeyResponseCollection.Count > 0 || entity.ServeyResponseCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ServeyResponseProvider.Save(transactionManager, entity.ServeyResponseCollection);
						
						deepHandles.Add("ServeyResponseCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ServeyResponse >) DataRepository.ServeyResponseProvider.DeepSave,
							new object[] { transactionManager, entity.ServeyResponseCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<HotelOwnerLink>
				if (CanDeepSave(entity.HotelOwnerLinkCollection, "List<HotelOwnerLink>|HotelOwnerLinkCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(HotelOwnerLink child in entity.HotelOwnerLinkCollection)
					{
						if(child.HotelIdSource != null)
						{
							child.HotelId = child.HotelIdSource.Id;
						}
						else
						{
							child.HotelId = entity.Id;
						}

					}

					if (entity.HotelOwnerLinkCollection.Count > 0 || entity.HotelOwnerLinkCollection.DeletedItems.Count > 0)
					{
						//DataRepository.HotelOwnerLinkProvider.Save(transactionManager, entity.HotelOwnerLinkCollection);
						
						deepHandles.Add("HotelOwnerLinkCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< HotelOwnerLink >) DataRepository.HotelOwnerLinkProvider.DeepSave,
							new object[] { transactionManager, entity.HotelOwnerLinkCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<LeftMenuLink>
				if (CanDeepSave(entity.LeftMenuLinkCollection, "List<LeftMenuLink>|LeftMenuLinkCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(LeftMenuLink child in entity.LeftMenuLinkCollection)
					{
						if(child.HotelIdSource != null)
						{
							child.HotelId = child.HotelIdSource.Id;
						}
						else
						{
							child.HotelId = entity.Id;
						}

					}

					if (entity.LeftMenuLinkCollection.Count > 0 || entity.LeftMenuLinkCollection.DeletedItems.Count > 0)
					{
						//DataRepository.LeftMenuLinkProvider.Save(transactionManager, entity.LeftMenuLinkCollection);
						
						deepHandles.Add("LeftMenuLinkCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< LeftMenuLink >) DataRepository.LeftMenuLinkProvider.DeepSave,
							new object[] { transactionManager, entity.LeftMenuLinkCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Booking>
				if (CanDeepSave(entity.BookingCollection, "List<Booking>|BookingCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Booking child in entity.BookingCollection)
					{
						if(child.HotelIdSource != null)
						{
							child.HotelId = child.HotelIdSource.Id;
						}
						else
						{
							child.HotelId = entity.Id;
						}

					}

					if (entity.BookingCollection.Count > 0 || entity.BookingCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BookingProvider.Save(transactionManager, entity.BookingCollection);
						
						deepHandles.Add("BookingCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Booking >) DataRepository.BookingProvider.DeepSave,
							new object[] { transactionManager, entity.BookingCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<DashboardLink>
				if (CanDeepSave(entity.DashboardLinkCollection, "List<DashboardLink>|DashboardLinkCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(DashboardLink child in entity.DashboardLinkCollection)
					{
						if(child.HotelIdSource != null)
						{
							child.HotelId = child.HotelIdSource.Id;
						}
						else
						{
							child.HotelId = entity.Id;
						}

					}

					if (entity.DashboardLinkCollection.Count > 0 || entity.DashboardLinkCollection.DeletedItems.Count > 0)
					{
						//DataRepository.DashboardLinkProvider.Save(transactionManager, entity.DashboardLinkCollection);
						
						deepHandles.Add("DashboardLinkCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< DashboardLink >) DataRepository.DashboardLinkProvider.DeepSave,
							new object[] { transactionManager, entity.DashboardLinkCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<PriorityBasket>
				if (CanDeepSave(entity.PriorityBasketCollection, "List<PriorityBasket>|PriorityBasketCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PriorityBasket child in entity.PriorityBasketCollection)
					{
						if(child.HotelIdSource != null)
						{
							child.HotelId = child.HotelIdSource.Id;
						}
						else
						{
							child.HotelId = entity.Id;
						}

					}

					if (entity.PriorityBasketCollection.Count > 0 || entity.PriorityBasketCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PriorityBasketProvider.Save(transactionManager, entity.PriorityBasketCollection);
						
						deepHandles.Add("PriorityBasketCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PriorityBasket >) DataRepository.PriorityBasketProvider.DeepSave,
							new object[] { transactionManager, entity.PriorityBasketCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<HotelFacilities>
				if (CanDeepSave(entity.HotelFacilitiesCollection, "List<HotelFacilities>|HotelFacilitiesCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(HotelFacilities child in entity.HotelFacilitiesCollection)
					{
						if(child.HotelIdSource != null)
						{
							child.HotelId = child.HotelIdSource.Id;
						}
						else
						{
							child.HotelId = entity.Id;
						}

					}

					if (entity.HotelFacilitiesCollection.Count > 0 || entity.HotelFacilitiesCollection.DeletedItems.Count > 0)
					{
						//DataRepository.HotelFacilitiesProvider.Save(transactionManager, entity.HotelFacilitiesCollection);
						
						deepHandles.Add("HotelFacilitiesCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< HotelFacilities >) DataRepository.HotelFacilitiesProvider.DeepSave,
							new object[] { transactionManager, entity.HotelFacilitiesCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<BookingLogs>
				if (CanDeepSave(entity.BookingLogsCollection, "List<BookingLogs>|BookingLogsCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(BookingLogs child in entity.BookingLogsCollection)
					{
						if(child.HotelIdSource != null)
						{
							child.HotelId = child.HotelIdSource.Id;
						}
						else
						{
							child.HotelId = entity.Id;
						}

					}

					if (entity.BookingLogsCollection.Count > 0 || entity.BookingLogsCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BookingLogsProvider.Save(transactionManager, entity.BookingLogsCollection);
						
						deepHandles.Add("BookingLogsCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< BookingLogs >) DataRepository.BookingLogsProvider.DeepSave,
							new object[] { transactionManager, entity.BookingLogsCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<WhiteLabelMappingWithHotel>
				if (CanDeepSave(entity.WhiteLabelMappingWithHotelCollection, "List<WhiteLabelMappingWithHotel>|WhiteLabelMappingWithHotelCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(WhiteLabelMappingWithHotel child in entity.WhiteLabelMappingWithHotelCollection)
					{
						if(child.HotelIdSource != null)
						{
							child.HotelId = child.HotelIdSource.Id;
						}
						else
						{
							child.HotelId = entity.Id;
						}

					}

					if (entity.WhiteLabelMappingWithHotelCollection.Count > 0 || entity.WhiteLabelMappingWithHotelCollection.DeletedItems.Count > 0)
					{
						//DataRepository.WhiteLabelMappingWithHotelProvider.Save(transactionManager, entity.WhiteLabelMappingWithHotelCollection);
						
						deepHandles.Add("WhiteLabelMappingWithHotelCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< WhiteLabelMappingWithHotel >) DataRepository.WhiteLabelMappingWithHotelProvider.DeepSave,
							new object[] { transactionManager, entity.WhiteLabelMappingWithHotelCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region HotelChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.Hotel</c>
	///</summary>
	public enum HotelChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Group</c> at GroupIdSource
		///</summary>
		[ChildEntityType(typeof(Group))]
		Group,
			
		///<summary>
		/// Composite Property for <c>Users</c> at UpdatedBySource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
	
		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for SpecialPriceAndPromoCollection
		///</summary>
		[ChildEntityType(typeof(TList<SpecialPriceAndPromo>))]
		SpecialPriceAndPromoCollection,

		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for StatisticsCollection
		///</summary>
		[ChildEntityType(typeof(TList<Statistics>))]
		StatisticsCollection,

		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for MeetingRoomCollection
		///</summary>
		[ChildEntityType(typeof(TList<MeetingRoom>))]
		MeetingRoomCollection,

		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for AvailabilityCollection
		///</summary>
		[ChildEntityType(typeof(TList<Availability>))]
		AvailabilityCollection,

		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for AgencyInvoiceCollection
		///</summary>
		[ChildEntityType(typeof(TList<AgencyInvoice>))]
		AgencyInvoiceCollection,

		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for OtherItemsPricingbyHotelCollection
		///</summary>
		[ChildEntityType(typeof(TList<OtherItemsPricingbyHotel>))]
		OtherItemsPricingbyHotelCollection,

		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for InvoiceCollection
		///</summary>
		[ChildEntityType(typeof(TList<Invoice>))]
		InvoiceCollection,

		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for HotelPhotoVideoGallaryCollection
		///</summary>
		[ChildEntityType(typeof(TList<HotelPhotoVideoGallary>))]
		HotelPhotoVideoGallaryCollection,

		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for ActualPackagePriceCollection
		///</summary>
		[ChildEntityType(typeof(TList<ActualPackagePrice>))]
		ActualPackagePriceCollection,

		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for PackageByHotelCollection
		///</summary>
		[ChildEntityType(typeof(TList<PackageByHotel>))]
		PackageByHotelCollection,

		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for HotelDescCollection
		///</summary>
		[ChildEntityType(typeof(TList<HotelDesc>))]
		HotelDescCollection,

		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for BedRoomCollection
		///</summary>
		[ChildEntityType(typeof(TList<BedRoom>))]
		BedRoomCollection,

		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for HotelContactCollection
		///</summary>
		[ChildEntityType(typeof(TList<HotelContact>))]
		HotelContactCollection,

		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for SpecialPriceForBedroomCollection
		///</summary>
		[ChildEntityType(typeof(TList<SpecialPriceForBedroom>))]
		SpecialPriceForBedroomCollection,

		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for PolicyHotelMappingCollection
		///</summary>
		[ChildEntityType(typeof(TList<PolicyHotelMapping>))]
		PolicyHotelMappingCollection,

		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for HotelTrailCollection
		///</summary>
		[ChildEntityType(typeof(TList<HotelTrail>))]
		HotelTrailCollection,

		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for MeetingRoomConfigCollection
		///</summary>
		[ChildEntityType(typeof(TList<MeetingRoomConfig>))]
		MeetingRoomConfigCollection,

		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for ServeyResponseCollection
		///</summary>
		[ChildEntityType(typeof(TList<ServeyResponse>))]
		ServeyResponseCollection,

		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for HotelOwnerLinkCollection
		///</summary>
		[ChildEntityType(typeof(TList<HotelOwnerLink>))]
		HotelOwnerLinkCollection,

		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for LeftMenuLinkCollection
		///</summary>
		[ChildEntityType(typeof(TList<LeftMenuLink>))]
		LeftMenuLinkCollection,

		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for BookingCollection
		///</summary>
		[ChildEntityType(typeof(TList<Booking>))]
		BookingCollection,

		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for DashboardLinkCollection
		///</summary>
		[ChildEntityType(typeof(TList<DashboardLink>))]
		DashboardLinkCollection,

		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for PriorityBasketCollection
		///</summary>
		[ChildEntityType(typeof(TList<PriorityBasket>))]
		PriorityBasketCollection,

		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for HotelFacilitiesCollection
		///</summary>
		[ChildEntityType(typeof(TList<HotelFacilities>))]
		HotelFacilitiesCollection,

		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for BookingLogsCollection
		///</summary>
		[ChildEntityType(typeof(TList<BookingLogs>))]
		BookingLogsCollection,

		///<summary>
		/// Collection of <c>Hotel</c> as OneToMany for WhiteLabelMappingWithHotelCollection
		///</summary>
		[ChildEntityType(typeof(TList<WhiteLabelMappingWithHotel>))]
		WhiteLabelMappingWithHotelCollection,
	}
	
	#endregion HotelChildEntityTypes
	
	#region HotelFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;HotelColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Hotel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelFilterBuilder : SqlFilterBuilder<HotelColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelFilterBuilder class.
		/// </summary>
		public HotelFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelFilterBuilder
	
	#region HotelParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;HotelColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Hotel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelParameterBuilder : ParameterizedSqlFilterBuilder<HotelColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelParameterBuilder class.
		/// </summary>
		public HotelParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelParameterBuilder
	
	#region HotelSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;HotelColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Hotel"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class HotelSortBuilder : SqlSortBuilder<HotelColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelSqlSortBuilder class.
		/// </summary>
		public HotelSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion HotelSortBuilder
	
} // end namespace
