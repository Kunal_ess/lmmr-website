﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="StaffAccountProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class StaffAccountProviderBaseCore : EntityProviderBase<LMMR.Entities.StaffAccount, LMMR.Entities.StaffAccountKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.StaffAccountKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_StaffAccount_Roles key.
		///		FK_StaffAccount_Roles Description: 
		/// </summary>
		/// <param name="_role"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.StaffAccount objects.</returns>
		public TList<StaffAccount> GetByRole(System.Int64 _role)
		{
			int count = -1;
			return GetByRole(_role, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_StaffAccount_Roles key.
		///		FK_StaffAccount_Roles Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_role"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.StaffAccount objects.</returns>
		/// <remarks></remarks>
		public TList<StaffAccount> GetByRole(TransactionManager transactionManager, System.Int64 _role)
		{
			int count = -1;
			return GetByRole(transactionManager, _role, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_StaffAccount_Roles key.
		///		FK_StaffAccount_Roles Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_role"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.StaffAccount objects.</returns>
		public TList<StaffAccount> GetByRole(TransactionManager transactionManager, System.Int64 _role, int start, int pageLength)
		{
			int count = -1;
			return GetByRole(transactionManager, _role, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_StaffAccount_Roles key.
		///		fkStaffAccountRoles Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_role"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.StaffAccount objects.</returns>
		public TList<StaffAccount> GetByRole(System.Int64 _role, int start, int pageLength)
		{
			int count =  -1;
			return GetByRole(null, _role, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_StaffAccount_Roles key.
		///		fkStaffAccountRoles Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_role"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.StaffAccount objects.</returns>
		public TList<StaffAccount> GetByRole(System.Int64 _role, int start, int pageLength,out int count)
		{
			return GetByRole(null, _role, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_StaffAccount_Roles key.
		///		FK_StaffAccount_Roles Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_role"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.StaffAccount objects.</returns>
		public abstract TList<StaffAccount> GetByRole(TransactionManager transactionManager, System.Int64 _role, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.StaffAccount Get(TransactionManager transactionManager, LMMR.Entities.StaffAccountKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_StaffAccount index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.StaffAccount"/> class.</returns>
		public LMMR.Entities.StaffAccount GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_StaffAccount index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.StaffAccount"/> class.</returns>
		public LMMR.Entities.StaffAccount GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_StaffAccount index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.StaffAccount"/> class.</returns>
		public LMMR.Entities.StaffAccount GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_StaffAccount index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.StaffAccount"/> class.</returns>
		public LMMR.Entities.StaffAccount GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_StaffAccount index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.StaffAccount"/> class.</returns>
		public LMMR.Entities.StaffAccount GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_StaffAccount index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.StaffAccount"/> class.</returns>
		public abstract LMMR.Entities.StaffAccount GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;StaffAccount&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;StaffAccount&gt;"/></returns>
		public static TList<StaffAccount> Fill(IDataReader reader, TList<StaffAccount> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.StaffAccount c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("StaffAccount")
					.Append("|").Append((System.Int64)reader[((int)StaffAccountColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<StaffAccount>(
					key.ToString(), // EntityTrackingKey
					"StaffAccount",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.StaffAccount();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)StaffAccountColumn.Id - 1)];
					c.StaffName = (reader.IsDBNull(((int)StaffAccountColumn.StaffName - 1)))?null:(System.String)reader[((int)StaffAccountColumn.StaffName - 1)];
					c.EmailId = (reader.IsDBNull(((int)StaffAccountColumn.EmailId - 1)))?null:(System.String)reader[((int)StaffAccountColumn.EmailId - 1)];
					c.Password = (reader.IsDBNull(((int)StaffAccountColumn.Password - 1)))?null:(System.String)reader[((int)StaffAccountColumn.Password - 1)];
					c.Role = (System.Int64)reader[((int)StaffAccountColumn.Role - 1)];
					c.CreationDate = (reader.IsDBNull(((int)StaffAccountColumn.CreationDate - 1)))?null:(System.DateTime?)reader[((int)StaffAccountColumn.CreationDate - 1)];
					c.IsActive = (System.Boolean)reader[((int)StaffAccountColumn.IsActive - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.StaffAccount"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.StaffAccount"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.StaffAccount entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)StaffAccountColumn.Id - 1)];
			entity.StaffName = (reader.IsDBNull(((int)StaffAccountColumn.StaffName - 1)))?null:(System.String)reader[((int)StaffAccountColumn.StaffName - 1)];
			entity.EmailId = (reader.IsDBNull(((int)StaffAccountColumn.EmailId - 1)))?null:(System.String)reader[((int)StaffAccountColumn.EmailId - 1)];
			entity.Password = (reader.IsDBNull(((int)StaffAccountColumn.Password - 1)))?null:(System.String)reader[((int)StaffAccountColumn.Password - 1)];
			entity.Role = (System.Int64)reader[((int)StaffAccountColumn.Role - 1)];
			entity.CreationDate = (reader.IsDBNull(((int)StaffAccountColumn.CreationDate - 1)))?null:(System.DateTime?)reader[((int)StaffAccountColumn.CreationDate - 1)];
			entity.IsActive = (System.Boolean)reader[((int)StaffAccountColumn.IsActive - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.StaffAccount"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.StaffAccount"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.StaffAccount entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.StaffName = Convert.IsDBNull(dataRow["StaffName"]) ? null : (System.String)dataRow["StaffName"];
			entity.EmailId = Convert.IsDBNull(dataRow["EmailId"]) ? null : (System.String)dataRow["EmailId"];
			entity.Password = Convert.IsDBNull(dataRow["Password"]) ? null : (System.String)dataRow["Password"];
			entity.Role = (System.Int64)dataRow["Role"];
			entity.CreationDate = Convert.IsDBNull(dataRow["CreationDate"]) ? null : (System.DateTime?)dataRow["CreationDate"];
			entity.IsActive = (System.Boolean)dataRow["IsActive"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.StaffAccount"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.StaffAccount Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.StaffAccount entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region RoleSource	
			if (CanDeepLoad(entity, "Roles|RoleSource", deepLoadType, innerList) 
				&& entity.RoleSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.Role;
				Roles tmpEntity = EntityManager.LocateEntity<Roles>(EntityLocator.ConstructKeyFromPkItems(typeof(Roles), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.RoleSource = tmpEntity;
				else
					entity.RoleSource = DataRepository.RolesProvider.GetById(transactionManager, entity.Role);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'RoleSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.RoleSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.RolesProvider.DeepLoad(transactionManager, entity.RoleSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion RoleSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.StaffAccount object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.StaffAccount instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.StaffAccount Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.StaffAccount entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region RoleSource
			if (CanDeepSave(entity, "Roles|RoleSource", deepSaveType, innerList) 
				&& entity.RoleSource != null)
			{
				DataRepository.RolesProvider.Save(transactionManager, entity.RoleSource);
				entity.Role = entity.RoleSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region StaffAccountChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.StaffAccount</c>
	///</summary>
	public enum StaffAccountChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Roles</c> at RoleSource
		///</summary>
		[ChildEntityType(typeof(Roles))]
		Roles,
		}
	
	#endregion StaffAccountChildEntityTypes
	
	#region StaffAccountFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;StaffAccountColumn&gt;"/> class
	/// that is used exclusively with a <see cref="StaffAccount"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class StaffAccountFilterBuilder : SqlFilterBuilder<StaffAccountColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the StaffAccountFilterBuilder class.
		/// </summary>
		public StaffAccountFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the StaffAccountFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public StaffAccountFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the StaffAccountFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public StaffAccountFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion StaffAccountFilterBuilder
	
	#region StaffAccountParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;StaffAccountColumn&gt;"/> class
	/// that is used exclusively with a <see cref="StaffAccount"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class StaffAccountParameterBuilder : ParameterizedSqlFilterBuilder<StaffAccountColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the StaffAccountParameterBuilder class.
		/// </summary>
		public StaffAccountParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the StaffAccountParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public StaffAccountParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the StaffAccountParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public StaffAccountParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion StaffAccountParameterBuilder
	
	#region StaffAccountSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;StaffAccountColumn&gt;"/> class
	/// that is used exclusively with a <see cref="StaffAccount"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class StaffAccountSortBuilder : SqlSortBuilder<StaffAccountColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the StaffAccountSqlSortBuilder class.
		/// </summary>
		public StaffAccountSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion StaffAccountSortBuilder
	
} // end namespace
