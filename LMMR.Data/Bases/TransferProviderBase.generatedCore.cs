﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="TransferProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class TransferProviderBaseCore : EntityProviderBase<LMMR.Entities.Transfer, LMMR.Entities.TransferKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.TransferKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Transfer_Booking key.
		///		FK_Transfer_Booking Description: 
		/// </summary>
		/// <param name="_oldBookingId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Transfer objects.</returns>
		public TList<Transfer> GetByOldBookingId(System.Int64 _oldBookingId)
		{
			int count = -1;
			return GetByOldBookingId(_oldBookingId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Transfer_Booking key.
		///		FK_Transfer_Booking Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_oldBookingId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Transfer objects.</returns>
		/// <remarks></remarks>
		public TList<Transfer> GetByOldBookingId(TransactionManager transactionManager, System.Int64 _oldBookingId)
		{
			int count = -1;
			return GetByOldBookingId(transactionManager, _oldBookingId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Transfer_Booking key.
		///		FK_Transfer_Booking Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_oldBookingId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Transfer objects.</returns>
		public TList<Transfer> GetByOldBookingId(TransactionManager transactionManager, System.Int64 _oldBookingId, int start, int pageLength)
		{
			int count = -1;
			return GetByOldBookingId(transactionManager, _oldBookingId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Transfer_Booking key.
		///		fkTransferBooking Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_oldBookingId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Transfer objects.</returns>
		public TList<Transfer> GetByOldBookingId(System.Int64 _oldBookingId, int start, int pageLength)
		{
			int count =  -1;
			return GetByOldBookingId(null, _oldBookingId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Transfer_Booking key.
		///		fkTransferBooking Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_oldBookingId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Transfer objects.</returns>
		public TList<Transfer> GetByOldBookingId(System.Int64 _oldBookingId, int start, int pageLength,out int count)
		{
			return GetByOldBookingId(null, _oldBookingId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Transfer_Booking key.
		///		FK_Transfer_Booking Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_oldBookingId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.Transfer objects.</returns>
		public abstract TList<Transfer> GetByOldBookingId(TransactionManager transactionManager, System.Int64 _oldBookingId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Transfer_Booking1 key.
		///		FK_Transfer_Booking1 Description: 
		/// </summary>
		/// <param name="_newBookingId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Transfer objects.</returns>
		public TList<Transfer> GetByNewBookingId(System.Int64 _newBookingId)
		{
			int count = -1;
			return GetByNewBookingId(_newBookingId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Transfer_Booking1 key.
		///		FK_Transfer_Booking1 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_newBookingId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Transfer objects.</returns>
		/// <remarks></remarks>
		public TList<Transfer> GetByNewBookingId(TransactionManager transactionManager, System.Int64 _newBookingId)
		{
			int count = -1;
			return GetByNewBookingId(transactionManager, _newBookingId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Transfer_Booking1 key.
		///		FK_Transfer_Booking1 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_newBookingId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Transfer objects.</returns>
		public TList<Transfer> GetByNewBookingId(TransactionManager transactionManager, System.Int64 _newBookingId, int start, int pageLength)
		{
			int count = -1;
			return GetByNewBookingId(transactionManager, _newBookingId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Transfer_Booking1 key.
		///		fkTransferBooking1 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_newBookingId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Transfer objects.</returns>
		public TList<Transfer> GetByNewBookingId(System.Int64 _newBookingId, int start, int pageLength)
		{
			int count =  -1;
			return GetByNewBookingId(null, _newBookingId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Transfer_Booking1 key.
		///		fkTransferBooking1 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_newBookingId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Transfer objects.</returns>
		public TList<Transfer> GetByNewBookingId(System.Int64 _newBookingId, int start, int pageLength,out int count)
		{
			return GetByNewBookingId(null, _newBookingId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Transfer_Booking1 key.
		///		FK_Transfer_Booking1 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_newBookingId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.Transfer objects.</returns>
		public abstract TList<Transfer> GetByNewBookingId(TransactionManager transactionManager, System.Int64 _newBookingId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.Transfer Get(TransactionManager transactionManager, LMMR.Entities.TransferKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Transfer index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Transfer"/> class.</returns>
		public LMMR.Entities.Transfer GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Transfer index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Transfer"/> class.</returns>
		public LMMR.Entities.Transfer GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Transfer index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Transfer"/> class.</returns>
		public LMMR.Entities.Transfer GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Transfer index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Transfer"/> class.</returns>
		public LMMR.Entities.Transfer GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Transfer index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Transfer"/> class.</returns>
		public LMMR.Entities.Transfer GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Transfer index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Transfer"/> class.</returns>
		public abstract LMMR.Entities.Transfer GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Transfer&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Transfer&gt;"/></returns>
		public static TList<Transfer> Fill(IDataReader reader, TList<Transfer> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.Transfer c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Transfer")
					.Append("|").Append((System.Int64)reader[((int)TransferColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Transfer>(
					key.ToString(), // EntityTrackingKey
					"Transfer",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.Transfer();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)TransferColumn.Id - 1)];
					c.OldBookingId = (System.Int64)reader[((int)TransferColumn.OldBookingId - 1)];
					c.NewBookingId = (System.Int64)reader[((int)TransferColumn.NewBookingId - 1)];
					c.TransferDate = (reader.IsDBNull(((int)TransferColumn.TransferDate - 1)))?null:(System.DateTime?)reader[((int)TransferColumn.TransferDate - 1)];
					c.TransferBy = (System.Int64)reader[((int)TransferColumn.TransferBy - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Transfer"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Transfer"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.Transfer entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)TransferColumn.Id - 1)];
			entity.OldBookingId = (System.Int64)reader[((int)TransferColumn.OldBookingId - 1)];
			entity.NewBookingId = (System.Int64)reader[((int)TransferColumn.NewBookingId - 1)];
			entity.TransferDate = (reader.IsDBNull(((int)TransferColumn.TransferDate - 1)))?null:(System.DateTime?)reader[((int)TransferColumn.TransferDate - 1)];
			entity.TransferBy = (System.Int64)reader[((int)TransferColumn.TransferBy - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Transfer"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Transfer"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.Transfer entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.OldBookingId = (System.Int64)dataRow["OldBookingId"];
			entity.NewBookingId = (System.Int64)dataRow["NewBookingId"];
			entity.TransferDate = Convert.IsDBNull(dataRow["TransferDate"]) ? null : (System.DateTime?)dataRow["TransferDate"];
			entity.TransferBy = (System.Int64)dataRow["TransferBy"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Transfer"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.Transfer Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.Transfer entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region OldBookingIdSource	
			if (CanDeepLoad(entity, "Booking|OldBookingIdSource", deepLoadType, innerList) 
				&& entity.OldBookingIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.OldBookingId;
				Booking tmpEntity = EntityManager.LocateEntity<Booking>(EntityLocator.ConstructKeyFromPkItems(typeof(Booking), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.OldBookingIdSource = tmpEntity;
				else
					entity.OldBookingIdSource = DataRepository.BookingProvider.GetById(transactionManager, entity.OldBookingId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OldBookingIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.OldBookingIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.BookingProvider.DeepLoad(transactionManager, entity.OldBookingIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion OldBookingIdSource

			#region NewBookingIdSource	
			if (CanDeepLoad(entity, "Booking|NewBookingIdSource", deepLoadType, innerList) 
				&& entity.NewBookingIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.NewBookingId;
				Booking tmpEntity = EntityManager.LocateEntity<Booking>(EntityLocator.ConstructKeyFromPkItems(typeof(Booking), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.NewBookingIdSource = tmpEntity;
				else
					entity.NewBookingIdSource = DataRepository.BookingProvider.GetById(transactionManager, entity.NewBookingId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'NewBookingIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.NewBookingIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.BookingProvider.DeepLoad(transactionManager, entity.NewBookingIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion NewBookingIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.Transfer object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.Transfer instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.Transfer Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.Transfer entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region OldBookingIdSource
			if (CanDeepSave(entity, "Booking|OldBookingIdSource", deepSaveType, innerList) 
				&& entity.OldBookingIdSource != null)
			{
				DataRepository.BookingProvider.Save(transactionManager, entity.OldBookingIdSource);
				entity.OldBookingId = entity.OldBookingIdSource.Id;
			}
			#endregion 
			
			#region NewBookingIdSource
			if (CanDeepSave(entity, "Booking|NewBookingIdSource", deepSaveType, innerList) 
				&& entity.NewBookingIdSource != null)
			{
				DataRepository.BookingProvider.Save(transactionManager, entity.NewBookingIdSource);
				entity.NewBookingId = entity.NewBookingIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region TransferChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.Transfer</c>
	///</summary>
	public enum TransferChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Booking</c> at OldBookingIdSource
		///</summary>
		[ChildEntityType(typeof(Booking))]
		Booking,
		}
	
	#endregion TransferChildEntityTypes
	
	#region TransferFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;TransferColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Transfer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TransferFilterBuilder : SqlFilterBuilder<TransferColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TransferFilterBuilder class.
		/// </summary>
		public TransferFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TransferFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TransferFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TransferFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TransferFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TransferFilterBuilder
	
	#region TransferParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;TransferColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Transfer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TransferParameterBuilder : ParameterizedSqlFilterBuilder<TransferColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TransferParameterBuilder class.
		/// </summary>
		public TransferParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TransferParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TransferParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TransferParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TransferParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TransferParameterBuilder
	
	#region TransferSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;TransferColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Transfer"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class TransferSortBuilder : SqlSortBuilder<TransferColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TransferSqlSortBuilder class.
		/// </summary>
		public TransferSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion TransferSortBuilder
	
} // end namespace
