﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="HoteldescTrailProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class HoteldescTrailProviderBaseCore : EntityProviderBase<LMMR.Entities.HoteldescTrail, LMMR.Entities.HoteldescTrailKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.HoteldescTrailKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.HoteldescTrail Get(TransactionManager transactionManager, LMMR.Entities.HoteldescTrailKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Hoteldesc_trails index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HoteldescTrail"/> class.</returns>
		public LMMR.Entities.HoteldescTrail GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Hoteldesc_trails index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HoteldescTrail"/> class.</returns>
		public LMMR.Entities.HoteldescTrail GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Hoteldesc_trails index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HoteldescTrail"/> class.</returns>
		public LMMR.Entities.HoteldescTrail GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Hoteldesc_trails index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HoteldescTrail"/> class.</returns>
		public LMMR.Entities.HoteldescTrail GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Hoteldesc_trails index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HoteldescTrail"/> class.</returns>
		public LMMR.Entities.HoteldescTrail GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Hoteldesc_trails index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HoteldescTrail"/> class.</returns>
		public abstract LMMR.Entities.HoteldescTrail GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;HoteldescTrail&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;HoteldescTrail&gt;"/></returns>
		public static TList<HoteldescTrail> Fill(IDataReader reader, TList<HoteldescTrail> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.HoteldescTrail c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("HoteldescTrail")
					.Append("|").Append((System.Int64)reader[((int)HoteldescTrailColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<HoteldescTrail>(
					key.ToString(), // EntityTrackingKey
					"HoteldescTrail",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.HoteldescTrail();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)HoteldescTrailColumn.Id - 1)];
					c.HotelDescId = (System.Int64)reader[((int)HoteldescTrailColumn.HotelDescId - 1)];
					c.HotelId = (System.Int64)reader[((int)HoteldescTrailColumn.HotelId - 1)];
					c.LanguageId = (System.Int64)reader[((int)HoteldescTrailColumn.LanguageId - 1)];
					c.Description = (reader.IsDBNull(((int)HoteldescTrailColumn.Description - 1)))?null:(System.String)reader[((int)HoteldescTrailColumn.Description - 1)];
					c.Address = (reader.IsDBNull(((int)HoteldescTrailColumn.Address - 1)))?null:(System.String)reader[((int)HoteldescTrailColumn.Address - 1)];
					c.UpdateDate = (reader.IsDBNull(((int)HoteldescTrailColumn.UpdateDate - 1)))?null:(System.DateTime?)reader[((int)HoteldescTrailColumn.UpdateDate - 1)];
					c.UpdatedBy = (reader.IsDBNull(((int)HoteldescTrailColumn.UpdatedBy - 1)))?null:(System.Int32?)reader[((int)HoteldescTrailColumn.UpdatedBy - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.HoteldescTrail"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.HoteldescTrail"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.HoteldescTrail entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)HoteldescTrailColumn.Id - 1)];
			entity.HotelDescId = (System.Int64)reader[((int)HoteldescTrailColumn.HotelDescId - 1)];
			entity.HotelId = (System.Int64)reader[((int)HoteldescTrailColumn.HotelId - 1)];
			entity.LanguageId = (System.Int64)reader[((int)HoteldescTrailColumn.LanguageId - 1)];
			entity.Description = (reader.IsDBNull(((int)HoteldescTrailColumn.Description - 1)))?null:(System.String)reader[((int)HoteldescTrailColumn.Description - 1)];
			entity.Address = (reader.IsDBNull(((int)HoteldescTrailColumn.Address - 1)))?null:(System.String)reader[((int)HoteldescTrailColumn.Address - 1)];
			entity.UpdateDate = (reader.IsDBNull(((int)HoteldescTrailColumn.UpdateDate - 1)))?null:(System.DateTime?)reader[((int)HoteldescTrailColumn.UpdateDate - 1)];
			entity.UpdatedBy = (reader.IsDBNull(((int)HoteldescTrailColumn.UpdatedBy - 1)))?null:(System.Int32?)reader[((int)HoteldescTrailColumn.UpdatedBy - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.HoteldescTrail"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.HoteldescTrail"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.HoteldescTrail entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["ID"];
			entity.HotelDescId = (System.Int64)dataRow["HotelDescID"];
			entity.HotelId = (System.Int64)dataRow["Hotel_Id"];
			entity.LanguageId = (System.Int64)dataRow["Language_Id"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.Address = Convert.IsDBNull(dataRow["Address"]) ? null : (System.String)dataRow["Address"];
			entity.UpdateDate = Convert.IsDBNull(dataRow["UpdateDate"]) ? null : (System.DateTime?)dataRow["UpdateDate"];
			entity.UpdatedBy = Convert.IsDBNull(dataRow["UpdatedBy"]) ? null : (System.Int32?)dataRow["UpdatedBy"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.HoteldescTrail"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.HoteldescTrail Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.HoteldescTrail entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.HoteldescTrail object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.HoteldescTrail instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.HoteldescTrail Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.HoteldescTrail entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region HoteldescTrailChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.HoteldescTrail</c>
	///</summary>
	public enum HoteldescTrailChildEntityTypes
	{
	}
	
	#endregion HoteldescTrailChildEntityTypes
	
	#region HoteldescTrailFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;HoteldescTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HoteldescTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HoteldescTrailFilterBuilder : SqlFilterBuilder<HoteldescTrailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HoteldescTrailFilterBuilder class.
		/// </summary>
		public HoteldescTrailFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the HoteldescTrailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HoteldescTrailFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HoteldescTrailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HoteldescTrailFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HoteldescTrailFilterBuilder
	
	#region HoteldescTrailParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;HoteldescTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HoteldescTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HoteldescTrailParameterBuilder : ParameterizedSqlFilterBuilder<HoteldescTrailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HoteldescTrailParameterBuilder class.
		/// </summary>
		public HoteldescTrailParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the HoteldescTrailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HoteldescTrailParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HoteldescTrailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HoteldescTrailParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HoteldescTrailParameterBuilder
	
	#region HoteldescTrailSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;HoteldescTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HoteldescTrail"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class HoteldescTrailSortBuilder : SqlSortBuilder<HoteldescTrailColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HoteldescTrailSqlSortBuilder class.
		/// </summary>
		public HoteldescTrailSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion HoteldescTrailSortBuilder
	
} // end namespace
