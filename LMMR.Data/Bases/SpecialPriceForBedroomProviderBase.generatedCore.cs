﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SpecialPriceForBedroomProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SpecialPriceForBedroomProviderBaseCore : EntityProviderBase<LMMR.Entities.SpecialPriceForBedroom, LMMR.Entities.SpecialPriceForBedroomKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.SpecialPriceForBedroomKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SpecialPriceForBedroom_BedRoom key.
		///		FK_SpecialPriceForBedroom_BedRoom Description: 
		/// </summary>
		/// <param name="_bedroomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SpecialPriceForBedroom objects.</returns>
		public TList<SpecialPriceForBedroom> GetByBedroomId(System.Int64? _bedroomId)
		{
			int count = -1;
			return GetByBedroomId(_bedroomId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SpecialPriceForBedroom_BedRoom key.
		///		FK_SpecialPriceForBedroom_BedRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bedroomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SpecialPriceForBedroom objects.</returns>
		/// <remarks></remarks>
		public TList<SpecialPriceForBedroom> GetByBedroomId(TransactionManager transactionManager, System.Int64? _bedroomId)
		{
			int count = -1;
			return GetByBedroomId(transactionManager, _bedroomId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_SpecialPriceForBedroom_BedRoom key.
		///		FK_SpecialPriceForBedroom_BedRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bedroomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SpecialPriceForBedroom objects.</returns>
		public TList<SpecialPriceForBedroom> GetByBedroomId(TransactionManager transactionManager, System.Int64? _bedroomId, int start, int pageLength)
		{
			int count = -1;
			return GetByBedroomId(transactionManager, _bedroomId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SpecialPriceForBedroom_BedRoom key.
		///		fkSpecialPriceForBedroomBedRoom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bedroomId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SpecialPriceForBedroom objects.</returns>
		public TList<SpecialPriceForBedroom> GetByBedroomId(System.Int64? _bedroomId, int start, int pageLength)
		{
			int count =  -1;
			return GetByBedroomId(null, _bedroomId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SpecialPriceForBedroom_BedRoom key.
		///		fkSpecialPriceForBedroomBedRoom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bedroomId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SpecialPriceForBedroom objects.</returns>
		public TList<SpecialPriceForBedroom> GetByBedroomId(System.Int64? _bedroomId, int start, int pageLength,out int count)
		{
			return GetByBedroomId(null, _bedroomId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SpecialPriceForBedroom_BedRoom key.
		///		FK_SpecialPriceForBedroom_BedRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bedroomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.SpecialPriceForBedroom objects.</returns>
		public abstract TList<SpecialPriceForBedroom> GetByBedroomId(TransactionManager transactionManager, System.Int64? _bedroomId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SpecialPriceForBedroom_SpecialPriceForBedroom key.
		///		FK_SpecialPriceForBedroom_SpecialPriceForBedroom Description: 
		/// </summary>
		/// <param name="_hotelId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SpecialPriceForBedroom objects.</returns>
		public TList<SpecialPriceForBedroom> GetByHotelId(System.Int64? _hotelId)
		{
			int count = -1;
			return GetByHotelId(_hotelId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SpecialPriceForBedroom_SpecialPriceForBedroom key.
		///		FK_SpecialPriceForBedroom_SpecialPriceForBedroom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SpecialPriceForBedroom objects.</returns>
		/// <remarks></remarks>
		public TList<SpecialPriceForBedroom> GetByHotelId(TransactionManager transactionManager, System.Int64? _hotelId)
		{
			int count = -1;
			return GetByHotelId(transactionManager, _hotelId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_SpecialPriceForBedroom_SpecialPriceForBedroom key.
		///		FK_SpecialPriceForBedroom_SpecialPriceForBedroom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SpecialPriceForBedroom objects.</returns>
		public TList<SpecialPriceForBedroom> GetByHotelId(TransactionManager transactionManager, System.Int64? _hotelId, int start, int pageLength)
		{
			int count = -1;
			return GetByHotelId(transactionManager, _hotelId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SpecialPriceForBedroom_SpecialPriceForBedroom key.
		///		fkSpecialPriceForBedroomSpecialPriceForBedroom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_hotelId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SpecialPriceForBedroom objects.</returns>
		public TList<SpecialPriceForBedroom> GetByHotelId(System.Int64? _hotelId, int start, int pageLength)
		{
			int count =  -1;
			return GetByHotelId(null, _hotelId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SpecialPriceForBedroom_SpecialPriceForBedroom key.
		///		fkSpecialPriceForBedroomSpecialPriceForBedroom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_hotelId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SpecialPriceForBedroom objects.</returns>
		public TList<SpecialPriceForBedroom> GetByHotelId(System.Int64? _hotelId, int start, int pageLength,out int count)
		{
			return GetByHotelId(null, _hotelId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SpecialPriceForBedroom_SpecialPriceForBedroom key.
		///		FK_SpecialPriceForBedroom_SpecialPriceForBedroom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.SpecialPriceForBedroom objects.</returns>
		public abstract TList<SpecialPriceForBedroom> GetByHotelId(TransactionManager transactionManager, System.Int64? _hotelId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.SpecialPriceForBedroom Get(TransactionManager transactionManager, LMMR.Entities.SpecialPriceForBedroomKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SpecialPriceForBedroom index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SpecialPriceForBedroom"/> class.</returns>
		public LMMR.Entities.SpecialPriceForBedroom GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SpecialPriceForBedroom index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SpecialPriceForBedroom"/> class.</returns>
		public LMMR.Entities.SpecialPriceForBedroom GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SpecialPriceForBedroom index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SpecialPriceForBedroom"/> class.</returns>
		public LMMR.Entities.SpecialPriceForBedroom GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SpecialPriceForBedroom index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SpecialPriceForBedroom"/> class.</returns>
		public LMMR.Entities.SpecialPriceForBedroom GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SpecialPriceForBedroom index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SpecialPriceForBedroom"/> class.</returns>
		public LMMR.Entities.SpecialPriceForBedroom GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SpecialPriceForBedroom index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SpecialPriceForBedroom"/> class.</returns>
		public abstract LMMR.Entities.SpecialPriceForBedroom GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SpecialPriceForBedroom&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SpecialPriceForBedroom&gt;"/></returns>
		public static TList<SpecialPriceForBedroom> Fill(IDataReader reader, TList<SpecialPriceForBedroom> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.SpecialPriceForBedroom c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SpecialPriceForBedroom")
					.Append("|").Append((System.Int64)reader[((int)SpecialPriceForBedroomColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SpecialPriceForBedroom>(
					key.ToString(), // EntityTrackingKey
					"SpecialPriceForBedroom",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.SpecialPriceForBedroom();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)SpecialPriceForBedroomColumn.Id - 1)];
					c.HotelId = (reader.IsDBNull(((int)SpecialPriceForBedroomColumn.HotelId - 1)))?null:(System.Int64?)reader[((int)SpecialPriceForBedroomColumn.HotelId - 1)];
					c.BedroomId = (reader.IsDBNull(((int)SpecialPriceForBedroomColumn.BedroomId - 1)))?null:(System.Int64?)reader[((int)SpecialPriceForBedroomColumn.BedroomId - 1)];
					c.PriceDate = (reader.IsDBNull(((int)SpecialPriceForBedroomColumn.PriceDate - 1)))?null:(System.DateTime?)reader[((int)SpecialPriceForBedroomColumn.PriceDate - 1)];
					c.PriceOfTheDaySingle = (reader.IsDBNull(((int)SpecialPriceForBedroomColumn.PriceOfTheDaySingle - 1)))?null:(System.Decimal?)reader[((int)SpecialPriceForBedroomColumn.PriceOfTheDaySingle - 1)];
					c.PriceOfTheDayDouble = (reader.IsDBNull(((int)SpecialPriceForBedroomColumn.PriceOfTheDayDouble - 1)))?null:(System.Decimal?)reader[((int)SpecialPriceForBedroomColumn.PriceOfTheDayDouble - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.SpecialPriceForBedroom"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SpecialPriceForBedroom"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.SpecialPriceForBedroom entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)SpecialPriceForBedroomColumn.Id - 1)];
			entity.HotelId = (reader.IsDBNull(((int)SpecialPriceForBedroomColumn.HotelId - 1)))?null:(System.Int64?)reader[((int)SpecialPriceForBedroomColumn.HotelId - 1)];
			entity.BedroomId = (reader.IsDBNull(((int)SpecialPriceForBedroomColumn.BedroomId - 1)))?null:(System.Int64?)reader[((int)SpecialPriceForBedroomColumn.BedroomId - 1)];
			entity.PriceDate = (reader.IsDBNull(((int)SpecialPriceForBedroomColumn.PriceDate - 1)))?null:(System.DateTime?)reader[((int)SpecialPriceForBedroomColumn.PriceDate - 1)];
			entity.PriceOfTheDaySingle = (reader.IsDBNull(((int)SpecialPriceForBedroomColumn.PriceOfTheDaySingle - 1)))?null:(System.Decimal?)reader[((int)SpecialPriceForBedroomColumn.PriceOfTheDaySingle - 1)];
			entity.PriceOfTheDayDouble = (reader.IsDBNull(((int)SpecialPriceForBedroomColumn.PriceOfTheDayDouble - 1)))?null:(System.Decimal?)reader[((int)SpecialPriceForBedroomColumn.PriceOfTheDayDouble - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.SpecialPriceForBedroom"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SpecialPriceForBedroom"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.SpecialPriceForBedroom entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.HotelId = Convert.IsDBNull(dataRow["HotelID"]) ? null : (System.Int64?)dataRow["HotelID"];
			entity.BedroomId = Convert.IsDBNull(dataRow["BedroomID"]) ? null : (System.Int64?)dataRow["BedroomID"];
			entity.PriceDate = Convert.IsDBNull(dataRow["PriceDate"]) ? null : (System.DateTime?)dataRow["PriceDate"];
			entity.PriceOfTheDaySingle = Convert.IsDBNull(dataRow["PriceOfTheDaySingle"]) ? null : (System.Decimal?)dataRow["PriceOfTheDaySingle"];
			entity.PriceOfTheDayDouble = Convert.IsDBNull(dataRow["PriceOfTheDayDouble"]) ? null : (System.Decimal?)dataRow["PriceOfTheDayDouble"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SpecialPriceForBedroom"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.SpecialPriceForBedroom Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.SpecialPriceForBedroom entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region BedroomIdSource	
			if (CanDeepLoad(entity, "BedRoom|BedroomIdSource", deepLoadType, innerList) 
				&& entity.BedroomIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.BedroomId ?? (long)0);
				BedRoom tmpEntity = EntityManager.LocateEntity<BedRoom>(EntityLocator.ConstructKeyFromPkItems(typeof(BedRoom), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.BedroomIdSource = tmpEntity;
				else
					entity.BedroomIdSource = DataRepository.BedRoomProvider.GetById(transactionManager, (entity.BedroomId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BedroomIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.BedroomIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.BedRoomProvider.DeepLoad(transactionManager, entity.BedroomIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion BedroomIdSource

			#region HotelIdSource	
			if (CanDeepLoad(entity, "Hotel|HotelIdSource", deepLoadType, innerList) 
				&& entity.HotelIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.HotelId ?? (long)0);
				Hotel tmpEntity = EntityManager.LocateEntity<Hotel>(EntityLocator.ConstructKeyFromPkItems(typeof(Hotel), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.HotelIdSource = tmpEntity;
				else
					entity.HotelIdSource = DataRepository.HotelProvider.GetById(transactionManager, (entity.HotelId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HotelIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.HotelIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.HotelProvider.DeepLoad(transactionManager, entity.HotelIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion HotelIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.SpecialPriceForBedroom object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.SpecialPriceForBedroom instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.SpecialPriceForBedroom Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.SpecialPriceForBedroom entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region BedroomIdSource
			if (CanDeepSave(entity, "BedRoom|BedroomIdSource", deepSaveType, innerList) 
				&& entity.BedroomIdSource != null)
			{
				DataRepository.BedRoomProvider.Save(transactionManager, entity.BedroomIdSource);
				entity.BedroomId = entity.BedroomIdSource.Id;
			}
			#endregion 
			
			#region HotelIdSource
			if (CanDeepSave(entity, "Hotel|HotelIdSource", deepSaveType, innerList) 
				&& entity.HotelIdSource != null)
			{
				DataRepository.HotelProvider.Save(transactionManager, entity.HotelIdSource);
				entity.HotelId = entity.HotelIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SpecialPriceForBedroomChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.SpecialPriceForBedroom</c>
	///</summary>
	public enum SpecialPriceForBedroomChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>BedRoom</c> at BedroomIdSource
		///</summary>
		[ChildEntityType(typeof(BedRoom))]
		BedRoom,
			
		///<summary>
		/// Composite Property for <c>Hotel</c> at HotelIdSource
		///</summary>
		[ChildEntityType(typeof(Hotel))]
		Hotel,
		}
	
	#endregion SpecialPriceForBedroomChildEntityTypes
	
	#region SpecialPriceForBedroomFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SpecialPriceForBedroomColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SpecialPriceForBedroom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SpecialPriceForBedroomFilterBuilder : SqlFilterBuilder<SpecialPriceForBedroomColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SpecialPriceForBedroomFilterBuilder class.
		/// </summary>
		public SpecialPriceForBedroomFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SpecialPriceForBedroomFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SpecialPriceForBedroomFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SpecialPriceForBedroomFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SpecialPriceForBedroomFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SpecialPriceForBedroomFilterBuilder
	
	#region SpecialPriceForBedroomParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SpecialPriceForBedroomColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SpecialPriceForBedroom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SpecialPriceForBedroomParameterBuilder : ParameterizedSqlFilterBuilder<SpecialPriceForBedroomColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SpecialPriceForBedroomParameterBuilder class.
		/// </summary>
		public SpecialPriceForBedroomParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SpecialPriceForBedroomParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SpecialPriceForBedroomParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SpecialPriceForBedroomParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SpecialPriceForBedroomParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SpecialPriceForBedroomParameterBuilder
	
	#region SpecialPriceForBedroomSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SpecialPriceForBedroomColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SpecialPriceForBedroom"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SpecialPriceForBedroomSortBuilder : SqlSortBuilder<SpecialPriceForBedroomColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SpecialPriceForBedroomSqlSortBuilder class.
		/// </summary>
		public SpecialPriceForBedroomSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SpecialPriceForBedroomSortBuilder
	
} // end namespace
