﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SurveyProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SurveyProviderBaseCore : EntityProviderBase<LMMR.Entities.Survey, LMMR.Entities.SurveyKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.SurveyKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.Survey Get(TransactionManager transactionManager, LMMR.Entities.SurveyKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Survey index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Survey"/> class.</returns>
		public LMMR.Entities.Survey GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Survey index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Survey"/> class.</returns>
		public LMMR.Entities.Survey GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Survey index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Survey"/> class.</returns>
		public LMMR.Entities.Survey GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Survey index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Survey"/> class.</returns>
		public LMMR.Entities.Survey GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Survey index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Survey"/> class.</returns>
		public LMMR.Entities.Survey GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Survey index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Survey"/> class.</returns>
		public abstract LMMR.Entities.Survey GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Survey&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Survey&gt;"/></returns>
		public static TList<Survey> Fill(IDataReader reader, TList<Survey> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.Survey c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Survey")
					.Append("|").Append((System.Int64)reader[((int)SurveyColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Survey>(
					key.ToString(), // EntityTrackingKey
					"Survey",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.Survey();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)SurveyColumn.Id - 1)];
					c.QuestionName = (reader.IsDBNull(((int)SurveyColumn.QuestionName - 1)))?null:(System.String)reader[((int)SurveyColumn.QuestionName - 1)];
					c.SurveyType = (reader.IsDBNull(((int)SurveyColumn.SurveyType - 1)))?null:(System.Int32?)reader[((int)SurveyColumn.SurveyType - 1)];
					c.IsActive = (System.Boolean)reader[((int)SurveyColumn.IsActive - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Survey"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Survey"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.Survey entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)SurveyColumn.Id - 1)];
			entity.QuestionName = (reader.IsDBNull(((int)SurveyColumn.QuestionName - 1)))?null:(System.String)reader[((int)SurveyColumn.QuestionName - 1)];
			entity.SurveyType = (reader.IsDBNull(((int)SurveyColumn.SurveyType - 1)))?null:(System.Int32?)reader[((int)SurveyColumn.SurveyType - 1)];
			entity.IsActive = (System.Boolean)reader[((int)SurveyColumn.IsActive - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Survey"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Survey"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.Survey entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.QuestionName = Convert.IsDBNull(dataRow["QuestionName"]) ? null : (System.String)dataRow["QuestionName"];
			entity.SurveyType = Convert.IsDBNull(dataRow["SurveyType"]) ? null : (System.Int32?)dataRow["SurveyType"];
			entity.IsActive = (System.Boolean)dataRow["IsActive"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Survey"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.Survey Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.Survey entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region SurveyResponseCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SurveyResponse>|SurveyResponseCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SurveyResponseCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SurveyResponseCollection = DataRepository.SurveyResponseProvider.GetBySurveyId(transactionManager, entity.Id);

				if (deep && entity.SurveyResponseCollection.Count > 0)
				{
					deepHandles.Add("SurveyResponseCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SurveyResponse>) DataRepository.SurveyResponseProvider.DeepLoad,
						new object[] { transactionManager, entity.SurveyResponseCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region SurveyLanguageCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SurveyLanguage>|SurveyLanguageCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SurveyLanguageCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SurveyLanguageCollection = DataRepository.SurveyLanguageProvider.GetBySurveyId(transactionManager, entity.Id);

				if (deep && entity.SurveyLanguageCollection.Count > 0)
				{
					deepHandles.Add("SurveyLanguageCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SurveyLanguage>) DataRepository.SurveyLanguageProvider.DeepLoad,
						new object[] { transactionManager, entity.SurveyLanguageCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.Survey object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.Survey instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.Survey Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.Survey entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<SurveyResponse>
				if (CanDeepSave(entity.SurveyResponseCollection, "List<SurveyResponse>|SurveyResponseCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SurveyResponse child in entity.SurveyResponseCollection)
					{
						if(child.SurveyIdSource != null)
						{
							child.SurveyId = child.SurveyIdSource.Id;
						}
						else
						{
							child.SurveyId = entity.Id;
						}

					}

					if (entity.SurveyResponseCollection.Count > 0 || entity.SurveyResponseCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SurveyResponseProvider.Save(transactionManager, entity.SurveyResponseCollection);
						
						deepHandles.Add("SurveyResponseCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SurveyResponse >) DataRepository.SurveyResponseProvider.DeepSave,
							new object[] { transactionManager, entity.SurveyResponseCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<SurveyLanguage>
				if (CanDeepSave(entity.SurveyLanguageCollection, "List<SurveyLanguage>|SurveyLanguageCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SurveyLanguage child in entity.SurveyLanguageCollection)
					{
						if(child.SurveyIdSource != null)
						{
							child.SurveyId = child.SurveyIdSource.Id;
						}
						else
						{
							child.SurveyId = entity.Id;
						}

					}

					if (entity.SurveyLanguageCollection.Count > 0 || entity.SurveyLanguageCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SurveyLanguageProvider.Save(transactionManager, entity.SurveyLanguageCollection);
						
						deepHandles.Add("SurveyLanguageCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SurveyLanguage >) DataRepository.SurveyLanguageProvider.DeepSave,
							new object[] { transactionManager, entity.SurveyLanguageCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SurveyChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.Survey</c>
	///</summary>
	public enum SurveyChildEntityTypes
	{

		///<summary>
		/// Collection of <c>Survey</c> as OneToMany for SurveyResponseCollection
		///</summary>
		[ChildEntityType(typeof(TList<SurveyResponse>))]
		SurveyResponseCollection,

		///<summary>
		/// Collection of <c>Survey</c> as OneToMany for SurveyLanguageCollection
		///</summary>
		[ChildEntityType(typeof(TList<SurveyLanguage>))]
		SurveyLanguageCollection,
	}
	
	#endregion SurveyChildEntityTypes
	
	#region SurveyFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SurveyColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Survey"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SurveyFilterBuilder : SqlFilterBuilder<SurveyColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SurveyFilterBuilder class.
		/// </summary>
		public SurveyFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SurveyFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SurveyFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SurveyFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SurveyFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SurveyFilterBuilder
	
	#region SurveyParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SurveyColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Survey"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SurveyParameterBuilder : ParameterizedSqlFilterBuilder<SurveyColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SurveyParameterBuilder class.
		/// </summary>
		public SurveyParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SurveyParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SurveyParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SurveyParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SurveyParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SurveyParameterBuilder
	
	#region SurveySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SurveyColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Survey"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SurveySortBuilder : SqlSortBuilder<SurveyColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SurveySqlSortBuilder class.
		/// </summary>
		public SurveySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SurveySortBuilder
	
} // end namespace
