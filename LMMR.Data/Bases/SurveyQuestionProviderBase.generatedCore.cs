﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SurveyQuestionProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SurveyQuestionProviderBaseCore : EntityProviderBase<LMMR.Entities.SurveyQuestion, LMMR.Entities.SurveyQuestionKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.SurveyQuestionKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.SurveyQuestion Get(TransactionManager transactionManager, LMMR.Entities.SurveyQuestionKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK__survey_Q__3214EC074BD989B7 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyQuestion"/> class.</returns>
		public LMMR.Entities.SurveyQuestion GetById(System.Int32 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__survey_Q__3214EC074BD989B7 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyQuestion"/> class.</returns>
		public LMMR.Entities.SurveyQuestion GetById(System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__survey_Q__3214EC074BD989B7 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyQuestion"/> class.</returns>
		public LMMR.Entities.SurveyQuestion GetById(TransactionManager transactionManager, System.Int32 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__survey_Q__3214EC074BD989B7 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyQuestion"/> class.</returns>
		public LMMR.Entities.SurveyQuestion GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__survey_Q__3214EC074BD989B7 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyQuestion"/> class.</returns>
		public LMMR.Entities.SurveyQuestion GetById(System.Int32 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__survey_Q__3214EC074BD989B7 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyQuestion"/> class.</returns>
		public abstract LMMR.Entities.SurveyQuestion GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SurveyQuestion&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SurveyQuestion&gt;"/></returns>
		public static TList<SurveyQuestion> Fill(IDataReader reader, TList<SurveyQuestion> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.SurveyQuestion c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SurveyQuestion")
					.Append("|").Append((System.Int32)reader[((int)SurveyQuestionColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SurveyQuestion>(
					key.ToString(), // EntityTrackingKey
					"SurveyQuestion",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.SurveyQuestion();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int32)reader[((int)SurveyQuestionColumn.Id - 1)];
					c.QuestionText = (reader.IsDBNull(((int)SurveyQuestionColumn.QuestionText - 1)))?null:(System.String)reader[((int)SurveyQuestionColumn.QuestionText - 1)];
					c.ParentId = (reader.IsDBNull(((int)SurveyQuestionColumn.ParentId - 1)))?null:(System.Int32?)reader[((int)SurveyQuestionColumn.ParentId - 1)];
					c.LanguageId = (reader.IsDBNull(((int)SurveyQuestionColumn.LanguageId - 1)))?null:(System.Int32?)reader[((int)SurveyQuestionColumn.LanguageId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.SurveyQuestion"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SurveyQuestion"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.SurveyQuestion entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int32)reader[((int)SurveyQuestionColumn.Id - 1)];
			entity.QuestionText = (reader.IsDBNull(((int)SurveyQuestionColumn.QuestionText - 1)))?null:(System.String)reader[((int)SurveyQuestionColumn.QuestionText - 1)];
			entity.ParentId = (reader.IsDBNull(((int)SurveyQuestionColumn.ParentId - 1)))?null:(System.Int32?)reader[((int)SurveyQuestionColumn.ParentId - 1)];
			entity.LanguageId = (reader.IsDBNull(((int)SurveyQuestionColumn.LanguageId - 1)))?null:(System.Int32?)reader[((int)SurveyQuestionColumn.LanguageId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.SurveyQuestion"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SurveyQuestion"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.SurveyQuestion entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int32)dataRow["Id"];
			entity.QuestionText = Convert.IsDBNull(dataRow["Question_Text"]) ? null : (System.String)dataRow["Question_Text"];
			entity.ParentId = Convert.IsDBNull(dataRow["ParentID"]) ? null : (System.Int32?)dataRow["ParentID"];
			entity.LanguageId = Convert.IsDBNull(dataRow["LanguageID"]) ? null : (System.Int32?)dataRow["LanguageID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SurveyQuestion"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.SurveyQuestion Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.SurveyQuestion entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region SurveyCategoryCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SurveyCategory>|SurveyCategoryCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SurveyCategoryCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SurveyCategoryCollection = DataRepository.SurveyCategoryProvider.GetByCategoryQuestionId(transactionManager, entity.Id);

				if (deep && entity.SurveyCategoryCollection.Count > 0)
				{
					deepHandles.Add("SurveyCategoryCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SurveyCategory>) DataRepository.SurveyCategoryProvider.DeepLoad,
						new object[] { transactionManager, entity.SurveyCategoryCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region SurveyAnswerCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SurveyAnswer>|SurveyAnswerCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SurveyAnswerCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SurveyAnswerCollection = DataRepository.SurveyAnswerProvider.GetByQuestionId(transactionManager, entity.Id);

				if (deep && entity.SurveyAnswerCollection.Count > 0)
				{
					deepHandles.Add("SurveyAnswerCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SurveyAnswer>) DataRepository.SurveyAnswerProvider.DeepLoad,
						new object[] { transactionManager, entity.SurveyAnswerCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.SurveyQuestion object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.SurveyQuestion instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.SurveyQuestion Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.SurveyQuestion entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<SurveyCategory>
				if (CanDeepSave(entity.SurveyCategoryCollection, "List<SurveyCategory>|SurveyCategoryCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SurveyCategory child in entity.SurveyCategoryCollection)
					{
						if(child.CategoryQuestionIdSource != null)
						{
							child.CategoryQuestionId = child.CategoryQuestionIdSource.Id;
						}
						else
						{
							child.CategoryQuestionId = entity.Id;
						}

					}

					if (entity.SurveyCategoryCollection.Count > 0 || entity.SurveyCategoryCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SurveyCategoryProvider.Save(transactionManager, entity.SurveyCategoryCollection);
						
						deepHandles.Add("SurveyCategoryCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SurveyCategory >) DataRepository.SurveyCategoryProvider.DeepSave,
							new object[] { transactionManager, entity.SurveyCategoryCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<SurveyAnswer>
				if (CanDeepSave(entity.SurveyAnswerCollection, "List<SurveyAnswer>|SurveyAnswerCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SurveyAnswer child in entity.SurveyAnswerCollection)
					{
						if(child.QuestionIdSource != null)
						{
							child.QuestionId = child.QuestionIdSource.Id;
						}
						else
						{
							child.QuestionId = entity.Id;
						}

					}

					if (entity.SurveyAnswerCollection.Count > 0 || entity.SurveyAnswerCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SurveyAnswerProvider.Save(transactionManager, entity.SurveyAnswerCollection);
						
						deepHandles.Add("SurveyAnswerCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SurveyAnswer >) DataRepository.SurveyAnswerProvider.DeepSave,
							new object[] { transactionManager, entity.SurveyAnswerCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SurveyQuestionChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.SurveyQuestion</c>
	///</summary>
	public enum SurveyQuestionChildEntityTypes
	{

		///<summary>
		/// Collection of <c>SurveyQuestion</c> as OneToMany for SurveyCategoryCollection
		///</summary>
		[ChildEntityType(typeof(TList<SurveyCategory>))]
		SurveyCategoryCollection,

		///<summary>
		/// Collection of <c>SurveyQuestion</c> as OneToMany for SurveyAnswerCollection
		///</summary>
		[ChildEntityType(typeof(TList<SurveyAnswer>))]
		SurveyAnswerCollection,
	}
	
	#endregion SurveyQuestionChildEntityTypes
	
	#region SurveyQuestionFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SurveyQuestionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SurveyQuestion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SurveyQuestionFilterBuilder : SqlFilterBuilder<SurveyQuestionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SurveyQuestionFilterBuilder class.
		/// </summary>
		public SurveyQuestionFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SurveyQuestionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SurveyQuestionFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SurveyQuestionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SurveyQuestionFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SurveyQuestionFilterBuilder
	
	#region SurveyQuestionParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SurveyQuestionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SurveyQuestion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SurveyQuestionParameterBuilder : ParameterizedSqlFilterBuilder<SurveyQuestionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SurveyQuestionParameterBuilder class.
		/// </summary>
		public SurveyQuestionParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SurveyQuestionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SurveyQuestionParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SurveyQuestionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SurveyQuestionParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SurveyQuestionParameterBuilder
	
	#region SurveyQuestionSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SurveyQuestionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SurveyQuestion"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SurveyQuestionSortBuilder : SqlSortBuilder<SurveyQuestionColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SurveyQuestionSqlSortBuilder class.
		/// </summary>
		public SurveyQuestionSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SurveyQuestionSortBuilder
	
} // end namespace
