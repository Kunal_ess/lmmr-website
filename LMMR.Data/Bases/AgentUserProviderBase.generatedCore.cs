﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AgentUserProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AgentUserProviderBaseCore : EntityProviderBase<LMMR.Entities.AgentUser, LMMR.Entities.AgentUserKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.AgentUserKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.AgentUser Get(TransactionManager transactionManager, LMMR.Entities.AgentUserKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_AgentUser index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.AgentUser"/> class.</returns>
		public LMMR.Entities.AgentUser GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AgentUser index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.AgentUser"/> class.</returns>
		public LMMR.Entities.AgentUser GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AgentUser index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.AgentUser"/> class.</returns>
		public LMMR.Entities.AgentUser GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AgentUser index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.AgentUser"/> class.</returns>
		public LMMR.Entities.AgentUser GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AgentUser index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.AgentUser"/> class.</returns>
		public LMMR.Entities.AgentUser GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AgentUser index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.AgentUser"/> class.</returns>
		public abstract LMMR.Entities.AgentUser GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;AgentUser&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;AgentUser&gt;"/></returns>
		public static TList<AgentUser> Fill(IDataReader reader, TList<AgentUser> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.AgentUser c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("AgentUser")
					.Append("|").Append((System.Int64)reader[((int)AgentUserColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<AgentUser>(
					key.ToString(), // EntityTrackingKey
					"AgentUser",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.AgentUser();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)AgentUserColumn.Id - 1)];
					c.AgencyReffId = (reader.IsDBNull(((int)AgentUserColumn.AgencyReffId - 1)))?null:(System.String)reader[((int)AgentUserColumn.AgencyReffId - 1)];
					c.AgencyMasterId = (System.Int64)reader[((int)AgentUserColumn.AgencyMasterId - 1)];
					c.FirstName = (reader.IsDBNull(((int)AgentUserColumn.FirstName - 1)))?null:(System.String)reader[((int)AgentUserColumn.FirstName - 1)];
					c.LastName = (reader.IsDBNull(((int)AgentUserColumn.LastName - 1)))?null:(System.String)reader[((int)AgentUserColumn.LastName - 1)];
					c.Email = (reader.IsDBNull(((int)AgentUserColumn.Email - 1)))?null:(System.String)reader[((int)AgentUserColumn.Email - 1)];
					c.LinkWith = (reader.IsDBNull(((int)AgentUserColumn.LinkWith - 1)))?null:(System.Int64?)reader[((int)AgentUserColumn.LinkWith - 1)];
					c.Password = (reader.IsDBNull(((int)AgentUserColumn.Password - 1)))?null:(System.String)reader[((int)AgentUserColumn.Password - 1)];
					c.IsFinancialInfo = (System.Boolean)reader[((int)AgentUserColumn.IsFinancialInfo - 1)];
					c.CreationDate = (reader.IsDBNull(((int)AgentUserColumn.CreationDate - 1)))?null:(System.DateTime?)reader[((int)AgentUserColumn.CreationDate - 1)];
					c.IsActive = (System.Boolean)reader[((int)AgentUserColumn.IsActive - 1)];
					c.IsDeleted = (System.Boolean)reader[((int)AgentUserColumn.IsDeleted - 1)];
					c.TransferUserId = (reader.IsDBNull(((int)AgentUserColumn.TransferUserId - 1)))?null:(System.Int64?)reader[((int)AgentUserColumn.TransferUserId - 1)];
					c.DeletedDate = (reader.IsDBNull(((int)AgentUserColumn.DeletedDate - 1)))?null:(System.DateTime?)reader[((int)AgentUserColumn.DeletedDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.AgentUser"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.AgentUser"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.AgentUser entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)AgentUserColumn.Id - 1)];
			entity.AgencyReffId = (reader.IsDBNull(((int)AgentUserColumn.AgencyReffId - 1)))?null:(System.String)reader[((int)AgentUserColumn.AgencyReffId - 1)];
			entity.AgencyMasterId = (System.Int64)reader[((int)AgentUserColumn.AgencyMasterId - 1)];
			entity.FirstName = (reader.IsDBNull(((int)AgentUserColumn.FirstName - 1)))?null:(System.String)reader[((int)AgentUserColumn.FirstName - 1)];
			entity.LastName = (reader.IsDBNull(((int)AgentUserColumn.LastName - 1)))?null:(System.String)reader[((int)AgentUserColumn.LastName - 1)];
			entity.Email = (reader.IsDBNull(((int)AgentUserColumn.Email - 1)))?null:(System.String)reader[((int)AgentUserColumn.Email - 1)];
			entity.LinkWith = (reader.IsDBNull(((int)AgentUserColumn.LinkWith - 1)))?null:(System.Int64?)reader[((int)AgentUserColumn.LinkWith - 1)];
			entity.Password = (reader.IsDBNull(((int)AgentUserColumn.Password - 1)))?null:(System.String)reader[((int)AgentUserColumn.Password - 1)];
			entity.IsFinancialInfo = (System.Boolean)reader[((int)AgentUserColumn.IsFinancialInfo - 1)];
			entity.CreationDate = (reader.IsDBNull(((int)AgentUserColumn.CreationDate - 1)))?null:(System.DateTime?)reader[((int)AgentUserColumn.CreationDate - 1)];
			entity.IsActive = (System.Boolean)reader[((int)AgentUserColumn.IsActive - 1)];
			entity.IsDeleted = (System.Boolean)reader[((int)AgentUserColumn.IsDeleted - 1)];
			entity.TransferUserId = (reader.IsDBNull(((int)AgentUserColumn.TransferUserId - 1)))?null:(System.Int64?)reader[((int)AgentUserColumn.TransferUserId - 1)];
			entity.DeletedDate = (reader.IsDBNull(((int)AgentUserColumn.DeletedDate - 1)))?null:(System.DateTime?)reader[((int)AgentUserColumn.DeletedDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.AgentUser"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.AgentUser"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.AgentUser entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.AgencyReffId = Convert.IsDBNull(dataRow["AgencyReffId"]) ? null : (System.String)dataRow["AgencyReffId"];
			entity.AgencyMasterId = (System.Int64)dataRow["AgencyMasterId"];
			entity.FirstName = Convert.IsDBNull(dataRow["FirstName"]) ? null : (System.String)dataRow["FirstName"];
			entity.LastName = Convert.IsDBNull(dataRow["LastName"]) ? null : (System.String)dataRow["LastName"];
			entity.Email = Convert.IsDBNull(dataRow["Email"]) ? null : (System.String)dataRow["Email"];
			entity.LinkWith = Convert.IsDBNull(dataRow["LinkWith"]) ? null : (System.Int64?)dataRow["LinkWith"];
			entity.Password = Convert.IsDBNull(dataRow["Password"]) ? null : (System.String)dataRow["Password"];
			entity.IsFinancialInfo = (System.Boolean)dataRow["IsFinancialInfo"];
			entity.CreationDate = Convert.IsDBNull(dataRow["CreationDate"]) ? null : (System.DateTime?)dataRow["CreationDate"];
			entity.IsActive = (System.Boolean)dataRow["IsActive"];
			entity.IsDeleted = (System.Boolean)dataRow["IsDeleted"];
			entity.TransferUserId = Convert.IsDBNull(dataRow["TransferUserId"]) ? null : (System.Int64?)dataRow["TransferUserId"];
			entity.DeletedDate = Convert.IsDBNull(dataRow["DeletedDate"]) ? null : (System.DateTime?)dataRow["DeletedDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.AgentUser"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.AgentUser Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.AgentUser entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region AgencyClientCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AgencyClient>|AgencyClientCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AgencyClientCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AgencyClientCollection = DataRepository.AgencyClientProvider.GetByAgencyUserId(transactionManager, entity.Id);

				if (deep && entity.AgencyClientCollection.Count > 0)
				{
					deepHandles.Add("AgencyClientCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AgencyClient>) DataRepository.AgencyClientProvider.DeepLoad,
						new object[] { transactionManager, entity.AgencyClientCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.AgentUser object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.AgentUser instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.AgentUser Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.AgentUser entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<AgencyClient>
				if (CanDeepSave(entity.AgencyClientCollection, "List<AgencyClient>|AgencyClientCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AgencyClient child in entity.AgencyClientCollection)
					{
						if(child.AgencyUserIdSource != null)
						{
							child.AgencyUserId = child.AgencyUserIdSource.Id;
						}
						else
						{
							child.AgencyUserId = entity.Id;
						}

					}

					if (entity.AgencyClientCollection.Count > 0 || entity.AgencyClientCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AgencyClientProvider.Save(transactionManager, entity.AgencyClientCollection);
						
						deepHandles.Add("AgencyClientCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AgencyClient >) DataRepository.AgencyClientProvider.DeepSave,
							new object[] { transactionManager, entity.AgencyClientCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AgentUserChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.AgentUser</c>
	///</summary>
	public enum AgentUserChildEntityTypes
	{

		///<summary>
		/// Collection of <c>AgentUser</c> as OneToMany for AgencyClientCollection
		///</summary>
		[ChildEntityType(typeof(TList<AgencyClient>))]
		AgencyClientCollection,
	}
	
	#endregion AgentUserChildEntityTypes
	
	#region AgentUserFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AgentUserColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AgentUser"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AgentUserFilterBuilder : SqlFilterBuilder<AgentUserColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AgentUserFilterBuilder class.
		/// </summary>
		public AgentUserFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AgentUserFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AgentUserFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AgentUserFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AgentUserFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AgentUserFilterBuilder
	
	#region AgentUserParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AgentUserColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AgentUser"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AgentUserParameterBuilder : ParameterizedSqlFilterBuilder<AgentUserColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AgentUserParameterBuilder class.
		/// </summary>
		public AgentUserParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AgentUserParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AgentUserParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AgentUserParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AgentUserParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AgentUserParameterBuilder
	
	#region AgentUserSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AgentUserColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AgentUser"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AgentUserSortBuilder : SqlSortBuilder<AgentUserColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AgentUserSqlSortBuilder class.
		/// </summary>
		public AgentUserSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AgentUserSortBuilder
	
} // end namespace
