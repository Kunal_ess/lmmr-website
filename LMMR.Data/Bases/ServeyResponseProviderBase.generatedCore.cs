﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ServeyResponseProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ServeyResponseProviderBaseCore : EntityProviderBase<LMMR.Entities.ServeyResponse, LMMR.Entities.ServeyResponseKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.ServeyResponseKey key)
		{
			return Delete(transactionManager, key.ServeyId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_serveyId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _serveyId)
		{
			return Delete(null, _serveyId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_serveyId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _serveyId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ServeyResponse_Booking key.
		///		FK_ServeyResponse_Booking Description: 
		/// </summary>
		/// <param name="_bookingId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.ServeyResponse objects.</returns>
		public TList<ServeyResponse> GetByBookingId(System.Int64 _bookingId)
		{
			int count = -1;
			return GetByBookingId(_bookingId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ServeyResponse_Booking key.
		///		FK_ServeyResponse_Booking Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bookingId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.ServeyResponse objects.</returns>
		/// <remarks></remarks>
		public TList<ServeyResponse> GetByBookingId(TransactionManager transactionManager, System.Int64 _bookingId)
		{
			int count = -1;
			return GetByBookingId(transactionManager, _bookingId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ServeyResponse_Booking key.
		///		FK_ServeyResponse_Booking Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bookingId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.ServeyResponse objects.</returns>
		public TList<ServeyResponse> GetByBookingId(TransactionManager transactionManager, System.Int64 _bookingId, int start, int pageLength)
		{
			int count = -1;
			return GetByBookingId(transactionManager, _bookingId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ServeyResponse_Booking key.
		///		fkServeyResponseBooking Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bookingId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.ServeyResponse objects.</returns>
		public TList<ServeyResponse> GetByBookingId(System.Int64 _bookingId, int start, int pageLength)
		{
			int count =  -1;
			return GetByBookingId(null, _bookingId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ServeyResponse_Booking key.
		///		fkServeyResponseBooking Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bookingId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.ServeyResponse objects.</returns>
		public TList<ServeyResponse> GetByBookingId(System.Int64 _bookingId, int start, int pageLength,out int count)
		{
			return GetByBookingId(null, _bookingId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ServeyResponse_Booking key.
		///		FK_ServeyResponse_Booking Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bookingId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.ServeyResponse objects.</returns>
		public abstract TList<ServeyResponse> GetByBookingId(TransactionManager transactionManager, System.Int64 _bookingId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ServeyResponse_Hotel key.
		///		FK_ServeyResponse_Hotel Description: 
		/// </summary>
		/// <param name="_vanueId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.ServeyResponse objects.</returns>
		public TList<ServeyResponse> GetByVanueId(System.Int64? _vanueId)
		{
			int count = -1;
			return GetByVanueId(_vanueId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ServeyResponse_Hotel key.
		///		FK_ServeyResponse_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_vanueId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.ServeyResponse objects.</returns>
		/// <remarks></remarks>
		public TList<ServeyResponse> GetByVanueId(TransactionManager transactionManager, System.Int64? _vanueId)
		{
			int count = -1;
			return GetByVanueId(transactionManager, _vanueId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ServeyResponse_Hotel key.
		///		FK_ServeyResponse_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_vanueId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.ServeyResponse objects.</returns>
		public TList<ServeyResponse> GetByVanueId(TransactionManager transactionManager, System.Int64? _vanueId, int start, int pageLength)
		{
			int count = -1;
			return GetByVanueId(transactionManager, _vanueId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ServeyResponse_Hotel key.
		///		fkServeyResponseHotel Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_vanueId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.ServeyResponse objects.</returns>
		public TList<ServeyResponse> GetByVanueId(System.Int64? _vanueId, int start, int pageLength)
		{
			int count =  -1;
			return GetByVanueId(null, _vanueId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ServeyResponse_Hotel key.
		///		fkServeyResponseHotel Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_vanueId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.ServeyResponse objects.</returns>
		public TList<ServeyResponse> GetByVanueId(System.Int64? _vanueId, int start, int pageLength,out int count)
		{
			return GetByVanueId(null, _vanueId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ServeyResponse_Hotel key.
		///		FK_ServeyResponse_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_vanueId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.ServeyResponse objects.</returns>
		public abstract TList<ServeyResponse> GetByVanueId(TransactionManager transactionManager, System.Int64? _vanueId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ServeyResponse_Users key.
		///		FK_ServeyResponse_Users Description: 
		/// </summary>
		/// <param name="_userId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.ServeyResponse objects.</returns>
		public TList<ServeyResponse> GetByUserId(System.Int64 _userId)
		{
			int count = -1;
			return GetByUserId(_userId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ServeyResponse_Users key.
		///		FK_ServeyResponse_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.ServeyResponse objects.</returns>
		/// <remarks></remarks>
		public TList<ServeyResponse> GetByUserId(TransactionManager transactionManager, System.Int64 _userId)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ServeyResponse_Users key.
		///		FK_ServeyResponse_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.ServeyResponse objects.</returns>
		public TList<ServeyResponse> GetByUserId(TransactionManager transactionManager, System.Int64 _userId, int start, int pageLength)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ServeyResponse_Users key.
		///		fkServeyResponseUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_userId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.ServeyResponse objects.</returns>
		public TList<ServeyResponse> GetByUserId(System.Int64 _userId, int start, int pageLength)
		{
			int count =  -1;
			return GetByUserId(null, _userId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ServeyResponse_Users key.
		///		fkServeyResponseUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_userId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.ServeyResponse objects.</returns>
		public TList<ServeyResponse> GetByUserId(System.Int64 _userId, int start, int pageLength,out int count)
		{
			return GetByUserId(null, _userId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ServeyResponse_Users key.
		///		FK_ServeyResponse_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.ServeyResponse objects.</returns>
		public abstract TList<ServeyResponse> GetByUserId(TransactionManager transactionManager, System.Int64 _userId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.ServeyResponse Get(TransactionManager transactionManager, LMMR.Entities.ServeyResponseKey key, int start, int pageLength)
		{
			return GetByServeyId(transactionManager, key.ServeyId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ServeyResponse index.
		/// </summary>
		/// <param name="_serveyId"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.ServeyResponse"/> class.</returns>
		public LMMR.Entities.ServeyResponse GetByServeyId(System.Int32 _serveyId)
		{
			int count = -1;
			return GetByServeyId(null,_serveyId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ServeyResponse index.
		/// </summary>
		/// <param name="_serveyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.ServeyResponse"/> class.</returns>
		public LMMR.Entities.ServeyResponse GetByServeyId(System.Int32 _serveyId, int start, int pageLength)
		{
			int count = -1;
			return GetByServeyId(null, _serveyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ServeyResponse index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_serveyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.ServeyResponse"/> class.</returns>
		public LMMR.Entities.ServeyResponse GetByServeyId(TransactionManager transactionManager, System.Int32 _serveyId)
		{
			int count = -1;
			return GetByServeyId(transactionManager, _serveyId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ServeyResponse index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_serveyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.ServeyResponse"/> class.</returns>
		public LMMR.Entities.ServeyResponse GetByServeyId(TransactionManager transactionManager, System.Int32 _serveyId, int start, int pageLength)
		{
			int count = -1;
			return GetByServeyId(transactionManager, _serveyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ServeyResponse index.
		/// </summary>
		/// <param name="_serveyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.ServeyResponse"/> class.</returns>
		public LMMR.Entities.ServeyResponse GetByServeyId(System.Int32 _serveyId, int start, int pageLength, out int count)
		{
			return GetByServeyId(null, _serveyId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ServeyResponse index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_serveyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.ServeyResponse"/> class.</returns>
		public abstract LMMR.Entities.ServeyResponse GetByServeyId(TransactionManager transactionManager, System.Int32 _serveyId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ServeyResponse&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ServeyResponse&gt;"/></returns>
		public static TList<ServeyResponse> Fill(IDataReader reader, TList<ServeyResponse> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.ServeyResponse c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ServeyResponse")
					.Append("|").Append((System.Int32)reader[((int)ServeyResponseColumn.ServeyId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ServeyResponse>(
					key.ToString(), // EntityTrackingKey
					"ServeyResponse",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.ServeyResponse();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ServeyId = (System.Int32)reader[((int)ServeyResponseColumn.ServeyId - 1)];
					c.UserId = (System.Int64)reader[((int)ServeyResponseColumn.UserId - 1)];
					c.VanueId = (reader.IsDBNull(((int)ServeyResponseColumn.VanueId - 1)))?null:(System.Int64?)reader[((int)ServeyResponseColumn.VanueId - 1)];
					c.AdditionalComment = (reader.IsDBNull(((int)ServeyResponseColumn.AdditionalComment - 1)))?null:(System.String)reader[((int)ServeyResponseColumn.AdditionalComment - 1)];
					c.BookingId = (System.Int64)reader[((int)ServeyResponseColumn.BookingId - 1)];
					c.BookingType = (System.Int32)reader[((int)ServeyResponseColumn.BookingType - 1)];
                    c.ReviewSubmitted = (reader.IsDBNull(((int)ServeyResponseColumn.ReviewSubmitted - 1))?DateTime.Now:(System.DateTime)reader[((int)ServeyResponseColumn.ReviewSubmitted - 1)]);
                    c.ReviewStatus = (reader.IsDBNull(((int)ServeyResponseColumn.ReviewStatus - 1)) ? false : (System.Boolean)reader[((int)ServeyResponseColumn.ReviewStatus - 1)]);
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.ServeyResponse"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.ServeyResponse"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.ServeyResponse entity)
		{
			if (!reader.Read()) return;
			
			entity.ServeyId = (System.Int32)reader[((int)ServeyResponseColumn.ServeyId - 1)];
			entity.UserId = (System.Int64)reader[((int)ServeyResponseColumn.UserId - 1)];
			entity.VanueId = (reader.IsDBNull(((int)ServeyResponseColumn.VanueId - 1)))?null:(System.Int64?)reader[((int)ServeyResponseColumn.VanueId - 1)];
			entity.AdditionalComment = (reader.IsDBNull(((int)ServeyResponseColumn.AdditionalComment - 1)))?null:(System.String)reader[((int)ServeyResponseColumn.AdditionalComment - 1)];
			entity.BookingId = (System.Int64)reader[((int)ServeyResponseColumn.BookingId - 1)];
			entity.BookingType = (System.Int32)reader[((int)ServeyResponseColumn.BookingType - 1)];
            entity.ReviewSubmitted = (System.DateTime)reader[((int)ServeyResponseColumn.ReviewSubmitted - 1)];
            entity.ReviewStatus = (System.Boolean)reader[((int)ServeyResponseColumn.ReviewStatus - 1)];
            entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.ServeyResponse"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.ServeyResponse"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.ServeyResponse entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ServeyId = (System.Int32)dataRow["ServeyID"];
			entity.UserId = (System.Int64)dataRow["UserID"];
			entity.VanueId = Convert.IsDBNull(dataRow["VanueID"]) ? null : (System.Int64?)dataRow["VanueID"];
			entity.AdditionalComment = Convert.IsDBNull(dataRow["AdditionalComment"]) ? null : (System.String)dataRow["AdditionalComment"];
			entity.BookingId = (System.Int64)dataRow["BookingID"];
			entity.BookingType = (System.Int32)dataRow["BookingType"];
            entity.ReviewSubmitted = (System.DateTime)dataRow["ReviewSubmitted"];
            entity.ReviewStatus = (System.Boolean)dataRow["ReviewStatus"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.ServeyResponse"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.ServeyResponse Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.ServeyResponse entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region BookingIdSource	
			if (CanDeepLoad(entity, "Booking|BookingIdSource", deepLoadType, innerList) 
				&& entity.BookingIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.BookingId;
				Booking tmpEntity = EntityManager.LocateEntity<Booking>(EntityLocator.ConstructKeyFromPkItems(typeof(Booking), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.BookingIdSource = tmpEntity;
				else
					entity.BookingIdSource = DataRepository.BookingProvider.GetById(transactionManager, entity.BookingId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BookingIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.BookingIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.BookingProvider.DeepLoad(transactionManager, entity.BookingIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion BookingIdSource

			#region VanueIdSource	
			if (CanDeepLoad(entity, "Hotel|VanueIdSource", deepLoadType, innerList) 
				&& entity.VanueIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.VanueId ?? (long)0);
				Hotel tmpEntity = EntityManager.LocateEntity<Hotel>(EntityLocator.ConstructKeyFromPkItems(typeof(Hotel), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.VanueIdSource = tmpEntity;
				else
					entity.VanueIdSource = DataRepository.HotelProvider.GetById(transactionManager, (entity.VanueId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'VanueIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.VanueIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.HotelProvider.DeepLoad(transactionManager, entity.VanueIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion VanueIdSource

			#region UserIdSource	
			if (CanDeepLoad(entity, "Users|UserIdSource", deepLoadType, innerList) 
				&& entity.UserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.UserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.UserIdSource = tmpEntity;
				else
					entity.UserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.UserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.UserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.UserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion UserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.ServeyResponse object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.ServeyResponse instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.ServeyResponse Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.ServeyResponse entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region BookingIdSource
			if (CanDeepSave(entity, "Booking|BookingIdSource", deepSaveType, innerList) 
				&& entity.BookingIdSource != null)
			{
				DataRepository.BookingProvider.Save(transactionManager, entity.BookingIdSource);
				entity.BookingId = entity.BookingIdSource.Id;
			}
			#endregion 
			
			#region VanueIdSource
			if (CanDeepSave(entity, "Hotel|VanueIdSource", deepSaveType, innerList) 
				&& entity.VanueIdSource != null)
			{
				DataRepository.HotelProvider.Save(transactionManager, entity.VanueIdSource);
				entity.VanueId = entity.VanueIdSource.Id;
			}
			#endregion 
			
			#region UserIdSource
			if (CanDeepSave(entity, "Users|UserIdSource", deepSaveType, innerList) 
				&& entity.UserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.UserIdSource);
				entity.UserId = entity.UserIdSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ServeyResponseChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.ServeyResponse</c>
	///</summary>
	public enum ServeyResponseChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Booking</c> at BookingIdSource
		///</summary>
		[ChildEntityType(typeof(Booking))]
		Booking,
			
		///<summary>
		/// Composite Property for <c>Hotel</c> at VanueIdSource
		///</summary>
		[ChildEntityType(typeof(Hotel))]
		Hotel,
			
		///<summary>
		/// Composite Property for <c>Users</c> at UserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
		}
	
	#endregion ServeyResponseChildEntityTypes
	
	#region ServeyResponseFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ServeyResponseColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ServeyResponse"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ServeyResponseFilterBuilder : SqlFilterBuilder<ServeyResponseColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ServeyResponseFilterBuilder class.
		/// </summary>
		public ServeyResponseFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ServeyResponseFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ServeyResponseFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ServeyResponseFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ServeyResponseFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ServeyResponseFilterBuilder
	
	#region ServeyResponseParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ServeyResponseColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ServeyResponse"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ServeyResponseParameterBuilder : ParameterizedSqlFilterBuilder<ServeyResponseColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ServeyResponseParameterBuilder class.
		/// </summary>
		public ServeyResponseParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ServeyResponseParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ServeyResponseParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ServeyResponseParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ServeyResponseParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ServeyResponseParameterBuilder
	
	#region ServeyResponseSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ServeyResponseColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ServeyResponse"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ServeyResponseSortBuilder : SqlSortBuilder<ServeyResponseColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ServeyResponseSqlSortBuilder class.
		/// </summary>
		public ServeyResponseSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ServeyResponseSortBuilder
	
} // end namespace
