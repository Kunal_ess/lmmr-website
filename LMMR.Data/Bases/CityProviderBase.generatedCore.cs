﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CityProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CityProviderBaseCore : EntityProviderBase<LMMR.Entities.City, LMMR.Entities.CityKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.CityKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_City_Country key.
		///		FK_City_Country Description: 
		/// </summary>
		/// <param name="_countryId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.City objects.</returns>
		public TList<City> GetByCountryId(System.Int64 _countryId)
		{
			int count = -1;
			return GetByCountryId(_countryId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_City_Country key.
		///		FK_City_Country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.City objects.</returns>
		/// <remarks></remarks>
		public TList<City> GetByCountryId(TransactionManager transactionManager, System.Int64 _countryId)
		{
			int count = -1;
			return GetByCountryId(transactionManager, _countryId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_City_Country key.
		///		FK_City_Country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.City objects.</returns>
		public TList<City> GetByCountryId(TransactionManager transactionManager, System.Int64 _countryId, int start, int pageLength)
		{
			int count = -1;
			return GetByCountryId(transactionManager, _countryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_City_Country key.
		///		fkCityCountry Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_countryId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.City objects.</returns>
		public TList<City> GetByCountryId(System.Int64 _countryId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCountryId(null, _countryId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_City_Country key.
		///		fkCityCountry Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_countryId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.City objects.</returns>
		public TList<City> GetByCountryId(System.Int64 _countryId, int start, int pageLength,out int count)
		{
			return GetByCountryId(null, _countryId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_City_Country key.
		///		FK_City_Country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.City objects.</returns>
		public abstract TList<City> GetByCountryId(TransactionManager transactionManager, System.Int64 _countryId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.City Get(TransactionManager transactionManager, LMMR.Entities.CityKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_City index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.City"/> class.</returns>
		public LMMR.Entities.City GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_City index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.City"/> class.</returns>
		public LMMR.Entities.City GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_City index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.City"/> class.</returns>
		public LMMR.Entities.City GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_City index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.City"/> class.</returns>
		public LMMR.Entities.City GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_City index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.City"/> class.</returns>
		public LMMR.Entities.City GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_City index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.City"/> class.</returns>
		public abstract LMMR.Entities.City GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;City&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;City&gt;"/></returns>
		public static TList<City> Fill(IDataReader reader, TList<City> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.City c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("City")
					.Append("|").Append((System.Int64)reader[((int)CityColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<City>(
					key.ToString(), // EntityTrackingKey
					"City",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.City();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)CityColumn.Id - 1)];
					c.CountryId = (System.Int64)reader[((int)CityColumn.CountryId - 1)];
					c.City = (reader.IsDBNull(((int)CityColumn.City - 1)))?null:(System.String)reader[((int)CityColumn.City - 1)];
					c.IsActive = (reader.IsDBNull(((int)CityColumn.IsActive - 1)))?null:(System.Boolean?)reader[((int)CityColumn.IsActive - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.City"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.City"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.City entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)CityColumn.Id - 1)];
			entity.CountryId = (System.Int64)reader[((int)CityColumn.CountryId - 1)];
			entity.City = (reader.IsDBNull(((int)CityColumn.City - 1)))?null:(System.String)reader[((int)CityColumn.City - 1)];
			entity.IsActive = (reader.IsDBNull(((int)CityColumn.IsActive - 1)))?null:(System.Boolean?)reader[((int)CityColumn.IsActive - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.City"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.City"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.City entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.CountryId = (System.Int64)dataRow["CountryId"];
			entity.City = Convert.IsDBNull(dataRow["City"]) ? null : (System.String)dataRow["City"];
			entity.IsActive = Convert.IsDBNull(dataRow["IsActive"]) ? null : (System.Boolean?)dataRow["IsActive"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.City"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.City Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.City entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CountryIdSource	
			if (CanDeepLoad(entity, "Country|CountryIdSource", deepLoadType, innerList) 
				&& entity.CountryIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CountryId;
				Country tmpEntity = EntityManager.LocateEntity<Country>(EntityLocator.ConstructKeyFromPkItems(typeof(Country), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CountryIdSource = tmpEntity;
				else
					entity.CountryIdSource = DataRepository.CountryProvider.GetById(transactionManager, entity.CountryId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CountryIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CountryIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CountryProvider.DeepLoad(transactionManager, entity.CountryIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CountryIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region UserDetailsCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<UserDetails>|UserDetailsCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UserDetailsCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.UserDetailsCollection = DataRepository.UserDetailsProvider.GetByCityId(transactionManager, entity.Id);

				if (deep && entity.UserDetailsCollection.Count > 0)
				{
					deepHandles.Add("UserDetailsCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<UserDetails>) DataRepository.UserDetailsProvider.DeepLoad,
						new object[] { transactionManager, entity.UserDetailsCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CityLanguageCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CityLanguage>|CityLanguageCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CityLanguageCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CityLanguageCollection = DataRepository.CityLanguageProvider.GetByCityId(transactionManager, entity.Id);

				if (deep && entity.CityLanguageCollection.Count > 0)
				{
					deepHandles.Add("CityLanguageCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CityLanguage>) DataRepository.CityLanguageProvider.DeepLoad,
						new object[] { transactionManager, entity.CityLanguageCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region FrontEndBottomCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<FrontEndBottom>|FrontEndBottomCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FrontEndBottomCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.FrontEndBottomCollection = DataRepository.FrontEndBottomProvider.GetByCityId(transactionManager, entity.Id);

				if (deep && entity.FrontEndBottomCollection.Count > 0)
				{
					deepHandles.Add("FrontEndBottomCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<FrontEndBottom>) DataRepository.FrontEndBottomProvider.DeepLoad,
						new object[] { transactionManager, entity.FrontEndBottomCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region MainPointCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<MainPoint>|MainPointCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MainPointCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.MainPointCollection = DataRepository.MainPointProvider.GetByCityId(transactionManager, entity.Id);

				if (deep && entity.MainPointCollection.Count > 0)
				{
					deepHandles.Add("MainPointCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<MainPoint>) DataRepository.MainPointProvider.DeepLoad,
						new object[] { transactionManager, entity.MainPointCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ZoneCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Zone>|ZoneCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ZoneCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ZoneCollection = DataRepository.ZoneProvider.GetByCityId(transactionManager, entity.Id);

				if (deep && entity.ZoneCollection.Count > 0)
				{
					deepHandles.Add("ZoneCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Zone>) DataRepository.ZoneProvider.DeepLoad,
						new object[] { transactionManager, entity.ZoneCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PriorityBasketCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PriorityBasket>|PriorityBasketCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PriorityBasketCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PriorityBasketCollection = DataRepository.PriorityBasketProvider.GetByCityId(transactionManager, entity.Id);

				if (deep && entity.PriorityBasketCollection.Count > 0)
				{
					deepHandles.Add("PriorityBasketCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PriorityBasket>) DataRepository.PriorityBasketProvider.DeepLoad,
						new object[] { transactionManager, entity.PriorityBasketCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.City object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.City instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.City Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.City entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CountryIdSource
			if (CanDeepSave(entity, "Country|CountryIdSource", deepSaveType, innerList) 
				&& entity.CountryIdSource != null)
			{
				DataRepository.CountryProvider.Save(transactionManager, entity.CountryIdSource);
				entity.CountryId = entity.CountryIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<UserDetails>
				if (CanDeepSave(entity.UserDetailsCollection, "List<UserDetails>|UserDetailsCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(UserDetails child in entity.UserDetailsCollection)
					{
						if(child.CityIdSource != null)
						{
							child.CityId = child.CityIdSource.Id;
						}
						else
						{
							child.CityId = entity.Id;
						}

					}

					if (entity.UserDetailsCollection.Count > 0 || entity.UserDetailsCollection.DeletedItems.Count > 0)
					{
						//DataRepository.UserDetailsProvider.Save(transactionManager, entity.UserDetailsCollection);
						
						deepHandles.Add("UserDetailsCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< UserDetails >) DataRepository.UserDetailsProvider.DeepSave,
							new object[] { transactionManager, entity.UserDetailsCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CityLanguage>
				if (CanDeepSave(entity.CityLanguageCollection, "List<CityLanguage>|CityLanguageCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CityLanguage child in entity.CityLanguageCollection)
					{
						if(child.CityIdSource != null)
						{
							child.CityId = child.CityIdSource.Id;
						}
						else
						{
							child.CityId = entity.Id;
						}

					}

					if (entity.CityLanguageCollection.Count > 0 || entity.CityLanguageCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CityLanguageProvider.Save(transactionManager, entity.CityLanguageCollection);
						
						deepHandles.Add("CityLanguageCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CityLanguage >) DataRepository.CityLanguageProvider.DeepSave,
							new object[] { transactionManager, entity.CityLanguageCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<FrontEndBottom>
				if (CanDeepSave(entity.FrontEndBottomCollection, "List<FrontEndBottom>|FrontEndBottomCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(FrontEndBottom child in entity.FrontEndBottomCollection)
					{
						if(child.CityIdSource != null)
						{
							child.CityId = child.CityIdSource.Id;
						}
						else
						{
							child.CityId = entity.Id;
						}

					}

					if (entity.FrontEndBottomCollection.Count > 0 || entity.FrontEndBottomCollection.DeletedItems.Count > 0)
					{
						//DataRepository.FrontEndBottomProvider.Save(transactionManager, entity.FrontEndBottomCollection);
						
						deepHandles.Add("FrontEndBottomCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< FrontEndBottom >) DataRepository.FrontEndBottomProvider.DeepSave,
							new object[] { transactionManager, entity.FrontEndBottomCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<MainPoint>
				if (CanDeepSave(entity.MainPointCollection, "List<MainPoint>|MainPointCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(MainPoint child in entity.MainPointCollection)
					{
						if(child.CityIdSource != null)
						{
							child.CityId = child.CityIdSource.Id;
						}
						else
						{
							child.CityId = entity.Id;
						}

					}

					if (entity.MainPointCollection.Count > 0 || entity.MainPointCollection.DeletedItems.Count > 0)
					{
						//DataRepository.MainPointProvider.Save(transactionManager, entity.MainPointCollection);
						
						deepHandles.Add("MainPointCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< MainPoint >) DataRepository.MainPointProvider.DeepSave,
							new object[] { transactionManager, entity.MainPointCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Zone>
				if (CanDeepSave(entity.ZoneCollection, "List<Zone>|ZoneCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Zone child in entity.ZoneCollection)
					{
						if(child.CityIdSource != null)
						{
							child.CityId = child.CityIdSource.Id;
						}
						else
						{
							child.CityId = entity.Id;
						}

					}

					if (entity.ZoneCollection.Count > 0 || entity.ZoneCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ZoneProvider.Save(transactionManager, entity.ZoneCollection);
						
						deepHandles.Add("ZoneCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Zone >) DataRepository.ZoneProvider.DeepSave,
							new object[] { transactionManager, entity.ZoneCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<PriorityBasket>
				if (CanDeepSave(entity.PriorityBasketCollection, "List<PriorityBasket>|PriorityBasketCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PriorityBasket child in entity.PriorityBasketCollection)
					{
						if(child.CityIdSource != null)
						{
							child.CityId = child.CityIdSource.Id;
						}
						else
						{
							child.CityId = entity.Id;
						}

					}

					if (entity.PriorityBasketCollection.Count > 0 || entity.PriorityBasketCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PriorityBasketProvider.Save(transactionManager, entity.PriorityBasketCollection);
						
						deepHandles.Add("PriorityBasketCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PriorityBasket >) DataRepository.PriorityBasketProvider.DeepSave,
							new object[] { transactionManager, entity.PriorityBasketCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CityChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.City</c>
	///</summary>
	public enum CityChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Country</c> at CountryIdSource
		///</summary>
		[ChildEntityType(typeof(Country))]
		Country,
	
		///<summary>
		/// Collection of <c>City</c> as OneToMany for UserDetailsCollection
		///</summary>
		[ChildEntityType(typeof(TList<UserDetails>))]
		UserDetailsCollection,

		///<summary>
		/// Collection of <c>City</c> as OneToMany for CityLanguageCollection
		///</summary>
		[ChildEntityType(typeof(TList<CityLanguage>))]
		CityLanguageCollection,

		///<summary>
		/// Collection of <c>City</c> as OneToMany for FrontEndBottomCollection
		///</summary>
		[ChildEntityType(typeof(TList<FrontEndBottom>))]
		FrontEndBottomCollection,

		///<summary>
		/// Collection of <c>City</c> as OneToMany for MainPointCollection
		///</summary>
		[ChildEntityType(typeof(TList<MainPoint>))]
		MainPointCollection,

		///<summary>
		/// Collection of <c>City</c> as OneToMany for ZoneCollection
		///</summary>
		[ChildEntityType(typeof(TList<Zone>))]
		ZoneCollection,

		///<summary>
		/// Collection of <c>City</c> as OneToMany for PriorityBasketCollection
		///</summary>
		[ChildEntityType(typeof(TList<PriorityBasket>))]
		PriorityBasketCollection,
	}
	
	#endregion CityChildEntityTypes
	
	#region CityFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="City"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CityFilterBuilder : SqlFilterBuilder<CityColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CityFilterBuilder class.
		/// </summary>
		public CityFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CityFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CityFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CityFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CityFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CityFilterBuilder
	
	#region CityParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="City"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CityParameterBuilder : ParameterizedSqlFilterBuilder<CityColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CityParameterBuilder class.
		/// </summary>
		public CityParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CityParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CityParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CityParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CityParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CityParameterBuilder
	
	#region CitySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="City"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CitySortBuilder : SqlSortBuilder<CityColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CitySqlSortBuilder class.
		/// </summary>
		public CitySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CitySortBuilder
	
} // end namespace
