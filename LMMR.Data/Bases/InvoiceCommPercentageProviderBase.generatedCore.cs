﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="InvoiceCommPercentageProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class InvoiceCommPercentageProviderBaseCore : EntityProviderBase<LMMR.Entities.InvoiceCommPercentage, LMMR.Entities.InvoiceCommPercentageKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.InvoiceCommPercentageKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_InvoiceCommPercentage_InvoiceCommission key.
		///		FK_InvoiceCommPercentage_InvoiceCommission Description: 
		/// </summary>
		/// <param name="_commissionDayId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.InvoiceCommPercentage objects.</returns>
		public TList<InvoiceCommPercentage> GetByCommissionDayId(System.Int32? _commissionDayId)
		{
			int count = -1;
			return GetByCommissionDayId(_commissionDayId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_InvoiceCommPercentage_InvoiceCommission key.
		///		FK_InvoiceCommPercentage_InvoiceCommission Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_commissionDayId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.InvoiceCommPercentage objects.</returns>
		/// <remarks></remarks>
		public TList<InvoiceCommPercentage> GetByCommissionDayId(TransactionManager transactionManager, System.Int32? _commissionDayId)
		{
			int count = -1;
			return GetByCommissionDayId(transactionManager, _commissionDayId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_InvoiceCommPercentage_InvoiceCommission key.
		///		FK_InvoiceCommPercentage_InvoiceCommission Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_commissionDayId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.InvoiceCommPercentage objects.</returns>
		public TList<InvoiceCommPercentage> GetByCommissionDayId(TransactionManager transactionManager, System.Int32? _commissionDayId, int start, int pageLength)
		{
			int count = -1;
			return GetByCommissionDayId(transactionManager, _commissionDayId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_InvoiceCommPercentage_InvoiceCommission key.
		///		fkInvoiceCommPercentageInvoiceCommission Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_commissionDayId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.InvoiceCommPercentage objects.</returns>
		public TList<InvoiceCommPercentage> GetByCommissionDayId(System.Int32? _commissionDayId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCommissionDayId(null, _commissionDayId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_InvoiceCommPercentage_InvoiceCommission key.
		///		fkInvoiceCommPercentageInvoiceCommission Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_commissionDayId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.InvoiceCommPercentage objects.</returns>
		public TList<InvoiceCommPercentage> GetByCommissionDayId(System.Int32? _commissionDayId, int start, int pageLength,out int count)
		{
			return GetByCommissionDayId(null, _commissionDayId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_InvoiceCommPercentage_InvoiceCommission key.
		///		FK_InvoiceCommPercentage_InvoiceCommission Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_commissionDayId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.InvoiceCommPercentage objects.</returns>
		public abstract TList<InvoiceCommPercentage> GetByCommissionDayId(TransactionManager transactionManager, System.Int32? _commissionDayId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.InvoiceCommPercentage Get(TransactionManager transactionManager, LMMR.Entities.InvoiceCommPercentageKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_InvoiceCommPercentage index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.InvoiceCommPercentage"/> class.</returns>
		public LMMR.Entities.InvoiceCommPercentage GetById(System.Int32 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_InvoiceCommPercentage index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.InvoiceCommPercentage"/> class.</returns>
		public LMMR.Entities.InvoiceCommPercentage GetById(System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_InvoiceCommPercentage index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.InvoiceCommPercentage"/> class.</returns>
		public LMMR.Entities.InvoiceCommPercentage GetById(TransactionManager transactionManager, System.Int32 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_InvoiceCommPercentage index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.InvoiceCommPercentage"/> class.</returns>
		public LMMR.Entities.InvoiceCommPercentage GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_InvoiceCommPercentage index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.InvoiceCommPercentage"/> class.</returns>
		public LMMR.Entities.InvoiceCommPercentage GetById(System.Int32 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_InvoiceCommPercentage index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.InvoiceCommPercentage"/> class.</returns>
		public abstract LMMR.Entities.InvoiceCommPercentage GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;InvoiceCommPercentage&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;InvoiceCommPercentage&gt;"/></returns>
		public static TList<InvoiceCommPercentage> Fill(IDataReader reader, TList<InvoiceCommPercentage> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.InvoiceCommPercentage c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("InvoiceCommPercentage")
					.Append("|").Append((System.Int32)reader[((int)InvoiceCommPercentageColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<InvoiceCommPercentage>(
					key.ToString(), // EntityTrackingKey
					"InvoiceCommPercentage",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.InvoiceCommPercentage();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int32)reader[((int)InvoiceCommPercentageColumn.Id - 1)];
					c.ProfileName = (reader.IsDBNull(((int)InvoiceCommPercentageColumn.ProfileName - 1)))?null:(System.String)reader[((int)InvoiceCommPercentageColumn.ProfileName - 1)];
					c.CommissionDayId = (reader.IsDBNull(((int)InvoiceCommPercentageColumn.CommissionDayId - 1)))?null:(System.Int32?)reader[((int)InvoiceCommPercentageColumn.CommissionDayId - 1)];
					c.CommissionPercentage = (reader.IsDBNull(((int)InvoiceCommPercentageColumn.CommissionPercentage - 1)))?null:(System.Int32?)reader[((int)InvoiceCommPercentageColumn.CommissionPercentage - 1)];
					c.HotelId = (reader.IsDBNull(((int)InvoiceCommPercentageColumn.HotelId - 1)))?null:(System.Int64?)reader[((int)InvoiceCommPercentageColumn.HotelId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.InvoiceCommPercentage"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.InvoiceCommPercentage"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.InvoiceCommPercentage entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int32)reader[((int)InvoiceCommPercentageColumn.Id - 1)];
			entity.ProfileName = (reader.IsDBNull(((int)InvoiceCommPercentageColumn.ProfileName - 1)))?null:(System.String)reader[((int)InvoiceCommPercentageColumn.ProfileName - 1)];
			entity.CommissionDayId = (reader.IsDBNull(((int)InvoiceCommPercentageColumn.CommissionDayId - 1)))?null:(System.Int32?)reader[((int)InvoiceCommPercentageColumn.CommissionDayId - 1)];
			entity.CommissionPercentage = (reader.IsDBNull(((int)InvoiceCommPercentageColumn.CommissionPercentage - 1)))?null:(System.Int32?)reader[((int)InvoiceCommPercentageColumn.CommissionPercentage - 1)];
			entity.HotelId = (reader.IsDBNull(((int)InvoiceCommPercentageColumn.HotelId - 1)))?null:(System.Int64?)reader[((int)InvoiceCommPercentageColumn.HotelId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.InvoiceCommPercentage"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.InvoiceCommPercentage"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.InvoiceCommPercentage entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int32)dataRow["Id"];
			entity.ProfileName = Convert.IsDBNull(dataRow["ProfileName"]) ? null : (System.String)dataRow["ProfileName"];
			entity.CommissionDayId = Convert.IsDBNull(dataRow["CommissionDayId"]) ? null : (System.Int32?)dataRow["CommissionDayId"];
			entity.CommissionPercentage = Convert.IsDBNull(dataRow["CommissionPercentage"]) ? null : (System.Int32?)dataRow["CommissionPercentage"];
			entity.HotelId = Convert.IsDBNull(dataRow["HotelId"]) ? null : (System.Int64?)dataRow["HotelId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.InvoiceCommPercentage"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.InvoiceCommPercentage Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.InvoiceCommPercentage entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CommissionDayIdSource	
			if (CanDeepLoad(entity, "InvoiceCommission|CommissionDayIdSource", deepLoadType, innerList) 
				&& entity.CommissionDayIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.CommissionDayId ?? (int)0);
				InvoiceCommission tmpEntity = EntityManager.LocateEntity<InvoiceCommission>(EntityLocator.ConstructKeyFromPkItems(typeof(InvoiceCommission), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CommissionDayIdSource = tmpEntity;
				else
					entity.CommissionDayIdSource = DataRepository.InvoiceCommissionProvider.GetById(transactionManager, (entity.CommissionDayId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CommissionDayIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CommissionDayIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.InvoiceCommissionProvider.DeepLoad(transactionManager, entity.CommissionDayIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CommissionDayIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.InvoiceCommPercentage object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.InvoiceCommPercentage instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.InvoiceCommPercentage Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.InvoiceCommPercentage entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CommissionDayIdSource
			if (CanDeepSave(entity, "InvoiceCommission|CommissionDayIdSource", deepSaveType, innerList) 
				&& entity.CommissionDayIdSource != null)
			{
				DataRepository.InvoiceCommissionProvider.Save(transactionManager, entity.CommissionDayIdSource);
				entity.CommissionDayId = entity.CommissionDayIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region InvoiceCommPercentageChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.InvoiceCommPercentage</c>
	///</summary>
	public enum InvoiceCommPercentageChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>InvoiceCommission</c> at CommissionDayIdSource
		///</summary>
		[ChildEntityType(typeof(InvoiceCommission))]
		InvoiceCommission,
		}
	
	#endregion InvoiceCommPercentageChildEntityTypes
	
	#region InvoiceCommPercentageFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;InvoiceCommPercentageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="InvoiceCommPercentage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class InvoiceCommPercentageFilterBuilder : SqlFilterBuilder<InvoiceCommPercentageColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the InvoiceCommPercentageFilterBuilder class.
		/// </summary>
		public InvoiceCommPercentageFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the InvoiceCommPercentageFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public InvoiceCommPercentageFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the InvoiceCommPercentageFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public InvoiceCommPercentageFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion InvoiceCommPercentageFilterBuilder
	
	#region InvoiceCommPercentageParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;InvoiceCommPercentageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="InvoiceCommPercentage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class InvoiceCommPercentageParameterBuilder : ParameterizedSqlFilterBuilder<InvoiceCommPercentageColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the InvoiceCommPercentageParameterBuilder class.
		/// </summary>
		public InvoiceCommPercentageParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the InvoiceCommPercentageParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public InvoiceCommPercentageParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the InvoiceCommPercentageParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public InvoiceCommPercentageParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion InvoiceCommPercentageParameterBuilder
	
	#region InvoiceCommPercentageSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;InvoiceCommPercentageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="InvoiceCommPercentage"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class InvoiceCommPercentageSortBuilder : SqlSortBuilder<InvoiceCommPercentageColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the InvoiceCommPercentageSqlSortBuilder class.
		/// </summary>
		public InvoiceCommPercentageSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion InvoiceCommPercentageSortBuilder
	
} // end namespace
