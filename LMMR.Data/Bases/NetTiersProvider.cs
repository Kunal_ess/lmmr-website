﻿
#region Using directives

using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Configuration.Provider;

using LMMR.Entities;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// The base class to implements to create a .NetTiers provider.
	///</summary>
	public abstract class NetTiersProvider : NetTiersProviderBase
	{
		
		///<summary>
		/// Current PackageMasterProviderBase instance.
		///</summary>
		public virtual PackageMasterProviderBase PackageMasterProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current PackageByHotelProviderBase instance.
		///</summary>
		public virtual PackageByHotelProviderBase PackageByHotelProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current PackageItemsProviderBase instance.
		///</summary>
		public virtual PackageItemsProviderBase PackageItemsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ActualPackagePriceProviderBase instance.
		///</summary>
		public virtual ActualPackagePriceProviderBase ActualPackagePriceProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current PackageByHotelTrailProviderBase instance.
		///</summary>
		public virtual PackageByHotelTrailProviderBase PackageByHotelTrailProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current OthersProviderBase instance.
		///</summary>
		public virtual OthersProviderBase OthersProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current OtherItemsPricingbyHotelProviderBase instance.
		///</summary>
		public virtual OtherItemsPricingbyHotelProviderBase OtherItemsPricingbyHotelProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current PackageDescriptionProviderBase instance.
		///</summary>
		public virtual PackageDescriptionProviderBase PackageDescriptionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current PolicyHotelMappingProviderBase instance.
		///</summary>
		public virtual PolicyHotelMappingProviderBase PolicyHotelMappingProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current PackageItemMappingProviderBase instance.
		///</summary>
		public virtual PackageItemMappingProviderBase PackageItemMappingProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current PermissionsProviderBase instance.
		///</summary>
		public virtual PermissionsProviderBase PermissionsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current OtherDescriptionLanguageProviderBase instance.
		///</summary>
		public virtual OtherDescriptionLanguageProviderBase OtherDescriptionLanguageProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current PackageMasterDescriptionProviderBase instance.
		///</summary>
		public virtual PackageMasterDescriptionProviderBase PackageMasterDescriptionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current NewsLetterSubscriberProviderBase instance.
		///</summary>
		public virtual NewsLetterSubscriberProviderBase NewsLetterSubscriberProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current MeetingRoomProviderBase instance.
		///</summary>
		public virtual MeetingRoomProviderBase MeetingRoomProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current MainPointProviderBase instance.
		///</summary>
		public virtual MainPointProviderBase MainPointProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current LeftMenuLinkProviderBase instance.
		///</summary>
		public virtual LeftMenuLinkProviderBase LeftMenuLinkProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current LanguageProviderBase instance.
		///</summary>
		public virtual LanguageProviderBase LanguageProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current MeetingRoomTrailProviderBase instance.
		///</summary>
		public virtual MeetingRoomTrailProviderBase MeetingRoomTrailProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current MeetingRoomConfigProviderBase instance.
		///</summary>
		public virtual MeetingRoomConfigProviderBase MeetingRoomConfigProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current MeetingRoomConfigTrailProviderBase instance.
		///</summary>
		public virtual MeetingRoomConfigTrailProviderBase MeetingRoomConfigTrailProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current MeetingRoomDescProviderBase instance.
		///</summary>
		public virtual MeetingRoomDescProviderBase MeetingRoomDescProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current MeetingRoomPictureVideoTrailProviderBase instance.
		///</summary>
		public virtual MeetingRoomPictureVideoTrailProviderBase MeetingRoomPictureVideoTrailProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current MeetingRoomPictureVideoProviderBase instance.
		///</summary>
		public virtual MeetingRoomPictureVideoProviderBase MeetingRoomPictureVideoProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current PriorityBasketProviderBase instance.
		///</summary>
		public virtual PriorityBasketProviderBase PriorityBasketProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current MeetingRoomDescTrailProviderBase instance.
		///</summary>
		public virtual MeetingRoomDescTrailProviderBase MeetingRoomDescTrailProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current RankingAlgoMasterProviderBase instance.
		///</summary>
		public virtual RankingAlgoMasterProviderBase RankingAlgoMasterProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current RankingAlgoConditionProviderBase instance.
		///</summary>
		public virtual RankingAlgoConditionProviderBase RankingAlgoConditionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current UsersProviderBase instance.
		///</summary>
		public virtual UsersProviderBase UsersProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current StaticLebelProviderBase instance.
		///</summary>
		public virtual StaticLebelProviderBase StaticLebelProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current StaticLebelDescriptionProviderBase instance.
		///</summary>
		public virtual StaticLebelDescriptionProviderBase StaticLebelDescriptionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current StaffAccountProviderBase instance.
		///</summary>
		public virtual StaffAccountProviderBase StaffAccountProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current StatisticsProviderBase instance.
		///</summary>
		public virtual StatisticsProviderBase StatisticsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SpecialPriceForBedroomProviderBase instance.
		///</summary>
		public virtual SpecialPriceForBedroomProviderBase SpecialPriceForBedroomProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ZoneProviderBase instance.
		///</summary>
		public virtual ZoneProviderBase ZoneProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TransferProviderBase instance.
		///</summary>
		public virtual TransferProviderBase TransferProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current WhiteLabelProviderBase instance.
		///</summary>
		public virtual WhiteLabelProviderBase WhiteLabelProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current WhiteLabelMappingWithHotelProviderBase instance.
		///</summary>
		public virtual WhiteLabelMappingWithHotelProviderBase WhiteLabelMappingWithHotelProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current UserDetailsProviderBase instance.
		///</summary>
		public virtual UserDetailsProviderBase UserDetailsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SpecialPriceAndPromoTrailProviderBase instance.
		///</summary>
		public virtual SpecialPriceAndPromoTrailProviderBase SpecialPriceAndPromoTrailProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SpecialPriceAndPromoProviderBase instance.
		///</summary>
		public virtual SpecialPriceAndPromoProviderBase SpecialPriceAndPromoProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SpandPpackageProviderBase instance.
		///</summary>
		public virtual SpandPpackageProviderBase SpandPpackageProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ServayDescriptionProviderBase instance.
		///</summary>
		public virtual ServayDescriptionProviderBase ServayDescriptionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SearchTracerProviderBase instance.
		///</summary>
		public virtual SearchTracerProviderBase SearchTracerProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current RpfProviderBase instance.
		///</summary>
		public virtual RpfProviderBase RpfProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current RolesProviderBase instance.
		///</summary>
		public virtual RolesProviderBase RolesProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ServeyAnswerProviderBase instance.
		///</summary>
		public virtual ServeyAnswerProviderBase ServeyAnswerProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ServeyAnswerDescriptionProviderBase instance.
		///</summary>
		public virtual ServeyAnswerDescriptionProviderBase ServeyAnswerDescriptionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SpandPbedroomProviderBase instance.
		///</summary>
		public virtual SpandPbedroomProviderBase SpandPbedroomProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SpandPmeetingRoomProviderBase instance.
		///</summary>
		public virtual SpandPmeetingRoomProviderBase SpandPmeetingRoomProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ServeyQuestionProviderBase instance.
		///</summary>
		public virtual ServeyQuestionProviderBase ServeyQuestionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ServeyResponseProviderBase instance.
		///</summary>
		public virtual ServeyResponseProviderBase ServeyResponseProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current InvoiceCommissionProviderBase instance.
		///</summary>
		public virtual InvoiceCommissionProviderBase InvoiceCommissionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ServeyresultProviderBase instance.
		///</summary>
		public virtual ServeyresultProviderBase ServeyresultProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CurrencyProviderBase instance.
		///</summary>
		public virtual CurrencyProviderBase CurrencyProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current BedRoomProviderBase instance.
		///</summary>
		public virtual BedRoomProviderBase BedRoomProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current BookingProviderBase instance.
		///</summary>
		public virtual BookingProviderBase BookingProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current BedRoomPictureImageProviderBase instance.
		///</summary>
		public virtual BedRoomPictureImageProviderBase BedRoomPictureImageProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CountryProviderBase instance.
		///</summary>
		public virtual CountryProviderBase CountryProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current BookingLogsProviderBase instance.
		///</summary>
		public virtual BookingLogsProviderBase BookingLogsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CmsProviderBase instance.
		///</summary>
		public virtual CmsProviderBase CmsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CancelationPolicyProviderBase instance.
		///</summary>
		public virtual CancelationPolicyProviderBase CancelationPolicyProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CityLanguageProviderBase instance.
		///</summary>
		public virtual CityLanguageProviderBase CityLanguageProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current BookedMeetingRoomProviderBase instance.
		///</summary>
		public virtual BookedMeetingRoomProviderBase BookedMeetingRoomProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current BuildMeetingConfigureProviderBase instance.
		///</summary>
		public virtual BuildMeetingConfigureProviderBase BuildMeetingConfigureProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CancelationPolicyLanguageProviderBase instance.
		///</summary>
		public virtual CancelationPolicyLanguageProviderBase CancelationPolicyLanguageProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current BuildPackageConfigureProviderBase instance.
		///</summary>
		public virtual BuildPackageConfigureProviderBase BuildPackageConfigureProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current BuildPackageConfigureDescProviderBase instance.
		///</summary>
		public virtual BuildPackageConfigureDescProviderBase BuildPackageConfigureDescProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CityProviderBase instance.
		///</summary>
		public virtual CityProviderBase CityProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AgentUserProviderBase instance.
		///</summary>
		public virtual AgentUserProviderBase AgentUserProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current BookedBedRoomProviderBase instance.
		///</summary>
		public virtual BookedBedRoomProviderBase BookedBedRoomProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AgencyClientProviderBase instance.
		///</summary>
		public virtual AgencyClientProviderBase AgencyClientProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AgencyInvoiceProviderBase instance.
		///</summary>
		public virtual AgencyInvoiceProviderBase AgencyInvoiceProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current InvoiceProviderBase instance.
		///</summary>
		public virtual InvoiceProviderBase InvoiceProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AvailabilityProviderBase instance.
		///</summary>
		public virtual AvailabilityProviderBase AvailabilityProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AvailabilityTrailProviderBase instance.
		///</summary>
		public virtual AvailabilityTrailProviderBase AvailabilityTrailProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current BedRoomPictureImageTrailProviderBase instance.
		///</summary>
		public virtual BedRoomPictureImageTrailProviderBase BedRoomPictureImageTrailProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AvailibilityOfRoomsProviderBase instance.
		///</summary>
		public virtual AvailibilityOfRoomsProviderBase AvailibilityOfRoomsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current BedRoomDescProviderBase instance.
		///</summary>
		public virtual BedRoomDescProviderBase BedRoomDescProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current BedRoomDescTrailProviderBase instance.
		///</summary>
		public virtual BedRoomDescTrailProviderBase BedRoomDescTrailProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current BedRoomTrailProviderBase instance.
		///</summary>
		public virtual BedRoomTrailProviderBase BedRoomTrailProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CmsDescriptionProviderBase instance.
		///</summary>
		public virtual CmsDescriptionProviderBase CmsDescriptionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CommissionPercentageProviderBase instance.
		///</summary>
		public virtual CommissionPercentageProviderBase CommissionPercentageProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current HotelProviderBase instance.
		///</summary>
		public virtual HotelProviderBase HotelProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ZoneLanguageProviderBase instance.
		///</summary>
		public virtual ZoneLanguageProviderBase ZoneLanguageProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current HotelContactProviderBase instance.
		///</summary>
		public virtual HotelContactProviderBase HotelContactProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current HotelTrailProviderBase instance.
		///</summary>
		public virtual HotelTrailProviderBase HotelTrailProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current FrontEndBottomProviderBase instance.
		///</summary>
		public virtual FrontEndBottomProviderBase FrontEndBottomProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current HotelContactTrailProviderBase instance.
		///</summary>
		public virtual HotelContactTrailProviderBase HotelContactTrailProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current HotelDescProviderBase instance.
		///</summary>
		public virtual HotelDescProviderBase HotelDescProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current HoteldescTrailProviderBase instance.
		///</summary>
		public virtual HoteldescTrailProviderBase HoteldescTrailProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current HotelPhotoVideoGallaryProviderBase instance.
		///</summary>
		public virtual HotelPhotoVideoGallaryProviderBase HotelPhotoVideoGallaryProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current HotelOwnerLinkProviderBase instance.
		///</summary>
		public virtual HotelOwnerLinkProviderBase HotelOwnerLinkProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current HotelFacilitiesTrailProviderBase instance.
		///</summary>
		public virtual HotelFacilitiesTrailProviderBase HotelFacilitiesTrailProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current HotelFacilitiesProviderBase instance.
		///</summary>
		public virtual HotelFacilitiesProviderBase HotelFacilitiesProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current HearUsQuestionProviderBase instance.
		///</summary>
		public virtual HearUsQuestionProviderBase HearUsQuestionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current GroupProviderBase instance.
		///</summary>
		public virtual GroupProviderBase GroupProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current FrontEndBottomDescriptionProviderBase instance.
		///</summary>
		public virtual FrontEndBottomDescriptionProviderBase FrontEndBottomDescriptionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current DashboardLinkProviderBase instance.
		///</summary>
		public virtual DashboardLinkProviderBase DashboardLinkProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CurrencyExchangeLogsProviderBase instance.
		///</summary>
		public virtual CurrencyExchangeLogsProviderBase CurrencyExchangeLogsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CreditCardDetailProviderBase instance.
		///</summary>
		public virtual CreditCardDetailProviderBase CreditCardDetailProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CountryLanguageProviderBase instance.
		///</summary>
		public virtual CountryLanguageProviderBase CountryLanguageProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current DbAuditProviderBase instance.
		///</summary>
		public virtual DbAuditProviderBase DbAuditProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current EmailConfigProviderBase instance.
		///</summary>
		public virtual EmailConfigProviderBase EmailConfigProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current FinancialInfoProviderBase instance.
		///</summary>
		public virtual FinancialInfoProviderBase FinancialInfoProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current FacilityTypeProviderBase instance.
		///</summary>
		public virtual FacilityTypeProviderBase FacilityTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current FacilityProviderBase instance.
		///</summary>
		public virtual FacilityProviderBase FacilityProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current EmailConfigMappingProviderBase instance.
		///</summary>
		public virtual EmailConfigMappingProviderBase EmailConfigMappingProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current InvoiceCommPercentageProviderBase instance.
		///</summary>
		public virtual InvoiceCommPercentageProviderBase InvoiceCommPercentageProvider{get {throw new NotImplementedException();}}
		
		
		///<summary>
		/// Current AvailabilityWithSpandpProviderBase instance.
		///</summary>
		public virtual AvailabilityWithSpandpProviderBase AvailabilityWithSpandpProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current BookingRequestViewListProviderBase instance.
		///</summary>
		public virtual BookingRequestViewListProviderBase BookingRequestViewListProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewAvailabilityAndSpecialManagerProviderBase instance.
		///</summary>
		public virtual ViewAvailabilityAndSpecialManagerProviderBase ViewAvailabilityAndSpecialManagerProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewAvailabilityOfBedRoomProviderBase instance.
		///</summary>
		public virtual ViewAvailabilityOfBedRoomProviderBase ViewAvailabilityOfBedRoomProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewAvailabilityOfRoomsProviderBase instance.
		///</summary>
		public virtual ViewAvailabilityOfRoomsProviderBase ViewAvailabilityOfRoomsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewBookingHotelsProviderBase instance.
		///</summary>
		public virtual ViewBookingHotelsProviderBase ViewBookingHotelsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewbookingrequestProviderBase instance.
		///</summary>
		public virtual ViewbookingrequestProviderBase ViewbookingrequestProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewClientContractProviderBase instance.
		///</summary>
		public virtual ViewClientContractProviderBase ViewClientContractProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewFacilitiesProviderBase instance.
		///</summary>
		public virtual ViewFacilitiesProviderBase ViewFacilitiesProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewFacilityIconProviderBase instance.
		///</summary>
		public virtual ViewFacilityIconProviderBase ViewFacilityIconProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewFindAvailableBedroomProviderBase instance.
		///</summary>
		public virtual ViewFindAvailableBedroomProviderBase ViewFindAvailableBedroomProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewForAvailabilityCheckProviderBase instance.
		///</summary>
		public virtual ViewForAvailabilityCheckProviderBase ViewForAvailabilityCheckProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewForCommissionFieldsProviderBase instance.
		///</summary>
		public virtual ViewForCommissionFieldsProviderBase ViewForCommissionFieldsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewForRequestSearchProviderBase instance.
		///</summary>
		public virtual ViewForRequestSearchProviderBase ViewForRequestSearchProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewForSetInvoiceProviderBase instance.
		///</summary>
		public virtual ViewForSetInvoiceProviderBase ViewForSetInvoiceProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewMeetingRoomAvailabilityProviderBase instance.
		///</summary>
		public virtual ViewMeetingRoomAvailabilityProviderBase ViewMeetingRoomAvailabilityProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewMeetingRoomForRequestProviderBase instance.
		///</summary>
		public virtual ViewMeetingRoomForRequestProviderBase ViewMeetingRoomForRequestProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewReportAvailabilityProviderBase instance.
		///</summary>
		public virtual ViewReportAvailabilityProviderBase ViewReportAvailabilityProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewReportBookingRequestHistoryProviderBase instance.
		///</summary>
		public virtual ViewReportBookingRequestHistoryProviderBase ViewReportBookingRequestHistoryProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewReportCancelationProviderBase instance.
		///</summary>
		public virtual ViewReportCancelationProviderBase ViewReportCancelationProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewReportCommissionControlProviderBase instance.
		///</summary>
		public virtual ViewReportCommissionControlProviderBase ViewReportCommissionControlProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewReportforProfileCompanyAgencyProviderBase instance.
		///</summary>
		public virtual ViewReportforProfileCompanyAgencyProviderBase ViewReportforProfileCompanyAgencyProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewReportInventoryHotelProviderBase instance.
		///</summary>
		public virtual ViewReportInventoryHotelProviderBase ViewReportInventoryHotelProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewreportInventoryReportofExistingHotelProviderBase instance.
		///</summary>
		public virtual ViewreportInventoryReportofExistingHotelProviderBase ViewreportInventoryReportofExistingHotelProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewReportLeadTimeProviderBase instance.
		///</summary>
		public virtual ViewReportLeadTimeProviderBase ViewReportLeadTimeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewReportOnLineInventoryReportProviderBase instance.
		///</summary>
		public virtual ViewReportOnLineInventoryReportProviderBase ViewReportOnLineInventoryReportProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewReportProductionCompanyAgencyProviderBase instance.
		///</summary>
		public virtual ViewReportProductionCompanyAgencyProviderBase ViewReportProductionCompanyAgencyProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewReportProductionReportHotelProviderBase instance.
		///</summary>
		public virtual ViewReportProductionReportHotelProviderBase ViewReportProductionReportHotelProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewReportRankingProviderBase instance.
		///</summary>
		public virtual ViewReportRankingProviderBase ViewReportRankingProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewRequestProviderBase instance.
		///</summary>
		public virtual ViewRequestProviderBase ViewRequestProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewSearchForDay2ProviderBase instance.
		///</summary>
		public virtual ViewSearchForDay2ProviderBase ViewSearchForDay2Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewSelectAllDetailsByPackageandMrProviderBase instance.
		///</summary>
		public virtual ViewSelectAllDetailsByPackageandMrProviderBase ViewSelectAllDetailsByPackageandMrProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewSelectAvailabilityProviderBase instance.
		///</summary>
		public virtual ViewSelectAvailabilityProviderBase ViewSelectAvailabilityProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewSpandPpercentageOfMeetingRoomProviderBase instance.
		///</summary>
		public virtual ViewSpandPpercentageOfMeetingRoomProviderBase ViewSpandPpercentageOfMeetingRoomProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewSpandPpercentageOfPackageProviderBase instance.
		///</summary>
		public virtual ViewSpandPpercentageOfPackageProviderBase ViewSpandPpercentageOfPackageProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ViewWsCheckAvailabilityProviderBase instance.
		///</summary>
		public virtual ViewWsCheckAvailabilityProviderBase ViewWsCheckAvailabilityProvider{get {throw new NotImplementedException();}}
		
	}
}
