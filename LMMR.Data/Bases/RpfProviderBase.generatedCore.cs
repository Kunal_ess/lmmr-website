﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="RpfProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class RpfProviderBaseCore : EntityProviderBase<LMMR.Entities.Rpf, LMMR.Entities.RpfKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.RpfKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_RPF_country key.
		///		FK_RPF_country Description: 
		/// </summary>
		/// <param name="_countryid"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Rpf objects.</returns>
		public TList<Rpf> GetByCountryid(System.Int64? _countryid)
		{
			int count = -1;
			return GetByCountryid(_countryid, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_RPF_country key.
		///		FK_RPF_country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryid"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Rpf objects.</returns>
		/// <remarks></remarks>
		public TList<Rpf> GetByCountryid(TransactionManager transactionManager, System.Int64? _countryid)
		{
			int count = -1;
			return GetByCountryid(transactionManager, _countryid, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_RPF_country key.
		///		FK_RPF_country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryid"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Rpf objects.</returns>
		public TList<Rpf> GetByCountryid(TransactionManager transactionManager, System.Int64? _countryid, int start, int pageLength)
		{
			int count = -1;
			return GetByCountryid(transactionManager, _countryid, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_RPF_country key.
		///		fkRpfCountry Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_countryid"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Rpf objects.</returns>
		public TList<Rpf> GetByCountryid(System.Int64? _countryid, int start, int pageLength)
		{
			int count =  -1;
			return GetByCountryid(null, _countryid, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_RPF_country key.
		///		fkRpfCountry Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_countryid"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Rpf objects.</returns>
		public TList<Rpf> GetByCountryid(System.Int64? _countryid, int start, int pageLength,out int count)
		{
			return GetByCountryid(null, _countryid, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_RPF_country key.
		///		FK_RPF_country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryid"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.Rpf objects.</returns>
		public abstract TList<Rpf> GetByCountryid(TransactionManager transactionManager, System.Int64? _countryid, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_RPF_Users key.
		///		FK_RPF_Users Description: 
		/// </summary>
		/// <param name="_userid"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Rpf objects.</returns>
		public TList<Rpf> GetByUserid(System.Int64 _userid)
		{
			int count = -1;
			return GetByUserid(_userid, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_RPF_Users key.
		///		FK_RPF_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userid"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Rpf objects.</returns>
		/// <remarks></remarks>
		public TList<Rpf> GetByUserid(TransactionManager transactionManager, System.Int64 _userid)
		{
			int count = -1;
			return GetByUserid(transactionManager, _userid, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_RPF_Users key.
		///		FK_RPF_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userid"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Rpf objects.</returns>
		public TList<Rpf> GetByUserid(TransactionManager transactionManager, System.Int64 _userid, int start, int pageLength)
		{
			int count = -1;
			return GetByUserid(transactionManager, _userid, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_RPF_Users key.
		///		fkRpfUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_userid"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Rpf objects.</returns>
		public TList<Rpf> GetByUserid(System.Int64 _userid, int start, int pageLength)
		{
			int count =  -1;
			return GetByUserid(null, _userid, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_RPF_Users key.
		///		fkRpfUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_userid"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Rpf objects.</returns>
		public TList<Rpf> GetByUserid(System.Int64 _userid, int start, int pageLength,out int count)
		{
			return GetByUserid(null, _userid, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_RPF_Users key.
		///		FK_RPF_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userid"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.Rpf objects.</returns>
		public abstract TList<Rpf> GetByUserid(TransactionManager transactionManager, System.Int64 _userid, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.Rpf Get(TransactionManager transactionManager, LMMR.Entities.RpfKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_RPF index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Rpf"/> class.</returns>
		public LMMR.Entities.Rpf GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_RPF index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Rpf"/> class.</returns>
		public LMMR.Entities.Rpf GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_RPF index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Rpf"/> class.</returns>
		public LMMR.Entities.Rpf GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_RPF index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Rpf"/> class.</returns>
		public LMMR.Entities.Rpf GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_RPF index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Rpf"/> class.</returns>
		public LMMR.Entities.Rpf GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_RPF index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Rpf"/> class.</returns>
		public abstract LMMR.Entities.Rpf GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Rpf&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Rpf&gt;"/></returns>
		public static TList<Rpf> Fill(IDataReader reader, TList<Rpf> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.Rpf c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Rpf")
					.Append("|").Append((System.Int64)reader[((int)RpfColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Rpf>(
					key.ToString(), // EntityTrackingKey
					"Rpf",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.Rpf();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)RpfColumn.Id - 1)];
					c.SafeNameEvent = (reader.IsDBNull(((int)RpfColumn.SafeNameEvent - 1)))?null:(System.String)reader[((int)RpfColumn.SafeNameEvent - 1)];
					c.RespocedueDate = (reader.IsDBNull(((int)RpfColumn.RespocedueDate - 1)))?null:(System.DateTime?)reader[((int)RpfColumn.RespocedueDate - 1)];
					c.Decisiondate = (reader.IsDBNull(((int)RpfColumn.Decisiondate - 1)))?null:(System.DateTime?)reader[((int)RpfColumn.Decisiondate - 1)];
					c.Countryid = (reader.IsDBNull(((int)RpfColumn.Countryid - 1)))?null:(System.Int64?)reader[((int)RpfColumn.Countryid - 1)];
					c.City = (reader.IsDBNull(((int)RpfColumn.City - 1)))?null:(System.String)reader[((int)RpfColumn.City - 1)];
					c.Venuetype = (reader.IsDBNull(((int)RpfColumn.Venuetype - 1)))?null:(System.String)reader[((int)RpfColumn.Venuetype - 1)];
					c.Venuename = (reader.IsDBNull(((int)RpfColumn.Venuename - 1)))?null:(System.String)reader[((int)RpfColumn.Venuename - 1)];
					c.Eventtype = (reader.IsDBNull(((int)RpfColumn.Eventtype - 1)))?null:(System.String)reader[((int)RpfColumn.Eventtype - 1)];
					c.Totalattandes = (reader.IsDBNull(((int)RpfColumn.Totalattandes - 1)))?null:(System.String)reader[((int)RpfColumn.Totalattandes - 1)];
					c.EventStartdate = (reader.IsDBNull(((int)RpfColumn.EventStartdate - 1)))?null:(System.DateTime?)reader[((int)RpfColumn.EventStartdate - 1)];
					c.EventEnddate = (reader.IsDBNull(((int)RpfColumn.EventEnddate - 1)))?null:(System.DateTime?)reader[((int)RpfColumn.EventEnddate - 1)];
					c.Isdateflexible = (System.Boolean)reader[((int)RpfColumn.Isdateflexible - 1)];
					c.Requirebedrooms = (System.Boolean)reader[((int)RpfColumn.Requirebedrooms - 1)];
					c.Bedroomtext = (reader.IsDBNull(((int)RpfColumn.Bedroomtext - 1)))?null:(System.String)reader[((int)RpfColumn.Bedroomtext - 1)];
					c.AdditionalInfomation = (reader.IsDBNull(((int)RpfColumn.AdditionalInfomation - 1)))?null:(System.String)reader[((int)RpfColumn.AdditionalInfomation - 1)];
					c.Userid = (System.Int64)reader[((int)RpfColumn.Userid - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Rpf"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Rpf"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.Rpf entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)RpfColumn.Id - 1)];
			entity.SafeNameEvent = (reader.IsDBNull(((int)RpfColumn.SafeNameEvent - 1)))?null:(System.String)reader[((int)RpfColumn.SafeNameEvent - 1)];
			entity.RespocedueDate = (reader.IsDBNull(((int)RpfColumn.RespocedueDate - 1)))?null:(System.DateTime?)reader[((int)RpfColumn.RespocedueDate - 1)];
			entity.Decisiondate = (reader.IsDBNull(((int)RpfColumn.Decisiondate - 1)))?null:(System.DateTime?)reader[((int)RpfColumn.Decisiondate - 1)];
			entity.Countryid = (reader.IsDBNull(((int)RpfColumn.Countryid - 1)))?null:(System.Int64?)reader[((int)RpfColumn.Countryid - 1)];
			entity.City = (reader.IsDBNull(((int)RpfColumn.City - 1)))?null:(System.String)reader[((int)RpfColumn.City - 1)];
			entity.Venuetype = (reader.IsDBNull(((int)RpfColumn.Venuetype - 1)))?null:(System.String)reader[((int)RpfColumn.Venuetype - 1)];
			entity.Venuename = (reader.IsDBNull(((int)RpfColumn.Venuename - 1)))?null:(System.String)reader[((int)RpfColumn.Venuename - 1)];
			entity.Eventtype = (reader.IsDBNull(((int)RpfColumn.Eventtype - 1)))?null:(System.String)reader[((int)RpfColumn.Eventtype - 1)];
			entity.Totalattandes = (reader.IsDBNull(((int)RpfColumn.Totalattandes - 1)))?null:(System.String)reader[((int)RpfColumn.Totalattandes - 1)];
			entity.EventStartdate = (reader.IsDBNull(((int)RpfColumn.EventStartdate - 1)))?null:(System.DateTime?)reader[((int)RpfColumn.EventStartdate - 1)];
			entity.EventEnddate = (reader.IsDBNull(((int)RpfColumn.EventEnddate - 1)))?null:(System.DateTime?)reader[((int)RpfColumn.EventEnddate - 1)];
			entity.Isdateflexible = (System.Boolean)reader[((int)RpfColumn.Isdateflexible - 1)];
			entity.Requirebedrooms = (System.Boolean)reader[((int)RpfColumn.Requirebedrooms - 1)];
			entity.Bedroomtext = (reader.IsDBNull(((int)RpfColumn.Bedroomtext - 1)))?null:(System.String)reader[((int)RpfColumn.Bedroomtext - 1)];
			entity.AdditionalInfomation = (reader.IsDBNull(((int)RpfColumn.AdditionalInfomation - 1)))?null:(System.String)reader[((int)RpfColumn.AdditionalInfomation - 1)];
			entity.Userid = (System.Int64)reader[((int)RpfColumn.Userid - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Rpf"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Rpf"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.Rpf entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["ID"];
			entity.SafeNameEvent = Convert.IsDBNull(dataRow["Event"]) ? null : (System.String)dataRow["Event"];
			entity.RespocedueDate = Convert.IsDBNull(dataRow["RespocedueDate"]) ? null : (System.DateTime?)dataRow["RespocedueDate"];
			entity.Decisiondate = Convert.IsDBNull(dataRow["Decisiondate"]) ? null : (System.DateTime?)dataRow["Decisiondate"];
			entity.Countryid = Convert.IsDBNull(dataRow["Countryid"]) ? null : (System.Int64?)dataRow["Countryid"];
			entity.City = Convert.IsDBNull(dataRow["City"]) ? null : (System.String)dataRow["City"];
			entity.Venuetype = Convert.IsDBNull(dataRow["Venuetype"]) ? null : (System.String)dataRow["Venuetype"];
			entity.Venuename = Convert.IsDBNull(dataRow["Venuename"]) ? null : (System.String)dataRow["Venuename"];
			entity.Eventtype = Convert.IsDBNull(dataRow["Eventtype"]) ? null : (System.String)dataRow["Eventtype"];
			entity.Totalattandes = Convert.IsDBNull(dataRow["Totalattandes"]) ? null : (System.String)dataRow["Totalattandes"];
			entity.EventStartdate = Convert.IsDBNull(dataRow["EventStartdate"]) ? null : (System.DateTime?)dataRow["EventStartdate"];
			entity.EventEnddate = Convert.IsDBNull(dataRow["EventEnddate"]) ? null : (System.DateTime?)dataRow["EventEnddate"];
			entity.Isdateflexible = (System.Boolean)dataRow["isdateflexible"];
			entity.Requirebedrooms = (System.Boolean)dataRow["requirebedrooms"];
			entity.Bedroomtext = Convert.IsDBNull(dataRow["bedroomtext"]) ? null : (System.String)dataRow["bedroomtext"];
			entity.AdditionalInfomation = Convert.IsDBNull(dataRow["AdditionalInfomation"]) ? null : (System.String)dataRow["AdditionalInfomation"];
			entity.Userid = (System.Int64)dataRow["Userid"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Rpf"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.Rpf Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.Rpf entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CountryidSource	
			if (CanDeepLoad(entity, "Country|CountryidSource", deepLoadType, innerList) 
				&& entity.CountryidSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.Countryid ?? (long)0);
				Country tmpEntity = EntityManager.LocateEntity<Country>(EntityLocator.ConstructKeyFromPkItems(typeof(Country), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CountryidSource = tmpEntity;
				else
					entity.CountryidSource = DataRepository.CountryProvider.GetById(transactionManager, (entity.Countryid ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CountryidSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CountryidSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CountryProvider.DeepLoad(transactionManager, entity.CountryidSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CountryidSource

			#region UseridSource	
			if (CanDeepLoad(entity, "Users|UseridSource", deepLoadType, innerList) 
				&& entity.UseridSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.Userid;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.UseridSource = tmpEntity;
				else
					entity.UseridSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.Userid);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UseridSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.UseridSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.UseridSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion UseridSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.Rpf object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.Rpf instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.Rpf Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.Rpf entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CountryidSource
			if (CanDeepSave(entity, "Country|CountryidSource", deepSaveType, innerList) 
				&& entity.CountryidSource != null)
			{
				DataRepository.CountryProvider.Save(transactionManager, entity.CountryidSource);
				entity.Countryid = entity.CountryidSource.Id;
			}
			#endregion 
			
			#region UseridSource
			if (CanDeepSave(entity, "Users|UseridSource", deepSaveType, innerList) 
				&& entity.UseridSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.UseridSource);
				entity.Userid = entity.UseridSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region RpfChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.Rpf</c>
	///</summary>
	public enum RpfChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Country</c> at CountryidSource
		///</summary>
		[ChildEntityType(typeof(Country))]
		Country,
			
		///<summary>
		/// Composite Property for <c>Users</c> at UseridSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
		}
	
	#endregion RpfChildEntityTypes
	
	#region RpfFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;RpfColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Rpf"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RpfFilterBuilder : SqlFilterBuilder<RpfColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RpfFilterBuilder class.
		/// </summary>
		public RpfFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the RpfFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RpfFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RpfFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RpfFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RpfFilterBuilder
	
	#region RpfParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;RpfColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Rpf"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RpfParameterBuilder : ParameterizedSqlFilterBuilder<RpfColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RpfParameterBuilder class.
		/// </summary>
		public RpfParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the RpfParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RpfParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RpfParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RpfParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RpfParameterBuilder
	
	#region RpfSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;RpfColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Rpf"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class RpfSortBuilder : SqlSortBuilder<RpfColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RpfSqlSortBuilder class.
		/// </summary>
		public RpfSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion RpfSortBuilder
	
} // end namespace
