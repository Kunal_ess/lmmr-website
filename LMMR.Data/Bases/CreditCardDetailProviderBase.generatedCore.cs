﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CreditCardDetailProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CreditCardDetailProviderBaseCore : EntityProviderBase<LMMR.Entities.CreditCardDetail, LMMR.Entities.CreditCardDetailKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.CreditCardDetailKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CreditCardDetail_Country key.
		///		FK_CreditCardDetail_Country Description: 
		/// </summary>
		/// <param name="_countryId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.CreditCardDetail objects.</returns>
		public TList<CreditCardDetail> GetByCountryId(System.Int64 _countryId)
		{
			int count = -1;
			return GetByCountryId(_countryId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CreditCardDetail_Country key.
		///		FK_CreditCardDetail_Country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.CreditCardDetail objects.</returns>
		/// <remarks></remarks>
		public TList<CreditCardDetail> GetByCountryId(TransactionManager transactionManager, System.Int64 _countryId)
		{
			int count = -1;
			return GetByCountryId(transactionManager, _countryId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CreditCardDetail_Country key.
		///		FK_CreditCardDetail_Country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.CreditCardDetail objects.</returns>
		public TList<CreditCardDetail> GetByCountryId(TransactionManager transactionManager, System.Int64 _countryId, int start, int pageLength)
		{
			int count = -1;
			return GetByCountryId(transactionManager, _countryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CreditCardDetail_Country key.
		///		fkCreditCardDetailCountry Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_countryId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.CreditCardDetail objects.</returns>
		public TList<CreditCardDetail> GetByCountryId(System.Int64 _countryId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCountryId(null, _countryId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CreditCardDetail_Country key.
		///		fkCreditCardDetailCountry Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_countryId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.CreditCardDetail objects.</returns>
		public TList<CreditCardDetail> GetByCountryId(System.Int64 _countryId, int start, int pageLength,out int count)
		{
			return GetByCountryId(null, _countryId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CreditCardDetail_Country key.
		///		FK_CreditCardDetail_Country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.CreditCardDetail objects.</returns>
		public abstract TList<CreditCardDetail> GetByCountryId(TransactionManager transactionManager, System.Int64 _countryId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CreditCardDetail_Users key.
		///		FK_CreditCardDetail_Users Description: 
		/// </summary>
		/// <param name="_userId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.CreditCardDetail objects.</returns>
		public TList<CreditCardDetail> GetByUserId(System.Int64 _userId)
		{
			int count = -1;
			return GetByUserId(_userId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CreditCardDetail_Users key.
		///		FK_CreditCardDetail_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.CreditCardDetail objects.</returns>
		/// <remarks></remarks>
		public TList<CreditCardDetail> GetByUserId(TransactionManager transactionManager, System.Int64 _userId)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CreditCardDetail_Users key.
		///		FK_CreditCardDetail_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.CreditCardDetail objects.</returns>
		public TList<CreditCardDetail> GetByUserId(TransactionManager transactionManager, System.Int64 _userId, int start, int pageLength)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CreditCardDetail_Users key.
		///		fkCreditCardDetailUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_userId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.CreditCardDetail objects.</returns>
		public TList<CreditCardDetail> GetByUserId(System.Int64 _userId, int start, int pageLength)
		{
			int count =  -1;
			return GetByUserId(null, _userId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CreditCardDetail_Users key.
		///		fkCreditCardDetailUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_userId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.CreditCardDetail objects.</returns>
		public TList<CreditCardDetail> GetByUserId(System.Int64 _userId, int start, int pageLength,out int count)
		{
			return GetByUserId(null, _userId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CreditCardDetail_Users key.
		///		FK_CreditCardDetail_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.CreditCardDetail objects.</returns>
		public abstract TList<CreditCardDetail> GetByUserId(TransactionManager transactionManager, System.Int64 _userId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.CreditCardDetail Get(TransactionManager transactionManager, LMMR.Entities.CreditCardDetailKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CreditCardDetail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.CreditCardDetail"/> class.</returns>
		public LMMR.Entities.CreditCardDetail GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CreditCardDetail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.CreditCardDetail"/> class.</returns>
		public LMMR.Entities.CreditCardDetail GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CreditCardDetail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.CreditCardDetail"/> class.</returns>
		public LMMR.Entities.CreditCardDetail GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CreditCardDetail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.CreditCardDetail"/> class.</returns>
		public LMMR.Entities.CreditCardDetail GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CreditCardDetail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.CreditCardDetail"/> class.</returns>
		public LMMR.Entities.CreditCardDetail GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CreditCardDetail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.CreditCardDetail"/> class.</returns>
		public abstract LMMR.Entities.CreditCardDetail GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CreditCardDetail&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CreditCardDetail&gt;"/></returns>
		public static TList<CreditCardDetail> Fill(IDataReader reader, TList<CreditCardDetail> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.CreditCardDetail c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CreditCardDetail")
					.Append("|").Append((System.Int64)reader[((int)CreditCardDetailColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CreditCardDetail>(
					key.ToString(), // EntityTrackingKey
					"CreditCardDetail",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.CreditCardDetail();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)CreditCardDetailColumn.Id - 1)];
					c.UserId = (System.Int64)reader[((int)CreditCardDetailColumn.UserId - 1)];
					c.Name = (reader.IsDBNull(((int)CreditCardDetailColumn.Name - 1)))?null:(System.String)reader[((int)CreditCardDetailColumn.Name - 1)];
					c.Email = (reader.IsDBNull(((int)CreditCardDetailColumn.Email - 1)))?null:(System.String)reader[((int)CreditCardDetailColumn.Email - 1)];
					c.Address = (reader.IsDBNull(((int)CreditCardDetailColumn.Address - 1)))?null:(System.String)reader[((int)CreditCardDetailColumn.Address - 1)];
					c.PostalCode = (reader.IsDBNull(((int)CreditCardDetailColumn.PostalCode - 1)))?null:(System.String)reader[((int)CreditCardDetailColumn.PostalCode - 1)];
					c.CountryId = (System.Int64)reader[((int)CreditCardDetailColumn.CountryId - 1)];
					c.ContactNumber = (reader.IsDBNull(((int)CreditCardDetailColumn.ContactNumber - 1)))?null:(System.String)reader[((int)CreditCardDetailColumn.ContactNumber - 1)];
					c.CardType = (reader.IsDBNull(((int)CreditCardDetailColumn.CardType - 1)))?null:(System.String)reader[((int)CreditCardDetailColumn.CardType - 1)];
					c.CardName = (reader.IsDBNull(((int)CreditCardDetailColumn.CardName - 1)))?null:(System.String)reader[((int)CreditCardDetailColumn.CardName - 1)];
					c.CardNumber = (reader.IsDBNull(((int)CreditCardDetailColumn.CardNumber - 1)))?null:(System.String)reader[((int)CreditCardDetailColumn.CardNumber - 1)];
					c.CvcNumber = (reader.IsDBNull(((int)CreditCardDetailColumn.CvcNumber - 1)))?null:(System.String)reader[((int)CreditCardDetailColumn.CvcNumber - 1)];
					c.ExpiryMonthYear = (reader.IsDBNull(((int)CreditCardDetailColumn.ExpiryMonthYear - 1)))?null:(System.String)reader[((int)CreditCardDetailColumn.ExpiryMonthYear - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.CreditCardDetail"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.CreditCardDetail"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.CreditCardDetail entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)CreditCardDetailColumn.Id - 1)];
			entity.UserId = (System.Int64)reader[((int)CreditCardDetailColumn.UserId - 1)];
			entity.Name = (reader.IsDBNull(((int)CreditCardDetailColumn.Name - 1)))?null:(System.String)reader[((int)CreditCardDetailColumn.Name - 1)];
			entity.Email = (reader.IsDBNull(((int)CreditCardDetailColumn.Email - 1)))?null:(System.String)reader[((int)CreditCardDetailColumn.Email - 1)];
			entity.Address = (reader.IsDBNull(((int)CreditCardDetailColumn.Address - 1)))?null:(System.String)reader[((int)CreditCardDetailColumn.Address - 1)];
			entity.PostalCode = (reader.IsDBNull(((int)CreditCardDetailColumn.PostalCode - 1)))?null:(System.String)reader[((int)CreditCardDetailColumn.PostalCode - 1)];
			entity.CountryId = (System.Int64)reader[((int)CreditCardDetailColumn.CountryId - 1)];
			entity.ContactNumber = (reader.IsDBNull(((int)CreditCardDetailColumn.ContactNumber - 1)))?null:(System.String)reader[((int)CreditCardDetailColumn.ContactNumber - 1)];
			entity.CardType = (reader.IsDBNull(((int)CreditCardDetailColumn.CardType - 1)))?null:(System.String)reader[((int)CreditCardDetailColumn.CardType - 1)];
			entity.CardName = (reader.IsDBNull(((int)CreditCardDetailColumn.CardName - 1)))?null:(System.String)reader[((int)CreditCardDetailColumn.CardName - 1)];
			entity.CardNumber = (reader.IsDBNull(((int)CreditCardDetailColumn.CardNumber - 1)))?null:(System.String)reader[((int)CreditCardDetailColumn.CardNumber - 1)];
			entity.CvcNumber = (reader.IsDBNull(((int)CreditCardDetailColumn.CvcNumber - 1)))?null:(System.String)reader[((int)CreditCardDetailColumn.CvcNumber - 1)];
			entity.ExpiryMonthYear = (reader.IsDBNull(((int)CreditCardDetailColumn.ExpiryMonthYear - 1)))?null:(System.String)reader[((int)CreditCardDetailColumn.ExpiryMonthYear - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.CreditCardDetail"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.CreditCardDetail"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.CreditCardDetail entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["ID"];
			entity.UserId = (System.Int64)dataRow["UserID"];
			entity.Name = Convert.IsDBNull(dataRow["Name"]) ? null : (System.String)dataRow["Name"];
			entity.Email = Convert.IsDBNull(dataRow["Email"]) ? null : (System.String)dataRow["Email"];
			entity.Address = Convert.IsDBNull(dataRow["Address"]) ? null : (System.String)dataRow["Address"];
			entity.PostalCode = Convert.IsDBNull(dataRow["PostalCode"]) ? null : (System.String)dataRow["PostalCode"];
			entity.CountryId = (System.Int64)dataRow["CountryID"];
			entity.ContactNumber = Convert.IsDBNull(dataRow["ContactNumber"]) ? null : (System.String)dataRow["ContactNumber"];
			entity.CardType = Convert.IsDBNull(dataRow["CardType"]) ? null : (System.String)dataRow["CardType"];
			entity.CardName = Convert.IsDBNull(dataRow["CardName"]) ? null : (System.String)dataRow["CardName"];
			entity.CardNumber = Convert.IsDBNull(dataRow["CardNumber"]) ? null : (System.String)dataRow["CardNumber"];
			entity.CvcNumber = Convert.IsDBNull(dataRow["CvcNumber"]) ? null : (System.String)dataRow["CvcNumber"];
			entity.ExpiryMonthYear = Convert.IsDBNull(dataRow["ExpiryMonthYear"]) ? null : (System.String)dataRow["ExpiryMonthYear"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.CreditCardDetail"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.CreditCardDetail Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.CreditCardDetail entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CountryIdSource	
			if (CanDeepLoad(entity, "Country|CountryIdSource", deepLoadType, innerList) 
				&& entity.CountryIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CountryId;
				Country tmpEntity = EntityManager.LocateEntity<Country>(EntityLocator.ConstructKeyFromPkItems(typeof(Country), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CountryIdSource = tmpEntity;
				else
					entity.CountryIdSource = DataRepository.CountryProvider.GetById(transactionManager, entity.CountryId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CountryIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CountryIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CountryProvider.DeepLoad(transactionManager, entity.CountryIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CountryIdSource

			#region UserIdSource	
			if (CanDeepLoad(entity, "Users|UserIdSource", deepLoadType, innerList) 
				&& entity.UserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.UserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.UserIdSource = tmpEntity;
				else
					entity.UserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.UserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.UserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.UserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion UserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.CreditCardDetail object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.CreditCardDetail instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.CreditCardDetail Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.CreditCardDetail entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CountryIdSource
			if (CanDeepSave(entity, "Country|CountryIdSource", deepSaveType, innerList) 
				&& entity.CountryIdSource != null)
			{
				DataRepository.CountryProvider.Save(transactionManager, entity.CountryIdSource);
				entity.CountryId = entity.CountryIdSource.Id;
			}
			#endregion 
			
			#region UserIdSource
			if (CanDeepSave(entity, "Users|UserIdSource", deepSaveType, innerList) 
				&& entity.UserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.UserIdSource);
				entity.UserId = entity.UserIdSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CreditCardDetailChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.CreditCardDetail</c>
	///</summary>
	public enum CreditCardDetailChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Country</c> at CountryIdSource
		///</summary>
		[ChildEntityType(typeof(Country))]
		Country,
			
		///<summary>
		/// Composite Property for <c>Users</c> at UserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
		}
	
	#endregion CreditCardDetailChildEntityTypes
	
	#region CreditCardDetailFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CreditCardDetailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CreditCardDetail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CreditCardDetailFilterBuilder : SqlFilterBuilder<CreditCardDetailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CreditCardDetailFilterBuilder class.
		/// </summary>
		public CreditCardDetailFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CreditCardDetailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CreditCardDetailFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CreditCardDetailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CreditCardDetailFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CreditCardDetailFilterBuilder
	
	#region CreditCardDetailParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CreditCardDetailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CreditCardDetail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CreditCardDetailParameterBuilder : ParameterizedSqlFilterBuilder<CreditCardDetailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CreditCardDetailParameterBuilder class.
		/// </summary>
		public CreditCardDetailParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CreditCardDetailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CreditCardDetailParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CreditCardDetailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CreditCardDetailParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CreditCardDetailParameterBuilder
	
	#region CreditCardDetailSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CreditCardDetailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CreditCardDetail"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CreditCardDetailSortBuilder : SqlSortBuilder<CreditCardDetailColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CreditCardDetailSqlSortBuilder class.
		/// </summary>
		public CreditCardDetailSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CreditCardDetailSortBuilder
	
} // end namespace
