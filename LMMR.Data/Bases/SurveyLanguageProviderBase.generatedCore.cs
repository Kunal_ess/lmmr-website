﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SurveyLanguageProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SurveyLanguageProviderBaseCore : EntityProviderBase<LMMR.Entities.SurveyLanguage, LMMR.Entities.SurveyLanguageKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.SurveyLanguageKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyLanguage_Language key.
		///		FK_SurveyLanguage_Language Description: 
		/// </summary>
		/// <param name="_languageId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyLanguage objects.</returns>
		public TList<SurveyLanguage> GetByLanguageId(System.Int64 _languageId)
		{
			int count = -1;
			return GetByLanguageId(_languageId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyLanguage_Language key.
		///		FK_SurveyLanguage_Language Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_languageId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyLanguage objects.</returns>
		/// <remarks></remarks>
		public TList<SurveyLanguage> GetByLanguageId(TransactionManager transactionManager, System.Int64 _languageId)
		{
			int count = -1;
			return GetByLanguageId(transactionManager, _languageId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyLanguage_Language key.
		///		FK_SurveyLanguage_Language Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_languageId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyLanguage objects.</returns>
		public TList<SurveyLanguage> GetByLanguageId(TransactionManager transactionManager, System.Int64 _languageId, int start, int pageLength)
		{
			int count = -1;
			return GetByLanguageId(transactionManager, _languageId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyLanguage_Language key.
		///		fkSurveyLanguageLanguage Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_languageId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyLanguage objects.</returns>
		public TList<SurveyLanguage> GetByLanguageId(System.Int64 _languageId, int start, int pageLength)
		{
			int count =  -1;
			return GetByLanguageId(null, _languageId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyLanguage_Language key.
		///		fkSurveyLanguageLanguage Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_languageId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyLanguage objects.</returns>
		public TList<SurveyLanguage> GetByLanguageId(System.Int64 _languageId, int start, int pageLength,out int count)
		{
			return GetByLanguageId(null, _languageId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyLanguage_Language key.
		///		FK_SurveyLanguage_Language Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_languageId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyLanguage objects.</returns>
		public abstract TList<SurveyLanguage> GetByLanguageId(TransactionManager transactionManager, System.Int64 _languageId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyLanguage_Survey key.
		///		FK_SurveyLanguage_Survey Description: 
		/// </summary>
		/// <param name="_surveyId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyLanguage objects.</returns>
		public TList<SurveyLanguage> GetBySurveyId(System.Int64 _surveyId)
		{
			int count = -1;
			return GetBySurveyId(_surveyId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyLanguage_Survey key.
		///		FK_SurveyLanguage_Survey Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_surveyId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyLanguage objects.</returns>
		/// <remarks></remarks>
		public TList<SurveyLanguage> GetBySurveyId(TransactionManager transactionManager, System.Int64 _surveyId)
		{
			int count = -1;
			return GetBySurveyId(transactionManager, _surveyId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyLanguage_Survey key.
		///		FK_SurveyLanguage_Survey Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_surveyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyLanguage objects.</returns>
		public TList<SurveyLanguage> GetBySurveyId(TransactionManager transactionManager, System.Int64 _surveyId, int start, int pageLength)
		{
			int count = -1;
			return GetBySurveyId(transactionManager, _surveyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyLanguage_Survey key.
		///		fkSurveyLanguageSurvey Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_surveyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyLanguage objects.</returns>
		public TList<SurveyLanguage> GetBySurveyId(System.Int64 _surveyId, int start, int pageLength)
		{
			int count =  -1;
			return GetBySurveyId(null, _surveyId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyLanguage_Survey key.
		///		fkSurveyLanguageSurvey Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_surveyId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyLanguage objects.</returns>
		public TList<SurveyLanguage> GetBySurveyId(System.Int64 _surveyId, int start, int pageLength,out int count)
		{
			return GetBySurveyId(null, _surveyId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyLanguage_Survey key.
		///		FK_SurveyLanguage_Survey Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_surveyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyLanguage objects.</returns>
		public abstract TList<SurveyLanguage> GetBySurveyId(TransactionManager transactionManager, System.Int64 _surveyId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.SurveyLanguage Get(TransactionManager transactionManager, LMMR.Entities.SurveyLanguageKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SurveyLanguage index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyLanguage"/> class.</returns>
		public LMMR.Entities.SurveyLanguage GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SurveyLanguage index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyLanguage"/> class.</returns>
		public LMMR.Entities.SurveyLanguage GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SurveyLanguage index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyLanguage"/> class.</returns>
		public LMMR.Entities.SurveyLanguage GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SurveyLanguage index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyLanguage"/> class.</returns>
		public LMMR.Entities.SurveyLanguage GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SurveyLanguage index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyLanguage"/> class.</returns>
		public LMMR.Entities.SurveyLanguage GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SurveyLanguage index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyLanguage"/> class.</returns>
		public abstract LMMR.Entities.SurveyLanguage GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SurveyLanguage&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SurveyLanguage&gt;"/></returns>
		public static TList<SurveyLanguage> Fill(IDataReader reader, TList<SurveyLanguage> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.SurveyLanguage c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SurveyLanguage")
					.Append("|").Append((System.Int64)reader[((int)SurveyLanguageColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SurveyLanguage>(
					key.ToString(), // EntityTrackingKey
					"SurveyLanguage",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.SurveyLanguage();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)SurveyLanguageColumn.Id - 1)];
					c.SurveyId = (System.Int64)reader[((int)SurveyLanguageColumn.SurveyId - 1)];
					c.LanguageId = (System.Int64)reader[((int)SurveyLanguageColumn.LanguageId - 1)];
					c.Question = (reader.IsDBNull(((int)SurveyLanguageColumn.Question - 1)))?null:(System.String)reader[((int)SurveyLanguageColumn.Question - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.SurveyLanguage"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SurveyLanguage"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.SurveyLanguage entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)SurveyLanguageColumn.Id - 1)];
			entity.SurveyId = (System.Int64)reader[((int)SurveyLanguageColumn.SurveyId - 1)];
			entity.LanguageId = (System.Int64)reader[((int)SurveyLanguageColumn.LanguageId - 1)];
			entity.Question = (reader.IsDBNull(((int)SurveyLanguageColumn.Question - 1)))?null:(System.String)reader[((int)SurveyLanguageColumn.Question - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.SurveyLanguage"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SurveyLanguage"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.SurveyLanguage entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.SurveyId = (System.Int64)dataRow["SurveyId"];
			entity.LanguageId = (System.Int64)dataRow["LanguageId"];
			entity.Question = Convert.IsDBNull(dataRow["Question"]) ? null : (System.String)dataRow["Question"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SurveyLanguage"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.SurveyLanguage Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.SurveyLanguage entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region LanguageIdSource	
			if (CanDeepLoad(entity, "Language|LanguageIdSource", deepLoadType, innerList) 
				&& entity.LanguageIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.LanguageId;
				Language tmpEntity = EntityManager.LocateEntity<Language>(EntityLocator.ConstructKeyFromPkItems(typeof(Language), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.LanguageIdSource = tmpEntity;
				else
					entity.LanguageIdSource = DataRepository.LanguageProvider.GetById(transactionManager, entity.LanguageId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'LanguageIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.LanguageIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.LanguageProvider.DeepLoad(transactionManager, entity.LanguageIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion LanguageIdSource

			#region SurveyIdSource	
			if (CanDeepLoad(entity, "Survey|SurveyIdSource", deepLoadType, innerList) 
				&& entity.SurveyIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SurveyId;
				Survey tmpEntity = EntityManager.LocateEntity<Survey>(EntityLocator.ConstructKeyFromPkItems(typeof(Survey), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SurveyIdSource = tmpEntity;
				else
					entity.SurveyIdSource = DataRepository.SurveyProvider.GetById(transactionManager, entity.SurveyId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SurveyIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SurveyIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SurveyProvider.DeepLoad(transactionManager, entity.SurveyIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SurveyIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.SurveyLanguage object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.SurveyLanguage instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.SurveyLanguage Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.SurveyLanguage entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region LanguageIdSource
			if (CanDeepSave(entity, "Language|LanguageIdSource", deepSaveType, innerList) 
				&& entity.LanguageIdSource != null)
			{
				DataRepository.LanguageProvider.Save(transactionManager, entity.LanguageIdSource);
				entity.LanguageId = entity.LanguageIdSource.Id;
			}
			#endregion 
			
			#region SurveyIdSource
			if (CanDeepSave(entity, "Survey|SurveyIdSource", deepSaveType, innerList) 
				&& entity.SurveyIdSource != null)
			{
				DataRepository.SurveyProvider.Save(transactionManager, entity.SurveyIdSource);
				entity.SurveyId = entity.SurveyIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SurveyLanguageChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.SurveyLanguage</c>
	///</summary>
	public enum SurveyLanguageChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Language</c> at LanguageIdSource
		///</summary>
		[ChildEntityType(typeof(Language))]
		Language,
			
		///<summary>
		/// Composite Property for <c>Survey</c> at SurveyIdSource
		///</summary>
		[ChildEntityType(typeof(Survey))]
		Survey,
		}
	
	#endregion SurveyLanguageChildEntityTypes
	
	#region SurveyLanguageFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SurveyLanguageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SurveyLanguage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SurveyLanguageFilterBuilder : SqlFilterBuilder<SurveyLanguageColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SurveyLanguageFilterBuilder class.
		/// </summary>
		public SurveyLanguageFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SurveyLanguageFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SurveyLanguageFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SurveyLanguageFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SurveyLanguageFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SurveyLanguageFilterBuilder
	
	#region SurveyLanguageParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SurveyLanguageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SurveyLanguage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SurveyLanguageParameterBuilder : ParameterizedSqlFilterBuilder<SurveyLanguageColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SurveyLanguageParameterBuilder class.
		/// </summary>
		public SurveyLanguageParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SurveyLanguageParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SurveyLanguageParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SurveyLanguageParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SurveyLanguageParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SurveyLanguageParameterBuilder
	
	#region SurveyLanguageSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SurveyLanguageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SurveyLanguage"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SurveyLanguageSortBuilder : SqlSortBuilder<SurveyLanguageColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SurveyLanguageSqlSortBuilder class.
		/// </summary>
		public SurveyLanguageSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SurveyLanguageSortBuilder
	
} // end namespace
