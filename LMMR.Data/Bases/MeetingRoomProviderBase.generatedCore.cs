﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="MeetingRoomProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class MeetingRoomProviderBaseCore : EntityProviderBase<LMMR.Entities.MeetingRoom, LMMR.Entities.MeetingRoomKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.MeetingRoomKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoom_Hotel key.
		///		FK_MeetingRoom_Hotel Description: 
		/// </summary>
		/// <param name="_hotelId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoom objects.</returns>
		public TList<MeetingRoom> GetByHotelId(System.Int64 _hotelId)
		{
			int count = -1;
			return GetByHotelId(_hotelId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoom_Hotel key.
		///		FK_MeetingRoom_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoom objects.</returns>
		/// <remarks></remarks>
		public TList<MeetingRoom> GetByHotelId(TransactionManager transactionManager, System.Int64 _hotelId)
		{
			int count = -1;
			return GetByHotelId(transactionManager, _hotelId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoom_Hotel key.
		///		FK_MeetingRoom_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoom objects.</returns>
		public TList<MeetingRoom> GetByHotelId(TransactionManager transactionManager, System.Int64 _hotelId, int start, int pageLength)
		{
			int count = -1;
			return GetByHotelId(transactionManager, _hotelId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoom_Hotel key.
		///		fkMeetingRoomHotel Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_hotelId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoom objects.</returns>
		public TList<MeetingRoom> GetByHotelId(System.Int64 _hotelId, int start, int pageLength)
		{
			int count =  -1;
			return GetByHotelId(null, _hotelId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoom_Hotel key.
		///		fkMeetingRoomHotel Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_hotelId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoom objects.</returns>
		public TList<MeetingRoom> GetByHotelId(System.Int64 _hotelId, int start, int pageLength,out int count)
		{
			return GetByHotelId(null, _hotelId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoom_Hotel key.
		///		FK_MeetingRoom_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoom objects.</returns>
		public abstract TList<MeetingRoom> GetByHotelId(TransactionManager transactionManager, System.Int64 _hotelId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoom_Users key.
		///		FK_MeetingRoom_Users Description: 
		/// </summary>
		/// <param name="_updatedBy"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoom objects.</returns>
		public TList<MeetingRoom> GetByUpdatedBy(System.Int64? _updatedBy)
		{
			int count = -1;
			return GetByUpdatedBy(_updatedBy, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoom_Users key.
		///		FK_MeetingRoom_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_updatedBy"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoom objects.</returns>
		/// <remarks></remarks>
		public TList<MeetingRoom> GetByUpdatedBy(TransactionManager transactionManager, System.Int64? _updatedBy)
		{
			int count = -1;
			return GetByUpdatedBy(transactionManager, _updatedBy, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoom_Users key.
		///		FK_MeetingRoom_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_updatedBy"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoom objects.</returns>
		public TList<MeetingRoom> GetByUpdatedBy(TransactionManager transactionManager, System.Int64? _updatedBy, int start, int pageLength)
		{
			int count = -1;
			return GetByUpdatedBy(transactionManager, _updatedBy, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoom_Users key.
		///		fkMeetingRoomUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_updatedBy"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoom objects.</returns>
		public TList<MeetingRoom> GetByUpdatedBy(System.Int64? _updatedBy, int start, int pageLength)
		{
			int count =  -1;
			return GetByUpdatedBy(null, _updatedBy, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoom_Users key.
		///		fkMeetingRoomUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_updatedBy"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoom objects.</returns>
		public TList<MeetingRoom> GetByUpdatedBy(System.Int64? _updatedBy, int start, int pageLength,out int count)
		{
			return GetByUpdatedBy(null, _updatedBy, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_MeetingRoom_Users key.
		///		FK_MeetingRoom_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_updatedBy"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.MeetingRoom objects.</returns>
		public abstract TList<MeetingRoom> GetByUpdatedBy(TransactionManager transactionManager, System.Int64? _updatedBy, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.MeetingRoom Get(TransactionManager transactionManager, LMMR.Entities.MeetingRoomKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_MeetingRoom index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoom"/> class.</returns>
		public LMMR.Entities.MeetingRoom GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoom index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoom"/> class.</returns>
		public LMMR.Entities.MeetingRoom GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoom index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoom"/> class.</returns>
		public LMMR.Entities.MeetingRoom GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoom index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoom"/> class.</returns>
		public LMMR.Entities.MeetingRoom GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoom index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoom"/> class.</returns>
		public LMMR.Entities.MeetingRoom GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_MeetingRoom index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.MeetingRoom"/> class.</returns>
		public abstract LMMR.Entities.MeetingRoom GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;MeetingRoom&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;MeetingRoom&gt;"/></returns>
		public static TList<MeetingRoom> Fill(IDataReader reader, TList<MeetingRoom> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.MeetingRoom c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("MeetingRoom")
					.Append("|").Append((System.Int64)reader[((int)MeetingRoomColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<MeetingRoom>(
					key.ToString(), // EntityTrackingKey
					"MeetingRoom",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.MeetingRoom();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)MeetingRoomColumn.Id - 1)];
					c.HotelId = (System.Int64)reader[((int)MeetingRoomColumn.HotelId - 1)];
					c.Name = (reader.IsDBNull(((int)MeetingRoomColumn.Name - 1)))?null:(System.String)reader[((int)MeetingRoomColumn.Name - 1)];
					c.Picture = (reader.IsDBNull(((int)MeetingRoomColumn.Picture - 1)))?null:(System.String)reader[((int)MeetingRoomColumn.Picture - 1)];
					c.DayLight = (System.Int32)reader[((int)MeetingRoomColumn.DayLight - 1)];
					c.Surface = (reader.IsDBNull(((int)MeetingRoomColumn.Surface - 1)))?null:(System.Int64?)reader[((int)MeetingRoomColumn.Surface - 1)];
					c.Height = (reader.IsDBNull(((int)MeetingRoomColumn.Height - 1)))?null:(System.Decimal?)reader[((int)MeetingRoomColumn.Height - 1)];
					c.IsActive = (System.Boolean)reader[((int)MeetingRoomColumn.IsActive - 1)];
					c.MrPlan = (reader.IsDBNull(((int)MeetingRoomColumn.MrPlan - 1)))?null:(System.String)reader[((int)MeetingRoomColumn.MrPlan - 1)];
					c.OrderNumber = (reader.IsDBNull(((int)MeetingRoomColumn.OrderNumber - 1)))?null:(System.Int32?)reader[((int)MeetingRoomColumn.OrderNumber - 1)];
					c.HalfdayPrice = (reader.IsDBNull(((int)MeetingRoomColumn.HalfdayPrice - 1)))?null:(System.Decimal?)reader[((int)MeetingRoomColumn.HalfdayPrice - 1)];
					c.FulldayPrice = (reader.IsDBNull(((int)MeetingRoomColumn.FulldayPrice - 1)))?null:(System.Decimal?)reader[((int)MeetingRoomColumn.FulldayPrice - 1)];
					c.IsOnline = (System.Boolean)reader[((int)MeetingRoomColumn.IsOnline - 1)];
					c.IsDeleted = (System.Boolean)reader[((int)MeetingRoomColumn.IsDeleted - 1)];
					c.UpdatedBy = (reader.IsDBNull(((int)MeetingRoomColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)MeetingRoomColumn.UpdatedBy - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.MeetingRoom"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.MeetingRoom"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.MeetingRoom entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)MeetingRoomColumn.Id - 1)];
			entity.HotelId = (System.Int64)reader[((int)MeetingRoomColumn.HotelId - 1)];
			entity.Name = (reader.IsDBNull(((int)MeetingRoomColumn.Name - 1)))?null:(System.String)reader[((int)MeetingRoomColumn.Name - 1)];
			entity.Picture = (reader.IsDBNull(((int)MeetingRoomColumn.Picture - 1)))?null:(System.String)reader[((int)MeetingRoomColumn.Picture - 1)];
			entity.DayLight = (System.Int32)reader[((int)MeetingRoomColumn.DayLight - 1)];
			entity.Surface = (reader.IsDBNull(((int)MeetingRoomColumn.Surface - 1)))?null:(System.Int64?)reader[((int)MeetingRoomColumn.Surface - 1)];
			entity.Height = (reader.IsDBNull(((int)MeetingRoomColumn.Height - 1)))?null:(System.Decimal?)reader[((int)MeetingRoomColumn.Height - 1)];
			entity.IsActive = (System.Boolean)reader[((int)MeetingRoomColumn.IsActive - 1)];
			entity.MrPlan = (reader.IsDBNull(((int)MeetingRoomColumn.MrPlan - 1)))?null:(System.String)reader[((int)MeetingRoomColumn.MrPlan - 1)];
			entity.OrderNumber = (reader.IsDBNull(((int)MeetingRoomColumn.OrderNumber - 1)))?null:(System.Int32?)reader[((int)MeetingRoomColumn.OrderNumber - 1)];
			entity.HalfdayPrice = (reader.IsDBNull(((int)MeetingRoomColumn.HalfdayPrice - 1)))?null:(System.Decimal?)reader[((int)MeetingRoomColumn.HalfdayPrice - 1)];
			entity.FulldayPrice = (reader.IsDBNull(((int)MeetingRoomColumn.FulldayPrice - 1)))?null:(System.Decimal?)reader[((int)MeetingRoomColumn.FulldayPrice - 1)];
			entity.IsOnline = (System.Boolean)reader[((int)MeetingRoomColumn.IsOnline - 1)];
			entity.IsDeleted = (System.Boolean)reader[((int)MeetingRoomColumn.IsDeleted - 1)];
			entity.UpdatedBy = (reader.IsDBNull(((int)MeetingRoomColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)MeetingRoomColumn.UpdatedBy - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.MeetingRoom"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.MeetingRoom"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.MeetingRoom entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.HotelId = (System.Int64)dataRow["Hotel_Id"];
			entity.Name = Convert.IsDBNull(dataRow["Name"]) ? null : (System.String)dataRow["Name"];
			entity.Picture = Convert.IsDBNull(dataRow["Picture"]) ? null : (System.String)dataRow["Picture"];
			entity.DayLight = (System.Int32)dataRow["DayLight"];
			entity.Surface = Convert.IsDBNull(dataRow["Surface"]) ? null : (System.Int64?)dataRow["Surface"];
			entity.Height = Convert.IsDBNull(dataRow["Height"]) ? null : (System.Decimal?)dataRow["Height"];
			entity.IsActive = (System.Boolean)dataRow["IsActive"];
			entity.MrPlan = Convert.IsDBNull(dataRow["MR_Plan"]) ? null : (System.String)dataRow["MR_Plan"];
			entity.OrderNumber = Convert.IsDBNull(dataRow["OrderNumber"]) ? null : (System.Int32?)dataRow["OrderNumber"];
			entity.HalfdayPrice = Convert.IsDBNull(dataRow["HalfdayPrice"]) ? null : (System.Decimal?)dataRow["HalfdayPrice"];
			entity.FulldayPrice = Convert.IsDBNull(dataRow["FulldayPrice"]) ? null : (System.Decimal?)dataRow["FulldayPrice"];
			entity.IsOnline = (System.Boolean)dataRow["IsOnline"];
			entity.IsDeleted = (System.Boolean)dataRow["IsDeleted"];
			entity.UpdatedBy = Convert.IsDBNull(dataRow["UpdatedBy"]) ? null : (System.Int64?)dataRow["UpdatedBy"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.MeetingRoom"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.MeetingRoom Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.MeetingRoom entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region HotelIdSource	
			if (CanDeepLoad(entity, "Hotel|HotelIdSource", deepLoadType, innerList) 
				&& entity.HotelIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.HotelId;
				Hotel tmpEntity = EntityManager.LocateEntity<Hotel>(EntityLocator.ConstructKeyFromPkItems(typeof(Hotel), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.HotelIdSource = tmpEntity;
				else
					entity.HotelIdSource = DataRepository.HotelProvider.GetById(transactionManager, entity.HotelId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HotelIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.HotelIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.HotelProvider.DeepLoad(transactionManager, entity.HotelIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion HotelIdSource

			#region UpdatedBySource	
			if (CanDeepLoad(entity, "Users|UpdatedBySource", deepLoadType, innerList) 
				&& entity.UpdatedBySource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.UpdatedBy ?? (long)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.UpdatedBySource = tmpEntity;
				else
					entity.UpdatedBySource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.UpdatedBy ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UpdatedBySource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.UpdatedBySource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.UpdatedBySource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion UpdatedBySource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region MeetingRoomDescCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<MeetingRoomDesc>|MeetingRoomDescCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MeetingRoomDescCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.MeetingRoomDescCollection = DataRepository.MeetingRoomDescProvider.GetByMeetingRoomId(transactionManager, entity.Id);

				if (deep && entity.MeetingRoomDescCollection.Count > 0)
				{
					deepHandles.Add("MeetingRoomDescCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<MeetingRoomDesc>) DataRepository.MeetingRoomDescProvider.DeepLoad,
						new object[] { transactionManager, entity.MeetingRoomDescCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region MeetingRoomConfigCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<MeetingRoomConfig>|MeetingRoomConfigCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MeetingRoomConfigCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.MeetingRoomConfigCollection = DataRepository.MeetingRoomConfigProvider.GetByMeetingRoomId(transactionManager, entity.Id);

				if (deep && entity.MeetingRoomConfigCollection.Count > 0)
				{
					deepHandles.Add("MeetingRoomConfigCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<MeetingRoomConfig>) DataRepository.MeetingRoomConfigProvider.DeepLoad,
						new object[] { transactionManager, entity.MeetingRoomConfigCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region SpandPmeetingRoomCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SpandPmeetingRoom>|SpandPmeetingRoomCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SpandPmeetingRoomCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SpandPmeetingRoomCollection = DataRepository.SpandPmeetingRoomProvider.GetByMeetingRoomId(transactionManager, entity.Id);

				if (deep && entity.SpandPmeetingRoomCollection.Count > 0)
				{
					deepHandles.Add("SpandPmeetingRoomCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SpandPmeetingRoom>) DataRepository.SpandPmeetingRoomProvider.DeepLoad,
						new object[] { transactionManager, entity.SpandPmeetingRoomCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region BookingCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Booking>|BookingCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BookingCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BookingCollection = DataRepository.BookingProvider.GetByMainMeetingRoomId(transactionManager, entity.Id);

				if (deep && entity.BookingCollection.Count > 0)
				{
					deepHandles.Add("BookingCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Booking>) DataRepository.BookingProvider.DeepLoad,
						new object[] { transactionManager, entity.BookingCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region BookedMeetingRoomCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<BookedMeetingRoom>|BookedMeetingRoomCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BookedMeetingRoomCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BookedMeetingRoomCollection = DataRepository.BookedMeetingRoomProvider.GetByMeetingRoomId(transactionManager, entity.Id);

				if (deep && entity.BookedMeetingRoomCollection.Count > 0)
				{
					deepHandles.Add("BookedMeetingRoomCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<BookedMeetingRoom>) DataRepository.BookedMeetingRoomProvider.DeepLoad,
						new object[] { transactionManager, entity.BookedMeetingRoomCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region MeetingRoomPictureVideoCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<MeetingRoomPictureVideo>|MeetingRoomPictureVideoCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MeetingRoomPictureVideoCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.MeetingRoomPictureVideoCollection = DataRepository.MeetingRoomPictureVideoProvider.GetByMeetingRoomId(transactionManager, entity.Id);

				if (deep && entity.MeetingRoomPictureVideoCollection.Count > 0)
				{
					deepHandles.Add("MeetingRoomPictureVideoCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<MeetingRoomPictureVideo>) DataRepository.MeetingRoomPictureVideoProvider.DeepLoad,
						new object[] { transactionManager, entity.MeetingRoomPictureVideoCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.MeetingRoom object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.MeetingRoom instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.MeetingRoom Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.MeetingRoom entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region HotelIdSource
			if (CanDeepSave(entity, "Hotel|HotelIdSource", deepSaveType, innerList) 
				&& entity.HotelIdSource != null)
			{
				DataRepository.HotelProvider.Save(transactionManager, entity.HotelIdSource);
				entity.HotelId = entity.HotelIdSource.Id;
			}
			#endregion 
			
			#region UpdatedBySource
			if (CanDeepSave(entity, "Users|UpdatedBySource", deepSaveType, innerList) 
				&& entity.UpdatedBySource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.UpdatedBySource);
				entity.UpdatedBy = entity.UpdatedBySource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<MeetingRoomDesc>
				if (CanDeepSave(entity.MeetingRoomDescCollection, "List<MeetingRoomDesc>|MeetingRoomDescCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(MeetingRoomDesc child in entity.MeetingRoomDescCollection)
					{
						if(child.MeetingRoomIdSource != null)
						{
							child.MeetingRoomId = child.MeetingRoomIdSource.Id;
						}
						else
						{
							child.MeetingRoomId = entity.Id;
						}

					}

					if (entity.MeetingRoomDescCollection.Count > 0 || entity.MeetingRoomDescCollection.DeletedItems.Count > 0)
					{
						//DataRepository.MeetingRoomDescProvider.Save(transactionManager, entity.MeetingRoomDescCollection);
						
						deepHandles.Add("MeetingRoomDescCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< MeetingRoomDesc >) DataRepository.MeetingRoomDescProvider.DeepSave,
							new object[] { transactionManager, entity.MeetingRoomDescCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<MeetingRoomConfig>
				if (CanDeepSave(entity.MeetingRoomConfigCollection, "List<MeetingRoomConfig>|MeetingRoomConfigCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(MeetingRoomConfig child in entity.MeetingRoomConfigCollection)
					{
						if(child.MeetingRoomIdSource != null)
						{
							child.MeetingRoomId = child.MeetingRoomIdSource.Id;
						}
						else
						{
							child.MeetingRoomId = entity.Id;
						}

					}

					if (entity.MeetingRoomConfigCollection.Count > 0 || entity.MeetingRoomConfigCollection.DeletedItems.Count > 0)
					{
						//DataRepository.MeetingRoomConfigProvider.Save(transactionManager, entity.MeetingRoomConfigCollection);
						
						deepHandles.Add("MeetingRoomConfigCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< MeetingRoomConfig >) DataRepository.MeetingRoomConfigProvider.DeepSave,
							new object[] { transactionManager, entity.MeetingRoomConfigCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<SpandPmeetingRoom>
				if (CanDeepSave(entity.SpandPmeetingRoomCollection, "List<SpandPmeetingRoom>|SpandPmeetingRoomCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SpandPmeetingRoom child in entity.SpandPmeetingRoomCollection)
					{
						if(child.MeetingRoomIdSource != null)
						{
							child.MeetingRoomId = child.MeetingRoomIdSource.Id;
						}
						else
						{
							child.MeetingRoomId = entity.Id;
						}

					}

					if (entity.SpandPmeetingRoomCollection.Count > 0 || entity.SpandPmeetingRoomCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SpandPmeetingRoomProvider.Save(transactionManager, entity.SpandPmeetingRoomCollection);
						
						deepHandles.Add("SpandPmeetingRoomCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SpandPmeetingRoom >) DataRepository.SpandPmeetingRoomProvider.DeepSave,
							new object[] { transactionManager, entity.SpandPmeetingRoomCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Booking>
				if (CanDeepSave(entity.BookingCollection, "List<Booking>|BookingCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Booking child in entity.BookingCollection)
					{
						if(child.MainMeetingRoomIdSource != null)
						{
							child.MainMeetingRoomId = child.MainMeetingRoomIdSource.Id;
						}
						else
						{
							child.MainMeetingRoomId = entity.Id;
						}

					}

					if (entity.BookingCollection.Count > 0 || entity.BookingCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BookingProvider.Save(transactionManager, entity.BookingCollection);
						
						deepHandles.Add("BookingCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Booking >) DataRepository.BookingProvider.DeepSave,
							new object[] { transactionManager, entity.BookingCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<BookedMeetingRoom>
				if (CanDeepSave(entity.BookedMeetingRoomCollection, "List<BookedMeetingRoom>|BookedMeetingRoomCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(BookedMeetingRoom child in entity.BookedMeetingRoomCollection)
					{
						if(child.MeetingRoomIdSource != null)
						{
							child.MeetingRoomId = child.MeetingRoomIdSource.Id;
						}
						else
						{
							child.MeetingRoomId = entity.Id;
						}

					}

					if (entity.BookedMeetingRoomCollection.Count > 0 || entity.BookedMeetingRoomCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BookedMeetingRoomProvider.Save(transactionManager, entity.BookedMeetingRoomCollection);
						
						deepHandles.Add("BookedMeetingRoomCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< BookedMeetingRoom >) DataRepository.BookedMeetingRoomProvider.DeepSave,
							new object[] { transactionManager, entity.BookedMeetingRoomCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<MeetingRoomPictureVideo>
				if (CanDeepSave(entity.MeetingRoomPictureVideoCollection, "List<MeetingRoomPictureVideo>|MeetingRoomPictureVideoCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(MeetingRoomPictureVideo child in entity.MeetingRoomPictureVideoCollection)
					{
						if(child.MeetingRoomIdSource != null)
						{
							child.MeetingRoomId = child.MeetingRoomIdSource.Id;
						}
						else
						{
							child.MeetingRoomId = entity.Id;
						}

					}

					if (entity.MeetingRoomPictureVideoCollection.Count > 0 || entity.MeetingRoomPictureVideoCollection.DeletedItems.Count > 0)
					{
						//DataRepository.MeetingRoomPictureVideoProvider.Save(transactionManager, entity.MeetingRoomPictureVideoCollection);
						
						deepHandles.Add("MeetingRoomPictureVideoCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< MeetingRoomPictureVideo >) DataRepository.MeetingRoomPictureVideoProvider.DeepSave,
							new object[] { transactionManager, entity.MeetingRoomPictureVideoCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region MeetingRoomChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.MeetingRoom</c>
	///</summary>
	public enum MeetingRoomChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Hotel</c> at HotelIdSource
		///</summary>
		[ChildEntityType(typeof(Hotel))]
		Hotel,
			
		///<summary>
		/// Composite Property for <c>Users</c> at UpdatedBySource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
	
		///<summary>
		/// Collection of <c>MeetingRoom</c> as OneToMany for MeetingRoomDescCollection
		///</summary>
		[ChildEntityType(typeof(TList<MeetingRoomDesc>))]
		MeetingRoomDescCollection,

		///<summary>
		/// Collection of <c>MeetingRoom</c> as OneToMany for MeetingRoomConfigCollection
		///</summary>
		[ChildEntityType(typeof(TList<MeetingRoomConfig>))]
		MeetingRoomConfigCollection,

		///<summary>
		/// Collection of <c>MeetingRoom</c> as OneToMany for SpandPmeetingRoomCollection
		///</summary>
		[ChildEntityType(typeof(TList<SpandPmeetingRoom>))]
		SpandPmeetingRoomCollection,

		///<summary>
		/// Collection of <c>MeetingRoom</c> as OneToMany for BookingCollection
		///</summary>
		[ChildEntityType(typeof(TList<Booking>))]
		BookingCollection,

		///<summary>
		/// Collection of <c>MeetingRoom</c> as OneToMany for BookedMeetingRoomCollection
		///</summary>
		[ChildEntityType(typeof(TList<BookedMeetingRoom>))]
		BookedMeetingRoomCollection,

		///<summary>
		/// Collection of <c>MeetingRoom</c> as OneToMany for MeetingRoomPictureVideoCollection
		///</summary>
		[ChildEntityType(typeof(TList<MeetingRoomPictureVideo>))]
		MeetingRoomPictureVideoCollection,
	}
	
	#endregion MeetingRoomChildEntityTypes
	
	#region MeetingRoomFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;MeetingRoomColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomFilterBuilder : SqlFilterBuilder<MeetingRoomColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomFilterBuilder class.
		/// </summary>
		public MeetingRoomFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomFilterBuilder
	
	#region MeetingRoomParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;MeetingRoomColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoom"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MeetingRoomParameterBuilder : ParameterizedSqlFilterBuilder<MeetingRoomColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomParameterBuilder class.
		/// </summary>
		public MeetingRoomParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MeetingRoomParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MeetingRoomParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MeetingRoomParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MeetingRoomParameterBuilder
	
	#region MeetingRoomSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;MeetingRoomColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MeetingRoom"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class MeetingRoomSortBuilder : SqlSortBuilder<MeetingRoomColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MeetingRoomSqlSortBuilder class.
		/// </summary>
		public MeetingRoomSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion MeetingRoomSortBuilder
	
} // end namespace
