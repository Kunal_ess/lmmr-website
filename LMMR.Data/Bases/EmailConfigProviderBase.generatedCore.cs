﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="EmailConfigProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class EmailConfigProviderBaseCore : EntityProviderBase<LMMR.Entities.EmailConfig, LMMR.Entities.EmailConfigKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.EmailConfigKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.EmailConfig Get(TransactionManager transactionManager, LMMR.Entities.EmailConfigKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_EmailConfig index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.EmailConfig"/> class.</returns>
		public LMMR.Entities.EmailConfig GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EmailConfig index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.EmailConfig"/> class.</returns>
		public LMMR.Entities.EmailConfig GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EmailConfig index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.EmailConfig"/> class.</returns>
		public LMMR.Entities.EmailConfig GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EmailConfig index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.EmailConfig"/> class.</returns>
		public LMMR.Entities.EmailConfig GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EmailConfig index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.EmailConfig"/> class.</returns>
		public LMMR.Entities.EmailConfig GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EmailConfig index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.EmailConfig"/> class.</returns>
		public abstract LMMR.Entities.EmailConfig GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;EmailConfig&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;EmailConfig&gt;"/></returns>
		public static TList<EmailConfig> Fill(IDataReader reader, TList<EmailConfig> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.EmailConfig c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("EmailConfig")
					.Append("|").Append((System.Int64)reader[((int)EmailConfigColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<EmailConfig>(
					key.ToString(), // EntityTrackingKey
					"EmailConfig",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.EmailConfig();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)EmailConfigColumn.Id - 1)];
					c.EmailConfigType = (reader.IsDBNull(((int)EmailConfigColumn.EmailConfigType - 1)))?null:(System.Int32?)reader[((int)EmailConfigColumn.EmailConfigType - 1)];
					c.IsActive = (System.Boolean)reader[((int)EmailConfigColumn.IsActive - 1)];
					c.CreatedDate = (reader.IsDBNull(((int)EmailConfigColumn.CreatedDate - 1)))?null:(System.DateTime?)reader[((int)EmailConfigColumn.CreatedDate - 1)];
					c.TypeOfPeriod = (System.Int32)reader[((int)EmailConfigColumn.TypeOfPeriod - 1)];
					c.LastCallDuration = (reader.IsDBNull(((int)EmailConfigColumn.LastCallDuration - 1)))?null:(System.DateTime?)reader[((int)EmailConfigColumn.LastCallDuration - 1)];
					c.PeriodOfCall = (reader.IsDBNull(((int)EmailConfigColumn.PeriodOfCall - 1)))?null:(System.Int32?)reader[((int)EmailConfigColumn.PeriodOfCall - 1)];
					c.PerameterSupported = (reader.IsDBNull(((int)EmailConfigColumn.PerameterSupported - 1)))?null:(System.String)reader[((int)EmailConfigColumn.PerameterSupported - 1)];
					c.EmailName = (reader.IsDBNull(((int)EmailConfigColumn.EmailName - 1)))?null:(System.String)reader[((int)EmailConfigColumn.EmailName - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.EmailConfig"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.EmailConfig"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.EmailConfig entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)EmailConfigColumn.Id - 1)];
			entity.EmailConfigType = (reader.IsDBNull(((int)EmailConfigColumn.EmailConfigType - 1)))?null:(System.Int32?)reader[((int)EmailConfigColumn.EmailConfigType - 1)];
			entity.IsActive = (System.Boolean)reader[((int)EmailConfigColumn.IsActive - 1)];
			entity.CreatedDate = (reader.IsDBNull(((int)EmailConfigColumn.CreatedDate - 1)))?null:(System.DateTime?)reader[((int)EmailConfigColumn.CreatedDate - 1)];
			entity.TypeOfPeriod = (System.Int32)reader[((int)EmailConfigColumn.TypeOfPeriod - 1)];
			entity.LastCallDuration = (reader.IsDBNull(((int)EmailConfigColumn.LastCallDuration - 1)))?null:(System.DateTime?)reader[((int)EmailConfigColumn.LastCallDuration - 1)];
			entity.PeriodOfCall = (reader.IsDBNull(((int)EmailConfigColumn.PeriodOfCall - 1)))?null:(System.Int32?)reader[((int)EmailConfigColumn.PeriodOfCall - 1)];
			entity.PerameterSupported = (reader.IsDBNull(((int)EmailConfigColumn.PerameterSupported - 1)))?null:(System.String)reader[((int)EmailConfigColumn.PerameterSupported - 1)];
			entity.EmailName = (reader.IsDBNull(((int)EmailConfigColumn.EmailName - 1)))?null:(System.String)reader[((int)EmailConfigColumn.EmailName - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.EmailConfig"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.EmailConfig"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.EmailConfig entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.EmailConfigType = Convert.IsDBNull(dataRow["EmailConfigType"]) ? null : (System.Int32?)dataRow["EmailConfigType"];
			entity.IsActive = (System.Boolean)dataRow["IsActive"];
			entity.CreatedDate = Convert.IsDBNull(dataRow["CreatedDate"]) ? null : (System.DateTime?)dataRow["CreatedDate"];
			entity.TypeOfPeriod = (System.Int32)dataRow["TypeOfPeriod"];
			entity.LastCallDuration = Convert.IsDBNull(dataRow["LastCallDuration"]) ? null : (System.DateTime?)dataRow["LastCallDuration"];
			entity.PeriodOfCall = Convert.IsDBNull(dataRow["PeriodOfCall"]) ? null : (System.Int32?)dataRow["PeriodOfCall"];
			entity.PerameterSupported = Convert.IsDBNull(dataRow["PerameterSupported"]) ? null : (System.String)dataRow["PerameterSupported"];
			entity.EmailName = Convert.IsDBNull(dataRow["EmailName"]) ? null : (System.String)dataRow["EmailName"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.EmailConfig"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.EmailConfig Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.EmailConfig entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region EmailConfigMappingCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<EmailConfigMapping>|EmailConfigMappingCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'EmailConfigMappingCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.EmailConfigMappingCollection = DataRepository.EmailConfigMappingProvider.GetByEmailConfigureId(transactionManager, entity.Id);

				if (deep && entity.EmailConfigMappingCollection.Count > 0)
				{
					deepHandles.Add("EmailConfigMappingCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<EmailConfigMapping>) DataRepository.EmailConfigMappingProvider.DeepLoad,
						new object[] { transactionManager, entity.EmailConfigMappingCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.EmailConfig object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.EmailConfig instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.EmailConfig Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.EmailConfig entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<EmailConfigMapping>
				if (CanDeepSave(entity.EmailConfigMappingCollection, "List<EmailConfigMapping>|EmailConfigMappingCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(EmailConfigMapping child in entity.EmailConfigMappingCollection)
					{
						if(child.EmailConfigureIdSource != null)
						{
							child.EmailConfigureId = child.EmailConfigureIdSource.Id;
						}
						else
						{
							child.EmailConfigureId = entity.Id;
						}

					}

					if (entity.EmailConfigMappingCollection.Count > 0 || entity.EmailConfigMappingCollection.DeletedItems.Count > 0)
					{
						//DataRepository.EmailConfigMappingProvider.Save(transactionManager, entity.EmailConfigMappingCollection);
						
						deepHandles.Add("EmailConfigMappingCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< EmailConfigMapping >) DataRepository.EmailConfigMappingProvider.DeepSave,
							new object[] { transactionManager, entity.EmailConfigMappingCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region EmailConfigChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.EmailConfig</c>
	///</summary>
	public enum EmailConfigChildEntityTypes
	{

		///<summary>
		/// Collection of <c>EmailConfig</c> as OneToMany for EmailConfigMappingCollection
		///</summary>
		[ChildEntityType(typeof(TList<EmailConfigMapping>))]
		EmailConfigMappingCollection,
	}
	
	#endregion EmailConfigChildEntityTypes
	
	#region EmailConfigFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EmailConfigColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailConfig"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailConfigFilterBuilder : SqlFilterBuilder<EmailConfigColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailConfigFilterBuilder class.
		/// </summary>
		public EmailConfigFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EmailConfigFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EmailConfigFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EmailConfigFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EmailConfigFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EmailConfigFilterBuilder
	
	#region EmailConfigParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EmailConfigColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailConfig"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailConfigParameterBuilder : ParameterizedSqlFilterBuilder<EmailConfigColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailConfigParameterBuilder class.
		/// </summary>
		public EmailConfigParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EmailConfigParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EmailConfigParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EmailConfigParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EmailConfigParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EmailConfigParameterBuilder
	
	#region EmailConfigSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EmailConfigColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailConfig"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class EmailConfigSortBuilder : SqlSortBuilder<EmailConfigColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailConfigSqlSortBuilder class.
		/// </summary>
		public EmailConfigSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion EmailConfigSortBuilder
	
} // end namespace
