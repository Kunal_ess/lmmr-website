﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="FrontEndBottomDescriptionProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class FrontEndBottomDescriptionProviderBaseCore : EntityProviderBase<LMMR.Entities.FrontEndBottomDescription, LMMR.Entities.FrontEndBottomDescriptionKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.FrontEndBottomDescriptionKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottomDescription_FrontEndBottom key.
		///		FK_FrontEndBottomDescription_FrontEndBottom Description: 
		/// </summary>
		/// <param name="_frontEndId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottomDescription objects.</returns>
		public TList<FrontEndBottomDescription> GetByFrontEndId(System.Int64 _frontEndId)
		{
			int count = -1;
			return GetByFrontEndId(_frontEndId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottomDescription_FrontEndBottom key.
		///		FK_FrontEndBottomDescription_FrontEndBottom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_frontEndId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottomDescription objects.</returns>
		/// <remarks></remarks>
		public TList<FrontEndBottomDescription> GetByFrontEndId(TransactionManager transactionManager, System.Int64 _frontEndId)
		{
			int count = -1;
			return GetByFrontEndId(transactionManager, _frontEndId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottomDescription_FrontEndBottom key.
		///		FK_FrontEndBottomDescription_FrontEndBottom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_frontEndId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottomDescription objects.</returns>
		public TList<FrontEndBottomDescription> GetByFrontEndId(TransactionManager transactionManager, System.Int64 _frontEndId, int start, int pageLength)
		{
			int count = -1;
			return GetByFrontEndId(transactionManager, _frontEndId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottomDescription_FrontEndBottom key.
		///		fkFrontEndBottomDescriptionFrontEndBottom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_frontEndId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottomDescription objects.</returns>
		public TList<FrontEndBottomDescription> GetByFrontEndId(System.Int64 _frontEndId, int start, int pageLength)
		{
			int count =  -1;
			return GetByFrontEndId(null, _frontEndId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottomDescription_FrontEndBottom key.
		///		fkFrontEndBottomDescriptionFrontEndBottom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_frontEndId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottomDescription objects.</returns>
		public TList<FrontEndBottomDescription> GetByFrontEndId(System.Int64 _frontEndId, int start, int pageLength,out int count)
		{
			return GetByFrontEndId(null, _frontEndId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottomDescription_FrontEndBottom key.
		///		FK_FrontEndBottomDescription_FrontEndBottom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_frontEndId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottomDescription objects.</returns>
		public abstract TList<FrontEndBottomDescription> GetByFrontEndId(TransactionManager transactionManager, System.Int64 _frontEndId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottomDescription_Language key.
		///		FK_FrontEndBottomDescription_Language Description: 
		/// </summary>
		/// <param name="_languageId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottomDescription objects.</returns>
		public TList<FrontEndBottomDescription> GetByLanguageId(System.Int64 _languageId)
		{
			int count = -1;
			return GetByLanguageId(_languageId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottomDescription_Language key.
		///		FK_FrontEndBottomDescription_Language Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_languageId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottomDescription objects.</returns>
		/// <remarks></remarks>
		public TList<FrontEndBottomDescription> GetByLanguageId(TransactionManager transactionManager, System.Int64 _languageId)
		{
			int count = -1;
			return GetByLanguageId(transactionManager, _languageId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottomDescription_Language key.
		///		FK_FrontEndBottomDescription_Language Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_languageId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottomDescription objects.</returns>
		public TList<FrontEndBottomDescription> GetByLanguageId(TransactionManager transactionManager, System.Int64 _languageId, int start, int pageLength)
		{
			int count = -1;
			return GetByLanguageId(transactionManager, _languageId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottomDescription_Language key.
		///		fkFrontEndBottomDescriptionLanguage Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_languageId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottomDescription objects.</returns>
		public TList<FrontEndBottomDescription> GetByLanguageId(System.Int64 _languageId, int start, int pageLength)
		{
			int count =  -1;
			return GetByLanguageId(null, _languageId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottomDescription_Language key.
		///		fkFrontEndBottomDescriptionLanguage Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_languageId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottomDescription objects.</returns>
		public TList<FrontEndBottomDescription> GetByLanguageId(System.Int64 _languageId, int start, int pageLength,out int count)
		{
			return GetByLanguageId(null, _languageId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FrontEndBottomDescription_Language key.
		///		FK_FrontEndBottomDescription_Language Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_languageId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.FrontEndBottomDescription objects.</returns>
		public abstract TList<FrontEndBottomDescription> GetByLanguageId(TransactionManager transactionManager, System.Int64 _languageId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.FrontEndBottomDescription Get(TransactionManager transactionManager, LMMR.Entities.FrontEndBottomDescriptionKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_FrontEndBottomDescription index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.FrontEndBottomDescription"/> class.</returns>
		public LMMR.Entities.FrontEndBottomDescription GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FrontEndBottomDescription index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.FrontEndBottomDescription"/> class.</returns>
		public LMMR.Entities.FrontEndBottomDescription GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FrontEndBottomDescription index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.FrontEndBottomDescription"/> class.</returns>
		public LMMR.Entities.FrontEndBottomDescription GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FrontEndBottomDescription index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.FrontEndBottomDescription"/> class.</returns>
		public LMMR.Entities.FrontEndBottomDescription GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FrontEndBottomDescription index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.FrontEndBottomDescription"/> class.</returns>
		public LMMR.Entities.FrontEndBottomDescription GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FrontEndBottomDescription index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.FrontEndBottomDescription"/> class.</returns>
		public abstract LMMR.Entities.FrontEndBottomDescription GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;FrontEndBottomDescription&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;FrontEndBottomDescription&gt;"/></returns>
		public static TList<FrontEndBottomDescription> Fill(IDataReader reader, TList<FrontEndBottomDescription> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.FrontEndBottomDescription c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("FrontEndBottomDescription")
					.Append("|").Append((System.Int64)reader[((int)FrontEndBottomDescriptionColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<FrontEndBottomDescription>(
					key.ToString(), // EntityTrackingKey
					"FrontEndBottomDescription",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.FrontEndBottomDescription();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)FrontEndBottomDescriptionColumn.Id - 1)];
					c.Name = (reader.IsDBNull(((int)FrontEndBottomDescriptionColumn.Name - 1)))?null:(System.String)reader[((int)FrontEndBottomDescriptionColumn.Name - 1)];
					c.LanguageId = (System.Int64)reader[((int)FrontEndBottomDescriptionColumn.LanguageId - 1)];
					c.FrontEndId = (System.Int64)reader[((int)FrontEndBottomDescriptionColumn.FrontEndId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.FrontEndBottomDescription"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.FrontEndBottomDescription"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.FrontEndBottomDescription entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)FrontEndBottomDescriptionColumn.Id - 1)];
			entity.Name = (reader.IsDBNull(((int)FrontEndBottomDescriptionColumn.Name - 1)))?null:(System.String)reader[((int)FrontEndBottomDescriptionColumn.Name - 1)];
			entity.LanguageId = (System.Int64)reader[((int)FrontEndBottomDescriptionColumn.LanguageId - 1)];
			entity.FrontEndId = (System.Int64)reader[((int)FrontEndBottomDescriptionColumn.FrontEndId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.FrontEndBottomDescription"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.FrontEndBottomDescription"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.FrontEndBottomDescription entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.Name = Convert.IsDBNull(dataRow["Name"]) ? null : (System.String)dataRow["Name"];
			entity.LanguageId = (System.Int64)dataRow["LanguageId"];
			entity.FrontEndId = (System.Int64)dataRow["FrontEndId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.FrontEndBottomDescription"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.FrontEndBottomDescription Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.FrontEndBottomDescription entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region FrontEndIdSource	
			if (CanDeepLoad(entity, "FrontEndBottom|FrontEndIdSource", deepLoadType, innerList) 
				&& entity.FrontEndIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.FrontEndId;
				FrontEndBottom tmpEntity = EntityManager.LocateEntity<FrontEndBottom>(EntityLocator.ConstructKeyFromPkItems(typeof(FrontEndBottom), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.FrontEndIdSource = tmpEntity;
				else
					entity.FrontEndIdSource = DataRepository.FrontEndBottomProvider.GetById(transactionManager, entity.FrontEndId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FrontEndIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.FrontEndIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.FrontEndBottomProvider.DeepLoad(transactionManager, entity.FrontEndIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion FrontEndIdSource

			#region LanguageIdSource	
			if (CanDeepLoad(entity, "Language|LanguageIdSource", deepLoadType, innerList) 
				&& entity.LanguageIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.LanguageId;
				Language tmpEntity = EntityManager.LocateEntity<Language>(EntityLocator.ConstructKeyFromPkItems(typeof(Language), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.LanguageIdSource = tmpEntity;
				else
					entity.LanguageIdSource = DataRepository.LanguageProvider.GetById(transactionManager, entity.LanguageId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'LanguageIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.LanguageIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.LanguageProvider.DeepLoad(transactionManager, entity.LanguageIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion LanguageIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.FrontEndBottomDescription object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.FrontEndBottomDescription instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.FrontEndBottomDescription Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.FrontEndBottomDescription entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region FrontEndIdSource
			if (CanDeepSave(entity, "FrontEndBottom|FrontEndIdSource", deepSaveType, innerList) 
				&& entity.FrontEndIdSource != null)
			{
				DataRepository.FrontEndBottomProvider.Save(transactionManager, entity.FrontEndIdSource);
				entity.FrontEndId = entity.FrontEndIdSource.Id;
			}
			#endregion 
			
			#region LanguageIdSource
			if (CanDeepSave(entity, "Language|LanguageIdSource", deepSaveType, innerList) 
				&& entity.LanguageIdSource != null)
			{
				DataRepository.LanguageProvider.Save(transactionManager, entity.LanguageIdSource);
				entity.LanguageId = entity.LanguageIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region FrontEndBottomDescriptionChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.FrontEndBottomDescription</c>
	///</summary>
	public enum FrontEndBottomDescriptionChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>FrontEndBottom</c> at FrontEndIdSource
		///</summary>
		[ChildEntityType(typeof(FrontEndBottom))]
		FrontEndBottom,
			
		///<summary>
		/// Composite Property for <c>Language</c> at LanguageIdSource
		///</summary>
		[ChildEntityType(typeof(Language))]
		Language,
		}
	
	#endregion FrontEndBottomDescriptionChildEntityTypes
	
	#region FrontEndBottomDescriptionFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;FrontEndBottomDescriptionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FrontEndBottomDescription"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FrontEndBottomDescriptionFilterBuilder : SqlFilterBuilder<FrontEndBottomDescriptionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomDescriptionFilterBuilder class.
		/// </summary>
		public FrontEndBottomDescriptionFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomDescriptionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FrontEndBottomDescriptionFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomDescriptionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FrontEndBottomDescriptionFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FrontEndBottomDescriptionFilterBuilder
	
	#region FrontEndBottomDescriptionParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;FrontEndBottomDescriptionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FrontEndBottomDescription"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FrontEndBottomDescriptionParameterBuilder : ParameterizedSqlFilterBuilder<FrontEndBottomDescriptionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomDescriptionParameterBuilder class.
		/// </summary>
		public FrontEndBottomDescriptionParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomDescriptionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FrontEndBottomDescriptionParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomDescriptionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FrontEndBottomDescriptionParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FrontEndBottomDescriptionParameterBuilder
	
	#region FrontEndBottomDescriptionSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;FrontEndBottomDescriptionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FrontEndBottomDescription"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class FrontEndBottomDescriptionSortBuilder : SqlSortBuilder<FrontEndBottomDescriptionColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FrontEndBottomDescriptionSqlSortBuilder class.
		/// </summary>
		public FrontEndBottomDescriptionSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion FrontEndBottomDescriptionSortBuilder
	
} // end namespace
