﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="BedRoomTrailProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class BedRoomTrailProviderBaseCore : EntityProviderBase<LMMR.Entities.BedRoomTrail, LMMR.Entities.BedRoomTrailKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.BedRoomTrailKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoom_Trail_BedRoom key.
		///		FK_BedRoom_Trail_BedRoom Description: 
		/// </summary>
		/// <param name="_bedroomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomTrail objects.</returns>
		public TList<BedRoomTrail> GetByBedroomId(System.Int64? _bedroomId)
		{
			int count = -1;
			return GetByBedroomId(_bedroomId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoom_Trail_BedRoom key.
		///		FK_BedRoom_Trail_BedRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bedroomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomTrail objects.</returns>
		/// <remarks></remarks>
		public TList<BedRoomTrail> GetByBedroomId(TransactionManager transactionManager, System.Int64? _bedroomId)
		{
			int count = -1;
			return GetByBedroomId(transactionManager, _bedroomId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoom_Trail_BedRoom key.
		///		FK_BedRoom_Trail_BedRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bedroomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomTrail objects.</returns>
		public TList<BedRoomTrail> GetByBedroomId(TransactionManager transactionManager, System.Int64? _bedroomId, int start, int pageLength)
		{
			int count = -1;
			return GetByBedroomId(transactionManager, _bedroomId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoom_Trail_BedRoom key.
		///		fkBedRoomTrailBedRoom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bedroomId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomTrail objects.</returns>
		public TList<BedRoomTrail> GetByBedroomId(System.Int64? _bedroomId, int start, int pageLength)
		{
			int count =  -1;
			return GetByBedroomId(null, _bedroomId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoom_Trail_BedRoom key.
		///		fkBedRoomTrailBedRoom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bedroomId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomTrail objects.</returns>
		public TList<BedRoomTrail> GetByBedroomId(System.Int64? _bedroomId, int start, int pageLength,out int count)
		{
			return GetByBedroomId(null, _bedroomId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoom_Trail_BedRoom key.
		///		FK_BedRoom_Trail_BedRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bedroomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomTrail objects.</returns>
		public abstract TList<BedRoomTrail> GetByBedroomId(TransactionManager transactionManager, System.Int64? _bedroomId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.BedRoomTrail Get(TransactionManager transactionManager, LMMR.Entities.BedRoomTrailKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_BedRoom_Trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomTrail"/> class.</returns>
		public LMMR.Entities.BedRoomTrail GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoom_Trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomTrail"/> class.</returns>
		public LMMR.Entities.BedRoomTrail GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoom_Trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomTrail"/> class.</returns>
		public LMMR.Entities.BedRoomTrail GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoom_Trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomTrail"/> class.</returns>
		public LMMR.Entities.BedRoomTrail GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoom_Trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomTrail"/> class.</returns>
		public LMMR.Entities.BedRoomTrail GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoom_Trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomTrail"/> class.</returns>
		public abstract LMMR.Entities.BedRoomTrail GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;BedRoomTrail&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;BedRoomTrail&gt;"/></returns>
		public static TList<BedRoomTrail> Fill(IDataReader reader, TList<BedRoomTrail> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.BedRoomTrail c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("BedRoomTrail")
					.Append("|").Append((System.Int64)reader[((int)BedRoomTrailColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<BedRoomTrail>(
					key.ToString(), // EntityTrackingKey
					"BedRoomTrail",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.BedRoomTrail();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)BedRoomTrailColumn.Id - 1)];
					c.BedroomId = (reader.IsDBNull(((int)BedRoomTrailColumn.BedroomId - 1)))?null:(System.Int64?)reader[((int)BedRoomTrailColumn.BedroomId - 1)];
					c.HotelId = (System.Int64)reader[((int)BedRoomTrailColumn.HotelId - 1)];
					c.Name = (reader.IsDBNull(((int)BedRoomTrailColumn.Name - 1)))?null:(System.String)reader[((int)BedRoomTrailColumn.Name - 1)];
					c.Types = (reader.IsDBNull(((int)BedRoomTrailColumn.Types - 1)))?null:(System.Int32?)reader[((int)BedRoomTrailColumn.Types - 1)];
					c.Allotment = (reader.IsDBNull(((int)BedRoomTrailColumn.Allotment - 1)))?null:(System.Int32?)reader[((int)BedRoomTrailColumn.Allotment - 1)];
					c.Picture = (reader.IsDBNull(((int)BedRoomTrailColumn.Picture - 1)))?null:(System.String)reader[((int)BedRoomTrailColumn.Picture - 1)];
					c.IsActive = (System.Boolean)reader[((int)BedRoomTrailColumn.IsActive - 1)];
					c.PriceDouble = (reader.IsDBNull(((int)BedRoomTrailColumn.PriceDouble - 1)))?null:(System.Decimal?)reader[((int)BedRoomTrailColumn.PriceDouble - 1)];
					c.PriceSingle = (reader.IsDBNull(((int)BedRoomTrailColumn.PriceSingle - 1)))?null:(System.Decimal?)reader[((int)BedRoomTrailColumn.PriceSingle - 1)];
					c.IsBreakFastInclude = (System.Boolean)reader[((int)BedRoomTrailColumn.IsBreakFastInclude - 1)];
					c.BreakfastPrice = (reader.IsDBNull(((int)BedRoomTrailColumn.BreakfastPrice - 1)))?null:(System.Decimal?)reader[((int)BedRoomTrailColumn.BreakfastPrice - 1)];
					c.IsDeleted = (System.Boolean)reader[((int)BedRoomTrailColumn.IsDeleted - 1)];
					c.UpdatedBy = (reader.IsDBNull(((int)BedRoomTrailColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)BedRoomTrailColumn.UpdatedBy - 1)];
					c.UpdateDate = (reader.IsDBNull(((int)BedRoomTrailColumn.UpdateDate - 1)))?null:(System.DateTime?)reader[((int)BedRoomTrailColumn.UpdateDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.BedRoomTrail"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BedRoomTrail"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.BedRoomTrail entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)BedRoomTrailColumn.Id - 1)];
			entity.BedroomId = (reader.IsDBNull(((int)BedRoomTrailColumn.BedroomId - 1)))?null:(System.Int64?)reader[((int)BedRoomTrailColumn.BedroomId - 1)];
			entity.HotelId = (System.Int64)reader[((int)BedRoomTrailColumn.HotelId - 1)];
			entity.Name = (reader.IsDBNull(((int)BedRoomTrailColumn.Name - 1)))?null:(System.String)reader[((int)BedRoomTrailColumn.Name - 1)];
			entity.Types = (reader.IsDBNull(((int)BedRoomTrailColumn.Types - 1)))?null:(System.Int32?)reader[((int)BedRoomTrailColumn.Types - 1)];
			entity.Allotment = (reader.IsDBNull(((int)BedRoomTrailColumn.Allotment - 1)))?null:(System.Int32?)reader[((int)BedRoomTrailColumn.Allotment - 1)];
			entity.Picture = (reader.IsDBNull(((int)BedRoomTrailColumn.Picture - 1)))?null:(System.String)reader[((int)BedRoomTrailColumn.Picture - 1)];
			entity.IsActive = (System.Boolean)reader[((int)BedRoomTrailColumn.IsActive - 1)];
			entity.PriceDouble = (reader.IsDBNull(((int)BedRoomTrailColumn.PriceDouble - 1)))?null:(System.Decimal?)reader[((int)BedRoomTrailColumn.PriceDouble - 1)];
			entity.PriceSingle = (reader.IsDBNull(((int)BedRoomTrailColumn.PriceSingle - 1)))?null:(System.Decimal?)reader[((int)BedRoomTrailColumn.PriceSingle - 1)];
			entity.IsBreakFastInclude = (System.Boolean)reader[((int)BedRoomTrailColumn.IsBreakFastInclude - 1)];
			entity.BreakfastPrice = (reader.IsDBNull(((int)BedRoomTrailColumn.BreakfastPrice - 1)))?null:(System.Decimal?)reader[((int)BedRoomTrailColumn.BreakfastPrice - 1)];
			entity.IsDeleted = (System.Boolean)reader[((int)BedRoomTrailColumn.IsDeleted - 1)];
			entity.UpdatedBy = (reader.IsDBNull(((int)BedRoomTrailColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)BedRoomTrailColumn.UpdatedBy - 1)];
			entity.UpdateDate = (reader.IsDBNull(((int)BedRoomTrailColumn.UpdateDate - 1)))?null:(System.DateTime?)reader[((int)BedRoomTrailColumn.UpdateDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.BedRoomTrail"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BedRoomTrail"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.BedRoomTrail entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.BedroomId = Convert.IsDBNull(dataRow["BedroomId"]) ? null : (System.Int64?)dataRow["BedroomId"];
			entity.HotelId = (System.Int64)dataRow["HotelId"];
			entity.Name = Convert.IsDBNull(dataRow["Name"]) ? null : (System.String)dataRow["Name"];
			entity.Types = Convert.IsDBNull(dataRow["Types"]) ? null : (System.Int32?)dataRow["Types"];
			entity.Allotment = Convert.IsDBNull(dataRow["Allotment"]) ? null : (System.Int32?)dataRow["Allotment"];
			entity.Picture = Convert.IsDBNull(dataRow["Picture"]) ? null : (System.String)dataRow["Picture"];
			entity.IsActive = (System.Boolean)dataRow["IsActive"];
			entity.PriceDouble = Convert.IsDBNull(dataRow["PriceDouble"]) ? null : (System.Decimal?)dataRow["PriceDouble"];
			entity.PriceSingle = Convert.IsDBNull(dataRow["PriceSingle"]) ? null : (System.Decimal?)dataRow["PriceSingle"];
			entity.IsBreakFastInclude = (System.Boolean)dataRow["IsBreakFastInclude"];
			entity.BreakfastPrice = Convert.IsDBNull(dataRow["BreakfastPrice"]) ? null : (System.Decimal?)dataRow["BreakfastPrice"];
			entity.IsDeleted = (System.Boolean)dataRow["IsDeleted"];
			entity.UpdatedBy = Convert.IsDBNull(dataRow["UpdatedBy"]) ? null : (System.Int64?)dataRow["UpdatedBy"];
			entity.UpdateDate = Convert.IsDBNull(dataRow["UpdateDate"]) ? null : (System.DateTime?)dataRow["UpdateDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BedRoomTrail"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.BedRoomTrail Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.BedRoomTrail entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region BedroomIdSource	
			if (CanDeepLoad(entity, "BedRoom|BedroomIdSource", deepLoadType, innerList) 
				&& entity.BedroomIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.BedroomId ?? (long)0);
				BedRoom tmpEntity = EntityManager.LocateEntity<BedRoom>(EntityLocator.ConstructKeyFromPkItems(typeof(BedRoom), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.BedroomIdSource = tmpEntity;
				else
					entity.BedroomIdSource = DataRepository.BedRoomProvider.GetById(transactionManager, (entity.BedroomId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BedroomIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.BedroomIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.BedRoomProvider.DeepLoad(transactionManager, entity.BedroomIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion BedroomIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.BedRoomTrail object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.BedRoomTrail instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.BedRoomTrail Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.BedRoomTrail entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region BedroomIdSource
			if (CanDeepSave(entity, "BedRoom|BedroomIdSource", deepSaveType, innerList) 
				&& entity.BedroomIdSource != null)
			{
				DataRepository.BedRoomProvider.Save(transactionManager, entity.BedroomIdSource);
				entity.BedroomId = entity.BedroomIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region BedRoomTrailChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.BedRoomTrail</c>
	///</summary>
	public enum BedRoomTrailChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>BedRoom</c> at BedroomIdSource
		///</summary>
		[ChildEntityType(typeof(BedRoom))]
		BedRoom,
		}
	
	#endregion BedRoomTrailChildEntityTypes
	
	#region BedRoomTrailFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;BedRoomTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BedRoomTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BedRoomTrailFilterBuilder : SqlFilterBuilder<BedRoomTrailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomTrailFilterBuilder class.
		/// </summary>
		public BedRoomTrailFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BedRoomTrailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BedRoomTrailFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BedRoomTrailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BedRoomTrailFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BedRoomTrailFilterBuilder
	
	#region BedRoomTrailParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;BedRoomTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BedRoomTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BedRoomTrailParameterBuilder : ParameterizedSqlFilterBuilder<BedRoomTrailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomTrailParameterBuilder class.
		/// </summary>
		public BedRoomTrailParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BedRoomTrailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BedRoomTrailParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BedRoomTrailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BedRoomTrailParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BedRoomTrailParameterBuilder
	
	#region BedRoomTrailSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;BedRoomTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BedRoomTrail"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class BedRoomTrailSortBuilder : SqlSortBuilder<BedRoomTrailColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomTrailSqlSortBuilder class.
		/// </summary>
		public BedRoomTrailSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion BedRoomTrailSortBuilder
	
} // end namespace
