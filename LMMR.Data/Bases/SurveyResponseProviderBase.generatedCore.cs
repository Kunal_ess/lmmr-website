﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SurveyResponseProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SurveyResponseProviderBaseCore : EntityProviderBase<LMMR.Entities.SurveyResponse, LMMR.Entities.SurveyResponseKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.SurveyResponseKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyResponse_Booking key.
		///		FK_SurveyResponse_Booking Description: 
		/// </summary>
		/// <param name="_bookingId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyResponse objects.</returns>
		public TList<SurveyResponse> GetByBookingId(System.Int64 _bookingId)
		{
			int count = -1;
			return GetByBookingId(_bookingId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyResponse_Booking key.
		///		FK_SurveyResponse_Booking Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bookingId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyResponse objects.</returns>
		/// <remarks></remarks>
		public TList<SurveyResponse> GetByBookingId(TransactionManager transactionManager, System.Int64 _bookingId)
		{
			int count = -1;
			return GetByBookingId(transactionManager, _bookingId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyResponse_Booking key.
		///		FK_SurveyResponse_Booking Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bookingId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyResponse objects.</returns>
		public TList<SurveyResponse> GetByBookingId(TransactionManager transactionManager, System.Int64 _bookingId, int start, int pageLength)
		{
			int count = -1;
			return GetByBookingId(transactionManager, _bookingId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyResponse_Booking key.
		///		fkSurveyResponseBooking Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bookingId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyResponse objects.</returns>
		public TList<SurveyResponse> GetByBookingId(System.Int64 _bookingId, int start, int pageLength)
		{
			int count =  -1;
			return GetByBookingId(null, _bookingId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyResponse_Booking key.
		///		fkSurveyResponseBooking Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bookingId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyResponse objects.</returns>
		public TList<SurveyResponse> GetByBookingId(System.Int64 _bookingId, int start, int pageLength,out int count)
		{
			return GetByBookingId(null, _bookingId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyResponse_Booking key.
		///		FK_SurveyResponse_Booking Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bookingId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyResponse objects.</returns>
		public abstract TList<SurveyResponse> GetByBookingId(TransactionManager transactionManager, System.Int64 _bookingId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyResponse_Survey key.
		///		FK_SurveyResponse_Survey Description: 
		/// </summary>
		/// <param name="_surveyId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyResponse objects.</returns>
		public TList<SurveyResponse> GetBySurveyId(System.Int64 _surveyId)
		{
			int count = -1;
			return GetBySurveyId(_surveyId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyResponse_Survey key.
		///		FK_SurveyResponse_Survey Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_surveyId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyResponse objects.</returns>
		/// <remarks></remarks>
		public TList<SurveyResponse> GetBySurveyId(TransactionManager transactionManager, System.Int64 _surveyId)
		{
			int count = -1;
			return GetBySurveyId(transactionManager, _surveyId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyResponse_Survey key.
		///		FK_SurveyResponse_Survey Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_surveyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyResponse objects.</returns>
		public TList<SurveyResponse> GetBySurveyId(TransactionManager transactionManager, System.Int64 _surveyId, int start, int pageLength)
		{
			int count = -1;
			return GetBySurveyId(transactionManager, _surveyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyResponse_Survey key.
		///		fkSurveyResponseSurvey Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_surveyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyResponse objects.</returns>
		public TList<SurveyResponse> GetBySurveyId(System.Int64 _surveyId, int start, int pageLength)
		{
			int count =  -1;
			return GetBySurveyId(null, _surveyId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyResponse_Survey key.
		///		fkSurveyResponseSurvey Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_surveyId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyResponse objects.</returns>
		public TList<SurveyResponse> GetBySurveyId(System.Int64 _surveyId, int start, int pageLength,out int count)
		{
			return GetBySurveyId(null, _surveyId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyResponse_Survey key.
		///		FK_SurveyResponse_Survey Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_surveyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyResponse objects.</returns>
		public abstract TList<SurveyResponse> GetBySurveyId(TransactionManager transactionManager, System.Int64 _surveyId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyResponse_Users key.
		///		FK_SurveyResponse_Users Description: 
		/// </summary>
		/// <param name="_userId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyResponse objects.</returns>
		public TList<SurveyResponse> GetByUserId(System.Int64 _userId)
		{
			int count = -1;
			return GetByUserId(_userId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyResponse_Users key.
		///		FK_SurveyResponse_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyResponse objects.</returns>
		/// <remarks></remarks>
		public TList<SurveyResponse> GetByUserId(TransactionManager transactionManager, System.Int64 _userId)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyResponse_Users key.
		///		FK_SurveyResponse_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyResponse objects.</returns>
		public TList<SurveyResponse> GetByUserId(TransactionManager transactionManager, System.Int64 _userId, int start, int pageLength)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyResponse_Users key.
		///		fkSurveyResponseUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_userId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyResponse objects.</returns>
		public TList<SurveyResponse> GetByUserId(System.Int64 _userId, int start, int pageLength)
		{
			int count =  -1;
			return GetByUserId(null, _userId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyResponse_Users key.
		///		fkSurveyResponseUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_userId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyResponse objects.</returns>
		public TList<SurveyResponse> GetByUserId(System.Int64 _userId, int start, int pageLength,out int count)
		{
			return GetByUserId(null, _userId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SurveyResponse_Users key.
		///		FK_SurveyResponse_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyResponse objects.</returns>
		public abstract TList<SurveyResponse> GetByUserId(TransactionManager transactionManager, System.Int64 _userId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.SurveyResponse Get(TransactionManager transactionManager, LMMR.Entities.SurveyResponseKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SurveyResponse index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyResponse"/> class.</returns>
		public LMMR.Entities.SurveyResponse GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SurveyResponse index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyResponse"/> class.</returns>
		public LMMR.Entities.SurveyResponse GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SurveyResponse index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyResponse"/> class.</returns>
		public LMMR.Entities.SurveyResponse GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SurveyResponse index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyResponse"/> class.</returns>
		public LMMR.Entities.SurveyResponse GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SurveyResponse index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyResponse"/> class.</returns>
		public LMMR.Entities.SurveyResponse GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SurveyResponse index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyResponse"/> class.</returns>
		public abstract LMMR.Entities.SurveyResponse GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SurveyResponse&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SurveyResponse&gt;"/></returns>
		public static TList<SurveyResponse> Fill(IDataReader reader, TList<SurveyResponse> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.SurveyResponse c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SurveyResponse")
					.Append("|").Append((System.Int64)reader[((int)SurveyResponseColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SurveyResponse>(
					key.ToString(), // EntityTrackingKey
					"SurveyResponse",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.SurveyResponse();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)SurveyResponseColumn.Id - 1)];
					c.UserId = (System.Int64)reader[((int)SurveyResponseColumn.UserId - 1)];
					c.SurveyId = (System.Int64)reader[((int)SurveyResponseColumn.SurveyId - 1)];
					c.Answer = (reader.IsDBNull(((int)SurveyResponseColumn.Answer - 1)))?null:(System.Int32?)reader[((int)SurveyResponseColumn.Answer - 1)];
					c.BookingId = (System.Int64)reader[((int)SurveyResponseColumn.BookingId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.SurveyResponse"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SurveyResponse"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.SurveyResponse entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)SurveyResponseColumn.Id - 1)];
			entity.UserId = (System.Int64)reader[((int)SurveyResponseColumn.UserId - 1)];
			entity.SurveyId = (System.Int64)reader[((int)SurveyResponseColumn.SurveyId - 1)];
			entity.Answer = (reader.IsDBNull(((int)SurveyResponseColumn.Answer - 1)))?null:(System.Int32?)reader[((int)SurveyResponseColumn.Answer - 1)];
			entity.BookingId = (System.Int64)reader[((int)SurveyResponseColumn.BookingId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.SurveyResponse"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SurveyResponse"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.SurveyResponse entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.UserId = (System.Int64)dataRow["UserId"];
			entity.SurveyId = (System.Int64)dataRow["SurveyId"];
			entity.Answer = Convert.IsDBNull(dataRow["Answer"]) ? null : (System.Int32?)dataRow["Answer"];
			entity.BookingId = (System.Int64)dataRow["BookingId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SurveyResponse"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.SurveyResponse Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.SurveyResponse entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region BookingIdSource	
			if (CanDeepLoad(entity, "Booking|BookingIdSource", deepLoadType, innerList) 
				&& entity.BookingIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.BookingId;
				Booking tmpEntity = EntityManager.LocateEntity<Booking>(EntityLocator.ConstructKeyFromPkItems(typeof(Booking), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.BookingIdSource = tmpEntity;
				else
					entity.BookingIdSource = DataRepository.BookingProvider.GetById(transactionManager, entity.BookingId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BookingIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.BookingIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.BookingProvider.DeepLoad(transactionManager, entity.BookingIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion BookingIdSource

			#region SurveyIdSource	
			if (CanDeepLoad(entity, "Survey|SurveyIdSource", deepLoadType, innerList) 
				&& entity.SurveyIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SurveyId;
				Survey tmpEntity = EntityManager.LocateEntity<Survey>(EntityLocator.ConstructKeyFromPkItems(typeof(Survey), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SurveyIdSource = tmpEntity;
				else
					entity.SurveyIdSource = DataRepository.SurveyProvider.GetById(transactionManager, entity.SurveyId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SurveyIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SurveyIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SurveyProvider.DeepLoad(transactionManager, entity.SurveyIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SurveyIdSource

			#region UserIdSource	
			if (CanDeepLoad(entity, "Users|UserIdSource", deepLoadType, innerList) 
				&& entity.UserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.UserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.UserIdSource = tmpEntity;
				else
					entity.UserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.UserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.UserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.UserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion UserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.SurveyResponse object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.SurveyResponse instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.SurveyResponse Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.SurveyResponse entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region BookingIdSource
			if (CanDeepSave(entity, "Booking|BookingIdSource", deepSaveType, innerList) 
				&& entity.BookingIdSource != null)
			{
				DataRepository.BookingProvider.Save(transactionManager, entity.BookingIdSource);
				entity.BookingId = entity.BookingIdSource.Id;
			}
			#endregion 
			
			#region SurveyIdSource
			if (CanDeepSave(entity, "Survey|SurveyIdSource", deepSaveType, innerList) 
				&& entity.SurveyIdSource != null)
			{
				DataRepository.SurveyProvider.Save(transactionManager, entity.SurveyIdSource);
				entity.SurveyId = entity.SurveyIdSource.Id;
			}
			#endregion 
			
			#region UserIdSource
			if (CanDeepSave(entity, "Users|UserIdSource", deepSaveType, innerList) 
				&& entity.UserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.UserIdSource);
				entity.UserId = entity.UserIdSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SurveyResponseChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.SurveyResponse</c>
	///</summary>
	public enum SurveyResponseChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Booking</c> at BookingIdSource
		///</summary>
		[ChildEntityType(typeof(Booking))]
		Booking,
			
		///<summary>
		/// Composite Property for <c>Survey</c> at SurveyIdSource
		///</summary>
		[ChildEntityType(typeof(Survey))]
		Survey,
			
		///<summary>
		/// Composite Property for <c>Users</c> at UserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
		}
	
	#endregion SurveyResponseChildEntityTypes
	
	#region SurveyResponseFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SurveyResponseColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SurveyResponse"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SurveyResponseFilterBuilder : SqlFilterBuilder<SurveyResponseColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SurveyResponseFilterBuilder class.
		/// </summary>
		public SurveyResponseFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SurveyResponseFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SurveyResponseFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SurveyResponseFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SurveyResponseFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SurveyResponseFilterBuilder
	
	#region SurveyResponseParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SurveyResponseColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SurveyResponse"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SurveyResponseParameterBuilder : ParameterizedSqlFilterBuilder<SurveyResponseColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SurveyResponseParameterBuilder class.
		/// </summary>
		public SurveyResponseParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SurveyResponseParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SurveyResponseParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SurveyResponseParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SurveyResponseParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SurveyResponseParameterBuilder
	
	#region SurveyResponseSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SurveyResponseColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SurveyResponse"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SurveyResponseSortBuilder : SqlSortBuilder<SurveyResponseColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SurveyResponseSqlSortBuilder class.
		/// </summary>
		public SurveyResponseSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SurveyResponseSortBuilder
	
} // end namespace
