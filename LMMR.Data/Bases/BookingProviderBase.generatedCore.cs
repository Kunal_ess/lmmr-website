﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="BookingProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class BookingProviderBaseCore : EntityProviderBase<LMMR.Entities.Booking, LMMR.Entities.BookingKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.BookingKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_AgentUser key.
		///		FK_Booking_AgentUser Description: 
		/// </summary>
		/// <param name="_agencyUserId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		public TList<Booking> GetByAgencyUserId(System.Int64? _agencyUserId)
		{
			int count = -1;
			return GetByAgencyUserId(_agencyUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_AgentUser key.
		///		FK_Booking_AgentUser Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_agencyUserId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		/// <remarks></remarks>
		public TList<Booking> GetByAgencyUserId(TransactionManager transactionManager, System.Int64? _agencyUserId)
		{
			int count = -1;
			return GetByAgencyUserId(transactionManager, _agencyUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_AgentUser key.
		///		FK_Booking_AgentUser Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_agencyUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		public TList<Booking> GetByAgencyUserId(TransactionManager transactionManager, System.Int64? _agencyUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByAgencyUserId(transactionManager, _agencyUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_AgentUser key.
		///		fkBookingAgentUser Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_agencyUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		public TList<Booking> GetByAgencyUserId(System.Int64? _agencyUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByAgencyUserId(null, _agencyUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_AgentUser key.
		///		fkBookingAgentUser Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_agencyUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		public TList<Booking> GetByAgencyUserId(System.Int64? _agencyUserId, int start, int pageLength,out int count)
		{
			return GetByAgencyUserId(null, _agencyUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_AgentUser key.
		///		FK_Booking_AgentUser Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_agencyUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		public abstract TList<Booking> GetByAgencyUserId(TransactionManager transactionManager, System.Int64? _agencyUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_Currency key.
		///		FK_Booking_Currency Description: 
		/// </summary>
		/// <param name="_currencyId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		public TList<Booking> GetByCurrencyId(System.Int64? _currencyId)
		{
			int count = -1;
			return GetByCurrencyId(_currencyId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_Currency key.
		///		FK_Booking_Currency Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_currencyId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		/// <remarks></remarks>
		public TList<Booking> GetByCurrencyId(TransactionManager transactionManager, System.Int64? _currencyId)
		{
			int count = -1;
			return GetByCurrencyId(transactionManager, _currencyId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_Currency key.
		///		FK_Booking_Currency Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_currencyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		public TList<Booking> GetByCurrencyId(TransactionManager transactionManager, System.Int64? _currencyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCurrencyId(transactionManager, _currencyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_Currency key.
		///		fkBookingCurrency Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_currencyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		public TList<Booking> GetByCurrencyId(System.Int64? _currencyId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCurrencyId(null, _currencyId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_Currency key.
		///		fkBookingCurrency Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_currencyId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		public TList<Booking> GetByCurrencyId(System.Int64? _currencyId, int start, int pageLength,out int count)
		{
			return GetByCurrencyId(null, _currencyId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_Currency key.
		///		FK_Booking_Currency Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_currencyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		public abstract TList<Booking> GetByCurrencyId(TransactionManager transactionManager, System.Int64? _currencyId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_Hotel key.
		///		FK_Booking_Hotel Description: 
		/// </summary>
		/// <param name="_hotelId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		public TList<Booking> GetByHotelId(System.Int64 _hotelId)
		{
			int count = -1;
			return GetByHotelId(_hotelId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_Hotel key.
		///		FK_Booking_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		/// <remarks></remarks>
		public TList<Booking> GetByHotelId(TransactionManager transactionManager, System.Int64 _hotelId)
		{
			int count = -1;
			return GetByHotelId(transactionManager, _hotelId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_Hotel key.
		///		FK_Booking_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		public TList<Booking> GetByHotelId(TransactionManager transactionManager, System.Int64 _hotelId, int start, int pageLength)
		{
			int count = -1;
			return GetByHotelId(transactionManager, _hotelId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_Hotel key.
		///		fkBookingHotel Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_hotelId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		public TList<Booking> GetByHotelId(System.Int64 _hotelId, int start, int pageLength)
		{
			int count =  -1;
			return GetByHotelId(null, _hotelId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_Hotel key.
		///		fkBookingHotel Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_hotelId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		public TList<Booking> GetByHotelId(System.Int64 _hotelId, int start, int pageLength,out int count)
		{
			return GetByHotelId(null, _hotelId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_Hotel key.
		///		FK_Booking_Hotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		public abstract TList<Booking> GetByHotelId(TransactionManager transactionManager, System.Int64 _hotelId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_MeetingRoom key.
		///		FK_Booking_MeetingRoom Description: 
		/// </summary>
		/// <param name="_mainMeetingRoomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		public TList<Booking> GetByMainMeetingRoomId(System.Int64 _mainMeetingRoomId)
		{
			int count = -1;
			return GetByMainMeetingRoomId(_mainMeetingRoomId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_MeetingRoom key.
		///		FK_Booking_MeetingRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_mainMeetingRoomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		/// <remarks></remarks>
		public TList<Booking> GetByMainMeetingRoomId(TransactionManager transactionManager, System.Int64 _mainMeetingRoomId)
		{
			int count = -1;
			return GetByMainMeetingRoomId(transactionManager, _mainMeetingRoomId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_MeetingRoom key.
		///		FK_Booking_MeetingRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_mainMeetingRoomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		public TList<Booking> GetByMainMeetingRoomId(TransactionManager transactionManager, System.Int64 _mainMeetingRoomId, int start, int pageLength)
		{
			int count = -1;
			return GetByMainMeetingRoomId(transactionManager, _mainMeetingRoomId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_MeetingRoom key.
		///		fkBookingMeetingRoom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_mainMeetingRoomId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		public TList<Booking> GetByMainMeetingRoomId(System.Int64 _mainMeetingRoomId, int start, int pageLength)
		{
			int count =  -1;
			return GetByMainMeetingRoomId(null, _mainMeetingRoomId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_MeetingRoom key.
		///		fkBookingMeetingRoom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_mainMeetingRoomId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		public TList<Booking> GetByMainMeetingRoomId(System.Int64 _mainMeetingRoomId, int start, int pageLength,out int count)
		{
			return GetByMainMeetingRoomId(null, _mainMeetingRoomId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_MeetingRoom key.
		///		FK_Booking_MeetingRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_mainMeetingRoomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		public abstract TList<Booking> GetByMainMeetingRoomId(TransactionManager transactionManager, System.Int64 _mainMeetingRoomId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_Users key.
		///		FK_Booking_Users Description: 
		/// </summary>
		/// <param name="_creatorId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		public TList<Booking> GetByCreatorId(System.Int64 _creatorId)
		{
			int count = -1;
			return GetByCreatorId(_creatorId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_Users key.
		///		FK_Booking_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_creatorId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		/// <remarks></remarks>
		public TList<Booking> GetByCreatorId(TransactionManager transactionManager, System.Int64 _creatorId)
		{
			int count = -1;
			return GetByCreatorId(transactionManager, _creatorId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_Users key.
		///		FK_Booking_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_creatorId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		public TList<Booking> GetByCreatorId(TransactionManager transactionManager, System.Int64 _creatorId, int start, int pageLength)
		{
			int count = -1;
			return GetByCreatorId(transactionManager, _creatorId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_Users key.
		///		fkBookingUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_creatorId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		public TList<Booking> GetByCreatorId(System.Int64 _creatorId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCreatorId(null, _creatorId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_Users key.
		///		fkBookingUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_creatorId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		public TList<Booking> GetByCreatorId(System.Int64 _creatorId, int start, int pageLength,out int count)
		{
			return GetByCreatorId(null, _creatorId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Booking_Users key.
		///		FK_Booking_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_creatorId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.Booking objects.</returns>
		public abstract TList<Booking> GetByCreatorId(TransactionManager transactionManager, System.Int64 _creatorId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.Booking Get(TransactionManager transactionManager, LMMR.Entities.BookingKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Booking index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Booking"/> class.</returns>
		public LMMR.Entities.Booking GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Booking index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Booking"/> class.</returns>
		public LMMR.Entities.Booking GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Booking index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Booking"/> class.</returns>
		public LMMR.Entities.Booking GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Booking index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Booking"/> class.</returns>
		public LMMR.Entities.Booking GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Booking index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Booking"/> class.</returns>
		public LMMR.Entities.Booking GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Booking index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Booking"/> class.</returns>
		public abstract LMMR.Entities.Booking GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Booking&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Booking&gt;"/></returns>
		public static TList<Booking> Fill(IDataReader reader, TList<Booking> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.Booking c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Booking")
					.Append("|").Append((System.Int64)reader[((int)BookingColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Booking>(
					key.ToString(), // EntityTrackingKey
					"Booking",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.Booking();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)BookingColumn.Id - 1)];
					c.HotelId = (System.Int64)reader[((int)BookingColumn.HotelId - 1)];
					c.MainMeetingRoomId = (System.Int64)reader[((int)BookingColumn.MainMeetingRoomId - 1)];
					c.BookingDate = (reader.IsDBNull(((int)BookingColumn.BookingDate - 1)))?null:(System.DateTime?)reader[((int)BookingColumn.BookingDate - 1)];
					c.ArrivalDate = (reader.IsDBNull(((int)BookingColumn.ArrivalDate - 1)))?null:(System.DateTime?)reader[((int)BookingColumn.ArrivalDate - 1)];
					c.DepartureDate = (reader.IsDBNull(((int)BookingColumn.DepartureDate - 1)))?null:(System.DateTime?)reader[((int)BookingColumn.DepartureDate - 1)];
					c.IsBookedByAgencyUser = (reader.IsDBNull(((int)BookingColumn.IsBookedByAgencyUser - 1)))?null:(System.Boolean?)reader[((int)BookingColumn.IsBookedByAgencyUser - 1)];
					c.AgencyUserId = (reader.IsDBNull(((int)BookingColumn.AgencyUserId - 1)))?null:(System.Int64?)reader[((int)BookingColumn.AgencyUserId - 1)];
					c.AgencyClientId = (reader.IsDBNull(((int)BookingColumn.AgencyClientId - 1)))?null:(System.Int64?)reader[((int)BookingColumn.AgencyClientId - 1)];
					c.CreatorId = (System.Int64)reader[((int)BookingColumn.CreatorId - 1)];
					c.IsPackageSelected = (System.Boolean)reader[((int)BookingColumn.IsPackageSelected - 1)];
					c.Accomodation = (reader.IsDBNull(((int)BookingColumn.Accomodation - 1)))?null:(System.Int32?)reader[((int)BookingColumn.Accomodation - 1)];
					c.IsBedroom = (System.Boolean)reader[((int)BookingColumn.IsBedroom - 1)];
					c.SpecialRequest = (reader.IsDBNull(((int)BookingColumn.SpecialRequest - 1)))?null:(System.String)reader[((int)BookingColumn.SpecialRequest - 1)];
					c.IsCancled = (reader.IsDBNull(((int)BookingColumn.IsCancled - 1)))?null:(System.Boolean?)reader[((int)BookingColumn.IsCancled - 1)];
					c.IsComissionDone = (reader.IsDBNull(((int)BookingColumn.IsComissionDone - 1)))?null:(System.Boolean?)reader[((int)BookingColumn.IsComissionDone - 1)];
					c.ComissionSubmitDate = (reader.IsDBNull(((int)BookingColumn.ComissionSubmitDate - 1)))?null:(System.DateTime?)reader[((int)BookingColumn.ComissionSubmitDate - 1)];
					c.RevenueReason = (reader.IsDBNull(((int)BookingColumn.RevenueReason - 1)))?null:(System.String)reader[((int)BookingColumn.RevenueReason - 1)];
					c.RevenueAmount = (reader.IsDBNull(((int)BookingColumn.RevenueAmount - 1)))?null:(System.Decimal?)reader[((int)BookingColumn.RevenueAmount - 1)];
					c.BookType = (reader.IsDBNull(((int)BookingColumn.BookType - 1)))?null:(System.Int32?)reader[((int)BookingColumn.BookType - 1)];
					c.IsUserBookingProcessDone = (System.Boolean)reader[((int)BookingColumn.IsUserBookingProcessDone - 1)];
					c.RequestStatus = (reader.IsDBNull(((int)BookingColumn.RequestStatus - 1)))?null:(System.Int32?)reader[((int)BookingColumn.RequestStatus - 1)];
					c.IsFrozen = (System.Boolean)reader[((int)BookingColumn.IsFrozen - 1)];
					c.FrozenDate = (reader.IsDBNull(((int)BookingColumn.FrozenDate - 1)))?null:(System.DateTime?)reader[((int)BookingColumn.FrozenDate - 1)];
					c.FinalTotalPrice = (reader.IsDBNull(((int)BookingColumn.FinalTotalPrice - 1)))?null:(System.Decimal?)reader[((int)BookingColumn.FinalTotalPrice - 1)];
					c.BedRoomTotalPrice = (reader.IsDBNull(((int)BookingColumn.BedRoomTotalPrice - 1)))?null:(System.Decimal?)reader[((int)BookingColumn.BedRoomTotalPrice - 1)];
					c.MeetingRoomTotalPrice = (reader.IsDBNull(((int)BookingColumn.MeetingRoomTotalPrice - 1)))?null:(System.Decimal?)reader[((int)BookingColumn.MeetingRoomTotalPrice - 1)];
					c.Duration = (reader.IsDBNull(((int)BookingColumn.Duration - 1)))?null:(System.Int64?)reader[((int)BookingColumn.Duration - 1)];
					c.IsSurveyDone = (System.Boolean)reader[((int)BookingColumn.IsSurveyDone - 1)];
					c.SurveyDate = (reader.IsDBNull(((int)BookingColumn.SurveyDate - 1)))?null:(System.DateTime?)reader[((int)BookingColumn.SurveyDate - 1)];
					c.CurrencyId = (reader.IsDBNull(((int)BookingColumn.CurrencyId - 1)))?null:(System.Int64?)reader[((int)BookingColumn.CurrencyId - 1)];
					c.Channel = (reader.IsDBNull(((int)BookingColumn.Channel - 1)))?null:(System.String)reader[((int)BookingColumn.Channel - 1)];
					c.SupportingDocNetto = (reader.IsDBNull(((int)BookingColumn.SupportingDocNetto - 1)))?null:(System.String)reader[((int)BookingColumn.SupportingDocNetto - 1)];
					c.InvoiceId = (reader.IsDBNull(((int)BookingColumn.InvoiceId - 1)))?null:(System.Int64?)reader[((int)BookingColumn.InvoiceId - 1)];
					c.BookingXml = (reader.IsDBNull(((int)BookingColumn.BookingXml - 1)))?null:(string)reader[((int)BookingColumn.BookingXml - 1)];
					c.ConfirmRevenueAmount = (reader.IsDBNull(((int)BookingColumn.ConfirmRevenueAmount - 1)))?null:(System.Decimal?)reader[((int)BookingColumn.ConfirmRevenueAmount - 1)];
					c.ChannelBy = (reader.IsDBNull(((int)BookingColumn.ChannelBy - 1)))?null:(System.String)reader[((int)BookingColumn.ChannelBy - 1)];
					c.ChannelId = (reader.IsDBNull(((int)BookingColumn.ChannelId - 1)))?null:(System.String)reader[((int)BookingColumn.ChannelId - 1)];
					c.RequestCollectionId = (reader.IsDBNull(((int)BookingColumn.RequestCollectionId - 1)))?null:(System.String)reader[((int)BookingColumn.RequestCollectionId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Booking"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Booking"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.Booking entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)BookingColumn.Id - 1)];
			entity.HotelId = (System.Int64)reader[((int)BookingColumn.HotelId - 1)];
			entity.MainMeetingRoomId = (System.Int64)reader[((int)BookingColumn.MainMeetingRoomId - 1)];
			entity.BookingDate = (reader.IsDBNull(((int)BookingColumn.BookingDate - 1)))?null:(System.DateTime?)reader[((int)BookingColumn.BookingDate - 1)];
			entity.ArrivalDate = (reader.IsDBNull(((int)BookingColumn.ArrivalDate - 1)))?null:(System.DateTime?)reader[((int)BookingColumn.ArrivalDate - 1)];
			entity.DepartureDate = (reader.IsDBNull(((int)BookingColumn.DepartureDate - 1)))?null:(System.DateTime?)reader[((int)BookingColumn.DepartureDate - 1)];
			entity.IsBookedByAgencyUser = (reader.IsDBNull(((int)BookingColumn.IsBookedByAgencyUser - 1)))?null:(System.Boolean?)reader[((int)BookingColumn.IsBookedByAgencyUser - 1)];
			entity.AgencyUserId = (reader.IsDBNull(((int)BookingColumn.AgencyUserId - 1)))?null:(System.Int64?)reader[((int)BookingColumn.AgencyUserId - 1)];
			entity.AgencyClientId = (reader.IsDBNull(((int)BookingColumn.AgencyClientId - 1)))?null:(System.Int64?)reader[((int)BookingColumn.AgencyClientId - 1)];
			entity.CreatorId = (System.Int64)reader[((int)BookingColumn.CreatorId - 1)];
			entity.IsPackageSelected = (System.Boolean)reader[((int)BookingColumn.IsPackageSelected - 1)];
			entity.Accomodation = (reader.IsDBNull(((int)BookingColumn.Accomodation - 1)))?null:(System.Int32?)reader[((int)BookingColumn.Accomodation - 1)];
			entity.IsBedroom = (System.Boolean)reader[((int)BookingColumn.IsBedroom - 1)];
			entity.SpecialRequest = (reader.IsDBNull(((int)BookingColumn.SpecialRequest - 1)))?null:(System.String)reader[((int)BookingColumn.SpecialRequest - 1)];
			entity.IsCancled = (reader.IsDBNull(((int)BookingColumn.IsCancled - 1)))?null:(System.Boolean?)reader[((int)BookingColumn.IsCancled - 1)];
			entity.IsComissionDone = (reader.IsDBNull(((int)BookingColumn.IsComissionDone - 1)))?null:(System.Boolean?)reader[((int)BookingColumn.IsComissionDone - 1)];
			entity.ComissionSubmitDate = (reader.IsDBNull(((int)BookingColumn.ComissionSubmitDate - 1)))?null:(System.DateTime?)reader[((int)BookingColumn.ComissionSubmitDate - 1)];
			entity.RevenueReason = (reader.IsDBNull(((int)BookingColumn.RevenueReason - 1)))?null:(System.String)reader[((int)BookingColumn.RevenueReason - 1)];
			entity.RevenueAmount = (reader.IsDBNull(((int)BookingColumn.RevenueAmount - 1)))?null:(System.Decimal?)reader[((int)BookingColumn.RevenueAmount - 1)];
			entity.BookType = (reader.IsDBNull(((int)BookingColumn.BookType - 1)))?null:(System.Int32?)reader[((int)BookingColumn.BookType - 1)];
			entity.IsUserBookingProcessDone = (System.Boolean)reader[((int)BookingColumn.IsUserBookingProcessDone - 1)];
			entity.RequestStatus = (reader.IsDBNull(((int)BookingColumn.RequestStatus - 1)))?null:(System.Int32?)reader[((int)BookingColumn.RequestStatus - 1)];
			entity.IsFrozen = (System.Boolean)reader[((int)BookingColumn.IsFrozen - 1)];
			entity.FrozenDate = (reader.IsDBNull(((int)BookingColumn.FrozenDate - 1)))?null:(System.DateTime?)reader[((int)BookingColumn.FrozenDate - 1)];
			entity.FinalTotalPrice = (reader.IsDBNull(((int)BookingColumn.FinalTotalPrice - 1)))?null:(System.Decimal?)reader[((int)BookingColumn.FinalTotalPrice - 1)];
			entity.BedRoomTotalPrice = (reader.IsDBNull(((int)BookingColumn.BedRoomTotalPrice - 1)))?null:(System.Decimal?)reader[((int)BookingColumn.BedRoomTotalPrice - 1)];
			entity.MeetingRoomTotalPrice = (reader.IsDBNull(((int)BookingColumn.MeetingRoomTotalPrice - 1)))?null:(System.Decimal?)reader[((int)BookingColumn.MeetingRoomTotalPrice - 1)];
			entity.Duration = (reader.IsDBNull(((int)BookingColumn.Duration - 1)))?null:(System.Int64?)reader[((int)BookingColumn.Duration - 1)];
			entity.IsSurveyDone = (System.Boolean)reader[((int)BookingColumn.IsSurveyDone - 1)];
			entity.SurveyDate = (reader.IsDBNull(((int)BookingColumn.SurveyDate - 1)))?null:(System.DateTime?)reader[((int)BookingColumn.SurveyDate - 1)];
			entity.CurrencyId = (reader.IsDBNull(((int)BookingColumn.CurrencyId - 1)))?null:(System.Int64?)reader[((int)BookingColumn.CurrencyId - 1)];
			entity.Channel = (reader.IsDBNull(((int)BookingColumn.Channel - 1)))?null:(System.String)reader[((int)BookingColumn.Channel - 1)];
			entity.SupportingDocNetto = (reader.IsDBNull(((int)BookingColumn.SupportingDocNetto - 1)))?null:(System.String)reader[((int)BookingColumn.SupportingDocNetto - 1)];
			entity.InvoiceId = (reader.IsDBNull(((int)BookingColumn.InvoiceId - 1)))?null:(System.Int64?)reader[((int)BookingColumn.InvoiceId - 1)];
			entity.BookingXml = (reader.IsDBNull(((int)BookingColumn.BookingXml - 1)))?null:(string)reader[((int)BookingColumn.BookingXml - 1)];
			entity.ConfirmRevenueAmount = (reader.IsDBNull(((int)BookingColumn.ConfirmRevenueAmount - 1)))?null:(System.Decimal?)reader[((int)BookingColumn.ConfirmRevenueAmount - 1)];
			entity.ChannelBy = (reader.IsDBNull(((int)BookingColumn.ChannelBy - 1)))?null:(System.String)reader[((int)BookingColumn.ChannelBy - 1)];
			entity.ChannelId = (reader.IsDBNull(((int)BookingColumn.ChannelId - 1)))?null:(System.String)reader[((int)BookingColumn.ChannelId - 1)];
			entity.RequestCollectionId = (reader.IsDBNull(((int)BookingColumn.RequestCollectionId - 1)))?null:(System.String)reader[((int)BookingColumn.RequestCollectionId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Booking"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Booking"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.Booking entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.HotelId = (System.Int64)dataRow["HotelId"];
			entity.MainMeetingRoomId = (System.Int64)dataRow["MainMeetingRoomId"];
			entity.BookingDate = Convert.IsDBNull(dataRow["BookingDate"]) ? null : (System.DateTime?)dataRow["BookingDate"];
			entity.ArrivalDate = Convert.IsDBNull(dataRow["ArrivalDate"]) ? null : (System.DateTime?)dataRow["ArrivalDate"];
			entity.DepartureDate = Convert.IsDBNull(dataRow["DepartureDate"]) ? null : (System.DateTime?)dataRow["DepartureDate"];
			entity.IsBookedByAgencyUser = Convert.IsDBNull(dataRow["IsBookedByAgencyUser"]) ? null : (System.Boolean?)dataRow["IsBookedByAgencyUser"];
			entity.AgencyUserId = Convert.IsDBNull(dataRow["AgencyUserId"]) ? null : (System.Int64?)dataRow["AgencyUserId"];
			entity.AgencyClientId = Convert.IsDBNull(dataRow["AgencyClientId"]) ? null : (System.Int64?)dataRow["AgencyClientId"];
			entity.CreatorId = (System.Int64)dataRow["CreatorId"];
			entity.IsPackageSelected = (System.Boolean)dataRow["IsPackageSelected"];
			entity.Accomodation = Convert.IsDBNull(dataRow["Accomodation"]) ? null : (System.Int32?)dataRow["Accomodation"];
			entity.IsBedroom = (System.Boolean)dataRow["IsBedroom"];
			entity.SpecialRequest = Convert.IsDBNull(dataRow["SpecialRequest"]) ? null : (System.String)dataRow["SpecialRequest"];
			entity.IsCancled = Convert.IsDBNull(dataRow["IsCancled"]) ? null : (System.Boolean?)dataRow["IsCancled"];
			entity.IsComissionDone = Convert.IsDBNull(dataRow["IsComissionDone"]) ? null : (System.Boolean?)dataRow["IsComissionDone"];
			entity.ComissionSubmitDate = Convert.IsDBNull(dataRow["ComissionSubmitDate"]) ? null : (System.DateTime?)dataRow["ComissionSubmitDate"];
			entity.RevenueReason = Convert.IsDBNull(dataRow["RevenueReason"]) ? null : (System.String)dataRow["RevenueReason"];
			entity.RevenueAmount = Convert.IsDBNull(dataRow["RevenueAmount"]) ? null : (System.Decimal?)dataRow["RevenueAmount"];
			entity.BookType = Convert.IsDBNull(dataRow["BookType"]) ? null : (System.Int32?)dataRow["BookType"];
			entity.IsUserBookingProcessDone = (System.Boolean)dataRow["IsUserBookingProcessDone"];
			entity.RequestStatus = Convert.IsDBNull(dataRow["RequestStatus"]) ? null : (System.Int32?)dataRow["RequestStatus"];
			entity.IsFrozen = (System.Boolean)dataRow["IsFrozen"];
			entity.FrozenDate = Convert.IsDBNull(dataRow["FrozenDate"]) ? null : (System.DateTime?)dataRow["FrozenDate"];
			entity.FinalTotalPrice = Convert.IsDBNull(dataRow["FinalTotalPrice"]) ? null : (System.Decimal?)dataRow["FinalTotalPrice"];
			entity.BedRoomTotalPrice = Convert.IsDBNull(dataRow["BedRoomTotalPrice"]) ? null : (System.Decimal?)dataRow["BedRoomTotalPrice"];
			entity.MeetingRoomTotalPrice = Convert.IsDBNull(dataRow["MeetingRoomTotalPrice"]) ? null : (System.Decimal?)dataRow["MeetingRoomTotalPrice"];
			entity.Duration = Convert.IsDBNull(dataRow["Duration"]) ? null : (System.Int64?)dataRow["Duration"];
			entity.IsSurveyDone = (System.Boolean)dataRow["IsSurveyDone"];
			entity.SurveyDate = Convert.IsDBNull(dataRow["SurveyDate"]) ? null : (System.DateTime?)dataRow["SurveyDate"];
			entity.CurrencyId = Convert.IsDBNull(dataRow["CurrencyID"]) ? null : (System.Int64?)dataRow["CurrencyID"];
			entity.Channel = Convert.IsDBNull(dataRow["Channel"]) ? null : (System.String)dataRow["Channel"];
			entity.SupportingDocNetto = Convert.IsDBNull(dataRow["SupportingDocNetto"]) ? null : (System.String)dataRow["SupportingDocNetto"];
			entity.InvoiceId = Convert.IsDBNull(dataRow["InvoiceId"]) ? null : (System.Int64?)dataRow["InvoiceId"];
			entity.BookingXml = Convert.IsDBNull(dataRow["BookingXML"]) ? null : (string)dataRow["BookingXML"];
			entity.ConfirmRevenueAmount = Convert.IsDBNull(dataRow["ConfirmRevenueAmount"]) ? null : (System.Decimal?)dataRow["ConfirmRevenueAmount"];
			entity.ChannelBy = Convert.IsDBNull(dataRow["ChannelBy"]) ? null : (System.String)dataRow["ChannelBy"];
			entity.ChannelId = Convert.IsDBNull(dataRow["ChannelID"]) ? null : (System.String)dataRow["ChannelID"];
			entity.RequestCollectionId = Convert.IsDBNull(dataRow["RequestCollectionID"]) ? null : (System.String)dataRow["RequestCollectionID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Booking"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.Booking Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.Booking entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region AgencyUserIdSource	
			if (CanDeepLoad(entity, "Users|AgencyUserIdSource", deepLoadType, innerList) 
				&& entity.AgencyUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.AgencyUserId ?? (long)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AgencyUserIdSource = tmpEntity;
				else
					entity.AgencyUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.AgencyUserId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AgencyUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AgencyUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.AgencyUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AgencyUserIdSource

			#region CurrencyIdSource	
			if (CanDeepLoad(entity, "Currency|CurrencyIdSource", deepLoadType, innerList) 
				&& entity.CurrencyIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.CurrencyId ?? (long)0);
				Currency tmpEntity = EntityManager.LocateEntity<Currency>(EntityLocator.ConstructKeyFromPkItems(typeof(Currency), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CurrencyIdSource = tmpEntity;
				else
					entity.CurrencyIdSource = DataRepository.CurrencyProvider.GetById(transactionManager, (entity.CurrencyId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CurrencyIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CurrencyIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CurrencyProvider.DeepLoad(transactionManager, entity.CurrencyIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CurrencyIdSource

			#region HotelIdSource	
			if (CanDeepLoad(entity, "Hotel|HotelIdSource", deepLoadType, innerList) 
				&& entity.HotelIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.HotelId;
				Hotel tmpEntity = EntityManager.LocateEntity<Hotel>(EntityLocator.ConstructKeyFromPkItems(typeof(Hotel), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.HotelIdSource = tmpEntity;
				else
					entity.HotelIdSource = DataRepository.HotelProvider.GetById(transactionManager, entity.HotelId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HotelIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.HotelIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.HotelProvider.DeepLoad(transactionManager, entity.HotelIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion HotelIdSource

			#region MainMeetingRoomIdSource	
			if (CanDeepLoad(entity, "MeetingRoom|MainMeetingRoomIdSource", deepLoadType, innerList) 
				&& entity.MainMeetingRoomIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.MainMeetingRoomId;
				MeetingRoom tmpEntity = EntityManager.LocateEntity<MeetingRoom>(EntityLocator.ConstructKeyFromPkItems(typeof(MeetingRoom), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.MainMeetingRoomIdSource = tmpEntity;
				else
					entity.MainMeetingRoomIdSource = DataRepository.MeetingRoomProvider.GetById(transactionManager, entity.MainMeetingRoomId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MainMeetingRoomIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.MainMeetingRoomIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.MeetingRoomProvider.DeepLoad(transactionManager, entity.MainMeetingRoomIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion MainMeetingRoomIdSource

			#region CreatorIdSource	
			if (CanDeepLoad(entity, "Users|CreatorIdSource", deepLoadType, innerList) 
				&& entity.CreatorIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CreatorId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CreatorIdSource = tmpEntity;
				else
					entity.CreatorIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.CreatorId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CreatorIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CreatorIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.CreatorIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CreatorIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region TransferCollectionGetByOldBookingId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Transfer>|TransferCollectionGetByOldBookingId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TransferCollectionGetByOldBookingId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.TransferCollectionGetByOldBookingId = DataRepository.TransferProvider.GetByOldBookingId(transactionManager, entity.Id);

				if (deep && entity.TransferCollectionGetByOldBookingId.Count > 0)
				{
					deepHandles.Add("TransferCollectionGetByOldBookingId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Transfer>) DataRepository.TransferProvider.DeepLoad,
						new object[] { transactionManager, entity.TransferCollectionGetByOldBookingId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region BookedMeetingRoomCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<BookedMeetingRoom>|BookedMeetingRoomCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BookedMeetingRoomCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BookedMeetingRoomCollection = DataRepository.BookedMeetingRoomProvider.GetByBookingId(transactionManager, entity.Id);

				if (deep && entity.BookedMeetingRoomCollection.Count > 0)
				{
					deepHandles.Add("BookedMeetingRoomCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<BookedMeetingRoom>) DataRepository.BookedMeetingRoomProvider.DeepLoad,
						new object[] { transactionManager, entity.BookedMeetingRoomCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region BuildPackageConfigureCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<BuildPackageConfigure>|BuildPackageConfigureCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BuildPackageConfigureCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BuildPackageConfigureCollection = DataRepository.BuildPackageConfigureProvider.GetByBookingId(transactionManager, entity.Id);

				if (deep && entity.BuildPackageConfigureCollection.Count > 0)
				{
					deepHandles.Add("BuildPackageConfigureCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<BuildPackageConfigure>) DataRepository.BuildPackageConfigureProvider.DeepLoad,
						new object[] { transactionManager, entity.BuildPackageConfigureCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ServeyResponseCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ServeyResponse>|ServeyResponseCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ServeyResponseCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ServeyResponseCollection = DataRepository.ServeyResponseProvider.GetByBookingId(transactionManager, entity.Id);

				if (deep && entity.ServeyResponseCollection.Count > 0)
				{
					deepHandles.Add("ServeyResponseCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ServeyResponse>) DataRepository.ServeyResponseProvider.DeepLoad,
						new object[] { transactionManager, entity.ServeyResponseCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region BuildMeetingConfigureCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<BuildMeetingConfigure>|BuildMeetingConfigureCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BuildMeetingConfigureCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BuildMeetingConfigureCollection = DataRepository.BuildMeetingConfigureProvider.GetByBookingId(transactionManager, entity.Id);

				if (deep && entity.BuildMeetingConfigureCollection.Count > 0)
				{
					deepHandles.Add("BuildMeetingConfigureCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<BuildMeetingConfigure>) DataRepository.BuildMeetingConfigureProvider.DeepLoad,
						new object[] { transactionManager, entity.BuildMeetingConfigureCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region BookingLogsCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<BookingLogs>|BookingLogsCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BookingLogsCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BookingLogsCollection = DataRepository.BookingLogsProvider.GetByBookingId(transactionManager, entity.Id);

				if (deep && entity.BookingLogsCollection.Count > 0)
				{
					deepHandles.Add("BookingLogsCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<BookingLogs>) DataRepository.BookingLogsProvider.DeepLoad,
						new object[] { transactionManager, entity.BookingLogsCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region BookedBedRoomCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<BookedBedRoom>|BookedBedRoomCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BookedBedRoomCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BookedBedRoomCollection = DataRepository.BookedBedRoomProvider.GetByBookingId(transactionManager, entity.Id);

				if (deep && entity.BookedBedRoomCollection.Count > 0)
				{
					deepHandles.Add("BookedBedRoomCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<BookedBedRoom>) DataRepository.BookedBedRoomProvider.DeepLoad,
						new object[] { transactionManager, entity.BookedBedRoomCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region InvoiceCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Invoice>|InvoiceCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'InvoiceCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.InvoiceCollection = DataRepository.InvoiceProvider.GetByBookedId(transactionManager, entity.Id);

				if (deep && entity.InvoiceCollection.Count > 0)
				{
					deepHandles.Add("InvoiceCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Invoice>) DataRepository.InvoiceProvider.DeepLoad,
						new object[] { transactionManager, entity.InvoiceCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region TransferCollectionGetByNewBookingId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Transfer>|TransferCollectionGetByNewBookingId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TransferCollectionGetByNewBookingId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.TransferCollectionGetByNewBookingId = DataRepository.TransferProvider.GetByNewBookingId(transactionManager, entity.Id);

				if (deep && entity.TransferCollectionGetByNewBookingId.Count > 0)
				{
					deepHandles.Add("TransferCollectionGetByNewBookingId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Transfer>) DataRepository.TransferProvider.DeepLoad,
						new object[] { transactionManager, entity.TransferCollectionGetByNewBookingId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.Booking object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.Booking instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.Booking Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.Booking entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region AgencyUserIdSource
			if (CanDeepSave(entity, "Users|AgencyUserIdSource", deepSaveType, innerList) 
				&& entity.AgencyUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.AgencyUserIdSource);
				entity.AgencyUserId = entity.AgencyUserIdSource.UserId;
			}
			#endregion 
			
			#region CurrencyIdSource
			if (CanDeepSave(entity, "Currency|CurrencyIdSource", deepSaveType, innerList) 
				&& entity.CurrencyIdSource != null)
			{
				DataRepository.CurrencyProvider.Save(transactionManager, entity.CurrencyIdSource);
				entity.CurrencyId = entity.CurrencyIdSource.Id;
			}
			#endregion 
			
			#region HotelIdSource
			if (CanDeepSave(entity, "Hotel|HotelIdSource", deepSaveType, innerList) 
				&& entity.HotelIdSource != null)
			{
				DataRepository.HotelProvider.Save(transactionManager, entity.HotelIdSource);
				entity.HotelId = entity.HotelIdSource.Id;
			}
			#endregion 
			
			#region MainMeetingRoomIdSource
			if (CanDeepSave(entity, "MeetingRoom|MainMeetingRoomIdSource", deepSaveType, innerList) 
				&& entity.MainMeetingRoomIdSource != null)
			{
				DataRepository.MeetingRoomProvider.Save(transactionManager, entity.MainMeetingRoomIdSource);
				entity.MainMeetingRoomId = entity.MainMeetingRoomIdSource.Id;
			}
			#endregion 
			
			#region CreatorIdSource
			if (CanDeepSave(entity, "Users|CreatorIdSource", deepSaveType, innerList) 
				&& entity.CreatorIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.CreatorIdSource);
				entity.CreatorId = entity.CreatorIdSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<Transfer>
				if (CanDeepSave(entity.TransferCollectionGetByOldBookingId, "List<Transfer>|TransferCollectionGetByOldBookingId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Transfer child in entity.TransferCollectionGetByOldBookingId)
					{
						if(child.OldBookingIdSource != null)
						{
							child.OldBookingId = child.OldBookingIdSource.Id;
						}
						else
						{
							child.OldBookingId = entity.Id;
						}

					}

					if (entity.TransferCollectionGetByOldBookingId.Count > 0 || entity.TransferCollectionGetByOldBookingId.DeletedItems.Count > 0)
					{
						//DataRepository.TransferProvider.Save(transactionManager, entity.TransferCollectionGetByOldBookingId);
						
						deepHandles.Add("TransferCollectionGetByOldBookingId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Transfer >) DataRepository.TransferProvider.DeepSave,
							new object[] { transactionManager, entity.TransferCollectionGetByOldBookingId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<BookedMeetingRoom>
				if (CanDeepSave(entity.BookedMeetingRoomCollection, "List<BookedMeetingRoom>|BookedMeetingRoomCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(BookedMeetingRoom child in entity.BookedMeetingRoomCollection)
					{
						if(child.BookingIdSource != null)
						{
							child.BookingId = child.BookingIdSource.Id;
						}
						else
						{
							child.BookingId = entity.Id;
						}

					}

					if (entity.BookedMeetingRoomCollection.Count > 0 || entity.BookedMeetingRoomCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BookedMeetingRoomProvider.Save(transactionManager, entity.BookedMeetingRoomCollection);
						
						deepHandles.Add("BookedMeetingRoomCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< BookedMeetingRoom >) DataRepository.BookedMeetingRoomProvider.DeepSave,
							new object[] { transactionManager, entity.BookedMeetingRoomCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<BuildPackageConfigure>
				if (CanDeepSave(entity.BuildPackageConfigureCollection, "List<BuildPackageConfigure>|BuildPackageConfigureCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(BuildPackageConfigure child in entity.BuildPackageConfigureCollection)
					{
						if(child.BookingIdSource != null)
						{
							child.BookingId = child.BookingIdSource.Id;
						}
						else
						{
							child.BookingId = entity.Id;
						}

					}

					if (entity.BuildPackageConfigureCollection.Count > 0 || entity.BuildPackageConfigureCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BuildPackageConfigureProvider.Save(transactionManager, entity.BuildPackageConfigureCollection);
						
						deepHandles.Add("BuildPackageConfigureCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< BuildPackageConfigure >) DataRepository.BuildPackageConfigureProvider.DeepSave,
							new object[] { transactionManager, entity.BuildPackageConfigureCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ServeyResponse>
				if (CanDeepSave(entity.ServeyResponseCollection, "List<ServeyResponse>|ServeyResponseCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ServeyResponse child in entity.ServeyResponseCollection)
					{
						if(child.BookingIdSource != null)
						{
							child.BookingId = child.BookingIdSource.Id;
						}
						else
						{
							child.BookingId = entity.Id;
						}

					}

					if (entity.ServeyResponseCollection.Count > 0 || entity.ServeyResponseCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ServeyResponseProvider.Save(transactionManager, entity.ServeyResponseCollection);
						
						deepHandles.Add("ServeyResponseCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ServeyResponse >) DataRepository.ServeyResponseProvider.DeepSave,
							new object[] { transactionManager, entity.ServeyResponseCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<BuildMeetingConfigure>
				if (CanDeepSave(entity.BuildMeetingConfigureCollection, "List<BuildMeetingConfigure>|BuildMeetingConfigureCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(BuildMeetingConfigure child in entity.BuildMeetingConfigureCollection)
					{
						if(child.BookingIdSource != null)
						{
							child.BookingId = child.BookingIdSource.Id;
						}
						else
						{
							child.BookingId = entity.Id;
						}

					}

					if (entity.BuildMeetingConfigureCollection.Count > 0 || entity.BuildMeetingConfigureCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BuildMeetingConfigureProvider.Save(transactionManager, entity.BuildMeetingConfigureCollection);
						
						deepHandles.Add("BuildMeetingConfigureCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< BuildMeetingConfigure >) DataRepository.BuildMeetingConfigureProvider.DeepSave,
							new object[] { transactionManager, entity.BuildMeetingConfigureCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<BookingLogs>
				if (CanDeepSave(entity.BookingLogsCollection, "List<BookingLogs>|BookingLogsCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(BookingLogs child in entity.BookingLogsCollection)
					{
						if(child.BookingIdSource != null)
						{
							child.BookingId = child.BookingIdSource.Id;
						}
						else
						{
							child.BookingId = entity.Id;
						}

					}

					if (entity.BookingLogsCollection.Count > 0 || entity.BookingLogsCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BookingLogsProvider.Save(transactionManager, entity.BookingLogsCollection);
						
						deepHandles.Add("BookingLogsCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< BookingLogs >) DataRepository.BookingLogsProvider.DeepSave,
							new object[] { transactionManager, entity.BookingLogsCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<BookedBedRoom>
				if (CanDeepSave(entity.BookedBedRoomCollection, "List<BookedBedRoom>|BookedBedRoomCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(BookedBedRoom child in entity.BookedBedRoomCollection)
					{
						if(child.BookingIdSource != null)
						{
							child.BookingId = child.BookingIdSource.Id;
						}
						else
						{
							child.BookingId = entity.Id;
						}

					}

					if (entity.BookedBedRoomCollection.Count > 0 || entity.BookedBedRoomCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BookedBedRoomProvider.Save(transactionManager, entity.BookedBedRoomCollection);
						
						deepHandles.Add("BookedBedRoomCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< BookedBedRoom >) DataRepository.BookedBedRoomProvider.DeepSave,
							new object[] { transactionManager, entity.BookedBedRoomCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Invoice>
				if (CanDeepSave(entity.InvoiceCollection, "List<Invoice>|InvoiceCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Invoice child in entity.InvoiceCollection)
					{
						if(child.BookedIdSource != null)
						{
							child.BookedId = child.BookedIdSource.Id;
						}
						else
						{
							child.BookedId = entity.Id;
						}

					}

					if (entity.InvoiceCollection.Count > 0 || entity.InvoiceCollection.DeletedItems.Count > 0)
					{
						//DataRepository.InvoiceProvider.Save(transactionManager, entity.InvoiceCollection);
						
						deepHandles.Add("InvoiceCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Invoice >) DataRepository.InvoiceProvider.DeepSave,
							new object[] { transactionManager, entity.InvoiceCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Transfer>
				if (CanDeepSave(entity.TransferCollectionGetByNewBookingId, "List<Transfer>|TransferCollectionGetByNewBookingId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Transfer child in entity.TransferCollectionGetByNewBookingId)
					{
						if(child.NewBookingIdSource != null)
						{
							child.NewBookingId = child.NewBookingIdSource.Id;
						}
						else
						{
							child.NewBookingId = entity.Id;
						}

					}

					if (entity.TransferCollectionGetByNewBookingId.Count > 0 || entity.TransferCollectionGetByNewBookingId.DeletedItems.Count > 0)
					{
						//DataRepository.TransferProvider.Save(transactionManager, entity.TransferCollectionGetByNewBookingId);
						
						deepHandles.Add("TransferCollectionGetByNewBookingId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Transfer >) DataRepository.TransferProvider.DeepSave,
							new object[] { transactionManager, entity.TransferCollectionGetByNewBookingId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region BookingChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.Booking</c>
	///</summary>
	public enum BookingChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Users</c> at AgencyUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
			
		///<summary>
		/// Composite Property for <c>Currency</c> at CurrencyIdSource
		///</summary>
		[ChildEntityType(typeof(Currency))]
		Currency,
			
		///<summary>
		/// Composite Property for <c>Hotel</c> at HotelIdSource
		///</summary>
		[ChildEntityType(typeof(Hotel))]
		Hotel,
			
		///<summary>
		/// Composite Property for <c>MeetingRoom</c> at MainMeetingRoomIdSource
		///</summary>
		[ChildEntityType(typeof(MeetingRoom))]
		MeetingRoom,
	
		///<summary>
		/// Collection of <c>Booking</c> as OneToMany for TransferCollection
		///</summary>
		[ChildEntityType(typeof(TList<Transfer>))]
		TransferCollectionGetByOldBookingId,

		///<summary>
		/// Collection of <c>Booking</c> as OneToMany for BookedMeetingRoomCollection
		///</summary>
		[ChildEntityType(typeof(TList<BookedMeetingRoom>))]
		BookedMeetingRoomCollection,

		///<summary>
		/// Collection of <c>Booking</c> as OneToMany for BuildPackageConfigureCollection
		///</summary>
		[ChildEntityType(typeof(TList<BuildPackageConfigure>))]
		BuildPackageConfigureCollection,

		///<summary>
		/// Collection of <c>Booking</c> as OneToMany for ServeyResponseCollection
		///</summary>
		[ChildEntityType(typeof(TList<ServeyResponse>))]
		ServeyResponseCollection,

		///<summary>
		/// Collection of <c>Booking</c> as OneToMany for BuildMeetingConfigureCollection
		///</summary>
		[ChildEntityType(typeof(TList<BuildMeetingConfigure>))]
		BuildMeetingConfigureCollection,

		///<summary>
		/// Collection of <c>Booking</c> as OneToMany for BookingLogsCollection
		///</summary>
		[ChildEntityType(typeof(TList<BookingLogs>))]
		BookingLogsCollection,

		///<summary>
		/// Collection of <c>Booking</c> as OneToMany for BookedBedRoomCollection
		///</summary>
		[ChildEntityType(typeof(TList<BookedBedRoom>))]
		BookedBedRoomCollection,

		///<summary>
		/// Collection of <c>Booking</c> as OneToMany for InvoiceCollection
		///</summary>
		[ChildEntityType(typeof(TList<Invoice>))]
		InvoiceCollection,

		///<summary>
		/// Collection of <c>Booking</c> as OneToMany for TransferCollection
		///</summary>
		[ChildEntityType(typeof(TList<Transfer>))]
		TransferCollectionGetByNewBookingId,
	}
	
	#endregion BookingChildEntityTypes
	
	#region BookingFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;BookingColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Booking"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BookingFilterBuilder : SqlFilterBuilder<BookingColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BookingFilterBuilder class.
		/// </summary>
		public BookingFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BookingFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BookingFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BookingFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BookingFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BookingFilterBuilder
	
	#region BookingParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;BookingColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Booking"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BookingParameterBuilder : ParameterizedSqlFilterBuilder<BookingColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BookingParameterBuilder class.
		/// </summary>
		public BookingParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BookingParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BookingParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BookingParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BookingParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BookingParameterBuilder
	
	#region BookingSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;BookingColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Booking"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class BookingSortBuilder : SqlSortBuilder<BookingColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BookingSqlSortBuilder class.
		/// </summary>
		public BookingSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion BookingSortBuilder
	
} // end namespace
