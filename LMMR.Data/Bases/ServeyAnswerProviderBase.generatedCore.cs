﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ServeyAnswerProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ServeyAnswerProviderBaseCore : EntityProviderBase<LMMR.Entities.ServeyAnswer, LMMR.Entities.ServeyAnswerKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.ServeyAnswerKey key)
		{
			return Delete(transactionManager, key.AnswerId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_answerId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _answerId)
		{
			return Delete(null, _answerId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_answerId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _answerId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ServeyAnswer_ServeyAnswerDescription key.
		///		FK_ServeyAnswer_ServeyAnswerDescription Description: 
		/// </summary>
		/// <param name="_questionId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.ServeyAnswer objects.</returns>
		public TList<ServeyAnswer> GetByQuestionId(System.Int32 _questionId)
		{
			int count = -1;
			return GetByQuestionId(_questionId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ServeyAnswer_ServeyAnswerDescription key.
		///		FK_ServeyAnswer_ServeyAnswerDescription Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.ServeyAnswer objects.</returns>
		/// <remarks></remarks>
		public TList<ServeyAnswer> GetByQuestionId(TransactionManager transactionManager, System.Int32 _questionId)
		{
			int count = -1;
			return GetByQuestionId(transactionManager, _questionId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ServeyAnswer_ServeyAnswerDescription key.
		///		FK_ServeyAnswer_ServeyAnswerDescription Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.ServeyAnswer objects.</returns>
		public TList<ServeyAnswer> GetByQuestionId(TransactionManager transactionManager, System.Int32 _questionId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionId(transactionManager, _questionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ServeyAnswer_ServeyAnswerDescription key.
		///		fkServeyAnswerServeyAnswerDescription Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.ServeyAnswer objects.</returns>
		public TList<ServeyAnswer> GetByQuestionId(System.Int32 _questionId, int start, int pageLength)
		{
			int count =  -1;
			return GetByQuestionId(null, _questionId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ServeyAnswer_ServeyAnswerDescription key.
		///		fkServeyAnswerServeyAnswerDescription Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.ServeyAnswer objects.</returns>
		public TList<ServeyAnswer> GetByQuestionId(System.Int32 _questionId, int start, int pageLength,out int count)
		{
			return GetByQuestionId(null, _questionId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ServeyAnswer_ServeyAnswerDescription key.
		///		FK_ServeyAnswer_ServeyAnswerDescription Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.ServeyAnswer objects.</returns>
		public abstract TList<ServeyAnswer> GetByQuestionId(TransactionManager transactionManager, System.Int32 _questionId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.ServeyAnswer Get(TransactionManager transactionManager, LMMR.Entities.ServeyAnswerKey key, int start, int pageLength)
		{
			return GetByAnswerId(transactionManager, key.AnswerId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ServeyAnswer index.
		/// </summary>
		/// <param name="_answerId"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.ServeyAnswer"/> class.</returns>
		public LMMR.Entities.ServeyAnswer GetByAnswerId(System.Int32 _answerId)
		{
			int count = -1;
			return GetByAnswerId(null,_answerId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ServeyAnswer index.
		/// </summary>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.ServeyAnswer"/> class.</returns>
		public LMMR.Entities.ServeyAnswer GetByAnswerId(System.Int32 _answerId, int start, int pageLength)
		{
			int count = -1;
			return GetByAnswerId(null, _answerId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ServeyAnswer index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_answerId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.ServeyAnswer"/> class.</returns>
		public LMMR.Entities.ServeyAnswer GetByAnswerId(TransactionManager transactionManager, System.Int32 _answerId)
		{
			int count = -1;
			return GetByAnswerId(transactionManager, _answerId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ServeyAnswer index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.ServeyAnswer"/> class.</returns>
		public LMMR.Entities.ServeyAnswer GetByAnswerId(TransactionManager transactionManager, System.Int32 _answerId, int start, int pageLength)
		{
			int count = -1;
			return GetByAnswerId(transactionManager, _answerId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ServeyAnswer index.
		/// </summary>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.ServeyAnswer"/> class.</returns>
		public LMMR.Entities.ServeyAnswer GetByAnswerId(System.Int32 _answerId, int start, int pageLength, out int count)
		{
			return GetByAnswerId(null, _answerId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ServeyAnswer index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.ServeyAnswer"/> class.</returns>
		public abstract LMMR.Entities.ServeyAnswer GetByAnswerId(TransactionManager transactionManager, System.Int32 _answerId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ServeyAnswer&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ServeyAnswer&gt;"/></returns>
		public static TList<ServeyAnswer> Fill(IDataReader reader, TList<ServeyAnswer> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.ServeyAnswer c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ServeyAnswer")
					.Append("|").Append((System.Int32)reader[((int)ServeyAnswerColumn.AnswerId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ServeyAnswer>(
					key.ToString(), // EntityTrackingKey
					"ServeyAnswer",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.ServeyAnswer();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AnswerId = (System.Int32)reader[((int)ServeyAnswerColumn.AnswerId - 1)];
					c.OriginalAnswerId = c.AnswerId;
					c.Answer = (reader.IsDBNull(((int)ServeyAnswerColumn.Answer - 1)))?null:(System.String)reader[((int)ServeyAnswerColumn.Answer - 1)];
					c.AnswerType = (reader.IsDBNull(((int)ServeyAnswerColumn.AnswerType - 1)))?null:(System.Boolean?)reader[((int)ServeyAnswerColumn.AnswerType - 1)];
					c.QuestionId = (System.Int32)reader[((int)ServeyAnswerColumn.QuestionId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.ServeyAnswer"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.ServeyAnswer"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.ServeyAnswer entity)
		{
			if (!reader.Read()) return;
			
			entity.AnswerId = (System.Int32)reader[((int)ServeyAnswerColumn.AnswerId - 1)];
			entity.OriginalAnswerId = (System.Int32)reader["AnswerID"];
			entity.Answer = (reader.IsDBNull(((int)ServeyAnswerColumn.Answer - 1)))?null:(System.String)reader[((int)ServeyAnswerColumn.Answer - 1)];
			entity.AnswerType = (reader.IsDBNull(((int)ServeyAnswerColumn.AnswerType - 1)))?null:(System.Boolean?)reader[((int)ServeyAnswerColumn.AnswerType - 1)];
			entity.QuestionId = (System.Int32)reader[((int)ServeyAnswerColumn.QuestionId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.ServeyAnswer"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.ServeyAnswer"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.ServeyAnswer entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AnswerId = (System.Int32)dataRow["AnswerID"];
			entity.OriginalAnswerId = (System.Int32)dataRow["AnswerID"];
			entity.Answer = Convert.IsDBNull(dataRow["Answer"]) ? null : (System.String)dataRow["Answer"];
			entity.AnswerType = Convert.IsDBNull(dataRow["AnswerType"]) ? null : (System.Boolean?)dataRow["AnswerType"];
			entity.QuestionId = (System.Int32)dataRow["QuestionID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.ServeyAnswer"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.ServeyAnswer Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.ServeyAnswer entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region QuestionIdSource	
			if (CanDeepLoad(entity, "ServeyQuestion|QuestionIdSource", deepLoadType, innerList) 
				&& entity.QuestionIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.QuestionId;
				ServeyQuestion tmpEntity = EntityManager.LocateEntity<ServeyQuestion>(EntityLocator.ConstructKeyFromPkItems(typeof(ServeyQuestion), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.QuestionIdSource = tmpEntity;
				else
					entity.QuestionIdSource = DataRepository.ServeyQuestionProvider.GetByQuestionId(transactionManager, entity.QuestionId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.QuestionIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ServeyQuestionProvider.DeepLoad(transactionManager, entity.QuestionIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion QuestionIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByAnswerId methods when available
			
			#region ServeyresultCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Serveyresult>|ServeyresultCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ServeyresultCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ServeyresultCollection = DataRepository.ServeyresultProvider.GetByAnswerId(transactionManager, entity.AnswerId);

				if (deep && entity.ServeyresultCollection.Count > 0)
				{
					deepHandles.Add("ServeyresultCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Serveyresult>) DataRepository.ServeyresultProvider.DeepLoad,
						new object[] { transactionManager, entity.ServeyresultCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.ServeyAnswer object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.ServeyAnswer instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.ServeyAnswer Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.ServeyAnswer entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region QuestionIdSource
			if (CanDeepSave(entity, "ServeyQuestion|QuestionIdSource", deepSaveType, innerList) 
				&& entity.QuestionIdSource != null)
			{
				DataRepository.ServeyQuestionProvider.Save(transactionManager, entity.QuestionIdSource);
				entity.QuestionId = entity.QuestionIdSource.QuestionId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<Serveyresult>
				if (CanDeepSave(entity.ServeyresultCollection, "List<Serveyresult>|ServeyresultCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Serveyresult child in entity.ServeyresultCollection)
					{
						if(child.AnswerIdSource != null)
						{
							child.AnswerId = child.AnswerIdSource.AnswerId;
						}
						else
						{
							child.AnswerId = entity.AnswerId;
						}

					}

					if (entity.ServeyresultCollection.Count > 0 || entity.ServeyresultCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ServeyresultProvider.Save(transactionManager, entity.ServeyresultCollection);
						
						deepHandles.Add("ServeyresultCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Serveyresult >) DataRepository.ServeyresultProvider.DeepSave,
							new object[] { transactionManager, entity.ServeyresultCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ServeyAnswerChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.ServeyAnswer</c>
	///</summary>
	public enum ServeyAnswerChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>ServeyQuestion</c> at QuestionIdSource
		///</summary>
		[ChildEntityType(typeof(ServeyQuestion))]
		ServeyQuestion,
	
		///<summary>
		/// Collection of <c>ServeyAnswer</c> as OneToMany for ServeyresultCollection
		///</summary>
		[ChildEntityType(typeof(TList<Serveyresult>))]
		ServeyresultCollection,
	}
	
	#endregion ServeyAnswerChildEntityTypes
	
	#region ServeyAnswerFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ServeyAnswerColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ServeyAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ServeyAnswerFilterBuilder : SqlFilterBuilder<ServeyAnswerColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ServeyAnswerFilterBuilder class.
		/// </summary>
		public ServeyAnswerFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ServeyAnswerFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ServeyAnswerFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ServeyAnswerFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ServeyAnswerFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ServeyAnswerFilterBuilder
	
	#region ServeyAnswerParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ServeyAnswerColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ServeyAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ServeyAnswerParameterBuilder : ParameterizedSqlFilterBuilder<ServeyAnswerColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ServeyAnswerParameterBuilder class.
		/// </summary>
		public ServeyAnswerParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ServeyAnswerParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ServeyAnswerParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ServeyAnswerParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ServeyAnswerParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ServeyAnswerParameterBuilder
	
	#region ServeyAnswerSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ServeyAnswerColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ServeyAnswer"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ServeyAnswerSortBuilder : SqlSortBuilder<ServeyAnswerColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ServeyAnswerSqlSortBuilder class.
		/// </summary>
		public ServeyAnswerSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ServeyAnswerSortBuilder
	
} // end namespace
