﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SurveyCategoryProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SurveyCategoryProviderBaseCore : EntityProviderBase<LMMR.Entities.SurveyCategory, LMMR.Entities.SurveyCategoryKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.SurveyCategoryKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__survey_ca__Categ__5192630D key.
		///		FK__survey_ca__Categ__5192630D Description: 
		/// </summary>
		/// <param name="_categoryQuestionId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyCategory objects.</returns>
		public TList<SurveyCategory> GetByCategoryQuestionId(System.Int32? _categoryQuestionId)
		{
			int count = -1;
			return GetByCategoryQuestionId(_categoryQuestionId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__survey_ca__Categ__5192630D key.
		///		FK__survey_ca__Categ__5192630D Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryQuestionId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyCategory objects.</returns>
		/// <remarks></remarks>
		public TList<SurveyCategory> GetByCategoryQuestionId(TransactionManager transactionManager, System.Int32? _categoryQuestionId)
		{
			int count = -1;
			return GetByCategoryQuestionId(transactionManager, _categoryQuestionId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK__survey_ca__Categ__5192630D key.
		///		FK__survey_ca__Categ__5192630D Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryQuestionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyCategory objects.</returns>
		public TList<SurveyCategory> GetByCategoryQuestionId(TransactionManager transactionManager, System.Int32? _categoryQuestionId, int start, int pageLength)
		{
			int count = -1;
			return GetByCategoryQuestionId(transactionManager, _categoryQuestionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__survey_ca__Categ__5192630D key.
		///		fkSurveyCaCateg5192630d Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_categoryQuestionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyCategory objects.</returns>
		public TList<SurveyCategory> GetByCategoryQuestionId(System.Int32? _categoryQuestionId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCategoryQuestionId(null, _categoryQuestionId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__survey_ca__Categ__5192630D key.
		///		fkSurveyCaCateg5192630d Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_categoryQuestionId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyCategory objects.</returns>
		public TList<SurveyCategory> GetByCategoryQuestionId(System.Int32? _categoryQuestionId, int start, int pageLength,out int count)
		{
			return GetByCategoryQuestionId(null, _categoryQuestionId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__survey_ca__Categ__5192630D key.
		///		FK__survey_ca__Categ__5192630D Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryQuestionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.SurveyCategory objects.</returns>
		public abstract TList<SurveyCategory> GetByCategoryQuestionId(TransactionManager transactionManager, System.Int32? _categoryQuestionId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.SurveyCategory Get(TransactionManager transactionManager, LMMR.Entities.SurveyCategoryKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK__survey_c__3214EC074FAA1A9B index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyCategory"/> class.</returns>
		public LMMR.Entities.SurveyCategory GetById(System.Int32 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__survey_c__3214EC074FAA1A9B index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyCategory"/> class.</returns>
		public LMMR.Entities.SurveyCategory GetById(System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__survey_c__3214EC074FAA1A9B index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyCategory"/> class.</returns>
		public LMMR.Entities.SurveyCategory GetById(TransactionManager transactionManager, System.Int32 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__survey_c__3214EC074FAA1A9B index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyCategory"/> class.</returns>
		public LMMR.Entities.SurveyCategory GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__survey_c__3214EC074FAA1A9B index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyCategory"/> class.</returns>
		public LMMR.Entities.SurveyCategory GetById(System.Int32 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__survey_c__3214EC074FAA1A9B index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SurveyCategory"/> class.</returns>
		public abstract LMMR.Entities.SurveyCategory GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SurveyCategory&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SurveyCategory&gt;"/></returns>
		public static TList<SurveyCategory> Fill(IDataReader reader, TList<SurveyCategory> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.SurveyCategory c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SurveyCategory")
					.Append("|").Append((System.Int32)reader[((int)SurveyCategoryColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SurveyCategory>(
					key.ToString(), // EntityTrackingKey
					"SurveyCategory",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.SurveyCategory();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int32)reader[((int)SurveyCategoryColumn.Id - 1)];
					c.CategoryText = (reader.IsDBNull(((int)SurveyCategoryColumn.CategoryText - 1)))?null:(System.String)reader[((int)SurveyCategoryColumn.CategoryText - 1)];
					c.LanguageId = (reader.IsDBNull(((int)SurveyCategoryColumn.LanguageId - 1)))?null:(System.Int32?)reader[((int)SurveyCategoryColumn.LanguageId - 1)];
					c.CategoryQuestionId = (reader.IsDBNull(((int)SurveyCategoryColumn.CategoryQuestionId - 1)))?null:(System.Int32?)reader[((int)SurveyCategoryColumn.CategoryQuestionId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.SurveyCategory"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SurveyCategory"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.SurveyCategory entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int32)reader[((int)SurveyCategoryColumn.Id - 1)];
			entity.CategoryText = (reader.IsDBNull(((int)SurveyCategoryColumn.CategoryText - 1)))?null:(System.String)reader[((int)SurveyCategoryColumn.CategoryText - 1)];
			entity.LanguageId = (reader.IsDBNull(((int)SurveyCategoryColumn.LanguageId - 1)))?null:(System.Int32?)reader[((int)SurveyCategoryColumn.LanguageId - 1)];
			entity.CategoryQuestionId = (reader.IsDBNull(((int)SurveyCategoryColumn.CategoryQuestionId - 1)))?null:(System.Int32?)reader[((int)SurveyCategoryColumn.CategoryQuestionId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.SurveyCategory"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SurveyCategory"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.SurveyCategory entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int32)dataRow["Id"];
			entity.CategoryText = Convert.IsDBNull(dataRow["Category_Text"]) ? null : (System.String)dataRow["Category_Text"];
			entity.LanguageId = Convert.IsDBNull(dataRow["LanguageID"]) ? null : (System.Int32?)dataRow["LanguageID"];
			entity.CategoryQuestionId = Convert.IsDBNull(dataRow["Category_questionID"]) ? null : (System.Int32?)dataRow["Category_questionID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SurveyCategory"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.SurveyCategory Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.SurveyCategory entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CategoryQuestionIdSource	
			if (CanDeepLoad(entity, "SurveyQuestion|CategoryQuestionIdSource", deepLoadType, innerList) 
				&& entity.CategoryQuestionIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.CategoryQuestionId ?? (int)0);
				SurveyQuestion tmpEntity = EntityManager.LocateEntity<SurveyQuestion>(EntityLocator.ConstructKeyFromPkItems(typeof(SurveyQuestion), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CategoryQuestionIdSource = tmpEntity;
				else
					entity.CategoryQuestionIdSource = DataRepository.SurveyQuestionProvider.GetById(transactionManager, (entity.CategoryQuestionId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CategoryQuestionIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CategoryQuestionIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SurveyQuestionProvider.DeepLoad(transactionManager, entity.CategoryQuestionIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CategoryQuestionIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region SurveyAnswerCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SurveyAnswer>|SurveyAnswerCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SurveyAnswerCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SurveyAnswerCollection = DataRepository.SurveyAnswerProvider.GetByCategoryId(transactionManager, entity.Id);

				if (deep && entity.SurveyAnswerCollection.Count > 0)
				{
					deepHandles.Add("SurveyAnswerCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SurveyAnswer>) DataRepository.SurveyAnswerProvider.DeepLoad,
						new object[] { transactionManager, entity.SurveyAnswerCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.SurveyCategory object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.SurveyCategory instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.SurveyCategory Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.SurveyCategory entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CategoryQuestionIdSource
			if (CanDeepSave(entity, "SurveyQuestion|CategoryQuestionIdSource", deepSaveType, innerList) 
				&& entity.CategoryQuestionIdSource != null)
			{
				DataRepository.SurveyQuestionProvider.Save(transactionManager, entity.CategoryQuestionIdSource);
				entity.CategoryQuestionId = entity.CategoryQuestionIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<SurveyAnswer>
				if (CanDeepSave(entity.SurveyAnswerCollection, "List<SurveyAnswer>|SurveyAnswerCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SurveyAnswer child in entity.SurveyAnswerCollection)
					{
						if(child.CategoryIdSource != null)
						{
							child.CategoryId = child.CategoryIdSource.Id;
						}
						else
						{
							child.CategoryId = entity.Id;
						}

					}

					if (entity.SurveyAnswerCollection.Count > 0 || entity.SurveyAnswerCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SurveyAnswerProvider.Save(transactionManager, entity.SurveyAnswerCollection);
						
						deepHandles.Add("SurveyAnswerCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SurveyAnswer >) DataRepository.SurveyAnswerProvider.DeepSave,
							new object[] { transactionManager, entity.SurveyAnswerCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SurveyCategoryChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.SurveyCategory</c>
	///</summary>
	public enum SurveyCategoryChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>SurveyQuestion</c> at CategoryQuestionIdSource
		///</summary>
		[ChildEntityType(typeof(SurveyQuestion))]
		SurveyQuestion,
	
		///<summary>
		/// Collection of <c>SurveyCategory</c> as OneToMany for SurveyAnswerCollection
		///</summary>
		[ChildEntityType(typeof(TList<SurveyAnswer>))]
		SurveyAnswerCollection,
	}
	
	#endregion SurveyCategoryChildEntityTypes
	
	#region SurveyCategoryFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SurveyCategoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SurveyCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SurveyCategoryFilterBuilder : SqlFilterBuilder<SurveyCategoryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SurveyCategoryFilterBuilder class.
		/// </summary>
		public SurveyCategoryFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SurveyCategoryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SurveyCategoryFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SurveyCategoryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SurveyCategoryFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SurveyCategoryFilterBuilder
	
	#region SurveyCategoryParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SurveyCategoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SurveyCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SurveyCategoryParameterBuilder : ParameterizedSqlFilterBuilder<SurveyCategoryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SurveyCategoryParameterBuilder class.
		/// </summary>
		public SurveyCategoryParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SurveyCategoryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SurveyCategoryParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SurveyCategoryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SurveyCategoryParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SurveyCategoryParameterBuilder
	
	#region SurveyCategorySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SurveyCategoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SurveyCategory"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SurveyCategorySortBuilder : SqlSortBuilder<SurveyCategoryColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SurveyCategorySqlSortBuilder class.
		/// </summary>
		public SurveyCategorySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SurveyCategorySortBuilder
	
} // end namespace
