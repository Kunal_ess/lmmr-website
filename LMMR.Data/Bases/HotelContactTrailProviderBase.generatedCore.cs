﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="HotelContactTrailProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class HotelContactTrailProviderBaseCore : EntityProviderBase<LMMR.Entities.HotelContactTrail, LMMR.Entities.HotelContactTrailKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.HotelContactTrailKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_HotelContact_trail_HotelContact key.
		///		FK_HotelContact_trail_HotelContact Description: 
		/// </summary>
		/// <param name="_hotelContactId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.HotelContactTrail objects.</returns>
		public TList<HotelContactTrail> GetByHotelContactId(System.Int64 _hotelContactId)
		{
			int count = -1;
			return GetByHotelContactId(_hotelContactId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_HotelContact_trail_HotelContact key.
		///		FK_HotelContact_trail_HotelContact Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelContactId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.HotelContactTrail objects.</returns>
		/// <remarks></remarks>
		public TList<HotelContactTrail> GetByHotelContactId(TransactionManager transactionManager, System.Int64 _hotelContactId)
		{
			int count = -1;
			return GetByHotelContactId(transactionManager, _hotelContactId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_HotelContact_trail_HotelContact key.
		///		FK_HotelContact_trail_HotelContact Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelContactId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.HotelContactTrail objects.</returns>
		public TList<HotelContactTrail> GetByHotelContactId(TransactionManager transactionManager, System.Int64 _hotelContactId, int start, int pageLength)
		{
			int count = -1;
			return GetByHotelContactId(transactionManager, _hotelContactId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_HotelContact_trail_HotelContact key.
		///		fkHotelContactTrailHotelContact Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_hotelContactId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.HotelContactTrail objects.</returns>
		public TList<HotelContactTrail> GetByHotelContactId(System.Int64 _hotelContactId, int start, int pageLength)
		{
			int count =  -1;
			return GetByHotelContactId(null, _hotelContactId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_HotelContact_trail_HotelContact key.
		///		fkHotelContactTrailHotelContact Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_hotelContactId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.HotelContactTrail objects.</returns>
		public TList<HotelContactTrail> GetByHotelContactId(System.Int64 _hotelContactId, int start, int pageLength,out int count)
		{
			return GetByHotelContactId(null, _hotelContactId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_HotelContact_trail_HotelContact key.
		///		FK_HotelContact_trail_HotelContact Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_hotelContactId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.HotelContactTrail objects.</returns>
		public abstract TList<HotelContactTrail> GetByHotelContactId(TransactionManager transactionManager, System.Int64 _hotelContactId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.HotelContactTrail Get(TransactionManager transactionManager, LMMR.Entities.HotelContactTrailKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_HotelContact_trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HotelContactTrail"/> class.</returns>
		public LMMR.Entities.HotelContactTrail GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_HotelContact_trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HotelContactTrail"/> class.</returns>
		public LMMR.Entities.HotelContactTrail GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_HotelContact_trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HotelContactTrail"/> class.</returns>
		public LMMR.Entities.HotelContactTrail GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_HotelContact_trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HotelContactTrail"/> class.</returns>
		public LMMR.Entities.HotelContactTrail GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_HotelContact_trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HotelContactTrail"/> class.</returns>
		public LMMR.Entities.HotelContactTrail GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_HotelContact_trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.HotelContactTrail"/> class.</returns>
		public abstract LMMR.Entities.HotelContactTrail GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;HotelContactTrail&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;HotelContactTrail&gt;"/></returns>
		public static TList<HotelContactTrail> Fill(IDataReader reader, TList<HotelContactTrail> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.HotelContactTrail c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("HotelContactTrail")
					.Append("|").Append((System.Int64)reader[((int)HotelContactTrailColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<HotelContactTrail>(
					key.ToString(), // EntityTrackingKey
					"HotelContactTrail",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.HotelContactTrail();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)HotelContactTrailColumn.Id - 1)];
					c.HotelContactId = (System.Int64)reader[((int)HotelContactTrailColumn.HotelContactId - 1)];
					c.HotelId = (System.Int64)reader[((int)HotelContactTrailColumn.HotelId - 1)];
					c.ContactType = (reader.IsDBNull(((int)HotelContactTrailColumn.ContactType - 1)))?null:(System.String)reader[((int)HotelContactTrailColumn.ContactType - 1)];
					c.UserType = (reader.IsDBNull(((int)HotelContactTrailColumn.UserType - 1)))?null:(System.String)reader[((int)HotelContactTrailColumn.UserType - 1)];
					c.JobTitle = (reader.IsDBNull(((int)HotelContactTrailColumn.JobTitle - 1)))?null:(System.String)reader[((int)HotelContactTrailColumn.JobTitle - 1)];
					c.FirstName = (reader.IsDBNull(((int)HotelContactTrailColumn.FirstName - 1)))?null:(System.String)reader[((int)HotelContactTrailColumn.FirstName - 1)];
					c.LastName = (reader.IsDBNull(((int)HotelContactTrailColumn.LastName - 1)))?null:(System.String)reader[((int)HotelContactTrailColumn.LastName - 1)];
					c.Phone = (reader.IsDBNull(((int)HotelContactTrailColumn.Phone - 1)))?null:(System.String)reader[((int)HotelContactTrailColumn.Phone - 1)];
					c.Ext = (reader.IsDBNull(((int)HotelContactTrailColumn.Ext - 1)))?null:(System.String)reader[((int)HotelContactTrailColumn.Ext - 1)];
					c.Email = (reader.IsDBNull(((int)HotelContactTrailColumn.Email - 1)))?null:(System.String)reader[((int)HotelContactTrailColumn.Email - 1)];
					c.UpdateDate = (reader.IsDBNull(((int)HotelContactTrailColumn.UpdateDate - 1)))?null:(System.DateTime?)reader[((int)HotelContactTrailColumn.UpdateDate - 1)];
					c.UpdatedBy = (reader.IsDBNull(((int)HotelContactTrailColumn.UpdatedBy - 1)))?null:(System.Int32?)reader[((int)HotelContactTrailColumn.UpdatedBy - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.HotelContactTrail"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.HotelContactTrail"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.HotelContactTrail entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)HotelContactTrailColumn.Id - 1)];
			entity.HotelContactId = (System.Int64)reader[((int)HotelContactTrailColumn.HotelContactId - 1)];
			entity.HotelId = (System.Int64)reader[((int)HotelContactTrailColumn.HotelId - 1)];
			entity.ContactType = (reader.IsDBNull(((int)HotelContactTrailColumn.ContactType - 1)))?null:(System.String)reader[((int)HotelContactTrailColumn.ContactType - 1)];
			entity.UserType = (reader.IsDBNull(((int)HotelContactTrailColumn.UserType - 1)))?null:(System.String)reader[((int)HotelContactTrailColumn.UserType - 1)];
			entity.JobTitle = (reader.IsDBNull(((int)HotelContactTrailColumn.JobTitle - 1)))?null:(System.String)reader[((int)HotelContactTrailColumn.JobTitle - 1)];
			entity.FirstName = (reader.IsDBNull(((int)HotelContactTrailColumn.FirstName - 1)))?null:(System.String)reader[((int)HotelContactTrailColumn.FirstName - 1)];
			entity.LastName = (reader.IsDBNull(((int)HotelContactTrailColumn.LastName - 1)))?null:(System.String)reader[((int)HotelContactTrailColumn.LastName - 1)];
			entity.Phone = (reader.IsDBNull(((int)HotelContactTrailColumn.Phone - 1)))?null:(System.String)reader[((int)HotelContactTrailColumn.Phone - 1)];
			entity.Ext = (reader.IsDBNull(((int)HotelContactTrailColumn.Ext - 1)))?null:(System.String)reader[((int)HotelContactTrailColumn.Ext - 1)];
			entity.Email = (reader.IsDBNull(((int)HotelContactTrailColumn.Email - 1)))?null:(System.String)reader[((int)HotelContactTrailColumn.Email - 1)];
			entity.UpdateDate = (reader.IsDBNull(((int)HotelContactTrailColumn.UpdateDate - 1)))?null:(System.DateTime?)reader[((int)HotelContactTrailColumn.UpdateDate - 1)];
			entity.UpdatedBy = (reader.IsDBNull(((int)HotelContactTrailColumn.UpdatedBy - 1)))?null:(System.Int32?)reader[((int)HotelContactTrailColumn.UpdatedBy - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.HotelContactTrail"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.HotelContactTrail"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.HotelContactTrail entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["ID"];
			entity.HotelContactId = (System.Int64)dataRow["HotelContactId"];
			entity.HotelId = (System.Int64)dataRow["Hotel_Id"];
			entity.ContactType = Convert.IsDBNull(dataRow["ContactType"]) ? null : (System.String)dataRow["ContactType"];
			entity.UserType = Convert.IsDBNull(dataRow["UserType"]) ? null : (System.String)dataRow["UserType"];
			entity.JobTitle = Convert.IsDBNull(dataRow["JobTitle"]) ? null : (System.String)dataRow["JobTitle"];
			entity.FirstName = Convert.IsDBNull(dataRow["FirstName"]) ? null : (System.String)dataRow["FirstName"];
			entity.LastName = Convert.IsDBNull(dataRow["LastName"]) ? null : (System.String)dataRow["LastName"];
			entity.Phone = Convert.IsDBNull(dataRow["Phone"]) ? null : (System.String)dataRow["Phone"];
			entity.Ext = Convert.IsDBNull(dataRow["Ext"]) ? null : (System.String)dataRow["Ext"];
			entity.Email = Convert.IsDBNull(dataRow["Email"]) ? null : (System.String)dataRow["Email"];
			entity.UpdateDate = Convert.IsDBNull(dataRow["UpdateDate"]) ? null : (System.DateTime?)dataRow["UpdateDate"];
			entity.UpdatedBy = Convert.IsDBNull(dataRow["UpdatedBy"]) ? null : (System.Int32?)dataRow["UpdatedBy"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.HotelContactTrail"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.HotelContactTrail Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.HotelContactTrail entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region HotelContactIdSource	
			if (CanDeepLoad(entity, "HotelContact|HotelContactIdSource", deepLoadType, innerList) 
				&& entity.HotelContactIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.HotelContactId;
				HotelContact tmpEntity = EntityManager.LocateEntity<HotelContact>(EntityLocator.ConstructKeyFromPkItems(typeof(HotelContact), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.HotelContactIdSource = tmpEntity;
				else
					entity.HotelContactIdSource = DataRepository.HotelContactProvider.GetById(transactionManager, entity.HotelContactId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HotelContactIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.HotelContactIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.HotelContactProvider.DeepLoad(transactionManager, entity.HotelContactIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion HotelContactIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.HotelContactTrail object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.HotelContactTrail instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.HotelContactTrail Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.HotelContactTrail entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region HotelContactIdSource
			if (CanDeepSave(entity, "HotelContact|HotelContactIdSource", deepSaveType, innerList) 
				&& entity.HotelContactIdSource != null)
			{
				DataRepository.HotelContactProvider.Save(transactionManager, entity.HotelContactIdSource);
				entity.HotelContactId = entity.HotelContactIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region HotelContactTrailChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.HotelContactTrail</c>
	///</summary>
	public enum HotelContactTrailChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>HotelContact</c> at HotelContactIdSource
		///</summary>
		[ChildEntityType(typeof(HotelContact))]
		HotelContact,
		}
	
	#endregion HotelContactTrailChildEntityTypes
	
	#region HotelContactTrailFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;HotelContactTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HotelContactTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelContactTrailFilterBuilder : SqlFilterBuilder<HotelContactTrailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelContactTrailFilterBuilder class.
		/// </summary>
		public HotelContactTrailFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelContactTrailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelContactTrailFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelContactTrailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelContactTrailFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelContactTrailFilterBuilder
	
	#region HotelContactTrailParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;HotelContactTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HotelContactTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HotelContactTrailParameterBuilder : ParameterizedSqlFilterBuilder<HotelContactTrailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelContactTrailParameterBuilder class.
		/// </summary>
		public HotelContactTrailParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the HotelContactTrailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HotelContactTrailParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HotelContactTrailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HotelContactTrailParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HotelContactTrailParameterBuilder
	
	#region HotelContactTrailSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;HotelContactTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HotelContactTrail"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class HotelContactTrailSortBuilder : SqlSortBuilder<HotelContactTrailColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HotelContactTrailSqlSortBuilder class.
		/// </summary>
		public HotelContactTrailSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion HotelContactTrailSortBuilder
	
} // end namespace
