﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="UserDetailsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class UserDetailsProviderBaseCore : EntityProviderBase<LMMR.Entities.UserDetails, LMMR.Entities.UserDetailsKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.UserDetailsKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserDetails_City key.
		///		FK_UserDetails_City Description: 
		/// </summary>
		/// <param name="_cityId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.UserDetails objects.</returns>
		public TList<UserDetails> GetByCityId(System.Int64? _cityId)
		{
			int count = -1;
			return GetByCityId(_cityId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserDetails_City key.
		///		FK_UserDetails_City Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cityId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.UserDetails objects.</returns>
		/// <remarks></remarks>
		public TList<UserDetails> GetByCityId(TransactionManager transactionManager, System.Int64? _cityId)
		{
			int count = -1;
			return GetByCityId(transactionManager, _cityId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserDetails_City key.
		///		FK_UserDetails_City Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cityId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.UserDetails objects.</returns>
		public TList<UserDetails> GetByCityId(TransactionManager transactionManager, System.Int64? _cityId, int start, int pageLength)
		{
			int count = -1;
			return GetByCityId(transactionManager, _cityId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserDetails_City key.
		///		fkUserDetailsCity Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_cityId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.UserDetails objects.</returns>
		public TList<UserDetails> GetByCityId(System.Int64? _cityId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCityId(null, _cityId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserDetails_City key.
		///		fkUserDetailsCity Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_cityId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.UserDetails objects.</returns>
		public TList<UserDetails> GetByCityId(System.Int64? _cityId, int start, int pageLength,out int count)
		{
			return GetByCityId(null, _cityId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserDetails_City key.
		///		FK_UserDetails_City Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cityId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.UserDetails objects.</returns>
		public abstract TList<UserDetails> GetByCityId(TransactionManager transactionManager, System.Int64? _cityId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserDetails_Country key.
		///		FK_UserDetails_Country Description: 
		/// </summary>
		/// <param name="_countryId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.UserDetails objects.</returns>
		public TList<UserDetails> GetByCountryId(System.Int64? _countryId)
		{
			int count = -1;
			return GetByCountryId(_countryId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserDetails_Country key.
		///		FK_UserDetails_Country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.UserDetails objects.</returns>
		/// <remarks></remarks>
		public TList<UserDetails> GetByCountryId(TransactionManager transactionManager, System.Int64? _countryId)
		{
			int count = -1;
			return GetByCountryId(transactionManager, _countryId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserDetails_Country key.
		///		FK_UserDetails_Country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.UserDetails objects.</returns>
		public TList<UserDetails> GetByCountryId(TransactionManager transactionManager, System.Int64? _countryId, int start, int pageLength)
		{
			int count = -1;
			return GetByCountryId(transactionManager, _countryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserDetails_Country key.
		///		fkUserDetailsCountry Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_countryId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.UserDetails objects.</returns>
		public TList<UserDetails> GetByCountryId(System.Int64? _countryId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCountryId(null, _countryId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserDetails_Country key.
		///		fkUserDetailsCountry Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_countryId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.UserDetails objects.</returns>
		public TList<UserDetails> GetByCountryId(System.Int64? _countryId, int start, int pageLength,out int count)
		{
			return GetByCountryId(null, _countryId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserDetails_Country key.
		///		FK_UserDetails_Country Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_countryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.UserDetails objects.</returns>
		public abstract TList<UserDetails> GetByCountryId(TransactionManager transactionManager, System.Int64? _countryId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserDetails_Users key.
		///		FK_UserDetails_Users Description: 
		/// </summary>
		/// <param name="_userId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.UserDetails objects.</returns>
		public TList<UserDetails> GetByUserId(System.Int64? _userId)
		{
			int count = -1;
			return GetByUserId(_userId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserDetails_Users key.
		///		FK_UserDetails_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.UserDetails objects.</returns>
		/// <remarks></remarks>
		public TList<UserDetails> GetByUserId(TransactionManager transactionManager, System.Int64? _userId)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserDetails_Users key.
		///		FK_UserDetails_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.UserDetails objects.</returns>
		public TList<UserDetails> GetByUserId(TransactionManager transactionManager, System.Int64? _userId, int start, int pageLength)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserDetails_Users key.
		///		fkUserDetailsUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_userId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.UserDetails objects.</returns>
		public TList<UserDetails> GetByUserId(System.Int64? _userId, int start, int pageLength)
		{
			int count =  -1;
			return GetByUserId(null, _userId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserDetails_Users key.
		///		fkUserDetailsUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_userId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.UserDetails objects.</returns>
		public TList<UserDetails> GetByUserId(System.Int64? _userId, int start, int pageLength,out int count)
		{
			return GetByUserId(null, _userId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserDetails_Users key.
		///		FK_UserDetails_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.UserDetails objects.</returns>
		public abstract TList<UserDetails> GetByUserId(TransactionManager transactionManager, System.Int64? _userId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.UserDetails Get(TransactionManager transactionManager, LMMR.Entities.UserDetailsKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_UserDetails index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.UserDetails"/> class.</returns>
		public LMMR.Entities.UserDetails GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_UserDetails index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.UserDetails"/> class.</returns>
		public LMMR.Entities.UserDetails GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_UserDetails index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.UserDetails"/> class.</returns>
		public LMMR.Entities.UserDetails GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_UserDetails index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.UserDetails"/> class.</returns>
		public LMMR.Entities.UserDetails GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_UserDetails index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.UserDetails"/> class.</returns>
		public LMMR.Entities.UserDetails GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_UserDetails index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.UserDetails"/> class.</returns>
		public abstract LMMR.Entities.UserDetails GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;UserDetails&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;UserDetails&gt;"/></returns>
		public static TList<UserDetails> Fill(IDataReader reader, TList<UserDetails> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.UserDetails c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("UserDetails")
					.Append("|").Append((System.Int64)reader[((int)UserDetailsColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<UserDetails>(
					key.ToString(), // EntityTrackingKey
					"UserDetails",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.UserDetails();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)UserDetailsColumn.Id - 1)];
					c.UserId = (reader.IsDBNull(((int)UserDetailsColumn.UserId - 1)))?null:(System.Int64?)reader[((int)UserDetailsColumn.UserId - 1)];
					c.CompanyName = (reader.IsDBNull(((int)UserDetailsColumn.CompanyName - 1)))?null:(System.String)reader[((int)UserDetailsColumn.CompanyName - 1)];
					c.CountryId = (reader.IsDBNull(((int)UserDetailsColumn.CountryId - 1)))?null:(System.Int64?)reader[((int)UserDetailsColumn.CountryId - 1)];
					c.CountryCode = (reader.IsDBNull(((int)UserDetailsColumn.CountryCode - 1)))?null:(System.String)reader[((int)UserDetailsColumn.CountryCode - 1)];
					c.CityId = (reader.IsDBNull(((int)UserDetailsColumn.CityId - 1)))?null:(System.Int64?)reader[((int)UserDetailsColumn.CityId - 1)];
					c.Phone = (reader.IsDBNull(((int)UserDetailsColumn.Phone - 1)))?null:(System.String)reader[((int)UserDetailsColumn.Phone - 1)];
					c.Address = (reader.IsDBNull(((int)UserDetailsColumn.Address - 1)))?null:(System.String)reader[((int)UserDetailsColumn.Address - 1)];
					c.PostalCode = (reader.IsDBNull(((int)UserDetailsColumn.PostalCode - 1)))?null:(System.String)reader[((int)UserDetailsColumn.PostalCode - 1)];
					c.HearFromUs = (reader.IsDBNull(((int)UserDetailsColumn.HearFromUs - 1)))?null:(System.String)reader[((int)UserDetailsColumn.HearFromUs - 1)];
					c.ActivationDate = (reader.IsDBNull(((int)UserDetailsColumn.ActivationDate - 1)))?null:(System.DateTime?)reader[((int)UserDetailsColumn.ActivationDate - 1)];
					c.IataNo = (reader.IsDBNull(((int)UserDetailsColumn.IataNo - 1)))?null:(System.String)reader[((int)UserDetailsColumn.IataNo - 1)];
					c.VatNo = (reader.IsDBNull(((int)UserDetailsColumn.VatNo - 1)))?null:(System.String)reader[((int)UserDetailsColumn.VatNo - 1)];
					c.VatValue = (reader.IsDBNull(((int)UserDetailsColumn.VatValue - 1)))?null:(System.Int64?)reader[((int)UserDetailsColumn.VatValue - 1)];
					c.Name = (reader.IsDBNull(((int)UserDetailsColumn.Name - 1)))?null:(System.String)reader[((int)UserDetailsColumn.Name - 1)];
					c.Department = (reader.IsDBNull(((int)UserDetailsColumn.Department - 1)))?null:(System.String)reader[((int)UserDetailsColumn.Department - 1)];
					c.CommunicationEmail = (reader.IsDBNull(((int)UserDetailsColumn.CommunicationEmail - 1)))?null:(System.String)reader[((int)UserDetailsColumn.CommunicationEmail - 1)];
					c.InvoiceSeriesFormat = (reader.IsDBNull(((int)UserDetailsColumn.InvoiceSeriesFormat - 1)))?null:(System.String)reader[((int)UserDetailsColumn.InvoiceSeriesFormat - 1)];
					c.AgencyCode = (reader.IsDBNull(((int)UserDetailsColumn.AgencyCode - 1)))?null:(System.String)reader[((int)UserDetailsColumn.AgencyCode - 1)];
					c.AgencyReffId = (reader.IsDBNull(((int)UserDetailsColumn.AgencyReffId - 1)))?null:(System.String)reader[((int)UserDetailsColumn.AgencyReffId - 1)];
					c.IsFinancialInfo = (System.Boolean)reader[((int)UserDetailsColumn.IsFinancialInfo - 1)];
					c.IsDeleted = (System.Boolean)reader[((int)UserDetailsColumn.IsDeleted - 1)];
					c.TransferUserId = (reader.IsDBNull(((int)UserDetailsColumn.TransferUserId - 1)))?null:(System.Int64?)reader[((int)UserDetailsColumn.TransferUserId - 1)];
					c.DeletedDate = (reader.IsDBNull(((int)UserDetailsColumn.DeletedDate - 1)))?null:(System.DateTime?)reader[((int)UserDetailsColumn.DeletedDate - 1)];
					c.LinkWith = (reader.IsDBNull(((int)UserDetailsColumn.LinkWith - 1)))?null:(System.String)reader[((int)UserDetailsColumn.LinkWith - 1)];
					c.LanguageId = (reader.IsDBNull(((int)UserDetailsColumn.LanguageId - 1)))?null:(System.Int32?)reader[((int)UserDetailsColumn.LanguageId - 1)];
					c.CityName = (reader.IsDBNull(((int)UserDetailsColumn.CityName - 1)))?null:(System.String)reader[((int)UserDetailsColumn.CityName - 1)];
					c.VisitCount = (reader.IsDBNull(((int)UserDetailsColumn.VisitCount - 1)))?null:(System.Int32?)reader[((int)UserDetailsColumn.VisitCount - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.UserDetails"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.UserDetails"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.UserDetails entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)UserDetailsColumn.Id - 1)];
			entity.UserId = (reader.IsDBNull(((int)UserDetailsColumn.UserId - 1)))?null:(System.Int64?)reader[((int)UserDetailsColumn.UserId - 1)];
			entity.CompanyName = (reader.IsDBNull(((int)UserDetailsColumn.CompanyName - 1)))?null:(System.String)reader[((int)UserDetailsColumn.CompanyName - 1)];
			entity.CountryId = (reader.IsDBNull(((int)UserDetailsColumn.CountryId - 1)))?null:(System.Int64?)reader[((int)UserDetailsColumn.CountryId - 1)];
			entity.CountryCode = (reader.IsDBNull(((int)UserDetailsColumn.CountryCode - 1)))?null:(System.String)reader[((int)UserDetailsColumn.CountryCode - 1)];
			entity.CityId = (reader.IsDBNull(((int)UserDetailsColumn.CityId - 1)))?null:(System.Int64?)reader[((int)UserDetailsColumn.CityId - 1)];
			entity.Phone = (reader.IsDBNull(((int)UserDetailsColumn.Phone - 1)))?null:(System.String)reader[((int)UserDetailsColumn.Phone - 1)];
			entity.Address = (reader.IsDBNull(((int)UserDetailsColumn.Address - 1)))?null:(System.String)reader[((int)UserDetailsColumn.Address - 1)];
			entity.PostalCode = (reader.IsDBNull(((int)UserDetailsColumn.PostalCode - 1)))?null:(System.String)reader[((int)UserDetailsColumn.PostalCode - 1)];
			entity.HearFromUs = (reader.IsDBNull(((int)UserDetailsColumn.HearFromUs - 1)))?null:(System.String)reader[((int)UserDetailsColumn.HearFromUs - 1)];
			entity.ActivationDate = (reader.IsDBNull(((int)UserDetailsColumn.ActivationDate - 1)))?null:(System.DateTime?)reader[((int)UserDetailsColumn.ActivationDate - 1)];
			entity.IataNo = (reader.IsDBNull(((int)UserDetailsColumn.IataNo - 1)))?null:(System.String)reader[((int)UserDetailsColumn.IataNo - 1)];
			entity.VatNo = (reader.IsDBNull(((int)UserDetailsColumn.VatNo - 1)))?null:(System.String)reader[((int)UserDetailsColumn.VatNo - 1)];
			entity.VatValue = (reader.IsDBNull(((int)UserDetailsColumn.VatValue - 1)))?null:(System.Int64?)reader[((int)UserDetailsColumn.VatValue - 1)];
			entity.Name = (reader.IsDBNull(((int)UserDetailsColumn.Name - 1)))?null:(System.String)reader[((int)UserDetailsColumn.Name - 1)];
			entity.Department = (reader.IsDBNull(((int)UserDetailsColumn.Department - 1)))?null:(System.String)reader[((int)UserDetailsColumn.Department - 1)];
			entity.CommunicationEmail = (reader.IsDBNull(((int)UserDetailsColumn.CommunicationEmail - 1)))?null:(System.String)reader[((int)UserDetailsColumn.CommunicationEmail - 1)];
			entity.InvoiceSeriesFormat = (reader.IsDBNull(((int)UserDetailsColumn.InvoiceSeriesFormat - 1)))?null:(System.String)reader[((int)UserDetailsColumn.InvoiceSeriesFormat - 1)];
			entity.AgencyCode = (reader.IsDBNull(((int)UserDetailsColumn.AgencyCode - 1)))?null:(System.String)reader[((int)UserDetailsColumn.AgencyCode - 1)];
			entity.AgencyReffId = (reader.IsDBNull(((int)UserDetailsColumn.AgencyReffId - 1)))?null:(System.String)reader[((int)UserDetailsColumn.AgencyReffId - 1)];
			entity.IsFinancialInfo = (System.Boolean)reader[((int)UserDetailsColumn.IsFinancialInfo - 1)];
			entity.IsDeleted = (System.Boolean)reader[((int)UserDetailsColumn.IsDeleted - 1)];
			entity.TransferUserId = (reader.IsDBNull(((int)UserDetailsColumn.TransferUserId - 1)))?null:(System.Int64?)reader[((int)UserDetailsColumn.TransferUserId - 1)];
			entity.DeletedDate = (reader.IsDBNull(((int)UserDetailsColumn.DeletedDate - 1)))?null:(System.DateTime?)reader[((int)UserDetailsColumn.DeletedDate - 1)];
			entity.LinkWith = (reader.IsDBNull(((int)UserDetailsColumn.LinkWith - 1)))?null:(System.String)reader[((int)UserDetailsColumn.LinkWith - 1)];
			entity.LanguageId = (reader.IsDBNull(((int)UserDetailsColumn.LanguageId - 1)))?null:(System.Int32?)reader[((int)UserDetailsColumn.LanguageId - 1)];
			entity.CityName = (reader.IsDBNull(((int)UserDetailsColumn.CityName - 1)))?null:(System.String)reader[((int)UserDetailsColumn.CityName - 1)];
			entity.VisitCount = (reader.IsDBNull(((int)UserDetailsColumn.VisitCount - 1)))?null:(System.Int32?)reader[((int)UserDetailsColumn.VisitCount - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.UserDetails"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.UserDetails"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.UserDetails entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.UserId = Convert.IsDBNull(dataRow["UserId"]) ? null : (System.Int64?)dataRow["UserId"];
			entity.CompanyName = Convert.IsDBNull(dataRow["CompanyName"]) ? null : (System.String)dataRow["CompanyName"];
			entity.CountryId = Convert.IsDBNull(dataRow["CountryId"]) ? null : (System.Int64?)dataRow["CountryId"];
			entity.CountryCode = Convert.IsDBNull(dataRow["CountryCode"]) ? null : (System.String)dataRow["CountryCode"];
			entity.CityId = Convert.IsDBNull(dataRow["CityId"]) ? null : (System.Int64?)dataRow["CityId"];
			entity.Phone = Convert.IsDBNull(dataRow["Phone"]) ? null : (System.String)dataRow["Phone"];
			entity.Address = Convert.IsDBNull(dataRow["Address"]) ? null : (System.String)dataRow["Address"];
			entity.PostalCode = Convert.IsDBNull(dataRow["PostalCode"]) ? null : (System.String)dataRow["PostalCode"];
			entity.HearFromUs = Convert.IsDBNull(dataRow["HearFromUs"]) ? null : (System.String)dataRow["HearFromUs"];
			entity.ActivationDate = Convert.IsDBNull(dataRow["ActivationDate"]) ? null : (System.DateTime?)dataRow["ActivationDate"];
			entity.IataNo = Convert.IsDBNull(dataRow["IATANo"]) ? null : (System.String)dataRow["IATANo"];
			entity.VatNo = Convert.IsDBNull(dataRow["VATNo"]) ? null : (System.String)dataRow["VATNo"];
			entity.VatValue = Convert.IsDBNull(dataRow["VATValue"]) ? null : (System.Int64?)dataRow["VATValue"];
			entity.Name = Convert.IsDBNull(dataRow["Name"]) ? null : (System.String)dataRow["Name"];
			entity.Department = Convert.IsDBNull(dataRow["Department"]) ? null : (System.String)dataRow["Department"];
			entity.CommunicationEmail = Convert.IsDBNull(dataRow["CommunicationEmail"]) ? null : (System.String)dataRow["CommunicationEmail"];
			entity.InvoiceSeriesFormat = Convert.IsDBNull(dataRow["InvoiceSeriesFormat"]) ? null : (System.String)dataRow["InvoiceSeriesFormat"];
			entity.AgencyCode = Convert.IsDBNull(dataRow["AgencyCode"]) ? null : (System.String)dataRow["AgencyCode"];
			entity.AgencyReffId = Convert.IsDBNull(dataRow["AgencyReffId"]) ? null : (System.String)dataRow["AgencyReffId"];
			entity.IsFinancialInfo = (System.Boolean)dataRow["IsFinancialInfo"];
			entity.IsDeleted = (System.Boolean)dataRow["IsDeleted"];
			entity.TransferUserId = Convert.IsDBNull(dataRow["TransferUserId"]) ? null : (System.Int64?)dataRow["TransferUserId"];
			entity.DeletedDate = Convert.IsDBNull(dataRow["DeletedDate"]) ? null : (System.DateTime?)dataRow["DeletedDate"];
			entity.LinkWith = Convert.IsDBNull(dataRow["LinkWith"]) ? null : (System.String)dataRow["LinkWith"];
			entity.LanguageId = Convert.IsDBNull(dataRow["LanguageId"]) ? null : (System.Int32?)dataRow["LanguageId"];
			entity.CityName = Convert.IsDBNull(dataRow["CityName"]) ? null : (System.String)dataRow["CityName"];
			entity.VisitCount = Convert.IsDBNull(dataRow["VisitCount"]) ? null : (System.Int32?)dataRow["VisitCount"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.UserDetails"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.UserDetails Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.UserDetails entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CityIdSource	
			if (CanDeepLoad(entity, "City|CityIdSource", deepLoadType, innerList) 
				&& entity.CityIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.CityId ?? (long)0);
				City tmpEntity = EntityManager.LocateEntity<City>(EntityLocator.ConstructKeyFromPkItems(typeof(City), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CityIdSource = tmpEntity;
				else
					entity.CityIdSource = DataRepository.CityProvider.GetById(transactionManager, (entity.CityId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CityIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CityIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CityProvider.DeepLoad(transactionManager, entity.CityIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CityIdSource

			#region CountryIdSource	
			if (CanDeepLoad(entity, "Country|CountryIdSource", deepLoadType, innerList) 
				&& entity.CountryIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.CountryId ?? (long)0);
				Country tmpEntity = EntityManager.LocateEntity<Country>(EntityLocator.ConstructKeyFromPkItems(typeof(Country), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CountryIdSource = tmpEntity;
				else
					entity.CountryIdSource = DataRepository.CountryProvider.GetById(transactionManager, (entity.CountryId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CountryIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CountryIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CountryProvider.DeepLoad(transactionManager, entity.CountryIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CountryIdSource

			#region UserIdSource	
			if (CanDeepLoad(entity, "Users|UserIdSource", deepLoadType, innerList) 
				&& entity.UserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.UserId ?? (long)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.UserIdSource = tmpEntity;
				else
					entity.UserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.UserId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.UserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.UserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion UserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.UserDetails object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.UserDetails instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.UserDetails Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.UserDetails entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CityIdSource
			if (CanDeepSave(entity, "City|CityIdSource", deepSaveType, innerList) 
				&& entity.CityIdSource != null)
			{
				DataRepository.CityProvider.Save(transactionManager, entity.CityIdSource);
				entity.CityId = entity.CityIdSource.Id;
			}
			#endregion 
			
			#region CountryIdSource
			if (CanDeepSave(entity, "Country|CountryIdSource", deepSaveType, innerList) 
				&& entity.CountryIdSource != null)
			{
				DataRepository.CountryProvider.Save(transactionManager, entity.CountryIdSource);
				entity.CountryId = entity.CountryIdSource.Id;
			}
			#endregion 
			
			#region UserIdSource
			if (CanDeepSave(entity, "Users|UserIdSource", deepSaveType, innerList) 
				&& entity.UserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.UserIdSource);
				entity.UserId = entity.UserIdSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region UserDetailsChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.UserDetails</c>
	///</summary>
	public enum UserDetailsChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>City</c> at CityIdSource
		///</summary>
		[ChildEntityType(typeof(City))]
		City,
			
		///<summary>
		/// Composite Property for <c>Country</c> at CountryIdSource
		///</summary>
		[ChildEntityType(typeof(Country))]
		Country,
			
		///<summary>
		/// Composite Property for <c>Users</c> at UserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
		}
	
	#endregion UserDetailsChildEntityTypes
	
	#region UserDetailsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;UserDetailsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UserDetails"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UserDetailsFilterBuilder : SqlFilterBuilder<UserDetailsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UserDetailsFilterBuilder class.
		/// </summary>
		public UserDetailsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UserDetailsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UserDetailsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UserDetailsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UserDetailsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UserDetailsFilterBuilder
	
	#region UserDetailsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;UserDetailsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UserDetails"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UserDetailsParameterBuilder : ParameterizedSqlFilterBuilder<UserDetailsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UserDetailsParameterBuilder class.
		/// </summary>
		public UserDetailsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UserDetailsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UserDetailsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UserDetailsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UserDetailsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UserDetailsParameterBuilder
	
	#region UserDetailsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;UserDetailsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UserDetails"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class UserDetailsSortBuilder : SqlSortBuilder<UserDetailsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UserDetailsSqlSortBuilder class.
		/// </summary>
		public UserDetailsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion UserDetailsSortBuilder
	
} // end namespace
