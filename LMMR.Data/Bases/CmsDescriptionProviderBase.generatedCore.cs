﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CmsDescriptionProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CmsDescriptionProviderBaseCore : EntityProviderBase<LMMR.Entities.CmsDescription, LMMR.Entities.CmsDescriptionKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.CmsDescriptionKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CMSDescription_CMS key.
		///		FK_CMSDescription_CMS Description: 
		/// </summary>
		/// <param name="_cmsId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.CmsDescription objects.</returns>
		public TList<CmsDescription> GetByCmsId(System.Int64 _cmsId)
		{
			int count = -1;
			return GetByCmsId(_cmsId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CMSDescription_CMS key.
		///		FK_CMSDescription_CMS Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cmsId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.CmsDescription objects.</returns>
		/// <remarks></remarks>
		public TList<CmsDescription> GetByCmsId(TransactionManager transactionManager, System.Int64 _cmsId)
		{
			int count = -1;
			return GetByCmsId(transactionManager, _cmsId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CMSDescription_CMS key.
		///		FK_CMSDescription_CMS Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cmsId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.CmsDescription objects.</returns>
		public TList<CmsDescription> GetByCmsId(TransactionManager transactionManager, System.Int64 _cmsId, int start, int pageLength)
		{
			int count = -1;
			return GetByCmsId(transactionManager, _cmsId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CMSDescription_CMS key.
		///		fkCmsDescriptionCms Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_cmsId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.CmsDescription objects.</returns>
		public TList<CmsDescription> GetByCmsId(System.Int64 _cmsId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCmsId(null, _cmsId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CMSDescription_CMS key.
		///		fkCmsDescriptionCms Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_cmsId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.CmsDescription objects.</returns>
		public TList<CmsDescription> GetByCmsId(System.Int64 _cmsId, int start, int pageLength,out int count)
		{
			return GetByCmsId(null, _cmsId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CMSDescription_CMS key.
		///		FK_CMSDescription_CMS Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cmsId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.CmsDescription objects.</returns>
		public abstract TList<CmsDescription> GetByCmsId(TransactionManager transactionManager, System.Int64 _cmsId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CMSDescription_Language key.
		///		FK_CMSDescription_Language Description: 
		/// </summary>
		/// <param name="_languageId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.CmsDescription objects.</returns>
		public TList<CmsDescription> GetByLanguageId(System.Int64 _languageId)
		{
			int count = -1;
			return GetByLanguageId(_languageId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CMSDescription_Language key.
		///		FK_CMSDescription_Language Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_languageId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.CmsDescription objects.</returns>
		/// <remarks></remarks>
		public TList<CmsDescription> GetByLanguageId(TransactionManager transactionManager, System.Int64 _languageId)
		{
			int count = -1;
			return GetByLanguageId(transactionManager, _languageId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CMSDescription_Language key.
		///		FK_CMSDescription_Language Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_languageId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.CmsDescription objects.</returns>
		public TList<CmsDescription> GetByLanguageId(TransactionManager transactionManager, System.Int64 _languageId, int start, int pageLength)
		{
			int count = -1;
			return GetByLanguageId(transactionManager, _languageId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CMSDescription_Language key.
		///		fkCmsDescriptionLanguage Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_languageId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.CmsDescription objects.</returns>
		public TList<CmsDescription> GetByLanguageId(System.Int64 _languageId, int start, int pageLength)
		{
			int count =  -1;
			return GetByLanguageId(null, _languageId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CMSDescription_Language key.
		///		fkCmsDescriptionLanguage Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_languageId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.CmsDescription objects.</returns>
		public TList<CmsDescription> GetByLanguageId(System.Int64 _languageId, int start, int pageLength,out int count)
		{
			return GetByLanguageId(null, _languageId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CMSDescription_Language key.
		///		FK_CMSDescription_Language Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_languageId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.CmsDescription objects.</returns>
		public abstract TList<CmsDescription> GetByLanguageId(TransactionManager transactionManager, System.Int64 _languageId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.CmsDescription Get(TransactionManager transactionManager, LMMR.Entities.CmsDescriptionKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CMS_Description index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.CmsDescription"/> class.</returns>
		public LMMR.Entities.CmsDescription GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CMS_Description index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.CmsDescription"/> class.</returns>
		public LMMR.Entities.CmsDescription GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CMS_Description index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.CmsDescription"/> class.</returns>
		public LMMR.Entities.CmsDescription GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CMS_Description index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.CmsDescription"/> class.</returns>
		public LMMR.Entities.CmsDescription GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CMS_Description index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.CmsDescription"/> class.</returns>
		public LMMR.Entities.CmsDescription GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CMS_Description index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.CmsDescription"/> class.</returns>
		public abstract LMMR.Entities.CmsDescription GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CmsDescription&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CmsDescription&gt;"/></returns>
		public static TList<CmsDescription> Fill(IDataReader reader, TList<CmsDescription> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.CmsDescription c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CmsDescription")
					.Append("|").Append((System.Int64)reader[((int)CmsDescriptionColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CmsDescription>(
					key.ToString(), // EntityTrackingKey
					"CmsDescription",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.CmsDescription();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)CmsDescriptionColumn.Id - 1)];
					c.CmsId = (System.Int64)reader[((int)CmsDescriptionColumn.CmsId - 1)];
					c.ContentTitle = (reader.IsDBNull(((int)CmsDescriptionColumn.ContentTitle - 1)))?null:(System.String)reader[((int)CmsDescriptionColumn.ContentTitle - 1)];
					c.ContentShortDesc = (reader.IsDBNull(((int)CmsDescriptionColumn.ContentShortDesc - 1)))?null:(System.String)reader[((int)CmsDescriptionColumn.ContentShortDesc - 1)];
					c.ContentsDesc = (reader.IsDBNull(((int)CmsDescriptionColumn.ContentsDesc - 1)))?null:(System.String)reader[((int)CmsDescriptionColumn.ContentsDesc - 1)];
					c.ContentImage = (reader.IsDBNull(((int)CmsDescriptionColumn.ContentImage - 1)))?null:(System.String)reader[((int)CmsDescriptionColumn.ContentImage - 1)];
					c.LanguageId = (System.Int64)reader[((int)CmsDescriptionColumn.LanguageId - 1)];
					c.PageUrl = (reader.IsDBNull(((int)CmsDescriptionColumn.PageUrl - 1)))?null:(System.String)reader[((int)CmsDescriptionColumn.PageUrl - 1)];
					c.UpdatedDate = (reader.IsDBNull(((int)CmsDescriptionColumn.UpdatedDate - 1)))?null:(System.DateTime?)reader[((int)CmsDescriptionColumn.UpdatedDate - 1)];
					c.ToDate = (reader.IsDBNull(((int)CmsDescriptionColumn.ToDate - 1)))?null:(System.DateTime?)reader[((int)CmsDescriptionColumn.ToDate - 1)];
					c.FromDate = (reader.IsDBNull(((int)CmsDescriptionColumn.FromDate - 1)))?null:(System.DateTime?)reader[((int)CmsDescriptionColumn.FromDate - 1)];
					c.CmsOrderBy = (reader.IsDBNull(((int)CmsDescriptionColumn.CmsOrderBy - 1)))?null:(System.Int32?)reader[((int)CmsDescriptionColumn.CmsOrderBy - 1)];
					c.IsActive = (System.Boolean)reader[((int)CmsDescriptionColumn.IsActive - 1)];
					c.MetaKeyword = (reader.IsDBNull(((int)CmsDescriptionColumn.MetaKeyword - 1)))?null:(System.String)reader[((int)CmsDescriptionColumn.MetaKeyword - 1)];
					c.MetaDescription = (reader.IsDBNull(((int)CmsDescriptionColumn.MetaDescription - 1)))?null:(System.String)reader[((int)CmsDescriptionColumn.MetaDescription - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.CmsDescription"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.CmsDescription"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.CmsDescription entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)CmsDescriptionColumn.Id - 1)];
			entity.CmsId = (System.Int64)reader[((int)CmsDescriptionColumn.CmsId - 1)];
			entity.ContentTitle = (reader.IsDBNull(((int)CmsDescriptionColumn.ContentTitle - 1)))?null:(System.String)reader[((int)CmsDescriptionColumn.ContentTitle - 1)];
			entity.ContentShortDesc = (reader.IsDBNull(((int)CmsDescriptionColumn.ContentShortDesc - 1)))?null:(System.String)reader[((int)CmsDescriptionColumn.ContentShortDesc - 1)];
			entity.ContentsDesc = (reader.IsDBNull(((int)CmsDescriptionColumn.ContentsDesc - 1)))?null:(System.String)reader[((int)CmsDescriptionColumn.ContentsDesc - 1)];
			entity.ContentImage = (reader.IsDBNull(((int)CmsDescriptionColumn.ContentImage - 1)))?null:(System.String)reader[((int)CmsDescriptionColumn.ContentImage - 1)];
			entity.LanguageId = (System.Int64)reader[((int)CmsDescriptionColumn.LanguageId - 1)];
			entity.PageUrl = (reader.IsDBNull(((int)CmsDescriptionColumn.PageUrl - 1)))?null:(System.String)reader[((int)CmsDescriptionColumn.PageUrl - 1)];
			entity.UpdatedDate = (reader.IsDBNull(((int)CmsDescriptionColumn.UpdatedDate - 1)))?null:(System.DateTime?)reader[((int)CmsDescriptionColumn.UpdatedDate - 1)];
			entity.ToDate = (reader.IsDBNull(((int)CmsDescriptionColumn.ToDate - 1)))?null:(System.DateTime?)reader[((int)CmsDescriptionColumn.ToDate - 1)];
			entity.FromDate = (reader.IsDBNull(((int)CmsDescriptionColumn.FromDate - 1)))?null:(System.DateTime?)reader[((int)CmsDescriptionColumn.FromDate - 1)];
			entity.CmsOrderBy = (reader.IsDBNull(((int)CmsDescriptionColumn.CmsOrderBy - 1)))?null:(System.Int32?)reader[((int)CmsDescriptionColumn.CmsOrderBy - 1)];
			entity.IsActive = (System.Boolean)reader[((int)CmsDescriptionColumn.IsActive - 1)];
			entity.MetaKeyword = (reader.IsDBNull(((int)CmsDescriptionColumn.MetaKeyword - 1)))?null:(System.String)reader[((int)CmsDescriptionColumn.MetaKeyword - 1)];
			entity.MetaDescription = (reader.IsDBNull(((int)CmsDescriptionColumn.MetaDescription - 1)))?null:(System.String)reader[((int)CmsDescriptionColumn.MetaDescription - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.CmsDescription"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.CmsDescription"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.CmsDescription entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.CmsId = (System.Int64)dataRow["CmsId"];
			entity.ContentTitle = Convert.IsDBNull(dataRow["ContentTitle"]) ? null : (System.String)dataRow["ContentTitle"];
			entity.ContentShortDesc = Convert.IsDBNull(dataRow["ContentShortDesc"]) ? null : (System.String)dataRow["ContentShortDesc"];
			entity.ContentsDesc = Convert.IsDBNull(dataRow["ContentsDesc"]) ? null : (System.String)dataRow["ContentsDesc"];
			entity.ContentImage = Convert.IsDBNull(dataRow["ContentImage"]) ? null : (System.String)dataRow["ContentImage"];
			entity.LanguageId = (System.Int64)dataRow["LanguageId"];
			entity.PageUrl = Convert.IsDBNull(dataRow["PageURL"]) ? null : (System.String)dataRow["PageURL"];
			entity.UpdatedDate = Convert.IsDBNull(dataRow["UpdatedDate"]) ? null : (System.DateTime?)dataRow["UpdatedDate"];
			entity.ToDate = Convert.IsDBNull(dataRow["ToDate"]) ? null : (System.DateTime?)dataRow["ToDate"];
			entity.FromDate = Convert.IsDBNull(dataRow["FromDate"]) ? null : (System.DateTime?)dataRow["FromDate"];
			entity.CmsOrderBy = Convert.IsDBNull(dataRow["CmsOrderBy"]) ? null : (System.Int32?)dataRow["CmsOrderBy"];
			entity.IsActive = (System.Boolean)dataRow["IsActive"];
			entity.MetaKeyword = Convert.IsDBNull(dataRow["MetaKeyword"]) ? null : (System.String)dataRow["MetaKeyword"];
			entity.MetaDescription = Convert.IsDBNull(dataRow["MetaDescription"]) ? null : (System.String)dataRow["MetaDescription"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.CmsDescription"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.CmsDescription Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.CmsDescription entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CmsIdSource	
			if (CanDeepLoad(entity, "Cms|CmsIdSource", deepLoadType, innerList) 
				&& entity.CmsIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CmsId;
				Cms tmpEntity = EntityManager.LocateEntity<Cms>(EntityLocator.ConstructKeyFromPkItems(typeof(Cms), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CmsIdSource = tmpEntity;
				else
					entity.CmsIdSource = DataRepository.CmsProvider.GetById(transactionManager, entity.CmsId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CmsIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CmsIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CmsProvider.DeepLoad(transactionManager, entity.CmsIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CmsIdSource

			#region LanguageIdSource	
			if (CanDeepLoad(entity, "Language|LanguageIdSource", deepLoadType, innerList) 
				&& entity.LanguageIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.LanguageId;
				Language tmpEntity = EntityManager.LocateEntity<Language>(EntityLocator.ConstructKeyFromPkItems(typeof(Language), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.LanguageIdSource = tmpEntity;
				else
					entity.LanguageIdSource = DataRepository.LanguageProvider.GetById(transactionManager, entity.LanguageId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'LanguageIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.LanguageIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.LanguageProvider.DeepLoad(transactionManager, entity.LanguageIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion LanguageIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.CmsDescription object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.CmsDescription instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.CmsDescription Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.CmsDescription entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CmsIdSource
			if (CanDeepSave(entity, "Cms|CmsIdSource", deepSaveType, innerList) 
				&& entity.CmsIdSource != null)
			{
				DataRepository.CmsProvider.Save(transactionManager, entity.CmsIdSource);
				entity.CmsId = entity.CmsIdSource.Id;
			}
			#endregion 
			
			#region LanguageIdSource
			if (CanDeepSave(entity, "Language|LanguageIdSource", deepSaveType, innerList) 
				&& entity.LanguageIdSource != null)
			{
				DataRepository.LanguageProvider.Save(transactionManager, entity.LanguageIdSource);
				entity.LanguageId = entity.LanguageIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CmsDescriptionChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.CmsDescription</c>
	///</summary>
	public enum CmsDescriptionChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Cms</c> at CmsIdSource
		///</summary>
		[ChildEntityType(typeof(Cms))]
		Cms,
			
		///<summary>
		/// Composite Property for <c>Language</c> at LanguageIdSource
		///</summary>
		[ChildEntityType(typeof(Language))]
		Language,
		}
	
	#endregion CmsDescriptionChildEntityTypes
	
	#region CmsDescriptionFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CmsDescriptionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CmsDescription"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CmsDescriptionFilterBuilder : SqlFilterBuilder<CmsDescriptionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CmsDescriptionFilterBuilder class.
		/// </summary>
		public CmsDescriptionFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CmsDescriptionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CmsDescriptionFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CmsDescriptionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CmsDescriptionFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CmsDescriptionFilterBuilder
	
	#region CmsDescriptionParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CmsDescriptionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CmsDescription"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CmsDescriptionParameterBuilder : ParameterizedSqlFilterBuilder<CmsDescriptionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CmsDescriptionParameterBuilder class.
		/// </summary>
		public CmsDescriptionParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CmsDescriptionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CmsDescriptionParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CmsDescriptionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CmsDescriptionParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CmsDescriptionParameterBuilder
	
	#region CmsDescriptionSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CmsDescriptionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CmsDescription"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CmsDescriptionSortBuilder : SqlSortBuilder<CmsDescriptionColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CmsDescriptionSqlSortBuilder class.
		/// </summary>
		public CmsDescriptionSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CmsDescriptionSortBuilder
	
} // end namespace
