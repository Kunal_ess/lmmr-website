﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="BedRoomDescProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class BedRoomDescProviderBaseCore : EntityProviderBase<LMMR.Entities.BedRoomDesc, LMMR.Entities.BedRoomDescKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.BedRoomDescKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomDesc_BedRoom key.
		///		FK_BedRoomDesc_BedRoom Description: 
		/// </summary>
		/// <param name="_bedRoomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomDesc objects.</returns>
		public TList<BedRoomDesc> GetByBedRoomId(System.Int64 _bedRoomId)
		{
			int count = -1;
			return GetByBedRoomId(_bedRoomId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomDesc_BedRoom key.
		///		FK_BedRoomDesc_BedRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bedRoomId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomDesc objects.</returns>
		/// <remarks></remarks>
		public TList<BedRoomDesc> GetByBedRoomId(TransactionManager transactionManager, System.Int64 _bedRoomId)
		{
			int count = -1;
			return GetByBedRoomId(transactionManager, _bedRoomId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomDesc_BedRoom key.
		///		FK_BedRoomDesc_BedRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bedRoomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomDesc objects.</returns>
		public TList<BedRoomDesc> GetByBedRoomId(TransactionManager transactionManager, System.Int64 _bedRoomId, int start, int pageLength)
		{
			int count = -1;
			return GetByBedRoomId(transactionManager, _bedRoomId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomDesc_BedRoom key.
		///		fkBedRoomDescBedRoom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bedRoomId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomDesc objects.</returns>
		public TList<BedRoomDesc> GetByBedRoomId(System.Int64 _bedRoomId, int start, int pageLength)
		{
			int count =  -1;
			return GetByBedRoomId(null, _bedRoomId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomDesc_BedRoom key.
		///		fkBedRoomDescBedRoom Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_bedRoomId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomDesc objects.</returns>
		public TList<BedRoomDesc> GetByBedRoomId(System.Int64 _bedRoomId, int start, int pageLength,out int count)
		{
			return GetByBedRoomId(null, _bedRoomId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomDesc_BedRoom key.
		///		FK_BedRoomDesc_BedRoom Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_bedRoomId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomDesc objects.</returns>
		public abstract TList<BedRoomDesc> GetByBedRoomId(TransactionManager transactionManager, System.Int64 _bedRoomId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomDesc_Language key.
		///		FK_BedRoomDesc_Language Description: 
		/// </summary>
		/// <param name="_languageId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomDesc objects.</returns>
		public TList<BedRoomDesc> GetByLanguageId(System.Int64 _languageId)
		{
			int count = -1;
			return GetByLanguageId(_languageId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomDesc_Language key.
		///		FK_BedRoomDesc_Language Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_languageId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomDesc objects.</returns>
		/// <remarks></remarks>
		public TList<BedRoomDesc> GetByLanguageId(TransactionManager transactionManager, System.Int64 _languageId)
		{
			int count = -1;
			return GetByLanguageId(transactionManager, _languageId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomDesc_Language key.
		///		FK_BedRoomDesc_Language Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_languageId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomDesc objects.</returns>
		public TList<BedRoomDesc> GetByLanguageId(TransactionManager transactionManager, System.Int64 _languageId, int start, int pageLength)
		{
			int count = -1;
			return GetByLanguageId(transactionManager, _languageId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomDesc_Language key.
		///		fkBedRoomDescLanguage Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_languageId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomDesc objects.</returns>
		public TList<BedRoomDesc> GetByLanguageId(System.Int64 _languageId, int start, int pageLength)
		{
			int count =  -1;
			return GetByLanguageId(null, _languageId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomDesc_Language key.
		///		fkBedRoomDescLanguage Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_languageId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomDesc objects.</returns>
		public TList<BedRoomDesc> GetByLanguageId(System.Int64 _languageId, int start, int pageLength,out int count)
		{
			return GetByLanguageId(null, _languageId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomDesc_Language key.
		///		FK_BedRoomDesc_Language Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_languageId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomDesc objects.</returns>
		public abstract TList<BedRoomDesc> GetByLanguageId(TransactionManager transactionManager, System.Int64 _languageId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomDesc_Users key.
		///		FK_BedRoomDesc_Users Description: 
		/// </summary>
		/// <param name="_updatedBy"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomDesc objects.</returns>
		public TList<BedRoomDesc> GetByUpdatedBy(System.Int64? _updatedBy)
		{
			int count = -1;
			return GetByUpdatedBy(_updatedBy, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomDesc_Users key.
		///		FK_BedRoomDesc_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_updatedBy"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomDesc objects.</returns>
		/// <remarks></remarks>
		public TList<BedRoomDesc> GetByUpdatedBy(TransactionManager transactionManager, System.Int64? _updatedBy)
		{
			int count = -1;
			return GetByUpdatedBy(transactionManager, _updatedBy, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomDesc_Users key.
		///		FK_BedRoomDesc_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_updatedBy"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomDesc objects.</returns>
		public TList<BedRoomDesc> GetByUpdatedBy(TransactionManager transactionManager, System.Int64? _updatedBy, int start, int pageLength)
		{
			int count = -1;
			return GetByUpdatedBy(transactionManager, _updatedBy, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomDesc_Users key.
		///		fkBedRoomDescUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_updatedBy"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomDesc objects.</returns>
		public TList<BedRoomDesc> GetByUpdatedBy(System.Int64? _updatedBy, int start, int pageLength)
		{
			int count =  -1;
			return GetByUpdatedBy(null, _updatedBy, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomDesc_Users key.
		///		fkBedRoomDescUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_updatedBy"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomDesc objects.</returns>
		public TList<BedRoomDesc> GetByUpdatedBy(System.Int64? _updatedBy, int start, int pageLength,out int count)
		{
			return GetByUpdatedBy(null, _updatedBy, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_BedRoomDesc_Users key.
		///		FK_BedRoomDesc_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_updatedBy"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.BedRoomDesc objects.</returns>
		public abstract TList<BedRoomDesc> GetByUpdatedBy(TransactionManager transactionManager, System.Int64? _updatedBy, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.BedRoomDesc Get(TransactionManager transactionManager, LMMR.Entities.BedRoomDescKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_BedRoomDesc index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomDesc"/> class.</returns>
		public LMMR.Entities.BedRoomDesc GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoomDesc index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomDesc"/> class.</returns>
		public LMMR.Entities.BedRoomDesc GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoomDesc index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomDesc"/> class.</returns>
		public LMMR.Entities.BedRoomDesc GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoomDesc index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomDesc"/> class.</returns>
		public LMMR.Entities.BedRoomDesc GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoomDesc index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomDesc"/> class.</returns>
		public LMMR.Entities.BedRoomDesc GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_BedRoomDesc index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.BedRoomDesc"/> class.</returns>
		public abstract LMMR.Entities.BedRoomDesc GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;BedRoomDesc&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;BedRoomDesc&gt;"/></returns>
		public static TList<BedRoomDesc> Fill(IDataReader reader, TList<BedRoomDesc> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.BedRoomDesc c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("BedRoomDesc")
					.Append("|").Append((System.Int64)reader[((int)BedRoomDescColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<BedRoomDesc>(
					key.ToString(), // EntityTrackingKey
					"BedRoomDesc",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.BedRoomDesc();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)BedRoomDescColumn.Id - 1)];
					c.BedRoomId = (System.Int64)reader[((int)BedRoomDescColumn.BedRoomId - 1)];
					c.LanguageId = (System.Int64)reader[((int)BedRoomDescColumn.LanguageId - 1)];
					c.Description = (reader.IsDBNull(((int)BedRoomDescColumn.Description - 1)))?null:(System.String)reader[((int)BedRoomDescColumn.Description - 1)];
					c.UpdatedBy = (reader.IsDBNull(((int)BedRoomDescColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)BedRoomDescColumn.UpdatedBy - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.BedRoomDesc"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BedRoomDesc"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.BedRoomDesc entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)BedRoomDescColumn.Id - 1)];
			entity.BedRoomId = (System.Int64)reader[((int)BedRoomDescColumn.BedRoomId - 1)];
			entity.LanguageId = (System.Int64)reader[((int)BedRoomDescColumn.LanguageId - 1)];
			entity.Description = (reader.IsDBNull(((int)BedRoomDescColumn.Description - 1)))?null:(System.String)reader[((int)BedRoomDescColumn.Description - 1)];
			entity.UpdatedBy = (reader.IsDBNull(((int)BedRoomDescColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)BedRoomDescColumn.UpdatedBy - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.BedRoomDesc"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BedRoomDesc"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.BedRoomDesc entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.BedRoomId = (System.Int64)dataRow["BedRoomId"];
			entity.LanguageId = (System.Int64)dataRow["LanguageId"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.UpdatedBy = Convert.IsDBNull(dataRow["UpdatedBy"]) ? null : (System.Int64?)dataRow["UpdatedBy"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.BedRoomDesc"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.BedRoomDesc Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.BedRoomDesc entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region BedRoomIdSource	
			if (CanDeepLoad(entity, "BedRoom|BedRoomIdSource", deepLoadType, innerList) 
				&& entity.BedRoomIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.BedRoomId;
				BedRoom tmpEntity = EntityManager.LocateEntity<BedRoom>(EntityLocator.ConstructKeyFromPkItems(typeof(BedRoom), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.BedRoomIdSource = tmpEntity;
				else
					entity.BedRoomIdSource = DataRepository.BedRoomProvider.GetById(transactionManager, entity.BedRoomId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BedRoomIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.BedRoomIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.BedRoomProvider.DeepLoad(transactionManager, entity.BedRoomIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion BedRoomIdSource

			#region LanguageIdSource	
			if (CanDeepLoad(entity, "Language|LanguageIdSource", deepLoadType, innerList) 
				&& entity.LanguageIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.LanguageId;
				Language tmpEntity = EntityManager.LocateEntity<Language>(EntityLocator.ConstructKeyFromPkItems(typeof(Language), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.LanguageIdSource = tmpEntity;
				else
					entity.LanguageIdSource = DataRepository.LanguageProvider.GetById(transactionManager, entity.LanguageId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'LanguageIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.LanguageIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.LanguageProvider.DeepLoad(transactionManager, entity.LanguageIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion LanguageIdSource

			#region UpdatedBySource	
			if (CanDeepLoad(entity, "Users|UpdatedBySource", deepLoadType, innerList) 
				&& entity.UpdatedBySource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.UpdatedBy ?? (long)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.UpdatedBySource = tmpEntity;
				else
					entity.UpdatedBySource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.UpdatedBy ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UpdatedBySource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.UpdatedBySource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.UpdatedBySource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion UpdatedBySource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region BedRoomDescTrailCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<BedRoomDescTrail>|BedRoomDescTrailCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'BedRoomDescTrailCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.BedRoomDescTrailCollection = DataRepository.BedRoomDescTrailProvider.GetByBedRoomDescId(transactionManager, entity.Id);

				if (deep && entity.BedRoomDescTrailCollection.Count > 0)
				{
					deepHandles.Add("BedRoomDescTrailCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<BedRoomDescTrail>) DataRepository.BedRoomDescTrailProvider.DeepLoad,
						new object[] { transactionManager, entity.BedRoomDescTrailCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.BedRoomDesc object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.BedRoomDesc instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.BedRoomDesc Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.BedRoomDesc entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region BedRoomIdSource
			if (CanDeepSave(entity, "BedRoom|BedRoomIdSource", deepSaveType, innerList) 
				&& entity.BedRoomIdSource != null)
			{
				DataRepository.BedRoomProvider.Save(transactionManager, entity.BedRoomIdSource);
				entity.BedRoomId = entity.BedRoomIdSource.Id;
			}
			#endregion 
			
			#region LanguageIdSource
			if (CanDeepSave(entity, "Language|LanguageIdSource", deepSaveType, innerList) 
				&& entity.LanguageIdSource != null)
			{
				DataRepository.LanguageProvider.Save(transactionManager, entity.LanguageIdSource);
				entity.LanguageId = entity.LanguageIdSource.Id;
			}
			#endregion 
			
			#region UpdatedBySource
			if (CanDeepSave(entity, "Users|UpdatedBySource", deepSaveType, innerList) 
				&& entity.UpdatedBySource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.UpdatedBySource);
				entity.UpdatedBy = entity.UpdatedBySource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<BedRoomDescTrail>
				if (CanDeepSave(entity.BedRoomDescTrailCollection, "List<BedRoomDescTrail>|BedRoomDescTrailCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(BedRoomDescTrail child in entity.BedRoomDescTrailCollection)
					{
						if(child.BedRoomDescIdSource != null)
						{
							child.BedRoomDescId = child.BedRoomDescIdSource.Id;
						}
						else
						{
							child.BedRoomDescId = entity.Id;
						}

					}

					if (entity.BedRoomDescTrailCollection.Count > 0 || entity.BedRoomDescTrailCollection.DeletedItems.Count > 0)
					{
						//DataRepository.BedRoomDescTrailProvider.Save(transactionManager, entity.BedRoomDescTrailCollection);
						
						deepHandles.Add("BedRoomDescTrailCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< BedRoomDescTrail >) DataRepository.BedRoomDescTrailProvider.DeepSave,
							new object[] { transactionManager, entity.BedRoomDescTrailCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region BedRoomDescChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.BedRoomDesc</c>
	///</summary>
	public enum BedRoomDescChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>BedRoom</c> at BedRoomIdSource
		///</summary>
		[ChildEntityType(typeof(BedRoom))]
		BedRoom,
			
		///<summary>
		/// Composite Property for <c>Language</c> at LanguageIdSource
		///</summary>
		[ChildEntityType(typeof(Language))]
		Language,
			
		///<summary>
		/// Composite Property for <c>Users</c> at UpdatedBySource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
	
		///<summary>
		/// Collection of <c>BedRoomDesc</c> as OneToMany for BedRoomDescTrailCollection
		///</summary>
		[ChildEntityType(typeof(TList<BedRoomDescTrail>))]
		BedRoomDescTrailCollection,
	}
	
	#endregion BedRoomDescChildEntityTypes
	
	#region BedRoomDescFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;BedRoomDescColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BedRoomDesc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BedRoomDescFilterBuilder : SqlFilterBuilder<BedRoomDescColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomDescFilterBuilder class.
		/// </summary>
		public BedRoomDescFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BedRoomDescFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BedRoomDescFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BedRoomDescFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BedRoomDescFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BedRoomDescFilterBuilder
	
	#region BedRoomDescParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;BedRoomDescColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BedRoomDesc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class BedRoomDescParameterBuilder : ParameterizedSqlFilterBuilder<BedRoomDescColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomDescParameterBuilder class.
		/// </summary>
		public BedRoomDescParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the BedRoomDescParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public BedRoomDescParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the BedRoomDescParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public BedRoomDescParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion BedRoomDescParameterBuilder
	
	#region BedRoomDescSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;BedRoomDescColumn&gt;"/> class
	/// that is used exclusively with a <see cref="BedRoomDesc"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class BedRoomDescSortBuilder : SqlSortBuilder<BedRoomDescColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the BedRoomDescSqlSortBuilder class.
		/// </summary>
		public BedRoomDescSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion BedRoomDescSortBuilder
	
} // end namespace
