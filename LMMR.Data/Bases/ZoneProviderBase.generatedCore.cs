﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ZoneProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ZoneProviderBaseCore : EntityProviderBase<LMMR.Entities.Zone, LMMR.Entities.ZoneKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.ZoneKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Zone_City key.
		///		FK_Zone_City Description: 
		/// </summary>
		/// <param name="_cityId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Zone objects.</returns>
		public TList<Zone> GetByCityId(System.Int64 _cityId)
		{
			int count = -1;
			return GetByCityId(_cityId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Zone_City key.
		///		FK_Zone_City Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cityId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Zone objects.</returns>
		/// <remarks></remarks>
		public TList<Zone> GetByCityId(TransactionManager transactionManager, System.Int64 _cityId)
		{
			int count = -1;
			return GetByCityId(transactionManager, _cityId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Zone_City key.
		///		FK_Zone_City Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cityId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Zone objects.</returns>
		public TList<Zone> GetByCityId(TransactionManager transactionManager, System.Int64 _cityId, int start, int pageLength)
		{
			int count = -1;
			return GetByCityId(transactionManager, _cityId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Zone_City key.
		///		fkZoneCity Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_cityId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Zone objects.</returns>
		public TList<Zone> GetByCityId(System.Int64 _cityId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCityId(null, _cityId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Zone_City key.
		///		fkZoneCity Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_cityId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Zone objects.</returns>
		public TList<Zone> GetByCityId(System.Int64 _cityId, int start, int pageLength,out int count)
		{
			return GetByCityId(null, _cityId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Zone_City key.
		///		FK_Zone_City Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cityId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.Zone objects.</returns>
		public abstract TList<Zone> GetByCityId(TransactionManager transactionManager, System.Int64 _cityId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.Zone Get(TransactionManager transactionManager, LMMR.Entities.ZoneKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Zone index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Zone"/> class.</returns>
		public LMMR.Entities.Zone GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Zone index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Zone"/> class.</returns>
		public LMMR.Entities.Zone GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Zone index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Zone"/> class.</returns>
		public LMMR.Entities.Zone GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Zone index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Zone"/> class.</returns>
		public LMMR.Entities.Zone GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Zone index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Zone"/> class.</returns>
		public LMMR.Entities.Zone GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Zone index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Zone"/> class.</returns>
		public abstract LMMR.Entities.Zone GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Zone&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Zone&gt;"/></returns>
		public static TList<Zone> Fill(IDataReader reader, TList<Zone> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.Zone c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Zone")
					.Append("|").Append((System.Int64)reader[((int)ZoneColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Zone>(
					key.ToString(), // EntityTrackingKey
					"Zone",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.Zone();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)ZoneColumn.Id - 1)];
					c.CityId = (System.Int64)reader[((int)ZoneColumn.CityId - 1)];
					c.Zone = (reader.IsDBNull(((int)ZoneColumn.Zone - 1)))?null:(System.String)reader[((int)ZoneColumn.Zone - 1)];
					c.IsActive = (reader.IsDBNull(((int)ZoneColumn.IsActive - 1)))?null:(System.Boolean?)reader[((int)ZoneColumn.IsActive - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Zone"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Zone"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.Zone entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)ZoneColumn.Id - 1)];
			entity.CityId = (System.Int64)reader[((int)ZoneColumn.CityId - 1)];
			entity.Zone = (reader.IsDBNull(((int)ZoneColumn.Zone - 1)))?null:(System.String)reader[((int)ZoneColumn.Zone - 1)];
			entity.IsActive = (reader.IsDBNull(((int)ZoneColumn.IsActive - 1)))?null:(System.Boolean?)reader[((int)ZoneColumn.IsActive - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Zone"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Zone"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.Zone entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.CityId = (System.Int64)dataRow["CityId"];
			entity.Zone = Convert.IsDBNull(dataRow["Zone"]) ? null : (System.String)dataRow["Zone"];
			entity.IsActive = Convert.IsDBNull(dataRow["IsActive"]) ? null : (System.Boolean?)dataRow["IsActive"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Zone"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.Zone Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.Zone entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CityIdSource	
			if (CanDeepLoad(entity, "City|CityIdSource", deepLoadType, innerList) 
				&& entity.CityIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CityId;
				City tmpEntity = EntityManager.LocateEntity<City>(EntityLocator.ConstructKeyFromPkItems(typeof(City), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CityIdSource = tmpEntity;
				else
					entity.CityIdSource = DataRepository.CityProvider.GetById(transactionManager, entity.CityId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CityIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CityIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CityProvider.DeepLoad(transactionManager, entity.CityIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CityIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region FrontEndBottomCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<FrontEndBottom>|FrontEndBottomCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FrontEndBottomCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.FrontEndBottomCollection = DataRepository.FrontEndBottomProvider.GetByZoneId(transactionManager, entity.Id);

				if (deep && entity.FrontEndBottomCollection.Count > 0)
				{
					deepHandles.Add("FrontEndBottomCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<FrontEndBottom>) DataRepository.FrontEndBottomProvider.DeepLoad,
						new object[] { transactionManager, entity.FrontEndBottomCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ZoneLanguageCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ZoneLanguage>|ZoneLanguageCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ZoneLanguageCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ZoneLanguageCollection = DataRepository.ZoneLanguageProvider.GetByZoneId(transactionManager, entity.Id);

				if (deep && entity.ZoneLanguageCollection.Count > 0)
				{
					deepHandles.Add("ZoneLanguageCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ZoneLanguage>) DataRepository.ZoneLanguageProvider.DeepLoad,
						new object[] { transactionManager, entity.ZoneLanguageCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.Zone object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.Zone instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.Zone Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.Zone entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CityIdSource
			if (CanDeepSave(entity, "City|CityIdSource", deepSaveType, innerList) 
				&& entity.CityIdSource != null)
			{
				DataRepository.CityProvider.Save(transactionManager, entity.CityIdSource);
				entity.CityId = entity.CityIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<FrontEndBottom>
				if (CanDeepSave(entity.FrontEndBottomCollection, "List<FrontEndBottom>|FrontEndBottomCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(FrontEndBottom child in entity.FrontEndBottomCollection)
					{
						if(child.ZoneIdSource != null)
						{
							child.ZoneId = child.ZoneIdSource.Id;
						}
						else
						{
							child.ZoneId = entity.Id;
						}

					}

					if (entity.FrontEndBottomCollection.Count > 0 || entity.FrontEndBottomCollection.DeletedItems.Count > 0)
					{
						//DataRepository.FrontEndBottomProvider.Save(transactionManager, entity.FrontEndBottomCollection);
						
						deepHandles.Add("FrontEndBottomCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< FrontEndBottom >) DataRepository.FrontEndBottomProvider.DeepSave,
							new object[] { transactionManager, entity.FrontEndBottomCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ZoneLanguage>
				if (CanDeepSave(entity.ZoneLanguageCollection, "List<ZoneLanguage>|ZoneLanguageCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ZoneLanguage child in entity.ZoneLanguageCollection)
					{
						if(child.ZoneIdSource != null)
						{
							child.ZoneId = child.ZoneIdSource.Id;
						}
						else
						{
							child.ZoneId = entity.Id;
						}

					}

					if (entity.ZoneLanguageCollection.Count > 0 || entity.ZoneLanguageCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ZoneLanguageProvider.Save(transactionManager, entity.ZoneLanguageCollection);
						
						deepHandles.Add("ZoneLanguageCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ZoneLanguage >) DataRepository.ZoneLanguageProvider.DeepSave,
							new object[] { transactionManager, entity.ZoneLanguageCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ZoneChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.Zone</c>
	///</summary>
	public enum ZoneChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>City</c> at CityIdSource
		///</summary>
		[ChildEntityType(typeof(City))]
		City,
	
		///<summary>
		/// Collection of <c>Zone</c> as OneToMany for FrontEndBottomCollection
		///</summary>
		[ChildEntityType(typeof(TList<FrontEndBottom>))]
		FrontEndBottomCollection,

		///<summary>
		/// Collection of <c>Zone</c> as OneToMany for ZoneLanguageCollection
		///</summary>
		[ChildEntityType(typeof(TList<ZoneLanguage>))]
		ZoneLanguageCollection,
	}
	
	#endregion ZoneChildEntityTypes
	
	#region ZoneFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ZoneColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Zone"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ZoneFilterBuilder : SqlFilterBuilder<ZoneColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ZoneFilterBuilder class.
		/// </summary>
		public ZoneFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ZoneFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ZoneFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ZoneFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ZoneFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ZoneFilterBuilder
	
	#region ZoneParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ZoneColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Zone"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ZoneParameterBuilder : ParameterizedSqlFilterBuilder<ZoneColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ZoneParameterBuilder class.
		/// </summary>
		public ZoneParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ZoneParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ZoneParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ZoneParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ZoneParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ZoneParameterBuilder
	
	#region ZoneSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ZoneColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Zone"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ZoneSortBuilder : SqlSortBuilder<ZoneColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ZoneSqlSortBuilder class.
		/// </summary>
		public ZoneSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ZoneSortBuilder
	
} // end namespace
