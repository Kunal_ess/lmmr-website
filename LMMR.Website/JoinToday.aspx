﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeMaster.master" AutoEventWireup="true"
    CodeFile="JoinToday.aspx.cs" Inherits="Default2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="UserControl/Frontend/HotelOfTheWeek.ascx" TagName="HotelOfTheWeek"
    TagPrefix="uc1" %>
<%@ Register Src="UserControl/Frontend/Benefits.ascx" TagName="Benefits" TagPrefix="uc2" %>
<%@ Register Src="UserControl/Frontend/RecentlyJoinedHotel.ascx" TagName="RecentlyJoinedHotel"
    TagPrefix="uc3" %>
<%@ Register Src="UserControl/Frontend/NewsSubscriber.ascx" TagName="NewsSubscriber"
    TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntLeft" runat="Server">
    <uc1:HotelOfTheWeek ID="HotelOfTheWeek2" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMainbody" runat="Server">
    <style type="text/css">
        .modalBackground
        {
            background-color: #CCCCFF;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }
        .water
        {
            color: Gray;
        }
    </style>
    <div class="mainbody-left">
        <div class="banner">
            <iframe height="100%" src="<%= SiteRootPath %>SlideShow.aspx" width="100%"></iframe>
        </div>
        <div class="jointoday">
            <span style="font-size: large">
                <%=GetKeyResult("PARTNER") %>
                : </span>
            <div class="jointoday-intro">
                <asp:Label ID="lblpartener" runat="server"></asp:Label>
            </div>
            <div class="jointoday-btn-body" style="margin-left: 78px">
                <div class="n-btn2">
                    <asp:LinkButton ID="btnRegisterAsHotel" class="register-hotel-btn" runat="server">
                     <div class="n-btn-left"></div>                    
                     <div  class="n-btn-mid"><%=GetKeyResult("REGISTERVENUE")%></div>                    
                     <div class="n-btn-right"></div>
                    </asp:LinkButton>
                </div>
                <div style="display: none">
                    <div class="n-g-btn3">
                        <asp:LinkButton ID="btnRegisterAsAgency" runat="server" class="register-btn" OnClick="btnRegisterAsAgency_Click"
                            Enabled="false">
                     <div class="n-g-btn-left"></div>                    
                     <div  class="n-g-btn-mid"><%=GetKeyResult("REGISTERAGENCY")%></div>                 
                     <div class="n-g-btn-right"></div>
                        </asp:LinkButton>
                    </div>
                </div>
        </div>
        <br><span style="font-size: large">
            <%=GetKeyResult("CLIENTS") %>
            : </span>
            <div class="jointoday-intro">
                <asp:Label ID="lblUsers" runat="server"></asp:Label>
            </div>
            <div class="jointoday-btn-body">
                <div class="n-btn" id="NormalbtnCompanyRegistrationDiv" runat="server">
                    <asp:LinkButton ID="btnCompanyRegistration" runat="server" class="register-btn1"
                        OnClick="btnCompanyRegistration_Click">
                     <div class="n-btn-left"></div>                    
                     <div  class="n-btn-mid"><%=GetKeyResult("REGISTERASCORPORATE")%></div>                    
                     <div class="n-btn-right"></div>
                    </asp:LinkButton>
                </div>
                <div class="n-g-btn" id="DisabledbtnCompanyRegistrationDiv" runat="server">
                    <asp:LinkButton ID="LinkButton1" runat="server" class="register-btn1" OnClientClick="alert('You are already registered.');return false;"
                        OnClick="btnCompanyRegistration_Click">
                     <div class="n-g-btn-left"></div>                    
                     <div  class="n-g-btn-mid"><%=GetKeyResult("REGISTERASCORPORATE")%></div>                    
                     <div class="n-g-btn-right"></div>
                    </asp:LinkButton>
                </div>
                <div class="n-btn1" id="normalbtnUserRegistrationDiv" runat="server">
                    <asp:LinkButton ID="btnUserRegistration" runat="server" class="register-hote2-btn"
                        OnClick="btnUserRegistration_Click">
                        <div class="n-btn-left">
                        </div>
                        <div class="n-btn-mid">
                            <%=GetKeyResult("REGISTERASINDIVIDUAL")%></div>
                        <div class="n-btn-right">
                        </div>
                    </asp:LinkButton>
                </div>
                <div class="n-g-btn1" id="disabledbtnUserRegistrationDiv" runat="server">
                    <asp:LinkButton ID="LinkButton2" runat="server" class="register-hote2-btn" OnClientClick="return registeredMessage();"
                        OnClick="btnUserRegistration_Click">
                        <div class="n-g-btn-left">
                        </div>
                        <div class="n-g-btn-mid">
                            <%=GetKeyResult("REGISTERASINDIVIDUAL")%></div>
                        <div class="n-g-btn-right">
                        </div>
                    </asp:LinkButton>
                </div>
            </div>
        </br>
        <div style="display: none">
            <span style="font-size: large">
                <%=GetKeyResult("REGISTERASTRAVELAGENCY")%>
                : </span>
            <div class="jointoday-intro">
                <%=GetKeyResult("FORMOREINFORMATION")%>
            </div>
            <div class="jointoday-btn-body">
            </div>
        </div>
    </div>
    </div>
    <div class="mainbody-right">
        <div id="beforeLogin" runat="server">
            <div class="mainbody-right-call">
                <div class="need">
                    <span>
                        <%= GetKeyResult("NEEDHELP")%></span>
                    <br />
                    <%= GetKeyResult("CALLUS")%>
                    5/7
                </div>
                <div class="phone">
                    +32 2 344 25 50 </div>
                <p style="font-size: 10px; color: White; text-align: center">
                    <%= GetKeyResult("CALLUSANDWEDOITFORYOU")%></p>
               <div class="mail">
                        <a href="http://needameetingroom.com/requestform/" onClick="javascript: _gaq.push(['_trackPageview', '/email']); window.open(this.href, 'popupwindow1','width=900,height=600,scrollbars,resizable,toolbar=NO'); &#13;&#10;return false;" Target="_blank"">
                        Send us your RFP</a>
                    </div>
            </div>
            <div class="mainbody-right-join" id="divjoinus" runat="server">
               <div class="join">
                    <a runat="server" id="joinToday">
                        <%= GetKeyResult("JOINUSTODAY")%></a></div>
                <%= GetKeyResult("FORHOTELSMEETINGFACILITIESEVENT")%>
            </div>
            <div class="mainbody-right-you">
                <a href="http://www.youtube.com/watch?v=rkt3NIWecG8&feature=youtu.be" target="_blank">
                    <img src="<%= SiteRootPath %>images/youtube-icon.png" /></a>&nbsp;&nbsp;<a href="http://www.youtube.com/watch?v=U3y7d7yxmvE&feature=relmfu"
                        target="_blank"><img src="<%= SiteRootPath %>images/small-vedio.png" /></a>
            </div>
        </div>
        <div style="width: 216px; height: auto; overflow: hidden; margin: 0px auto; display: none"
            id="afterLogin" runat="server">
            <div class="mainbody-right-call">
                <div class="need">
                    <span>
                        <%= GetKeyResult("NEEDHELP")%></span>
                    <br />
                    <%= GetKeyResult("CALLUS")%>
                    5/7
                </div>
                <div class="phone">
                    +32 2 344 25 50 </div>
                <p style="font-size: 10px; color: White; text-align: center">
                    <%= GetKeyResult("CALLUSANDWEDOITFORYOU")%></p>
                <div class="mail">
                        <a href="http://needameetingroom.com/requestform/" onClick="javascript: _gaq.push(['_trackPageview', '/email']); window.open(this.href, 'popupwindow1','width=900,height=600,scrollbars,resizable,toolbar=NO'); &#13;&#10;return false;" Target="_blank"">
                        Send us your RFP</a>
                    </div>
            </div>
            <!--start mainbody-right-afterlogin-body -->
            <div class="mainbody-right-afterlogin-body">
                <div class="mainbody-right-afterlogin-top">
                    <div class="mainbody-right-afterlogin-inner">
                        <b>
                            <%= GetKeyResult("TODAY")%>
                            :</b>
                        <asp:Label ID="lstLoginTime" runat="server"></asp:Label>
                        <div class="afterlogin-welcome">
                            <span>
                                <%= GetKeyResult("WELCOME")%></span>
                            <br>
                            <asp:Label ID="lblLoginUserName" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="mainbody-right-afterlogin-bottom">
                    <div class="mainbody-right-afterlogin-bottom-inner">
                        <asp:LinkButton ID="hypManageProfile" runat="server" OnClick="hypManageProfile_Click"><%= GetKeyResult("MANAGEPROFILE")%></asp:LinkButton>
                        <br />
                        <asp:LinkButton ID="hypChangepassword" runat="server" OnClick="hypChangepassword_Click"><%= GetKeyResult("CHANGEPASSWORD")%></asp:LinkButton><br />
                        <asp:LinkButton ID="hypListBookings" runat="server" OnClick="hypListBookings_Click"
                            Visible="true"><%= GetKeyResult("MYBOOKINGS")%></asp:LinkButton><br />
                        <asp:LinkButton ID="hypListRequests" runat="server" OnClick="hypListRequests_Click"
                            Visible="true"><%= GetKeyResult("MYREQUESTS")%></asp:LinkButton>
                    </div>
                </div>
            </div>
            <!--end mainbody-right-afterlogin-body -->
        </div>
        <div class="mainbody-right-news">
            <uc4:NewsSubscriber ID="NewsSubscriber1" runat="server" />
        </div>
        <uc3:RecentlyJoinedHotel ID="RecentlyJoinedHotel1" runat="server" />
    </div>
    <div id="divJoinToday-overlay">
    </div>
    <div id="divJoinToday">
        <div class="popup-top">
        </div>
        <div class="popup-mid">
            <div class="popup-mid-inner">
                <div class="error" id="errorPopup" runat="server">
                </div>
                <table cellspacing="10">
                    <tr>
                        <td>
                            <%= GetKeyResult("VENUE")%>
                        </td>
                        <td>
                            <asp:TextBox ID="txtHotelName" runat="server" value="" class="inputregister" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%= GetKeyResult("TITLE")%>
                        </td>
                        <td>
                            <asp:TextBox ID="txtTitle" runat="server" value="" class="inputregister" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%= GetKeyResult("CONTACTPERSON")%>
                        </td>
                        <td>
                            <asp:TextBox ID="txtContactPerson" runat="server" value="" class="inputregister"
                                MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <%--<tr>
                        <td valign="top">
                            <%= GetKeyResult("ADDRESS")%>
                        </td>
                        <td>
                            <asp:TextBox ID="txtAddress" runat="server" value="" MaxLength="100" TextMode="MultiLine"
                                CssClass="jointodaytextareareg" oncopy="return false" onpaste="return false"
                                class="inputregister"></asp:TextBox>
                        </td>
                    </tr>--%>
                    <tr>
                        <td>
                            <%= GetKeyResult("EMAIL")%>
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmail" runat="server" value="" class="inputregister" MaxLength="250"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%= GetKeyResult("PHONE")%>
                        </td>
                        <td>
                            <asp:TextBox ID="txtphoneNo" runat="server" value="" class="inputregister" MaxLength="15"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtphoneNo"
                                ValidChars="-0123456789">
                            </asp:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%= GetKeyResult("NUMBEROFMEEYINGROOMS")%>
                        </td>
                        <td>
                            <asp:TextBox ID="txtNoMeetingRoom" runat="server" value="" class="inputboxsmall"
                                MaxLength="3" Style="text-align: left;"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" TargetControlID="txtNoMeetingRoom"
                                ValidChars="0123456789" runat="server">
                            </asp:FilteredTextBoxExtender>
                        </td>
                    </tr>
                </table>
                <div class="subscribe-btn">
                    <div class="save-cancel-btn1 button_section">
                        <asp:LinkButton ID="btnSend" runat="server" CssClass="select" OnClientClick="javascript: _gaq.push(['_trackPageview', 'RegisterOK-Venue.aspx']);"
                            OnClick="btnSend_Click"><%=GetKeyResult("SEND") %></asp:LinkButton>
                        &nbsp;or&nbsp;
                        <asp:LinkButton ID="lnkCnacel" runat="server" CssClass="cancelpop" OnClientClick="return CloseJoinTodayPopUp();"><%=GetKeyResult("CANCEL") %></asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <div class="popup-bottom">
        </div>
    </div>
    <script language="javascript" type="text/javascript">
        function registeredMessage() {
            alert('<%= GetKeyResultForJavaScript("ALREADYREGISTERED")%>');
            return false;
        }

        function CloseJoinTodayPopUp() {
            document.getElementsByTagName('html')[0].style.overflow = 'auto';
            document.getElementById('divJoinToday').style.display = 'none';
            document.getElementById('divJoinToday-overlay').style.display = 'none';
            jQuery('#<%=txtHotelName.ClientID %>').val('');
            jQuery('#<%=txtTitle.ClientID %>').val('');
            jQuery('#<%=txtContactPerson.ClientID %>').val('');
            jQuery('#<%=txtEmail.ClientID %>').val('');
            jQuery('#<%=txtphoneNo.ClientID %>').val('');
            jQuery('#<%=txtNoMeetingRoom.ClientID %>').val('');
            return false;
        }
        jQuery(document).ready(function () {
            jQuery("#<%= btnRegisterAsHotel.ClientID %>").bind("click", function () {
                if (jQuery("#<%= errorPopup.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= errorPopup.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                }
                jQuery("#<%= errorPopup.ClientID %>").html("");
                jQuery("#<%= errorPopup.ClientID %>").hide();
                //jQuery("#Loding_overlay").show();
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                jQuery("#divJoinToday").show();
                jQuery("#divJoinToday-overlay").show();
                return false;
            });
            jQuery("#<%= errorPopup.ClientID %>").hide();
            document.getElementById('subscribe').style.display = 'none';
            jQuery("#errorPopup").hide();
            jQuery("#<%= btnSend.ClientID %>").bind("click", function () {
                if (jQuery("#<%= errorPopup.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= errorPopup.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                }
                var isvalid = true;
                var errormessage = "";
                var HotelName = jQuery("#<%= txtHotelName.ClientID %>").val();
                if (HotelName.length <= 0 || HotelName == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>"
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("ERRORNAME")%>';
                    isvalid = false;
                }

                var Title = jQuery("#<%= txtTitle.ClientID %>").val();
                if (Title.length <= 0 || Title == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>"
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("ERRORTITLE")%>';
                    isvalid = false;
                }

                var contactperson = jQuery("#<%= txtContactPerson.ClientID %>").val();
                if (contactperson.length <= 0 || contactperson == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>"
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("ERRORCONTACTPERSON")%>';
                    isvalid = false;
                }

                var emailp1 = jQuery("#<%= txtEmail.ClientID %>").val();
                if (emailp1.length <= 0 || emailp1 == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("ERROR")%>';
                    isvalid = false;
                }

                else {
                    if (!validateEmail(emailp1)) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += '<%= GetKeyResultForJavaScript("ERROR")%>';
                        isvalid = false;
                    }
                }

                var Phone = jQuery("#<%=txtphoneNo.ClientID %>").val();
                if (Phone.length <= 0 || Phone == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>"
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("ERRORPHONE")%>';
                    isvalid = false;
                }

                var Numbermeetingrooms = jQuery("#<%=txtNoMeetingRoom.ClientID %>").val();
                if (Numbermeetingrooms.length <= 0 || Numbermeetingrooms == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>"
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("ERRORNUMBEROFMEETINGROOM")%>';
                    isvalid = false;
                }

                if (!isvalid) {
                    jQuery("#<%= errorPopup.ClientID %>").show();
                    jQuery("#<%= errorPopup.ClientID %>").html(errormessage);
                    return false;
                }
                jQuery("#<%= errorPopup.ClientID %>").html("");
                jQuery("#<%= errorPopup.ClientID %>").hide();
                jQuery("#Loding_overlay").show();
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                jQuery("#divJoinToday").show();
                jQuery("#divJoinToday-overlay").show();
            });

        });
        function validateEmail(txtEmail) {
            var a = txtEmail;
            var filter = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
            if (filter.test(a)) {
                return true;
            }
            else {
                return false;
            }
        }

    </script>
</asp:Content>
