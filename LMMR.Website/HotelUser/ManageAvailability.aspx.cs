﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using System.Configuration;
using log4net;
using log4net.Config;
#endregion

public partial class HotelUser_ManageAvailability : System.Web.UI.Page
{
    #region Variables and Properties
    private HotelManager objHotelManager = new HotelManager();
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_ManageAvailability));
    public string MonthYearDayDate
    {
        get;
        set;
    }

    public string StartAvailableDates
    {
        get;
        set;
    }

    public string EndAvailableDate
    {
        get;
        set;
    }

    public string DivNames
    {
        get;
        set;
    }
    public string DivNamesBR
    {
        get;
        set;
    }
    public string IsBedroomAvailable
    {
        get;
        set;
    }
    public string CurrentPanel
    {
        get;
        set;
    }
    //Get Site Root path
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            if (string.IsNullOrEmpty(appRootUrl) || appRootUrl == "/")
            {
                return host + "/";
            }
            else
            {
                return host + appRootUrl + "/";
            }
        }
    }
    //Service URL set from here
    public string ServiceUrl
    {
        get
        {
            return "'" + SiteRootPath + "LMMRwebservice.asmx'";
            //return "'" + Convert.ToString(ConfigurationManager.AppSettings["WebserviceURL"]) + "'";
        }
    }
    #endregion

    #region Pageload
    /// <summary>
    /// Method to manage the page load functionality.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["CurrentHotelID"] == null)
            {
                Response.Redirect("~/login.aspx",false);
                return;
            }
            Session["LinkID"] = "lnkAvailability";
            if (!Page.IsPostBack)
            {
                CurrentPanel = (Session["CurrentPanel"] == null ? "1" : Convert.ToString(Session["CurrentPanel"]));
                Page.Form.DefaultButton = btnnothing.UniqueID;
                Session["CurrentHotelID"] = Session["CurrentHotelID"] == null ? 0 : Session["CurrentHotelID"];
                txtFrom.Text = DateTime.Now.ToString("dd/MM/yy");
                txtFromBr.Text = DateTime.Now.ToString("dd/MM/yy");
                txtTo.Text = DateTime.Now.ToString("dd/MM/yy");
                txtToBr.Text = DateTime.Now.ToString("dd/MM/yy");
                CalendarExtender1.StartDate = DateTime.Now;
                CalendarExtender1.EndDate = DateTime.Now.AddDays(60);
                CalendarExtender2.StartDate = DateTime.Now;
                CalendarExtender2.EndDate = DateTime.Now.AddDays(60);
                calExtFromBr.StartDate = DateTime.Now;
                calExtFromBr.EndDate = DateTime.Now.AddDays(60);
                calExToBr.StartDate = DateTime.Now;
                calExToBr.EndDate = DateTime.Now.AddDays(60);
                BindFormAvailability();
                lblHotelName.Text = objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Name;
                Session["CurrentPanel"] = null;
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region BindAvailability
    /// <summary>
    /// Get All the details of Binding Availability.
    /// </summary>
    public void BindFormAvailability()
    {
        try
        {
            string GetmonthDayYear = "[";
            string StartDate = string.Empty;
            string EndDate = string.Empty;
            int startday = 0;
            int monthstart = 0;
            DateTime dt = new DateTime();

            while (startday < 60)
            {
                if (monthstart == 0)
                {
                    dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    monthstart++;
                    startday += System.DateTime.DaysInMonth(dt.Year, dt.Month) - DateTime.Now.Day + 1;
                    StartDate = "[" + DateTime.Now.Day + "," + DateTime.Now.Month + "]";
                }
                else
                {
                    dt = dt.AddMonths(1);
                    monthstart++;
                    int oldstartday = startday;
                    startday += (startday + System.DateTime.DaysInMonth(dt.Year, dt.Month)) <= 60 ? System.DateTime.DaysInMonth(dt.Year, dt.Month) : Math.Abs(60 - startday);
                    EndDate = "[" + ((oldstartday + System.DateTime.DaysInMonth(dt.Year, dt.Month)) <= 60 ? System.DateTime.DaysInMonth(dt.Year, dt.Month) : Math.Abs(60 - oldstartday)) + "," + dt.Month + "]";
                }
                GetmonthDayYear += "[" + dt.Month + "," + dt.Year + "," + System.DateTime.DaysInMonth(dt.Year, dt.Month) + "," + (int)dt.DayOfWeek + "],";

            }
            if (GetmonthDayYear.Length != 0)
            {
                GetmonthDayYear = GetmonthDayYear.Substring(0, GetmonthDayYear.Length - 1);
            }
            GetmonthDayYear += "]";
            MonthYearDayDate = GetmonthDayYear;
            StartAvailableDates = StartDate;
            EndAvailableDate = EndDate;
            TList<MeetingRoom> lstMeetingRoom = new TList<MeetingRoom>();
            lstMeetingRoom = objHotelManager.GetMeetingRoomByHotelId(Convert.ToInt64(Session["CurrentHotelID"]));
            DivNames = "[";//'meetingRoom1_M', 'meetingRoom1_A', 'meetingRoom2_M', 'meetingRoom2_A', 'meetingRoom3_M', 'meetingRoom3_A', 
            foreach (MeetingRoom m in lstMeetingRoom)
            {
                ltrMeetingRoom.Text += @"<tr class='raw3'><td colspan='32' height='22'>" + m.Name + "</td></tr><tr class='raw5'><td colspan='32' height='22'>Morning</td></tr><tr class='raw4' id='" + m.Id + "_MrMorning'></tr><tr class='raw5'><td colspan='32' height='22'>Afternoon</td></tr><tr class='raw4' id='" + m.Id + "_MrAfternoon'>";
                DivNames += "'" + m.Id + "',";
                ltrMeetingRoomCheckbox.Text += @"<div class='adjust-meeting-form-body-box1left'>" + m.Name + "</div><div class='adjust-meeting-form-body-box1right'><input type='checkbox' alt='" + m.Id + "' id='chk" + m.Name.Trim().Replace(" ", "") + "' name='chbox' class='NoClassApply' ></div>";
            }
            if (lstMeetingRoom.Count > 0)
            {
                pnlMeetingRoom.Visible = true;
                msgEmpty.Visible = false;
            }
            else
            {
                pnlMeetingRoom.Visible = false;
                msgEmpty.Visible = true;
            }
            if (DivNames.Length > 0)
            {
                DivNames = DivNames.Substring(0, DivNames.Length);
            }
            DivNames += "]";
            TList<BedRoom> lstBedRoom = new TList<BedRoom>();
            lstBedRoom = objHotelManager.GetBedRoomByhotelId(Convert.ToInt64(Session["CurrentHotelID"]));
            DivNamesBR += "[";
            if (lstBedRoom.Count > 0)
            {
                pnlBedroom.Visible = true;
                IsBedroomAvailable = "true";
                for (int i = 0; i < lstBedRoom.Count; i++)
                {
                    string bedroomtype = lstBedRoom[i].Types == null ? "Standard" : (lstBedRoom[i].Types == (int)LMMR.Business.BedRoomType.Standard ? "Standard" : (lstBedRoom[i].Types == (int)LMMR.Business.BedRoomType.Superior ? "Superior" : (lstBedRoom[i].Types == (int)LMMR.Business.BedRoomType.Executive ? "Executive" : lstBedRoom[i].Types == (int)LMMR.Business.BedRoomType.Business ? "Business" : lstBedRoom[i].Types == (int)LMMR.Business.BedRoomType.Classic ? "Classic" : lstBedRoom[i].Types == (int)LMMR.Business.BedRoomType.Deluxe ? "Deluxe" : "Standard")));
                    ltrBedroomDetails.Text += @"<tr class='raw3'><td height='22' colspan='32'>" + bedroomtype + "&nbsp;&bull;<a href='javascript:void(0);' id='" + lstBedRoom[i].Id + "_modifyBR' >Modify</a><a href='javascript:void(0);' id='" + lstBedRoom[i].Id + "_CancelBR' >Cancel</a>&nbsp;<a href='javascript:void(0);' id='" + lstBedRoom[i].Id + "_SaveBR' >Save</a></td></tr><tr class='raw4' id='" + lstBedRoom[i].Id + "_bedroom' ></tr><tr class='raw4' id='" + lstBedRoom[i].Id + "_AvailBedroom' ></tr><tr class='raw4' id='" + lstBedRoom[i].Id + "_EditBedroom' ></tr>";
                    tblBR.Visible = true;
                    DivNamesBR += "'" + lstBedRoom[i].Id + "',";
                    ltrBedRoomCheckbox.Text += @"<div class='adjust-meeting-form-body-box1left'>" + bedroomtype + "</div><div class='adjust-meeting-form-body-box1right'><input type='checkbox' alt='" + lstBedRoom[i].Id + "' id='chk" + bedroomtype.Trim().Replace(" ", "") + "' name='chbox' class='NoClassApply' ></div>";
                }
                if (DivNamesBR.Length > 0)
                {
                    DivNamesBR = DivNamesBR.Substring(0, DivNamesBR.Length - 1);
                }
            }
            else
            {
                pnlBedroom.Visible = false;
                IsBedroomAvailable = "false";
            }
            DivNamesBR += "]";
            if (lstMeetingRoom.Count <= 0)
            {
                pnlBedroom.Visible = false;
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    #region Submit
    #region ManageAvailability
    /// <summary>
    /// Manage Availability of the Meeting room.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ancSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            objHotelManager.AdjustMeetingRoom(hdnMeetingRoomIds.Value.Replace("'", ""), hdnMeetingDays.Value.Replace("'", ""), hdnClosingStatus.Value.Replace("'", ""), hdnOpenStatus.Value.Replace("'", ""), txtFrom.Text.Replace("'", ""), txtTo.Text.Replace("'", ""));
            objHotelManager.CheckAvailabilityForFullProp(Convert.ToInt64(Session["CurrentHotelID"]), txtFrom.Text.Replace("'", ""), txtTo.Text.Replace("'", ""));
            Session["CurrentPanel"] = hdnCurrentPanel.Value;
            Response.Redirect("~/HotelUser/ManageAvailability.aspx",false);
            return;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    /// <summary>
    /// Manage Availability of Bedroom.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ancSubmitBr_Click(object sender, EventArgs e)
    {
        try
        {
            objHotelManager.AdjustBedRoom(hdnBedroomIds.Value.Replace("'", ""), hdnBedroomDays.Value.Replace("'", ""), hdnClosingStatusBr.Value.Replace("'", ""), hdnOpeningstatusBr.Value.Replace("'", ""), txtFromBr.Text.Replace("'", ""), txtToBr.Text.Replace("'", ""));
            Session["CurrentPanel"] = hdnCurrentPanel.Value;
            Response.Redirect("~/HotelUser/ManageAvailability.aspx",false);
            return;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Leedtime Save
    /// <summary>
    /// Manage Leed time of the month.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSaveLeed_Click(object sender, EventArgs e)
    {
        try
        {
            string leadtimes = hdnLeadTime.Value;
            string startmonthandyear = hdnStartingDate.Value;
            DateTime startdate;
            if (Convert.ToInt32(startmonthandyear.Split('-')[0]) == DateTime.Now.Month)
            {
                startdate = new DateTime(Convert.ToInt32(startmonthandyear.Split('-')[1]), Convert.ToInt32(startmonthandyear.Split('-')[0]), DateTime.Now.Day);
            }
            else
            {
                startdate = new DateTime(Convert.ToInt32(startmonthandyear.Split('-')[1]), Convert.ToInt32(startmonthandyear.Split('-')[0]), 1);
            }
            objHotelManager.UpdateLeadTimeByHotelId(Convert.ToInt64(Session["CurrentHotelID"]), startdate, leadtimes);
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Session["CurrentPanel"] = hdnCurrentPanel.Value;
        Response.Redirect("~/HotelUser/ManageAvailability.aspx",false);
        return;
    }
    #endregion

    /// <summary>
    /// Update Availability of Bedroom by bedroom Id.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSubmit1_Click(object sender, EventArgs e)
    {
        try
        {
            string availablerooms = hdn_AvailabileRooms.Value;
            string startmonthandyear = hdn_Date.Value;
            DateTime startdate;
            if (Convert.ToInt32(startmonthandyear.Split('-')[0]) == DateTime.Now.Month)
            {
                startdate = new DateTime(Convert.ToInt32(startmonthandyear.Split('-')[1]), Convert.ToInt32(startmonthandyear.Split('-')[0]), DateTime.Now.Day);
            }
            else
            {
                startdate = new DateTime(Convert.ToInt32(startmonthandyear.Split('-')[1]), Convert.ToInt32(startmonthandyear.Split('-')[0]), 1);
            }
            //hdnBedroomID1.Value
            objHotelManager.UpdateAvailabilityOfBedroomByHotelId(Convert.ToInt64(Session["CurrentHotelID"]), Convert.ToInt64(hdnBedroomID.Value), startdate, availablerooms);
            Session["CurrentPanel"] = hdnCurrentPanel.Value;
            Response.Redirect("~/HotelUser/ManageAvailability.aspx",false);
            return;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion
}