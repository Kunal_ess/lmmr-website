﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using LMMR.Entities;
using LMMR.Data;
using LMMR.Business;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Configuration;
using log4net;
using log4net.Config;
using System.Xml;
#endregion

public partial class HotelUser_usercontrols_BookingDetails : System.Web.UI.UserControl
{

    #region variables
    VList<ViewBookingHotels> vlist;
    VList<Viewbookingrequest> vlistreq;
    ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
    HotelManager objHotelManager = new HotelManager();
    string Typelink = "", AdminUser = "", Hotelid = "0";
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_usercontrols_BookingDetails));
    WizardLinkSettingManager ObjWizardManager = new WizardLinkSettingManager();
    Createbooking objBooking = null;
    BookingManager bm = new BookingManager();
    CurrencyManager cm = new CurrencyManager();
    HotelManager objHotel = new HotelManager();
    public string CurrencySign
    {
        get;
        set;
    }
    public XmlDocument XMLLanguage
    {
        get { return (XmlDocument)ViewState["Language"]; }
        set { ViewState["Language"] = value; }
    }
    public string UserCurrency
    {
        get { return "1"; }
        set { ViewState["UserCurrency"] = "1"; }
    }
    public string HotelCurrency
    {
        get { return "1"; }
        set { ViewState["HotelCurrency"] = "1"; }
    }
    public decimal CurrencyConvert
    {
        get { return 1; }
        set { ViewState["CurrencyConvert"] = 1; }
    }
    public decimal CurrentLat
    {
        get;
        set;
    }
    public decimal CurrentLong
    {
        get;
        set;
    }
    public Int64 CurrentMeetingRoomID
    {
        get;
        set;
    }
    public Int64 CurrentMeetingRoomConfigureID
    {
        get;
        set;
    }
    public int CurrentPackageType
    {
        get;
        set;
    }
    public List<ManagePackageItem> CurrentPackageItem
    {
        get;
        set;
    }
    public TList<PackageItems> objHp
    {
        get;
        set;
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        //bindDetails();
        //BindBooking();
        Nettotalview.Visible = false;
        rptVatList.Visible = false;
       
    }

    


    protected void rptVatList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblVatPrice = (Label)e.Item.FindControl("lblVatPrice");
            VatCollection v = e.Item.DataItem as VatCollection;
            if (v != null)
            {
                lblVatPrice.Text = Math.Round(v.CalculatedPrice , 2).ToString();
            }
        }
    }

    protected void rptRoomManage_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            RoomManage objRoomManage = e.Item.DataItem as RoomManage;
            Label lblRoomIndex = (Label)e.Item.FindControl("lblRoomIndex");
            Label lblAllotmantName = (Label)e.Item.FindControl("lblAllotmantName");
            Label lblCheckIn = (Label)e.Item.FindControl("lblCheckIn");
            Label lblCheckOut = (Label)e.Item.FindControl("lblCheckOut");
            Label lblNote = (Label)e.Item.FindControl("lblNote");
            lblRoomIndex.Text = (e.Item.ItemIndex + 1).ToString();
            lblAllotmantName.Text = objRoomManage.RoomAssignedTo;
            lblCheckIn.Text = objRoomManage.CheckInTime;
            lblCheckOut.Text = objRoomManage.checkoutTime;
            lblNote.Text = objRoomManage.Notes;
        }
    }

    protected void rptPackageItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPackageItem = (Label)e.Item.FindControl("lblPackageItem");
            Label lblFromTime = (Label)e.Item.FindControl("lblFromTime");
            Label lblToTime = (Label)e.Item.FindControl("lblToTime");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            PackageItems p = e.Item.DataItem as PackageItems;
            if (p != null)
            {
                ManagePackageItem mpi = CurrentPackageItem.Where(a => a.ItemId == p.Id).FirstOrDefault();
                if (mpi != null)
                {
                    lblPackageItem.Text = p.ItemName;
                    lblFromTime.Text = mpi.FromTime;
                    lblToTime.Text = mpi.ToTime;
                    lblQuantity.Text = Convert.ToString(mpi.Quantity);
                }
            }
        }
    }


    protected void rptMeetingRoomConfigure_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            BookedMrConfig objBookedMrConfig = e.Item.DataItem as BookedMrConfig;
            //Bind Meetingroom//
            MeetingRoom objMeetingroom = objHotel.GetMeetingRoomDetailsById(CurrentMeetingRoomID);
            MeetingRoomDesc objDesc = objMeetingroom.MeetingRoomDescCollection.Where(a => a.LanguageId == Convert.ToInt32(Session["LanguageID"])).FirstOrDefault();
            MeetingRoomConfig objMrConfig = objMeetingroom.MeetingRoomConfigCollection.Where(a => a.Id == CurrentMeetingRoomConfigureID).FirstOrDefault();

            Label lblMeetingRoomName = (Label)e.Item.FindControl("lblMeetingRoomName");
            Label lblMeetingRoomType = (Label)e.Item.FindControl("lblMeetingRoomType");
            Label lblMinMaxCapacity = (Label)e.Item.FindControl("lblMinMaxCapacity");
            Label lblMeetingRoomActualPrice = (Label)e.Item.FindControl("lblMeetingRoomActualPrice");
            Label lblTotalMeetingRoomPrice = (Label)e.Item.FindControl("lblTotalMeetingRoomPrice");
            Label lblTotalFinalMeetingRoomPrice = (Label)e.Item.FindControl("lblTotalFinalMeetingRoomPrice");
            lblMeetingRoomName.Text = objMeetingroom.Name;
            lblMeetingRoomType.Text = Enum.GetName(typeof(RoomShape), objMrConfig.RoomShapeId);
            lblMinMaxCapacity.Text = objMrConfig.MinCapacity + " - " + objMrConfig.MaxCapicity;
            lblMeetingRoomActualPrice.Text = Math.Round(objBookedMrConfig.MeetingroomPrice , 2).ToString();
            lblTotalFinalMeetingRoomPrice.Text = Math.Round(objBookedMrConfig.MeetingroomPrice , 2).ToString();
            decimal intDiscountMR = objBookedMrConfig.MeetingroomDiscount;
            lblTotalMeetingRoomPrice.Text = Math.Round(objBookedMrConfig.MeetingroomPrice , 2).ToString();

            //Bind Other details//
            Label lblSelectedDay = (Label)e.Item.FindControl("lblSelectedDay");
            Label lblStart = (Label)e.Item.FindControl("lblStart");
            Label lblEnd = (Label)e.Item.FindControl("lblEnd");
            Label lblNumberOfParticepant = (Label)e.Item.FindControl("lblNumberOfParticepant");
            lblSelectedDay.Text = objBookedMrConfig.SelectedDay.ToString();
            //lblTotalMeetingRoomPrice.Text = Math.Round(objBookedMrConfig.MeetingroomPrice , 2).ToString();
            lblStart.Text = objBookedMrConfig.FromTime;
            lblEnd.Text = objBookedMrConfig.ToTime;
            lblNumberOfParticepant.Text = objBookedMrConfig.NoOfParticepant.ToString();
            Panel pnlNotIsBreakdown = (Panel)e.Item.FindControl("pnlNotIsBreakdown");
            Panel pnlIsBreakdown = (Panel)e.Item.FindControl("pnlIsBreakdown");
            if (objBookedMrConfig.IsBreakdown == true)
            {
                if (pnlIsBreakdown == null)
                {
                }
                else
                {
                    pnlIsBreakdown.Visible = true;
                    pnlNotIsBreakdown.Visible = false;
                }
            }
            else
            {
                if (pnlIsBreakdown == null)
                {
                }
                else
                {
                    pnlIsBreakdown.Visible = false;
                    pnlNotIsBreakdown.Visible = true;
                }
            }
            #region Package and Equipment Collection
            //Bind Package//
            Panel pnlIsPackageSelected = (Panel)e.Item.FindControl("pnlIsPackageSelected");
            Panel pnlBuildMeeting = (Panel)e.Item.FindControl("pnlBuildMeeting");
            if (objBookedMrConfig.PackageID == 0)
            {
                pnlIsPackageSelected.Visible = false;
                if (objBookedMrConfig.BuildManageMRLst.Count > 0)
                {
                    pnlBuildMeeting.Visible = true;
                    Repeater rptBuildMeeting = (Repeater)e.Item.FindControl("rptBuildMeeting");
                    Label lblTotalBuildPackagePrice = (Label)e.Item.FindControl("lblTotalBuildPackagePrice");
                    CurrentPackageType = objBookedMrConfig.SelectedTime;
                    rptBuildMeeting.DataSource = objBookedMrConfig.BuildManageMRLst;
                    rptBuildMeeting.DataBind();
                    lblTotalBuildPackagePrice.Text = Math.Round((objBookedMrConfig.BuildPackagePriceTotal - (objBookedMrConfig.MeetingroomPrice)) , 2).ToString();
                }
                else
                {
                    pnlBuildMeeting.Visible = false;
                }
            }
            else
            {
                pnlIsPackageSelected.Visible = true;
                pnlBuildMeeting.Visible = false;
                Label lblSelectedPackage = (Label)e.Item.FindControl("lblSelectedPackage");
                Label lblPackageDescription = (Label)e.Item.FindControl("lblPackageDescription");
                Label lblPackagePrice = (Label)e.Item.FindControl("lblPackagePrice");
                Label lblTotalPackagePrice = (Label)e.Item.FindControl("lblTotalPackagePrice");
                lblPackagePrice.Text = Math.Round(objBookedMrConfig.PackagePriceTotal , 2).ToString();
                Repeater rptPackageItem = (Repeater)e.Item.FindControl("rptPackageItem");

                PackageByHotel p = objHotel.GetPackageDetailsByHotel(objBooking.HotelID).FindAllDistinct(PackageByHotelColumn.PackageId).Where(a => a.PackageId == objBookedMrConfig.PackageID).FirstOrDefault();
                if (p != null)
                {
                    CurrentPackageItem = objBookedMrConfig.ManagePackageLst;
                    rptPackageItem.DataSource = objHotel.GetPackageItemDetailsByPackageID(Convert.ToInt64(p.PackageId));
                    rptPackageItem.DataBind();

                    lblSelectedPackage.Text = p.PackageIdSource.PackageName;
                    if (p.PackageIdSource.PackageName.ToLower() == "favourite")
                    {
                        lblPackageDescription.Text = GetKeyResult("FAVOURITEDESCRIPTION");
                    }
                    if (p.PackageIdSource.PackageName.ToLower() == "elegant")
                    {
                        lblPackageDescription.Text = GetKeyResult("ELEGANTDESCRIPTION");
                    }
                    if (p.PackageIdSource.PackageName.ToLower() == "standard")
                    {
                        lblPackageDescription.Text = GetKeyResult("STANDARDDESCRIPTION");
                    }
                }
                Panel pnlIsExtra = (Panel)e.Item.FindControl("pnlIsExtra");
                Label lblextratitle = (Label)e.Item.FindControl("lblextratitle");
                Label lblExtraPrice = (Label)e.Item.FindControl("lblExtraPrice");
                if (objBookedMrConfig.ManageExtrasLst.Count > 0)
                {
                    Repeater rptExtra = (Repeater)e.Item.FindControl("rptExtra");
                    objHp = objHotel.GetIsExtraItemDetailsByHotel(objBooking.HotelID).FindAllDistinct(PackageItemsColumn.Id);
                    rptExtra.DataSource = objBookedMrConfig.ManageExtrasLst;
                    rptExtra.DataBind();
                    pnlIsExtra.Visible = true;

                    lblExtraPrice.Text = Math.Round(objBookedMrConfig.ExtraPriceTotal , 2).ToString();
                }
                else
                {
                    lblExtraPrice.Visible = false;

                    lblextratitle.Visible = false;
                    pnlIsExtra.Visible = false;

                }
                lblTotalPackagePrice.Text = Math.Round((objBookedMrConfig.PackagePriceTotal + objBookedMrConfig.ExtraPriceTotal) , 2).ToString();
            }
            Panel pnlEquipment = (Panel)e.Item.FindControl("pnlEquipment");
            if (objBookedMrConfig.EquipmentLst.Count > 0)
            {
                pnlEquipment.Visible = true;
                Repeater rptEquipment = (Repeater)e.Item.FindControl("rptEquipment");
                rptEquipment.DataSource = objBookedMrConfig.EquipmentLst;
                rptEquipment.DataBind();
                Label lblTotalEquipmentPrice = (Label)e.Item.FindControl("lblTotalEquipmentPrice");
                lblTotalEquipmentPrice.Text = Math.Round(objBookedMrConfig.EquipmentPriceTotal , 2).ToString();
            }
            else
            {
                pnlEquipment.Visible = false;
            }
            #endregion
        }
    }

    #region Language
    //This is used for language conversion for static contants.
    public string GetKeyResult(string key)
    {
        //if (XMLLanguage == null)
        //{
        //    XMLLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
        //}
        //XmlNode nodes = XMLLanguage.SelectSingleNode("items/item[@key='" + Key + "']");
        //return nodes.InnerText;//
        return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    }
    #endregion


    protected void rptMeetingRoom_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            BookedMR objBookedMr = e.Item.DataItem as BookedMR;
            CurrentMeetingRoomID = objBookedMr.MRId;
            CurrentMeetingRoomConfigureID = objBookedMr.MrConfigId;

            Repeater rptMeetingRoomConfigure = (Repeater)e.Item.FindControl("rptMeetingRoomConfigure");
            rptMeetingRoomConfigure.DataSource = objBookedMr.MrDetails;
            rptMeetingRoomConfigure.DataBind();
        }
    }

    #region save as pdf
    /// <summary>
    /// method to save as pdf
    /// </summary>

    protected void lnkSavePDF_Click(object sender, EventArgs e)
    {
        try
        {
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=UserDetails.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            PrepareDivForExport(Divdetails);
            Divdetails.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }




    }
    #endregion


    protected void rptAccomodation_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Accomodation objAccomodation = e.Item.DataItem as Accomodation;

            Label lblBedroomName = (Label)e.Item.FindControl("lblBedroomName");
            Label lblBedroomType = (Label)e.Item.FindControl("lblBedroomType");
            Label lblBedroomPrice = (Label)e.Item.FindControl("lblBedroomPrice");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            Label lblTotalBedroomPrice = (Label)e.Item.FindControl("lblTotalBedroomPrice");
            if (objAccomodation != null)
            {
                //BedRoom objBedroom = objHotel.GetBedRoomDetailsByBedroomID(objAccomodation.BedroomId);
                //lblBedroomName.Text = Enum.GetName(typeof(BedRoomType), objBedroom.Types);
                //lblBedroomType.Text = objAccomodation.RoomType;
                //lblBedroomPrice.Text = Math.Round(Convert.ToDecimal(objAccomodation.RoomType == "Single" ? objBedroom.PriceSingle : objBedroom.PriceDouble) * CurrencyConvert, 2).ToString();
                //lblQuantity.Text = objAccomodation.Quantity.ToString();
                //lblTotalBedroomPrice.Text = Math.Round(Convert.ToDecimal(objAccomodation.RoomType == "Single" ? objBedroom.PriceSingle : objBedroom.PriceDouble) * CurrencyConvert * objAccomodation.Quantity, 2).ToString();
                //Repeater rptRoomManage = (Repeater)e.Item.FindControl("rptRoomManage");
                //rptRoomManage.DataSource = objAccomodation.RoomManageLst;
                //rptRoomManage.DataBind();
            }
        }
    }

    protected void rptBuildMeeting_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            BuildYourMR objBuildYourMR = e.Item.DataItem as BuildYourMR;
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblItemDescription = (Label)e.Item.FindControl("lblItemDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            Label lblTotalItemPrice = (Label)e.Item.FindControl("lblTotalItemPrice");
            PackageItems p = objHotel.GetItemDetailsByHotelandType(objBooking.HotelID, ItemType.FoodBeverages).FindAllDistinct(PackageItemsColumn.Id).Where(a => a.Id == objBuildYourMR.ItemId).FirstOrDefault();
            if (p != null)
            {
                lblItemName.Text = p.ItemName;
                lblItemDescription.Text = p.PackageDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64(Session["LanguageID"])).FirstOrDefault().ItemDescription;
                lblQuantity.Text = objBuildYourMR.Quantity.ToString();
                lblTotalItemPrice.Text = Math.Round(objBuildYourMR.ItemPrice * objBuildYourMR.Quantity , 2).ToString();
                //Package Hotel pricing
                //PackageByHotel objp = p.PackageByHotelCollection.Where(a => a.HotelId == objBooking.HotelID && a.PackageId == null).FirstOrDefault();
                //if(objp!=null)
                //{
                //     = Math.Round((CurrentPackageType == 0 ? Convert.ToDecimal(objp.FulldayPrice) : Convert.ToDecimal(objp.HalfdayPrice)) * objBuildYourMR.Quantity * CurrencyConvert,2).ToString();
                //}
            }
        }
    }

    protected void rptEquipment_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ManageEquipment objManageEquipment = e.Item.DataItem as ManageEquipment;
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblItemDescription = (Label)e.Item.FindControl("lblItemDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            Label lblTotalPrice = (Label)e.Item.FindControl("lblTotalPrice");
            PackageItems p = objHotel.GetItemDetailsByHotelandType(objBooking.HotelID, ItemType.Equipment).FindAllDistinct(PackageItemsColumn.Id).Where(a => a.Id == objManageEquipment.ItemId).FirstOrDefault();
            if (p != null)
            {
                lblItemName.Text = p.ItemName;
                lblItemDescription.Text = p.PackageDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64(Session["LanguageID"])).FirstOrDefault().ItemDescription;
                lblQuantity.Text = objManageEquipment.Quantity.ToString();
                lblTotalPrice.Text = Math.Round(objManageEquipment.ItemPrice * objManageEquipment.Quantity , 2).ToString();
                //Package Hotel pricing
                //PackageByHotel objp = p.PackageByHotelCollection.Where(a => a.HotelId == objBooking.HotelID && a.PackageId == null).FirstOrDefault();
                //if (objp != null)
                //{
                //    lblTotalPrice.Text = Math.Round((CurrentPackageType == 0 ? Convert.ToDecimal(objp.FulldayPrice) : Convert.ToDecimal(objp.HalfdayPrice)) * objManageEquipment.Quantity * CurrencyConvert,2).ToString();
                //}
            }
        }
    }

    protected void rptExtra_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPackageItem = (Label)e.Item.FindControl("lblPackageItem");
            Label lblFrom = (Label)e.Item.FindControl("lblFrom");
            Label lblTo = (Label)e.Item.FindControl("lblTo");
            Label lblQuntity = (Label)e.Item.FindControl("lblQuntity");
            ManageExtras mngExt = e.Item.DataItem as ManageExtras;
            if (mngExt != null)
            {
                PackageItems p = objHp.Where(a => a.Id == mngExt.ItemId).FirstOrDefault();
                if (p != null)
                {
                    lblPackageItem.Text = p.ItemName;
                    lblFrom.Text = mngExt.FromTime;
                    lblTo.Text = mngExt.ToTime;
                    lblQuntity.Text = mngExt.Quantity.ToString();
                }
            }
            else
            {
                lblQuntity.Text = "0";
            }
        }
    }



    #region PrepareGridViewForExportDIV
    /// <summary>
    /// method to PrepareGridViewForExport a div
    /// </summary>
    private void PrepareDivForExport(Control gv)
    {
        try
        {

            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {

                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].GetType() == typeof(System.Web.UI.HtmlControls.HtmlAnchor))
                {

                    l.Text = (gv.Controls[i] as System.Web.UI.HtmlControls.HtmlAnchor).InnerText;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(DropDownList))
                {

                    l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(GridView))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    // gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Repeater))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Panel))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Table))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }


                else if (gv.Controls[i].GetType() == typeof(CheckBox))
                {

                    l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(HyperLink))
                {

                    l.Text = (gv.Controls[i] as HyperLink).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                //else if (gv.Controls[i].GetType() == typeof(Label))
                //{

                //    l.Text = (gv.Controls[i] as Label).Text;

                //    gv.Controls.Remove(gv.Controls[i]);

                //    gv.Controls.AddAt(i, l);

                //}
                else if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.Image))
                {

                    // l.Text = (gv.Controls[i] as Image).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareDivForExport(gv.Controls[i]);

                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        //PrepareGridViewForExport();
        //PrepareGridViewForExport();

    }


    #endregion



    public void BindBooking(int bookingId)
    {
        // Users object
        Users obj = objViewBooking_Hotel.GetUser(Convert.ToInt64(bookingId));

        //UserDetails object
        TList<UserDetails> objDetails = objViewBooking_Hotel.getuserdetails(obj.UserId);
        lblConatctpersonEmail.HRef = "mailto:" + obj.EmailId;
        lblConatctpersonEmail.InnerText = obj.EmailId;
        lblContactPerson.Text = obj.FirstName + " " + obj.LastName;
        if (objDetails.Count > 0)
        {
            lblContactAddress.Text = objDetails[0].Address;
            lblContactPhone.Text = objDetails[0].Phone;
        }
        objBooking = objViewBooking_Hotel.getxml(Convert.ToInt64(bookingId));
        ////Bind Booking Details
        //BindBooking();

        if (Session["ListBookings"] != null)
        {
            tAndCPopUp.Visible = true;
            tAndCPopUp.OnClientClick = "return open_win2('Policy.aspx?hid=" + objBooking.HotelID + "')";

        }
        //divbookingdetails.Visible = true;


        lblFromDt.Text = objBooking.ArivalDate.ToString("dd MMM yyyy");
        lblToDate.Text = objBooking.DepartureDate.ToString("dd MMM yyyy");
        lblBookedDays.Text = objBooking.Duration == 1 ? objBooking.Duration + " Day" : objBooking.Duration + " Days";

        // BindHotelDetails(objBooking.HotelID);

        rptMeetingRoom.DataSource = objBooking.MeetingroomList;
        rptMeetingRoom.DataBind();
        BindAccomodation();
        lblSpecialRequest.Text = objBooking.SpecialRequest;
        Calculate(objBooking);
        lblNetTotal.Text = Math.Round((objBooking.TotalBookingPrice - VatCalculation.Sum(a => a.CalculatedPrice)) , 2).ToString();
        rptVatList.DataSource = VatCalculation;
        rptVatList.DataBind();
        lblFinalTotal.Text = Math.Round(objBooking.TotalBookingPrice , 2).ToString();
        //tAndCPopUp.OnClientClick = "return open_win2('Policy.aspx?hid=" + objBooking.HotelID + "')";
        //tAndCPopUp.OnClientClick = "return open_win2('Policy.aspx?hid=" + objBooking.HotelID + "')";
        //TimeSpan t = DateTime.Now.Subtract(DateTime.Now.AddDays(7));
        //if (t.Days < 7)
        //{
        //    lblCancelation.Visible = true;
        //    lblCancelation.Text = GetKeyResult("THISBOOKINGCANBECANCELED") + " <b>" + GetKeyResult("FREE") + "</b> " + GetKeyResult("OFCHANGEUNTIL") + " <b> " + DateTime.Now.AddDays(7).ToString("dd MMM yyyy") + "</b>";
        //}
        //else
        //{
        //    lblCancelation.Visible = false;
        //}
    }
   
    public void BindAccomodation()
    {
        if (objBooking.ManageAccomodationLst.Count > 0)
        {
            pnlAccomodation.Visible = false;
            rptAccomodation.DataSource = objBooking.ManageAccomodationLst;
            rptAccomodation.DataBind();
            lblAccomodationPrice.Text = Math.Round(objBooking.AccomodationPriceTotal , 2).ToString();
        }
        else
        {
            pnlAccomodation.Visible = false;
        }
    }

    public void bindDetails()
    {
        //BindBooking();
    }

    List<VatCollection> _VatCalculation = new List<VatCollection>();
    public List<VatCollection> VatCalculation
    {
        get
        {
            if (_VatCalculation == null)
            {
                _VatCalculation = new List<VatCollection>();
            }
            return _VatCalculation;
        }
        set
        {
            _VatCalculation = value;
        }
    }


    #region Go for Calculation
    public void Calculate(Createbooking objCreateBook)
    {
        decimal TotalMeetingroomPrice = 0;
        decimal TotalPackagePrice = 0;
        decimal TotalBuildYourPackagePrice = 0;
        decimal TotalEquipmentPrice = 0;
        decimal TotalExtraPrice = 0;
        bool PackageSelected = false;
        VatCalculation = null;
        VatCalculation = new List<VatCollection>();
        if (objCreateBook != null)
        {
            foreach (BookedMR objb in objCreateBook.MeetingroomList)
            {
                foreach (BookedMrConfig objconfig in objb.MrDetails)
                {
                    TotalMeetingroomPrice = objconfig.NoOfParticepant * objconfig.MeetingroomPrice;
                    //if (VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault() == null)
                    //{
                    //    VatCollection v = new VatCollection();
                    //    v.VATPercent = objconfig.MeetingroomVAT;
                    //    v.CalculatedPrice = objconfig.MeetingroomPrice * objconfig.MeetingroomVAT / 100;
                    //    VatCalculation.Add(v);
                    //}
                    //else
                    //{
                    //    VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault().CalculatedPrice += objconfig.MeetingroomPrice * objconfig.MeetingroomVAT / 100;
                    //}
                    //Build mr
                    foreach (BuildYourMR bmr in objconfig.BuildManageMRLst)
                    {
                        TotalBuildYourPackagePrice += bmr.ItemPrice * bmr.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == bmr.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = bmr.vatpercent;
                            v.CalculatedPrice = (bmr.ItemPrice - (bmr.ItemPrice / ((100 + bmr.vatpercent) / 100))) * bmr.Quantity;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == bmr.vatpercent).FirstOrDefault().CalculatedPrice += (bmr.ItemPrice - (bmr.ItemPrice / ((100 + bmr.vatpercent) / 100))) * bmr.Quantity;
                        }
                    }
                    PackageSelected = Convert.ToBoolean(objconfig.PackageID);
                    //Equipment
                    foreach (ManageEquipment eqp in objconfig.EquipmentLst)
                    {
                        TotalEquipmentPrice += eqp.ItemPrice * eqp.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = eqp.vatpercent;
                            v.CalculatedPrice = (eqp.ItemPrice - (eqp.ItemPrice / ((100 + eqp.vatpercent) / 100))) * eqp.Quantity;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault().CalculatedPrice += (eqp.ItemPrice - (eqp.ItemPrice / ((100 + eqp.vatpercent) / 100))) * eqp.Quantity;
                        }
                    }
                    //Manage Extras
                    foreach (ManageExtras ext in objconfig.ManageExtrasLst)
                    {
                        TotalExtraPrice += ext.ItemPrice * ext.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == ext.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = ext.vatpercent;
                            v.CalculatedPrice = (ext.ItemPrice - (ext.ItemPrice / ((100 + ext.vatpercent) / 100))) * ext.Quantity;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == ext.vatpercent).FirstOrDefault().CalculatedPrice += (ext.ItemPrice - (ext.ItemPrice / ((100 + ext.vatpercent) / 100))) * ext.Quantity;
                        }
                    }
                    //Manage Package Item
                    decimal restmeetingroomprice = 0;
                    decimal itemtotalprice = 0;
                    foreach (ManagePackageItem pck in objconfig.ManagePackageLst)
                    {
                        itemtotalprice += pck.ItemPrice;
                        TotalPackagePrice += pck.ItemPrice * pck.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == pck.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = pck.vatpercent;
                            v.CalculatedPrice = (pck.ItemPrice - (pck.ItemPrice / ((100 + pck.vatpercent) / 100))) * pck.Quantity;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == pck.vatpercent).FirstOrDefault().CalculatedPrice += (pck.ItemPrice - (pck.ItemPrice / ((100 + pck.vatpercent) / 100))) * pck.Quantity;
                        }
                    }
                    restmeetingroomprice = (objconfig.PackagePricePerPerson - itemtotalprice) * objconfig.NoOfParticepant;
                    if (PackageSelected)
                    {
                        if (VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = objconfig.MeetingroomVAT;
                            v.CalculatedPrice = restmeetingroomprice - (restmeetingroomprice / ((100 + objconfig.MeetingroomVAT) / 100));
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault().CalculatedPrice += restmeetingroomprice - (restmeetingroomprice / ((100 + objconfig.MeetingroomVAT) / 100));
                        }
                    }
                    else
                    {
                        if (VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = objconfig.MeetingroomVAT;
                            v.CalculatedPrice = objconfig.MeetingroomPrice - (objconfig.MeetingroomPrice / ((100 + objconfig.MeetingroomVAT) / 100));
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault().CalculatedPrice += objconfig.MeetingroomPrice - (objconfig.MeetingroomPrice / ((100 + objconfig.MeetingroomVAT) / 100));
                        }
                    }
                }
            }

        }
    }
    #endregion

   



}