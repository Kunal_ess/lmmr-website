﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RequestDetails.ascx.cs" Inherits="HotelUser_usercontrols_RequestDetails" %>
<div id="divbookingdetails" align="left" runat="server">
    <div class="booking-details" id="Divdetails" runat="server">
        <!--request-step3-body ENDS HERE-->
        <div class="booking-step3-detail-body" style="width: 100%;">
            <div class="booking-step3-detail-body" style="width: 100%;">
                <table width="100%" cellpadding="5" cellspacing="1" border="0" class="one">
                    <tr>
                        <td>
                            <b>Contact name:</b>
                        </td>
                        <td>
                            <asp:Label ID="lblContactPerson" runat="server" Text=""></asp:Label>
                        </td>
                        <td>
                            <b>Phone No:</b>
                        </td>
                        <td>
                            <asp:Label ID="lblContactPhone" runat="server" Text=""></asp:Label>
                        </td>
                        <td colspan="2">
                            <b>Email :</b> <a id="lblConatctpersonEmail" runat="server" href="#"></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Date from:</b>
                        </td>
                        <td>
                            <asp:Label ID="lblFromDt" runat="server" Text=""></asp:Label>
                        </td>
                        <td>
                            <b>To:</b>
                        </td>
                        <td>
                            <asp:Label ID="lblToDate" runat="server" Text=""></asp:Label>
                        </td>
                        <td colspan="2">
                            <b>Duration:</b>
                            <asp:Label ID="lblBookedDays" runat="server" Text=""></asp:Label>
                            <asp:Label ID="lblBookedDays1" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Contact Address</b>
                        </td>
                        <td colspan="5">
                            <asp:Label ID="lblContactAddress" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <table width="100%" bgcolor="#9cc6ca" cellspacing="0" cellpadding="1">
                <tr bgcolor="#E3F0F1">
                    <td>
                        <!--request-date-body ENDS HERE-->
                        <!--Hotels / Facility body START HERE-->
                        <table width="100%" bgcolor="#9cc6ca" cellspacing="0" cellpadding="1">
                            <asp:Repeater ID="rptHotel" runat="server" OnItemDataBound="rptHotel_ItemDataBound">
                                <ItemTemplate>
                                    <tr bgcolor="#E3F0F1">
                                        <td>
                                            <h3 class="first">
                                                <asp:Label ID="lblHotelName" runat="server"></asp:Label></h3>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%">
                                            <table width="100%" bgcolor="#9cc6ca" cellspacing="1" cellpadding="1">
                                                <tr bgcolor="#CCD8D8" valign="top">
                                                    <td align="left" valign="top">
                                                        <div class="hotels-facility-step3-inner-box1">
                                                            <b>Meeting rooms </b>
                                                        </div>
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <div class="hotels-facility-step3-inner-box2">
                                                            <b>Descriptions</b>
                                                        </div>
                                                    </td>
                                                    <td align="center" valign="top">
                                                        <div class="hotels-facility-step3-inner-box3">
                                                            <b>Qty</b>
                                                        </div>
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <div class="hotels-facility-step3-inner-box4">
                                                            <b>Category </b>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <div class="hotels-facility-step3-inner">
                                                    <asp:Repeater ID="rptMeetingroom" runat="server" OnItemDataBound="rptMeetingroom_ItemDataBound">
                                                        <ItemTemplate>
                                                            <tr bgcolor="#E3F0F1">
                                                                <td align="left">
                                                                    <div class="hotels-facility-step3-inner-box1">
                                                                        <asp:Label ID="lblMeetingRoomName" runat="server"></asp:Label>
                                                                    </div>
                                                                </td>
                                                                <td align="left">
                                                                    <div class="hotels-facility-step3-inner-box2">
                                                                        <p>
                                                                            <asp:Label ID="lblConfigurationType" runat="server">Conference</asp:Label></p>
                                                                        <p>
                                                                            <asp:Label ID="lblMaxandMinCapacity" runat="server">50-60</asp:Label></p>
                                                                    </div>
                                                                </td>
                                                                <td align="center">
                                                                    <div class="hotels-facility-step3-inner-box3">
                                                                        <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                                                    </div>
                                                                </td>
                                                                <td align="left">
                                                                    <div class="hotels-facility-step3-inner-box4">
                                                                        <asp:Label ID="lblIsMain" runat="server"></asp:Label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                            </table>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                        <!--Hotels / Facility body ENDS HERE-->
                        <!--request-step3-body START HERE-->
                        <asp:Panel ID="pnlPackage" runat="server">
                            <table width="100%" bgcolor="#9cc6ca" cellspacing="1" cellpadding="1">
                                <tr bgcolor="#f1f8f8">
                                    <td colspan="2">
                                        <h1>
                                            <span>Package booked</span></h1>
                                    </td>
                                </tr>
                                <tr bgcolor="#ccd8d8" valign="top">
                                    <td width="30%">
                                        <div class="request-step2-body-inner1-haeding1">
                                            &nbsp;
                                        </div>
                                    </td>
                                    <td width="69%">
                                        <div class="request-step3-body-inner-haeding2">
                                            Description
                                        </div>
                                    </td>
                                </tr>
                                <tr bgcolor="#e3f0f1">
                                    <td width="30%">
                                        <div class="request-step2-ul-body1-box1">
                                            <asp:Label ID="lblPackageName" runat="server"></asp:Label>
                                        </div>
                                    </td>
                                    <td width="69%">
                                        <div class="request-step2-ul-body1-box2">
                                            <asp:Label ID="lblpackageDescription" runat="server"></asp:Label>
                                        </div>
                                    </td>
                                </tr>
                                <tr bgcolor="#e3f0f1">
                                    <td colspan="2">
                                        <table cellpadding="2" cellspacing="2" width="100%">
                                            <asp:Repeater ID="rptPackageItem" runat="server" OnItemDataBound="rptPackageItem_ItemDataBound">
                                                <HeaderTemplate>
                                                    <tr>
                                                        <th>
                                                        </th>
                                                        <th>
                                                            Description
                                                        </th>
                                                        <th>
                                                            Qty
                                                        </th>
                                                    </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblItemName" runat="server"></asp:Label><asp:HiddenField ID="hdnItemId"
                                                                runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="txtQuantity" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </table>
                                    </td>
                                </tr>
                                <asp:Panel ID="pnlExtra" runat="server" Width="100%">
                                    <tr bgcolor="#f1f8f8">
                                        <td colspan="3">
                                            <h1>
                                                Extra's</h1>
                                        </td>
                                    </tr>
                                    <tr bgcolor="#ccd8d8" valign="top">
                                        <td>
                                            <div class="request-step3-body-inner-haeding1">
                                                &nbsp;
                                            </div>
                                        </td>
                                        <td>
                                            <div class="request-step3-body-inner-haeding2">
                                                Description
                                            </div>
                                        </td>
                                        <td>
                                            <div class="request-step3-body-inner-haeding3">
                                                Qty
                                            </div>
                                        </td>
                                    </tr>
                                    <asp:Repeater ID="rptExtras" runat="server" OnItemDataBound="rptExtras_ItemDataBound">
                                        <ItemTemplate>
                                            <tr bgcolor="#e3f0f1">
                                                <td>
                                                    <div class="request-step3-ul-body-box1">
                                                        <asp:Label ID="lblItemName" runat="server"></asp:Label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="request-step3-ul-body-box2">
                                                        <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="request-step3-ul-body-box3">
                                                        <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </asp:Panel>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            <div class="request-step3-body">
                <asp:Panel ID="pnlFoodAndBravrages" runat="server">
                    <table width="100%" bgcolor="#9cc6ca" cellspacing="1" cellpadding="8">
                        <tr bgcolor="#f1f8f8">
                            <td colspan="3">
                                <h1 style="overflow: hidden" class="request">
                                    <span>Build your meeting</span></h1>
                            </td>
                        </tr>
                        <tr bgcolor="#ccd8d8" valign="top">
                            <td width="30%">
                                <div class="request-step3-body-inner-haeding1">
                                    &nbsp;
                                </div>
                            </td>
                            <td width="65%">
                                <div class="request-step3-body-inner-haeding2">
                                    Description
                                </div>
                            </td>
                            <td width="5%">
                                <div class="request-step3-body-inner-haeding3">
                                    Qty
                                </div>
                            </td>
                        </tr>
                        <asp:Repeater ID="rptFoodandBravragesDay" runat="server" OnItemDataBound="rptFoodandBravragesDay_ItemDataBound">
                            <ItemTemplate>
                                <tr bgcolor="#f1f8f8">
                                    <td colspan="3">
                                        <div class="request-step3-ul-body">
                                            <h3 class="requesth3">
                                                Day
                                                <asp:Label ID="lblSelectDay" runat="server"></asp:Label></h3>
                                        </div>
                                    </td>
                                </tr>
                                <asp:Repeater ID="rptFoodandBravragesItem" runat="server" OnItemDataBound="rptFoodandBravragesItem_ItemDataBound">
                                    <ItemTemplate>
                                        <tr bgcolor="#e3f0f1">
                                            <td>
                                                <div class="request-step3-ul-body-box1">
                                                    <asp:Label ID="lblItemName" runat="server"></asp:Label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="request-step3-ul-body-box2">
                                                    <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="request-step3-ul-body-box3">
                                                    <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlEquipment" runat="server">
                    <table width="100%" bgcolor="#9cc6ca" cellspacing="1" cellpadding="8">
                        <tr bgcolor="#f1f8f8">
                            <td colspan="3">
                                <h1 class="request">
                                    Equipment</h1>
                            </td>
                        </tr>
                        <tr bgcolor="#ccd8d8" valign="top">
                            <td width="30%">
                                <div class="request-step3-body-inner-haeding1">
                                    &nbsp;
                                </div>
                            </td>
                            <td width="65%">
                                <div class="request-step3-body-inner-haeding2">
                                    Description
                                </div>
                            </td>
                            <td width="5%">
                                <div class="request-step3-body-inner-haeding3">
                                    Qty
                                </div>
                            </td>
                        </tr>
                        <asp:Repeater ID="rptEquipmentDay" runat="server" OnItemDataBound="rptEquipmentDay_ItemDataBound">
                            <ItemTemplate>
                                <div class="request-step3-ul-body">
                                    <tr bgcolor="#f1f8f8">
                                        <td colspan="3">
                                            <h3 class="requesth3">
                                                Day
                                                <asp:Label ID="lblSelectDay" runat="server"></asp:Label></h3>
                                        </td>
                                    </tr>
                                    <div class="request-step3-ul-body-box">
                                        <asp:Repeater ID="rptEquipmentItem" runat="server" OnItemDataBound="rptEquipmentItem_ItemDataBound">
                                            <ItemTemplate>
                                                <tr bgcolor="#e3f0f1">
                                                    <td>
                                                        <div class="request-step3-ul-body-box1">
                                                            <asp:Label ID="lblItemName" runat="server"></asp:Label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="request-step3-ul-body-box2">
                                                            <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="request-step3-ul-body-box4">
                                                            <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlAccomodation" runat="server">
                    <table width="100%" bgcolor="#9cc6ca" cellspacing="1" cellpadding="8">
                        <tr bgcolor="#E3F0F1">
                            <td colspan="3">
                                <h1 class="request">
                                    Accommodation</h1>
                                <br />
                            </td>
                        </tr>
                        <tr bgcolor="#CCD8D8" valign="top">
                            <td width="30%">
                                <div class="request-step3-body-inner-haeding1">
                                    &nbsp;
                                </div>
                            </td>
                            <td width="65%">
                                <div class="request-step3-body-inner-haeding2">
                                </div>
                            </td>
                            <td width="5%">
                                <div class="request-step3-body-inner-haeding3">
                                    Qty
                                </div>
                            </td>
                        </tr>
                        <tr bgcolor="#E3F0F1">
                            <td>
                                <div class="request-step3-ul-body-box1">
                                    Bedroom
                                </div>
                            </td>
                            <td>
                                <div class="request-step3-ul-body-box2">
                                </div>
                            </td>
                            <td>
                                <div class="request-step3-ul-body-box4">
                                    <asp:Label ID="lblaccomodationQun" runat="server">0</asp:Label>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
            <!--request-step3-body ENDS HERE-->
            <div class="request-step3-special-request">
                <table width="100%" bgcolor="#9cc6ca" cellspacing="1" cellpadding="8">
                    <tr bgcolor="#E3F0F1">
                        <td>
                            <h1 style="width: 720px; overflow: hidden;" class="request">
                                Special request</h1>
                        </td>
                    </tr>
                    <tr bgcolor="#E3F0F1">
                        <td>
                            <div class="request-step3-special-request-textareabox">
                                <asp:Label ID="lblSpecialRequest" runat="server">No special request</asp:Label>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <!--special-request ENDS HERE-->
        </div>
    </div>
    <%-- <br />
    <table width="99%" bgcolor="#9cc6ca" cellspacing="1" cellpadding="1">
        <tr bgcolor="#E3F0F1">
            <td>
                <img src="../Images/help.jpg" />&nbsp;&nbsp;Red elements have no pricing in your
                venue backend system. In order to also present them online, fill out pricing on
                venue backend system
            </td>
        </tr>
    </table>--%>
</div>
