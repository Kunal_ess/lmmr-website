﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BookingDetails.ascx.cs" Inherits="HotelUser_usercontrols_BookingDetails" %>

<div id="divbookingdetails" align="left" runat="server" style="width: 100%;">
    <div class="booking-details" id="Divdetails" runat="server" style="width: 100%;">
        <div class="booking-step3-detail-body" style="width: 100%;">
            <table width="100%" cellpadding="8" bgcolor="#fffff" cellspacing="1" border="0">
                <tr>
                    <td>
                        <b>Contact name:</b>
                    </td>
                    <td>
                        <asp:Label ID="lblContactPerson" runat="server" Text=""></asp:Label>
                    </td>
                    <td>
                        <b>Phone No:</b>
                    </td>
                    <td>
                        <asp:Label ID="lblContactPhone" runat="server" Text=""></asp:Label>
                    </td>
                    <td colspan="2">
                        <b>Email :</b> <a id="lblConatctpersonEmail" runat="server" href="#"></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Date from:</b>
                    </td>
                    <td>
                        <asp:Label ID="lblFromDt" runat="server" Text=""></asp:Label>
                    </td>
                    <td>
                        <b>To:</b>
                    </td>
                    <td>
                        <asp:Label ID="lblToDate" runat="server" Text=""></asp:Label>
                    </td>
                    <td colspan="2">
                        <b>Duration:</b>
                        <asp:Label ID="lblBookedDays" runat="server" Text=""></asp:Label>
                        <asp:Label ID="lblBookedDays1" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Contact Address</b>
                    </td>
                    <td colspan="5">
                        <asp:Label ID="lblContactAddress" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <table width="100%" bgcolor="#bfd2a5" cellspacing="1" cellpadding="8">
            <tr bgcolor="#ecf2e3">
                <td>
                    <asp:Repeater ID="rptMeetingRoom" runat="server" OnItemDataBound="rptMeetingRoom_ItemDataBound">
                        <ItemTemplate>
                            <table width="100%" bgcolor="#bfd2a5" cellspacing="1" cellpadding="8">
                                <tr bgcolor="#ecf2e3">
                                    <td>
                                        <h1>
                                            Meeting room 1</h1>
                                    </td>
                                </tr>
                            </table>
                            <asp:Repeater ID="rptMeetingRoomConfigure" runat="server" OnItemDataBound="rptMeetingRoomConfigure_ItemDataBound">
                                <ItemTemplate>
                                    <h3>
                                        Day
                                        <asp:Label ID="lblSelectedDay" runat="server">1</asp:Label></h3>
                                    <table width="100%" bgcolor="#bfd2a5" cellspacing="1" cellpadding="8">
                                        <tr bgcolor="#d4d9cc" style="font-weight: bold">
                                            <th>
                                            </th>
                                            <th align="center">
                                                Description
                                            </th>
                                            <th align="center">
                                                Price
                                            </th>
                                            <th align="center">
                                                Start
                                            </th>
                                            <th align="center">
                                                End
                                            </th>
                                            <th align="center">
                                                Qty
                                            </th>
                                            <th align="center">
                                                Total
                                            </th>
                                        </tr>
                                        <tr bgcolor="#ecf2e3">
                                            <td>
                                               
                                                    "<asp:Label ID="lblMeetingRoomName" runat="server">Ambasador</asp:Label>"
                                             
                                            </td>
                                            <td align="center">
                                               
                                                    <p>
                                                        <asp:Label ID="lblMeetingRoomType" runat="server">Conference</asp:Label></p>
                                                    <p>
                                                        <asp:Label ID="lblMinMaxCapacity" runat="server">50-60</asp:Label></p>
                                               
                                            </td>
                                            <td align="center">
                                               
                                                    <span class="currencyClass"></span>
                                                    <asp:Label ID="lblMeetingRoomActualPrice" runat="server">&#8364; 400</asp:Label>
                                            
                                            </td>
                                            <td align="center">
                                               
                                                    <asp:Label ID="lblStart" runat="server">08:00</asp:Label>
                                               
                                            </td>
                                            <td align="center">
                                                
                                                    <asp:Label ID="lblEnd" runat="server">12:15</asp:Label>
                                            
                                            </td>
                                            <td align="center">
                                                
                                                    <asp:Label ID="lblNumberOfParticepant" runat="server">32</asp:Label>
                                        
                                            </td>
                                            <td align="center">
                                               
                                                    <span class="currencyClass"></span>
                                                    <asp:Label ID="lblTotalMeetingRoomPrice" runat="server">&#8364; 600</asp:Label>
                                           
                                            </td>
                                        </tr>
                                        <tr bgcolor="#ffffff">
                                            <td colspan="7" align="right">
                                              
                                                    Total:&nbsp; &nbsp; <b><span class="currencyClass"></span>
                                                        <asp:Label ID="lblTotalFinalMeetingRoomPrice" runat="server"> 600</asp:Label></b>
                                                
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:Panel ID="pnlIsPackageSelected" runat="server">
                                        <!--booking-step3-packages-mainbody START HERE-->
                                        
                                            <table width="100%" bgcolor="#bfd2a5" cellspacing="1" cellpadding="8">
                                                <tr bgcolor="#ecf2e3">
                                                    <td>
                                                        <h3>
                                                            Packages
                                                            <br />
                                                        </h3>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table cellpadding="8" cellspacing="1" bgcolor="#bfd2a5" width="100%" align="center">
                                                <tr bgcolor="#d4d9cc">
                                                    <th>
                                                    </th>
                                                    <th align="left">
                                                        <strong>Description </strong>
                                                    </th>
                                                    <th align="center">
                                                        <strong>Total</strong>
                                                    </th>
                                                </tr>
                                                <tr bgcolor="#ecf2e3">
                                                    <td>
                                                    
                                                            <asp:Label ID="lblSelectedPackage" runat="server">DDr2</asp:Label>
                                                    
                                                    </td>
                                                    <td>
                                                        
                                                            <p>
                                                                <asp:Label ID="lblPackageDescription" runat="server">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam mollis, leo nec consequat
                                                    vehicula, felis augue scelerisque nunc,</asp:Label>
                                                            </p>
                                                     
                                                    </td>
                                                    <td align="center">
                                                      
                                                            <span class="currencyClass"></span>
                                                            <asp:Label ID="lblPackagePrice" runat="server">&#8364; 600</asp:Label>
                                                  
                                                    </td>
                                                </tr>
                                                <asp:Repeater ID="rptPackageItem" runat="server" OnItemDataBound="rptPackageItem_ItemDataBound">
                                                    <ItemTemplate>
                                                 
                                                                <asp:HiddenField ID="hdnItemID" runat="server" />
                                                             
                                                              
                                                                        <tr bgcolor="#ecf2e3">
                                                                            <td>
                                                                                
                                                                                    <asp:Label ID="lblPackageItem" runat="server">Welcome cofee break</asp:Label>
                                                                               
                                                                            </td>
                                                                            <td>
                                                                                <table width="100%">
                                                                                    <tr>
                                                                                        <td align="left" valign="top">
                                                                                            From:
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                            
                                                                                                <asp:Label ID="lblFromTime" runat="server"></asp:Label>
                                                                                            
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                            To:
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                            
                                                                                                <asp:Label ID="lblToTime" runat="server"></asp:Label>
                                                                                            
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                            Qty:
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                            
                                                                                                <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                                                                            
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                   
                                                     
                                                           
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <asp:Panel ID="pnlIsExtra" runat="server">
                                                    <tr bgcolor="#ecf2e3">
                                                        <td colspan="3">
                                                          
                                                                <asp:Label ID="lblextratitle" runat="server" Text=" Extra's"></asp:Label>
                                                       
                                                        </td>
                                                    </tr>
                                                    <asp:Repeater ID="rptExtra" runat="server" OnItemDataBound="rptExtra_ItemDataBound">
                                                                    <ItemTemplate>
                                                                       
                                                                      
                                                                                <tr  bgcolor="#ecf2e3">
                                                                                    <td>
                                                                                        
                                                                                            <asp:Label ID="lblPackageItem" runat="server">Welcome cofee break</asp:Label>
                                                                                        
                                                                                    </td>
                                                                                    <td>
                                                                                        
                                                                                        <table width="100%">
                                                                                        <tr >
                                                                                        <td width="15%">From : </td>
                                                                                        <td width="15%"><asp:Label ID="lblFrom" runat="server"></asp:Label> <asp:Label ID="lblTo" runat="server"></asp:Label></td>
                                                                                        <td width="15%">Qty :</td>
                                                                                        <td width="15%"><asp:Label ID="lblQuntity" runat="server"></asp:Label></td>
                                                                                        <td width="27%"></td>
                                                                                        </tr>
                                                                                        </table>
                                                                                        &nbsp; &nbsp;
                                                                                     
                                                                                        
                                                                                    </td>
                                                                                  
                                                                                    <td>
                                                                                      <asp:Label ID="lblTotal" runat="server" ></asp:Label>  
                                                                                    </td>
                                                                                </tr>
                                                                         
                                                                      
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                    <tr bgcolor="#ecf2e3">
                                                        <td align="right" colspan="3">
                                                            
                                                                <span class="currencyClass"></span>
                                                                <asp:Label runat="server" ID="lblExtraPrice">&#8364; 120</asp:Label>
                                                            
                                                        </td>
                                                    </tr>
                                                </asp:Panel>
                                                <tr>
                                                    <td colspan="3" align="right" bgcolor="#ffffff">
                                                        
                                                            Total:&nbsp; &nbsp; <b><span class="currencyClass"></span>
                                                                <asp:Label ID="lblTotalPackagePrice" runat="server">&#8364; 720</asp:Label></b>
                                                        
                                                    </td>
                                                </tr>
                                            </table>
                                        
                                    </asp:Panel>
                                    <asp:Panel ID="pnlBuildMeeting" runat="server">
                                        <!--booking-step3-BuildMeeting-mainbody START HERE-->
                                        
                                            <table width="100%" bgcolor="#bfd2a5" cellspacing="1" cellpadding="8">
                                                <tr bgcolor="#ecf2e3">
                                                    <td>
                                                        <h3>
                                                            Build Your Meeting<br />
                                                        </h3>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" bgcolor="#bfd2a5" cellspacing="1" cellpadding="8">
                                                <tr bgcolor="#d4d9cc">
                                                    <th>
                                                    </th>
                                                    <th align="left">
                                                        Description
                                                    </th>
                                                    <th align="center">
                                                        Qty
                                                    </th>
                                                    <th align="center">
                                                        Total
                                                    </th>
                                                </tr>
                                              
                                                            <asp:Repeater ID="rptBuildMeeting" runat="server" OnItemDataBound="rptBuildMeeting_ItemDataBound">
                                                                <ItemTemplate>
                                                                    <tr bgcolor="#ecf2e3">
                                                                        <td align="left" valign="top">
                                                                            
                                                                                <asp:Label ID="lblItemName" runat="server">Beamer</asp:Label>
                                                                            
                                                                        </td>
                                                                        <td align="left" valign="top">
                                                                            
                                                                                <p>
                                                                                    <asp:Label ID="lblItemDescription" runat="server">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam mollis, leo nec consequat
                                                            vehicula, felis augue scelerisque nunc,</asp:Label>
                                                                                </p>
                                                                            
                                                                        </td>
                                                                        <td align="center" valign="top">
                                                                            
                                                                                <asp:Label ID="lblQuantity" runat="server">1</asp:Label>
                                                                            
                                                                        </td>
                                                                        <td align="center" valign="top">
                                                                            
                                                                                <span class="currencyClass"></span>
                                                                                <asp:Label ID="lblTotalItemPrice" runat="server">&#8364; 5</asp:Label>
                                                                            
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                      
                                                <tr bgcolor="#ffffff" align="right">
                                                    <td colspan="4">
                                                        
                                                            Total:&nbsp; &nbsp; <b><span class="currencyClass"></span>
                                                                <asp:Label ID="lblTotalBuildPackagePrice" runat="server">&#8364; 30</asp:Label></b>
                                                        
                                                    </td>
                                                </tr>
                                            </table>
                                        
                                    </asp:Panel>
                                    <!--booking-step3-BuildMeeting ENDS HERE-->
                                    <asp:Panel ID="pnlEquipment" runat="server">
                                        <!--booking-step3-equipment-mainbody START HERE-->
                                        
                                            <table width="100%" bgcolor="#bfd2a5" cellspacing="1" cellpadding="8">
                                                <tr bgcolor="#ecf2e3">
                                                    <td>
                                                        <h3>
                                                            Equipment</h3>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" bgcolor="#bfd2a5" cellspacing="1" cellpadding="8">
                                                <tr bgcolor="#d4d9cc">
                                                    <th>
                                                    </th>
                                                    <th align="left">
                                                        Description
                                                    </th>
                                                    <th align="center">
                                                        Qty
                                                    </th>
                                                    <th align="center">
                                                        Total
                                                    </th>
                                                </tr>
                                                <asp:Repeater ID="rptEquipment" runat="server" OnItemDataBound="rptEquipment_ItemDataBound">
                                                    <ItemTemplate>
                                                        <tr bgcolor="#ecf2e3">
                                                            <td>
                                                                
                                                                    <asp:Label ID="lblItemName" runat="server">Beamer</asp:Label>
                                                                
                                                            </td>
                                                            <td>
                                                                
                                                                    <p>
                                                                        <asp:Label ID="lblItemDescription" runat="server">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam mollis, leo nec consequat
                                                            vehicula, felis augue scelerisque nunc,</asp:Label>
                                                                    </p>
                                                                
                                                            </td>
                                                            <td align="center">
                                                                
                                                                    <asp:Label ID="lblQuantity" runat="server">1</asp:Label>
                                                                
                                                            </td>
                                                            <td align="center">
                                                                
                                                                    <span class="currencyClass"></span>
                                                                    <asp:Label ID="lblTotalPrice" runat="server">&#8364; 5</asp:Label>
                                                                
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <tr bgcolor="#ffffff">
                                                    <td colspan="4" align="right">
                                                        
                                                            Total:&nbsp; &nbsp; <b><span class="currencyClass"></span>
                                                                <asp:Label ID="lblTotalEquipmentPrice" runat="server">&#8364; 30</asp:Label></b>
                                                        
                                                    </td>
                                                </tr>
                                            </table>
                                        
                                        <!--booking-step3-equipment ENDS HERE-->
                                    </asp:Panel>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <table width="100%" bgcolor="#bfd2a5" cellspacing="1" cellpadding="8">
                                <tr bgcolor="#ecf2e3">
                                    <td>
                                        
                                            <h1>
                                                Meeting room 2</h1>
                                        
                                    </td>
                                </tr>
                            </table>
                            <asp:Repeater ID="rptMeetingRoomConfigure" runat="server" OnItemDataBound="rptMeetingRoomConfigure_ItemDataBound">
                                <ItemTemplate>
                                    
                                        <h3>
                                            Day
                                            <asp:Label ID="lblSelectedDay" runat="server">1</asp:Label></h3>
                                    
                                    <table width="100%" bgcolor="#bfd2a5" cellspacing="1" cellpadding="8">
                                        <tr bgcolor="#d4d9cc" style="font-weight: bold">
                                            <th>
                                            </th>
                                            <th align="left">
                                                Description
                                            </th>
                                            <th align="center">
                                                Price
                                            </th>
                                            <th align="center">
                                                Start
                                            </th>
                                            <th align="center">
                                                End
                                            </th>
                                            <th align="center">
                                                Qty
                                            </th>
                                            <th align="center">
                                                Total
                                            </th>
                                        </tr>
                                        <tr bgcolor="#ecf2e3">
                                            <td>
                                                
                                                    "<asp:Label ID="lblMeetingRoomName" runat="server">Ambasador</asp:Label>"
                                                
                                            </td>
                                            <td align="center">
                                                
                                                    <p>
                                                        <asp:Label ID="lblMeetingRoomType" runat="server">Conference</asp:Label></p>
                                                    <p>
                                                        <asp:Label ID="lblMinMaxCapacity" runat="server">50-60</asp:Label></p>
                                                
                                            </td>
                                            <td align="center">
                                                
                                                    <span class="currencyClass"></span>
                                                    <asp:Label ID="lblMeetingRoomActualPrice" runat="server">&#8364; 400</asp:Label>
                                                
                                            </td>
                                            <td align="center">
                                                
                                                    <asp:Label ID="lblStart" runat="server">08:00</asp:Label>
                                                
                                            </td>
                                            <td align="center">
                                                
                                                    <asp:Label ID="lblEnd" runat="server">12:15</asp:Label>
                                                
                                            </td>
                                            <td align="center">
                                                
                                                    <asp:Label ID="lblNumberOfParticepant" runat="server">32</asp:Label>
                                                
                                            </td>
                                            <td align="center">
                                                
                                                    <span class="currencyClass"></span>
                                                    <asp:Label ID="lblTotalMeetingRoomPrice" runat="server">&#8364; 600</asp:Label>
                                                
                                            </td>
                                        </tr>
                                        <tr bgcolor="#ffffff">
                                            <td colspan="7" align="right">
                                                
                                                    Total:&nbsp; &nbsp; <b><span class="currencyClass"></span>
                                                        <asp:Label ID="lblTotalFinalMeetingRoomPrice" runat="server">&#8364; 600</asp:Label></b>
                                                
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:Panel ID="pnlIsBreakdown" runat="server">
                                        <table>
                                            <tr bgcolor="#ecf2e3">
                                                <td colspan="7" align="right">
                                                    
                                                        <h3>
                                                            This meeting room is breakout.</h3>
                                                    
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlNotIsBreakdown" runat="server">
                                        <asp:Panel ID="pnlIsPackageSelected" runat="server">
                                            <!--booking-step3-packages-mainbody START HERE-->
                                            
                                                <table width="100%" bgcolor="#bfd2a5" cellspacing="1" cellpadding="8">
                                                    <tr bgcolor="#ecf2e3">
                                                        <td>
                                                            <h3>
                                                                Packages
                                                                <br />
                                                            </h3>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table cellpadding="8" cellspacing="1" bgcolor="#bfd2a5" width="100%" align="center">
                                                    <tr bgcolor="#d4d9cc">
                                                        <th>
                                                        </th>
                                                        <th align="left">
                                                            <strong>Description </strong>
                                                        </th>
                                                        <th align="center">
                                                            <strong>Total</strong>
                                                        </th>
                                                    </tr>
                                                    <tr bgcolor="#ecf2e3">
                                                        <td>
                                                            
                                                                <asp:Label ID="lblSelectedPackage" runat="server">DDr2</asp:Label>
                                                            
                                                        </td>
                                                        <td>
                                                            
                                                                <p>
                                                                    <asp:Label ID="lblPackageDescription" runat="server">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam mollis, leo nec consequat
                                                    vehicula, felis augue scelerisque nunc,</asp:Label>
                                                                </p>
                                                            
                                                        </td>
                                                        <td align="center">
                                                            
                                                                <span class="currencyClass"></span>
                                                                <asp:Label ID="lblPackagePrice" runat="server">&#8364; 600</asp:Label>
                                                            
                                                        </td>
                                                    </tr>
                                                    <asp:Repeater ID="rptPackageItem" runat="server" OnItemDataBound="rptPackageItem_ItemDataBound">
                                                    <ItemTemplate>
                                                 
                                                                <asp:HiddenField ID="hdnItemID" runat="server" />
                                                                
                                                              
                                                                        <tr bgcolor="#ecf2e3">
                                                                            <td>
                                                                                
                                                                                    <asp:Label ID="lblPackageItem" runat="server">Welcome cofee break</asp:Label>
                                                                                
                                                                            </td>
                                                                            <td>
                                                                                <table width="100%">
                                                                                    <tr>
                                                                                        <td align="left" valign="top">
                                                                                            From:
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                            
                                                                                                <asp:Label ID="lblFromTime" runat="server"></asp:Label>
                                                                                            
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                            To:
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                            
                                                                                                <asp:Label ID="lblToTime" runat="server"></asp:Label>
                                                                                            
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                            Qty:
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                            
                                                                                                <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                                                                            
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                   
                                                                
                                                           
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                    <asp:Panel ID="pnlIsExtra" runat="server">
                                                        <tr bgcolor="#ecf2e3">
                                                            <td colspan="3">
                                                                
                                                                    <asp:Label ID="lblextratitle" runat="server" Text=" Extra's"></asp:Label>
                                                                
                                                            </td>
                                                        </tr>
                                                     
                                                      <asp:Repeater ID="rptExtra" runat="server" OnItemDataBound="rptExtra_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        
                                                                      
                                                                                <tr  bgcolor="#ecf2e3">
                                                                                    <td>
                                                                                        
                                                                                            <asp:Label ID="lblPackageItem" runat="server">Welcome cofee break</asp:Label>
                                                                                        
                                                                                    </td>
                                                                                    <td>
                                                                                        
                                                                                        <table cellpadding="8" cellspacing="1" bgcolor="#bfd2a5" width="100%">
                                                                                        <tr bgcolor="#ecf2e3">
                                                                                        <td>From : </td>
                                                                                        <td>  <asp:Label ID="lblFrom" runat="server"></asp:Label> <asp:Label ID="lblTo" runat="server"></asp:Label></td>
                                                                                        <td>Qty :</td>
                                                                                        <td><asp:Label ID="lblQuntity" runat="server"></asp:Label></td>
                                                                                        </tr>
                                                                                        </table>
                                                                                        &nbsp; &nbsp;
                                                                                     
                                                                                        
                                                                                    </td>
                                                                                  
                                                                                    <td>
                                                                                      <asp:Label ID="lblTotal" runat="server" ></asp:Label>  
                                                                                    </td>
                                                                                </tr>
                                                                         
                                                                        
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                       
                                                        <tr bgcolor="#ecf2e3">
                                                            <td align="right" colspan="3">
                                                                
                                                                    <span class="currencyClass"></span>
                                                                    <asp:Label runat="server" ID="lblExtraPrice">&#8364; 120</asp:Label>
                                                                
                                                            </td>
                                                        </tr>
                                                    </asp:Panel>
                                                    <tr>
                                                        <td colspan="3" align="right" bgcolor="#ffffff">
                                                            
                                                                Total:&nbsp; &nbsp; <b><span class="currencyClass"></span>
                                                                    <asp:Label ID="lblTotalPackagePrice" runat="server">&#8364; 720</asp:Label></b>
                                                            
                                                        </td>
                                                    </tr>
                                                </table>
                                            
                                        </asp:Panel>
                                        <asp:Panel ID="pnlBuildMeeting" runat="server">
                                            <!--booking-step3-BuildMeeting-mainbody START HERE-->
                                            
                                                <table width="100%" bgcolor="#bfd2a5" cellspacing="1" cellpadding="8">
                                                    <tr bgcolor="#ecf2e3">
                                                        <td>
                                                            <h3>
                                                                Build Your Meeting<br />
                                                            </h3>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="100%" bgcolor="#bfd2a5" cellspacing="1" cellpadding="8">
                                                    <tr bgcolor="#d4d9cc">
                                                        <th>
                                                        </th>
                                                        <th align="left">
                                                            Description
                                                        </th>
                                                        <th align="left">
                                                            Qty
                                                        </th>
                                                        <th align="center">
                                                            Total
                                                        </th>
                                                    </tr>
                                      
                                                    <asp:Repeater ID="rptBuildMeeting" runat="server" OnItemDataBound="rptBuildMeeting_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <tr bgcolor="#ecf2e3">
                                                                            <td align="left" valign="top">
                                                                                
                                                                                    <asp:Label ID="lblItemName" runat="server">Beamer</asp:Label>
                                                                                
                                                                            </td>
                                                                            <td align="left" valign="top">
                                                                                
                                                                                    <p>
                                                                                        <asp:Label ID="lblItemDescription" runat="server">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam mollis, leo nec consequat
                                                            vehicula, felis augue scelerisque nunc,</asp:Label>
                                                                                    </p>
                                                                                
                                                                            </td>
                                                                            <td align="center" valign="top">
                                                                                
                                                                                    <asp:Label ID="lblQuantity" runat="server">1</asp:Label>
                                                                                
                                                                            </td>
                                                                            <td align="center" valign="top">
                                                                                
                                                                                    <span class="currencyClass"></span>
                                                                                    <asp:Label ID="lblTotalItemPrice" runat="server">&#8364; 5</asp:Label>
                                                                                
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                    
                                                    <tr bgcolor="#ffffff" align="right">
                                                        <td colspan="4">
                                                            
                                                                Total:&nbsp; &nbsp; <b><span class="currencyClass"></span>
                                                                    <asp:Label ID="lblTotalBuildPackagePrice" runat="server">&#8364; 30</asp:Label></b>
                                                            
                                                        </td>
                                                    </tr>
                                                </table>
                                            
                                        </asp:Panel>
                                        <!--booking-step3-BuildMeeting ENDS HERE-->
                                        <asp:Panel ID="pnlEquipment" runat="server">
                                            <!--booking-step3-equipment-mainbody START HERE-->
                                            
                                                <table width="100%" bgcolor="#bfd2a5" cellspacing="1" cellpadding="8">
                                                    <tr bgcolor="#ecf2e3">
                                                        <td>
                                                            <h3>
                                                                Equipment</h3>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="100%" bgcolor="#bfd2a5" cellspacing="1" cellpadding="8">
                                                    <tr bgcolor="#d4d9cc">
                                                        <th>
                                                        </th>
                                                        <th align="left">
                                                            Description
                                                        </th>
                                                        <th align="center">
                                                            Qty
                                                        </th>
                                                        <th align="center">
                                                            Total
                                                        </th>
                                                    </tr>
                                                    <asp:Repeater ID="rptEquipment" runat="server" OnItemDataBound="rptEquipment_ItemDataBound">
                                                        <ItemTemplate>
                                                            <tr bgcolor="#ecf2e3">
                                                                <td>
                                                                    
                                                                        <asp:Label ID="lblItemName" runat="server">Beamer</asp:Label>
                                                                    
                                                                </td>
                                                                <td>
                                                                    
                                                                        <p>
                                                                            <asp:Label ID="lblItemDescription" runat="server">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam mollis, leo nec consequat
                                                            vehicula, felis augue scelerisque nunc,</asp:Label>
                                                                        </p>
                                                                    
                                                                </td>
                                                                <td align="center">
                                                                    
                                                                        <asp:Label ID="lblQuantity" runat="server">1</asp:Label>
                                                                    
                                                                </td>
                                                                <td align="center">
                                                                    
                                                                        <span class="currencyClass"></span>
                                                                        <asp:Label ID="lblTotalPrice" runat="server">&#8364; 5</asp:Label>
                                                                   
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                    <tr bgcolor="#ffffff">
                                                        <td colspan="4" align="right">
                                                            
                                                                Total:&nbsp; &nbsp; <b><span class="currencyClass"></span>
                                                                    <asp:Label ID="lblTotalEquipmentPrice" runat="server">&#8364; 30</asp:Label></b>
                                                            
                                                        </td>
                                                    </tr>
                                                </table>
                                            
                                            <!--booking-step3-equipment ENDS HERE-->
                                        </asp:Panel>
                                    </asp:Panel>
                                </ItemTemplate>
                            </asp:Repeater>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                    <asp:Panel ID="pnlAccomodation" runat="server">
                        <!--booking-step3-accomodation-body START HERE-->
                        <div class="booking-step3-accomodation-body">
                            <h1>
                                Bedrooms</h1>
                            <div class="booking-step3-accomodation-heading">
                                <div class="booking-step3-accomodation-heading1">
                                    Description
                                </div>
                                <div class="booking-step3-accomodation-heading2">
                                    &nbsp;
                                </div>
                                <div class="booking-step3-accomodation-heading3">
                                    Type
                                </div>
                                <div class="booking-step3-accomodation-heading4">
                                    Price
                                </div>
                                <div class="booking-step3-accomodation-heading5">
                                    Qty
                                </div>
                                <div class="booking-step3-accomodation-heading6">
                                    Total
                                </div>
                            </div>
                            <ul>
                                <asp:Repeater ID="rptAccomodation" runat="server" OnItemDataBound="rptAccomodation_ItemDataBound">
                                    <ItemTemplate>
                                        <li>
                                            <div class="booking-step3-accomodation-litop-body">
                                                <div class="booking-step3-accomodation-litop-body-main">
                                                    <div class="booking-step3-accomodation-litop1">
                                                        "<asp:Label ID="lblBedroomName" runat="server">Executive</asp:Label>"
                                                    </div>
                                                    <div class="booking-step3-accomodation-litop2">
                                                        &nbsp;
                                                    </div>
                                                    <div class="booking-step3-accomodation-litop3">
                                                        <asp:Label ID="lblBedroomType" runat="server">Single</asp:Label>
                                                    </div>
                                                    <div class="booking-step3-accomodation-litop4">
                                                        <span class="currencyClass"></span>
                                                        <asp:Label ID="lblBedroomPrice" runat="server">&#8364; 45</asp:Label>
                                                    </div>
                                                    <div class="booking-step3-accomodation-litop5">
                                                        <asp:Label ID="lblQuantity" runat="server">2</asp:Label>
                                                    </div>
                                                    <div class="booking-step3-accomodation-litop6">
                                                        <span class="currencyClass"></span>
                                                        <asp:Label ID="lblTotalBedroomPrice" runat="server">&#8364; 90</asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="booking-step3-accomodation-limid-body">
                                                <div class="booking-step3-accomodation-limid1">
                                                    &nbsp;
                                                </div>
                                                <div class="booking-step3-accomodation-limid2">
                                                    Check in
                                                </div>
                                                <div class="booking-step3-accomodation-limid3">
                                                    Checkout
                                                </div>
                                                <div class="booking-step3-accomodation-limid4">
                                                    Notes
                                                </div>
                                            </div>
                                            <div class="booking-step3-accomodation-room-body">
                                                <ul>
                                                    <asp:Repeater ID="rptRoomManage" runat="server" OnItemDataBound="rptRoomManage_ItemDataBound">
                                                        <ItemTemplate>
                                                            <li>
                                                                <div class="booking-step3-accomodation-room-inner-body">
                                                                    <div class="booking-step3-accomodation-room-inner-body-main">
                                                                        <div class="booking-step3-accomodation-liroom1">
                                                                            <span>Room
                                                                                <asp:Label ID="lblRoomIndex" runat="server">1</asp:Label></span>
                                                                            <asp:Label ID="lblAllotmantName" runat="server">Name of big boss</asp:Label>
                                                                        </div>
                                                                        <div class="booking-step3-accomodation-liroom2">
                                                                            <asp:Label ID="lblCheckIn" runat="server">17 / 10 / 2011</asp:Label>
                                                                        </div>
                                                                        <div class="booking-step3-accomodation-liroom3">
                                                                            <asp:Label ID="lblCheckOut" runat="server">18 / 10 / 2011</asp:Label>
                                                                        </div>
                                                                        <div class="booking-step3-accomodation-liroom4">
                                                                            <asp:Label ID="lblNote" runat="server">Need smoking...</asp:Label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </ul>
                                            </div>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                            <div class="booking-step3-accomodation-total">
                                Total: <b><span class="currencyClass"></span>
                                    <asp:Label ID="lblAccomodationPrice" runat="server">&#8364; 150</asp:Label></b>
                            </div>
                        </div>
                        <!--booking-step3-accomodation ENDS HERE-->
                    </asp:Panel>
                    <!--special-request-step3-content START HERE-->
                    <table width="100%" bgcolor="#bfd2a5" cellspacing="1" cellpadding="8">
                        <tr bgcolor="#ecf2e3">
                            <td>
                                <h3>
                                    Special request</h3>
                            </td>
                        </tr>
                        <tr bgcolor="#ecf2e3">
                            <td>
                                <asp:Label ID="lblSpecialRequest" runat="server">No Special Requests</asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" bgcolor="#bfd2a5" cellspacing="1" cellpadding="8">
                        <tr bgcolor="#ffffff" align="right" valign="top">
                            <td>
                                
                                    Final Total: <span class="currencyClass"></span>
                                    <asp:Label ID="lblFinalTotal" runat="server">&#8364; 1120</asp:Label>
                                    <!--special-request ENDS HERE-->
                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:LinkButton ID="tAndCPopUp" runat="server" Visible="false"><%= GetKeyResult("CANCELLATIONTEXT")%>.</asp:LinkButton>
    </div>
    <div id="Nettotalview" runat="server" style="display: none;">
    <table width="100%" bgcolor="#ffffff" cellspacing="1" cellpadding="8">
    <tr>
    <td align="right" >Net Total: <span class="currencyClass"></span>
            <asp:Label ID="lblNetTotal" runat="server">&#8364; 1120</asp:Label></td>
    </tr>
    </table>

    </div>

    <%--<div class="final-total-inner" id="Nettotalview" runat="server" style="display: none;">
        <div class="final-total-inner-left">
            Net Total:
        </div>
        <div class="final-total-inner-right">
            <span class="currencyClass"></span>
            <asp:Label ID="lblNetTotal" runat="server">&#8364; 1120</asp:Label>
        </div>
    </div>--%>
    <asp:Repeater ID="rptVatList" runat="server" OnItemDataBound="rptVatList_ItemDataBound">
        <ItemTemplate>
            <div class="final-total-inner" style="display: none;">
                <div class="final-total-inner-left">
                    <%= ("VAT")%>
                    <asp:Label ID="lblPercentage" runat="server"><%#Eval("VATPercent","{0:0.00}") %></asp:Label>%:
                </div>
                <div class="final-total-inner-right">
                    <span class="currencyClass"></span>
                    <asp:Label ID="lblVatPrice" runat="server"></asp:Label>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <div class="button-center">
        <%--<div class="blue-button" id="divlinkpro" runat="server" align="center">
                            <asp:LinkButton ID="Lnkmovetoprocessed" runat="server" CssClass="link" ForeColor="White"
                                Font-Underline="false" OnClick="Lnkmovetoprocessed_Click">Process</asp:LinkButton>
                        </div>--%>
    </div>
</div>
