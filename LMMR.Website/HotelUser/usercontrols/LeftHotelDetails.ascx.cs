﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Data;
using LMMR.Business;
using LMMR.Entities;
using System.Configuration;
using log4net;
using log4net.Config;
#endregion


public partial class HotelUser_usercontrols_LeftHotelDetails : System.Web.UI.UserControl
{
    #region Variables and Properties
    HotelManager objHotel = new HotelManager();
    HotelInfo ObjHotelinfo = new HotelInfo();
    WizardLinkSettingManager Objwizard = new WizardLinkSettingManager();
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_usercontrols_LeftHotelDetails));
    #endregion

    #region PageLoad
    /// <summary>
    /// This Method is called to bind all the Initials Values in the form.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentUserID"] == null)
        {
            Response.Redirect("~/login.aspx", false);
            return;
        }
        if (Session["CurrentHotelID"] == null)
        {
            Response.Redirect("~/login.aspx", false);
            return;
        }
        if (!IsPostBack)
        {
            BindHotelDetails();            
        }
    }
    #endregion

    #region Methods
    /// <summary>
    /// Bind the Hotel Dropdown and Image of the Selected Hotel.
    /// </summary>
    public void BindHotelDetails()
    {
        if (Session["CurrentHotelID"] == null)
        {
            Response.Redirect("~/login.aspx", false);
            return;
        }
        drpHotelList.DataTextField = "Name";
        drpHotelList.DataValueField = "Id";
        TList<Hotel> htlList;
        //This section is used after operator comes from hotel facility access
        if (Convert.ToString(Session["Operator"]) == "1")
        {
            string whereclause = "Id='" + Session["CurrentHotelID"] + "'";
            htlList = ObjHotelinfo.GetHotelbyCondition(whereclause, String.Empty);
        }
        //This section is used after operator comes from hotel facility access
        else
        {
            htlList = objHotel.GetHotelByClientId(Session["CurrentUserID"] == null ? 0 : Convert.ToInt64(Session["CurrentUserID"]));//Session["CurrentUserID"]
        }        
        if (htlList.Count > 0)
        {
            drpHotelList.DataSource = htlList;
            drpHotelList.DataBind();
            drpHotelList.SelectedValue = Convert.ToString(Session["CurrentHotelID"] == null ? "" : Session["CurrentHotelID"]);
            //Session["CurrentHotelID"] = drpHotelList.SelectedValue;
            imgHotelLogo.ImageUrl = (string.IsNullOrEmpty(htlList.FirstOrDefault(a => a.Id == Convert.ToInt64(Session["CurrentHotelID"])).Logo) ? ConfigurationManager.AppSettings["FilePath"] + "/HotelImage/default_hotel_logo.png" : ConfigurationManager.AppSettings["FilePath"] + "/HotelImage/" + htlList.FirstOrDefault(a => a.Id == Convert.ToInt64(Session["CurrentHotelID"])).Logo);
        }
        else
        {
            Session.Remove("CurrentHotelID");
            Response.Redirect("~/login.aspx", false);
            return;
        }        
    }
    #endregion

    #region Events
    /// <summary>
    /// Work according to Selected dropdown value.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void drpHotelList_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Hotel objhotel = objHotel.GetHotelDetailsById(Convert.ToInt64(drpHotelList.SelectedValue));
            imgHotelLogo.ImageUrl = (string.IsNullOrEmpty(objhotel.Logo) ? ConfigurationManager.AppSettings["FilePath"] + "/HotelImage/default_hotel_logo.png" : ConfigurationManager.AppSettings["FilePath"] + "/HotelImage/" + objhotel.Logo);
            Session["CurrentHotelID"] = objhotel.Id;
            Response.Redirect(Request.RawUrl, false);

            //changes by manas for checking whether this hotel is go online or not, if not then redirect it to dashboard page on drp selected index
            if (ObjHotelinfo.GetHotelGoOnline(Convert.ToInt32(drpHotelList.SelectedValue)).Count > 0)
            {

            }
            else
            {
                Response.Redirect("~/HotelUser/HotelDashboard.aspx", false);
            }
            //changes by manas
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion
    
}