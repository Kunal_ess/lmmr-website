﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using LMMR.Entities;
using LMMR.Data;
using LMMR.Business;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using log4net;
using log4net.Config;
using System.Configuration;
using System.Xml;

#endregion

public partial class HotelUser_ViewBookings : System.Web.UI.Page
{

    #region variables
    VList<ViewBookingHotels> vlist;
    VList<Viewbookingrequest> vlistreq;
    ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
    HotelManager objHotelManager = new HotelManager();
    string Typelink = "", AdminUser = "", Hotelid = "0";
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_ViewBookings));
    WizardLinkSettingManager ObjWizardManager = new WizardLinkSettingManager();
    Createbooking objBooking = null;
    BookingManager bm = new BookingManager();
    CurrencyManager cm = new CurrencyManager();
    HotelManager objHotel = new HotelManager();
    PackagePricingManager objPackagePricingManager = new PackagePricingManager();
    Hotel objRequestHotel = new Hotel();
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl + "/";
        }
    }
    int countMr = 0;
    CreateRequest objRequest = null;
    public List<PackageItemDetails> SelectedPackage
    {
        get;
        set;
    }
    public List<RequestExtra> SelectedExtra
    {
        get;
        set;
    }
    public TList<PackageItems> AllPackageItem
    {
        get;
        set;
    }
    public TList<PackageItems> AllFoodAndBravrages
    {
        get;
        set;
    }
    public TList<PackageItems> AllEquipments
    {
        get;
        set;
    }
    public TList<PackageByHotel> AllPackageByHotel
    {
        get;
        set;
    }

    public string CurrencySign
    {
        get;
        set;
    }
    public XmlDocument XMLLanguage
    {
        get { return (XmlDocument)ViewState["Language"]; }
        set { ViewState["Language"] = value; }
    }
    #endregion

    #region Pageload
    /// <summary>
    /// Method to manage the page load functionality.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Session check               
            if (Session["CurrentHotelID"] == null)
            {
                //Response.Redirect("~/login.aspx");
                Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                Session.Abandon();
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "login/english");
                }
            }
            Hotelid = Convert.ToString(Session["CurrentHotelID"].ToString());
            //Session check
            if (IsPostBack)
            {
                ApplyPaging();
            }
            else if (!IsPostBack)
            {
                Session["LinkID"] = "lnkRequests";
                btnSubmit.ValidationGroup = "popup";
                FetchListofHotel();
                //--- if querystring "type" = 5 then its for list of offers, else for new request
                if (Typelink == "5")
                {
                    lblheadertext.Text = "Offers for : " + "<b>" + Convert.ToString(objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Name) + "</b>";
                    divsearch.Visible = false;
                    bindgrid("5");
                    divbutton.Visible = false;
                }
                else
                {
                    lblheadertext.Text = "New Request for : " + "<b>" + Convert.ToString(objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Name) + "</b>";
                    divsearch.Visible = false;
                    bindgrid("1");
                    //divbutton.Visible = false;
                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region fetchlistofhotels
    /// <summary>
    /// Method to fetch list of hotel for the particular user
    /// </summary>

    protected void FetchListofHotel()
    {
        try
        {
            AdminUser = Convert.ToString(Session["CurrentUserID"]);

            Typelink = Convert.ToString(Request.QueryString["Type"]);

            //-------------- changed according Therirry comment in SRS v1.2
            Hotelid = Convert.ToString(Session["CurrentHotelID"].ToString());
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region BindGrid
    /// <summary>
    /// Method to bind the grid
    /// </summary>
    /// <param name="linktype"></param>
    /// linktype = 1 the its new request 5- old request  

    protected void bindgrid(string linktype)
    {
        try
        {
            Typelink = Convert.ToString(Request.QueryString["Type"]);
            FetchListofHotel();
            int totalcount = 0;
            string whereclaus = "";
            if (linktype == "1")
                whereclaus = "hotelid in (" + Hotelid + ") and requeststatus='" + linktype + "' and booktype in ( 1,2) "; // book type for request and booking
            else
                whereclaus = "hotelid in (" + Hotelid + ") and requeststatus in (5,7,2) and booktype  in ( 1,2) "; // book type for request and booking


            string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclaus, orderby);
            grdViewBooking.DataSource = vlistreq;
            grdViewBooking.DataBind();

            #region Disableaction header
            if (Typelink == "5")
            {
                grdViewBooking.Columns[9].Visible = false;
                grdViewBooking.Columns[10].Visible = true;
                grdViewBooking.Columns[11].Visible = true;
            }
            else
            {
                grdViewBooking.Columns[9].Visible = false;
                grdViewBooking.Columns[10].Visible = false;
                grdViewBooking.Columns[11].Visible = false;
            }

            #endregion
            if (grdViewBooking.Rows.Count <= 0)
            {
                divprintpdfllink.Visible = false;
            }

            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region PageIndexChanging
    /// <summary>
    /// PageIndex Changing of grid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdViewBooking_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            grdViewBooking.PageIndex = e.NewPageIndex;
            ViewState["CurrentPage"] = e.NewPageIndex;
            bindgrid(Convert.ToString(Request.QueryString["Type"]));
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region View Request Details
    /// <summary>
    /// Method to view details of Request would be seen
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lblRefNo_Click(object sender, EventArgs e)
    {
        //Changed The Code For Using Usercontrol:Pratik//
        try
        {

            LinkButton lnk = (LinkButton)sender;
            //-- viewstate object
            ViewState["BookingID"] = lnk.Text;

    Booking obj=        objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(ViewState["BookingID"].ToString()));
    if (obj.BookType == 1)
    {
        divwarningdetails.Visible = false;
        lblwarning.Text = "";
        requestDetails1.BindHotel(Convert.ToInt32(lnk.Text));
        divbookingdetails.Visible = true;
        requestDetails1.Visible = true;
        BookingDetails1.Visible = false;
    }
    else if (obj.BookType == 2)
    {
        divwarningdetails.Visible = true;
        lblwarning.Text = "This was initially a booking converted in request due to no (or low) bedroom avaibility";
        BookingDetails1.BindBooking(Convert.ToInt32(lnk.Text));
        divbookingdetails.Visible = true;
        BookingDetails1.Visible = true;
        requestDetails1.Visible = false;
    }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region fillbedroom details
    /// <summary>
    /// Method to get bedroom details
    /// </summary>
    protected void FillbedroomDetails()
    {
        try
        {
            // BookedBedRoom object
            TList<BookedBedRoom> objBookedBR = objViewBooking_Hotel.getbookedBedroom(Convert.ToInt64(Convert.ToString(ViewState["BookingID"])));

            foreach (BookedBedRoom br in objBookedBR)
            {

                //lblBedroomType.Text = Enum.GetName(typeof(BedRoomType), br.BedRoomIdSource.Types); // Enum.GetName();
                //lblChekOut.Text = String.Format("{0:dd/MM/yyyy}", br.CheckOut);
                //lblCheckIn.Text = String.Format("{0:dd/MM/yyyy}", br.CheckIn);
                //lblpersonName.Text = br.PersonName;
                //lblNote.Text = br.Note;

                //lblbedroomtotalprice.Text = String.Format("{0:#,###}", br.Total);

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region ItemDataBound
    /// <summary>
    /// ItemDataBound of repeater
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rpmain_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                Label lblbmrid = e.Item.FindControl("lblbmrid") as Label;
                Label lblPackageName = e.Item.FindControl("lblPackageName") as Label;
                Label lbltotalpackage = e.Item.FindControl("lbltotalpackage") as Label;
                Label lblextratotal = e.Item.FindControl("lblextratotal") as Label;
                GridView grdDetailspackage = e.Item.FindControl("grdDetailspackage") as GridView;
                GridView grdextra = e.Item.FindControl("grdextra") as GridView;
                GridView grdequipment = e.Item.FindControl("grdequipment") as GridView;

                //BuildPackageConfigure object
                TList<BuildPackageConfigure> obbuildpack = objViewBooking_Hotel.getPackageDetails(Convert.ToInt64(lblbmrid.Text));
                //BuildMeetingConfigure object
                TList<BuildMeetingConfigure> objBuildMeetingConfigure = objViewBooking_Hotel.getextra(Convert.ToInt64(lblbmrid.Text));

                grdextra.DataSource = objBuildMeetingConfigure.FindAll(a => a.PackageIdSource.IsExtra == true);
                grdextra.DataBind();
                grdequipment.DataSource = objBuildMeetingConfigure.FindAll(a => Convert.ToInt32(a.PackageIdSource.ItemType) == (int)ItemType.Equipment);
                grdequipment.DataBind();
                int cntextra = 0;
                foreach (BuildMeetingConfigure bmc in objBuildMeetingConfigure.FindAll(a => Convert.ToInt32(a.PackageIdSource.ItemType) == (int)ItemType.Equipment))
                {
                    cntextra += Convert.ToInt32(String.Format("{0:#,###}", bmc.TotalPrice));

                }
                lblextratotal.Text = Convert.ToString(cntextra);
                foreach (BuildPackageConfigure bpc in obbuildpack)
                {

                    lblPackageName.Text = bpc.PackageItemIdSource.PackageName;
                    lbltotalpackage.Text = String.Format("{0:#,###}", bpc.TotalPrice);

                    //BuildPackageConfigureDesc object
                    TList<BuildPackageConfigureDesc> obdesc = objViewBooking_Hotel.getpackagedetails(bpc.Id);
                    grdDetailspackage.DataSource = obdesc;
                    grdDetailspackage.DataBind();

                }


            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region Printdetails
    /// <summary>
    /// to print the details
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkPrint_Click(object sender, EventArgs e)
    {
        try
        {
            PrintHelper.PrintWebControl(Divdetails);
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region save as pdf
    /// <summary>
    /// method to save as pdf
    /// </summary>

    protected void lnkSavePDF_Click(object sender, EventArgs e)
    {
        try
        {
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=UserDetails.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            PrepareGridViewForExport(Divdetails);
            Divdetails.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region VerifyRenderingInServerForm
    /// <summary>
    /// method to VerifyRenderingInServerForm
    /// </summary>

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    #endregion

    #region PrepareGridViewForExport
    /// <summary>
    /// method to PrepareGridViewForExport
    /// </summary>
    private void PrepareGridViewForExport(Control gv)
    {

        try
        {
            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {

                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].GetType() == typeof(System.Web.UI.HtmlControls.HtmlAnchor))
                {

                    l.Text = (gv.Controls[i] as System.Web.UI.HtmlControls.HtmlAnchor).InnerText;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(DropDownList))
                {

                    l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(GridView))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareGridViewForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    // gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Repeater))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareGridViewForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Panel))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareGridViewForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Table))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareGridViewForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }


                else if (gv.Controls[i].GetType() == typeof(CheckBox))
                {

                    l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(HyperLink))
                {

                    l.Text = (gv.Controls[i] as HyperLink).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                //else if (gv.Controls[i].GetType() == typeof(Label))
                //{

                //    l.Text = (gv.Controls[i] as Label).Text;

                //    gv.Controls.Remove(gv.Controls[i]);

                //    gv.Controls.AddAt(i, l);

                //}
                else if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.Image))
                {

                    // l.Text = (gv.Controls[i] as Image).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareGridViewForExport(gv.Controls[i]);

                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region Bookingbind






    //public void BindHotel()
    //{
    //    try
    //    {
    //        var hotelID = Convert.ToInt64(Hotelid);
    //        rptHotel.DataSource = objRequest.HotelList.Where(a => a.MeetingroomList.Count > 0 && a.HotelID == hotelID);
    //        rptHotel.DataBind();

    //        lblBookedDays.Text = objRequest.Duration == 1 ? objRequest.Duration + " Day" : objRequest.Duration + " Days";
    //        //lblBookedDays.Text = objRequest.ArivalDate.ToString("dd/MMM/yyyy");
    //        //lblBookedDays1.Text = objRequest.DepartureDate.ToString("dd/MMM/yyyy");
    //        Int64 intHotelID = objRequest.HotelList[0].HotelID;
    //        if (objRequest.PackageID != 0)
    //        {
    //            pnlPackage.Visible = true;
    //            PackageMaster packagedetails = objPackagePricingManager.GetAllPackageName().Where(a => a.Id == objRequest.PackageID).FirstOrDefault();
    //            if (packagedetails != null)
    //            {

    //                #region highlight red

    //                List<PackageByHotel> tlistpACK = DataRepository.PackageByHotelProvider.GetByHotelId(Convert.ToInt64(Hotelid)).Where(a => a.PackageId == objRequest.PackageID && a.IsOnline == true).ToList();
    //                if (tlistpACK.Count == 0)
    //                    lblPackageName.ForeColor = System.Drawing.Color.Red;
    //                #endregion


    //                lblPackageName.Text = packagedetails.PackageName;
    //                if (packagedetails.PackageName == "Standard")
    //                {
    //                    lblpackageDescription.Text = "Package includes main meeting room rental, note pads, pencils, flipchart, mineral water, morning coffee, Sandwich buffet with salads (incl. non-alcoholic beverages) and afternoon coffee break (tea, coffee, soft drinks and pastries).For half day packages, either morning or afternoon break is included. The Standard package is available as of 10 meeting delegates.";

    //                }
    //                else if (packagedetails.PackageName == "Favourite")
    //                {
    //                    lblpackageDescription.Text = "Package includes main meeting room rental, note pads, pencils, flipchart, mineral water, morning coffee, 2-courses menu (incl. non-alcoholic beverages) and afternoon coffee break (tea, coffee, soft drinks and pastries).For half day packages, either morning or afternoon break is included. The Favourite package is available as of 10 meeting delegates.";
    //                }
    //                else if (packagedetails.PackageName == "Elegant")
    //                {
    //                    lblpackageDescription.Text = "Package includes main meeting room rental, note pads, pencils, flipchart, mineral water, morning coffee, hot and cold buffet (incl. non-alcoholic beverages) and afternoon coffee break (tea, coffee, soft drinks and pastries).For half day packages, either morning or afternoon break is included. The Elegant package is available as of 20 meeting delegates.";
    //                }
    //                else
    //                {
    //                    lblpackageDescription.Text = "";
    //                }
    //            }
    //            AllPackageItem = objPackagePricingManager.GetAllPackageItems();
    //            rptPackageItem.DataSource = objPackagePricingManager.GetPackageItemsByPackageID(objRequest.PackageID);
    //            rptPackageItem.DataBind();
    //            if (objRequest.ExtraList.Count > 0)
    //            {
    //                //Package Selection.

    //                pnlExtra.Visible = true;
    //                rptExtras.DataSource = objRequest.ExtraList;
    //                rptExtras.DataBind();//check AllPackageItem.Where(a => a.IsExtra == true)
    //            }
    //            else
    //            {
    //                pnlExtra.Visible = false;
    //            }
    //            pnlFoodAndBravrages.Visible = false;
    //        }
    //        else
    //        {
    //            pnlPackage.Visible = false;

    //            if (objRequest.BuildYourMeetingroomList.Count > 0)
    //            {
    //                AllFoodAndBravrages = objPackagePricingManager.GetAllFoodBeveragesItems(intHotelID);
    //                pnlFoodAndBravrages.Visible = true;
    //                rptFoodandBravragesDay.DataSource = objRequest.DaysList;
    //                rptFoodandBravragesDay.DataBind();
    //            }
    //            else
    //            {
    //                pnlFoodAndBravrages.Visible = false;
    //            }
    //        }


    //        if (objRequest.EquipmentList.Count > 0)
    //        {
    //            AllEquipments = objPackagePricingManager.GetAllEquipmentItems(intHotelID);
    //            pnlEquipment.Visible = true;
    //            rptEquipmentDay.DataSource = objRequest.DaysList;
    //            rptEquipmentDay.DataBind();
    //        }
    //        else
    //        {
    //            pnlEquipment.Visible = false;
    //        }
    //        if (objRequest.IsAccomodation)
    //        {
    //            pnlAccomodation.Visible = true;
    //            //   lblaccomodationQun.Text = Convert.ToString(objRequest.RequestAccomodationList.Quantity);
    //        }
    //        else
    //        {
    //            pnlAccomodation.Visible = true;
    //        }
    //        lblSpecialRequest.Text = objRequest.SpecialRequest;
    //    }
    //    catch (Exception ex)
    //    {
    //        logger.Error(ex);
    //    }
    //}

    #endregion

    #region Language
    //This is used for language conversion for static contants.
    public string GetKeyResult(string key)
    {
        //if (XMLLanguage == null)
        //{
        //    XMLLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
        //}
        //XmlNode nodes = XMLLanguage.SelectSingleNode("items/item[@key='" + Key + "']");
        //return nodes.InnerText;//
        return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    }
    #endregion



    #region Process
    /// <summary>
    /// method to move to process
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Lnkmovetoprocessed_Click(object sender, EventArgs e)
    {
        try
        {
            //  changestatus();
            Booking objbooking = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(ViewState["BookingID"].ToString()));
            objbooking.RevenueReason = ddlreason.SelectedItem.Text; //decline reason is save din revenue reason
            objbooking.RequestStatus = 5;
            if (objViewBooking_Hotel.updatecheckComm(objbooking, Convert.ToInt64(Session["CurrentUserID"])))
            {
                ddlreason.SelectedIndex = 0;
            }
            // txtrealvalue.Text = "";


            divbookingdetails.Visible = false;
            divbutton.Visible = false;
            Response.Redirect("ViewRequest.aspx?type=" + Convert.ToString(Request.QueryString["type"]));
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        finally
        {

            divbookingdetails.Visible = false;
            divbutton.Visible = false;
            Response.Redirect("ViewRequest.aspx?type=" + Convert.ToString(Request.QueryString["type"]));
        }
    }
    #endregion


    #region RowDataBound
    /// <summary>
    /// RowDataBound of grid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdViewBooking_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            int intarivalday = 0;
            foreach (GridViewRow gr in grdViewBooking.Rows)
            {
                Label lblArrivalDt = (Label)gr.FindControl("lblArrivalDt");
                LinkButton lblRefNo = (LinkButton)gr.FindControl("lblRefNo");
                TList<BookedMeetingRoom> bookedMR = objViewBooking_Hotel.getbookedmeetingroom(Convert.ToInt64(lblRefNo.Text));
                Label lblDepartureDt = (Label)gr.FindControl("lblDepartureDt");
                Label lblExpiryDt = (Label)gr.FindControl("lblExpiryDt");
                LinkButton btnchkcommision = (LinkButton)gr.FindControl("btnchkcommision");
                Label lblBookingDt = (Label)gr.FindControl("lblBookingDt");
                Label lblstatus = (Label)gr.FindControl("lblstatus");
                DateTime startTime = DateTime.Now;
                string depdt = lblDepartureDt.Text;
                LinkButton btnchkcommision1 = (LinkButton)gr.FindControl("btnchkcommision1");

                Booking obj=        objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(lblRefNo.Text));
                if (obj.BookType == 2)
                {
                    gr.BackColor = System.Drawing.Color.PaleGreen;
                }
                

                #region Bindrequeststaus

                lblstatus.Text = Enum.GetName(typeof(BookingRequestStatus), Convert.ToInt32(lblstatus.ToolTip));

                #endregion


                #region ExpiryDate

                DateTime exptime;
                DateTime dtmeetingdt = new DateTime(Convert.ToInt32("20" + lblBookingDt.Text.Split('/')[2]), Convert.ToInt32(lblBookingDt.Text.Split('/')[1]), Convert.ToInt32(lblBookingDt.Text.Split('/')[0]));
                //string day = Convert.ToString(dtmeetingdt.DayOfWeek); // as asked by martin to remove in MOM of 2609
                //if (day == "Friday")
                //{
                //    lblExpiryDt.Text = dtmeetingdt.AddDays(3).ToString("dd/MM/yyyy");
                //    exptime = dtmeetingdt.AddDays(3);
                //}
                //else
                //{
                //    lblExpiryDt.Text = dtmeetingdt.AddDays(2).ToString("dd/MM/yyyy");
                //    exptime = dtmeetingdt.AddDays(2);
                //}
                exptime=  dtmeetingdt.AddDays(5); // as asked by client
                lblExpiryDt.Text = dtmeetingdt.AddDays(5).ToString("dd/MM/yyyy");



                #endregion

                

                DateTime endTime = new DateTime(Convert.ToInt32(depdt.Split('/')[2]), Convert.ToInt32(depdt.Split('/')[1]), Convert.ToInt32(depdt.Split('/')[0]));
                //DateTime endTime = Convert.ToDateTime(depdt);
                TimeSpan span = endTime.Subtract(startTime);
                int spn = span.Days;


                //---- set display commission button
                #region show commsion button
                if (lblstatus.Text.ToString() == BookingRequestStatus.Definite.ToString())
                {
                    if (btnchkcommision.ToolTip.ToLower() == "false" || string.IsNullOrEmpty(btnchkcommision.ToolTip))
                    {
                        if (spn < 0)
                        {

                            btnchkcommision.Text = "Check Commission";
                            btnchkcommision.Visible = true;
                            ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
                        }
                        else
                        {
                            btnchkcommision.Visible = false;
                            ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
                        }

                    }
                    else if (btnchkcommision.ToolTip.ToLower() == "true")
                    {
                        btnchkcommision.Visible = false;
                        ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = true;
                    }
                    else
                    {
                        btnchkcommision.Visible = false;
                        ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
                    }

                }
                #endregion


                #region show chnage Status buttom
                if (spn < 0)
                {

                    if (lblstatus.Text.ToString() == BookingRequestStatus.Cancel.ToString())
                    {
                        btnchkcommision1.Visible = false;
                        btnchkcommision.Visible = false;
                        ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
                    }
                    if (lblstatus.Text.ToString() == BookingRequestStatus.New.ToString())
                    {
                        btnchkcommision1.Visible = false;
                        btnchkcommision.Visible = false;
                        ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
                    }
                    if (lblstatus.Text.ToString() == BookingRequestStatus.Definite.ToString())
                    {
                        btnchkcommision1.Visible = false;

                    }
                    if (lblstatus.Text.ToString() == BookingRequestStatus.Tentative.ToString())
                    {
                        btnchkcommision1.Visible = true;
                        btnchkcommision.Visible = false;
                        ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
                    }
                }
                else
                {
                    if (lblstatus.Text.ToString() == BookingRequestStatus.Cancel.ToString())
                    {
                        btnchkcommision1.Visible = true;
                        btnchkcommision.Visible = false;
                        ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
                    }
                    if (lblstatus.Text.ToString() == BookingRequestStatus.New.ToString())
                    {
                        btnchkcommision1.Visible = true;
                        btnchkcommision.Visible = false;
                        ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
                    }
                    if (lblstatus.Text.ToString() == BookingRequestStatus.Definite.ToString())
                    {
                        btnchkcommision1.Visible = true;

                    }
                    if (lblstatus.Text.ToString() == BookingRequestStatus.Tentative.ToString())
                    {
                        btnchkcommision1.Visible = true;
                        btnchkcommision.Visible = false;
                        ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
                    }
                }
                #endregion

               


                //#region Freeze row in gray color

                //if (Convert.ToInt32(startTime.Day) >= 8)
                //{

                //    DateTime bookingdt = new DateTime(Convert.ToInt32("20" + lblBookingDt.Text.Split('/')[2]), Convert.ToInt32(lblBookingDt.Text.Split('/')[1]), Convert.ToInt32(lblBookingDt.Text.Split('/')[0]));
                //    if (bookingdt.Month < startTime.Month)
                //    {
                //        gr.BackColor = System.Drawing.Color.LightGray;
                //    }
                //    else
                //    {
                //        btnchkcommision.Visible = true;
                //    }
                //}

               // #endregion



            }

        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region Confirm Commsion
    /// <summary>
    /// when check commsion was confirmed
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>    

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            Booking objbooking = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(ViewState["ChkcommBookingID"].ToString()));
            if (Convert.ToString(ViewState["popup"]) == "1")
            {
                if (chkmeetingNotheld.Checked)
                {
                    objbooking.RequestStatus = (int)BookingRequestStatus.Cancel;
                    objbooking.RevenueReason = "Meeting was not held";
                }
                else
                {
                    #region check file upload
                    string strPlanName = string.Empty;
                    if (ulPlan.HasFile)
                    {
                        string fileExtension = Path.GetExtension(ulPlan.PostedFile.FileName.ToString());

                        if (fileExtension == ".xls" || fileExtension == ".xlsx" || fileExtension == ".doc" || fileExtension == ".docx" || fileExtension.ToLower() == ".pdf")
                        {
                            strPlanName = Path.GetFileName(ulPlan.FileName);
                        }
                        else
                        {
                            divmessage.InnerHtml = "Please select file in (word,excel,pdf) formats only";
                            divmessage.Attributes.Add("class", "error");
                            divmessage.Style.Add("display", "block");
                            modalcheckcomm.Show();
                            return;
                        }

                        if (ulPlan.PostedFile.ContentLength > 1048576)
                        {
                            divmessage.InnerHtml = "Filesize of Supporting Document is too large. Maximum file size permitted is 1 MB.";
                            divmessage.Attributes.Add("class", "error");
                            divmessage.Style.Add("display", "block");
                            modalcheckcomm.Show();
                            return;
                        }
                        if (string.IsNullOrEmpty(txtrealvalue.Text))
                        {
                            divmessage.InnerHtml = "Kindly enter the Netto value";
                            divmessage.Attributes.Add("class", "error");
                            divmessage.Style.Add("display", "block");
                            modalcheckcomm.Show();
                            return;
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(txtrealvalue.Text))
                            divmessage.InnerHtml = "Kindly upload the Supporting Document and the Netto value";
                        else
                            divmessage.InnerHtml = "Kindly upload the Supporting Document.";
                        divmessage.Attributes.Add("class", "error");
                        divmessage.Style.Add("display", "block");
                        modalcheckcomm.Show();
                        return;
                    }

                    #endregion

                    ulPlan.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "SupportDocNetto/") + strPlanName);
                    objbooking.RequestStatus = (int)BookingRequestStatus.Definite;
                    if (string.IsNullOrEmpty(txtrealvalue.Text))
                        txtrealvalue.Text = "0";
                    // decimal netvalue = Convert.ToDecimal(txtrealvalue.Text);
                    // decimal revenue = (Convert.ToDecimal(objbooking.FinalTotalPrice) / (netvalue - 1)) * 100;
                    // objbooking.RevenueAmount = netvalue;
                    objbooking.FinalTotalPrice = Convert.ToDecimal(txtrealvalue.Text); // updated
                    objbooking.ConfirmRevenueAmount = Convert.ToDecimal(txtrealvalue.Text); // updated
                    objbooking.ComissionSubmitDate = System.DateTime.Now;
                    objbooking.IsComissionDone = true;
                    objbooking.RevenueReason = ddlreason.SelectedItem.Text;
                    objbooking.SupportingDocNetto = strPlanName;
                }


            }
            else
            {
                objbooking.RequestStatus = Convert.ToInt32(ddlstatus.SelectedValue);

            }
            if (objViewBooking_Hotel.updatecheckComm(objbooking, Convert.ToInt64(Session["CurrentUserID"])))
            {
                FetchListofHotel();
                bindgrid(Typelink);
                modalcheckcomm.Hide();
                ModalPopupCheck.Hide();
            }

        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            divmessage.InnerHtml = ex.Message;
            divmessage.Attributes.Add("class", "error");
            divmessage.Style.Add("display", "block");
            if (Convert.ToString(ViewState["popup"]) == "1")
            {
                modalcheckcomm.Show();
            }
            else
            {
                ModalPopupCheck.Show();
            }
        }
       
    }
    #endregion
    #region Check Commission
    /// <summary>
    /// when check commsion was checked
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  
    protected void btnchkcommision_Click(object sender, EventArgs e)
    {
        try
        {

            LinkButton btnchkcomm = (LinkButton)sender;
            ViewState["ChkcommBookingID"] = btnchkcomm.CommandArgument;

            if (btnchkcomm.Text.ToLower() == "change status")
            {
                divmessage.Style.Add("display", "none");
                ModalPopupCheck.Show();
                modalcheckcomm.Hide();
                ViewState["popup"] = "0";
            }
            else
            {
                //  txtrealvalue.ReadOnly = true;
              //  string script1 = "<script type='text/javascript'>alert('hello');</script>";


                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () {alert('You are about to validate your commission.Please have the invoice ready in order to accept / adjust the commissionable revenue.');});", true);
               
               
                divmessage.Style.Add("display", "none");
                chkmeetingNotheld.Checked = false;
                modalcheckcomm.Show();
                ModalPopupCheck.Hide();
                ViewState["popup"] = "1";
            }


        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion





    #region Decline
    /// <summary>
    /// decline a request
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  
    protected void lnkdecline_Click(object sender, EventArgs e)
    {
        try
        {
            Booking objbooking = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(ViewState["BookingID"].ToString()));


            objbooking.RevenueReason = "";

            objbooking.RequestStatus = 2;
            if (objViewBooking_Hotel.updatecheckComm(objbooking, Convert.ToInt64(Session["CurrentUserID"])))
            {

                FetchListofHotel();
                bindgrid(Typelink);
                Response.Redirect("ViewRequest.aspx?type=" + Typelink);
            }
            // txtrealvalue.Text = "";
            ddlreason.SelectedIndex = 0;
            bindgrid(Typelink);
            divbookingdetails.Visible = false;
            divbutton.Visible = false;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion


    #region Apply Paging
    /// <summary>
    /// Method to apply Paging in grid
    /// </summary>
    private void ApplyPaging()
    {
        try
        {
            if (grdViewBooking.Rows.Count <= 0)
            {
                //divprint.Visible = false;
                return;
            }

            GridViewRow row = grdViewBooking.TopPagerRow;
            if (row != null)
            {
                PlaceHolder ph;
                LinkButton lnkPaging;
                LinkButton lnkPrevPage;
                LinkButton lnkNextPage;
                lnkPrevPage = new LinkButton();
                lnkPrevPage.CssClass = "pre";
                lnkPrevPage.Width = Unit.Pixel(73);
                lnkPrevPage.CommandName = "Page";
                lnkPrevPage.CommandArgument = "prev";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPrevPage);
                if (grdViewBooking.PageIndex == 0)
                {
                    lnkPrevPage.Enabled = false;

                }
                for (int i = 1; i <= grdViewBooking.PageCount; i++)
                {
                    lnkPaging = new LinkButton();
                    if (ViewState["CurrentPage"] != null)
                    {
                        if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    else
                    {
                        if (i == 1)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    lnkPaging.Width = Unit.Pixel(16);
                    lnkPaging.Text = i.ToString();
                    lnkPaging.CommandName = "Page";
                    lnkPaging.CommandArgument = i.ToString();
                    if (i == grdViewBooking.PageIndex + 1)
                        ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkPaging);
                }
                lnkNextPage = new LinkButton();
                lnkNextPage.CssClass = "nex";
                lnkNextPage.Width = Unit.Pixel(42);
                lnkNextPage.CommandName = "Page";
                lnkNextPage.CommandArgument = "next";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkNextPage);
                ph = (PlaceHolder)row.FindControl("ph");
                if (grdViewBooking.PageIndex == grdViewBooking.PageCount - 1)
                {
                    lnkNextPage.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion




    #region Cancel Button Click
    /// <summary>
    /// Modal popup cancel event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            divmessage.InnerHtml = "";
            divmessage.Style.Add("display", "none");
            modalcheckcomm.Hide();
            ModalPopupCheck.Hide();
            ddlstatus.SelectedIndex = 0;
            txtrealvalue.Text = "";
            chkmeetingNotheld.Checked = false;
            ddlreason.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    #endregion

    #region SelectedIndexChanged
    /// <summary>
    /// method to SelectedIndexChanged
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  
    protected void ddlstatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (grdViewBooking.Rows.Count > 0)
            {

                DropDownList ddl = (DropDownList)sender;

                foreach (GridViewRow row in grdViewBooking.Rows)
                {

                    if (((Label)row.FindControl("lblstatus")).ToolTip.IndexOf(ddl.SelectedValue) >= 0)
                        row.Visible = true;
                    else if ("All" == ddl.SelectedItem.Text)
                        row.Visible = true;
                    else
                        row.Visible = false;



                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }


    #endregion


    #region Search
    /// <summary>
    /// Search
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  
    protected void lbtSearch_Click(object sender, EventArgs e)
    {
        try
        {
            FetchListofHotel();
            int totalcount = 0;
            string whereclaus = "";
            if (Typelink == "1")
                whereclaus = "hotelid in (" + Hotelid + ") and requeststatus='" + Typelink + "' and booktype = 1 "; // book type for request and booking
            else
                whereclaus = "hotelid in (" + Hotelid + ") and requeststatus not in (1) and booktype = 1 "; // book type for request and booking

            if (!string.IsNullOrEmpty(txtFromdate.Text) && !string.IsNullOrEmpty(txtTodate.Text))
                whereclaus += " and arrivaldate between convert(DATETIME, '" + txtFromdate.Text + "',103) and convert(DATETIME, '" + txtTodate.Text + "',103) ";


            string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclaus, orderby);
            grdViewBooking.DataSource = vlistreq;
            grdViewBooking.DataBind();

            #region Disableaction header
            if (Typelink == "5")
            {
                grdViewBooking.Columns[10].Visible = true;
                grdViewBooking.Columns[11].Visible = true;
            }
            else
            {
                grdViewBooking.Columns[10].Visible = false;
                grdViewBooking.Columns[11].Visible = false;
            }

            #endregion
            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion



    #region sAVE AS PDF
    /// <summary>
    /// method to save as pdf
    /// </summary>
    protected void uiLinkButtonSaveAsPdfGrid_Click(object sender, EventArgs e)
    {
        try
        {
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=UserDetails.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            grdViewBooking.AllowPaging = false;
            bindgrid(Convert.ToString(Request.QueryString["type"]));
            grdViewBooking.Columns[11].Visible = false;
            PrepareGridViewForExport(grdViewBooking);
            grdViewBooking.GridLines = GridLines.Both;
            grdViewBooking.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }


    }
    #endregion

    #region RowCreated
    /// <summary>
    /// method to RowCreated
    /// </summary>
    protected void grdViewBooking_RowCreated(object sender, GridViewRowEventArgs e)
    {
        try
        {
            string rowID = String.Empty;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#FF9'");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;");
                ViewState["rowID"] = e.Row.RowIndex;

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion
}