﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using System.Text;
using LMMR.Entities;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using LMMR.Data;
using System.Configuration;
using log4net;
using log4net.Config;
#endregion
public partial class HotelUser_MeetingRoomDesc : System.Web.UI.Page
{

    #region Variables and Properties
    UserManager objUserManager = new UserManager();
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_MeetingRoomDesc));
    WizardLinkSettingManager ObjWizardLinkSetting = new WizardLinkSettingManager();
    PictureVideoManagementManager ObjPictureManager = new PictureVideoManagementManager();
    DashboardManager ObjDashBoard = new DashboardManager();
    MeetingRoomDescManager ObjMeetingRoomDesc = new MeetingRoomDescManager();
    MeetingRoomConfigManager ObjMeetingRoomConfig = new MeetingRoomConfigManager();
    StringBuilder ObjSBLanguage = new StringBuilder();
    HotelInfo ObjHotelinfo = new HotelInfo();
    HotelManager objHotelManager = new HotelManager();
    public static TList<MeetingRoom> Objlst = new TList<MeetingRoom>();
    public static TList<MeetingRoomDesc> ObjAllLanguageDescription = new TList<MeetingRoomDesc>();
    int intHotelID = 0;
    int count = 0;
    long intUserID = 0;
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentHotelID"] != null)
        {
            if (Session["CurrentUser"] != null)
            {
                Users objUser = (Users)Session["CurrentUser"];
                intUserID = objUser.UserId;
                intHotelID = Convert.ToInt32(Session["CurrentHotelID"].ToString());
                if (!IsPostBack)
                {
                    #region Leftmenu
                    HyperLink lnk = (HyperLink)this.Master.FindControl("lnkConferenceInfo");
                    lnk.CssClass = "";
                    lnk = (HyperLink)this.Master.FindControl("lnkContactDetails");
                    lnk.CssClass = "";
                    lnk = (HyperLink)this.Master.FindControl("lnkFacilities");
                    lnk.CssClass = "";
                    lnk = (HyperLink)this.Master.FindControl("lnkCancelationPolicy");
                    lnk.CssClass = "";
                    lnk = (HyperLink)this.Master.FindControl("lnkDescriptionMeetingRoom");
                    lnk.CssClass = "selected";
                    #endregion
                    divmessage.Style.Add("display", "none");
                    divmessage.Attributes.Add("class", "error");
                    Session["LinkID"] = "lnkPropertyLevel";
                    CreateDynamicLanguageTab(intHotelID);
                    BindMeetingRoomListView();
                    IsOneMeetingRoomDone();
                    lblHotel.Text = objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Name;
                }
            }

        }
        else
        {
            Response.Redirect("~/login.aspx", false);
            return;
        }
    }
    #endregion

    #region Functions
    /// <summary>
    /// For check go online status for current hotel
    /// </summary>
    public void Getgoonline()
    {
        if (ObjHotelinfo.GetHotelGoOnline(intHotelID).Count > 0)
        {
            divNext.Visible = false;
            ViewState["Count"] = ObjHotelinfo.GetHotelGoOnline(intHotelID).Count;
            //Disable description textbox if hotel is goonline
            if (Convert.ToString(Session["Operator"]) == "1")
            {

            }
            else
            {
                txtDesc.ReadOnly = true;
            }
            //Disable description textbox if hotel is goonline
        }
    }

    /// <summary>
    /// This function used for check at least one meetion room is done. 
    /// </summary>
    void IsOneMeetingRoomDone()
    {
        Hotel hotel = ObjHotelinfo.GetHotelByHotelID(intHotelID);
        TList<CountryLanguage> lstlangCountry = ObjHotelinfo.GetAllRequiredLanguageByCountryId(Convert.ToInt32(hotel.CountryId));
        TList<MeetingRoom> ObjMeetingRoomCount = ObjMeetingRoomDesc.GetAllMeetingRoom(intHotelID);
        //Start this condition is added, if meeting room is deleted then next button will show
        if (Convert.ToString(ViewState["Count"]) != "1" && ObjWizardLinkSetting.CheckLeftmenu(intHotelID, Convert.ToInt32(PropertyLevelSectionLeftMenu.lnkDescriptionMeetingRoom)).Count == 1)
        {
            divNext.Visible = true;
        }
        //End this condition is added, if meeting room is deleted then next button will show
        for (int i = 0; i <= ObjMeetingRoomCount.Count - 1; i++)
        {
            TList<MeetingRoomDesc> lstCountdesc = DataRepository.MeetingRoomDescProvider.GetByMeetingRoomId(ObjMeetingRoomCount[i].Id);
            if (lstlangCountry.Count == lstCountdesc.Count)
            {
                divNext.Visible = true;
                if (Convert.ToString(ViewState["Count"]) != "1" && ObjWizardLinkSetting.CheckLeftmenu(intHotelID, Convert.ToInt32(PropertyLevelSectionLeftMenu.lnkDescriptionMeetingRoom)).Count == 0)
                {
                    divmessage.InnerHtml += "&nbsp; Please click on next button to move ahead.";
                }
                break;
            }
        }
        Getgoonline();

    }

    /// <summary>
    /// create language tab by countryID.
    /// </summary>
    /// <param name="hotelID"></param>
    public void CreateDynamicLanguageTab(int hotelID)
    {
        Hotel ObjHotel = DataRepository.HotelProvider.GetById(hotelID);
        TList<CountryLanguage> lstlangbyCountry = ObjHotelinfo.GetAllRequiredLanguageByCountryId(Convert.ToInt32(ObjHotel.CountryId));
        string whereclause = LanguageColumn.Id + " in (";
        foreach (CountryLanguage cl in lstlangbyCountry)
        {
            whereclause += cl.LanguageId + ",";
        }
        if (whereclause.Length > 0)
        {
            whereclause = whereclause.Substring(0, whereclause.Length - 1);
        }
        whereclause += ")";
        int count = 0;
        TList<Language> lstLang = DataRepository.LanguageProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out count);

        ObjAllLanguageDescription = ObjMeetingRoomDesc.GetMeetingRoomDescByMeetingRoomID(Convert.ToInt32(hndMeetingRoomID.Value));
        if (lstLang.Count > 0)
        {
            if (hndMeetingRoomID.Value != "0")
            {
                ObjSBLanguage.Append(@"<ul class=""TabbedPanelsTabGroup"">");
                for (int i = 0; i <= lstLang.Count - 1; i++)
                {
                    int intlanguaegeID = Convert.ToInt32(lstLang[i].Id);
                    var varLanguage = ObjMeetingRoomDesc.GetMeetingRoomDescByMeetingRoomID(Convert.ToInt32(hndMeetingRoomID.Value)).Find(a => a.LanguageId == intlanguaegeID);

                    if (varLanguage != null)
                    {

                        ObjSBLanguage.Append(@"<li class='TabbedPanelsTab' onclick='javascript:GetLanguageID(" + lstLang[i].Id + ");'><a href='#' >" + lstLang[i].Name + "</a></li>");
                    }
                    else
                    {
                        ObjSBLanguage.Append(@"<li class='TabbedPanelsTab1' onclick='javascript:GetLanguageID(" + lstLang[i].Id + ");'><a href='#'>" + lstLang[i].Name + "</a></li>");

                    }

                }
                ObjSBLanguage.Append(@"</ul>");
                ObjSBLanguage.Append(@"<div class=""TabbedPanelsContentGroup"">");
                for (int i = 0; i <= lstLang.Count - 1; i++)
                {
                    ObjSBLanguage.Append(@"<div class=""TabbedPanelsContent""></div>");
                }
                ObjSBLanguage.Append(@"</div>");
            }
            else
            {
                ObjSBLanguage.Append(@"<ul class=""TabbedPanelsTabGroup"">");
                for (int i = 0; i <= lstLang.Count - 1; i++)
                {
                    ObjSBLanguage.Append(@"<li class='TabbedPanelsTab1' onclick='javascript:GetLanguageID(" + lstLang[i].Id + ");'><a href='#' >" + lstLang[i].Name + "</a></li>");

                }
                ObjSBLanguage.Append(@"</ul>");
                ObjSBLanguage.Append(@"<div class=""TabbedPanelsContentGroup"">");
                for (int i = 0; i <= lstLang.Count - 1; i++)
                {
                    ObjSBLanguage.Append(@"<div class=""TabbedPanelsContent""></div>");
                }
                ObjSBLanguage.Append(@"</div>");
            }
            litLangugeTab.Text = "";
            litLangugeTab.Text = ObjSBLanguage.ToString();
        }
        divmessage.Style.Add("display", "none");
        divmessage.Attributes.Add("class", "error");
    }

    /// <summary>
    /// Bind all meetingroom with listview by hotelId.
    /// </summary>
    void BindMeetingRoomListView()
    {
        if (ObjMeetingRoomDesc.GetAllMeetingRoom(intHotelID).Count > 0)
        {
            lstViewMeetingRoomDesc.Visible = true;
            addmeeting.Visible = true;
            divMeetingRoomForm.Visible = false;
            trLagend.Visible = true;
            Objlst = ObjMeetingRoomDesc.GetAllMeetingRoom(intHotelID);
            lstViewMeetingRoomDesc.DataSource = Objlst;
            lstViewMeetingRoomDesc.DataBind();
        }
        else
        {
            lstViewMeetingRoomDesc.Visible = false;
            addmeeting.Visible = false;
            divMeetingRoomForm.Visible = true;
            btnUpdate.Visible = false;
            btnSave.Visible = true;
            trLagend.Visible = false;
            ClearForm();
            ScriptManager.RegisterStartupScript(this, typeof(Page), "colorChange", "CallMyMethod();", true);
        }
    }

    /// <summary>
    /// Get meetingroom desc By LanguageID.
    /// </summary>
    /// <param name="languageID"></param>
    /// <returns></returns>
    [WebMethod]
    public static string GetDescByLanguageID(int languageID, int meetingroomID)
    {
        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        MeetingRoomDescManager ObjMeetingRoomDesc = new MeetingRoomDescManager();
        TList<MeetingRoomDesc> lsthdesc = ObjMeetingRoomDesc.GetMeetingRoomDescByMeetingRoomID(meetingroomID);
        var data = lsthdesc.Find(a => a.LanguageId == languageID);
        if (data != null)
        {
            var varDesc = new { data.Description, data.Id };
            return serializer.Serialize(varDesc);
        }
        else
        {
            return null;
        }


    }

    /// <summary>
    /// Get Language Name By LanguageID.
    /// This Methode call by ajax function from the aspx page.
    /// </summary>
    /// <param name="languageID"></param>
    /// <returns>string</returns>
    [WebMethod]
    public static string GetNameByLanguageID(int languageID)
    {
        BedRoomDescManager objBedRoomDescManager = new BedRoomDescManager();
        string strName = objBedRoomDescManager.GetLanguageNameById(languageID);
        return strName;
    }


    /// <summary>
    /// Function to get description by meetingroomId and LanguageID.
    /// </summary>
    /// <param name="meetingRoomID"></param>
    /// <param name="languageID"></param>
    /// <returns></returns>
    public string GetMeetingRoomDescription(int meetingRoomID, int languageID)
    {
        TList<MeetingRoomDesc> ObjMeetingRoomDescription = ObjMeetingRoomDesc.GetMeetingRoomDescByMeetingRoomID(meetingRoomID);

        var getDesc = ObjMeetingRoomDescription.Find(a => a.LanguageId == languageID);
        if (getDesc != null)
        {

            return getDesc.Description;
        }
        else
        {
            return "";
        }
    }

    /// <summary>
    /// // function to reset form value.
    /// </summary>
    void ClearForm()
    {
        txtName.Text = "";
        ddlDayLight.SelectedIndex = 0;
        txtDesc.Text = "";
        txtheight.Text = "";
        txtSurf.Text = "";
    }

    /// <summary>
    /// Function to get meetingroom detail by meetingroomId.
    /// </summary>
    /// <param name="meetingRoomID"></param>
    void FillMeetingRoom(int meetingRoomID)
    {
        ClearForm();
        MeetingRoom ObjMeetingRoom = ObjMeetingRoomDesc.GetMeetingRoomByID(meetingRoomID);
        hdnMeetingRoomName.Value = ObjMeetingRoom.Name;
        txtName.Text = ObjMeetingRoom.Name;
        txtSurf.Text = ObjMeetingRoom.Surface.ToString();
        txtheight.Text = ObjMeetingRoom.Height.ToString();
        ddlDayLight.SelectedValue = Convert.ToInt32(ObjMeetingRoom.DayLight).ToString();
        int intLanguageID = Convert.ToInt32(ObjMeetingRoomDesc.GetAllRequiredLanguage().Find(a => a.Name == "English").Id);
        txtDesc.Text = GetMeetingRoomDescription(meetingRoomID, intLanguageID);
        hdnImageName.Value = ObjMeetingRoom.Picture;
        hdnPlanName.Value = ObjMeetingRoom.MrPlan;
    }

    #endregion

    #region Events

    /// <summary>
    /// This button event used for save data into database.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            hdnSelectedRowID.Value = "0";
            int intLanguageID = 0;
            //get language ID
            if (hdnIDS.Value == "")
            {
                intLanguageID = Convert.ToInt32(ObjMeetingRoomDesc.GetAllRequiredLanguage().Find(a => a.Name == "English").Id);
            }
            else
            {
                intLanguageID = Convert.ToInt32(hdnIDS.Value);
            }

            if (intLanguageID == Convert.ToInt32(ObjMeetingRoomDesc.GetAllRequiredLanguage().Find(a => a.Name == "English").Id))
            {
                string strImageName = null;
                // get image name
                if (ulImage.HasFile)
                {
                    int fileSize = ulImage.PostedFile.ContentLength;
                    if (fileSize > (Convert.ToInt32(ConfigurationManager.AppSettings["LimitKB2"])))
                    {
                        divmessage.InnerHtml = "Filesize of image is too large. Maximum file size permitted is " + ConfigurationManager.AppSettings["MessageKB2"] + ".";
                        divmessage.Attributes.Add("class", "error");
                        divmessage.Style.Add("display", "block");
                        return;
                    }

                    else
                    {
                        Guid objID = Guid.NewGuid();
                        strImageName = Path.GetFileName(ulImage.FileName);
                        strImageName = strImageName.Replace(" ", "").Replace("%20", "").Trim();
                        strImageName = System.Text.RegularExpressions.Regex.Replace(strImageName, @"[^a-zA-Z 0-9'.@]", string.Empty).Trim();
                        strImageName = objID + strImageName;
                        ulImage.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "MeetingRoomImage/") + strImageName);
                    }

                }

                string strPlanName = string.Empty;
                // get file name
                if (ulPlan.HasFile)
                {
                    int fileSize = ulPlan.PostedFile.ContentLength;
                    if (fileSize > (1048576))
                    {
                        divmessage.InnerHtml = "Filesize of plan is too large. Maximum file size permitted is 1 MB.";
                        divmessage.Attributes.Add("class", "error");
                        divmessage.Style.Add("display", "block");
                        return;
                    }
                    else
                    {
                        strPlanName = Path.GetFileName(ulPlan.FileName);
                        ulPlan.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "Plan/") + strPlanName);
                    }
                }

                int intDayLight = Convert.ToInt32(ddlDayLight.SelectedValue);

                MeetingRoom objMeetingroom = new MeetingRoom();
                objMeetingroom.HotelId = intHotelID;
                objMeetingroom.Name = txtName.Text.Trim();
                objMeetingroom.DayLight = intDayLight;
                objMeetingroom.Surface = Convert.ToInt32(txtSurf.Text);
                objMeetingroom.Height = Convert.ToDecimal(txtheight.Text);
                objMeetingroom.Picture = strImageName;
                objMeetingroom.MrPlan = strPlanName;
                objMeetingroom.UpdatedBy = intUserID;
                int? orderNumberByHotelID = ObjMeetingRoomDesc.GetOrderNumberOfMeetingroom(intHotelID);
                if (orderNumberByHotelID != null)
                {
                    objMeetingroom.OrderNumber = orderNumberByHotelID + 1;

                }
                else
                {
                    objMeetingroom.OrderNumber = 1;
                }

                MeetingRoomPictureVideo objMeetingroomPictureVideo = new MeetingRoomPictureVideo();

                objMeetingroomPictureVideo.ImageName = strImageName;
                objMeetingroomPictureVideo.IsMain = true;
                objMeetingroomPictureVideo.FileType = 0;
                objMeetingroomPictureVideo.OrderNumber = 1;

                MeetingRoomDesc objMeetingroomDesc = new MeetingRoomDesc();
                objMeetingroomDesc.LanguageId = intLanguageID;
                objMeetingroomDesc.Description = txtDesc.Text;
                //call function to add new meeting room 
                ObjMeetingRoomDesc.AddNewMeetingRoom(objMeetingroom, objMeetingroomDesc, objMeetingroomPictureVideo);

                //Get meeting room desc by meeting room Id.
                ObjAllLanguageDescription = ObjMeetingRoomDesc.GetMeetingRoomDescByMeetingRoomID(Convert.ToInt32(objMeetingroom.Id));

                int intMinEnumValue = ObjMeetingRoomConfig.GetMinMeetingRoomShapeID();
                int intMaxEnumValue = ObjMeetingRoomConfig.GetMaxMeetingRoomShapeID();
                int intIndex;
                for (intIndex = intMinEnumValue; intIndex <= intMaxEnumValue; intIndex++)
                {
                    if (Enum.GetName(typeof(DashboardLinks), intIndex) != null)
                    {
                        // call function to add meeting room Configuration.
                        MeetingRoomConfig objMeetignRoomConfig = new MeetingRoomConfig();
                        objMeetignRoomConfig.MeetingRoomId = objMeetingroom.Id;
                        objMeetignRoomConfig.RoomShapeId = intIndex;
                        objMeetignRoomConfig.MinCapacity = 00;
                        objMeetignRoomConfig.MaxCapicity = 00;
                        ObjMeetingRoomConfig.AddNewMeetingRoomCofig(objMeetignRoomConfig);
                    }

                }
                CreateDynamicLanguageTab(intHotelID);
                divMeetingRoomForm.Visible = false;
                BindMeetingRoomListView();
                divmessage.InnerHtml = ObjMeetingRoomDesc.propMessage;
                divmessage.Attributes.Add("class", "succesfuly");
                divmessage.Style.Add("display", "block");
                return;
            }
            else
            {
                ClearForm();
                divmessage.InnerHtml = "Please insert details in english language tab first.";
                divmessage.Attributes.Add("class", "error");
                divmessage.Style.Add("display", "block");
                return;
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This button click event used for update existing data.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            hdnSelectedRowID.Value = "0";
            int intLanguageID = 0;
            int MeetingRoomDescId = 0;
            string meetignRoomName = string.Empty;
            //get language ID
            if (hdnIDS.Value == "")
            {
                intLanguageID = Convert.ToInt32(ObjMeetingRoomDesc.GetAllRequiredLanguage().Find(a => a.Name == "English").Id);
            }
            else
            {
                intLanguageID = Convert.ToInt32(hdnIDS.Value);
            }

            if (intLanguageID == Convert.ToInt32(ObjMeetingRoomDesc.GetAllRequiredLanguage().Find(a => a.Name == "English").Id))
            {
                meetignRoomName = txtName.Text;
            }
            else
            {
                meetignRoomName = hdnMeetingRoomName.Value;
            }
            string strImageName = hdnImageName.Value;

            // get image name
            if (ulImage.HasFile)
            {
                int fileSize = ulImage.PostedFile.ContentLength;
                if (fileSize > (Convert.ToInt32(ConfigurationManager.AppSettings["LimitKB2"])))
                {
                    divmessage.InnerHtml = "Filesize of image is too large. Maximum file size permitted is " + ConfigurationManager.AppSettings["MessageKB2"] + ".";
                    divmessage.Attributes.Add("class", "error");
                    divmessage.Style.Add("display", "block");
                    return;
                }

                else
                {
                    File.Delete(Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "MeetingRoomImage/") + strImageName);
                    Guid objID = Guid.NewGuid();
                    strImageName = Path.GetFileName(ulImage.FileName);
                    strImageName = strImageName.Replace(" ", "").Replace("%20", "").Trim();
                    strImageName = System.Text.RegularExpressions.Regex.Replace(strImageName, @"[^a-zA-Z 0-9'.@]", string.Empty).Trim();
                    strImageName = objID + strImageName;

                    ulImage.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "MeetingRoomImage/") + strImageName);
                }
            }

            string strPlanName = Convert.ToString(hdnPlanName.Value);
            // get file name
            if (ulPlan.HasFile)
            {
                int fileSize = ulPlan.PostedFile.ContentLength;
                if (fileSize > (1048576))
                {
                    divmessage.InnerHtml = "Filesize of plan is too large. Maximum file size permitted is 1 MB.";
                    divmessage.Attributes.Add("class", "error");
                    divmessage.Style.Add("display", "block");
                    return;
                }
                else
                {
                    strPlanName = Path.GetFileName(ulPlan.FileName);
                    ulPlan.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "Plan/") + strPlanName);
                }
            }

            int intDayLight = Convert.ToInt32(ddlDayLight.SelectedValue);
            int intMeetingRoomId = Convert.ToInt32(hndMeetingRoomID.Value);

            MeetingRoom objMeetingroom = ObjMeetingRoomDesc.GetMeetingRoomByID(intMeetingRoomId);
            objMeetingroom.HotelId = intHotelID;
            objMeetingroom.Name = meetignRoomName;
            objMeetingroom.DayLight = intDayLight;
            objMeetingroom.Surface = Convert.ToInt32(txtSurf.Text);
            objMeetingroom.Height = Convert.ToDecimal(txtheight.Text);
            objMeetingroom.Picture = strImageName;
            objMeetingroom.MrPlan = strPlanName;
            objMeetingroom.UpdatedBy = intUserID;

            if (Convert.ToInt32(hdnMeetingRoomDescID.Value) > 0)
            {
                MeetingRoomDescId = Convert.ToInt32(hdnMeetingRoomDescID.Value);
                MeetingRoomDesc objMeetingroomDesc = ObjMeetingRoomDesc.GetMeetingRoomDescByID(MeetingRoomDescId);
                objMeetingroomDesc.LanguageId = intLanguageID;
                objMeetingroomDesc.Description = txtDesc.Text.Trim();
                //call function to update meetingroom.
                ObjMeetingRoomDesc.UpdateMeetingRoom(objMeetingroom, objMeetingroomDesc);
            }
            else
            {
                //Get meeting room desc by meeting room Id.
                ObjAllLanguageDescription = ObjMeetingRoomDesc.GetMeetingRoomDescByMeetingRoomID(Convert.ToInt32(objMeetingroom.Id));
                if (ObjAllLanguageDescription.Find(a => a.LanguageId == intLanguageID) != null)
                {
                    MeetingRoomDescId = Convert.ToInt32(ObjAllLanguageDescription.Find(a => a.LanguageId == intLanguageID).Id);
                    MeetingRoomDesc objMeetingroomDesc = ObjMeetingRoomDesc.GetMeetingRoomDescByID(MeetingRoomDescId);
                    objMeetingroomDesc.LanguageId = intLanguageID;
                    objMeetingroomDesc.Description = txtDesc.Text.Trim();
                    //call function to update meetingroom.
                    ObjMeetingRoomDesc.UpdateMeetingRoom(objMeetingroom, objMeetingroomDesc);
                }
                else
                {
                    MeetingRoomDesc objMeetingroomDesc = new MeetingRoomDesc();
                    objMeetingroomDesc.LanguageId = intLanguageID;
                    objMeetingroomDesc.Description = txtDesc.Text.Trim();
                    ObjMeetingRoomDesc.AddNewMeetingroomDesc(objMeetingroom, objMeetingroomDesc);
                }

            }

            hdnIDS.Value = "";
            divmessage.InnerHtml = ObjMeetingRoomDesc.propMessage;
            //CreateDynamicLanguageTab(intHotelID);
            BindMeetingRoomListView();
            IsOneMeetingRoomDone();
            divmessage.Attributes.Add("class", "succesfuly");
            divmessage.Style.Add("display", "block");
            divMeetingRoomForm.Visible = false;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This button click event used for reset form.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReset_Click(object sender, EventArgs e)
    {
        ClearForm();
        divmessage.Style.Add("display", "none");
        divmessage.Attributes.Add("class", "error");
    }

    /// <summary>
    /// This button click event used for hide form.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        hdnIDS.Value = "";
        BindMeetingRoomListView();
        ClearForm();
        divmessage.Style.Add("display", "none");
        divmessage.Attributes.Add("class", "error");
        hdnSelectedRowID.Value = "0";
    }

    /// <summary>
    /// This click event used for open form for new entry.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkAddNewMeetingRoom_Click(object sender, EventArgs e)
    {
        hdnIDS.Value = "";
        hdnMeetingRoomDescID.Value = "0";
        hndMeetingRoomID.Value = "0";
        CreateDynamicLanguageTab(intHotelID);
        divMeetingRoomForm.Visible = true;
        btnSave.Visible = true;
        btnCancel.Visible = true;
        btnUpdate.Visible = false;
        txtDesc.ReadOnly = false;
        ClearForm();
        txtName.Focus();
        divmessage.Style.Add("display", "none");
        divmessage.Attributes.Add("class", "error");
        hdnSelectedRowID.Value = "0";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "colorChange", "CallMyMethod();", true);

    }

    /// <summary>
    /// This event used for bound meeting room data with listview.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lstViewMeetingRoomDesc_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            ImageButton btnMoveUp = (ImageButton)e.Item.FindControl("btnMoveUp");
            ImageButton btnMoveDown = (ImageButton)e.Item.FindControl("btnMoveDown");
            if (count + 1 == 1)
            {
                btnMoveUp.Visible = false;
                btnMoveUp.CommandArgument = DataBinder.Eval(e.Item.DataItem, "Id").ToString() + "," + count;
            }
            else
            {
                if (Objlst.Count > 1)
                {
                    HiddenField hdnOrderNumber = (HiddenField)e.Item.FindControl("hdnOrderNumber");
                    MeetingRoom lst = Objlst.Where(a => a.OrderNumber < Convert.ToInt32(hdnOrderNumber.Value)).OrderByDescending(a => a.OrderNumber).FirstOrDefault();
                    btnMoveUp.CommandArgument = DataBinder.Eval(e.Item.DataItem, "Id").ToString() + "," + lst.Id;
                }
            }
            if (count + 1 == Objlst.Count)
            {
                btnMoveDown.Visible = false;
                btnMoveDown.CommandArgument = DataBinder.Eval(e.Item.DataItem, "Id").ToString() + "," + (count + 1);
            }
            else
            {
                if (Objlst.Count > 1)
                {
                    HiddenField hdnOrderNumber = (HiddenField)e.Item.FindControl("hdnOrderNumber");
                    MeetingRoom lst = Objlst.Where(a => a.OrderNumber > Convert.ToInt32(hdnOrderNumber.Value)).OrderBy(a => a.OrderNumber).FirstOrDefault();
                    btnMoveDown.CommandArgument = DataBinder.Eval(e.Item.DataItem, "Id").ToString() + "," + lst.Id;
                }
            }
            count++;
            ListViewDataItem currentItem = (ListViewDataItem)e.Item;
            DataKey currentDataKey = this.lstViewMeetingRoomDesc.DataKeys[currentItem.DataItemIndex];
            MeetingRoom room = e.Item.DataItem as MeetingRoom;
            Button btnModify = (Button)e.Item.FindControl("lnkModify");
            btnModify.OnClientClick = "ChangeRowColor('li" + room.Id + "')";
            Label lblMeetingRoom = (Label)currentItem.FindControl("lblMeetingRoomName");
            lblMeetingRoom.Text = room.Name.ToString();
            Label lblDayLight = (Label)currentItem.FindControl("lblDayLight");
            if (room.DayLight == 1)
            {
                lblDayLight.Text = "Yes";
            }
            else if (room.DayLight == 2)
            {
                lblDayLight.Text = "Partial";
            }
            else
            {
                lblDayLight.Text = "No";
            }
            Label lblSurf = (Label)currentItem.FindControl("lblSurf");
            lblSurf.Text = room.Surface.ToString();
            Label lblHeight = (Label)currentItem.FindControl("lblHeight");
            lblHeight.Text = room.Height.ToString();
            Label lblDesc = (Label)currentItem.FindControl("lblDescription");
            var getDescription = ObjMeetingRoomDesc.GetMeetingRoomDescByMeetingRoomID(Convert.ToInt32(currentDataKey.Value));
            int englishID = Convert.ToInt32((ObjMeetingRoomDesc.GetAllRequiredLanguage().Find(b => b.Name == "English")).Id);
            var englishDesc = getDescription.Find(a => a.LanguageId == englishID);
            if (englishDesc != null)
            {
                string desc = englishDesc.Description;
                lblDesc.Text = desc;
            }
            HyperLink PlanView = (HyperLink)currentItem.FindControl("linkviewPlan");
            if (room.MrPlan != "")
            {
                PlanView.NavigateUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "Plan/" + room.MrPlan;
                PlanView.ToolTip = room.MrPlan;
            }
            else
            {
                PlanView.Visible = false;
            }
            if (room.Picture != null)
            {
                Image imgHotel = (Image)currentItem.FindControl("imgMeetingRoom");
                imgHotel.Width = 70;
                imgHotel.Height = 70;
                imgHotel.ImageUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "MeetingRoomImage/" + room.Picture;
            }
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
        }

    }

    /// <summary>
    /// This event used for perform view,delete and swap.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lstViewMeetingRoomDesc_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        //Disable description textbox if hotel is goonline
        if (Convert.ToString(ViewState["Count"]) == "1")
        {
            if (Convert.ToString(Session["Operator"]) == "1")
            {

            }
            else
            {
                int intRooomID = Convert.ToInt32(e.CommandArgument);
                Hotel hotel = ObjHotelinfo.GetHotelByHotelID(intHotelID);
                TList<CountryLanguage> lstlangCountry = ObjHotelinfo.GetAllRequiredLanguageByCountryId(Convert.ToInt32(hotel.CountryId));
                TList<MeetingRoom> ObjMeetingRoomCount = ObjMeetingRoomDesc.GetAllMeetingRoom(intHotelID);
                TList<MeetingRoomDesc> lstCountdesc = DataRepository.MeetingRoomDescProvider.GetByMeetingRoomId(intRooomID);
                if (lstlangCountry.Count <= lstCountdesc.Count)
                {
                    txtDesc.ReadOnly = true;
                }
                else
                {
                    txtDesc.ReadOnly = false;
                }
            }
        }
        //Disable description textbox if hotel is goonline
        int intMeetingRooomID = 0;
        switch (e.CommandName.Trim())
        {
            case "View":
                hdnIDS.Value = "";
                hdnMeetingRoomDescID.Value = "0";
                intMeetingRooomID = Convert.ToInt32(e.CommandArgument);
                hndMeetingRoomID.Value = intMeetingRooomID.ToString();
                divMeetingRoomForm.Visible = true;
                CreateDynamicLanguageTab(intHotelID);
                FillMeetingRoom(intMeetingRooomID);
                ObjAllLanguageDescription = ObjMeetingRoomDesc.GetMeetingRoomDescByMeetingRoomID(intMeetingRooomID);
                btnSave.Visible = false;
                btnUpdate.Visible = true;
                txtDesc.Focus();
                ScriptManager.RegisterStartupScript(this, typeof(Page), "colorChange", "CallMyMethod();", true);
                break;

            case "deleteItem":
                hndMeetingRoomID.Value = "0";
                string msg = ObjMeetingRoomDesc.DeleteMeetingRoom(Convert.ToInt32(e.CommandArgument));
                //divMeetingRoomForm.Visible = false;
                BindMeetingRoomListView();
                IsOneMeetingRoomDone();
                CreateDynamicLanguageTab(intHotelID);
                break;

            case "Swap":
                divMeetingRoomForm.Visible = false;
                string[] ids = e.CommandArgument.ToString().Split(',');
                //Current Item
                MeetingRoom objCurrentItem = Objlst.Single(a => a.Id == Convert.ToInt32(ids[0]));
                //Next Item
                MeetingRoom objNextClient = Objlst.Single(a => a.Id == Convert.ToInt32(ids[1]));
                int? CurrentOrder = objCurrentItem.OrderNumber;
                objCurrentItem.OrderNumber = objNextClient.OrderNumber;
                objNextClient.OrderNumber = CurrentOrder;
                ObjMeetingRoomDesc.UpdateCurrentOrder(objCurrentItem);
                ObjMeetingRoomDesc.UpdateNextOrder(objNextClient);
                BindMeetingRoomListView();
                break;
            default:
                break;
        }
        divmessage.Style.Add("display", "none");
        divmessage.Attributes.Add("class", "error");
    }

    /// <summary>
    /// This button click event used for proceed to next step.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNext_Click(object sender, EventArgs e)
    {
        divmessage.Style.Add("display", "none");
        divmessage.Attributes.Add("class", "error");
        ObjWizardLinkSetting.ThisPageIsDone(intHotelID, Convert.ToInt32(PropertyLevelSectionLeftMenu.lnkDescriptionMeetingRoom));
        Response.Redirect("MeetingRoomConfig.aspx", false);
        return;
    }

    #endregion
}