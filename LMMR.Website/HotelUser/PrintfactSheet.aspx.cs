﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using LMMR.Entities;
using LMMR.Data;
using LMMR.Business;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Configuration;
using log4net;
using log4net.Config;
#endregion

public partial class HotelUser_PrintfactSheet : System.Web.UI.Page
{
    #region variable declaration
    MeetingRoomConfigManager ObjMeetingRoomConfigManager = new MeetingRoomConfigManager();
    MeetingRoomDescManager ObjMeetingRoomDescManager = new MeetingRoomDescManager();
    int hotelPid = 0;
    WizardLinkSettingManager Objwizard = new WizardLinkSettingManager();
    DashboardManager Objdash = new DashboardManager();
    HotelManager objHotelManager = new HotelManager();
    HotelInfo ObjHotelinfo = new HotelInfo();
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_PrintfactSheet));
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl + "/";
        }
    }
    #endregion

    #region Additional Methods
    /// <summary>
    /// For check go online for this hotel
    /// </summary>    
    public void Getgoonline()
    {
        if (ObjHotelinfo.GetHotelGoOnline(hotelPid).Count > 0)
        {
            divNext.Visible = false;
        }
    }

    /// <summary>
    /// Function all meeting room with details to Listview
    /// </summary>
    void BindMeetingRoomConfig()
    {
        try
        {
            lstViewMeetingRoomConfig.DataSource = ObjMeetingRoomDescManager.GetAllMeetingRoom(hotelPid);
            lstViewMeetingRoomConfig.DataBind();
            if(ObjMeetingRoomDescManager.GetAllMeetingRoom(hotelPid).Count == 0)
            {
                DivPrint.Visible = false;
            }

            Hotel hotel = ObjHotelinfo.GetHotelByHotelID(hotelPid);
            contentbody_imgMainimage.Src = string.IsNullOrEmpty(hotel.Logo) ? ConfigurationManager.AppSettings["FilePath"] + "/HotelImage/default_hotel_logo.png" : SiteRootPath + ConfigurationManager.AppSettings["FilePath"].Replace("~/", "") + "HotelImage/" + hotel.Logo;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// Function for gridview to export PDF with condition of different server controls
    /// </summary>
    private void PrepareGridViewForExport(Control gv)
    {

        LinkButton lb = new LinkButton();

        Literal l = new Literal();

        string name = String.Empty;

        for (int i = 0; i < gv.Controls.Count; i++)
        {

            if (gv.Controls[i].GetType() == typeof(LinkButton))
            {

                l.Text = (gv.Controls[i] as LinkButton).Text;

                gv.Controls.Remove(gv.Controls[i]);

                gv.Controls.AddAt(i, l);

            }

            else if (gv.Controls[i].GetType() == typeof(DropDownList))
            {

                l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;

                gv.Controls.Remove(gv.Controls[i]);

                gv.Controls.AddAt(i, l);

            }

            else if (gv.Controls[i].GetType() == typeof(CheckBox))
            {

                l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";

                gv.Controls.Remove(gv.Controls[i]);

                gv.Controls.AddAt(i, l);

            }
            else if (gv.Controls[i].GetType() == typeof(HyperLink))
            {

                l.Text = (gv.Controls[i] as HyperLink).Text;

                gv.Controls.Remove(gv.Controls[i]);

                gv.Controls.AddAt(i, l);

            }

            else if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.Image))
            {

                // l.Text = (gv.Controls[i] as Image).Checked ? "True" : "False";

                gv.Controls.Remove(gv.Controls[i]);

                gv.Controls.AddAt(i, l);

            }

            if (gv.Controls[i].HasControls())
            {

                PrepareGridViewForExport(gv.Controls[i]);

            }

        }

    }

    //Rendor Method
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }   

    #endregion   

    #region PageLoad
    /// <summary>
    /// On page load we manage all the containts and Apply paging if there is a post back.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Session for Hotelname        
            if (Session["CurrentHotelID"] == null)
            {
                //Response.Redirect("~/login.aspx");
                Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                Session.Abandon();
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "login/english");
                }
            }
            hotelPid = Convert.ToInt32(Session["CurrentHotelID"]);
            //Session for Hotelnames  

            if (!IsPostBack)
            {
                Session["LinkID"] = "lnkPropertyLevel";

                //Call function for Binding meeeting room configuration
                BindMeetingRoomConfig();

                //Call function for Go online
                Getgoonline();

                #region Leftmenu
                HyperLink lnk = (HyperLink)this.Master.FindControl("lnkConferenceInfo");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkContactDetails");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkFacilities");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkCancelationPolicy");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkDescriptionMeetingRoom");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkConfiguration");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkPrintFactSheet");
                lnk.CssClass = "selected";
                #endregion

                //Bind static Images for Gridview header
                imgTheatre.Src = SiteRootPath + "Images/theatre.gif";
                imgSchool.Src = SiteRootPath + "Images/classroom.gif";
                imgShape.Src = SiteRootPath + "Images/ushape.gif";
                imgBoardroom.Src = SiteRootPath + "Images/boardroom.gif";
                imgCocktail.Src = SiteRootPath + "Images/cocktail.gif";
            }

            #region
            /// <summary>
            /// Fetch the hotel information 
            /// </summary>
            lblHotel.Text = objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Name;
            lblHotelName.Text = objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Name;
            lblTel.Text = objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).PhoneExt + " - " + objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).PhoneNumber;
            lblFax.Text = objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).CountryCodeFax + " - " + objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).FaxNumber;
            lblEmail.Text = objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Email;
            #endregion

            #region
            /// <summary>
            /// Get the hotel address and description of English id
            /// </summary>
            int intLanguageID = 0;
            intLanguageID = Convert.ToInt32(ObjHotelinfo.GetAllRequiredLanguage().Find(a => a.Name == "English").Id);            
            string whereclause = "Hotel_Id='" + hotelPid + "' and " + " Language_Id='" + intLanguageID + "' ";
            TList<HotelDesc> lsthCount = ObjHotelinfo.GetHotelDesc(whereclause, String.Empty);
            if (lsthCount.Count > 0)
            {
                lblAddress.Text = lsthCount[0].Address;
                lblDescription.Text = lsthCount[0].Description;
            }
            #endregion
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion    

    #region Fetch meeting room details
    /// <summary>
    /// Function for fetch all images,halfday,fullday price of meeting room
    /// </summary>
    protected void lstViewMeetingRoomConfig_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {

                ListViewDataItem currentItem = (ListViewDataItem)e.Item;
                DataKey currentDataKey = this.lstViewMeetingRoomConfig.DataKeys[currentItem.DataItemIndex];

                int intMeetingRoomID = Convert.ToInt32(currentDataKey.Value);
                //for image
                //System.Web.UI.WebControls.Image hotelImage = (System.Web.UI.WebControls.Image)e.Item.FindControl("imgHotelImage");
                System.Web.UI.HtmlControls.HtmlImage hotelImage = (System.Web.UI.HtmlControls.HtmlImage)e.Item.FindControl("imgHotelImage");
                if (!string.IsNullOrEmpty(DataBinder.Eval(e.Item.DataItem, "Picture").ToString()))
                {
                    hotelImage.Src = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].Replace("~/","") + "MeetingRoomImage/" + DataBinder.Eval(e.Item.DataItem, "Picture");
                    //hotelImage.Src = SiteRootPath + SiteRootPath + "MeetingRoomImage/" + DataBinder.Eval(e.Item.DataItem, "Picture");

                }
                else
                    hotelImage.Src = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].Replace("~/", "") + "HotelImage/default_hotel_logo.png";

                //Get Meeting room description
                Label lblDesc = (Label)currentItem.FindControl("lblDescription");
                var getDescription = ObjMeetingRoomDescManager.GetMeetingRoomDescByMeetingRoomID(Convert.ToInt32(currentDataKey.Value));
                int englishID = Convert.ToInt32((ObjMeetingRoomDescManager.GetAllRequiredLanguage().Find(b => b.Name == "English")).Id);
                var englishDesc = getDescription.Find(a => a.LanguageId == englishID);
                if (englishDesc != null)
                {
                    string desc = englishDesc.Description;
                    lblDesc.Text = desc;
                }


                Label lblHalfday = (Label)e.Item.FindControl("lblHalfday");
                Label lblFullday = (Label)e.Item.FindControl("lblFullday");
                if (DataBinder.Eval(e.Item.DataItem, "HalfdayPrice") != null)
                {
                    lblHalfday.Text = "Half-Day :" + String.Format("{0:#,###}", DataBinder.Eval(e.Item.DataItem, "HalfdayPrice"));
                }
                else
                {
                    lblHalfday.Text = "Half-Day : 0";
                }
                if (DataBinder.Eval(e.Item.DataItem, "FulldayPrice") != null)
                {
                    lblFullday.Text = "Full-Day :" + String.Format("{0:#,###}", DataBinder.Eval(e.Item.DataItem, "FulldayPrice"));
                }
                else
                {
                    lblFullday.Text = "Full-Day : 0";
                }
                //for image

                TList<MeetingRoomConfig> lst = ObjMeetingRoomConfigManager.GetConfigByMeetingRoomID(intMeetingRoomID);

                int intMinEnumValue = ObjMeetingRoomConfigManager.GetMinMeetingRoomShapeID();
                int intMaxEnumValue = ObjMeetingRoomConfigManager.GetMaxMeetingRoomShapeID();
                int intIndex;
                for (intIndex = intMinEnumValue; intIndex <= intMaxEnumValue; intIndex++)
                {
                    var getRow = lst.Find(a => a.RoomShapeId == intIndex);
                    if (getRow != null)
                    {

                        if (getRow.RoomShapeId == 1)
                        {
                            Label lblTheatreMin = (Label)e.Item.FindControl("lblTheatreMin");
                            Label lblTheatreMax = (Label)e.Item.FindControl("lblTheatreMax");
                            lblTheatreMin.Text = getRow.MinCapacity.ToString() + " - ";
                            lblTheatreMax.Text = getRow.MaxCapicity.ToString();
                            if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                            {
                                lblTheatreMin.Text = "";
                                lblTheatreMax.Text = "";
                            }

                        }
                        else if (getRow.RoomShapeId == 2)
                        {

                            Label lblSchoolMin = (Label)e.Item.FindControl("lblSchoolMin");
                            Label lblSchoolMax = (Label)e.Item.FindControl("lblSchoolMax");
                            lblSchoolMin.Text = getRow.MinCapacity.ToString() + " - ";
                            lblSchoolMax.Text = getRow.MaxCapicity.ToString();
                            if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                            {
                                lblSchoolMin.Text = "";
                                lblSchoolMax.Text = "";
                            }
                        }
                        else if (getRow.RoomShapeId == 3)
                        {

                            Label lblUShapeMin = (Label)e.Item.FindControl("lblUShapeMin");
                            Label lblUShapeMax = (Label)e.Item.FindControl("lblUShapeMax");
                            lblUShapeMin.Text = getRow.MinCapacity.ToString() + " - ";
                            lblUShapeMax.Text = getRow.MaxCapicity.ToString();
                            if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                            {
                                lblUShapeMin.Text = "";
                                lblUShapeMax.Text = "";
                            }

                        }
                        else if (getRow.RoomShapeId == 4)
                        {
                            Label lblBoardroomMin = (Label)e.Item.FindControl("lblBoardroomMin");
                            Label lblBoardroomMax = (Label)e.Item.FindControl("lblBoardroomMax");
                            lblBoardroomMin.Text = getRow.MinCapacity.ToString() + " - ";
                            lblBoardroomMax.Text = getRow.MaxCapicity.ToString();
                            if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                            {
                                lblBoardroomMin.Text = "";
                                lblBoardroomMax.Text = "";
                            }
                        }
                        else if (getRow.RoomShapeId == 5)
                        {

                            Label lblCocktailMin = (Label)e.Item.FindControl("lblCocktailMin");
                            Label lblCocktailMax = (Label)e.Item.FindControl("lblCocktailMax");
                            lblCocktailMin.Text = getRow.MinCapacity.ToString() + " - ";
                            lblCocktailMax.Text = getRow.MaxCapicity.ToString();
                            if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                            {
                                lblCocktailMin.Text = "";
                                lblCocktailMax.Text = "";
                            }

                        }
                        else if (getRow.RoomShapeId == 6)
                        {

                            Label lblClassRoomMin = (Label)e.Item.FindControl("lblClassRoomMin");
                            Label lblClassRoomMax = (Label)e.Item.FindControl("lblClassRoomMax");
                            lblClassRoomMin.Text = getRow.MinCapacity.ToString() + " - ";
                            lblClassRoomMax.Text = getRow.MaxCapicity.ToString();
                            if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                            {
                                lblClassRoomMin.Text = "";
                                lblClassRoomMax.Text = "";
                            }

                        }
                    }


                }


            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region Move to Next page
    /// <summary>
    /// For moving to next page
    /// </summary>
    protected void btnNext_Click(object sender, EventArgs e)
    {
        Objwizard.ThisPageIsDone(hotelPid, Convert.ToInt32(PropertyLevelSectionLeftMenu.lnkPrintFactSheet));
        Response.Redirect("PreviewOnWeb.aspx");
    }
    #endregion

    #region Export to PDF
    /// <summary>
    /// Function for export to PDF 
    /// </summary>
    protected void uiLinkButtonSaveAsPdfGrid_Click(object sender, EventArgs e)
    {
        try
        {
            //Dynamic Images Bind for PDF export
            imgTheatre.Src = SiteRootPath + "Images/icon-1.jpg";
            imgTheatre.Width = 131;            
            imgSchool.Src = SiteRootPath + "Images/icon-2.jpg";
            imgSchool.Width = 131;
            imgShape.Src = SiteRootPath + "Images/icon-3.jpg";
            imgShape.Width = 131;
            imgBoardroom.Src = SiteRootPath + "Images/icon-4.jpg";
            imgBoardroom.Width = 131;
            imgCocktail.Src = SiteRootPath + "Images/icon-5.jpg";
            imgCocktail.Width = 131;
            imgLogo.Src = SiteRootPath + "Images/logo.png";
            imgLogo.Width = 198;
            //Set margin of Parent table(tblParent) and for applying border in PDF's gridview set border in every TD
            tblParent.CellPadding = 35;
            

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=UserDetails.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            PrepareGridViewForExport(divGrid);
            divGrid.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    

    
}