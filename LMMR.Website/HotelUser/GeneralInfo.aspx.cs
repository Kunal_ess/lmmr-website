﻿#region Using
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Data;
using LMMR.Entities;
using System.Text;
using System.Web.Services;
using log4net;
using log4net.Config;
#endregion

public partial class GeneralInfo : System.Web.UI.Page
{
    #region variable declaration
    public static Hotel hotel;
    public static HotelDesc hdesc;
    string strnew = "1";
    StringBuilder ObjSBLanguage = new StringBuilder();
    public static int hotelPid=0;
    HotelInfo ObjHotelinfo = new HotelInfo();
    ILog logger = log4net.LogManager.GetLogger(typeof(GeneralInfo));
    public static TList<HotelDesc> ObjAllLanguageDescription = new TList<HotelDesc>();
    public static TList<Language> ObjDefaultLanguage = new TList<Language>();
    WizardLinkSettingManager Objwizard = new WizardLinkSettingManager();
    DashboardManager Objdash = new DashboardManager();
    HotelManager objHotelManager = new HotelManager();
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl + "/";
        }
    }
    #endregion

    #region PageLoad
    /// <summary>
    /// On page load we manage all the containts and Apply paging if there is a post back.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Session for Hotelname        
            if (Session["CurrentHotelID"] == null)
            {
                Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                Session.Abandon();
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "login/english");
                }
            }

            hotelPid = Convert.ToInt32(Session["CurrentHotelID"]);
            //Session for Hotelnames      
            if (!IsPostBack)
            {
                #region Leftmenu
                HyperLink lnk = (HyperLink)this.Master.FindControl("lnkConferenceInfo");
                lnk.CssClass = "selected";
                #endregion

                divmessage.Style.Add("display", "none");
                divmessage.Attributes.Add("class", "error");
                Session["LinkID"] = "lnkPropertyLevel";
                hotel = new Hotel();
                hdesc = new HotelDesc();

                //Call function for Dynamic tab 
                DynamicTab();

                //Call function for final check for which all language description has been filled or not
                ChkCountFinalChk();

                //Call function for binding all hotel information when page loading
                GetGeneralinfoDetails();

                //Get data for all language id on the basis of hotel id
                ObjAllLanguageDescription = ObjHotelinfo.GetHotelDescByHotelID(hotelPid);
                ObjDefaultLanguage = ObjHotelinfo.GetAllRequiredLanguage();

                //Get Hotel name on basis of hotel session id
                lblHotel.Text = objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Name;
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Insert/Update Hotel information
    /// <summary>
    //Function for submit all information by each language tab
    /// </summary> 
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            int intLanguageID = 0;
            if (hdnIDS.Value == "")
            {
                intLanguageID = Convert.ToInt32(ObjHotelinfo.GetAllRequiredLanguage().Find(a => a.Name == "English").Id);
            }
            else
            {
                intLanguageID = Convert.ToInt32(hdnIDS.Value);
            }

            if (intLanguageID != 1 && ObjHotelinfo.CheckLanguageId(hotelPid, 1).Count == 0)
            {
                divmessage.InnerHtml = "Please insert details in english language tab first.";
                divmessage.Attributes.Add("class", "error");
                divmessage.Style.Add("display", "block");
                return;
            }

            hotel = ObjHotelinfo.GetHotelByHotelID(hotelPid);
            hdesc = new HotelDesc();
            hdesc.HotelId = hotel.Id;            
            string whereclause = "Hotel_Id='" + hotel.Id + "' and " + " Language_Id='" + intLanguageID + "' ";
            //string orderbyclaus = HotelDescColumn.Id + " ASC";
            TList<HotelDesc> lsthCount = ObjHotelinfo.GetHotelDesc(whereclause, String.Empty);
            if (lsthCount.Count > 0)
            {
                int HotelDescId;
                if (Convert.ToInt32(hdnHotelDescID.Value) > 0)
                {
                    HotelDescId = Convert.ToInt32(hdnHotelDescID.Value);
                }
                else
                {
                    int intLanguageId = Convert.ToInt32(ObjHotelinfo.GetAllRequiredLanguage().Find(a => a.Name == "English").Id);                    
                    string wherecls = "Hotel_Id=" + hotelPid + " and " + "Language_Id=" + intLanguageId;
                    TList<HotelDesc> lsthdesc = ObjHotelinfo.GetHotelDesc(wherecls, String.Empty);
                    HotelDescId = Convert.ToInt32(lsthdesc[0].Id);
                }
                //call function to update meeting room desc.
                ObjHotelinfo.UpdateHotelDesc(HotelDescId, Convert.ToInt32(hotel.Id), intLanguageID, txtDescription.Text.Trim(), txtAddress.Text.Trim());
                if (intLanguageID == Convert.ToInt32(ObjHotelinfo.GetAllRequiredLanguage().Find(a => a.Name == "English").Id))
                {
                    Hotel objHotelAds = ObjHotelinfo.GetHotelByHotelID(hotelPid);
                    objHotelAds.HotelAddress = txtAddress.Text.Trim();                    
                    ObjHotelinfo.UpdateHotelInfo(objHotelAds);
                }
            }
            else
            {
                //For insert new description                
                ObjHotelinfo.InsertHotelDesc(hotelPid, Convert.ToInt32(intLanguageID), txtDescription.Text.Trim(), txtAddress.Text.Trim());
                if (intLanguageID == Convert.ToInt32(ObjHotelinfo.GetAllRequiredLanguage().Find(a => a.Name == "English").Id))
                {
                    Hotel objHotelAds = new Hotel();
                    objHotelAds.HotelAddress = txtAddress.Text.Trim();
                    ObjHotelinfo.UpdateHotelInfo(objHotelAds);
                }
            }

            hotel.CountryCodePhone = drpExtPhone.SelectedItem.Text.Replace("'", "");
            hotel.PhoneNumber = txtPhone.Text.Replace("'", "");
            hotel.CountryCodeFax = drpExtFax.SelectedItem.Text.Replace("'", "");
            hotel.FaxNumber = txtFaxNumber.Text.Replace("'", "");
            hotel.TimeZone = drpTimeZone.SelectedItem.Value.Replace("'", "");
            hotel.RtMFrom = droFromMorning.SelectedItem.Text.Replace("'", "");
            hotel.RtMTo = drpToMorning.SelectedItem.Text.Replace("'", "");
            hotel.RtAFrom = droFromAfternoon.SelectedItem.Text.Replace("'", "");
            hotel.RtATo = drpToAfternoon.SelectedItem.Text.Replace("'", "");
            hotel.RtFFrom = drpFromFullday.SelectedItem.Text.Replace("'", "");
            hotel.RtFTo = drpToFullday.SelectedItem.Text.Replace("'", "");
            hotel.ZipCode = txtZipCode.Text.Replace("'", "");
            hotel.Theme = drpMeetingFacility.SelectedItem.Text.Replace("'", "");
            if (drpBedroom.SelectedIndex == 1)
            {
                hotel.IsBedroomAvailable = true;
                txtNoofBed.Enabled = true;
                if (txtNoofBed.Text != "")
                {
                    hotel.NumberOfBedroom = Convert.ToInt32(txtNoofBed.Text.Replace("'", ""));
                }
            }
            else if (drpBedroom.SelectedIndex == 2)
            {
                hotel.IsBedroomAvailable = false;
                hotel.NumberOfBedroom = 0;
                //-- cleared the field of no of bedroom
                txtNoofBed.Text = "";
            }

            string type = "";
            for (int i = 0; i < chkCreditCard.Items.Count; i++)
            {
                if (chkCreditCard.Items[i].Selected)
                {
                    type += chkCreditCard.Items[i].Text + ",";
                }
            }
            hotel.CreditCardType = type.Replace("'", "");

            ObjHotelinfo.UpdateHotelInfo(hotel);

            if (ObjHotelinfo.GetHotelGoOnline(hotelPid).Count > 0)
            {
                drpBedroom.Enabled = false;
                txtNoofBed.Enabled = false;
                if (Convert.ToString(Session["Operator"]) == "1")
                {
                    drpBedroom.Enabled = true;
                    txtNoofBed.Enabled = true;
                }
            }

            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "succesfuly");
            divmessage.InnerHtml = "General info updated successfully!";           
            ChkCountFinalChk();
            DynamicTab();
            Getgoonline();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion    

    #region Additional Methods
    /// <summary>
    //Get All information for selected hotel and foe each language   
    /// </summary>    
    public int GetGeneralinfoDetails()
    {                
        //start for checking whether next button is clicked or not so that bedroom can't be changed
        if(ObjHotelinfo.GetHotelGoOnline(hotelPid).Count > 0)      
        {
            drpBedroom.Enabled = false;            
            txtNoofBed.Enabled = false;
            //DivBedType.Disabled = true;
            divNext.Visible = false;
            ViewState["Count"] = ObjHotelinfo.GetHotelGoOnline(hotelPid).Count;
            //Disable description textbox if hotel is goonline
            if (Convert.ToString(Session["Operator"]) == "1")
            {
                drpBedroom.Enabled = true;
                txtNoofBed.Enabled = true;
            }
            else
            {
                txtDescription.ReadOnly = true;
            }
            //Disable description textbox if hotel is goonline
        }
        //end for checking whether next button is clicked or not so that bedroom can't be changed
        try
        {
            //Get Hotel information by hotelid
            hotel = ObjHotelinfo.GetHotelByHotelID(hotelPid);
            lblHotelName.Text = hotel.Name;            
            int englishID = Convert.ToInt32(ObjHotelinfo.GetAllRequiredLanguage().Find(a => a.Name == "English").Id);
            string whereclause = "Hotel_Id=" + hotel.Id + " and " + "Language_Id=" + englishID;
            TList<HotelDesc> lsthdesc = ObjHotelinfo.GetHotelDesc(whereclause, String.Empty);
            if (lsthdesc.Count > 0)
            {
                hdesc = lsthdesc[0];
                txtAddress.Text = hdesc.Address;
                txtDescription.Text = hdesc.Description;
            }
            else
            {
                txtAddress.Text = hotel.HotelAddress;
            }
            txtPhone.Text = hotel.PhoneNumber;
            drpExtPhone.SelectedIndex = drpExtPhone.Items.IndexOf(drpExtPhone.Items.FindByText(hotel.CountryCodePhone));
            drpExtFax.SelectedIndex = drpExtFax.Items.IndexOf(drpExtFax.Items.FindByText(hotel.CountryCodeFax));
            txtFaxNumber.Text = hotel.FaxNumber;

            lblCountry.Text = ObjHotelinfo.GetCountryByID(Convert.ToInt32(hotel.CountryId));
            lblCity.Text = ObjHotelinfo.GetCityByID(Convert.ToInt32(hotel.CityId));
            lblZone.Text = ObjHotelinfo.GetZoneByID(Convert.ToInt32(hotel.ZoneId)); 
            lblLatitude.Text = hotel.Latitude;
            lblLongitude.Text = hotel.Longitude;
            drpTimeZone.SelectedIndex = drpTimeZone.Items.IndexOf(drpTimeZone.Items.FindByValue(hotel.TimeZone));
            droFromMorning.SelectedIndex = droFromMorning.Items.IndexOf(droFromMorning.Items.FindByText(hotel.RtMFrom));
            drpToMorning.SelectedIndex = drpToMorning.Items.IndexOf(drpToMorning.Items.FindByText(hotel.RtMTo));
            droFromAfternoon.SelectedIndex = droFromAfternoon.Items.IndexOf(droFromAfternoon.Items.FindByText(hotel.RtAFrom));
            drpToAfternoon.SelectedIndex = drpToAfternoon.Items.IndexOf(drpToAfternoon.Items.FindByText(hotel.RtATo));
            drpFromFullday.SelectedIndex = drpFromFullday.Items.IndexOf(drpFromFullday.Items.FindByText(hotel.RtFFrom));
            drpToFullday.SelectedIndex = drpToFullday.Items.IndexOf(drpToFullday.Items.FindByText(hotel.RtFTo));
            drpMeetingFacility.SelectedIndex = drpMeetingFacility.Items.IndexOf(drpMeetingFacility.Items.FindByText(hotel.Theme));
            txtZipCode.Text = hotel.ZipCode;

            //for static hotel star
            if (hotel.Stars == 1)
            {
                imgStars.ImageUrl = "~/Images/1.png";
            }
            else if (hotel.Stars == 2)
            {
                imgStars.ImageUrl = "~/Images/2.png";
            }
            else if (hotel.Stars == 3)
            {
                imgStars.ImageUrl = "~/Images/3.png";
            }
            else if (hotel.Stars == 4)
            {
                imgStars.ImageUrl = "~/Images/4.png";
            }
            else if (hotel.Stars == 5)
            {
                imgStars.ImageUrl = "~/Images/5.png";
            }
            else if (hotel.Stars == 6)
            {
                imgStars.ImageUrl = "~/Images/6.png";
            }
            else if (hotel.Stars == 7)
            {
                imgStars.ImageUrl = "~/Images/7.png";
            }
            //for static hotel star

            if (hotel.IsBedroomAvailable.ToString() == "True")
            {
                drpBedroom.SelectedIndex = drpBedroom.Items.IndexOf(drpBedroom.Items.FindByText("Yes"));
                txtNoofBed.Text = hotel.NumberOfBedroom.ToString();
                divBed.Visible = true;
            }
            else
            {
                drpBedroom.SelectedIndex = drpBedroom.Items.IndexOf(drpBedroom.Items.FindByText("No"));
                divBed.Visible = false;
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        string strorigin = hotel.CreditCardType;
        if (strorigin != null)
        {
            string[] str = strorigin.Split(',');
            for (int i = 0; i < str.Length; i++)
            {
                foreach (ListItem li in chkCreditCard.Items)
                {
                    if (li.Text == str[i].ToString())
                        li.Selected = true;
                }
            }
        }

        return 1;
    }

    /// <summary>
    //Bind dynamic tab from language table by country
    /// </summary> 
    public void DynamicTab()
    {
        try
        {
            hotel = ObjHotelinfo.GetHotelByHotelID(hotelPid);
            TList<CountryLanguage> lstlangbyCountry = ObjHotelinfo.GetAllRequiredLanguageByCountryId(Convert.ToInt32(hotel.CountryId));
            string whereclause = string.Empty;
            if (lstlangbyCountry.Count > 0)
            {
                whereclause = LanguageColumn.Id + " in (";
                foreach (CountryLanguage cl in lstlangbyCountry)
                {
                    whereclause += cl.LanguageId + ",";
                }
                if (whereclause.Length > 0)
                {
                    whereclause = whereclause.Substring(0, whereclause.Length - 1);
                }
                whereclause += ")";
            }            
            TList<Language> lstLang = ObjHotelinfo.GetLanguage(whereclause, string.Empty);


            // TList<Language> lstLang = ObjHotelinfo.GetAllRequiredLanguage();
            if (lstLang.Count > 0 && whereclause != "")
            {                
                string whereclauselang = "Hotel_Id=" + hotel.Id + " and " + "Language_Id=" + lstLang[0].Id;
                TList<HotelDesc> lsthdesc = ObjHotelinfo.GetHotelDesc(whereclauselang, String.Empty);
                if (lsthdesc.Count > 0)
                {
                    //if (hndHotelID.Value != "0")
                    //{
                    ObjSBLanguage.Append(@"<ul class=""TabbedPanelsTabGroup"">");
                    for (int i = 0; i <= lstLang.Count - 1; i++)
                    {
                        int intlanguaegeID = Convert.ToInt32(lstLang[i].Id);
                        var varLanguage = ObjHotelinfo.GetHotelDescByHotelID(Convert.ToInt32(lsthdesc[0].HotelId)).Find(a => a.LanguageId == intlanguaegeID);

                        if (varLanguage != null)
                        {

                            ObjSBLanguage.Append(@"<li class='TabbedPanelsTab' onclick='javascript:GetLanguageID(" + lstLang[i].Id + ");'><a href='#' >" + lstLang[i].Name + "</a></li>");
                        }
                        else
                        {
                            ObjSBLanguage.Append(@"<li class='TabbedPanelsTab1' onclick='javascript:GetLanguageID(" + lstLang[i].Id + ");'><a href='#'>" + lstLang[i].Name + "</a></li>");

                        }

                    }
                    ObjSBLanguage.Append(@"</ul>");
                    ObjSBLanguage.Append(@"<div class=""TabbedPanelsContentGroup"">");
                    for (int i = 0; i <= lstLang.Count - 1; i++)
                    {
                        ObjSBLanguage.Append(@"<div class=""TabbedPanelsContent""></div>");
                    }
                    ObjSBLanguage.Append(@"</div>");
                }
                else
                {
                    ObjSBLanguage.Append(@"<ul class=""TabbedPanelsTabGroup"">");
                    for (int i = 0; i <= lstLang.Count - 1; i++)
                    {
                        ObjSBLanguage.Append(@"<li class='TabbedPanelsTab1' onclick='javascript:GetLanguageID(" + lstLang[i].Id + ");'><a href='#' >" + lstLang[i].Name + "</a></li>");

                    }
                    ObjSBLanguage.Append(@"</ul>");
                    ObjSBLanguage.Append(@"<div class=""TabbedPanelsContentGroup"">");
                    for (int i = 0; i <= lstLang.Count - 1; i++)
                    {
                        ObjSBLanguage.Append(@"<div class=""TabbedPanelsContent""></div>");
                    }
                    ObjSBLanguage.Append(@"</div>");




                }
                litLangugeTab.Text = "";
                litLangugeTab.Text = ObjSBLanguage.ToString();
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    // Get Description By LanguageID when clicks on language tab using Json
    /// </summary> 
    [WebMethod]
    public static string GetDescByLanguageID(int languageID, string hotelID)
    {
        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        var engLanguage = ObjDefaultLanguage.Find(b => b.Name == "English");
        var data = ObjAllLanguageDescription.Find(a => a.LanguageId == languageID);
        HotelInfo ObjHotel = new HotelInfo();
        hotelPid = Convert.ToInt32(hotelID);
        //for checking address field, if address is null then bind english address                
        string whereclause = "Hotel_Id=" + hotelPid + " and " + "Language_Id=" + engLanguage.Id;
        TList<HotelDesc> lsthdesc = ObjHotel.GetHotelDesc(whereclause, String.Empty);

        string whereclause1 = "Hotel_Id=" + hotelPid + " and " + "Language_Id=" + languageID;
        TList<HotelDesc> lsthdescription = ObjHotel.GetHotelDesc(whereclause1, String.Empty);

        string Address = ObjHotel.GetHotelByHotelID(hotelPid).HotelAddress;

        if (lsthdescription.Count > 0)
        {
            //var varDesc = new { data.Description, data.Address, data.Id };
            var varDesc = new { lsthdescription[0].Description, lsthdescription[0].Address, lsthdescription[0].Id };
            return serializer.Serialize(varDesc);
        }
        else
        {            
            if (lsthdesc.Count > 0)
            {
                var varDesc = new { lsthdesc[0].Address };
                return serializer.Serialize(varDesc);
            }
            else
            {
                var varDesc = new { Address };
                return serializer.Serialize(varDesc);
            } 
                                  
        }


    }

    /// <summary>
    //Function for checking hotel whether the current hotel is goonline or not
    /// </summary>
    public void Getgoonline()
    {
        if (ObjHotelinfo.GetHotelGoOnline(hotelPid).Count > 0)
        {
            divNext.Visible = false;           
        }
    }

    /// <summary>
    //Count check for visible Next button whether description in all language filled or not
    /// </summary>
    public void ChkCountFinalChk()
    {
        hotel = ObjHotelinfo.GetHotelByHotelID(hotelPid);
        TList<CountryLanguage> lstlangCountry = ObjHotelinfo.GetAllRequiredLanguageByCountryId(Convert.ToInt32(hotel.CountryId));
        TList<HotelDesc> lstCountdesc = ObjHotelinfo.GetHotelDescByHotelID(hotelPid);
        if (lstlangCountry.Count == lstCountdesc.Count && lstCountdesc.Count > 0)
        {
            divNext.Visible = true;
            if (Convert.ToString(ViewState["Count"]) != "1")
            {
                divmessage.InnerHtml += "&nbsp; Please click on next button to move ahead.";
            }         
        }
    }

    #endregion       

    #region Enable/Disable Bedroom Field
    /// <summary>
    //This function is user when bedroom is available then textbox will be visible
    /// </summary>
    protected void drpBedroom_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpBedroom.SelectedValue == "1")
        {
            divBed.Visible = true;            
        }
        else
        {
            divBed.Visible = false;
        }
    }
    #endregion

    #region Move Next page
    /// <summary>
    //After submit all language information next button will be visible and it will redirect to next page
    /// </summary>
    protected void btnNext_Click(object sender, EventArgs e)
    {
        //Objwizard.ThisPageIsDone(hotelPid,        
        Objwizard.ThisPageIsDone(hotelPid, Convert.ToInt32(PropertyLevelSectionLeftMenu.lnkConferenceInfo));
        Response.Redirect("ContactDetails.aspx");
    }
    #endregion

    #region Reset Information
    /// <summary>
    //For Clearing all related fields on cancel button
    /// </summary>
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            //lblMessage.Text = "";
            int intLanguageID = 0;
            if (hdnIDS.Value == "")
            {
                intLanguageID = Convert.ToInt32(ObjHotelinfo.GetAllRequiredLanguage().Find(a => a.Name == "English").Id);
                txtZipCode.Text = "";
                txtAddress.Text = "";
                txtPhone.Text = "";
                drpExtPhone.SelectedIndex = -1;
                drpExtFax.SelectedIndex = -1;
                txtFaxNumber.Text = "";
                drpTimeZone.SelectedIndex = -1;
                drpToMorning.SelectedIndex = -1;
                drpToAfternoon.SelectedIndex = -1;
                drpToFullday.SelectedIndex = -1;
                droFromMorning.SelectedIndex = -1;
                droFromAfternoon.SelectedIndex = -1;
                drpFromFullday.SelectedIndex = -1;
                txtDescription.Text = "";
                chkCreditCard.SelectedIndex = -1;
                drpMeetingFacility.SelectedIndex = -1;
                drpBedroom.SelectedIndex = -1;
                //--- added txtNoof bed clear function
                txtNoofBed.Text = "";
                divBed.Visible = false;
            }
            else
            {
                intLanguageID = Convert.ToInt32(hdnIDS.Value);
                txtAddress.Text = "";
                txtDescription.Text = "";
            }
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion
}