﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master" AutoEventWireup="true" CodeFile="ViewBookings1.aspx.cs" Inherits="HotelUser_ViewBookings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" Runat="Server">
    <asp:Label ID="lblheadertext" runat="server" Text="New Booking For:"></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" Runat="Server">
    <div class="booking-details">
    <asp:HyperLink ID="HyperLink1" runat="server">ref 1</asp:HyperLink>
    <asp:GridView ID="grdViewBooking" runat="server" Width="100%" AutoGenerateColumns="false" RowStyle-HorizontalAlign="Center" RowStyle-VerticalAlign="Middle">
    <Columns>
    <asp:TemplateField HeaderText="Ref No">
    <ItemTemplate>
        <asp:Label ID="lblRefNo" runat="server"  ></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>
 
    <asp:TemplateField HeaderText="Meeting Room">
    <ItemTemplate>
        <asp:Label ID="lblmeetingroom" runat="server" ></asp:Label>
    </ItemTemplate>
    </asp:TemplateField> 

    <asp:TemplateField HeaderText="Booking Date">
    <ItemTemplate>
        <asp:Label ID="lblBookingDt" runat="server" ></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

    
    <asp:TemplateField HeaderText="Company">
    <ItemTemplate>
        <asp:Label ID="lblCompany" runat="server" ></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField HeaderText="Contact Name">
    <ItemTemplate>
        <asp:Label ID="lblConctName" runat="server" ></asp:Label>
    </ItemTemplate>
    </asp:TemplateField> 

    <asp:TemplateField HeaderText="Arrival Date">
    <ItemTemplate>
        <asp:Label ID="lblArrivalDt" runat="server" ></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

      <asp:TemplateField HeaderText="Depature Date">
    <ItemTemplate>
        <asp:Label ID="lblDepartureDt" runat="server" ></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField HeaderText="Total">
    <ItemTemplate>
        <asp:Label ID="lblFinalTotal" runat="server" ></asp:Label>
    </ItemTemplate>
    </asp:TemplateField> 
  
    </Columns>
  <HeaderStyle  CssClass="head" />
<RowStyle CssClass="value1" />
<AlternatingRowStyle CssClass="value2" />
    </asp:GridView>

	
					<%--	<ul>
						<li class="head">
							<div class="col1">Ref</div>
							<div class="col2">Meeting Room</div>
							<div class="col3">Booking date</div>
							<div class="col4">Company</div>
							<div class="col5">Contact Name</div>
							<div class="col6">Arrival T.</div>
							<div class="col7">Departure T.</div>
							<div class="col8">Total</div>
						</li>
						<li class="value1">
							<div class="col1"><a href="#">5090</a></div>
							<div class="col2">New York confere...</div>
							<div class="col3">03/08 &bull; 11:50</div>
							<div class="col4">Sawer Co</div>
							<div class="col5"><a href="#">Mary Lou Retton</a></div>
							<div class="col6">06/08 &bull; 14:00</div>
							<div class="col7">06/08 &bull; 18:00</div>
							<div class="col8">&euro; 8000</div>
						</li>
						<li class="value2">
							<div class="col1"><a href="#">8907</a></div>
							<div class="col2">Lotus budha </div>
							<div class="col3">03/08 &bull; 10:50</div>
							<div class="col4">Awer Co</div>
							<div class="col5"><a href="#">Tom Sawer</a></div>
							<div class="col6">06/08 &bull; 14:00</div>
							<div class="col7">06/08 &bull; 18:00</div>
							<div class="col8">&euro; 6500</div>
						</li>
						<li class="value1">
							<div class="col1"><a href="#">5092</a></div>
							<div class="col2">New York confere...</div>
							<div class="col3">03/08 &bull; 11:50</div>
							<div class="col4">Xeros</div>
							<div class="col5"><a href="#">John Travolta</a></div>
							<div class="col6">06/08 &bull; 14:00</div>
							<div class="col7">06/08 &bull; 18:00</div>
							<div class="col8">&euro; 4750</div>
						</li>
						<li class="value2">
							<div class="col1"><a href="#">8907</a></div>
							<div class="col2">Lotus budha </div>
							<div class="col3">03/08 &bull; 10:50</div>
							<div class="col4">Awer Co</div>
							<div class="col5"><a href="#">Kenny B Double</a></div>
							<div class="col6">06/08 &bull; 14:00</div>
							<div class="col7">06/08 &bull; 18:00</div>
							<div class="col8">&euro; 6500</div>
						</li>
						<li class="value3"><div class="col9"><a href="#">Print</a> &bull; <a href="#">Save as pdf</a></div></li>
						</ul>--%>
						
				
				</div>
				<h1 class="new">Booking details</h1>
				
				<div class="booking-details">
						<ul>
						<li class="value6"><div class="col14"><a href="#">Print</a> &bull; <a href="#">Save as pdf</a> &bull; <a href="#">Move to processed</a> &bull; <a href="#">Transfer</a>
					
						</div></li>
						<li class="value4">
							<div class="col10"><strong>Property name:</strong></div>
							<div class="col11">
                                <asp:Label ID="lblProteryname" runat="server" Text=""></asp:Label></div>
							<div class="col12">Booking done on :<asp:Label ID="lblbookingdoneOn" runat="server" Text=""></asp:Label></div>
						</li>
						<li class="value5">
							<div class="col10"><strong>Meeting room name:</strong></div>
							<div class="col11">
                                <asp:Label ID="lblMeetingroom" runat="server" Text=""></asp:Label></div>
							<div class="col12"><strong>Contact person:</strong> 
                                <asp:Label ID="lblContactPerson" runat="server" Text=""></asp:Label></div>
						</li>
						<li class="value4">
							<div class="col10"><strong>Booked for day(s):</strong></div>
							<div class="col11">
                                <asp:Label ID="lblBookedDays" runat="server" Text=""></asp:Label></div>
							<div class="col12"><strong>Contact email:</strong> 
                                <asp:Label ID="lblConatctpersonEmail" runat="server" Text="Label"></asp:Label></div>
						</li>
						<li class="value5">
							<div class="col10"><strong>Participants:</strong></div>
							<div class="col11">
                                <asp:Label ID="lblParticipants" runat="server" Text=""></asp:Label></div>
							<div class="col12"><strong>Contact phone: 
                                <asp:Label ID="lblContactPersonPhno" runat="server" Text="Label"></asp:Label></strong></a></div>
						</li>
						<li class="value4">
							<div class="col10"><strong>Budget:</strong></div>
							<div class="col11">
                                <asp:Label ID="lblBudget" runat="server" Text="Not Selected"></asp:Label></div>
							<div class="col13"></div>
						</li>
						<li class="value5">
							<div class="col10"><strong>Choosen shape</strong></div>
							<div class="col11">Banquet 40-50</div>
							<div class="col13"><strong>37 x 42 = &euro; 1264</strong></div>
						</li>
						<li class="value4">
							<div class="col10"><strong>Extra</strong></div>
							<div class="col11">Beamer</div>
							<div class="col13"><strong>&euro; 49</strong></div>
						</li>
						<li class="value5">
							<div class="col10"></div>
							<div class="col11">Projector</div>
							<div class="col13"><strong>&euro; 57</strong></div>
						</li>
						<li class="value4">
							<div class="col10"></div>
							<div class="col11">Etc</div>
							<div class="col13"><strong>&euro; 80</strong></div>
						</li>
						<li class="value5">
							<div class="col10"></div>
							<div class="col11"><strong>TOTAL</strong></div>
							<div class="col13"><strong>&euro; 4750</strong></div>
						</li>
						<li class="value4">
							<div class="col10"><strong>Other</strong></div>
							<div class="col11"></div>
							<div class="col13"></div>
						</li>
						<li class="value5">
							<div class="col15"><div class="col16"><b>Comments</b> </div><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sem diam, varius a adipiscing nec, ultricies vitae leo. Etiam ornare lacinia viverra. Curabitur at augue purus. Sed pulvinar facilisis vehicula. Cras commodo, erat et fermentum dignissim, nunc nisl porttitor metus, eget mollis lacus neque vitae tellus. 
</p></div>
							
						</li>
						<li class="value7">
							<div class="col14"><a href="#"><img src="../images/decline_bt.png" /></a> <a href="#"><img src="../images/confirm_bt.png" /></a></div>
						
						</li>
						
						
						</ul>
						
				
				</div>
</asp:Content>

