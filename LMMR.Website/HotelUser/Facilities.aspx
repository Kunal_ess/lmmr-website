﻿<%@ Page Title="Hotel User - Facilities" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master" AutoEventWireup="true"
    CodeFile="Facilities.aspx.cs" Inherits="HotelUser_Facilities" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" runat="Server">
    <h1>
        Facilities : <b>
            <asp:Label ID="lblHotel" runat="server" Text=""></asp:Label></b></h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" runat="Server">
<div style=" width:758px; overflow:hidden;">
     <div id="divmessage" runat="server">
            </div>
            </div>
    <div class="contact-details">
        <table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tr>
                <td valign="top">
                    <b>General</b>
                    <table width="100%" border="0" cellspacing="0" cellpadding="4">
                        <tr>
                            <td>
                                <asp:CheckBoxList ID="chkGeneral" runat="server" RepeatDirection="Horizontal" RepeatColumns="3"
                                    Width="100%">
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Activities</b>
                    <table width="100%" border="0" cellspacing="0" cellpadding="4">
                        <tr>
                            <td>
                                <asp:CheckBoxList ID="chkActivities" runat="server" RepeatDirection="Horizontal"
                                    Width="100%" RepeatColumns="3">
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Services</b>
                    <table width="100%" border="0" cellspacing="0" cellpadding="4">
                        <tr>
                            <td>
                                <asp:CheckBoxList ID="chkServices" runat="server" RepeatDirection="Horizontal" RepeatColumns="3"
                                    Width="100%">
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Meeting Specifics</b>
                    <table width="100%" border="0" cellspacing="0" cellpadding="4">
                        <tr>
                            <td>
                                <asp:CheckBoxList ID="chkMeeting" runat="server" RepeatDirection="Horizontal" RepeatColumns="3"
                                    Width="100%">
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Bedrooms</b>
                    <table width="100%" border="0" cellspacing="0" cellpadding="4">
                        <tr>
                            <td>
                                <asp:CheckBoxList ID="chkBedrooms" runat="server" RepeatDirection="Horizontal" RepeatColumns="3"
                                    Width="100%">
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div class="booking-details" style="width:760px;">
                <ul>
                <li class="value10">
                    <div class="col21" style="width:752px;">
                    <div class="button_section">
                            <asp:LinkButton ID="btnSubmit" runat="server" CssClass="select" Text="Save" OnClick="btnSubmit_Click" /> <span>or</span>
            <asp:LinkButton ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
            <div style="float: right">
            <asp:Label ID="lblMessage" runat="server" Text="" Font-Bold="true"></asp:Label>
        </div>
                        </div>   
                        </div>
                </li>
                </ul></div>
    
    <div class="col21" id="divNext" runat="server">
        <div class="button_sectionNext">
            <asp:LinkButton ID="btnNext" runat="server" class="RemoveCookie" Text="Next &gt;&gt;" OnClick="btnNext_Click" />
        </div>
    </div>
    <div id="Loding_overlaySec">
        <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br /><span>Saving...</span></div>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#<%= btnSubmit.ClientID %>").bind("click", function () {
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                jQuery("#Loding_overlaySec span").html("Saving...");
                jQuery("#Loding_overlaySec").show();
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            });
            jQuery("#<%= chkGeneral.ClientID %> td").width("33%");
            jQuery("#<%= chkActivities.ClientID %> td").width("33%");
            jQuery("#<%= chkServices.ClientID %> td").width("33%");
            jQuery("#<%= chkMeeting.ClientID %> td").width("33%");
            jQuery("#<%= chkBedrooms.ClientID %> td").width("33%");
        });
    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#<%= btnNext.ClientID %>").bind("click", function () {
                if (confirm('Are you sure you want proceed to next without saving ?')) {
                    jQuery("body").scrollTop(0);
                    jQuery("html").scrollTop(0);
                    jQuery("#Loding_overlaySec span").html("Loading...");
                    jQuery("#Loding_overlaySec").show();
                    document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                    return true;
                }
                else {
                    return false;
                }
            });
        });
    </script>
</asp:Content>
