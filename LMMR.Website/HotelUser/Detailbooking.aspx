﻿<%@ Page Title="Hotel User - Search" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master"
    AutoEventWireup="true" CodeFile="Detailbooking.aspx.cs" Inherits="HotelUser_ViewDetailbooking" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/UserControl/HotelUser/BookingDetails.ascx" TagName="BookingDetails" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" runat="Server">
    <h1>
        <asp:Label ID="lblheadertext" runat="server" Text="Search result for :"></asp:Label></h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" runat="Server">
    <style type="text/css">
        .modalBackground
        {
            background-color: #CCCCFF;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }
    </style>
    <link href="../css/style22.css" rel="stylesheet" type="text/css" />
    <%--<link href="../css/style-front.css" rel="stylesheet" media="all" type="text/css" />--%>
    <div class="booking-details">
         <div id="divmessage" runat="server" class="error" >
                </div>
        <table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#ecf2e3">
            <tr>
                <td>
                    Ref No. :
                </td>
                <td colspan="3">
                    <asp:TextBox ID="txtRefNo" runat="server" class="textfield2" MaxLength="15"></asp:TextBox>
                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtRefNo"
                        FilterType="Numbers" runat="server">
                    </asp:FilteredTextBoxExtender>
                </td>
                <td>
                    Client Name :
                </td>
                <td colspan="4">
                    <asp:TextBox ID="txtClientName" runat="server" class="textfield2"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Arrival Date :
                </td>
                <td valign="middle">
                    <asp:TextBox ID="txtArrivaldate" runat="server"   class="textfield2"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;    <input type="image" src="../images/date-icon.png" id="calarr"/></td> <td align="left">
                    <asp:CalendarExtender ID="CalendarExtender3" runat="server" PopupButtonID="calarr" TargetControlID="txtArrivaldate"
                        Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                </td>
                <td align="right">    
                    From :&nbsp;&nbsp;&nbsp;
                </td>
                <td align="center">
                    <asp:TextBox ID="txtFromdate" runat="server" CssClass="textfield3"></asp:TextBox>
                   &nbsp;&nbsp;&nbsp;            <input type="image" src="../images/date-icon.png" id="calFrom"/></td>      <td>
                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFromdate" PopupButtonID="calFrom"
                        Format="dd/MM/yyyy">
                    </asp:CalendarExtender>                    								
                </td>
                <td>
                    To :
                </td>
                <td align="left" valign="bottom">
                    <asp:TextBox ID="txtTodate" runat="server"  CssClass="textfield3"></asp:TextBox>
                       &nbsp;&nbsp;&nbsp;        <input type="image" src="../images/date-icon.png" id="calTo"/>
                           <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTodate" PopupButtonID="calTo"     Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                
                </td>   <td>
         
                </td>
            </tr>
            <tr align="center">
            <td colspan="2"><br /></td>
                <td colspan="4" align="left">
                    <div class="blue-button" align="center">
                        <asp:LinkButton ID="lbtSearch" runat="server" CssClass="link" ForeColor="White" OnClick="lbtSearch_Click">Search</asp:LinkButton></div>
                         <div class="blue-button" align="center">
                        <asp:LinkButton ID="lnkclear" runat="server" CssClass="link" ForeColor="White" 
                                 onclick="lnkclear_Click">Clear</asp:LinkButton></div>
               
                </td>
              
            </tr>
        </table>
    </div>
    <div class="contract-list" id="divAlphabeticPaging" runat="server">
        <div class="contract-list-right" style="width: 676px;">
            <ul runat="server" id="AlphaList">
                        <li id="Li1" runat="server"><a href="#" class="select" runat="server" id="all" onserverclick="PageChange">all</a></li>
                        <li id="Li2" runat="server"><a href="#" runat="server" id="a" onserverclick="PageChange">a</a></li>
                        <li id="Li3" runat="server"><a href="#" runat="server" id="b" onserverclick="PageChange">b</a></li>
                        <li id="Li4" runat="server"><a href="#" runat="server" id="c" onserverclick="PageChange">c</a></li>
                        <li id="Li5" runat="server"><a href="#" runat="server" id="d" onserverclick="PageChange">d</a></li>
                        <li id="Li6" runat="server"><a href="#" runat="server" id="e" onserverclick="PageChange">e</a></li>
                        <li id="Li7" runat="server"><a href="#" runat="server" id="f" onserverclick="PageChange">f</a></li>
                        <li id="Li8" runat="server"><a href="#" runat="server" id="g" onserverclick="PageChange">g</a></li>
                        <li id="Li9" runat="server"><a href="#" runat="server" id="h" onserverclick="PageChange">h</a></li>
                        <li id="Li10" runat="server"><a href="#" runat="server" id="i" onserverclick="PageChange">i</a></li>
                        <li id="Li11" runat="server"><a href="#" runat="server" id="j" onserverclick="PageChange">j</a></li>
                        <li id="Li12" runat="server"><a href="#" runat="server" id="k" onserverclick="PageChange">k</a></li>
                        <li id="Li13" runat="server"><a href="#" runat="server" id="l" onserverclick="PageChange">l</a></li>
                        <li id="Li14" runat="server"><a href="#" runat="server" id="m" onserverclick="PageChange">m</a></li>
                        <li id="Li15" runat="server"><a href="#" runat="server" id="n" onserverclick="PageChange">n</a></li>
                        <li id="Li16" runat="server"><a href="#" runat="server" id="o" onserverclick="PageChange">o</a></li>
                        <li id="Li17" runat="server"><a href="#" runat="server" id="p" onserverclick="PageChange">p</a></li>
                        <li id="Li18" runat="server"><a href="#" runat="server" id="q" onserverclick="PageChange">q</a></li>
                        <li id="Li19" runat="server"><a href="#" runat="server" id="r" onserverclick="PageChange">r</a></li>
                        <li id="Li20" runat="server"><a href="#" runat="server" id="s" onserverclick="PageChange">s</a></li>
                        <li id="Li21" runat="server"><a href="#" runat="server" id="t" onserverclick="PageChange">t</a></li>
                        <li id="Li22" runat="server"><a href="#" runat="server" id="u" onserverclick="PageChange">u</a></li>
                        <li id="Li23" runat="server"><a href="#" runat="server" id="v" onserverclick="PageChange">v</a></li>
                        <li id="Li24" runat="server"><a href="#" runat="server" id="w" onserverclick="PageChange">w</a></li>
                        <li id="Li25" runat="server"><a href="#" runat="server" id="x" onserverclick="PageChange">x</a></li>
                        <li id="Li26" runat="server"><a href="#" runat="server" id="y" onserverclick="PageChange">y</a></li>
                        <li id="Li27" runat="server"><a href="#" runat="server" id="z" onserverclick="PageChange">z</a></li>
                    </ul>
        </div>
    </div>
    <asp:UpdateProgress ID="uprog" runat="server" AssociatedUpdatePanelID="updTest">
        <ProgressTemplate>
            <div id="Loding_overlay" style="display: block;">
                <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
                Loading...
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="Loding_overlay" style="display: block;">
                <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
                Loading...
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel runat="server" ID="updTest">
        <ContentTemplate>
            <div id="divGrid">
                <asp:GridView ID="grdViewBooking" runat="server" Width="100%" AutoGenerateColumns="False"
                    RowStyle-HorizontalAlign="Center" RowStyle-VerticalAlign="Middle" CellPadding="4"
                    OnRowDataBound="grdViewBooking_RowDataBound" EmptyDataText="No record Found!"
                    EmptyDataRowStyle-Font-Bold="true" EmptyDataRowStyle-HorizontalAlign="Center"
                    EditRowStyle-VerticalAlign="Top" OnPageIndexChanging="grdViewBooking_PageIndexChanging"
                    AllowPaging="true" GridLines="None" CssClass="cofig" BackColor="#ffffff" 
                    PageSize="10" onrowcreated="grdViewBooking_RowCreated">
                    <Columns>
                        <asp:TemplateField HeaderText="Ref No">
                            <ItemTemplate>
                                <asp:LinkButton ID="lblRefNo" runat="server" ToolTip='<%# Eval("HotelID") %>' Text='<%# Eval("Id") %>'  CommandArgument='<%# ((GridViewRow) Container).RowIndex %>' 
                                    ForeColor="#339966" OnClick="lblRefNo_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meeting Room">
                            <ItemTemplate>
                                <asp:Label ID="lblmeetingroom" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="200px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Booking Date">
                            <ItemTemplate>
                                <asp:Label ID="lblBookingDt" runat="server" ToolTip='<%# Eval("BookingDate") %>'
                                    Text='<%# Eval("BookingDate", "{0:dd/MM/yy}") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="200px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Channel">
                            <ItemTemplate>
                                <asp:Label ID="lblCompany" runat="server" Text='<%# Eval("Usertype ") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="200px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblType" runat="server" Text='<%# Eval("TypeUser ") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="200px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Contact Name">
                            <ItemTemplate>
                              <asp:Label ID="lblConctName" runat="server"   Text='<%# Eval("Contact ") %>'></asp:Label>
                                <%--      <asp:LinkButton ID="lblConctName"  runat="server"  
                            Text='<%# Eval("Contact ") %>' OnClick="lblConctName_Click"></asp:LinkButton>--%>
                            </ItemTemplate>
                            <ItemStyle Width="200px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Arrival Date">
                            <ItemTemplate>
                                <asp:Label ID="lblArrivalDt" runat="server" Text='<%# String.Format("{0:dd/MM/yyyy}", Eval("ArrivalDate"))%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="200px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Depature Date">
                            <ItemTemplate>
                                <asp:Label ID="lblDepartureDt" runat="server"   Text='<%# String.Format("{0:dd/MM/yyyy}",Eval("DepartureDate"))%>'  ToolTip='<%# Eval("DepartureDate")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="200px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Expiry Date">
                            <ItemTemplate>
                                <asp:Label ID="lblExpiryDt" runat="server"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="200px" />
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Total (incl VAT)">
                                <ItemTemplate>
                                    <asp:Label ID="lblFinalTotal" runat="server"  Text='<%# String.Format("{0:###,###.###}",Eval("Finaltotalprice")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total (excl VAT)">
                                <ItemTemplate>
                                    <asp:Label ID="lblFinalTotalRev" runat="server"  Text='<%# String.Format("{0:###,###.###}",Eval("ConfirmRevenueAmount")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                                <asp:Label ID="lblstatus" runat="server" ToolTip='<%# Eval("requeststatus") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Commission">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnchkcommision" runat="server" Text="Change Status" OnClientClick=" alert ('You are about to validate your commission.Please have the invoice ready in order to accept / adjust the commissionable revenue.')" ForeColor="#339966"  ToolTip='<%# Eval("IsComissionDone") %>'
                                    CommandArgument='<%# Eval("Id") %>' OnClick="btnchkcommision_Click"></asp:LinkButton>
                                     <asp:Image ID="imgCheck" runat="server" ImageUrl="~/Images/select-check-bx.png" Visible="false"/>
                                    
                                    </td>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" BackColor="White" />
                    <PagerTemplate>
                        <table cellpadding="0" border="0" align="right">
                            <tr>
                                <td style="vertical-align=middle; height: 22px;">
                                    <%--<asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>--%>
                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                </td>
                            </tr>
                        </table>
                    </PagerTemplate>
                    <%-- <EditRowStyle BackColor="#7C6F57" />
                            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />--%>
                    <HeaderStyle BackColor="#CCD8D8" />
                    <PagerSettings Mode="NumericFirstLast" Position="Top" />
                    <PagerStyle BackColor="White" ForeColor="White" HorizontalAlign="Right" Font-Overline="False"
                        Font-Strikeout="False" />
                    <RowStyle BackColor="#E3F0F1" />
                    <AlternatingRowStyle BackColor="#ffffff" />
                    <%--<SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#F8FAFA" />
                            <SortedAscendingHeaderStyle BackColor="#246B61" />
                            <SortedDescendingCellStyle BackColor="#D4DFE1" />
                            <SortedDescendingHeaderStyle BackColor="#15524A" />--%>
                </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="divbookingdetails" align="left" runat="server">
                <table width="100%" bgcolor="#ffffff" cellspacing="1" cellpadding="8">
                    <tr>
                        <td>
                            <h1>
                                <asp:Label ID="lblbookingdetails" runat="server"></asp:Label>
                            </h1>
                        </td>
                    </tr>
                </table>
                <br />
                <div id="Divdetails" runat="server">
                    <uc1:bookingdetails id="bookingDetails" runat="server"></uc1:bookingdetails>
                </div>
            </div>
            <br />
              
        </ContentTemplate>
        <%-- <Triggers>
            <asp:PostBackTrigger ControlID="lnkSavePDF"  />
            </Triggers>--%>
    </asp:UpdatePanel>
    <asp:LinkButton ID="lnkbtn" OnClientClick="javascript:document.getElementById('contentbody_pnlchkCommission').style.display='block';"
        runat="server"></asp:LinkButton>
    <asp:ModalPopupExtender ID="modalcheckcomm" TargetControlID="lnkbtn" BackgroundCssClass="modalBackground"
        PopupControlID="pnlchkCommission" runat="server">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlchkCommission" BorderColor="#999999" Width="757px" BorderWidth="5"
        Style="display: none; padding-top: 7px;" runat="server" BackColor="White">
        <div class="popup-mid-inner-body" align="center">
            Revenue validation for commission
        </div>
        <div id="div1" runat="server">
        </div>
        <div >
            <asp:UpdatePanel ID="upcheckcomm" runat="server">
                <ContentTemplate>
                    <table width="100%" cellspacing="0" cellpadding="3" align="center" style="background-color:#dddddd">
                        <tr>
                            <td width="30%">
                                <table>
                                    <tr>
                                        <td align="right">
                                            Revenue booked :
                                        </td>
                                        <td>
                                            <asp:Label ID="lblrevenue" runat="server" Text="6.820"></asp:Label>
                                            &nbsp;Euro
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <%--<asp:CheckBox ID="chkconfirmrevenue" Text="Confirm the revenue" onclick="callme()" runat="server" />--%>
                                            <asp:CheckBox ID="chkconfirmrevenue"  runat="server" CssClass="NoClassApply" onclick="callme()"
                                        Text="Confirm the revenue"  />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td align="center" colspan="2">
                                            <b>Adjust Revenue (if needed)</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            Insert Netto (VAT excluded) :
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtrealvalue" runat="server" Width="120px" MaxLength="7"></asp:TextBox><asp:FilteredTextBoxExtender
                                                ID="FilteredTextBoxExtenderrealvalue" runat="server" TargetControlID="txtrealvalue"
                                                ValidChars="." FilterMode="ValidChars" FilterType="Numbers,Custom">
                                            </asp:FilteredTextBoxExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            Reason :
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlreason" runat="server" CssClass="NoClassApply">
                                                <asp:ListItem>Cancelled and agreed between hotel and client</asp:ListItem>
                                                <asp:ListItem>Changes in Duration</asp:ListItem>
                                                <asp:ListItem>Increased number of participants</asp:ListItem>
                                                <asp:ListItem>No-Show</asp:ListItem>
                                                <asp:ListItem>Reduced number of participants</asp:ListItem>
                                                <asp:ListItem>Variation on package price</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top">
                                            Supporting document :
                                        </td>
                                        <td>
                                            <asp:FileUpload ID="ulPlan" runat="server" />&nbsp;<br />*
                                             <span style="font-family: Arial; font-size: Smaller; color: Gray">The supporting document is mandatory to process commission</span>
                                            
<br />
                                            <span style="font-family: Arial; font-size: Smaller; color: Gray">eg: Invoice, File format: PDF/Word/Excel & max size 1MB. </span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
            <table width="100%">
            </table>
        </div>
        <div>
            <div class="popup-mid-inner-body2_i">
            </div>
            <div class="booking-details" style="width: 760px;">
                <ul>
                    <li class="value10">
                        <div class="col21" style="width: 752px;">
                            <div class="button_section">
                                <asp:LinkButton ID="btnSubmit" runat="server" Text="Save" CssClass="select" OnClick="btnSubmit_Click" />
                                <span>or</span>
                                <asp:LinkButton ID="btnCancel" runat="server" Text="Cancel"  OnClick="btnCancel_Click" />
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </asp:Panel>
    <script language="javascript" type="text/javascript">
                        jQuery(document).ready(function () {
                            <% if (ViewState["SearchAlpha"] != null) {%>
                                jQuery('#<%= AlphaList.ClientID %> li a').removeClass('select');
                                jQuery('#contentbody_<%= ViewState["SearchAlpha"]%>').addClass('select');
                            <% }%>
                        });
                    </script>
    <script language="javascript" type="text/javascript">
                           jQuery(document).ready(function () {
                               jQuery("#<%= txtFromdate.ClientID %>").attr("disabled", true);
                               jQuery("#<%= txtTodate.ClientID %>").attr("disabled", true);
                               jQuery("#<%= txtArrivaldate.ClientID %>").attr("disabled", true);
                               jQuery("#<%= lbtSearch.ClientID %>").bind("click", function () {
                                   jQuery("#contentbody_txtFromdate").attr("disabled", false);
                                   jQuery("#contentbody_txtTodate").attr("disabled", false);
                                   jQuery("#contentbody_txtArrivaldate").attr("disabled", false);
                               });
                           });
     
                    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#<%= lbtSearch.ClientID %>").bind("click", function () {
                var fromdate = jQuery("#<%= txtFromdate.ClientID %>").val();
                var todate = jQuery("#<%= txtTodate.ClientID %>").val();
                var todayArr = todate.split('/');
                var formdayArr = fromdate.split('/');
                var fromdatecheck = new Date();
                var todatecheck = new Date();
                fromdatecheck.setFullYear(parseInt("20" + formdayArr[2]), (parseInt(formdayArr[1]) - 1), formdayArr[0]);
                todatecheck.setFullYear(parseInt("20" + todayArr[2]), (parseInt(todayArr[1]) - 1), todayArr[0]);
                if (fromdatecheck > todatecheck) {
                    jQuery(".error").show();
                    jQuery(".error").html("From date must be less than To date.");
                    return false;
                }
                jQuery("#Loding_overlay").show();
            });
        });
    </script>

    <script language="javascript" type="text/javascript">
        function callme() {
            if (jQuery("#contentbody_chkconfirmrevenue").is(":checked")) {

                jQuery("#contentbody_txtrealvalue").val('');
                jQuery("#contentbody_txtrealvalue").attr("disabled", true);
                jQuery("#contentbody_ddlreason").attr("disabled", true);
//                jQuery("#contentbody_ulPlan").val('');
//                jQuery("#contentbody_ulPlan").attr("disabled", true);

            }
            else {

                jQuery("#contentbody_txtrealvalue").val('');
                jQuery("#contentbody_txtrealvalue").attr("disabled", false);
                jQuery("#contentbody_ddlreason").attr("disabled", false);
//                jQuery("#contentbody_ulPlan").val('');
//                jQuery("#contentbody_ulPlan").attr("disabled", false);
            }
        }

     
  );

    </script>
    
</asp:Content>
