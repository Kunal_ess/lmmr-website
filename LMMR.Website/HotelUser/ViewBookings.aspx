﻿<%@ Page Title="Hotel User - Booking" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master"
    AutoEventWireup="true" CodeFile="ViewBookings.aspx.cs" Inherits="HotelUser_ViewBookings"
    EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/UserControl/HotelUser/BookingDetails.ascx" TagName="BookingDetails" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" runat="Server">
    <h1>
        <asp:Label ID="lblheadertext" runat="server" Text="New Booking For:"></asp:Label></h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" runat="Server">
    <style type="text/css">
        .modalBackground
        {
            background-color: #CCCCFF;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }
    </style>
     <link href="../css/stylew423.css" rel="stylesheet" type="text/css" />
    <%--    <link href="../css/style.css" rel="stylesheet" type="text/css" />
    <link href="../css/style-front.css" rel="stylesheet" media="all" type="text/css" />--%>
    <style type="text/css">
        .LinkPaging
        {
            width: 20;
            background-color: White;
            border: Solid 1 Black;
            text-align: center;
            margin-left: 8;
        }
    </style>
    <asp:UpdateProgress ID="uprog" runat="server" AssociatedUpdatePanelID="updTest">
        <ProgressTemplate>
            <div id="Loding_overlay" style="display: block;">
                <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
                Loading...
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="Loding_overlay" style="display: block;">
                <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
                Loading...
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="booking-details" class="booking-details">
        <asp:UpdatePanel runat="server" ID="updTest">
            <ContentTemplate>
                <asp:HiddenField ID="hdnSelectedRowID" runat="server" Value="0" />
                <div id="divGrid">
                    <asp:GridView ID="grdViewBooking" runat="server" Width="100%" AutoGenerateColumns="False"
                        RowStyle-HorizontalAlign="Center" RowStyle-VerticalAlign="Middle" CellPadding="6"
                        OnRowDataBound="grdViewBooking_RowDataBound" EmptyDataText="No record Found!"
                        EmptyDataRowStyle-Font-Bold="true" EmptyDataRowStyle-HorizontalAlign="Center"
                        EditRowStyle-VerticalAlign="Top" OnPageIndexChanging="grdViewBooking_PageIndexChanging"
                        AllowPaging="true" GridLines="None" PageSize="10" CssClass="cofig" BackColor="#ffffff"
                        OnRowCreated="grdViewBooking_RowCreated">
                        <Columns>
                            <asp:TemplateField HeaderText="Ref No">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lblRefNo" runat="server" ToolTip='<%# Eval("HotelID") %>' CommandName="high"
                                        Text='<%# Eval("Id") %>' CommandArgument='<%# ((GridViewRow) Container).RowIndex %>'
                                        ForeColor="#339966" OnClick="lblRefNo_Click"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Meeting Room">
                                <ItemTemplate>
                                    <asp:Label ID="lblmeetingroom" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Booking Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblBookingDt" runat="server" Text='<%# Eval("BookingDate","{0:dd/MM/yy}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Channel">
                                <ItemTemplate>
                                    <asp:Label ID="lblCompany" runat="server" Text='<%# Eval("Usertype ") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Contact Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblConctName" runat="server" Text='<%# Eval("Contact ") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Arrival Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblArrivalDt" runat="server"  Text='<%# String.Format("{0:dd/MM/yyyy}",Eval("ArrivalDate"))%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Departure Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblDepartureDt" runat="server"  Text='<%#  String.Format("{0:dd/MM/yyyy}", Eval("DepartureDate"))%>' ToolTip='<%# Eval("DepartureDate")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                                                       <asp:TemplateField HeaderText="Total (incl VAT)">
                                <ItemTemplate>
                                    <asp:Label ID="lblFinalTotal" runat="server"  Text='<%# String.Format("{0:###,###.###}",Eval("Finaltotalprice")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total (excl VAT)">
                                <ItemTemplate>
                                    <asp:Label ID="lblFinalTotalRev" runat="server"  Text='<%# String.Format("{0:###,###.###}",Eval("ConfirmRevenueAmount")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Commission">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnchkcommision" runat="server" ForeColor="#339966" OnClientClick="alert ('You are about to validate your commission.Please have the invoice ready in order to accept / adjust the commissionable revenue.')"  CommandArgument='<%# Eval("Id") %>' ToolTip='<%# Eval("IsComissionDone") %>'
                                        OnClick="btnchkcommision_Click"></asp:LinkButton>
                                    <asp:Image ID="imgCheck" runat="server" ImageUrl="~/Images/select-check-bx.png" Visible="false"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle HorizontalAlign="Right" BackColor="White" />
                        <PagerTemplate>
                            <table cellpadding="0" border="0" align="right">
                                <tr>
                                    <td style="vertical-align=middle; height: 220;">
                                        <%--<asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>--%>
                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </PagerTemplate>
                        <HeaderStyle BackColor="#CCD8D8" />
                        <PagerSettings Mode="NumericFirstLast" Position="Top" />
                        <PagerStyle BackColor="White" ForeColor="White" HorizontalAlign="Right" Font-Overline="False"
                            Font-Strikeout="False" />
                        <RowStyle BackColor="#E3F0F1" />
                        <SelectedRowStyle BackColor="Yellow" />
                        <AlternatingRowStyle BackColor="#ffffff" />
                    </asp:GridView>
                </div>
                <div class="booking-details" id="divprintpdfllink" runat="server">
                    <ul>
                        <li class="value3">
                            <div class="col9">
                                <img src="../Images/print.png" />&nbsp; <a id="uiLinkButtonPrintGrid" style="cursor: pointer;"
                                    onclick="javascript:Button1_onclick('divGrid');">Print</a> &bull;
                                <img src="../Images/pdf.png" />&nbsp;<asp:LinkButton ID="uiLinkButtonSaveAsPdfGrid"
                                    runat="server" OnClick="uiLinkButtonSaveAsPdfGrid_Click">Save as PDF</asp:LinkButton>
                            </div>
                        </li>
                    </ul>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="uiLinkButtonSaveAsPdfGrid" />
            </Triggers>
        </asp:UpdatePanel>
        <br />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div id="divbookingdetails" align="left" runat="server">
                    <br />
                    <h1 class="new">
                        Booking details</h1>
                    <div class="booking-details" id="divprint" runat="server">
                        <ul>
                            <li class="value3">
                                <div class="col9">
                                    <img src="../Images/print.png" />&nbsp; <a id="ADetails" style="cursor: pointer;"
                                        onclick="javascript:Button1_onclick('<%= Divdetails.ClientID%>');">Print</a>
                                    &bull;
                                    <img src="../Images/pdf.png" />&nbsp;
                                    <asp:LinkButton ID="lnkSavePDF" runat="server" OnClick="lnkSavePDF_Click">Save as PDF</asp:LinkButton>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div id="Divdetails" runat="server" class="meinnewbody">



                        <uc1:BookingDetails ID="bookingDetails" runat="server"></uc1:BookingDetails>
                    </div>
                    <br />
                    <div class="button-center">
                        <div class="blue-button" id="divlinkpro" runat="server" align="center">
                            <asp:LinkButton ID="Lnkmovetoprocessed" runat="server" CssClass="link" ForeColor="White"
                                Font-Underline="false" OnClick="Lnkmovetoprocessed_Click">Process</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <br />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lnkSavePDF" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <asp:LinkButton ID="lnkbtn" OnClientClick="javascript:document.getElementById('contentbody_pnlchkCommission').style.display='block';"
        runat="server"></asp:LinkButton>
    <asp:ModalPopupExtender ID="modalcheckcomm" TargetControlID="lnkbtn" BackgroundCssClass="modalBackground"
        PopupControlID="pnlchkCommission" runat="server">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlchkCommission" BorderColor="#999999" Width="757px" BorderWidth="5"
        Style="display: none; padding-top: 7px;" runat="server" BackColor="White">
        <div class="popup-mid-inner-body" align="center">
            Revenue validation for commission
        </div>
        <div id="divmessage" runat="server">
        </div>
        <div >
            <asp:UpdatePanel ID="upcheckcomm" runat="server">
                <ContentTemplate>
                    <table width="100%" cellspacing="0" cellpadding="3" align="center" style="background-color:#dddddd">
                        <tr>
                            <td width="30%">
                                <table>
                                    <tr>
                                        <td align="right">
                                            Revenue booked :
                                        </td>
                                        <td>
                                            <asp:Label ID="lblrevenue" runat="server" Text="6.820"></asp:Label>
                                            &nbsp;Euro
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <%--<asp:CheckBox ID="chkconfirmrevenue" Text="Confirm the revenue" onclick="callme()" runat="server" />--%>
                                            <asp:CheckBox ID="chkconfirmrevenue"  runat="server" CssClass="NoClassApply" onclick="callme()"
                                        Text="Confirm the revenue"  />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td align="center" colspan="2">
                                            <b>Adjust Revenue (if needed)</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            Insert Netto (VAT excluded) :
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtrealvalue" runat="server" Width="120px" MaxLength="7"></asp:TextBox><asp:FilteredTextBoxExtender
                                                ID="FilteredTextBoxExtenderrealvalue" runat="server" TargetControlID="txtrealvalue"
                                                ValidChars="." FilterMode="ValidChars" FilterType="Numbers,Custom">
                                            </asp:FilteredTextBoxExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            Reason :
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlreason" runat="server" CssClass="NoClassApply">
                                                <asp:ListItem>Cancelled and agreed between hotel and client</asp:ListItem>
                                                <asp:ListItem>Changes in Duration</asp:ListItem>
                                                <asp:ListItem>Increased number of participants</asp:ListItem>
                                                <asp:ListItem>No-Show</asp:ListItem>
                                                <asp:ListItem>Reduced number of participants</asp:ListItem>
                                                <asp:ListItem>Variation on package price</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top">
                                            Supporting document :
                                        </td>
                                        <td>
                                            <asp:FileUpload ID="ulPlan" runat="server" />&nbsp;<br />*
                                             <span style="font-family: Arial; font-size: Smaller; color: Gray">The supporting document is mandatory to process commission</span>
                                            
<br />
                                            <span style="font-family: Arial; font-size: Smaller; color: Gray">eg: Invoice, File format: PDF/Word/Excel & max size 1MB. </span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
            <table width="100%">
            </table>
        </div>
        <div>
            <div class="popup-mid-inner-body2_i">
            </div>
            <div class="booking-details" style="width: 760px;">
                <ul>
                    <li class="value10">
                        <div class="col21" style="width: 752px;">
                            <div class="button_section">
                                <asp:LinkButton ID="btnSubmit" runat="server" Text="Save" CssClass="select" OnClick="btnSubmit_Click" />
                                <span>or</span>
                                <asp:LinkButton ID="btnCancel" runat="server" Text="Cancel"  OnClick="btnCancel_Click" />
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </asp:Panel>
    <script language="javascript" type="text/javascript">
        function callme() {
            if (jQuery("#contentbody_chkconfirmrevenue").is(":checked")) {

                jQuery("#contentbody_txtrealvalue").val('');
                jQuery("#contentbody_txtrealvalue").attr("disabled", true);
                jQuery("#contentbody_ddlreason").attr("disabled", true);
               // jQuery("#contentbody_ulPlan").val('');
                //jQuery("#contentbody_ulPlan").attr("disabled", true);

            }
            else {

                jQuery("#contentbody_txtrealvalue").val('');
                jQuery("#contentbody_txtrealvalue").attr("disabled", false);
                jQuery("#contentbody_ddlreason").attr("disabled", false);
               // jQuery("#contentbody_ulPlan").val('');
                //jQuery("#contentbody_ulPlan").attr("disabled", false);
            }
        }

        function Button1_onclick(strid) {

            var prtContent = document.getElementById(strid);
            var WinPrint = window.open('', '', 'letf=0,top=0,width=800,height=800,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();


        }


        function ChangeRowColor(rowID) {
            jQuery('#' + rowID).css("background-color", "#FF9");
            jQuery('#<%=hdnSelectedRowID.ClientID %>').val(rowID);
        }
  

    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            if (jQuery("#contentbody_chkconfirmrevenue").is(":checked")) {

                jQuery("#contentbody_txtrealvalue").val('');
                jQuery("#contentbody_txtrealvalue").attr("disabled", true);
                jQuery("#contentbody_ddlreason").attr("disabled", true);
                //                jQuery("#contentbody_ulPlan").val('');
                //                jQuery("#contentbody_ulPlan").attr("disabled", true);

            }
            else {

                jQuery("#contentbody_txtrealvalue").val('');
                jQuery("#contentbody_txtrealvalue").attr("disabled", false);
                jQuery("#contentbody_ddlreason").attr("disabled", false);
                //                jQuery("#contentbody_ulPlan").val('');
                //                jQuery("#contentbody_ulPlan").attr("disabled", false);
            }
        }
        );
     
  

    </script>
</asp:Content>
