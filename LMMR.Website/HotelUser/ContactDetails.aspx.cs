﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using log4net;
using log4net.Config;
#endregion

public partial class HotelUser_ContactDetails : System.Web.UI.Page
{
    #region variable declaration
    ContactDetail objContactdtl = new ContactDetail();
    int hotelID = 0;
    WizardLinkSettingManager Objwizard = new WizardLinkSettingManager();
    DashboardManager Objdash = new DashboardManager();
    HotelManager objHotelManager = new HotelManager();
    HotelInfo ObjHotelinfo = new HotelInfo();
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_ContactDetails));
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl + "/";
        }
    }
    #endregion    

    #region Additional Methods
    /// <summary>
    /// For check go online for this hotel
    /// </summary>
    public void Getgoonline()
    {
        if (ObjHotelinfo.GetHotelGoOnline(hotelID).Count > 0)
        {            
            divNext.Visible = false;
            ViewState["Count"] = ObjHotelinfo.GetHotelGoOnline(hotelID).Count;            
        }        
    }

    /// <summary>
    /// Check whether hotel user has inserted or not..If inserted then bind all fields
    /// </summary>    
    public void ContactDetailsBind()
    {
        try
        {
            if (objContactdtl.GetContactDetails(hotelID, "PrimaryContact1").Count > 0)
            {
                NewPC1.Visible = false;
                ViewPC1.Visible = true;
                dlistPrimaryContact1.DataSource = objContactdtl.GetContactDetails(hotelID, "PrimaryContact1");
                dlistPrimaryContact1.DataBind();
            }
            else
            {
                ViewPC1.Visible=false;               
            }
            if (objContactdtl.GetContactDetails(hotelID, "PrimaryContact2").Count > 0)
            {
                NewPC2.Visible = false;
                ViewPc2.Visible = true;
                dlistPrimaryContact2.DataSource = objContactdtl.GetContactDetails(hotelID, "PrimaryContact2");
                dlistPrimaryContact2.DataBind();
            }
            else
            {
                ViewPc2.Visible = false;
            }
            if (objContactdtl.GetContactDetails(hotelID, "SecondaryContact1").Count > 0)
            {
                NewSc1.Visible = false;
                divWeekdaySec.Visible = true;
                lnbSecCon1.Visible = false;
                ViewSc1.Visible = true;
                dlistSecondaryContact1.DataSource = objContactdtl.GetContactDetails(hotelID, "SecondaryContact1");
                dlistSecondaryContact1.DataBind();
            }
            if (objContactdtl.GetContactDetails(hotelID, "SecondaryContact2").Count > 0)
            {
                NewSc2.Visible = false;
                divWeekendSec.Visible = true;
                lnbSecCon2.Visible = false;
                ViewSc2.Visible = true;
                dlistSecondaryContact2.DataSource = objContactdtl.GetContactDetails(hotelID, "SecondaryContact2");
                dlistSecondaryContact2.DataBind();
            }

            if (objContactdtl.GetHotelContactByHotelid(hotelID).Count > 0)
            {
                divNext.Visible = true;                
                clickhereweekday.Visible = false;
                clickhereweekend.Visible = false;
            }
            else
            {                
                clickhereweekday.Visible = true;
                clickhereweekend.Visible = true;
            }

        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region PageLoad
    /// <summary>
    /// On page load we manage all the containts and Apply paging if there is a post back.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Session for Hotelname
            if (Session["CurrentHotelID"] == null)
            {
                Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                Session.Abandon();
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "login/english");
                }
            }
            hotelID = Convert.ToInt32(Session["CurrentHotelID"]);
            //Session for Hotelnames        

            if (!IsPostBack)
            {
                //By default to hide div messages
                divmessage.Style.Add("display", "none");
                divmessage.Attributes.Add("class", "error");

                Session["LinkID"] = "lnkPropertyLevel";

                #region Leftmenu
            HyperLink lnk = (HyperLink)this.Master.FindControl("lnkConferenceInfo");
            lnk.CssClass = "";
            lnk = (HyperLink)this.Master.FindControl("lnkContactDetails");
            lnk.CssClass = "selected";
            #endregion

                //Displayed all contacts details per hotel id
                ContactDetailsBind();

                //Display hotel name on top of the page
                lblHotel.Text = objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Name;

                //Check Goonline check for Hotel Id
                Getgoonline();
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region Insert Contacts
    /// <summary>
    /// Function is user for Insert/update contact details into database for all types
    /// </summary>
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (objContactdtl.GetContactDetails(hotelID, "PrimaryContact1").Count > 0)
            {
                if (hidPc1.Value != "")
                {
                    HotelContact objCont = objContactdtl.GetContractByID(Convert.ToInt32(hidPc1.Value));                    
                    objCont.HotelId = hotelID;
                    objCont.ContactType = "PrimaryContact1";
                    objCont.UserType = "WeekDay";
                    objCont.JobTitle = txtJobTitlePc1.Text.Trim().Replace("'", "");
                    objCont.FirstName = txtFnamePc1.Text.Trim().Replace("'", "");
                    objCont.LastName = txtLnamePc1.Text.Trim().Replace("'", "");
                    objCont.Phone = txtPhonePc1.Text.Trim().Replace("'", "");
                    objCont.Ext = drpExtPhone1.SelectedItem.Text.Replace("'", "");
                    objCont.Email = txtEmailPc1.Text.Trim().Replace("'", "");
                    objContactdtl.UpdateContactDetail(ref objCont);
                    ViewPC1.Visible = true;
                    NewPC1.Visible = false;
                }
            }
            else
            {
                if (txtJobTitlePc1.Text != "")
                {
                    HotelContact objCont = new HotelContact();
                    objCont.HotelId = hotelID;
                    objCont.ContactType = "PrimaryContact1";
                    objCont.UserType = "WeekDay";
                    objCont.JobTitle = txtJobTitlePc1.Text.Trim().Replace("'", "");
                    objCont.FirstName = txtFnamePc1.Text.Trim().Replace("'", "");
                    objCont.LastName = txtLnamePc1.Text.Trim().Replace("'", "");
                    objCont.Phone = txtPhonePc1.Text.Trim().Replace("'", "");
                    objCont.Ext = drpExtPhone1.SelectedItem.Text.Replace("'", "");
                    objCont.Email = txtEmailPc1.Text.Trim().Replace("'", "");
                    objContactdtl.AddNewContactDetail(objCont);
                }
            }


            if (objContactdtl.GetContactDetails(hotelID, "PrimaryContact2").Count > 0)
            {
                if (hidPc2.Value != "")
                {
                    HotelContact objCont = new HotelContact();
                    objCont = objContactdtl.GetContractByID(Convert.ToInt32(hidPc2.Value));                    
                    objCont.HotelId = hotelID;
                    objCont.ContactType = "PrimaryContact2";
                    objCont.UserType = "WeekEnd";
                    objCont.JobTitle = txtJobTitlePc2.Text.Trim().Replace("'", "");
                    objCont.FirstName = txtFnamePc2.Text.Trim().Replace("'", "");
                    objCont.LastName = txtLnamePc2.Text.Trim().Replace("'", "");
                    objCont.Phone = txtPhonePc2.Text.Trim().Replace("'", "");
                    objCont.Ext = drpExtPhonePc2.SelectedItem.Text.Replace("'", "");
                    objCont.Email = txtEmailPc2.Text.Trim().Replace("'", "");
                    objContactdtl.UpdateContactDetail(ref objCont);
                    ViewPc2.Visible = true;
                    NewPC2.Visible = false;
                }
            }
            else
            {
                if (txtJobTitlePc2.Text != "")
                {
                    HotelContact objCont = new HotelContact();
                    objCont.HotelId = hotelID;
                    objCont.ContactType = "PrimaryContact2";
                    objCont.UserType = "WeekEnd";
                    objCont.JobTitle = txtJobTitlePc2.Text.Trim().Replace("'", "");
                    objCont.FirstName = txtFnamePc2.Text.Trim().Replace("'", "");
                    objCont.LastName = txtLnamePc2.Text.Trim().Replace("'", "");
                    objCont.Phone = txtPhonePc2.Text.Trim().Replace("'", "");
                    objCont.Ext = drpExtPhonePc2.SelectedItem.Text.Replace("'", "");
                    objCont.Email = txtEmailPc2.Text.Trim().Replace("'", "");
                    objContactdtl.AddNewContactDetail(objCont);
                }
            }


            if (objContactdtl.GetContactDetails(hotelID, "SecondaryContact1").Count > 0)
            {
                if (hidSc1.Value != "")
                {
                    HotelContact objCont = new HotelContact();
                    objCont = objContactdtl.GetContractByID(Convert.ToInt32(hidSc1.Value));    
                    objCont.HotelId = hotelID;
                    objCont.ContactType = "SecondaryContact1";
                    objCont.UserType = "WeekDay";
                    objCont.JobTitle = txtJobTitleSc1.Text.Trim().Replace("'", "");
                    objCont.FirstName = txtFnameSc1.Text.Trim().Replace("'", "");
                    objCont.LastName = txtLnameSc1.Text.Trim().Replace("'", "");
                    objCont.Phone = txtPhoneSc1.Text.Trim().Replace("'", "");
                    objCont.Ext = drpExtPhoneSc1.SelectedItem.Text.Replace("'", "");
                    objCont.Email = txtEmailSc1.Text.Trim().Replace("'", "");
                    objContactdtl.UpdateContactDetail(ref objCont);
                    ViewSc1.Visible = true;
                    NewSc1.Visible = false;
                }
            }
            else
            {
                if (txtJobTitleSc1.Text != "")
                {
                    HotelContact objCont = new HotelContact();
                    objCont.HotelId = hotelID;
                    objCont.ContactType = "SecondaryContact1";
                    objCont.UserType = "WeekDay";
                    objCont.JobTitle = txtJobTitleSc1.Text.Trim().Replace("'", "");
                    objCont.FirstName = txtFnameSc1.Text.Trim().Replace("'", "");
                    objCont.LastName = txtLnameSc1.Text.Trim().Replace("'", "");
                    objCont.Phone = txtPhoneSc1.Text.Trim().Replace("'", "");
                    objCont.Ext = drpExtPhoneSc1.SelectedItem.Text.Replace("'", "");
                    objCont.Email = txtEmailSc1.Text.Trim().Replace("'", "");
                    objContactdtl.AddNewContactDetail(objCont);
                }
            }


            if (objContactdtl.GetContactDetails(hotelID, "SecondaryContact2").Count > 0)
            {
                if (hidSc2.Value != "")
                {
                    HotelContact objCont = new HotelContact();
                    objCont = objContactdtl.GetContractByID(Convert.ToInt32(hidSc2.Value));                      
                    objCont.HotelId = hotelID;
                    objCont.ContactType = "SecondaryContact2";
                    objCont.UserType = "WeekEnd";
                    objCont.JobTitle = txtJobTitleSc2.Text.Trim().Replace("'", "");
                    objCont.FirstName = txtFnameSc2.Text.Trim().Replace("'", "");
                    objCont.LastName = txtLnameSc2.Text.Trim().Replace("'", "");
                    objCont.Phone = txtPhoneSc2.Text.Trim().Replace("'", "");
                    objCont.Ext = drpExtPhoneSc2.SelectedItem.Text.Replace("'", "");
                    objCont.Email = txtEmailSc2.Text.Trim().Replace("'", "");
                    objContactdtl.UpdateContactDetail(ref objCont);
                    ViewSc2.Visible = true;
                    NewSc2.Visible = false;
                }
            }
            else
            {
                if (txtJobTitleSc2.Text != "")
                {
                    HotelContact objCont = new HotelContact();
                    objCont.HotelId = hotelID;
                    objCont.ContactType = "SecondaryContact2";
                    objCont.UserType = "WeekEnd";
                    objCont.JobTitle = txtJobTitleSc2.Text.Trim().Replace("'", "");
                    objCont.FirstName = txtFnameSc2.Text.Trim().Replace("'", "");
                    objCont.LastName = txtLnameSc2.Text.Trim().Replace("'", "");
                    objCont.Phone = txtPhoneSc2.Text.Trim().Replace("'", "");
                    objCont.Ext = drpExtPhoneSc2.SelectedItem.Text.Replace("'", "");
                    objCont.Email = txtEmailSc2.Text.Trim().Replace("'", "");
                    objContactdtl.AddNewContactDetail(objCont);
                }
            }
            ContactDetailsBind();
            Getgoonline();
            hidPc1.Value = "";
            hidPc2.Value = "";
            hidSc1.Value = "";
            hidSc2.Value = "";
            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "succesfuly");
            divmessage.InnerHtml = "Contact details updated successfully!";
            if (Convert.ToString(ViewState["Count"]) != "1")
            {
                divmessage.InnerHtml += "&nbsp; Please click on next button to move ahead.";
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region Get Data on Modify
    /// <summary>
    /// Function for fetch all details of primary contact1 after cliking on modify
    /// </summary>   
    protected void lnbPrimaryCnt2_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton myButton = sender as LinkButton;
            if (myButton != null)
            {
                int id = Convert.ToInt32(myButton.CommandArgument);
                ViewPc2.Visible = false;
                NewPC2.Visible = true;
                HotelContact hotcnt = objContactdtl.GetContractByID(id);
                txtJobTitlePc2.Text = hotcnt.JobTitle;
                txtFnamePc2.Text = hotcnt.FirstName;
                txtLnamePc2.Text = hotcnt.LastName;
                txtPhonePc2.Text = hotcnt.Phone;                
                drpExtPhonePc2.SelectedIndex = drpExtPhonePc2.Items.IndexOf(drpExtPhonePc2.Items.FindByText(hotcnt.Ext));
                txtEmailPc2.Text = hotcnt.Email;
                hidPc2.Value = id.ToString();
            }
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// Function for fetch all details of primary contact2 after cliking on modify
    /// </summary>   
    protected void lnbPrimaryCnt1_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton myButton = sender as LinkButton;
            if (myButton != null)
            {
                int id = Convert.ToInt32(myButton.CommandArgument);
                ViewPC1.Visible = false;
                NewPC1.Visible = true;
                clickhereweekday.Visible = true;
                HotelContact hotcnt = objContactdtl.GetContractByID(id);
                txtJobTitlePc1.Text = hotcnt.JobTitle;
                txtFnamePc1.Text = hotcnt.FirstName;
                txtLnamePc1.Text = hotcnt.LastName;
                txtPhonePc1.Text = hotcnt.Phone;                
                drpExtPhone1.SelectedIndex = drpExtPhone1.Items.IndexOf(drpExtPhone1.Items.FindByText(hotcnt.Ext));
                txtEmailPc1.Text = hotcnt.Email;
                hidPc1.Value = id.ToString();
            }
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// Function for fetch all details of secondary contact1 after cliking on modify
    /// </summary>   
    protected void lnbSecondaryCnt1_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton myButton = sender as LinkButton;
            if (myButton != null)
            {
                int id = Convert.ToInt32(myButton.CommandArgument);
                ViewSc1.Visible = false;
                NewSc1.Visible = true;
                clickhereweekend.Visible = true;
                HotelContact hotcnt = objContactdtl.GetContractByID(id);
                txtJobTitleSc1.Text = hotcnt.JobTitle;
                txtFnameSc1.Text = hotcnt.FirstName;
                txtLnameSc1.Text = hotcnt.LastName;
                txtPhoneSc1.Text = hotcnt.Phone;                
                drpExtPhoneSc1.SelectedIndex = drpExtPhoneSc1.Items.IndexOf(drpExtPhoneSc1.Items.FindByText(hotcnt.Ext));
                txtEmailSc1.Text = hotcnt.Email;
                hidSc1.Value = id.ToString();
            }
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// Function for fetch all details of secondary contact2 after cliking on modify
    /// </summary>   
    protected void lnbSecondaryCnt2_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton myButton = sender as LinkButton;
            if (myButton != null)
            {
                int id = Convert.ToInt32(myButton.CommandArgument);
                ViewSc2.Visible = false;
                NewSc2.Visible = true;
                HotelContact hotcnt = objContactdtl.GetContractByID(id);
                txtJobTitleSc2.Text = hotcnt.JobTitle;
                txtFnameSc2.Text = hotcnt.FirstName;
                txtLnameSc2.Text = hotcnt.LastName;
                txtPhoneSc2.Text = hotcnt.Phone;                
                drpExtPhoneSc2.SelectedIndex = drpExtPhoneSc2.Items.IndexOf(drpExtPhoneSc2.Items.FindByText(hotcnt.Ext));
                txtEmailSc2.Text = hotcnt.Email;
                hidSc2.Value = id.ToString();
            }
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Copy One Contact to another
    /// <summary>
    /// Copy data from primary contact1 to primary contact2
    /// </summary>
    protected void lnbCopyPc_Click(object sender, EventArgs e)
    {
        //Copy all data from Primary Contact1 to Primary Contact2
        try
        {
            txtJobTitlePc2.Text = txtJobTitlePc1.Text;
            txtFnamePc2.Text = txtFnamePc1.Text;
            txtLnamePc2.Text = txtLnamePc1.Text;
            drpExtPhonePc2.SelectedValue= drpExtPhone1.SelectedValue;
            txtPhonePc2.Text = txtPhonePc1.Text;
            txtEmailPc2.Text = txtEmailPc1.Text;

            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");

            ViewPc2.Visible = false;
            NewPC2.Visible = true;

            TList<HotelContact> hc1 = objContactdtl.GetContactDetails(hotelID, "PrimaryContact2");
            if (hc1.Count > 0)
            {
                hidPc2.Value = hc1[0].Id.ToString();
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
   
    /// <summary>
    /// Copy data from secondary contact1 to secondary contact2
    /// </summary>
    protected void lnbCopySc_Click(object sender, EventArgs e)
    {
        try
        {
            //Copy all data from Secondary Contact1 to Secondary Contact2
            divWeekendSec.Visible = true;
            txtJobTitleSc2.Text = txtJobTitleSc1.Text;
            txtFnameSc2.Text = txtFnameSc1.Text;
            txtLnameSc2.Text = txtLnameSc1.Text;
            drpExtPhoneSc2.SelectedValue = drpExtPhoneSc1.SelectedValue;
            txtPhoneSc2.Text = txtPhoneSc1.Text;
            txtEmailSc2.Text = txtEmailSc1.Text;
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");

            ViewSc2.Visible = false;
            NewSc2.Visible = true;

            TList<HotelContact> hc2 = objContactdtl.GetContactDetails(hotelID, "SecondaryContact2");
            if (hc2.Count > 0)
            {
                hidSc2.Value = hc2[0].Id.ToString();
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Open Secondary contact 1 and 2
    /// <summary>
    /// After clicks on this link, secondary contact1 will appear
    /// </summary>
    protected void lnbSecCon1_Click(object sender, EventArgs e)
    {
        divWeekdaySec.Visible = true;
        divmessage.Style.Add("display", "none");
        divmessage.Attributes.Add("class", "error");
        clickhereweekend.Visible = true;
        ViewSc1.Visible = false;
        DivSecondarywd.Visible = false;
    }
   
    /// <summary>
    /// After clicks on this link, secondary contact2 will appear
    /// </summary>
    protected void lnbSecCon2_Click(object sender, EventArgs e)
    {
        divWeekendSec.Visible = true;
        divmessage.Style.Add("display", "none");
        divmessage.Attributes.Add("class", "error");
        ViewSc2.Visible = false;
        DivSecondarywe.Visible = false;
    }
    #endregion
   
    #region Move to Next page
    /// <summary>
    /// Function to move for next page
    /// </summary>   
    protected void btnNext_Click(object sender, EventArgs e)
    {
        //Objwizard.ThisPageIsDone(hotelPid,        
        Objwizard.ThisPageIsDone(hotelID, Convert.ToInt32(PropertyLevelSectionLeftMenu.lnkContactDetails));
        Response.Redirect("Facilities.aspx");
    }
    #endregion

    #region Reset Information 
    /// <summary>
    /// Function to clear textbox for all contact details
    /// </summary>   
    protected void btnCancel_Click(object sender, EventArgs e)
    {        
        ContactDetailsBind();
        Getgoonline();
        divmessage.Style.Add("display", "none");
        divmessage.Attributes.Add("class", "error");
    }
    #endregion
}