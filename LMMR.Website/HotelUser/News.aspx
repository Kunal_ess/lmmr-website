﻿<%@ Page Title="Hotel User - News" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master" AutoEventWireup="true" CodeFile="News.aspx.cs" Inherits="HotelUser_News" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" Runat="Server">
<h1>News</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" Runat="Server">
<div class="contact-details">
        <div class="contact-details-inner1">
            <b>
                <asp:Label ID="lnlgreeting" runat="server" Text="Dear Client,"></asp:Label>
                <br />
            <br />
          Today you are part of lastminutemeetingroom.com and enter a new way of getting your inventory on the market and increase your revenue.

<br/>First of all we would like to thank you for your willingness and believe to participate in this web platform and together we will for sure create a new environment where distribution 

of your last minute inventory, yieldable pricing and reinforcement of special promotions are key to success.



            <br />
            </b><br /><b>Please do not hesitate to send us your remarks if you encounter a problem or want to make a suggestion. We need all the input we can get to make it even better and more functional, not only towards you, the backend user, but to create an optimised result for the business consumer.</b><br />
&nbsp;<br />
            <b>
               We are ready to enter this exciting journey and for any question do not hesitate to contact us on  <a href="mailto:hotelsupport@lastminutemeetingroom.com">hotelsupport@lastminutemeetingroom.com</a>

</b><br />
            <br />
            <br />

            <br />
            <b>
                <asp:Label ID="Label3" runat="server" Text="Many thanks to all of you, "></asp:Label>
            <br />
            </b><br />
            <b>
                <asp:Label ID="Label4" runat="server" Text="Thierry Vermeiren & Martin Duchateau"></asp:Label></b>
        </div>
    </div>
</asp:Content>

