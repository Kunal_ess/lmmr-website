﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Data;
using LMMR.Entities;
using LMMR.Business;
using System.IO;
using System.Configuration;
using log4net;
using log4net.Config;
#endregion


public partial class HotelUser_AddPromotion : System.Web.UI.Page
{
    #region Variables and Properties
    SendMails objSendmail = new SendMails();
    int intHotelID = 0;
    HotelManager objHotelManager = new HotelManager();
    EmailConfigManager em = new EmailConfigManager();
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_AddPromotion));
    #endregion

    #region PageLoad
    /// <summary>
    /// This method is call on the Page load and Initlize all the components of the page to its initial stage.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Check if Session exist
            if (Session["CurrentHotelID"] != null)
            {
                intHotelID = Convert.ToInt32(Session["CurrentHotelID"].ToString());
            }
            else
            {
                Response.Redirect("~/login.aspx",true);
            }

            Session["LinkID"] = "lnkPromotions";
            if (!IsPostBack)
            {
                //Set default Requirment at the time of page load
                CalendarExtender1.StartDate = DateTime.Now;
                CalendarExtender2.StartDate = DateTime.Now;
                divmessage.Style.Add("display", "none");
                divmessage.Attributes.Add("class", "error");
                lblHotel.Text = objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Name;
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Submit
    /// <summary>
    /// This Method is call for hotel Promotion.
    /// In this methos Mail is sent to the operator from the user.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            EmailConfig eConfig = em.GetByName("add promotion");
            if (eConfig.IsActive)
            {
                EmailConfigMapping emap = new EmailConfigMapping();
                string AttachmentPath = string.Empty;
                if (flAttachment.HasFile)
                {
                    //Check Attached file size and if less then 1mb then save it.
                    if (flAttachment.PostedFile.ContentLength > 1048576)
                    {
                        divmessage.InnerHtml = "File size exceeds the maximum limits of 1Mb.";
                        divmessage.Style.Add("display", "block");
                        divmessage.Attributes.Add("class", "error");
                        return;
                    }
                    flAttachment.SaveAs(Server.MapPath("~/EmailTemplets/EmailAttachment/" + flAttachment.FileName));
                    AttachmentPath = Server.MapPath("~/EmailTemplets/EmailAttachment/" + flAttachment.FileName);
                }
                objSendmail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
                //Send mail to the operator. If no operator present then send mail to the "gaurav-shrivastava@essindia.biz"
                Users objoperator = new HotelManager().GetFirstOperatorInDataBase();
                if (objoperator != null)
                {
                    objSendmail.ToEmail = objoperator.EmailId;
                    //objSendmail.Bcc = "gaurav-shrivastava@essindia.co.in";
                }
                else
                {
                    //objSendmail.ToEmail = "gaurav-shrivastava@essindia.co.in";
                    throw (new Exception("No operator email id found.", null));
                }


                string bodymsg = File.ReadAllText(Server.MapPath("~/EmailTemplets/EmailTempletSample.html"));
                EmailConfigMapping emap2 = new EmailConfigMapping();
                emap2 = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                bodymsg = bodymsg.Replace("@CONTENTS", emap2.EmailContents);
                EmailValueCollection objEmailValues = new EmailValueCollection();
                Users objUser = (Users)Session["CurrentUser"];
                objSendmail.Subject = "Request for Hotel of Week (Hotel: " + lblHotel.Text + " | From: " + txtFrom.Text + " | To: " + txtTo.Text + ")";
                foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForAddPromotions(lblHotel.Text, txtFrom.Text, txtTo.Text, txtBody.Text, objUser.FirstName + " " + objUser.LastName))
                {
                    bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                }
                objSendmail.Body = bodymsg;
                if (!string.IsNullOrEmpty(AttachmentPath))
                {
                    objSendmail.Attachment.Add(new System.Net.Mail.Attachment(AttachmentPath));
                }
                //Check mail sent or not if sent successful then set the default condition else give error message.
                if (objSendmail.SendMail() == "Send Mail")
                {
                    txtBody.Text = "";
                    txtFrom.Text = "dd/mm/yy";
                    txtTo.Text = "dd/mm/yy";
                    divmessage.InnerHtml = "Request sent sucessfully.";
                    divmessage.Style.Add("display", "block");
                    divmessage.Attributes.Add("class", "succesfuly");
                }
                else
                {
                    divmessage.InnerHtml = "Error on creating request.";
                    divmessage.Style.Add("display", "block");
                    divmessage.Attributes.Add("class", "error");
                }
            }
            else
            {
                divmessage.InnerHtml = "This mail functionality is not activated yet.";
                divmessage.Style.Add("display", "block");
                divmessage.Attributes.Add("class", "error");
            }
        }
        catch (Exception ex)
        {
            divmessage.InnerHtml = ex.Message;
            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "error");
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion
}