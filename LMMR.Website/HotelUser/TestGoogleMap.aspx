﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master" AutoEventWireup="true"
    CodeFile="TestGoogleMap.aspx.cs" Inherits="HotelUser_TestGoogleMap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" runat="Server">
    <table width="800px" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td colspan="2" align="center">
                <h2>
                    Map My Address</h2>
            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 200px;">
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td align='left'>
                            If you have address please enter it below and click "Show" button.<br />
                            Otherwise use the map to find the address you want. Just click at any point on the
                            map to find its address.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Address
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <textarea id="address" rows="5">Taj mahal Agra, Tajganj, Agra, Uttar Pradesh 282001, India</textarea>
                            <input type="button" value="Show" onclick="codeAddress()">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Latitude/Longitude
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span id="latlong"></span>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <div id="map_canvas" style="height: 500px; width: 500px;">
                </div>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        var geocoder;
        var map;
        var markersArray = [];
        var marker;
        var infowindow = new google.maps.InfoWindow();

        function initialize() {
            geocoder = new google.maps.Geocoder();
            var myOptions = {
                zoom: 8,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

            codeAddress();

            google.maps.event.addListener(map, 'click', function (event) {
                placeMarker(event.latLng);
            });


        }

        function codeAddress() {
            var address = document.getElementById("address").value;
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    clearOverlays();

                    document.getElementById("address").value = results[0]['formatted_address'];
                    document.getElementById("latlong").innerText = results[0].geometry.location;
                    map.setCenter(results[0].geometry.location);
                    marker = new google.maps.Marker({
                        map: map,
                        title: results[0]['formatted_address'],
                        position: results[0].geometry.location,
                        animation: google.maps.Animation.DROP
                    });


                    //                google.maps.event.addListener(marker, 'click', function () {
                    //                    infowindow.setContent(results[0].formatted_address);
                    //                    infowindow.open(map, this);
                    //                    document.getElementById("latlong").innerText = results[0].geometry.location;
                    //                    document.getElementById("address").value = results[0]['formatted_address'];
                    //                });

                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);

                    markersArray.push(marker);


                    //                google.maps.event.addListener(marker, 'click', function () {
                    //                    // Calling the open method of the infoWindow 
                    //                    infowindow.open(map, marker);
                    //                });

                } else {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }

        function placeMarker(location) {

            geocoder.geocode({ 'latLng': location }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        clearOverlays();
                        document.getElementById("address").value = results[1].formatted_address;
                        document.getElementById("latlong").innerText = results[0].geometry.location;
                        marker = new google.maps.Marker({
                            position: location,
                            title: results[1].formatted_address,
                            map: map,
                            animation: google.maps.Animation.DROP
                        });



                        infowindow.setContent(results[1].formatted_address);
                        infowindow.open(map, marker);

                        markersArray.push(marker);
                        //google.maps.event.addListener(marker, 'click', toggleBounce);
                        map.setCenter(location);

                        //mouseover
                        google.maps.event.addListener(marker, 'click', function () {
                            infowindow.setContent(results[1].formatted_address);
                            infowindow.open(map, this);
                            document.getElementById("latlong").innerText = results[0].geometry.location;
                            document.getElementById("address").value = results[0]['formatted_address'];
                        });
                    }
                } else {
                    alert("Geocoder failed due to: " + status);
                }
            });

        }


        function clearOverlays() {
            if (markersArray) {
                for (i in markersArray) {
                    markersArray[i].setMap(null);
                }
            }
        }

        function toggleBounce() {

            if (marker.getAnimation() != null) {
                marker.setAnimation(null);
            } else {
                marker.setAnimation(google.maps.Animation.BOUNCE);
            }
        }
        
    </script>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-20626488-1']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
</asp:Content>
