﻿<%@ Page Title="Hotel User - Contact Details" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master"
    MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeFile="ContactDetails.aspx.cs"
    Inherits="HotelUser_ContactDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" runat="Server">

    <h1>
        Contact Details : <b>
            <asp:Label ID="lblHotel" runat="server" Text=""></asp:Label></b></h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" runat="Server">
    <style>
        #contentbody_UpdatePanel1
        {
            float: left;
        }
    </style>    
    <!-- start rightside_inner_container--> 
    <div style=" width:758px; overflow:hidden;">
    <div id="divmessage" runat="server">
                </div>
  </div>
            <div class="contact-details">                                   
                <div class="contact-details-inner clearfix" style="vertical-align: top">
                    <div class="contact-details-left-body clearfix">
                        <div class="contact-details-left clearfix">
                            <div class="contact-details-heading">
                                Week Days</div>
                            <fieldset class="big" id="NewPC1" runat="server">
                                <legend>Primary Contact</legend>
                                <div class="contact-details-row">
                                    <div class="contact-details-row-left1">
                                        Job title <span style="color: Red">*</span>
                                    </div>
                                    <div class="contact-details-row-right">
                                        <asp:TextBox ID="txtJobTitlePc1" runat="server" CssClass="textfield2" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="contact-details-row">
                                    <div class="contact-details-row-left1">
                                        First name<span style="color: Red"> *</span>
                                    </div>
                                    <div class="contact-details-row-right">
                                        <asp:TextBox ID="txtFnamePc1" runat="server" CssClass="textfield2" MaxLength="20"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="contact-details-row">
                                    <div class="contact-details-row-left1">
                                        Last name<span style="color: Red"> *</span>
                                    </div>
                                    <div class="contact-details-row-right">
                                        <asp:TextBox ID="txtLnamePc1" runat="server" CssClass="textfield2" MaxLength="20"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="contact-details-row2 clearfix">
                                    <div class="contact-details-row-left1">
                                        Phone<span style="color: Red"> *</span>
                                    </div>
                                    <div class="contact-details-row-right2 clearfix">
                                    <div class="drpext">                                           
                                             <asp:DropDownList ID="drpExtPhone1" runat="server" CssClass="NoClassApply" Width="80%">
                         <asp:ListItem Value="0">Extension</asp:ListItem>
<asp:ListItem>1</asp:ListItem>
<asp:ListItem>7</asp:ListItem>
<asp:ListItem>20</asp:ListItem>
<asp:ListItem>27</asp:ListItem>
<asp:ListItem>30</asp:ListItem>
<asp:ListItem>31</asp:ListItem>
<asp:ListItem>32</asp:ListItem>
<asp:ListItem>33</asp:ListItem>
<asp:ListItem>34</asp:ListItem>
<asp:ListItem>36</asp:ListItem>
<asp:ListItem>39</asp:ListItem>
<asp:ListItem>40</asp:ListItem>
<asp:ListItem>41</asp:ListItem>
<asp:ListItem>43</asp:ListItem>
<asp:ListItem>44</asp:ListItem>
<asp:ListItem>45</asp:ListItem>
<asp:ListItem>46</asp:ListItem>
<asp:ListItem>47</asp:ListItem>
<asp:ListItem>48</asp:ListItem>
<asp:ListItem>49</asp:ListItem>
<asp:ListItem>51</asp:ListItem>
<asp:ListItem>52</asp:ListItem>
<asp:ListItem>53</asp:ListItem>
<asp:ListItem>54</asp:ListItem>
<asp:ListItem>55</asp:ListItem>
<asp:ListItem>56</asp:ListItem>
<asp:ListItem>57</asp:ListItem>
<asp:ListItem>58</asp:ListItem>
<asp:ListItem>60</asp:ListItem>
<asp:ListItem>61</asp:ListItem>
<asp:ListItem>62</asp:ListItem>
<asp:ListItem>63</asp:ListItem>
<asp:ListItem>64</asp:ListItem>
<asp:ListItem>65</asp:ListItem>
<asp:ListItem>66</asp:ListItem>
<asp:ListItem>81</asp:ListItem>
<asp:ListItem>82</asp:ListItem>
<asp:ListItem>84</asp:ListItem>
<asp:ListItem>86</asp:ListItem>
<asp:ListItem>90</asp:ListItem>
<asp:ListItem>91</asp:ListItem>
<asp:ListItem>92</asp:ListItem>
<asp:ListItem>93</asp:ListItem>
<asp:ListItem>94</asp:ListItem>
<asp:ListItem>95</asp:ListItem>
<asp:ListItem>98</asp:ListItem>
<asp:ListItem>212</asp:ListItem>
<asp:ListItem>213</asp:ListItem>
<asp:ListItem>216</asp:ListItem>
<asp:ListItem>218</asp:ListItem>
<asp:ListItem>220</asp:ListItem>
<asp:ListItem>221</asp:ListItem>
<asp:ListItem>222</asp:ListItem>
<asp:ListItem>223</asp:ListItem>
<asp:ListItem>224</asp:ListItem>
<asp:ListItem>225</asp:ListItem>
<asp:ListItem>226</asp:ListItem>
<asp:ListItem>227</asp:ListItem>
<asp:ListItem>228</asp:ListItem>
<asp:ListItem>229</asp:ListItem>
<asp:ListItem>230</asp:ListItem>
<asp:ListItem>231</asp:ListItem>
<asp:ListItem>232</asp:ListItem>
<asp:ListItem>233</asp:ListItem>
<asp:ListItem>234</asp:ListItem>
<asp:ListItem>235</asp:ListItem>
<asp:ListItem>236</asp:ListItem>
<asp:ListItem>237</asp:ListItem>
<asp:ListItem>238</asp:ListItem>
<asp:ListItem>239</asp:ListItem>
<asp:ListItem>240</asp:ListItem>
<asp:ListItem>241</asp:ListItem>
<asp:ListItem>242</asp:ListItem>
<asp:ListItem>243</asp:ListItem>
<asp:ListItem>244</asp:ListItem>
<asp:ListItem>245</asp:ListItem>
<asp:ListItem>248</asp:ListItem>
<asp:ListItem>249</asp:ListItem>
<asp:ListItem>250</asp:ListItem>
<asp:ListItem>251</asp:ListItem>
<asp:ListItem>252</asp:ListItem>
<asp:ListItem>253</asp:ListItem>
<asp:ListItem>254</asp:ListItem>
<asp:ListItem>255</asp:ListItem>
<asp:ListItem>256</asp:ListItem>
<asp:ListItem>257</asp:ListItem>
<asp:ListItem>258</asp:ListItem>
<asp:ListItem>260</asp:ListItem>
<asp:ListItem>261</asp:ListItem>
<asp:ListItem>262</asp:ListItem>
<asp:ListItem>263</asp:ListItem>
<asp:ListItem>264</asp:ListItem>
<asp:ListItem>265</asp:ListItem>
<asp:ListItem>266</asp:ListItem>
<asp:ListItem>267</asp:ListItem>
<asp:ListItem>268</asp:ListItem>
<asp:ListItem>269</asp:ListItem>
<asp:ListItem>290</asp:ListItem>
<asp:ListItem>291</asp:ListItem>
<asp:ListItem>297</asp:ListItem>
<asp:ListItem>298</asp:ListItem>
<asp:ListItem>299</asp:ListItem>
<asp:ListItem>350</asp:ListItem>
<asp:ListItem>351</asp:ListItem>
<asp:ListItem>352</asp:ListItem>
<asp:ListItem>353</asp:ListItem>
<asp:ListItem>354</asp:ListItem>
<asp:ListItem>355</asp:ListItem>
<asp:ListItem>356</asp:ListItem>
<asp:ListItem>357</asp:ListItem>
<asp:ListItem>358</asp:ListItem>
<asp:ListItem>359</asp:ListItem>
<asp:ListItem>370</asp:ListItem>
<asp:ListItem>371</asp:ListItem>
<asp:ListItem>372</asp:ListItem>
<asp:ListItem>373</asp:ListItem>
<asp:ListItem>374</asp:ListItem>
<asp:ListItem>375</asp:ListItem>
<asp:ListItem>376</asp:ListItem>
<asp:ListItem>377</asp:ListItem>
<asp:ListItem>378</asp:ListItem>
<asp:ListItem>380</asp:ListItem>
<asp:ListItem>381</asp:ListItem>
<asp:ListItem>382</asp:ListItem>
<asp:ListItem>385</asp:ListItem>
<asp:ListItem>386</asp:ListItem>
<asp:ListItem>387</asp:ListItem>
<asp:ListItem>389</asp:ListItem>
<asp:ListItem>420</asp:ListItem>
<asp:ListItem>421</asp:ListItem>
<asp:ListItem>423</asp:ListItem>
<asp:ListItem>500</asp:ListItem>
<asp:ListItem>501</asp:ListItem>
<asp:ListItem>502</asp:ListItem>
<asp:ListItem>503</asp:ListItem>
<asp:ListItem>504</asp:ListItem>
<asp:ListItem>505</asp:ListItem>
<asp:ListItem>506</asp:ListItem>
<asp:ListItem>507</asp:ListItem>
<asp:ListItem>508</asp:ListItem>
<asp:ListItem>509</asp:ListItem>
<asp:ListItem>590</asp:ListItem>
<asp:ListItem>591</asp:ListItem>
<asp:ListItem>592</asp:ListItem>
<asp:ListItem>593</asp:ListItem>
<asp:ListItem>595</asp:ListItem>
<asp:ListItem>597</asp:ListItem>
<asp:ListItem>598</asp:ListItem>
<asp:ListItem>599</asp:ListItem>
<asp:ListItem>670</asp:ListItem>
<asp:ListItem>672</asp:ListItem>
<asp:ListItem>673</asp:ListItem>
<asp:ListItem>674</asp:ListItem>
<asp:ListItem>675</asp:ListItem>
<asp:ListItem>676</asp:ListItem>
<asp:ListItem>677</asp:ListItem>
<asp:ListItem>678</asp:ListItem>
<asp:ListItem>679</asp:ListItem>
<asp:ListItem>680</asp:ListItem>
<asp:ListItem>681</asp:ListItem>
<asp:ListItem>682</asp:ListItem>
<asp:ListItem>683</asp:ListItem>
<asp:ListItem>685</asp:ListItem>
<asp:ListItem>686</asp:ListItem>
<asp:ListItem>687</asp:ListItem>
<asp:ListItem>688</asp:ListItem>
<asp:ListItem>689</asp:ListItem>
<asp:ListItem>690</asp:ListItem>
<asp:ListItem>691</asp:ListItem>
<asp:ListItem>692</asp:ListItem>
<asp:ListItem>850</asp:ListItem>
<asp:ListItem>852</asp:ListItem>
<asp:ListItem>853</asp:ListItem>
<asp:ListItem>855</asp:ListItem>
<asp:ListItem>856</asp:ListItem>
<asp:ListItem>870</asp:ListItem>
<asp:ListItem>880</asp:ListItem>
<asp:ListItem>886</asp:ListItem>
<asp:ListItem>960</asp:ListItem>
<asp:ListItem>961</asp:ListItem>
<asp:ListItem>962</asp:ListItem>
<asp:ListItem>963</asp:ListItem>
<asp:ListItem>964</asp:ListItem>
<asp:ListItem>965</asp:ListItem>
<asp:ListItem>966</asp:ListItem>
<asp:ListItem>967</asp:ListItem>
<asp:ListItem>968</asp:ListItem>
<asp:ListItem>970</asp:ListItem>
<asp:ListItem>971</asp:ListItem>
<asp:ListItem>972</asp:ListItem>
<asp:ListItem>973</asp:ListItem>
<asp:ListItem>974</asp:ListItem>
<asp:ListItem>975</asp:ListItem>
<asp:ListItem>976</asp:ListItem>
<asp:ListItem>977</asp:ListItem>
<asp:ListItem>992</asp:ListItem>
<asp:ListItem>993</asp:ListItem>
<asp:ListItem>994</asp:ListItem>
<asp:ListItem>995</asp:ListItem>
<asp:ListItem>996</asp:ListItem>
<asp:ListItem>998</asp:ListItem>
<asp:ListItem>1242</asp:ListItem>
<asp:ListItem>1246</asp:ListItem>
<asp:ListItem>1264</asp:ListItem>
<asp:ListItem>1268</asp:ListItem>
<asp:ListItem>1284</asp:ListItem>
<asp:ListItem>1340</asp:ListItem>
<asp:ListItem>1345</asp:ListItem>
<asp:ListItem>1441</asp:ListItem>
<asp:ListItem>1473</asp:ListItem>
<asp:ListItem>1599</asp:ListItem>
<asp:ListItem>1649</asp:ListItem>
<asp:ListItem>1664</asp:ListItem>
<asp:ListItem>1670</asp:ListItem>
<asp:ListItem>1671</asp:ListItem>
<asp:ListItem>1684</asp:ListItem>
<asp:ListItem>1758</asp:ListItem>
<asp:ListItem>1767</asp:ListItem>
<asp:ListItem>1784</asp:ListItem>
<asp:ListItem>1809</asp:ListItem>
<asp:ListItem>1868</asp:ListItem>
<asp:ListItem>1869</asp:ListItem>
<asp:ListItem>1876</asp:ListItem>
 <asp:ListItem>00420</asp:ListItem>
                        </asp:DropDownList>                                             
                                    </div>  
                                    <div class="drpext-inputbox" >                                           
                                            <asp:TextBox ID="txtPhonePc1" runat="server" CssClass="inputbox4" MaxLength="15"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtPhonePc1"
                                                FilterType="Numbers">
                                            </asp:FilteredTextBoxExtender>
                                    </div>                                                                                                                                                                  
                                    </div>
                                </div>
                                <div class="contact-details-row">
                                    <div class="contact-details-row-left1">
                                        Email<span style="color: Red"> *</span>
                                    </div>
                                    <div class="contact-details-row-right">
                                        <asp:TextBox ID="txtEmailPc1" runat="server" CssClass="textfield2" MaxLength="200"></asp:TextBox>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="inner" id="ViewPC1" runat="server">
                                <asp:DataList ID="dlistPrimaryContact1" RepeatColumns="2" RepeatDirection="Horizontal"
                                    runat="server">
                                    <%-- Create a DataList control template named "ItemTemplate". --%>
                                    <ItemTemplate>
                                        <legend>Primary Contact</legend>
                                        <div style="float: right">
                                            
                                        </div>
                                        <div style="float: right">
                                            <asp:LinkButton ID="lnbPrimaryCnt1" runat="server" CommandName="SelectItem" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id")%>'
                                                OnClick="lnbPrimaryCnt1_Click">Modify</asp:LinkButton></div>
                                        <div style="clear: both">
                                        </div>
                                        <%--<div class="list">--%>
                                        <div class="contact-details-row1">
                                            <div class="contact-details-row-left">
                                                Job title :
                                            </div>
                                            <div class="contact-details-row-right">
                                                <%--<asp:HiddenField ID="tt" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "Id")%>'/>--%>
                                                <%# DataBinder.Eval(Container.DataItem, "JobTitle")%>
                                            </div>
                                        </div>
                                        <div class="contact-details-row1">
                                            <div class="contact-details-row-left">
                                                First name :
                                            </div>
                                            <div class="contact-details-row-right">
                                                <%# DataBinder.Eval(Container.DataItem, "FirstName")%>
                                            </div>
                                        </div>
                                        <div class="contact-details-row1">
                                            <div class="contact-details-row-left">
                                                Last name :
                                            </div>
                                            <div class="contact-details-row-right">
                                                <%# DataBinder.Eval(Container.DataItem, "LastName")%>
                                            </div>
                                        </div>
                                        <div class="contact-details-row1">
                                            <div class="contact-details-row-left">
                                                Phone :
                                            </div>
                                            <div class="contact-details-row-right">
                                                <%# DataBinder.Eval(Container.DataItem, "Ext")%>
                                                <%# DataBinder.Eval(Container.DataItem, "Phone")%>
                                            </div>
                                        </div>
                                        <div class="contact-details-row1">
                                            <div class="contact-details-row-left">
                                                Email :
                                            </div>
                                            <div class="contact-details-row-right">
                                                <%# DataBinder.Eval(Container.DataItem, "Email")%>
                                            </div>
                                        </div>
                                        <%--</div>--%>
                                    </ItemTemplate>
                                </asp:DataList>
                            </fieldset>
                            <div class="contact-details-row-bttom" id="clickhereweekday" runat="server" style="float: left; text-align: center; width: 100%; padding-top: 10px;">
                                click
                                <asp:LinkButton ID="lnbCopyPc" runat="server" OnClick="lnbCopyPc_Click" ValidationGroup="Group1">here</asp:LinkButton>
                                to copy data to weekend contact<asp:HiddenField ID="hidPc1" runat="server" />
                            </div><br />                           
                        </div>
                        <div class="contact-details-row-bttom" id="DivSecondarywd" runat="server">
                            <asp:LinkButton ID="lnbSecCon1" runat="server" OnClick="lnbSecCon1_Click">Add secondary weekday contact</asp:LinkButton>
                        </div>
                        <br />
                        <div class="contact-details-left" id="divWeekdaySec" runat="server" visible="false"><br />
                            <fieldset class="big" id="NewSc1" runat="server">
                                <legend>Secondary Contact</legend>
                                <div class="contact-details-row">
                                    <div class="contact-details-row-left1">
                                        Job title<span style="color: Red"> *</span>
                                    </div>
                                    <div class="contact-details-row-right">
                                        <asp:TextBox ID="txtJobTitleSc1" runat="server" CssClass="textfield2" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="contact-details-row">
                                    <div class="contact-details-row-left1">
                                        First name<span style="color: Red"> *</span>
                                    </div>
                                    <div class="contact-details-row-right">
                                        <asp:TextBox ID="txtFnameSc1" runat="server" CssClass="textfield2" MaxLength="20"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="contact-details-row">
                                    <div class="contact-details-row-left1">
                                        Last name<span style="color: Red"> *</span>
                                    </div>
                                    <div class="contact-details-row-right">
                                        <asp:TextBox ID="txtLnameSc1" runat="server" CssClass="textfield2" MaxLength="20"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="contact-details-row2 clearfix">
                                    <div class="contact-details-row-left1">
                                        Phone<span style="color: Red"> *</span>
                                    </div>
                                    <div class="contact-details-row-right2 clearfix">
                                            <div class="drpext">                                           
                                            <asp:DropDownList ID="drpExtPhoneSc1" runat="server" CssClass="NoClassApply" Width="80%">
                         <asp:ListItem Value="0">Extension</asp:ListItem>
<asp:ListItem>1</asp:ListItem>
<asp:ListItem>7</asp:ListItem>
<asp:ListItem>20</asp:ListItem>
<asp:ListItem>27</asp:ListItem>
<asp:ListItem>30</asp:ListItem>
<asp:ListItem>31</asp:ListItem>
<asp:ListItem>32</asp:ListItem>
<asp:ListItem>33</asp:ListItem>
<asp:ListItem>34</asp:ListItem>
<asp:ListItem>36</asp:ListItem>
<asp:ListItem>39</asp:ListItem>
<asp:ListItem>40</asp:ListItem>
<asp:ListItem>41</asp:ListItem>
<asp:ListItem>43</asp:ListItem>
<asp:ListItem>44</asp:ListItem>
<asp:ListItem>45</asp:ListItem>
<asp:ListItem>46</asp:ListItem>
<asp:ListItem>47</asp:ListItem>
<asp:ListItem>48</asp:ListItem>
<asp:ListItem>49</asp:ListItem>
<asp:ListItem>51</asp:ListItem>
<asp:ListItem>52</asp:ListItem>
<asp:ListItem>53</asp:ListItem>
<asp:ListItem>54</asp:ListItem>
<asp:ListItem>55</asp:ListItem>
<asp:ListItem>56</asp:ListItem>
<asp:ListItem>57</asp:ListItem>
<asp:ListItem>58</asp:ListItem>
<asp:ListItem>60</asp:ListItem>
<asp:ListItem>61</asp:ListItem>
<asp:ListItem>62</asp:ListItem>
<asp:ListItem>63</asp:ListItem>
<asp:ListItem>64</asp:ListItem>
<asp:ListItem>65</asp:ListItem>
<asp:ListItem>66</asp:ListItem>
<asp:ListItem>81</asp:ListItem>
<asp:ListItem>82</asp:ListItem>
<asp:ListItem>84</asp:ListItem>
<asp:ListItem>86</asp:ListItem>
<asp:ListItem>90</asp:ListItem>
<asp:ListItem>91</asp:ListItem>
<asp:ListItem>92</asp:ListItem>
<asp:ListItem>93</asp:ListItem>
<asp:ListItem>94</asp:ListItem>
<asp:ListItem>95</asp:ListItem>
<asp:ListItem>98</asp:ListItem>
<asp:ListItem>212</asp:ListItem>
<asp:ListItem>213</asp:ListItem>
<asp:ListItem>216</asp:ListItem>
<asp:ListItem>218</asp:ListItem>
<asp:ListItem>220</asp:ListItem>
<asp:ListItem>221</asp:ListItem>
<asp:ListItem>222</asp:ListItem>
<asp:ListItem>223</asp:ListItem>
<asp:ListItem>224</asp:ListItem>
<asp:ListItem>225</asp:ListItem>
<asp:ListItem>226</asp:ListItem>
<asp:ListItem>227</asp:ListItem>
<asp:ListItem>228</asp:ListItem>
<asp:ListItem>229</asp:ListItem>
<asp:ListItem>230</asp:ListItem>
<asp:ListItem>231</asp:ListItem>
<asp:ListItem>232</asp:ListItem>
<asp:ListItem>233</asp:ListItem>
<asp:ListItem>234</asp:ListItem>
<asp:ListItem>235</asp:ListItem>
<asp:ListItem>236</asp:ListItem>
<asp:ListItem>237</asp:ListItem>
<asp:ListItem>238</asp:ListItem>
<asp:ListItem>239</asp:ListItem>
<asp:ListItem>240</asp:ListItem>
<asp:ListItem>241</asp:ListItem>
<asp:ListItem>242</asp:ListItem>
<asp:ListItem>243</asp:ListItem>
<asp:ListItem>244</asp:ListItem>
<asp:ListItem>245</asp:ListItem>
<asp:ListItem>248</asp:ListItem>
<asp:ListItem>249</asp:ListItem>
<asp:ListItem>250</asp:ListItem>
<asp:ListItem>251</asp:ListItem>
<asp:ListItem>252</asp:ListItem>
<asp:ListItem>253</asp:ListItem>
<asp:ListItem>254</asp:ListItem>
<asp:ListItem>255</asp:ListItem>
<asp:ListItem>256</asp:ListItem>
<asp:ListItem>257</asp:ListItem>
<asp:ListItem>258</asp:ListItem>
<asp:ListItem>260</asp:ListItem>
<asp:ListItem>261</asp:ListItem>
<asp:ListItem>262</asp:ListItem>
<asp:ListItem>263</asp:ListItem>
<asp:ListItem>264</asp:ListItem>
<asp:ListItem>265</asp:ListItem>
<asp:ListItem>266</asp:ListItem>
<asp:ListItem>267</asp:ListItem>
<asp:ListItem>268</asp:ListItem>
<asp:ListItem>269</asp:ListItem>
<asp:ListItem>290</asp:ListItem>
<asp:ListItem>291</asp:ListItem>
<asp:ListItem>297</asp:ListItem>
<asp:ListItem>298</asp:ListItem>
<asp:ListItem>299</asp:ListItem>
<asp:ListItem>350</asp:ListItem>
<asp:ListItem>351</asp:ListItem>
<asp:ListItem>352</asp:ListItem>
<asp:ListItem>353</asp:ListItem>
<asp:ListItem>354</asp:ListItem>
<asp:ListItem>355</asp:ListItem>
<asp:ListItem>356</asp:ListItem>
<asp:ListItem>357</asp:ListItem>
<asp:ListItem>358</asp:ListItem>
<asp:ListItem>359</asp:ListItem>
<asp:ListItem>370</asp:ListItem>
<asp:ListItem>371</asp:ListItem>
<asp:ListItem>372</asp:ListItem>
<asp:ListItem>373</asp:ListItem>
<asp:ListItem>374</asp:ListItem>
<asp:ListItem>375</asp:ListItem>
<asp:ListItem>376</asp:ListItem>
<asp:ListItem>377</asp:ListItem>
<asp:ListItem>378</asp:ListItem>
<asp:ListItem>380</asp:ListItem>
<asp:ListItem>381</asp:ListItem>
<asp:ListItem>382</asp:ListItem>
<asp:ListItem>385</asp:ListItem>
<asp:ListItem>386</asp:ListItem>
<asp:ListItem>387</asp:ListItem>
<asp:ListItem>389</asp:ListItem>
<asp:ListItem>420</asp:ListItem>
<asp:ListItem>421</asp:ListItem>
<asp:ListItem>423</asp:ListItem>
<asp:ListItem>500</asp:ListItem>
<asp:ListItem>501</asp:ListItem>
<asp:ListItem>502</asp:ListItem>
<asp:ListItem>503</asp:ListItem>
<asp:ListItem>504</asp:ListItem>
<asp:ListItem>505</asp:ListItem>
<asp:ListItem>506</asp:ListItem>
<asp:ListItem>507</asp:ListItem>
<asp:ListItem>508</asp:ListItem>
<asp:ListItem>509</asp:ListItem>
<asp:ListItem>590</asp:ListItem>
<asp:ListItem>591</asp:ListItem>
<asp:ListItem>592</asp:ListItem>
<asp:ListItem>593</asp:ListItem>
<asp:ListItem>595</asp:ListItem>
<asp:ListItem>597</asp:ListItem>
<asp:ListItem>598</asp:ListItem>
<asp:ListItem>599</asp:ListItem>
<asp:ListItem>670</asp:ListItem>
<asp:ListItem>672</asp:ListItem>
<asp:ListItem>673</asp:ListItem>
<asp:ListItem>674</asp:ListItem>
<asp:ListItem>675</asp:ListItem>
<asp:ListItem>676</asp:ListItem>
<asp:ListItem>677</asp:ListItem>
<asp:ListItem>678</asp:ListItem>
<asp:ListItem>679</asp:ListItem>
<asp:ListItem>680</asp:ListItem>
<asp:ListItem>681</asp:ListItem>
<asp:ListItem>682</asp:ListItem>
<asp:ListItem>683</asp:ListItem>
<asp:ListItem>685</asp:ListItem>
<asp:ListItem>686</asp:ListItem>
<asp:ListItem>687</asp:ListItem>
<asp:ListItem>688</asp:ListItem>
<asp:ListItem>689</asp:ListItem>
<asp:ListItem>690</asp:ListItem>
<asp:ListItem>691</asp:ListItem>
<asp:ListItem>692</asp:ListItem>
<asp:ListItem>850</asp:ListItem>
<asp:ListItem>852</asp:ListItem>
<asp:ListItem>853</asp:ListItem>
<asp:ListItem>855</asp:ListItem>
<asp:ListItem>856</asp:ListItem>
<asp:ListItem>870</asp:ListItem>
<asp:ListItem>880</asp:ListItem>
<asp:ListItem>886</asp:ListItem>
<asp:ListItem>960</asp:ListItem>
<asp:ListItem>961</asp:ListItem>
<asp:ListItem>962</asp:ListItem>
<asp:ListItem>963</asp:ListItem>
<asp:ListItem>964</asp:ListItem>
<asp:ListItem>965</asp:ListItem>
<asp:ListItem>966</asp:ListItem>
<asp:ListItem>967</asp:ListItem>
<asp:ListItem>968</asp:ListItem>
<asp:ListItem>970</asp:ListItem>
<asp:ListItem>971</asp:ListItem>
<asp:ListItem>972</asp:ListItem>
<asp:ListItem>973</asp:ListItem>
<asp:ListItem>974</asp:ListItem>
<asp:ListItem>975</asp:ListItem>
<asp:ListItem>976</asp:ListItem>
<asp:ListItem>977</asp:ListItem>
<asp:ListItem>992</asp:ListItem>
<asp:ListItem>993</asp:ListItem>
<asp:ListItem>994</asp:ListItem>
<asp:ListItem>995</asp:ListItem>
<asp:ListItem>996</asp:ListItem>
<asp:ListItem>998</asp:ListItem>
<asp:ListItem>1242</asp:ListItem>
<asp:ListItem>1246</asp:ListItem>
<asp:ListItem>1264</asp:ListItem>
<asp:ListItem>1268</asp:ListItem>
<asp:ListItem>1284</asp:ListItem>
<asp:ListItem>1340</asp:ListItem>
<asp:ListItem>1345</asp:ListItem>
<asp:ListItem>1441</asp:ListItem>
<asp:ListItem>1473</asp:ListItem>
<asp:ListItem>1599</asp:ListItem>
<asp:ListItem>1649</asp:ListItem>
<asp:ListItem>1664</asp:ListItem>
<asp:ListItem>1670</asp:ListItem>
<asp:ListItem>1671</asp:ListItem>
<asp:ListItem>1684</asp:ListItem>
<asp:ListItem>1758</asp:ListItem>
<asp:ListItem>1767</asp:ListItem>
<asp:ListItem>1784</asp:ListItem>
<asp:ListItem>1809</asp:ListItem>
<asp:ListItem>1868</asp:ListItem>
<asp:ListItem>1869</asp:ListItem>
<asp:ListItem>1876</asp:ListItem>
 <asp:ListItem>00420</asp:ListItem>
                        </asp:DropDownList>                                             
                                            </div>
                                            <div class="drpext-inputbox" > 
                                            <asp:TextBox ID="txtPhoneSc1" runat="server" CssClass="inputbox4" MaxLength="15"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtPhoneSc1"
                                                FilterType="Numbers">
                                            </asp:FilteredTextBoxExtender>
                                            </div>                                         
                                    </div>
                                </div>
                                <div class="contact-details-row">
                                    <div class="contact-details-row-left1">
                                        Email<span style="color: Red"> *</span>
                                    </div>
                                    <div class="contact-details-row-right">
                                        <asp:TextBox ID="txtEmailSc1" runat="server" CssClass="textfield2" MaxLength="200"></asp:TextBox>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="inner" id="ViewSc1" runat="server">
                                <asp:DataList ID="dlistSecondaryContact1" RepeatColumns="2" RepeatDirection="Horizontal"
                                    runat="server">
                                    <%-- Create a DataList control template named "ItemTemplate". --%>
                                    <ItemTemplate>
                                        <legend>Secondary Contact</legend>
                                        <div style="float: right">
                                            <asp:LinkButton ID="lnbSecondaryCnt1" runat="server" CommandName="SelectItem" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id")%>'
                                                OnClick="lnbSecondaryCnt1_Click">Modify</asp:LinkButton></div>
                                        <div style="clear: both">
                                        </div>
                                        <div class="contact-details-row1">
                                            <div class="contact-details-row-left">
                                                Job title :
                                            </div>
                                            <div class="contact-details-row-right">
                                                <%# DataBinder.Eval(Container.DataItem, "JobTitle")%>
                                            </div>
                                        </div>
                                        <div class="contact-details-row1">
                                            <div class="contact-details-row-left">
                                                First name :
                                            </div>
                                            <div class="contact-details-row-right">
                                                <%# DataBinder.Eval(Container.DataItem, "FirstName")%>
                                            </div>
                                        </div>
                                        <div class="contact-details-row1">
                                            <div class="contact-details-row-left">
                                                Last name :
                                            </div>
                                            <div class="contact-details-row-right">
                                                <%# DataBinder.Eval(Container.DataItem, "LastName")%>
                                            </div>
                                        </div>
                                        <div class="contact-details-row1">
                                            <div class="contact-details-row-left">
                                                Phone :
                                            </div>
                                            <div class="contact-details-row-right">
                                                <%# DataBinder.Eval(Container.DataItem, "Ext")%>
                                                <%# DataBinder.Eval(Container.DataItem, "Phone")%>
                                            </div>
                                        </div>
                                        <div class="contact-details-row1">
                                            <div class="contact-details-row-left">
                                                Email :
                                            </div>
                                            <div class="contact-details-row-right">
                                                <%# DataBinder.Eval(Container.DataItem, "Email")%>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </fieldset>
                            <div class="contact-details-row-bttom" id="clickhereweekend" runat="server" style="float: left; text-align: center; width: 100%; padding-top: 10px;">
                                click <a href="#">
                                    <asp:LinkButton ID="lnbCopySc" runat="server" OnClick="lnbCopySc_Click" ValidationGroup="Group3">here</asp:LinkButton></a>
                                to copy data to weekend contact
                                <asp:HiddenField ID="hidSc1" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="contact-details-right-body">
                        <div class="contact-details-right">
                            <div class="contact-details-heading">
                                Weekend
                            </div>
                            <fieldset class="big" id="NewPC2" runat="server">
                                <legend>Primary Contact</legend>
                                <div class="contact-details-row">
                                    <div class="contact-details-row-left1">
                                        Job title<span style="color: Red"> *</span>
                                    </div>
                                    <div class="contact-details-row-right">
                                        <asp:TextBox ID="txtJobTitlePc2" runat="server" CssClass="textfield2" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="contact-details-row">
                                    <div class="contact-details-row-left1">
                                        First name<span style="color: Red"> *</span>
                                    </div>
                                    <div class="contact-details-row-right">
                                        <asp:TextBox ID="txtFnamePc2" runat="server" CssClass="textfield2" MaxLength="20"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="contact-details-row">
                                    <div class="contact-details-row-left1">
                                        Last name<span style="color: Red"> *</span>
                                    </div>
                                    <div class="contact-details-row-right">
                                        <asp:TextBox ID="txtLnamePc2" runat="server" CssClass="textfield2" MaxLength="20"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="contact-details-row2 clearfix">
                                    <div class="contact-details-row-left1">
                                            Phone<span style="color: Red"> *</span>
                                        </div>
                                    <div class="contact-details-row-right2 clearfix">
                                            <div class="drpext">                                            
                                            <asp:DropDownList ID="drpExtPhonePc2" runat="server" CssClass="NoClassApply" Width="80%">
                         <asp:ListItem Value="0">Extension</asp:ListItem>
<asp:ListItem>1</asp:ListItem>
<asp:ListItem>7</asp:ListItem>
<asp:ListItem>20</asp:ListItem>
<asp:ListItem>27</asp:ListItem>
<asp:ListItem>30</asp:ListItem>
<asp:ListItem>31</asp:ListItem>
<asp:ListItem>32</asp:ListItem>
<asp:ListItem>33</asp:ListItem>
<asp:ListItem>34</asp:ListItem>
<asp:ListItem>36</asp:ListItem>
<asp:ListItem>39</asp:ListItem>
<asp:ListItem>40</asp:ListItem>
<asp:ListItem>41</asp:ListItem>
<asp:ListItem>43</asp:ListItem>
<asp:ListItem>44</asp:ListItem>
<asp:ListItem>45</asp:ListItem>
<asp:ListItem>46</asp:ListItem>
<asp:ListItem>47</asp:ListItem>
<asp:ListItem>48</asp:ListItem>
<asp:ListItem>49</asp:ListItem>
<asp:ListItem>51</asp:ListItem>
<asp:ListItem>52</asp:ListItem>
<asp:ListItem>53</asp:ListItem>
<asp:ListItem>54</asp:ListItem>
<asp:ListItem>55</asp:ListItem>
<asp:ListItem>56</asp:ListItem>
<asp:ListItem>57</asp:ListItem>
<asp:ListItem>58</asp:ListItem>
<asp:ListItem>60</asp:ListItem>
<asp:ListItem>61</asp:ListItem>
<asp:ListItem>62</asp:ListItem>
<asp:ListItem>63</asp:ListItem>
<asp:ListItem>64</asp:ListItem>
<asp:ListItem>65</asp:ListItem>
<asp:ListItem>66</asp:ListItem>
<asp:ListItem>81</asp:ListItem>
<asp:ListItem>82</asp:ListItem>
<asp:ListItem>84</asp:ListItem>
<asp:ListItem>86</asp:ListItem>
<asp:ListItem>90</asp:ListItem>
<asp:ListItem>91</asp:ListItem>
<asp:ListItem>92</asp:ListItem>
<asp:ListItem>93</asp:ListItem>
<asp:ListItem>94</asp:ListItem>
<asp:ListItem>95</asp:ListItem>
<asp:ListItem>98</asp:ListItem>
<asp:ListItem>212</asp:ListItem>
<asp:ListItem>213</asp:ListItem>
<asp:ListItem>216</asp:ListItem>
<asp:ListItem>218</asp:ListItem>
<asp:ListItem>220</asp:ListItem>
<asp:ListItem>221</asp:ListItem>
<asp:ListItem>222</asp:ListItem>
<asp:ListItem>223</asp:ListItem>
<asp:ListItem>224</asp:ListItem>
<asp:ListItem>225</asp:ListItem>
<asp:ListItem>226</asp:ListItem>
<asp:ListItem>227</asp:ListItem>
<asp:ListItem>228</asp:ListItem>
<asp:ListItem>229</asp:ListItem>
<asp:ListItem>230</asp:ListItem>
<asp:ListItem>231</asp:ListItem>
<asp:ListItem>232</asp:ListItem>
<asp:ListItem>233</asp:ListItem>
<asp:ListItem>234</asp:ListItem>
<asp:ListItem>235</asp:ListItem>
<asp:ListItem>236</asp:ListItem>
<asp:ListItem>237</asp:ListItem>
<asp:ListItem>238</asp:ListItem>
<asp:ListItem>239</asp:ListItem>
<asp:ListItem>240</asp:ListItem>
<asp:ListItem>241</asp:ListItem>
<asp:ListItem>242</asp:ListItem>
<asp:ListItem>243</asp:ListItem>
<asp:ListItem>244</asp:ListItem>
<asp:ListItem>245</asp:ListItem>
<asp:ListItem>248</asp:ListItem>
<asp:ListItem>249</asp:ListItem>
<asp:ListItem>250</asp:ListItem>
<asp:ListItem>251</asp:ListItem>
<asp:ListItem>252</asp:ListItem>
<asp:ListItem>253</asp:ListItem>
<asp:ListItem>254</asp:ListItem>
<asp:ListItem>255</asp:ListItem>
<asp:ListItem>256</asp:ListItem>
<asp:ListItem>257</asp:ListItem>
<asp:ListItem>258</asp:ListItem>
<asp:ListItem>260</asp:ListItem>
<asp:ListItem>261</asp:ListItem>
<asp:ListItem>262</asp:ListItem>
<asp:ListItem>263</asp:ListItem>
<asp:ListItem>264</asp:ListItem>
<asp:ListItem>265</asp:ListItem>
<asp:ListItem>266</asp:ListItem>
<asp:ListItem>267</asp:ListItem>
<asp:ListItem>268</asp:ListItem>
<asp:ListItem>269</asp:ListItem>
<asp:ListItem>290</asp:ListItem>
<asp:ListItem>291</asp:ListItem>
<asp:ListItem>297</asp:ListItem>
<asp:ListItem>298</asp:ListItem>
<asp:ListItem>299</asp:ListItem>
<asp:ListItem>350</asp:ListItem>
<asp:ListItem>351</asp:ListItem>
<asp:ListItem>352</asp:ListItem>
<asp:ListItem>353</asp:ListItem>
<asp:ListItem>354</asp:ListItem>
<asp:ListItem>355</asp:ListItem>
<asp:ListItem>356</asp:ListItem>
<asp:ListItem>357</asp:ListItem>
<asp:ListItem>358</asp:ListItem>
<asp:ListItem>359</asp:ListItem>
<asp:ListItem>370</asp:ListItem>
<asp:ListItem>371</asp:ListItem>
<asp:ListItem>372</asp:ListItem>
<asp:ListItem>373</asp:ListItem>
<asp:ListItem>374</asp:ListItem>
<asp:ListItem>375</asp:ListItem>
<asp:ListItem>376</asp:ListItem>
<asp:ListItem>377</asp:ListItem>
<asp:ListItem>378</asp:ListItem>
<asp:ListItem>380</asp:ListItem>
<asp:ListItem>381</asp:ListItem>
<asp:ListItem>382</asp:ListItem>
<asp:ListItem>385</asp:ListItem>
<asp:ListItem>386</asp:ListItem>
<asp:ListItem>387</asp:ListItem>
<asp:ListItem>389</asp:ListItem>
<asp:ListItem>420</asp:ListItem>
<asp:ListItem>421</asp:ListItem>
<asp:ListItem>423</asp:ListItem>
<asp:ListItem>500</asp:ListItem>
<asp:ListItem>501</asp:ListItem>
<asp:ListItem>502</asp:ListItem>
<asp:ListItem>503</asp:ListItem>
<asp:ListItem>504</asp:ListItem>
<asp:ListItem>505</asp:ListItem>
<asp:ListItem>506</asp:ListItem>
<asp:ListItem>507</asp:ListItem>
<asp:ListItem>508</asp:ListItem>
<asp:ListItem>509</asp:ListItem>
<asp:ListItem>590</asp:ListItem>
<asp:ListItem>591</asp:ListItem>
<asp:ListItem>592</asp:ListItem>
<asp:ListItem>593</asp:ListItem>
<asp:ListItem>595</asp:ListItem>
<asp:ListItem>597</asp:ListItem>
<asp:ListItem>598</asp:ListItem>
<asp:ListItem>599</asp:ListItem>
<asp:ListItem>670</asp:ListItem>
<asp:ListItem>672</asp:ListItem>
<asp:ListItem>673</asp:ListItem>
<asp:ListItem>674</asp:ListItem>
<asp:ListItem>675</asp:ListItem>
<asp:ListItem>676</asp:ListItem>
<asp:ListItem>677</asp:ListItem>
<asp:ListItem>678</asp:ListItem>
<asp:ListItem>679</asp:ListItem>
<asp:ListItem>680</asp:ListItem>
<asp:ListItem>681</asp:ListItem>
<asp:ListItem>682</asp:ListItem>
<asp:ListItem>683</asp:ListItem>
<asp:ListItem>685</asp:ListItem>
<asp:ListItem>686</asp:ListItem>
<asp:ListItem>687</asp:ListItem>
<asp:ListItem>688</asp:ListItem>
<asp:ListItem>689</asp:ListItem>
<asp:ListItem>690</asp:ListItem>
<asp:ListItem>691</asp:ListItem>
<asp:ListItem>692</asp:ListItem>
<asp:ListItem>850</asp:ListItem>
<asp:ListItem>852</asp:ListItem>
<asp:ListItem>853</asp:ListItem>
<asp:ListItem>855</asp:ListItem>
<asp:ListItem>856</asp:ListItem>
<asp:ListItem>870</asp:ListItem>
<asp:ListItem>880</asp:ListItem>
<asp:ListItem>886</asp:ListItem>
<asp:ListItem>960</asp:ListItem>
<asp:ListItem>961</asp:ListItem>
<asp:ListItem>962</asp:ListItem>
<asp:ListItem>963</asp:ListItem>
<asp:ListItem>964</asp:ListItem>
<asp:ListItem>965</asp:ListItem>
<asp:ListItem>966</asp:ListItem>
<asp:ListItem>967</asp:ListItem>
<asp:ListItem>968</asp:ListItem>
<asp:ListItem>970</asp:ListItem>
<asp:ListItem>971</asp:ListItem>
<asp:ListItem>972</asp:ListItem>
<asp:ListItem>973</asp:ListItem>
<asp:ListItem>974</asp:ListItem>
<asp:ListItem>975</asp:ListItem>
<asp:ListItem>976</asp:ListItem>
<asp:ListItem>977</asp:ListItem>
<asp:ListItem>992</asp:ListItem>
<asp:ListItem>993</asp:ListItem>
<asp:ListItem>994</asp:ListItem>
<asp:ListItem>995</asp:ListItem>
<asp:ListItem>996</asp:ListItem>
<asp:ListItem>998</asp:ListItem>
<asp:ListItem>1242</asp:ListItem>
<asp:ListItem>1246</asp:ListItem>
<asp:ListItem>1264</asp:ListItem>
<asp:ListItem>1268</asp:ListItem>
<asp:ListItem>1284</asp:ListItem>
<asp:ListItem>1340</asp:ListItem>
<asp:ListItem>1345</asp:ListItem>
<asp:ListItem>1441</asp:ListItem>
<asp:ListItem>1473</asp:ListItem>
<asp:ListItem>1599</asp:ListItem>
<asp:ListItem>1649</asp:ListItem>
<asp:ListItem>1664</asp:ListItem>
<asp:ListItem>1670</asp:ListItem>
<asp:ListItem>1671</asp:ListItem>
<asp:ListItem>1684</asp:ListItem>
<asp:ListItem>1758</asp:ListItem>
<asp:ListItem>1767</asp:ListItem>
<asp:ListItem>1784</asp:ListItem>
<asp:ListItem>1809</asp:ListItem>
<asp:ListItem>1868</asp:ListItem>
<asp:ListItem>1869</asp:ListItem>
<asp:ListItem>1876</asp:ListItem>
 <asp:ListItem>00420</asp:ListItem>
                        </asp:DropDownList>                                             
                                            </div>
                                            <div class="drpext-inputbox" >
                                            <asp:TextBox ID="txtPhonePc2" runat="server" CssClass="inputbox4" MaxLength="15"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtPhonePc2"
                                                FilterType="Numbers">
                                            </asp:FilteredTextBoxExtender>
                                            </div>
                                    </div>
                                </div>
                                <div class="contact-details-row">
                                    <div class="contact-details-row-left1">
                                        Email<span style="color: Red"> *</span>
                                    </div>
                                    <div class="contact-details-row-right">
                                        <asp:TextBox ID="txtEmailPc2" runat="server" CssClass="textfield2" MaxLength="200"></asp:TextBox>
                                    </div>
                                </div>
                                <asp:HiddenField ID="hidPc2" runat="server" />
                            </fieldset>
                            <fieldset class="inner" id="ViewPc2" runat="server">
                                <asp:DataList ID="dlistPrimaryContact2" RepeatColumns="2" RepeatDirection="Horizontal"
                                    DataKeyField="Id" runat="server">
                                    <%-- Create a DataList control template named "ItemTemplate". --%>
                                    <ItemTemplate>
                                        <legend>Primary Contact</legend>
                                        <div style="float: right">
                                            <asp:LinkButton ID="lnbPrimaryCnt2" runat="server" CommandName="SelectItem" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id")%>'
                                                OnClick="lnbPrimaryCnt2_Click">Modify</asp:LinkButton></div>
                                        <div style="clear: both">
                                        </div>
                                        <div class="contact-details-row1">
                                            <div class="contact-details-row-left">
                                                Job title :
                                            </div>
                                            <div class="contact-details-row-right">
                                                <%# DataBinder.Eval(Container.DataItem, "JobTitle")%>
                                            </div>
                                        </div>
                                        <div class="contact-details-row1">
                                            <div class="contact-details-row-left">
                                                First name :
                                            </div>
                                            <div class="contact-details-row-right">
                                                <%# DataBinder.Eval(Container.DataItem, "FirstName")%>
                                            </div>
                                        </div>
                                        <div class="contact-details-row1">
                                            <div class="contact-details-row-left">
                                                Last name :
                                            </div>
                                            <div class="contact-details-row-right">
                                                <%# DataBinder.Eval(Container.DataItem, "LastName")%>
                                            </div>
                                        </div>
                                        <div class="contact-details-row1">
                                            <div class="contact-details-row-left">
                                                Phone :
                                            </div>
                                            <div class="contact-details-row-right">
                                                <%# DataBinder.Eval(Container.DataItem, "Ext")%>
                                                <%# DataBinder.Eval(Container.DataItem, "Phone")%>
                                            </div>
                                        </div>
                                        <div class="contact-details-row1">
                                            <div class="contact-details-row-left">
                                                Email :
                                            </div>
                                            <div class="contact-details-row-right">
                                                <%# DataBinder.Eval(Container.DataItem, "Email")%>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </fieldset><br />
                        
                        </div>
                        
                            
                        <div class="contact-details-row-bttom" id="DivSecondarywe" runat="server">
                            <asp:LinkButton ID="lnbSecCon2" runat="server" OnClick="lnbSecCon2_Click">Add secondary weekend contact</asp:LinkButton>
                        </div>
                        <br />
                        <div class="contact-details-right" id="divWeekendSec" runat="server" visible="false" ><br />
                            <fieldset class="big" id="NewSc2" runat="server">
                                <legend>Secondary Contact</legend>
                                <div class="contact-details-row">
                                    <div class="contact-details-row-left1">
                                        Job title<span style="color: Red"> *</span>
                                    </div>
                                    <div class="contact-details-row-right">
                                        <asp:TextBox ID="txtJobTitleSc2" runat="server" CssClass="textfield2" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="contact-details-row">
                                    <div class="contact-details-row-left1">
                                        First name<span style="color: Red"> *</span>
                                    </div>
                                    <div class="contact-details-row-right">
                                        <asp:TextBox ID="txtFnameSc2" runat="server" CssClass="textfield2" MaxLength="20"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="contact-details-row">
                                    <div class="contact-details-row-left1">
                                        Last name<span style="color: Red"> *</span>
                                    </div>
                                    <div class="contact-details-row-right">
                                        <asp:TextBox ID="txtLnameSc2" runat="server" CssClass="textfield2" MaxLength="20"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="contact-details-row2 clearfix">
                                    <div class="contact-details-row-left1">
                                        Phone<span style="color: Red"> *</span>
                                    </div>
                                     <div class="contact-details-row-right2 clearfix">
                                            <div class="drpext">                                           
                                            <asp:DropDownList ID="drpExtPhoneSc2" runat="server" CssClass="NoClassApply" Width="80%">
                         <asp:ListItem Value="0">Extension</asp:ListItem>
<asp:ListItem>1</asp:ListItem>
<asp:ListItem>7</asp:ListItem>
<asp:ListItem>20</asp:ListItem>
<asp:ListItem>27</asp:ListItem>
<asp:ListItem>30</asp:ListItem>
<asp:ListItem>31</asp:ListItem>
<asp:ListItem>32</asp:ListItem>
<asp:ListItem>33</asp:ListItem>
<asp:ListItem>34</asp:ListItem>
<asp:ListItem>36</asp:ListItem>
<asp:ListItem>39</asp:ListItem>
<asp:ListItem>40</asp:ListItem>
<asp:ListItem>41</asp:ListItem>
<asp:ListItem>43</asp:ListItem>
<asp:ListItem>44</asp:ListItem>
<asp:ListItem>45</asp:ListItem>
<asp:ListItem>46</asp:ListItem>
<asp:ListItem>47</asp:ListItem>
<asp:ListItem>48</asp:ListItem>
<asp:ListItem>49</asp:ListItem>
<asp:ListItem>51</asp:ListItem>
<asp:ListItem>52</asp:ListItem>
<asp:ListItem>53</asp:ListItem>
<asp:ListItem>54</asp:ListItem>
<asp:ListItem>55</asp:ListItem>
<asp:ListItem>56</asp:ListItem>
<asp:ListItem>57</asp:ListItem>
<asp:ListItem>58</asp:ListItem>
<asp:ListItem>60</asp:ListItem>
<asp:ListItem>61</asp:ListItem>
<asp:ListItem>62</asp:ListItem>
<asp:ListItem>63</asp:ListItem>
<asp:ListItem>64</asp:ListItem>
<asp:ListItem>65</asp:ListItem>
<asp:ListItem>66</asp:ListItem>
<asp:ListItem>81</asp:ListItem>
<asp:ListItem>82</asp:ListItem>
<asp:ListItem>84</asp:ListItem>
<asp:ListItem>86</asp:ListItem>
<asp:ListItem>90</asp:ListItem>
<asp:ListItem>91</asp:ListItem>
<asp:ListItem>92</asp:ListItem>
<asp:ListItem>93</asp:ListItem>
<asp:ListItem>94</asp:ListItem>
<asp:ListItem>95</asp:ListItem>
<asp:ListItem>98</asp:ListItem>
<asp:ListItem>212</asp:ListItem>
<asp:ListItem>213</asp:ListItem>
<asp:ListItem>216</asp:ListItem>
<asp:ListItem>218</asp:ListItem>
<asp:ListItem>220</asp:ListItem>
<asp:ListItem>221</asp:ListItem>
<asp:ListItem>222</asp:ListItem>
<asp:ListItem>223</asp:ListItem>
<asp:ListItem>224</asp:ListItem>
<asp:ListItem>225</asp:ListItem>
<asp:ListItem>226</asp:ListItem>
<asp:ListItem>227</asp:ListItem>
<asp:ListItem>228</asp:ListItem>
<asp:ListItem>229</asp:ListItem>
<asp:ListItem>230</asp:ListItem>
<asp:ListItem>231</asp:ListItem>
<asp:ListItem>232</asp:ListItem>
<asp:ListItem>233</asp:ListItem>
<asp:ListItem>234</asp:ListItem>
<asp:ListItem>235</asp:ListItem>
<asp:ListItem>236</asp:ListItem>
<asp:ListItem>237</asp:ListItem>
<asp:ListItem>238</asp:ListItem>
<asp:ListItem>239</asp:ListItem>
<asp:ListItem>240</asp:ListItem>
<asp:ListItem>241</asp:ListItem>
<asp:ListItem>242</asp:ListItem>
<asp:ListItem>243</asp:ListItem>
<asp:ListItem>244</asp:ListItem>
<asp:ListItem>245</asp:ListItem>
<asp:ListItem>248</asp:ListItem>
<asp:ListItem>249</asp:ListItem>
<asp:ListItem>250</asp:ListItem>
<asp:ListItem>251</asp:ListItem>
<asp:ListItem>252</asp:ListItem>
<asp:ListItem>253</asp:ListItem>
<asp:ListItem>254</asp:ListItem>
<asp:ListItem>255</asp:ListItem>
<asp:ListItem>256</asp:ListItem>
<asp:ListItem>257</asp:ListItem>
<asp:ListItem>258</asp:ListItem>
<asp:ListItem>260</asp:ListItem>
<asp:ListItem>261</asp:ListItem>
<asp:ListItem>262</asp:ListItem>
<asp:ListItem>263</asp:ListItem>
<asp:ListItem>264</asp:ListItem>
<asp:ListItem>265</asp:ListItem>
<asp:ListItem>266</asp:ListItem>
<asp:ListItem>267</asp:ListItem>
<asp:ListItem>268</asp:ListItem>
<asp:ListItem>269</asp:ListItem>
<asp:ListItem>290</asp:ListItem>
<asp:ListItem>291</asp:ListItem>
<asp:ListItem>297</asp:ListItem>
<asp:ListItem>298</asp:ListItem>
<asp:ListItem>299</asp:ListItem>
<asp:ListItem>350</asp:ListItem>
<asp:ListItem>351</asp:ListItem>
<asp:ListItem>352</asp:ListItem>
<asp:ListItem>353</asp:ListItem>
<asp:ListItem>354</asp:ListItem>
<asp:ListItem>355</asp:ListItem>
<asp:ListItem>356</asp:ListItem>
<asp:ListItem>357</asp:ListItem>
<asp:ListItem>358</asp:ListItem>
<asp:ListItem>359</asp:ListItem>
<asp:ListItem>370</asp:ListItem>
<asp:ListItem>371</asp:ListItem>
<asp:ListItem>372</asp:ListItem>
<asp:ListItem>373</asp:ListItem>
<asp:ListItem>374</asp:ListItem>
<asp:ListItem>375</asp:ListItem>
<asp:ListItem>376</asp:ListItem>
<asp:ListItem>377</asp:ListItem>
<asp:ListItem>378</asp:ListItem>
<asp:ListItem>380</asp:ListItem>
<asp:ListItem>381</asp:ListItem>
<asp:ListItem>382</asp:ListItem>
<asp:ListItem>385</asp:ListItem>
<asp:ListItem>386</asp:ListItem>
<asp:ListItem>387</asp:ListItem>
<asp:ListItem>389</asp:ListItem>
<asp:ListItem>420</asp:ListItem>
<asp:ListItem>421</asp:ListItem>
<asp:ListItem>423</asp:ListItem>
<asp:ListItem>500</asp:ListItem>
<asp:ListItem>501</asp:ListItem>
<asp:ListItem>502</asp:ListItem>
<asp:ListItem>503</asp:ListItem>
<asp:ListItem>504</asp:ListItem>
<asp:ListItem>505</asp:ListItem>
<asp:ListItem>506</asp:ListItem>
<asp:ListItem>507</asp:ListItem>
<asp:ListItem>508</asp:ListItem>
<asp:ListItem>509</asp:ListItem>
<asp:ListItem>590</asp:ListItem>
<asp:ListItem>591</asp:ListItem>
<asp:ListItem>592</asp:ListItem>
<asp:ListItem>593</asp:ListItem>
<asp:ListItem>595</asp:ListItem>
<asp:ListItem>597</asp:ListItem>
<asp:ListItem>598</asp:ListItem>
<asp:ListItem>599</asp:ListItem>
<asp:ListItem>670</asp:ListItem>
<asp:ListItem>672</asp:ListItem>
<asp:ListItem>673</asp:ListItem>
<asp:ListItem>674</asp:ListItem>
<asp:ListItem>675</asp:ListItem>
<asp:ListItem>676</asp:ListItem>
<asp:ListItem>677</asp:ListItem>
<asp:ListItem>678</asp:ListItem>
<asp:ListItem>679</asp:ListItem>
<asp:ListItem>680</asp:ListItem>
<asp:ListItem>681</asp:ListItem>
<asp:ListItem>682</asp:ListItem>
<asp:ListItem>683</asp:ListItem>
<asp:ListItem>685</asp:ListItem>
<asp:ListItem>686</asp:ListItem>
<asp:ListItem>687</asp:ListItem>
<asp:ListItem>688</asp:ListItem>
<asp:ListItem>689</asp:ListItem>
<asp:ListItem>690</asp:ListItem>
<asp:ListItem>691</asp:ListItem>
<asp:ListItem>692</asp:ListItem>
<asp:ListItem>850</asp:ListItem>
<asp:ListItem>852</asp:ListItem>
<asp:ListItem>853</asp:ListItem>
<asp:ListItem>855</asp:ListItem>
<asp:ListItem>856</asp:ListItem>
<asp:ListItem>870</asp:ListItem>
<asp:ListItem>880</asp:ListItem>
<asp:ListItem>886</asp:ListItem>
<asp:ListItem>960</asp:ListItem>
<asp:ListItem>961</asp:ListItem>
<asp:ListItem>962</asp:ListItem>
<asp:ListItem>963</asp:ListItem>
<asp:ListItem>964</asp:ListItem>
<asp:ListItem>965</asp:ListItem>
<asp:ListItem>966</asp:ListItem>
<asp:ListItem>967</asp:ListItem>
<asp:ListItem>968</asp:ListItem>
<asp:ListItem>970</asp:ListItem>
<asp:ListItem>971</asp:ListItem>
<asp:ListItem>972</asp:ListItem>
<asp:ListItem>973</asp:ListItem>
<asp:ListItem>974</asp:ListItem>
<asp:ListItem>975</asp:ListItem>
<asp:ListItem>976</asp:ListItem>
<asp:ListItem>977</asp:ListItem>
<asp:ListItem>992</asp:ListItem>
<asp:ListItem>993</asp:ListItem>
<asp:ListItem>994</asp:ListItem>
<asp:ListItem>995</asp:ListItem>
<asp:ListItem>996</asp:ListItem>
<asp:ListItem>998</asp:ListItem>
<asp:ListItem>1242</asp:ListItem>
<asp:ListItem>1246</asp:ListItem>
<asp:ListItem>1264</asp:ListItem>
<asp:ListItem>1268</asp:ListItem>
<asp:ListItem>1284</asp:ListItem>
<asp:ListItem>1340</asp:ListItem>
<asp:ListItem>1345</asp:ListItem>
<asp:ListItem>1441</asp:ListItem>
<asp:ListItem>1473</asp:ListItem>
<asp:ListItem>1599</asp:ListItem>
<asp:ListItem>1649</asp:ListItem>
<asp:ListItem>1664</asp:ListItem>
<asp:ListItem>1670</asp:ListItem>
<asp:ListItem>1671</asp:ListItem>
<asp:ListItem>1684</asp:ListItem>
<asp:ListItem>1758</asp:ListItem>
<asp:ListItem>1767</asp:ListItem>
<asp:ListItem>1784</asp:ListItem>
<asp:ListItem>1809</asp:ListItem>
<asp:ListItem>1868</asp:ListItem>
<asp:ListItem>1869</asp:ListItem>
<asp:ListItem>1876</asp:ListItem>
 <asp:ListItem>00420</asp:ListItem>
                        </asp:DropDownList>                                             
                                            </div>
                                            <div class="drpext-inputbox" >
                                            <asp:TextBox ID="txtPhoneSc2" runat="server" CssClass="inputbox4" MaxLength="15"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtPhoneSc2"
                                                FilterType="Numbers">
                                            </asp:FilteredTextBoxExtender>
                                            </div>
                                        
                                    </div>
                                </div>
                                <div class="contact-details-row">
                                    <div class="contact-details-row-left1">
                                        Email<span style="color: Red"> *</span>
                                    </div>
                                    <div class="contact-details-row-right">
                                        <asp:TextBox ID="txtEmailSc2" runat="server" CssClass="textfield2" MaxLength="200"></asp:TextBox>
                                    </div>
                                </div>
                                <asp:HiddenField ID="hidSc2" runat="server" />
                            </fieldset>
                            <fieldset class="inner" id="ViewSc2" runat="server">
                                <asp:DataList ID="dlistSecondaryContact2" RepeatColumns="2" RepeatDirection="Horizontal"
                                    runat="server">
                                    <%-- Create a DataList control template named "ItemTemplate". --%>
                                    <ItemTemplate>
                                        <legend>Secondary Contact</legend>
                                        <div style="float: right">
                                            <asp:LinkButton ID="lnbSecondaryCnt2" runat="server" CommandName="SelectItem" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id")%>'
                                                OnClick="lnbSecondaryCnt2_Click">Modify</asp:LinkButton></div>
                                        <div style="clear: both">
                                        </div>
                                        <div class="contact-details-row1">
                                            <div class="contact-details-row-left">
                                                Job title :
                                            </div>
                                            <div class="contact-details-row-right">
                                                <%# DataBinder.Eval(Container.DataItem, "JobTitle")%>
                                            </div>
                                        </div>
                                        <div class="contact-details-row1">
                                            <div class="contact-details-row-left">
                                                First name :
                                            </div>
                                            <div class="contact-details-row-right">
                                                <%# DataBinder.Eval(Container.DataItem, "FirstName")%>
                                            </div>
                                        </div>
                                        <div class="contact-details-row1">
                                            <div class="contact-details-row-left">
                                                Last name :
                                            </div>
                                            <div class="contact-details-row-right">
                                                <%# DataBinder.Eval(Container.DataItem, "LastName")%>
                                            </div>
                                        </div>
                                        <div class="contact-details-row1">
                                            <div class="contact-details-row-left">
                                                Phone :
                                            </div>
                                            <div class="contact-details-row-right">
                                                <%# DataBinder.Eval(Container.DataItem, "Ext")%>
                                                <%# DataBinder.Eval(Container.DataItem, "Phone")%>
                                            </div>
                                        </div>
                                        <div class="contact-details-row1">
                                            <div class="contact-details-row-left">
                                                Email :
                                            </div>
                                            <div class="contact-details-row-right">
                                                <%# DataBinder.Eval(Container.DataItem, "Email")%>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </fieldset>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="booking-details" style="width:760px;">
                <ul>
                <li class="value10">
                    <div class="col21" style="width:752px;">
                    <div class="button_section">
                            <asp:LinkButton ID="btnSubmit" runat="server" Text="Save" OnClick="btnSubmit_Click"
                        CssClass="select" OnClientClick="javascript:return validatePage();" />
                    <span>or</span>
                    <asp:LinkButton ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                        </div>   
                        </div>
                </li>
                </ul></div>       
    <div class="col21" id="divNext" runat="server" visible="false">
        <div class="button_sectionNext">
            <asp:LinkButton ID="btnNext" runat="server" class="RemoveCookie" Text="Next &gt;&gt;" OnClick="btnNext_Click" />
        </div>
    </div>
    <div id="Loding_overlaySec">
        <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br /><span>Saving...</span></div>        
    <script language="javascript" type="text/javascript">
                             jQuery(document).ready(function () {
                            jQuery("#<%= btnSubmit.ClientID %>").bind("click", function () {
                                if (jQuery("#<%= divmessage.ClientID %>").hasClass("succesfuly")) {
                                    jQuery("#<%= divmessage.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                                }
                                var isvalid = true;
                                var errormessage = "";
                                <% if(NewPC1.Visible == true){ %>
                                    var titelp1 = jQuery("#<%= txtJobTitlePc1.ClientID %>").val();
                                    
                                    if (titelp1.length <= 0) {
                                        if (errormessage.length > 0) {
                                            errormessage += "<br/>";
                                        }
                                        errormessage += "Primary Job Title is required.(weekdays)";
                                        isvalid = false;
                                    }

                                    var firstnamep1 = jQuery("#<%= txtFnamePc1.ClientID %>").val();
                                    if (firstnamep1.length <= 0) {
                                        if (errormessage.length > 0) {
                                            errormessage += "<br/>";
                                        }
                                        errormessage += "Primary First name is required.(weekdays)";
                                        isvalid = false;
                                    }

                                    var lastnamep1 = jQuery("#<%= txtLnamePc1.ClientID %>").val();
                                    if (lastnamep1.length <= 0) {
                                        if (errormessage.length > 0) {
                                            errormessage += "<br/>";
                                        }
                                        errormessage += "Primary Last name is required.(weekdays)";
                                        isvalid = false;
                                    }

                                    var phoneext = jQuery("#<%= drpExtPhone1.ClientID %>").val(); 
                                    if (phoneext == "0") {
                                        if (errormessage.length > 0) {
                                            errormessage += "<br/>";
                                        }
                                        errormessage += "Please select phone extension.(weekdays)";
                                        isvalid = false;
                                    }

                                    var phonep1 = jQuery("#<%= txtPhonePc1.ClientID %>").val();
                                    if (phonep1.length <= 0) {
                                        if (errormessage.length > 0) {
                                            errormessage += "<br/>";
                                        }
                                        errormessage += "Primary Phone number is required.(weekdays)";
                                        isvalid = false;
                                    }

                                    var emailp1 = jQuery("#<%= txtEmailPc1.ClientID %>").val();
                                    if (emailp1.length <= 0) {
                                        if (errormessage.length > 0) {
                                            errormessage += "<br/>";
                                        }
                                        errormessage += "Primary Email is required.(weekdays)";
                                        isvalid = false;
                                    }
                                    else{
                                    if (!validateEmail(emailp1)) {
                     if (errormessage.length > 0) {
                         errormessage += "<br/>";
                     }
                     errormessage += "Enter valid primary email.(weekdays)";
                     isvalid = false;
                 }
                                    }
                                <%} %>
                                <% if(NewSc1.Visible == true){ %>
                                    var titels1 = jQuery("#<%= txtJobTitleSc1.ClientID %>").val();
                                    if (titels1.length <= 0) {
                                        if (errormessage.length > 0) {
                                            errormessage += "<br/>";
                                        }
                                        errormessage += "Secondary Job Title is required.(weekdays)";
                                        isvalid = false;
                                    }

                                    var firstnames1 = jQuery("#<%= txtFnameSc1.ClientID %>").val();
                                    if (firstnames1.length <= 0) {
                                        if (errormessage.length > 0) {
                                            errormessage += "<br/>";
                                        }
                                        errormessage += "Secondary First name is required.(weekdays)";
                                        isvalid = false;
                                    }

                                    var lastnames1 = jQuery("#<%= txtLnameSc1.ClientID %>").val();
                                    if (lastnames1.length <= 0) {
                                        if (errormessage.length > 0) {
                                            errormessage += "<br/>";
                                        }
                                        errormessage += "Secondary Last name is required.(weekdays)";
                                        isvalid = false;
                                    }

                                    var phoneext = jQuery("#<%= drpExtPhoneSc1.ClientID %>").val(); 
                                    if (phoneext == "0") {
                                        if (errormessage.length > 0) {
                                            errormessage += "<br/>";
                                        }
                                        errormessage += "Please select phone extension.(weekdays)";
                                        isvalid = false;
                                    }                                   

                                    var phones1 = jQuery("#<%= txtPhoneSc1.ClientID %>").val();
                                    if (phones1.length <= 0) {
                                        if (errormessage.length > 0) {
                                            errormessage += "<br/>";
                                        }
                                        errormessage += "Secondary Phone number is required.(weekdays)";
                                        isvalid = false;
                                    }

                                    var emails1 = jQuery("#<%= txtEmailSc1.ClientID %>").val();
                                    if (emails1.length <= 0) {
                                        if (errormessage.length > 0) {
                                            errormessage += "<br/>";
                                        }
                                        errormessage += "Secondary Email is required.(weekdays)";
                                        isvalid = false;
                                    }
                                    else{
                                    if (!validateEmail(emails1)) {
                     if (errormessage.length > 0) {
                         errormessage += "<br/>";
                     }
                     errormessage += "Enter valid Secondary email.(weekdays)";
                     isvalid = false;
                 }
                                    }
                                <%} %>
                                <% if(NewPC2.Visible == true){ %>
                                 var titlep2 = jQuery("#<%= txtJobTitlePc2.ClientID %>").val();
                                    if (titlep2.length <= 0) {
                                        if (errormessage.length > 0) {
                                            errormessage += "<br/>";
                                        }
                                        errormessage += "Primary Job Title is required.(weekends)";
                                        isvalid = false;
                                    }

                                    var firstnamep2 = jQuery("#<%= txtFnamePc2.ClientID %>").val();
                                    if (firstnamep2.length <= 0) {
                                        if (errormessage.length > 0) {
                                            errormessage += "<br/>";
                                        }
                                        errormessage += "Primary First name is required.(weekends)";
                                        isvalid = false;
                                    }

                                    var lastnamep2 = jQuery("#<%= txtLnamePc2.ClientID %>").val();
                                    if (lastnamep2.length <= 0) {
                                        if (errormessage.length > 0) {
                                            errormessage += "<br/>";
                                        }
                                        errormessage += "Primary Last name is required.(weekends)";
                                        isvalid = false;
                                    }

                                    var phoneext = jQuery("#<%= drpExtPhonePc2.ClientID %>").val(); 
                                    if (phoneext == "0") {
                                        if (errormessage.length > 0) {
                                            errormessage += "<br/>";
                                        }
                                        errormessage += "Please select phone extension.(weekends)";
                                        isvalid = false;
                                    }                                    

                                    var phonep2 = jQuery("#<%= txtPhonePc2.ClientID %>").val();
                                    if (phonep2.length <= 0) {
                                        if (errormessage.length > 0) {
                                            errormessage += "<br/>";
                                        }
                                        errormessage += "Primary Phone number is required.(weekends)";
                                        isvalid = false;
                                    }

                                    var emailp2 = jQuery("#<%= txtEmailPc2.ClientID %>").val();
                                    if (emailp2.length <= 0) {
                                        if (errormessage.length > 0) {
                                            errormessage += "<br/>";
                                        }
                                        errormessage += "Primary Email is required.(weekends)";
                                        isvalid = false;
                                    }
                                    else{
                                    if (!validateEmail(emailp2)) {
                     if (errormessage.length > 0) {
                         errormessage += "<br/>";
                     }
                     errormessage += "Enter valid primary email.(weekends)";
                     isvalid = false;
                 }
                                    }
                                <% } %>
                                <% if(NewSc2.Visible == true){ %>
                                    var titles2 = jQuery("#<%= txtJobTitleSc2.ClientID %>").val();
                                    if (titles2.length <= 0) {
                                        if (errormessage.length > 0) {
                                            errormessage += "<br/>";
                                        }
                                        errormessage += "Secondary Job Title is required.(weekends)";
                                        isvalid = false;
                                    }

                                    var firstnames2 = jQuery("#<%= txtFnameSc2.ClientID %>").val();
                                    if (firstnames2.length <= 0) {
                                        if (errormessage.length > 0) {
                                            errormessage += "<br/>";
                                        }
                                        errormessage += "Secondary First name is required.(weekends)";
                                        isvalid = false;
                                    }

                                    var lastnames2 = jQuery("#<%= txtLnameSc2.ClientID %>").val();
                                    if (lastnames2.length <= 0) {
                                        if (errormessage.length > 0) {
                                            errormessage += "<br/>";
                                        }
                                        errormessage += "Secondary Last name is required.(weekends)";
                                        isvalid = false;
                                    }

                                    var phoneext = jQuery("#<%= drpExtPhoneSc2.ClientID %>").val(); 
                                    if (phoneext == "0") {
                                        if (errormessage.length > 0) {
                                            errormessage += "<br/>";
                                        }
                                        errormessage += "Please select phone extension.(weekends)";
                                        isvalid = false;
                                    }
                                   
                                    var phones2 = jQuery("#<%= txtPhoneSc2.ClientID %>").val();
                                    if (phones2.length <= 0) {
                                        if (errormessage.length > 0) {
                                            errormessage += "<br/>";
                                        }
                                        errormessage += "Secondary Phone number is required.(weekends)";
                                        isvalid = false;
                                    }

                                    var emails2 = jQuery("#<%= txtEmailSc2.ClientID %>").val();
                                    if (emails2.length <= 0) {
                                        if (errormessage.length > 0) {
                                            errormessage += "<br/>";
                                        }
                                        errormessage += "Secondary Email is required.(weekends)";
                                        isvalid = false;
                                    }
                                    else{
                                    if (!validateEmail(emails2)) {
                     if (errormessage.length > 0) {
                         errormessage += "<br/>";
                     }
                     errormessage += "Enter valid Secondary email.(weekends)";
                     isvalid = false;
                 }
                                    }
                                <% } %>
                                if (!isvalid) {
                                    jQuery("#<%= divmessage.ClientID %>").show();
                                    jQuery("#<%= divmessage.ClientID %>").html(errormessage);
                                    var offseterror = jQuery(".error").offset();
                                    jQuery("body").scrollTop(offseterror.top);
                                    jQuery("html").scrollTop(offseterror.top);
                                    return false;
                                }
                                jQuery(".error").html("");
                                jQuery(".error").hide();
                                jQuery("#Loding_overlaySec span").html("Saving...");
                                jQuery("#Loding_overlaySec").show();
                                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                            });
                        });

                         function validateEmail(txtEmail) {
            var a = txtEmail;
            var filter = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
            if (filter.test(a)) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#<%= lnbCopyPc.ClientID %>").bind("click", function () {
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                jQuery("#Loding_overlaySec span").html("Loading...");
                jQuery("#Loding_overlaySec").show();
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            });
            jQuery("#<%= lnbCopySc.ClientID %>").bind("click", function () {
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                jQuery("#Loding_overlaySec span").html("Loading...");
                jQuery("#Loding_overlaySec").show();
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            });
            jQuery("#<%= btnNext.ClientID %>").bind("click", function () {
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                jQuery("#Loding_overlaySec span").html("Loading...");
                jQuery("#Loding_overlaySec").show();
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            });
        });
    </script>
    <!-- start rightside_inner_container-->
</asp:Content>
