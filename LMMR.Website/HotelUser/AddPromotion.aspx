﻿<%@ Page Language="C#" MasterPageFile="~/HotelUser/HotelMain.master" AutoEventWireup="true" CodeFile="AddPromotion.aspx.cs" Title="Hotel User - Add promotion" Inherits="HotelUser_AddPromotion" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" Runat="Server">
    <h1>Request Hotel of the week for : <b><asp:Label ID="lblHotel" runat="server" Text=""></asp:Label></b></h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" Runat="Server">
    <div class="booking-details">
    <div id="divmessage" runat="server"></div>
        <ul>
            <li class="value8">
                <div class="col17">
                    <strong>From</strong><span style="color: Red"> *</span></div>
                <div class="col18">
                    <asp:TextBox ID="txtFrom" runat="server" CssClass="textfield" Text="dd/mm/yy" Width="200px" MaxLength="20" ></asp:TextBox><asp:CalendarExtender ID="CalendarExtender2" runat="server" 
                            TargetControlID="txtFrom" PopupButtonID="calFrom" Format="dd/MM/yy">
                        </asp:CalendarExtender><input type="image" src="../Images/cal-2.png" id="calFrom" tabindex="1"/></div>
            </li>
            <li class="value9">
                <div class="col17">
                    <strong>To</strong><span style="color: Red"> *</span></div>
                <div class="col18">
                    <asp:TextBox ID="txtTo" runat="server" CssClass="textfield" Text="dd/mm/yy" Width="200px" MaxLength="20" ></asp:TextBox><asp:CalendarExtender ID="CalendarExtender1" runat="server" 
                            TargetControlID="txtTo" PopupButtonID="calTo" Format="dd/MM/yy">
                        </asp:CalendarExtender><input type="image" src="../Images/cal-2.png" id="calTo" tabindex="1"/></div>
            </li>
            <li class="value8">
                <div class="col17">
                    <strong>Attachment</strong><span style="color: Red"> *</span></div>
                <div class="col18"><asp:FileUpload runat="server" ID="flAttachment" Width="200px" TabIndex="1" /><br /><span style="font-family: Arial; font-size: Smaller;">File format: JPG,JPEG,PNG,GIF,PDF,DOC,DOCX
                            & max size 1MB. </span></div>
            </li>
            <li class="value9">
                <div class="col17">
                    <strong>Description</strong><span style="color: Red"> *</span></div>
                <div class="col19">
                    <asp:TextBox ID="txtBody" runat="server" CssClass="textarea" TextMode="MultiLine" MaxLength="250" TabIndex="1" ></asp:TextBox>
                    <span id="lblCount" style="font-family: Arial; font-size: Smaller;">Description should
                            be less than 250 characters.</span>
                </div>
            </li>
            <li class="value10">
               <div class="col21">
                    <div class="button_section">
                        <asp:LinkButton ID="btnSubmit" runat="server" Text="Save" CssClass="select"
                            ValidationGroup="LOGIN" onclick="btnSubmit_Click" /> <span>or</span>
                        <asp:LinkButton ID="btnCancel" runat="server" Text="Cancel"  />
                    </div>
                </div>
            </li>
        </ul>
    </div>
     
                <div id="Loding_overlay">
        <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />Saving...</div>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            var MaxLength = 250;
            jQuery('#<%=txtBody.ClientID %>').keyup(function (e) {
                var total = parseInt(jQuery(this).val().length,10);
                jQuery("#lblCount").html('Characters entered <b>' + total + '</b> out of 250.');
            });
        });
        jQuery(document).ready(function () {
            jQuery("#contentbody_txtFrom").attr("disabled", true);
            jQuery("#contentbody_txtTo").attr("disabled", true);
            jQuery("#contentbody_btnSubmit").bind("click", function () {
                if (jQuery("#<%= divmessage.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= divmessage.ClientID %>").removeClass("succesfuly").addClass("error");
                }
                var errormessage = "";
                var isvalid = true;
                if (jQuery("#contentbody_txtFrom").val() == "dd/mm/yy" || jQuery("#contentbody_txtFrom").val() == null || jQuery("#contentbody_txtFrom").val() == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "From date is required.";
                    isvalid = false;
                }
                if (jQuery("#contentbody_txtTo").val() == "dd/mm/yy" || jQuery("#contentbody_txtTo").val() == null || jQuery("#contentbody_txtTo").val() == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "To date is required.";
                    isvalid = false;
                }
                var fromdate = jQuery("#contentbody_txtFrom").val();
                var todate = jQuery("#contentbody_txtTo").val();
                var todayArr = todate.split('/');
                var formdayArr = fromdate.split('/');
                var fromdatecheck = new Date();
                var todatecheck = new Date();
                fromdatecheck.setFullYear(parseInt("20" + formdayArr[2],10), (parseInt(formdayArr[1],10) - 1), formdayArr[0]);
                todatecheck.setFullYear(parseInt("20" + todayArr[2],10), (parseInt(todayArr[1],10) - 1), todayArr[0]);
                if (fromdatecheck > todatecheck) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "From date must be less than To date.";
                    isvalid = false;
                }
                var file = jQuery('#<%= flAttachment.ClientID %>').val();

                if (file.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Attachment is required.";
                    isvalid = false;
                }
                else {
                    if (!validate_file_format(file, "gif,GIF,Gif,png,Png,PNG,jpg,Jpg,JPG,jpeg,Jpeg,JPEG,PDF,Pdf,pdf,DOC,Doc,doc,DOCX,Docx,docx")) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += "Please select file in (Gif, JPG, JPEG, PNG, PDF, DOC and DOCX) formats only.";
                        isvalid = false;
                    }
                }
                var address = jQuery("#<%= txtBody.ClientID %>").val();
                if (address.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Description is Required.";
                    isvalid = false;
                }
                else if (address.length > 250) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Description must have 250 character.";
                    isvalid = false;
                }
                if (!isvalid) {
                    jQuery(".error").show();
                    jQuery(".error").html(errormessage);
                    return false;
                }
                jQuery(".error").html("");
                jQuery(".error").hide();
                jQuery("#contentbody_txtFrom").attr("disabled", false);
                jQuery("#contentbody_txtTo").attr("disabled", false);
                jQuery("#Loding_overlay").show();
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            });
        });

        function validate_file_format(file_name, allowed_ext) {            
            field_value = file_name;
            if (field_value != "") {
                var file_ext = (field_value.substring((field_value.lastIndexOf('.') + 1)).toLowerCase());
                ext = allowed_ext.split(',');
                var allow = 0;
                for (var i = 0; i < ext.length; i++) {
                    if (ext[i] == file_ext) {
                        allow = 1;
                    }
                }
                if (!allow) {
                    return false;
                }
                else {
                    return true;
                }
            }
            return false;
        } 
    </script>
</asp:Content>

