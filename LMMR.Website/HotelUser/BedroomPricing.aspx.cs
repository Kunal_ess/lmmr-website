﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using log4net;
using log4net.Config;
using System.Net;
#endregion
public partial class HotelUser_BedroomPricing : System.Web.UI.Page
{

    #region Variables and Properties
    UserManager objUserManager = new UserManager();
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_BedroomPricing));
    HotelManager objHotelManager = new HotelManager();
    BedroomPricingManager ObjBedroomPriceManage = new BedroomPricingManager();
    WizardLinkSettingManager ObjWizardLinkSetting = new WizardLinkSettingManager();
    DashboardManager ObjDashBoard = new DashboardManager();
    HotelInfo ObjHotelinfo = new HotelInfo();
    CurrencyManager objCurrency = new CurrencyManager();
    int intHotelID = 0;
    #endregion

    #region PageLoad
    /// <summary>
    /// This Method is used to Set all the initial Values of the page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["CurrentHotelID"] != null)
        {
            Session["LinkID"] = "lnkPropertyLevel";
            intHotelID = Convert.ToInt32(Session["CurrentHotelID"].ToString());
            lblHotel.Text = objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Name;
            if (!IsPostBack)
            {
                #region Leftmenu
                HyperLink lnk = (HyperLink)this.Master.FindControl("lnkConferenceInfo");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkContactDetails");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkFacilities");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkCancelationPolicy");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkDescriptionMeetingRoom");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkConfiguration");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkPrintFactSheet");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkPreviewOnWeb");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkDescriptionBedRoom");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkMeetingRooms");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkBedrooms");
                lnk.CssClass = "selected";
                #endregion
                BindLstViewBedroomPrice();
                BindCurrencySymbole();
                divBedRoomRoomPriceForm.Visible = false;
                Getgoonline();
            }
        }
        else
        {
            Response.Redirect("~/login.aspx", true);

        }

    }
    #endregion

    #region Fuctions
    /// <summary>
    /// For check goOnline status for current hotel.
    /// </summary>
    public void Getgoonline()
    {
        if (ObjHotelinfo.GetHotelGoOnline(intHotelID).Count > 0)
        {
            divNext.Visible = false;
            ViewState["Count"] = ObjHotelinfo.GetHotelGoOnline(intHotelID).Count;
        }
    }

    private void BindCurrencySymbole()
    {
        long intCountryID = 0;
        long intCurrencyID = 0;
        Hotel objHotel = objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"]));
        intCountryID = objHotel.CountryId;
        intCurrencyID = objHotel.CurrencyId;
        Currency objCurr = objCurrency.GetCurrencyDetailsByID(intCurrencyID);
        if ((Label)lstViewBedroomPricing.FindControl("lblCSSingle") != null)
        {
            ((Label)lstViewBedroomPricing.FindControl("lblCSSingle")).Text = "(" + WebUtility.HtmlDecode(objCurr.CurrencySignature) + ")";

        }
        if ((Label)lstViewBedroomPricing.FindControl("lblCSDouble") != null)
        {
            ((Label)lstViewBedroomPricing.FindControl("lblCSDouble")).Text = "(" + WebUtility.HtmlDecode(objCurr.CurrencySignature) + ")";

        }
        if ((Label)lstViewBedroomPricing.FindControl("lblCSBreakfast") != null)
        {
            ((Label)lstViewBedroomPricing.FindControl("lblCSBreakfast")).Text = "(" + WebUtility.HtmlDecode(objCurr.CurrencySignature) + ")";

        }
    }

    /// <summary>
    /// Bind bedroom list with listview control by hotelID.
    /// </summary>
    void BindLstViewBedroomPrice()
    {
        lstViewBedroomPricing.DataSource = ObjBedroomPriceManage.GetBedroomByHotelID(intHotelID);
        lstViewBedroomPricing.DataBind();

    }

    /// <summary>
    /// Fill form by bedroomID.
    /// </summary>
    /// <param name="bedroomID"></param>
    void FillForm(int bedroomID)
    {
        BedRoom ObjBedroom = ObjBedroomPriceManage.GedBedroomById(bedroomID);
        hdnBedroomId.Value = ObjBedroom.Id.ToString();
        lblBedroomType.Text = Enum.GetName(typeof(BedRoomType), Convert.ToInt32(ObjBedroom.Types));
        txtSingle.Text = ObjBedroom.PriceSingle.ToString();
        txtDouble.Text = ObjBedroom.PriceDouble.ToString();
        bool isBreakfast = Convert.ToBoolean(ObjBedroom.IsBreakFastInclude);
        if (isBreakfast == true)
        {
            rdIncl.Checked = true;
            rdExcl.Checked = false;
            rowBreakfast.Style.Add("display", "none");
            txtSupplyBreakfast.Text = "";
        }
        else if (isBreakfast == false)
        {
            rdExcl.Checked = true;
            rdIncl.Checked = false;
            rowBreakfast.Style.Add("display", "block");
            txtSupplyBreakfast.Text = ObjBedroom.BreakfastPrice.ToString();
        }

    }
    #endregion

    #region Events
    /// <summary>
    /// This event used for bind Item of bedroomprice listview.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lstViewBedroomPricing_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {

            ListViewDataItem currentItem = (ListViewDataItem)e.Item;
            DataKey currentDataKey = this.lstViewBedroomPricing.DataKeys[currentItem.DataItemIndex];
            int intBedroomID = Convert.ToInt32(currentDataKey.Value);

            Button btnModify = (Button)e.Item.FindControl("lnkModify");
            btnModify.OnClientClick = "ChangeRowColor('tr" + intBedroomID + "')";
            BedRoom room = e.Item.DataItem as BedRoom;
            Label lblBedroomType = (Label)e.Item.FindControl("lblBedroomType");
            Label lblSingle = (Label)e.Item.FindControl("lblSingle");
            Label lblDouble = (Label)e.Item.FindControl("lblDouble");
            Label lblBreakfast = (Label)e.Item.FindControl("lblBreakfast");
            Label lblSuplBreakfast = (Label)e.Item.FindControl("lblSuplBreakfast");
            lblBedroomType.Text = Enum.GetName(typeof(BedRoomType), room.Types);
            if (room.PriceSingle != null && room.PriceDouble != null)
            {
                lblSingle.Text = room.PriceSingle.ToString();
                lblDouble.Text = room.PriceDouble.ToString();
            }
            else
            {
                lblSingle.Text = "00";
                lblDouble.Text = "00";

            }
            if (room.IsBreakFastInclude)
            {
                lblBreakfast.Text = "Incl";
                lblSuplBreakfast.Text = "";
            }
            else
            {
                lblBreakfast.Text = "Excl";
                lblSuplBreakfast.Text = room.BreakfastPrice.ToString();

            }
        }
    }

    /// <summary>
    /// This event used for perform view command.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lstViewBedroomPricing_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {
            FillForm(Convert.ToInt32(e.CommandArgument));
            ScriptManager.RegisterStartupScript(this, typeof(Page), "colorChange", "CallMyMethod();", true);
            divBedRoomRoomPriceForm.Visible = true;
            divMessage.Style.Add("display", "none");
            divMessage.Attributes.Add("class", "error");
            pnlbedRoomPrice.DefaultButton = "btnUpdate";
        }
    }

    /// <summary>
    /// This event used for update existing data.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            hdnSelectedRowID.Value = "0";
            bool IsBreakfast = false;
            if (rdIncl.Checked == true)
            {
                IsBreakfast = true;
                BedRoom objBedroom = ObjBedroomPriceManage.GedBedroomById(Convert.ToInt32(hdnBedroomId.Value));
                objBedroom.HotelId = intHotelID;
                objBedroom.PriceSingle = Convert.ToDecimal(txtSingle.Text);
                objBedroom.PriceDouble = Convert.ToDecimal(txtDouble.Text);
                objBedroom.IsBreakFastInclude = IsBreakfast;
                objBedroom.BreakfastPrice = null;
                ObjBedroomPriceManage.UpdateBedroomPrice(objBedroom);
            }
            else
            {
                IsBreakfast = false;
                txtSupplyBreakfast.Text = txtSupplyBreakfast.Text;
                BedRoom objBedroom = ObjBedroomPriceManage.GedBedroomById(Convert.ToInt32(hdnBedroomId.Value));
                objBedroom.HotelId = intHotelID;
                objBedroom.PriceSingle = Convert.ToDecimal(txtSingle.Text);
                objBedroom.PriceDouble = Convert.ToDecimal(txtDouble.Text);
                objBedroom.IsBreakFastInclude = IsBreakfast;
                objBedroom.BreakfastPrice = Convert.ToDecimal(txtSupplyBreakfast.Text);
                ObjBedroomPriceManage.UpdateBedroomPrice(objBedroom);
            }


            BindLstViewBedroomPrice();
            divBedRoomRoomPriceForm.Visible = false;
            divMessage.InnerHtml = ObjBedroomPriceManage.propMessage;

            if (Convert.ToString(ViewState["Count"]) != "1")
            {
                divMessage.InnerHtml += "&nbsp; Please click on next button to move ahead.";
            }
            divMessage.Attributes.Add("class", "succesfuly");
            divMessage.Style.Add("display", "block");
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This button click used for hide form. 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        hdnSelectedRowID.Value = "0";
        divBedRoomRoomPriceForm.Visible = true;
        divMessage.Style.Add("display", "none");
        divMessage.Attributes.Add("class", "error");
        divBedRoomRoomPriceForm.Visible = false;
        BindLstViewBedroomPrice();

    }

    /// <summary>
    /// This button click event used for proceed to next step.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNext_Click(object sender, EventArgs e)
    {
        divBedRoomRoomPriceForm.Visible = true;
        divMessage.Style.Add("display", "none");
        divMessage.Attributes.Add("class", "error");
        ObjWizardLinkSetting.ThisPageIsDone(intHotelID, Convert.ToInt32(PropertyLevelSectionLeftMenu.lnkBedrooms));
        ObjDashBoard.DoneThisStep(intHotelID, Convert.ToInt32(PropertyLevelSection.lnkPricing));
        Response.Redirect("PictureVideoManagement.aspx", true);
    }
    #endregion
}