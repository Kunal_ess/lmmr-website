﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using log4net.Config;
#endregion

public partial class HotelUser_FinanceInformation : System.Web.UI.Page
{
    #region Veriables and Properties
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_FinanceInformation));
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                Session["LinkID"] = "lnkFinance";
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion
}