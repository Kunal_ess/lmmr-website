﻿#region Using
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Linq;
using LMMR.Data;
using System.Configuration;
using log4net;
using log4net.Config;
#endregion
public partial class HotelUser_HotelDashboard : System.Web.UI.Page
{
    #region Variables and Properties
    DashboardManager ObjDashBoard = new DashboardManager();
    WizardLinkSettingManager ObjWizardLinkSetting = new WizardLinkSettingManager();
    ViewBooking_Hotel objvewbooking = new ViewBooking_Hotel();
    HotelManager objHotelManager = new HotelManager();
    FinanceInvoice objFinance = new FinanceInvoice();
    HotelInfo ObjHotelinfo = new HotelInfo();
    Hotel objHotel = new Hotel();
    int intHotelID = 0;
    bool boolGoOnlineStatus = false;
    SuperAdminTaskManager manager = new SuperAdminTaskManager();
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_HotelDashboard));
    
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl + "/";
        }
    }
    public string TimeoutTime
    {
        get
        {
            return System.Web.Security.FormsAuthentication.Timeout.Minutes.ToString();
        }
    }
    #endregion
    
    #region Page Load
    /// <summary>
    /// This method is used to set all the components of the page.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentUserID"] == null || Session["CurrentUser"] == null)
        {
            //Response.Redirect("~/login.aspx");
            Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
            Session.Abandon();
            if (l != null)
            {
                Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
            }
            else
            {
                Response.Redirect(SiteRootPath + "login/english");
            }
        }
        else
        {
            Users objUsers = (Users)Session["CurrentUser"];
            lblLoginUserName.Text = objUsers.FirstName + " " + objUsers.LastName;// +" | UserID=" + objUsers.UserId + " | HotelId=" + Convert.ToString(Session["CurrentHotelID"]);
            objUsers.LastLogin = DateTime.Now;
            lstLoginTime.Text = DateTime.Now.ToLongDateString();
            TList<Hotel> htlList = null;
            if (Convert.ToString(Session["Operator"]) == "1")
            {

            }
            else
            {
                htlList = objHotelManager.GetHotelByClientId(Session["CurrentUserID"] == null ? 0 : Convert.ToInt64(Session["CurrentUserID"]));
            }
            if (!IsPostBack)
            {
                BindHotelDetails();
                Cms obj = manager.getCmsEntityOnType("HotelBackendBanner");
                TList<CmsDescription> list = null;
                if (obj != null)
                {
                    list = manager.getCmsDescriptionOnCmsID(obj.Id);
                    if (list != null)
                    {
                        imgBanner.ImageUrl = ConfigurationManager.AppSettings["FilePath"] + "FrontImages/" + list.FirstOrDefault().ContentImage;
                    }
                }
                //imgBanner.ImageUrl = "";
                lblnewbooking.Text = "(" + Convert.ToString(objvewbooking.countbooking(Convert.ToInt32(Session["CurrentHotelID"]))) + ")";
                lbloldbooking.Text = "(" + Convert.ToString(objvewbooking.countProcessedbooking(Convert.ToInt32(Session["CurrentHotelID"]))) + ")";
                lblRequest.Text = "(" + Convert.ToString(objvewbooking.countrequest(Convert.ToInt32(Session["CurrentHotelID"]))) + ")";
                string whereClause = "HotelId=" + Convert.ToInt32(Session["CurrentHotelID"]) + " and " + "InvoiceSent=1" + " and " + "Ispaid=0";
                TList<Invoice> lstInvoice = objFinance.GetinvoicebyClient(whereClause);
                lnkInvoices.Text = "Statement(" + lstInvoice.Count + ")";
            }
            if (Session["CurrentHotelID"] == null)
            {
                intHotelID = Convert.ToInt32(htlList.FirstOrDefault().Id);

            }
            else
            {
                intHotelID = Convert.ToInt32(Session["CurrentHotelID"]);
            }
            Hotel objHotel = DataRepository.HotelProvider.GetById(intHotelID);
            boolGoOnlineStatus = Convert.ToBoolean(objHotel.GoOnline);
            if (!boolGoOnlineStatus)
            {
                DisableDashboard();
                DisableTopMenu(intHotelID);
                HyperLink anchorControl = (HyperLink)this.Page.FindControl("lnkNews");
                anchorControl.Enabled = true;
                //--- to enable the links of news ,gdt, and chang  password
                anchorControl.Style.Add("color", "#1B7AC2");
                anchorControl = (HyperLink)this.Page.FindControl("lnkGdt");
                anchorControl.Enabled = true;
                anchorControl.Style.Add("color", "#1B7AC2");
                anchorControl = (HyperLink)this.Page.FindControl("lnkchangePassword");
                anchorControl.Enabled = true;
                anchorControl.Style.Add("color", "#1B7AC2");

                divavailability.Attributes.Add("class", "gray_white_box");
                divBooking.Attributes.Add("class", "gray_white_box");
                divfinance.Attributes.Add("class", "gray_white_box");

                divpricing.Attributes.Add("class", "gray_white_box");
                divstatistics.Attributes.Add("class", "gray_white_box");
                divrequest.Attributes.Add("class", "gray_white_box");
            }
            else
            {
                liGoOnline.Visible = false;
            }
        }
    }
    #endregion

    #region Bind hotel Details 
    /// <summary>
    /// Bind the Hotel details to the drop down drpHotelList
    /// </summary>
    public void BindHotelDetails()
    {
        try
        {
            drpHotelList.DataTextField = "Name";
            drpHotelList.DataValueField = "Id";
            TList<Hotel> htlList;
            //This section is used after operator comes from hotel facility access
            if (Convert.ToString(Session["Operator"]) == "1")
            {
                string whereclause = "Id='" + Session["CurrentHotelID"] + "'";
                htlList = ObjHotelinfo.GetHotelbyCondition(whereclause, String.Empty);
                divLogindtls.Visible = false;
            }
            //This section is used after operator comes from hotel facility access
            else
            {
                htlList = objHotelManager.GetHotelByClientId(Session["CurrentUserID"] == null ? 0 : Convert.ToInt64(Session["CurrentUserID"]));//Session["CurrentUserID"]
            }
            if (htlList.Count > 0)
            {
                drpHotelList.DataSource = htlList;
                drpHotelList.DataBind();
                drpHotelList.SelectedValue = Convert.ToString(Session["CurrentHotelID"] == null ? "" : Session["CurrentHotelID"]);
                Session["CurrentHotelID"] = drpHotelList.SelectedValue;
                imgHotelLogo.ImageUrl = (string.IsNullOrEmpty(htlList.FirstOrDefault(a => a.Id == Convert.ToInt64(Session["CurrentHotelID"])).Logo) ? ConfigurationManager.AppSettings["FilePath"] + "/HotelImage/default_hotel_logo.png" : ConfigurationManager.AppSettings["FilePath"] + "/HotelImage/" + htlList.FirstOrDefault(a => a.Id == Convert.ToInt64(Session["CurrentHotelID"])).Logo);
            }
            else
            {
                //When there is no active hotel for the logged in user
                Session.Remove("CurrentHotelID");
                Session.RemoveAll();
                Session.Abandon();
                Response.Redirect("~/Login.aspx", false);
                return;
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion
    
    #region Events
    protected void drpHotelList_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Hotel objhotel = objHotelManager.GetHotelDetailsById(Convert.ToInt64(drpHotelList.SelectedValue));
            imgHotelLogo.ImageUrl = (string.IsNullOrEmpty(objhotel.Logo) ? ConfigurationManager.AppSettings["FilePath"] + "/HotelImage/default_hotel_logo.png" : ConfigurationManager.AppSettings["FilePath"] + "/HotelImage/" + objhotel.Logo);
            Session["CurrentHotelID"] = objhotel.Id;
            Response.Redirect(Request.RawUrl, false);
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    protected void lnkLastMinutMeeting_Click(object sender, EventArgs e)
    {

    }
    protected void lnkPropertyLevel_Click(object sender, EventArgs e)
    {
        try
        {

            Session["LinkID"] = "lnkPropertyLevel";
            Response.Redirect("GeneralInfo.aspx",false);
            return;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    protected void lnkAvailability_Click(object sender, EventArgs e)
    {
        try
        {
            Session["LinkID"] = "lnkAvailability";
            Response.Redirect("ManageAvailability.aspx",false);
            return;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    protected void lnkBookings_Click(object sender, EventArgs e)
    {
        try
        {
            Session["LinkID"] = "lnkBookings";
            Response.Redirect("ViewBookings.aspx?Type=1",false);
            return;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    protected void lnkFinance_Click(object sender, EventArgs e)
    {
        Session["LinkID"] = "lnkFinance";
        Response.Redirect("FinanceInformation.aspx",false);
        return;
    }
    protected void lnkStatistics_Click(object sender, EventArgs e)
    {
        Session["LinkID"] = "lnkNews";
        Response.Redirect("Statistics.aspx?type=1");
    }
    protected void lnkPromotions_Click(object sender, EventArgs e)
    {
        Session["LinkID"] = "lnkPromotions";
        Response.Redirect("SalesAndPromo.aspx",false);
        return;

    }
    protected void lnkRequests_Click(object sender, EventArgs e)
    {

        Session["LinkID"] = "lnkRequests";
        Response.Redirect("ViewRequest.aspx?type=1",false);
        return;
    }

    protected void lnklogout_Click(object sender, EventArgs e)
    {
        logger.Debug("Log out User ID:" + Session["CurrentUserID"]);
        Session.Remove("CurrentUserID");
        Session.Remove("CurrentUser");
        Session.Remove("CurrentHotelID");
        Session.Remove("LinkID");
        Session.Remove("masterInput");
        //Response.Redirect("~/login.aspx");
        Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "login/english");
        }
    }
    #endregion

    #region Methods
    //function to disable top menu.
    void DisableTopMenu(int hotelID)
    {
        try
        {
            if (!Convert.ToBoolean(ObjWizardLinkSetting.IsGoOnline(hotelID)))
            {
                lnkAvailability.Enabled = false;
                lnkBookings.Enabled = false;
                lnkFinance.Enabled = false;
                lnkStatistics.Enabled = false;
                lnkPromotions.Enabled = false;
                lnkRequests.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    // Function for disable dashboard link.
    void DisableDashboard()
    {
        try
        {
            int getValue = 0;
            TList<DashboardLink> ObjList = new TList<DashboardLink>();
            ObjList = ObjDashBoard.GetLinkByHotelID(intHotelID);
            if (ObjList.Count > 0)
            {
                int minID = ObjDashBoard.GetMinPropertyLevelSectionID();
                int maxID = ObjDashBoard.GetMaxPropertyLevelSectionID();
                int Index = 0;

                for (Index = minID; Index <= maxID; Index++)
                {
                    var IsExist = ObjList.Find(a => a.DashBoardLinkId == Index);
                    if (IsExist != null)
                    {
                        if (IsExist.DashBoardLinkId != ObjDashBoard.GetMaxPropertyLevelSectionID())
                        {

                            ViewState["getValue"] = Convert.ToInt32(IsExist.DashBoardLinkId);

                        }
                    }

                }
                getValue = Convert.ToInt32(ViewState["getValue"]);
                DisableLink(getValue);
                enableLink(getValue);
            }
            else
            {
                getValue = ObjDashBoard.GetMinPropertyLevelSectionID() - 1;
                DisableLink(getValue);
                enableLink(getValue);
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);

            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    //This Function use for enable links of dashboard.
    void enableLink(int lnkVlaue)
    {
        try
        {
            int enableElementID = lnkVlaue + 1;
            //DisableLink();
            string linkID = Enum.GetName(typeof(DashboardLinks), enableElementID);
            HyperLink anchorControl = (HyperLink)this.Page.FindControl(linkID);
            anchorControl.Enabled = true;
            anchorControl.Style.Add("color", "red");
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    // This Function Use for disable links of dashboard.
    void DisableLink(int lnkValue)
    {
        try
        {
            lnkValue = lnkValue + 1;
            int dashboardlinkMax = ObjDashBoard.GetMaxDashboardID();
            int dashboardlinkMin = ObjDashBoard.GetMinDashboardID();
            int i = 0;
            for (i = dashboardlinkMin; i <= dashboardlinkMax; i++)
            {
                if (lnkValue < i)
                {
                    string linkID = Enum.GetName(typeof(DashboardLinks), i);
                    HyperLink anchorControl = (HyperLink)this.Page.FindControl(linkID);
                    anchorControl.Enabled = false;
                    anchorControl.Style.Add("color", "gray");
                }
                else
                {
                    string linkID = Enum.GetName(typeof(DashboardLinks), i);
                    HyperLink anchorControl = (HyperLink)this.Page.FindControl(linkID);
                    anchorControl.Enabled = true;
                    anchorControl.Style.Add("color", "#1B7AC2");
                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion
}