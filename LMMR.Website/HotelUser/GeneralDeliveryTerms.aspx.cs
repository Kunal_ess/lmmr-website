﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using log4net.Config;
#endregion

public partial class HotelUser_GeneralDelevryTerms : System.Web.UI.Page
{
    #region variable declaration
    int intHotelID = 0;
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_GeneralDelevryTerms));
    #endregion

    #region PageLoad
    /// <summary>
    /// On page load we manage all the containts and Apply paging if there is a post back.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Session["LinkID"] = "lnkGdt";
            intHotelID = Convert.ToInt32(Session["CurrentHotelID"] == null ? 0 : Session["CurrentHotelID"]);
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion
}