﻿<%@ Page Title="Hotel User - Statistics" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master"
    AutoEventWireup="true" CodeFile="Statistics.aspx.cs" Inherits="HotelUser_News" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" runat="Server">
    <h1>
        <asp:Label ID="lblheader" runat="server" Text="Statistics for : "></asp:Label>
    </h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" runat="Server">
    <div class="rightside_inner_container">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="statistics">
            <tr>
                <td align="left">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left" class="one1">
                    <tr>
                    <td colspan="6">
                      <div  id="divmessage" runat="server" class="error">
                        </div>
                    </td>
                    </tr>
                        <tr>
                            <td>
                              &nbsp;&nbsp;  From&nbsp;
                            </td>
                            <td>
                                <asp:TextBox ID="txtFromdate" runat="server" CssClass="dateinput"></asp:TextBox>
                                &nbsp;
                                <input type="image" src="../images/date-icon.png" id="calFrom" />
                                <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtFromdate"
                                    PopupButtonID="calFrom" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                            </td>
                            <td align="center" >
                                To&nbsp;
                            </td>
                            <td >
                                <asp:TextBox ID="txtTodate" runat="server" CssClass="dateinput"></asp:TextBox>
                                &nbsp;
                                <input type="image" src="../images/date-icon.png" id="calTo" />
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtTodate"
                                    PopupButtonID="calTo" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                            </td>
                            <td align="right">
                                <asp:LinkButton ID="lbtSearch" runat="server" Visible="false" CssClass="Search-button"
                                    ForeColor="White" OnClick="lbtSearch_Click">Search</asp:LinkButton>
                            </td>
                            <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left" class="one1">
                        <tr>
                            <td style="padding: 10px">
                                <b>Click to filter by :</b>
                            </td>
                            <td align="center" style="padding: 10px">
                      
                        <div class="n-btn">

                        <asp:LinkButton ID="lnkDays" class="btn-active" runat="server" ForeColor="White" OnClick="lnkDays_Click">
                        
                        <div class="n-btn-left"></div>                    
                    	<div  class="n-btn-mid">Days</div>                    
                    	<div class="n-btn-right"></div>  
                        
                        </asp:LinkButton>
                 


                        </div>   
                            </td>
                            <td align="center" style="padding: 10px">
                               <div class="n-btn">
                   <asp:LinkButton ID="lnkWeeks" runat="server"  class="btn" ForeColor="White" OnClick="lnkWeeks_Click">
                   		<div class="n-btn-left"></div>                    
                    	<div  class="n-btn-mid">  Weeks</div>                    
                    	<div class="n-btn-right"></div>
                    </asp:LinkButton>
                </div>
                               
                            </td>
                            <td align="center" style="padding: 10px">
                               <div class="n-btn">
                   <asp:LinkButton ID="lnkMonths" runat="server"  class="btn" ForeColor="White" OnClick="lnkMonths_Click">
                   		<div class="n-btn-left"></div>                    
                    	<div  class="n-btn-mid">    Months</div>                    
                    	<div class="n-btn-right"></div>
                  </asp:LinkButton>
                </div>
                              
                            </td>
                            <td align="center" style="padding: 10px">
                                     <div class="n-btn">
                   <asp:LinkButton ID="lnkYear" runat="server"  class="btn" ForeColor="White" OnClick="lnkYear_Click">
                   		<div class="n-btn-left"></div>                    
                    	<div  class="n-btn-mid">   Years</div>                    
                    	<div class="n-btn-right"></div>
                   </asp:LinkButton>
                </div>
                                
                            </td>                          
                            
                            
                        </tr>
                    </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table width="60%" border="0" cellspacing="0" cellpadding="0" align="left" class="one1">
                        
                    </table>
                </td>
            </tr>
            <tr id="trvisitors" visible="false" runat="server">
                <td align="left">
                    <div class="scroll">
                        <br />
                        <h2>
                            <asp:Label ID="lblVisthead" runat="server" Text="Visitors (Days)"></asp:Label>          </h2>
                        <br />
                        <asp:DataList ID="dtlstVisitors" runat="server" Width="100%" RepeatDirection="Horizontal"
                            CellPadding="0" CellSpacing="0">
                            <ItemTemplate>
                                <table align="center" width="100%" cellspacing="0" cellpadding="0" border="0" class="statistics two">
                                    <tr bgcolor="#c3d6e2" align="center" >
                                        <th>
                                            <asp:Label ID="lbldate" runat="server" Text='<%# Eval("currentdate","{0:dd/MM/yy}") %>'></asp:Label>
                                        </th>
                                    </tr>
                                    <tr bgcolor="#ecf7fe" align="center">
                                        <th>
                                            <asp:Label ID="lblvisits" runat="server" Text='<%# Eval("statisticvalue") %>'></asp:Label>
                                        </th>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                        <br />
                        <h2>
                            Conversions</h2> (Total No of Bookings) / (No of vsitors)<br />
&nbsp;<br />
                        <asp:DataList ID="dtlstConversion" runat="server" Width="100%" RepeatDirection="Horizontal"
                            CellPadding="0" CellSpacing="0">
                            <ItemTemplate>
                                <table align="center" width="100%" cellspacing="0" cellpadding="0" border="0" class="two">
                                    <tr bgcolor="#c3d6e2" align="center" >
                                        <th>
                                            <asp:Label ID="lbldate" runat="server" Text='<%# Eval("currentdate","{0:dd/MM/yy}") %>'></asp:Label>
                                        </th>
                                    </tr>
                                    <tr bgcolor="#ecf7fe" align="center">
                                        <th>
                                            <asp:Label ID="lblconfirm" runat="server" Text='<%# Eval("confirmvalue") %>'></asp:Label>
                                            /
                                            <asp:Label ID="lblvisits" runat="server" Text='<%# Eval("statisticvalue") %>'></asp:Label>
                                        </th>
                                    </tr>
                                    <tr bgcolor="#d9eefc" align="center">
                                        <th>
                                            <asp:Label ID="lblconversion" runat="server" Text='<%# Eval("conversion") %>'></asp:Label>%
                                        </th>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                    </div>
                </td>
            </tr>
            <tr id="trBookReq" visible="false" runat="server">
                <td align="left">
                    <br />
                    <h2>
                        <asp:Label ID="lblhead" runat="server">Bookings (Days)</asp:Label></h2>
                    <br />
                   <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td valign="top" width="150px">
                             <table width="150px" cellspacing="0" cellpadding="0">
                             <tr>
                             <td>
                                        <table align="center" width="150px" cellspacing="0" cellpadding="0" border="0" class="statistics two"
                                            style="margin-left: 13px; border-left: 1px solid #a3d4f7;">
                                            <tr bgcolor="#c3d6e2">
                                                <th>
                                                    <asp:Label ID="lbldat" runat="server" Text=""></asp:Label><br />
                                                </th>
                                            </tr>
                                            <tr bgcolor="#ecf7fe">
                                                <th>
                                                    <asp:Label ID="lblvisit" runat="server" Text="Visits"></asp:Label>
                                                </th>
                                            </tr>
                                       
                                            <tr bgcolor="#d9eefc">
                                                <th>
                                                    <asp:Label ID="lblBooking" runat="server" Text="Total Booking"></asp:Label>
                                                </th>
                                            </tr>
                                           <tr bgcolor="#ecf7fe">
                                                <th>
                                                    <asp:Label ID="lblFT" runat="server" Text="Failure Timeout"></asp:Label>
                                                </th>
                                            </tr>
                                            <tr bgcolor="#d9eefc">
                                                <th>
                                                    <asp:Label ID="lblFC" runat="server" Text="Failure Cancel"></asp:Label>
                                                </th>
                                            </tr>
                                        </table>
                                         </td>
                             </tr>
                             </table>
                                  
                            </td>
                            <td valign="top">
                                <div class="scroll2" >
                                    <asp:DataList ID="dtlistBookReq" runat="server" Width="100%" RepeatDirection="Horizontal"
                                        CellPadding="0" CellSpacing="0">
                                        <ItemTemplate>
                                            <table align="center" width="100%" cellspacing="0" cellpadding="0" border="0" class="statistics two">
                                                <tr bgcolor="#c3d6e2" align="center">
                                                    <th  nowrap="nowrap">
                                                        <asp:Label ID="lblDate" runat="server" Text='<%# Eval("currentdate","{0:dd/MM/yy}") %>'></asp:Label>
                                                    </th>
                                                </tr>
                                                 
                                                <tr bgcolor="#ecf7fe" align="center">
                                                    <th>
                                                        <asp:Label ID="lblvisits" runat="server" Text='<%# Eval("statisticvalue") %>'></asp:Label>
                                                    </th>
                                                </tr>
                                                <tr bgcolor="#d9eefc" align="center">
                                                    <th>
                                                        <asp:Label ID="lblBooking" runat="server" Text='<%# Eval("Booking") %>'></asp:Label>
                                                    </th>
                                                </tr>
                                             
                                                <tr bgcolor="#ecf7fe" align="center">
                                                    <th>
                                                        <asp:Label ID="lblFT" runat="server" Text='<%# Eval("FailureTimeout") %>'></asp:Label>
                                                    </th>
                                                </tr>
                                                <tr bgcolor="#d9eefc" align="center">
                                                    <th>
                                                        <asp:Label ID="lblFC" runat="server" Text='<%# Eval("FailureCancel") %>'></asp:Label>
                                                    </th>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
<%--             Request--%>

            <tr id="trrequest" visible="false"   runat="server">
                <td align="left">
                    <br />
                    <h2>
                        <asp:Label ID="lblReqHead" runat="server">Requests (Days)</asp:Label></h2>
                    <br />
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td valign="top" width="150px">
                             <table width="150px" cellspacing="0" cellpadding="0">
                             <tr>
                             <td>
                             <table align="center" width="150px" cellspacing="0" cellpadding="0" border="0" class="statistics two"
                                            style="margin-left: 13px; border-left: 1px solid #a3d4f7;">
                                            <tr id="trdfgd" bgcolor="#c3d6e2">
                                                <th>
                                                    <asp:Label ID="lbldat1" runat="server" Text=""></asp:Label><br />
                                                </th>
                                            </tr>
                                            <tr bgcolor="#ecf7fe">
                                                <th>
                                                    <asp:Label ID="lblVisits1" runat="server" Text="Visits"></asp:Label>
                                                </th>
                                            </tr>
                                            <tr bgcolor="#d9eefc">
                                                <th>
                                                 <asp:Label ID="lblsensitive" runat="server" Text="Total Requests"></asp:Label>
                                                  
                                                </th>
                                            </tr>
                                            <tr bgcolor="#ecf7fe">
                                                <th>
                                                     <asp:Label ID="lblBooking1" runat="server" Text="Requests Confirmed"></asp:Label>
                                                </th>
                                            </tr>
                                            <tr bgcolor="#d9eefc">
                                                <th>
                                                    <asp:Label ID="lblFT1" runat="server" Text="Failure Timeout"></asp:Label>
                                                </th>
                                            </tr>
                                            <tr bgcolor="#ecf7fe">
                                                <th>
                                                    <asp:Label ID="lblFC1" runat="server" Text="Failure Cancel"></asp:Label>
                                                </th>
                                            </tr>
                                        </table>
                             </td>
                             </tr>
                             </table>
                                        

                            </td>
                            <td valign="top">
                                <div class="scroll2">
                                    <asp:DataList ID="dtlistreq" runat="server" Width="100%" RepeatDirection="Horizontal"
                                        CellPadding="0" CellSpacing="0">
                                        <ItemTemplate>
                                            <table align="center" width="100%" cellspacing="0" cellpadding="0" border="0" class="statistics two">
                                                <tr bgcolor="#c3d6e2" align="center">
                                                    <th nowrap="nowrap">
                                                        <asp:Label ID="lblDate" runat="server" Text='<%# Eval("currentdate","{0:dd/MM/yy}") %>'></asp:Label>
                                                    </th>
                                                </tr>
                                                <tr bgcolor="#ecf7fe" align="center">
                                                    <th>
                                                        <asp:Label ID="lblvisits" runat="server" Text='<%# Eval("statisticvalue") %>'></asp:Label>
                                                    </th>
                                                </tr>
                                                <tr bgcolor="#d9eefc" align="center">
                                                    <th>
                                                        <asp:Label ID="lblBooking" runat="server" Text='<%# Eval("Booking") %>'></asp:Label>
                                                    </th>
                                                </tr>
                                                <tr bgcolor="#ecf7fe" align="center">
                                                    <th>
                                                        <asp:Label ID="lblSen" runat="server" Text='<%# Eval("Sensitive") %>'></asp:Label>
                                                    </th>
                                                </tr>
                                                <tr bgcolor="#d9eefc" align="center">
                                                    <th>
                                                        <asp:Label ID="lblFT" runat="server" Text='<%# Eval("FailureTimeout") %>'></asp:Label>
                                                    </th>
                                                </tr>
                                                <tr bgcolor="#ecf7fe" align="center">
                                                    <th>
                                                        <asp:Label ID="lblFC" runat="server" Text='<%# Eval("FailureCancel") %>'></asp:Label>
                                                    </th>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#<%= txtFromdate.ClientID %>").attr("disabled", true);
            jQuery("#<%= txtTodate.ClientID %>").attr("disabled", true);
            jQuery("#<%= lbtSearch.ClientID %>").bind("click", function () {
                jQuery("#contentbody_txtFromdate").attr("disabled", false);
                jQuery("#contentbody_txtTodate").attr("disabled", false);

            });

            jQuery("#<%= lnkWeeks.ClientID %>").bind("click", function () {
                jQuery("#contentbody_txtFromdate").attr("disabled", false);
                jQuery("#contentbody_txtTodate").attr("disabled", false);

            });
            jQuery("#<%= lnkDays.ClientID %>").bind("click", function () {
                jQuery("#contentbody_txtFromdate").attr("disabled", false);
                jQuery("#contentbody_txtTodate").attr("disabled", false);

            });


            jQuery("#<%= lnkMonths.ClientID %>").bind("click", function () {
                jQuery("#contentbody_txtFromdate").attr("disabled", false);
                jQuery("#contentbody_txtTodate").attr("disabled", false);

            });

            jQuery("#<%= lnkYear.ClientID %>").bind("click", function () {
                jQuery("#contentbody_txtFromdate").attr("disabled", false);
                jQuery("#contentbody_txtTodate").attr("disabled", false);

            });

        });
     
    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#<%= lbtSearch.ClientID %>").bind("click", function () {
                var fromdate = jQuery("#<%= txtFromdate.ClientID %>").val();
                var todate = jQuery("#<%= txtTodate.ClientID %>").val();
                var todayArr = todate.split('/');
                var formdayArr = fromdate.split('/');
                var fromdatecheck = new Date();
                var todatecheck = new Date();
                fromdatecheck.setFullYear(parseInt("20" + formdayArr[2]), (parseInt(formdayArr[1]) - 1), formdayArr[0]);
                todatecheck.setFullYear(parseInt("20" + todayArr[2]), (parseInt(todayArr[1]) - 1), todayArr[0]);
                if (fromdatecheck > todatecheck) {
                    jQuery(".error").show();
                    jQuery(".error").html("From date must be less than To date.");
                    jQuery("#contentbody_txtFromdate").attr("disabled", true);
                    jQuery("#contentbody_txtTodate").attr("disabled", true);
                    return false;
                }
                jQuery("#Loding_overlay").show();
            });

            jQuery("#<%= lnkWeeks.ClientID %>").bind("click", function () {
                var fromdate = jQuery("#<%= txtFromdate.ClientID %>").val();
                var todate = jQuery("#<%= txtTodate.ClientID %>").val();
                var todayArr = todate.split('/');
                var formdayArr = fromdate.split('/');
                var fromdatecheck = new Date();
                var todatecheck = new Date();
                fromdatecheck.setFullYear(parseInt(formdayArr[2], 10), (parseInt(formdayArr[1], 10) - 1), formdayArr[0]);
                todatecheck.setFullYear(parseInt(todayArr[2], 10), (parseInt(todayArr[1], 10) - 1), todayArr[0]);
                if (fromdatecheck > todatecheck) {
                    jQuery(".error").show();
                    jQuery(".error").html("From date must be less than To date.");
                    jQuery("#contentbody_txtFromdate").attr("disabled", true);
                    jQuery("#contentbody_txtTodate").attr("disabled", true);
                    return false;
                }
                jQuery("#Loding_overlay").show();
            });

            jQuery("#<%= lnkDays.ClientID %>").bind("click", function () {
                var fromdate = jQuery("#<%= txtFromdate.ClientID %>").val();
                var todate = jQuery("#<%= txtTodate.ClientID %>").val();
                var todayArr = todate.split('/');
                var formdayArr = fromdate.split('/');
                var fromdatecheck = new Date();
                var todatecheck = new Date();
                fromdatecheck.setFullYear(parseInt(formdayArr[2], 10), (parseInt(formdayArr[1], 10) - 1), formdayArr[0]);
                todatecheck.setFullYear(parseInt( todayArr[2],10), (parseInt(todayArr[1],10) - 1), todayArr[0]);
                if (fromdatecheck > todatecheck) {
                    jQuery(".error").show();
                    jQuery(".error").html("From date must be less than To date.");
                    jQuery("#contentbody_txtFromdate").attr("disabled", true);
                    jQuery("#contentbody_txtTodate").attr("disabled", true);
                    return false;
                }
                jQuery("#Loding_overlay").show();
            });


            jQuery("#<%= lnkMonths.ClientID %>").bind("click", function () {
                var fromdate = jQuery("#<%= txtFromdate.ClientID %>").val();
                var todate = jQuery("#<%= txtTodate.ClientID %>").val();
                var todayArr = todate.split('/');
                var formdayArr = fromdate.split('/');
                var fromdatecheck = new Date();
                var todatecheck = new Date();
                fromdatecheck.setFullYear(parseInt(formdayArr[2], 10), (parseInt(formdayArr[1], 10) - 1), formdayArr[0]);
                todatecheck.setFullYear(parseInt(todayArr[2], 10), (parseInt(todayArr[1], 10) - 1), todayArr[0]);
                if (fromdatecheck > todatecheck) {
                    jQuery(".error").show();
                    jQuery(".error").html("From date must be less than To date.");
                    jQuery("#contentbody_txtFromdate").attr("disabled", true);
                    jQuery("#contentbody_txtTodate").attr("disabled", true);
                    return false;
                }
                jQuery("#Loding_overlay").show();
            });

            jQuery("#<%= lnkYear.ClientID %>").bind("click", function () {
                var fromdate = jQuery("#<%= txtFromdate.ClientID %>").val();
                var todate = jQuery("#<%= txtTodate.ClientID %>").val();
                var todayArr = todate.split('/');
                var formdayArr = fromdate.split('/');
                var fromdatecheck = new Date();
                var todatecheck = new Date();
                fromdatecheck.setFullYear(parseInt(formdayArr[2], 10), (parseInt(formdayArr[1], 10) - 1), formdayArr[0]);
                todatecheck.setFullYear(parseInt(todayArr[2], 10), (parseInt(todayArr[1], 10) - 1), todayArr[0]);
                if (fromdatecheck > todatecheck) {
                    jQuery(".error").show();
                    jQuery(".error").html("From date must be less than To date.");
                    jQuery("#contentbody_txtFromdate").attr("disabled", true);
                    jQuery("#contentbody_txtTodate").attr("disabled", true);
                    return false;
                }
                jQuery("#Loding_overlay").show();
            });
        });
    </script>
</asp:Content>
