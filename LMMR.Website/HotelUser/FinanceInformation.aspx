﻿<%@ Page Title="Hotel User - Finance Information" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master" AutoEventWireup="true" CodeFile="FinanceInformation.aspx.cs" Inherits="HotelUser_FinanceInformation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" Runat="Server">
<h1>Information</h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" Runat="Server">

<div class="contact-details">
            <div class="contact-details-inner1"> 
                <b><asp:Label ID="Label3" runat="server" Text="If you have questions regarding your invoice, please contact us on"></asp:Label></b><br /><br />
                <b><asp:Label ID="Label4" runat="server" Text="Accounting@lastminutemeetingroom.com"></asp:Label></b><br /><br />
                <b><asp:Label ID="Label5" runat="server" Text="Or call +32 2 344 25 50"></asp:Label></b><br />
                <b><asp:Label ID="Label6" runat="server" Text="Or Fax: +32 2 343 24 50"></asp:Label></b><br />
            </div>
</div>
</asp:Content>

