﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Entities;
using LMMR.Data;
using LMMR.Business;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.HtmlControls;
using log4net;
using log4net.Config;
#endregion


public partial class HotelUser_ViewDetailbooking : System.Web.UI.Page
{
    #region Variables and Properties
    VList<ViewBookingHotels> vlist;
    VList<Viewbookingrequest> vlistreq;
    ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
    HotelManager objHotelManager = new HotelManager();
    string Typelink = "", AdminUser = "", Hotelid = "0";
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_ViewDetailbooking));
    #endregion

    #region Page Load
    /// <summary>
    /// Page load for set initial values of variables.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            ApplyPaging();

        }
        else if (!IsPostBack)
        {
            Session["LinkID"] = "lnkBookings";
      
            string hotelname = objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Name;

            lblheadertext.Text = "Search Result list of bookings for the : " + "<b>" + hotelname + "</b>";
            
            bindgrid();
        }
    }
    #endregion

    #region Methods
    /// <summary>
    /// Bind grids
    /// </summary>
    protected void bindgrid()
    {
        Hotelid = Convert.ToString(Session["CurrentHotelID"].ToString());
        int totalcount = 0;
        string whereclaus = "hotelid in (" + Hotelid + ") and booktype = 0 "; // book type for request and booking
        string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
        vlistreq = DataRepository.ViewbookingrequestProvider.GetPaged(whereclaus, "", 0, int.MaxValue, out totalcount);
        grdViewBooking.DataSource = vlistreq;
        grdViewBooking.DataBind();
        ApplyPaging();

        divbookingdetails.Visible = false;
    }

    /// <summary>
    /// Apply paging
    /// </summary>
    private void ApplyPaging()
    {
        if (grdViewBooking.Rows.Count <= 0)
        {
            //     divprint.Visible = false;
            return;
        }

        GridViewRow row = grdViewBooking.TopPagerRow;
        PlaceHolder ph;
        LinkButton lnkPaging;
        //ImageButton lnkFirstPage;
        LinkButton lnkPrevPage;
        LinkButton lnkNextPage;
        lnkPrevPage = new LinkButton();
        lnkPrevPage.CssClass = "pre";
        lnkPrevPage.Width = Unit.Pixel(73);
        lnkPrevPage.CommandName = "Page";
        lnkPrevPage.CommandArgument = "prev";
        ph = (PlaceHolder)row.FindControl("ph");
        ph.Controls.Add(lnkPrevPage);
        if (grdViewBooking.PageIndex == 0)
        {
            lnkPrevPage.Enabled = false;
        }
        for (int i = 1; i <= grdViewBooking.PageCount; i++)
        {
            lnkPaging = new LinkButton();

            lnkPaging.CssClass = "pag";
            lnkPaging.Width = Unit.Pixel(16);
            lnkPaging.Text = i.ToString();
            lnkPaging.CommandName = "Page";
            lnkPaging.CommandArgument = i.ToString();
            if (i == grdViewBooking.PageIndex + 1)
                ph = (PlaceHolder)row.FindControl("ph");
            ph.Controls.Add(lnkPaging);
        }
        lnkNextPage = new LinkButton();
        lnkNextPage.CssClass = "nex";
        lnkNextPage.Width = Unit.Pixel(42);
        lnkNextPage.CommandName = "Page";
        lnkNextPage.CommandArgument = "next";
        ph = (PlaceHolder)row.FindControl("ph");
        ph.Controls.Add(lnkNextPage);
        ph = (PlaceHolder)row.FindControl("ph");
        if (grdViewBooking.PageIndex == grdViewBooking.PageCount - 1)
        {
            lnkNextPage.Enabled = false;

        }
    }
    #endregion

    #region Events
    /// <summary>
    /// Search button click
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtSearch_Click(object sender, EventArgs e)
    {
        Hotelid = Convert.ToString(Session["CurrentHotelID"].ToString());
         string whereclaus = " (hotelid in (" + Hotelid + ") and booktype = 0) "; // book type for request and booking
         whereclaus += " and ( 1=1 ";
         if (!string.IsNullOrEmpty(txtRefNo.Text))
             whereclaus += " and Id ='" + txtRefNo.Text + "'";
         if (!string.IsNullOrEmpty(txtClientName.Text))
             whereclaus += " and Contact like '" + txtClientName.Text + "%'";
         if (!string.IsNullOrEmpty(txtArrivaldate.Text))
             whereclaus += " and arrivaldate = convert(DATETIME, '" + txtArrivaldate.Text + "',103)";
         if (!string.IsNullOrEmpty(txtFromdate.Text) || !string.IsNullOrEmpty(txtTodate.Text))
             whereclaus += " and arrivaldate between convert(DATETIME, '" + txtFromdate.Text + "',103) and convert(DATETIME, '" + txtTodate.Text + "',103) ";
         whereclaus += " and  1=1 )";
         int totalcount = 0;
      //  string whereclaus = "hotelid in (" + Hotelid + ") and booktype = 0 "; // book type for request and booking
        string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
        vlistreq = DataRepository.ViewbookingrequestProvider.GetPaged(whereclaus, "", 0, int.MaxValue, out totalcount);
        grdViewBooking.DataSource = vlistreq;
        grdViewBooking.DataBind();
        ApplyPaging();

    }

    /// <summary>
    /// Refrence number link button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lblRefNo_Click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;



        ViewState["BookingID"] = lnk.Text;
        int totalcount = 0;
        string whereclaus = "hotelid=" + lnk.ToolTip + " and id=" + lnk.Text; // change hotel ID make it dynamic
        string orderby = ViewBookingHotelsColumn.BookingDate + " ASC";
        vlist = DataRepository.ViewBookingHotelsProvider.GetPaged(whereclaus, orderby, 0, int.MaxValue, out totalcount);


        Hotel htl = DataRepository.HotelProvider.GetById(Convert.ToInt64(lnk.ToolTip));

        lblProteryname.Text = htl.Name;
        foreach (ViewBookingHotels vi in vlist)
        {
            #region Duration dates
            TList<BookedMeetingRoom> bookedMR = DataRepository.BookedMeetingRoomProvider.GetByBookingId(Convert.ToInt64(lnk.Text));
            #region ArrivalDate
            foreach (BookedMeetingRoom br in bookedMR)
            {
                string day = "";
                if (br.MeetingDay == 0)
                    day = "Morning";
                if (br.MeetingDay == 1)
                    day = "Afternoon";
                if (br.MeetingDay == 2)
                    day = "Full Day";

                DateTime dtmeetingdt = (DateTime)br.MeetingDate;
                lblBookedDays.Text = dtmeetingdt.ToString("dd-MMM") + " " + day;
                break;
            }
            #endregion

            #region DepartureDate
            if (bookedMR.Count > 1)
            {
                foreach (BookedMeetingRoom br in bookedMR)
                {
                    string day = "";
                    if (br.MeetingDay == 0)
                        day = "Morning";
                    if (br.MeetingDay == 1)
                        day = "Afternoon";
                    if (br.MeetingDay == 2)
                        day = "Full Day";
                    DateTime dtmeetingdt = (DateTime)br.MeetingDate;
                    lblBookedDays1.Text = dtmeetingdt.ToString("dd-MMM") + " " + day;
                    //   return;
                }
            }
            #endregion

            #endregion
            //lblBookedDays.Text = Convert.ToString( vi.Duration);
            lblConatctpersonEmail.HRef = "mailto:" + vi.EmailId;
            lblConatctpersonEmail.InnerText = vi.EmailId;
            lblContactPerson.Text = vi.Contact;
            lblMeetingroom.Text = vi.Name;
            lblParticipants.Text = Convert.ToString(vi.NoofParticipants);
            lblbookingdoneOn.Text = Convert.ToString(vi.BookingDate);
            lblfinalprice.Text = Convert.ToString(vi.FinalTotalPrice);
            lblsplcomments.Text = Convert.ToString(vi.SpecialRequest);
            #region fetching the meeting shape and size also price
            TList<MeetingRoomConfig> objmeetingroomconfig = DataRepository.MeetingRoomConfigProvider.GetByMeetingRoomId(Convert.ToInt64(vi.MainMeetingRoomId));
            decimal totalpp = 0;
            foreach (MeetingRoomConfig mrc in objmeetingroomconfig)
            {
                string roomshape = Enum.GetName(typeof(RoomShape), mrc.RoomShapeId);
                lblShape.Text = roomshape;
                lblMaxshape.Text = Convert.ToString(mrc.MaxCapicity);
                lblMinShape.Text = Convert.ToString(mrc.MinCapacity);

                // for price
                MeetingRoom mrprice = DataRepository.MeetingRoomProvider.GetById(Convert.ToInt64(vi.MainMeetingRoomId));
                lblprice.Text = Convert.ToString(mrprice.FulldayPrice);
                lbldura.Text = lblParticipants.Text;
                // calculaion total

                totalpp = Convert.ToDecimal(mrprice.FulldayPrice) * Convert.ToDecimal(lblParticipants.Text);
                lbltotal.Text = Convert.ToString(totalpp);
                //  lbldiffer.Text = Convert.ToString(Convert.ToDecimal(lblfinalprice.Text) - Convert.ToDecimal(lbltotal.Text));
            }

            # endregion
            #region Bookingpackage details
            List<Viewdetails> objMyList = objViewBooking_Hotel.ViewDetails(Convert.ToInt64(lnk.Text));
            decimal Total = objMyList.Sum(a => a.totalprice);
            grddetails.DataSource = objMyList;
            grddetails.DataBind();
            #endregion

            lblfinalprice.Text = Convert.ToString(Total + totalpp);
        }
        // DivHidem.Visible = true;
        divbookingdetails.Visible = true;

    }

    /// <summary>
    /// Grid view page index changing.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdViewBooking_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        grdViewBooking.PageIndex = e.NewPageIndex;
        bindgrid();
    }

    /// <summary>
    /// grid booking row data bound.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdViewBooking_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        foreach (GridViewRow gr in grdViewBooking.Rows)
        {

            LinkButton btnchkcommision = (LinkButton)gr.FindControl("btnchkcommision");

            Label lblArrivalDt = (Label)gr.FindControl("lblArrivalDt");
            Label lblBookingDt = (Label)gr.FindControl("lblBookingDt");
            LinkButton lblRefNo = (LinkButton)gr.FindControl("lblRefNo");
            TList<BookedMeetingRoom> bookedMR = DataRepository.BookedMeetingRoomProvider.GetByBookingId(Convert.ToInt64(lblRefNo.Text));
            Label lblDepartureDt = (Label)gr.FindControl("lblDepartureDt");
            Label lblExpiryDt = (Label)gr.FindControl("lblExpiryDt");
            string depdt = "";
            #region ArrivalDate
            foreach (BookedMeetingRoom br in bookedMR)
            {
                DateTime dtmeetingdt = (DateTime)br.MeetingDate;
                lblArrivalDt.Text = dtmeetingdt.ToString("dd-MMM") + "-" + br.StartTime.ToString();
                break;
            }
            #endregion



            #region DepartureDate
            foreach (BookedMeetingRoom br in bookedMR)
            {
                DateTime dtmeetingdt = (DateTime)br.MeetingDate;
                lblDepartureDt.Text = dtmeetingdt.ToString("dd-MMM") + "-" + br.EndTime.ToString();
                depdt = Convert.ToString(br.MeetingDate);
            }
            #endregion


            DateTime startTime = DateTime.Now;
            DateTime endTime = Convert.ToDateTime(depdt);
            TimeSpan span = endTime.Subtract(startTime);
            int spn = span.Days;

            #region Freeze row in gray color

            if (Convert.ToInt32( startTime.Day) >= 8)
            {

                DateTime bookingdt = Convert.ToDateTime(lblBookingDt.ToolTip);

                if (bookingdt.Month < startTime.Month)
                {
                    gr.BackColor = System.Drawing.Color.LightGray;
                }
                else
                {
                    btnchkcommision.Visible = true;
                }
            
            }


            #endregion

            //#region change button visile
            //// The Check revenue button will only been seen two days after the meeting. According to SRS v1.1 , Sharon
            //if (spn < -1)
            //{
            //    btnchkcommision.Visible = true;


            //    // the replace check image wherver renvue is done
            //    Booking bookcomm = DataRepository.BookingProvider.GetById(Convert.ToInt64(btnchkcommision.CommandArgument));

            //    if (bookcomm.RevenueAmount == null)
            //    {
            //        ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
            //        // btnchkcommision.Visible = true;
            //    }
            //    else
            //    {
            //        ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = true;
            //        btnchkcommision.Visible = false;
            //    }
            //}
            //else
            //{
            //    btnchkcommision.Visible = false;


            //    // the replace check image wherver renvue is done
            //    Booking bookcomm = DataRepository.BookingProvider.GetById(Convert.ToInt64(btnchkcommision.CommandArgument));

            //    if (bookcomm.RevenueAmount == null)
            //    {
            //        ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
            //        // btnchkcommision.Visible = true;
            //    }
            //    else
            //    {
            //        ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = true;
            //        //btnchkcommision.Visible = false;
            //    }
            //}

            //#endregion
            #region ArrivalDate
            foreach (BookedMeetingRoom br in bookedMR)
            {
                DateTime dtmeetingdt = (DateTime)br.MeetingDate;
                lblArrivalDt.Text = dtmeetingdt.ToString("dd-MMM") + "-" + br.StartTime.ToString();
                break;
            }
            #endregion



            #region DepartureDate
            foreach (BookedMeetingRoom br in bookedMR)
            {
                DateTime dtmeetingdt = (DateTime)br.MeetingDate;
                lblDepartureDt.Text = dtmeetingdt.ToString("dd-MMM") + "-" + br.EndTime.ToString();
                //   return;
            }
            #endregion

        }

    }

    /// <summary>
    /// Click on link button check commision. 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnchkcommision_Click(object sender, EventArgs e)
    {
        LinkButton btnchkcomm = (LinkButton)sender;
        ViewState["ChkcommBookingID"] = btnchkcomm.CommandArgument;
        txtrealvalue.Text = "";
        ddlreason.SelectedIndex = 0;
        modalcheckcomm.Show();
    }

    /// <summary>
    /// Submit button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        Booking objbooking = DataRepository.BookingProvider.GetById(Convert.ToInt64(ViewState["ChkcommBookingID"].ToString()));
        if (chkconfirmrevenue.Checked)
        {
            objbooking.RevenueAmount = Convert.ToDecimal(lblrevenue.Text);
            objbooking.RevenueReason = "";
        }
        else
        {
            objbooking.RevenueAmount = Convert.ToDecimal(txtrealvalue.Text);
            objbooking.RevenueReason = ddlreason.SelectedItem.Text;
        }

        if (DataRepository.BookingProvider.Update(objbooking))
        {
            if (objViewBooking_Hotel.updaterequestStatus(Convert.ToInt64(ViewState["ChkcommBookingID"].ToString())))
            {
               // FetchListofHotel();
                bindgrid();
            }

        }
        txtrealvalue.Text = "";
        ddlreason.SelectedIndex = 0;

    }

    /// <summary>
    /// Event of paging page change.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void PageChange(object sender, EventArgs e)
    {
        HtmlAnchor anc = (sender as HtmlAnchor);
        int totalcount = 0;
        string whereclaus = "";
        var d = anc.InnerHtml;
        if (d == "All")
        {
            whereclaus = "";
        }
        else
        {
            whereclaus += "Contact like '" + d + "%'";
        }
        string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
        vlistreq = DataRepository.ViewbookingrequestProvider.GetPaged(whereclaus, "", 0, int.MaxValue, out totalcount);
        grdViewBooking.DataSource = vlistreq;
        grdViewBooking.DataBind();
        if (vlistreq.Count > 0)
        {
            grdViewBooking.DataSource = vlistreq;
            grdViewBooking.DataBind();
            ApplyPaging();
        }
        else
        {
            grdViewBooking.DataSource = null;
            grdViewBooking.DataBind();
        }

    }
    #endregion
}