﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using log4net.Config;
using System.Data;
using LMMR.Entities;
using LMMR.Data;
using LMMR.Business;
using System.IO;
#endregion


public partial class HotelUser_News : System.Web.UI.Page
{
    #region Variables and properties
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_News));
    Viewstatistics objViewstatistics = new Viewstatistics();
    HotelManager objHotelManager = new HotelManager();
    #endregion

    #region Page Load
    /// <summary>
    /// Initilize the controls at pageload.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["LinkID"] = "lnkStatistics";
        if (Session["CurrentHotelID"] == null)
        {
            Response.Redirect("~/login.aspx", false);
            return;
        }
        if (!Page.IsPostBack)
        {
            //Session check               
            if (Session["CurrentHotelID"] == null)
            {
                Response.Redirect("~/login.aspx", false);
                return;
            }
            objViewstatistics.HotelId =(Session["CurrentHotelID"].ToString());
            //Session check
            txtFromdate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            txtTodate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            Bindlist(txtFromdate.Text, txtTodate.Text,"days");
            divmessage.Style.Add("display", "none");
            if (Convert.ToString(Request.QueryString["type"]) == "1")
            { trvisitors.Visible = true;
            lblheader.Text = "Statistics for Visitors :"+ objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Name;
          
            }
            else  if (Convert.ToString(Request.QueryString["type"]) == "2")
                        { trBookReq.Visible = true;
                        lblheader.Text = "Statistics for Bookings  :" + objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Name;
         
            }
            else  if (Convert.ToString(Request.QueryString["type"]) == "3")
            { trrequest.Visible = true;
            lblheader.Text = "Statistics for Requests :" + objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Name;
         
            }

        }
    }
    #endregion

    #region BindList
    /// <summary>
    /// BindList
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Bindlist(string Fromdt,string Todt, string type)
    {
        objViewstatistics.HotelId = (Session["CurrentHotelID"].ToString());

        string quertype = Convert.ToString(Request.QueryString["type"]);
        DateTime fromdate = new DateTime(Convert.ToInt32(Fromdt.Split('/')[2]), Convert.ToInt32(Fromdt.Split('/')[1]), Convert.ToInt32(Fromdt.Split('/')[0]));
        DateTime todate = new DateTime(Convert.ToInt32(Todt.Split('/')[2]), Convert.ToInt32(Todt.Split('/')[1]), Convert.ToInt32(Todt.Split('/')[0]));

        if (type.ToLower() == "days")
        {
            if (quertype == "1" || string.IsNullOrEmpty(quertype))
            {

                dtlstConversion.DataSource = objViewstatistics.getbydate(fromdate, todate, "0");
                dtlstConversion.DataBind();
                dtlstVisitors.DataSource = objViewstatistics.getbydate(fromdate, todate, "0");
                dtlstVisitors.DataBind();
            }
            else if (quertype == "2")
            {
                dtlistBookReq.DataSource = objViewstatistics.getbydate(fromdate, todate, quertype);
                dtlistBookReq.DataBind();
            }
            else if (quertype == "3")
            {

                dtlistreq.DataSource = objViewstatistics.getbydate(fromdate, todate, quertype);
                dtlistreq.DataBind();
            }
            
            }

        if (type.ToLower() == "months")
        {
            if (quertype == "1" || string.IsNullOrEmpty(quertype))
            {

                dtlstConversion.DataSource = objViewstatistics.getbyMonth(fromdate, todate, "0");
                dtlstConversion.DataBind();
                dtlstVisitors.DataSource = objViewstatistics.getbyMonth(fromdate, todate, "0");
                dtlstVisitors.DataBind();
            }
            else if (quertype == "2")
            {
                dtlistBookReq.DataSource = objViewstatistics.getbyMonth(fromdate, todate, quertype);
                dtlistBookReq.DataBind();
            }
            else if (quertype == "3")
            {

                dtlistreq.DataSource = objViewstatistics.getbyMonth(fromdate, todate, quertype);
                dtlistreq.DataBind();
            }
            
           
        }

        if (type.ToLower() == "weeks")
        {
            if (quertype == "1" || string.IsNullOrEmpty(quertype))
            {

                dtlstConversion.DataSource = objViewstatistics.getbyweek(fromdate, todate, "0");
                dtlstConversion.DataBind();
                dtlstVisitors.DataSource = objViewstatistics.getbyweek(fromdate, todate, "0");
                dtlstVisitors.DataBind();
            }
            else if (quertype == "2")
            {
                dtlistBookReq.DataSource = objViewstatistics.getbyweek(fromdate, todate, quertype);
                dtlistBookReq.DataBind();
            }
            else if (quertype == "3")
            {

                dtlistreq.DataSource = objViewstatistics.getbyweek(fromdate, todate, quertype);
                dtlistreq.DataBind();
            }
        }
        if (type.ToLower() == "years")
        {
            if (quertype == "1" || string.IsNullOrEmpty(quertype))
            {

                dtlstConversion.DataSource = objViewstatistics.getbyYear(fromdate, todate, "0");
                dtlstConversion.DataBind();
                dtlstVisitors.DataSource = objViewstatistics.getbyYear(fromdate, todate, "0");
                dtlstVisitors.DataBind();
            }
            else if (quertype == "2")
            {
                dtlistBookReq.DataSource = objViewstatistics.getbyYear(fromdate, todate, quertype);
                dtlistBookReq.DataBind();
            }
            else if (quertype == "3")
            {

                dtlistreq.DataSource = objViewstatistics.getbyYear(fromdate, todate, quertype);
                dtlistreq.DataBind();
            }
        }
    }
    #endregion

    #region Search 
    /// <summary>
    /// seatch by dates
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtSearch_Click(object sender, EventArgs e)
    {

        Bindlist(txtFromdate.Text, txtTodate.Text, "days");
    }
    #endregion
       
    #region fliter by Year 
    /// <summary>
    /// method to fliter by Year     
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkYear_Click(object sender, EventArgs e)
    {
        Bindlist(txtFromdate.Text, txtTodate.Text, "years");
        lblhead.Text = "Bookings (Years)";
        lblReqHead.Text = "Requests (Years)";
        lblVisthead.Text = "Visitors (Years)";

        lnkYear.Attributes.Add("class", "btn-active");
        lnkMonths.Attributes.Add("class", "btn");
        lnkWeeks.Attributes.Add("class", "btn");
        lnkDays.Attributes.Add("class", "btn");

    }
    #endregion

    #region fliter by Month
    /// <summary>
    /// method to fliter by Month     
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkMonths_Click(object sender, EventArgs e)
    {
        Bindlist(txtFromdate.Text, txtTodate.Text, "months");
        lblhead.Text = "Bookings (Months)";
        lblReqHead.Text = "Requests (Months)";
        lblVisthead.Text = "Visitors (Months)";
        lnkYear.Attributes.Add("class", "btn");
        lnkMonths.Attributes.Add("class", "btn-active");
        lnkWeeks.Attributes.Add("class", "btn");
        lnkDays.Attributes.Add("class", "btn");

    }
    #endregion

    #region fliter by Week
    /// <summary>
    /// method to fliter by Week     
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkWeeks_Click(object sender, EventArgs e)
    {
        Bindlist(txtFromdate.Text, txtTodate.Text, "weeks");
        lblhead.Text = "Bookings (Weeks)";
        lblReqHead.Text = "Requests (Weeks)";
        lblVisthead.Text = "Visitors (Weeks)";
        lnkYear.Attributes.Add("class", "btn");
        lnkMonths.Attributes.Add("class", "btn");
        lnkWeeks.Attributes.Add("class", "btn-active");
        lnkDays.Attributes.Add("class", "btn");

    }
    #endregion

    #region fliter by Days
    /// <summary>
    /// method to fliter by Days     
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkDays_Click(object sender, EventArgs e)
    {
        Bindlist(txtFromdate.Text, txtTodate.Text, "days");
        lblhead.Text = "Bookings (Days)";
        lblReqHead.Text = "Requests (Days)";
        lblVisthead.Text = "Visitors (Days)";
        lnkYear.Attributes.Add("class", "btn");
        lnkMonths.Attributes.Add("class", "btn");
        lnkWeeks.Attributes.Add("class", "btn");
        lnkDays.Attributes.Add("class", "btn-active");
    }
    #endregion
}