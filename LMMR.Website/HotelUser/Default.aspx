﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Google GeoCoder</title>
    <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=<%= Google_API_key%>"
        type="text/javascript"></script>
</head>
<body onload="initialize_map()">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <!-- Hidden Values -->
    <asp:HiddenField ID="hf_address" runat="server" />
    <asp:HiddenField ID="hf_default_zoom" runat="server" />
    <asp:HiddenField ID="hf_latlong" runat="server" />
    <!-- Layout -->
    <a href="http://stuartmanning.com">StuartManning.com</a>
    <h1>
        Map</h1>
    <div id="map_action">
        Lat:<asp:TextBox ID="txt_lat" runat="server"></asp:TextBox>
        Long:<asp:TextBox ID="txt_long" runat="server"></asp:TextBox>
        <asp:Button ID="btn_store" runat="server" Text="Save Map Settings" OnClick="btn_store_Click" />
        <br />
        Find Address:<asp:TextBox ID="txt_address" runat="server" Width="300px"></asp:TextBox>
        <asp:Button ID="btn_find_address" runat="server" Text="Find on Map" OnClick="btn_find_address_Click" />
        <br />
        <br />
        <strong>Click on map to set Reverse GeoCode Address and Lat/Long</strong>
        <br />
        <br />
        <div id="map_canvas" style="width: 600px; height: 400px;">
        </div>
    </div>
    <!-- Google Script -->
    <script type="text/javascript">

        var map;
        var geocoder;

        var address = '<%= hf_address.Value %>';
        var default_zoom = parseInt('<%= default_zoom %>');

        var default_point = new GLatLng(-79.45278, 43.65669);       

        //ASP.NET Controls using MicrosoftAjax.js - loaded by Scriptmanager
        var txt_lat = $get("<%= txt_lat.ClientID %>");
        var txt_long = $get("<%= txt_long.ClientID %>");

        //Functions

        function initialize_map() {
            map = new GMap2(document.getElementById("map_canvas"));
            map.setCenter(default_point, default_zoom);
            map.addControl(new GLargeMapControl);
            GEvent.addListener(map, "click", getAddress);
            geocoder = new GClientGeocoder();
            get_latlong(address);
        }

        function getAddress(overlay, latlng) {
            if (latlng != null) {
                address = latlng;
                geocoder.getLocations(latlng, showAddress);
            }
        }

        function get_latlong(address) {
            if (geocoder) {
                geocoder.getLatLng(
                  address,
                  function (point) {
                      if (!point) {
                          alert(address + " not found");
                      } else {
                          map.setCenter(point, 13);
                          var greenIcon = new GIcon(G_DEFAULT_ICON);
                          greenIcon.image = "http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
                          var markerOptions = { icon: greenIcon };
                          var marker = new GMarker(point, markerOptions);
                          map.addOverlay(marker);
                          marker.openInfoWindowHtml(address);

                          txt_lat.value = point.y;
                          txt_long.value = point.x;
                      }
                  }
                );
            }
        }


        function showAddress(response) {
            map.clearOverlays();
            if (!response || response.Status.code != 200) {
                alert("Status Code:" + response.Status.code);
            } else {
                place = response.Placemark[0];
                point = new GLatLng(place.Point.coordinates[1], place.Point.coordinates[0]);
                var greenIcon = new GIcon(G_DEFAULT_ICON);
                greenIcon.image = "http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
                var markerOptions = { icon: greenIcon };

                //var marker = new GMarker(point, markerOptions);
                marker = new GMarker(point, markerOptions);
                map.addOverlay(marker);
                map.setCenter(point);

                try {
                    txt_lat.value = point.y;
                    txt_long.value = point.x;
                    marker.openInfoWindowHtml(
                    '<b>orig latlng:</b>' + response.name + '<br/>' +
'<b>latlng:</b>' + place.Point.coordinates[0] + "," + place.Point.coordinates[1] + '<br>' +
                    //'<b>Status Code:</b>' + response.Status.code + '<br>' +
                    //'<b>Status Request:</b>' + response.Status.request + '<br>' +
'<b>Address:</b>' + place.address + '<br>' +
'<b>Accuracy:</b>' + place.AddressDetails.Accuracy + '<br>' +
'<b>Country:</b> ' + place.AddressDetails.Country.CountryName);

                    //alert(elem.value);
                }
                catch (err) {

                }
            }
        }



    </script>
    <!-- End Google Script -->
    </form>
</body>
</html>
