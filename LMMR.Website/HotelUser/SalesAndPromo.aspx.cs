﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Data;
using LMMR.Entities;
using System.Configuration;
using log4net;
using log4net.Config;
#endregion


public partial class HotelUser_SalesAndPromo : System.Web.UI.Page
{
    #region Variables and properties
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_SalesAndPromo));
    public int count = 0;
    public string MonthYearDayDate
    {
        get;
        set;
    }
    public string StartSalesandPromo
    {
        get;
        set;
    }

    public string EndSalesandPromo
    {
        get;
        set;
    }

    public string PackageDivNames
    {
        get;
        set;
    }
    public string MeetingRoomDivNames
    {
        get;
        set;
    }
    public string BedroomDivs
    {
        get;
        set;
    }
    public string CurrentPanel
    {
        get;
        set;
    }
    //Get Site Root path
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            if (string.IsNullOrEmpty(appRootUrl) || appRootUrl == "/")
            {
                return host + "/";
            }
            else
            {
                return host + appRootUrl + "/";
            }
        }
    }
    
    //Service URL set from here
    public string ServiceUrl
    {
        get
        {
            return "'" + SiteRootPath + "LMMRwebservice.asmx'";
            //return "'" + Convert.ToString(ConfigurationManager.AppSettings["WebserviceURL"]) + "'";
        }
    }
    public int Bedroom1Available
    {
        get;
        set;
    }
    public string Bedroom1Type
    {
        get;
        set;
    }
    public int Bedroom2Available
    {
        get;
        set;
    }
    public string Bedroom2Type
    {
        get;
        set;
    }
    HotelManager objHotelManager = new HotelManager();
    #endregion

    #region PageLoad
    /// <summary>
    /// Method to manage the page load functionality.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["CurrentHotelID"] == null)
            {
                Response.Redirect("~/login.aspx",false );
                return;
            }
            Session["LinkID"] = "lnkPromotions";
            if (!Page.IsPostBack)
            {
                CurrentPanel = (Session["CurrentPanel"] == null ? "1" : Convert.ToString(Session["CurrentPanel"]));
                Session["CurrentHotelID"] = Session["CurrentHotelID"] == null ? 0 : Session["CurrentHotelID"];
                txtFrom.Text = DateTime.Now.ToString("dd/MM/yy");
                txtTo.Text = DateTime.Now.ToString("dd/MM/yy");
                CalendarExtender1.StartDate = DateTime.Now;
                CalendarExtender1.EndDate = DateTime.Now.AddDays(60);
                CalendarExtender2.StartDate = DateTime.Now;
                CalendarExtender2.EndDate = DateTime.Now.AddDays(60);
                txtBRFrom.Text = DateTime.Now.ToString("dd/MM/yy");
                txtBRTo.Text = DateTime.Now.ToString("dd/MM/yy");
                CalendarExtender3.StartDate = DateTime.Now;
                CalendarExtender3.EndDate = DateTime.Now.AddDays(60);
                CalendarExtender4.StartDate = DateTime.Now;
                CalendarExtender4.EndDate = DateTime.Now.AddDays(60);
                BindFormSpecialPriceAndPromo();
                Session["CurrentPanel"] = null;
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Bind Special Price and Promo
    /// <summary>
    /// Bind Special price and Promo
    /// </summary>
    protected void BindFormSpecialPriceAndPromo()
    {
        try
        {
            //Set all the Month days and Year value.
            string GetmonthDayYear = "[";
            string StartDate = string.Empty;
            string EndDate = string.Empty;
            int startday = 0;
            int monthstart = 0;
            DateTime dt = new DateTime();
            //Check all the values for month, day and year.
            while (startday < 60)
            {
                if (monthstart == 0)
                {
                    dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    monthstart++;
                    startday += System.DateTime.DaysInMonth(dt.Year, dt.Month) - DateTime.Now.Day + 1;
                    StartDate = "[" + (DateTime.Now.Day) + "," + DateTime.Now.Month + "]";
                }
                else
                {
                    dt = dt.AddMonths(1);
                    monthstart++;
                    int oldstartday = startday;
                    startday += (startday + System.DateTime.DaysInMonth(dt.Year, dt.Month)) <= 60 ? System.DateTime.DaysInMonth(dt.Year, dt.Month) : Math.Abs(60 - startday);
                    EndDate = "[" + ((oldstartday + System.DateTime.DaysInMonth(dt.Year, dt.Month)) <= 60 ? System.DateTime.DaysInMonth(dt.Year, dt.Month) : Math.Abs(60 - oldstartday)) + "," + dt.Month + "]";
                }
                GetmonthDayYear += "[" + dt.Month + "," + dt.Year + "," + System.DateTime.DaysInMonth(dt.Year, dt.Month) + "," + (int)dt.DayOfWeek + "],";
            }
            if (GetmonthDayYear.Length != 0)
            {
                GetmonthDayYear = GetmonthDayYear.Substring(0, GetmonthDayYear.Length - 1);
            }
            GetmonthDayYear += "]";
            MonthYearDayDate = GetmonthDayYear;
            StartSalesandPromo = StartDate;
            EndSalesandPromo = EndDate;
            //------------Check Meetingroom By Current Hotel ID. ------------------
            //THIS PART IS NO MORE IN USE. BECAUSE CLIENT NEED TO REMOVE THE MEETINGROOM FROM THE FRONTEND OF SPECIAL PRICE AND PROMO PAGE
            #region Commented By Gaurav. Date:29th March 2012
            TList<MeetingRoom> lstMeetingRoom = new TList<MeetingRoom>();
            lstMeetingRoom = objHotelManager.GetMeetingRoomByHotelId(Convert.ToInt64(Session["CurrentHotelID"]));
            MeetingRoomDivNames = "[";
            foreach (MeetingRoom m in lstMeetingRoom)
            {
                //ltrMeetingRoom.Text += @"<h4 class='ddr1'>" + m.Name + "</h4><h4 class='ddr2'>1/2 Day:</h4><ul class='ddr3' id='" + m.Id + "_MRHalfday' ></ul><h4 class='ddr2'>Full Day:</h4><ul class='ddr3' id='" + m.Id + "_MRFullday' ></ul>";
                //MeetingRoomDivNames += "'" + m.Id + "',";
                //ltrMeetingRoomCheckbox.Text += @"<div class='adjust-meeting-form-body-box1left'>" + m.Name + "</div><div class='adjust-meeting-form-body-box1right'><input type='checkbox' alt='" + m.Id + "' id='chk" + m.Name.Trim().Replace(" ", "") + "' name='chbox'></div>";
            }

            //if (MeetingRoomDivNames.Length > 1)
            //{
            //    MeetingRoomDivNames = MeetingRoomDivNames.Substring(0, MeetingRoomDivNames.Length-1);
            //}
            MeetingRoomDivNames += "]";
            #endregion

            //--------------Check for packages according to current hotel-------------------
            //ONLY WORK FOR STANDARED PACKAGE AND REMOVE ALL THE OTHER PACKAGE FROM THE RESULT.
            PackageDivNames = "[";
            TList<PackageMaster> objPackage = objHotelManager.GetActiveStandardPackageByHotel(Convert.ToInt64(Session["CurrentHotelID"]));
            Int64 oldID = 0;
            foreach (PackageMaster p in objPackage)
            {
                if (oldID != p.Id)
                {
                    //ltrPackage.Text += @"<h4 class='ddr1'>" + p.PackageName + "</h4><h4 class='ddr2'>1/2 Day:</h4><ul class='ddr3' id='" + p.Id + "_PHalfday' ></ul><h4 class='ddr2'>Full Day:</h4><ul class='ddr3' id='" + p.Id + "_PFullday' ><ul>";
                    ltrPackage.Text += @"<tr bgcolor='#d9eefc'><th colspan='31' class='head2' height='21' align='left'>Package " + p.PackageName + "</th><tr bgcolor='#d9eefc'><th colspan='31' style='text-align:left;' align='left'><span>Full day price:</span></th></tr><tr id='" + p.Id + "_PHalfday' ></tr><tr bgcolor='#d9eefc'><th colspan='31' style='text-align:left;' align='left'><span>New price:</span></th></tr><tr id='" + p.Id + "_PFullday' ></tr>";
                    PackageDivNames += "'" + p.Id + "',";
                    oldID = p.Id;
                }
            }
            if (PackageDivNames.Length > 1)
            {
                PackageDivNames = PackageDivNames.Substring(0, PackageDivNames.Length-1);
            }
            PackageDivNames += "]";
            
            //-----------------Check for bedroom details according to current Hotel id-----------------------
            //THIS IS NEW PART ADDED BY CLICNT TO SAVE THE DETAILS OF THE BEDROOM FOR EACHDAY
            BedroomDivs = "[";
            TList<BedRoom> objBedroom = objHotelManager.GetBedRoomByhotelId(Convert.ToInt64(Session["CurrentHotelID"]));
            
            foreach (BedRoom b in objBedroom)
            {
                string bedroomtype = b.Types == null ? "Standard" : (b.Types == (int)LMMR.Business.BedRoomType.Standard ? "Standard" : (b.Types == (int)LMMR.Business.BedRoomType.Superior ? "Superior" : (b.Types == (int)LMMR.Business.BedRoomType.Executive ? "Executive" : b.Types == (int)LMMR.Business.BedRoomType.Business ? "Business" : b.Types == (int)LMMR.Business.BedRoomType.Classic ? "Classic" : b.Types == (int)LMMR.Business.BedRoomType.Deluxe ? "Deluxe" : "Standard")));
                if (count == 0)
                {
                    hdnBedroom1ID.Value = Convert.ToString(b.Id);
                    Bedroom1Available = 1;
                    Bedroom1Type = bedroomtype;
                }
                else
                {
                    hdnBedroom2ID.Value = Convert.ToString(b.Id);
                    Bedroom2Available = 1;
                    Bedroom2Type = bedroomtype;
                }
                ltrBedroom.Text += @"<tr bgcolor='#d9eefc'><th colspan='31' class='head2' height='21' align='left'>" + bedroomtype + "</th><tr bgcolor='#d9eefc'><th colspan='31' style='text-align:left;' align='left'><span>Single bedroom price:</span></th></tr><tr id='" + b.Id + "_Single' ></tr><tr bgcolor='#d9eefc'><th colspan='31' style='text-align:left;' align='left'><span>Double bedroom price:</span></th></tr><tr id='" + b.Id + "_Double' ></tr>";
                BedroomDivs += "'" + b.Id + "',";
                oldID = b.Id;
                count++;
            }
            if (BedroomDivs.Length > 1)
            {
                BedroomDivs = BedroomDivs.Substring(0, BedroomDivs.Length - 1);
            }
            BedroomDivs += "]";
            if (count > 0)
            {
                savebedroom.Visible = true;
            }
            else
            {
                savebedroom.Visible = false;
            }
            Others onew = objHotelManager.GetOtherSectionValuesByHotelID(Convert.ToInt64(Session["CurrentHotelID"]));
            if (onew != null)
            {
                hdnMinPercentage.Value = Convert.ToString(onew.MinIntervalVariation);
                hdnMaxPercentage.Value = Convert.ToString(onew.MaxIntervalVariation);
            }
            else
            {
                hdnMinPercentage.Value = Convert.ToString("-100");
                hdnMaxPercentage.Value = Convert.ToString("100");
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Submit Discount
    /// <summary>
    /// adjust all values of special price and promo.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkSave_Click(object sender, EventArgs e)
    {
        try
        {
            string discountmeetingroom = hdnMeetingRoomDiscount.Value;
            string discountpackage = hdnPackageDiscount.Value;
            string startmonthandyear = hdnStartingDate.Value;
            DateTime startdate;
            if (Convert.ToInt32(startmonthandyear.Split('-')[0]) == DateTime.Now.Month)
            {
                startdate = new DateTime(Convert.ToInt32(startmonthandyear.Split('-')[1]), Convert.ToInt32(startmonthandyear.Split('-')[0]), DateTime.Now.Day);
            }
            else
            {
                startdate = new DateTime(Convert.ToInt32(startmonthandyear.Split('-')[1]), Convert.ToInt32(startmonthandyear.Split('-')[0]), 1);
            }
            objHotelManager.UpdateDiscountByHotelId(Convert.ToInt64(Session["CurrentHotelID"]), startdate, discountmeetingroom, discountpackage);
            Session["CurrentPanel"] = hdnCurrentPanel.Value;
            Response.Redirect("~/HotelUser/SalesAndPromo.aspx",false);
            return;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }


    /// <summary>
    /// Save all values of the bedroom
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkSaveBedroom_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(hdnBedroom1ID.Value))
        {
            string singlePrice = hdnBedroom1single.Value;
            string doublePrice = hdnBedroom1double.Value;
            string startmonthandyear = hdnStartingDate.Value;
            DateTime startdate;
            if (Convert.ToInt32(startmonthandyear.Split('-')[0]) == DateTime.Now.Month)
            {
                startdate = new DateTime(Convert.ToInt32(startmonthandyear.Split('-')[1]), Convert.ToInt32(startmonthandyear.Split('-')[0]), DateTime.Now.Day);
            }
            else
            {
                startdate = new DateTime(Convert.ToInt32(startmonthandyear.Split('-')[1]), Convert.ToInt32(startmonthandyear.Split('-')[0]), 1);
            }
            objHotelManager.UpdateSpecialPriceofBedroomHotelId(Convert.ToInt64(Session["CurrentHotelID"]), startdate, singlePrice, doublePrice, Convert.ToInt64(hdnBedroom1ID.Value));
        }
        if (!string.IsNullOrEmpty(hdnBedroom2ID.Value))
        {
            string singlePrice = hdnBedroom2single.Value;
            string doublePrice = hdnBedroom2double.Value;
            string startmonthandyear = hdnStartingDate.Value;
            DateTime startdate;
            if (Convert.ToInt32(startmonthandyear.Split('-')[0]) == DateTime.Now.Month)
            {
                startdate = new DateTime(Convert.ToInt32(startmonthandyear.Split('-')[1]), Convert.ToInt32(startmonthandyear.Split('-')[0]), DateTime.Now.Day);
            }
            else
            {
                startdate = new DateTime(Convert.ToInt32(startmonthandyear.Split('-')[1]), Convert.ToInt32(startmonthandyear.Split('-')[0]), 1);
            }
            objHotelManager.UpdateSpecialPriceofBedroomHotelId(Convert.ToInt64(Session["CurrentHotelID"]), startdate, singlePrice, doublePrice, Convert.ToInt64(hdnBedroom2ID.Value));
        }
        Session["CurrentPanel"] = hdnCurrentPanel.Value;
        Response.Redirect("~/HotelUser/SalesAndPromo.aspx",false);
        return;
    }

    /// <summary>
    /// Save Bedroom prices
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ancBRSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string DaysChecked = "";
            string PriceEntered = "";
            if (chkBRAll.Checked)
            {
                DaysChecked = "1|1|1|1|1|1|1";
                PriceEntered = (string.IsNullOrEmpty(txtBRALL.Text.Trim()) ? "0" : txtBRALL.Text.Trim()) + "|" + (string.IsNullOrEmpty(txtBRALL.Text.Trim()) ? "0" : txtBRALL.Text.Trim()) + "|" + (string.IsNullOrEmpty(txtBRALL.Text.Trim()) ? "0" : txtBRALL.Text.Trim()) + "|" + (string.IsNullOrEmpty(txtBRALL.Text.Trim()) ? "0" : txtBRALL.Text.Trim()) + "|" + (string.IsNullOrEmpty(txtBRALL.Text.Trim()) ? "0" : txtBRALL.Text.Trim()) + "|" + (string.IsNullOrEmpty(txtBRALL.Text.Trim()) ? "0" : txtBRALL.Text.Trim()) + "|" + (string.IsNullOrEmpty(txtBRALL.Text.Trim()) ? "0" : txtBRALL.Text.Trim());
            }
            else
            {
                if (chkBRSunday.Checked)
                {
                    DaysChecked += "1|";
                    PriceEntered += (string.IsNullOrEmpty(txtBRSunday.Text.Trim()) ? "0" : txtBRSunday.Text.Trim()) + "|";
                }
                else
                {
                    DaysChecked += "0|";
                    PriceEntered += "0|";
                }
                if (chkBRMonday.Checked)
                {
                    DaysChecked += "1|";
                    PriceEntered += (string.IsNullOrEmpty(txtBRMonday.Text.Trim()) ? "0" : txtBRMonday.Text.Trim()) + "|";
                }
                else
                {
                    DaysChecked += "0|";
                    PriceEntered += "0|";
                }
                if (chkBRTuesday.Checked)
                {
                    DaysChecked += "1|";
                    PriceEntered += (string.IsNullOrEmpty(txtBRTuesday.Text.Trim()) ? "0" : txtBRTuesday.Text.Trim()) + "|";
                }
                else
                {
                    DaysChecked += "0|";
                    PriceEntered += "0|";
                }
                if (chkBRWednesday.Checked)
                {
                    DaysChecked += "1|";
                    PriceEntered += (string.IsNullOrEmpty(txtBRWednesday.Text.Trim()) ? "0" : txtBRWednesday.Text.Trim()) + "|";
                }
                else
                {
                    DaysChecked += "0|";
                    PriceEntered += "0|";
                }
                if (chkBRThursday.Checked)
                {
                    DaysChecked += "1|";
                    PriceEntered += (string.IsNullOrEmpty(txtBRThursday.Text.Trim()) ? "0" : txtBRThursday.Text.Trim()) + "|";
                }
                else
                {
                    DaysChecked += "0|";
                    PriceEntered += "0|";
                }
                if (chkBRFriday.Checked)
                {
                    DaysChecked += "1|";
                    PriceEntered += (string.IsNullOrEmpty(txtBRFriday.Text.Trim()) ? "0" : txtBRFriday.Text.Trim()) + "|";
                }
                else
                {
                    DaysChecked += "0|";
                    PriceEntered += "0|";
                }
                if (chkBRSaturday.Checked)
                {
                    DaysChecked += "1|";
                    PriceEntered += (string.IsNullOrEmpty(txtBRSaturday.Text.Trim()) ? "0" : txtBRSaturday.Text.Trim()) + "|";
                }
                else
                {
                    DaysChecked += "0|";
                    PriceEntered += "0|";
                }
                if (DaysChecked.Length > 0)
                {
                    DaysChecked = DaysChecked.Substring(0, DaysChecked.Length - 1);
                }
                if (PriceEntered.Length > 0)
                {
                    PriceEntered = PriceEntered.Substring(0, PriceEntered.Length - 1);
                }
            }
            if (rbtnBRAll.Checked)
            {
                objHotelManager.SpecialPriceAdjustBedroom(Convert.ToInt64(Session["CurrentHotelID"]), DaysChecked.Replace("'", ""), PriceEntered.Replace("'", ""), txtBRFrom.Text.Replace("'", ""), txtBRTo.Text.Replace("'", ""), 0,"All");
            }
            if (rbtnBrBedroom1.Checked)
            {
                objHotelManager.SpecialPriceAdjustBedroom(Convert.ToInt64(Session["CurrentHotelID"]), DaysChecked.Replace("'", ""), PriceEntered.Replace("'", ""), txtBRFrom.Text.Replace("'", ""), txtBRTo.Text.Replace("'", ""), Convert.ToInt64(hdnBedroom1ID.Value),"Single");
            }
            if (rbtnBrBedroom1Double.Checked)
            {
                objHotelManager.SpecialPriceAdjustBedroom(Convert.ToInt64(Session["CurrentHotelID"]), DaysChecked.Replace("'", ""), PriceEntered.Replace("'", ""), txtBRFrom.Text.Replace("'", ""), txtBRTo.Text.Replace("'", ""), Convert.ToInt64(hdnBedroom1ID.Value),"Double");
            }
            if (rbtnBrBedroom2.Checked)
            {
                objHotelManager.SpecialPriceAdjustBedroom(Convert.ToInt64(Session["CurrentHotelID"]), DaysChecked.Replace("'", ""), PriceEntered.Replace("'", ""), txtBRFrom.Text.Replace("'", ""), txtBRTo.Text.Replace("'", ""), Convert.ToInt64(hdnBedroom2ID.Value), "Single");
            }
            if (rbtnBrBedroom2.Checked)
            {
                objHotelManager.SpecialPriceAdjustBedroom(Convert.ToInt64(Session["CurrentHotelID"]), DaysChecked.Replace("'", ""), PriceEntered.Replace("'", ""), txtBRFrom.Text.Replace("'", ""), txtBRTo.Text.Replace("'", ""), Convert.ToInt64(hdnBedroom2ID.Value), "Double");
            }
            Session["CurrentPanel"] = hdnCurrentPanel.Value;
            Response.Redirect("~/HotelUser/SalesAndPromo.aspx",false);
            return;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// Adjust special price and promo.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ancSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string DaysChecked = "";
            string discountEntered = "";
            if (chkAll.Checked)
            {
                DaysChecked = "1|1|1|1|1|1|1";
                discountEntered = (string.IsNullOrEmpty(txtAll.Text.Trim()) ? "0" : txtAll.Text.Trim()) + "|" + (string.IsNullOrEmpty(txtAll.Text.Trim()) ? "0" : txtAll.Text.Trim()) + "|" + (string.IsNullOrEmpty(txtAll.Text.Trim()) ? "0" : txtAll.Text.Trim()) + "|" + (string.IsNullOrEmpty(txtAll.Text.Trim()) ? "0" : txtAll.Text.Trim()) + "|" + (string.IsNullOrEmpty(txtAll.Text.Trim()) ? "0" : txtAll.Text.Trim()) + "|" + (string.IsNullOrEmpty(txtAll.Text.Trim()) ? "0" : txtAll.Text.Trim()) + "|" + (string.IsNullOrEmpty(txtAll.Text.Trim()) ? "0" : txtAll.Text.Trim());
            }
            else
            {
                if (chkSunday.Checked)
                {
                    DaysChecked += "1|";
                    discountEntered += (string.IsNullOrEmpty(txtSunday.Text.Trim()) ? "0" : txtSunday.Text.Trim()) + "|";
                }
                else
                {
                    DaysChecked += "0|";
                    discountEntered += "0|";
                }
                if (chkMonday.Checked)
                {
                    DaysChecked += "1|";
                    discountEntered += (string.IsNullOrEmpty(txtMonday.Text.Trim()) ? "0" : txtMonday.Text.Trim()) + "|";
                }
                else
                {
                    DaysChecked += "0|";
                    discountEntered += "0|";
                }
                if (chkTuesday.Checked)
                {
                    DaysChecked += "1|";
                    discountEntered += (string.IsNullOrEmpty(txtTuesday.Text.Trim()) ? "0" : txtTuesday.Text.Trim()) + "|";
                }
                else
                {
                    DaysChecked += "0|";
                    discountEntered += "0|";
                }
                if (chkWednesday.Checked)
                {
                    DaysChecked += "1|";
                    discountEntered += (string.IsNullOrEmpty(txtWednesday.Text.Trim()) ? "0" : txtWednesday.Text.Trim()) + "|";
                }
                else
                {
                    DaysChecked += "0|";
                    discountEntered += "0|";
                }
                if (chkThursday.Checked)
                {
                    DaysChecked += "1|";
                    discountEntered += (string.IsNullOrEmpty(txtThursday.Text.Trim()) ? "0" : txtThursday.Text.Trim()) + "|";
                }
                else
                {
                    DaysChecked += "0|";
                    discountEntered += "0|";
                }
                if (chkFriday.Checked)
                {
                    DaysChecked += "1|";
                    discountEntered += (string.IsNullOrEmpty(txtFriday.Text.Trim()) ? "0" : txtFriday.Text.Trim()) + "|";
                }
                else
                {
                    DaysChecked += "0|";
                    discountEntered += "0|";
                }
                if (chkSaturday.Checked)
                {
                    DaysChecked += "1|";
                    discountEntered += (string.IsNullOrEmpty(txtSaturday.Text.Trim()) ? "0" : txtSaturday.Text.Trim()) + "|";
                }
                else
                {
                    DaysChecked += "0|";
                    discountEntered += "0|";
                }
                if (DaysChecked.Length > 0)
                {
                    DaysChecked = DaysChecked.Substring(0, DaysChecked.Length - 1);
                }
                if (discountEntered.Length > 0)
                {
                    discountEntered = discountEntered.Substring(0, discountEntered.Length - 1);
                }
            }
            if (rbtnAll.Checked)
            {
                objHotelManager.SpecialPriceAdjust(Convert.ToInt64(Session["CurrentHotelID"]), DaysChecked.Replace("'", ""), discountEntered.Replace("'", ""), txtFrom.Text.Replace("'", ""), txtTo.Text.Replace("'", ""), "ALL");
            }
            if (rbtnDDRs.Checked)
            {
                objHotelManager.SpecialPriceAdjust(Convert.ToInt64(Session["CurrentHotelID"]), DaysChecked.Replace("'", ""), discountEntered.Replace("'", ""), txtFrom.Text.Replace("'", ""), txtTo.Text.Replace("'", ""), "DDRs");
            }
            if (rbtnMeetingRoom.Checked)
            {
                objHotelManager.SpecialPriceAdjust(Convert.ToInt64(Session["CurrentHotelID"]), DaysChecked.Replace("'", ""), discountEntered.Replace("'", ""), txtFrom.Text.Replace("'", ""), txtTo.Text.Replace("'", ""), "MeetingRoom");
            }
            Session["CurrentPanel"] = hdnCurrentPanel.Value;
            Response.Redirect("~/HotelUser/SalesAndPromo.aspx",false);
            return;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion
    
}