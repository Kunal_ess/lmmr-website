﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using System.Web.UI.HtmlControls;
using System.Configuration;
using log4net;
using log4net.Config;
#endregion

public partial class HotelUser_PreviewOnWeb : System.Web.UI.Page
{
    #region variable declaration
    MeetingRoomConfigManager ObjMeetingRoomConfigManager = new MeetingRoomConfigManager();
    MeetingRoomDescManager ObjMeetingRoomDescManager = new MeetingRoomDescManager();
    int hotelPid = 0;    
    public int default_zoom = 1;
    WizardLinkSettingManager Objwizard = new WizardLinkSettingManager();
    DashboardManager Objdash = new DashboardManager();
    HotelManager objHotel = new HotelManager();
    BookingRequest objBookingRequest = new BookingRequest();
    PackagePricingManager objPackagePricingManager = new PackagePricingManager();
    Facilities objFacility = new Facilities();
    VList<ViewFacilityIcon> vicon;
    HotelInfo ObjHotelinfo = new HotelInfo();
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_PreviewOnWeb));
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl + "/";
        }
    }
    #endregion    

    #region PageLoad
    /// <summary>
    /// On page load we manage all the containts and Apply paging if there is a post back.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Session for Hotelname        
            if (Session["CurrentHotelID"] == null)
            {
                //Response.Redirect("~/login.aspx");
                Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                Session.Abandon();
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "login/english");
                }
            }

            hotelPid = Convert.ToInt32(Session["CurrentHotelID"]);
            //Session for Hotelnames  
            if (!IsPostBack)
            {
                Session["LinkID"] = "lnkPropertyLevel";
                BindMeetingRoomBooking(hotelPid);
                Bindlanguage();
                BindHotelDetails();
                //#region Leftmenu
                //HyperLink lnk = (HyperLink)this.Master.FindControl("lnkConferenceInfo");
                //lnk.CssClass = "";
                //lnk = (HyperLink)this.Master.FindControl("lnkContactDetails");
                //lnk.CssClass = "";
                //lnk = (HyperLink)this.Master.FindControl("lnkFacilities");
                //lnk.CssClass = "";
                //lnk = (HyperLink)this.Master.FindControl("lnkCancelationPolicy");
                //lnk.CssClass = "";
                //lnk = (HyperLink)this.Master.FindControl("lnkDescriptionMeetingRoom");
                //lnk.CssClass = "";
                //lnk = (HyperLink)this.Master.FindControl("lnkConfiguration");
                //lnk.CssClass = "";
                //lnk = (HyperLink)this.Master.FindControl("lnkPrintFactSheet");
                //lnk.CssClass = "";
                //lnk = (HyperLink)this.Master.FindControl("lnkPreviewOnWeb");
                //lnk.CssClass = "selected";

                //#endregion 
                //For facility icon
                BindFacilityIcon();
                Getgoonline();
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion    

    #region Additional Methods
    /// <summary>
    /// For check go online for this hotel
    /// </summary>
    public void Getgoonline()
    {
        try
        {
            if (ObjHotelinfo.GetHotelGoOnline(hotelPid).Count > 0)
            {
                divNext.Visible = false;
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        
        }
    }

    /// <summary>
    /// Bind all meeting room details to list view for Booking and request
    /// </summary>
    void BindMeetingRoomBooking(int hotelPid)
    {
        try
        {
            lstViewBooking.DataSource = ObjMeetingRoomDescManager.GetAllMeetingRoom(hotelPid);
            lstViewBooking.DataBind();

            lstViewRequest.DataSource = ObjMeetingRoomDescManager.GetAllMeetingRoom(hotelPid);
            lstViewRequest.DataBind();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// For geeting hotelname,description,latitude,longitude per specific hotel
    /// </summary>
    public void BindHotelDetails()
    {
        try
        {
            Hotel hotel = ObjHotelinfo.GetHotelByHotelID(hotelPid);
            lblHotelName.Text = hotel.Name;
            imgHotelMainImage.ImageUrl = ConfigurationManager.AppSettings["FilePath"] + "/HotelImage/" + hotel.Logo;
            string whereclausedesc = "Hotel_Id=" + hotel.Id + " and " + "Language_Id=" + drplanguage.SelectedValue + "";
            TList<HotelDesc> lsthdesc = ObjHotelinfo.GetHotelDesc(whereclausedesc, String.Empty);
            if (lsthdesc.Count > 0)
            {
                lblDescription.Text = lsthdesc[0].Description;
                lblAddress.Text = lsthdesc[0].Address;
                hf_address.Value = lsthdesc[0].Address;
                //For google map
                //hf_address.Value = lsthdesc[0].Address;
                hf_latitude.Value = hotel.Latitude;
                hf_longtitude.Value = hotel.Longitude;
                //For google map
            }
            else
            {
                lblDescription.Text = "Description Not Available";
                lblAddress.Text = "Address Not Available";
            }
            //for static hotel star
            if (hotel.Stars == 1)
            {
                imgStars.ImageUrl = "~/Images/1.png";
            }
            else if (hotel.Stars == 2)
            {
                imgStars.ImageUrl = "~/Images/2.png";
            }
            else if (hotel.Stars == 3)
            {
                imgStars.ImageUrl = "~/Images/3.png";
            }
            else if (hotel.Stars == 4)
            {
                imgStars.ImageUrl = "~/Images/4.png";
            }
            else if (hotel.Stars == 5)
            {
                imgStars.ImageUrl = "~/Images/5.png";
            }
            else if (hotel.Stars == 6)
            {
                imgStars.ImageUrl = "~/Images/6.png";
            }
            else if (hotel.Stars == 7)
            {
                imgStars.ImageUrl = "~/Images/7.png";
            }
            //for static hotel star

            //Get Price from actualpackageprice by hotelid with discount
            DateTime fromDate = new DateTime(Convert.ToInt32("20" + DateTime.Now.ToString("dd/MM/yy").Split('/')[2]), Convert.ToInt32(DateTime.Now.ToString("dd/MM/yy").Split('/')[1]), Convert.ToInt32(DateTime.Now.ToString("dd/MM/yy").Split('/')[0]));            
            decimal intDiscountPK = 0;
            intDiscountPK = objHotel.GetPackageDiscountByPackageIDandDate(hotel.Id, fromDate);
            if (intDiscountPK < 0)
            {                
                divDiscount.Style.Add("display", "block");
                lblActPkgPrice.Text = String.Format("{0:#,###}", objBookingRequest.GetActualpkgPricebyHotelid(Convert.ToInt32(hotel.Id))[0].ActualFullDayPrice);
                decimal ActualPrice=Convert.ToDecimal(objBookingRequest.GetActualpkgPricebyHotelid(Convert.ToInt32(hotel.Id))[0].ActualFullDayPrice);
                lblActPkgPrice.Text = String.Format("{0:#,###}", (Math.Abs(ActualPrice)));
                TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(hotel.Id));
                decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => u.PackageId == 2).Select(u => u.FulldayPrice).Sum());
                decimal Fulldaypkgprice = (Math.Abs((ActualPrice - getTotleFullDayPrice)));
                decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * intDiscountPK / 100)));
                lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (ActualPrice - PkgDiscountPrice));                
            }
            //Get Price from actualpackageprice by hotelid without discount
            else
            {
                if (objBookingRequest.GetActualpkgPricebyHotelid(Convert.ToInt32(hotel.Id)).Count > 0)
                {
                    divPkg.Style.Add("display", "block");
                    lblActPAckagePrice.Text = String.Format("{0:#,##,##0.0}", objBookingRequest.GetActualpkgPricebyHotelid(Convert.ToInt32(hotel.Id))[0].ActualFullDayPrice);
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }


    /// <summary>
    /// Start binding all active language
    /// </summary>
    public void Bindlanguage()
    {
        try
        {
            string whereclause = "IsActive=1";
            TList<Language> lsthActiveLang = ObjHotelinfo.GetLanguage(whereclause, String.Empty);
            drplanguage.DataTextField = "Name";
            drplanguage.DataValueField = "Id";
            drplanguage.DataSource = lsthActiveLang;
            drplanguage.DataBind();
        }
        catch (Exception ex)
        {

            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// Start bind facility icon if it exist for this particular hotel
    /// </summary>    
    public void BindFacilityIcon()
    {
        try
        {
            string whereclaus1 = "Hotel_Id=" + hotelPid + " and Icon='bar-resto.png'";
            vicon = objFacility.GetFacilityIcon(whereclaus1, String.Empty);
            if (vicon.Count > 0)
            {
                img1.Visible = true;
            }

            string whereclaus3 = "Hotel_Id=" + hotelPid + " and Icon='daylight-in-all-meeting-rooms.png'";
            vicon = objFacility.GetFacilityIcon(whereclaus3, String.Empty);
            if (vicon.Count > 0)
            {
                img2.Visible = true;
            }

            string whereclaus4 = "Id=" + hotelPid + " and Icon='meeting.png'";
            vicon = objFacility.GetFacilityIcon(whereclaus4, String.Empty);
            if (vicon.Count > 0)
            {
                img3.Visible = true;
            }
            string whereclaus = "Hotel_Id=" + hotelPid + " and Icon='private-parking.png'";
            vicon = objFacility.GetFacilityIcon(whereclaus, String.Empty);
            if (vicon.Count > 0)
            {
                img4.Visible = true;
            }

            string whereclaus2 = "Hotel_Id=" + hotelPid + " and Icon='wifi.png'";
            vicon = objFacility.GetFacilityIcon(whereclaus2, String.Empty);
            if (vicon.Count > 0)
            {
                img5.Visible = true;
            }
            string whercaluse6 = "Id=" + hotelPid + " and " + "IsBedroomAvailable=1";
            TList<Hotel> lsthLink = ObjHotelinfo.GetHotelbyCondition(whercaluse6, String.Empty);
            if (lsthLink.Count > 0)
            {
                img6.Visible = true;
            }
          
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// Call all hotel details information on language dropdown
    /// </summary>
    protected void drplanguage_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindHotelDetails();
    }

    #endregion
       
    #region Bind Configuration
    /// <summary>
    /// Binding Meeting room configuration for book online
    /// </summary>
    protected void lstViewBooking_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {

                ListViewDataItem currentItem = (ListViewDataItem)e.Item;
                DataKey currentDataKey = this.lstViewBooking.DataKeys[currentItem.DataItemIndex];

                //LinkButton SiteLink = (LinkButton)currentItem.FindControl("SiteLink");

                int intMeetingRoomID = Convert.ToInt32(currentDataKey.Value);

                //for image,plan,room size and height from meeting table
                Image hotelImage = (Image)e.Item.FindControl("imgHotelImage");
                hotelImage.ImageUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "/MeetingRoomImage/" + DataBinder.Eval(e.Item.DataItem, "Picture");
                HyperLink PlanView = (HyperLink)e.Item.FindControl("linkviewPlan");
                if (DataBinder.Eval(e.Item.DataItem, "MRPlan") != "")
                {
                    PlanView.NavigateUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "Plan/" + DataBinder.Eval(e.Item.DataItem, "MRPlan");
                }
                else
                {
                    PlanView.Visible = false;
                }
                Label lblRoomSize = (Label)e.Item.FindControl("lblRoomSize");
                Label lblHeight = (Label)e.Item.FindControl("lblHeight");
                lblRoomSize.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Surface"));
                lblHeight.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Height"));
                //for image,plan,room size and height from meeting table

                TList<MeetingRoomConfig> lst = ObjMeetingRoomConfigManager.GetConfigByMeetingRoomID(intMeetingRoomID);

                int intMinEnumValue = ObjMeetingRoomConfigManager.GetMinMeetingRoomShapeID();
                int intMaxEnumValue = ObjMeetingRoomConfigManager.GetMaxMeetingRoomShapeID();
                int intIndex;
                for (intIndex = intMinEnumValue; intIndex <= intMaxEnumValue; intIndex++)
                {
                    var getRow = lst.Find(a => a.RoomShapeId == intIndex);
                    if (getRow != null)
                    {

                        if (getRow.RoomShapeId == 1)
                        {
                            Label lblTheatreMin = (Label)e.Item.FindControl("lblTheatreMin");
                            Label lblTheatreMax = (Label)e.Item.FindControl("lblTheatreMax");
                            CheckBox chkTheatre = (CheckBox)e.Item.FindControl("chkTheatre");
                            lblTheatreMin.Text = getRow.MinCapacity.ToString() + " - ";
                            lblTheatreMax.Text = getRow.MaxCapicity.ToString();
                            if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                            {
                                chkTheatre.Visible = false;
                                lblTheatreMin.Text = "";
                                lblTheatreMax.Text = "";
                            }
                        }
                        else if (getRow.RoomShapeId == 2)
                        {

                            Label lblReceptionMin = (Label)e.Item.FindControl("lblReceptionMin");
                            Label lblReceptionMax = (Label)e.Item.FindControl("lblReceptionMax");
                            CheckBox chkSchool = (CheckBox)e.Item.FindControl("chkSchool");
                            lblReceptionMin.Text = getRow.MinCapacity.ToString() + " - ";
                            lblReceptionMax.Text = getRow.MaxCapicity.ToString();
                            if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                            {
                                chkSchool.Visible = false;
                                lblReceptionMin.Text = "";
                                lblReceptionMax.Text = "";
                            }


                        }
                        else if (getRow.RoomShapeId == 3)
                        {

                            Label lblUShapeMin = (Label)e.Item.FindControl("lblUShapeMin");
                            Label lblUShapeMax = (Label)e.Item.FindControl("lblUShapeMax");
                            CheckBox chkUshape = (CheckBox)e.Item.FindControl("chkUshape");
                            lblUShapeMin.Text = getRow.MinCapacity.ToString() + " - ";
                            lblUShapeMax.Text = getRow.MaxCapicity.ToString();
                            if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                            {
                                chkUshape.Visible = false;
                                lblUShapeMin.Text = "";
                                lblUShapeMax.Text = "";
                            }

                        }
                        else if (getRow.RoomShapeId == 4)
                        {

                            Label lblBanquetMin = (Label)e.Item.FindControl("lblBanquetMin");
                            Label lblBanquetMax = (Label)e.Item.FindControl("lblBanquetMax");
                            CheckBox chkBoardroom = (CheckBox)e.Item.FindControl("chkBoardroom");
                            lblBanquetMin.Text = getRow.MinCapacity.ToString() + " - ";
                            lblBanquetMax.Text = getRow.MaxCapicity.ToString();
                            if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                            {
                                chkBoardroom.Visible = false;
                                lblBanquetMin.Text = "";
                                lblBanquetMax.Text = "";
                            }
                        }
                        else if (getRow.RoomShapeId == 5)
                        {

                            Label lblCocktailMin = (Label)e.Item.FindControl("lblCocktailMin");
                            Label lblCocktailMax = (Label)e.Item.FindControl("lblCocktailMax");
                            CheckBox chkCocktail = (CheckBox)e.Item.FindControl("chkCocktail");
                            lblCocktailMin.Text = getRow.MinCapacity.ToString() + " - ";
                            lblCocktailMax.Text = getRow.MaxCapicity.ToString();
                            if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                            {
                                chkCocktail.Visible = false;
                                lblCocktailMin.Text = "";
                                lblCocktailMax.Text = "";
                            }
                        }
                    }


                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
   

    /// <summary>
    /// Binding Meeting room configuration for booking request
    /// </summary>
    protected void lstViewRequest_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {

                ListViewDataItem currentItem = (ListViewDataItem)e.Item;
                DataKey currentDataKey = this.lstViewRequest.DataKeys[currentItem.DataItemIndex];

                //LinkButton SiteLink = (LinkButton)currentItem.FindControl("SiteLink");

                int intMeetingRoomID = Convert.ToInt32(currentDataKey.Value);

                //for image,plan,room size and height from meeting table
                Image hotelImage = (Image)e.Item.FindControl("imgHotelImage");
                hotelImage.ImageUrl = ConfigurationManager.AppSettings["FilePath"] + "/MeetingRoomImage/" + DataBinder.Eval(e.Item.DataItem, "Picture");
                HyperLink PlanView = (HyperLink)e.Item.FindControl("linkviewPlan");
                if (DataBinder.Eval(e.Item.DataItem, "MRPlan") != "")
                {
                    PlanView.NavigateUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "Plan/" + DataBinder.Eval(e.Item.DataItem, "MRPlan");
                }
                else
                {
                    PlanView.Visible = false;
                }
                Label lblRoomSize = (Label)e.Item.FindControl("lblRoomSize");
                Label lblHeight = (Label)e.Item.FindControl("lblHeight");
                lblRoomSize.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Surface"));
                lblHeight.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Height"));
                //for image,plan,room size and height from meeting table

                TList<MeetingRoomConfig> lst = ObjMeetingRoomConfigManager.GetConfigByMeetingRoomID(intMeetingRoomID);

                int intMinEnumValue = ObjMeetingRoomConfigManager.GetMinMeetingRoomShapeID();
                int intMaxEnumValue = ObjMeetingRoomConfigManager.GetMaxMeetingRoomShapeID();
                int intIndex;
                for (intIndex = intMinEnumValue; intIndex <= intMaxEnumValue; intIndex++)
                {
                    var getRow = lst.Find(a => a.RoomShapeId == intIndex);
                    if (getRow != null)
                    {

                        if (getRow.RoomShapeId == 1)
                        {
                            Label lblTheatreMin = (Label)e.Item.FindControl("lblTheatreMin");
                            Label lblTheatreMax = (Label)e.Item.FindControl("lblTheatreMax");
                            CheckBox chkTheatre = (CheckBox)e.Item.FindControl("chkTheatre");
                            lblTheatreMin.Text = getRow.MinCapacity.ToString() + " - ";
                            lblTheatreMax.Text = getRow.MaxCapicity.ToString();
                            if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                            {
                                chkTheatre.Visible = false;
                                lblTheatreMin.Text = "";
                                lblTheatreMax.Text = "";
                            }
                        }
                        else if (getRow.RoomShapeId == 2)
                        {

                            Label lblReceptionMin = (Label)e.Item.FindControl("lblReceptionMin");
                            Label lblReceptionMax = (Label)e.Item.FindControl("lblReceptionMax");
                            CheckBox chkSchool = (CheckBox)e.Item.FindControl("chkSchool");
                            lblReceptionMin.Text = getRow.MinCapacity.ToString() + " - ";
                            lblReceptionMax.Text = getRow.MaxCapicity.ToString();
                            if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                            {
                                chkSchool.Visible = false;
                                lblReceptionMin.Text = "";
                                lblReceptionMax.Text = "";
                            }


                        }
                        else if (getRow.RoomShapeId == 3)
                        {

                            Label lblUShapeMin = (Label)e.Item.FindControl("lblUShapeMin");
                            Label lblUShapeMax = (Label)e.Item.FindControl("lblUShapeMax");
                            CheckBox chkUshape = (CheckBox)e.Item.FindControl("chkUshape");
                            lblUShapeMin.Text = getRow.MinCapacity.ToString() + " - ";
                            lblUShapeMax.Text = getRow.MaxCapicity.ToString();
                            if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                            {
                                chkUshape.Visible = false;
                                lblUShapeMin.Text = "";
                                lblUShapeMax.Text = "";
                            }

                        }
                        else if (getRow.RoomShapeId == 4)
                        {

                            Label lblBanquetMin = (Label)e.Item.FindControl("lblBanquetMin");
                            Label lblBanquetMax = (Label)e.Item.FindControl("lblBanquetMax");
                            CheckBox chkBoardroom = (CheckBox)e.Item.FindControl("chkBoardroom");
                            lblBanquetMin.Text = getRow.MinCapacity.ToString() + " - ";
                            lblBanquetMax.Text = getRow.MaxCapicity.ToString();
                            if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                            {
                                chkBoardroom.Visible = false;
                                lblBanquetMin.Text = "";
                                lblBanquetMax.Text = "";
                            }
                        }
                        else if (getRow.RoomShapeId == 5)
                        {

                            Label lblCocktailMin = (Label)e.Item.FindControl("lblCocktailMin");
                            Label lblCocktailMax = (Label)e.Item.FindControl("lblCocktailMax");
                            CheckBox chkCocktail = (CheckBox)e.Item.FindControl("chkCocktail");
                            lblCocktailMin.Text = getRow.MinCapacity.ToString() + " - ";
                            lblCocktailMax.Text = getRow.MaxCapicity.ToString();
                            if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                            {
                                chkCocktail.Visible = false;
                                lblCocktailMin.Text = "";
                                lblCocktailMax.Text = "";
                            }
                        }
                    }


                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion       

    #region Move to Next Page
    /// <summary>
    /// Move forward for next page
    /// </summary>
    protected void btnNext_Click(object sender, EventArgs e)
    {
        //Objwizard.ThisPageIsDone(hotelPid,        
        Objwizard.ThisPageIsDone(hotelPid, Convert.ToInt32(PropertyLevelSectionLeftMenu.lnkPreviewOnWeb));
        Objdash.DoneThisStep(hotelPid, Convert.ToInt32(PropertyLevelSection.lnkMeetingRooms));
        Response.Redirect("BedRoomDesc.aspx");
    }
    #endregion   
}