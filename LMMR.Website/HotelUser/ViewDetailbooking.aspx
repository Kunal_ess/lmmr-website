﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master" AutoEventWireup="true" CodeFile="ViewDetailbooking.aspx.cs" Inherits="HotelUser_ViewDetailbooking" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" Runat="Server">

    <h1>
        <b>
            <asp:Label ID="lblheadertext" runat="server" Text="Search result for :"></asp:Label></b></h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" Runat="Server">
    <style type="text/css">
        .modalBackground
        {
            background-color: #CCCCFF;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }
    </style>
    <link href="../css/style.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .LinkPaging
        {
            width: 20px;
            background-color: White;
            border: Solid 1px Black;
            text-align: center;
            margin-left: 8px;
        }
        .style1
        {
            width: 286px;
        }
    </style>
<table width="100%" cellpadding="4" cellspacing="4">

<tr>
<td  hieght="100px" class="style1">
  <div class="search-bookings-date-box_1"> Ref No : </div>
					  <div class="search-bookings-date-box">
                          <asp:TextBox ID="txtRefNo" runat="server" class="inputbox1_1"></asp:TextBox>                         
						</div>
						<div class="search-bookings-date-box1">  </div>
</td>
<td hieght="100px">
  <div class="search-bookings-date-box_1"> Client Name </div>
					  <div class="search-bookings-date-box">
                          <asp:TextBox ID="txtClientName" runat="server" class="inputbox1_1"></asp:TextBox>                         
						</div>
						<div class="search-bookings-date-box1">  </div>
</td></tr>

<tr>
<td hieght="100px" class="style1">
  <div class="search-bookings-date-box_1"> Arrival Date </div>
					  <div class="search-bookings-date-box">
                          <asp:TextBox ID="txtArrivaldate" runat="server" class="inputbox1_1"></asp:TextBox> 
                                <asp:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtArrivaldate" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>                        
						</div>
						<div class="search-bookings-date-box1">  </div>
</td>
<td hieght="100px">
<div class="search-bookings-date" align="center">
												<div class="search-bookings-date-box1"> From </div>
											  <div class="search-bookings-date-box">
												 <asp:TextBox ID="txtFromdate" runat="server" CssClass="inputbox1"></asp:TextBox>
                                                  <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFromdate" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
												</div>											  
											  <div class="search-bookings-date-box1"> To </div>
											  <div class="search-bookings-date-box">
												  <asp:TextBox ID="txtTodate" runat="server" CssClass="inputbox1"></asp:TextBox>
                                                  <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTodate" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
												</div>											  
    
											</div>
</td></tr>
<tr>
<td colspan="2" align="center">
<div class="search-bookings-btn" align="center">
												<div class="save-cancel-btn1"> 
                                                    <asp:LinkButton ID="lbtSearch" runat="server" CssClass="save-btn_1" 
                                                        onclick="lbtSearch_Click">Search</asp:LinkButton></div>
											</div>
</td>
</tr>
</table>


<%--////////////Mukesh--%>
  <div class="contract-list" id="divAlphabeticPaging" runat="server">
      
        <div class="contract-list-right" style="width:100%" >
            <ul id="Ul1" runat="server">
            <li id="Li27"  runat="server"><a href="#" runat="server"  id="all"  onserverclick ="PageChange"  class="select">All</a></li>
                <li id="Li1"  runat="server"><a href="#" runat="server"  id="a"  onserverclick ="PageChange">a</a></li><li id="Li2" runat="server"><a href="#" runat="server"  id="b"  onserverclick ="PageChange">b</a></li><li id="Li3" runat="server"><a href="#" runat="server"  id="c"  onserverclick ="PageChange">c</a></li><li id="Li4" runat="server">
                    <a href="#" runat="server"  id="d"  onserverclick ="PageChange" >d</a></li><li id="Li5" runat="server"><a href="#" runat="server"  id="e"  onserverclick ="PageChange" >e</a></li><li id="Li6" runat="server"><a href="#" runat="server"  id="f"  onserverclick ="PageChange">f</a></li>
                    <li id="Li7" runat="server"><a href="#" runat="server"  id="g"  onserverclick ="PageChange">g</a></li>
                    <li id="Li8" runat="server"><a href="#" runat="server"  id="h"  onserverclick ="PageChange">h</a></li>
                    <li id="Li9" runat="server"><a href="#" runat="server"  id="i"  onserverclick ="PageChange">i</a></li>
                    <li id="Li10" runat="server"><a href="#" runat="server"  id="j"  onserverclick ="PageChange"> j</a></li>
                    <li id="Li11" runat="server"><a href="#" runat="server"  id="k"  onserverclick ="PageChange">k</a></li>
                    <li id="Li12" runat="server"><a href="#" runat="server"  id="l"  onserverclick ="PageChange">l</a></li>
                    <li id="Li13" runat="server"><a href="#" runat="server"  id="m"  onserverclick ="PageChange">m</a></li>
                    <li id="Li14" runat="server"><a href="#" runat="server"  id="n"  onserverclick ="PageChange">n</a></li>
                    <li id="Li15" runat="server"><a href="#" runat="server"  id="o"  onserverclick ="PageChange">o</a></li>
                    <li id="Li16" runat="server"><a href="#" runat="server"  id="p"  onserverclick ="PageChange">p</a></li>
                    <li id="Li17" runat="server"><a href="#" runat="server"  id="q"  onserverclick ="PageChange">q</a></li>                
                    <li id="Li18" runat="server"><a href="#" runat="server"  id="r"  onserverclick ="PageChange">r</a></li>
                    <li id="Li19" runat="server"><a href="#" runat="server"  id="s"  onserverclick ="PageChange">s</a></li>
                    <li id="Li20" runat="server"><a href="#" runat="server"  id="t"  onserverclick ="PageChange">t</a></li>
                    <li id="Li21" runat="server"><a href="#" runat="server"  id="u"  onserverclick ="PageChange">u</a></li>
                    <li id="Li22" runat="server"><a href="#" runat="server"  id="v"  onserverclick ="PageChange">v</a></li>
                    <li id="Li23" runat="server"><a href="#" runat="server"  id="w"  onserverclick ="PageChange">w</a></li>
                    <li id="Li24" runat="server"><a href="#" runat="server"  id="x"  onserverclick ="PageChange">x</a></li>
                    <li id="Li25" runat="server"><a href="#" runat="server"  id="y"  onserverclick ="PageChange">y</a></li>
                    <li id="Li26" runat="server"><a href="#" runat="server"  id="z"  onserverclick ="PageChange">z</a></li>
                                            
            </ul>
        </div>
    </div>
<%--/////////Mukesh--%>
      <asp:UpdatePanel runat="server" ID="updTest">
            <ContentTemplate>
            <div id="divGrid">
                <asp:GridView ID="grdViewBooking" runat="server" Width="100%" AutoGenerateColumns="False"
                    RowStyle-HorizontalAlign="Center" RowStyle-VerticalAlign="Middle" CellPadding="4"
                    ForeColor="#333333" OnRowDataBound="grdViewBooking_RowDataBound" EmptyDataText="No records"
                     OnPageIndexChanging="grdViewBooking_PageIndexChanging" AllowPaging="true" GridLines="None"
                    PageSize="2" >
                    <Columns>
                        <asp:TemplateField HeaderText="Ref No">
                            <ItemTemplate>
                                <asp:LinkButton ID="lblRefNo" runat="server" ToolTip='<%# Eval("HotelID") %>' Text='<%# Eval("Id") %>'
                                    ForeColor="#339966" OnClick="lblRefNo_Click"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Meeting Room">
                            <ItemTemplate>
                                <asp:Label ID="lblmeetingroom" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Booking Date">
                            <ItemTemplate>
                                <asp:Label ID="lblBookingDt" runat="server" tooltip='<%# Eval("BookingDate")%>' Text='<%# Eval("BookingDate","{0:dd-MMM}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Channel">
                            <ItemTemplate>
                             <asp:Label ID="lblCompany" runat="server" Text='<%# Eval("Usertype") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Contact Name">
                            <ItemTemplate>
                              <asp:HyperLink ID="lblConctName"  ForeColor="#339966" runat="server" NavigateUrl='<%#"mailto:" +DataBinder.Eval(Container.DataItem,"emailid") %>'
                              Text='<%# Eval("Contact ") %>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Arrival Date">
                            <ItemTemplate>
                                <asp:Label ID="lblArrivalDt" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Departure Date">
                            <ItemTemplate>
                                <asp:Label ID="lblDepartureDt" runat="server" ToolTip='<%# Eval("DepartureDate")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total">
                            <ItemTemplate>
                                <asp:Label ID="lblFinalTotal" runat="server" Text='<%# Eval("Finaltotalprice") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnchkcommision" runat="server" Text="Change" ForeColor="#339966"
                                    CommandArgument='<%# Eval("Id") %>' OnClick="btnchkcommision_Click"></asp:LinkButton>
                                <asp:Image ID="imgCheck" runat="server" ImageUrl="~/Images/select-check-bx.png" Visible="false" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" BackColor="White" />
                    <PagerTemplate>
                        <table cellpadding="0" border="0"  align=right>
                            <tr>
                                <td style="vertical-align=middle;  height:22px;">
                                   
                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                </td>
                            </tr>
                        </table>
                    </PagerTemplate>
                    <EditRowStyle BackColor="#7C6F57" />
                    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle CssClass="head" BackColor="#d4d9cc" />
                    <PagerSettings Mode="NumericFirstLast" Position="Top" />
                    <PagerStyle BackColor="White" ForeColor="White" HorizontalAlign="Right" Font-Overline="False"
                        Font-Strikeout="False" />
                    <RowStyle CssClass="value1" BackColor="#ecf2e3" />
                    <AlternatingRowStyle CssClass="value2" BackColor="#f6f9f1" />
                    <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F8FAFA" />
                    <SortedAscendingHeaderStyle BackColor="#246B61" />
                    <SortedDescendingCellStyle BackColor="#D4DFE1" />
                    <SortedDescendingHeaderStyle BackColor="#15524A" />
                </asp:GridView>
                </div>
                
                <%--	<ul>
						<li class="head">
							<div class="col1">Ref</div>
							<div class="col2">Meeting Room</div>
							<div class="col3">Booking date</div>
							<div class="col4">Company</div>
							<div class="col5">Contact Name</div>
							<div class="col6">Arrival T.</div>
							<div class="col7">Departure T.</div>
							<div class="col8">Total</div>
						</li>
						<li class="value1">
							<div class="col1"><a href="#">5090</a></div>
							<div class="col2">New York confere...</div>
							<div class="col3">03/08 &bull; 11:50</div>
							<div class="col4">Sawer Co</div>
							<div class="col5"><a href="#">Mary Lou Retton</a></div>
							<div class="col6">06/08 &bull; 14:00</div>
							<div class="col7">06/08 &bull; 18:00</div>
							<div class="col8">&euro; 8000</div>
						</li>
						<li class="value2">
							<div class="col1"><a href="#">8907</a></div>
							<div class="col2">Lotus budha </div>
							<div class="col3">03/08 &bull; 10:50</div>
							<div class="col4">Awer Co</div>
							<div class="col5"><a href="#">Tom Sawer</a></div>
							<div class="col6">06/08 &bull; 14:00</div>
							<div class="col7">06/08 &bull; 18:00</div>
							<div class="col8">&euro; 6500</div>
						</li>
						<li class="value1">
							<div class="col1"><a href="#">5092</a></div>
							<div class="col2">New York confere...</div>
							<div class="col3">03/08 &bull; 11:50</div>
							<div class="col4">Xeros</div>
							<div class="col5"><a href="#">John Travolta</a></div>
							<div class="col6">06/08 &bull; 14:00</div>
							<div class="col7">06/08 &bull; 18:00</div>
							<div class="col8">&euro; 4750</div>
						</li>
						<li class="value2">
							<div class="col1"><a href="#">8907</a></div>
							<div class="col2">Lotus budha </div>
							<div class="col3">03/08 &bull; 10:50</div>
							<div class="col4">Awer Co</div>
							<div class="col5"><a href="#">Kenny B Double</a></div>
							<div class="col6">06/08 &bull; 14:00</div>
							<div class="col7">06/08 &bull; 18:00</div>
							<div class="col8">&euro; 6500</div>
						</li>
						<li class="value3"><div class="col9"><a href="#">Print</a> &bull; <a href="#">Save as pdf</a></div></li>
						</ul>--%>
              </ContentTemplate>
        </asp:UpdatePanel>
                
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
                <div id="divbookingdetails" align="left" runat="server" >
               <ul>
               <li>
                 <h1 >
                        Booking details</h1>
               </li>
               </ul>
                  
                    <br />
                      
        
                    <div class="booking-details" id="Divdetails" runat="server">
                        <ul>
                            <%--<li class="value6"></li>--%>
                            <li class="value4">
                                <div class="col10">
                                    <strong>Property name:</strong></div>
                                <div class="col11">
                                    <asp:Label ID="lblProteryname" runat="server" Text=""></asp:Label></div>
                                <div class="col12">
                                    <strong> Booking done on :</strong><asp:Label ID="lblbookingdoneOn" runat="server" Text=""></asp:Label></div>
                            </li>
                            <li class="value5">
                                <div class="col10">
                                    <strong>Meeting room name:</strong></div>
                                <div class="col11">
                                    <asp:Label ID="lblMeetingroom" runat="server" Text=""></asp:Label></div>
                                <div class="col12">
                                    <strong>Contact person:</strong>
                                    <asp:Label ID="lblContactPerson" runat="server" Text=""></asp:Label></div>
                            </li>
                            <li class="value4">
                                <div class="col10">
                                    <strong>Booked for day(s):</strong></div>
                                <div class="col11">
                                    <asp:Label ID="lblBookedDays" runat="server" Text=""></asp:Label>                                    
                                    <asp:Label ID="lblBookedDays1" runat="server" Text=""></asp:Label></div>
                                <div class="col12">
                                    <strong>Contact email:</strong><a id="lblConatctpersonEmail" runat="server" href="#" ></a>
                             </div>
                            </li>
                            <li class="value5">
                                <div class="col10">
                                    <strong>Participants:</strong></div>
                                <div class="col11">
                                    <asp:Label ID="lblParticipants" runat="server" Text=""></asp:Label></div>
                                <div class="col12">
                                    <%--<strong>Contact phone: 
                                <asp:Label ID="lblContactPersonPhno" runat="server" Text=""></asp:Label></strong></a>--%></div>
                            </li>
                            <li class="value4">
                                <div class="col10">
                                    <strong>Budget:</strong></div>
                                <div class="col11">
                                    <asp:Label ID="lblBudget" runat="server" Text="Not Selected"></asp:Label></div>
                                <div class="col13">
                                </div>
                            </li>
                            <li class="value5">
                                <div class="col10">
                                    <strong>Choosen shape</strong></div>
                                <div class="col11">
                                    <asp:Label ID="lblShape" runat="server" Text=""></asp:Label>
                                    <asp:Label ID="lblMinShape" runat="server" Text=""></asp:Label>-<asp:Label ID="lblMaxshape"
                                        runat="server" Text=""></asp:Label></div>
                                <div class="col13">
                                    <strong>
                                        <asp:Label ID="lblprice" runat="server" Text=""></asp:Label>
                                        x
                                        <asp:Label ID="lbldura" runat="server" Text=""></asp:Label>
                                        &nbsp;=<asp:Label ID="lbltotal" runat="server" Text=""></asp:Label></strong>
                                </div>
                            </li>
                            
                            <li >
                                                                                           

 
                            <asp:GridView ID="grddetails" AutoGenerateColumns="false" BorderWidth="0" CellPadding="0" CellSpacing="0"  GridLines="None" ShowHeader="false" runat="server">
                                    <Columns>
                                    <asp:TemplateField>
                                
                                    <ItemTemplate>
                                     
                                                              
                                        <asp:Label CssClass="col10" ID="lblitenempty" runat="server" ></asp:Label>
                                     
                                    </ItemTemplate>
                                    </asp:TemplateField>   
                                        <asp:TemplateField>
                                
                                    <ItemTemplate>
                                     
                                                              
                                        <asp:Label CssClass="col11" ID="lbliten" runat="server" Text='<%# Eval("itemname") %>'></asp:Label>
                                     
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField>
                                    <ItemTemplate>
                                    
                                    <strong>
                                        <asp:Label CssClass="col13" ID="lblprice" runat="server" Text='<%# Eval("totalprice") %>'></asp:Label>
                                        </strong>
                                
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    </Columns>
                                    <RowStyle BackColor="#ecf2e3" />
                                    <AlternatingRowStyle BackColor="#f6f9f1" />
                                    </asp:GridView>
                                
                               
                                
                            </li>
                            <li class="value5">
                                <div class="col10">
                                </div>
                                <div class="col11">
                                    <strong>TOTAL</strong></div>
                                <div class="col13">
                                    <strong>
                                        <asp:Label ID="lblfinalprice" Font-Bold="true" runat="server" Text=""></asp:Label></strong></div>
                            </li>
                            <li class="value5">
                                <div class="col15">
                                    <div class="col16">
                                        <b>Other Comments</b>
                                    </div>
                                    <p>
                                        <asp:Label ID="lblsplcomments" runat="server" Text=""></asp:Label></p>
                                </div>
                            </li>
                        </ul>
                        
                    </div>
                </div>
                <br />
            </ContentTemplate>
           <%-- <Triggers>
            <asp:PostBackTrigger ControlID="lnkSavePDF"  />
            </Triggers>--%>
        </asp:UpdatePanel>
        
    <asp:LinkButton ID="lnkbtn" runat="server"></asp:LinkButton>
    <asp:ModalPopupExtender ID="modalcheckcomm" TargetControlID="lnkbtn" BackgroundCssClass="modalBackground"
        PopupControlID="pnlchkCommission" runat="server">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlchkCommission" BorderColor="Black" Width="800px" BorderWidth="1"
        runat="server" BackColor="White">
        <div class="popup-mid-inner-body">
            Here you can confirm Booking revenue or adjust it.
        </div>
        <div class="popup-mid-inner-body1">
            <table width="100%" border="1" cellspacing="0" cellpadding="3" align="center">
                <tr>
                    <td width="40%">
                        <table>
                            <tr>
                                <td align="right">
                                    Revenue :
                                </td>
                                <td>
                                    <asp:Label ID="lblrevenue" runat="server" Text="6.820"></asp:Label>
                                    &nbsp;Euro
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="upcheckcomm" runat="server">
                                        <ContentTemplate>
                                            <asp:CheckBox ID="chkconfirmrevenue"  Checked="true" Text="Confirm the revenue"
                                               runat="server" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table width="100%">
                            <tr>
                                <td align="center" colspan="2">
                                    <b>Adjust Revenue (if needed)</b>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    Insert Netto (VAT excluded) :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtrealvalue" runat="server"></asp:TextBox><asp:FilteredTextBoxExtender
                                        ID="FilteredTextBoxExtenderrealvalue" runat="server" TargetControlID="txtrealvalue"
                                        ValidChars="." FilterMode="ValidChars" FilterType="Numbers,Custom">
                                    </asp:FilteredTextBoxExtender>
                                    <%-- <asp:RequiredFieldValidator
                            ID="reqtxtvalue" runat="server" ControlToValidate="txtrealvalue" ValidationGroup="popup"
                            ErrorMessage="*"></asp:RequiredFieldValidator>--%>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    Reason :
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlreason" runat="server">
                                        <asp:ListItem>Reduced number of participants</asp:ListItem>
                                        <asp:ListItem>Increased number of participants</asp:ListItem>
                                        <asp:ListItem>Variation on package price</asp:ListItem>
                                        <asp:ListItem>Cancelled and agreed between hotel and client</asp:ListItem>                                                                         
                                        <asp:ListItem>No Show</asp:ListItem>
                                        <asp:ListItem>Changes in Duration</asp:ListItem>
  
                                    </asp:DropDownList>
                                    <%--<asp:TextBox ID="txtreason" runat="server"></asp:TextBox>--%>
                                    <%-- <asp:RequiredFieldValidator
                            ID="reqreason" runat="server" ControlToValidate="ddlreason" ValidationGroup="popup" InitialValue="0" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <div class="popup-mid-inner-body2_i">
            </div>
            <div class="popup-mid-inner-body3" align="center">
            <div class="green-bt" id="div1" runat="server" align="center">
                <asp:LinkButton ID="btnCancel" runat="server" CssClass="link" ForeColor="White"
                    Font-Bold="true" Text="Cancel" />&nbsp;&nbsp;&nbsp;
                    </div>
                    <div class="green-bt" id="div2" runat="server" align="center">
                <asp:LinkButton ID="btnSubmit" runat="server" Text="Submit" ForeColor="White" CssClass="link" Font-Bold="true"
                    OnClick="btnSubmit_Click" />
                    </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>



