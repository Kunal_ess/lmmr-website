﻿<%@ Page Title="Hotel User - Picture Management" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master"
    AutoEventWireup="true" CodeFile="PictureVideoManagement.aspx.cs" ValidateRequest="false"
    Inherits="HotelUser_PictureVideoManagement" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" runat="Server">
    <div id="divHide">
        <div id="modifyPictureVideoPopup">
            <div class="picture-video-form-body">
                <div id="divOperatorVideoUpload" runat="server">
                    <div class="picture-video-form-body-top" id="divUploadVideo">
                        <div class="picture-video-form-left">
                            Upload video
                        </div>
                        <div class="picture-video-form-right">
                            <%--
                            <asp:AsyncFileUpload OnClientUploadError="uploadErrorVideo" OnClientUploadComplete="uploadCompleteVideo"
                                runat="server" ID="AsyncVideoFileUpload" Width="400px" CompleteBackColor="White"
                                ThrobberID="imgLoader2" OnUploadedComplete="FileUploadCompleteVideo" />
                            <asp:Image ID="imgLoader2" runat="server" ImageUrl="~/images/loader.gif" />
                            <span style="font-family: Arial; font-size: Smaller;">Video format: SWF,FLV & max size
                                1MB. </span>--%>
                            <asp:Label ID="lblUploadVideoMsg" runat="server" Text="" ForeColor="Red"></asp:Label>
                            <asp:TextBox ID="txtVideoUrl" runat="server" Text="" Width="250px" TextMode="MultiLine"></asp:TextBox>
                            <asp:HiddenField ID="hdnVideoUrl" Value="" runat="server" />
                        </div>
                        <div class="picture-video-form-right1">
                        </div>
                        <div class="images-main-body-video-new" id="divVideoImage">
                        </div>
                    </div>
                </div>
                <div id="divMessageForOperatorVideo" runat="server">
                    <center>
                        <span style="font-family: Arial; font-size: medium;">For video upload, contact the operator
                            at hotelsupport@lastminutemeetingroom.com or +32 2 344 25 50 </span>
                    </center>
                </div>
                <br />
                <div class="picture-video-form-body-top">
                    <div class="picture-video-form-left">
                        Upload picture
                    </div>
                    <div class="picture-video-form-right-new">
                        <div style="float: left; width: 240px;">
                            <asp:AsyncFileUpload OnClientUploadError="uploadError" runat="server" OnClientUploadComplete="uploadComplete"
                                Width="220px" ID="asyncflPicture" CompleteBackColor="White" ThrobberID="imgLoader"
                                OnUploadedComplete="FileUploadComplete" />
                        </div>
                        <div style="float: left; width: 50px; margin-left: 0px">
                            <asp:Image ID="imgLoader" runat="server" ImageUrl="~/images/loader.gif" />
                        </div>
                        <div style="float: left; width: 300px;">
                            <span style="font-family: Arial; font-size: Smaller;">Image format: JPG,JPEG,PNG,GIF
                                & max size <%=System.Configuration.ConfigurationManager.AppSettings["MessageKB2"]%>. </span>
                        </div>
                    </div>
                    <div class="picture-video-form-right1">
                    </div>
                </div>
                <asp:Label ID="lblUploadMsg" runat="server" Text="" ForeColor="Red"></asp:Label>
                <div class="picture-video-form-body-bottom">
                    <div class="picture-video-form-body-bottom-left">
                        <div id="content" class="images-main-body1">
                        </div>
                    </div>
                    <div class="picture-video-form-body-bottom-right" id="divdhowPicOnMouse">
                        <div style="text-align: right" id="divDeleteButton">
                            <asp:LinkButton runat="server" ID="lnkDelete" OnClientClick="return deleteImage();">Delete</asp:LinkButton>
                        </div>
                        <div class="picture-video-form-body-bottom-right-image">
                            <asp:Image ID="imgSelectedPicture" runat="server" Height="130px" Width="195px" />
                            <asp:HiddenField ID="hdnImageName" runat="server" />
                        </div>
                        <div>
                            <div class="picture-name" style="text-align: left">
                                Picture Title</div>
                            <asp:TextBox ID="txtImageTitle" runat="server" class="inputbox" MaxLength="30"></asp:TextBox>
                            <asp:Image ID="imgImageTitleHelp" ImageUrl="~/Images/help.jpg" runat="server" ToolTip="This is the title of the picture." />
                        </div>
                        <div>
                            <div class="picture-name" style="text-align: left">
                                Description</div>
                            <asp:TextBox ID="txtAlterText" runat="server" class="inputbox" MaxLength="30"></asp:TextBox>
                            <asp:Image ID="imgAlterHelp" ImageUrl="~/Images/help.jpg" runat="server" ToolTip="This description will be shown on the mouseover of the picture." />
                        </div>
                        <div style="width: 195px; overflow: hidden;" id="div1">
                            <div class="picture-name" style="text-align: left; width: 195px;">
                                Main</div>
                            <asp:CheckBox CssClass="NoClassApply" ID="chkSelectMain" runat="server"></asp:CheckBox>
                            &nbsp;&nbsp;
                            <asp:Image ID="imgIsMainHelp" ImageUrl="~/Images/help.jpg" ToolTip="This picture will be the main (first) picture to be shown on a list."
                                runat="server" align="absmiddle" />
                        </div>
                        <div style="display: none">
                            <table width="100%">
                                <tr>
                                    <td width="20%">
                                        <asp:TextBox ID="txtOrderNumber" runat="server" class="inputboxsmall"></asp:TextBox>
                                    </td>
                                    <td width="70%" valign="middle">
                                        /
                                        <asp:Label ID="lblOutOf" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br />
                        <div style="padding-left: 50px;">
                            <asp:LinkButton runat="server" ID="lnkSaveImageData" OnClientClick="return setImageData();">Save</asp:LinkButton>
                            or
                            <asp:LinkButton runat="server" ID="lnkCancelImageData" OnClientClick="return cancelEdit();">Cancel</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="button_section">
                    <asp:LinkButton ID="lnkSaveImages" runat="server" CssClass="select" OnClientClick="return IsMainReq();"
                        OnClick="lnkSaveImages_Click">Save</asp:LinkButton>
                    <span>or</span>
                    <asp:LinkButton ID="lnkCancelImages" runat="server" OnClick="lnkCancelImages_Click"
                        OnClientClick="CloseOrCancel();">Cancel</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="sepmain-pic">
        </div>
        <div class="heading-body">
            <div class="heading-left">
                <h1>
                    Pictures/Video : <b>
                        <asp:Label ID="lblHotel" runat="server" Text=""></asp:Label></b><img src="Uploads/HotelVideo/video-1.png"
                            width="20px" height="22px" style="margin-left: 20px" />
                </h1>
                <span style="color: Red; float: left;">Please do not upload file with special characters
                    in name.</span>
            </div>
            <div class="heading-right">
                <asp:LinkButton ID="lnkModifyHotelImage" runat="server" class="cancel-btn">modify</asp:LinkButton></div>
        </div>
        <div class="images-main-body">
            <asp:DataList ID="dlShowHotelImage" runat="server" RepeatDirection="Horizontal" RepeatColumns="5"
                OnItemDataBound="dlShowHotelImage_ItemBound">
                <ItemTemplate>
                    <div class="images-body-out">
                        <div class="images-main1">
                            <div class="images-main-inner" id="divIMain" runat="server">
                                Main
                                <asp:Image ID="imgIsMain" ImageUrl="~/Images/tick.png" runat="server" />
                            </div>
                        </div>
                        <div class="images-body">
                            <asp:Image ID="imgHotelImage" runat="server" />
                        </div>
                        <div class="images-name">
                            <asp:Label ID="lblFileName" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblEmpty" Text="no result" runat="server" Visible='<%#bool.Parse((dlShowHotelImage.Items.Count==0).ToString())%>'>
                    </asp:Label>
                </FooterTemplate>
            </asp:DataList>
        </div>
    </div>
    <asp:ListView ID="lstViewMeetingRoomPicture" runat="server" ItemPlaceholderID="itemPlacehoderID"
        DataKeyNames="Id" OnItemDataBound="lstViewMeetingRoomPicture_ItemDataBound">
        <LayoutTemplate>
            <div class="picture-video">
                <asp:PlaceHolder ID="itemPlacehoderID" runat="server"></asp:PlaceHolder>
            </div>
        </LayoutTemplate>
        <ItemTemplate>
            <div class="sepmain-pic">
            </div>
            <div class="heading-body">
                <div class="heading-left">
                    <h2>
                        Pictures meetingroom :
                        <asp:Label ID="lblMeetingRoomName" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                    </h2>
                </div>
                <div class="heading-right">
                    <asp:LinkButton ID="lnkModifyMeetingRoomPicture" class="cancel-btn" runat="server">modify</asp:LinkButton>
                </div>
                <div class="images-main-body">
                    <asp:DataList ID="dlShowImage" runat="server" RepeatDirection="Horizontal" RepeatColumns="5"
                        OnItemDataBound="dlShowImage_ItemBound">
                        <ItemTemplate>
                            <div class="images-body-out">
                                <div class="images-main1">
                                    <div class="images-main-inner" id="divIMain" runat="server">
                                        Main
                                        <asp:Image ID="imgIsMain" ImageUrl="~/Images/tick.png" runat="server" />
                                    </div>
                                </div>
                                <div class="images-body">
                                    <asp:Image ID="imgMeetingRoomImage" runat="server" />
                                </div>
                                <div class="images-name">
                                    <asp:Label ID="lblFileName" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                </div>
            </div>
        </ItemTemplate>
    </asp:ListView>
    <asp:ListView ID="lstViewBedRoomPicture" runat="server" ItemPlaceholderID="itemPlacehoderID"
        DataKeyNames="Id" OnItemDataBound="lstViewBedRoomPicture_ItemDataBound">
        <LayoutTemplate>
            <div class="picture-video">
                <asp:PlaceHolder ID="itemPlacehoderID" runat="server"></asp:PlaceHolder>
            </div>
        </LayoutTemplate>
        <ItemTemplate>
            <div class="sepmain-pic">
            </div>
            <div class="heading-body">
                <div class="heading-left">
                    <h2>
                        Pictures bedroom :
                        <asp:Label ID="lblBedRoomName" runat="server" Text=""></asp:Label>
                    </h2>
                </div>
                <div class="heading-right">
                    <asp:LinkButton ID="lnkModifyBedRoomPicture" class="cancel-btn" CommandArgument='<%# Eval("Id") %>'
                        CommandName="Modify" runat="server">modify</asp:LinkButton>
                </div>
                <div class="images-main-body">
                    <asp:DataList ID="dlShowImageBedRoom" runat="server" RepeatDirection="Horizontal"
                        RepeatColumns="5" OnItemDataBound="dlShowImageBedRoom_ItemBound">
                        <ItemTemplate>
                            <div class="images-body-out">
                                <div class="images-main1">
                                    <div class="images-main-inner" id="divIMain" runat="server">
                                        Main
                                        <asp:Image ID="imgIsMain" ImageUrl="~/Images/tick.png" runat="server" />
                                    </div>
                                </div>
                                <div class="images-body">
                                    <asp:Image ID="imgBedRoomImage" runat="server" />
                                </div>
                                <div class="images-name">
                                    <asp:Label ID="lblFileName" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                </div>
            </div>
        </ItemTemplate>
    </asp:ListView>
    <div class="button_sectionNext" id="divNext" runat="server">
        <asp:LinkButton ID="btnNext" runat="server" Text="Next &gt;&gt;" class="RemoveCookie"
            OnClick="btnNext_Click" />
    </div>
    <div id="Loding_overlaySec">
        <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
        <span>Loading...</span></div>
    <script language="javascript" type="text/javascript">
        function IsMainReq() {
            if (jQuery(".images-main-body1 img").length > 1) {
                var len = jQuery(".images-main-body1 img:[isMain=true]").length;
                if (len == 0) {
                    var parents = jQuery("#content").children();

                    alert("Please select one main image.");

                    return false;
                }
                //                else {
                //                    var urlval = jQuery("#<%=txtVideoUrl.ClientID %>").val();
                //                   if (urlval != "") {
                //                        jQuery("#<%=hdnVideoUrl.ClientID %>").val(urlval)
                //                    
                //                        var urlregex = new RegExp("^(http:\/\/www.|https:\/\/www.|ftp:\/\/www.|www.){1}([0-9A-Za-z]+\.)");
                //                        if (urlregex.test(urlval)) {
                //                            return (true);
                //                        }
                //                        alert("Please enter valid video url.");
                //                        return (false);
                //                    }
                //               }
            }

        }

    </script>
    <script type="text/javascript">

        var strtype = null;
        var intcount = 0;
        function cancelEdit() {
            jQuery('#divdhowPicOnMouse').hide();
            return false;
        }

        function deleteImage() {
            var hdnImageName = jQuery('#contentbody_hdnImageName').val();
            if (hdnImageName != 0) {
                var contentID = document.getElementById('content');
                contentID.removeChild(document.getElementById('strText' + hdnImageName));
                intcount = intcount - 1;
                if (intcount > 1) {
                    jQuery("#divDeleteButton").show();
                }
                else {
                    jQuery("#divDeleteButton").hide();
                }

                jQuery.ajax({
                    type: "POST",
                    url: "PictureVideoManagement.aspx/DeleteImageData",
                    data: "{'imageName':'" + hdnImageName + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        jQuery("#divdhowPicOnMouse").hide();

                    }

                });


            }
            return false;
        }

        function uploadComplete(sender, args) {
            // var imageName = args.get_fileName();

            jQuery.ajax({
                type: "POST",
                url: "PictureVideoManagement.aspx/GetUniqueImageName",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {

                    var result = jQuery.parseJSON(msg.d);
                    if (result != null) {
                        if (result[0] != null) {
                            addElement(result[0]);
                            jQuery('#<%=lblUploadMsg.ClientID %>').hide();
                        } else {
                            jQuery('#<%=lblUploadMsg.ClientID %>').show();
                            jQuery('#<%=lblUploadMsg.ClientID %>').html(result[1]);
                        }
                    }
                }

            });

            return false;
        }


        function uploadCompleteVideo(sender, args) {

            jQuery.ajax({
                type: "POST",
                url: "PictureVideoManagement.aspx/GetUniqueVideoName",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var result = jQuery.parseJSON(msg.d);
                    if (result != null) {
                        if (result[0] != null) {
                            jQuery("#divVideoImage").html('');
                            jQuery("#divVideoImage").show();
                            jQuery("#divVideoImage").append("<div class='images-body-out-video-new'><div class='images-body1'><img src='<%=ResolveUrl(UploadFolderPath) %>" + "HotelVideo/video.png' Width='62px' Height='70px'/></div></br><div class='images-name1'>" + result[0] + "</div></div>");
                            jQuery('#<%=lblUploadVideoMsg.ClientID %>').hide();
                        } else {
                            jQuery('#<%=lblUploadVideoMsg.ClientID %>').html(result[1]);
                            jQuery('#<%=lblUploadVideoMsg.ClientID %>').show();
                        }
                    }
                }

            });

        }

        function uploadErrorVideo(sender) {

        }

        function uploadError(sender) {

        }


        // add new uploaded image in div
        function addElement(imageName, fileName, isMain) {
            if (fileName == null) {
                fileName = "No Title";
            }
            var makeDivId = imageName.split('.');
            intcount = intcount + 1;
            var contentID = document.getElementById('content');
            var newTBDiv = document.createElement('div');
            //newTBDiv.setAttribute('id', 'strText' + intcount);
            newTBDiv.setAttribute('id', 'strText' + imageName);
            newTBDiv.setAttribute('className', 'images-body-out1');
            newTBDiv.setAttribute('class', 'images-body-out1');
            if (isMain == true) {
                newTBDiv.innerHTML = "<div class='images-main-new'><div class='images-main-inner'>Main<img id='imgIsMainPopup' src='<%=ResolveUrl(IsMainImagePath) %>'/></div></div><div class='images-body1' id='divOnclickSelect-" + makeDivId[0] + "'  style='cursor: pointer'><img id = 'imgDisplay" + intcount + "' src='<%=ResolveUrl(UploadFolderPath) %>" + strtype + "/" + imageName + "' Width='70px' Height='70px' isMain='" + isMain + "' onclick='aditUpdateImage(\"" + imageName + "\");' /></div><div class='images-name1'>" + fileName + "</div>";
            }
            else {
                newTBDiv.innerHTML = "<div class='images-main-new'><div class='images-main-inner'></div></div><div class='images-body1' id='divOnclickSelect-" + makeDivId[0] + "'  style='cursor: pointer;'><img id = 'imgDisplay" + intcount + "' src='<%=ResolveUrl(UploadFolderPath) %>" + strtype + "/" + imageName + "' Width='70px' Height='70px' isMain='" + isMain + "' onclick='aditUpdateImage(\"" + imageName + "\");' /></div><div class='images-name1'>" + fileName + "</div>";

            }
            contentID.appendChild(newTBDiv);

            if (intcount > 1) {
                jQuery("#divDeleteButton").show();
            }
            else {
                jQuery("#divDeleteButton").hide();
            }

        }
        var dialogWrapper = null;
        function CloseOrCancel() {
            (typeof (dialogWrapper) != 'undefined' || dialogWrapper != null) ? dialogWrapper.close() : '';
            intcount = 0;
        };


        // This Function Use for get old image. 
        function getOldImage(strType, intId) {
            strtype = strType;
            if (strType == "HotelImage") {
                jQuery("#divUploadVideo").show();
                //jQuery('#<%=divMessageForOperatorVideo.ClientID %>').show();
            } else {
                jQuery("#divUploadVideo").hide();
                //jQuery('#<%=divMessageForOperatorVideo.ClientID %>').hide();

            }
            jQuery('#<%=lblUploadMsg.ClientID %>').html('');
            jQuery('#<%=lblUploadVideoMsg.ClientID %>').html('');
            jQuery("#divVideoImage").html('');
            jQuery("#divVideoImage").hide();

            jQuery.ajax({
                type: "POST",
                url: "PictureVideoManagement.aspx/GetOldData",
                data: "{'strType':'" + strType + "','intId':'" + intId + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var result = jQuery.parseJSON(msg.d);
                    if (result != null) {

                        intcount = 0;

                        var contentID = document.getElementById('content');
                        while (contentID.hasChildNodes()) {
                            contentID.removeChild(contentID.lastChild);
                        }

                        jQuery("#divdhowPicOnMouse").hide();

                        jQuery.each(result, function (i, data) {

                            if (data.FileType != 0) {
                                jQuery("#divVideoImage").html('');
                                jQuery("#divVideoImage").show();
                                jQuery("#divVideoImage").append("<div class='images-body-out-video-new'><div class='images-body1'><img src='<%=ResolveUrl(UploadFolderPath) %>" + "HotelVideo/video.png' Width='62px' Height='70px'/></div></br><div class='images-name1'>" + data.FileName + "</div></div>");
                                jQuery("#<%=txtVideoUrl.ClientID %>").val(data.VideoName);
                            }
                            else {
                                addElement(data.ImageName, data.FileName, data.IsMain);
                            }
                        });
                    }
                }
            });

            dialogWrapper = ShowHtmlAsPopup('Modify pictures / video', 'modifyPictureVideoPopup', true, 'auto', 'auto', false, false);
            return false;
        }

        // this function use for update image data.
        function aditUpdateImage(imageName) {
            var parents = jQuery("#content").children();
            jQuery("div", parents).removeClass("images-body1-onselect");
            jQuery.ajax({
                type: "POST",
                url: "PictureVideoManagement.aspx/GetData",
                data: "{'imageName':'" + imageName + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var result = jQuery.parseJSON(msg.d);
                    if (result != null) {
                        var findDivId = imageName.split('.');
                        jQuery('#divOnclickSelect-' + findDivId[0]).addClass('images-body1-onselect');
                        jQuery("#divdhowPicOnMouse").show();
                        jQuery('#<%= imgSelectedPicture.ClientID %>').prop("src", "<%=ResolveUrl(UploadFolderPath) %>" + strtype + "/" + result.ImageName);
                        jQuery('#<%= txtImageTitle.ClientID %>').val(result.FileName);
                        jQuery('#<%= hdnImageName.ClientID %>').val(result.ImageName);
                        jQuery('#<%= txtAlterText.ClientID %>').val(result.AlterText);
                        jQuery('#<%= txtOrderNumber.ClientID %>').val(result.OrderNumber);
                        jQuery('#<%= lblOutOf.ClientID %>').html(intcount);
                        jQuery('#<%= chkSelectMain.ClientID %>').prop("checked", result.IsMain);

                    }
                }
            });
        }

        function setImageData() {

            var txtFileName = jQuery("#contentbody_txtImageTitle").val();
            var hdnImageName = jQuery("#contentbody_hdnImageName").val();
            var txtAlterText = jQuery("#contentbody_txtAlterText").val();
            var txtOrderNumber = jQuery("#contentbody_txtOrderNumber").val();
            var isMain = jQuery('#contentbody_chkSelectMain').is(':checked');

            jQuery.ajax({
                type: "POST",
                url: "PictureVideoManagement.aspx/SetImageData",
                data: "{'fileName':'" + txtFileName + "','alterText':'" + txtAlterText + "','isMain':'" + isMain + "','orderNumber':'" + txtOrderNumber + "','imageName':'" + hdnImageName + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {

                    var result = jQuery.parseJSON(msg.d);
                    if (result != null) {
                        intcount = 0;
                        var contentID = document.getElementById('content');
                        while (contentID.hasChildNodes()) {
                            contentID.removeChild(contentID.lastChild);
                        }

                        jQuery.each(result, function (i, data) {

                            if (data.ImageName != null) {
                                addElement(data.ImageName, data.FileName, data.IsMain);
                            }

                        });
                        jQuery("#divdhowPicOnMouse").hide();
                    }
                }

            });
            return false;
        }

        jQuery(document).ready(function () {
            jQuery("#divdhowPicOnMouse").hide();
            jQuery("#divHide").hide();
        });


                
    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#<%= btnNext.ClientID %>").bind("click", function () {
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                jQuery("#Loding_overlaySec span").html("Loading...");
                jQuery("#Loding_overlaySec").show();
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            });
        });
    </script>
</asp:Content>
