﻿<%@ Page Title="Hotel User - Bedroom Pricing" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master"
    AutoEventWireup="true" CodeFile="BedroomPricing.aspx.cs" Inherits="HotelUser_BedroomPricing" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" runat="Server">
    <h1>
        Bedroom Pricing : <b>"<asp:Label ID="lblHotel" runat="server" Text="Label"></asp:Label>"</b></h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" runat="Server">
    
    <asp:UpdateProgress ID="uprog" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="Loding_overlay" style="display: block;">
                <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
                Loading...</div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="meeting-room-hire">
                <div id="divMessage" runat="server">
                </div>
            </div>
            <asp:ListView ID="lstViewBedroomPricing" runat="server" ItemPlaceholderID="itemPlacehoderID"
                AutomaticGenerateColumns="false" DataKeyNames="Id" OnItemDataBound="lstViewBedroomPricing_ItemDataBound"
                OnItemCommand="lstViewBedroomPricing_ItemCommand">
                <LayoutTemplate>
                    <div class="meeting-description-list">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td bgcolor="#98bcd6">
                                    <table width="100%" border="0" cellspacing="1" cellpadding="5">
                                        <tr bgcolor="#c4d6e2">
                                            <td>
                                                BedRoom Type
                                            </td>
                                            <td align="center">
                                                Single &nbsp;<asp:Label ID="lblCSSingle" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td align="center">
                                                Double &nbsp;<asp:Label ID="lblCSDouble" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td align="center">
                                                Breakfast 
                                            </td>
                                            <td align="center">
                                                Suppl Breakfast pp &nbsp;<asp:Label ID="lblCSBreakfast" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <asp:PlaceHolder ID="itemPlacehoderID" runat="server"></asp:PlaceHolder>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr bgcolor="#d8eefc" class=" row" id="tr<%# Eval("Id") %>">
                        <td>
                            <asp:Label ID="lblBedroomType" runat="server" Text=""></asp:Label>
                            <br>
                            <br>
                            <span class=" modify">
                                <asp:Button ID="lnkModify" CssClass="modify-i" CommandName="View" CommandArgument='<%#Eval("Id") %>'
                                    runat="server" Text="Modify"></asp:Button></span>
                        </td>
                        <td align="center">
                            <asp:Label ID="lblSingle" runat="server"></asp:Label>
                        </td>
                        <td align="center">
                            <asp:Label ID="lblDouble" runat="server"></asp:Label>
                        </td>
                        <td align="center">
                            <asp:Label ID="lblBreakfast" runat="server"></asp:Label>
                        </td>
                        <td align="center">
                            <asp:Label ID="lblSuplBreakfast" runat="server"></asp:Label>
                        </td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr bgcolor="#edf8fe" class=" row" id="tr<%# Eval("Id") %>">
                        <td>
                            <asp:Label ID="lblBedroomType" runat="server" Text=""></asp:Label>
                            <br>
                            <br>
                            <span class=" modify">
                                <asp:Button ID="lnkModify" CssClass="modify-i" CommandName="View" CommandArgument='<%#Eval("Id") %>'
                                    runat="server" Text="Modify"></asp:Button></span>
                        </td>
                        <td align="center">
                            <asp:Label ID="lblSingle" runat="server"></asp:Label>
                        </td>
                        <td align="center">
                            <asp:Label ID="lblDouble" runat="server"></asp:Label>
                        </td>
                        <td align="center">
                            <asp:Label ID="lblBreakfast" runat="server"></asp:Label>
                        </td>
                        <td align="center">
                            <asp:Label ID="lblSuplBreakfast" runat="server"></asp:Label>
                        </td>
                    </tr>
                </AlternatingItemTemplate>
                <EmptyDataTemplate>
                    <div class="meeting-description-list">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom: 15px;">
                            <tr>
                                <td bgcolor="#98bcd6">
                                    <table width="100%" border="0" cellspacing="1" cellpadding="5">
                                        <%--<tr bgcolor="#c4d6e2">
                                            <td>
                                                BedRoom Type
                                            </td>
                                            <td>
                                                Single
                                            </td>
                                            <td>
                                                Double
                                            </td>
                                            <td>
                                                Breakfast
                                            </td>
                                            <td>
                                                Suppl Breakfast pp
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <td align="center" colspan="5" align="center">
                                                <b>No record found !</b>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </EmptyDataTemplate>
            </asp:ListView>
            <asp:HiddenField ID="hdnBedroomId" Value="0" runat="server" />
            <asp:HiddenField ID="hdnSelectedRowID" runat="server" Value="0" />
            <asp:Panel ID="pnlbedRoomPrice" runat="server">
            <div class="booking-details" id="divBedRoomRoomPriceForm" runat="server">
                <ul>
                    <li class="error" style="display: none;"></li>
                    <li class="value9">
                        <div class="col17">
                            <strong>BedRoom Type</strong>
                        </div>
                        <div class="col18">
                            <div class="rowElem" id="bedroomtype">
                                <asp:Label ID="lblBedroomType" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                    </li>
                    <li class="value8">
                        <div class="col17">
                            <strong>Single <span style="color: Red">*</span></strong>
                        </div>
                        <div class="col18">
                            <asp:TextBox ID="txtSingle" runat="server" class="inputbox10" Width="50px" MaxLength="10"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="txtSingle"
                                ValidChars="0123456789." runat="server">
                            </asp:FilteredTextBoxExtender>
                        </div>
                    </li>
                    <li class="value8">
                        <div class="col17">
                            <strong>Double <span style="color: Red">*</span></strong>
                        </div>
                        <div class="col18">
                            <asp:TextBox ID="txtDouble" runat="server" class="inputbox10" Width="50px" MaxLength="10"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtDouble"
                                ValidChars="0123456789." runat="server">
                            </asp:FilteredTextBoxExtender>
                        </div>
                    </li>
                    <li class="value9">
                        <div class="col17">
                            <strong>Breakfast </strong>
                        </div>
                        <div class="col19">
                            <div class="col19-left" id="divIsBreakfast">
                                <asp:RadioButton ID="rdIncl" GroupName="IsBreakfast" runat="server" class="styled" />Incl
                                <asp:RadioButton ID="rdExcl" GroupName="IsBreakfast" runat="server" class="styled" />Excl
                            </div>
                        </div>
                    </li>
                    <li class="value9" id="rowBreakfast" runat="server">
                        <div class="col17">
                            <strong>Suppl Breakfast pp <span style="color: Red">*</span></strong>
                        </div>
                        <div class="col19">
                            <div class="col19-left">
                                <asp:TextBox ID="txtSupplyBreakfast" runat="server" class="inputbox10" Width="50px"
                                    MaxLength="10"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" TargetControlID="txtSupplyBreakfast"
                                    ValidChars="0123456789." runat="server">
                                </asp:FilteredTextBoxExtender>
                            </div>
                        </div>
                    </li>
                    <li class="value10">
                        <div class="col21">
                            <div class="button_section">
                                <asp:LinkButton ID="btnUpdate" CssClass="select" OnClick="btnUpdate_Click" OnClientClick="return UpdateValid();"
                                    runat="server" Text="Save" />
                                    <span>or</span>
                                <asp:LinkButton ID="btnCancel" OnClick="btnCancel_Click" runat="server" Text="Cancel" />
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpdate" />
            <asp:PostBackTrigger ControlID="btnCancel" />
        </Triggers>
    </asp:UpdatePanel>
    <div id="Loding_overlaySec">
        <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
        <span>Saving...</span></div>
    <div class="button_sectionNext" id="divNext" runat="server">
        <asp:LinkButton ID="btnNext" runat="server" Text="Next >>" class="RemoveCookie" OnClick="btnNext_Click" />
    </div>
    <script type="text/javascript">

        jQuery(document).ready(function () {
        CheckIsBreakfast(); 
        });
        function CheckIsBreakfast() {
            
           jQuery("#<%= rdExcl.ClientID %>").change(function () {
            debugger;
                if (jQuery("#<%= rdExcl.ClientID %>").is("checked")) {
                    jQuery("#<%=rowBreakfast.ClientID %>").hide();
                }
                else {
                    jQuery("#<%=rowBreakfast.ClientID %>").show();
                }
            });
            jQuery("#<%= rdIncl.ClientID %>").change(function () {
                if (jQuery("#<%= rdIncl.ClientID %>").is("checked")) {
                    jQuery("#<%=rowBreakfast.ClientID %>").show();
                }
                else {
                    jQuery("#<%=rowBreakfast.ClientID %>").hide();
                }
            });
        
        
        }
        
    </script>
    <script language="javascript" type="text/javascript">

        function UpdateValid() {

            var isvalid = true;
            var errormessage = "";

            var singlePrice = jQuery("#<%= txtSingle.ClientID %>").val();

            if (singlePrice.length <= 0 || parseFloat(singlePrice, 10) == 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Single price is required.";
                isvalid = false;
            }
            else if (isNaN(singlePrice)) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }

                errormessage += "Enter valid single price";
                isvalid = false;
            }
            var doublePrice = jQuery("#<%= txtDouble.ClientID %>").val();

            if (doublePrice.length <= 0 || parseFloat(doublePrice, 10) == 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Double price is required.";
                isvalid = false;
            }
            else if (isNaN(doublePrice)) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }

                errormessage += "Enter valid Double price";
                isvalid = false;
            }

            if (jQuery("#<%= rdExcl.ClientID %>:checked").val() == "rdExcl") {
                var BreakfastPrice = jQuery("#<%= txtSupplyBreakfast.ClientID %>").val();
                if (BreakfastPrice.length <= 0 || parseFloat(BreakfastPrice, 10) == 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Suppl breakfast price is required.";
                    isvalid = false;
                }
                else if (isNaN(BreakfastPrice)) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }

                    errormessage += "Enter valid breakfast price";
                    isvalid = false;
                }

            }

            if (!isvalid) {
                jQuery("#<%= divBedRoomRoomPriceForm.ClientID %> .error").show();
                jQuery("#<%= divBedRoomRoomPriceForm.ClientID %> .error").html(errormessage);
                var offseterror = jQuery("#<%= divBedRoomRoomPriceForm.ClientID %> .error").offset();
                jQuery("body").scrollTop(offseterror.top);
                jQuery("html").scrollTop(offseterror.top);
                return false;
            }
            jQuery("#<%= divBedRoomRoomPriceForm.ClientID %> .error").html("");
            jQuery("#<%= divBedRoomRoomPriceForm.ClientID %> .error").hide();
            jQuery("#Loding_overlaySec span").html("Saving...");
            jQuery("#Loding_overlaySec").show();
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';
        }
    </script>
    <script language="javascript" type="text/javascript">

        jQuery(document).ready(function () {
            CallMyMethod();
        });

        function CallMyMethod() {

            CheckIsBreakfast();
            var SeletedRowID = jQuery('#<%=hdnSelectedRowID.ClientID %>').val();
            if (SeletedRowID != 0) {
                jQuery('#' + SeletedRowID).css("background-color", "#FF9");
            }
            jQuery('.rowElem').jqTransform({ imgPath: ' ' });
        }

        function ChangeRowColor(rowID) {
            jQuery('#' + rowID).css("background-color", "#FF9");
            jQuery('#<%=hdnSelectedRowID.ClientID %>').val(rowID);
        }

    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#<%= btnNext.ClientID %>").bind("click", function () {
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                jQuery("#Loding_overlaySec span").html("Loading...");
                jQuery("#Loding_overlaySec").show();
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            });
        });
    </script>
</asp:Content>
