﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using LMMR.Entities;
using System.Text;
using LMMR.Business;
using System.Collections;
using System.IO;
using LMMR.Data;
using LMMR;
using System.Configuration;
using log4net;
using log4net.Config;
#endregion
public partial class HotelUser_BedRoomDesc : System.Web.UI.Page
{
    #region Variables and Properties
    UserManager objUserManager = new UserManager();
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_BedRoomDesc));
    public static TList<BedRoomDesc> ObjAllLanguageDescription = new TList<BedRoomDesc>();
    BedRoomDescManager objBedRoomDescManager = new BedRoomDescManager();
    WizardLinkSettingManager ObjWizardLinkSetting = new WizardLinkSettingManager();
    DashboardManager ObjDashBoard = new DashboardManager();
    StringBuilder ObjSBLanguage = new StringBuilder();
    HotelInfo ObjHotelinfo = new HotelInfo();
    HotelManager objHotelManager = new HotelManager();
    WizardLinkSettingManager Objwizard = new WizardLinkSettingManager();
    DashboardManager Objdash = new DashboardManager();
    int intHotelID = 0;
    long intUserID = 0;
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentHotelID"] != null)
        {
            intHotelID = Convert.ToInt32(Session["CurrentHotelID"].ToString());
            Users objUser = (Users)Session["CurrentUser"];
            intUserID = objUser.UserId;
            if (!IsPostBack)
            {
                #region Leftmenu
                HyperLink lnk = (HyperLink)this.Master.FindControl("lnkConferenceInfo");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkContactDetails");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkFacilities");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkCancelationPolicy");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkDescriptionMeetingRoom");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkConfiguration");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkPrintFactSheet");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkPreviewOnWeb");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkDescriptionBedRoom");
                lnk.CssClass = "selected";
                #endregion
                divMessage.Style.Add("display", "none");
                divMessage.Attributes.Add("class", "error");
                divMessage.Attributes.Add("float", "left");
                Session["LinkID"] = "lnkPropertyLevel";

                if (ObjHotelinfo.GetHotelByHotelID(intHotelID).IsBedroomAvailable)
                {
                    CreateDynamicLanguageTab(intHotelID);
                    BindBedRoomListView();
                    BindBedRoomType();
                    IsOneBedRoomDone();
                }
                else
                {
                    //divMessage.InnerHtml = " No bedrooms are provided by your property. Press next button to proceed to the next level.";
                    divMessage.InnerHtml = " No bedrooms were added to the system. You can add bedrooms if you want before going online. In case you want to add bedrooms after going online, please contact the hotel support on hotelsupport@lastminutemeetingroom.com";
                    divMessage.Attributes.Add("class", "warning");
                    divMessage.Style.Add("display", "block");
                    divNext.Visible = true;
                    addbedroom.Visible = false;
                    divBedRoomRoomForm.Visible = false;
                    trLegend.Visible = false;

                }
            }

            lblHotel.Text = objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Name;

        }
        else
        {
            Response.Redirect("~/login.aspx", true);

        }


    }
    #endregion

    #region Function
    /// <summary>
    /// For check go online status for current hotel
    /// </summary>
    public void Getgoonline()
    {
        if (ObjHotelinfo.GetHotelGoOnline(intHotelID).Count > 0)
        {
            divNext.Visible = false;
            ViewState["Count"] = ObjHotelinfo.GetHotelGoOnline(intHotelID).Count;
            //Disable description textbox if hotel is goonline
            if (Convert.ToString(Session["Operator"]) == "1")
            {

            }
            else
            {
                txtDesc.ReadOnly = true;
            }
            //Disable description textbox if hotel is goonline
        }
    }

    /// <summary>
    /// Bind dropdown with bedroom type with enum
    /// </summary>
    void BindBedRoomType()
    {
        Hashtable ht = GetEnumForBind(typeof(BedRoomType));
        ddlBedroomType.Items.Clear();
        ddlBedroomType.Items.Add(new ListItem
        {
            Text = "--Select--",
            Value = "0"
        });

        ddlBedroomType.AppendDataBoundItems = true;
        ddlBedroomType.DataSource = ht;
        ddlBedroomType.DataTextField = "value";
        ddlBedroomType.DataValueField = "key";
        ddlBedroomType.DataBind();
    }

    /// <summary>
    /// This function used for check at least one bedroom is done in all language. 
    /// </summary>
    void IsOneBedRoomDone()
    {
        Hotel hotel = ObjHotelinfo.GetHotelByHotelID(intHotelID);
        TList<CountryLanguage> lstlangCountry = ObjHotelinfo.GetAllRequiredLanguageByCountryId(Convert.ToInt32(hotel.CountryId));
        TList<BedRoom> ObjBedRoomCount = objBedRoomDescManager.GetAllBedRoom(intHotelID);
        //Start this condition is added, if bed room is deleted then next button will show
        if (Convert.ToString(ViewState["Count"]) != "1" && ObjWizardLinkSetting.CheckLeftmenu(intHotelID, Convert.ToInt32(PropertyLevelSectionLeftMenu.lnkDescriptionBedRoom)).Count == 1)
        {
            divNext.Visible = true;
        }
        //End this condition is added, if bed room is deleted then next button will show
        for (int i = 0; i <= ObjBedRoomCount.Count - 1; i++)
        {
            TList<BedRoomDesc> lstCountdesc = DataRepository.BedRoomDescProvider.GetByBedRoomId(ObjBedRoomCount[i].Id);
            if (lstlangCountry.Count == lstCountdesc.Count)
            {
                divNext.Visible = true;
                if (Convert.ToString(ViewState["Count"]) != "1" && ObjWizardLinkSetting.CheckLeftmenu(intHotelID, Convert.ToInt32(PropertyLevelSectionLeftMenu.lnkDescriptionBedRoom)).Count == 0)
                {
                    divMessage.InnerHtml += "&nbsp; Please click on next button to move ahead.";
                }
                break;
            }
        }
        Getgoonline();

    }

    /// <summary>
    /// Get enum by value and name
    /// </summary>
    /// <param name="enumeration"></param>
    /// <returns>Hashtable</returns>
    public Hashtable GetEnumForBind(Type enumeration)
    {
        string[] names = Enum.GetNames(enumeration);
        Array values = Enum.GetValues(enumeration);
        Hashtable ht = new Hashtable();
        for (int i = 0; i < names.Length; i++)
        {
            ht.Add(Convert.ToInt32(values.GetValue(i)).ToString(), names[i]);
        }
        return ht;
    }

    /// <summary>
    /// Bind Bedroom list with listview control. 
    /// </summary>
    void BindBedRoomListView()
    {
        if (objBedRoomDescManager.GetAllBedRoom(intHotelID).Count > 0)
        {
            addbedroom.Visible = true;
            lstViewBedRoomDesc.Visible = true;
            divBedRoomRoomForm.Visible = false;
            trLegend.Visible = true;
            lstViewBedRoomDesc.DataSource = objBedRoomDescManager.GetAllBedRoom(intHotelID);
            lstViewBedRoomDesc.DataBind();
        }
        else
        {
            ClearForm();
            ddlBedroomType.Enabled = true;
            addbedroom.Visible = false;
            lstViewBedRoomDesc.Visible = false;
            divBedRoomRoomForm.Visible = true;
            trLegend.Visible = false;
            btnUpdate.Visible = false;
            btnSave.Visible = true;
            ScriptManager.RegisterStartupScript(this, typeof(Page), "colorChange", "CallMyMethod();", true);
        }
        if (objBedRoomDescManager.GetAllBedRoom(intHotelID).Count == 2)
        {
            addbedroom.Visible = false;
        }
    }

    /// <summary>
    /// Get Bedroom Description By LanguageID.
    /// This Methode call by ajax function from the aspx page.
    /// </summary>
    /// <param name="languageID"></param>
    /// <returns>string</returns>
    [WebMethod]
    public static string GetDescByLanguageID(int languageID, int bedroomID)
    {
        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        BedRoomDescManager ObjBedRoomDesc = new BedRoomDescManager();
        TList<BedRoomDesc> lsthdesc = ObjBedRoomDesc.GetBedRoomDescByBedRoomID(bedroomID);
        var data = lsthdesc.Find(a => a.LanguageId == languageID);

        if (data != null)
        {
            var varDesc = new { data.Description, data.Id };
            return serializer.Serialize(varDesc);
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// Get Language Name By LanguageID.
    /// This Methode call by ajax function from the aspx page.
    /// </summary>
    /// <param name="languageID"></param>
    /// <returns>string</returns>
    [WebMethod]
    public static string GetNameByLanguageID(int languageID)
    {
        BedRoomDescManager objBedRoomDescManager = new BedRoomDescManager();
        string strName = objBedRoomDescManager.GetLanguageNameById(languageID);
        return strName;
    }



    /// <summary>
    /// Bind language tab daynamic.
    /// </summary>
    /// <param name="hotelID"></param>
    public void CreateDynamicLanguageTab(int hotelID)
    {
        Hotel ObjHotel = DataRepository.HotelProvider.GetById(hotelID);
        TList<CountryLanguage> lstlangbyCountry = ObjHotelinfo.GetAllRequiredLanguageByCountryId(Convert.ToInt32(ObjHotel.CountryId));
        string whereclause = LanguageColumn.Id + " in (";
        foreach (CountryLanguage cl in lstlangbyCountry)
        {
            whereclause += cl.LanguageId + ",";
        }
        if (whereclause.Length > 0)
        {
            whereclause = whereclause.Substring(0, whereclause.Length - 1);
        }
        whereclause += ")";
        int count = 0;
        ObjAllLanguageDescription = objBedRoomDescManager.GetBedRoomDescByBedRoomID(Convert.ToInt32(hdnBedRoomID.Value));
        TList<Language> lstLang = DataRepository.LanguageProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out count);

        //TList<Language> lstLang = objBedRoomDescManager.GetAllRequiredLanguage();
        if (lstLang.Count > 0)
        {
            if (hdnBedRoomID.Value != "0")
            {
                ObjSBLanguage.Append(@"<ul class=""TabbedPanelsTabGroup"">");
                for (int i = 0; i <= lstLang.Count - 1; i++)
                {
                    int intlanguaegeID = Convert.ToInt32(lstLang[i].Id);
                    var varLanguage = objBedRoomDescManager.GetBedRoomDescByBedRoomID(Convert.ToInt32(hdnBedRoomID.Value)).Find(a => a.LanguageId == intlanguaegeID);

                    if (varLanguage != null)
                    {

                        ObjSBLanguage.Append(@"<li class='TabbedPanelsTab' onclick='javascript:GetLanguageID(" + lstLang[i].Id + ");'><a href='#' >" + lstLang[i].Name + "</a></li>");
                    }
                    else
                    {
                        ObjSBLanguage.Append(@"<li class='TabbedPanelsTab1' onclick='javascript:GetLanguageID(" + lstLang[i].Id + ");'><a href='#'>" + lstLang[i].Name + "</a></li>");

                    }

                }
                ObjSBLanguage.Append(@"</ul>");
                ObjSBLanguage.Append(@"<div class=""TabbedPanelsContentGroup"">");
                for (int i = 0; i <= lstLang.Count - 1; i++)
                {
                    ObjSBLanguage.Append(@"<div class=""TabbedPanelsContent""></div>");
                }
                ObjSBLanguage.Append(@"</div>");
            }
            else
            {
                ObjSBLanguage.Append(@"<ul class=""TabbedPanelsTabGroup"">");
                for (int i = 0; i <= lstLang.Count - 1; i++)
                {
                    ObjSBLanguage.Append(@"<li class='TabbedPanelsTab1' onclick='javascript:GetLanguageID(" + lstLang[i].Id + ");'><a href='#' >" + lstLang[i].Name + "</a></li>");

                }
                ObjSBLanguage.Append(@"</ul>");
                ObjSBLanguage.Append(@"<div class=""TabbedPanelsContentGroup"">");
                for (int i = 0; i <= lstLang.Count - 1; i++)
                {
                    ObjSBLanguage.Append(@"<div class=""TabbedPanelsContent""></div>");
                }
                ObjSBLanguage.Append(@"</div>");




            }
            litLangugeTab.Text = ObjSBLanguage.ToString();
        }
    }

    /// <summary>
    ///  Function to get bedroom detail by bedroomID.
    /// </summary>
    /// <param name="bedRoomID"></param>
    void FillBedRoom(int bedRoomID)
    {
        BedRoom ObjBedRoom = objBedRoomDescManager.GetBedRoomByID(bedRoomID);
        ddlBedroomType.SelectedValue = ObjBedRoom.Types.ToString();
        txtAllotment.Text = ObjBedRoom.Allotment.ToString();
        int intLanguageID = Convert.ToInt32(objBedRoomDescManager.GetAllRequiredLanguage().Find(a => a.Name == "English").Id);
        txtDesc.Text = GetBedRoomDescription(bedRoomID, intLanguageID);
        hdnBedRoomImage.Value = ObjBedRoom.Picture;

    }

    /// <summary>
    /// Function to get bedroom description by bedroomID and LanguageID.
    /// </summary>
    /// <param name="meetingRoomID"></param>
    /// <param name="languageID"></param>
    /// <returns>String</returns>
    public string GetBedRoomDescription(int meetingRoomID, int languageID)
    {
        string desc = string.Empty;
        TList<BedRoomDesc> ObjMeetingRoomDescription = objBedRoomDescManager.GetBedRoomDescByBedRoomID(meetingRoomID);
        var getData = ObjMeetingRoomDescription.Find(a => a.LanguageId == languageID);
        if (getData != null)
        {
            desc = getData.Description;
        }
        else
        {
            desc = "";
        }
        return desc;
    }

    /// <summary>
    /// Function to remove form value.
    /// </summary>
    void ClearForm()
    {
        txtAllotment.Text = "";
        txtDesc.Text = "";
        ddlBedroomType.SelectedValue = "0";
    }

    #endregion

    #region Events
    /// <summary>
    /// This ItemCommand event used for perform operation view data, delete data
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lstViewBedRoomDesc_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        //Disable description textbox if hotel is goonline
        if (Convert.ToString(ViewState["Count"]) == "1")
        {
            if (Convert.ToString(Session["Operator"]) == "1")
            {

            }
            else
            {
                int intRooomID = Convert.ToInt32(e.CommandArgument);
                Hotel hotel = ObjHotelinfo.GetHotelByHotelID(intHotelID);
                TList<CountryLanguage> lstlangCountry = ObjHotelinfo.GetAllRequiredLanguageByCountryId(Convert.ToInt32(hotel.CountryId));                
                TList<BedRoomDesc> lstCountdesc = DataRepository.BedRoomDescProvider.GetByBedRoomId(intRooomID);
                if (lstlangCountry.Count <= lstCountdesc.Count)
                {
                    txtDesc.ReadOnly = true;
                }
                else
                {
                    txtDesc.ReadOnly = false;
                }
            }
        }
        //Disable description textbox if hotel is goonline
        int intBedroomID = 0;
        switch (e.CommandName.Trim())
        {
            case "View":
                ClearForm();
                hdnIDS.Value = "";
                hdnBedRoomDescID.Value = "0";
                intBedroomID = Convert.ToInt32(e.CommandArgument);
                hdnBedRoomID.Value = intBedroomID.ToString();
                divBedRoomRoomForm.Visible = true;
                CreateDynamicLanguageTab(intHotelID);
                FillBedRoom(Convert.ToInt32(hdnBedRoomID.Value));
                ObjAllLanguageDescription = objBedRoomDescManager.GetBedRoomDescByBedRoomID(Convert.ToInt32(hdnBedRoomID.Value));
                btnSave.Visible = false;
                btnUpdate.Visible = true;
                ddlBedroomType.Enabled = false;
                txtDesc.Focus();
                ScriptManager.RegisterStartupScript(this, typeof(Page), "colorChange", "CallMyMethod();", true);
                break;

            case "deleteItem":
                intBedroomID = 0;
                hdnBedRoomID.Value = "0";
                string msg = objBedRoomDescManager.DeleteBedRoom(Convert.ToInt32(e.CommandArgument));
                BindBedRoomListView();
                IsOneBedRoomDone();
                CreateDynamicLanguageTab(intHotelID);
                break;

            default:
                break;
        }
        divMessage.Style.Add("display", "none");
        divMessage.Attributes.Add("class", "error");
    }

    /// <summary>
    /// This event used for bindbedroom Item with listview.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lstViewBedRoomDesc_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {

            ListViewDataItem currentItem = (ListViewDataItem)e.Item;
            DataKey currentDataKey = this.lstViewBedRoomDesc.DataKeys[currentItem.DataItemIndex];
            BedRoom room = e.Item.DataItem as BedRoom;
            Button btnModify = (Button)e.Item.FindControl("lnkModify");
            btnModify.OnClientClick = "ChangeRowColor('tr" + room.Id + "')";
            Label lblBedRoomType = (Label)currentItem.FindControl("lblBedRoomType");
            lblBedRoomType.Text = Enum.GetName(typeof(BedRoomType), room.Types);
            Label lblDesc = (Label)currentItem.FindControl("lblDescription");
            var getDescription = objBedRoomDescManager.GetBedRoomDescByBedRoomID(Convert.ToInt32(currentDataKey.Value));
            int englishID = Convert.ToInt32((objBedRoomDescManager.GetAllRequiredLanguage().Find(b => b.Name == "English")).Id);
            var englishDesc = getDescription.Find(a => a.LanguageId == englishID);
            if (englishDesc != null)
            {
                string desc = englishDesc.Description;
                lblDesc.Text = desc;
            }
            else
            {
                lblDesc.Text = "No Description available!";
            }
            if (room.Picture != null)
            {
                Image imgHotel = (Image)currentItem.FindControl("imgBedRoom");
                imgHotel.Width = 70;
                imgHotel.Height = 70;
                imgHotel.ImageUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "BedroomImage/" + room.Picture;
            }
        }

    }

    /// <summary>
    /// This button click button used for add new data in database.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            hdnSelectedRowID.Value = "0";
            int intLanguageID = 0;

            int intSelectedType = Convert.ToInt32(ddlBedroomType.SelectedValue);
            var getExistBedRoom = objBedRoomDescManager.GetAllBedRoom(intHotelID).Find(a => a.Types == intSelectedType);

            if (getExistBedRoom != null)
            {
                divMessage.InnerHtml = "This type of bedroom already exist !";
                divMessage.Attributes.Add("class", "error");
                divMessage.Style.Add("display", "block");
            }
            else if (objBedRoomDescManager.GetAllBedRoom(intHotelID).Count == 2)
            {
                divMessage.InnerHtml = "You can not able to add more then two bedroom !";
                divMessage.Attributes.Add("class", "error");
                divMessage.Style.Add("display", "block");
            }
            else
            {
                //get language ID
                if (hdnIDS.Value == "")
                {
                    intLanguageID = Convert.ToInt32(objBedRoomDescManager.GetAllRequiredLanguage().Find(a => a.Name == "English").Id);
                }
                else
                {
                    intLanguageID = Convert.ToInt32(hdnIDS.Value);
                }

                if (intLanguageID == Convert.ToInt32(objBedRoomDescManager.GetAllRequiredLanguage().Find(a => a.Name == "English").Id))
                {
                    string strImageName = "";
                    // get image name
                    if (ulImage.HasFile)
                    {
                        int fileSize = ulImage.PostedFile.ContentLength;
                        if (fileSize > (Convert.ToInt32(ConfigurationManager.AppSettings["LimitKB2"])))
                        {
                            divMessage.InnerHtml = "Filesize of image is too large. Maximum file size permitted is " + ConfigurationManager.AppSettings["MessageKB2"] + ".";
                            divMessage.Attributes.Add("class", "error");
                            divMessage.Style.Add("display", "block");
                            return;
                        }

                        else
                        {
                            Guid objID = Guid.NewGuid();
                            strImageName = Path.GetFileName(ulImage.FileName);
                            strImageName = strImageName.Replace(" ", "").Replace("%20", "").Trim();
                            strImageName = System.Text.RegularExpressions.Regex.Replace(strImageName, @"[^a-zA-Z 0-9'.@]", string.Empty).Trim();
                            strImageName = objID + strImageName;
                            ulImage.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "BedroomImage/") + strImageName);
                        }
                    }

                    BedRoom bedRoom = new BedRoom();
                    bedRoom.HotelId = intHotelID;
                    bedRoom.Types = Convert.ToInt32(ddlBedroomType.SelectedValue);
                    bedRoom.Allotment = Convert.ToInt32(txtAllotment.Text.Replace("'", ""));
                    bedRoom.Picture = strImageName;
                    bedRoom.UpdatedBy = intUserID;


                    BedRoomDesc bedRoomDesc = new BedRoomDesc();
                    bedRoomDesc.LanguageId = intLanguageID;
                    bedRoomDesc.Description = txtDesc.Text.Trim();


                    BedRoomPictureImage ObjPic = new BedRoomPictureImage();
                    ObjPic.ImageName = strImageName;
                    ObjPic.IsMain = true;
                    ObjPic.FileType = 0;
                    ObjPic.OrderNumber = 1;

                    //call function to add new bedroom room 
                    objBedRoomDescManager.AddNewBedRoom(bedRoom, bedRoomDesc, ObjPic);


                    //Get meeting room desc by Bed room Id.
                    ObjAllLanguageDescription = objBedRoomDescManager.GetBedRoomDescByBedRoomID(Convert.ToInt32(bedRoom.Id));

                    //Create tab Again....
                    CreateDynamicLanguageTab(intHotelID);
                    BindBedRoomListView();
                    divMessage.InnerHtml = objBedRoomDescManager.propMessage;
                    divMessage.Attributes.Add("class", "succesfuly");
                    divMessage.Style.Add("display", "block");

                }
                else
                {
                    divMessage.InnerHtml = "Please insert first in english !";
                    divMessage.Attributes.Add("class", "error");
                    divMessage.Style.Add("display", "block");
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This button click event used for update existing data into table.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            hdnSelectedRowID.Value = "0";
            string strImageName = string.Empty;
            int intLanguageID = 0;
            int intBedRoomDescId = 0;
            int intSelectedType = Convert.ToInt32(ddlBedroomType.SelectedValue);
            var getExistBedRoom = objBedRoomDescManager.GetAllBedRoom(intHotelID).Find(a => a.Types == intSelectedType);

            //get language ID
            if (hdnIDS.Value == "")
            {
                intLanguageID = Convert.ToInt32(objBedRoomDescManager.GetAllRequiredLanguage().Find(a => a.Name == "English").Id);
            }
            else
            {
                intLanguageID = Convert.ToInt32(hdnIDS.Value);
            }

            strImageName = hdnBedRoomImage.Value; ;
            // get image name by filename
            if (ulImage.HasFile)
            {
                int fileSize = ulImage.FileBytes.Length;
                if (fileSize > (Convert.ToInt32(ConfigurationManager.AppSettings["LimitKB2"])))
                {
                    divMessage.InnerHtml = "Filesize of image is too large. Maximum file size permitted is " + ConfigurationManager.AppSettings["MessageKB2"] + ".";
                    divMessage.Attributes.Add("class", "error");
                    divMessage.Style.Add("display", "block");
                    return;
                }

                else
                {
                    File.Delete(Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "BedroomImage/") + strImageName);
                    Guid objID = Guid.NewGuid();
                    strImageName = Path.GetFileName(ulImage.FileName);
                    strImageName = strImageName.Replace(" ", "").Replace("%20", "").Trim();
                    strImageName = System.Text.RegularExpressions.Regex.Replace(strImageName, @"[^a-zA-Z 0-9'.@]", string.Empty).Trim();
                    strImageName = objID + strImageName;
                    ulImage.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "BedroomImage/") + strImageName);
                }

            }

            BedRoom objBedroom = objBedRoomDescManager.GetBedRoomByID(Convert.ToInt32(hdnBedRoomID.Value));
            objBedroom.HotelId = intHotelID;
            objBedroom.Types = Convert.ToInt32(ddlBedroomType.SelectedValue);
            objBedroom.Allotment = Convert.ToInt32(txtAllotment.Text.Replace("'", ""));
            objBedroom.Picture = strImageName;
            objBedroom.UpdatedBy = intUserID;
            if (Convert.ToInt32(hdnBedRoomDescID.Value) > 0)
            {
                intBedRoomDescId = Convert.ToInt32(hdnBedRoomDescID.Value);
                BedRoomDesc objBedRoomDesc = objBedRoomDescManager.GetBedroomDescByID(intBedRoomDescId);
                objBedRoomDesc.LanguageId = intLanguageID;
                objBedRoomDesc.Description = txtDesc.Text.Trim();

                //call function to update bedroom room 
                objBedRoomDescManager.UpdateBedRoom(objBedroom, objBedRoomDesc);
            }
            else
            {
                ObjAllLanguageDescription = objBedRoomDescManager.GetBedRoomDescByBedRoomID(Convert.ToInt32(objBedroom.Id));
                if (ObjAllLanguageDescription.Find(a => a.LanguageId == intLanguageID) != null)
                {
                    intBedRoomDescId = Convert.ToInt32(ObjAllLanguageDescription.Find(a => a.LanguageId == intLanguageID).Id);
                    BedRoomDesc objBedRoomDesc = objBedRoomDescManager.GetBedroomDescByID(intBedRoomDescId);
                    objBedRoomDesc.LanguageId = intLanguageID;
                    objBedRoomDesc.Description = txtDesc.Text.Trim();
                    //call function to update bedroom room 
                    objBedRoomDescManager.UpdateBedRoom(objBedroom, objBedRoomDesc);
                }
                else
                {
                    BedRoomDesc objBedroomDesc = new BedRoomDesc();
                    objBedroomDesc.LanguageId = intLanguageID;
                    objBedroomDesc.Description = txtDesc.Text.Trim();
                    objBedRoomDescManager.AddBedroomDesc(objBedroom, objBedroomDesc);
                }

            }
            hdnIDS.Value = "";
            divMessage.InnerHtml = objBedRoomDescManager.propMessage;
            //CreateDynamicLanguageTab(intHotelID);
            BindBedRoomListView();
            IsOneBedRoomDone();
            divMessage.Attributes.Add("class", "succesfuly");
            divMessage.Style.Add("display", "block");
            divBedRoomRoomForm.Visible = false;

        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This button click used for hide bedroom detail form and reset too.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        //divBedRoomRoomForm.Visible = false;
        ClearForm();
        divMessage.Style.Add("display", "none");
        divMessage.Attributes.Add("class", "error");
        hdnSelectedRowID.Value = "0";
        hdnIDS.Value = "";
        BindBedRoomListView();
    }

    /// <summary>
    /// This button click used for proceed to next step
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNext_Click(object sender, EventArgs e)
    {
        Objwizard.ThisPageIsDone(intHotelID, Convert.ToInt32(PropertyLevelSectionLeftMenu.lnkDescriptionBedRoom));
        Objdash.DoneThisStep(intHotelID, Convert.ToInt32(PropertyLevelSection.lnkBedRooms));
        Response.Redirect("PricingMeetingRooms.aspx", true);
    }

    /// <summary>
    /// This function used for open form for new entry.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkAddNewBedRoom_Click(object sender, EventArgs e)
    {
        hdnIDS.Value = "";
        hdnSelectedRowID.Value = "0";
        ddlBedroomType.Enabled = true;
        hdnBedRoomDescID.Value = "0";
        hdnBedRoomID.Value = "0";
        CreateDynamicLanguageTab(intHotelID);
        divBedRoomRoomForm.Visible = true;
        btnSave.Visible = true;
        btnCancel.Visible = true;
        btnUpdate.Visible = false;
        txtDesc.ReadOnly = false;
        ClearForm();
        divMessage.Style.Add("display", "none");
        divMessage.Attributes.Add("class", "error");
        ScriptManager.RegisterStartupScript(this, typeof(Page), "colorChange", "CallMyMethod();", true);
    }
    #endregion
}