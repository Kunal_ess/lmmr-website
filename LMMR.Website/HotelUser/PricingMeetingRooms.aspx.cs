﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using System.Data;
using LMMR.Entities;
using System.Text;
using log4net;
using log4net.Config;
using System.Net;
#endregion

public partial class HotelUser_PricingMeetingRooms : System.Web.UI.Page
{

    #region Variables and Properties
    UserManager objUserManager = new UserManager();
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_PricingMeetingRooms));
    MeetingRoomConfigManager ObjMeetingRoomConfigManager = new MeetingRoomConfigManager();
    PackagePricingManager objPackagePricingManager = new PackagePricingManager();
    MeetingRoomDescManager ObjMeetingRoomDesc = new MeetingRoomDescManager();
    WizardLinkSettingManager ObjWizardLinkSetting = new WizardLinkSettingManager();
    StringBuilder ObjSBPackage = new StringBuilder();
    HotelManager objHotelManager = new HotelManager();
    HotelInfo ObjHotelinfo = new HotelInfo();
    CurrencyManager objCurrency = new CurrencyManager();
    int intHotelID = 0;
    long intCountryID = 0;
    long intCurrencyID = 0;
    decimal? decimalMeetingRoomVat = 0;
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl + "/";
        }
    }
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["CurrentHotelID"] != null)
        {
            Session["LinkID"] = "lnkPropertyLevel";
            intHotelID = Convert.ToInt32(Session["CurrentHotelID"].ToString());
            lblHotel.Text = objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Name;
            intCountryID = objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).CountryId;
            if (!IsPostBack)
            {
                #region Leftmenu
                HyperLink lnk = (HyperLink)this.Master.FindControl("lnkConferenceInfo");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkContactDetails");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkFacilities");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkCancelationPolicy");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkDescriptionMeetingRoom");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkConfiguration");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkPrintFactSheet");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkPreviewOnWeb");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkDescriptionBedRoom");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkMeetingRooms");
                lnk.CssClass = "selected";
                #endregion
                BindPackageList();
                //BindPackageWithCheckBoxList();
                CreateDynamicPackageTab();
                BindFoodBeveragesList();
                BindEquipmentList();
                BindMeetingRoomPriceList();
                BindOthersList();
                //if user is hoteladmin.
                divPackageModify.Visible = false;
                Getgoonline();

            }

        }
        else
        {

            Response.Redirect("~/login.aspx", false);
            return;
        }

    }
    #endregion

    #region Functions
    /// <summary>
    /// For check go online status for current hotel.
    /// </summary>
    public void Getgoonline()
    {
        if (ObjHotelinfo.GetHotelGoOnline(intHotelID).Count > 0)
        {
            divNext.Visible = false;
            ViewState["Count"] = ObjHotelinfo.GetHotelGoOnline(intHotelID).Count;
        }
    }


    private void BindCurrencySymbole()
    {
        Hotel objHotel = objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"]));
        intCountryID = objHotel.CountryId;
        intCurrencyID = objHotel.CurrencyId;
        Currency objCurr = objCurrency.GetCurrencyDetailsByID(intCurrencyID);
        if (objCurr != null)
        {
            lblCSPackageFull.Text = "(" + WebUtility.HtmlDecode(objCurr.CurrencySignature) + ")";
            lblCSPackageFullEdit.Text = "(" + WebUtility.HtmlDecode(objCurr.CurrencySignature) + ")";
            lblCSPackageHalf.Text = "(" + WebUtility.HtmlDecode(objCurr.CurrencySignature) + ")";
            lblCSPackageHalfEdit.Text = "(" + WebUtility.HtmlDecode(objCurr.CurrencySignature) + ")";
            lblCSPackageOnlien.Text = "(" + WebUtility.HtmlDecode(objCurr.CurrencySignature) + ")";
            lblCSPackageOnlineEdit.Text = "(" + WebUtility.HtmlDecode(objCurr.CurrencySignature) + ")";
            ViewState["CurrencySign"] = WebUtility.HtmlDecode(objCurr.CurrencySignature);
        }
    }
    /// <summary>
    /// function to bind package price by packageId with listview.
    /// </summary>
    void BindPackageList()
    {

        if (Session["CurrentHotelID"] != null)
        {
            BindCurrencySymbole();
        }
        else
        {
            ///Response.Redirect("~/login.aspx");
            Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
            Session.Abandon();
            if (l != null)
            {
                Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
            }
            else
            {
                Response.Redirect(SiteRootPath + "login/english");
            }
        }

        int intPackageID = 0;
        if (hdnPackage.Value == "")
        {
            if (objPackagePricingManager.GetAllPackageName(intCountryID).Count > 0)
            {
                hdnPackage.Value = objPackagePricingManager.GetAllPackageName(intCountryID).FirstOrDefault().Id.ToString();
                intPackageID = Convert.ToInt32(hdnPackage.Value);
                divDeactiePackage.Visible = false;
                divDeactiePackageStandard.Visible = true;
                divpackages.Visible = true;
                divpackagesNot.Visible = false;
                divNext.Visible = true;
            }
            else
            {
                divpackages.Visible = false;
                divpackagesNot.Visible = true;
                divNext.Visible = false;
                return;
            }

        }
        intPackageID = Convert.ToInt32(hdnPackage.Value);

        if (intPackageID == objPackagePricingManager.GetAllPackageName(intCountryID).FirstOrDefault().Id)
        {
            divDeactiePackage.Visible = false;
            divDeactiePackageStandard.Visible = true;

        }
        else
        {
            divDeactiePackage.Visible = true;
            divDeactiePackageStandard.Visible = false;
        }

        var PackageStatus = objPackagePricingManager.GetAllPackagePrice(intHotelID).Find(a => a.PackageId == intPackageID);

        if (PackageStatus != null)
        {
            if (ObjHotelinfo.GetHotelGoOnline(intHotelID).Count > 0)
            {
                //This condition has been added when operator comes and change package prices
                if (Convert.ToString(Session["Operator"]) == "1")
                {
                    BindPackagePriceEdit(intPackageID);
                    divSaveCancelPackage.Visible = true;
                    divInfoTab.Visible = true;
                    chkActivePackage.Enabled = true;
                    divViewPackagePrice.Visible = false;
                    divViewPackagePriceEdit.Visible = true;
                    lnkCancelPackagePrice.Enabled = false;

                }
                //This condition has been added when operator comes and change package prices
                else
                {
                    BindPackagePrice(intPackageID);
                    divSaveCancelPackage.Visible = false;
                    divInfoTab.Visible = false;
                    chkActivePackage.Enabled = false;
                    divViewPackagePrice.Visible = true;
                    divViewPackagePriceEdit.Visible = false;
                    lnkCancelPackagePrice.Enabled = true;
                }
            }
            else
            {
                BindPackagePriceEdit(intPackageID);
                divSaveCancelPackage.Visible = true;
                divInfoTab.Visible = true;
                chkActivePackage.Enabled = true;
                divViewPackagePrice.Visible = false;
                divViewPackagePriceEdit.Visible = true;
                lnkCancelPackagePrice.Enabled = false;

            }

        }
        else
        {
            chkActivePackage.Enabled = true;
            divViewPackagePrice.Visible = false;
            divInfoTab.Visible = true;
            divViewPackagePriceEdit.Visible = true;
            BindPackagePriceEdit(intPackageID);
            divSaveCancelPackage.Visible = true;
        }

    }

    /// <summary>
    /// function to bind equipmentItems with listview.
    /// </summary>
    void BindEquipmentList()
    {

        if (objPackagePricingManager.IsExistEquipmentItems(intHotelID))
        {
            EquipmentListView();
            EquipmentListViewEdit();
            lstViewEquipmentPriceEdit.Visible = false;
            lnkModifyEqipmentPrice.Visible = true;
            divSaveCancelEquipment.Visible = false;
            lnkcancelEqipmentPrice.Enabled = true;
            hdnWithOutChangeEquipmentItem.Value = "1";
            if (ViewState["CurrencySign"] != null)
            {
                if ((Label)lstViewEquipmentPrice.FindControl("lblCSHalf") != null && (Label)lstViewEquipmentPriceEdit.FindControl("lblCSHalf") != null)
                {
                    ((Label)lstViewEquipmentPrice.FindControl("lblCSHalf")).Text = "(" + ViewState["CurrencySign"].ToString() + ")";
                    ((Label)lstViewEquipmentPriceEdit.FindControl("lblCSHalf")).Text = "(" + ViewState["CurrencySign"].ToString() + ")";
                }

                if ((Label)lstViewEquipmentPrice.FindControl("lblCSFull") != null && (Label)lstViewEquipmentPriceEdit.FindControl("lblCSFull") != null)
                {
                    ((Label)lstViewEquipmentPrice.FindControl("lblCSFull")).Text = "(" + ViewState["CurrencySign"].ToString() + ")";
                    ((Label)lstViewEquipmentPriceEdit.FindControl("lblCSFull")).Text = "(" + ViewState["CurrencySign"].ToString() + ")";
                }
            }
        }
        else
        {
            EquipmentListViewEdit();
            lnkModifyEqipmentPrice.Visible = false;
            lnkcancelEqipmentPrice.Enabled = false;
            if (ViewState["CurrencySign"] != null)
            {
                if ((Label)lstViewEquipmentPriceEdit.FindControl("lblCSHalf") != null)
                {

                    ((Label)lstViewEquipmentPriceEdit.FindControl("lblCSHalf")).Text = "(" + ViewState["CurrencySign"].ToString() + ")";
                }

                if ((Label)lstViewEquipmentPriceEdit.FindControl("lblCSFull") != null)
                {
                    ((Label)lstViewEquipmentPriceEdit.FindControl("lblCSFull")).Text = "(" + ViewState["CurrencySign"].ToString() + ")";
                }
            }
        }

    }

    /// <summary>
    ///function to bind foodbeverages items with listview.
    /// </summary>
    void BindFoodBeveragesList()
    {

        if (objPackagePricingManager.IsExistFoodBeveragesItems(intHotelID))
        {
            FoodBeveragesListView();
            FoodBeveragesListViewEdit();
            lstViewFoodBeveragesPriceEdit.Visible = false;
            lnkModifyBeverPrice.Visible = true;
            divSaveCancelFoodBeverage.Visible = false;
            lnkcancelFoodBeveragesPrice.Enabled = true;
            hdnWithOutChangeFoodBeverages.Value = "1";
            if (ViewState["CurrencySign"] != null)
            {
                if ((Label)lstViewFoodBeveragesPrice.FindControl("lblCSHalf") != null && (Label)lstViewFoodBeveragesPriceEdit.FindControl("lblCSHalf") != null)
                {
                    ((Label)lstViewFoodBeveragesPrice.FindControl("lblCSHalf")).Text = "(" + ViewState["CurrencySign"].ToString() + ")";
                    ((Label)lstViewFoodBeveragesPriceEdit.FindControl("lblCSHalf")).Text = "(" + ViewState["CurrencySign"].ToString() + ")";
                }

                if ((Label)lstViewFoodBeveragesPrice.FindControl("lblCSFull") != null && (Label)lstViewFoodBeveragesPriceEdit.FindControl("lblCSFull") != null)
                {
                    ((Label)lstViewFoodBeveragesPrice.FindControl("lblCSFull")).Text = "(" + ViewState["CurrencySign"].ToString() + ")";
                    ((Label)lstViewFoodBeveragesPriceEdit.FindControl("lblCSFull")).Text = "(" + ViewState["CurrencySign"].ToString() + ")";
                }
            }
        }
        else
        {
            FoodBeveragesListViewEdit();
            lnkModifyBeverPrice.Visible = false;
            lnkcancelFoodBeveragesPrice.Enabled = false;
            if (ViewState["CurrencySign"] != null)
            {
                if ((Label)lstViewFoodBeveragesPriceEdit.FindControl("lblCSHalf") != null)
                {
                    ((Label)lstViewFoodBeveragesPriceEdit.FindControl("lblCSHalf")).Text = "(" + ViewState["CurrencySign"].ToString() + ")";
                }

                if ((Label)lstViewFoodBeveragesPriceEdit.FindControl("lblCSFull") != null)
                {
                    ((Label)lstViewFoodBeveragesPriceEdit.FindControl("lblCSFull")).Text = "(" + ViewState["CurrencySign"].ToString() + ")";
                }
            }
        }

    }

    /// <summary>
    /// function to bind meetingroom price with listview.
    /// </summary>
    void BindMeetingRoomPriceList()
    {
        if (objPackagePricingManager.IsExistMeetingRoomPrice(intHotelID))
        {
            BindMeetingRoomConfig();
            BindMeetingRoomConfigEdit();
            lstViewMeetingRoomConfigEdit.Visible = false;
            lstViewMeetingRoomConfig.Visible = true;
            lnkModifyMeetingRoomPrice.Visible = true;
            divSaveCancelMeetingRoom.Visible = false;
            lnkcancelMeetingRoomsPrice.Enabled = true;
            hdnWithOutChangeMeetingroom.Value = "1";
            if (ViewState["CurrencySign"] != null)
            {
                if ((Label)lstViewMeetingRoomConfig.FindControl("lblCSHalf") != null && (Label)lstViewMeetingRoomConfigEdit.FindControl("lblCSHalf") != null)
                {
                    ((Label)lstViewMeetingRoomConfig.FindControl("lblCSHalf")).Text = "(" + ViewState["CurrencySign"].ToString() + ")";
                    ((Label)lstViewMeetingRoomConfigEdit.FindControl("lblCSHalf")).Text = "(" + ViewState["CurrencySign"].ToString() + ")";
                }

                if ((Label)lstViewMeetingRoomConfig.FindControl("lblCSFull") != null && (Label)lstViewMeetingRoomConfigEdit.FindControl("lblCSFull") != null)
                {
                    ((Label)lstViewMeetingRoomConfig.FindControl("lblCSFull")).Text = "(" + ViewState["CurrencySign"].ToString() + ")";
                    ((Label)lstViewMeetingRoomConfigEdit.FindControl("lblCSFull")).Text = "(" + ViewState["CurrencySign"].ToString() + ")";
                }
            }

        }
        else
        {
            BindMeetingRoomConfigEdit();
            lstViewMeetingRoomConfigEdit.Visible = true;
            lstViewMeetingRoomConfig.Visible = false;
            lnkModifyMeetingRoomPrice.Visible = false;
            lnkcancelMeetingRoomsPrice.Enabled = false;
            if (ViewState["CurrencySign"] != null)
            {
                if ((Label)lstViewMeetingRoomConfigEdit.FindControl("lblCSHalf") != null)
                {
                    ((Label)lstViewMeetingRoomConfigEdit.FindControl("lblCSHalf")).Text = "(" + ViewState["CurrencySign"].ToString() + ")";
                }

                if ((Label)lstViewMeetingRoomConfigEdit.FindControl("lblCSFull") != null)
                {
                    ((Label)lstViewMeetingRoomConfigEdit.FindControl("lblCSFull")).Text = "(" + ViewState["CurrencySign"].ToString() + ")";
                }
            }
        }

    }


    /// <summary>
    /// function to bind equipmentItems with listview.
    /// </summary>
    void BindOthersList()
    {

        if (objPackagePricingManager.IsExistOthersItems(intHotelID))
        {
            OthersListView();
            OthersListViewEdit();
            lstViewOthersPriceEdit.Visible = false;
            lstViewOthersPrice.Visible = true;
            lnkModifyOthersPrice.Visible = true;
            divSaveCancelOthers.Visible = false;
            lnkcancelOthersPrice.Enabled = true;
            hdnWithOutChangeOthersItem.Value = "1";
            if (ViewState["CurrencySign"] != null)
            {
                if ((Label)lstViewOthersPrice.FindControl("lblCSHalf") != null && (Label)lstViewOthersPriceEdit.FindControl("lblCSHalf") != null)
                {
                    ((Label)lstViewOthersPrice.FindControl("lblCSHalf")).Text = "(" + ViewState["CurrencySign"].ToString() + ")";
                    ((Label)lstViewOthersPriceEdit.FindControl("lblCSHalf")).Text = "(" + ViewState["CurrencySign"].ToString() + ")";
                }

                if ((Label)lstViewOthersPrice.FindControl("lblCSFull") != null && (Label)lstViewOthersPriceEdit.FindControl("lblCSFull") != null)
                {
                    ((Label)lstViewOthersPrice.FindControl("lblCSFull")).Text = "(" + ViewState["CurrencySign"].ToString() + ")";
                    ((Label)lstViewOthersPriceEdit.FindControl("lblCSFull")).Text = "(" + ViewState["CurrencySign"].ToString() + ")";
                }
            }
        }
        else
        {
            OthersListViewEdit();
            lnkModifyOthersPrice.Visible = false;
            lnkcancelOthersPrice.Enabled = false;
            if (ViewState["CurrencySign"] != null)
            {
                if ((Label)lstViewOthersPriceEdit.FindControl("lblCSHalf") != null)
                {
                    ((Label)lstViewOthersPriceEdit.FindControl("lblCSHalf")).Text = "(" + ViewState["CurrencySign"].ToString() + ")";
                }

                if ((Label)lstViewOthersPriceEdit.FindControl("lblCSFull") != null)
                {
                    ((Label)lstViewOthersPriceEdit.FindControl("lblCSFull")).Text = "(" + ViewState["CurrencySign"].ToString() + ")";
                }
            }
        }

    }
    /// <summary>
    /// function to create package tab.
    /// </summary>
    public void CreateDynamicPackageTab()
    {
        long intCountryID = 0;
        if (Session["CurrentHotelID"] != null)
        {
            intCountryID = objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).CountryId;
        }
        else
        {
            //Response.Redirect("~/login.aspx");
            Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
            Session.Abandon();
            if (l != null)
            {
                Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
            }
            else
            {
                Response.Redirect(SiteRootPath + "login/english");
            }
        }
        string strTitle = string.Empty;
        TList<PackageMaster> lstPackageMaster = objPackagePricingManager.GetAllPackageName(intCountryID);
        if (lstPackageMaster.Count > 0)
        {
            if (hdnPackage.Value != "")
            {
                int intPackageId = Convert.ToInt32(hdnPackage.Value);
                ObjSBPackage.Append(@"<ul style="" display:inline;"">");
                for (int i = 0; i <= lstPackageMaster.Count - 1; i++)
                {
                    int intPackageMasterID = Convert.ToInt32(lstPackageMaster[i].Id);
                    var varPackagePrice = objPackagePricingManager.GetAllPackagePrice(intHotelID).Find(a => a.PackageId == lstPackageMaster[i].Id);
                    long englishLanguageID = objPackagePricingManager.EglishLanguageID();
                    var getRow = objPackagePricingManager.GetPackageMasterDescription(intPackageMasterID).Find(a => a.LanguageId == englishLanguageID);
                    if (getRow != null)
                    {
                        strTitle = getRow.Description;
                    }
                    else
                    {
                        strTitle = "Package";
                    }
                    //string strPackageName = lstPackageMaster[i].PackageName;
                    //if (strPackageName == "Standard")
                    //{
                    //    strTitle = "Package includes main meeting room rental, note pads, pencils, flipchart, mineral water, morning coffee, Sandwich buffet with salads (incl. non-alcoholic beverages) and afternoon coffee break (tea, coffee, soft drinks and pastries).For half day packages, either morning or afternoon break is included. The Standard package is available as of 10 meeting delegates.";

                    //}
                    //else if (strPackageName == "Favourite")
                    //{
                    //    strTitle = "Package includes main meeting room rental, note pads, pencils, flipchart, mineral water, morning coffee, 2-courses menu (incl. non-alcoholic beverages) and afternoon coffee break (tea, coffee, soft drinks and pastries).For half day packages, either morning or afternoon break is included. The Favourite package is available as of 10 meeting delegates.";
                    //}
                    //else if (strPackageName == "Elegant")
                    //{
                    //    strTitle = "Package includes main meeting room rental, note pads, pencils, flipchart, mineral water, morning coffee, hot and cold buffet (incl. non-alcoholic beverages) and afternoon coffee break (tea, coffee, soft drinks and pastries).For half day packages, either morning or afternoon break is included. The Elegant package is available as of 20 meeting delegates.";
                    //}
                    //else
                    //{
                    //    strTitle = "";

                    //}

                    if (varPackagePrice != null)
                    {

                        ObjSBPackage.Append(@"<li class='packagetabblue' Title='" + strTitle + "' id='" + lstPackageMaster[i].Id + "' OnClick='SelectPackage(" + lstPackageMaster[i].Id + ")'><a href='#' >" + lstPackageMaster[i].PackageName + "</a>&nbsp;<img alt='' src='../Images/help-2.png' align='absmiddle'/></li>");
                    }
                    else
                    {
                        ObjSBPackage.Append(@"<li class='packagetabred' Title='" + strTitle + "' id='" + lstPackageMaster[i].Id + "' OnClick='SelectPackage(" + lstPackageMaster[i].Id + ")'><a href='#' >" + lstPackageMaster[i].PackageName + "</a>&nbsp;<img alt='' src='../Images/help-2.png' align='absmiddle' /></li>");

                    }

                }
                ObjSBPackage.Append(@"</ul>");

            }
            else
            {
                ObjSBPackage.Append(@"<ul style="" display:inline;"">");
                for (int i = 0; i <= lstPackageMaster.Count - 1; i++)
                {
                    string strPackageName = lstPackageMaster[i].PackageName;
                    if (strPackageName == "Standard")
                    {
                        strTitle = "Package includes main meeting room rental, note pads, pencils, flipchart, mineral water, morning coffee, Sandwich buffet with salads (incl. non-alcoholic beverages) and afternoon coffee break (tea, coffee, soft drinks and pastries).For half day packages, either morning or afternoon break is included. The Standard package is available as of 10 meeting delegates.";

                    }
                    else if (strPackageName == "Favourite")
                    {
                        strTitle = "Package includes main meeting room rental, note pads, pencils, flipchart, mineral water, morning coffee, 2-courses menu (incl. non-alcoholic beverages) and afternoon coffee break (tea, coffee, soft drinks and pastries).For half day packages, either morning or afternoon break is included. The Favourite package is available as of 10 meeting delegates.";
                    }
                    else if (strPackageName == "Elegant")
                    {
                        strTitle = "Package includes main meeting room rental, note pads, pencils, flipchart, mineral water, morning coffee, hot and cold buffet (incl. non-alcoholic beverages) and afternoon coffee break (tea, coffee, soft drinks and pastries).For half day packages, either morning or afternoon break is included. The Elegant package is available as of 20 meeting delegates.";
                    }
                    else
                    {
                        strTitle = "";

                    }


                    ObjSBPackage.Append(@"<li class='packagetabred' Title='" + strTitle + "' id='" + lstPackageMaster[i].Id + "' OnClick='SelectPackage(" + lstPackageMaster[i].Id + ")'><a href='#' >" + lstPackageMaster[i].PackageName + "</a>&nbsp;<img alt='' src='../Images/help-2.png' align='absmiddle' /></li>");

                }

                ObjSBPackage.Append(@"</ul>");

            }
            litPackageTab.Text = "";
            litPackageTab.Text = ObjSBPackage.ToString();
        }
    }

    /// <summary>
    /// function to bind package price with listview
    /// </summary>
    /// <param name="packageId"></param>
    void BindPackagePrice(int packageId)
    {
        lstViewPackagePrice.DataSource = objPackagePricingManager.GetPackageItemsByPackageID(packageId);
        lstViewPackagePrice.DataBind();
    }

    /// <summary>
    /// function to bind package price  with listview. 
    /// </summary>
    /// <param name="packageId"></param>
    void BindPackagePriceEdit(int packageId)
    {
        if (objPackagePricingManager.GetPackageItemsByPackageID(packageId).Count == 0)
        {
            divSaveCancelPackage.Visible = false;
        }
        else
        {
            divSaveCancelPackage.Visible = false;
        }

        lstViewPackagePriceEdit.DataSource = objPackagePricingManager.GetPackageItemsByPackageID(packageId);
        lstViewPackagePriceEdit.DataBind();

    }

    /// <summary>
    /// function to bind meeting room price with listview 
    /// </summary>
    void BindMeetingRoomConfig()
    {
        var getVat = objPackagePricingManager.GetMeetingRoomVat(intHotelID).FirstOrDefault();
        if (getVat != null)
        {
            decimalMeetingRoomVat = getVat.Vat;
        }
        else
        {
            decimalMeetingRoomVat = 00;
        }
        lstViewMeetingRoomConfig.DataSource = ObjMeetingRoomDesc.GetAllMeetingRoom(intHotelID);
        lstViewMeetingRoomConfig.DataBind();
    }

    /// <summary>
    /// function to bind meeting room price with listview. 
    /// </summary>
    void BindMeetingRoomConfigEdit()
    {
        var getVat = objPackagePricingManager.GetMeetingRoomVat(intHotelID).FirstOrDefault();
        if (getVat != null)
        {
            decimalMeetingRoomVat = Convert.ToInt32(getVat.Vat);
        }
        else
        {
            decimalMeetingRoomVat = 00;
        }
        lstViewMeetingRoomConfigEdit.DataSource = ObjMeetingRoomDesc.GetAllMeetingRoom(intHotelID);
        lstViewMeetingRoomConfigEdit.DataBind();
        if (ObjMeetingRoomDesc.GetAllMeetingRoom(intHotelID).Count == 0)
        {
            divSaveCancelMeetingRoom.Visible = false;

        }
        else
        {
            divSaveCancelMeetingRoom.Visible = true;
            hdnWithOutChangeMeetingroom.Value = "0";
        }

    }

    /// <summary>
    /// function to bind EquipmentItems with listview for view.
    /// </summary>
    void EquipmentListView()
    {
        lstViewEquipmentPrice.DataSource = objPackagePricingManager.GetAllEquipmentItems(intHotelID);//objPackagePricingManager.OnlineEquipmentItems(intHotelID);
        lstViewEquipmentPrice.DataBind();
    }

    /// <summary>
    /// function to bind OthersItem with listview for view.
    /// </summary>
    void OthersListView()
    {
        lstViewOthersPrice.DataSource = objPackagePricingManager.GetAllOtherItems(intHotelID);//objPackagePricingManager.OnlineEquipmentItems(intHotelID);
        lstViewOthersPrice.DataBind();
    }

    /// <summary>
    /// Bind others Items with listview for edit.
    /// </summary>
    void OthersListViewEdit()
    {
        lnkModifyOthersPrice.Visible = false;
        TList<PackageItems> objOthersItems = objPackagePricingManager.GetAllOtherItems(intHotelID);
        lstViewOthersPriceEdit.DataSource = objOthersItems;
        lstViewOthersPriceEdit.DataBind();
        if (objOthersItems.Count == 0)
        {
            divSaveCancelOthers.Visible = false;

        }
        else
        {
            lstViewOthersPriceEdit.Visible = true;
            lstViewOthersPrice.Visible = false;
            divSaveCancelOthers.Visible = true;
            hdnWithOutChangeOthersItem.Value = "0";
        }
    }

    /// <summary>
    /// Bind Equipment Items with listview for edit.
    /// </summary>
    void EquipmentListViewEdit()
    {
        lnkModifyEqipmentPrice.Visible = false;
        lstViewEquipmentPriceEdit.DataSource = objPackagePricingManager.GetAllEquipmentItems(intHotelID);
        lstViewEquipmentPriceEdit.DataBind();
        if (objPackagePricingManager.GetAllEquipmentItems(intHotelID).Count == 0)
        {
            divSaveCancelEquipment.Visible = false;

        }
        else
        {
            divSaveCancelEquipment.Visible = true;
            hdnWithOutChangeEquipmentItem.Value = "0";
        }
    }

    /// <summary>
    /// Bind foodBeveragesItems with listview for view.
    /// </summary>
    void FoodBeveragesListView()
    {
        lstViewFoodBeveragesPrice.DataSource = objPackagePricingManager.GetAllFoodBeveragesItems(intHotelID);//objPackagePricingManager.OnlineFoodBeveragesItems(intHotelID);
        lstViewFoodBeveragesPrice.DataBind();
    }

    /// <summary>
    /// bind foodbeveragesItems with listview for Edit.
    /// </summary>
    void FoodBeveragesListViewEdit()
    {
        lnkModifyBeverPrice.Visible = false;
        lstViewFoodBeveragesPriceEdit.DataSource = objPackagePricingManager.GetAllFoodBeveragesItems(intHotelID);
        lstViewFoodBeveragesPriceEdit.DataBind();
        if (objPackagePricingManager.GetAllFoodBeveragesItems(intHotelID).Count == 0)
        {
            divSaveCancelFoodBeverage.Visible = false;

        }
        else
        {
            divSaveCancelFoodBeverage.Visible = true;
            hdnWithOutChangeFoodBeverages.Value = "0";
        }
    }

    #endregion

    #region Events

    /// <summary>
    /// this event will be execute on click of package tab.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GetPriceByPackage_Click(object sender, EventArgs e)
    {
        divmsgpackageByServer.Style.Add("display", "none");
        BindPackageList();
    }

    /// <summary>
    /// This event used for bound meetingroom price.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lstViewMeetingRoomConfig_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            ListViewDataItem currentItem = (ListViewDataItem)e.Item;
            DataKey currentDataKey = this.lstViewMeetingRoomConfig.DataKeys[currentItem.DataItemIndex];
            int intMeetingRoomID = Convert.ToInt32(currentDataKey.Value);
            Label lblMeetingRoomVat = (Label)e.Item.FindControl("lblMeetingRoomVat");
            lblMeetingRoomVat.Text = decimalMeetingRoomVat.ToString();
            MeetingRoom room = e.Item.DataItem as MeetingRoom;
            if (room != null)
            {
                Label lblHalfDayPrice = (Label)e.Item.FindControl("lblHalfDayPrice");
                Label lblFullDayPrice = (Label)e.Item.FindControl("lblFullDayPrice");
                if (room.HalfdayPrice != 0)
                {
                    lblHalfDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(room.HalfdayPrice), 2));// String.Format("{0:#,###}", room.HalfdayPrice);
                }
                else
                {
                    lblHalfDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(room.HalfdayPrice), 2));// Convert.ToInt32(room.HalfdayPrice).ToString();
                }

                if (room.FulldayPrice != 0)
                {
                    lblFullDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(room.FulldayPrice), 2)); //String.Format("{0:#,###}", room.FulldayPrice);
                }
                else
                {
                    lblFullDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(room.FulldayPrice), 2));// Convert.ToInt32(room.FulldayPrice).ToString();
                }

                Image imgIsOnline = (Image)e.Item.FindControl("imgIsOnline");
                if (room.IsOnline == true)
                {
                    imgIsOnline.ImageUrl = "../images/tick.png";
                }
                else
                {
                    imgIsOnline.ImageUrl = "../images/tick.png";
                    imgIsOnline.Visible = false;
                }
            }

            TList<MeetingRoomConfig> lst = ObjMeetingRoomConfigManager.GetConfigByMeetingRoomID(intMeetingRoomID);

            int intMinEnumValue = ObjMeetingRoomConfigManager.GetMinMeetingRoomShapeID();
            int intMaxEnumValue = ObjMeetingRoomConfigManager.GetMaxMeetingRoomShapeID();
            int intIndex;
            for (intIndex = intMinEnumValue; intIndex <= intMaxEnumValue; intIndex++)
            {
                var getRow = lst.Find(a => a.RoomShapeId == intIndex);
                if (getRow != null)
                {

                    if (getRow.RoomShapeId == 1)
                    {
                        Label lblTheatreMin = (Label)e.Item.FindControl("lblTheatreMin");
                        Label lblTheatreMax = (Label)e.Item.FindControl("lblTheatreMax");
                        lblTheatreMin.Text = getRow.MinCapacity.ToString();
                        lblTheatreMax.Text = getRow.MaxCapicity.ToString();
                    }
                    else if (getRow.RoomShapeId == 2)
                    {
                        Label lblSchoolMin = (Label)e.Item.FindControl("lblSchoolMin");
                        Label lblSchoolMax = (Label)e.Item.FindControl("lblSchoolMax");
                        lblSchoolMin.Text = getRow.MinCapacity.ToString();
                        lblSchoolMax.Text = getRow.MaxCapicity.ToString();
                    }
                    else if (getRow.RoomShapeId == 3)
                    {
                        Label lblUShapeMin = (Label)e.Item.FindControl("lblUShapeMin");
                        Label lblUShapeMax = (Label)e.Item.FindControl("lblUShapeMax");
                        lblUShapeMin.Text = getRow.MinCapacity.ToString();
                        lblUShapeMax.Text = getRow.MaxCapicity.ToString();
                    }
                    else if (getRow.RoomShapeId == 4)
                    {
                        Label lblBedroomMin = (Label)e.Item.FindControl("lblBedroomMin");
                        Label lblBedroomMax = (Label)e.Item.FindControl("lblBedroomMax");
                        lblBedroomMin.Text = getRow.MinCapacity.ToString();
                        lblBedroomMax.Text = getRow.MaxCapicity.ToString();
                    }
                    else if (getRow.RoomShapeId == 5)
                    {
                        Label lblCocktailMin = (Label)e.Item.FindControl("lblCocktailMin");
                        Label lblCocktailMax = (Label)e.Item.FindControl("lblCocktailMax");
                        lblCocktailMin.Text = getRow.MinCapacity.ToString();
                        lblCocktailMax.Text = getRow.MaxCapicity.ToString();
                    }
                }

            }

        }

    }

    /// <summary>
    /// This function used for bound meeting room price for edit.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lstViewMeetingRoomConfigEdit_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            ListViewDataItem currentItem = (ListViewDataItem)e.Item;
            DataKey currentDataKey = this.lstViewMeetingRoomConfigEdit.DataKeys[currentItem.DataItemIndex];

            //LinkButton SiteLink = (LinkButton)currentItem.FindControl("SiteLink");

            int intMeetingRoomID = Convert.ToInt32(currentDataKey.Value);
            Label lblMeetingRoomVat = (Label)e.Item.FindControl("lblMeetingRoomVat");
            lblMeetingRoomVat.Text = decimalMeetingRoomVat.ToString();
            MeetingRoom room = e.Item.DataItem as MeetingRoom;
            if (room != null)
            {
                TextBox txtHalfDayPrice = (TextBox)e.Item.FindControl("txtHalfDayPrice");
                TextBox txtFullDayPrice = (TextBox)e.Item.FindControl("txtFullDayPrice");
                if (room.HalfdayPrice != 0)
                {
                    txtHalfDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(room.HalfdayPrice), 2));// String.Format("{0:#,###}", room.HalfdayPrice);
                }
                else
                {
                    txtHalfDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(room.HalfdayPrice), 2));// Convert.ToInt32(room.HalfdayPrice).ToString();
                }

                if (room.FulldayPrice != 0)
                {
                    txtFullDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(room.FulldayPrice), 2));// String.Format("{0:#,###}", room.FulldayPrice);
                }
                else
                {
                    txtFullDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(room.FulldayPrice), 2));// Convert.ToInt32(room.FulldayPrice).ToString();
                }
                CheckBox chkIsOnline = (CheckBox)e.Item.FindControl("chkIsOnline");
                if (room.IsOnline == true)
                {
                    chkIsOnline.Checked = true;
                }
                else
                {
                    chkIsOnline.Checked = false;
                }

            }
            TList<MeetingRoomConfig> lst = ObjMeetingRoomConfigManager.GetConfigByMeetingRoomID(intMeetingRoomID);

            int intMinEnumValue = ObjMeetingRoomConfigManager.GetMinMeetingRoomShapeID();
            int intMaxEnumValue = ObjMeetingRoomConfigManager.GetMaxMeetingRoomShapeID();
            int intIndex;
            for (intIndex = intMinEnumValue; intIndex <= intMaxEnumValue; intIndex++)
            {
                var getRow = lst.Find(a => a.RoomShapeId == intIndex);
                if (getRow != null)
                {

                    if (getRow.RoomShapeId == 1)
                    {
                        Label lblTheatreMin = (Label)e.Item.FindControl("lblTheatreMin");
                        Label lblTheatreMax = (Label)e.Item.FindControl("lblTheatreMax");
                        lblTheatreMin.Text = getRow.MinCapacity.ToString();
                        lblTheatreMax.Text = getRow.MaxCapicity.ToString();


                    }
                    else if (getRow.RoomShapeId == 2)
                    {

                        Label lblSchoolMin = (Label)e.Item.FindControl("lblSchoolMin");
                        Label lblSchoolMax = (Label)e.Item.FindControl("lblSchoolMax");
                        lblSchoolMin.Text = getRow.MinCapacity.ToString();
                        lblSchoolMax.Text = getRow.MaxCapicity.ToString();


                    }
                    else if (getRow.RoomShapeId == 3)
                    {

                        Label lblUShapeMin = (Label)e.Item.FindControl("lblUShapeMin");
                        Label lblUShapeMax = (Label)e.Item.FindControl("lblUShapeMax");
                        lblUShapeMin.Text = getRow.MinCapacity.ToString();
                        lblUShapeMax.Text = getRow.MaxCapicity.ToString();


                    }
                    else if (getRow.RoomShapeId == 4)
                    {

                        Label lblBedroomMin = (Label)e.Item.FindControl("lblBedroomMin");
                        Label lblBedroomMax = (Label)e.Item.FindControl("lblBedroomMax");
                        lblBedroomMin.Text = getRow.MinCapacity.ToString();
                        lblBedroomMax.Text = getRow.MaxCapicity.ToString();

                    }
                    else if (getRow.RoomShapeId == 5)
                    {

                        Label lblCocktailMin = (Label)e.Item.FindControl("lblCocktailMin");
                        Label lblCocktailMax = (Label)e.Item.FindControl("lblCocktailMax");
                        lblCocktailMin.Text = getRow.MinCapacity.ToString();
                        lblCocktailMax.Text = getRow.MaxCapicity.ToString();

                    }
                }


            }

        }

    }

    /// <summary>
    /// This button click event used for save meetingroom price.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkSaveMeetingRoomPrice_Click(object sender, EventArgs e)
    {
        try
        {
            decimal? intHalfDay = null;
            decimal? intFullDay = null;
            foreach (ListViewItem lstItem in lstViewMeetingRoomConfigEdit.Items)
            {
                HiddenField hdnMeetingRoomID = (HiddenField)lstItem.FindControl("hdnMeetingRoomID");
                TextBox txtHalfDayPrice = (TextBox)lstItem.FindControl("txtHalfDayPrice");
                TextBox txtFullDayPrice = (TextBox)lstItem.FindControl("txtFullDayPrice");
                CheckBox chkIsOnline = (CheckBox)lstItem.FindControl("chkIsOnline");
                int intID = Convert.ToInt32(hdnMeetingRoomID.Value);
                if (txtHalfDayPrice.Text != "")
                {
                    intHalfDay = Convert.ToDecimal(txtHalfDayPrice.Text);
                }
                else
                {
                    intHalfDay = null;
                }
                if (txtFullDayPrice.Text != "")
                {
                    intFullDay = Convert.ToDecimal(txtFullDayPrice.Text);
                }
                else
                {
                    intFullDay = null;
                }

                bool boolIsOnline = chkIsOnline.Checked;
                string msg = objPackagePricingManager.UpdateMeetingRoomPrice(intID, intHalfDay, intFullDay, boolIsOnline);
            }
            BindMeetingRoomPriceList();
            hdnWithOutChangeMeetingroom.Value = "1";
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// this event used for bound food beveragesItem with listview.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lstViewFoodBeveragesPrice_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            ListViewDataItem currentItem = (ListViewDataItem)e.Item;
            DataKey currentDataKey = this.lstViewFoodBeveragesPrice.DataKeys[currentItem.DataItemIndex];
            int intItemID = Convert.ToInt32(currentDataKey.Value);
            TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(intHotelID);

            ////////////////////// Fill Package Price///////////////////////////////

            var getRow = lstPackagePrice.Find(a => a.ItemId == intItemID && a.PackageId == null);
            if (getRow != null)
            {

                Label lblHalfDayPrice = (Label)e.Item.FindControl("lblHalfDayPrice");
                Label lblFullDayPrice = (Label)e.Item.FindControl("lblFullDayPrice");

                if (getRow.HalfdayPrice != 0)
                {
                    lblHalfDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(getRow.HalfdayPrice), 2));// String.Format("{0:#,###}", getRow.HalfdayPrice);
                }
                else
                {
                    lblHalfDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(getRow.HalfdayPrice), 2));// Convert.ToInt32(getRow.HalfdayPrice).ToString();
                }

                if (getRow.FulldayPrice != 0)
                {
                    lblFullDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(getRow.FulldayPrice), 2));// String.Format("{0:#,###}", getRow.FulldayPrice);
                }
                else
                {
                    lblFullDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(getRow.FulldayPrice), 2)); //Convert.ToInt32(getRow.FulldayPrice).ToString();
                }

                Image imgIsOnline = (Image)e.Item.FindControl("imgIsOnline");
                if (getRow.IsOnline == true)
                {
                    imgIsOnline.ImageUrl = "../images/tick.png";

                }
                else
                {
                    imgIsOnline.ImageUrl = "../images/tick.png";
                    imgIsOnline.Visible = false;
                }

            }


            // ////////////Fill Description ///////////////////////////
            int intLanguageID = Convert.ToInt32(ObjMeetingRoomDesc.GetAllRequiredLanguage().Find(a => a.Name == "English").Id);
            var getDescription = objPackagePricingManager.GetPackageDescriptionByItemID(intItemID).Find(a => a.LanguageId == intLanguageID);
            if (getDescription != null)
            {

                Label lblDesc = (Label)e.Item.FindControl("lblDesc");
                lblDesc.Text = getDescription.ItemDescription.ToString();

            }
            /////////////////////////////////////////////////////////////

        }

    }

    /// <summary>
    /// This event used for bound FoodBeverages Item for edit.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lstViewFoodBeveragesPriceEdit_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {

            ListViewDataItem currentItem = (ListViewDataItem)e.Item;
            DataKey currentDataKey = this.lstViewFoodBeveragesPriceEdit.DataKeys[currentItem.DataItemIndex];
            int intItemID = Convert.ToInt32(currentDataKey.Value);
            TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(intHotelID);
            HiddenField hdnItemID = (HiddenField)e.Item.FindControl("hdnItemID");
            hdnItemID.Value = intItemID.ToString();

            ////////////////////// Fill Package Price///////////////////////////////
            //int intIndex;
            //for (intIndex = 0; intIndex <= lstPackagePrice.Count - 1; intIndex++)
            //{
            var getRow = lstPackagePrice.Find(a => a.ItemId == intItemID && a.PackageId == null);
            if (getRow != null)
            {

                TextBox txtHalfDayPrice = (TextBox)e.Item.FindControl("txtHalfDayPrice");
                TextBox txtFullDayPrice = (TextBox)e.Item.FindControl("txtFullDayPrice");
                HiddenField hdnPackageByHotelID = (HiddenField)e.Item.FindControl("hdnPackageByHotelID");
                txtHalfDayPrice.Text = String.Format("{0:0.##}", getRow.HalfdayPrice);
                txtFullDayPrice.Text = String.Format("{0:0.##}", getRow.FulldayPrice);
                hdnPackageByHotelID.Value = getRow.Id.ToString();

                CheckBox chkIsOnline = (CheckBox)e.Item.FindControl("chkIsOnline");
                if (getRow.IsOnline == true)
                {
                    chkIsOnline.Checked = true;

                }
                else
                {

                    chkIsOnline.Checked = false;

                }

            }
            // }
            ///////////////////////////////////////////

            // ////////////Fill Description ///////////////////////////
            int intLanguageID = Convert.ToInt32(ObjMeetingRoomDesc.GetAllRequiredLanguage().Find(a => a.Name == "English").Id);
            var getDescription = objPackagePricingManager.GetPackageDescriptionByItemID(intItemID).Find(a => a.LanguageId == intLanguageID);
            if (getDescription != null)
            {

                Label lblDesc = (Label)e.Item.FindControl("lblDesc");
                lblDesc.Text = getDescription.ItemDescription.ToString();

            }
            /////////////////////////////////////////////////////////////

        }
    }

    /// <summary>
    /// This button click event used for save foodbeverages price.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkSaveFoodBeveragesPrice_Click(object sender, EventArgs e)
    {
        try
        {
            foreach (ListViewItem lstItem in lstViewFoodBeveragesPriceEdit.Items)
            {
                decimal? decimalHalfDay = null;
                decimal? decimalFullDay = null;
                //HiddenField hdnPackageByHotelID = (HiddenField)lstItem.FindControl("hdnPackageByHotelID");
                TextBox txtHalfDayPrice = (TextBox)lstItem.FindControl("txtHalfDayPrice");
                TextBox txtFullDayPrice = (TextBox)lstItem.FindControl("txtFullDayPrice");
                CheckBox chkIsOnline = (CheckBox)lstItem.FindControl("chkIsOnline");
                //int intID = Convert.ToInt32(hdnPackageByHotelID.Value);
                if (txtHalfDayPrice.Text != "")
                {
                    decimalHalfDay = Convert.ToDecimal(txtHalfDayPrice.Text);
                }
                if (txtFullDayPrice.Text != "")
                {
                    decimalFullDay = Convert.ToDecimal(txtFullDayPrice.Text);
                }
                bool boolIsOnline = chkIsOnline.Checked;

                HiddenField hdnItemId = (HiddenField)lstItem.FindControl("hdnItemID");
                int ItemId = Convert.ToInt32(hdnItemId.Value);
                PackageByHotel pricingrow = objPackagePricingManager.IsPriceExists(ItemId, intHotelID).FirstOrDefault();
                if (pricingrow != null)
                {
                    PackageByHotel objPackagePrice = objPackagePricingManager.GetPriceByID(Convert.ToInt64(pricingrow.Id));
                    objPackagePrice.IsComplementary = false;
                    objPackagePrice.HalfdayPrice = decimalHalfDay;
                    objPackagePrice.FulldayPrice = decimalFullDay;
                    objPackagePrice.IsOnline = boolIsOnline;
                    string msg = objPackagePricingManager.UpdatePackagrPraice(objPackagePrice);
                }
                else
                {
                    PackageByHotel objPackagePrice = new PackageByHotel();
                    objPackagePrice.PackageId = null;
                    objPackagePrice.ItemId = ItemId;
                    objPackagePrice.IsComplementary = false;
                    objPackagePrice.HalfdayPrice = decimalHalfDay;
                    objPackagePrice.FulldayPrice = decimalFullDay;
                    objPackagePrice.HotelId = intHotelID;
                    objPackagePrice.IsOnline = boolIsOnline;

                    string msg = objPackagePricingManager.AddPackagrPraice(objPackagePrice);
                }

            }
            BindFoodBeveragesList();
            hdnWithOutChangeFoodBeverages.Value = "1";
            lstViewFoodBeveragesPriceEdit.Visible = false;
            lstViewFoodBeveragesPrice.Visible = true;
            divSaveCancelFoodBeverage.Visible = false;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This event used for bound equipmentitem for view.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lstViewEquipmentPrice_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            ListViewDataItem currentItem = (ListViewDataItem)e.Item;
            DataKey currentDataKey = this.lstViewEquipmentPrice.DataKeys[currentItem.DataItemIndex];
            int intItemID = Convert.ToInt32(currentDataKey.Value);
            TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(intHotelID);

            ////////////////////// Fill Package Price///////////////////////////////

            var getRow = lstPackagePrice.Find(a => a.ItemId == intItemID && a.PackageId == null);
            if (getRow != null)
            {

                Label lblHalfDayPrice = (Label)e.Item.FindControl("lblHalfDayPrice");
                Label lblFullDayPrice = (Label)e.Item.FindControl("lblFullDayPrice");

                Image imgIsEmentary = (Image)e.Item.FindControl("imgIsEmentary");

                if (getRow.IsComplementary == true)
                {
                    imgIsEmentary.ImageUrl = "~/images/tick.png";
                    lblHalfDayPrice.Text = "";
                    lblFullDayPrice.Text = "";

                }
                else
                {
                    imgIsEmentary.Visible = false;
                    if (getRow.HalfdayPrice != null)
                    {
                        if (getRow.HalfdayPrice != 0)
                        {
                            lblHalfDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(getRow.HalfdayPrice), 2));// String.Format("{0:#,###}", getRow.HalfdayPrice);
                        }
                        else
                        {
                            lblHalfDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(getRow.HalfdayPrice), 2));// Convert.ToInt32(getRow.HalfdayPrice).ToString();

                        }
                    }
                    else
                    {
                        lblHalfDayPrice.Text = "";
                    }
                    if (getRow.FulldayPrice != null)
                    {
                        if (getRow.FulldayPrice != 0)
                        {
                            lblFullDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(getRow.FulldayPrice), 2)); //String.Format("{0:#,###}", getRow.FulldayPrice);
                        }
                        else
                        {
                            lblFullDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(getRow.FulldayPrice), 2)); //Convert.ToInt32(getRow.FulldayPrice).ToString();
                        }
                    }
                    else
                    {
                        lblFullDayPrice.Text = "";
                    }
                }

                Image imgIsOnline = (Image)e.Item.FindControl("imgIsOnline");
                if (getRow.IsOnline == true)
                {
                    imgIsOnline.ImageUrl = "~/images/tick.png";

                }
                else
                {
                    imgIsOnline.Visible = false;
                }

            }

            // ////////////Fill Description ///////////////////////////
            int intLanguageID = Convert.ToInt32(ObjMeetingRoomDesc.GetAllRequiredLanguage().Find(a => a.Name == "English").Id);
            var getDescription = objPackagePricingManager.GetPackageDescriptionByItemID(intItemID).Find(a => a.LanguageId == intLanguageID);
            if (getDescription != null)
            {

                Label lblDesc = (Label)e.Item.FindControl("lblDesc");
                lblDesc.Text = getDescription.ItemDescription.ToString();

            }
            /////////////////////////////////////////////////////////////
        }

    }

    /// <summary>
    /// This event used for bound equipmentItem for edit.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lstViewEquipmentPriceEdit_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            ListViewDataItem currentItem = (ListViewDataItem)e.Item;
            DataKey currentDataKey = this.lstViewEquipmentPriceEdit.DataKeys[currentItem.DataItemIndex];
            int intItemID = Convert.ToInt32(currentDataKey.Value);
            TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(intHotelID);
            HiddenField hdnItemID = (HiddenField)e.Item.FindControl("hdnItemID");
            hdnItemID.Value = intItemID.ToString();

            ////////////////////// Fill Package Price///////////////////////////////

            var getRow = lstPackagePrice.Find(a => a.ItemId == intItemID && a.PackageId == null);
            if (getRow != null)
            {
                TextBox txtHalfDayPrice = (TextBox)e.Item.FindControl("txtHalfDayPrice");
                TextBox txtFullDayPrice = (TextBox)e.Item.FindControl("txtFullDayPrice");
                HiddenField hdnPackageByHotelID = (HiddenField)e.Item.FindControl("hdnPackageByHotelID");
                hdnPackageByHotelID.Value = getRow.Id.ToString();

                CheckBox chkIsEmentary = (CheckBox)e.Item.FindControl("chkIsEmentary");
                if (getRow.IsComplementary == true)
                {
                    chkIsEmentary.Checked = true;
                    txtHalfDayPrice.Visible = false;
                    txtFullDayPrice.Visible = false;
                }
                else
                {
                    chkIsEmentary.Checked = false;
                    txtHalfDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(getRow.HalfdayPrice), 2));// String.Format("{0:0.##}", getRow.HalfdayPrice);
                    txtFullDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(getRow.FulldayPrice), 2)); //String.Format("{0:0.##}", getRow.FulldayPrice);
                }

                CheckBox chkIsOnline = (CheckBox)e.Item.FindControl("chkIsOnline");
                if (getRow.IsOnline == true)
                {
                    chkIsOnline.Checked = true;
                }
                else
                {
                    chkIsOnline.Checked = false;
                }

            }

            // ////////////Fill Description ///////////////////////////
            int intLanguageID = Convert.ToInt32(ObjMeetingRoomDesc.GetAllRequiredLanguage().Find(a => a.Name == "English").Id);
            var getDescription = objPackagePricingManager.GetPackageDescriptionByItemID(intItemID).Find(a => a.LanguageId == intLanguageID);
            if (getDescription != null)
            {

                Label lblDesc = (Label)e.Item.FindControl("lblDesc");
                lblDesc.Text = getDescription.ItemDescription.ToString();

            }
            /////////////////////////////////////////////////////////////
        }
    }

    /// <summary>
    /// This button click event used for save equipmentItem price.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkSaveEquipmentPrice_Click(object sender, EventArgs e)
    {
        try
        {
            foreach (ListViewItem lstItem in lstViewEquipmentPriceEdit.Items)
            {
                decimal? decimalHalfDay = null;
                decimal? decimalFullDay = null;
                //HiddenField hdnPackageByHotelID = (HiddenField)lstItem.FindControl("hdnPackageByHotelID");
                TextBox txtHalfDayPrice = (TextBox)lstItem.FindControl("txtHalfDayPrice");
                TextBox txtFullDayPrice = (TextBox)lstItem.FindControl("txtFullDayPrice");
                CheckBox chkIsOnline = (CheckBox)lstItem.FindControl("chkIsOnline");
                CheckBox chkIsEmentary = (CheckBox)lstItem.FindControl("chkIsEmentary");
                bool boolIsEmentary = chkIsEmentary.Checked;
                if (boolIsEmentary != true)
                {
                    if (txtHalfDayPrice.Text != "")
                    {
                        if (txtHalfDayPrice.Text != "")
                        {
                            decimalHalfDay = Convert.ToDecimal(txtHalfDayPrice.Text);
                        }
                        if (txtFullDayPrice.Text != "")
                        {
                            decimalFullDay = Convert.ToDecimal(txtFullDayPrice.Text);
                        }
                    }
                }
                else
                {
                    if (txtHalfDayPrice.Text != "")
                    {
                        decimalHalfDay = Convert.ToDecimal(txtHalfDayPrice.Text);
                    }
                    if (txtFullDayPrice.Text != "")
                    {
                        decimalFullDay = Convert.ToDecimal(txtFullDayPrice.Text);
                    }

                }
                //int intID = Convert.ToInt32(hdnPackageByHotelID.Value);
                bool boolIsOnline = chkIsOnline.Checked;
                HiddenField hdnItemId = (HiddenField)lstItem.FindControl("hdnItemID");
                int ItemId = Convert.ToInt32(hdnItemId.Value);
                PackageByHotel pricingrow = objPackagePricingManager.IsPriceExists(ItemId, intHotelID).FirstOrDefault();
                if (pricingrow != null)
                {
                    PackageByHotel objPackagePrice = objPackagePricingManager.GetPriceByID(Convert.ToInt64(pricingrow.Id));
                    objPackagePrice.IsComplementary = boolIsEmentary;
                    objPackagePrice.HalfdayPrice = decimalHalfDay;
                    objPackagePrice.FulldayPrice = decimalFullDay;
                    objPackagePrice.IsOnline = boolIsOnline;
                    string msg = objPackagePricingManager.UpdatePackagrPraice(objPackagePrice);
                }
                else
                {
                    PackageByHotel objPackagePrice = new PackageByHotel();
                    objPackagePrice.PackageId = null;
                    objPackagePrice.ItemId = ItemId;
                    objPackagePrice.IsComplementary = boolIsEmentary;
                    objPackagePrice.HalfdayPrice = decimalHalfDay;
                    objPackagePrice.FulldayPrice = decimalFullDay;
                    objPackagePrice.HotelId = intHotelID;
                    objPackagePrice.IsOnline = boolIsOnline;
                    string msg = objPackagePricingManager.AddPackagrPraice(objPackagePrice);
                }

            }
            BindEquipmentList();
            hdnWithOutChangeEquipmentItem.Value = "1";
            lstViewEquipmentPriceEdit.Visible = false;
            lstViewEquipmentPrice.Visible = true;
            divSaveCancelEquipment.Visible = false;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This button click used for show edit mode.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkModifyMeetingRoomPrice_Click(object sender, EventArgs e)
    {
        lstViewMeetingRoomConfig.Visible = false;
        lstViewMeetingRoomConfigEdit.Visible = true;
        divSaveCancelMeetingRoom.Visible = true;
        lnkModifyMeetingRoomPrice.Visible = false;
        divmsgpackageByServer.Style.Add("display", "none");
        BindPackageList();
        BindMeetingRoomConfigEdit();

    }

    /// <summary>
    /// This button click used for show edit mode.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkModifyBeverPrice_Click(object sender, EventArgs e)
    {
        lstViewFoodBeveragesPrice.Visible = false;
        lstViewFoodBeveragesPriceEdit.Visible = true;
        divSaveCancelFoodBeverage.Visible = true;
        divmsgpackageByServer.Style.Add("display", "none");
        BindPackageList();
        FoodBeveragesListViewEdit();
    }

    /// <summary>
    /// This button click used for show edit mode.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkModifyEqipmentPrice_Click(object sender, EventArgs e)
    {
        lstViewEquipmentPrice.Visible = false;
        lstViewEquipmentPriceEdit.Visible = true;
        divSaveCancelEquipment.Visible = true;
        divmsgpackageByServer.Style.Add("display", "none");
        BindPackageList();
        EquipmentListViewEdit();

    }

    /// <summary>
    /// This button click used for show view mode.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkcancelMeetingRoomsPrice_Click(object sender, EventArgs e)
    {
        lstViewMeetingRoomConfigEdit.Visible = false;
        lstViewMeetingRoomConfig.Visible = true;
        divSaveCancelMeetingRoom.Visible = false;
        lnkModifyMeetingRoomPrice.Visible = true;
        hdnWithOutChangeMeetingroom.Value = "1";
    }

    /// <summary>
    /// This button click used for show view mode.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkcancelFoodBeveragesPrice_Click(object sender, EventArgs e)
    {
        lstViewFoodBeveragesPriceEdit.Visible = false;
        lstViewFoodBeveragesPrice.Visible = true;
        divSaveCancelFoodBeverage.Visible = false;
        lnkModifyBeverPrice.Visible = true;
        hdnWithOutChangeFoodBeverages.Value = "1";

    }

    /// <summary>
    /// This button click used for show view mode.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkcancelEqipmentPrice_Click(object sender, EventArgs e)
    {
        lstViewEquipmentPriceEdit.Visible = false;
        lstViewEquipmentPrice.Visible = true;
        divSaveCancelEquipment.Visible = false;
        lnkModifyEqipmentPrice.Visible = true;
        hdnWithOutChangeEquipmentItem.Value = "1";
    }

    /// <summary>
    /// This event used for bound package price.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lstViewPackagePrice_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        int intHalfDayPrice = 0;
        int intFullDayPrice = 0;
        int intPackageId = 0;
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            ListViewDataItem currentItem = (ListViewDataItem)e.Item;
            DataKey currentDataKey = this.lstViewPackagePrice.DataKeys[currentItem.DataItemIndex];
            int ItemID = Convert.ToInt32(currentDataKey.Value);

            var allItems = objPackagePricingManager.GetAllPackageItems();
            if (allItems != null)
            {
                var ItmesNameByItemId = allItems.Find(a => a.Id == ItemID);
                if (ItmesNameByItemId != null)
                {
                    Label lblItemName = (Label)e.Item.FindControl("lblItemName");
                    Label lblVat = (Label)e.Item.FindControl("lblVat");
                    lblItemName.Text = ItmesNameByItemId.ItemName;
                    lblVat.Text = ItmesNameByItemId.Vat.ToString();
                }
            }

            intPackageId = Convert.ToInt32(hdnPackage.Value);
            HiddenField hdnItemID = (HiddenField)e.Item.FindControl("hdnItemID");
            hdnItemID.Value = ItemID.ToString();
            TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(intHotelID);


            ///////////////////////// Actual price ////////////////

            ActualPackagePrice actualPrice = objPackagePricingManager.GetActualPackagePriceByPackage(intHotelID, intPackageId).FirstOrDefault();
            if (actualPrice != null)
            {
                lblActualHalfDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(actualPrice.ActualHalfDayPrice), 2));// String.Format("{0:##,##,###}", actualPrice.ActualHalfDayPrice);
                lblActualFullDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(actualPrice.ActualFullDayPrice), 2));//String.Format("{0:##,##,###}", actualPrice.ActualFullDayPrice);
                decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => u.PackageId == intPackageId).Select(u => u.HalfdayPrice).Sum());
                decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => u.PackageId == intPackageId).Select(u => u.FulldayPrice).Sum());
                lblActualHalfTotal.Text = Convert.ToString(Convert.ToDecimal((Convert.ToDecimal(actualPrice.ActualHalfDayPrice) - getTotleHalfDayPrice)));
                lblActualFullTotal.Text = Convert.ToString(Convert.ToDecimal((Convert.ToDecimal(actualPrice.ActualFullDayPrice) - getTotleFullDayPrice)));// (Convert.ToDecimal(actualPrice.ActualFullDayPrice) - getTotleFullDayPrice).ToString();
                lblRentalVatView.Text = objPackagePricingManager.GetRoomRentalVatByCountry(intCountryID).ToString();


            }

            //////////////////////////////// End ///////////////////////////////////

            var getRow = lstPackagePrice.Find(a => a.ItemId == ItemID && a.PackageId == intPackageId);
            if (getRow != null)
            {
                HiddenField hdnPackageByHotelID = (HiddenField)e.Item.FindControl("hdnPackageByHotelID");
                Label lblHalfDayPrice = (Label)e.Item.FindControl("lblHalfDayPrice");
                Label lblFullDayPrice = (Label)e.Item.FindControl("lblFullDayPrice");
                if (getRow.HalfdayPrice != 0)
                {
                    lblHalfDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(getRow.HalfdayPrice), 2));
                }
                else
                {
                    lblHalfDayPrice.Text = "0";
                }
                if (getRow.FulldayPrice != 0)
                {
                    lblFullDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(getRow.FulldayPrice), 2));// String.Format("{0:##,##,##}", getRow.FulldayPrice);
                }
                else
                {
                    lblFullDayPrice.Text = "0";
                }
                intHalfDayPrice = Convert.ToInt32(getRow.HalfdayPrice);
                intFullDayPrice = Convert.ToInt32(getRow.FulldayPrice);
                hdnPackageByHotelID.Value = getRow.Id.ToString();
                chkActivePackage.Checked = getRow.IsOnline;

            }

        }

    }

    /// <summary>
    /// This event used for package price for edit.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lstViewPackagePriceEdit_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            ListViewDataItem currentItem = (ListViewDataItem)e.Item;
            DataKey currentDataKey = this.lstViewPackagePriceEdit.DataKeys[currentItem.DataItemIndex];
            int ItemID = Convert.ToInt32(currentDataKey.Value);

            var allItems = objPackagePricingManager.GetAllPackageItems();
            if (allItems != null)
            {
                var ItmesNameByItemId = allItems.Find(a => a.Id == ItemID);
                if (ItmesNameByItemId != null)
                {
                    Label lblItemName = (Label)e.Item.FindControl("lblItemName");
                    Label lblVat = (Label)e.Item.FindControl("lblVat");
                    lblItemName.Text = ItmesNameByItemId.ItemName;
                    lblVat.Text = ItmesNameByItemId.Vat.ToString();
                    lblRentalVatEdit.Text = objPackagePricingManager.GetRoomRentalVatByCountry(intCountryID).ToString();
                }

            }
            int intPackageId = Convert.ToInt32(hdnPackage.Value);
            HiddenField hdnItemID = (HiddenField)e.Item.FindControl("hdnItemID");
            hdnItemID.Value = ItemID.ToString();
            TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(intHotelID);

            ////////////////////// Fill Package Price///////////////////////////////

            var getRow = lstPackagePrice.Find(a => a.ItemId == ItemID && a.PackageId == intPackageId);
            if (getRow != null)
            {
                HiddenField hdnPackageByHotelID = (HiddenField)e.Item.FindControl("hdnPackageByHotelID");
                TextBox txtHalfDayPrice = (TextBox)e.Item.FindControl("txtHalfDayPrice");
                TextBox txtFullDayPrice = (TextBox)e.Item.FindControl("txtFullDayPrice");
                if (getRow.HalfdayPrice != 0)
                {
                    txtHalfDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(getRow.HalfdayPrice), 2));// String.Format("{0:##,##,###}", getRow.HalfdayPrice);
                }
                else
                {
                    txtHalfDayPrice.Text = "0";
                }
                if (getRow.FulldayPrice != 0)
                {
                    txtFullDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(getRow.FulldayPrice), 2));// String.Format("{0:##,##,###}", getRow.FulldayPrice);
                }
                else
                {
                    txtFullDayPrice.Text = "0";
                }
                hdnPackageByHotelID.Value = getRow.Id.ToString();
                chkActivePackage.Checked = getRow.IsOnline;
            }

            ActualPackagePrice actualPrice = objPackagePricingManager.GetActualPackagePriceByPackage(intHotelID, intPackageId).FirstOrDefault();
            if (actualPrice != null)
            {
                txtActualHalfPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(actualPrice.ActualHalfDayPrice), 2));// String.Format("{0:##,##,###}", actualPrice.ActualHalfDayPrice);
                txtActualFullPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(actualPrice.ActualFullDayPrice), 2));// String.Format("{0:##,##,###}", actualPrice.ActualFullDayPrice);
                decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => u.PackageId == intPackageId).Select(u => u.HalfdayPrice).Sum());
                decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => u.PackageId == intPackageId).Select(u => u.FulldayPrice).Sum());
                calcHalfActual.InnerHtml = Convert.ToString(Convert.ToDecimal(actualPrice.ActualHalfDayPrice) - getTotleHalfDayPrice);//(Convert.ToDecimal(actualPrice.ActualHalfDayPrice) - getTotleHalfDayPrice).ToString();
                calcFullActual.InnerHtml = (Convert.ToDecimal(actualPrice.ActualFullDayPrice) - getTotleFullDayPrice).ToString();// (Convert.ToInt32(actualPrice.ActualFullDayPrice) - getTotleFullDayPrice).ToString();
                hdnHaflCalValue.Value = "1";
                hdnFullCalValue.Value = "1";
                //lblRentalVatEdit.Text = objPackagePricingManager.GetRoomRentalVat().ToString();

            }
            else
            {

                txtActualHalfPrice.Text = "";
                txtActualFullPrice.Text = "";

                ScriptManager.RegisterStartupScript(this, typeof(Page), "TotalHalf", "calculateSum(3);", true);
                ScriptManager.RegisterStartupScript(this, typeof(Page), "TotalFull", "calculateSum(4);", true);
            }
        }
    }

    /// <summary>
    /// This event used for modify package price.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkPackagePriceModify_Click(object sender, EventArgs e)
    {
        divInfoTab.Visible = true;
        divSaveCancelPackage.Visible = true;
        divViewPackagePriceEdit.Visible = true;
        divViewPackagePrice.Visible = false;
    }

    /// <summary>
    /// This button click used for save package price.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkSavePackagePrice_Click(object sender, EventArgs e)
    {
        try
        {
            bool IsNumber = false;
            decimal intHalfdayTotal = Convert.ToDecimal(hdnHaflCalValue.Value);//Convert.ToInt32(Math.Round(Convert.ToDecimal(hdnHaflCalValue.Value), 0));
            decimal intFulldayTotal = Convert.ToDecimal(hdnFullCalValue.Value);//Convert.ToInt32(Math.Round(Convert.ToDecimal(hdnFullCalValue.Value), 0)); 
            if (intHalfdayTotal >= 0 && intFulldayTotal >= 0)
            {
                IsNumber = true;
            }
            if (!IsNumber)
            {
                divmsgpackageByServer.InnerHtml = "Room rental price is not valid.";
                divmsgpackageByServer.Attributes.Add("class", "error");
                divmsgpackageByServer.Style.Add("display", "block");
                ScriptManager.RegisterStartupScript(this, typeof(Page), "TotalHalf", "calculateSum(3);", true);
                ScriptManager.RegisterStartupScript(this, typeof(Page), "TotalFull", "calculateSum(4);", true);
                return;
            }
            else
            {
                //objPackagePricingManager.DeletePackageByHotel(intHotelID, Convert.ToInt32(hdnPackage.Value));

                foreach (ListViewItem lstItem in lstViewPackagePriceEdit.Items)
                {
                    //HiddenField hdnPackageByHotelID = (HiddenField)lstItem.FindControl("hdnPackageByHotelID");
                    HiddenField hdnItemId = (HiddenField)lstItem.FindControl("hdnItemID");
                    TextBox txtHalfDayPrice = (TextBox)lstItem.FindControl("txtHalfDayPrice");
                    TextBox txtFullDayPrice = (TextBox)lstItem.FindControl("txtFullDayPrice");

                    //int intID = Convert.ToInt32(hdnPackageByHotelID.Value);
                    decimal intHalfDay = Convert.ToDecimal(txtHalfDayPrice.Text);
                    decimal intFullDay = Convert.ToDecimal(txtFullDayPrice.Text);
                    decimal intActualHalfDay = Convert.ToDecimal(txtActualHalfPrice.Text);
                    decimal intActualFullDay = Convert.ToDecimal(txtActualFullPrice.Text);
                    int packageId = Convert.ToInt32(hdnPackage.Value);
                    int itemID = Convert.ToInt32(hdnItemId.Value);
                    PackageByHotel pricingrow = objPackagePricingManager.IsPriceExists(packageId, itemID, intHotelID).FirstOrDefault();
                    if (pricingrow != null)
                    {
                        PackageByHotel objPackagePrice = objPackagePricingManager.GetPriceByID(Convert.ToInt32(pricingrow.Id));
                        objPackagePrice.IsComplementary = false;
                        objPackagePrice.HalfdayPrice = intHalfDay;
                        objPackagePrice.FulldayPrice = intFullDay;
                        objPackagePrice.IsOnline = chkActivePackage.Checked;
                        string msg = objPackagePricingManager.UpdatePackagrPraice(objPackagePrice);
                    }
                    else
                    {
                        PackageByHotel objPackagePrice = new PackageByHotel();
                        objPackagePrice.PackageId = packageId;
                        objPackagePrice.ItemId = itemID;
                        objPackagePrice.IsComplementary = false;
                        objPackagePrice.HalfdayPrice = intHalfDay;
                        objPackagePrice.FulldayPrice = intFullDay;
                        objPackagePrice.HotelId = intHotelID;
                        objPackagePrice.IsOnline = chkActivePackage.Checked;
                        string msg = objPackagePricingManager.AddPackagrPraice(objPackagePrice);
                    }
                    objPackagePricingManager.AddUpdatePackageActualPrice(intHotelID, packageId, intActualHalfDay, intActualFullDay);

                }
            }

            divmsgpackageByServer.InnerHtml = objPackagePricingManager.propMassage;
            if (Convert.ToString(ViewState["Count"]) != "1")
            {
                divmsgpackageByServer.InnerHtml += "&nbsp; Please click on next button to move ahead.";
            }
            divmsgpackageByServer.Attributes.Add("class", "succesfuly");
            divmsgpackageByServer.Style.Add("display", "block");
            BindPackageList();
            CreateDynamicPackageTab();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This event used for complementry check.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void check(object sender, EventArgs e)
    {
        CheckBox cb = (CheckBox)sender;
        if (cb.Checked)
        {
            TextBox txtHalfDay = (TextBox)((ListViewItem)cb.Parent).FindControl("txtHalfDayPrice");
            TextBox txtFullDay = (TextBox)((ListViewItem)cb.Parent).FindControl("txtFullDayPrice");
            txtHalfDay.Text = "";
            txtFullDay.Text = "";
            txtHalfDay.Visible = false;
            txtFullDay.Visible = false;

        }
        else
        {
            TextBox txtHalfDay = (TextBox)((ListViewItem)cb.Parent).FindControl("txtHalfDayPrice");
            TextBox txtFullDay = (TextBox)((ListViewItem)cb.Parent).FindControl("txtFullDayPrice");
            txtHalfDay.Text = "";
            txtFullDay.Text = "";
            txtHalfDay.Visible = true;
            txtFullDay.Visible = true;

        }
    }

    /// <summary>
    /// This button click event used for proceed to next step.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNext_Click(object sender, EventArgs e)
    {
        ObjWizardLinkSetting.ThisPageIsDone(intHotelID, Convert.ToInt32(PropertyLevelSectionLeftMenu.lnkMeetingRooms));
        Response.Redirect("BedroomPricing.aspx", false);
        return;
    }

    /// <summary>
    /// This event used for bound equipmentitem for view.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lstViewOthersPrice_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            ListViewDataItem currentItem = (ListViewDataItem)e.Item;
            DataKey currentDataKey = this.lstViewOthersPrice.DataKeys[currentItem.DataItemIndex];
            int intItemID = Convert.ToInt32(currentDataKey.Value);
            TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(intHotelID);

            ////////////////////// Fill Package Price///////////////////////////////

            var getRow = lstPackagePrice.Find(a => a.ItemId == intItemID && a.PackageId == null);
            if (getRow != null)
            {

                Label lblHalfDayPrice = (Label)e.Item.FindControl("lblHalfDayPrice");
                Label lblFullDayPrice = (Label)e.Item.FindControl("lblFullDayPrice");

                Image imgIsEmentary = (Image)e.Item.FindControl("imgIsEmentary");

                if (getRow.IsComplementary == true)
                {
                    imgIsEmentary.ImageUrl = "~/images/tick.png";
                    lblHalfDayPrice.Text = "";
                    lblFullDayPrice.Text = "";

                }
                else
                {
                    imgIsEmentary.ImageUrl = "~/images/tick.png";
                    imgIsEmentary.Visible = false;
                    if (getRow.HalfdayPrice != null)
                    {
                        if (getRow.HalfdayPrice != 0)
                        {
                            lblHalfDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(getRow.HalfdayPrice), 2));// String.Format("{0:#,###}", getRow.HalfdayPrice);
                        }
                        else
                        {
                            lblHalfDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(getRow.HalfdayPrice), 2));// Convert.ToInt32(getRow.HalfdayPrice).ToString();

                        }
                    }
                    else
                    {
                        lblHalfDayPrice.Text = "";
                    }
                    if (getRow.FulldayPrice != null)
                    {
                        if (getRow.FulldayPrice != 0)
                        {
                            lblFullDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(getRow.FulldayPrice), 2)); //String.Format("{0:#,###}", getRow.FulldayPrice);
                        }
                        else
                        {
                            lblFullDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(getRow.FulldayPrice), 2)); //Convert.ToInt32(getRow.FulldayPrice).ToString();
                        }
                    }
                    else
                    {
                        lblFullDayPrice.Text = "";
                    }
                }

                Image imgIsOnline = (Image)e.Item.FindControl("imgIsOnline");
                if (getRow.IsOnline == true)
                {
                    imgIsOnline.ImageUrl = "~/images/tick.png";

                }
                else
                {
                    imgIsOnline.ImageUrl = "~/images/tick.png";
                    imgIsOnline.Visible = false;
                }

            }

            // ////////////Fill Description ///////////////////////////
            int intLanguageID = Convert.ToInt32(ObjMeetingRoomDesc.GetAllRequiredLanguage().Find(a => a.Name == "English").Id);
            var getDescription = objPackagePricingManager.GetPackageDescriptionByItemID(intItemID).Find(a => a.LanguageId == intLanguageID);
            if (getDescription != null)
            {

                Label lblDesc = (Label)e.Item.FindControl("lblDesc");
                lblDesc.Text = getDescription.ItemDescription.ToString();

            }
            /////////////////////////////////////////////////////////////
        }

    }

    /// <summary>
    /// This event used for bound equipmentItem for edit.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lstViewOthersPriceEdit_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            ListViewDataItem currentItem = (ListViewDataItem)e.Item;
            DataKey currentDataKey = this.lstViewOthersPriceEdit.DataKeys[currentItem.DataItemIndex];
            int intItemID = Convert.ToInt32(currentDataKey.Value);
            TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(intHotelID);
            HiddenField hdnItemID = (HiddenField)e.Item.FindControl("hdnItemID");
            hdnItemID.Value = intItemID.ToString();

            ////////////////////// Fill Package Price///////////////////////////////

            var getRow = lstPackagePrice.Find(a => a.ItemId == intItemID && a.PackageId == null);
            if (getRow != null)
            {
                TextBox txtHalfDayPrice = (TextBox)e.Item.FindControl("txtHalfDayPrice");
                TextBox txtFullDayPrice = (TextBox)e.Item.FindControl("txtFullDayPrice");
                HiddenField hdnPackageByHotelID = (HiddenField)e.Item.FindControl("hdnPackageByHotelID");
                hdnPackageByHotelID.Value = getRow.Id.ToString();

                CheckBox chkIsEmentary = (CheckBox)e.Item.FindControl("chkIsEmentary");
                if (getRow.IsComplementary == true)
                {
                    chkIsEmentary.Checked = true;
                    txtHalfDayPrice.Visible = false;
                    txtFullDayPrice.Visible = false;
                }
                else
                {
                    chkIsEmentary.Checked = false;
                    txtHalfDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(getRow.HalfdayPrice), 2));// String.Format("{0:0.##}", getRow.HalfdayPrice);
                    txtFullDayPrice.Text = Convert.ToString(Math.Round(Convert.ToDecimal(getRow.FulldayPrice), 2)); //String.Format("{0:0.##}", getRow.FulldayPrice);
                }

                CheckBox chkIsOnline = (CheckBox)e.Item.FindControl("chkIsOnline");
                if (getRow.IsOnline == true)
                {
                    chkIsOnline.Checked = true;
                }
                else
                {
                    chkIsOnline.Checked = false;
                }

            }

            // ////////////Fill Description ///////////////////////////
            int intLanguageID = Convert.ToInt32(ObjMeetingRoomDesc.GetAllRequiredLanguage().Find(a => a.Name == "English").Id);
            var getDescription = objPackagePricingManager.GetPackageDescriptionByItemID(intItemID).Find(a => a.LanguageId == intLanguageID);
            if (getDescription != null)
            {

                Label lblDesc = (Label)e.Item.FindControl("lblDesc");
                lblDesc.Text = getDescription.ItemDescription.ToString();

            }
            /////////////////////////////////////////////////////////////
        }
    }

    /// <summary>
    /// This button click event used for save equipmentItem price.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkSaveOthersPrice_Click(object sender, EventArgs e)
    {
        try
        {
            foreach (ListViewItem lstItem in lstViewOthersPriceEdit.Items)
            {
                decimal? decimalHalfDay = null;
                decimal? decimalFullDay = null;
                //HiddenField hdnPackageByHotelID = (HiddenField)lstItem.FindControl("hdnPackageByHotelID");
                TextBox txtHalfDayPrice = (TextBox)lstItem.FindControl("txtHalfDayPrice");
                TextBox txtFullDayPrice = (TextBox)lstItem.FindControl("txtFullDayPrice");
                CheckBox chkIsOnline = (CheckBox)lstItem.FindControl("chkIsOnline");
                CheckBox chkIsEmentary = (CheckBox)lstItem.FindControl("chkIsEmentary");
                bool boolIsEmentary = chkIsEmentary.Checked;
                if (boolIsEmentary != true)
                {
                    if (txtHalfDayPrice.Text != "")
                    {
                        if (txtHalfDayPrice.Text != "")
                        {
                            decimalHalfDay = Convert.ToDecimal(txtHalfDayPrice.Text);
                        }
                        if (txtFullDayPrice.Text != "")
                        {
                            decimalFullDay = Convert.ToDecimal(txtFullDayPrice.Text);
                        }
                    }
                }
                else
                {
                    if (txtHalfDayPrice.Text != "")
                    {
                        decimalHalfDay = Convert.ToDecimal(txtHalfDayPrice.Text);
                    }
                    if (txtFullDayPrice.Text != "")
                    {
                        decimalFullDay = Convert.ToDecimal(txtFullDayPrice.Text);
                    }

                }
                //int intID = Convert.ToInt32(hdnPackageByHotelID.Value);

                bool boolIsOnline = chkIsOnline.Checked;
                HiddenField hdnItemId = (HiddenField)lstItem.FindControl("hdnItemID");
                int ItemId = Convert.ToInt32(hdnItemId.Value);
                PackageByHotel pricingrow = objPackagePricingManager.IsPriceExists(ItemId, intHotelID).FirstOrDefault();
                if (pricingrow != null)
                {
                    PackageByHotel objPackagePrice = objPackagePricingManager.GetPriceByID(Convert.ToInt64(pricingrow.Id));
                    objPackagePrice.IsComplementary = boolIsEmentary;
                    objPackagePrice.HalfdayPrice = decimalHalfDay;
                    objPackagePrice.FulldayPrice = decimalFullDay;
                    objPackagePrice.IsOnline = boolIsOnline;
                    string msg = objPackagePricingManager.UpdatePackagrPraice(objPackagePrice);
                }
                else
                {
                    PackageByHotel objPackagePrice = new PackageByHotel();
                    objPackagePrice.PackageId = null;
                    objPackagePrice.ItemId = ItemId;
                    objPackagePrice.IsComplementary = boolIsEmentary;
                    objPackagePrice.HalfdayPrice = decimalHalfDay;
                    objPackagePrice.FulldayPrice = decimalFullDay;
                    objPackagePrice.HotelId = intHotelID;
                    objPackagePrice.IsOnline = boolIsOnline;
                    string msg = objPackagePricingManager.AddPackagrPraice(objPackagePrice);
                }

            }
            BindOthersList();
            hdnWithOutChangeOthersItem.Value = "1";
            lstViewOthersPriceEdit.Visible = false;
            lstViewOthersPrice.Visible = true;
            divSaveCancelOthers.Visible = false;

        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This button click used for show edit mode.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //protected void lnkModifyMeetingRoomPrice_Click(object sender, EventArgs e)
    //{
    //    lstViewMeetingRoomConfig.Visible = false;
    //    lstViewMeetingRoomConfigEdit.Visible = true;
    //    divSaveCancelMeetingRoom.Visible = true;
    //    lnkModifyMeetingRoomPrice.Visible = false;
    //    divmsgpackageByServer.Style.Add("display", "none");
    //    BindPackageList();
    //    BindMeetingRoomConfigEdit();

    //}

    /// <summary>
    /// This button click used for show view mode.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkcancelOthersPrice_Click(object sender, EventArgs e)
    {
        lstViewOthersPriceEdit.Visible = false;
        lstViewOthersPrice.Visible = true;
        divSaveCancelOthers.Visible = false;
        lnkModifyOthersPrice.Visible = true;
        hdnWithOutChangeOthersItem.Value = "1";
    }

    protected void lnkModifyOthersPrice_Click(object sender, EventArgs e)
    {
        lstViewOthersPrice.Visible = false;
        lstViewOthersPriceEdit.Visible = true;
        divSaveCancelOthers.Visible = true;
        divmsgpackageByServer.Style.Add("display", "none");
        BindPackageList();
        OthersListViewEdit();

    }
    #endregion
}