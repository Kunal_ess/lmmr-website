﻿<%@ Page Title="Hotel User - General Info" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master"
    AutoEventWireup="true" CodeFile="GeneralInfo.aspx.cs" Inherits="GeneralInfo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .hidebutton
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" runat="Server">
    <h1>
        General info : <b>
            <asp:Label ID="lblHotel" runat="server" Text=""></asp:Label></b></h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" runat="Server">
    <asp:HiddenField ID="hdnIDS" runat="server" />
    <asp:HiddenField ID="hndHotelID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnHotelDescID" runat="server" Value="0" />
    <asp:Panel ID="pnlGeneralInfo" runat="server" DefaultButton="btnSubmit">
        <div class="booking-details">
            <div id="divmessage" runat="server">
            </div>
            <div id="TabbedPanels1" class="TabbedPanels">
                <asp:Literal ID="litLangugeTab" runat="server"></asp:Literal>
            </div>
            <script type="text/javascript">
                var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
            </script>
            <ul>
                <li class="value8">
                    <div class="col17">
                        <strong>Hotel / Conference Facility Name</strong></div>
                    <div class="col18">
                        <asp:Label ID="lblHotelName" runat="server" Text=""></asp:Label></div>
                </li>
                <li class="value9">
                    <div class="col17">
                        <strong>Stars</strong></div>
                    <div class="col18">
                        <asp:Image ID="imgStars" runat="server" />
                    </div>
                </li>
                <li class="value8">
                    <div class="col17">
                        <strong>Country</strong></div>
                    <div class="col18">
                        <asp:Label ID="lblCountry" runat="server" Text=""></asp:Label></div>
                </li>
                <li class="value9">
                    <div class="col17">
                        <strong>City</strong></div>
                    <div class="col18">
                        <asp:Label ID="lblCity" runat="server" Text=""></asp:Label></div>
                </li>
                <li class="value8">
                    <div class="col17">
                        <strong>Zone</strong></div>
                    <div class="col18">
                        <asp:Label ID="lblZone" runat="server" Text=""></asp:Label></div>
                </li>
                <li class="value9">
                    <div class="col17">
                        <strong>Zip code<span style="color: Red"> *</span></strong></div>
                    <div class="col19">
                        <asp:TextBox ID="txtZipCode" runat="server" CssClass="textfield" MaxLength="15"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtZipCode"
                            FilterType="Numbers">
                        </asp:FilteredTextBoxExtender>
                    </div>
                </li>
                <li class="value8">
                    <div class="col17">
                        <strong>Address<span style="color: Red"> *</span> </strong>
                    </div>
                    <div class="col19">
                        <asp:TextBox ID="txtAddress" runat="server" CssClass="textarea-new" TextMode="MultiLine"
                            MaxLength="100"></asp:TextBox>
                    </div>
                </li>
                <li class="value9">
                    <div class="col17">
                        <strong>Phone<span style="color: Red"> *</span> </strong>
                    </div>
                    <div class="col19">
                        <span style="float: left">
                            <asp:DropDownList ID="drpExtPhone" runat="server" CssClass="NoClassApply">
                                <asp:ListItem Value="0">Extension</asp:ListItem>
                                <asp:ListItem>1</asp:ListItem>
                                <asp:ListItem>7</asp:ListItem>
                                <asp:ListItem>20</asp:ListItem>
                                <asp:ListItem>27</asp:ListItem>
                                <asp:ListItem>30</asp:ListItem>
                                <asp:ListItem>31</asp:ListItem>
                                <asp:ListItem>32</asp:ListItem>
                                <asp:ListItem>33</asp:ListItem>
                                <asp:ListItem>34</asp:ListItem>
                                <asp:ListItem>36</asp:ListItem>
                                <asp:ListItem>39</asp:ListItem>
                                <asp:ListItem>40</asp:ListItem>
                                <asp:ListItem>41</asp:ListItem>
                                <asp:ListItem>43</asp:ListItem>
                                <asp:ListItem>44</asp:ListItem>
                                <asp:ListItem>45</asp:ListItem>
                                <asp:ListItem>46</asp:ListItem>
                                <asp:ListItem>47</asp:ListItem>
                                <asp:ListItem>48</asp:ListItem>
                                <asp:ListItem>49</asp:ListItem>
                                <asp:ListItem>51</asp:ListItem>
                                <asp:ListItem>52</asp:ListItem>
                                <asp:ListItem>53</asp:ListItem>
                                <asp:ListItem>54</asp:ListItem>
                                <asp:ListItem>55</asp:ListItem>
                                <asp:ListItem>56</asp:ListItem>
                                <asp:ListItem>57</asp:ListItem>
                                <asp:ListItem>58</asp:ListItem>
                                <asp:ListItem>60</asp:ListItem>
                                <asp:ListItem>61</asp:ListItem>
                                <asp:ListItem>62</asp:ListItem>
                                <asp:ListItem>63</asp:ListItem>
                                <asp:ListItem>64</asp:ListItem>
                                <asp:ListItem>65</asp:ListItem>
                                <asp:ListItem>66</asp:ListItem>
                                <asp:ListItem>81</asp:ListItem>
                                <asp:ListItem>82</asp:ListItem>
                                <asp:ListItem>84</asp:ListItem>
                                <asp:ListItem>86</asp:ListItem>
                                <asp:ListItem>90</asp:ListItem>
                                <asp:ListItem>91</asp:ListItem>
                                <asp:ListItem>92</asp:ListItem>
                                <asp:ListItem>93</asp:ListItem>
                                <asp:ListItem>94</asp:ListItem>
                                <asp:ListItem>95</asp:ListItem>
                                <asp:ListItem>98</asp:ListItem>
                                <asp:ListItem>212</asp:ListItem>
                                <asp:ListItem>213</asp:ListItem>
                                <asp:ListItem>216</asp:ListItem>
                                <asp:ListItem>218</asp:ListItem>
                                <asp:ListItem>220</asp:ListItem>
                                <asp:ListItem>221</asp:ListItem>
                                <asp:ListItem>222</asp:ListItem>
                                <asp:ListItem>223</asp:ListItem>
                                <asp:ListItem>224</asp:ListItem>
                                <asp:ListItem>225</asp:ListItem>
                                <asp:ListItem>226</asp:ListItem>
                                <asp:ListItem>227</asp:ListItem>
                                <asp:ListItem>228</asp:ListItem>
                                <asp:ListItem>229</asp:ListItem>
                                <asp:ListItem>230</asp:ListItem>
                                <asp:ListItem>231</asp:ListItem>
                                <asp:ListItem>232</asp:ListItem>
                                <asp:ListItem>233</asp:ListItem>
                                <asp:ListItem>234</asp:ListItem>
                                <asp:ListItem>235</asp:ListItem>
                                <asp:ListItem>236</asp:ListItem>
                                <asp:ListItem>237</asp:ListItem>
                                <asp:ListItem>238</asp:ListItem>
                                <asp:ListItem>239</asp:ListItem>
                                <asp:ListItem>240</asp:ListItem>
                                <asp:ListItem>241</asp:ListItem>
                                <asp:ListItem>242</asp:ListItem>
                                <asp:ListItem>243</asp:ListItem>
                                <asp:ListItem>244</asp:ListItem>
                                <asp:ListItem>245</asp:ListItem>
                                <asp:ListItem>248</asp:ListItem>
                                <asp:ListItem>249</asp:ListItem>
                                <asp:ListItem>250</asp:ListItem>
                                <asp:ListItem>251</asp:ListItem>
                                <asp:ListItem>252</asp:ListItem>
                                <asp:ListItem>253</asp:ListItem>
                                <asp:ListItem>254</asp:ListItem>
                                <asp:ListItem>255</asp:ListItem>
                                <asp:ListItem>256</asp:ListItem>
                                <asp:ListItem>257</asp:ListItem>
                                <asp:ListItem>258</asp:ListItem>
                                <asp:ListItem>260</asp:ListItem>
                                <asp:ListItem>261</asp:ListItem>
                                <asp:ListItem>262</asp:ListItem>
                                <asp:ListItem>263</asp:ListItem>
                                <asp:ListItem>264</asp:ListItem>
                                <asp:ListItem>265</asp:ListItem>
                                <asp:ListItem>266</asp:ListItem>
                                <asp:ListItem>267</asp:ListItem>
                                <asp:ListItem>268</asp:ListItem>
                                <asp:ListItem>269</asp:ListItem>
                                <asp:ListItem>290</asp:ListItem>
                                <asp:ListItem>291</asp:ListItem>
                                <asp:ListItem>297</asp:ListItem>
                                <asp:ListItem>298</asp:ListItem>
                                <asp:ListItem>299</asp:ListItem>
                                <asp:ListItem>350</asp:ListItem>
                                <asp:ListItem>351</asp:ListItem>
                                <asp:ListItem>352</asp:ListItem>
                                <asp:ListItem>353</asp:ListItem>
                                <asp:ListItem>354</asp:ListItem>
                                <asp:ListItem>355</asp:ListItem>
                                <asp:ListItem>356</asp:ListItem>
                                <asp:ListItem>357</asp:ListItem>
                                <asp:ListItem>358</asp:ListItem>
                                <asp:ListItem>359</asp:ListItem>
                                <asp:ListItem>370</asp:ListItem>
                                <asp:ListItem>371</asp:ListItem>
                                <asp:ListItem>372</asp:ListItem>
                                <asp:ListItem>373</asp:ListItem>
                                <asp:ListItem>374</asp:ListItem>
                                <asp:ListItem>375</asp:ListItem>
                                <asp:ListItem>376</asp:ListItem>
                                <asp:ListItem>377</asp:ListItem>
                                <asp:ListItem>378</asp:ListItem>
                                <asp:ListItem>380</asp:ListItem>
                                <asp:ListItem>381</asp:ListItem>
                                <asp:ListItem>382</asp:ListItem>
                                <asp:ListItem>385</asp:ListItem>
                                <asp:ListItem>386</asp:ListItem>
                                <asp:ListItem>387</asp:ListItem>
                                <asp:ListItem>389</asp:ListItem>
                                <asp:ListItem>420</asp:ListItem>
                                <asp:ListItem>421</asp:ListItem>
                                <asp:ListItem>423</asp:ListItem>
                                <asp:ListItem>500</asp:ListItem>
                                <asp:ListItem>501</asp:ListItem>
                                <asp:ListItem>502</asp:ListItem>
                                <asp:ListItem>503</asp:ListItem>
                                <asp:ListItem>504</asp:ListItem>
                                <asp:ListItem>505</asp:ListItem>
                                <asp:ListItem>506</asp:ListItem>
                                <asp:ListItem>507</asp:ListItem>
                                <asp:ListItem>508</asp:ListItem>
                                <asp:ListItem>509</asp:ListItem>
                                <asp:ListItem>590</asp:ListItem>
                                <asp:ListItem>591</asp:ListItem>
                                <asp:ListItem>592</asp:ListItem>
                                <asp:ListItem>593</asp:ListItem>
                                <asp:ListItem>595</asp:ListItem>
                                <asp:ListItem>597</asp:ListItem>
                                <asp:ListItem>598</asp:ListItem>
                                <asp:ListItem>599</asp:ListItem>
                                <asp:ListItem>670</asp:ListItem>
                                <asp:ListItem>672</asp:ListItem>
                                <asp:ListItem>673</asp:ListItem>
                                <asp:ListItem>674</asp:ListItem>
                                <asp:ListItem>675</asp:ListItem>
                                <asp:ListItem>676</asp:ListItem>
                                <asp:ListItem>677</asp:ListItem>
                                <asp:ListItem>678</asp:ListItem>
                                <asp:ListItem>679</asp:ListItem>
                                <asp:ListItem>680</asp:ListItem>
                                <asp:ListItem>681</asp:ListItem>
                                <asp:ListItem>682</asp:ListItem>
                                <asp:ListItem>683</asp:ListItem>
                                <asp:ListItem>685</asp:ListItem>
                                <asp:ListItem>686</asp:ListItem>
                                <asp:ListItem>687</asp:ListItem>
                                <asp:ListItem>688</asp:ListItem>
                                <asp:ListItem>689</asp:ListItem>
                                <asp:ListItem>690</asp:ListItem>
                                <asp:ListItem>691</asp:ListItem>
                                <asp:ListItem>692</asp:ListItem>
                                <asp:ListItem>850</asp:ListItem>
                                <asp:ListItem>852</asp:ListItem>
                                <asp:ListItem>853</asp:ListItem>
                                <asp:ListItem>855</asp:ListItem>
                                <asp:ListItem>856</asp:ListItem>
                                <asp:ListItem>870</asp:ListItem>
                                <asp:ListItem>880</asp:ListItem>
                                <asp:ListItem>886</asp:ListItem>
                                <asp:ListItem>960</asp:ListItem>
                                <asp:ListItem>961</asp:ListItem>
                                <asp:ListItem>962</asp:ListItem>
                                <asp:ListItem>963</asp:ListItem>
                                <asp:ListItem>964</asp:ListItem>
                                <asp:ListItem>965</asp:ListItem>
                                <asp:ListItem>966</asp:ListItem>
                                <asp:ListItem>967</asp:ListItem>
                                <asp:ListItem>968</asp:ListItem>
                                <asp:ListItem>970</asp:ListItem>
                                <asp:ListItem>971</asp:ListItem>
                                <asp:ListItem>972</asp:ListItem>
                                <asp:ListItem>973</asp:ListItem>
                                <asp:ListItem>974</asp:ListItem>
                                <asp:ListItem>975</asp:ListItem>
                                <asp:ListItem>976</asp:ListItem>
                                <asp:ListItem>977</asp:ListItem>
                                <asp:ListItem>992</asp:ListItem>
                                <asp:ListItem>993</asp:ListItem>
                                <asp:ListItem>994</asp:ListItem>
                                <asp:ListItem>995</asp:ListItem>
                                <asp:ListItem>996</asp:ListItem>
                                <asp:ListItem>998</asp:ListItem>
                                <asp:ListItem>1242</asp:ListItem>
                                <asp:ListItem>1246</asp:ListItem>
                                <asp:ListItem>1264</asp:ListItem>
                                <asp:ListItem>1268</asp:ListItem>
                                <asp:ListItem>1284</asp:ListItem>
                                <asp:ListItem>1340</asp:ListItem>
                                <asp:ListItem>1345</asp:ListItem>
                                <asp:ListItem>1441</asp:ListItem>
                                <asp:ListItem>1473</asp:ListItem>
                                <asp:ListItem>1599</asp:ListItem>
                                <asp:ListItem>1649</asp:ListItem>
                                <asp:ListItem>1664</asp:ListItem>
                                <asp:ListItem>1670</asp:ListItem>
                                <asp:ListItem>1671</asp:ListItem>
                                <asp:ListItem>1684</asp:ListItem>
                                <asp:ListItem>1758</asp:ListItem>
                                <asp:ListItem>1767</asp:ListItem>
                                <asp:ListItem>1784</asp:ListItem>
                                <asp:ListItem>1809</asp:ListItem>
                                <asp:ListItem>1868</asp:ListItem>
                                <asp:ListItem>1869</asp:ListItem>
                                <asp:ListItem>1876</asp:ListItem>
                                 <asp:ListItem>00420</asp:ListItem>
                            </asp:DropDownList>
                        </span>
                        <asp:TextBox ID="txtPhone" runat="server" CssClass="textfield" MaxLength="15" Style="margin-left: 10px;"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtPhone"
                            FilterType="Numbers">
                        </asp:FilteredTextBoxExtender>
                    </div>
                </li>
                <li class="value8">
                    <div class="col17">
                        <strong>Fax<span style="color: Red"> *</span> </strong>
                    </div>
                    <div class="col19">
                        <span style="float: left">
                            <asp:DropDownList ID="drpExtFax" runat="server" CssClass="NoClassApply">
                                <asp:ListItem Value="0">Extension</asp:ListItem>
                                <asp:ListItem>1</asp:ListItem>
                                <asp:ListItem>7</asp:ListItem>
                                <asp:ListItem>20</asp:ListItem>
                                <asp:ListItem>27</asp:ListItem>
                                <asp:ListItem>30</asp:ListItem>
                                <asp:ListItem>31</asp:ListItem>
                                <asp:ListItem>32</asp:ListItem>
                                <asp:ListItem>33</asp:ListItem>
                                <asp:ListItem>34</asp:ListItem>
                                <asp:ListItem>36</asp:ListItem>
                                <asp:ListItem>39</asp:ListItem>
                                <asp:ListItem>40</asp:ListItem>
                                <asp:ListItem>41</asp:ListItem>
                                <asp:ListItem>43</asp:ListItem>
                                <asp:ListItem>44</asp:ListItem>
                                <asp:ListItem>45</asp:ListItem>
                                <asp:ListItem>46</asp:ListItem>
                                <asp:ListItem>47</asp:ListItem>
                                <asp:ListItem>48</asp:ListItem>
                                <asp:ListItem>49</asp:ListItem>
                                <asp:ListItem>51</asp:ListItem>
                                <asp:ListItem>52</asp:ListItem>
                                <asp:ListItem>53</asp:ListItem>
                                <asp:ListItem>54</asp:ListItem>
                                <asp:ListItem>55</asp:ListItem>
                                <asp:ListItem>56</asp:ListItem>
                                <asp:ListItem>57</asp:ListItem>
                                <asp:ListItem>58</asp:ListItem>
                                <asp:ListItem>60</asp:ListItem>
                                <asp:ListItem>61</asp:ListItem>
                                <asp:ListItem>62</asp:ListItem>
                                <asp:ListItem>63</asp:ListItem>
                                <asp:ListItem>64</asp:ListItem>
                                <asp:ListItem>65</asp:ListItem>
                                <asp:ListItem>66</asp:ListItem>
                                <asp:ListItem>81</asp:ListItem>
                                <asp:ListItem>82</asp:ListItem>
                                <asp:ListItem>84</asp:ListItem>
                                <asp:ListItem>86</asp:ListItem>
                                <asp:ListItem>90</asp:ListItem>
                                <asp:ListItem>91</asp:ListItem>
                                <asp:ListItem>92</asp:ListItem>
                                <asp:ListItem>93</asp:ListItem>
                                <asp:ListItem>94</asp:ListItem>
                                <asp:ListItem>95</asp:ListItem>
                                <asp:ListItem>98</asp:ListItem>
                                <asp:ListItem>212</asp:ListItem>
                                <asp:ListItem>213</asp:ListItem>
                                <asp:ListItem>216</asp:ListItem>
                                <asp:ListItem>218</asp:ListItem>
                                <asp:ListItem>220</asp:ListItem>
                                <asp:ListItem>221</asp:ListItem>
                                <asp:ListItem>222</asp:ListItem>
                                <asp:ListItem>223</asp:ListItem>
                                <asp:ListItem>224</asp:ListItem>
                                <asp:ListItem>225</asp:ListItem>
                                <asp:ListItem>226</asp:ListItem>
                                <asp:ListItem>227</asp:ListItem>
                                <asp:ListItem>228</asp:ListItem>
                                <asp:ListItem>229</asp:ListItem>
                                <asp:ListItem>230</asp:ListItem>
                                <asp:ListItem>231</asp:ListItem>
                                <asp:ListItem>232</asp:ListItem>
                                <asp:ListItem>233</asp:ListItem>
                                <asp:ListItem>234</asp:ListItem>
                                <asp:ListItem>235</asp:ListItem>
                                <asp:ListItem>236</asp:ListItem>
                                <asp:ListItem>237</asp:ListItem>
                                <asp:ListItem>238</asp:ListItem>
                                <asp:ListItem>239</asp:ListItem>
                                <asp:ListItem>240</asp:ListItem>
                                <asp:ListItem>241</asp:ListItem>
                                <asp:ListItem>242</asp:ListItem>
                                <asp:ListItem>243</asp:ListItem>
                                <asp:ListItem>244</asp:ListItem>
                                <asp:ListItem>245</asp:ListItem>
                                <asp:ListItem>248</asp:ListItem>
                                <asp:ListItem>249</asp:ListItem>
                                <asp:ListItem>250</asp:ListItem>
                                <asp:ListItem>251</asp:ListItem>
                                <asp:ListItem>252</asp:ListItem>
                                <asp:ListItem>253</asp:ListItem>
                                <asp:ListItem>254</asp:ListItem>
                                <asp:ListItem>255</asp:ListItem>
                                <asp:ListItem>256</asp:ListItem>
                                <asp:ListItem>257</asp:ListItem>
                                <asp:ListItem>258</asp:ListItem>
                                <asp:ListItem>260</asp:ListItem>
                                <asp:ListItem>261</asp:ListItem>
                                <asp:ListItem>262</asp:ListItem>
                                <asp:ListItem>263</asp:ListItem>
                                <asp:ListItem>264</asp:ListItem>
                                <asp:ListItem>265</asp:ListItem>
                                <asp:ListItem>266</asp:ListItem>
                                <asp:ListItem>267</asp:ListItem>
                                <asp:ListItem>268</asp:ListItem>
                                <asp:ListItem>269</asp:ListItem>
                                <asp:ListItem>290</asp:ListItem>
                                <asp:ListItem>291</asp:ListItem>
                                <asp:ListItem>297</asp:ListItem>
                                <asp:ListItem>298</asp:ListItem>
                                <asp:ListItem>299</asp:ListItem>
                                <asp:ListItem>350</asp:ListItem>
                                <asp:ListItem>351</asp:ListItem>
                                <asp:ListItem>352</asp:ListItem>
                                <asp:ListItem>353</asp:ListItem>
                                <asp:ListItem>354</asp:ListItem>
                                <asp:ListItem>355</asp:ListItem>
                                <asp:ListItem>356</asp:ListItem>
                                <asp:ListItem>357</asp:ListItem>
                                <asp:ListItem>358</asp:ListItem>
                                <asp:ListItem>359</asp:ListItem>
                                <asp:ListItem>370</asp:ListItem>
                                <asp:ListItem>371</asp:ListItem>
                                <asp:ListItem>372</asp:ListItem>
                                <asp:ListItem>373</asp:ListItem>
                                <asp:ListItem>374</asp:ListItem>
                                <asp:ListItem>375</asp:ListItem>
                                <asp:ListItem>376</asp:ListItem>
                                <asp:ListItem>377</asp:ListItem>
                                <asp:ListItem>378</asp:ListItem>
                                <asp:ListItem>380</asp:ListItem>
                                <asp:ListItem>381</asp:ListItem>
                                <asp:ListItem>382</asp:ListItem>
                                <asp:ListItem>385</asp:ListItem>
                                <asp:ListItem>386</asp:ListItem>
                                <asp:ListItem>387</asp:ListItem>
                                <asp:ListItem>389</asp:ListItem>
                                <asp:ListItem>420</asp:ListItem>
                                <asp:ListItem>421</asp:ListItem>
                                <asp:ListItem>423</asp:ListItem>
                                <asp:ListItem>500</asp:ListItem>
                                <asp:ListItem>501</asp:ListItem>
                                <asp:ListItem>502</asp:ListItem>
                                <asp:ListItem>503</asp:ListItem>
                                <asp:ListItem>504</asp:ListItem>
                                <asp:ListItem>505</asp:ListItem>
                                <asp:ListItem>506</asp:ListItem>
                                <asp:ListItem>507</asp:ListItem>
                                <asp:ListItem>508</asp:ListItem>
                                <asp:ListItem>509</asp:ListItem>
                                <asp:ListItem>590</asp:ListItem>
                                <asp:ListItem>591</asp:ListItem>
                                <asp:ListItem>592</asp:ListItem>
                                <asp:ListItem>593</asp:ListItem>
                                <asp:ListItem>595</asp:ListItem>
                                <asp:ListItem>597</asp:ListItem>
                                <asp:ListItem>598</asp:ListItem>
                                <asp:ListItem>599</asp:ListItem>
                                <asp:ListItem>670</asp:ListItem>
                                <asp:ListItem>672</asp:ListItem>
                                <asp:ListItem>673</asp:ListItem>
                                <asp:ListItem>674</asp:ListItem>
                                <asp:ListItem>675</asp:ListItem>
                                <asp:ListItem>676</asp:ListItem>
                                <asp:ListItem>677</asp:ListItem>
                                <asp:ListItem>678</asp:ListItem>
                                <asp:ListItem>679</asp:ListItem>
                                <asp:ListItem>680</asp:ListItem>
                                <asp:ListItem>681</asp:ListItem>
                                <asp:ListItem>682</asp:ListItem>
                                <asp:ListItem>683</asp:ListItem>
                                <asp:ListItem>685</asp:ListItem>
                                <asp:ListItem>686</asp:ListItem>
                                <asp:ListItem>687</asp:ListItem>
                                <asp:ListItem>688</asp:ListItem>
                                <asp:ListItem>689</asp:ListItem>
                                <asp:ListItem>690</asp:ListItem>
                                <asp:ListItem>691</asp:ListItem>
                                <asp:ListItem>692</asp:ListItem>
                                <asp:ListItem>850</asp:ListItem>
                                <asp:ListItem>852</asp:ListItem>
                                <asp:ListItem>853</asp:ListItem>
                                <asp:ListItem>855</asp:ListItem>
                                <asp:ListItem>856</asp:ListItem>
                                <asp:ListItem>870</asp:ListItem>
                                <asp:ListItem>880</asp:ListItem>
                                <asp:ListItem>886</asp:ListItem>
                                <asp:ListItem>960</asp:ListItem>
                                <asp:ListItem>961</asp:ListItem>
                                <asp:ListItem>962</asp:ListItem>
                                <asp:ListItem>963</asp:ListItem>
                                <asp:ListItem>964</asp:ListItem>
                                <asp:ListItem>965</asp:ListItem>
                                <asp:ListItem>966</asp:ListItem>
                                <asp:ListItem>967</asp:ListItem>
                                <asp:ListItem>968</asp:ListItem>
                                <asp:ListItem>970</asp:ListItem>
                                <asp:ListItem>971</asp:ListItem>
                                <asp:ListItem>972</asp:ListItem>
                                <asp:ListItem>973</asp:ListItem>
                                <asp:ListItem>974</asp:ListItem>
                                <asp:ListItem>975</asp:ListItem>
                                <asp:ListItem>976</asp:ListItem>
                                <asp:ListItem>977</asp:ListItem>
                                <asp:ListItem>992</asp:ListItem>
                                <asp:ListItem>993</asp:ListItem>
                                <asp:ListItem>994</asp:ListItem>
                                <asp:ListItem>995</asp:ListItem>
                                <asp:ListItem>996</asp:ListItem>
                                <asp:ListItem>998</asp:ListItem>
                                <asp:ListItem>1242</asp:ListItem>
                                <asp:ListItem>1246</asp:ListItem>
                                <asp:ListItem>1264</asp:ListItem>
                                <asp:ListItem>1268</asp:ListItem>
                                <asp:ListItem>1284</asp:ListItem>
                                <asp:ListItem>1340</asp:ListItem>
                                <asp:ListItem>1345</asp:ListItem>
                                <asp:ListItem>1441</asp:ListItem>
                                <asp:ListItem>1473</asp:ListItem>
                                <asp:ListItem>1599</asp:ListItem>
                                <asp:ListItem>1649</asp:ListItem>
                                <asp:ListItem>1664</asp:ListItem>
                                <asp:ListItem>1670</asp:ListItem>
                                <asp:ListItem>1671</asp:ListItem>
                                <asp:ListItem>1684</asp:ListItem>
                                <asp:ListItem>1758</asp:ListItem>
                                <asp:ListItem>1767</asp:ListItem>
                                <asp:ListItem>1784</asp:ListItem>
                                <asp:ListItem>1809</asp:ListItem>
                                <asp:ListItem>1868</asp:ListItem>
                                <asp:ListItem>1869</asp:ListItem>
                                <asp:ListItem>1876</asp:ListItem>
                                <asp:ListItem>00420</asp:ListItem>
                            </asp:DropDownList>
                        </span>
                        <asp:TextBox ID="txtFaxNumber" runat="server" CssClass="textfield" MaxLength="15"
                            Style="margin-left: 10px;"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtFaxNumber"
                            FilterType="Numbers">
                        </asp:FilteredTextBoxExtender>
                    </div>
                </li>
                <li class="value9">
                    <div class="col17">
                        <strong>Latitude / Longitude</strong></div>
                    <div class="col18">
                        <asp:Label ID="lblLatitude" runat="server" Text=""></asp:Label>
                        /
                        <asp:Label ID="lblLongitude" runat="server" Text=""></asp:Label></div>
                </li>
                <li class="value8">
                    <div class="col17">
                        <strong>Time Zone<span style="color: Red"> *</span> </strong>
                    </div>
                    <div class="col19">
                        <asp:DropDownList ID="drpTimeZone" runat="server" CssClass="NoClassApply">
                            <asp:ListItem Value="0">Select Time Zone</asp:ListItem>
                            <asp:ListItem Value="1">GMT-11:00 (Samoa Time)</asp:ListItem>
                            <asp:ListItem Value="2">GMT-10:00 (Hawaii Time)</asp:ListItem>
                            <asp:ListItem Value="3">GMT-08:00 (Alaska Daylight Time)</asp:ListItem>
                            <asp:ListItem Value="4">GMT-08:00 (Mexico Pacific Standard Time)</asp:ListItem>
                            <asp:ListItem Value="5">GMT-07:00 (Mountain Time)</asp:ListItem>
                            <asp:ListItem Value="6">GMT-07:00 (Mexico Mountain Standard Time)</asp:ListItem>
                            <asp:ListItem Value="7">GMT-07:00 (Pacific Daylight Time)</asp:ListItem>
                            <asp:ListItem Value="8">GMT-06:00 (Mountain Daylight Time)</asp:ListItem>
                            <asp:ListItem Value="9">GMT-06:00 (Mexico Standard Time)</asp:ListItem>
                            <asp:ListItem Value="10">GMT-06:00 (Central Time)</asp:ListItem>
                            <asp:ListItem Value="11">GMT-06:00 (Honduras Time)</asp:ListItem>
                            <asp:ListItem Value="12">GMT-05:00 (S. America Pacific Time)</asp:ListItem>
                            <asp:ListItem Value="13">GMT-05:00 (Central Daylight Time)</asp:ListItem>
                            <asp:ListItem Value="14">GMT-04:30 (S. America Western Time)</asp:ListItem>
                            <asp:ListItem Value="15">GMT-04:00 (Eastern Daylight Time)</asp:ListItem>
                            <asp:ListItem Value="16">GMT-04:00 (S. America Western Time)</asp:ListItem>
                            <asp:ListItem Value="17">GMT-04:00 (Eastern Daylight Time)</asp:ListItem>
                            <asp:ListItem Value="18">GMT-03:00 (S. America Eastern Standard Time)</asp:ListItem>
                            <asp:ListItem Value="19">GMT-03:00 (S. America Eastern Time)</asp:ListItem>
                            <asp:ListItem Value="20">GMT-03:00 (Atlantic Daylight Time)</asp:ListItem>
                            <asp:ListItem Value="21">GMT-03:00 (Greenland Standard Time)</asp:ListItem>
                            <asp:ListItem Value="22">GMT-03:00 (S. America Eastern Time)</asp:ListItem>
                            <asp:ListItem Value="23">GMT-02:30 (Newfoundland Daylight Time)</asp:ListItem>
                            <asp:ListItem Value="24">GMT-02:00 (Mid-Atlantic Standard Time)</asp:ListItem>
                            <asp:ListItem Value="25">GMT-01:00 (Azores Time)</asp:ListItem>
                            <asp:ListItem Value="26">GMT (Morocco Standard Time)</asp:ListItem>
                            <asp:ListItem Value="27">GMT (GMT Time)</asp:ListItem>
                            <asp:ListItem Value="28">GMT (Greenwich Time)</asp:ListItem>
                            <asp:ListItem Value="29">GMT+01:00 (Europe Time, Amsterdam)</asp:ListItem>
                            <asp:ListItem Value="30">GMT+01:00 (Europe Time, Berlin)</asp:ListItem>
                            <asp:ListItem Value="31">GMT+01:00 (Europe Time, Madrid)</asp:ListItem>
                            <asp:ListItem Value="32">GMT+01:00 (Europe Time, Paris)</asp:ListItem>
                            <asp:ListItem Value="33">GMT+01:00 (Europe Time, Rome)</asp:ListItem>
                            <asp:ListItem Value="34">GMT+01:00 (Sweden Time)</asp:ListItem>
                            <asp:ListItem Value="35">GMT+01:00 (West Africa Time)</asp:ListItem>
                            <asp:ListItem Value="36">GMT+02:00 (Jordan Standard Time)</asp:ListItem>
                            <asp:ListItem Value="37">GMT+02:00 (Greece Time)</asp:ListItem>
                            <asp:ListItem Value="38">GMT+02:00 (Egypt Time)</asp:ListItem>
                            <asp:ListItem Value="39">GMT+02:00 (Northern Europe Time)</asp:ListItem>
                            <asp:ListItem Value="40">GMT+02:00 (Eastern Europe Time)</asp:ListItem>
                            <asp:ListItem Value="41">GMT+02:00 (South Africa Time)</asp:ListItem>
                            <asp:ListItem Value="42">GMT+02:00 (Israel Standard Time)</asp:ListItem>
                            <asp:ListItem Value="43">GMT+02:00 (Nairobi Time)</asp:ListItem>
                            <asp:ListItem Value="44">GMT+02:00 (Saudi Arabia Time)</asp:ListItem>
                            <asp:ListItem Value="45">GMT+03:30 (Iran Standard Time)</asp:ListItem>
                            <asp:ListItem Value="46">GMT+04:00 (Arabian Time)</asp:ListItem>
                            <asp:ListItem Value="47">GMT+04:00 (Baku Standard Time)</asp:ListItem>
                            <asp:ListItem Value="48">GMT+04:00 (Russian Time)</asp:ListItem>
                            <asp:ListItem Value="49">GMT+04:30 (Afghanistan Time)</asp:ListItem>
                            <asp:ListItem Value="50">GMT+05:00 (West Asia Time)</asp:ListItem>
                            <asp:ListItem Value="51">GMT+05:00 (West Asia Time)</asp:ListItem>
                            <asp:ListItem Value="52">GMT+05:30 (Colombo Time)</asp:ListItem>
                            <asp:ListItem Value="53">GMT+05:30 (India Time)</asp:ListItem>
                            <asp:ListItem Value="54">GMT+05:45 (Nepal Time)</asp:ListItem>
                            <asp:ListItem Value="55">GMT+06:00 (Central Asia Time)</asp:ListItem>
                            <asp:ListItem Value="56">GMT+07:00 (Bangkok Time)</asp:ListItem>
                            <asp:ListItem Value="57">GMT+08:00 (China Time)</asp:ListItem>
                            <asp:ListItem Value="58">GMT+08:00 (Malaysia Time)</asp:ListItem>
                            <asp:ListItem Value="59">GMT+08:00 (Australia Western Time)</asp:ListItem>
                            <asp:ListItem Value="60">GMT+08:00 (Singapore Time)</asp:ListItem>
                            <asp:ListItem Value="61">GMT+08:00 (Taipei Time)</asp:ListItem>
                            <asp:ListItem Value="62">GMT+09:00 (Korea Time)</asp:ListItem>
                            <asp:ListItem Value="63">GMT+09:00 (Japan Time)</asp:ListItem>
                            <asp:ListItem Value="64">GMT+09:00 (Yakutsk Standard Time)</asp:ListItem>
                            <asp:ListItem Value="65">GMT+09:30 (Australia Central Time)</asp:ListItem>
                            <asp:ListItem Value="66">GMT+10:00 (Australia Eastern Time)</asp:ListItem>
                            <asp:ListItem Value="67">GMT+10:00 (West Pacific Time)</asp:ListItem>
                            <asp:ListItem Value="68">GMT+10:00 (Vladivostok Standard Time)</asp:ListItem>
                            <asp:ListItem Value="69">GMT+10:30 (Australia Central Daylight Time)</asp:ListItem>
                            <asp:ListItem Value="70">GMT+11:00 (Tasmania Daylight Time)</asp:ListItem>
                            <asp:ListItem Value="71">GMT+11:00 (Central Pacific Time)</asp:ListItem>
                            <asp:ListItem Value="72">GMT+11:00 (Australia Eastern Daylight Time)</asp:ListItem>
                            <asp:ListItem Value="73">GMT+12:00 (Fiji Time)</asp:ListItem>
                            <asp:ListItem Value="74">GMT+13:00 (New Zealand Daylight Time)</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </li>
                <li class="value9">
                    <div class="col17">
                        <strong>Renting time</strong></div>
                    <div class="col18">
                    </div>
                </li>
                <li class="value8">
                    <div class="col20">
                        <strong>&bull; Morning<span style="color: Red"> *</span> </strong>
                    </div>
                    <div class="col19">
                        <table>
                            <tr>
                                <td>
                                    <label>
                                        From</label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="droFromMorning" runat="server" CssClass="NoClassApply">
                                        <asp:ListItem Value="0">Select Time</asp:ListItem>
                                        <asp:ListItem Value="1">00:00 am</asp:ListItem>
                                        <asp:ListItem Value="2">00:30 am</asp:ListItem>
                                        <asp:ListItem Value="3">01:00 am</asp:ListItem>
                                        <asp:ListItem Value="4">01:30 am</asp:ListItem>
                                        <asp:ListItem Value="5">02:00 am</asp:ListItem>
                                        <asp:ListItem Value="6">02:30 am</asp:ListItem>
                                        <asp:ListItem Value="7">03:00 am</asp:ListItem>
                                        <asp:ListItem Value="8">03:30 am</asp:ListItem>
                                        <asp:ListItem Value="9">04:00 am</asp:ListItem>
                                        <asp:ListItem Value="10">04:30 am</asp:ListItem>
                                        <asp:ListItem Value="11">05:00 am</asp:ListItem>
                                        <asp:ListItem Value="12">05:30 am</asp:ListItem>
                                        <asp:ListItem Value="13">06:00 am</asp:ListItem>
                                        <asp:ListItem Value="14">06:30 am</asp:ListItem>
                                        <asp:ListItem Value="15">07:00 am</asp:ListItem>
                                        <asp:ListItem Value="16">07:30 am</asp:ListItem>
                                        <asp:ListItem Value="17">08:00 am</asp:ListItem>
                                        <asp:ListItem Value="18">08:30 am</asp:ListItem>
                                        <asp:ListItem Value="19">09:00 am</asp:ListItem>
                                        <asp:ListItem Value="20">09:30 am</asp:ListItem>
                                        <asp:ListItem Value="21">10:00 am</asp:ListItem>
                                        <asp:ListItem Value="22">10:30 am</asp:ListItem>
                                        <asp:ListItem Value="23">11:00 am</asp:ListItem>
                                        <asp:ListItem Value="24">11:30 am</asp:ListItem>
                                        <asp:ListItem Value="25">12:00 pm</asp:ListItem>
                                        <asp:ListItem Value="26">12:30 pm</asp:ListItem>
                                        <asp:ListItem Value="27">01:00 pm</asp:ListItem>
                                        <asp:ListItem Value="28">01:30 pm</asp:ListItem>
                                        <asp:ListItem Value="29">02:00 pm</asp:ListItem>
                                        <asp:ListItem Value="30">02:30 pm</asp:ListItem>
                                        <asp:ListItem Value="31">03:00 pm</asp:ListItem>
                                        <asp:ListItem Value="32">03:30 pm</asp:ListItem>
                                        <asp:ListItem Value="33">04:00 pm</asp:ListItem>
                                        <asp:ListItem Value="34">04:30 pm</asp:ListItem>
                                        <asp:ListItem Value="35">05:00 pm</asp:ListItem>
                                        <asp:ListItem Value="36">05:30 pm</asp:ListItem>
                                        <asp:ListItem Value="37">06:00 pm</asp:ListItem>
                                        <asp:ListItem Value="38">06:30 pm</asp:ListItem>
                                        <asp:ListItem Value="39">07:00 pm</asp:ListItem>
                                        <asp:ListItem Value="40">07:30 pm</asp:ListItem>
                                        <asp:ListItem Value="41">08:00 pm</asp:ListItem>
                                        <asp:ListItem Value="42">08:30 pm</asp:ListItem>
                                        <asp:ListItem Value="43">09:00 pm</asp:ListItem>
                                        <asp:ListItem Value="44">09:30 pm</asp:ListItem>
                                        <asp:ListItem Value="45">10:00 pm</asp:ListItem>
                                        <asp:ListItem Value="46">10:30 pm</asp:ListItem>
                                        <asp:ListItem Value="47">11:00 pm</asp:ListItem>
                                        <asp:ListItem Value="48">11:30 pm</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <label>
                                        &nbsp;To</label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="drpToMorning" runat="server" CssClass="NoClassApply">
                                        <asp:ListItem Value="0">Select Time</asp:ListItem>
                                        <asp:ListItem Value="1">00:00 am</asp:ListItem>
                                        <asp:ListItem Value="2">00:30 am</asp:ListItem>
                                        <asp:ListItem Value="3">01:00 am</asp:ListItem>
                                        <asp:ListItem Value="4">01:30 am</asp:ListItem>
                                        <asp:ListItem Value="5">02:00 am</asp:ListItem>
                                        <asp:ListItem Value="6">02:30 am</asp:ListItem>
                                        <asp:ListItem Value="7">03:00 am</asp:ListItem>
                                        <asp:ListItem Value="8">03:30 am</asp:ListItem>
                                        <asp:ListItem Value="9">04:00 am</asp:ListItem>
                                        <asp:ListItem Value="10">04:30 am</asp:ListItem>
                                        <asp:ListItem Value="11">05:00 am</asp:ListItem>
                                        <asp:ListItem Value="12">05:30 am</asp:ListItem>
                                        <asp:ListItem Value="13">06:00 am</asp:ListItem>
                                        <asp:ListItem Value="14">06:30 am</asp:ListItem>
                                        <asp:ListItem Value="15">07:00 am</asp:ListItem>
                                        <asp:ListItem Value="16">07:30 am</asp:ListItem>
                                        <asp:ListItem Value="17">08:00 am</asp:ListItem>
                                        <asp:ListItem Value="18">08:30 am</asp:ListItem>
                                        <asp:ListItem Value="19">09:00 am</asp:ListItem>
                                        <asp:ListItem Value="20">09:30 am</asp:ListItem>
                                        <asp:ListItem Value="21">10:00 am</asp:ListItem>
                                        <asp:ListItem Value="22">10:30 am</asp:ListItem>
                                        <asp:ListItem Value="23">11:00 am</asp:ListItem>
                                        <asp:ListItem Value="24">11:30 am</asp:ListItem>
                                        <asp:ListItem Value="25">12:00 pm</asp:ListItem>
                                        <asp:ListItem Value="26">12:30 pm</asp:ListItem>
                                        <asp:ListItem Value="27">01:00 pm</asp:ListItem>
                                        <asp:ListItem Value="28">01:30 pm</asp:ListItem>
                                        <asp:ListItem Value="29">02:00 pm</asp:ListItem>
                                        <asp:ListItem Value="30">02:30 pm</asp:ListItem>
                                        <asp:ListItem Value="31">03:00 pm</asp:ListItem>
                                        <asp:ListItem Value="32">03:30 pm</asp:ListItem>
                                        <asp:ListItem Value="33">04:00 pm</asp:ListItem>
                                        <asp:ListItem Value="34">04:30 pm</asp:ListItem>
                                        <asp:ListItem Value="35">05:00 pm</asp:ListItem>
                                        <asp:ListItem Value="36">05:30 pm</asp:ListItem>
                                        <asp:ListItem Value="37">06:00 pm</asp:ListItem>
                                        <asp:ListItem Value="38">06:30 pm</asp:ListItem>
                                        <asp:ListItem Value="39">07:00 pm</asp:ListItem>
                                        <asp:ListItem Value="40">07:30 pm</asp:ListItem>
                                        <asp:ListItem Value="41">08:00 pm</asp:ListItem>
                                        <asp:ListItem Value="42">08:30 pm</asp:ListItem>
                                        <asp:ListItem Value="43">09:00 pm</asp:ListItem>
                                        <asp:ListItem Value="44">09:30 pm</asp:ListItem>
                                        <asp:ListItem Value="45">10:00 pm</asp:ListItem>
                                        <asp:ListItem Value="46">10:30 pm</asp:ListItem>
                                        <asp:ListItem Value="47">11:00 pm</asp:ListItem>
                                        <asp:ListItem Value="48">11:30 pm</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </div>
                </li>
                <li class="value9">
                    <div class="col20">
                        <strong>&bull; Afternoon<span style="color: Red"> *</span> </strong>
                    </div>
                    <div class="col19">
                        <table>
                            <tr>
                                <td>
                                    <label>
                                        From</label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="droFromAfternoon" runat="server" CssClass="NoClassApply">
                                        <asp:ListItem Value="0">Select Time</asp:ListItem>
                                        <asp:ListItem Value="1">00:00 am</asp:ListItem>
                                        <asp:ListItem Value="2">00:30 am</asp:ListItem>
                                        <asp:ListItem Value="3">01:00 am</asp:ListItem>
                                        <asp:ListItem Value="4">01:30 am</asp:ListItem>
                                        <asp:ListItem Value="5">02:00 am</asp:ListItem>
                                        <asp:ListItem Value="6">02:30 am</asp:ListItem>
                                        <asp:ListItem Value="7">03:00 am</asp:ListItem>
                                        <asp:ListItem Value="8">03:30 am</asp:ListItem>
                                        <asp:ListItem Value="9">04:00 am</asp:ListItem>
                                        <asp:ListItem Value="10">04:30 am</asp:ListItem>
                                        <asp:ListItem Value="11">05:00 am</asp:ListItem>
                                        <asp:ListItem Value="12">05:30 am</asp:ListItem>
                                        <asp:ListItem Value="13">06:00 am</asp:ListItem>
                                        <asp:ListItem Value="14">06:30 am</asp:ListItem>
                                        <asp:ListItem Value="15">07:00 am</asp:ListItem>
                                        <asp:ListItem Value="16">07:30 am</asp:ListItem>
                                        <asp:ListItem Value="17">08:00 am</asp:ListItem>
                                        <asp:ListItem Value="18">08:30 am</asp:ListItem>
                                        <asp:ListItem Value="19">09:00 am</asp:ListItem>
                                        <asp:ListItem Value="20">09:30 am</asp:ListItem>
                                        <asp:ListItem Value="21">10:00 am</asp:ListItem>
                                        <asp:ListItem Value="22">10:30 am</asp:ListItem>
                                        <asp:ListItem Value="23">11:00 am</asp:ListItem>
                                        <asp:ListItem Value="24">11:30 am</asp:ListItem>
                                        <asp:ListItem Value="25">12:00 pm</asp:ListItem>
                                        <asp:ListItem Value="26">12:30 pm</asp:ListItem>
                                        <asp:ListItem Value="27">01:00 pm</asp:ListItem>
                                        <asp:ListItem Value="28">01:30 pm</asp:ListItem>
                                        <asp:ListItem Value="29">02:00 pm</asp:ListItem>
                                        <asp:ListItem Value="30">02:30 pm</asp:ListItem>
                                        <asp:ListItem Value="31">03:00 pm</asp:ListItem>
                                        <asp:ListItem Value="32">03:30 pm</asp:ListItem>
                                        <asp:ListItem Value="33">04:00 pm</asp:ListItem>
                                        <asp:ListItem Value="34">04:30 pm</asp:ListItem>
                                        <asp:ListItem Value="35">05:00 pm</asp:ListItem>
                                        <asp:ListItem Value="36">05:30 pm</asp:ListItem>
                                        <asp:ListItem Value="37">06:00 pm</asp:ListItem>
                                        <asp:ListItem Value="38">06:30 pm</asp:ListItem>
                                        <asp:ListItem Value="39">07:00 pm</asp:ListItem>
                                        <asp:ListItem Value="40">07:30 pm</asp:ListItem>
                                        <asp:ListItem Value="41">08:00 pm</asp:ListItem>
                                        <asp:ListItem Value="42">08:30 pm</asp:ListItem>
                                        <asp:ListItem Value="43">09:00 pm</asp:ListItem>
                                        <asp:ListItem Value="44">09:30 pm</asp:ListItem>
                                        <asp:ListItem Value="45">10:00 pm</asp:ListItem>
                                        <asp:ListItem Value="46">10:30 pm</asp:ListItem>
                                        <asp:ListItem Value="47">11:00 pm</asp:ListItem>
                                        <asp:ListItem Value="48">11:30 pm</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <label>
                                        &nbsp;To</label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="drpToAfternoon" runat="server" CssClass="NoClassApply">
                                        <asp:ListItem Value="0">Select Time</asp:ListItem>
                                        <asp:ListItem Value="1">00:00 am</asp:ListItem>
                                        <asp:ListItem Value="2">00:30 am</asp:ListItem>
                                        <asp:ListItem Value="3">01:00 am</asp:ListItem>
                                        <asp:ListItem Value="4">01:30 am</asp:ListItem>
                                        <asp:ListItem Value="5">02:00 am</asp:ListItem>
                                        <asp:ListItem Value="6">02:30 am</asp:ListItem>
                                        <asp:ListItem Value="7">03:00 am</asp:ListItem>
                                        <asp:ListItem Value="8">03:30 am</asp:ListItem>
                                        <asp:ListItem Value="9">04:00 am</asp:ListItem>
                                        <asp:ListItem Value="10">04:30 am</asp:ListItem>
                                        <asp:ListItem Value="11">05:00 am</asp:ListItem>
                                        <asp:ListItem Value="12">05:30 am</asp:ListItem>
                                        <asp:ListItem Value="13">06:00 am</asp:ListItem>
                                        <asp:ListItem Value="14">06:30 am</asp:ListItem>
                                        <asp:ListItem Value="15">07:00 am</asp:ListItem>
                                        <asp:ListItem Value="16">07:30 am</asp:ListItem>
                                        <asp:ListItem Value="17">08:00 am</asp:ListItem>
                                        <asp:ListItem Value="18">08:30 am</asp:ListItem>
                                        <asp:ListItem Value="19">09:00 am</asp:ListItem>
                                        <asp:ListItem Value="20">09:30 am</asp:ListItem>
                                        <asp:ListItem Value="21">10:00 am</asp:ListItem>
                                        <asp:ListItem Value="22">10:30 am</asp:ListItem>
                                        <asp:ListItem Value="23">11:00 am</asp:ListItem>
                                        <asp:ListItem Value="24">11:30 am</asp:ListItem>
                                        <asp:ListItem Value="25">12:00 pm</asp:ListItem>
                                        <asp:ListItem Value="26">12:30 pm</asp:ListItem>
                                        <asp:ListItem Value="27">01:00 pm</asp:ListItem>
                                        <asp:ListItem Value="28">01:30 pm</asp:ListItem>
                                        <asp:ListItem Value="29">02:00 pm</asp:ListItem>
                                        <asp:ListItem Value="30">02:30 pm</asp:ListItem>
                                        <asp:ListItem Value="31">03:00 pm</asp:ListItem>
                                        <asp:ListItem Value="32">03:30 pm</asp:ListItem>
                                        <asp:ListItem Value="33">04:00 pm</asp:ListItem>
                                        <asp:ListItem Value="34">04:30 pm</asp:ListItem>
                                        <asp:ListItem Value="35">05:00 pm</asp:ListItem>
                                        <asp:ListItem Value="36">05:30 pm</asp:ListItem>
                                        <asp:ListItem Value="37">06:00 pm</asp:ListItem>
                                        <asp:ListItem Value="38">06:30 pm</asp:ListItem>
                                        <asp:ListItem Value="39">07:00 pm</asp:ListItem>
                                        <asp:ListItem Value="40">07:30 pm</asp:ListItem>
                                        <asp:ListItem Value="41">08:00 pm</asp:ListItem>
                                        <asp:ListItem Value="42">08:30 pm</asp:ListItem>
                                        <asp:ListItem Value="43">09:00 pm</asp:ListItem>
                                        <asp:ListItem Value="44">09:30 pm</asp:ListItem>
                                        <asp:ListItem Value="45">10:00 pm</asp:ListItem>
                                        <asp:ListItem Value="46">10:30 pm</asp:ListItem>
                                        <asp:ListItem Value="47">11:00 pm</asp:ListItem>
                                        <asp:ListItem Value="48">11:30 pm</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </div>
                </li>
                <li class="value8">
                    <div class="col20">
                        <strong>&bull; Full day<span style="color: Red"> *</span> </strong>
                    </div>
                    <div class="col19">
                        <table>
                            <tr>
                                <td>
                                    <label>
                                        From</label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="drpFromFullday" runat="server" CssClass="NoClassApply">
                                        <asp:ListItem Value="0">Select Time</asp:ListItem>
                                        <asp:ListItem Value="1">00:00 am</asp:ListItem>
                                        <asp:ListItem Value="2">00:30 am</asp:ListItem>
                                        <asp:ListItem Value="3">01:00 am</asp:ListItem>
                                        <asp:ListItem Value="4">01:30 am</asp:ListItem>
                                        <asp:ListItem Value="5">02:00 am</asp:ListItem>
                                        <asp:ListItem Value="6">02:30 am</asp:ListItem>
                                        <asp:ListItem Value="7">03:00 am</asp:ListItem>
                                        <asp:ListItem Value="8">03:30 am</asp:ListItem>
                                        <asp:ListItem Value="9">04:00 am</asp:ListItem>
                                        <asp:ListItem Value="10">04:30 am</asp:ListItem>
                                        <asp:ListItem Value="11">05:00 am</asp:ListItem>
                                        <asp:ListItem Value="12">05:30 am</asp:ListItem>
                                        <asp:ListItem Value="13">06:00 am</asp:ListItem>
                                        <asp:ListItem Value="14">06:30 am</asp:ListItem>
                                        <asp:ListItem Value="15">07:00 am</asp:ListItem>
                                        <asp:ListItem Value="16">07:30 am</asp:ListItem>
                                        <asp:ListItem Value="17">08:00 am</asp:ListItem>
                                        <asp:ListItem Value="18">08:30 am</asp:ListItem>
                                        <asp:ListItem Value="19">09:00 am</asp:ListItem>
                                        <asp:ListItem Value="20">09:30 am</asp:ListItem>
                                        <asp:ListItem Value="21">10:00 am</asp:ListItem>
                                        <asp:ListItem Value="22">10:30 am</asp:ListItem>
                                        <asp:ListItem Value="23">11:00 am</asp:ListItem>
                                        <asp:ListItem Value="24">11:30 am</asp:ListItem>
                                        <asp:ListItem Value="25">12:00 pm</asp:ListItem>
                                        <asp:ListItem Value="26">12:30 pm</asp:ListItem>
                                        <asp:ListItem Value="27">01:00 pm</asp:ListItem>
                                        <asp:ListItem Value="28">01:30 pm</asp:ListItem>
                                        <asp:ListItem Value="29">02:00 pm</asp:ListItem>
                                        <asp:ListItem Value="30">02:30 pm</asp:ListItem>
                                        <asp:ListItem Value="31">03:00 pm</asp:ListItem>
                                        <asp:ListItem Value="32">03:30 pm</asp:ListItem>
                                        <asp:ListItem Value="33">04:00 pm</asp:ListItem>
                                        <asp:ListItem Value="34">04:30 pm</asp:ListItem>
                                        <asp:ListItem Value="35">05:00 pm</asp:ListItem>
                                        <asp:ListItem Value="36">05:30 pm</asp:ListItem>
                                        <asp:ListItem Value="37">06:00 pm</asp:ListItem>
                                        <asp:ListItem Value="38">06:30 pm</asp:ListItem>
                                        <asp:ListItem Value="39">07:00 pm</asp:ListItem>
                                        <asp:ListItem Value="40">07:30 pm</asp:ListItem>
                                        <asp:ListItem Value="41">08:00 pm</asp:ListItem>
                                        <asp:ListItem Value="42">08:30 pm</asp:ListItem>
                                        <asp:ListItem Value="43">09:00 pm</asp:ListItem>
                                        <asp:ListItem Value="44">09:30 pm</asp:ListItem>
                                        <asp:ListItem Value="45">10:00 pm</asp:ListItem>
                                        <asp:ListItem Value="46">10:30 pm</asp:ListItem>
                                        <asp:ListItem Value="47">11:00 pm</asp:ListItem>
                                        <asp:ListItem Value="48">11:30 pm</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <label>
                                        &nbsp;To</label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="drpToFullday" runat="server" CssClass="NoClassApply">
                                        <asp:ListItem Value="0">Select Time</asp:ListItem>
                                        <asp:ListItem Value="1">00:00 am</asp:ListItem>
                                        <asp:ListItem Value="2">00:30 am</asp:ListItem>
                                        <asp:ListItem Value="3">01:00 am</asp:ListItem>
                                        <asp:ListItem Value="4">01:30 am</asp:ListItem>
                                        <asp:ListItem Value="5">02:00 am</asp:ListItem>
                                        <asp:ListItem Value="6">02:30 am</asp:ListItem>
                                        <asp:ListItem Value="7">03:00 am</asp:ListItem>
                                        <asp:ListItem Value="8">03:30 am</asp:ListItem>
                                        <asp:ListItem Value="9">04:00 am</asp:ListItem>
                                        <asp:ListItem Value="10">04:30 am</asp:ListItem>
                                        <asp:ListItem Value="11">05:00 am</asp:ListItem>
                                        <asp:ListItem Value="12">05:30 am</asp:ListItem>
                                        <asp:ListItem Value="13">06:00 am</asp:ListItem>
                                        <asp:ListItem Value="14">06:30 am</asp:ListItem>
                                        <asp:ListItem Value="15">07:00 am</asp:ListItem>
                                        <asp:ListItem Value="16">07:30 am</asp:ListItem>
                                        <asp:ListItem Value="17">08:00 am</asp:ListItem>
                                        <asp:ListItem Value="18">08:30 am</asp:ListItem>
                                        <asp:ListItem Value="19">09:00 am</asp:ListItem>
                                        <asp:ListItem Value="20">09:30 am</asp:ListItem>
                                        <asp:ListItem Value="21">10:00 am</asp:ListItem>
                                        <asp:ListItem Value="22">10:30 am</asp:ListItem>
                                        <asp:ListItem Value="23">11:00 am</asp:ListItem>
                                        <asp:ListItem Value="24">11:30 am</asp:ListItem>
                                        <asp:ListItem Value="25">12:00 pm</asp:ListItem>
                                        <asp:ListItem Value="26">12:30 pm</asp:ListItem>
                                        <asp:ListItem Value="27">01:00 pm</asp:ListItem>
                                        <asp:ListItem Value="28">01:30 pm</asp:ListItem>
                                        <asp:ListItem Value="29">02:00 pm</asp:ListItem>
                                        <asp:ListItem Value="30">02:30 pm</asp:ListItem>
                                        <asp:ListItem Value="31">03:00 pm</asp:ListItem>
                                        <asp:ListItem Value="32">03:30 pm</asp:ListItem>
                                        <asp:ListItem Value="33">04:00 pm</asp:ListItem>
                                        <asp:ListItem Value="34">04:30 pm</asp:ListItem>
                                        <asp:ListItem Value="35">05:00 pm</asp:ListItem>
                                        <asp:ListItem Value="36">05:30 pm</asp:ListItem>
                                        <asp:ListItem Value="37">06:00 pm</asp:ListItem>
                                        <asp:ListItem Value="38">06:30 pm</asp:ListItem>
                                        <asp:ListItem Value="39">07:00 pm</asp:ListItem>
                                        <asp:ListItem Value="40">07:30 pm</asp:ListItem>
                                        <asp:ListItem Value="41">08:00 pm</asp:ListItem>
                                        <asp:ListItem Value="42">08:30 pm</asp:ListItem>
                                        <asp:ListItem Value="43">09:00 pm</asp:ListItem>
                                        <asp:ListItem Value="44">09:30 pm</asp:ListItem>
                                        <asp:ListItem Value="45">10:00 pm</asp:ListItem>
                                        <asp:ListItem Value="46">10:30 pm</asp:ListItem>
                                        <asp:ListItem Value="47">11:00 pm</asp:ListItem>
                                        <asp:ListItem Value="48">11:30 pm</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </div>
                </li>
                <li class="value9">
                    <div class="col17">
                        <strong>General Hotel / Conference Description<span style="color: Red"> *</span>
                        </strong>
                    </div>
                    <div class="col19">
                        <asp:TextBox ID="txtDescription" runat="server" class="textarea-new" TextMode="MultiLine"
                            MaxLength="250"></asp:TextBox>
                        <span id="lblCount" style="font-family: Arial; font-size: Smaller;">Description should
                            be less than 250 characters.</span>
                    </div>
                </li>
                <li class="value8">
                    <div class="col17">
                        <strong>Accepted Credit Cards<span style="color: Red"> *</span></strong></div>
                    <div class="col19">
                        <div class="rowElem">
                            <asp:CheckBoxList ID="chkCreditCard" runat="server" CellPadding="4">
                                <asp:ListItem Text="American Express" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Visa" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Master Card" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Diners" Value="3"></asp:ListItem>
                            </asp:CheckBoxList>
                        </div>
                    </div>
                </li>
                <li class="value9">
                    <div class="col17">
                        <strong>Themes<span style="color: Red"> *</span> </strong>
                    </div>
                    <div class="col19">
                        <asp:DropDownList ID="drpMeetingFacility" runat="server" CssClass="NoClassApply">
                            <asp:ListItem Value="0">Select Theme</asp:ListItem>
                            <asp:ListItem Text="Luxury" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Chique" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Trendy" Value="3"></asp:ListItem>
                            <asp:ListItem Text="Modern" Value="4"></asp:ListItem>
                            <asp:ListItem Text="Fashion" Value="5"></asp:ListItem>
                            <asp:ListItem Text="Budget" Value="6"></asp:ListItem>
                            <asp:ListItem Text="Rural" Value="7"></asp:ListItem>
                            <asp:ListItem Text="Classic" Value="8"></asp:ListItem>
                            <asp:ListItem Text="Golf Club" Value="9"></asp:ListItem>
                            <asp:ListItem Text="Castle" Value="10"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </li>
                <li class="value8" id="DivBedType" runat="server">
                    <div class="col17">
                        <strong>Bedrooms available <span style="color: Red">*</span></strong>
                    </div>
                    <div class="col19">
                        <span style="float: left">
                            <asp:DropDownList ID="drpBedroom" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpBedroom_SelectedIndexChanged"
                                CssClass="NoClassApply">
                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                <asp:ListItem Text="No" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                        </span>
                        <asp:Label ID="lblbedroom" runat="server" Text=""></asp:Label>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always" ChildrenAsTriggers="true">
                            <ContentTemplate>
                                <div class="sec1" id="divBed" runat="server">
                                    <asp:TextBox ID="txtNoofBed" runat="server" CssClass="textfield" MaxLength="4"></asp:TextBox>
                                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtNoofBed"
                                        ForeColor="White" Type="Integer" ValidationGroup="LOGIN" ErrorMessage="Maximum value should be 5000"
                                        MaximumValue="5000" MinimumValue="1" Text=""></asp:RangeValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Please insert number of beds"
                                        ControlToValidate="txtNoofBed" ForeColor="Red" ValidationGroup="LOGIN"></asp:RequiredFieldValidator>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtNoofBed"
                                        FilterType="Numbers">
                                    </asp:FilteredTextBoxExtender>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="drpBedroom" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </li>
                <li class="value10">
                    <div class="col21">
                        <div class="button_section">
                            <asp:LinkButton ID="btnSubmit" CssClass="select" runat="server" Text="Save" OnClick="btnSubmit_Click"
                                ValidationGroup="LOGIN" />
                            <span>or</span>
                            <asp:LinkButton ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="info-tab">
            <table width="100%" border="0" cellpadding="2">
                <tr>
                    <td width="3%" valign="top">
                        <div class="info-tab-white">
                        </div>
                    </td>
                    <td width="30%" valign="top">
                        Active Tab
                    </td>
                    <td width="3%" valign="top">
                        <div class="info-tab-blue">
                        </div>
                    </td>
                    <td width="30%" valign="top">
                        Tab with filled details
                    </td>
                    <td width="3%" valign="top">
                        <div class="info-tab-red">
                        </div>
                    </td>
                    <td width="30%" valign="top">
                        Tab with unfilled details/New tab
                    </td>
                </tr>
                <tr>
                    <td colspan="6" valign="top" style="padding: 10px 0px 0px 2px; font-weight: bold"
                        align="left">
                        <img src="../Images/help.jpg" />&nbsp;&nbsp;Save & Cancel button is not common for
                        all the tabs. Please save information for a tab before switching to another tab.
                    </td>
                </tr>
                <tr>
                    <td colspan="6" valign="top" style="padding: 10px 0px 0px 2px; font-weight: bold"
                        align="left">
                        <img src="../Images/help.jpg" />&nbsp;&nbsp;Please be sure you have filled description
                        in all the languages.
                    </td>
                </tr>
            </table>
        </div>
        <div class="button_sectionNext" id="divNext" runat="server" visible="false">
            <asp:LinkButton ID="btnNext" runat="server" class="RemoveCookie" Text="Next &gt;&gt;"
                OnClick="btnNext_Click" />
        </div>
    </asp:Panel>
    <div id="Loding_overlay">
        <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
        <span>Saving...</span></div>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {

            jQuery("#<%= btnSubmit.ClientID %>").bind("click", function () {
                if (jQuery("#<%= divmessage.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= divmessage.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                }
                var zipcode = jQuery("#<%= txtZipCode.ClientID %>").val();
                var isvalid = true;
                var errormessage = "";
                if (zipcode.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Zip code is required.";
                }
                var address = jQuery("#<%= txtAddress.ClientID %>").val();
                if (address.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Address is required.";
                    isvalid = false;
                }
                var phoneext = jQuery("#<%= drpExtPhone.ClientID %>").val();
                if (phoneext == "0") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select phone extension.";
                    isvalid = false;
                }
                var phone = jQuery("#<%= txtPhone.ClientID %>").val();
                if (phone.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Phone is required.";
                    isvalid = false;
                }
                var faxext = jQuery("#<%= drpExtFax.ClientID %>").val();
                if (faxext == "0") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select fax extension.";
                    isvalid = false;
                }
                var fax = jQuery("#<%= txtFaxNumber.ClientID %>").val();
                if (fax.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Fax number is required.";
                    isvalid = false;
                }
                var timez = jQuery("#<%= drpTimeZone.ClientID %>").val();
                //alert(timez);
                if (timez == "0") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select Time zone.";
                    isvalid = false;
                }
                var tomor = jQuery("#<%= drpToMorning.ClientID %>").val();
                if (tomor == "0") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select Morning To-Time.";
                    isvalid = false;
                }
                var toafter = jQuery("#<%= drpToAfternoon.ClientID %>").val();
                if (toafter == "0") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select Afternoon To-Time.";
                    isvalid = false;
                }
                var tofull = jQuery("#<%= drpToFullday.ClientID %>").val();
                if (tofull == "0") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select Full day To-Time.";
                    isvalid = false;
                }



                var frommor = jQuery("#<%= droFromMorning.ClientID %>").val();
                if (frommor == "0") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select Morning From-Time.";
                    isvalid = false;
                }
                var fromafter = jQuery("#<%= droFromAfternoon.ClientID %>").val();
                if (fromafter == "0") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select Afternoon From-Time.";
                    isvalid = false;
                }
                var fromfull = jQuery("#<%= drpFromFullday.ClientID %>").val();
                if (fromfull == "0") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select Full day From-Time.";
                    isvalid = false;
                }



                var description = jQuery("#<%= txtDescription.ClientID %>").val();
                if (description.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Description is required.";
                    isvalid = false;
                }
                else if (description.length > 250) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Description must have 250 character.";
                    isvalid = false;
                }
                var creditcardcheck = jQuery("#<%= chkCreditCard.ClientID %>").find("input:checkbox:checked").length;
                if (creditcardcheck == 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select atleast one credit card.";
                    isvalid = false;
                }
                var bedroom = jQuery("#<%= drpBedroom.ClientID %>").val();
                if (bedroom == "0") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select bedroom";
                    isvalid = false;
                }
                var bedchange = jQuery("#<%= drpBedroom.ClientID %>").val();
                if (bedchange == "1") {
                    var num = jQuery("#<%= txtNoofBed.ClientID %>").val();
                    if (num.trim().length <= 0) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += "Enter number of bedroom.";
                        isvalid = false;
                    }
                    if (parseInt(num) <= 0) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += "Please enter number of bedroom greater than 0.";
                        isvalid = false;
                    }
                    else if (parseInt(num) > 5000) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += "Please enter number of bedroom less than 5000.";
                        isvalid = false;
                    }
                }
                //                var themecheck = jQuery("#<%= drpMeetingFacility.ClientID %>").val();
                //                                if (themecheck == "" || themecheck == null) {
                //                                    if (errormessage.length > 0) {
                //                                        errormessage += "<br/>";
                //                                    } 
                //                                    errormessage += "Please select theme.";
                //                                    isvalid = false;
                //                                }
                var fromfull = jQuery("#<%= drpMeetingFacility.ClientID %>").val();
                if (fromfull == "0") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Please select theme";
                    isvalid = false;
                }

                if (!isvalid) {
                    jQuery(".error").show();
                    jQuery(".error").html(errormessage);
                    var offseterror = jQuery(".error").offset();
                    jQuery("body").scrollTop(offseterror.top);
                    jQuery("html").scrollTop(offseterror.top);
                    return false;
                }
                jQuery(".error").html("");
                jQuery(".error").hide();
                jQuery("#Loding_overlay span").html("Saving...");
                jQuery("#Loding_overlay").show();
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            });
        });
    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#<%= btnNext.ClientID %>").bind("click", function () {
                jQuery(".error").html("");
                jQuery(".error").hide();
                jQuery("#Loding_overlay span").html("Loading...");
                jQuery("#Loding_overlay").show();
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            });
        });
    </script>
    <script language="javascript" type="text/javascript">
        //This function using for get language id by click on language tab.
        function GetLanguageID(languageID) {
            document.getElementById("<%= hdnIDS.ClientID %>").value = languageID.toString();
            jQuery("#Loding_overlay span").html("Loading...");
            jQuery("#Loding_overlay").show();
            jQuery.ajax({
                type: "POST",
                url: "GeneralInfo.aspx/GetDescByLanguageID",
                data: "{'languageID':'" + parseInt(languageID) + "','hotelID':'"+<%=Session["CurrentHotelID"] %>+"'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {                    
                    var result = jQuery.parseJSON(msg.d)
                    if (result != null) {
                        document.getElementById("<%= txtAddress.ClientID %>").value = result.Address == undefined ? "" : result.Address;
                        if (result.Description != null) {
                            document.getElementById("<%= txtDescription.ClientID %>").value = result.Description;
                        }
                        else {
                            document.getElementById("<%= txtDescription.ClientID %>").value = "";
                        }
                        document.getElementById("<%= hdnHotelDescID.ClientID %>").value = result.Id;
                        jQuery("#<%= divmessage.ClientID %>").hide();
                    } else {
                        //document.getElementById("<%= txtAddress.ClientID %>").value = "";
                        document.getElementById("<%= txtDescription.ClientID %>").value = "";
                        document.getElementById("<%= hdnHotelDescID.ClientID %>").value = "0";
                        jQuery("#<%= divmessage.ClientID %>").hide();
                    }
                    jQuery("#Loding_overlay span").html("Loading...");
                    jQuery("#Loding_overlay").hide();
                }

            });
            
        }

        function ValidateModuleList(source, args) {
            var chkListModules = document.getElementById('<%= chkCreditCard.ClientID %>');
            var chkListinputs = chkListModules.getElementsByTagName("input");
            for (var i = 0; i < chkListinputs.length; i++) {
                if (chkListinputs[i].checked) {
                    args.IsValid = true;
                    return;
                }
            }
            args.IsValid = false;
        }

        jQuery(document).ready(function () {
            var MaxLength = 250;
//            jQuery('#<%=txtDescription.ClientID %>').keypress(function (e) {
//                var keycode = e.keyCode ? e.keyCode : e.which;
//                if (jQuery(this).val().length >= MaxLength && keycode!=8) {
//                    e.preventDefault();
//                }
//            });
            jQuery('#<%=txtDescription.ClientID %>').keyup(function (e) {
                var total = parseInt(jQuery(this).val().length);
                jQuery("#lblCount").html('Characters entered <b>' + total + '</b> out of 250.');
            });
        });

        jQuery(document).ready(function () {
            var MaxLength = 100;
            jQuery('#<%=txtAddress.ClientID %>').keypress(function (e) {
                var keycode = e.keyCode ? e.keyCode : e.which;
                if (jQuery(this).val().length >= MaxLength && keycode != 8) {
                    e.preventDefault();
                }
            });           
        }); 
    
    </script>
</asp:Content>
