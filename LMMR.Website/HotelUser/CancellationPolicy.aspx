﻿<%@ Page Title="Hotel User - Cancellation Policy" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master"
    AutoEventWireup="true" CodeFile="CancellationPolicy.aspx.cs" Inherits="HotelUser_CancellationPolicy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" runat="Server">
    <h1>
        Cancellation policy : <b>
            <asp:Label ID="lblHotel" runat="server" Text=""></asp:Label></b></h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" runat="Server">
    <div class="superadmin-mainbody-inner">
        <div class="superadmin-mainbody-sub4" style="display: none">
            <asp:Label ID="lblMessage" runat="server" Text="Policy not exist" ForeColor="#e48249"></asp:Label>
        </div>
    </div>
    <div class="superadmin-mainbody-inner" id="divPolicy" runat="server">
        <div class="superadmin-mainbody-sub4">
            Name : <b>
                <asp:Label ID="lblName" runat="server" Text=""></asp:Label></b>
        </div>
        <div class="superadmin-mainbody-sub4">
            <div class="superadmin-mainbody-sub4-left">
                User can cancel a booking : <b>
                    <asp:Label ID="lblCalcellationLimit" runat="server" Text=""></asp:Label></b>
                days before arrival
            </div>
        </div>
        <div class="superadmin-mainbody-sub4">
            <b>Section 1</b> Between day <b>
                <asp:Label ID="lblTimeFrameMax1" runat="server" Text=""></asp:Label></b> and
            day <b>
                <asp:Label ID="lblTimeFrameMin1" runat="server" Text=""></asp:Label></b>
        </div>
        <div class="cancelation-tabbody1">
            <div id="TabbedPanels2" class="TabbedPanels">
                <div class="TabbedPanelsContentGroup">
                    <div class="TabbedPanelsContent">
                        <div class="tab-textarea1">
                            <asp:Label ID="lblSection1" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="superadmin-mainbody-sub4">
            <b>Section 2</b> Between day <b>
                <asp:Label ID="lblTimeFrameMax2" runat="server" Text=""></asp:Label></b> and
            day <b>
                <asp:Label ID="lblTimeFrameMin2" runat="server" Text=""></asp:Label></b>
        </div>
        <div class="cancelation-tabbody1">
            <div id="TabbedPanels3" class="TabbedPanels">
                <div class="TabbedPanelsContentGroup">
                    <div class="TabbedPanelsContent">
                        <div class="tab-textarea1">
                            <asp:Label ID="lblSection2" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="superadmin-mainbody-sub4">
            <b>Section 3</b> Between day <b>
                <asp:Label ID="lblTimeFrameMax3" runat="server" Text=""></asp:Label></b> and
            day <b>
                <asp:Label ID="lblTimeFrameMin3" runat="server" Text=""></asp:Label></b>
        </div>
        <div class="cancelation-tabbody1">
            <div id="TabbedPanels4" class="TabbedPanels">
                <div class="TabbedPanelsContentGroup">
                    <div class="TabbedPanelsContent">
                        <div class="tab-textarea1">
                            <asp:Label ID="lblSection3" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="superadmin-mainbody-sub4">
            <b>Section 4</b> Between day <b>
                <asp:Label ID="lblTimeFrameMax4" runat="server" Text=""></asp:Label></b> and
            day <b>
                <asp:Label ID="lblTimeFrameMin4" runat="server" Text=""></asp:Label></b>
        </div>
        <div class="cancelation-tabbody1">
            <div id="Div1" class="TabbedPanels">
                <div class="TabbedPanelsContentGroup">
                    <div class="TabbedPanelsContent">
                        <div class="tab-textarea1">
                            <asp:Label ID="lblSection4" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div style="padding-left: 10px;" runat="server" id="divMessage">
                 In case you want to change or update the cancellation & change policy, please contact
                our Support team at <a href="mailto:hotelsupport@lastminutemeetingroom.com">hotelsupport@lastminutemeetingroom.com</a>
              
            </div>
            <br />
        </div>
    </div>
    <div class="button_sectionNext" id="divNext" runat="server">
        <asp:LinkButton ID="btnNext" runat="server" class="RemoveCookie" Text="Next &gt;&gt;"
            OnClick="btnNext_Click" />
    </div>
    <div id="Loding_overlaySec">
        <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
        <span>Loading...</span></div>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#<%= btnNext.ClientID %>").bind("click", function () {
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                jQuery("#Loding_overlaySec span").html("Loading...");
                jQuery("#Loding_overlaySec").show();
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            });
        });
    </script>
</asp:Content>
