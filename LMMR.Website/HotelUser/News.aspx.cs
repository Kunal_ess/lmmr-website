﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using log4net.Config;
#endregion


public partial class HotelUser_News : System.Web.UI.Page
{
    #region Variables and properties
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_News));
    #endregion

    #region PageLoad
    /// <summary>
    /// Initilize the controls at pageload.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["LinkID"] = "lnkNews";
        if (!Page.IsPostBack)
        {

        }
    }
    #endregion
}