﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Data;
using System.Configuration;
using log4net;
using log4net.Config;
#endregion


public partial class HotelUser_FinanceInvoice : System.Web.UI.Page
{
    #region variable declaration
    FinanceInvoice objFinance = new FinanceInvoice();
    VList<Viewbookingrequest> vlistreq;
    int hotelPid = 0;
    VList<ViewBookingHotels> vlist;
    ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
    HotelInfo ObjHotelinfo = new HotelInfo();
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_FinanceInvoice));
    HotelManager objHotel = new HotelManager();
    decimal TotalBookingValue = 0;
    ManageOthers objOthers = new ManageOthers();
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl + "/";
        }
    }
    #endregion

    #region PageLoad
    /// <summary>
    /// On page load we manage all the containts and Apply paging if there is a post back.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Session for Hotelname        
            if (Session["CurrentHotelID"] == null)
            {
                Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                Session.Abandon();
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "login/english");
                }
            }

            hotelPid = Convert.ToInt32(Session["CurrentHotelID"]);
            Session["LinkID"] = "lnkFinance";
            //Session for Hotelnames  
            if (IsPostBack)
            {
                //Call paging function 
                ApplyPaging();
            }
            else if (!IsPostBack)
            {
                //For hide div messages
                divmessage.Style.Add("display", "none");
                divmessage.Attributes.Add("class", "error");

                //Bind Finance invoice Grid
                BindFinanceInvoice();
                //Bind Invoice date and All hotel name on gridview header
                BindDropdownlist();               

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Additional Methods
    /// <summary>
    //Bind finance invoice details of selected hotel into list view
    /// </summary> 
    void BindFinanceInvoice()
    {
        try
        {
            string whereClause = "HotelId=" + hotelPid + " and " + "InvoiceSent=1";            
            TList<Invoice> lstInvoice = objFinance.GetinvoicebyClient(whereClause);
            grvFinanceInvoice.DataSource = lstInvoice;          
            grvFinanceInvoice.DataBind();
            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
  
    /// <summary>
    //Bind data in dropdown for Date and all hotelname
    /// </summary>
    void BindDropdownlist()
    {
        try
        {
            GridViewRow header = grvFinanceInvoice.HeaderRow;

            //// Retrieve control for period
            DropDownList ddlPeriod = header.FindControl("drpPeriod") as DropDownList;
            ddlPeriod.DataValueField = "Display";
            ddlPeriod.DataTextField = "Display";
            ddlPeriod.DataSource = objFinance.GetInvoiceByHotelid(hotelPid).Select(a => new { Display = Convert.ToDateTime(a.DateFrom).ToString("dd/MM/yyyy") + " - "+ Convert.ToDateTime(a.DateTo).ToString("dd/MM/yyyy") });
            ddlPeriod.DataBind();
            ddlPeriod.Items.Insert(0, new ListItem("Select Period", ""));

            //// Retrieve control for period
            TList<Hotel> hotel = objHotel.GetHotelByClientId(Session["CurrentUserID"] == null ? 0 : Convert.ToInt64(Session["CurrentUserID"]));//objFinance.GetInvoiceByAllHotel();
            DropDownList ddlHotelName = header.FindControl("drpHotelName") as DropDownList;
            ddlHotelName.DataValueField = "Id";
            ddlHotelName.DataTextField = "Name";
            ddlHotelName.DataSource = hotel;
            ddlHotelName.DataBind();
            if (hotel.Count > 1)
            {
                ddlHotelName.Items.Insert(0, new ListItem("All Hotel", ""));
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }    

    /// <summary>
    //Start Get Bedroom details
    /// </summary>            
    protected void FillbedroomDetails()
    {
        try
        {
            TList<BookedBedRoom> objBookedBR = objViewBooking_Hotel.getbookedBedroom(Convert.ToInt64(Convert.ToString(ViewState["BookingID"])));

            foreach (BookedBedRoom br in objBookedBR)
            {
                lblBedroomType.Text = Enum.GetName(typeof(BedRoomType), br.BedRoomIdSource.Types); // Enum.GetName();
                lblChekOut.Text = String.Format("{0:dd/MM/yyyy}", br.CheckOut);
                lblCheckIn.Text = String.Format("{0:dd/MM/yyyy}", br.CheckIn);
                lblpersonName.Text = br.PersonName;
                lblNote.Text = br.Note;

                lblbedroomtotalprice.Text = String.Format("{0:#,##,##0.00}", br.Total);

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }

    /// <summary>
    //Call shorting function
    /// </summary>      
    protected void grvFinanceInvoice_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            ConvertSortDirectionToSql(e.SortExpression, e.SortDirection);
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    //Function for shorting on invoice date
    /// </summary>      
    public static SortDirection s = SortDirection.Ascending;
    private void ConvertSortDirectionToSql(string OrderBy, SortDirection sortDirection)
    {
        switch (s)
        {
            case SortDirection.Ascending:
                OrderBy += " ASC";
                s = SortDirection.Descending;
                break;

            case SortDirection.Descending:
                OrderBy += " DESC";
                s = SortDirection.Ascending;
                break;
        }
        grvFinanceInvoice.DataSource = objFinance.GetInvoiceByHotelid(hotelPid, OrderBy);
        grvFinanceInvoice.DataBind();
        BindDropdownlist();
        ApplyPaging();
    }


    /// <summary>
    //For Bind Plan,Images,CreditNote
    /// </summary>    
    protected void grvFinanceInvoice_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            foreach (GridViewRow gr in grvFinanceInvoice.Rows)
            {
                if (((Label)gr.FindControl("lblPaid")).Text == "True")
                {
                    ((Image)gr.FindControl("imgStatus")).Visible = true;
                }
                else
                {
                    ((Label)gr.FindControl("lblPaid")).Visible = true;
                    ((Label)gr.FindControl("lblPaid")).Text = "Not Paid";
                }

                HiddenField hdf = ((HiddenField)gr.FindControl("hdfImage"));
                HyperLink PlanView = (HyperLink)gr.FindControl("linkviewPlan");
                HiddenField hdfcreditnote = ((HiddenField)gr.FindControl("hdfCreditnote"));
                HyperLink creditnote = ((HyperLink)gr.FindControl("linkCreditnote"));
                if (hdf.Value != "")
                {
                    PlanView.NavigateUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "InvoiceFile/" + hdf.Value;
                }
                else
                {
                    PlanView.Visible = false;
                }
                if (hdfcreditnote.Value != "")
                {
                    creditnote.NavigateUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "InvoiceFile/" + hdf.Value;
                }
                else
                {
                    creditnote.Visible = false;
                }

                //For adding channel filed
                LinkButton lnbBookedId = (LinkButton)gr.FindControl("lblRefNo");
                if (lnbBookedId.ToolTip != "")
                {
                    Booking onjbooking = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(lnbBookedId.ToolTip));
                    ((Label)gr.FindControl("lblChannel")).Text = onjbooking.Channel;
                }
                //For adding channel filed

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    //Bind Extra item grid for booking details
    /// </summary>  
    protected void rpmain_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                Label lblbmrid = e.Item.FindControl("lblbmrid") as Label;
                Label lblPackageName = e.Item.FindControl("lblPackageName") as Label;
                Label lbltotalpackage = e.Item.FindControl("lbltotalpackage") as Label;
                Label lblextratotal = e.Item.FindControl("lblextratotal") as Label;
                GridView grdDetailspackage = e.Item.FindControl("grdDetailspackage") as GridView;
                GridView grdextra = e.Item.FindControl("grdextra") as GridView;
                GridView grdequipment = e.Item.FindControl("grdequipment") as GridView;

                TList<BuildPackageConfigure> obbuildpack = objViewBooking_Hotel.getPackageDetails(Convert.ToInt64(lblbmrid.Text.Replace("'", "")));
                TList<BuildMeetingConfigure> objBuildMeetingConfigure = objViewBooking_Hotel.getextra(Convert.ToInt64(lblbmrid.Text.Replace("'", "")));

                grdextra.DataSource = objBuildMeetingConfigure.FindAll(a => a.PackageIdSource.IsExtra == true);
                grdextra.DataBind();
                grdequipment.DataSource = objBuildMeetingConfigure.FindAll(a => Convert.ToInt32(a.PackageIdSource.ItemType) == (int)ItemType.Equipment);
                grdequipment.DataBind();
                int cntextra = 0;
                foreach (BuildMeetingConfigure bmc in objBuildMeetingConfigure.FindAll(a => Convert.ToInt32(a.PackageIdSource.ItemType) == (int)ItemType.Equipment))
                {
                    cntextra += Convert.ToInt32(bmc.TotalPrice);

                }
                lblextratotal.Text = String.Format("{0:#,##,##0.00}", cntextra);
                foreach (BuildPackageConfigure bpc in obbuildpack)
                {

                    lblPackageName.Text = bpc.PackageItemIdSource.PackageName;
                    lbltotalpackage.Text = String.Format("{0:#,##,##0.00}", bpc.TotalPrice);


                    TList<BuildPackageConfigureDesc> obdesc = objViewBooking_Hotel.getpackagedetails(bpc.Id);
                    grdDetailspackage.DataSource = obdesc;
                    grdDetailspackage.DataBind();

                }


            }
        }        
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }

    #endregion    

    #region Search Methods 
    /// <summary>
    //Start Search function for client name and from-to date
    /// </summary>    
    protected void lbtSearch_Click(object sender, EventArgs e)
    {
        try
        {
            //Start Searching for from-to date and as per hotel also       
            Divdetails.Visible = false;
            
            if (txtFromdate.Text != "dd/mm/yy" && txtTodate.Text != "dd/mm/yy")
            {                
                DateTime fromdate = new DateTime(Convert.ToInt32(txtFromdate.Text.Split('/')[2]), Convert.ToInt32(txtFromdate.Text.Split('/')[1]), Convert.ToInt32(txtFromdate.Text.Split('/')[0]));
                DateTime todate = new DateTime(Convert.ToInt32(txtTodate.Text.Split('/')[2]), Convert.ToInt32(txtTodate.Text.Split('/')[1]), Convert.ToInt32(txtTodate.Text.Split('/')[0]));
                string whereclause = "DueDate between '" + fromdate.ToString().Replace("'", "") + "' and '" + todate.ToString().Replace("'", "") + "' and " + "HotelId=" + hotelPid + "" + "InvoiceSent=1";                
                TList<Invoice> lstInvoice = objFinance.GetinvoicebyClient(whereclause);
                grvFinanceInvoice.DataSource = lstInvoice;
                grvFinanceInvoice.DataBind(); 
                grvFinanceInvoice.Visible = true;
                if (lstInvoice.Count > 0)
                {
                    BindDropdownlist();
                    divmessage.Style.Add("display", "none");
                    divmessage.Attributes.Add("class", "error");
                }
                else
                {
                    divmessage.Style.Add("display", "none");
                    divmessage.Attributes.Add("class", "error");                    
                }
                return;
            }
            else if (txtFromdate.Text != "dd/mm/yy" || txtTodate.Text != "dd/mm/yy")
            {
                divmessage.Style.Add("display", "block");
                divmessage.Attributes.Add("class", "error");
                divmessage.InnerHtml = "From date and To date is required";
                return;
            }
            else
            {
                divmessage.Style.Add("display", "block");
                divmessage.Attributes.Add("class", "error");
                divmessage.InnerHtml = "From date and To date is required";
            }
        }        
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        //end Searching for from-to date and as per hotel also
    }

    /// <summary>
    //Generate index for paging
    /// </summary>       
    private void ApplyPaging()
    {
        GridViewRow row = grvFinanceInvoice.TopPagerRow;
        if (row != null)
        {
            PlaceHolder ph;
            LinkButton lnkPaging;
            LinkButton lnkPrevPage;
            LinkButton lnkNextPage;
            lnkPrevPage = new LinkButton();
            lnkPrevPage.CssClass = "pre";
            lnkPrevPage.Width = Unit.Pixel(73);
            lnkPrevPage.CommandName = "Page";
            lnkPrevPage.CommandArgument = "prev";
            ph = (PlaceHolder)row.FindControl("ph");
            ph.Controls.Add(lnkPrevPage);
            if (grvFinanceInvoice.PageIndex == 0)
            {
                lnkPrevPage.Enabled = false;

            }
            for (int i = 1; i <= grvFinanceInvoice.PageCount; i++)
            {
                lnkPaging = new LinkButton();
                if (ViewState["CurrentPage"] != null)
                {
                    if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                    {
                        lnkPaging.CssClass = "pag2";
                    }
                    else
                    {
                        lnkPaging.CssClass = "pag";
                    }
                }
                else
                {
                    if (i == 1)
                    {
                        lnkPaging.CssClass = "pag2";
                    }
                    else
                    {
                        lnkPaging.CssClass = "pag";
                    }
                }
                lnkPaging.Width = Unit.Pixel(16);
                lnkPaging.Text = i.ToString();
                lnkPaging.CommandName = "Page";
                lnkPaging.CommandArgument = i.ToString();
                if (i == grvFinanceInvoice.PageIndex + 1)
                    ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPaging);
            }
            lnkNextPage = new LinkButton();
            lnkNextPage.CssClass = "nex";
            lnkNextPage.Width = Unit.Pixel(42);
            lnkNextPage.CommandName = "Page";
            lnkNextPage.CommandArgument = "next";
            ph = (PlaceHolder)row.FindControl("ph");
            ph.Controls.Add(lnkNextPage);
            ph = (PlaceHolder)row.FindControl("ph");
            if (grvFinanceInvoice.PageIndex == grvFinanceInvoice.PageCount - 1)
            {
                lnkNextPage.Enabled = false;

            }
        }

    }


    /// <summary>
    //Get result when clicking on Period dropdown in Finance gridview header control
    /// </summary>           
    protected void drpPeriod_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");

            GridViewRow header = grvFinanceInvoice.HeaderRow;

            //// Retrieve control
            DropDownList ddlPeriod = header.FindControl("drpPeriod") as DropDownList;

            if (ddlPeriod.SelectedIndex == 0)
            {
                BindFinanceInvoice();
                BindDropdownlist();
                return;
            }
            else
            {
                grvFinanceInvoice.DataSource = objFinance.GetDateSearch(hotelPid, ddlPeriod.SelectedItem.Text.Replace("'", ""));
                grvFinanceInvoice.DataBind();
                BindDropdownlist();
            }           
        }        
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    //Get result when clicking on hotel name dropdown in Finance gridview header control
    /// </summary>    
    protected void drpHotelName_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            
            GridViewRow header = grvFinanceInvoice.HeaderRow;
            //// Retrieve control
            DropDownList drpHotelName = header.FindControl("drpHotelName") as DropDownList;
            if (drpHotelName.SelectedIndex == 0)
            {
                grvFinanceInvoice.DataSource = objFinance.GetInvoiceByHotelName();
                grvFinanceInvoice.DataBind();
            }
            else
            {
                if (objFinance.GetInvoiceByHotelid(Convert.ToInt32(drpHotelName.SelectedValue)).Count > 0)
                {
                    grvFinanceInvoice.DataSource = objFinance.GetInvoiceByHotelid(Convert.ToInt32(drpHotelName.SelectedValue));
                    grvFinanceInvoice.DataBind();
                }
                else
                {
                    grvFinanceInvoice.Visible = false;
                    divmessage.Style.Add("display", "block");
                    divmessage.Attributes.Add("class", "error");
                    divmessage.InnerHtml = "No record Found!";
                }
            }
            BindDropdownlist();
            drpHotelName.SelectedIndex = drpHotelName.Items.IndexOf(drpHotelName.Items.FindByText(drpHotelName.SelectedItem.Text.Replace("'", "")));
        }        
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }    

    

    #endregion
    
    #region Finance Invoice page index
    /// <summary>
    //Paging index for finance invoice grid
    /// </summary>    
    protected void grvFinanceInvoice_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grvFinanceInvoice.PageIndex = e.NewPageIndex;
        ViewState["CurrentPage"] = e.NewPageIndex;
        BindFinanceInvoice();
        BindDropdownlist();
    }
    #endregion
   
    #region Get Booking Information
    /// <summary>
    //Start Get all information of booking details when clicking on Invoice Number
    /// </summary>        
    protected void lblRefNo_Click(object sender, EventArgs e)
    {
        try
        {
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
            LinkButton lnk = (LinkButton)sender;

            ViewState["BookingID"] = lnk.ToolTip;
            Invoice IvnHId = objFinance.GetInvoiceByID(Convert.ToInt32(lnk.Text.Replace("'", "")));            
            string whereclaus = "hotelid=" + IvnHId.HotelId + " and id=" + lnk.ToolTip; // change hotel ID make it dynamic
            string orderby = ViewBookingHotelsColumn.DepartureDate + " ASC";
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclaus, orderby);

            Hotel htl = objViewBooking_Hotel.GetbyHotelID((Convert.ToInt32(lnk.ToolTip)));

            List<ViewMeetingroom> vlistmr = objViewBooking_Hotel.viewMR(Convert.ToInt64(Convert.ToString(ViewState["BookingID"])));

            rpmain.DataSource = vlistmr;
            rpmain.DataBind();

            Users obj = objViewBooking_Hotel.GetUser(Convert.ToInt64(Convert.ToString(ViewState["BookingID"])));
            TList<UserDetails> objDetails = objViewBooking_Hotel.getuserdetails(obj.UserId);
            lblConatctpersonEmail.HRef = "mailto:" + obj.EmailId;
            lblConatctpersonEmail.InnerText = obj.EmailId;
            lblContactPerson.Text = obj.FirstName + " " + obj.LastName;
            if (objDetails.Count > 0)
            {
                lblContactAddress.Text = objDetails[0].Address;
                lblContactPhone.Text = objDetails[0].Phone;
            }

            FillbedroomDetails();
            foreach (Viewbookingrequest vi in vlistreq)
            {
                #region Duration dates
                TList<BookedMeetingRoom> bookedMR = objViewBooking_Hotel.getbookedmeetingroom(Convert.ToInt64(lnk.Text.Replace("'", "")));
                #region ArrivalDate
                foreach (BookedMeetingRoom br in bookedMR)
                {
                    string day = "";
                    if (br.MeetingDay == 0)
                        day = "Morning";
                    if (br.MeetingDay == 1)
                        day = "Afternoon";
                    if (br.MeetingDay == 2)
                        day = "Full Day";

                    DateTime dtmeetingdt = (DateTime)br.MeetingDate;
                    lblBookedDays.Text = dtmeetingdt.ToString("dd/MM/yy") + " " + day;
                    lblFromDt.Text = dtmeetingdt.ToString("dd/MM/yy");
                    break;
                }
                #endregion

                #region DepartureDate
                if (bookedMR.Count == 1)
                {
                    foreach (BookedMeetingRoom br in bookedMR)
                    {
                        DateTime dtmeetingdt = (DateTime)br.MeetingDate;
                        lblToDate.Text = dtmeetingdt.ToString("dd/MM/yy");
                    }
                }
                if (bookedMR.Count > 1)
                {
                    foreach (BookedMeetingRoom br in bookedMR)
                    {
                        string day = "";
                        if (br.MeetingDay == 0)
                            day = "Morning";
                        if (br.MeetingDay == 1)
                            day = "Afternoon";
                        if (br.MeetingDay == 2)
                            day = "Full Day";
                        DateTime dtmeetingdt = (DateTime)br.MeetingDate;
                        lblBookedDays1.Text = dtmeetingdt.ToString("dd/MM/yy") + " " + day;                        

                        lblToDate.Text = dtmeetingdt.ToString("dd/MM/yy");
                    }
                }


                #endregion

                #endregion
                lblfinalprice.Text = String.Format("{0:#,##,##0.00}", vi.FinalTotalPrice);
                lblsplcomments.Text = Convert.ToString(vi.SpecialRequest);
            }
            Divdetails.Visible = true;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region Get Booking/Request Information after clicking on Invoice Number
    /// <summary>
    //Start Get all information of booking details when clicking on Invoice Number
    /// </summary> 
    protected void lnbInvoiceNo_Click(object sender, EventArgs e)
    {
        try
        {
            lblTotalValue.Text = "0.00";
            lblTotalRequest.Text = "0.00";
            lblTotalBooking.Text = "0.00";
            lblFinalTotal.Text = "0.00";
            lblTotalVat.Text = "0.00";

            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
            LinkButton lnk = (LinkButton)sender;            
            
            string whereclause = "HotelId='" + hotelPid + "' and " + "Id='" + lnk.ToolTip + "' ";            
            TList<Invoice> lstInvoice = objFinance.GetInvoicebyCondition(whereclause, String.Empty);

            DateTime fromDate = (DateTime)lstInvoice[0].DateFrom;
            DateTime toDate = (DateTime)lstInvoice[0].DateTo;
            DateTime invoiceDate = (DateTime)lstInvoice[0].DueDate;
            lblFrom.Text = fromDate.ToString("dd/MM/yyyy");
            lblTo.Text = toDate.ToString("dd/MM/yyyy");
            lblInvoiceDate.Text = invoiceDate.ToString("dd/MM/yyyy");
            lblInvoiceNumber.Text = lstInvoice[0].InvoiceNumber;
            lblUniqueNumber.Text = lstInvoice[0].InvoiceNumber;

            //Get Hotel client details
            Hotel hotel = ObjHotelinfo.GetHotelByHotelID(hotelPid);
            lblClientAddress.Text = hotel.HotelAddress;
            lblClientPhone.Text = hotel.PhoneNumber;
            lblClientCode.Text = hotel.ContractId;
            lblVenueName.Text = hotel.Name;
            TList<UserDetails> userdtls = ObjHotelinfo.GetUserDetails(Convert.ToInt32(hotel.ClientId));
            if (userdtls.Count > 0)
            {
                lblClientVat.Text = userdtls[0].VatNo;
            }
            //lblCTo.Text = userdtls[0].Name;

            // For Booking
            string whereclaus = "hotelid in (" + hotelPid + ") and booktype = 0 and DepartureDate between '" + lstInvoice[0].DateFrom + "' and '" + lstInvoice[0].DateTo + "' and requeststatus=4";
            string orderby = ViewbookingrequestColumn.DepartureDate + " DESC";
            VList<Viewbookingrequest> vlistBooking = objViewBooking_Hotel.Bindgrid(whereclaus, orderby);
            grvBooking.DataSource = vlistBooking;
            grvBooking.DataBind();

            //For Request
            string whereclausreq = "hotelid in (" + hotelPid + ") and Booktype in (1,2) and DepartureDate between '" + lstInvoice[0].DateFrom + "' and '" + lstInvoice[0].DateTo + "' and requeststatus=4 and IsComissionDone=1";
            string orderbyreq = ViewbookingrequestColumn.DepartureDate + " DESC";
            VList<Viewbookingrequest> vlistreq = objViewBooking_Hotel.Bindgrid(whereclausreq, orderbyreq);
            grvRequest.DataSource = vlistreq;
            grvRequest.DataBind();
            Divdetails.Visible = true;

            if (vlistBooking.Count == 0 && vlistreq.Count == 0)
            {
                divTotal.Visible = false;
            }
            else
            {
                divTotal.Visible = true;
            }

            lblTotalValue.Text = String.Format("{0:#,##,##0.00}", Convert.ToDecimal(lblTotalBooking.Text) + Convert.ToDecimal(lblTotalRequest.Text));
            if (ObjHotelinfo.GetCountryByID(Convert.ToInt32(hotel.CountryId)) == "Belgium")
            {
                lblTotalVat.Text = String.Format("{0:#,##,##0.00}", Convert.ToDecimal(lblTotalValue.Text) * 21 / 100);
                lblFinalTotal.Text = Convert.ToString(Convert.ToDecimal(lblTotalValue.Text) + Convert.ToDecimal(lblTotalVat.Text));
            }
            else
            {
                lblFinalTotal.Text = Convert.ToString(Convert.ToDecimal(lblTotalValue.Text) + Convert.ToDecimal(lblTotalVat.Text));
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }

    /// <summary>
    //Start Get all information of booking details when clicking on Invoice Number and calculate net amount as per booking id
    /// </summary> 
    protected void grvBooking_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblId = (Label)e.Row.FindControl("lblId");
                // the replace check image wherver renvue is done
                Booking bookcomm = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(lblId.Text));                

                #region Calculate commission as per date difference
                //Label lblBookingDate = (Label)e.Row.FindControl("lblBookingDate");
                //Label lblDepartureDt = (Label)e.Row.FindControl("lblDepartureDt");
                //HiddenField hdnArrivalDate = (HiddenField)e.Row.FindControl("hdnArrivalDate");
                //Label lblCommision = (Label)e.Row.FindControl("lblCommision");

                //DateTime fromDate = new DateTime(Convert.ToInt32(lblBookingDate.Text.Split('/')[2]), Convert.ToInt32(lblBookingDate.Text.Split('/')[1]), Convert.ToInt32(lblBookingDate.Text.Split('/')[0]));
                //DateTime todate = new DateTime(Convert.ToInt32(hdnArrivalDate.Value.Split('/')[2]), Convert.ToInt32(hdnArrivalDate.Value.Split('/')[1]), Convert.ToInt32(hdnArrivalDate.Value.Split('/')[0]), 23, 59, 59);
                //TimeSpan ts = todate - fromDate;
                //int days = ts.Days;                
                #endregion

                #region Dynamic set invoice
                //VList<ViewForSetInvoice> objView = objOthers.GetCommissionDetailsForHotelId(Convert.ToInt32(bookcomm.HotelId));
                //if (objView.Count > 0)
                //{
                //    for (int i = 0; i < objView.Count; i++)
                //    {
                //        if (days >= objView[i].FromDay && days <= objView[i].ToDay)
                //        {
                //            lblCommision.Text = objView[i].CommissionPercentage.ToString();
                //            break;
                //        }
                //    }
                //}
                //else
                //{
                //    TList<InvoiceCommission> objComm = objOthers.GetInvoiceCommission();
                //    for (int i = 0; i < objComm.Count; i++)
                //    {
                //        if (days >= objComm[i].FromDay && days <= objComm[i].ToDay)
                //        {
                //            lblCommision.Text = objComm[i].DefaultCommission.ToString();
                //            break;
                //        }
                //    }
                //}
                #endregion

                #region Calculate the final value i.e. confirmed value multiply lmmr commission
                Label lblFinalValue = (Label)e.Row.FindControl("lblFinalValue");
                Label lblRevenueAmount = (Label)e.Row.FindControl("lblRevenueAmount");
                
                Label lblCommision = (Label)e.Row.FindControl("lblCommision");
                if (bookcomm.Accomodation != null)
                {
                    lblCommision.Text = Convert.ToString(bookcomm.Accomodation);
                }                
                if (lblRevenueAmount.Text != "" && lblRevenueAmount.Text != null)
                {
                    lblFinalValue.Text = String.Format("{0:#,##,##0.00}", Math.Round((Convert.ToDecimal(lblCommision.Text) * Convert.ToDecimal(lblRevenueAmount.Text) / 100), 2));
                }
                else
                {
                    lblFinalValue.Text = "0";
                }
                #endregion

                lblTotalBooking.Text = String.Format("{0:#,##,##0.00}", Math.Round((Convert.ToDecimal(lblFinalValue.Text) + Convert.ToDecimal(lblTotalBooking.Text)), 2));

                
               

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    //Start Get all information of request details when clicking on Invoice Number and calculate net amount as per request id
    /// </summary> 
    protected void grvRequest_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblId = (Label)e.Row.FindControl("lblId");
                // the replace check image wherver renvue is done
                Booking bookcomm = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(lblId.Text));                

                #region Calculate commission as per date difference
                //Label lblBookingDate = (Label)e.Row.FindControl("lblBookingDate");
                //Label lblDepartureDt = (Label)e.Row.FindControl("lblDepartureDt");
                //HiddenField hdnArrivalDate = (HiddenField)e.Row.FindControl("hdnArrivalDate");
                //Label lblCommision = (Label)e.Row.FindControl("lblCommision");

                //DateTime fromDate = new DateTime(Convert.ToInt32(lblBookingDate.Text.Split('/')[2]), Convert.ToInt32(lblBookingDate.Text.Split('/')[1]), Convert.ToInt32(lblBookingDate.Text.Split('/')[0]));
                //DateTime todate = new DateTime(Convert.ToInt32(hdnArrivalDate.Value.Split('/')[2]), Convert.ToInt32(hdnArrivalDate.Value.Split('/')[1]), Convert.ToInt32(hdnArrivalDate.Value.Split('/')[0]), 23, 59, 59);
                //TimeSpan ts = todate - fromDate;
                //int days = ts.Days;                
                #endregion

                #region Dynamic set invoice
                //VList<ViewForSetInvoice> objView = objOthers.GetCommissionDetailsForHotelId(Convert.ToInt32(bookcomm.HotelId));
                //if (objView.Count > 0)
                //{
                //    for (int i = 0; i < objView.Count; i++)
                //    {
                //        if (days >= objView[i].FromDay && days <= objView[i].ToDay)
                //        {
                //            lblCommision.Text = objView[i].CommissionPercentage.ToString();
                //            break;
                //        }
                //    }
                //}
                //else
                //{
                //    TList<InvoiceCommission> objComm = objOthers.GetInvoiceCommission();
                //    for (int i = 0; i < objComm.Count; i++)
                //    {
                //        if (days >= objComm[i].FromDay && days <= objComm[i].ToDay)
                //        {
                //            lblCommision.Text = objComm[i].DefaultCommission.ToString();
                //            break;
                //        }
                //    }
                //}
                #endregion

                #region Calculate the final value i.e. confirmed value multiply lmmr commission                
                Label lblFinalValue = (Label)e.Row.FindControl("lblFinalValue");
                Label lblRevenueAmount = (Label)e.Row.FindControl("lblRevenueAmount");

                Label lblCommision = (Label)e.Row.FindControl("lblCommision");
                if (bookcomm.Accomodation != null)
                {
                    lblCommision.Text = Convert.ToString(bookcomm.Accomodation);
                }
                
                if (lblRevenueAmount.Text != "" && lblRevenueAmount.Text != null)
                {
                    lblFinalValue.Text = String.Format("{0:#,##,##0.00}", Math.Round((Convert.ToDecimal(lblCommision.Text) * Convert.ToDecimal(lblRevenueAmount.Text) / 100), 2));
                }
                else
                {
                    lblFinalValue.Text = "0";
                }
                #endregion

                lblTotalRequest.Text = String.Format("{0:#,##,##0.00}", Math.Round((Convert.ToDecimal(lblFinalValue.Text) + Convert.ToDecimal(lblTotalRequest.Text)), 2));                           
            }            
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion
}