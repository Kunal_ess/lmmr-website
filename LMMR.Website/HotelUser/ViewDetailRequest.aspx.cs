﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Entities;
using LMMR.Data;
using LMMR.Business;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.HtmlControls;
using log4net;
using log4net.Config;
using System.Configuration;
using System.Xml;
#endregion
public partial class HotelUser_ViewDetailbooking : System.Web.UI.Page
{
    #region variables

    VList<ViewBookingHotels> vlist;
    VList<Viewbookingrequest> vlistreq;
    ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
    HotelManager objHotelManager = new HotelManager();
    string Typelink = "", AdminUser = "", Hotelid = "0";
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_ViewDetailbooking));
    WizardLinkSettingManager ObjWizardManager = new WizardLinkSettingManager();
    Createbooking objBooking = null;
    BookingManager bm = new BookingManager();
    CurrencyManager cm = new CurrencyManager();
    HotelManager objHotel = new HotelManager();
    PackagePricingManager objPackagePricingManager = new PackagePricingManager();
    Hotel objRequestHotel = new Hotel();
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl + "/";
        }
    }

    int countMr = 0;
    CreateRequest objRequest = null;
    public List<PackageItemDetails> SelectedPackage
    {
        get;
        set;
    }
    public List<RequestExtra> SelectedExtra
    {
        get;
        set;
    }
    public TList<PackageItems> AllPackageItem
    {
        get;
        set;
    }
    public TList<PackageItems> AllFoodAndBravrages
    {
        get;
        set;
    }
    public TList<PackageItems> AllEquipments
    {
        get;
        set;
    }
    public TList<PackageByHotel> AllPackageByHotel
    {
        get;
        set;
    }

    public string CurrencySign
    {
        get;
        set;
    }
    public XmlDocument XMLLanguage
    {
        get { return (XmlDocument)ViewState["Language"]; }
        set { ViewState["Language"] = value; }
    }
    #endregion

    #region Pageload
    /// <summary>
    /// Method to manage the page load functionality.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Session check               
            if (Session["CurrentHotelID"] == null)
            {
                //Response.Redirect("~/login.aspx");
                Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                Session.Abandon();
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "login/english");
                }
            }
            Hotelid = Convert.ToString(Session["CurrentHotelID"].ToString());
            //Session check
            modalcheckcomm.Hide();
           
            if (IsPostBack)
            {
                ApplyPaging();

            }
            else if (!IsPostBack)
            {
                divmessage.Style.Add("display", "none");
                Typelink = Convert.ToString(Request.QueryString["type"]);
                //--- if querystring "type" = 0 then its for bookings, else for request
                if (Typelink == "0")
                {

                    Session["LinkID"] = "lnkBookings";
                    Hotelid = Convert.ToString(Session["CurrentHotelID"].ToString());
                    string hotelname = objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Name;
                    lblbookingdetails.Text = "Booking details";
                    lblheadertext.Text = "Search Result list of bookings for the : " + "<b>" + hotelname + "</b>";
                }
                else if (Typelink == "1")
                {
                    Session["LinkID"] = "lnkRequests";
                    Hotelid = Convert.ToString(Session["CurrentHotelID"].ToString());
                    string hotelname = objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Name;
                    lblbookingdetails.Text = "Request details";
                    lblheadertext.Text = "Search Result list of request for the : " + "<b>" + hotelname + "</b>";
                    grdViewBooking.Columns[9].Visible = false;

                }
                ViewState["SearchAlpha"] = "all";
                bindgrid(Typelink);
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion


    #region BindGrid
    /// <summary>
    /// Method to bind the grid
    /// </summary>
    /// <param name="linktype"></param>
    /// linktype = 0 the its Booking else request   
  
    protected void bindgrid(string linktype)
    {
        try
        {
        Hotelid = Convert.ToString(Session["CurrentHotelID"].ToString());
        int totalcount = 0;
        linktype = Request.QueryString["type"].ToString();
        string whereclaus = "";
        if (string.IsNullOrEmpty(Convert.ToString(Session["whereclau"])))
        {
            whereclaus = "hotelid in (" + Hotelid + ") and booktype in ( '" + linktype+"',2)";  // book type for request and booking
        }
        else
        {

            whereclaus = Convert.ToString(Session["whereclau"]);
        }
       
        if (ViewState["SearchAlpha"].ToString() != "all")
        {
            whereclaus += " and Contact like '" + ViewState["SearchAlpha"].ToString() + "%'";
        }
        string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
        vlistreq = objViewBooking_Hotel.Bindgrid(whereclaus,orderby);
        grdViewBooking.DataSource = vlistreq;
        grdViewBooking.DataBind();
        ApplyPaging();
     
        #region Disableaction header
        if (Typelink == "1")
        {
            grdViewBooking.Columns[8].Visible = true;
            grdViewBooking.Columns[9].Visible = false;
           // grdViewBooking.Columns[11].Visible = false;
           // trcancelrequets.Visible = true;
        }
        else
        {
            grdViewBooking.Columns[8].Visible = false;
            grdViewBooking.Columns[9].Visible = false;
           // grdViewBooking.Columns[11].Visible = false;
          //  trcancelrequets.Visible = false;

        }

        #endregion
        divbookingdetails.Visible = false;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Apply Paging
    /// <summary>
    /// Method to apply Paging in grid
    /// </summary>
    private void ApplyPaging()
    {
        try
        {
        if (grdViewBooking.Rows.Count <= 0)
        {
            //     divprint.Visible = false;
            return;
        }

        GridViewRow row = grdViewBooking.TopPagerRow;
        if (row != null)
        {
            PlaceHolder ph;
            LinkButton lnkPaging;
            LinkButton lnkPrevPage;
            LinkButton lnkNextPage;
            lnkPrevPage = new LinkButton();
            lnkPrevPage.CssClass = "pre";
            lnkPrevPage.Width = Unit.Pixel(73);
            lnkPrevPage.CommandName = "Page";
            lnkPrevPage.CommandArgument = "prev";
            ph = (PlaceHolder)row.FindControl("ph");
            ph.Controls.Add(lnkPrevPage);
            if (grdViewBooking.PageIndex == 0)
            {
                lnkPrevPage.Enabled = false;

            }
            for (int i = 1; i <= grdViewBooking.PageCount; i++)
            {
                lnkPaging = new LinkButton();
                if (ViewState["CurrentPage"] != null)
                {
                    if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                    {
                        lnkPaging.CssClass = "pag2";
                    }
                    else
                    {
                        lnkPaging.CssClass = "pag";
                    }
                }
                else
                {
                    if (i == 1)
                    {
                        lnkPaging.CssClass = "pag2";
                    }
                    else
                    {
                        lnkPaging.CssClass = "pag";
                    }
                }
                lnkPaging.Width = Unit.Pixel(16);
                lnkPaging.Text = i.ToString();
                lnkPaging.CommandName = "Page";
                lnkPaging.CommandArgument = i.ToString();
                if (i == grdViewBooking.PageIndex + 1)
                    ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPaging);
            }
            lnkNextPage = new LinkButton();
            lnkNextPage.CssClass = "nex";
            lnkNextPage.Width = Unit.Pixel(42);
            lnkNextPage.CommandName = "Page";
            lnkNextPage.CommandArgument = "next";
            ph = (PlaceHolder)row.FindControl("ph");
            ph.Controls.Add(lnkNextPage);
            ph = (PlaceHolder)row.FindControl("ph");
            if (grdViewBooking.PageIndex == grdViewBooking.PageCount - 1)
            {
                lnkNextPage.Enabled = false;
            }
        }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Search
    /// <summary>
    /// Method to search
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtSearch_Click(object sender, EventArgs e)
    {
        try
        {
            modalcheckcomm.Hide();
            Hotelid = Convert.ToString(Session["CurrentHotelID"].ToString());
            Typelink = Convert.ToString(Request.QueryString["type"]);
         

            string whereclaus = " 1=1 ";
      
            if (!string.IsNullOrEmpty(txtRefNo.Text))
                whereclaus += " and Id ='" + txtRefNo.Text + "'";
            if (!string.IsNullOrEmpty(txtClientName.Text))
                whereclaus += " and Contact like '" + txtClientName.Text + "%'";
            if (!string.IsNullOrEmpty(txtArrivaldate.Text))
                whereclaus += " and arrivaldate = convert(DATETIME, '" + txtArrivaldate.Text + "',103)";
            if (!string.IsNullOrEmpty(txtFromdate.Text) && !string.IsNullOrEmpty(txtTodate.Text))
                whereclaus += " and arrivaldate between convert(DATETIME, '" + txtFromdate.Text + "',103) and convert(DATETIME , '" + txtTodate.Text + "',103) ";
            else
            {
                if (!string.IsNullOrEmpty(txtFromdate.Text))
                    whereclaus += " and  convert(DATETIME, '" + txtFromdate.Text + "',103) <= arrivaldate ";

                if (!string.IsNullOrEmpty(txtTodate.Text))
                    whereclaus += " and  convert(DATETIME, '" + txtTodate.Text + "',103) >= arrivaldate ";
            }


            whereclaus += " and hotelid in (" + Hotelid + ") and booktype in ( '" + Typelink + "',2)";

            Session["whereclau"] = whereclaus;
            int totalcount = 0;
            //  string whereclaus = "hotelid in (" + Hotelid + ") and booktype = 0 "; // book type for request and booking
            string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclaus, orderby);// DataRepository.ViewbookingrequestProvider.GetPaged(whereclaus, "", 0, int.MaxValue, out totalcount);
            grdViewBooking.DataSource = vlistreq;
            grdViewBooking.DataBind();
            ApplyPaging();
            if (grdViewBooking.Rows.Count <= 0)
            {
              //  divAlphabeticPaging.Visible = false;
            }

           divbookingdetails.Visible = false;
        

        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region View Booking/Request Details
    /// <summary>
    /// Method to view details of request /Booking would be seen
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lblRefNo_Click(object sender, EventArgs e)
    {
        //Changed The Code For Using Usercontrol:Pratik//
        try
        {

            LinkButton lnk = (LinkButton)sender;
            //-- viewstate object
            ViewState["BookingID"] = lnk.Text;
          




            Booking obj = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(ViewState["BookingID"].ToString()));
            if (obj.BookType == 1)
            {
                divwarningdetails.Visible = false;
                lblwarning.Text = "";
                requestDetails1.BindHotel(Convert.ToInt32(lnk.Text));
                divbookingdetails.Visible = true;
                requestDetails1.Visible = true;
                BookingDetails1.Visible = false;
            }
            else if (obj.BookType == 2)
            {

                divwarningdetails.Visible = true;
                lblwarning.Text = "This was initially a booking converted in request due to no (or low) bedroom avaibility";
                BookingDetails1.BindBooking(Convert.ToInt32(lnk.Text));
                divbookingdetails.Visible = true;
                BookingDetails1.Visible = true;
                requestDetails1.Visible = false;
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region fillbedroom details
    /// <summary>
    /// Method to get bedroom details
    /// </summary>

    protected void FillbedroomDetails()
    {
        try
        {
            //BookedBedRoom object
            //TList<BookedBedRoom> objBookedBR = objViewBooking_Hotel.getbookedBedroom(Convert.ToInt64(Convert.ToString(ViewState["BookingID"])));

            //foreach (BookedBedRoom br in objBookedBR)
            //{
            //    lblBedroomType.Text = Enum.GetName(typeof(BedRoomType), br.BedRoomIdSource.Types); // Enum.GetName();
            //    lblChekOut.Text = String.Format("{0:dd/MM/yyyy}", br.CheckOut);
            //    lblCheckIn.Text = String.Format("{0:dd/MM/yyyy}", br.CheckIn);
            //    lblpersonName.Text = br.PersonName;
            //    lblNote.Text = br.Note;

            //    lblbedroomtotalprice.Text = String.Format("{0:#,###}", br.Total);

            //}
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region ItemDataBound
    /// <summary>
    /// ItemDataBound of repeater
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void rpmain_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                Label lblbmrid = e.Item.FindControl("lblbmrid") as Label;
                Label lblPackageName = e.Item.FindControl("lblPackageName") as Label;
                Label lbltotalpackage = e.Item.FindControl("lbltotalpackage") as Label;
                Label lblextratotal = e.Item.FindControl("lblextratotal") as Label;
                GridView grdDetailspackage = e.Item.FindControl("grdDetailspackage") as GridView;
                GridView grdextra = e.Item.FindControl("grdextra") as GridView;
                GridView grdequipment = e.Item.FindControl("grdequipment") as GridView;

                // BuildPackageConfigure object
                TList<BuildPackageConfigure> obbuildpack = objViewBooking_Hotel.getPackageDetails(Convert.ToInt64(lblbmrid.Text));
                // BuildPackageConfigure object
                TList<BuildMeetingConfigure> objBuildMeetingConfigure = objViewBooking_Hotel.getextra(Convert.ToInt64(lblbmrid.Text));

                grdextra.DataSource = objBuildMeetingConfigure.FindAll(a => a.PackageIdSource.IsExtra == true);
                grdextra.DataBind();
                grdequipment.DataSource = objBuildMeetingConfigure.FindAll(a => Convert.ToInt32(a.PackageIdSource.ItemType) == (int)ItemType.Equipment);
                grdequipment.DataBind();
                int cntextra = 0;
                foreach (BuildMeetingConfigure bmc in objBuildMeetingConfigure.FindAll(a => Convert.ToInt32(a.PackageIdSource.ItemType) == (int)ItemType.Equipment))
                {
                    cntextra += Convert.ToInt32(bmc.TotalPrice);

                }
                lblextratotal.Text = Convert.ToString(cntextra);
                foreach (BuildPackageConfigure bpc in obbuildpack)
                {

                    lblPackageName.Text = bpc.PackageItemIdSource.PackageName;
                    lbltotalpackage.Text = String.Format("{0:#,###}", bpc.TotalPrice);

                    // BuildPackageConfigureDesc object
                    TList<BuildPackageConfigureDesc> obdesc = objViewBooking_Hotel.getpackagedetails(bpc.Id);
                    grdDetailspackage.DataSource = obdesc;
                    grdDetailspackage.DataBind();

                }


            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region PageIndexChanging
    /// <summary>
    /// PageIndex Changing of grid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void grdViewBooking_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
        grdViewBooking.PageIndex = e.NewPageIndex;
        ViewState["CurrentPage"] = e.NewPageIndex;
        bindgrid(Convert.ToString(Request.QueryString["Type"]));
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region RowDataBound
    /// <summary>
    /// RowDataBound of grid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void grdViewBooking_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            foreach (GridViewRow gr in grdViewBooking.Rows)
            {

                LinkButton btnchkcommision = (LinkButton)gr.FindControl("btnchkcommision");

                Label lblArrivalDt = (Label)gr.FindControl("lblArrivalDt");
                Label lblBookingDt = (Label)gr.FindControl("lblBookingDt");
                LinkButton lblRefNo = (LinkButton)gr.FindControl("lblRefNo");
                TList<BookedMeetingRoom> bookedMR = objViewBooking_Hotel.getbookedmeetingroom(Convert.ToInt64(lblRefNo.Text));
                Label lblDepartureDt = (Label)gr.FindControl("lblDepartureDt");
                Label lblExpiryDt = (Label)gr.FindControl("lblExpiryDt");
                Label lblstatus = (Label)gr.FindControl("lblstatus");
                DateTime startTime = DateTime.Now;
                string depdt = lblDepartureDt.Text;
                LinkButton btnchkcommision1 = (LinkButton)gr.FindControl("btnchkcommision1");


                Booking obj = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(lblRefNo.Text));
                if (obj.BookType == 2)
                {
                    gr.BackColor = System.Drawing.Color.PaleGreen;
                }

                #region Bindrequeststaus

                lblstatus.Text = Enum.GetName(typeof(BookingRequestStatus), Convert.ToInt32(lblstatus.ToolTip));

                #endregion

                #region ExpiryDate

                DateTime exptime;
                DateTime dtmeetingdt = new DateTime(Convert.ToInt32("20" + lblBookingDt.Text.Split('/')[2]), Convert.ToInt32(lblBookingDt.Text.Split('/')[1]), Convert.ToInt32(lblBookingDt.Text.Split('/')[0]));
                //string day = Convert.ToString(dtmeetingdt.DayOfWeek); // as asked by martin to remove in MOM of 2609
                //if (day == "Friday")
                //{
                //    lblExpiryDt.Text = dtmeetingdt.AddDays(3).ToString("dd/MM/yyyy");
                //    exptime = dtmeetingdt.AddDays(3);
                //}
                //else
                //{
                //    lblExpiryDt.Text = dtmeetingdt.AddDays(2).ToString("dd/MM/yyyy");
                //    exptime = dtmeetingdt.AddDays(2);
                //}
                exptime = dtmeetingdt.AddDays(5); // as asked by client
                lblExpiryDt.Text = dtmeetingdt.AddDays(5).ToString("dd/MM/yyyy");

                #endregion
              
                #region calculation of finalamt after renvenue was adjusted
                Booking bookcomm = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(btnchkcommision.CommandArgument));

                Label lblFinalTotal = (Label)gr.FindControl("lblFinalTotal");
                decimal finalamt = Convert.ToDecimal(bookcomm.FinalTotalPrice);

                //if (bookcomm.RevenueAmount != null)
                //{
                //    finalamt = (decimal)bookcomm.RevenueAmount;
                //    finalamt = Convert.ToDecimal(bookcomm.FinalTotalPrice) / (1 + finalamt / 100);
                //}
                lblFinalTotal.Text = String.Format("{0:#,###}", (Math.Round(finalamt, 2)));
                #endregion

                DateTime endTime = new DateTime(Convert.ToInt32(depdt.Split('/')[2]), Convert.ToInt32(depdt.Split('/')[1]), Convert.ToInt32(depdt.Split('/')[0]));
                //  DateTime endTime = Convert.ToDateTime(depdt);
                TimeSpan span = endTime.Subtract(startTime);
                int spn = span.Days;

                //---- set display commission button
                #region show commsion button
                if (lblstatus.Text.ToString() == BookingRequestStatus.Definite.ToString())
                {
                    if (btnchkcommision.ToolTip.ToLower() == "false" || string.IsNullOrEmpty(btnchkcommision.ToolTip))
                    {
                        if (spn < 0)
                        {

                            btnchkcommision.Text = "Check Commission";
                            btnchkcommision.Visible = true;
                            ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
                        }
                        else
                        {
                            btnchkcommision.Visible = false;
                            ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
                        }

                    }
                    else if (btnchkcommision.ToolTip.ToLower() == "true")
                    {
                        btnchkcommision.Visible = false;
                        ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = true;
                    }
                    else
                    {
                        btnchkcommision.Visible = false;
                        ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
                    }

                }
                #endregion


                #region show chnage Status buttom
                if (spn < 0)
                {

                    if (lblstatus.Text.ToString() == BookingRequestStatus.Cancel.ToString())
                    {
                        btnchkcommision1.Visible = false;
                        btnchkcommision.Visible = false;
                        ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
                    }
                    if (lblstatus.Text.ToString() == BookingRequestStatus.New.ToString())
                    {
                        btnchkcommision1.Visible = false;
                        btnchkcommision.Visible = false;
                        ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
                    }
                    if (lblstatus.Text.ToString() == BookingRequestStatus.Definite.ToString())
                    {
                        btnchkcommision1.Visible = false;

                    }
                    if (lblstatus.Text.ToString() == BookingRequestStatus.Tentative.ToString())
                    {
                        btnchkcommision1.Visible = true;
                        btnchkcommision.Visible = false;
                        ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
                    }
                }
                else
                {
                    if (lblstatus.Text.ToString() == BookingRequestStatus.Cancel.ToString())
                    {
                        btnchkcommision1.Visible = true;
                        btnchkcommision.Visible = false;
                        ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
                    }
                    if (lblstatus.Text.ToString() == BookingRequestStatus.New.ToString())
                    {
                        btnchkcommision1.Visible = false;
                        btnchkcommision.Visible = false;
                        ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
                    }
                    if (lblstatus.Text.ToString() == BookingRequestStatus.Definite.ToString())
                    {
                        btnchkcommision1.Visible = true;

                    }
                    if (lblstatus.Text.ToString() == BookingRequestStatus.Tentative.ToString())
                    {
                        btnchkcommision1.Visible = true;
                        btnchkcommision.Visible = false;
                        ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
                    }
                }
                #endregion

               
                #region ExpiryDate
                if (lblstatus.Text == BookingRequestStatus.Expired.ToString())
                {
                    lblRefNo.Enabled = false;
                    gr.BackColor = System.Drawing.Color.LightGray;
                    btnchkcommision.Visible = false;
                    ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
                    btnchkcommision1.Visible = false;
                }

                #endregion
                #region frozen
                if (lblstatus.Text == BookingRequestStatus.Frozen.ToString())
                {
                    lblRefNo.Enabled = false;
                    gr.BackColor = System.Drawing.Color.LightGray;
                    btnchkcommision.Visible = false;
                    ((System.Web.UI.WebControls.Image)gr.FindControl("imgCheck")).Visible = false;
                    btnchkcommision1.Visible = false;
                }

                #endregion





            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region Check Commission
    /// <summary>
    /// when check commsion was checked
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  
 
    protected void btnchkcommision_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton btnchkcomm = (LinkButton)sender;
            ViewState["ChkcommBookingID"] = btnchkcomm.CommandArgument;

            if (btnchkcomm.Text.ToLower() == "change status")
            {
                divmessage.Style.Add("display", "none");
                ModalPopupCheck.Show();
                modalcheckcomm.Hide();
                ViewState["popup"] = "0";
            }
            else
            {
                //  txtrealvalue.ReadOnly = true;
              //  string script1 = "<script type='text/javascript'>alert('hello');</script>";


                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () {alert('You are about to validate your commission.Please have the invoice ready in order to accept / adjust the commissionable revenue.');});", true);
               
               
                divmessage.Style.Add("display", "none");
                chkmeetingNotheld.Checked = false;
                modalcheckcomm.Show();
                ModalPopupCheck.Hide();
                ViewState["popup"] = "1";
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion


    #region Confirm Commsion
    /// <summary>
    /// when check commsion was confirmed
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            //Booking object
            Booking objbooking = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(ViewState["ChkcommBookingID"].ToString()));
            if (Convert.ToString(ViewState["popup"]) == "1")
            {
                if (chkmeetingNotheld.Checked)
                {
                    objbooking.RequestStatus = (int)BookingRequestStatus.Cancel;
                    objbooking.RevenueReason = "Meeting was not held";
                }
                else
                {
                    #region check file upload
                    string strPlanName = string.Empty;
                    if (ulPlan.HasFile)
                    {
                        string fileExtension = Path.GetExtension(ulPlan.PostedFile.FileName.ToString());

                        if (fileExtension == ".xls" || fileExtension == ".xlsx" || fileExtension == ".doc" || fileExtension == ".docx" || fileExtension.ToLower() == ".pdf")
                        {
                            strPlanName = Path.GetFileName(ulPlan.FileName);
                        }
                        else
                        {
                            divmessage.InnerHtml = "Please select file in (word,excel,pdf) formats only";
                            divmessage.Attributes.Add("class", "error");
                            divmessage.Style.Add("display", "block");
                            modalcheckcomm.Show();
                            return;
                        }

                        if (ulPlan.PostedFile.ContentLength > 1048576)
                        {
                            divmessage.InnerHtml = "Filesize of Supporting Document is too large. Maximum file size permitted is 1 MB.";
                            divmessage.Attributes.Add("class", "error");
                            divmessage.Style.Add("display", "block");
                            modalcheckcomm.Show();
                            return;
                        }
                        if (string.IsNullOrEmpty(txtrealvalue.Text))
                        {
                            divmessage.InnerHtml = "Kindly enter the Netto value";
                            divmessage.Attributes.Add("class", "error");
                            divmessage.Style.Add("display", "block");
                            modalcheckcomm.Show();
                            return;
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(txtrealvalue.Text))
                            divmessage.InnerHtml = "Kindly upload the Supporting Document and the Netto value";
                        else
                            divmessage.InnerHtml = "Kindly upload the Supporting Document.";
                        divmessage.Attributes.Add("class", "error");
                        divmessage.Style.Add("display", "block");
                        modalcheckcomm.Show();
                        return;
                    }

                    #endregion

                    ulPlan.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "SupportDocNetto/") + strPlanName);
                    objbooking.RequestStatus = (int)BookingRequestStatus.Definite;
                    if (string.IsNullOrEmpty(txtrealvalue.Text))
                        txtrealvalue.Text = "0";
                    // decimal netvalue = Convert.ToDecimal(txtrealvalue.Text);
                    // decimal revenue = (Convert.ToDecimal(objbooking.FinalTotalPrice) / (netvalue - 1)) * 100;
                    // objbooking.RevenueAmount = netvalue;
                    objbooking.FinalTotalPrice = Convert.ToDecimal(txtrealvalue.Text); // updated
                    objbooking.ConfirmRevenueAmount = Convert.ToDecimal(txtrealvalue.Text); // updated
                    objbooking.ComissionSubmitDate = System.DateTime.Now;
                    objbooking.IsComissionDone = true;
                    objbooking.RevenueReason = ddlreason.SelectedItem.Text;
                    objbooking.SupportingDocNetto = strPlanName;
                }


            }
            else
            {
                objbooking.RequestStatus = Convert.ToInt32(ddlstatus.SelectedValue);

            }
            if (objViewBooking_Hotel.updatecheckComm(objbooking,Convert.ToInt64(Session["CurrentHotelID"])))
            {
                txtrealvalue.Text = "";
                ddlreason.SelectedIndex = 0;
                chkmeetingNotheld.Checked = false;
                bindgrid(Typelink);
                modalcheckcomm.Hide();
                ModalPopupCheck.Hide();
            }

        
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    

    }
    #endregion


    

    #region CheckedChanged
    /// <summary>
    /// when meetingheld was checked/unchecked
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  

    protected void chkmeetingNotheld_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            if (chkmeetingNotheld.Checked)
            {
                txtrealvalue.Text = "";
                ddlreason.SelectedIndex = 0;
                
            }
            else
            {
                
                //btnSubmit.ValidationGroup = "popup";
            }
            divmessage.Style.Add("display", "none");
            modalcheckcomm.Show();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Aplhabetic Paging
    /// <summary>
    /// alphabetic paging is done
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  

    public void PageChange(object sender, EventArgs e)
    {
        try
        {
        HtmlAnchor anc = (sender as HtmlAnchor);
        Hotelid = Convert.ToString(Session["CurrentHotelID"]);
        int totalcount = 0;
        string whereclaus = "";
        var d = anc.InnerHtml;

        if (string.IsNullOrEmpty(Convert.ToString(Session["whereclau"])))
        {
            whereclaus = "hotelid in (" + Hotelid + ") and booktype  in ( '" + Convert.ToString(Request.QueryString["type"])+"',2)";  // book type for request and booking
        }
        else
        {

            whereclaus = Convert.ToString(Session["whereclau"]);
        }
        if (d == "all")
        {
            whereclaus += "";
            ViewState["SearchAlpha"] = d;
        }
        else
        {
            whereclaus += " and Contact like '" + d + "%'";
            ViewState["SearchAlpha"] = d;
        
        }
        string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
        vlistreq = objViewBooking_Hotel.Bindgrid(whereclaus, orderby);// DataRepository.ViewbookingrequestProvider.GetPaged(whereclaus, "", 0, int.MaxValue, out totalcount);
        grdViewBooking.DataSource = vlistreq;
        grdViewBooking.DataBind();
        if (vlistreq.Count > 0)
        {
            grdViewBooking.DataSource = vlistreq;
            grdViewBooking.DataBind();
            ApplyPaging();
            Session["whereclau"] = null;
        }
        else
        {
            grdViewBooking.DataSource = null;
            grdViewBooking.DataBind();
        }
        //ViewState["SearchAlpha"] = "all";
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region Cancel Button Click
    /// <summary>
    /// Modal popup cancel event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  
   
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            divmessage.InnerHtml = "";
            divmessage.Style.Add("display", "none");
        modalcheckcomm.Hide();       
        txtrealvalue.Text = "";      
        chkmeetingNotheld.Checked = false;
        ddlreason.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion
    #region RowCreated
    /// <summary>
    /// method to RowCreated
    /// </summary>
    protected void grdViewBooking_RowCreated(object sender, GridViewRowEventArgs e)
    {
        try
        {
            string rowID = String.Empty;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#FF9'");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;");
                ViewState["rowID"] = e.Row.RowIndex;

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion
    #region clear button
    /// <summary>
    /// method to clear search fields
    /// </summary>
    protected void lnkclear_Click(object sender, EventArgs e)
    {
        txtArrivaldate.Text = "";
        txtClientName.Text = "";
        txtFromdate.Text = "";
        txtRefNo.Text = "";
        txtTodate.Text = "";
        ViewState["SearchAlpha"] = "all";
        bindgrid(Convert.ToString(Request.QueryString["type"]));
    }
    #endregion

    #region Bookingbind


    List<VatCollection> _VatCalculation = new List<VatCollection>();
    public List<VatCollection> VatCalculation
    {
        get
        {
            if (_VatCalculation == null)
            {
                _VatCalculation = new List<VatCollection>();
            }
            return _VatCalculation;
        }
        set
        {
            _VatCalculation = value;
        }
    }



    //public void BindHotel()
    //{
    //    try
    //    {
    //        var hotelID = Convert.ToInt64(Hotelid);
    //        rptHotel.DataSource = objRequest.HotelList.Where(a => a.MeetingroomList.Count > 0 && a.HotelID == hotelID);
    //        rptHotel.DataBind();
           
    //        Int64 intHotelID = objRequest.HotelList[0].HotelID;
    //        if (objRequest.PackageID != 0)
    //        {
    //            pnlPackage.Visible = true;
    //            PackageMaster packagedetails = objPackagePricingManager.GetAllPackageName().Where(a => a.Id == objRequest.PackageID).FirstOrDefault();
    //            if (packagedetails != null)
    //            {
    //                #region highlight red

    //                List<PackageByHotel> tlistpACK = DataRepository.PackageByHotelProvider.GetByHotelId(Convert.ToInt64(Hotelid)).Where(a => a.PackageId == objRequest.PackageID && a.IsOnline == true).ToList();
    //                if (tlistpACK.Count == 0)
    //                    lblPackageName.ForeColor = System.Drawing.Color.Red;
    //                #endregion
    //                lblPackageName.Text = packagedetails.PackageName;
    //                if (packagedetails.PackageName == "Standard")
    //                {
    //                    lblpackageDescription.Text = "Package includes main meeting room rental, note pads, pencils, flipchart, mineral water, morning coffee, Sandwich buffet with salads (incl. non-alcoholic beverages) and afternoon coffee break (tea, coffee, soft drinks and pastries).For half day packages, either morning or afternoon break is included. The Standard package is available as of 10 meeting delegates.";

    //                }
    //                else if (packagedetails.PackageName == "Favourite")
    //                {
    //                    lblpackageDescription.Text = "Package includes main meeting room rental, note pads, pencils, flipchart, mineral water, morning coffee, 2-courses menu (incl. non-alcoholic beverages) and afternoon coffee break (tea, coffee, soft drinks and pastries).For half day packages, either morning or afternoon break is included. The Favourite package is available as of 10 meeting delegates.";
    //                }
    //                else if (packagedetails.PackageName == "Elegant")
    //                {
    //                    lblpackageDescription.Text = "Package includes main meeting room rental, note pads, pencils, flipchart, mineral water, morning coffee, hot and cold buffet (incl. non-alcoholic beverages) and afternoon coffee break (tea, coffee, soft drinks and pastries).For half day packages, either morning or afternoon break is included. The Elegant package is available as of 20 meeting delegates.";
    //                }
    //                else
    //                {
    //                    lblpackageDescription.Text = "";
    //                }
    //            }
    //            AllPackageItem = objPackagePricingManager.GetAllPackageItems();
    //            rptPackageItem.DataSource = objPackagePricingManager.GetPackageItemsByPackageID(objRequest.PackageID);
    //            rptPackageItem.DataBind();
    //            if (objRequest.ExtraList.Count > 0)
    //            {
    //                //Package Selection.
                   
    //                pnlExtra.Visible = true;
    //                rptExtras.DataSource = objRequest.ExtraList;
    //                rptExtras.DataBind();//check AllPackageItem.Where(a => a.IsExtra == true)
    //            }
    //            else
    //            {
    //                pnlExtra.Visible = false;
    //            }
    //            pnlFoodAndBravrages.Visible = false;
    //        }
    //        else
    //        {
    //            pnlPackage.Visible = false;

    //            if (objRequest.BuildYourMeetingroomList.Count > 0)
    //            {
    //                AllFoodAndBravrages = objPackagePricingManager.GetAllFoodBeveragesItems(intHotelID);
    //                pnlFoodAndBravrages.Visible = true;
    //                rptFoodandBravragesDay.DataSource = objRequest.DaysList;
    //                rptFoodandBravragesDay.DataBind();
    //            }
    //            else
    //            {
    //                pnlFoodAndBravrages.Visible = false;
    //            }
    //        }


    //        if (objRequest.EquipmentList.Count > 0)
    //        {
    //            AllEquipments = objPackagePricingManager.GetAllEquipmentItems(intHotelID);
    //            pnlEquipment.Visible = true;
    //            rptEquipmentDay.DataSource = objRequest.DaysList;
    //            rptEquipmentDay.DataBind();
    //        }
    //        else
    //        {
    //            pnlEquipment.Visible = false;
    //        }
    //        if (objRequest.IsAccomodation)
    //        {
    //            pnlAccomodation.Visible = true;
    //           // lblaccomodationQun.Text = Convert.ToString(objRequest.RequestAccomodationList.Quantity);
    //        }
    //        else
    //        {
    //            pnlAccomodation.Visible = true;
    //        }
    //        lblSpecialRequest.Text = objRequest.SpecialRequest;
    //    }
    //    catch (Exception ex)
    //    {
    //        logger.Error(ex);
    //    }
    //}

    protected void rptPackageItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label txtQuantity = (Label)e.Item.FindControl("txtQuantity");
            HiddenField hdnItemId = (HiddenField)e.Item.FindControl("hdnItemId");
            PackageItemMapping pim = e.Item.DataItem as PackageItemMapping;
            if (pim != null)
            {
                PackageItems p = AllPackageItem.Where(a => a.Id == pim.ItemId).FirstOrDefault();
                if (p != null)
                {
                    hdnItemId.Value = Convert.ToString(p.Id);
                    lblItemName.Text = p.ItemName;
                    PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblDescription.Text = pdesc.ItemDescription;
                    }
                    else
                    {
                        lblDescription.Text = "";
                    }
                    PackageItemDetails pid = objRequest.PackageItemList.Where(a => a.ItemID == p.Id).FirstOrDefault();
                    if (pid != null)
                    {
                        txtQuantity.Text = Convert.ToString(pid.Quantity);
                    }
                    else
                    {
                        txtQuantity.Text = "0";
                    }
                }
            }
        }
    }

    protected void rptHotel_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            // Label lblIndex = (Label)e.Item.FindControl("lblIndex");
            Label lblHotelName = (Label)e.Item.FindControl("lblHotelName");
            Repeater rptMeetingroom = (Repeater)e.Item.FindControl("rptMeetingroom");
            BuildHotelsRequest b = e.Item.DataItem as BuildHotelsRequest;
            if (b != null)
            {
                //  lblIndex.Text = (e.Item.ItemIndex + 1).ToString();
                Hotel objHtl = objHotel.GetHotelDetailsById(b.HotelID);
                lblHotelName.Text = objHtl.Name;
                rptMeetingroom.DataSource = b.MeetingroomList;
                rptMeetingroom.DataBind();
            }
        }
    }

    protected void rptMeetingroom_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //  Label lblIndex = (Label)e.Item.FindControl("lblIndex");
            Label lblMeetingRoomName = (Label)e.Item.FindControl("lblMeetingRoomName");
            Label lblConfigurationType = (Label)e.Item.FindControl("lblConfigurationType");
            Label lblMaxandMinCapacity = (Label)e.Item.FindControl("lblMaxandMinCapacity");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            Label lblIsMain = (Label)e.Item.FindControl("lblIsMain");
            BuildMeetingRoomRequest brm = e.Item.DataItem as BuildMeetingRoomRequest;
            if (brm != null)
            {
                // lblIndex.Text = (e.Item.ItemIndex + 1).ToString();                
                MeetingRoom objMeetingroom = objHotel.GetMeetingRoomDetailsById(brm.MeetingRoomID);
                MeetingRoomDesc objDesc = objMeetingroom.MeetingRoomDescCollection.Where(a => a.LanguageId == Convert.ToInt32(Session["LanguageID"])).FirstOrDefault();
                MeetingRoomConfig objMrConfig = objMeetingroom.MeetingRoomConfigCollection.Where(a => a.Id == brm.ConfigurationID).FirstOrDefault();
                lblMeetingRoomName.Text = objMeetingroom.Name;
                lblConfigurationType.Text = (objMrConfig.RoomShapeId == (int)RoomShape.Boardroom ? RoomShape.Boardroom.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Classroom ? RoomShape.Classroom.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Cocktail ? RoomShape.Cocktail.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.School ? RoomShape.School.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Theatre ? RoomShape.Theatre.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.UShape ? RoomShape.UShape.ToString() : RoomShape.Boardroom.ToString());
                lblMaxandMinCapacity.Text = objMrConfig.MinCapacity + " - " + objMrConfig.MaxCapicity;
                lblQuantity.Text = brm.Quantity.ToString();
                lblIsMain.Text = brm.IsMain == true ? "Main" : "Breakout";
            }
        }
    }

    protected void rptExtras_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            RequestExtra r = e.Item.DataItem as RequestExtra;
            if (r != null)
            {
                PackageItems objPackage = AllPackageItem.Where(a => a.IsExtra == true && a.Id == r.ItemID).FirstOrDefault();
                if (objPackage != null)
                {
                    #region highlight red

                    List<PackageByHotel> tlistpACK = DataRepository.PackageByHotelProvider.GetByHotelId(Convert.ToInt64(Hotelid)).Where(a => a.ItemId == r.ItemID && a.IsOnline == true).ToList();
                    if (tlistpACK.Count == 0)
                        lblItemName.ForeColor = System.Drawing.Color.Red;
                    #endregion

                    lblItemName.Text = objPackage.ItemName;
                    PackageDescription pdesc = objPackage.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblDescription.Text = pdesc.ItemDescription;
                    }
                    else
                    {
                        lblDescription.Text = "";
                    }
                    lblQuantity.Text = Convert.ToString(r.Quantity);
                }
            }
        }
    }

    protected void rptFoodandBravragesDay_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblSelectDay = (Label)e.Item.FindControl("lblSelectDay");
            Repeater rptFoodandBravragesItem = (Repeater)e.Item.FindControl("rptFoodandBravragesItem");
            NumberOfDays nod = e.Item.DataItem as NumberOfDays;
            if (nod != null)
            {
                rptFoodandBravragesItem.DataSource = objRequest.BuildYourMeetingroomList.Where(a => a.SelectDay == nod.SelectedDay);
                rptFoodandBravragesItem.DataBind();
            }
        }
    }

    protected void rptFoodandBravragesItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {


        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            BuildYourMeetingroomRequest bm = e.Item.DataItem as BuildYourMeetingroomRequest;
            if (bm != null)
            {
                PackageItems p = AllFoodAndBravrages.Where(a => a.Id == bm.ItemID).FirstOrDefault();

                if (p != null)
                {
                    #region highlight red

                    List<PackageByHotel> tlistpACK = DataRepository.PackageByHotelProvider.GetByHotelId(Convert.ToInt64(Hotelid)).Where(a => a.ItemId == bm.ItemID && a.IsOnline == true).ToList();
                    if (tlistpACK.Count == 0)
                        lblItemName.ForeColor = System.Drawing.Color.Red;
                    #endregion

                    lblItemName.Text = p.ItemName;
                    PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblDescription.Text = pdesc.ItemDescription;
                    }
                    else
                    {
                        lblDescription.Text = "";
                    }
                    lblQuantity.Text = Convert.ToString(bm.Quantity);
                }
            }
        }
    }

    protected void rptEquipmentDay_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblSelectDay = (Label)e.Item.FindControl("lblSelectDay");
            Repeater rptEquipmentItem = (Repeater)e.Item.FindControl("rptEquipmentItem");
            NumberOfDays nod = e.Item.DataItem as NumberOfDays;
            if (nod != null)
            {
                lblSelectDay.Text = nod.SelectedDay.ToString();
                rptEquipmentItem.DataSource = objRequest.EquipmentList.Where(a => a.SelectedDay == nod.SelectedDay);
                rptEquipmentItem.DataBind();
            }
        }
    }

    protected void rptEquipmentItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            RequestEquipment re = e.Item.DataItem as RequestEquipment;
            if (re != null)
            {
                PackageItems p = AllEquipments.Where(a => a.Id == re.ItemID).FirstOrDefault();
                if (p != null)
                {
                    #region highlight red

                    List<PackageByHotel> tlistpACK = DataRepository.PackageByHotelProvider.GetByHotelId(Convert.ToInt64(Hotelid)).Where(a => a.ItemId == re.ItemID && a.IsOnline == true).ToList();
                    if (tlistpACK.Count == 0)
                        lblItemName.ForeColor = System.Drawing.Color.Red;
                    #endregion

                    lblItemName.Text = p.ItemName;
                    PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblDescription.Text = pdesc.ItemDescription;
                    }
                    else
                    {
                        lblDescription.Text = "";
                    }
                    lblQuantity.Text = Convert.ToString(re.Quantity);
                }
            }
        }
    }

    #endregion

    #region Language
    //This is used for language conversion for static contants.
    public string GetKeyResult(string key)
    {
        //if (XMLLanguage == null)
        //{
        //    XMLLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
        //}
        //XmlNode nodes = XMLLanguage.SelectSingleNode("items/item[@key='" + Key + "']");
        //return nodes.InnerText;//
        return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    }
    #endregion

    #region Go for Calculation
    public void Calculate(Createbooking objCreateBook)
    {
        decimal TotalMeetingroomPrice = 0;
        decimal TotalPackagePrice = 0;
        decimal TotalBuildYourPackagePrice = 0;
        decimal TotalEquipmentPrice = 0;
        decimal TotalExtraPrice = 0;
        bool PackageSelected = false;
        VatCalculation = null;
        VatCalculation = new List<VatCollection>();
        if (objCreateBook != null)
        {
            foreach (BookedMR objb in objCreateBook.MeetingroomList)
            {
                foreach (BookedMrConfig objconfig in objb.MrDetails)
                {
                    TotalMeetingroomPrice = objconfig.NoOfParticepant * objconfig.MeetingroomPrice;
                    //Build mr
                    foreach (BuildYourMR bmr in objconfig.BuildManageMRLst)
                    {
                        TotalBuildYourPackagePrice += bmr.ItemPrice * bmr.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == bmr.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = bmr.vatpercent;
                            v.CalculatedPrice = bmr.ItemPrice * bmr.Quantity * bmr.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == bmr.vatpercent).FirstOrDefault().CalculatedPrice += bmr.ItemPrice * bmr.Quantity * bmr.vatpercent / 100;
                        }
                    }
                    PackageSelected = Convert.ToBoolean(objconfig.PackageID);
                    //Equipment
                    foreach (ManageEquipment eqp in objconfig.EquipmentLst)
                    {
                        TotalEquipmentPrice += eqp.ItemPrice * eqp.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = eqp.vatpercent;
                            v.CalculatedPrice = eqp.ItemPrice * eqp.Quantity * eqp.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault().CalculatedPrice += eqp.ItemPrice * eqp.Quantity * eqp.vatpercent / 100;
                        }
                    }
                    //Manage Extras
                    foreach (ManageExtras ext in objconfig.ManageExtrasLst)
                    {
                        TotalExtraPrice += ext.ItemPrice * ext.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == ext.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = ext.vatpercent;
                            v.CalculatedPrice = ext.ItemPrice * ext.Quantity * ext.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == ext.vatpercent).FirstOrDefault().CalculatedPrice += ext.ItemPrice * ext.Quantity * ext.vatpercent / 100;
                        }
                    }
                    //Manage Package Item
                    foreach (ManagePackageItem pck in objconfig.ManagePackageLst)
                    {
                        TotalPackagePrice += pck.ItemPrice * pck.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == pck.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = pck.vatpercent;
                            v.CalculatedPrice = pck.ItemPrice * pck.Quantity * pck.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == pck.vatpercent).FirstOrDefault().CalculatedPrice += pck.ItemPrice * pck.Quantity * pck.vatpercent / 100;
                        }
                    }

                    //if (PackageSelected)
                    //{
                    //    objBooking.TotalBookingPrice += TotalPackagePrice + TotalExtraPrice + TotalEquipmentPrice + TotalBuildYourPackagePrice;
                    //}
                    //else
                    //{
                    //    objBooking.TotalBookingPrice += TotalMeetingroomPrice + TotalEquipmentPrice + TotalBuildYourPackagePrice;
                    //}
                }
            }

        }
    }
    #endregion
    
}