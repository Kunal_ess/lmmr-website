﻿#region Using 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using log4net;
using log4net.Config;
#endregion
public partial class HotelUser_MeetingRoomConfig : System.Web.UI.Page
{
    #region Variables and Properties
    UserManager objUserManager = new UserManager();
    ILog logger = log4net.LogManager.GetLogger(typeof(HotelUser_MeetingRoomConfig));
    WizardLinkSettingManager ObjWizardLinkSetting = new WizardLinkSettingManager();
    DashboardManager ObjDashBoard = new DashboardManager();
    MeetingRoomConfigManager ObjMeetingRoomConfigManager = new MeetingRoomConfigManager();
    MeetingRoomDescManager ObjMeetingRoomDescManager = new MeetingRoomDescManager();
    HotelInfo ObjHotelinfo = new HotelInfo();
    HotelManager objHotelManager = new HotelManager();
    int intHotelID = 0;
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentHotelID"] != null)
        {

            intHotelID = Convert.ToInt32(Session["CurrentHotelID"].ToString());
            lblHotel.Text = objHotelManager.GetHotelDetailsById(Convert.ToInt64(Session["CurrentHotelID"])).Name;
            if (!IsPostBack)
            {
                Session["LinkID"] = "lnkPropertyLevel";
                BindMeetingRoomConfig(intHotelID);
                divFormMeetingRoomConfig.Visible = false;
                Getgoonline();
                #region Leftmenu
                HyperLink lnk = (HyperLink)this.Master.FindControl("lnkConferenceInfo");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkContactDetails");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkFacilities");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkCancelationPolicy");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkDescriptionMeetingRoom");
                lnk.CssClass = "";
                lnk = (HyperLink)this.Master.FindControl("lnkConfiguration");
                lnk.CssClass = "selected";
                #endregion
            }

        }
        else
        {
            Response.Redirect("~/login.aspx",false);
            return;
        }
               
    }
    #endregion

    #region Functions
    /// <summary>
    /// For check go online status for this current hotel.
    /// </summary>
    public void Getgoonline()
    {
        if (ObjHotelinfo.GetHotelGoOnline(intHotelID).Count > 0)
        {
            divNext.Visible = false;
            ViewState["Count"] = ObjHotelinfo.GetHotelGoOnline(intHotelID).Count;  
        }
    }  

    /// <summary>
    /// Bind meetiongroom configuration data with list view. 
    /// </summary>
    /// <param name="hotelID"></param>
    void BindMeetingRoomConfig(int hotelID)
    {
        if (ObjMeetingRoomDescManager.GetAllMeetingRoom(hotelID).Count > 0)
        {
            lstViewMeetingRoomConfig.DataSource = ObjMeetingRoomDescManager.GetAllMeetingRoom(hotelID);
            lstViewMeetingRoomConfig.DataBind();
            trLegend.Visible = true;
        }
        else
        {
            lstViewMeetingRoomConfig.DataSource = ObjMeetingRoomDescManager.GetAllMeetingRoom(hotelID);
            lstViewMeetingRoomConfig.DataBind();
            trLegend.Visible = false;
        }
    }
    #endregion

    #region Events
    /// <summary>
    /// This event used for bound meetingroom configuration data. 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lstViewMeetingRoomConfig_ItemDataBound(object sender, ListViewItemEventArgs e)
    {

        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            ListViewDataItem currentItem = (ListViewDataItem)e.Item;
            DataKey currentDataKey = this.lstViewMeetingRoomConfig.DataKeys[currentItem.DataItemIndex];
            int intMeetingRoomID = Convert.ToInt32(currentDataKey.Value);
            Button btnModify = (Button)e.Item.FindControl("lnkModify");
            btnModify.OnClientClick = "ChangeRowColor('tr" + intMeetingRoomID + "')";
            TList<MeetingRoomConfig> lst = ObjMeetingRoomConfigManager.GetConfigByMeetingRoomID(intMeetingRoomID);
            int intMinEnumValue = ObjMeetingRoomConfigManager.GetMinMeetingRoomShapeID();
            int intMaxEnumValue = ObjMeetingRoomConfigManager.GetMaxMeetingRoomShapeID();
            int intIndex;
            for (intIndex = intMinEnumValue; intIndex <= intMaxEnumValue; intIndex++)
            {
                var getRow = lst.Find(a => a.RoomShapeId == intIndex);
                if (getRow != null)
                {
                    if (getRow.RoomShapeId == 1)
                    {
                        Label lblTheatreMin = (Label)e.Item.FindControl("lblTheatreMin");
                        Label lblTheatreMax = (Label)e.Item.FindControl("lblTheatreMax");
                        lblTheatreMin.Text = getRow.MinCapacity.ToString();
                        lblTheatreMax.Text = getRow.MaxCapicity.ToString();
                    }
                    else if (getRow.RoomShapeId == 2)
                    {
                        Label lblSchoolMin = (Label)e.Item.FindControl("lblSchoolMin");
                        Label lblSchoolMax = (Label)e.Item.FindControl("lblSchoolMax");
                        lblSchoolMin.Text = getRow.MinCapacity.ToString();
                        lblSchoolMax.Text = getRow.MaxCapicity.ToString();
                    }
                    else if (getRow.RoomShapeId == 3)
                    {
                        Label lblUShapeMin = (Label)e.Item.FindControl("lblUShapeMin");
                        Label lblUShapeMax = (Label)e.Item.FindControl("lblUShapeMax");
                        lblUShapeMin.Text = getRow.MinCapacity.ToString();
                        lblUShapeMax.Text = getRow.MaxCapicity.ToString();
                    }
                    else if (getRow.RoomShapeId == 4)
                    {
                        Label lblBoardroomMin = (Label)e.Item.FindControl("lblBoardroomMin");
                        Label lblBoardroomMax = (Label)e.Item.FindControl("lblBoardroomMax");
                        lblBoardroomMin.Text = getRow.MinCapacity.ToString();
                        lblBoardroomMax.Text = getRow.MaxCapicity.ToString();
                    }
                    else if (getRow.RoomShapeId == 5)
                    {
                        Label lblCocktailMin = (Label)e.Item.FindControl("lblCocktailMin");
                        Label lblCocktailMax = (Label)e.Item.FindControl("lblCocktailMax");
                        lblCocktailMin.Text = getRow.MinCapacity.ToString();
                        lblCocktailMax.Text = getRow.MaxCapicity.ToString();
                    }
                    else if (getRow.RoomShapeId == 6)
                    {
                        Label lblClassRoomMin = (Label)e.Item.FindControl("lblClassRoomMin");
                        Label lblClassRoomMax = (Label)e.Item.FindControl("lblClassRoomMax");
                        lblClassRoomMin.Text = getRow.MinCapacity.ToString();
                        lblClassRoomMax.Text = getRow.MaxCapicity.ToString();
                    }
                }

            }

        }

    }

    /// <summary>
    /// This event used for perfom view operation.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lstViewMeetingRoomConfig_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        if (e.CommandName == "View")

        hdnMeetingRoomID.Value = e.CommandArgument.ToString();
        txtMeetingRoom.Text = ((Label)e.Item.FindControl("lblMeetingRoomName")).Text;
        txtMinTheatre.Text = ((Label)e.Item.FindControl("lblTheatreMin")).Text;
        txtMaxTheatre.Text = ((Label)e.Item.FindControl("lblTheatreMax")).Text;
        txtSchoolMin.Text = ((Label)e.Item.FindControl("lblSchoolMin")).Text;
        txtSchoolMax.Text = ((Label)e.Item.FindControl("lblSchoolMax")).Text;
        txtUShapeMin.Text = ((Label)e.Item.FindControl("lblUShapeMin")).Text;
        txtUShapeMax.Text = ((Label)e.Item.FindControl("lblUShapeMax")).Text;
        txtBoardroomMin.Text = ((Label)e.Item.FindControl("lblBoardroomMin")).Text;
        txtBoardroomMax.Text = ((Label)e.Item.FindControl("lblBoardroomMax")).Text;
        txtCocktailMin.Text = ((Label)e.Item.FindControl("lblCocktailMin")).Text;
        txtCocktailMax.Text = ((Label)e.Item.FindControl("lblCocktailMax")).Text;
        divFormMeetingRoomConfig.Visible = true;
        divmessage.Style.Add("display", "none");
        divmessage.Attributes.Add("class", "error");
        ScriptManager.RegisterStartupScript(this, typeof(Page), "colorChange", "CallMyMethod();", true);

    }

    /// <summary>
    /// This button click used for save data into table.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            hdnSelectedRowID.Value = "0";
            string shape = string.Empty;
            TList<MeetingRoomConfig> ObjLst = ObjMeetingRoomConfigManager.GetConfigByMeetingRoomID(Convert.ToInt32(hdnMeetingRoomID.Value));

            int intMinEnumValue = ObjMeetingRoomConfigManager.GetMinMeetingRoomShapeID();
            int intMaxEnumValue = ObjMeetingRoomConfigManager.GetMaxMeetingRoomShapeID();
            int intIndex;
            for (intIndex = intMinEnumValue; intIndex <= intMaxEnumValue; intIndex++)
            {
                if (Enum.GetName(typeof(MeetingRoomShapeName), intIndex) != null)
                {
                    shape = Enum.GetName(typeof(MeetingRoomShapeName), intIndex);

                    if (shape == "TheaterStyle")
                    {
                        var row = ObjLst.Find(a => a.RoomShapeId == intIndex);
                        if (row != null)
                        {
                            // call function to update meeting room Configuration.
                            MeetingRoomConfig objMeetignRoomConfig = ObjMeetingRoomConfigManager.GetMeetingroomConfigByID(Convert.ToInt32(row.Id));
                            objMeetignRoomConfig.MeetingRoomId = Convert.ToInt32(hdnMeetingRoomID.Value);
                            objMeetignRoomConfig.RoomShapeId = intIndex;
                            objMeetignRoomConfig.MinCapacity = Convert.ToInt32(txtMinTheatre.Text);
                            objMeetignRoomConfig.MaxCapicity = Convert.ToInt32(txtMaxTheatre.Text);
                            ObjMeetingRoomConfigManager.UpdateMeetingRoomCofig(objMeetignRoomConfig);
                        }
                    }
                    else if (shape == "School")
                    {
                        var row = ObjLst.Find(a => a.RoomShapeId == intIndex);
                        if (row != null)
                        {
                            // call function to update meeting room Configuration.
                            MeetingRoomConfig objMeetignRoomConfig = ObjMeetingRoomConfigManager.GetMeetingroomConfigByID(Convert.ToInt32(row.Id));
                            objMeetignRoomConfig.MeetingRoomId = Convert.ToInt32(hdnMeetingRoomID.Value);
                            objMeetignRoomConfig.RoomShapeId = intIndex;
                            objMeetignRoomConfig.MinCapacity = Convert.ToInt32(txtSchoolMin.Text);
                            objMeetignRoomConfig.MaxCapicity = Convert.ToInt32(txtSchoolMax.Text);
                            ObjMeetingRoomConfigManager.UpdateMeetingRoomCofig(objMeetignRoomConfig);
                        }
                    }
                    else if (shape == "UShape")
                    {

                        var row = ObjLst.Find(a => a.RoomShapeId == intIndex);
                        if (row != null)
                        {
                            // call function to update meeting room Configuration.
                            MeetingRoomConfig objMeetignRoomConfig = ObjMeetingRoomConfigManager.GetMeetingroomConfigByID(Convert.ToInt32(row.Id));
                            objMeetignRoomConfig.MeetingRoomId = Convert.ToInt32(hdnMeetingRoomID.Value);
                            objMeetignRoomConfig.RoomShapeId = intIndex;
                            objMeetignRoomConfig.MinCapacity = Convert.ToInt32(txtUShapeMin.Text);
                            objMeetignRoomConfig.MaxCapicity = Convert.ToInt32(txtUShapeMax.Text);
                            ObjMeetingRoomConfigManager.UpdateMeetingRoomCofig(objMeetignRoomConfig);
                        }

                    }
                    else if (shape == "Boardroom")
                    {
                        var row = ObjLst.Find(a => a.RoomShapeId == intIndex);
                        if (row != null)
                        {
                            // call function to update meeting room Configuration.
                            MeetingRoomConfig objMeetignRoomConfig = ObjMeetingRoomConfigManager.GetMeetingroomConfigByID(Convert.ToInt32(row.Id));
                            objMeetignRoomConfig.MeetingRoomId = Convert.ToInt32(hdnMeetingRoomID.Value);
                            objMeetignRoomConfig.RoomShapeId = intIndex;
                            objMeetignRoomConfig.MinCapacity = Convert.ToInt32(txtBoardroomMin.Text);
                            objMeetignRoomConfig.MaxCapicity = Convert.ToInt32(txtBoardroomMax.Text);
                            ObjMeetingRoomConfigManager.UpdateMeetingRoomCofig(objMeetignRoomConfig);
                        }
                    }
                    else if (shape == "Cocktail")
                    {

                        var row = ObjLst.Find(a => a.RoomShapeId == intIndex);
                        if (row != null)
                        {
                            // call function to update meeting room Configuration.
                            MeetingRoomConfig objMeetignRoomConfig = ObjMeetingRoomConfigManager.GetMeetingroomConfigByID(Convert.ToInt32(row.Id));
                            objMeetignRoomConfig.MeetingRoomId = Convert.ToInt32(hdnMeetingRoomID.Value);
                            objMeetignRoomConfig.RoomShapeId = intIndex;
                            objMeetignRoomConfig.MinCapacity = Convert.ToInt32(txtCocktailMin.Text);
                            objMeetignRoomConfig.MaxCapicity = Convert.ToInt32(txtCocktailMax.Text);
                            ObjMeetingRoomConfigManager.UpdateMeetingRoomCofig(objMeetignRoomConfig);
                        }
                    }
                }
            }
            divmessage.InnerHtml = ObjMeetingRoomConfigManager.propMessage;
            if (Convert.ToString(ViewState["Count"]) != "1")
            {
                divmessage.InnerHtml += "&nbsp; Please click on next button to move ahead.";
            }
            divmessage.Attributes.Add("class", "succesfuly");
            divmessage.Style.Add("display", "block");

            BindMeetingRoomConfig(intHotelID);
            divFormMeetingRoomConfig.Visible = false;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This button click event used for hide form and reset value.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        hdnSelectedRowID.Value = "0";
        divFormMeetingRoomConfig.Visible = false;
        divmessage.Style.Add("display", "none");
        divmessage.Attributes.Add("class", "error");
    }

    /// <summary>
    /// This button click event used for proceed to next step.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNext_Click(object sender, EventArgs e)
    {
        divmessage.Style.Add("display", "none");
        divmessage.Attributes.Add("class", "error");
        ObjWizardLinkSetting.ThisPageIsDone(intHotelID, Convert.ToInt32(PropertyLevelSectionLeftMenu.lnkConfiguration));
        Response.Redirect("PrintfactSheet.aspx",false);
        return;
    }
    #endregion
}