﻿<%@ Page Title="Hotel User - Meeting room Config" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master"
    AutoEventWireup="true" CodeFile="MeetingRoomConfig.aspx.cs" Inherits="HotelUser_MeetingRoomConfig" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" runat="Server">
    <h1>
        Configuration meeting rooms : <b>
            <asp:Label ID="lblHotel" runat="server" Text=""></asp:Label></b></h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" runat="Server">
    <asp:UpdateProgress ID="uprog" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="Loding_overlay">
                <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
                Loading...</div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:HiddenField ID="hdnMeetingRoomConfigID" runat="server" />
            <asp:HiddenField ID="hdnMeetingRoomID" runat="server" />
            <asp:HiddenField ID="hdnSelectedRowID" runat="server" Value="0" />
            <div style="float: left;">
                <div id="divmessage" runat="server" style="width: 710px;">
                </div>
            </div>
            <asp:ListView ID="lstViewMeetingRoomConfig" runat="server" ItemPlaceholderID="itemPlacehoderID"
                AutomaticGenerateColumns="false" DataKeyNames="Id" OnItemDataBound="lstViewMeetingRoomConfig_ItemDataBound"
                OnItemCommand="lstViewMeetingRoomConfig_ItemCommand">
                <LayoutTemplate>
                    <div class="meeting-description-list" style="float: left; margin-bottom: 0px;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td bgcolor="#98bcd6">
                                    <table width="100%" border="0" cellspacing="1" cellpadding="5">
                                        <tr bgcolor="#c4d6e2" align="center">
                                            <td width="30%">
                                                Meeting Room
                                            </td>
                                            <td width="14%">
                                                Theatre Style
                                            </td>
                                            <td width="14%">
                                                School
                                            </td>
                                            <td width="14%">
                                                U-Shape
                                            </td>
                                            <td width="14%">
                                                Boardroom
                                            </td>
                                            <td width="14%">
                                                Cocktail
                                            </td>
                                        </tr>
                                        <tr bgcolor="#c4d6e2">
                                            <td align="center">
                                            </td>
                                            <td align="center">
                                                <img src="../Images/theatre.gif" alt="" />
                                            </td>
                                            <td align="center">
                                                <img src="../Images/classroom.gif" alt="" />
                                            </td>
                                            <td align="center">
                                                <img src="../Images/ushape.gif" alt="" />
                                            </td>
                                            <td align="center">
                                                <img src="../Images/boardroom.gif" alt="" />
                                            </td>
                                            <td align="center">
                                                <img src="../Images/cocktail.gif" alt="" />
                                            </td>
                                        </tr>
                                        <asp:PlaceHolder ID="itemPlacehoderID" runat="server"></asp:PlaceHolder>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr bgcolor="#d8eefc" class=" row" id="tr<%# Eval("Id") %>">
                        <td>
                            <asp:Label ID="lblMeetingRoomName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                            <br>
                            <br>
                            <span class=" modify">
                                <asp:Button ID="lnkModify" CssClass="modify-i" CommandName="View" CommandArgument='<%#Eval("Id") %>'
                                    runat="server" Text="Modify"></asp:Button></span>
                        </td>
                        <td align="center">
                            <asp:Label ID="lblTheatreMin" runat="server" Text="0"></asp:Label>
                            &nbsp;-&nbsp;
                            <asp:Label ID="lblTheatreMax" runat="server" Text="0"></asp:Label>
                        </td>
                        <td align="center">
                            <asp:Label ID="lblSchoolMin" runat="server" Text="0"></asp:Label>&nbsp;-&nbsp;
                            <asp:Label ID="lblSchoolMax" runat="server" Text="0"></asp:Label>
                        </td>
                        <td align="center">
                            <asp:Label ID="lblUShapeMin" runat="server" Text="0"></asp:Label>&nbsp;-&nbsp;
                            <asp:Label ID="lblUShapeMax" runat="server" Text="0"></asp:Label>
                        </td>
                        <td align="center">
                            <asp:Label ID="lblBoardroomMin" runat="server" Text="0"></asp:Label>&nbsp;-&nbsp;
                            <asp:Label ID="lblBoardroomMax" runat="server" Text="0"></asp:Label>
                        </td>
                        <td align="center">
                            <asp:Label ID="lblCocktailMin" runat="server" Text="0"></asp:Label>&nbsp;-&nbsp;
                            <asp:Label ID="lblCocktailMax" runat="server" Text="0"></asp:Label>
                        </td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr bgcolor="#edf8fe" class=" row" id="tr<%# Eval("Id") %>">
                        <td>
                            <asp:Label ID="lblMeetingRoomName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                            <br>
                            <br>
                            <span class="modify">
                                <asp:Button ID="lnkModify" CssClass="modify-i" CommandName="View" CommandArgument='<%#Eval("Id") %>'
                                    runat="server" Text="Modify"></asp:Button></span>
                        </td>
                        <td align="center">
                            <asp:Label ID="lblTheatreMin" runat="server" Text="0"></asp:Label>
                            &nbsp;-&nbsp;
                            <asp:Label ID="lblTheatreMax" runat="server" Text="0"></asp:Label>
                        </td>
                        <td align="center">
                            <asp:Label ID="lblSchoolMin" runat="server" Text="0"></asp:Label>&nbsp;-&nbsp;
                            <asp:Label ID="lblSchoolMax" runat="server" Text="0"></asp:Label>
                        </td>
                        <td align="center">
                            <asp:Label ID="lblUShapeMin" runat="server" Text="0"></asp:Label>&nbsp;-&nbsp;
                            <asp:Label ID="lblUShapeMax" runat="server" Text="0"></asp:Label>
                        </td>
                        <td align="center">
                            <asp:Label ID="lblBoardroomMin" runat="server" Text="0"></asp:Label>&nbsp;-&nbsp;
                            <asp:Label ID="lblBoardroomMax" runat="server" Text="0"></asp:Label>
                        </td>
                        <td align="center">
                            <asp:Label ID="lblCocktailMin" runat="server" Text="0"></asp:Label>&nbsp;-&nbsp;
                            <asp:Label ID="lblCocktailMax" runat="server" Text="0"></asp:Label>
                        </td>
                    </tr>
                </AlternatingItemTemplate>
                <EmptyDataTemplate>
                    <table width="100%" style="float:left;" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td bgcolor="#98bcd6">
                                <table width="100%" border="0" cellspacing="1" cellpadding="5">
                                    <tr bgcolor="#c4d6e2" align="center">
                                        <td width="30%">
                                            Meeting Room
                                        </td>
                                        <td width="14%">
                                            Theatre Style
                                        </td>
                                        <td width="14%">
                                            School
                                        </td>
                                        <td width="14%">
                                            U-Shape
                                        </td>
                                        <td width="14%">
                                            Boardroom
                                        </td>
                                        <td width="14%">
                                            Cocktail
                                        </td>
                                    </tr>
                                    <tr bgcolor="#c4d6e2">
                                        <td align="center">
                                        </td>
                                        <td align="center">
                                            <img src="../Images/theatre.gif" alt="" />
                                        </td>
                                        <td align="center">
                                            <img src="../Images/classroom.gif" alt="" />
                                        </td>
                                        <td align="center">
                                            <img src="../Images/ushape.gif" alt="" />
                                        </td>
                                        <td align="center">
                                            <img src="../Images/boardroom.gif" alt="" />
                                        </td>
                                        <td align="center">
                                            <img src="../Images/cocktail.gif" alt="" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" align="center">
                                            <b>No Record found </b>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
            </asp:ListView>
            <table id="trLegend" style="float: left" runat="server">
                <tr>
                    <td colspan="6" valign="top" style="padding: 10px 0px 0px 2px; font-weight: bold"
                        align="left">
                        <img src="../Images/help.jpg" />&nbsp;&nbsp;Kindly update the configuration value.
                        To update click on modify.<br />
                        <img src="../Images/help.jpg" />&nbsp;&nbsp;Insert 0-0, if the configuration does not match for the meeting room.<br />
                         <img src="../Images/help.jpg" />&nbsp;&nbsp;The following configuration
will allow you to protect your inventory. MIN should represent the minimum
number of delegates you want to accept in this specific meeting room and
layout. MAX should represent the maximum number of delegates you want to
accept in this specific meeting room and layout.
                    </td>
                </tr>
            </table>
            <div class="booking-details" id="divFormMeetingRoomConfig" runat="server">
                <ul>
                    <li class="error" style="display: none;"></li>
                    <li class="value8">
                        <div class="col17">
                            <strong>Meeting Room</strong>
                        </div>
                        <div class="col18">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="txtMeetingRoom" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </li>
                    <li class="value9">
                        <div class="col17">
                            <strong>Theatre Style <span style="color: Red">*</span></strong>
                        </div>
                        <div class="col18">
                            <table>
                                <tr>
                                    <td>
                                        Min :
                                        <asp:TextBox ID="txtMinTheatre" runat="server" class="inputbox10" MaxLength="6"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtMinTheatre"
                                            FilterType="Numbers" runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        Max :
                                        <asp:TextBox ID="txtMaxTheatre" runat="server" class="inputbox10" MaxLength="6"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="txtMaxTheatre"
                                            FilterType="Numbers" runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        <asp:CompareValidator ID="CompareValidatorThearter" runat="server" ControlToValidate="txtMaxTheatre"
                                            ControlToCompare="txtMinTheatre" ValidationGroup="check" Operator="GreaterThanEqual"
                                            Type="Integer" ErrorMessage="*"></asp:CompareValidator>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </li>
                    <li class="value9">
                        <div class="col17">
                            <strong>School <span style="color: Red">*</span></strong>
                        </div>
                        <div class="col18">
                            <table>
                                <tr>
                                    <td>
                                        Min :
                                        <asp:TextBox ID="txtSchoolMin" runat="server" class="inputbox10" MaxLength="6"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" TargetControlID="txtSchoolMin"
                                            FilterType="Numbers" runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        Max :
                                        <asp:TextBox ID="txtSchoolMax" runat="server" class="inputbox10" MaxLength="6"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" TargetControlID="txtSchoolMax"
                                            FilterType="Numbers" runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        <asp:CompareValidator ID="CompareValidatorReception" runat="server" ControlToValidate="txtSchoolMax"
                                            ControlToCompare="txtSchoolMin" ValidationGroup="check" Operator="GreaterThanEqual"
                                            Type="Integer" ErrorMessage="*"></asp:CompareValidator>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </li>
                    <li class="value9">
                        <div class="col17">
                            <strong>U-Shape <span style="color: Red">*</span></strong>
                        </div>
                        <div class="col18">
                            <table>
                                <tr>
                                    <td>
                                        Min :
                                        <asp:TextBox ID="txtUShapeMin" runat="server" class="inputbox10" MaxLength="6"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" TargetControlID="txtUShapeMin"
                                            FilterType="Numbers" runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        Max :
                                        <asp:TextBox ID="txtUShapeMax" runat="server" class="inputbox10" MaxLength="6"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" TargetControlID="txtUShapeMax"
                                            FilterType="Numbers" runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        <asp:CompareValidator ID="CompareValidatorUShape" runat="server" ControlToValidate="txtUShapeMax"
                                            ControlToCompare="txtUShapeMin" ValidationGroup="check" Operator="GreaterThanEqual"
                                            Type="Integer" ErrorMessage="*"></asp:CompareValidator>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </li>
                    <li class="value9">
                        <div class="col17">
                            <strong>Boardroom <span style="color: Red">*</span></strong>
                        </div>
                        <div class="col18">
                            <table>
                                <tr>
                                    <td>
                                        Min :
                                        <asp:TextBox ID="txtBoardroomMin" runat="server" class="inputbox10" MaxLength="6"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" TargetControlID="txtBoardroomMin"
                                            FilterType="Numbers" runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        Max :
                                        <asp:TextBox ID="txtBoardroomMax" runat="server" class="inputbox10" MaxLength="6"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" TargetControlID="txtBoardroomMax"
                                            FilterType="Numbers" runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        <asp:CompareValidator ID="CompareValidatorBanque" runat="server" ControlToValidate="txtBoardroomMax"
                                            ControlToCompare="txtBoardroomMin" ValidationGroup="check" Operator="GreaterThanEqual"
                                            Type="Integer" ErrorMessage="*"></asp:CompareValidator>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </li>
                    <li class="value9">
                        <div class="col17">
                            <strong>Cocktail <span style="color: Red">*</span></strong>
                        </div>
                        <div class="col18">
                            <table>
                                <tr>
                                    <td>
                                        Min :
                                        <asp:TextBox ID="txtCocktailMin" runat="server" class="inputbox10" MaxLength="6"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" TargetControlID="txtCocktailMin"
                                            FilterType="Numbers" runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        Max :
                                        <asp:TextBox ID="txtCocktailMax" runat="server" class="inputbox10" MaxLength="6"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" TargetControlID="txtCocktailMax"
                                            FilterType="Numbers" runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        <asp:CompareValidator ID="CompareValidatorConference" runat="server" ControlToValidate="txtCocktailMax"
                                            ControlToCompare="txtCocktailMin" ValidationGroup="check" Operator="GreaterThanEqual"
                                            Type="Integer" ErrorMessage="*"></asp:CompareValidator>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </li>
                    <li class="value10">
                        <div class="col21">
                            <div class="button_section">
                                <asp:LinkButton ID="btnSave" CssClass="select" OnClick="btnSave_Click" ValidationGroup="check"
                                    runat="server" Text="Save" OnClientClick="return UpdateValid();" />
                                <span>or</span>
                                <asp:LinkButton ID="btnCancel" OnClick="btnCancel_Click" runat="server" Text="Cancel" />
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSave" />
            <asp:PostBackTrigger ControlID="btnCancel" />
        </Triggers>
    </asp:UpdatePanel>
    <div class="button_sectionNext" id="divNext" runat="server">
        <asp:LinkButton ID="btnNext" runat="server" class="RemoveCookie" Text="Next &gt;&gt;"
            OnClick="btnNext_Click" />
    </div>
    <div id="Loding_overlaySec">
        <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
        <span>Saving...</span></div>
    <script language="javascript" type="text/javascript">

        function UpdateValid() {
            var isvalid = true;
            var errormessage = "";

            var theatermin = jQuery("#<%= txtMinTheatre.ClientID %>").val();
            if (theatermin.length <= 0 || parseInt(theatermin, 10) < 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Theatre min value is required.";
                isvalid = false;
            }
            var theatermax = jQuery("#<%= txtMaxTheatre.ClientID %>").val();

            if (theatermax.length <= 0 || parseInt(theatermax, 10) < 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Theatre max value is required.";
                isvalid = false;
            }
            if (parseInt(theatermin, 10) > parseInt(theatermax, 10)) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Theatre min value must be less than max value.";
                isvalid = false;
            }
            //////
            var schoolmin = jQuery("#<%= txtSchoolMin.ClientID %>").val();
            if (schoolmin.length <= 0 || parseInt(schoolmin, 10) < 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "School min value is required.";
                isvalid = false;
            }
            var schoolmax = jQuery("#<%= txtSchoolMax.ClientID %>").val();
            if (schoolmax.length <= 0 || parseInt(schoolmax, 10) < 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "School max value is required.";
                isvalid = false;
            }
            if (parseInt(schoolmin, 10) > parseInt(schoolmax, 10)) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "School min value must be less than max value.";
                isvalid = false;
            }
            ///////
            var ushapemin = jQuery("#<%= txtUShapeMin.ClientID %>").val();
            if (ushapemin.length <= 0 || parseInt(ushapemin, 10) < 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "U-Shape min value is required.";
                isvalid = false;
            }
            var ushapemax = jQuery("#<%= txtUShapeMax.ClientID %>").val();
            if (ushapemax.length <= 0 || parseInt(ushapemax, 10) < 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "U-Shape max value is required.";
                isvalid = false;
            }
            if (parseInt(ushapemin, 10) > parseInt(ushapemax, 10)) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "U-Shape min value must be less than max value.";
                isvalid = false;
            }
            /////
            var bedroommin = jQuery("#<%= txtBoardroomMin.ClientID %>").val();
            if (bedroommin.length <= 0 || parseInt(bedroommin, 10) < 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Boardroom min value is required.";
                isvalid = false;
            }
            var bedroommax = jQuery("#<%= txtBoardroomMax.ClientID %>").val();
            if (bedroommax.length <= 0 || parseInt(bedroommax, 10) < 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Boardroom max value is required.";
                isvalid = false;
            }
            if (parseInt(bedroommin, 10) > parseInt(bedroommax, 10)) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Boardroom min value must be less than max value.";
                isvalid = false;
            }
            ///////
            var cocktailmmin = jQuery("#<%= txtCocktailMin.ClientID %>").val();
            if (cocktailmmin.length <= 0 || parseInt(cocktailmmin, 10) < 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Cocktail min value is required.";
                isvalid = false;
            }
            var cocktailmax = jQuery("#<%= txtCocktailMax.ClientID %>").val();
            if (cocktailmax.length <= 0 || parseInt(cocktailmax, 10) < 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Cocktail max value is required.";
                isvalid = false;
            }
            if (parseInt(cocktailmmin, 10) > parseInt(cocktailmax, 10)) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Cocktail min value must be less than max value.";
                isvalid = false;
            }
            if (!isvalid) {
                jQuery("#<%= divFormMeetingRoomConfig.ClientID %> .error").show();
                jQuery("#<%= divFormMeetingRoomConfig.ClientID %> .error").html(errormessage);
                var offseterror = jQuery("#<%= divFormMeetingRoomConfig.ClientID %> .error").offset();
                jQuery("body").scrollTop(offseterror.top);
                jQuery("html").scrollTop(offseterror.top);
                return false;
            }
            jQuery("#<%= divFormMeetingRoomConfig.ClientID %> .error").html("");
            jQuery("#<%= divFormMeetingRoomConfig.ClientID %> .error").hide();
            jQuery("#Loding_overlaySec span").html("Saving...");
            jQuery("#Loding_overlaySec").show();
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';
        }
       
    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#<%= btnNext.ClientID %>").bind("click", function () {
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                jQuery("#Loding_overlaySec span").html("Loading...");
                jQuery("#Loding_overlaySec").show();
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            });
        });
    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            CallMyMethod();
        });

        function CallMyMethod() {
            var SeletedRowID = jQuery('#<%=hdnSelectedRowID.ClientID %>').val();
            if (SeletedRowID != 0) {
                jQuery('#' + SeletedRowID).css("background-color", "#FF9");
            }
            jQuery('.rowElem').jqTransform({ imgPath: ' ' });
        }

        function ChangeRowColor(rowID) {
            jQuery('#' + rowID).css("background-color", "#FF9");
            jQuery('#<%=hdnSelectedRowID.ClientID %>').val(rowID);
        }  
    </script>
</asp:Content>
