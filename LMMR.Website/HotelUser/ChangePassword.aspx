﻿<%@ Page Title="Hotel User - Change Password" Language="C#" MasterPageFile="~/HotelUser/HotelMain.master"
    AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="HotelUser_ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentheadlabel" runat="Server">
    <h1>
        <b>Change Password</b></h1>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="contentbody" runat="Server">
    <div class="booking-details">
        <ul>
            <li id="divmessage" runat="server"></li>
            <li class="value8">
                <div class="col17">
                    <strong>Old Password <span style="color: Red">*</span></strong></div>
                <div class="col18">
                    <div class="sec1">
                        <asp:TextBox ID="txtOldPassword" runat="server" Class="textfield2" TextMode="Password"
                            MaxLength="50"></asp:TextBox>
                    </div>
                </div>
            </li>
            <li class="value9">
                <div class="col17">
                    <strong>New Password <span style="color: Red">*</span></strong></div>
                <div class="col18">
                    <div class="sec1">
                        <asp:TextBox ID="txtNewPassword" runat="server" Class="textfield2" TextMode="Password"
                            MaxLength="50"></asp:TextBox>
                    </div>
                </div>
            </li>
            <li class="value8">
                <div class="col17">
                    <strong>Confirm New Password <span style="color: Red">*</span></strong></div>
                <div class="col19">
                    <div class="sec1">
                        <asp:TextBox ID="txtConfirmNewpassword" runat="server" class="textfield2" TextMode="Password"
                            MaxLength="50"></asp:TextBox>
                    </div>
                </div>
            </li>
            <li class="value10">
                <div class="col21">
                    <div class="button_section">
                        <asp:LinkButton ID="btnSave" runat="server" CssClass="select" Text="Save" OnClick="btnSave_Click" />
                         <span>or</span>
                        <asp:LinkButton ID="btnCancel" OnClick="btnCancel_Click" runat="server" Text="Cancel" />
                    </div>
                </div>
            </li>
        </ul>
        <table width="100%" border="0" cellpadding="2">
            <tr>
                <td colspan="6" valign="top" style="padding: 10px 0px 0px 2px; font-weight: bold"
                    align="left">
                    <img src="../Images/help.jpg" />&nbsp;&nbsp; Password must be atleast of 6 characters and should contain Numeric characters.  
                </td>
            </tr>
        </table>
    </div>
    <div id="Loding_overlaySec">
        <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />Saving...</div>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {


            jQuery("#<%= btnSave.ClientID %>").bind("click", function () {
                if (jQuery("#<%= divmessage.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= divmessage.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                }
                var oldpassword = jQuery("#<%= txtOldPassword.ClientID %>").val();
                var isvalid = true;
                var errormessage = "";
                if (oldpassword.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Old password is required.";
                    isvalid = false;
                }
                var newpassword = jQuery("#<%= txtNewPassword.ClientID %>").val();
                if (newpassword.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "New password is required.";
                    isvalid = false;
                }
                else if (newpassword.length < 6) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "New password must have 6 letters.";
                    isvalid = false;
                }
                else if (!checkAlphabetic(newpassword)) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "New password must contains alphanumaric values.";
                    isvalid = false;                    
                }
                var confirmpassword = jQuery("#<%= txtConfirmNewpassword.ClientID %>").val();
                if (confirmpassword.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Confirm password is required.";
                    isvalid = false;
                }
                else if (newpassword != confirmpassword) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Password not matched.";
                    isvalid = false;
                }
                if (!isvalid) {
                    jQuery(".error").show();
                    jQuery(".error").html(errormessage);
                    var offseterror = jQuery(".error").offset();
                    jQuery("body").scrollTop(offseterror.top);
                    jQuery("html").scrollTop(offseterror.top);
                    return false;
                }
                jQuery(".error").html("");
                jQuery(".error").hide();
                jQuery("#Loding_overlaySec").show();
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            });
        });

        function checkAlphabetic(sText) {
            var ValidChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var IsAlphabetic = true;
            var Char;
            var i = 0;
            for(i = 0; i < sText.length; i++)
            {
                Char = sText.charAt(i);
                if (ValidChars.indexOf(Char) == -1) {
                    IsAlphabetic = false;
                    break;
                }
            }
            return IsAlphabetic;
        }
    </script>
</asp:Content>
