﻿<%@ Page Title="Booking Step 3" Language="C#" MasterPageFile="~/HomeMaster.master" AutoEventWireup="true" CodeFile="BookingStep3.aspx.cs" Inherits="BookingStep3" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntLeft" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMainbody" Runat="Server">
    <!-- ClickTale Top part -->
<script type="text/javascript">
    var WRInitTime = (new Date()).getTime();
</script>
<!-- ClickTale end of Top part -->
    
    <!--mainbody START HERE-->
    <div class="mainbody">
        <!--today-date START HERE-->
        <div class="today-date">
            <div class="today-date-left">
                <b><%= GetKeyResult("TODAY")%>:</b> <asp:Label ID="lblLoginTime" runat="server" Text="25 october 2011"></asp:Label>
            </div>
            <div class="today-date-right">
                <%= GetKeyResult("YOUARELOGGEDINAS")%>: <b><asp:Label ID="lblLoginUser" runat="server" Text="John Dohle"></asp:Label></b>
            </div>
        </div>
        <!--today-date ENDS HERE-->
        <!--booking-heading START HERE-->
        <div class="booking-step3body">
            <div class="booking-step3-step1">
                <span><%= GetKeyResult("STEP1")%>.</span> <%= GetKeyResult("STEP1SELECTMEETINGROOM")%>
            </div>
            <div class="booking-step3-step2">
                <span><%= GetKeyResult("STEP2")%>.</span> <%= GetKeyResult("STEP2EXTRAOPTIONSBEDROOMS")%>
            </div>
            <div class="booking-step3-step3">
                <span><%= GetKeyResult("STEP3")%>.</span> <%= GetKeyResult("STEP3SENDIT")%>
            </div>
            <div class="booking-step3-step4">
                <span><%= GetKeyResult("STEPFINILIZED")%></span>
            </div>
        </div>
        <!--booking-heading ENDS HERE-->
        <!--booking-heading START HERE-->
        <div class="booking-heading1">
            <h1>
                <span><%= GetKeyResult("YOURBOOKING")%>:</span> <%= GetKeyResult("RECAPBOOKING")%> <%--#<asp:Label ID="lblBookingID" runat="server" >5789</asp:Label>--%></h1>
            <p >
            
                <b><asp:Label ID="lblCancelation" ForeColor="#8EC436"  Font-Size="14px"  runat="server" ></asp:Label></b></p>
        </div>
        <!--booking-heading ENDS HERE-->
        <div id="divWarnning" runat="server" class="warning" style="display:none;"></div>
        <!--booking-step3-detail-body START HERE-->
        <div class="booking-step3-detail-body">
            <!-- Client Details -->
            <div class="booking-step3-detail-inner">
                <div class="booking-step3-detail-box1">
                    <%= GetKeyResult("CLIENTNAME")%>:
                </div>
                <div class="booking-step3-detail-box2">
                    <asp:Label ID="lblclientName" runat="server" Text="John Dohle"></asp:Label>
                </div>
                <asp:Panel ID="pnlIsCompOrAgency" runat="server" >
                <div class="booking-step3-detail-box3">
                    <%= GetKeyResult("COMPANY")%>:
                </div>
                <div class="booking-step3-detail-box4">
                    <asp:Label ID="lblCompany" runat="server"></asp:Label>
                </div>
                <div class="booking-step3-detail-box5">
                    <asp:LinkButton ID="lnkChangeProfile" runat="server" Text="Change profile"></asp:LinkButton>
                </div>
                </asp:Panel>
            </div>
            <!-- Client Details End-->
            <!-- Booking Details -->
            <div class="booking-step3-detail-inner">
                <div class="booking-step3-detail-box1">
                    <%= GetKeyResult("DATEFROM")%>:
                </div>
                <div class="booking-step3-detail-box2">
                    <asp:Label ID="lblArivalDate" runat="server" >17 /10 / 2011</asp:Label>
                </div>
                <div class="booking-step3-detail-box3">
                    <%= GetKeyResult("DATETO")%>:
                </div>
                <div class="booking-step3-detail-box4">
                    <asp:Label ID="lblDepartureDate" runat="server" >17 /10 / 2011</asp:Label>
                </div>
                <div class="booking-step3-detail-box5">
                    <b><%= GetKeyResult("DURATION")%>:</b> &nbsp;&nbsp; <asp:Label ID="lblDuration" runat="server" >1day</asp:Label>
                </div>
            </div>
            <!-- Booking Details End-->
            <!-- Hotel Details -->
            <div class="booking-step3-detail-inner1">
                <div class="booking-step3-detail1-box1">
                    <%= GetKeyResult("HOTELNAME")%>:
                </div>
                <div class="booking-step3-detail1-box2">
                    <h3><asp:Label ID="lblHotelName" runat="server" ></asp:Label></h3>
                    <div class="star3">
                        <%--<img src="../images/star3.png">--%>
                        <asp:Image ID="imgHotelStars" runat="server" ImageUrl="~/images/star3.png" />
                    </div>
                    <p>
                        <asp:Label ID="lblHotelAddress" runat="server"></asp:Label></p>
                    <p>
                        <asp:Label ID="lblHotelPhone" runat="server"></asp:Label></p>
                </div>
                <div class="booking-step3-detail1-box3">
                    <a href="javascript:void(0);" id="showMap" >
                <img src="<%= SiteRootPath %>images/map-icon.png" align="absmiddle" /> <%= GetKeyResult("SHOWMAP")%></a>
                </div>
                <div id="maploc" style="background-color: #E5E3DF;border: 2px solid green;float: right;height: 176px;left: 923px;overflow: hidden;position: absolute;top: 294px;width: 249px;z-index: 51;display:none;" ></div>
                <input type="image" src="Images/close.png" style="float: right;position: absolute;z-index: 55;display:none;" class="close"/>
        <script language="javascript" type="text/javascript">
            jQuery(document).ready(function () {
                jQuery("#showMap").bind("click",function () {
                    var offsetshowmap = jQuery("#showMap").offset();
                    jQuery("#maploc").css({"top":(offsetshowmap.top+20) + "px","left":(offsetshowmap.left-150)+"px"});
                    jQuery(".close").css({"top":(offsetshowmap.top+10) + "px","left":(offsetshowmap.left + 90)+"px"})
                    jQuery("#maploc").show();
                    jQuery(".close").show();
                    LoadMapLatLong();
                });
                jQuery(".close").bind("click",function(){ jQuery("#maploc").hide();jQuery(".close").hide();return false;});
            });
            function LoadMapLatLong() {

                var map = new google.maps.Map(document.getElementById('maploc'), {
                    zoom: 14,
                    center: new google.maps.LatLng(<%= CurrentLat %>, <%= CurrentLong %>),
                    mapTypeId: google.maps.MapTypeId.ROADMAP

                });

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(<%= CurrentLat %>, <%= CurrentLong %>),
                    map: map
                });

            }
        </script>
            </div>
            <!-- Hotel Details End-->
        </div>
        <!--booking-step3-detail-body ENDS HERE-->
        <!--booking-step3-mainbody START HERE-->
        <div class="booking-step3-mainbody">
            <asp:Repeater ID="rptMeetingRoom" runat="server" 
                onitemdatabound="rptMeetingRoom_ItemDataBound" >
                <ItemTemplate>
                <div class="booking-step3-meeting-room">
                    <h1>
                        <%= GetKeyResult("MEETINGROOM")%> 1</h1></div>
                    <asp:Repeater ID="rptMeetingRoomConfigure" runat="server" OnItemDataBound="rptMeetingRoomConfigure_ItemDataBound" >
                        <ItemTemplate><div class="booking-step3-meeting-room">
                            <h3>
                                <%= GetKeyResult("DAY")%> <asp:Label ID="lblSelectedDay" runat="server" >1</asp:Label></h3></div>
                            <div class="booking-step3-meeting-room-heading">
                                <div class="booking-step3-meeting-room-heading1">
                                    &nbsp;
                                </div>
                                <div class="booking-step3-meeting-room-heading2">
                                    <%= GetKeyResult("DESCRIPTION")%>
                                </div>
                                <div class="booking-step3-meeting-room-heading3" id="divpriceMeetingroom4" runat="server">
                                    <%= GetKeyResult("PRICE")%>
                                </div>
                                <div class="booking-step3-meeting-room-heading4">
                                    <%= GetKeyResult("START")%>
                                </div>
                                <div class="booking-step3-meeting-room-heading5">
                                    <%= GetKeyResult("END")%>
                                </div>
                                <div class="booking-step3-meeting-room-heading6">
                                    <%= GetKeyResult("QTY")%>
                                </div>
                                <div class="booking-step3-meeting-room-heading7" id="divpriceMeetingroom3" runat="server">
                                    <%= GetKeyResult("TOTAL")%>
                                </div>
                            </div>
                            <ul>
                                <li>
                                    <div class="booking-step3-meeting-room-box-main">
                                        <div class="booking-step3-meeting-room-box1">
                                            "<asp:Label ID="lblMeetingRoomName" runat="server" ></asp:Label>"
                                        </div>
                                        <div class="booking-step3-meeting-room-box2">
                                            <p>
                                                <asp:Label ID="lblMeetingRoomType" runat="server" ></asp:Label></p>
                                            <p>
                                                <asp:Label ID="lblMinMaxCapacity" runat="server" ></asp:Label></p>
                                        </div>
                                        <div class="booking-step3-meeting-room-box3" id="divpriceMeetingroom2" runat="server">
                                            <span class="currencyClass"></span> <asp:Label ID="lblMeetingRoomActualPrice" runat="server"></asp:Label>
                                        </div>
                                        <div class="booking-step3-meeting-room-box4">
                                            <asp:Label ID="lblStart" runat="server" ></asp:Label>
                                        </div>
                                        <div class="booking-step3-meeting-room-box5">
                                            <asp:Label ID="lblEnd" runat="server"></asp:Label>
                                        </div>
                                        <div class="booking-step3-meeting-room-box6">
                                            <asp:Label ID="lblNumberOfParticepant" runat="server"></asp:Label>
                                        </div>
                                        <div class="booking-step3-meeting-room-box7" id="divpriceMeetingroom1" runat="server">
                                            <span class="currencyClass"></span> <asp:Label ID="lblTotalMeetingRoomPrice" runat="server" ></asp:Label>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <div class="booking-step3-meeting-room-total" id="divpriceMeetingroom" runat="server" >
                                <%= GetKeyResult("TOTAL")%>:&nbsp; &nbsp; <b><span class="currencyClass"></span> <asp:Label ID="lblTotalFinalMeetingRoomPrice" runat="server"></asp:Label></b>
                            </div>
                            
                            <asp:Panel ID="pnlIsPackageSelected" runat="server" >
                            <!--booking-step3-packages-mainbody START HERE-->
                            <div class="booking-step3-packages">
                                <h1>
                                    <%= GetKeyResult("PACKAGES")%></h1>
                                <div class="booking-step3-packages-heading">
                                    <div class="booking-step3-packages-heading1">
                                        &nbsp;
                                    </div>
                                    <div class="booking-step3-packages-heading2">
                                        <%= GetKeyResult("DESCRIPTION")%>
                                    </div>
                                    <div class="booking-step3-packages-heading7">
                                        <%= GetKeyResult("TOTAL")%>
                                    </div>
                                </div>
                                <ul>
                                    <li>
                                        <div class="booking-step3-packages-box-main">
                                            <div class="booking-step3-packages-box1">
                                                <asp:Label ID="lblSelectedPackage" runat="server" ></asp:Label>
                                            </div>
                                            <div class="booking-step3-packages-box2">
                                                <p>
                                                    <asp:Label ID="lblPackageDescription" runat="server"></asp:Label>
                                                </p>
                                            </div>
                                            <div class="booking-step3-packages-box7">
                                                <span class="currencyClass"></span> <asp:Label ID="lblPackagePrice" runat="server" ></asp:Label>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="choice1-box-popup-heading">
                                            <div class="choice1-box-popup-heading2">
                                                <%= GetKeyResult("STARTINGTIME")%>
                                            </div>
                                            <div class="choice1-box-popup-heading3">
                                                <%= GetKeyResult("STARTINGTIME")%>
                                            </div>
                                            <div class="choice1-box-popup-heading4">
                                                <%= GetKeyResult("QTY")%>
                                            </div>
                                        </div>
                                        <ul>
                                            <asp:Repeater ID="rptPackageItem" runat="server" OnItemDataBound="rptPackageItem_ItemDataBound">
                                                <ItemTemplate><asp:HiddenField ID="hdnItemID" runat="server" />
                                                    <li>
                                                        <div class="choice1-box-popup-box-main">
                                                            <div class="choice1-box-popup-box1">
                                                                <asp:Label ID="lblPackageItem" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="choice1-box-popup-box2">
                                                                <asp:Label ID="lblFromTime" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="choice1-box-popup-box3">
                                                                <asp:Label ID="lblToTime" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="choice1-box-popup-box4 txtmeetingroom1">
                                                                <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </li>
                                    <asp:Panel ID="pnlIsExtra" runat="server">
                                    <li>
                                        <div class="booking-step3-packages-box-main">
                                            <div class="booking-step3-packages-box1">
                                                <%= GetKeyResult("EXTRAS")%>
                                            </div>
                                            <div class="booking-step3-packages-box2">
                                                <p>
                                                    <%--Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam mollis, leo nec consequat
                                                    vehicula, felis augue scelerisque nunc,--%>
                                                </p>
                                            </div>
                                            <div class="booking-step3-packages-box7">
                                                
                                            </div>
                                        </div>
                                        <div class="choice1-box-popup-heading">
                                            <div class="choice1-box-popup-heading2">
                                                <%= GetKeyResult("FROM")%>
                                            </div>
                                            <div class="choice1-box-popup-heading3">
                                                <%= GetKeyResult("QTY")%>
                                            </div>
                                            <div class="choice1-box-popup-heading4">
                                                <%= GetKeyResult("TOTAL")%>
                                            </div>
                                        </div>
                                        <ul>
                                            <asp:Repeater ID="rptExtra" runat="server" OnItemDataBound="rptExtra_ItemDataBound">
                                                <ItemTemplate>
                                                    <li>
                                                        <div class="choice1-box-popup-box-main">
                                                            <div class="choice1-box-popup-box1">
                                                                <asp:Label ID="lblPackageItem" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="choice1-box-popup-box2">
                                                                <asp:Label ID="lblFrom" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="choice1-box-popup-box3">
                                                                <asp:Label ID="lblQuntity" runat="server" ></asp:Label>
                                                            </div>
                                                            <div class="choice1-box-popup-box4">
                                                                <span class="currencyClass"></span> <asp:Label ID="lblTotal" runat="server" ></asp:Label>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                <div class="booking-step3-meeting-room-total" >
                                Total:&nbsp; &nbsp; <b><span class="currencyClass"></span> <asp:Label runat="server" ID="lblExtraPrice" ></asp:Label></b>
                            </div>
                                    </li>
                                    </asp:Panel>
                                </ul>
                                <div class="booking-step3-packages-total">
                                    <%= GetKeyResult("TOTAL")%>:&nbsp; &nbsp; <b><span class="currencyClass"></span> <asp:Label ID="lblTotalPackagePrice" runat="server" ></asp:Label></b>
                                </div>
                            </div>
                            <!--booking-step3-packages ENDS HERE-->
                            </asp:Panel>
                            <asp:Panel ID="pnlBuildMeeting" runat="server" >
                            <!--booking-step3-BuildMeeting-mainbody START HERE-->
                            <div class="booking-step3-equipment">
                                <h1>
                                    <%= GetKeyResult("CHOICE2BUILDYOURMEETING")%></h1>
                                <div class="booking-step3-equipment-heading">
                                    <div class="booking-step3-equipment-heading1">
                                        &nbsp;
                                    </div>
                                    <div class="booking-step3-equipment-heading2">
                                        <%= GetKeyResult("DESCRIPTION")%>
                                    </div>
                                    <div class="booking-step3-equipment-heading3">
                                        <%= GetKeyResult("QTY")%>
                                    </div>
                                    <div class="booking-step3-equipment-heading7">
                                        <%= GetKeyResult("TOTAL")%>
                                    </div>
                                </div>
                                <ul>
                                    <asp:Repeater ID="rptBuildMeeting" runat="server" OnItemDataBound="rptBuildMeeting_ItemDataBound">
                                        <ItemTemplate>
                                            <li>
                                                <div class="booking-step3-equipment-box-main">
                                                    <div class="booking-step3-equipment-box1">
                                                        <asp:Label ID="lblItemName" runat="server" ></asp:Label> 
                                                    </div>
                                                    <div class="booking-step3-equipment-box2">
                                                        <p>
                                                            <asp:Label ID="lblItemDescription" runat="server"></asp:Label>
                                                        </p>
                                                    </div>
                                                    <div class="booking-step3-equipment-box3">
                                                        <asp:Label ID="lblQuantity" runat="server" ></asp:Label>
                                                    </div>
                                                    <div class="booking-step3-equipment-box7">
                                                        <span class="currencyClass"></span> <asp:Label ID="lblTotalItemPrice" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                                <div class="booking-step3-equipment-total">
                                    <%= GetKeyResult("TOTAL")%>:&nbsp; &nbsp; <b><span class="currencyClass"></span> <asp:Label ID="lblTotalBuildPackagePrice" runat="server"></asp:Label></b>
                                </div>
                            </div>
                            </asp:Panel>
                            
                            <!--booking-step3-BuildMeeting ENDS HERE-->
                            <asp:Panel ID="pnlEquipment" runat="server" >
                            <!--booking-step3-equipment-mainbody START HERE-->
                            <div class="booking-step3-equipment">
                                <h1>
                                    <%= GetKeyResult("EQUIPMENT")%></h1>
                                <div class="booking-step3-equipment-heading">
                                    <div class="booking-step3-equipment-heading1">
                                        &nbsp;
                                    </div>
                                    <div class="booking-step3-equipment-heading2">
                                        <%= GetKeyResult("DESCRIPTION")%>
                                    </div>
                                    <div class="booking-step3-equipment-heading3">
                                        <%= GetKeyResult("QTY")%>
                                    </div>
                                    <div class="booking-step3-equipment-heading7">
                                        <%= GetKeyResult("TOTAL")%>
                                    </div>
                                </div>
                                <ul>
                                    <asp:Repeater ID="rptEquipment" runat="server" OnItemDataBound="rptEquipment_ItemDataBound">
                                        <ItemTemplate>
                                            <li>
                                                <div class="booking-step3-equipment-box-main">
                                                    <div class="booking-step3-equipment-box1">
                                                        <asp:Label ID="lblItemName" runat="server" ></asp:Label>
                                                    </div>
                                                    <div class="booking-step3-equipment-box2">
                                                        <p>
                                                            <asp:Label ID="lblItemDescription" runat="server" ></asp:Label>
                                                        </p>
                                                    </div>
                                                    <div class="booking-step3-equipment-box3">
                                                        <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="booking-step3-equipment-box7">
                                                        <span class="currencyClass"></span> <asp:Label ID="lblTotalPrice" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                                <div class="booking-step3-equipment-total">
                                    <%= GetKeyResult("TOTAL")%>:&nbsp; &nbsp; <b><span class="currencyClass"></span> <asp:Label ID="lblTotalEquipmentPrice" runat="server"></asp:Label></b>
                                </div>
                            </div>
                            <!--booking-step3-equipment ENDS HERE-->
                            </asp:Panel>
                            <asp:Panel ID="pnlOthers" runat="server" >
                            <!--booking-step3-equipment-mainbody START HERE-->
                            <div class="booking-step3-equipment">
                                <h1>
                                    <%= GetKeyResult("OTHERS")%></h1>
                                <div class="booking-step3-equipment-heading">
                                    <div class="booking-step3-equipment-heading1">
                                        &nbsp;
                                    </div>
                                    <div class="booking-step3-equipment-heading2">
                                        <%= GetKeyResult("DESCRIPTION")%>
                                    </div>
                                    <div class="booking-step3-equipment-heading3">
                                        <%= GetKeyResult("QTY")%>
                                    </div>
                                    <div class="booking-step3-equipment-heading7">
                                        <%= GetKeyResult("TOTAL")%>
                                    </div>
                                </div>
                                <ul>
                                    <asp:Repeater ID="rptOthers" runat="server" OnItemDataBound="rptOthers_ItemDataBound">
                                        <ItemTemplate>
                                            <li>
                                                <div class="booking-step3-equipment-box-main">
                                                    <div class="booking-step3-equipment-box1">
                                                        <asp:Label ID="lblItemName" runat="server" ></asp:Label>
                                                    </div>
                                                    <div class="booking-step3-equipment-box2">
                                                        <p>
                                                            <asp:Label ID="lblItemDescription" runat="server" ></asp:Label>
                                                        </p>
                                                    </div>
                                                    <div class="booking-step3-equipment-box3">
                                                        <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="booking-step3-equipment-box7">
                                                        <span class="currencyClass"></span> <asp:Label ID="lblTotalPrice" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                                <div class="booking-step3-equipment-total">
                                    <%= GetKeyResult("TOTAL")%>:&nbsp; &nbsp; <b><span class="currencyClass"></span> <asp:Label ID="lblOtherTotals" runat="server"></asp:Label></b>
                                </div>
                            </div>
                            </asp:Panel>
                        </ItemTemplate>
                    </asp:Repeater>
                </ItemTemplate>
                <AlternatingItemTemplate>
                <div class="booking-step3-meeting-room">
                    <h1>
                    <%= GetKeyResult("MEETINGROOM")%> 2</h1></div>
                    <asp:Repeater ID="rptMeetingRoomConfigure" runat="server" OnItemDataBound="rptMeetingRoomConfigure_ItemDataBound" >
                        <ItemTemplate><div class="booking-step3-meeting-room">
                            <h3>
                                <%= GetKeyResult("DAY")%> <asp:Label ID="lblSelectedDay" runat="server" >1</asp:Label></h3></div>
                            <div class="booking-step3-meeting-room-heading">
                                <div class="booking-step3-meeting-room-heading1">
                                    &nbsp;
                                </div>
                                <div class="booking-step3-meeting-room-heading2">
                                    <%= GetKeyResult("DESCRIPTION")%>
                                </div>
                                <div class="booking-step3-meeting-room-heading3" id="divpriceMeetingroom4" runat="server">
                                    <%= GetKeyResult("PRICE")%>
                                </div>
                                <div class="booking-step3-meeting-room-heading4">
                                    <%= GetKeyResult("START")%>
                                </div>
                                <div class="booking-step3-meeting-room-heading5">
                                    <%= GetKeyResult("END")%>
                                </div>
                                <div class="booking-step3-meeting-room-heading6">
                                    <%= GetKeyResult("QTY")%>
                                </div>
                                <div class="booking-step3-meeting-room-heading7" id="divpriceMeetingroom3" runat="server">
                                    <%= GetKeyResult("TOTAL")%>
                                </div>
                            </div>
                            <ul>
                                <li>
                                    <div class="booking-step3-meeting-room-box-main">
                                        <div class="booking-step3-meeting-room-box1">
                                            "<asp:Label ID="lblMeetingRoomName" runat="server" ></asp:Label>"
                                        </div>
                                        <div class="booking-step3-meeting-room-box2">
                                            <p>
                                                <asp:Label ID="lblMeetingRoomType" runat="server" ></asp:Label></p>
                                            <p>
                                                <asp:Label ID="lblMinMaxCapacity" runat="server" ></asp:Label></p>
                                        </div>
                                        <div class="booking-step3-meeting-room-box3" id="divpriceMeetingroom2" runat="server">
                                            <span class="currencyClass"></span> <asp:Label ID="lblMeetingRoomActualPrice" runat="server"> </asp:Label>
                                        </div>
                                        <div class="booking-step3-meeting-room-box4">
                                            <asp:Label ID="lblStart" runat="server" ></asp:Label>
                                        </div>
                                        <div class="booking-step3-meeting-room-box5">
                                            <asp:Label ID="lblEnd" runat="server"></asp:Label>
                                        </div>
                                        <div class="booking-step3-meeting-room-box6">
                                            <asp:Label ID="lblNumberOfParticepant" runat="server"></asp:Label>
                                        </div>
                                        <div class="booking-step3-meeting-room-box7" id="divpriceMeetingroom1" runat="server">
                                            <span class="currencyClass"></span> <asp:Label ID="lblTotalMeetingRoomPrice" runat="server" > </asp:Label>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <div class="booking-step3-meeting-room-total" id="divpriceMeetingroom" runat="server">
                                <%= GetKeyResult("TOTAL")%>:&nbsp; &nbsp; <b><span class="currencyClass"></span> <asp:Label ID="lblTotalFinalMeetingRoomPrice" runat="server"> </asp:Label></b>
                            </div>
                            
                            <asp:Panel ID="pnlIsBreakdown" runat="server" >
                            <div class="special-request-step3-inner">
                                <h3><%= GetKeyResult("MEETINGBREAKDOWN")%></h3></div>
                            </asp:Panel>
                            <asp:Panel ID="pnlNotIsBreakdown" runat="server" >
                            <asp:Panel ID="pnlIsPackageSelected" runat="server" >
                            <!--booking-step3-packages-mainbody START HERE-->
                            <div class="booking-step3-packages">
                                <h1>
                                    <%= GetKeyResult("PACKAGES")%></h1>
                                <div class="booking-step3-packages-heading">
                                    <div class="booking-step3-packages-heading1">
                                        &nbsp;
                                    </div>
                                    <div class="booking-step3-packages-heading2">
                                        <%= GetKeyResult("DESCRIPTION")%>
                                    </div>
                                    <div class="booking-step3-packages-heading7">
                                        <%= GetKeyResult("TOTAL")%>
                                    </div>
                                </div>
                                <ul>
                                    <li>
                                        <div class="booking-step3-packages-box-main">
                                            <div class="booking-step3-packages-box1">
                                                <asp:Label ID="lblSelectedPackage" runat="server" ></asp:Label>
                                            </div>
                                            <div class="booking-step3-packages-box2">
                                                <p>
                                                    <asp:Label ID="lblPackageDescription" runat="server"></asp:Label>
                                                </p>
                                            </div>
                                            <div class="booking-step3-packages-box7">
                                                <span class="currencyClass"></span> <asp:Label ID="lblPackagePrice" runat="server" ></asp:Label>
                                            </div>
                                        </div>
                                    </li>
                                    <li><div class="choice1-box-popup-heading">
                                            <div class="choice1-box-popup-heading2">
                                                <%= GetKeyResult("STARTINGTIME")%>
                                            </div>
                                            <div class="choice1-box-popup-heading3">
                                                <%= GetKeyResult("STARTINGTIME")%>
                                            </div>
                                            <div class="choice1-box-popup-heading4">
                                                <%= GetKeyResult("QTY")%>
                                            </div>
                                        </div>
                                    <ul>
                                            <asp:Repeater ID="rptPackageItem" runat="server" OnItemDataBound="rptPackageItem_ItemDataBound">
                                                <ItemTemplate><asp:HiddenField ID="hdnItemID" runat="server" />
                                                    <li>
                                                        <div class="choice1-box-popup-box-main">
                                                            <div class="choice1-box-popup-box1">
                                                                <asp:Label ID="lblPackageItem" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="choice1-box-popup-box2">
                                                                <asp:Label ID="lblFromTime" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="choice1-box-popup-box3">
                                                                <asp:Label ID="lblToTime" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="choice1-box-popup-box4 txtmeetingroom1">
                                                                <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul></li>
                                    <asp:Panel ID="pnlIsExtra" runat="server">
                                    <li>
                                        <div class="booking-step3-packages-box-main">
                                            <div class="booking-step3-packages-box1">
                                                <%= GetKeyResult("EXTRAS")%>
                                            </div>
                                            <div class="booking-step3-packages-box2">
                                                <p>
                                                    <%--Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam mollis, leo nec consequat
                                                    vehicula, felis augue scelerisque nunc,--%>
                                                </p>
                                            </div>
                                            <div class="booking-step3-packages-box7">
                                                
                                            </div>
                                        </div>
                                        <div class="choice1-box-popup-heading">
                                            <div class="choice1-box-popup-heading2">
                                                <%= GetKeyResult("FROM")%>
                                            </div>
                                            <div class="choice1-box-popup-heading3">
                                                <%= GetKeyResult("QTY")%>
                                            </div>
                                            <div class="choice1-box-popup-heading4">
                                                <%= GetKeyResult("TOTAL")%>
                                            </div>
                                        </div>
                                        <ul>
                                            <asp:Repeater ID="rptExtra" runat="server" OnItemDataBound="rptExtra_ItemDataBound">
                                                <ItemTemplate>
                                                    <li>
                                                        <div class="choice1-box-popup-box-main">
                                                            <div class="choice1-box-popup-box1">
                                                                <asp:Label ID="lblPackageItem" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="choice1-box-popup-box2">
                                                                <asp:Label ID="lblFrom" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="choice1-box-popup-box3">
                                                                <asp:Label ID="lblQuntity" runat="server" ></asp:Label>
                                                            </div>
                                                            <div class="choice1-box-popup-box4">
                                                                <span class="currencyClass"></span> <asp:Label ID="lblTotal" runat="server" ></asp:Label>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                <div class="booking-step3-meeting-room-total" >
                                <%= GetKeyResult("TOTAL")%>:&nbsp; &nbsp; <b><span class="currencyClass"></span> <asp:Label runat="server" ID="lblExtraPrice" ></asp:Label></b>
                            </div>
                                    </li>
                                    </asp:Panel>
                                </ul>
                                <div class="booking-step3-packages-total">
                                    <%= GetKeyResult("TOTAL")%>:&nbsp; &nbsp; <b><span class="currencyClass"></span> <asp:Label ID="lblTotalPackagePrice" runat="server" ></asp:Label></b>
                                </div>
                            </div>
                            <!--booking-step3-packages ENDS HERE-->
                            </asp:Panel>
                            <asp:Panel ID="pnlBuildMeeting" runat="server" >
                            <!--booking-step3-BuildMeeting-mainbody START HERE-->
                            <div class="booking-step3-equipment">
                                <h1>
                                    <%= GetKeyResult("CHOICE2BUILDYOURMEETING")%></h1>
                                <div class="booking-step3-equipment-heading">
                                    <div class="booking-step3-equipment-heading1">
                                        &nbsp;
                                    </div>
                                    <div class="booking-step3-equipment-heading2">
                                        <%= GetKeyResult("DESCRIPTION")%>
                                    </div>
                                    <div class="booking-step3-equipment-heading3">
                                        <%= GetKeyResult("QTY")%>
                                    </div>
                                    <div class="booking-step3-equipment-heading7">
                                        <%= GetKeyResult("TOTAL")%>
                                    </div>
                                </div>
                                <ul>
                                    <asp:Repeater ID="rptBuildMeeting" runat="server" OnItemDataBound="rptBuildMeeting_ItemDataBound">
                                        <ItemTemplate>
                                            <li>
                                                <div class="booking-step3-equipment-box-main">
                                                    <div class="booking-step3-equipment-box1">
                                                        <asp:Label ID="lblItemName" runat="server" ></asp:Label> 
                                                    </div>
                                                    <div class="booking-step3-equipment-box2">
                                                        <p>
                                                            <asp:Label ID="lblItemDescription" runat="server"></asp:Label>
                                                        </p>
                                                    </div>
                                                    <div class="booking-step3-equipment-box3">
                                                        <asp:Label ID="lblQuantity" runat="server" ></asp:Label>
                                                    </div>
                                                    <div class="booking-step3-equipment-box7">
                                                        <span class="currencyClass"></span> <asp:Label ID="lblTotalItemPrice" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                                <div class="booking-step3-equipment-total">
                                    <%= GetKeyResult("TOTAL")%>:&nbsp; &nbsp; <b><span class="currencyClass"></span> <asp:Label ID="lblTotalBuildPackagePrice" runat="server"> </asp:Label></b>
                                </div>
                            </div>
                            </asp:Panel>
                            <!--booking-step3-BuildMeeting ENDS HERE-->
                            
                            <asp:Panel ID="pnlEquipment" runat="server" >
                            <!--booking-step3-equipment-mainbody START HERE-->
                            <div class="booking-step3-equipment">
                                <h1>
                                    <%= GetKeyResult("EQUIPMENT")%></h1>
                                <div class="booking-step3-equipment-heading">
                                    <div class="booking-step3-equipment-heading1">
                                        &nbsp;
                                    </div>
                                    <div class="booking-step3-equipment-heading2">
                                        <%= GetKeyResult("DESCRIPTION")%>
                                    </div>
                                    <div class="booking-step3-equipment-heading3">
                                        <%= GetKeyResult("QTY")%>
                                    </div>
                                    <div class="booking-step3-equipment-heading7">
                                        <%= GetKeyResult("TOTAL")%>
                                    </div>
                                </div>
                                <ul>
                                    <asp:Repeater ID="rptEquipment" runat="server" OnItemDataBound="rptEquipment_ItemDataBound">
                                        <ItemTemplate>
                                            <li>
                                                <div class="booking-step3-equipment-box-main">
                                                    <div class="booking-step3-equipment-box1">
                                                        <asp:Label ID="lblItemName" runat="server" ></asp:Label>
                                                    </div>
                                                    <div class="booking-step3-equipment-box2">
                                                        <p>
                                                            <asp:Label ID="lblItemDescription" runat="server" ></asp:Label>
                                                        </p>
                                                    </div>
                                                    <div class="booking-step3-equipment-box3">
                                                        <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="booking-step3-equipment-box7">
                                                        <span class="currencyClass"></span> <asp:Label ID="lblTotalPrice" runat="server"> </asp:Label>
                                                    </div>
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                                <div class="booking-step3-equipment-total">
                                    <%= GetKeyResult("TOTAL")%>:&nbsp; &nbsp; <b><span class="currencyClass"></span> <asp:Label ID="lblTotalEquipmentPrice" runat="server"> </asp:Label></b>
                                </div>
                            </div>
                            <!--booking-step3-equipment ENDS HERE-->
                            </asp:Panel>

                            <asp:Panel ID="pnlOthers" runat="server" >
                            <!--booking-step3-equipment-mainbody START HERE-->
                            <div class="booking-step3-equipment">
                                <h1>
                                    <%= GetKeyResult("OTHERS")%></h1>
                                <div class="booking-step3-equipment-heading">
                                    <div class="booking-step3-equipment-heading1">
                                        &nbsp;
                                    </div>
                                    <div class="booking-step3-equipment-heading2">
                                        <%= GetKeyResult("DESCRIPTION")%>
                                    </div>
                                    <div class="booking-step3-equipment-heading3">
                                        <%= GetKeyResult("QTY")%>
                                    </div>
                                    <div class="booking-step3-equipment-heading7">
                                        <%= GetKeyResult("TOTAL")%>
                                    </div>
                                </div>
                                <ul>
                                    <asp:Repeater ID="rptOthers" runat="server" OnItemDataBound="rptOthers_ItemDataBound">
                                        <ItemTemplate>
                                            <li>
                                                <div class="booking-step3-equipment-box-main">
                                                    <div class="booking-step3-equipment-box1">
                                                        <asp:Label ID="lblItemName" runat="server" ></asp:Label>
                                                    </div>
                                                    <div class="booking-step3-equipment-box2">
                                                        <p>
                                                            <asp:Label ID="lblItemDescription" runat="server" ></asp:Label>
                                                        </p>
                                                    </div>
                                                    <div class="booking-step3-equipment-box3">
                                                        <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="booking-step3-equipment-box7">
                                                        <span class="currencyClass"></span> <asp:Label ID="lblTotalPrice" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                                <div class="booking-step3-equipment-total">
                                    <%= GetKeyResult("TOTAL")%>:&nbsp; &nbsp; <b><span class="currencyClass"></span> <asp:Label ID="lblOtherTotals" runat="server"></asp:Label></b>
                                </div>
                            </div>
                            </asp:Panel>
                            </asp:Panel>
                        </ItemTemplate>
                    </asp:Repeater>
                </AlternatingItemTemplate>
            </asp:Repeater>
            
                    
            <asp:Panel ID="pnlAccomodation" runat="server" >
            <!--Accomodation START HERE-->
        <div class="accomodation-body">
            <h1>
                <%= GetKeyResult("ACCOMODATION")%>.</h1>
            <div class="booking-booking-details-bodytop2" >
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <asp:Repeater ID="rptAccomodationDaysManager" runat="server" OnItemDataBound="rptAccomodationDaysManager_ItemDataBound">
                        <HeaderTemplate>
                            <tr bgcolor="#d4d9cc">
                                <td colspan="2" style="border-top: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;
                                    padding: 0px">
                                    <%= GetKeyResult("BEDROOMDATE")%>
                                </td>
                                <asp:Repeater ID="rptDays2" runat="server" OnItemDataBound="rptDays2_ItemDataBound" >
                                    <ItemTemplate>
                                        <td valign="top" style="border-left: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;
                                            border-top: #bfd2a5 solid 1px; padding: 0px">
                                            <asp:Label ID="lblDay" runat="server"></asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendDays" runat="server" ></asp:Literal>
                                <td valign="top" style="border-left: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;
                                    border-top: #bfd2a5 solid 1px; padding: 0px; width: 70px">
                                    <%= GetKeyResult("TOTAL")%>
                                </td>
                            </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td colspan="8" style="padding: 0px">
                                    <strong>&quot;<asp:Label ID="lblBedroomType" runat="server"></asp:Label>&quot;<asp:HiddenField ID="hdnBedroomId" runat="server" /> </strong>
                                </td>
                                <td valign="top" style="padding: 0px" align="center">
                                    
                                </td>
                            </tr>
                            <tr bgcolor="#cee5ad">
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                  <%= GetKeyResult("PRICE")%>  
                                </td>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("SINGLE")%><br />
                                            <%= GetKeyResult("DOUBLE")%></td>
                                <asp:Repeater ID="rptPrice" runat="server" OnItemDataBound="rptPrice_ItemDataBound">
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                            <span class="currencyClass"></span>
                                            <asp:Label ID="lblPriceDay" runat="server"></asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendPrice" runat="server"></asp:Literal>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("ONLINEAVAILABILITY") %>
                                </td>
                                <asp:Repeater ID="rptNoOfDays" runat="server" OnItemDataBound="rptNoOfDays_ItemDataBound">
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                            <asp:Label ID="lblAvailableRoomDay" runat="server">3</asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendNoDays" runat="server"></asp:Literal>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr bgcolor="#cee5ad">
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("YOURQTY")%>
                                </td>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("SINGLE")%>
                                </td>
                                <asp:Repeater ID="rptSingleQuantity" runat="server" OnItemDataBound="rptSingleQuantity_ItemDataBound" >
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                            <asp:HiddenField ID="hdnDate" runat="server" />
                                            <asp:HiddenField ID="hdnPrice" runat="server" />
                                            <asp:Label ID="lblQuantitySDay" runat="server"></asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendSQuantity" runat="server"></asp:Literal>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr bgcolor="#cee5ad">
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("DOUBLE")%>
                                </td>
                                <asp:Repeater ID="rptDoubleQuantity" runat="server" OnItemDataBound="rptDoubleQuantity_ItemDataBound">
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                            <asp:HiddenField ID="hdnDate" runat="server" />
                                            <asp:HiddenField ID="hdnPrice" runat="server" />
                                            <asp:Label ID="lblQuantityDDay" runat="server"></asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendDQuantity" runat="server"></asp:Literal>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">&nbsp;</td>
                                <%--<td colspan="2" style="padding: 0px">
                                    
                                </td>--%>
                                <td colspan="4" style="padding: 0px;height:45px;">
                                    <%--<div class="n-btn-re">
                                        <a href="#">
                                            <div class="n-btn-left">
                                            </div>
                                            <div class="n-btn-mid">
                                                (re) Calculate</div>
                                            <div class="n-btn-right">
                                            </div>
                                        </a>
                                    </div>--%>
                                    <%-- <div class="n-btn-red" id="ckkAvail1" style="display:none;">
                                        <a href="javascript:void(0);" onclick="return CheckAvailabilityRoom(1);">
                                            <div class="n-btn-red-left">
                                            </div>
                                            <div class="n-btn-red-mid">
                                                <%= GetKeyResult("CHECKAVAILABILITY")%></div>
                                            <div class="n-btn-red-right">
                                            </div>
                                        </a>
                                    </div>--%>
                                </td>
                                <td valign="top" style="padding: 0px" align="center">
                                    <span class="currencyClass"></span> <asp:Label ID="lblTotalAccomodation" CssClass="price" runat="server">0.00</asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr>
                                <td colspan="8" style="padding: 0px">
                                    <strong>&quot;<asp:Label ID="lblBedroomType" runat="server"></asp:Label>&quot; <asp:HiddenField ID="hdnBedroomId" runat="server" /></strong>
                                </td>
                                <td valign="top" style="padding: 0px" align="center">
                                    
                                </td>
                            </tr>
                            <tr bgcolor="#cee5ad">
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("PRICE")%>
                                </td>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("SINGLE")%><br />
                                            <%= GetKeyResult("DOUBLE")%></td>
                                <asp:Repeater ID="rptPrice" runat="server" OnItemDataBound="rptPrice_ItemDataBound">
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                            <span class="currencyClass">€</span>
                                            <asp:Label ID="lblPriceDay" runat="server"></asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendPrice" runat="server"></asp:Literal>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("ONLINEAVAILABILITY") %>
                                </td>
                                <asp:Repeater ID="rptNoOfDays" runat="server" OnItemDataBound="rptNoOfDays_ItemDataBound">
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                            <asp:Label ID="lblAvailableRoomDay" runat="server">3</asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendNoDays" runat="server"></asp:Literal>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr bgcolor="#cee5ad">
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("YOURQTY")%>
                                </td>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("SINGLE")%>
                                </td>
                                <asp:Repeater ID="rptSingleQuantity" runat="server" OnItemDataBound="rptSingleQuantity_ItemDataBound" >
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                            <asp:HiddenField ID="hdnDate" runat="server" />
                                            <asp:HiddenField ID="hdnPrice" runat="server" />
                                            <asp:Label ID="lblQuantitySDay" runat="server" ></asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendSQuantity" runat="server"></asp:Literal>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr bgcolor="#cee5ad">
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("DOUBLE")%>
                                </td>
                                <asp:Repeater ID="rptDoubleQuantity" runat="server" OnItemDataBound="rptDoubleQuantity_ItemDataBound">
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                            <asp:HiddenField ID="hdnDate" runat="server" />
                                            <asp:HiddenField ID="hdnPrice" runat="server" />
                                            <asp:Label ID="lblQuantityDDay" runat="server"></asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendDQuantity" runat="server"></asp:Literal>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">&nbsp;</td>
                                <%--<td colspan="2" style="padding: 0px">
                                    
                                </td>--%>
                                <td colspan="4" style="padding: 0px;height:45px;">
                                    <%--<div class="n-btn-re">
                                        <a href="#">
                                            <div class="n-btn-left">
                                            </div>
                                            <div class="n-btn-mid">
                                                (re) Calculate</div>
                                            <div class="n-btn-right">
                                            </div>
                                        </a>
                                    </div>--%>
                                    <%-- <div class="n-btn-red" id="ckkAvail2" style="display:none;" >
                                        <a href="javascript:void(0);" onclick="return CheckAvailabilityRoom(2);">
                                            <div class="n-btn-red-left">
                                            </div>
                                            <div class="n-btn-red-mid">
                                                <%= GetKeyResult("CHECKAVAILABILITY")%></div>
                                            <div class="n-btn-red-right">
                                            </div>
                                        </a>
                                    </div>--%>
                                </td>
                                <td valign="top" style="padding: 0px" align="center">
                                    <span class="currencyClass"></span> <asp:Label ID="lblTotalAccomodation" CssClass="price" runat="server">0.00</asp:Label>
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                    <tr>
                        <td colspan="9" style="padding: 0px;" class="special-request-textareabox">
                            <strong><%= GetKeyResult("SPECIALREQUEST")%></strong>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="9" style="padding: 0px;" class="special-request-textareabox">
                            <asp:Label ID="lblNoteBedroom" runat="server" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="padding: 0px">
                            &nbsp;
                        </td>
                        <td valign="top" style="padding: 0px">
                            &nbsp;
                        </td>
                        <td valign="top" style="padding: 0px">
                            &nbsp;
                        </td>
                        <td valign="top" style="padding: 0px">
                            &nbsp;
                        </td>
                        <td valign="top" style="padding: 0px">
                            &nbsp;
                        </td>
                        <td colspan="3" valign="top" style="padding: 0px" align="right">
                            <%= GetKeyResult("ACCOMODATION")%> :
                        </td>
                        <td valign="top" style="padding: 0px">
                           <span class="currencyClass"></span> <asp:Label ID="lblTotalAccomodation" CssClass="price" runat="server">0.00</asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- Accomodation Code Remove from here-->
        </div>
        <!--Accomodation ENDS HERE-->
            </asp:Panel> 
            <!--special-request-step3-content START HERE-->
            <div class="special-request-step3">
                <h1>
                    <%= GetKeyResult("SPECIALREQUEST")%>.</h1>
                <div class="special-request-step3-inner">
                    <asp:Label ID="lblSpecialRequest" runat="server"></asp:Label>
                </div>
            </div>
            <!--special-request-step3-detail-body ENDS HERE-->
            <!--booking-step3-content START HERE-->
            <div class="booking-step3-content">
                <h3>
                    <%= GetKeyResult("TERMSANDCONDITIONS")%></h3>
                <p>
                    <asp:HyperLink ID="hypTermsandCondition" runat="server"  ><%= GetKeyResult("TERMSANDCONDITIONSTEXT")%>.</asp:HyperLink> 
                </p>
                <div id="divCancelation" runat="server" style="float:left;">
                <h3>
                    <%= GetKeyResult("CANCELLATIONPOLICY")%></h3>
                <p>
                    <asp:LinkButton ID="tAndCPopUp" runat="server"><%= GetKeyResult("CANCELLATIONTEXT")%>.</asp:LinkButton>
                </p></div>
                <br /><br /><br />
                <input type="checkbox" id="chkIAgree" />&nbsp;&nbsp;<b><%= GetKeyResult("IREADANDAGREE")%></b>
                                
            </div>
            <!--booking-step3-content ENDS HERE-->

            <!--Final Total START HERE-->
            <div class="final-total-body">
                <div class="final-total-inner-body">
                    <div class="final-total-inner" style="display:none;">
                        <div class="final-total-inner-left">
                                <%= GetKeyResult("NETTOTAL")%>:
                        </div>
                        <div class="final-total-inner-right">
                                <span class="currencyClass"></span> <asp:Label ID="lblNetTotal" runat="server"></asp:Label>
                        </div>
                    </div>
                    <asp:Repeater ID="rptVatList" runat="server" OnItemDataBound="rptVatList_ItemDataBound">
                    <ItemTemplate>
                    <div class="final-total-inner" style="display:none;">
                        <div class="final-total-inner-left">
                            <%= GetKeyResult("VAT")%> <asp:Label ID="lblPercentage" runat="server"><%#Eval("VATPercent","{0:0.00}") %></asp:Label>%:
                        </div>
                        <div class="final-total-inner-right">
                            <span class="currencyClass"></span> <asp:Label ID="lblVatPrice" runat="server"></asp:Label>
                        </div>
                    </div>
                    </ItemTemplate>
                    </asp:Repeater>
                    <div class="final-total-inner">
                        <div class="final-total-inner-left">
                            <h1>
                                <%= GetKeyResult("FINALTOTAL")%>:</h1>
                        </div>
                        <div class="final-total-inner-right">
                            <h1>
                                <span class="currencyClass"></span> <asp:Label ID="lblFinalTotal" runat="server"></asp:Label></h1>
                        </div>
                    </div>
                </div>
            </div>
            <!--special-request ENDS HERE-->
        </div>
        <!--booking-step3-mainbody ENDS HERE-->
        <!--next button START HERE-->
        <div class="next-button">
            <asp:LinkButton ID="lnkPrevious" runat="server" CssClass="step-pre-btn" 
            onclick="lnkPrevious_Click" ><%= GetKeyResult("PREVIOUSSTEP")%></asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton 
            ID="btnNext" runat="server" CssClass="confirm-btn" Text="Confirm" 
                onclick="btnNext_Click1" ></asp:LinkButton>
        </div>
        <!--next button ENDS HERE-->
    </div>
    <!--mainbody ENDS HERE-->


     <div id="divpolicy-overlay" >
    </div>
    <div id="divpolicy">
       
       

              <div class="error" id="Div2" runat="server">
                </div>

                <div runat="server" visible="false" style=" padding:5px; width:640px;">
        <asp:Label ID="lblMessage" runat="server" Text="Policy not exist" ForeColor="#e48249" Visible="false"></asp:Label>
        </div>

        <table>

        <tr><th><%= GetKeyResult("CANCELATIONPOLICY")%></th></tr>


        <tr>
        <td style=" border-top: 1px solid #A3D4F7; padding:5px;  width:640px;">
            <%= GetKeyResult("NAME")%> : <b>
                <asp:Label ID="lblName" runat="server" Text=""></asp:Label></b></td>
        </tr>
         <tr>
        <td style=" border-top: 1px solid #A3D4F7; padding:5px;">
            <%= GetKeyResult("USERCANCANCEL")%> : <b>
                    <asp:Label ID="lblCalcellationLimit" runat="server" Text=""></asp:Label></b></td>
                
        </tr>

         <tr>
        <td style=" border-top: 1px solid #A3D4F7; padding:5px;">
           <b><%= GetKeyResult("SECTION")%> 1</b> <%= GetKeyResult("BETWEENDAY")%> <b>
                <asp:Label ID="lblTimeFrameMax1" runat="server" Text=""></asp:Label></b> <%= GetKeyResult("AND")%>
            <%= GetKeyResult("DAY")%> <b>
                <asp:Label ID="lblTimeFrameMin1" runat="server" Text=""></asp:Label></b></td>
        </tr>
        <tr>
        <td style="  border: 1px solid #0377C8; padding:5px;"> <asp:Label ID="lblSection1" runat="server" Text=""></asp:Label></td>
        </tr>

         <tr>
        <td style=" padding:5px;">
           <b><%= GetKeyResult("SECTION")%> 2</b> <%= GetKeyResult("BETWEENDAY")%> <b>
                <asp:Label ID="lblTimeFrameMax2" runat="server" Text=""></asp:Label></b> <%= GetKeyResult("AND")%>
            <%= GetKeyResult("DAY")%> <b>
                <asp:Label ID="lblTimeFrameMin2" runat="server" Text=""></asp:Label></b></td>
        </tr>
         <tr>
        <td style="  border: 1px solid #0377C8; padding:5px;"> <asp:Label ID="lblSection2" runat="server" Text=""></asp:Label></td>
        </tr>
         <tr>
        <td style=" padding:5px;">
           <b><%= GetKeyResult("SECTION")%> 3</b> <%= GetKeyResult("BETWEENDAY")%> <b>
                <asp:Label ID="lblTimeFrameMax3" runat="server" Text=""></asp:Label></b> <%= GetKeyResult("AND")%>
            <%= GetKeyResult("DAY")%> <b>
                <asp:Label ID="lblTimeFrameMin3" runat="server" Text=""></asp:Label></b></td>
        </tr>

        <tr>
        <td style="  border: 1px solid #0377C8; padding:5px;"> <asp:Label ID="lblSection3" runat="server" Text=""></asp:Label></td>
        </tr>
        <tr>
        <td style="  padding:5px;">
           <b><%= GetKeyResult("SECTION")%> 4</b> <%= GetKeyResult("BETWEENDAY")%> <b>
                <asp:Label ID="lblTimeFrameMax4" runat="server" Text=""></asp:Label></b> <%= GetKeyResult("AND")%>
            <%= GetKeyResult("DAY")%> <b>
                <asp:Label ID="lblTimeFrameMin4" runat="server" Text=""></asp:Label></b></td>
        </tr>

        <tr>
        <td style="  border: 1px solid #0377C8; padding:5px;"> <asp:Label ID="lblSection4" runat="server" Text=""></asp:Label></td>
        </tr>
             
        </table>


       
<%--                <div class="error" id="errorPopup" runat="server">
                </div>
                <div class="superadmin-mainbody-inner">
        <div class="superadmin-mainbody-sub4" style="display: none">
            <asp:Label ID="lblMessage" runat="server" Text="Policy not exist" ForeColor="#e48249"></asp:Label>
        </div>
    </div>
    <div class="superadmin-mainbody-inner" id="divPolicy" runat="server">
        <div class="superadmin-mainbody-sub4">
            Name : <b>
                <asp:Label ID="lblName" runat="server" Text=""></asp:Label></b>
        </div>
       
        <div class="superadmin-mainbody-sub4">
            <b>Section 1</b> Between day <b>
                <asp:Label ID="lblTimeFrameMax1" runat="server" Text=""></asp:Label></b> and
            day <b>
                <asp:Label ID="lblTimeFrameMin1" runat="server" Text=""></asp:Label></b>
        </div>
        <div class="cancelation-tabbody1">
            <div id="TabbedPanels2" class="TabbedPanels">
                <div class="TabbedPanelsContentGroup">
                    <div class="TabbedPanelsContent">
                        <div class="tab-textarea1">
                            <asp:Label ID="lblSection1" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="superadmin-mainbody-sub4">
            <b>Section 2</b> Between day <b>
                <asp:Label ID="lblTimeFrameMax2" runat="server" Text=""></asp:Label></b> and
            day <b>
                <asp:Label ID="lblTimeFrameMin2" runat="server" Text=""></asp:Label></b>
        </div>
        <div class="cancelation-tabbody1">
            <div id="TabbedPanels3" class="TabbedPanels">
                <div class="TabbedPanelsContentGroup">
                    <div class="TabbedPanelsContent">
                        <div class="tab-textarea1">
                            <asp:Label ID="lblSection2" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="superadmin-mainbody-sub4">
            <b>Section 3</b> Between day <b>
                <asp:Label ID="lblTimeFrameMax3" runat="server" Text=""></asp:Label></b> and
            day <b>
                <asp:Label ID="lblTimeFrameMin3" runat="server" Text=""></asp:Label></b>
        </div>
        <div class="cancelation-tabbody1">
            <div id="TabbedPanels4" class="TabbedPanels">
                <div class="TabbedPanelsContentGroup">
                    <div class="TabbedPanelsContent">
                        <div class="tab-textarea1">
                            <asp:Label ID="lblSection3" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="superadmin-mainbody-sub4">
            <b>Section 4</b> Between day <b>
                <asp:Label ID="lblTimeFrameMax4" runat="server" Text=""></asp:Label></b> and
            day <b>
                <asp:Label ID="lblTimeFrameMin4" runat="server" Text=""></asp:Label></b>
        </div>
        <div class="cancelation-tabbody1">
            <div id="Div1" class="TabbedPanels">
                <div class="TabbedPanelsContentGroup">
                    <div class="TabbedPanelsContent">
                        <div class="tab-textarea1">
                            <asp:Label ID="lblSection4" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <br />
           
            <br />
        </div>
    </div>
            </div>--%>
       
        <div class="save-cancel-btn1 button_section">
                        <asp:LinkButton ID="lnkCnacel" runat="server" CssClass="cancelpop" OnClientClick="return CloseTermsAndConditionsPopUp();"><%=GetKeyResult("CLOSE") %></asp:LinkButton>
                    </div>
      
    </div>
   

    <script language="javascript" type="text/javascript" >
        function open_win2(pageurl) {

            var url = pageurl;
            var winName = 'myWindow';
            var w = '700';
            var h = '500';
            var scroll = 'no';
            var popupWindow = null;
            LeftPosition = (screen.width) ? (screen.width - w) / 2 : 0;
            TopPosition = (screen.height) ? (screen.height - h) / 2 : 0;
            settings = 'height=' + h + ',width=' + w + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=yes,directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no';
            popupWindow = window.open(url, winName, settings)
            return false;

        }


        var currencySign = '<%= CurrencySign %>';
        //debugger;
        jQuery(document).ready(function () {
            jQuery(".currencyClass").each(function () {
                jQuery(this).html(currencySign);
            });
            jQuery("#<%= btnNext.ClientID %>").bind("click",function(){
            //debugger;
                if(document.getElementById('chkIAgree').checked)
                {
                    return true;
                }
                else
                {
                    alert('<%= GetKeyResultForJavaScript("AGREETERMSANDCONDITION") %>.');
                    return false;
                }
            });
        });



//        jQuery(document).ready(function () {
//            jQuery("#<%= tAndCPopUp.ClientID %>").bind("click", function () {
//                jQuery(".error").html("");
//                jQuery(".error").hide();
//                //jQuery("#Loding_overlay").show();
//                jQuery("body").scrollTop(0);
//                jQuery("html").scrollTop(0);
//                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
//                jQuery("#divpolicy").show();
//                jQuery("#divpolicy-overlay").show();
//                return false;
//            });

      //  });

        function CloseTermsAndConditionsPopUp() {
            document.getElementsByTagName('html')[0].style.overflow = 'auto';
            document.getElementById('divpolicy').style.display = 'none';
            document.getElementById('divpolicy-overlay').style.display = 'none';
            return false;
        }
    </script>
    
<!-- ClickTale Bottom part -->
<div id="ClickTaleDiv" style="display: none;"></div>
<script type="text/javascript">
    if (document.location.protocol != 'https:')
        document.write(unescape("%3Cscript%20src='http://s.clicktale.net/WRd.js'%20type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
    if (typeof ClickTale == 'function') ClickTale(24322, 0.0586, "www02");
</script>
<!-- ClickTale end of Bottom part -->
</asp:Content>

