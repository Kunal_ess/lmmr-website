﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Xml;

public partial class MainMaster : BaseMasterPage
{
    public Language l
    {
        get;
        set;
    }

    
    //private string _keywords;
    //private string _description;
    //private string _pagetitel;

    
    ///// <SUMMARY>
    ///// Gets or sets the Meta Keywords tag for the page
    ///// </SUMMARY>
    //public string Meta_Keywords
    //{
    //    get
    //    {
    //        return _keywords;
    //    }
    //    set
    //    {
    //        // strip out any excessive white-space, newlines and linefeeds
    //        _keywords = Regex.Replace(value, "\\s+", " ");
    //    }
    //}

    ///// <SUMMARY>
    ///// Gets or sets the Meta Description tag for the page
    ///// </SUMMARY>
    //public string Meta_Description
    //{
    //    get
    //    {
    //        return _description;
    //    }
    //    set
    //    {
    //        // strip out any excessive white-space, newlines and linefeeds
    //        _description = Regex.Replace(value, "\\s+", " ");
    //    }
    //}

    ///// <SUMMARY>
    ///// Gets or sets the Page Titel tag for the page
    ///// </SUMMARY>
    //public string Page_Titel
    //{
    //    get
    //    {
    //        return _pagetitel;
    //    }
    //    set
    //    {
    //        // strip out any excessive white-space, newlines and linefeeds
    //        _pagetitel = Regex.Replace(value, "\\s+", " ");
    //    }
    //}
    //public MainMaster()
    //{
    //    base.Init += new EventHandler(MainMaster_Init);
    //}

    //void MainMaster_Init(object sender, EventArgs e)
    //{
        
    //    XmlDocument seoXml = new XmlDocument();
    //    seoXml.Load(Server.MapPath("~/SEO.xml"));
    //    XmlNode xnod = null;
    //    l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
    //    string search = "";
    //    if (l != null)
    //    {
    //        search += "/links/link[@language='" + l.Name.ToLower() + "']";
    //    }
    //    else
    //    {
    //        search += "/links/link[@language='english']";
    //    }
    //    string url = Request.RawUrl.Split('/')[Request.RawUrl.Split('/').Length - 2];
    //    search += "[@linkName='" + url + "']";
    //    xnod = seoXml.SelectSingleNode(search);
    //    XmlNode xnodKey = null;
    //    if (xnod != null)
    //    {
    //        if (xnod.HasChildNodes)
    //        {
    //            xnodKey = xnod.FirstChild;
    //            if (xnodKey != null)
    //            {
    //                Meta_Keywords = xnodKey.InnerText == null ? "" : xnodKey.InnerText;
    //            }
    //            xnodKey = xnodKey.NextSibling;
    //            if (xnodKey != null)
    //            {
    //                Meta_Description = xnodKey.InnerText == null ? "" : xnodKey.InnerText;
    //            }
    //            xnodKey = xnodKey.NextSibling;
    //            if (xnodKey != null)
    //            {
    //                Page_Titel = xnodKey.InnerText == null ? "" : xnodKey.InnerText;
    //            }
    //        }
    //    }
    //    seoXml = null;
    //    //Meta_Description = "My Decsription";
    //    if (!String.IsNullOrEmpty(Meta_Description))
    //    {
    //        HtmlMeta tag = new HtmlMeta();
    //        tag.Name = "description";
    //        tag.Content = Meta_Description;
    //        Page.Header.Controls.AddAt(0, tag);
    //    }
    //    //Meta_Keywords = "My Keywords";
    //    if (!String.IsNullOrEmpty(Meta_Keywords))
    //    {
    //        HtmlMeta tag = new HtmlMeta();
    //        tag.Name = "keywords";
    //        tag.Content = Meta_Keywords;
    //        Page.Header.Controls.AddAt(0, tag);
    //    }

    //    if (!string.IsNullOrEmpty(Page_Titel))
    //    {
    //        Page.Title = Page_Titel;
    //    }
    //    else
    //    {
    //        Page.Title = "Last Minute Meeting Room";
    //    }
    //    TList<CmsDescription> lstDesc = new ManageCMSContent().GetCMSContent(4, Convert.ToInt64(Session["LanguageID"]));
    //    foreach (CmsDescription cd in lstDesc)
    //    {
    //        if (Request.RawUrl.ToLower().Contains(cd.PageUrl.ToLower()))
    //        {
    //            Meta_Description = cd.MetaDescription == null ? "" : cd.MetaDescription;
    //            if (!String.IsNullOrEmpty(Meta_Description))
    //            {
    //                HtmlMeta tag = new HtmlMeta();
    //                tag.Name = "description";
    //                tag.Content = Meta_Description;
    //                Page.Header.Controls.AddAt(0, tag);
    //            }
    //            Meta_Keywords = cd.MetaKeyword == null ? "" : cd.MetaKeyword;
    //            if (!String.IsNullOrEmpty(Meta_Keywords))
    //            {
    //                HtmlMeta tag = new HtmlMeta();
    //                tag.Name = "keywords";
    //                tag.Content = Meta_Keywords;
    //                Page.Header.Controls.AddAt(0, tag);
    //            }
    //            Page_Titel = cd.ContentShortDesc == null ? "" : cd.ContentShortDesc;
    //            if (!string.IsNullOrEmpty(Page_Titel))
    //            {
    //                Page.Title = Page_Titel;
    //            }
    //            else
    //            {
    //                Page.Title = "Last Minute Meeting Room";
    //            }
    //            break;
    //        }
    //    }
    //}
    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Write("<div style='color:red;'>Main Master Start load:"+ DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond + "</div>");
        if (Session["CurrentUser"] != null)
        {
            Users objUsers = (Users)Session["CurrentUser"];
            if (!Convert.ToBoolean(objUsers.IsfromWl))
            {
                if (objUsers.Usertype != (int)Usertype.Agency && objUsers.Usertype != (int)Usertype.agencyuser && objUsers.Usertype != (int)Usertype.Company && objUsers.Usertype != (int)Usertype.privateuser)
                {
                    Session.Remove("CurrentUser");
                    //Response.Redirect("Login.aspx");
                    Language l = new Language();
                    if (Session["Language"] != null)
                    {
                        l = Session["Language"] as Language;
                    }
                    else
                    {
                        l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                    }
                    if (l != null)
                    {
                        Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                    }
                    else
                    {
                        Response.Redirect(SiteRootPath + "login/english");
                    }
                }
            }
            else
            {
                Session.Remove("CurrentUser");
                if (Request.RawUrl.ToLower().Contains("search-requests") || Request.RawUrl.ToLower().Contains("find-by-map") || Request.RawUrl.ToLower().Contains("booking-step") || Request.RawUrl.ToLower().Contains("request-step") || Request.RawUrl.ToLower().Contains("requeststep") || Request.RawUrl.ToLower().Contains("bookingstep") || Request.RawUrl.ToLower().Contains("searchresult"))
                {
                    hdrltr.Text = "<script src=\"http://maps.google.com/maps/api/js?sensor=false\" type=\"text/javascript\"></script>";
                }
            }

        }
        if (Request.RawUrl.ToLower().Contains("search-requests") || Request.RawUrl.ToLower().Contains("find-by-map") || Request.RawUrl.ToLower().Contains("booking-step") || Request.RawUrl.ToLower().Contains("request-step") || Request.RawUrl.ToLower().Contains("requeststep") || Request.RawUrl.ToLower().Contains("bookingstep") || Request.RawUrl.ToLower().Contains("searchresult"))
        {
            hdrltr.Text = "<script src=\"http://maps.google.com/maps/api/js?sensor=false\" type=\"text/javascript\"></script>";
        }
        if (Request.RawUrl.ToLower().Contains("booking-step4") || Request.RawUrl.ToLower().Contains("bookingstep4.aspx"))
        {
            hdrltr.Text += "<script src=\"" + SiteRootPath + "js/creditcard.js\" type=\"text/javascript\"></script>";
        }
        //Response.Write("<div style='color:red;'>Main Master end load:" + DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond + "</div>");
    }
}
