﻿#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using log4net;
using log4net.Config;
using System.IO;
using System.Text;
using System.Linq;
using System.Xml;
using System.Web.Services;
using System.Xml.Serialization;
#endregion

public partial class HomeMaster : System.Web.UI.MasterPage
{
    public event EventHandler contentCallEvent;
    ManageWhiteLabel objwlMapping = new ManageWhiteLabel();
    public Language l
    {
        get;
        set;
    }
    public XmlDocument xmlLanguage
    {
        get
        {
            if (Session["xmlLanguage"] != null)
            {
                return (Session["xmlLanguage"] as XmlDocument);
            }
            else
            {
                return null;
            }
        }
        set
        {
            Session["xmlLanguage"] = value;
        }
    }
    //public string CDNPath
    //{
        //get
        //{
            //return ConfigurationManager.AppSettings["FilePathCDNout"];
        //}
    //}
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            if (string.IsNullOrEmpty(appRootUrl) || appRootUrl == "/")
            {
                return host + "/";
            }
            else
            {
                return host + appRootUrl + "/";
            }
        }
    }
    public string GetKeyResult(string key)
    {
        if (Session["LanguageChange"] != null)
        {
            xmlLanguage = null;
            Session["LanguageChange"] = null;
        }
        if (Session["Language"] != null)
        {
            Language currentL = Session["Language"] as Language;
            if (Request.RawUrl.ToLower().Contains(currentL.Name.ToLower()))
            {
                if (xmlLanguage != null)
                {
                    XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                    return System.Net.WebUtility.HtmlDecode(nodes.InnerText);
                }
                else
                {
                    xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
                    XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                    return System.Net.WebUtility.HtmlDecode(nodes.InnerText);
                }
            }
            else
            {
                xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
                XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                return System.Net.WebUtility.HtmlDecode(nodes.InnerText);
            }
        }
        else
        {
            xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
            XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
            return System.Net.WebUtility.HtmlDecode(nodes.InnerText);
        }
        //return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    }
    public string GetKeyResultForJavaScript(string key)
    {
        if (Session["LanguageChange"] != null)
        {
            xmlLanguage = null;
            Session["LanguageChange"] = null;
        }
        if (Session["Language"] != null)
        {
            Language currentL = Session["Language"] as Language;
            if (Request.RawUrl.ToLower().Contains(currentL.Name.ToLower()))
            {
                if (xmlLanguage != null)
                {
                    XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                    return System.Net.WebUtility.HtmlDecode(nodes.InnerText).Replace("'", "\\'").Replace("\"", "\\\"");
                }
                else
                {
                    xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
                    XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                    return System.Net.WebUtility.HtmlDecode(nodes.InnerText).Replace("'", "\\'").Replace("\"", "\\\"");
                }
            }
            else
            {
                xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
                XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                return System.Net.WebUtility.HtmlDecode(nodes.InnerText).Replace("'", "\\'").Replace("\"", "\\\"");
            }
        }
        else
        {
            xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
            XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
            return System.Net.WebUtility.HtmlDecode(nodes.InnerText).Replace("'", "\\'").Replace("\"", "\\\"");
        }
        //return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key)).Replace("'", "\\'").Replace("\"", "\\\"");
    }
    ManageMapSearch objManageSearch = new ManageMapSearch();
    ManageCMSContent objManageCMSContent = new ManageCMSContent();

    BookingRequest objBookingRequest = null;
    public int intTypeCMSid;
    public string HotelData;
    public XmlDocument XMLLanguage
    {
        get { return (XmlDocument)ViewState["Language"]; }
        set { ViewState["Language"] = value; }
    }
    public string TimeoutTime
    {
        get;
        set;
    }

    [WebMethod]
    public static void AbandonSession()
    {
        SearchTracer st = null;
        Createbooking objBooking = null;
        BookingManager bm = new BookingManager();
        if (objBooking == null)
        {
            if (HttpContext.Current.Session["SerachID"] == null)
            {
                objBooking = HttpContext.Current.Session["Search"] as Createbooking;
            }
            else
            {
                st = bm.GetSearchDetailsBySearchID(Convert.ToString(HttpContext.Current.Session["SerachID"]));
                objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
            }
        }
        bm.ResetIsReserverFalse(objBooking);
        HttpContext.Current.Session.Abandon();
    }

    #region PageLoad
    /// <summary>
    /// This method is call on the Page load and Initlize all the components of the page to its initial stage.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {

        l = Session["Language"] as Language;
        if (Request.QueryString["wl"] == null)
        {
            if (l != null)
            {
                logoHotel.HRef = SiteRootPath + "default/" + l.Name.ToLower();
            }
            else
            {
                logoHotel.HRef = SiteRootPath + "default/english";
            }
        }
        string s = Request.Url.AbsoluteUri.Split('/')[Request.Url.AbsoluteUri.Split('/').Length - 1];
        if (Request.RawUrl.ToLower().Contains("booking-step"))
        {
            //ltrHeader.Text = "<script language='javascript' >window.onbeforeunload = function(){PageMethods.AbandonSession();}</script>";
        }
        //HotelData = "[[]]";

        btnSearch.Attributes.Add("onclick", "return validate();");
        Session.Remove("Country");
        Session.Remove("City");
        //Session.Remove("masterInput");
        CalendarExtender1.StartDate = DateTime.Now.Date;
        //CalendarExtender1.EndDate = DateTime.Now.Date.AddMonths(2);
        if (Convert.ToString(Session["masterInput"]) == "")
        {
            CalendarExtender1.SelectedDate = DateTime.Now;
        }
        if (Session["CurrentRestUserID"] != null)
        {
            TimeoutTime = GetExectTimeOut(new HotelManager().GetSessionTimeoutValue());
        }
        else
        {
            TimeoutTime = "0";
        }
        if (!Page.IsPostBack)
        {
            fillStaticData();
            //if (Session["CurrentRestUserID"] != null)
            //{
            //    TimeoutTime = GetExectTimeOut(new HotelManager().GetSessionTimeoutValue());
            //}
            //Checking input parameteres from master page
            Dictionary<long, string> participants = Session["participants"] as Dictionary<long, string>;
            if (participants != null)
            {
                if (participants.Count > 1)
                {
                    var first = participants.FirstOrDefault();
                    participants.Clear();
                    participants.Add(first.Key, first.Value);
                }
            }
            Session["participants"] = participants;

            BindCountryDropdown();
            if (Convert.ToString(Session["masterInput"]) != "")
            {
                drpCountry.SelectedValue = ((Session["masterInput"] as BookingRequest)).propCountry;
                hdnCountry.Value = drpCity.SelectedValue;
            }
            BindCityDropdownByCountryID(Convert.ToInt32(drpCountry.SelectedValue));
            if (Convert.ToString(Session["masterInput"]) != "")
            {
                drpCity.SelectedValue = ((Session["masterInput"] as BookingRequest)).propCity;
                hdnCityDetail.Value = drpCity.SelectedValue;
                txtBookingDate.Text = ((Session["masterInput"] as BookingRequest)).propDate;
                drpDays.SelectedValue = ((Session["masterInput"] as BookingRequest)).propDays;
                drpDay2.SelectedValue = ((Session["masterInput"] as BookingRequest)).propDay2;
                drpDuration.SelectedValue = ((Session["masterInput"] as BookingRequest)).propDuration;
                if (drpDuration.SelectedValue == "2")
                {
                    divDay2.Style.Add("display", "block");
                }
                else
                {
                    divDay2.Style.Add("display", "none");
                }
                if (((Session["masterInput"] as BookingRequest)).propParticipants != "0")
                {
                    txtParticipant.Text = ((Session["masterInput"] as BookingRequest)).propParticipants;
                }
                else
                {
                    txtParticipant.Text = "";
                }
            }
        }
        if (Request.QueryString["wl"] == null)
        {
            //By Pranayesh For WL.
            drpCountry.Enabled = true;
            drpCity.Enabled = true;
            HtmlGenericControl topMenu = (HtmlGenericControl)tmFrontEnd.FindControl("divSubNav");
            topMenu.Visible = true;
            HtmlGenericControl topMenuleft = (HtmlGenericControl)tmFrontEnd.FindControl("divLanguage");
            topMenuleft.Visible = true;
            HtmlGenericControl phmedia = (HtmlGenericControl)Master.FindControl("divMainFooter");
            HtmlGenericControl phlink = (HtmlGenericControl)Master.FindControl("divMainBottomLink");
            HtmlGenericControl phpowered = (HtmlGenericControl)Master.FindControl("DivMainPowerd");
            phpowered.Visible = false;
            phmedia.Visible = true;
            phlink.Visible = true;
            divlogo.Attributes.Add("class", "logo");
            imgSiteLogo.Src = SiteRootPath + "images/logo-front.png";
            Divmapbutton.Style.Add("display", "block");
            ////////////////////////////////////////////////////////////
        }
        else
        {
            //By Pranayesh For WL.
            logoHotel.Attributes.Remove("HRef");
            drpCountry.Enabled = false;
            drpCity.Enabled = false;
            
            HtmlGenericControl topMenu = (HtmlGenericControl)tmFrontEnd.FindControl("divSubNav");
            topMenu.Visible = false;
            HtmlGenericControl topMenuleft = (HtmlGenericControl)tmFrontEnd.FindControl("divLanguage");
            topMenuleft.Visible = false;
            HtmlGenericControl phmedia = (HtmlGenericControl)Master.FindControl("divMainFooter");
            HtmlGenericControl phlink = (HtmlGenericControl)Master.FindControl("divMainBottomLink");
            HtmlGenericControl phpowered = (HtmlGenericControl)Master.FindControl("DivMainPowerd");
            phpowered.Visible = true;
            phmedia.Visible = false;
            phlink.Visible = false;
            divlogo.Attributes.Add("class", "logo-wl");
            Divmapbutton.Style.Add("display", "none");
            WhiteLabel objwl = objwlMapping.GetWhiteLabelByID(Convert.ToInt64(Request.QueryString["wl"]));
            if (objwl != null)
            {
                imgSiteLogo.Src = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Replace("~/", "") + "TargetSiteLogo/" + objwl.TargetSiteLogo; ;
            }
            ////////////////////////////////////////////////////////////

        }
    }
    #endregion

    #region function

    /// <summary>
    /// This function used for bind all country.
    /// </summary>
    void BindCountryDropdown()
    {
        if (objManageSearch.GetCountryData().Count == 1)
        {
            drpCountry.DataTextField = "CountryName";
            drpCountry.DataValueField = "Id";
            drpCountry.DataSource = objManageSearch.GetCountryData();
            drpCountry.DataBind();
            //drpCountry.Items.Insert(0, new ListItem("--Select country--", "0"));
            drpCountry.Items.FindByText("Belgium").Selected = true;
            hdnCountry.Value = drpCountry.SelectedValue;
        }
        else
        {
            drpCountry.DataTextField = "CountryName";
            drpCountry.DataValueField = "Id";
            drpCountry.DataSource = objManageSearch.GetCountryData();
            drpCountry.DataBind();
            drpCountry.Items.Insert(0, new ListItem(GetKeyResult("SELECTCOUNTRY2"), "0"));

        }
    }

    /// <summary>
    /// This function used for bind cities by countryID.
    /// </summary>
    /// <param name="intcountryID"></param>
    void BindCityDropdownByCountryID(int intcountryID)
    {
        if (intcountryID != 0)
        {
            drpCity.DataValueField = "Id";
            drpCity.DataTextField = "City";
            drpCity.DataSource = objManageSearch.GetCityData(Convert.ToInt32(drpCountry.SelectedItem.Value));
            drpCity.DataBind();
            drpCity.Items.Insert(0, new ListItem(GetKeyResult("SELECTCITY"), "0"));
        }
        else
        {
            drpCity.Items.Clear();
            drpCity.Items.Insert(0, new ListItem(GetKeyResult("SELECTCITY"), "0"));
        }
    }

    void ManageSession()
    {
        string WhereClause = string.Empty;
        string WhereClauseday2 = string.Empty;
        if (hdnCountry.Value != "0")
        {
            if (hdnCountry.Value != "")
            {
                if (WhereClause.Length > 0)
                {
                    WhereClause += " and ";
                    WhereClauseday2 += " and ";
                }
                WhereClause += HotelColumn.CountryId + "=" + hdnCountry.Value;
                WhereClauseday2 += HotelColumn.CountryId + "=" + hdnCountry.Value;
            }
        }

        if (hdnCityDetail.Value != "0")
        {
            if (hdnCountry.Value != "")
            {
                if (WhereClause.Length > 0)
                {
                    WhereClause += " and ";
                    WhereClauseday2 += " and ";
                }
                WhereClause += HotelColumn.CityId + "=" + hdnCountry.Value;
                WhereClauseday2 += HotelColumn.CityId + "=" + hdnCountry.Value;
            }
        }
        if (drpDuration.SelectedValue == "1")
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                if (drpDays.SelectedValue == "0")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (drpDays.SelectedValue == "1")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (drpDays.SelectedValue == "2")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
            }
        }
        if (drpDuration.SelectedValue == "2")
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                WhereClauseday2 += " and ";
                if (drpDays.SelectedValue == "0")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (drpDays.SelectedValue == "1")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (drpDays.SelectedValue == "2")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }


                if (drpDay2.SelectedValue == "0")
                {
                    WhereClauseday2 += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (drpDay2.SelectedValue == "1")
                {
                    WhereClauseday2 += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (drpDay2.SelectedValue == "2")
                {
                    WhereClauseday2 += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
            }
        }

        if (txtBookingDate.Text != "")
        {
            DateTime fromDate = new DateTime(Convert.ToInt32("20" + txtBookingDate.Text.Split('/')[2]), Convert.ToInt32(txtBookingDate.Text.Split('/')[1]), Convert.ToInt32(txtBookingDate.Text.Split('/')[0]));

            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                WhereClauseday2 += " and ";
            }
            WhereClause += AvailabilityColumn.AvailabilityDate + "='" + fromDate + "'" + " and " + " (DATEDIFF(day, GETDATE(),'" + fromDate + "')) >= " + AvailabilityColumn.LeadTimeForMeetingRoom + "";
            WhereClauseday2 += AvailabilityColumn.AvailabilityDate + "='" + fromDate.AddDays(1) + "'" + " and " + " (DATEDIFF(day, GETDATE(),'" + fromDate.AddDays(1) + "')) >= " + AvailabilityColumn.LeadTimeForMeetingRoom + "";
        }


        if (txtParticipant.Text != "" && txtParticipant.Text != "0")
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                WhereClauseday2 += " and ";
            }
            WhereClause += txtParticipant.Text + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + "";
            WhereClauseday2 += txtParticipant.Text + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + "";
        }

        //Send all search session value
        Session["Where"] = WhereClause;
        if (drpDuration.SelectedValue == "2")
        {
            Session["Where2"] = WhereClauseday2;
        }

        //Maintain session value for all controls 
        objBookingRequest = new BookingRequest();
        objBookingRequest.propCountry = (!string.IsNullOrEmpty(drpCountry.SelectedValue)) ? (drpCountry.SelectedValue == "0" ? hdnCountry.Value : drpCountry.SelectedValue) : hdnCountry.Value; 
        objBookingRequest.propCity = (!string.IsNullOrEmpty(drpCity.SelectedValue)) ? (drpCity.SelectedValue == "0" ? hdnCityDetail.Value : drpCity.SelectedValue) : hdnCityDetail.Value; 
        objBookingRequest.propDate = txtBookingDate.Text;
        objBookingRequest.propDuration = drpDuration.SelectedValue;
        objBookingRequest.propDays = drpDays.SelectedValue;
        objBookingRequest.propDay2 = drpDay2.SelectedValue;
        if (txtParticipant.Text != "")
        {
            objBookingRequest.propParticipants = txtParticipant.Text;
        }
        else
        {
            objBookingRequest.propParticipants = "0";
        }
        Session["masterInput"] = objBookingRequest;

        //Dictionary<long, string> participants = new Dictionary<long, string>();
        //participants.Add(1, objBookingRequest.propParticipants);
        //Session["participants"] = participants;
    }

    #endregion


    public void fillStaticData()
    {
        btnSearch.Text = GetKeyResult("FINDYOURMEETINGROOM");

        imgIcontext.ToolTip = GetKeyResult("YOUCANONLYBOOKONLINEMESSAGE");
        drpDays.Items.Add(new ListItem(GetKeyResult("FULLDAY"), "0"));
        drpDays.Items.Add(new ListItem(GetKeyResult("MORNING"), "1"));
        drpDays.Items.Add(new ListItem(GetKeyResult("AFTERNOON"), "2"));

        drpDay2.Items.Add(new ListItem(GetKeyResult("FULLDAY"), "0"));
        drpDay2.Items.Add(new ListItem(GetKeyResult("MORNING"), "1"));
        drpDay2.Items.Add(new ListItem(GetKeyResult("AFTERNOON"), "2"));

    }

    public string GetExectTimeOut(string timeout)
    {
        SearchTracer st = null;
        Createbooking objBooking = null;
        BookingManager bm = new BookingManager();
        if (objBooking == null)
        {
            if (Session["SerachID"] != null)
            {
                objBooking = Session["Search"] as Createbooking;
                st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
            }
            else
            {
                st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
                if (st != null)
                {
                    objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
                }
            }
        }
        if (objBooking != null && st != null)
        {
            DateTime ExpireTime = st.CreationDate.AddMinutes(Convert.ToDouble(timeout));
            TimeSpan t = DateTime.Now.Subtract(st.CreationDate);
            if (Convert.ToDouble(timeout) > t.Minutes)
            {
                if (t.Minutes == Convert.ToDouble(timeout) - 1 && t.Seconds > 0)
                {
                    return Convert.ToString(((Convert.ToDouble(timeout) - (t.Minutes + (t.Seconds * .01))) * 60000) + 30000);
                }
                else
                {
                    return Convert.ToString((Convert.ToDouble(timeout) - (t.Minutes + (t.Seconds * .01))) * 60000);
                }
            }
            else
            {
                return "0";
            }
        }
        else
        {
            return (Convert.ToDouble(timeout) * 60000).ToString();
        }
    }

    #region events

    ///// <summary>
    ///// this events is used for index changing of dropdown list.
    ///// </summary>
    ///// <param name="sender"></param>
    ///// <param name="e"></param>
    //protected void DDLlanguage_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    Session["LanguageID"] = DDLlanguage.SelectedValue;
    //    Response.Redirect(Request.RawUrl, false);

    //}

    /// <summary>
    /// This event Bind the city dropdown according to selected countryid.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void drpCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpCountry.SelectedValue != "")
        {
            //BindCityDropdownByCountryID(Convert.ToInt32(drpCountry.SelectedValue));
            Session["Country"] = drpCountry.SelectedValue;
            MiscWebService objMisc = new MiscWebService();
            drpCity.DataSource = objMisc.GetCityByCountryIDForFrontend(Convert.ToInt64(drpCountry.SelectedValue));
            drpCity.DataTextField = "Name";
            drpCity.DataValueField = "id";
            drpCity.DataBind();
            drpCity.Items.Insert(0, new ListItem(GetKeyResult("SELECTCITY"), "0"));
            //if (Request.Url.AbsolutePath.Contains("MapSearch.aspx") || Request.Url.AbsolutePath.Contains("SearchBooking.aspx"))
            //{
            //    ScriptManager.RegisterStartupScript(this, typeof(Page), "LoadMap2", "DefaultMap('" + drpCountry.SelectedItem.Text + "');", true);
            //}
        }

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (Request.RawUrl.ToLower().Contains("booking-step"))
        {
            SearchTracer st = null;
            Createbooking objBooking = null;
            BookingManager bm = new BookingManager();
            if (objBooking == null)
            {
                if (Session["SerachID"] != null)
                {
                    st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
                    objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
                    if (objBooking != null)
                    {
                        bm.ResetIsReserverFalse(objBooking);
                    }
                }
            }
            Session.Remove("SearchID");
            Session.Remove("Search");
        }
        else
        {
            Session.Remove("RequestID");
            Session.Remove("Request");
        }
        Session.Remove("MeetingRoomConfig");
        Session.Remove("MeetingRoom");
        Session.Remove("Hotel");
        Session.Remove("RequestedmeetingroomConfig");
        Session.Remove("Requestedmeetingroom");
        Session.Remove("Requestedhotel");
        Session["Country"] = (!string.IsNullOrEmpty(drpCountry.SelectedValue)) ? (drpCountry.SelectedValue == "0" ? hdnCountry.Value : drpCountry.SelectedValue) : hdnCountry.Value; 
        Session["City"] = (!string.IsNullOrEmpty(drpCity.SelectedValue)) ? (drpCity.SelectedValue == "0" ? hdnCityDetail.Value : drpCity.SelectedValue) : hdnCityDetail.Value;
        Session.Remove("masterInput");
        ManageSession();


        Session.Remove("BookingDone");
        Session["participants"] = new Dictionary<long, Dictionary<long, string>>();
        Session["PostBack"] = null;
        //Response.Redirect("SearchResult.aspx");
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (Request.QueryString["wl"] == null)
        {
            if (l != null)
            {
                Response.Redirect(SiteRootPath + "search-results/" + l.Name.ToLower());
            }
            else
            {
                Response.Redirect(SiteRootPath + "search-results/english");
            }
        }
        else
        {

            string[] date = (txtBookingDate.Text).Split('/');
            string query = "wl=" + Request.QueryString["wl"].ToString() + "&country=" + hdnCountry.Value + "&city=" + hdnCityDetail.Value + "&d=" + date[0] + "&m=" + date[1] + "&y=" + date[2] + "&delegate=" + txtParticipant.Text + "&duration=" + drpDuration.SelectedValue + "&day1=" + drpDays.SelectedValue + "&day2=" + drpDay2.SelectedValue + "";
            Response.Redirect("searchresult.aspx?" + query);

        }

    }

    #endregion

    protected void drpDuration_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpDuration.SelectedValue == "2")
        {
            divDay2.Visible = true;
        }
        else
        {
            divDay2.Visible = false;
        }
    }
    protected void drpCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(drpCity.SelectedValue) != 0 && Convert.ToInt32(drpCountry.SelectedValue) != 0)
        {
            //Session["City"] = drpCity.SelectedValue;
            //if (Request.Url.AbsolutePath.Contains("SearchBooking.aspx"))
            //{
            //    if (Session["FirstTime"] != null)
            //    {
            //        if (Session["FirstTime"].ToString() == "1")
            //            ManageSession();
            //    }
            //}

            //if (Request.Url.AbsolutePath.Contains("MapSearch.aspx") || Request.Url.AbsolutePath.Contains("SearchBooking.aspx"))
            //{
            //    GetCityHotel();
            //    GetCenterPoint(Convert.ToInt32(Session["City"]));

            //}
        }

    }

    void GetCenterPoint(int cityID)
    {

        //Function to get main center point of the city.
        string strLatitudeOfCenter = "";
        string strLogitudeOfCenter = "";
        string strCenterPlace = "";
        string strCenterPoint = "";
        int intZoomLevel = 10;
        TList<MainPoint> objCenterPoint = objManageSearch.GetCenterPoint(cityID);
        if (objCenterPoint.Count > 0)
        {
            MainPoint objPoint = objCenterPoint.Find(a => a.IsCenter == true);
            //strLatitudeOfCenter = objPoint.Latitude.ToString();
            //strLogitudeOfCenter = objPoint.Longitude.ToString();
            //hdnMapLatitude.Value = objPoint.Latitude.ToString();
            //hdnMapLogitude.Value = objPoint.Longitude.ToString();
            //strCenterPlace = objPoint.MainPointName;
            //intZoomLevel = 12;
        }
        strCenterPoint = strLatitudeOfCenter + "," + strLogitudeOfCenter;
        ScriptManager.RegisterStartupScript(this, typeof(Page), "LoadMap3", "OnDemand('" + strCenterPoint + "','" + strCenterPlace + "','" + intZoomLevel + "');", true);
    }

    void GetCityHotel()
    {
        string WhereClause = string.Empty;
        if (hdnCountry.Value != "0")
        {
            if (hdnCountry.Value != "")
            {
                if (WhereClause.Length > 0)
                {
                    WhereClause += " and ";
                }
                WhereClause += HotelColumn.CountryId + "=" + hdnCountry.Value;
            }
        }
        else
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
            }
            WhereClause += HotelColumn.CountryId + "=" + Session["Country"];

        }

        if (hdnCityDetail.Value != "0")
        {
            if (hdnCityDetail.Value != "")
            {
                if (WhereClause.Length > 0)
                {
                    WhereClause += " and ";
                }
                WhereClause += HotelColumn.CityId + "=" + hdnCityDetail.Value;
            }
        }
        else
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
            }
            WhereClause += HotelColumn.CityId + "=" + Session["City"];

        }

        //string whereclaus = Convert.ToString(Session["Where"]);
        string orderby = string.Empty;//"IsPriority DESC," + HotelColumn.BookingAlgo + " DESC";
        StringBuilder objSB = new StringBuilder();
        objBookingRequest = new BookingRequest();
        //TList<Hotel> objHotel = objManageSearch.GetHotelByCity(Convert.ToInt32(drpCity.SelectedValue),Convert.ToInt32(drpCountry.SelectedValue));
        VList<BookingRequestViewList> objHotel = objBookingRequest.GetBookingReqDetails(WhereClause, orderby).FindAllDistinct(BookingRequestViewListColumn.HotelId);
        HotelData = "[";
        if (objHotel.Count > 0)
        {
            foreach (BookingRequestViewList h in objHotel)
            {
                string IsPromoted = "";
                string strSpecialDeal = "";
                string strActualprice = "";
                if (h.DdrPercent < 0)
                {
                    IsPromoted = "YES";
                    strActualprice = String.Format("{0:#,###}", objBookingRequest.GetActualpkgPricebyHotelid(Convert.ToInt32(h.HotelId))[0].ActualFullDayPrice);
                    strSpecialDeal = String.Format("{0:#,###}", (Math.Abs((Convert.ToDecimal(strActualprice) * Convert.ToDecimal(h.DdrPercent) / 100))));
                }
                else
                {
                    IsPromoted = "NO";
                }

                if (HotelData == "[")
                {

                    HotelData += "['" + h.HotelName + "', " + h.Latitude + "," + h.Longitude + "," + 1 + ",'" + IsPromoted + "','" + h.HotelAddress + "','" + h.Stars + "','" + strActualprice + "','" + strSpecialDeal + "']";
                }
                else
                {
                    HotelData = HotelData + ",[" + "'" + h.HotelName + "', " + h.Latitude + "," + h.Longitude + "," + 1 + ",'" + IsPromoted + "','" + h.HotelAddress + "','" + h.Stars + "','" + strActualprice + "','" + strSpecialDeal + "']";
                }
            }
            HotelData += "]";
        }
        if (HotelData == "[")
        {
            HotelData = "[[]]";
        }

    }
    protected void imgMapSearch_Click(object sender, ImageClickEventArgs e)
    {
        Session["hdnMapLatitude"] = null;
        Session["hdnMapLongitude"] = null;
        Session["Country"] = (!string.IsNullOrEmpty(drpCountry.SelectedValue)) ? (drpCountry.SelectedValue == "0" ? hdnCountry.Value : drpCountry.SelectedValue) : hdnCountry.Value;
        Session["City"] = (!string.IsNullOrEmpty(drpCity.SelectedValue)) ? (drpCity.SelectedValue == "0" ? hdnCityDetail.Value : drpCity.SelectedValue) : hdnCityDetail.Value;
        Session["IsMapSearch"] = "YES";
        ManageSession();
        Session["PostBack"] = null;
        //Response.Redirect("FindByMap.aspx");
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (Request.QueryString["wl"] == null)
        {
            if (l != null)
            {
                Response.Redirect(SiteRootPath + "find-by-map/" + l.Name.ToLower());
            }
            else
            {
                Response.Redirect(SiteRootPath + "find-by-map/english");
            }
        }
    }
    protected void btnLoginAgain_OnClick(object sender, EventArgs e)
    {

        SearchTracer st = null;
        Createbooking objBooking = null;
        BookingManager bm = new BookingManager();
        if (objBooking == null)
        {
            if (Session["SerachID"] == null)
            {
                objBooking = Session["Search"] as Createbooking;
            }
            else
            {
                st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
                objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
            }
        }
        bm.ResetIsReserverFalse(objBooking);
        Session.Remove("Search");
        Session.Remove("SearchID");
        Session.Remove("Request");
        Session.Remove("RequestID");
        Session.Remove("Request");
        //Response.Redirect("Default.aspx");
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (Request.QueryString["wl"] == null)
        {
            if (l != null)
            {
                Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
            }
            else
            {
                Response.Redirect(SiteRootPath + "default/english");
            }
        }
        else
        {
            Response.Redirect("~/WL/Expired.aspx");
        }
    }
    protected void btnResume_OnClick(object sender, EventArgs e)
    {
        BookingManager bm = new BookingManager();
        SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
        st.CreationDate = DateTime.Now;
        bm.SaveSearch(st);
        if (contentCallEvent != null)
            contentCallEvent(this, EventArgs.Empty);
        Response.Redirect(Request.RawUrl);
    }

    protected void btnNo_OnClick(object sender, EventArgs e)
    {
        SearchTracer st = null;
        Createbooking objBooking = null;
        BookingManager bm = new BookingManager();
        if (objBooking == null)
        {
            if (Session["SerachID"] == null)
            {
                objBooking = Session["Search"] as Createbooking;
            }
            else
            {
                st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
                objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
            }
        }
        bm.ResetIsReserverFalse(objBooking);
        Session.Remove("Search");
        Session.Remove("SearchID");
        Session.Remove("Request");
        Session.Remove("RequestID");
        Session.Remove("Request");
        //Response.Redirect("Default.aspx");
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (Request.QueryString["wl"] == null)
        {
            if (l != null)
            {
                Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
            }
            else
            {
                Response.Redirect(SiteRootPath + "default/english");
            }
        }
        else
        {
            Response.Redirect("~/WL/Expired.aspx");
        }
    }
    /// <summary>
    /// This method is passing the key value
    /// </summary>
    /// <param name="Key"></param>
    /// <returns></returns>
    //public string GetKeyResult(string key)
    //{
    //    if (XMLLanguage == null)
    //    {
    //        XMLLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
    //    }
    //    XmlNode nodes = XMLLanguage.SelectSingleNode("items/item[@key='" + Key + "']");
    //    return nodes.InnerText;// System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    //}

}
