﻿#region NameSpaces

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using log4net;
using log4net.Config;
using System.IO;
using System.Configuration;

#endregion

public partial class Registration : BasePage
{
    #region variable declaration
    NewUser newUserObject = new NewUser();
    string status;
    public string userType;
    EmailConfigManager em = new EmailConfigManager();
    string CurrentRestUserID;
    string CurrentUserID;
    #endregion

    #region Page Load

    /// <summary>
    /// Page Load initializations.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["task"] != null)
        {
            if (Session["registerType"] != null)
            {
                if (Convert.ToInt32(Session["registerType"]) == 7)
                {
                    passwordDiv.Visible = false;
                }
            }
            if(Session["CurrentRestUserID"] !=null)
            {
                CurrentRestUserID = Session["CurrentRestUserID"].ToString();
            }
            else if (Session["CurrentUserID"] != null)
            {
                CurrentUserID = Session["CurrentUserID"].ToString();
            }

            
            if (Session["task"].ToString() == "Edit")
            {
                if (Session["superadmin"] == null)
                {
                    lblHeading.Text = GetKeyResult("MANAGEYOURPROFILE");
                }
                else
                {
                    lblHeading.Text ="Manage Profile";
                }
                if (Session["LanguageID"] == null)
                {
                    Session["LanguageID"] = 1;
                }
                ViewState["_Pass"] = password.Text;
                ViewState["_RePass"] = retype.Text;
                if (!IsPostBack)
                {
                    divmessage.Style.Add("display", "none");
                    divmessage.Attributes.Add("class", "error");
                    fillStaticContent();
                    passwordDiv.Visible = false;
                    long id;
                    //if (Convert.ToInt64(Session["CurrentUserID"]) != null)
                    //{
                    
                    //}
                    //else
                    //{
                    //    id = 0;
                    //    Response.Redirect("Login.aspx");
                    //}
                    NewUser newUser = new NewUser();
                    UserDetails details = new UserDetails();
                    Users user = new Users();

                    if (Session["superadmin"] == null)
                    {

                        id = Convert.ToInt64(CurrentRestUserID);// In the event of session expiring
                    }
                    else
                    {
                        id = Convert.ToInt64(CurrentUserID); // In the event of session expiring
                    }
                    user = newUser.GetUserByID(id);
                    if (user != null)
                    {
                        firstName.Text = user.FirstName == null ? "" : user.FirstName;
                        lastName.Text = user.LastName == null ? "" : user.LastName;
                        email.Text = user.EmailId == null ? "" : user.EmailId;
                        ViewState["pwd"] = user.Password == null ? "" : user.Password;
                        ViewState["isActive"] = user.IsActive;
                        Login.Text = user.EmailId == null ? "" : user.EmailId;
                        if (user.CreatedDate != null)
                        {
                            ViewState["createddate"] = user.CreatedDate;
                        }
                        else
                        {
                            ViewState["createddate"] = DateTime.Now;
                        }

                    }
                    email.ReadOnly = true; // Set the Email UnEditable.
                    reEnterEmailDiv.Visible = false;// Hiding the re enter email label and textbox.
                    details = newUser.getDetailsByID(id);
                    BindCountry();
                    if (details != null)
                    {
                        countryList.SelectedValue = details.CountryId == null ? "0" : details.CountryId.ToString();//Watch it !!!!
                        if (details.CountryId != null)
                        {
                            BindGeneralIfoCity((int)details.CountryId);
                        }

                        //cityList.SelectedValue = details.CityId.ToString();//Watch it !!!!
                        txtCity.Text = details.CityName == null ? "" : details.CityName;
                        address.Text = details.Address == null ? "" : details.Address;
                        countryCodeList1.SelectedValue = details.CountryCode == null ? "" : details.CountryCode;// Watch it !!!!
                        phone.Text = details.Phone == null ? "" : details.Phone;
                        postalCode.Text = details.PostalCode == null ? "" : details.PostalCode;
                        if (details.LanguageId != null)
                        {
                            languageList.SelectedValue = details.LanguageId == null ? "0" : details.LanguageId.ToString();
                        }
                        if (countryList.SelectedValue != "")
                        {
                            TList<HearUsQuestion> Obj = newUserObject.GetQuestionByCountryID(Convert.ToInt32(countryList.SelectedValue));
                            if (Obj.Count > 0)
                            {
                                modeList.Items.Clear();
                                modeList.DataSource = Obj;
                                modeList.DataTextField = "Question";
                                modeList.DataValueField = "Id";
                                modeList.DataBind();
                            }
                            else
                            {
                                modeList.Items.Clear();
                                modeList.Items.Add(new ListItem("--Select--", "0"));
                            }
                        }
                        modeList.SelectedValue = details.HearFromUs == null ? "0" : details.HearFromUs.ToString();
                    }
                    preferredLanguages();
                    registerBtn.Text = "Update";
                    if (Convert.ToInt32(Session["registerType"]) == 10)
                    {
                        //user specific rendering.
                        agencySpecificSpan.Visible = false;
                        agency_companySpan.Visible = false;
                        companySpecificSpan.Visible = false;
                    }
                    if ((Convert.ToInt32(Session["registerType"]) == 7 || (Convert.ToInt32(Session["registerType"]) == 9)))
                    {
                        if (details != null)
                        {
                            //Agency and Company specific rendering.
                            if ((Convert.ToInt32(Session["registerType"]) == 7))
                            {
                                companyName.Text = details.CompanyName;
                                companySpecificSpan.Visible = false;
                            }
                            else
                            {
                                company.Text = details.CompanyName;
                                agencySpecificSpan.Visible = false;
                            }
                        }
                        FinancialInfo financeInfo = new FinancialInfo();
                        financeInfo = newUser.getFinanceInfoByUserID(id);
                        if (financeInfo != null)
                        {
                            vatNo.Text = financeInfo.VatNo == null ? "" : financeInfo.VatNo;
                            txtName.Text = financeInfo.Name == null ? "" : financeInfo.Name;
                            dept.Text = financeInfo.Department == null ? "" : financeInfo.Department;
                            txtEmail.Text = financeInfo.CommunicationEmail == null ? "" : financeInfo.CommunicationEmail;
                            selectCountryList.SelectedValue = financeInfo.CountryId == null ? "0" : financeInfo.CountryId.ToString();// Watch it !!!!
                            BindFinancialInfoCity((int)financeInfo.CountryId);
                            //cityDropList.SelectedValue = financeInfo.CityId.ToString();// Watch it !!!!
                            txtFinanceCity.Text = financeInfo.CityName == null ? "" : financeInfo.CityName;//txtCity.Text;
                            txtAddress.Text = financeInfo.Address == null ? "" : financeInfo.Address;
                            countryCodeList2.SelectedValue = financeInfo.CountryCode == null ? "0" : financeInfo.CountryCode;
                            txtPhone.Text = financeInfo.Phone == null ? "" : financeInfo.Phone;
                            txtPostalCode.Text = financeInfo.PostalCode == null ? "" : financeInfo.PostalCode;
                        }
                    }
                }
            }

            else
            {
                if (Session["registerType"] != null)
                {
                    if (Session["LanguageID"] == null)
                    {
                        Session["LanguageID"] = 1;
                    }
                    int userCode = Convert.ToInt32(Session["registerType"]);
                    if (userCode == 10) userType = "User";
                    else if (userCode == 7) userType = " Meet 2 Solution";
                    else if (userCode == 9) userType = "Company";
                    lblHeading.Text = GetKeyResult("REGISTERAS") + " " + userType;
                    ViewState["_Pass"] = password.Text;
                    ViewState["_RePass"] = retype.Text;
                    if (!IsPostBack)
                    {
                        loginDiv.Style.Add("display", "none");
                        divmessage.Style.Add("display", "none");
                        divmessage.Attributes.Add("class", "error");
                        fillStaticContent();
                        if (userCode == 10 || userCode == 9)
                        {
                            if (userCode == 9) { lblPrivateUser.Visible = true; lblCompanyMessage.Visible = true; }
                            agencySpecificSpan.Visible = false;
                        }
                        if (userCode == 10)
                        {
                            lblPrivateUser.Visible = true;
                            lblPrivateUserLine2.Visible = true;
                            agency_companySpan.Visible = false;
                            companySpecificSpan.Visible = false;
                        }
                        if (userCode == 7)
                        {
                            //agencySpan.Visible = false;
                            //agencySpecificSpan2.Visible = false;
                            companySpecificSpan.Visible = false;
                        }
                        BindCountry();
                        BindGeneralIfoCity(0);
                        BindFinancialInfoCity(0);
                        preferredLanguages();
                    }
                    if (email.Text != "")
                    {
                        loginDiv.Style.Add("display", "block");
                        Login.Text = email.Text;
                    }
                    else
                    {
                        loginDiv.Style.Add("display", "none");
                    }
                }
                else
                {
                    //Response.Redirect("Login");
                    //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                    if (l != null)
                    {
                        Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                    }
                    else
                    {
                        Response.Redirect(SiteRootPath + "login/english");
                    }
                }
            }
        }
        //else if ()
        else
        {
            //Response.Redirect("Login.aspx");
            //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
            if (l != null)
            {
                Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
            }
            else
            {
                Response.Redirect(SiteRootPath + "login/english");
            }
        }
    }

    #endregion

    #region Page_PreRender

    protected void Page_PreRender(object sender, System.EventArgs e)
    {
        password.Attributes.Add("Value", ViewState["_Pass"].ToString());
        retype.Attributes.Add("Value", ViewState["_RePass"].ToString());
    }

    #endregion

    #region Events

    /// <summary>
    /// This event is fired at the register button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void registerBtn_Click(object sender, EventArgs e)
    {
        if (registerBtn.Text == "Update")
        {
            {
                Users newUser = new Users();
                UserDetails details = new UserDetails();
                FinancialInfo financeInfo = new FinancialInfo();

                long CurrentUserID = Convert.ToInt32(Session["CurrentUserID"]);
                //Reading info that is general in all cases whether user or agency or company---------
                
                newUser.FirstName = firstName.Text;
                newUser.LastName = lastName.Text;
                newUser.EmailId = email.Text;
                newUser.Usertype = 10;
                newUser.UserId = CurrentUserID;
                newUser.IsActive = (bool)ViewState["isActive"];
                newUser.Password = (string)ViewState["pwd"];
                newUser.CreatedDate = Convert.ToDateTime(ViewState["createddate"]);

                details.CountryId = Convert.ToInt64(countryList.SelectedItem.Value);
                details.CityName = txtCity.Text.Trim();
                details.Phone = phone.Text;
                details.CountryCode = countryCodeList1.SelectedItem.Value;
                details.Address = address.Text;
                details.PostalCode = postalCode.Text;
                details.UserId = CurrentUserID;
                string emailtxt = email.Text.ToString();

                if (languageList.SelectedValue != null)
                {
                    details.LanguageId = Convert.ToInt32(languageList.SelectedValue);
                }


                details.HearFromUs = modeList.SelectedItem.Value;
                details.VisitCount = 1;

                switch (Convert.ToInt32(Session["registerType"]))
                {
                    case 10: status = newUserObject.update(newUser, details);
                        if (status == "Information updated successfully.")
                        {
                            Session["status"] = status;
                            Session["CurrentRestUser"] = newUserObject.GetUserByID(newUser.UserId);
                            //Response.Redirect("Default.aspx");
                            //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                            if (Session["superadmin"] == null)
                            {
                                if (l != null)
                                {
                                    Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
                                }
                                else
                                {
                                    Response.Redirect(SiteRootPath + "default/english");
                                }
                            }
                            else
                            {
                                Session["Update"] = status;
                                Response.Redirect("~/Operator/usermenu.aspx");

                            }
                        }

                        break;
                    case 7: financeInfo.VatNo = vatNo.Text;
                        financeInfo.Name = txtName.Text;
                        financeInfo.Department = dept.Text;
                        financeInfo.CommunicationEmail = txtEmail.Text;
                        financeInfo.CountryId = Convert.ToInt32(selectCountryList.SelectedItem.Value);
                        financeInfo.Address = txtAddress.Text;
                        financeInfo.PostalCode = txtPostalCode.Text;
                        //financeInfo.CityId = 0; //Convert.ToInt32(cityDropList.SelectedItem.Value);
                        financeInfo.CityName = txtFinanceCity.Text;
                        financeInfo.Phone = txtPhone.Text;
                        financeInfo.CountryCode = countryCodeList2.SelectedValue;
                        financeInfo.VatNo = vatNo.Text;
                        newUser.Usertype = 7;
                        details.IataNo = null;
                        details.CompanyName = companyName.Text;
                        status = newUserObject.update(newUser, details);
                        financeInfo.UserId = newUser.UserId;
                        if (status == "Information updated successfully.")
                        {
                            status = newUserObject.update(financeInfo, newUser.UserId);
                            if (status == "Information updated successfully.")
                            {
                                Session["status"] = status;
                                Session["CurrentRestUser"] = newUserObject.GetUserByID(newUser.UserId);
                                //Response.Redirect("Default.aspx");
                                //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                                if (Session["superadmin"] == null)
                                {
                                    if (l != null)
                                    {
                                        Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
                                    }
                                    else
                                    {
                                        Response.Redirect(SiteRootPath + "default/english");
                                    }
                                }
                                else
                                {
                                    Session["Update"] = status;
                                    Response.Redirect("~/Operator/usermenu.aspx");
                                }
                            }
                        }
                        break;
                    case 9: financeInfo.VatNo = vatNo.Text;
                        financeInfo.Name = txtName.Text;
                        financeInfo.Department = dept.Text;
                        financeInfo.CommunicationEmail = txtEmail.Text;
                        financeInfo.CountryId = Convert.ToInt32(selectCountryList.SelectedItem.Value);
                        financeInfo.Address = txtAddress.Text;
                        financeInfo.PostalCode = txtPostalCode.Text;
                        //financeInfo.CityId = 0;//Convert.ToInt32(cityDropList.SelectedItem.Value);
                        financeInfo.CityName = txtFinanceCity.Text;
                        financeInfo.Phone = txtPhone.Text;
                        financeInfo.CountryCode = countryCodeList2.SelectedValue;
                        financeInfo.VatNo = vatNo.Text;
                        details.CompanyName = company.Text;
                        newUser.Usertype = 9;
                        status = newUserObject.update(newUser, details);
                        financeInfo.UserId = newUser.UserId;
                        if (status == "Information updated successfully.")
                        {
                            status = newUserObject.update(financeInfo, newUser.UserId);
                            if (status == "Information updated successfully.")
                            {
                                Session["status"] = status;
                                Session["CurrentRestUser"] = newUserObject.GetUserByID(newUser.UserId);
                                //Response.Redirect("Default.aspx");
                                //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                                if (Session["superadmin"] == null)
                                {
                                    if (l != null)
                                    {
                                        Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
                                    }
                                    else
                                    {
                                        Response.Redirect(SiteRootPath + "default/english");
                                    }
                                }
                                else
                                {
                                    Session["Update"] = status;
                                    Response.Redirect("~/Operator/usermenu.aspx");
                                }

                            }

                        }
                        break;
                }
                if (status == "Information could not be saved." || status == "Information could not be saved.Please contact Administrator.")
                {
                    Session["status"] = status;
                    //Response.Redirect("Default.aspx");
                    //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                    if (Session["superadmin"] == null)
                    {
                        if (l != null)
                        {
                            Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
                        }
                        else
                        {
                            Response.Redirect(SiteRootPath + "default/english");
                        }
                    }
                    else
                    {
                        Session["Update"] = status;
                        Response.Redirect("~/Operator/usermenu.aspx");
                    }

                }
            }
        }
        else
        {
            if (Session["registerType"] != null)
            {

                Users newUser = new Users();
                UserDetails details = new UserDetails();
                FinancialInfo financeInfo = new FinancialInfo();

                //Reading info that is general in all cases whether user or agency or company---------

                newUser.FirstName = firstName.Text;
                newUser.LastName = lastName.Text;
                newUser.EmailId = email.Text.Trim().Replace("'", "");
                switch (Convert.ToInt32(Session["registerType"]))
                {
                    case 7:
                        newUser.Password = null;
                        break;
                    default:
                        newUser.Password = PasswordManager.Encrypt(password.Text, true);
                        break;
                }
                
                newUser.Usertype = 10;
                newUser.CreatedDate = DateTime.Now;
                details.CountryId = Convert.ToInt64(countryList.SelectedItem.Value);
                details.CityName = txtCity.Text.Trim();
                //details.CityId = 0; //Convert.ToInt64(cityList.SelectedItem.Value);
                details.Phone = phone.Text;
                details.CountryCode = countryCodeList1.SelectedItem.Value;
                details.Address = address.Text;
                details.PostalCode = postalCode.Text;
                if (languageList.SelectedValue != null)
                {
                    details.LanguageId = Convert.ToInt32(languageList.SelectedItem.Value);
                }

                details.HearFromUs = modeList.SelectedItem.Value;
                details.VisitCount = 0;
                //---------------------//

                switch (Convert.ToInt32(Session["registerType"]))
                {
                    case 10: status = newUserObject.register(newUser, details);
                        break;
                    case 7:
                        SendMails mail1 = new SendMails();
                        mail1.ToEmail = email.Text;
                        if (!mail1.IsDomainExist())
                        {
                            financeInfo.VatNo = vatNo.Text;
                            financeInfo.Name = txtName.Text;
                            financeInfo.Department = dept.Text;
                            financeInfo.CommunicationEmail = txtEmail.Text;
                            financeInfo.CountryId = Convert.ToInt32(selectCountryList.SelectedItem.Value);
                            financeInfo.Address = txtAddress.Text;
                            financeInfo.PostalCode = txtPostalCode.Text;
                            financeInfo.CityName = txtFinanceCity.Text;
                            //financeInfo.CityId = 0; //Convert.ToInt32(cityDropList.SelectedValue);
                            financeInfo.Phone = txtPhone.Text;
                            financeInfo.CountryCode = countryCodeList2.SelectedItem.Value;
                            newUser.Usertype = 7;
                            details.IataNo = null;
                            details.CompanyName = companyName.Text;
                            status = newUserObject.register(newUser, details);
                            financeInfo.UserId = newUser.UserId;
                            if (status == "Please activate you account by clicking on the link from the email message.")
                            {
                                status = newUserObject.register(financeInfo);
                            }
                        }

                        else
                        {
                            status = "Please enter valid company email id";
                        }

                        
                        break;
                    case 9:
                        SendMails mail = new SendMails();
                        mail.ToEmail = email.Text;
                        if (!mail.IsDomainExist())
                        {
                            financeInfo.VatNo = vatNo.Text;
                            financeInfo.Name = txtName.Text;
                            financeInfo.Department = dept.Text;
                            financeInfo.CommunicationEmail = txtEmail.Text;
                            financeInfo.CountryId = Convert.ToInt32(selectCountryList.SelectedItem.Value);
                            financeInfo.Address = txtAddress.Text;
                            financeInfo.PostalCode = txtPostalCode.Text;
                            financeInfo.CityName = txtFinanceCity.Text;
                            //financeInfo.CityId = 0;//Convert.ToInt32(cityDropList.SelectedValue);
                            financeInfo.Phone = txtPhone.Text;
                            financeInfo.CountryCode = countryCodeList2.SelectedItem.Value;
                            details.CompanyName = company.Text;
                            newUser.Usertype = 9;
                            status = newUserObject.register(newUser, details);
                            financeInfo.UserId = newUser.UserId;
                            if (status == "Please activate you account by clicking on the link from the email message.")
                            {

                                status = newUserObject.register(financeInfo);
                            }
                        }
                        else
                        {
                            status = "Please enter valid company email id";
                        }

                        break;
                }

                if (status == "Please activate you account by clicking on the link from the email message.")
                {
                    EmailConfig eConfig = em.GetByName("Registration mail");
                    if (eConfig.IsActive)
                    {
                        SendMails mail = new SendMails();
                        mail.FromEmail = ConfigurationManager.AppSettings["ClientSupportEmailID"];
                        mail.ToEmail = email.Text.Trim();
                        mail.Bcc = ConfigurationManager.AppSettings["WatchEMailID"];
                        //string bodymsg = File.ReadAllText(Server.MapPath("~/EmailTemplets/registrationEmailTemplate.htm"));
                        string bodymsg = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
                        EmailConfigMapping emap = new EmailConfigMapping();
                        if (details.LanguageId == 0)
                        {
                            emap = null;
                        }
                        else
                        {
                            emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == details.LanguageId).FirstOrDefault();
                        }
                        if (emap != null)
                        {
                            bodymsg = bodymsg.Replace("@CONTENTS", emap.EmailContents);
                        }
                        else
                        {
                            emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                            bodymsg = bodymsg.Replace("@CONTENTS", emap.EmailContents);
                        }
                        EmailValueCollection objEmailValues = new EmailValueCollection();
                        mail.Subject = "LMMR User Registration";
                        string strActivationLink = "activation.aspx?activate=" + newUser.UserId;
                        foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForUserRegistration(newUser.FirstName + " " + newUser.LastName, email.Text, password.Text, ConfigurationManager.AppSettings["Sender"], strActivationLink))
                        {
                            bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                        }
                        mail.Body = bodymsg;
                        if (Session["superadmin"] == null)
                        {
                            switch (Convert.ToInt32(Session["registerType"]))
                            {

                                case (int)Usertype.privateuser: mail.SendMail();
                                    registerBtn.OnClientClick = "javascript: _gaq.push(['_trackPageview', 'RegisterOK-IndivUser.aspx']);";
                                    if (l != null)
                                    {
                                        Response.Redirect(SiteRootPath + "register-indvuser/" + l.Name.ToLower());
                                    }
                                    else
                                    {
                                        Response.Redirect(SiteRootPath + "register-indvuser/english");
                                    }

                                    break;
                                case (int)Usertype.Company: mail.SendMailToCompany();
                                    registerBtn.OnClientClick = "javascript: _gaq.push(['_trackPageview', 'RegisterOK-CorpUser.aspx']);";
                                    //divmessage.InnerHtml = mail.FromEmail + " |||| " + mail.ToEmail + " |||| " + mail.Bcc + " <br/>" + mail.Body;
                                    //return;
                                    if (l != null)
                                    {
                                        Response.Redirect(SiteRootPath + "register-corpuser/" + l.Name.ToLower());
                                    }
                                    else
                                    {
                                        Response.Redirect(SiteRootPath + "register-corpuser/english");
                                    }

                                    break;
                                case (int)Usertype.Agency: mail.SendMail();
                                    registerBtn.OnClientClick = "javascript: _gaq.push(['_trackPageview', 'RegisterOK-Agency.aspx']);";
                                    if (l != null)
                                    {
                                        Response.Redirect(SiteRootPath + "register-agencyuser/" + l.Name.ToLower());
                                    }
                                    else
                                    {
                                        Response.Redirect(SiteRootPath + "register-agencyuser/english");
                                    }
                                    break;


                            }


                        }
                        else
                        {
                            switch (Convert.ToInt32(Session["registerType"]))
                            {

                                case (int)Usertype.privateuser: mail.SendMail();
                                    Session["Update"] = status;
                                    Response.Redirect("~/Operator/usermenu.aspx");

                                    break;
                                case (int)Usertype.Company: mail.SendMailToCompany();
                                    Session["Update"] = status;
                                    Response.Redirect("~/Operator/usermenu.aspx");
                                    break;
                                default:
                                     Session["Update"] = status;
                                    Response.Redirect("~/Operator/usermenu.aspx");
                                    break;

                                   

                            }
                        }

                    }
                    divmessage.Style.Add("display", "block");
                    divmessage.Attributes.Add("class", "succesfuly");
                    divmessage.InnerHtml = status;
                    btnRegisterCancel.Visible = false;
                }
                else
                {
                    divmessage.Attributes.Add("class", "error");
                    divmessage.Style.Add("display", "block");
                    divmessage.InnerHtml = status;
                    btnRegisterCancel.Visible = true;
                }

            }
        }
    }

    /// <summary>
    /// This event is fired on the change in the selected index of the coutry list dropdown.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void countryList_SelectedIndexChanged(object sender, EventArgs e)
    {

        try
        {
            if (Convert.ToInt32(countryList.SelectedValue) != 0)
            {
                BindGeneralIfoCity(Convert.ToInt32(countryList.SelectedValue));
            }
            else
            {
                BindGeneralIfoCity(0);
            }

            if (countryList.SelectedValue != "")
            {
                TList<HearUsQuestion> Obj = newUserObject.GetQuestionByCountryID(Convert.ToInt32(countryList.SelectedValue));
                //if (Obj.Count > 0)
                //{
                    modeList.DataSource = Obj;
                    modeList.DataTextField = "Question";
                    modeList.DataValueField = "Id";
                    modeList.DataBind();
                //}
                //else
                //{
                    modeList.Items.Insert(0, new ListItem("--Select--", "0"));
                //}
            }

        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This event is fired on the change in the selected index of the coutry list dropdown in the financial section.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void selectCountryList_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (Convert.ToInt32(selectCountryList.SelectedValue) != 0)
            {
                BindFinancialInfoCity(Convert.ToInt32(selectCountryList.SelectedValue));
            }
            else
            {
                BindFinancialInfoCity(0);
            }

        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This event is fired on changing the checked status of the same data check box.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void sameDataCheckBox_CheckedChanged(object sender, EventArgs e)
    {
        if (sameDataCheckBox.Checked == true)
        {
            txtName.Text = firstName.Text;
            txtEmail.Text = email.Text;
            selectCountryList.SelectedIndex = countryList.SelectedIndex;
            if (Convert.ToInt32(selectCountryList.SelectedItem.Value) != 0)
            {
                //ListItem item = new ListItem("--Select city--", "0");
                cityDropList.DataValueField = "Id";
                cityDropList.DataTextField = "City";
                cityDropList.DataSource = newUserObject.GetCityByCountry(Convert.ToInt32(selectCountryList.SelectedItem.Value));
                cityDropList.DataBind();
                //cityDropList.Items.Add(item);
                cityDropList.SelectedValue = cityList.SelectedValue;
            }
            else
            {
                cityDropList.Items.Clear();
                ListItem item = new ListItem("--Select city--", "0");
                cityDropList.Items.Add(item);
            }
            txtAddress.Text = address.Text;
            countryCodeList2.SelectedIndex = countryCodeList1.SelectedIndex;
            txtPhone.Text = phone.Text;
            txtPostalCode.Text = postalCode.Text;
            txtFinanceCity.Text = txtCity.Text;
        }
        else
        {
            txtName.Text = "";
            txtEmail.Text = "";
            selectCountryList.SelectedIndex = 0;
            if (Convert.ToInt32(selectCountryList.SelectedItem.Value) != 0)
            {
                //ListItem item = new ListItem("--Select city--", "0");
                cityDropList.DataValueField = "Id";
                cityDropList.DataTextField = "City";
                cityDropList.DataSource = newUserObject.GetCityByCountry(Convert.ToInt32(selectCountryList.SelectedItem.Value));
                cityDropList.DataBind();
                //cityDropList.Items.Add(item);
            }
            else
            {
                cityDropList.Items.Clear();
                ListItem item = new ListItem("--Select city--", "0");
                cityDropList.Items.Add(item);
            }
            txtAddress.Text = "";
            countryCodeList2.SelectedIndex = 0;
            txtPhone.Text = "";
            txtPostalCode.Text = "";
            txtFinanceCity.Text = "";
        }
        divmessage.Attributes.Add("class", "error");
        divmessage.Style.Add("display", "none");
        divmessage.InnerHtml = status;
    }

    /// <summary>
    /// This event used for redirect to the login page and to the user home page.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void cancelBtn_Click(object sender, EventArgs e)
    {
        if (Session["superadmin"] == null)
        {
            if (registerBtn.Text == "Update")
            {
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "default/english");
                }
            }
            if (l != null)
            {
                Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
            }
            else
            {
                Response.Redirect(SiteRootPath + "login/english");
            }
        }
        else
        {
            Response.Redirect("~/Operator/usermenu.aspx");
        }

    }

    #endregion

    #region Methods

    /// <summary>
    /// This method fills the coutry lists with countries names.
    /// </summary>
    public void BindCountry()
    {
        TList<Country> countries = newUserObject.GetByAllCountry();

        countryList.DataValueField = "Id";
        countryList.DataTextField = "CountryName";
        countryList.DataSource = countries;
        countryList.DataBind();
        countryList.Items.Insert(0, new ListItem("--" + GetKeyResult("SELECTCOUNTRY") + "--", "0"));

        //This function use for bind finecial info country.
        selectCountryList.DataValueField = "Id";
        selectCountryList.DataTextField = "CountryName";
        selectCountryList.DataSource = countries;
        selectCountryList.DataBind();
        selectCountryList.Items.Insert(0, new ListItem("--" + GetKeyResult("SELECTCOUNTRY") + "--", "0"));
    }

    /// <summary>
    /// This function used for bind financial Info city by countryId.
    /// </summary>
    /// <param name="countryID"></param>
    public void BindFinancialInfoCity(int countryID)
    {
        if (countryID != 0)
        {
            cityDropList.DataValueField = "Id";
            cityDropList.DataTextField = "City";
            cityDropList.DataSource = newUserObject.GetCityByCountry(Convert.ToInt32(selectCountryList.SelectedItem.Value));
            cityDropList.DataBind();
            //cityDropList.Items.Insert(0, new ListItem("--Select city--", "0"));
        }
        else
        {
            cityDropList.Items.Clear();
            cityDropList.Items.Insert(0, new ListItem("--Select city--", "0"));

        }

    }

    /// <summary>
    /// This function used for bind general  info city by countryID. 
    /// </summary>
    /// <param name="countryID"></param>
    public void BindGeneralIfoCity(int countryID)
    {
        if (countryList.SelectedIndex != 0)
        {
            cityList.DataValueField = "Id";
            cityList.DataTextField = "City";
            cityList.DataSource = newUserObject.GetCityByCountry(Convert.ToInt32(countryList.SelectedItem.Value));
            cityList.DataBind();
            //cityList.Items.Insert(0, new ListItem("--Select city--", "0"));
        }
        else
        {
            cityList.Items.Clear();
            cityList.Items.Insert(0, new ListItem("--select city--", "0"));

        }

    }

    /// <summary>
    /// This method returns the resultant string for the passed key
    /// </summary>
    /// <param name="key"></param>
    /// <returns>string</returns>
    public string GetKeyResult(string key)
    {
        return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    }

    /// <summary>
    /// Binds the preferred language dropdown with the appropriate datasource
    /// </summary>
    public void preferredLanguages()
    {
        TList<Language> languages = newUserObject.GetAllLanguages();
        languageList.DataValueField = "Id";
        languageList.DataTextField = "Name";
        languageList.DataSource = languages;
        languageList.DataBind();
    }


    public void fillStaticContent()
    {

        countryCodeList1.Items.Insert(0, new ListItem(GetKeyResult("EXTENSION"), ""));
        countryCodeList2.Items.Insert(0, new ListItem(GetKeyResult("EXTENSION"), ""));
        modeList.Items.Insert(0, new ListItem("--Select--", "0"));


        //modeList.Items.Add(new ListItem(GetKeyResult("INTERNETSEARCH"),"1"));
        //modeList.Items.Add(new ListItem(GetKeyResult("COLLEAGUE"),"2"));
        //modeList.Items.Add(new ListItem(GetKeyResult("MARKETING"),"3"));
        //modeList.Items.Add(new ListItem(GetKeyResult("OURSALESTEAM"),"4"));
        //modeList.Items.Add(new ListItem(GetKeyResult("ONLINEREFERRAL"),"5"));
        //modeList.Items.Add(new ListItem(GetKeyResult("EUMAORG"),"6"));
        //modeList.Items.Add(new ListItem(GetKeyResult("CORPORATETRAVELLER"),"7"));
        //modeList.Items.Add(new ListItem(GetKeyResult("EXPERIENCEMAGAZINE"),"8"));
        //modeList.Items.Add(new ListItem(GetKeyResult("MIMMEGAZINE"),"9"));
        //modeList.Items.Add(new ListItem(GetKeyResult("HEADQUARTERS"),"10"));
        //modeList.Items.Add(new ListItem(GetKeyResult("EVENTNEWS"),"11"));
        //modeList.Items.Add(new ListItem(GetKeyResult("OTHER"),"12"));
        registerBtn.Text = GetKeyResult("REGISTER");
        cancelBtn.Text = GetKeyResult("CANCEL");
    }
    #endregion

}