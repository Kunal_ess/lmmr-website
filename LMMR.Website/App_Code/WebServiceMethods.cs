﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;
using LMMR.Data;
using LMMR.Business;
using LMMR.Entities;
using System.Xml;
using System.Data;

/// <summary>
/// Summary description for WebServiceMethods
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class WebServiceMethods : System.Web.Services.WebService {

    public WebServiceMethods () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    
    [WebMethod]
    public VList<Viewbookingrequest> BookingRequestList(string Countryname, string hotelname, string fromdate, string todate)
    {
        VList<Viewbookingrequest> vlistreq;
        ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
        
        //-- get country
            int count = 0;
            string orderbyclause = string.Empty;
            string where = CountryColumn.CountryName + " like  '%" + Countryname + "%'";
            Country objCountryName = DataRepository.CountryProvider.GetPaged(where, orderbyclause, 0, int.MaxValue, out count).FirstOrDefault();

        //-- start search condition
            string whereclaus = "hotelname like '%" + hotelname + "%'  and countryid =" + objCountryName.Id; // book type for request and booking
        //-- for dates
            if (!string.IsNullOrEmpty(fromdate) && !string.IsNullOrEmpty(todate))
                whereclaus += " and arrivaldate between convert(DATETIME, '" + fromdate + "',103) and convert(DATETIME , '" + todate + "',103) ";
            else
            {
                if (!string.IsNullOrEmpty(fromdate))
                    whereclaus += " and  convert(DATETIME, '" + fromdate + "',103) <= arrivaldate ";

                if (!string.IsNullOrEmpty(todate))
                    whereclaus += " and  convert(DATETIME, '" + todate + "',103) >= arrivaldate ";
            }


    
        string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
        vlistreq = objViewBooking_Hotel.Bindgrid(whereclaus, orderby);

        return vlistreq;
    }

    [WebMethod]
    public List<Availability> CheckAvailability(string Countryname, string Hotelname, string Fromdate, string Todate)
    {
        VList<ViewWsCheckAvailability> vlist;
        ViewBooking_Hotel objViewBooking = new ViewBooking_Hotel();

        //-- get country                                      
        string whereclaus = "Name like '%" + Hotelname + "%'  and CountryName like '%" + Countryname + "%'";        
        if (!string.IsNullOrEmpty(Fromdate) && !string.IsNullOrEmpty(Todate))
            whereclaus += " and AvailabilityDate between convert(DATETIME, '" + Fromdate + "',103) and convert(DATETIME , '" + Todate + "',103) ";
        else
        {
            if (!string.IsNullOrEmpty(Fromdate))
                whereclaus += " and  convert(DATETIME, '" + Fromdate + "',103) <= AvailabilityDate ";

            if (!string.IsNullOrEmpty(Todate))
                whereclaus += " and  convert(DATETIME, '" + Todate + "',103) >= AvailabilityDate ";
        }



        string orderby = ViewWsCheckAvailabilityColumn.AvailabilityDate + " DESC";
        vlist = objViewBooking.GetWsCheckAvailability(whereclaus, orderby).FindAllDistinct(ViewWsCheckAvailabilityColumn.AvailabilityDate);
        return vlist.Select(a => new Availability() { CountryName = a.CountryName,CityName=a.CityName, HotelName = a.Name, AvailabilityDate = Convert.ToDateTime(a.AvailabilityDate) }).ToList();

        //DataSet ds = new DataSet();
        //ds.Tables.Add(vlist);
        //XmlDocument xdoc = new XmlDocument();
        //xdoc.LoadXml(ds.GetXml());       
    }    


    
}
[Serializable]
public class Availability
{
    public string CountryName
    {
        get;
        set;
    }
    public string CityName
    {
        get;
        set;
    }
    public string HotelName
    {
        get;
        set;
    }   
    public DateTime AvailabilityDate
    {
        get;
        set;
    }
}
