﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using LMMR.Data;
using LMMR.Business;
using LMMR.Entities;

/// <summary>
/// Summary description for EmailFire
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class EmailFire : System.Web.Services.WebService {

    public EmailFire () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }


    [WebMethod]
    public void CallEmail()
    {
         periodicmailbase mail = new periodicmailbase();
        mail.callmail();
    }
}
