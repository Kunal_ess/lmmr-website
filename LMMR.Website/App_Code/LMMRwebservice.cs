﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
#endregion


/// <summary>
/// Summary description for LMMRwebservice
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class LMMRwebservice : System.Web.Services.WebService
{
    #region Variables and Properties
    private AvalibalityManager objAvailabilityManager = new AvalibalityManager();
    private SpecialPriceandPromoManager objSpecialPriceandPromo = new SpecialPriceandPromoManager();
    #endregion

    #region Contracts
    public LMMRwebservice () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    #endregion

    #region Hello World Example
    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }

    /// <summary>
    /// Get Property Status.(Only for test)
    /// </summary>
    /// <param name="newresult"></param>
    /// <returns></returns>
    [WebMethod]
    public LMMR.Entities.Hotel propertyStatus(string newresult)
    {
        LMMR.Entities.Hotel objHotel = new LMMR.Entities.Hotel();
        objHotel.Email = newresult;
        return objHotel;
    }

    /// <summary>
    /// Get Details.(only for test)
    /// </summary>
    /// <param name="dataval"></param>
    /// <returns></returns>
    [WebMethod]
    public string getDetails(string dataval)
    {
        return dataval;
    }
    #endregion

    #region Availability
    /// <summary>
    /// Get Full Property Details
    /// </summary>
    /// <param name="HotelId"></param>
    /// <param name="AvailabilityDate"></param>
    /// <param name="StartDate"></param>
    /// <param name="EndDate"></param>
    /// <returns></returns>
    [WebMethod]
    public VList<AvailabilityWithSpandp> GetFullPropertDetails(int HotelId, string AvailabilityDate, string StartDate, string EndDate)
    {
        //Get full property details from server.
        return objAvailabilityManager.GetFullPropertDetails(HotelId, AvailabilityDate, StartDate, EndDate);
    }

    /// <summary>
    /// Get all Meeting room details.
    /// </summary>
    /// <param name="HotelId"></param>
    /// <param name="AvailabilityDate"></param>
    /// <param name="StartDate"></param>
    /// <param name="EndDate"></param>
    /// <returns></returns>
    [WebMethod]
    public VList<ViewAvailabilityOfRooms> GetMeetingRoomDetails(int HotelId, string AvailabilityDate, string StartDate, string EndDate, long meetingroomid)
    {
        //Get Meetingroom Details By Hotel Id
        return objAvailabilityManager.GetAllMeetingRoomDetailsByHotelID(HotelId, AvailabilityDate, StartDate, EndDate, meetingroomid);
    }

    /// <summary>
    /// Get all bedroom details.
    /// </summary>
    /// <param name="HotelId"></param>
    /// <param name="AvailabilityDate"></param>
    /// <param name="StartDate"></param>
    /// <param name="EndDate"></param>
    /// <returns></returns>
    [WebMethod]
    public VList<ViewAvailabilityOfBedRoom> GetBedRoomDetails(int HotelId, string AvailabilityDate, string StartDate, string EndDate)
    {
        //Get All Bedroom Details By Hotel ID
        return objAvailabilityManager.GetAllBedRoomDetailsByHotelID(HotelId, AvailabilityDate, StartDate, EndDate);
    }

    /// <summary>
    /// Update full property of Availability and Update other details with Full property also.
    /// </summary>
    /// <param name="FullPropDetails"></param>
    /// <returns></returns>
    [WebMethod]
    public string UpdateFullProperty(string FullPropDetails)
    {
        string[] Propertydtl = FullPropDetails.Split('|');
        string result = "";
        if (Propertydtl.Length > 0)
        {
            Int64 availabilityID = Convert.ToInt64(Propertydtl[Propertydtl.Length - 1]);
            string[] daymonthyear = Propertydtl[0].Split('/');
            DateTime dtavailability;
            if (daymonthyear.Length > 0)
            {
                dtavailability = new DateTime(Convert.ToInt32(daymonthyear[2]), Convert.ToInt32(daymonthyear[1]), Convert.ToInt32(daymonthyear[0]));
            }
            else
            {
                dtavailability = DateTime.Now;
            }
            string statusAvailability = Propertydtl[Propertydtl.Length - 2];
            //Update Full Property according to Availability ID, Date of Availability and Status of Availability
            result = objAvailabilityManager.UpdateFullPropertyMeetingRoom(availabilityID, dtavailability, statusAvailability);
        }
        return "Update";
    }

    /// <summary>
    /// Update full property only in Availability Screen
    /// </summary>
    /// <param name="FullPropDetails"></param>
    /// <returns></returns>
    [WebMethod]
    public string UpdateonlyFullPropertyDetails(string FullPropDetails)
    {
        string[] Propertydtl = FullPropDetails.Split('|');
        string result = "";
        if (Propertydtl.Length > 0)
        {
            Int64 availabilityID = Convert.ToInt64(Propertydtl[Propertydtl.Length - 1]);
            string[] daymonthyear = Propertydtl[0].Split('/');
            DateTime dtavailability;
            if (daymonthyear.Length > 0)
            {
                dtavailability = new DateTime(Convert.ToInt32(daymonthyear[2]), Convert.ToInt32(daymonthyear[1]), Convert.ToInt32(daymonthyear[0]));
            }
            else
            {
                dtavailability = DateTime.Now;
            }
            string statusAvailability = Propertydtl[Propertydtl.Length - 2];
            //Update the full propert details According to Availability ID, Date of Availability and Status of Availability.
            result = objAvailabilityManager.UpdateFullPropertyMeetingRoomOnly(availabilityID, dtavailability, statusAvailability);
        }
        return "Update";
    }

    /// <summary>
    /// Update Full bedroom details of Availability Screen (NOW THIS IS NOT IN USE AS PER CLIENT CHANGE)
    /// </summary>
    /// <param name="FullBRPropDetails"></param>
    /// <returns></returns>
    [WebMethod]
    public string UpdateFullBedRoomProperty(string FullBRPropDetails)
    {
        string[] Propertydtl = FullBRPropDetails.Split('|');
        string result = "";
        if (Propertydtl.Length > 0)
        {
            Int64 availabilityID = Convert.ToInt64(Propertydtl[Propertydtl.Length - 1]);
            string[] daymonthyear = Propertydtl[0].Split('/');
            DateTime dtavailability;
            if (daymonthyear.Length > 0)
            {
                dtavailability = new DateTime(Convert.ToInt32(daymonthyear[2]), Convert.ToInt32(daymonthyear[1]), Convert.ToInt32(daymonthyear[0]));
            }
            else
            {
                dtavailability = DateTime.Now;
            }
            string statusAvailability = Propertydtl[Propertydtl.Length - 2];
            //Update Full Propery of Bedroom.
            result = objAvailabilityManager.UpdateFullPropertyBedRoom(availabilityID, dtavailability, statusAvailability);
        }
        return "Update";
    }

    /// <summary>
    /// Update Availability of Meeting room by Meetingroom Details
    /// </summary>
    /// <param name="MeetingRoomDetails"></param>
    /// <returns></returns>
    [WebMethod]
    public string UpdateAvailability(string MeetingRoomDetails)
    {
        string[] Propertydtl = MeetingRoomDetails.Split('|');
        string result = "";
        if (Propertydtl.Length > 0)
        {
            Int64 availabilityID = Convert.ToInt64(Propertydtl[Propertydtl.Length - 1]);
            string[] daymonthyear = Propertydtl[0].Split('/');
            DateTime dtavailability;
            if (daymonthyear.Length > 0)
            {
                dtavailability = new DateTime(Convert.ToInt32(daymonthyear[2]), Convert.ToInt32(daymonthyear[1]), Convert.ToInt32(daymonthyear[0]));
            }
            else
            {
                dtavailability = DateTime.Now;
            }
            string Daystatus = Propertydtl[Propertydtl.Length - 3];
            string statusAvailability = Propertydtl[Propertydtl.Length - 2];
            result = objAvailabilityManager.UpdateCurrentAvailability(availabilityID, dtavailability, Daystatus, statusAvailability);
        }

        return result;
    }

    /// <summary>
    /// Update Availability of Bedroom by bedroom details.
    /// </summary>
    /// <param name="BedRoomDetails"></param>
    /// <returns></returns>
    [WebMethod]
    public string UpdateAvailabilityBR(string BedRoomDetails)
    {
        string[] Propertydtl = BedRoomDetails.Split('|');
        string result = "";
        if (Propertydtl.Length > 0)
        {
            Int64 availabilityID = Convert.ToInt64(Propertydtl[Propertydtl.Length - 1]);
            string[] daymonthyear = Propertydtl[0].Split('/');
            DateTime dtavailability;
            if (daymonthyear.Length > 0)
            {
                dtavailability = new DateTime(Convert.ToInt32(daymonthyear[2]), Convert.ToInt32(daymonthyear[1]), Convert.ToInt32(daymonthyear[0]));
            }
            else
            {
                dtavailability = DateTime.Now;
            }
            //string Daystatus = Propertydtl[Propertydtl.Length - 3];
            string statusAvailability = Propertydtl[Propertydtl.Length - 2];
            result = objAvailabilityManager.UpdateCurrentBRAvailability(availabilityID, dtavailability, string.Empty, statusAvailability);
        }

        return result;
    }

    /// <summary>
    /// Adjust Meetingroom Details by meetingroom details.
    /// </summary>
    /// <param name="MeetingRoomIds"></param>
    /// <param name="MeetingDays"></param>
    /// <param name="ClosingStatus"></param>
    /// <param name="OpeningStatus"></param>
    /// <param name="FromDate"></param>
    /// <param name="ToDate"></param>
    /// <returns></returns>
    [WebMethod]
    public string AdjustMeetingRoom(string MeetingRoomIds, string MeetingDays, string ClosingStatus, string OpeningStatus, string FromDate, string ToDate)
    {
        foreach (string s in MeetingRoomIds.Split('|'))
        {
            for (int i = 0; i < MeetingDays.Split('|').Length; i++)
            {
                DateTime startdate = new DateTime(Convert.ToInt32("20" + FromDate.Split('/')[2]), Convert.ToInt32(FromDate.Split('/')[1]), Convert.ToInt32(FromDate.Split('/')[1]));
                DateTime enddate = new DateTime(Convert.ToInt32("20" + ToDate.Split('/')[2]), Convert.ToInt32(ToDate.Split('/')[1]), Convert.ToInt32(ToDate.Split('/')[1]));
                for (int j = 0; startdate >= enddate; startdate = startdate.AddDays(j))
                {
                    if (Convert.ToInt32(MeetingDays.Split('|')[i]) == (int)startdate.DayOfWeek)
                    {
                        objAvailabilityManager.AdjustMeetingRoombyMRIDandDate(Convert.ToInt32(s), startdate, ClosingStatus.Split('|')[i], OpeningStatus.Split('|')[i]);
                    }
                }
            }
        }
        return "";
    }
    #endregion

    #region Special Price and Promo
    /// <summary>
    /// Get Special Price and Promo by Date according to Hotelid, Availability Date, Start Date and End Date.
    /// </summary>
    /// <param name="HotelId"></param>
    /// <param name="AvailabilityDate"></param>
    /// <param name="StartDate"></param>
    /// <param name="EndDate"></param>
    /// <returns></returns>
    [WebMethod]
    public TList<SpecialPriceAndPromo> GetSpecialPriceandPromoByDate(int HotelId, string AvailabilityDate, string StartDate, string EndDate)
    {
        return objSpecialPriceandPromo.GetSpecialPriceandPromoByDate(HotelId, AvailabilityDate, StartDate, EndDate);
    }

    /// <summary>
    /// Get Special Price and Promo of Package by HotelId, AvailabilityDate, Startdate and Enddate and Package Id also.
    /// </summary>
    /// <param name="HotelId"></param>
    /// <param name="AvailabilityDate"></param>
    /// <param name="StartDate"></param>
    /// <param name="EndDate"></param>
    /// <param name="PId"></param>
    /// <returns></returns>
    [WebMethod]
    public VList<ViewSpandPpercentageOfPackage> GetSpecialPriceandPromoByPackage(int HotelId, string AvailabilityDate, string StartDate, string EndDate, Int64 PId)
    {
        return objSpecialPriceandPromo.GetSpecialPriceandPromoByPackage(HotelId, AvailabilityDate, StartDate, EndDate, PId);
    }

    /// <summary>
    /// Get Special Price and Promo for Availability panel.
    /// </summary>
    /// <param name="HotelId"></param>
    /// <param name="AvailabilityDate"></param>
    /// <param name="StartDate"></param>
    /// <param name="EndDate"></param>
    /// <returns></returns>
    [WebMethod]
    public List<AvailabilitySPandP> GetSpecialPriceandPromoAvailability(int HotelId, string AvailabilityDate, string StartDate, string EndDate)
    {
        return objSpecialPriceandPromo.GetSpecialPriceandPromoAvailability(HotelId, AvailabilityDate, StartDate, EndDate);
    }

    /// <summary>
    /// Get Special Price and Promo by Meeting room Id.
    /// </summary>
    /// <param name="HotelId"></param>
    /// <param name="AvailabilityDate"></param>
    /// <param name="StartDate"></param>
    /// <param name="EndDate"></param>
    /// <param name="MRId"></param>
    /// <returns></returns>
    [WebMethod]
    public VList<ViewSpandPpercentageOfMeetingRoom> GetSpecialPriceandPromoByMeetingRoom(int HotelId, string AvailabilityDate, string StartDate, string EndDate, Int64 MRId)
    {
        return objSpecialPriceandPromo.GetSpecialPriceandPromoByMeetingRoom(HotelId, AvailabilityDate, StartDate, EndDate, MRId);
    }

    /// <summary>
    /// Get Special Price and Promo by BedroomID.
    /// </summary>
    /// <param name="HotelId"></param>
    /// <param name="AvailabilityDate"></param>
    /// <param name="StartDate"></param>
    /// <param name="EndDate"></param>
    /// <param name="BRId"></param>
    /// <returns></returns>
    [WebMethod]
    public TList<SpecialPriceForBedroom> GetSpecialPriceandPromoByBedRoom(int HotelId, string AvailabilityDate, string StartDate, string EndDate, Int64 BRId)
    {
        return objSpecialPriceandPromo.GetSpecialPriceandPromoByBedRoom(HotelId, AvailabilityDate, StartDate, EndDate, BRId);
    }
    #endregion

    [WebMethod]
    public bool CallMeIfRequired()
    {
        HotelManager hm = new HotelManager();

        return hm.AddUpdateNewSpandP();
    }
    [WebMethod]
    public bool CallMeIfRequired2()
    {
        HotelManager hm = new HotelManager();

        return hm.AddUpdateNewSpandP();
    }
    void callAvailability(object sender)
    {
        HotelManager hm = new HotelManager();

        hm.AddUpdateNewSpandP();
    }

    [WebMethod]
    public void DeleteCalls(string date)
    {
        DateTime dt = new DateTime(Convert.ToInt32(date.Split('-')[0]),Convert.ToInt32(date.Split('-')[1]), Convert.ToInt32(date.Split('-')[2]));
        int count = 0;
        TList<LMMR.Entities.Availability> avail = DataRepository.AvailabilityProvider.GetPaged(LMMR.Entities.AvailabilityColumn.AvailabilityDate + ">= '" + dt + "'","",0,int.MaxValue,out count);
        DataRepository.AvailabilityProvider.Delete(avail);
    }

    [WebMethod]
    public FinalResultForAvailability BindCalanderWithListWhere(string whereClause, DateTime startdate, DateTime enddate)
    {
        
        VList<ViewAvailabilityAndSpecialManager> vAvailabilitySpecial = new VList<ViewAvailabilityAndSpecialManager>();
        FinalResultForAvailability frfa = new FinalResultForAvailability();

        string trAppend = "";
        vAvailabilitySpecial = new AvailabilityAndSpecialManager().GetByPaged(whereClause, "p.Id, SpecialPriceDate ASC", 0, int.MaxValue);
        var Vfirst = vAvailabilitySpecial.Select(a => a.SpecialPriceDate).FirstOrDefault();
        List<DateTime?> ListDays = new List<DateTime?>();//vAvailabilitySpecial.Select(a => a.SpecialPriceDate).Distinct().ToList();
        for (int i = 0; startdate.AddDays(i) <= enddate; i++)
        {
            ListDays.Add(new DateTime(startdate.AddDays(i).Year,startdate.AddDays(i).Month,startdate.AddDays(i).Day));
        }
        if (Vfirst != null)
        {
            int currentMonth = 0;
            int previousMonth = Vfirst.Value.Month;
            int currentColSpan = 0;
            int count = 0;
            frfa.HotelID = vAvailabilitySpecial[0].Id;
            string DateCollection = string.Empty;
            //int counter = 0;
            //for (int v = 0; v < ListHotel.Count; v++)
            //{
            
            trAppend += "<tr class='raw4'>";

            for (int vdate = 0; vdate < ListDays.Count(); vdate++)
            {
                ViewAvailabilityAndSpecialManager vm = vAvailabilitySpecial.Where(a => a.SpecialPriceDate == ListDays[vdate].Value).FirstOrDefault();
                if (vm != null)
                {
                    int discountcount = (int)(40 - (40 * (vm.BookedMeetingRoom.Value / (vm.MeetingroomCount.Value == 0 ? 1 : vm.MeetingroomCount.Value))));
                    trAppend += "<td valign='top' bgcolor='#FF0000'><div class='adjust-main'><div class='adjust-first' style='height:" + discountcount + "px;'><div class='adjust-inner'>" + Math.Round(((vm.BookedMeetingRoom.Value / (vm.MeetingroomCount.Value == 0 ? 1 : vm.MeetingroomCount.Value)) * 100), 2) + "%</div></div></div></td>";
                }
                else
                {
                    trAppend += "<td valign='top' bgcolor='#FF0000'><div class='adjust-main'><div class='adjust-first-disable' style='height:40px;'><div class='adjust-inner'></div></div></div></td>";
                }
            }
            trAppend += "</tr>";
            trAppend += "<tr class='raw2'>";
            foreach (DateTime vcontent in ListDays)
            {
                ViewAvailabilityAndSpecialManager vm = vAvailabilitySpecial.Where(a => a.SpecialPriceDate == vcontent).FirstOrDefault();
                if (vm != null)
                {
                    trAppend += "<td height='35'>" + (vm.DdrPercent == 0 ? "" : Math.Round(vm.DdrPercent.Value, 2).ToString()) + "<br/>" + Math.Round(vm.BookedMeetingRoom.Value, 1) + "/" + vm.MeetingroomCount + "<br/>" + vm.BookedBedroom.Value + "/" + vm.BedroomCount + "</td>";
                }
                else
                {
                    trAppend += "<td height='35'>N/A</td>";
                }
            }
            trAppend += "</tr>";
                //counter++;
                //if (counter == 10)
                //{
                //    break;
                //}
            //}
        }
        else
        {
            trAppend = "<div class='warning'>No record found!</div>";
        }
        frfa.HotelString = trAppend;
        return frfa;
    }
}
public class FinalResultForAvailability
{
    public long HotelID
    {
        get;
        set;
    }
    public string HotelString
    {
        get;
        set;
    }
}