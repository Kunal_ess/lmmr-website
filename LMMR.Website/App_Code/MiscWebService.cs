﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
/// <summary>
/// Summary description for MiscWebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService()]
public class MiscWebService : System.Web.Services.WebService {
    ManageMapSearch objManageSearch = new ManageMapSearch();
    public MiscWebService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }

    [WebMethod]
    public List<MyCity> GetCityByCountryIDForFrontend(long CountryID)
    {
        List<MyCity> objMyCity = new List<MyCity>();
        foreach (City c in objManageSearch.GetCityData(Convert.ToInt32(CountryID)))
        {
            MyCity m = new MyCity();
            m.id = c.Id;
            m.Name = c.City;
            objMyCity.Add(m);
        }
        return objMyCity;
    }
    [WebMethod]
    public VList<BookingRequestViewList> GetBookingReqDetails(string where, string orderby)
    {
        int totalcount = 0;
        VList<BookingRequestViewList> brViewList = new VList<BookingRequestViewList>();
        if (where.Length > 0)
        {
            where += " and ";
        }
        orderby = "";
        where += BookingRequestViewListColumn.IsPriority + "<>'NO'";
        brViewList = DataRepository.BookingRequestViewListProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount);
        return brViewList;   
    }
}

public class MyCity
{
    public long id
    {
        get;
        set;
    }
    public string Name
    {
        get;
        set;
    }
}
