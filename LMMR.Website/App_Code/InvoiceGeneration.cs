﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Net;
using System.Text;
using System.IO;
using LMMR.Data;
using LMMR.Business;
using LMMR.Entities;

/// <summary>
/// Summary description for InvoiceGeneration
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class InvoiceGeneration : System.Web.Services.WebService
{

    public InvoiceGeneration()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
   
    [WebMethod]
    public bool SaveFile()
    {
        try
        {
            #region For Invoice Generation
            FinanceInvoice objFinance = new FinanceInvoice();
            DateTime currentdate = new DateTime(Convert.ToInt32(System.DateTime.Now.Year), Convert.ToInt32(System.DateTime.Now.Month), Convert.ToInt32(System.DateTime.Now.Day));
            if (Convert.ToInt32(currentdate.Day) == 7 || Convert.ToInt32(currentdate.Day) == 8)
            {
                DateTime firstDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1);
                DateTime lastDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(-1);


                #region Generate All Invoice details into Invoice table for last month
                ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
                string where = "DepartureDate" + " between '" + firstDate + "' and '" + lastDate.AddDays(1) + "' and (requeststatus=6 or (requeststatus=7 and IsComissionDone=1)) and InvoiceId is null";
                string orderby = "";
                int totalcount = 0;
                int intCount = 1;
                VList<Viewbookingrequest> Vlistreq = new VList<Viewbookingrequest>();
                Vlistreq = DataRepository.ViewbookingrequestProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount).FindAllDistinct(ViewbookingrequestColumn.HotelId);
                if (Vlistreq.Count > 0)
                {
                    foreach (Viewbookingrequest vl in Vlistreq)
                    {
                        #region Insert invoice of all hotel on every month of 7th
                        DateTime now = DateTime.Now;
                        Invoice invDetails = new Invoice();
                        invDetails.HotelId = vl.HotelId;
                        //invDetails.TotalAmount = vl.ConfirmRevenueAmount;
                        //invDetails.IsPaid = true;
                        invDetails.DateFrom = firstDate;
                        invDetails.DateTo = lastDate;
                        invDetails.DueDate = System.DateTime.Now;
                        invDetails.InvoiceNumber = now.ToString("yyyy") + '-' + now.ToString("MM") + '-' + "00" + intCount++;
                        DataRepository.InvoiceProvider.Insert(invDetails);

                        string whereClause = "HotelId=" + vl.HotelId + " and " + "DepartureDate" + " between '" + firstDate + "' and '" + lastDate.AddDays(1) + "' and (requeststatus=6 or (requeststatus=7 and IsComissionDone=1))";
                        TList<Booking> objBooking = objViewBooking_Hotel.CheckBookingInvoice(whereClause);
                        foreach (Booking b in objBooking)
                        {
                            b.InvoiceId = invDetails.Id;
                            b.RequestStatus = (int) BookingRequestStatus.Frozen;
                            DataRepository.BookingProvider.Update(objBooking);
                        }
                        #endregion
                    }
                }
                #endregion
                
                #region For Pdf generation
                string whereInv = "Datefrom >='" + firstDate + "' and  Dateto <='" + lastDate.AddDays(1) + "' and InvoiceFile is null";
                TList<Invoice> lstInvoiceChk = objFinance.GetInvoicebyCondition(whereInv, String.Empty);
                if (lstInvoiceChk.Count > 0)
                {
                    foreach (Invoice vl in lstInvoiceChk)
                    {                        
                        //HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://107.21.215.144/LMMR.website/Operator/InvoiceGenerate.aspx?HotelId=" + vl.HotelId + "&Id=" + vl.Id + "");
                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(SiteRootPath + "Operator/InvoiceGenerate.aspx?HotelId=" + vl.HotelId + "&Id=" + vl.Id + "");                        

                        // Set some reasonable limits on resources used by this request
                        request.MaximumAutomaticRedirections = 4;
                        request.MaximumResponseHeadersLength = 4;
                        // Set credentials to use for this request.
                        request.Credentials = CredentialCache.DefaultCredentials;
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                        Console.WriteLine("Content length is {0}", response.ContentLength);
                        Console.WriteLine("Content type is {0}", response.ContentType);

                        // Get the stream associated with the response.
                        Stream receiveStream = response.GetResponseStream();

                        // Pipes the stream to a higher level stream reader with the required encoding format. 
                        StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

                        Console.WriteLine("Response stream received.");
                        Console.WriteLine(readStream.ReadToEnd());
                        response.Close();
                        readStream.Close();
                    }
                }
                #endregion


            }
            return true;
            #endregion
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl + "/";
        }
    }
}
