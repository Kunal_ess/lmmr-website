﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeMaster.master" AutoEventWireup="true"
    MaintainScrollPositionOnPostback="true" CodeFile="SearchBooking.aspx.cs" Inherits="SearchBooking" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntLeft" runat="Server">
    <!--basket-request START HERE-->
    <div id="divshoppingcart" runat="server" style="display: none">
        <div class="shopping-cart-body">
            <div class="shopping-cart-heading">
                Shopping<br>
                cart
            </div>
            <div class="shopping-cart-inner">
                <div class="shopping-cart-inner-left">
                    <h2>
                        Hotel/Facility names</h2>
                </div>
                <div class="shopping-cart-inner-right">
                    all <a href="#">
                        <img src="<%= SiteRootPath %>images/delete-ol-btn.png" align="absmiddle" />
                    </a>
                </div>
            </div>
            <div class="shopping-cart-inner-ol">
                <ol>
                    <asp:ListView ID="lstViewAddToShopping" runat="server" ItemPlaceholderID="itemHolder"
                        OnItemDataBound="lstViewAddToShopping_ItemDataBound" DataKeyNames="Id" OnItemCommand="lstViewAddToShopping_ItemCommand">
                        <LayoutTemplate>
                            <asp:PlaceHolder ID="itemHolder" runat="server"></asp:PlaceHolder>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <li class="last">
                                <div class="shopping-cart-inner-ol-left">
                                    <%# Eval("Name") %></div>
                                <div class="shopping-cart-inner-ol-right">
                                    <asp:ImageButton ID="imgDelete" ImageUrl="images/delete-ol-btn.png" CommandName="deletehotel"
                                        CommandArgument='<%#Eval("Id") %>' runat="server" />
                                </div>
                                <ol>
                                    <asp:ListView ID="lstViewMeetingRoomAddToCart" runat="server" ItemPlaceholderID="MeetingroomHolder"
                                        DataKeyNames="Id" OnItemCommand="lstViewMeetingRoomAddToCart_ItemCommand">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder ID="MeetingroomHolder" runat="server"></asp:PlaceHolder>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <li>
                                                <div class="shopping-cart-inner-ol-left1">
                                                    <%# Eval("Name") %></div>
                                                <div class="shopping-cart-inner-ol-right1">
                                                    <asp:ImageButton ID="imgDelete" ImageUrl="images/delete-ol-btn.png" CommandName="deletemeetingroom"
                                                        CommandArgument='<%#Eval("Id") %>' runat="server" />
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </ol>
                            </li>
                        </ItemTemplate>
                    </asp:ListView>
                </ol>
            </div>
        </div>
        <div class="con-with-check-btn">
            <asp:LinkButton ID="lbnContinue" runat="server" OnClick="lbnContinue_Click">Continue with Checkout</asp:LinkButton>
        </div>
    </div>
    <!--basket-request ENDS HERE-->
    <!--filter-form START HERE-->
    <div class="filter-form">
        <div class="filter-form-filter-results">
             <%= GetKeyResult("FILTER")%>
            <br>
             <%= GetKeyResult("RESULTS")%>
        </div>
        <div class="filter-form-inner">            
            <div id="Area">
                <select name="test" class="bigselect">
                    <option>Area</option>
                    <option value="Area1">Area1</option>
                    <option value="Area2">Area2</option>
                    <option value="Area3">Area3</option>
                </select>
            </div>            
            <%--<div id="radiusbody1">
                <div class="radiustext">
                    <%= GetKeyResult("SEARCHINRADIUSOFKM")%>:</div>
                <div id="radius1">
                    <select name="test" class="midselect1">
                        <option>0,5 km</option>
                        <option value="0,5 km">0,5 km</option>
                        <option value="6,10 km">6,10 km</option>
                        <option value="11,15 km">11,15 km</option>
                    </select>
                </div>
            </div>--%>
            <div id="Stars-Modern">
                <div id="Stars">
                    <label>
                        <%= GetKeyResult("STARS")%> &nbsp;</label><input type="text" value="1" name="stars[]" id="stars74" class="inputbox">
                    <input type="button" onclick="var qty_el = document.getElementById('stars74'); var qty = qty_el.value; if( !isNaN( qty )) qty_el.value++;return false;"
                        class="button_up">
                    <input type="button" onclick="var qty_el = document.getElementById('stars74'); var qty = qty_el.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) qty_el.value--;return false;"
                        class="button_down">
                </div>
                <div id="theme">
                    Theme</div>
                <div id="Modern">
                    <select name="test" class="midselect1">
                        <option>Modern</option>
                        <option value="Modern1">Modern1</option>
                        <option value="Modern2">Modern2</option>
                        <option value="Modern3">Modern3</option>
                    </select>
                </div>
            </div>
            <div class="daily-dudget">
                <div class="daily-dudgettext">
                    Daily dudget
                    <br>
                    <span>(per person)<span>
                </div>
                <img src="<%= SiteRootPath %>images/results-para.png" align="absmiddle" />
            </div>
            <input type="button" value="Filter your results" class="filter-button" />           
        </div>
    </div>
    <!--filter-form ENDS HERE-->
    </span></span>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMainbody" runat="Server">
    <!--mainbody START HERE-->
    <asp:HiddenField ID="hdnHotelId" runat="server" />
    <asp:HiddenField ID="hdnMeetingRoomId" runat="server" />
    <asp:HiddenField ID="hdnConfigurationId" runat="server" />
    <asp:HiddenField ID="hdnType" runat="server" />
    <asp:HiddenField ID="hdnColId" runat="server" />
    <div class="mainbody">
        <!--mainbody-left START HERE-->
        <div class="mainbody-left">
            <!--banner START HERE-->
            <div class="banner">
                <div id="map_canvas" style="width: 460px; height: 300px; float: left;">
                </div>
                <asp:Image ID="imgMap" runat="server" Height="300px" Width="460px" ImageUrl="~/Images/faded-map.jpg"
                    Visible="false" />
                <br />
                <span id="mapMessgage" style="display:none; color:Red;">
                Please click on map to set your location.
                </span>
            </div>
            <!--banner ENDS HERE-->
        </div>
        <!--mainbody-left ENDS HERE-->
        <!--mainbody-right START HERE-->
        <div class="mainbody-right">
            <!--mainbody-right-call START HERE-->
            <div class="mainbody-right-call">
                <div class="need">
                    <span><%= GetKeyResult("NEEDHELP")%></span>
                    <br />
                    <%= GetKeyResult("CALLUS")%> 5/7
                </div>
                <div class="phone">
                    +32 2 344 25 50 </div>
             <p style="font-size:10px;color:White;text-align:center">  CALL US and WE DO IT FOR YOU</p>
                <div class="mail">
                        <a href="http://needameetingroom.com/requestform/" onClick="javascript: _gaq.push(['_trackPageview', '/email']); window.open(this.href, 'popupwindow1','width=900,height=600,scrollbars,resizable,toolbar=NO'); &#13;&#10;return false;" Target="_blank"">
                        Send us your RFP</a>
                    </div>
            </div>
            <!--mainbody-right-call ENDS HERE-->
            <!--mainbody-right-join START HERE-->
            <div class="mainbody-right-join">
                <div class="join">
                    <a href="#"><%= GetKeyResult("JOINUSTODAY")%></a></div>
                <%= GetKeyResult("FORHOTELSMEETINGFACILITIESEVENT")%>
            </div>
            <!--mainbody-right-join ENDS HERE-->
            <!--mainbody-right-news START HERE-->
            <div class="mainbody-right-news">
                <div class="mainbody-right-news-top">
                </div>
                <div class="mainbody-right-news-mid">
                    <div class="mainbody-right-news-inner-result">
                        <h2 class="newsletter">
                            <%= GetKeyResult("NEWSLETTER")%>:</h2>
                        <div class="subsribe-body">
                            <input type="text" value="Enter your email" class="inputbox" onclick="this.value=''" />
                            <input type="button" value="Subsribe" class="subsribe" /></div>
                    </div>
                </div>
                <div class="mainbody-right-news-bottom">
                </div>
            </div>
            <!--mainbody-right-news ENDS HERE-->
        </div>
        <!--mainbody-right ENDS HERE-->
    </div>
    <!--mainbody ENDS HERE-->
    <!--resultbody START HERE-->
    <div class="resultbody">
        <!--found-results START HERE-->
        <div class="found-results">
            <div class="found">
                <%= GetKeyResult("WEFOUND")%> <span>
                    <asp:Label ID="lblResultCount" runat="server" Text=""></asp:Label></span> <%= GetKeyResult("RESULTS")%></div>
            <div class="sort">
                <%= GetKeyResult("SORTBY")%>:</div>
            <div class="price">
                <a href="#" class="pricelink"><%= GetKeyResult("PRICE")%></a></div>
            <div class="popularity">
                <a href="#"><%= GetKeyResult("POPULARITY")%></a></div>
            <div class="stars">
                <a href="#"><%= GetKeyResult("STARS")%></a></div>
            <div class="distance">
                <a href="#"><%= GetKeyResult("DISTANCETO")%></a> <span>Airport</span> <a href="#">
                    <img src="<%= SiteRootPath %>images/results-found-arrow.png" align="absmiddle" /></a></div>
            <div class="guest-re">
                <a href="#"><%= GetKeyResult("GUESTREVIEW")%></a></div>
        </div>
        <!--found-results ENDS HERE-->
        <!--pageing START HERE-->
        <%-- <div class="pageing">
            <a href="#" class="arrow-right select"></a>
            <div class="pageing-mid">
                <a href="#">Prevous</a> <a href="#" class="no">1</a><a href="#" class="no select">2</a><a
                    href="#" class="no">3</a> ... <a href="#" class="no">8</a> <a href="#">next</a></div>
            <a href="#" class="arrow-left"></a>
        </div>--%>
        <!--pageing ENDS HERE-->
        <div class="bodyfor-icon">
          <div class="bodyfor-icon-left">
          <i>More result can be found on request</i>
          </div>
          <div class="bodyfor-icon-right">
          <img src="<%= SiteRootPath %>images/map-green.jpg" align="absmiddle" /> Bookable <img src="<%= SiteRootPath %>images/map-red.jpg" align="absmiddle" /> Bookable with promotion  <img src="<%= SiteRootPath %>images/map-blue.jpg" align="absmiddle" /> Send Request
          </div>
          </div>
        <!--found-results-mainbody START HERE-->
        ,<div id="TabbedPanels2" class="TabbedPanels">
            <ul class="TabbedPanelsTabGroup">
                <li class="TabbedPanelsTab-green" tabindex="0"><%= GetKeyResult("BOOKONLINE")%></li>
                <li class="TabbedPanelsTab" tabindex="0"><%= GetKeyResult("SENDREQUEST")%></li>
            </ul>
            <div class="TabbedPanelsContentGroup">
                <div class="TabbedPanelsContent">
                    <div class="found-results-mainbody">
                        <asp:GridView runat="server" ID="grvBooking" AutoGenerateColumns="false" Width="100%"
                            AllowPaging="True" EmptyDataRowStyle-HorizontalAlign="Center" BorderWidth="0"
                            OnPageIndexChanging="grvBooking_PageIndexChanging" GridLines="None" CellPadding="0"
                            CellSpacing="0" ShowHeader="true" PageSize="10" OnRowDataBound="grvBooking_RowDataBound" EmptyDataText="For selected date and location no meeting facilities can be booked. Please select Request Tab."
                            OnRowCommand="grvBooking_RowCommand">
                            <%--<PagerStyle BackColor="White" ForeColor="White" HorizontalAlign="Right" Font-Overline="False"
                        Font-Strikeout="False" BorderWidth="0" />--%>
                             <EmptyDataTemplate>
                               <ul>
                                <li class="first">
                                 Search result not found.
                                </li> 
                                </ul>                    
                    </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>                                                                    
                                        <ul>
                                            <li class="first">
                                                <!--all-results START HERE-->
                                                <div class="all-results">
                                                    <!--all-results-left START HERE-->
                                                    <div class="all-results-left">
                                                        <!--all-results-left-left START HERE-->
                                                        <div class="all-results-left-left">
                                                            <asp:Image ID="imgHotel" runat="server" Height="100" Width="100" />
                                                        </div>
                                                        <!--all-results-left-left START HERE-->
                                                        <!--all-results-left-right END HERE-->
                                                        <div class="all-results-left-right">
                                                            <div class="star">
                                                                <asp:Image ID="imgStar" runat="server" />
                                                            </div>
                                                            <h3>
                                                                <asp:HiddenField ID="hdnHotelId" runat="server" Value='<%#Eval("HotelId") %>' />
                                                                <asp:Label ID="lblHotelName" runat="server" Text='<%#Eval("HotelName") %>'></asp:Label>
                                                            </h3>
                                                            <h4>
                                                                <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label></h4>
                                                            <p>
                                                                <asp:Label ID="lblDescription" runat="server" Text=""></asp:Label>
                                                            </p>
                                                            <div class="watch" id="divVideo" runat="server">
                                                                <img src="<%= SiteRootPath %>images/r-video-icon.png" align="absmiddle" />
                                                                <span>
                                                                    <asp:LinkButton ID="lnbVideo" runat="server" ForeColor="Black">Watch <%= GetKeyResult("VIDEO")%></asp:LinkButton>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <!--all-results-left-right END HERE-->
                                                    </div>
                                                    <!--all-results-left END HERE-->
                                                    <!--all-results-right START HERE-->
                                                    <div class="all-results-right">
                                                        <div class="results-top">
                                                            <div class="results-review">
                                                                <p>
                                                                    <%= GetKeyResult("REVIEW")%></p>
                                                                <span>N/A</span>
                                                            </div>
                                                            <div class="results-from-airport">
                                                                <p>
                                                                     <%= GetKeyResult("FROMAIRPORT")%></p>
                                                                <span>0.9 km</span>
                                                            </div>
                                                            <div class="results-price-dis" style="display: none" id="divDiscount" runat="server">
                                                                <div class="results-price-dis-left">
                                                                    <img src="<%= SiteRootPath %>images/euro-red.png" />
                                                                </div>
                                                                <div class="results-price-dis-right">
                                                                    <span class="euro">€</span> <span><span class="dis-bg">
                                                                        <asp:Label ID="lblDiscountPrice" runat="server" Text='<%#Eval("DDRPercent") %>'></asp:Label></span></span>
                                                                    <b><%= GetKeyResult("PPDAY")%></b>
                                                                    <p>
                                                                        From <span>€
                                                                            <asp:Label ID="lblActPkgPrice" runat="server" Text=""></asp:Label></span> <%= GetKeyResult("PPDAY")%></p>
                                                                    <%--<p>Discount <span class="red">-7%</span></p>--%>
                                                                </div>
                                                            </div>
                                                            <div class="results-price" style="display: none" id="divPkg" runat="server">
                                                                <span class="euro">€</span> <span>
                                                                    <asp:Label ID="lblActPAckagePrice" runat="server" Text=""></asp:Label></span>
                                                                <b><%= GetKeyResult("PPDAY")%></b>
                                                            </div>
                                                        </div>
                                                        <div class="results-bottom">
                                                            <asp:LinkButton ID="lnbBookOnline" runat="server" CommandName="Booking" CommandArgument='<%#Eval("HotelId") %>'
                                                                CssClass="book-online-btn"><%= GetKeyResult("BOOKONLINE")%></asp:LinkButton>
                                                            <span>or</span>
                                                            <asp:LinkButton ID="lnbRequestOnline" runat="server" CommandName="Request" CommandArgument='<%#Eval("HotelId") %>'
                                                                CssClass="send-request-btn"><%= GetKeyResult("SENDAREQUEST")%></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                    <!--all-results-right  END HERE-->
                                                </div>
                                                <!--all-results END HERE-->
                                                <!--all-results-bottom START HERE-->
                                                <div class="all-results-bottom">
                                                    <div class="all-results-bottom-left">
                                                        <div class="all-results-bottom-left-online">
                                                            <span>
                                                                <asp:Label ID="lblMeetingroomOnline" runat="server" Text=""></asp:Label></span>
                                                            online to book
                                                        </div>
                                                        <div class="all-results-bottom-left-icon">
                                                            <asp:HyperLink ID="hypClock" runat="server" Visible="false">
                                                                <asp:Image ID="imgClock" runat="server" ImageUrl="~/Uploads/FacilityIcon/bar-resto.png" class="select" ToolTip="Restaurant"/>
                                                            </asp:HyperLink>
                                                            <asp:HyperLink ID="hypDateIcon" runat="server" Visible="false">
                                                                <asp:Image ID="imgDateIcon" ImageUrl="~/Uploads/FacilityIcon/daylight-in-all-meeting-rooms.png" runat="server" ToolTip="All meeting rooms have natural daylight"/></asp:HyperLink>
                                                            <asp:HyperLink ID="hyplockicon" runat="server" Visible="false">
                                                                <asp:Image ID="Image1" ImageUrl="~/Uploads/FacilityIcon/meeting.png" runat="server"  ToolTip="Dedicated meeting coordinator"/></asp:HyperLink>
                                                            <asp:HyperLink ID="hypPrinticon" runat="server" Visible="false">
                                                                <asp:Image ID="Image2" ImageUrl="~/Uploads/FacilityIcon/private-parking.png" runat="server" ToolTip="Free parking"/></asp:HyperLink>
                                                            <asp:HyperLink ID="hypchticon" runat="server" Visible="false">
                                                                <asp:Image ID="Image3" ImageUrl="~/Uploads/FacilityIcon/wifi.png" runat="server" ToolTip="Complimenteary wireless internet" /></asp:HyperLink>                                                           
                                                        </div>
                                                        <div class="showmapdiv">
                                                        <div class="all-results-bottom-left-map" style="position:  relative;">                                                         
                                                            <a href="javascript:void(0);" id="showMap" >
                                                            <img src="<%= SiteRootPath %>images/map-icon.png" align="absmiddle" /></a><%= GetKeyResult("SHOWMAP")%>
                                                            <div class="classLat">
                                                            <asp:HiddenField ID="hidLat" runat="server" Value='<%#Eval("Latitude") %>' /></div>
                                                            <div class="classLong">
                                                            <asp:HiddenField ID="hidLong" runat="server" Value='<%#Eval("Longitude") %>'/></div>
                                                        </div>
                                                        <div id="maploc" style="background-color: #E5E3DF;border: 2px solid green;float: right;height: 250px;left: 100px;overflow: hidden;position: absolute;top: 0px;width: 400px;z-index: 1001;display:none;" ></div>
                                                        </div>
                                                    </div>
                                                    <div class="all-results-bottom-right">
                                                        <%--<a href="#" id="see_all">See all</a>  --%>
                                                        <asp:LinkButton ID="btnSeeAll" runat="server" CommandName="Popup" CommandArgument='<%#Eval("HotelId") %>'>See all</asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div class="booking" id="divBmeeting" runat="server" style="display: none">
                                                    <!--booking-online START HERE-->
                                                    <div class="booking-online" id="divBookingMeeting" runat="server">
                                                        <h2>
                                                            Book online <span class="h2arrow">&nbsp;</span></h2>
                                                        <!--booking-online-body START HERE-->
                                                        <div class="booking-online-body">
                                                            <div class="shape">
                                                                <ul>
                                                                    <li class="dark">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Shape 1
                                                                        <br>
                                                                        <img src="<%= SiteRootPath %>images/theatre.gif" /></li>
                                                                    <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Shape 2
                                                                        <br>
                                                                        <img src="<%= SiteRootPath %>images/classroom.gif" /></li>
                                                                    <li class="dark">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Shape 3
                                                                        <br>
                                                                        <img src="<%= SiteRootPath %>images/ushape.gif" /></li>
                                                                    <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Shape 4
                                                                        <br>
                                                                        <img src="<%= SiteRootPath %>images/boardroom.gif" /></li>
                                                                    <li class="dark">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Shape 5<br>
                                                                        <img src="<%= SiteRootPath %>images/cocktail.gif" /></li>
                                                                </ul>
                                                            </div>
                                                            <!--Bind Booking for meeting room-->
                                                            <div class="booking-online-body-inner">
                                                            <asp:GridView runat="server" ID="grvBookMeeting" AutoGenerateColumns="false" Width="100%"
                                                                    BorderWidth="0" GridLines="None" CellPadding="0" CellSpacing="0" ShowHeader="false"
                                                                    OnRowDataBound="grvBookMeeting_RowDataBound">
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <ul>
                                                                                    <!--meeting room START HERE-->
                                                                                    <li>
                                                                                        <div class="booking-online-body-inner-left">
                                                                                            <asp:Image ID="imgHotelImage" runat="server" Width="69" Height="69" />
                                                                                        </div>
                                                                                        <div class="booking-online-body-inner-right">
                                                                                            <div class="booking-online-body-inner-right-top">
                                                                                                <asp:HiddenField ID="hdnMId" runat="server" Value='<%#Eval("MeetingRoomId") %>' />
                                                                                                <asp:Label ID="lblMeetingRoomName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                                                                            </div>
                                                                                            <div class="booking-online-body-inner-right-bottom">
                                                                                                <ul>
                                                                                                    <li class="firstli">
                                                                                                        <div class="firstli-inner">
                                                                                                            <img src="<%= SiteRootPath %>images/book-arrow1.png" align="absmiddle" /><asp:Label ID="lblRoomSize"
                                                                                                                runat="server" Text='<%#Eval("Surface") %>'></asp:Label></div>
                                                                                                        <div class="firstli-inner">
                                                                                                            <img src="<%= SiteRootPath %>images/book-arrow2.png" align="absmiddle" /><asp:Label ID="lblHeight" runat="server"
                                                                                                                Text='<%#Eval("Height") %>'></asp:Label></div>
                                                                                                        <div class="firstli-inner1">
                                                                                                            <asp:HyperLink ID="linkviewPlan" runat="server" Target="_blank"><%= GetKeyResult("FLOORPLAN")%>
                                                                                                            </asp:HyperLink></div>
                                                                                                    </li>
                                                                                                    <li class="dark">
                                                                                                        <div class="meetingroom">
                                                                                                            <p>
                                                                                                              <%= GetKeyResult("THEATRE")%> </p>
                                                                                                            <p>
                                                                                                                <asp:Label ID="lblTheatreMin" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax"
                                                                                                                    runat="server" Text=""></asp:Label></p>
                                                                                                        </div>
                                                                                                        <asp:CheckBox ID="chkTheatre" runat="server" class="styledcheckbox"/>
                                                                                                    </li>
                                                                                                    <li>
                                                                                                        <div class="meetingroom">
                                                                                                            <p>
                                                                                                              School</p>
                                                                                                            <p>
                                                                                                                <asp:Label ID="lblReceptionMin" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </p>
                                                                                                        </div>
                                                                                                        <asp:CheckBox ID="chkSchool" runat="server" class="styledcheckbox" />
                                                                                                    </li>
                                                                                                    <li class="dark">
                                                                                                        <div class="meetingroom">
                                                                                                            <p>
                                                                                                             <%= GetKeyResult("USHAPE")%> </p>
                                                                                                            <p>
                                                                                                                <asp:Label ID="lblUShapeMin" runat="server" Text=""></asp:Label>
                                                                                                                <asp:Label ID="lblUShapeMax" runat="server" Text=""></asp:Label></p>
                                                                                                        </div>
                                                                                                        <asp:CheckBox ID="chkUshape" runat="server" class="styledcheckbox" />
                                                                                                    </li>
                                                                                                    <li>
                                                                                                        <div class="meetingroom">
                                                                                                            <p>
                                                                                                              Boardroom</p>
                                                                                                            <p>
                                                                                                                <asp:Label ID="lblBanquetMin" runat="server" Text=""></asp:Label>
                                                                                                                <asp:Label ID="lblBanquetMax" runat="server" Text=""></asp:Label></p>
                                                                                                        </div>
                                                                                                        <asp:CheckBox ID="chkBoardroom" runat="server" class="styledcheckbox"/>
                                                                                                    </li>
                                                                                                    <li class="dark">
                                                                                                        <div class="meetingroom">
                                                                                                            <p>
                                                                                                              <%= GetKeyResult("COCKTAIL")%> </p>
                                                                                                            <p>
                                                                                                                <asp:Label ID="lblCocktailMin" runat="server" Text=""></asp:Label>
                                                                                                                <asp:Label ID="lblCocktailMax" runat="server" Text=""></asp:Label></p>
                                                                                                        </div>
                                                                                                        <asp:CheckBox ID="chkCocktail" runat="server" class="styledcheckbox"/>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                    </li>
                                                                                    <!--meeting room END HERE-->
                                                                                </ul>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                            <!--Bind booking for Secondary meeting room-->
                                                            <div class="booking-online-body-inner">
                                                                <asp:Repeater ID="rptSecondaryMeeting" runat="server" OnItemDataBound="rptSecondaryMeeting_ItemDataBound">                                                                
                                                                    <ItemTemplate>
                                                                        <ul>
                                                                            <!--meeting room START HERE-->
                                                                            <li>
                                                                                <div class="booking-online-body-inner-left">
                                                                                    <asp:Image ID="imgHotelImage" runat="server" Width="69" Height="69" />
                                                                                    <div class="booking-online-body-inner-left-text">
                                                                                        <asp:HyperLink ID="linkviewPlan" runat="server" Target="_blank"><%= GetKeyResult("FLOORPLAN")%></asp:HyperLink></div>
                                                                                </div>
                                                                                <div class="booking-online-body-inner-right" id="divRptId" runat="server">
                                                                                    <div class="booking-online-body-inner-right-top">
                                                                                        <asp:HiddenField ID="hdnMId" runat="server" Value='<%#Eval("MeetingRoomId") %>' />
                                                                                        <asp:Label ID="lblMeetingRoomName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                                                                    </div>
                                                                                    <div class="booking-online-body-inner-right-top2">
                                                                                        <div class="booking-online-body-inner-right-top1-left">
                                                                                            <asp:Label ID="lblDate" runat="server" Text=""></asp:Label>
                                                                                        </div>                                                                                      
                                                                                    </div>
                                                                                    <div class="booking-online-body-inner-right-bottom1">
                                                                                        <ul>
                                                                                            <li class="processfirstli">Full Day </li>
                                                                                            <li class="processdark">
                                                                                                <asp:CheckBox ID="chkTheatre2" runat="server" />
                                                                                                <asp:Label ID="lblTheatreMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax2"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                            <li class="process">
                                                                                                <asp:CheckBox ID="chkSchool2" runat="server" />
                                                                                                <asp:Label ID="lblReceptionMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax2"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                            <li class="processdark">
                                                                                                <asp:CheckBox ID="chkUshape2" runat="server" />
                                                                                                <asp:Label ID="lblUShapeMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblUShapeMax2"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                            <li class="process">
                                                                                                <asp:CheckBox ID="chkBoardroom2" runat="server" />
                                                                                                <asp:Label ID="lblBanquetMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblBanquetMax2"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                            <li class="processdark">
                                                                                                <asp:CheckBox ID="chkCocktail2" runat="server" />
                                                                                                <asp:Label ID="lblCocktailMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblCocktailMax2"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                    <div class="booking-online-body-inner-right-bottom1">
                                                                                        <ul>
                                                                                            <li class="processfirstli">Morning </li>
                                                                                            <li class="processdark">
                                                                                                <asp:CheckBox ID="chkTheatre" runat="server" />
                                                                                                <asp:Label ID="lblTheatreMin" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                            <li class="process">
                                                                                                <asp:CheckBox ID="chkSchool" runat="server" />
                                                                                                <asp:Label ID="lblReceptionMin" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                            <li class="processdark">
                                                                                                <asp:CheckBox ID="chkUshape" runat="server" />
                                                                                                <asp:Label ID="lblUShapeMin" runat="server" Text=""></asp:Label><asp:Label ID="lblUShapeMax"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                            <li class="process">
                                                                                                <asp:CheckBox ID="chkBoardroom" runat="server" />
                                                                                                <asp:Label ID="lblBanquetMin" runat="server" Text=""></asp:Label><asp:Label ID="lblBanquetMax"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                            <li class="processdark">
                                                                                                <asp:CheckBox ID="chkCocktail" runat="server" />
                                                                                                <asp:Label ID="lblCocktailMin" runat="server" Text=""></asp:Label><asp:Label ID="lblCocktailMax"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                    <div class="booking-online-body-inner-right-bottom1">
                                                                                        <ul>
                                                                                            <li class="processfirstli">Afternoon </li>
                                                                                            <li class="processdark">
                                                                                                <asp:CheckBox ID="chkTheatre1" runat="server" />
                                                                                                <asp:Label ID="lblTheatreMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax1"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                            <li class="process">
                                                                                                <asp:CheckBox ID="chkSchool1" runat="server" />
                                                                                                <asp:Label ID="lblReceptionMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax1"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                            <li class="processdark">
                                                                                                <asp:CheckBox ID="chkUshape1" runat="server" />
                                                                                                <asp:Label ID="lblUShapeMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblUShapeMax1"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                            <li class="process">
                                                                                                <asp:CheckBox ID="chkBoardroom1" runat="server" />
                                                                                                <asp:Label ID="lblBanquetMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblBanquetMax1"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                            <li class="processdark">
                                                                                                <asp:CheckBox ID="chkCocktail1" runat="server" />
                                                                                                <asp:Label ID="lblCocktailMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblCocktailMax1"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <!--meeting room END HERE-->
                                                                        </ul>
                                                                    </ItemTemplate>
                                                                    <AlternatingItemTemplate>
                                                                        <ul>
                                                                            <!--meeting room START HERE-->
                                                                            <li>
                                                                                <div class="booking-online-body-inner-left">
                                                                                    <%--<asp:Image ID="imgHotelImage" runat="server" Width="69" Height="69" />
                                                                                    <div class="booking-online-body-inner-left-text">
                                                                                        <asp:HyperLink ID="linkviewPlan" runat="server" Target="_blank" Text="Open FloorPlan"></asp:HyperLink></div>--%>
                                                                                </div>
                                                                                <div class="booking-online-body-inner-right" id="divRptId" runat="server">
                                                                                    <div class="booking-online-body-inner-right-top">
                                                                                        <asp:HiddenField ID="hdnMId" runat="server" Value='<%#Eval("MeetingRoomId") %>' />
                                                                                        <%--<asp:Label ID="lblMeetingRoomName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>--%>
                                                                                    </div>
                                                                                    <div class="booking-online-body-inner-right-top2">
                                                                                        <div class="booking-online-body-inner-right-top1-left">
                                                                                            <asp:Label ID="lblDate" runat="server" Text=""></asp:Label>
                                                                                        </div>                                                                                        
                                                                                    </div>
                                                                                    <div class="booking-online-body-inner-right-bottom1">
                                                                                        <ul>
                                                                                            <li class="processfirstli">FullDay </li>
                                                                                            <li class="processdark">
                                                                                                <asp:CheckBox ID="chkTheatre2" runat="server" />
                                                                                                <asp:Label ID="lblTheatreMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax2"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                            <li class="process">
                                                                                                <asp:CheckBox ID="chkSchool2" runat="server" />
                                                                                                <asp:Label ID="lblReceptionMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax2"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                            <li class="processdark">
                                                                                                <asp:CheckBox ID="chkUshape2" runat="server" />
                                                                                                <asp:Label ID="lblUShapeMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblUShapeMax2"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                            <li class="process">
                                                                                                <asp:CheckBox ID="chkBoardroom2" runat="server" />
                                                                                                <asp:Label ID="lblBanquetMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblBanquetMax2"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                            <li class="processdark">
                                                                                                <asp:CheckBox ID="chkCocktail2" runat="server" />
                                                                                                <asp:Label ID="lblCocktailMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblCocktailMax2"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                    <div class="booking-online-body-inner-right-bottom1">
                                                                                        <ul>
                                                                                            <li class="processfirstli">Morning </li>
                                                                                            <li class="processdark">
                                                                                                <asp:CheckBox ID="chkTheatre" runat="server" />
                                                                                                <asp:Label ID="lblTheatreMin" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                            <li class="process">
                                                                                                <asp:CheckBox ID="chkSchool" runat="server" />
                                                                                                <asp:Label ID="lblReceptionMin" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                            <li class="processdark">
                                                                                                <asp:CheckBox ID="chkUshape" runat="server" />
                                                                                                <asp:Label ID="lblUShapeMin" runat="server" Text=""></asp:Label><asp:Label ID="lblUShapeMax"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                            <li class="process">
                                                                                                <asp:CheckBox ID="chkBoardroom" runat="server" />
                                                                                                <asp:Label ID="lblBanquetMin" runat="server" Text=""></asp:Label><asp:Label ID="lblBanquetMax"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                            <li class="processdark">
                                                                                                <asp:CheckBox ID="chkCocktail" runat="server" />
                                                                                                <asp:Label ID="lblCocktailMin" runat="server" Text=""></asp:Label><asp:Label ID="lblCocktailMax"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                    <div class="booking-online-body-inner-right-bottom1">
                                                                                        <ul>
                                                                                            <li class="processfirstli">Afternoon </li>
                                                                                            <li class="processdark">
                                                                                                <asp:CheckBox ID="chkTheatre1" runat="server" />
                                                                                                <asp:Label ID="lblTheatreMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax1"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                            <li class="process">
                                                                                                <asp:CheckBox ID="chkSchool1" runat="server" />
                                                                                                <asp:Label ID="lblReceptionMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax1"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                            <li class="processdark">
                                                                                                <asp:CheckBox ID="chkUshape1" runat="server" />
                                                                                                <asp:Label ID="lblUShapeMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblUShapeMax1"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                            <li class="process">
                                                                                                <asp:CheckBox ID="chkBoardroom1" runat="server" />
                                                                                                <asp:Label ID="lblBanquetMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblBanquetMax1"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                            <li class="processdark">
                                                                                                <asp:CheckBox ID="chkCocktail1" runat="server" />
                                                                                                <asp:Label ID="lblCocktailMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblCocktailMax1"
                                                                                                    runat="server" Text=""></asp:Label>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <!--meeting room END HERE-->
                                                                        </ul>
                                                                    </AlternatingItemTemplate>
                                                                </asp:Repeater>
                                                            </div>
                                                        </div>
                                                        <!--booking-online-body END HERE-->
                                                        <!--booking-online-btn START HERE-->
                                                        <div class="booking-online-btn">
                                                            <asp:LinkButton ID="lnkAddtoShopping" runat="server" class="add-shopping-cart-btn"
                                                                OnClientClick="return AddToShopping();" OnClick="lnkAddtoShopping_Click">Add to shopping cart</asp:LinkButton>
                                                            or
                                                            <asp:LinkButton ID="lnkCancel" runat="server" OnClientClick="return CloseBookingDiv();"><%= GetKeyResult("CANCEL")%></asp:LinkButton>
                                                        </div>
                                                        <!--booking-online-btn END HERE-->
                                                    </div>
                                                    <!--booking-online END HERE-->
                                                    <!--booking-request START HERE-->
                                                    <div class="booking-request" id="divRequest" runat="server">
                                                        <h2>
                                                            Book on request
                                                        </h2>
                                                        <!--booking-request-body START HERE-->
                                                        <div class="booking-request-body">
                                                            <div class="shape">
                                                                <ul>
                                                                    <li class="dark">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Shape 1
                                                                        <br>
                                                                        <img src="<%= SiteRootPath %>images/theatre.gif" /></li>
                                                                    <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Shape 2
                                                                        <br>
                                                                        <img src="<%= SiteRootPath %>images/classroom.gif" /></li>
                                                                    <li class="dark">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Shape 3
                                                                        <br>
                                                                        <img src="<%= SiteRootPath %>images/ushape.gif" /></li>
                                                                    <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Shape 4
                                                                        <br>
                                                                        <img src="<%= SiteRootPath %>images/boardroom.gif" /></li>
                                                                    <li class="dark">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Shape 5<br>
                                                                        <img src="<%= SiteRootPath %>images/cocktail.gif" /></li>
                                                                </ul>
                                                            </div>
                                                            <!--booking-request-body-inner END HERE-->
                                                            <asp:GridView runat="server" ID="grvRequestMeeting" AutoGenerateColumns="false" Width="100%"
                                                                BorderWidth="0" GridLines="None" CellPadding="0" CellSpacing="0" ShowHeader="false"
                                                                OnRowDataBound="grvRequestMeeting_RowDataBound">
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <ul>
                                                                                <!--meeting room START HERE-->
                                                                                <li>
                                                                                    <div class="booking-online-body-inner-left">
                                                                                        <asp:Image ID="imgHotelImage" runat="server" Width="57" Height="57" />
                                                                                    </div>
                                                                                    <div class="booking-online-body-inner-right">
                                                                                        <div class="booking-online-body-inner-right-top">
                                                                                            <asp:HiddenField ID="hdnMId" runat="server" Value='<%#Eval("MeetingRoomId") %>' />
                                                                                            <asp:Label ID="lblMeetingRoomName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                                                                        </div>
                                                                                        <div class="booking-online-body-inner-right-bottom">
                                                                                            <ul>
                                                                                                <li class="firstli">
                                                                                                    <div class="firstli-inner">
                                                                                                        <img src="<%= SiteRootPath %>images/book-arrow1.png" align="absmiddle" /><asp:Label ID="lblRoomSize"
                                                                                                            runat="server" Text='<%#Eval("Surface") %>'></asp:Label></div>
                                                                                                    <div class="firstli-inner">
                                                                                                        <img src="<%= SiteRootPath %>images/book-arrow2.png" align="absmiddle" /><asp:Label ID="lblHeight" runat="server"
                                                                                                            Text='<%#Eval("Height") %>'></asp:Label></div>
                                                                                                    <div class="firstli-inner1">
                                                                                                        <asp:HyperLink ID="linkviewPlan" runat="server" Target="_blank"><%= GetKeyResult("FLOORPLAN")%>
                                                                                                        </asp:HyperLink></div>
                                                                                                </li>
                                                                                                <li class="dark">
                                                                                                    <div class="meetingroom">
                                                                                                        <p>
                                                                                                            <%= GetKeyResult("THEATRE")%></p>
                                                                                                        <p>
                                                                                                            <asp:Label ID="lblTheatreMin" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax"
                                                                                                                runat="server" Text=""></asp:Label></p>
                                                                                                    </div>
                                                                                                    <asp:CheckBox ID="chkTheatre" runat="server" class="styledcheckbox"/>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <div class="meetingroom">
                                                                                                        <p>
                                                                                                           <%= GetKeyResult("SCHOOL")%></p>
                                                                                                        <p>
                                                                                                            <asp:Label ID="lblReceptionMin" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                    <asp:CheckBox ID="chkSchool" runat="server" class="styledcheckbox"/>
                                                                                                </li>
                                                                                                <li class="dark">
                                                                                                    <div class="meetingroom">
                                                                                                        <p>
                                                                                                            <%= GetKeyResult("USHAPE")%></p>
                                                                                                        <p>
                                                                                                            <asp:Label ID="lblUShapeMin" runat="server" Text=""></asp:Label>
                                                                                                            <asp:Label ID="lblUShapeMax" runat="server" Text=""></asp:Label></p>
                                                                                                    </div>
                                                                                                    <asp:CheckBox ID="chkUshape" runat="server" class="styledcheckbox"/>
                                                                                                </li>
                                                                                                <li>
                                                                                                    <div class="meetingroom">
                                                                                                        <p>
                                                                                                            <%= GetKeyResult("BOARDROOM")%></p>
                                                                                                        <p>
                                                                                                            <asp:Label ID="lblBanquetMin" runat="server" Text=""></asp:Label>
                                                                                                            <asp:Label ID="lblBanquetMax" runat="server" Text=""></asp:Label></p>
                                                                                                    </div>
                                                                                                    <asp:CheckBox ID="chkBoardroom" runat="server" class="styledcheckbox"/>
                                                                                                </li>
                                                                                                <li class="dark">
                                                                                                    <div class="meetingroom">
                                                                                                        <p>
                                                                                                            <%= GetKeyResult("COCKTAIL")%></p>
                                                                                                        <p>
                                                                                                            <asp:Label ID="lblCocktailMin" runat="server" Text=""></asp:Label>
                                                                                                            <asp:Label ID="lblCocktailMax" runat="server" Text=""></asp:Label></p>
                                                                                                    </div>
                                                                                                    <asp:CheckBox ID="chkCocktail" runat="server" class="styledcheckbox"/>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                                <!--meeting room END HERE-->
                                                                            </ul>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                            <!--booking-request-body-inner END HERE-->
                                                        </div>
                                                        <!--booking-request-body END HERE-->
                                                        <!--booking-request-btn START HERE-->
                                                        <div class="booking-request-btn">
                                                            <div class="booking-request-btn-basket">
                                                                <a href="#" class="add-basket-btn"> <%= GetKeyResult("ADDTOBASKET")%></a> or  <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="return CloseDiv();"><%= GetKeyResult("CANCEL")%></asp:LinkButton>
                                                            </div>
                                                            <div class="booking-request-btn-close">
                                                                <asp:LinkButton ID="lnbClose" runat="server" OnClientClick="return CloseRequestDiv();"><img src="<%= SiteRootPath %>images/close-btn.png"/></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                        <!--booking-request-btn END HERE-->
                                                    </div>
                                                    <!--booking-request END HERE-->
                                                </div>
                                                <!--all-results-bottom END HERE-->
                                            </li>
                                        </ul>                                        
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle HorizontalAlign="Center"></EmptyDataRowStyle>
                            <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                            <PagerStyle HorizontalAlign="Right" BackColor="White" />
                            <PagerTemplate>
                                <tr>
                                    <td colspan="11" bgcolor="#ffffff">
                                        <div style="float: right; width: 200px;">
                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                        </div>
                                    </td>
                                </tr>
                            </PagerTemplate>
                            <%--<PagerTemplate>
                        <div id="Paging" style="width: 100%; display: none;">
                            <table cellpadding="0" align="right">
                                <tr>
                                    <td style="vertical-align=middle; height: 22px;">
                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </PagerTemplate>--%>
                        </asp:GridView>
                    </div>
                </div>
                <div class="TabbedPanelsContent">
                    <div class="TabbedPanelsContent">
                        <div class="found-results-mainbody">
                            <asp:GridView runat="server" ID="grvRequest" AutoGenerateColumns="false" Width="100%"
                                AllowPaging="True" EmptyDataRowStyle-HorizontalAlign="Center" BorderWidth="0"
                                GridLines="None" CellPadding="0" CellSpacing="0" ShowHeader="false">
                                <%--<PagerStyle BackColor="White" ForeColor="White" HorizontalAlign="Right" Font-Overline="False"
                        Font-Strikeout="False" BorderWidth="0" />--%>
                                <%-- <EmptyDataTemplate>
                        <div class="booking-details" style="width: 960px;" id="divLastDetails" runat="server">
                            <ul>
                                <li class="value10">
                                    <div class="col21" style="width: 960px;">
                                        <div class="button_section">
                                            Search result not found.
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </EmptyDataTemplate>--%>
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <ul>
                                                <li class="first">
                                                    <!--all-results START HERE-->
                                                    <div class="all-results">
                                                        <!--all-results-left START HERE-->
                                                        <div class="all-results-left">
                                                            <!--all-results-left-left START HERE-->
                                                            <div class="all-results-left-left">
                                                                <img src="<%= SiteRootPath %>images/r-img1.png" />
                                                            </div>
                                                            <!--all-results-left-left START HERE-->
                                                            <!--all-results-left-right END HERE-->
                                                            <div class="all-results-left-right">
                                                                <div class="star">
                                                                    <img src="<%= SiteRootPath %>images/r-star.png" />
                                                                </div>
                                                                <h3>
                                                                    <asp:Label ID="lblHotelName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                                                </h3>
                                                                <h4>
                                                                    Rue de Laffayete 132, 1000 Bruxelles - Belgium</h4>
                                                                <p>
                                                                    Aenean porta, tellus vel accumsan fermentum, diam arcu condimentum purus, hendrerit
                                                                    posuere.
                                                                </p>
                                                                <div class="watch">
                                                                    <img src="<%= SiteRootPath %>images/r-video-icon.png" align="absmiddle" />
                                                                    <span>Watch  <%= GetKeyResult("VIDEO")%></span>
                                                                </div>
                                                            </div>
                                                            <!--all-results-left-right END HERE-->
                                                        </div>
                                                        <!--all-results-left END HERE-->
                                                        <!--all-results-right START HERE-->
                                                        <div class="all-results-right">
                                                            <div class="results-top">
                                                                <div class="results-review">
                                                                    <p>
                                                                        <%= GetKeyResult("REVIEW")%></p>
                                                                    <span>700,800</span>
                                                                </div>
                                                                <div class="results-from-airport">
                                                                    <p>
                                                                         <%= GetKeyResult("FROMAIRPORT")%></p>
                                                                    <span>0,9000 km</span>
                                                                </div>
                                                                <div class="results-price-dis">
                                                                    <div class="results-price-dis-left">
                                                                        <img src="<%= SiteRootPath %>images/euro-red.png" />
                                                                    </div>
                                                                    <div class="results-price-dis-right">
                                                                        <span class="euro">€</span> <span><span class="dis-bg">400</span></span> <b><%= GetKeyResult("PPDAY")%></b>
                                                                        <p>
                                                                            From <span>€ 2900</span> <%= GetKeyResult("PPDAY")%></p>
                                                                        <%--<p>
                                                                            Discount <span class="red">-100%</span></p>--%>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="results-bottom" style="text-align: right;">
                                                                <a href="#" class="send-request-btn" style="margin-right: 13px;">Send a request</a>
                                                            </div>
                                                        </div>
                                                        <!--all-results-right  END HERE-->
                                                    </div>
                                                    <!--all-results END HERE-->
                                                    <!--all-results-bottom START HERE-->
                                                    <div class="all-results-bottom">
                                                        <div class="all-results-bottom-left">
                                                            <div class="all-results-bottom-left-online">
                                                                <span>2</span> online to book
                                                            </div>
                                                            <div class="all-results-bottom-left-icon">
                                                                <a href="#">
                                                                    <img src="<%= SiteRootPath %>images/watch-icon.png" class="select" /></a><a href="#"><img src="<%= SiteRootPath %>images/date-icon.png" /></a><a
                                                                        href="#"><img src="<%= SiteRootPath %>images/lock-icon.png" /></a><a href="#"><img src="<%= SiteRootPath %>images/print-icon.png" /></a><a
                                                                            href="#"><img src="<%= SiteRootPath %>images/chhoot-icon.png" /></a><a href="#"><img src="<%= SiteRootPath %>images/last-icon.png" /></a>
                                                            </div>
                                                            <div class="all-results-bottom-left-map">
                                                                <a href="#">
                                                                    <img src="<%= SiteRootPath %>images/map-icon.png" align="absmiddle" /></a> Show map
                                                            </div>
                                                        </div>
                                                        <div class="all-results-bottom-right">
                                                            <a href="#"><%= GetKeyResult("SEEALL")%></a></div>
                                                    </div>
                                                    <!--all-results-bottom END HERE-->
                                                </li>
                                            </ul>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Center"></EmptyDataRowStyle>
                                <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                                <PagerStyle HorizontalAlign="Right" BackColor="White" />
                                <%--<PagerTemplate>
                        <div id="Paging" style="width: 100%; display: none;">
                            <table cellpadding="0" align="right">
                                <tr>
                                    <td style="vertical-align=middle; height: 22px;">
                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </PagerTemplate>--%>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="divJoinToday-overlay">
        </div>
        <div id="divJoinToday">
            <div class="popup-top">
            </div>
            <div class="popup-mid">
                <div class="popup-mid-inner">
                    <table cellspacing="10" style="margin-left: 20px">
                        <tr>
                            <td>
                                Do you want a secondary <%= GetKeyResult("MEETINGROOM")%> ?
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RadioButton ID="rbYes" GroupName="checkout" runat="server" />
                                &nbsp;&nbsp; Yes, I need for :&nbsp;&nbsp;
                                <asp:TextBox ID="txtSecparticipants" runat="server" CssClass="input-white-small"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtSecparticipants" FilterType="Numbers"></asp:FilteredTextBoxExtender>
                                &nbsp;&nbsp;People
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RadioButton ID="rbNo" GroupName="checkout" runat="server" Checked="true" />
                                &nbsp;&nbsp; No, I don't need another <%= GetKeyResult("MEETINGROOM")%>
                            </td>
                        </tr>
                    </table>
                    <div class="subscribe-btn1">
                        <div class="save-cancel-btn1 button_section">
                            <asp:LinkButton ID="btnContinue" runat="server" CssClass="send-request-btn" OnClick="btnContinue_Click">Continue</asp:LinkButton>
                            &nbsp;&nbsp; &nbsp;&nbsp; <a title="Close" class="Close-btn" href="#" onclick="document.getElementsByTagName('html')[0].style.overflow='auto';document.getElementById('divJoinToday').style.display='none';document.getElementById('divJoinToday-overlay').style.display='none';">
                                <%= GetKeyResult("CANCEL")%></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="popup-bottom">
            </div>
        </div>
        <script type="text/javascript">
            var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels2");
        </script>
        <!--found-results-mainbody ENDS HERE-->
        <!--pageing START HERE-->
        <%--   <div class="pageing">
            <a href="#" class="arrow-right select"></a>
            <div class="pageing-mid">
                <a href="#">Prevous</a> <a href="#" class="no">1</a><a href="#" class="no select">2</a><a
                    href="#" class="no">3</a> ... <a href="#" class="no">8</a> <a href="#">next</a></div>
            <a href="#" class="arrow-left"></a>
        </div>--%>
        <!--pageing ENDS HERE-->
    </div>
    <script type="text/javascript" language="javascript">

        function Navigate(Id) {

            var url = 'Video.aspx?HotelId=' + Id;
            var winName = 'myWindow';
            var w = '500';
            var h = '300';
            var scroll = 'no';
            var popupWindow = null;
            LeftPosition = (screen.width) ? (screen.width - w) / 2 : 0;
            TopPosition = (screen.height) ? (screen.height - h) / 2 : 0;
            settings = 'scrollbars=no,directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no, height=' + h + ',width=' + w + ',top=' + TopPosition + ',left=' + LeftPosition + ''
            popupWindow = window.open(url, winName, settings)
            return false;
        }

        function NavigateFacility(Id) {

            var url = 'FacilitySearch.aspx?HotelId=' + Id;
            var winName = 'myWindow';
            var w = '700';
            var h = '500';
            var scroll = 'no';
            var popupWindow = null;
            LeftPosition = (screen.width) ? (screen.width - w) / 2 : 0;
            TopPosition = (screen.height) ? (screen.height - h) / 2 : 0;
            settings = 'scrollbars=yes,directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no, height=' + h + ',width=' + w + ',top=' + TopPosition + ',left=' + LeftPosition + ''
            popupWindow = window.open(url, winName, settings)
            return false;
        }

        function CheckOne(obj, meetingroomID, configuretionID) {

            if (obj.checked == true) {                
                jQuery('#<%=hdnMeetingRoomId.ClientID %>').val(meetingroomID);
                jQuery('#<%=hdnConfigurationId.ClientID %>').val(configuretionID);
               
                jQuery("#cntMainBody_cntMainbody_grvBooking_grvBookMeeting_0 input:checkbox:checked").attr("checked", "");
                //jQuery("input[type=checkbox]","#cntMainBody_cntMainbody_grvBooking_grvBookMeeting_0").attr("checked","");
                obj.checked = true;
            }
            else {
                jQuery("#cntMainBody_cntMainbody_grvBooking_grvBookMeeting_0 input:checkbox:checked").attr("checked", "");

                jQuery('#<%=hdnMeetingRoomId.ClientID %>').val(0);
                jQuery('#<%=hdnConfigurationId.ClientID %>').val(0);
            }

        }

        function CheckSecondary(obj, meetingroomID, configuretionID, Days, RptId, ColId) {

            if (obj.checked == true) {                
                jQuery('#<%=hdnMeetingRoomId.ClientID %>').val(meetingroomID);
                jQuery('#<%=hdnConfigurationId.ClientID %>').val(configuretionID);
                jQuery('#<%=hdnType.ClientID %>').val(Days);
                jQuery('#<%=hdnColId.ClientID %>').val(ColId);                
                //alert(jQuery("ul li").find("#" + obj.id).index());         
                jQuery("#" + RptId + " input:checkbox").attr("checked", false);                
                //jQuery("#" + obj.id).attr("checked", true);                                            
                jQuery("#cntMainBody_cntMainbody_grvBooking_rptSecondaryMeeting_0 input:checkbox:checked").attr("checked", "");
                obj.checked = true;
                if (jQuery("#cntMainBody_cntMainbody_grvBooking_rptSecondaryMeeting_0_divRptId_1") != undefined) {
                    jQuery("#cntMainBody_cntMainbody_grvBooking_rptSecondaryMeeting_0_divRptId_1").find("li input:checkbox").attr("disabled", true);
                    jQuery("#cntMainBody_cntMainbody_grvBooking_rptSecondaryMeeting_0_divRptId_1").find("li input:checkbox").attr("checked", false);
                    jQuery("#cntMainBody_cntMainbody_grvBooking_rptSecondaryMeeting_0_divRptId_1").find("li:nth-child("+ColId+") input:checkbox").attr("disabled", false);
                }
                jQuery("#" + obj.id).attr("checked", true); 
            }
            else {
                jQuery("#cntMainBody_cntMainbody_grvBooking_rptSecondaryMeeting_0 input:checkbox:checked").attr("checked", "");
                jQuery("#" + RptId + " input:checkbox").attr("checked", false);                
                jQuery('#<%=hdnMeetingRoomId.ClientID %>').val(0);
                jQuery('#<%=hdnConfigurationId.ClientID %>').val(0);
                jQuery('#<%=hdnType.ClientID %>').val();
                jQuery('#<%=hdnColId.ClientID %>').val();
            }

        }



        function CloseDiv() {
            jQuery('.booking').hide();
            //jQuery('<%=Session["IsSecondary"] %>').val="";
            return false;
        }
        function CloseBookingDiv() {
            jQuery('.booking-online').hide();            
            return false;
        }
        function CloseRequestDiv() {
            jQuery('.booking-request').hide();
            return false;
        }

        function AddToShopping() {

            if (jQuery('#<%=hdnMeetingRoomId.ClientID %>').val() != 0 && jQuery('#<%=hdnConfigurationId.ClientID %>').val() != 0) {                
                if ('<%=Session["IsSecondary"] %>' == "NO" || '<%=Session["IsSecondary"] %>' == "") {
                    jQuery("body").scrollTop(0);
                    jQuery("html").scrollTop(0);
                    document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                    jQuery("#divJoinToday").show();
                    jQuery("#divJoinToday-overlay").show();
                    document.getElementById('<%=txtSecparticipants.ClientID %>').value = "";
                    return false;
                }
                else {                    
                    jQuery('#<%=rbYes.ClientID %>').attr("checked", true);                    
                    return true;
                }
            }
            else {
                alert('<%= GetKeyResultForJavaScript("PLEASESELECTURMEETINGROOM")%> !');
                return false;
            }

            
        }
    </script>
    <script type="text/javascript" language="javascript">
        jQuery("#<%= btnContinue.ClientID %>").bind("click", function () {
            if (document.getElementById('<%=rbYes.ClientID %>').checked == true) {
                if (document.getElementById('<%=txtSecparticipants.ClientID %>').value.length <= 0) {
                    alert('<%= GetKeyResultForJavaScript("YOUMUSTENTERTHENUMBEROFPARTICIPENTS")%>.'); 
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                return true;
            }
        });
</script>
<script language="javascript" type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(".showmapdiv #showMap").toggle(function () {
            var lat = jQuery(this).parent().children(".classLat").children("input:hidden").val();
            var long = jQuery(this).parent().children(".classLong").children("input:hidden").val();
            LoadMapLatLong(lat, long);
            var position = jQuery(this).offset();
            jQuery(this).parent().parent().children("#maploc").show().css({ 'left': (position.left - 100) + 'px', 'top': (position.top + 20) + 'px' });
            //jQuery("#maploc").show();
        }, function () { jQuery("#maploc").hide(); });
    });
            function LoadMapLatLong(lat, long) {

                var map = new google.maps.Map(document.getElementById('maploc'), {
                    zoom: 14,
                    center: new google.maps.LatLng(lat, long),
                    mapTypeId: google.maps.MapTypeId.ROADMAP

                });

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(lat, long),
                    map: map
                });

            }
        </script>
        

</asp:Content>
