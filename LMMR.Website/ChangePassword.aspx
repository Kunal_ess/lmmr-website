﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeMaster.master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntLeft" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMainbody" Runat="Server">
<br/><br/><br/>
    <h1>    
        <b><%= GetKeyResult("CHANGEPASSWORD")%></b></h1><br/><br/>
<div class="booking-details">
        <ul>
            <li id="divmessage" runat="server"></li>
            <li class="value8">
                <div class="col17">
                    <strong><%= GetKeyResult("OLDPASSWORD")%><span style="color: Red">*</span></strong></div>
                <div class="col18">
                    <div class="sec1">
                        <asp:TextBox ID="txtOldPassword" runat="server" Class="textfield2" TextMode="Password"
                            MaxLength="50"></asp:TextBox>
                    </div>
                </div>
            </li>
            <li class="value9">
                <div class="col17">
                    <strong><%= GetKeyResult("NEWPASSWORD")%><span style="color: Red">*</span></strong></div>
                <div class="col18">
                    <div class="sec1">
                        <asp:TextBox ID="txtNewPassword" runat="server" Class="textfield2" TextMode="Password"
                            MaxLength="50"></asp:TextBox>
                    </div>
                </div>
            </li>
            <li class="value8">
                <div class="col17">
                    <strong><%= GetKeyResult("CONFIRMPASSWORD")%><span style="color: Red">*</span></strong></div>
                <div class="col19">
                    <div class="sec1">
                        <asp:TextBox ID="txtConfirmNewpassword" runat="server" class="textfield2" TextMode="Password"
                            MaxLength="50"></asp:TextBox>
                    </div>
                </div>
            </li>
            <li class="value10">
                <div class="col21">
                    <div class="button_section">
                        <asp:LinkButton ID="btnSave" runat="server" CssClass="select" OnClick="btnSave_Click" />
                         <span>or</span>
                        <asp:LinkButton ID="btnCancel" OnClick="btnCancel_Click" runat="server" />
                    </div>
                </div>
            </li>
        </ul>
        <table width="100%" border="0" cellpadding="2">
            <tr>
                <td colspan="6" valign="top" style="padding: 10px 0px 0px 2px; font-weight: bold"
                    align="left">
                    <img src="Images/help.jpg" />&nbsp;&nbsp;<%= GetKeyResult("PASSWORDMUSTTEXT") %> 
                </td>
            </tr>
        </table>
    </div>
    <div id="Loding_overlaySec">
        <img src="<%= SiteRootPath %>Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br /><%= GetKeyResult("SAVING")%>
        </div>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {


            jQuery("#<%= btnSave.ClientID %>").bind("click", function () {
                if (jQuery("#<%= divmessage.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= divmessage.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                }
                var oldpassword = jQuery("#<%= txtOldPassword.ClientID %>").val();
                var isvalid = true;
                var errormessage = "";
                if (oldpassword.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("OLDPWDREQUIRED")%>';
                    isvalid = false;
                }
                var newpassword = jQuery("#<%= txtNewPassword.ClientID %>").val();
                if (newpassword.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("NEWPWDREQUIRED")%>';
                    isvalid = false;
                }
                else if (newpassword.length < 6) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("PWDCHARREQUIRED")%>';
                    isvalid = false;
                }
                else if (!checkAlphabetic(newpassword)) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("NEWPWDALPHANUMERIC")%>';
                    isvalid = false;
                }
                var confirmpassword = jQuery("#<%= txtConfirmNewpassword.ClientID %>").val();
                if (confirmpassword.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("CONFIRMPWD")%>';
                    isvalid = false;
                }
                else if (newpassword != confirmpassword) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("PWDNOTMATCHED")%>';
                    isvalid = false;
                }
                if (!isvalid) {
                    jQuery(".error").show();
                    jQuery(".error").html(errormessage);
                    var offseterror = jQuery(".error").offset();
                    jQuery("body").scrollTop(offseterror.top);
                    jQuery("html").scrollTop(offseterror.top);
                    return false;
                }
                jQuery(".error").html("");
                jQuery(".error").hide();
                jQuery("#Loding_overlaySec").show();
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            });
        });

        function checkAlphabetic(sText) {
            var ValidChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var IsAlphabetic = true;
            var Char;
            var i = 0;
            for (i = 0; i < sText.length; i++) {
                Char = sText.charAt(i);
                if (ValidChars.indexOf(Char) == -1) {
                    IsAlphabetic = false;
                    break;
                }
            }
            return IsAlphabetic;
        }
    </script>
     
    <script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
    </script>
    
    <noscript>
    
        <div style="display: inline;">
            <img height="1" width="1" style="border-style: none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1018259257/?value=0&amp;label=XAV3CP-n1QIQuc7F5QM&amp;guid=ON&amp;script=0" />
        </div>
    </noscript>
</asp:Content>

