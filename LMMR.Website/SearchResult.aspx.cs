﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using System.Text;
using LMMR.Entities;
using LMMR.Data;
using System.Configuration;
using System.Web.UI.HtmlControls;
public partial class SearchResult : BasePage
{
    public SearchResult()
    {
        base.PreInit += new EventHandler(SearchResult_PreInit);
        base.PreRender += new EventHandler(SearchResult_PreRender);
    }

    void SearchResult_PreInit(object sender, EventArgs e)
    {
        //Response.Write("<div style='color:red;'>Page PreInit:" + DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond + "</div>");
    }
    #region Property & variable
    ManageMapSearch objManageSearch = new ManageMapSearch();
    ManageCMSContent objManageCMSContent = new ManageCMSContent();
    PackagePricingManager objPackagePricingManager = new PackagePricingManager();
    StringBuilder objSB = new StringBuilder();
    ManageOthers objOthers = new ManageOthers();
    BookingRequest objBookingRequest = null;
    VList<BookingRequestViewList> Tempvlist;
    VList<BookingRequestViewList> vlist2day;
    ManageWhiteLabel objwlMapping = new ManageWhiteLabel();
    public int intTypeCMSid;
    public string HotelData;

    #endregion
    
    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        
        
        //Response.Write("<div style='color:red;'>PageLoad Started:" + DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond + "</div>");
        if (Request.QueryString["wl"] == null)
        {
            //By Pranayesh For WL.
            DivRightPart.Style.Add("display", "block");
            drpCountry.Enabled = true;
            drpCity.Enabled = true;
            HtmlGenericControl topMenu = (HtmlGenericControl)tmFrontEnd.FindControl("divSubNav");
            topMenu.Visible = true;
            HtmlGenericControl topMenuleft = (HtmlGenericControl)tmFrontEnd.FindControl("divLanguage");
            topMenuleft.Visible = true;
            HtmlGenericControl phmedia = (HtmlGenericControl)Master.FindControl("divMainFooter");
            HtmlGenericControl phlink = (HtmlGenericControl)Master.FindControl("divMainBottomLink");
            HtmlGenericControl phpowered = (HtmlGenericControl)Master.FindControl("DivMainPowerd");
            phpowered.Visible = false;
            phmedia.Visible = true;
            phlink.Visible = true;
            divlogo.Attributes.Add("class", "logo");
            imgSiteLogo.Src = SiteRootPath + "images/logo-front.png";
            Divmapbutton.Style.Add("display", "block");
            ////////////////////////////////////////////////////////////
            if (l != null)
            {
                logoHotel.HRef = SiteRootPath + "default/" + l.Name.ToLower();
            }
            else
            {
                logoHotel.HRef = SiteRootPath + "default/english";
            }
            if (string.IsNullOrEmpty(Convert.ToString(Session["masterInput"])) && (Session["Mycountry"] == null && Session["Mycity"] == null))
            {
                //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "default/english");
                }
            }
            HotelData = "[[]]";
            btnSearch.Attributes.Add("onclick", "return validate();");
            if (Session["LanguageID"] == null)
            {
                Session["LanguageID"] = 1;
            }
            if (Session["CurrentRestUser"] != null)
            {
                Users objUsers = (Users)Session["CurrentRestUser"];
                lblLoginUserName.Text = objUsers.FirstName + " " + objUsers.LastName;
                objUsers.LastLogin = DateTime.Now;
                lstLoginTime.Text = DateTime.Now.ToLongDateString();
                AfterLogin(objUsers.UserId, Convert.ToInt32(objUsers.Usertype));
            }

            if (Session["status"] != null)
            {
                Page.RegisterStartupScript("msg", "<script language='javascript'>alert(" + "'" + Session["status"] + "'" + ");</script>");
                Session["status"] = null;
            }
            if (Session["passwordChangeStatus"] != null)
            {
                Page.RegisterStartupScript("msg", "<script language='javascript'>alert(" + "'" + Session["passwordChangeStatus"] + "'" + ");</script>");
                Session["passwordChangeStatus"] = null;
            }


            if (!IsPostBack)
            {
                Session["CityHotel"] = null;
                fillStaticData();
                BindURLs();
                // this part used by buttom link
                if (Session["Mycountry"] != null && Session["Mycity"] != null)
                {
                    CalendarExtender1.StartDate = DateTime.Now.Date;
                    //CalendarExtender1.EndDate = DateTime.Now.Date.AddMonths(2);
                    BindCountryDropdown();

                    //drpCountry.Items.FindByValue(Convert.ToString(Session["Mycountry"])).Selected = true;
                    drpCountry.SelectedValue = Convert.ToString(Session["Mycountry"]);
                    hdnCountry.Value = Convert.ToString(Session["Mycountry"]);
                    BindCityDropdownByCountryID(Convert.ToInt32(drpCountry.SelectedValue));
                    if (drpCity.Items.FindByValue(Convert.ToString(Session["Mycity"])) != null)
                    {
                        //drpCity.Items.FindByValue(Convert.ToString(Session["Mycity"])).Selected = true;
                        drpCity.SelectedValue = Convert.ToString(Session["Mycity"]);
                        hdnCityDetail.Value = Convert.ToString(Session["Mycity"]);
                    }
                    else
                    {
                        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                        if (l != null)
                        {
                            Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
                        }
                        else
                        {
                            Response.Redirect(SiteRootPath + "default/english");
                        }
                        //Response.Redirect("~/Default.aspx");
                    }
                    if (Session["Date"] == null)
                    {
                        txtBookingDate.Text = String.Format("{0:dd/MM/yy}", DateTime.Now.Date);
                    }
                    else
                    {
                        txtBookingDate.Text = Session["Date"].ToString();
                    }
                    drpDays.SelectedValue = "0";
                    drpDay2.SelectedValue = "0";
                    drpDuration.SelectedValue = "1";
                    if (Session["NumberOfParticipant"] == null)
                    {
                        txtParticipant.Text = "";
                    }
                    else
                    {
                        txtParticipant.Text = Session["NumberOfParticipant"].ToString();
                    }
                    Session["Country"] = hdnCountry.Value;
                    Session["City"] = hdnCityDetail.Value;
                    if (Session["Myzone"] != null)
                    {
                        Session["Zone"] = Session["Myzone"].ToString();
                    }
                    else
                    {
                        Session.Remove("zone");
                    }
                }
                else
                {

                    CalendarExtender1.StartDate = DateTime.Now.Date;
                    //CalendarExtender1.EndDate = DateTime.Now.Date.AddMonths(2);
                    Session.Remove("Zone");
                    if (Convert.ToString(Session["masterInput"]) != "")
                    {
                        BindCountryDropdown();
                        drpCountry.SelectedValue = ((Session["masterInput"] as BookingRequest)).propCountry;
                        hdnCountry.Value = drpCountry.SelectedValue;
                        BindCityDropdownByCountryID(Convert.ToInt32(drpCountry.SelectedValue));
                        drpCity.SelectedValue = ((Session["masterInput"] as BookingRequest)).propCity;
                        hdnCityDetail.Value = drpCity.SelectedValue;
                        txtBookingDate.Text = ((Session["masterInput"] as BookingRequest)).propDate;
                        drpDays.SelectedValue = ((Session["masterInput"] as BookingRequest)).propDays;
                        drpDay2.SelectedValue = ((Session["masterInput"] as BookingRequest)).propDay2;
                        drpDuration.SelectedValue = ((Session["masterInput"] as BookingRequest)).propDuration;
                        if (drpDuration.SelectedValue == "2")
                        {
                            divDay2.Style.Add("display", "block");
                        }
                        else
                        {
                            divDay2.Style.Add("display", "none");
                        }
                        if (((Session["masterInput"] as BookingRequest)).propParticipants != "0")
                        {
                            txtParticipant.Text = ((Session["masterInput"] as BookingRequest)).propParticipants;
                        }
                        else
                        {
                            txtParticipant.Text = "";
                        }
                    }
                    else
                    {
                        if (Session["Country"] != null && Session["City"] != null)
                        {
                            imgMap.Visible = false;
                            BindCountryDropdown();
                            drpCountry.SelectedValue = Session["Country"].ToString();
                            hdnCountry.Value = drpCountry.SelectedValue;
                            BindCityDropdownByCountryID(Convert.ToInt32(Session["Country"].ToString()));
                            drpCity.Items.FindByValue(Convert.ToString(Session["City"])).Selected = true;
                            hdnCityDetail.Value = drpCity.SelectedValue;
                            GetCenterPoint(Convert.ToInt32(Session["City"]));
                            ManageSession();
                            GetCityHotel();
                        }
                    }
                }

                if (hdnCityDetail.Value != null && hdnCityDetail.Value != "0" && hdnCityDetail.Value != "" && Session["CityHotel"] == null)
                {
                    ManageSession();
                    GetCenterPoint(Convert.ToInt32(hdnCityDetail.Value));
                    GetCityHotel();
                    Session["CityHotel"] = "Yes";
                }
                hdnSearchButtonClick.Value = "0";
            }
            else
            {
                if (hdnCityDetail.Value != null && hdnCityDetail.Value != "0" && hdnCityDetail.Value != "" && Session["CityHotel"] != null && hdnSearchButtonClick.Value != "1")
                {
                    if (((Session["masterInput"] as BookingRequest)).propCity == hdnCityDetail.Value)
                    {
                        ManageSession();
                        GetCenterPoint(Convert.ToInt32(hdnCityDetail.Value));
                        GetCityHotel();
                        Session["CityHotel"] = "Yes";
                    }
                    hdnSearchButtonClick.Value = "0";
                }
            }

            //Page.MaintainScrollPositionOnPostBack = true;
        }
        else
        {
            //Pranayesh for WL.
            DivRightPart.Style.Add("display", "block");
            drpCountry.Enabled = true;
            drpCity.Enabled = true;
            HtmlGenericControl phmedia = (HtmlGenericControl)Master.FindControl("divMainFooter");
            HtmlGenericControl phlink = (HtmlGenericControl)Master.FindControl("divMainBottomLink");
            HtmlGenericControl phpowered = (HtmlGenericControl)Master.FindControl("DivMainPowerd");
            phpowered.Visible = true;
            phmedia.Visible = false;
            phlink.Visible = false;
            divlogo.Attributes.Add("class", "logo-wl");
            WhiteLabel objwl = objwlMapping.GetWhiteLabelByID(Convert.ToInt64(Request.QueryString["wl"]));
            if (objwl != null)
            {
                HtmlGenericControl topMenu = (HtmlGenericControl)tmFrontEnd.FindControl("divSubNav");
                topMenu.Visible = false;
                HtmlGenericControl topMenuleft = (HtmlGenericControl)tmFrontEnd.FindControl("divLanguage");
                topMenuleft.Visible = false;
                Divmapbutton.Style.Add("display", "none");
                imgSiteLogo.Src = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Replace("~/", "") + "TargetSiteLogo/" + objwl.TargetSiteLogo; ;
            }

            if (hdnSearchButtonClick.Value == "0")
            {

                logoHotel.Attributes.Remove("HRef");
                if (Request.QueryString["country"] != null && Request.QueryString["city"] != null)
                {
                    CalendarExtender1.StartDate = DateTime.Now.Date;
                    BindCountryDropdown();
                    drpCountry.SelectedValue = Convert.ToString(Request.QueryString["country"]);
                    hdnCountry.Value = drpCountry.SelectedValue;
                    BindCityDropdownByCountryID(Convert.ToInt32(drpCountry.SelectedValue));
                    if (drpCity.Items.FindByValue(Convert.ToString(Request.QueryString["city"])) != null)
                    {
                        drpCity.SelectedValue = Convert.ToString(Request.QueryString["city"]);
                        hdnCityDetail.Value = drpCity.SelectedValue;
                    }
                    if (Request.QueryString["d"] == null && Request.QueryString["m"] == null && Request.QueryString["y"] == null)
                    {
                        txtBookingDate.Text = String.Format("{0:dd/MM/yy}", DateTime.Now.Date);
                    }
                    else
                    {
                        DateTime dd = new DateTime(Convert.ToInt32(Request.QueryString["y"]), Convert.ToInt32(Request.QueryString["m"]), Convert.ToInt32(Request.QueryString["d"]));
                        txtBookingDate.Text = dd.ToString("dd/MM/yy");// String.Format("{0:dd/MM/yy}", date);
                    }
                    if (Request.QueryString["duration"] != null && Request.QueryString["day1"] != null && Request.QueryString["day2"] != null)
                    {
                        fillStaticData();
                        drpDays.SelectedValue = Request.QueryString["day1"].ToString();
                        drpDay2.SelectedValue = Request.QueryString["day2"].ToString();
                        if (Request.QueryString["duration"].ToString() == "2")
                        {
                            divDay2.Style.Add("display", "block");
                            drpDuration.SelectedValue = "2";
                        }
                    }
                    else
                    {
                        drpDuration.SelectedValue = "1";
                    }
                    if (Request.QueryString["delegate"] == null)
                    {
                        txtParticipant.Text = "";
                    }
                    else
                    {
                        txtParticipant.Text = Convert.ToString(Request.QueryString["delegate"]);
                    }
                }
            }

            drpCountry.Enabled = false;
            drpCity.Enabled = false;
            ManageSession();
            GetCenterPoint(Convert.ToInt32(hdnCityDetail.Value));
            GetCityHotel();
        }
        //Response.Write("<div style='color:red;'>Page Loadend:" + DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond + "</div>");
    }

    

    void SearchResult_PreRender(object sender, EventArgs e)
    {
        //Response.Write("<div style='color:red;'>Page Render Started:" + DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond + "</div>");
    }
    public void AfterLogin(long userID, int userType)
    {
        beforeLogin.Style.Add("display", "none");
        afterLogin.Style.Add("display", "block");
        NewsSubscriber1.Visible = false;
        Session["registerType"] = userType;
        //hypManageProfile.NavigateUrl = "Registration.aspx";
    }

    public void BindURLs()
    {
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            joinToday.HRef = SiteRootPath + "join-today/" + l.Name.ToLower();
        }
        else
        {
            joinToday.HRef = SiteRootPath + "join-today/english";
        }
    }

    #endregion

    #region Function

    public void fillStaticData()
    {
        btnSearch.Text = GetKeyResult("FINDYOURMEETINGROOM");
        imgIcontext.ToolTip = GetKeyResult("YOUCANONLYBOOKONLINEMESSAGE");
        drpDays.Items.Add(new ListItem(GetKeyResult("FULLDAY"), "0"));
        drpDays.Items.Add(new ListItem(GetKeyResult("MORNING"), "1"));
        drpDays.Items.Add(new ListItem(GetKeyResult("AFTERNOON"), "2"));

        drpDay2.Items.Add(new ListItem(GetKeyResult("FULLDAY"), "0"));
        drpDay2.Items.Add(new ListItem(GetKeyResult("MORNING"), "1"));
        drpDay2.Items.Add(new ListItem(GetKeyResult("AFTERNOON"), "2"));

    }
    /// <summary>
    /// This function used for bind cities by countryID.
    /// </summary>
    /// <param name="intcountryID"></param>
    void BindCityDropdownByCountryID(int intcountryID)
    {
        if (intcountryID != 0)
        {
                drpCity.DataValueField = "Id";
                drpCity.DataTextField = "City";
                drpCity.DataSource = objManageSearch.GetCityData(Convert.ToInt32(drpCountry.SelectedValue));
                drpCity.DataBind();
                drpCity.Items.Insert(0, new ListItem("--Select city--", "0"));
        }
        else
        {
            drpCity.Items.Clear();
            drpCity.Items.Insert(0, new ListItem("--Select city--", "0"));
        }
    }

    /// <summary>
    /// This function used for bind all country.
    /// </summary>
    void BindCountryDropdown()
    {
        drpCountry.DataTextField = "CountryName";
        drpCountry.DataValueField = "Id";
        drpCountry.DataSource = objManageSearch.GetCountryData();
        drpCountry.DataBind();
        //drpCountry.Items.Insert(0, new ListItem("--Select country--", "0"));
        drpCountry.Items.FindByText("Belgium").Selected = true;
    }

    /// <summary>
    /// Get All Hote of the city.
    /// </summary>
    //void GetCityHotel()
    //{
    //    string WhereClause = Session["Where"].ToString();
    //    string orderby = string.Empty;
    //    DateTime BokingDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));
    //    string whereclausRequest = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and AvailabilityDate='" + BokingDate + "'";
    //    objBookingRequest = new BookingRequest();
    //    VList<BookingRequestViewList> objHotel = objBookingRequest.GetBookingReqDetails(WhereClause, orderby).FindAllDistinct(BookingRequestViewListColumn.HotelId);
    //    VList<ViewForRequestSearch> objRequested = objBookingRequest.GetReqDetails(whereclausRequest, orderby).FindAllDistinct(ViewForRequestSearchColumn.HotelId);
    //    HotelData = "[";
    //    if (objHotel.Count > 0 || objRequested.Count > 0)
    //    {
    //        foreach (BookingRequestViewList h in objHotel)
    //        {
    //            string IsPromoted = "";
    //            string strSpecialDeal = "";
    //            string strActualprice = "";
    //            if (h.DdrPercent < 0)
    //            {
    //                IsPromoted = "YES";
    //                strActualprice = String.Format("{0:#,###}", objBookingRequest.GetActualpkgPricebyHotelid(Convert.ToInt32(h.HotelId))[0].ActualFullDayPrice);
    //                strSpecialDeal = String.Format("{0:#,###}", (Math.Abs((Convert.ToDecimal(strActualprice) * Convert.ToDecimal(h.DdrPercent) / 100))));
    //            }
    //            else
    //            {
    //                IsPromoted = "NO";
    //            }

    //            if (HotelData == "[")
    //            {

    //                HotelData += "['" + h.HotelName + "', " + h.Latitude + "," + h.Longitude + "," + 1 + ",'" + IsPromoted + "','" + h.HotelAddress + "','" + h.Stars + "','" + strActualprice + "','" + strSpecialDeal + "']";
    //            }
    //            else
    //            {
    //                HotelData = HotelData + ",[" + "'" + h.HotelName + "', " + h.Latitude + "," + h.Longitude + "," + 1 + ",'" + IsPromoted + "','" + h.HotelAddress + "','" + h.Stars + "','" + strActualprice + "','" + strSpecialDeal + "']";
    //            }
    //        }
    //        foreach (ViewForRequestSearch rh in objRequested)
    //        {
    //            if (objHotel.Where(a => a.HotelId == rh.HotelId).FirstOrDefault() == null)
    //            {
    //                string IsPromoted = "RE";
    //                string strSpecialDeal = "";
    //                string strActualprice = "";

    //                if (HotelData == "[")
    //                {
    //                    HotelData += "['" + rh.HotelName + "', " + rh.Latitude + "," + rh.Longitude + "," + 1 + ",'" + IsPromoted + "','" + rh.HotelAddress + "','" + rh.Stars + "','" + strActualprice + "','" + strSpecialDeal + "']";
    //                }
    //                else
    //                {
    //                    HotelData = HotelData + ",[" + "'" + rh.HotelName + "', " + rh.Latitude + "," + rh.Longitude + "," + 1 + ",'" + IsPromoted + "','" + rh.HotelAddress + "','" + rh.Stars + "','" + strActualprice + "','" + strSpecialDeal + "']";
    //                }

    //            }


    //        }
    //        HotelData += "]";
    //    }
    //    if (HotelData == "[")
    //    {
    //        HotelData = "[[]]";
    //    }

    //}
    void GetCityHotel()
    {
        string WhereClause = Session["Where"].ToString();
        string orderby = string.Empty;
        string whereclausRequest = string.Empty;
        int countparticipants = 0;
        DateTime BokingDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));
        if (Session["WLHotel"] != null)
        {
            whereclausRequest = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + " and HotelId IN (" + Convert.ToString(Session["WLHotel"]) + ")";
            countparticipants = Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants);
        }
        else
        {
            whereclausRequest = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity)  +"'";// and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity;
            countparticipants = Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants);
        }
        objBookingRequest = new BookingRequest();
        VList<BookingRequestViewList> objHotel = new VList<BookingRequestViewList>();

        //Get Minimum consider special as per country id
        TList<Others> objOther = objOthers.GetInfoByCountryID(Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry));
        if (objOther.Count > 0 && objOther[0].CountryId != null)
        {
            ViewState["MinConsider"] = objOther[0].MinConsiderSpl;
        }
        else
        {
            ViewState["MinConsider"] = null;
        }
        VList<ViewForRequestSearch> objRequested = new VList<ViewForRequestSearch>();
        //if (Session["2"] != null)
        //{
        //For day2 logic
        if (Convert.ToString(((Session["masterInput"] as BookingRequest)).propDuration) == "2")
        {
            string whereclaus2 = Convert.ToString(Session["Where2"]);
            vlist2day = objBookingRequest.GetBookingReqDetails(whereclaus2, orderby).FindAllDistinct(BookingRequestViewListColumn.HotelId);
            Tempvlist = objBookingRequest.GetBookingReqDetails(WhereClause, orderby).FindAllDistinct(BookingRequestViewListColumn.HotelId);

            for (int i = 0; i < Tempvlist.Count; i++)
            {
                BookingRequestViewList b = vlist2day.Where(a => a.HotelId == Tempvlist[i].HotelId && a.MeetingRoomId == Tempvlist[i].MeetingRoomId).FirstOrDefault();
                if (b != null)
                {
                    objHotel.Add(b);
                }
            }
        }
        else
        {
            objHotel = objBookingRequest.GetBookingReqDetails(WhereClause, orderby).FindAllDistinct(BookingRequestViewListColumn.HotelId);
            //VList<BookingRequestViewList> lstnew = new VList<BookingRequestViewList>();
            //for (int i = 0; i < objHotel.Count; i++)
            //{
            //    if(lstnew.Where(u=>u.HotelId==objHotel[i].HotelId).Count() <= 0 )
            //    {
            //        lstnew.Add(objHotel[i]);
            //    }
            //}
            //objHotel = lstnew;
        }
        //SearchResult1.BookingDataSource = objHotel;

        VList<ViewForRequestSearch> lstRequestall = new VList<ViewForRequestSearch>();//
        objRequested = objBookingRequest.GetReqDetails(whereclausRequest, orderby);
        VList<ViewForRequestSearch> objAllRequest = objRequested.Copy();
        foreach (var p in objRequested)
        {
            if (p.MinCapacity <= countparticipants && p.MaxCapicity >= countparticipants)
            {
                //if (lstRequestall.Where(u => u.HotelId == p.HotelId).Count() <= 0)
                //{
                lstRequestall.Add(p);
                //}
            }
        }
        //objRequested = lstRequestall;//.FindAllDistinct(ViewForRequestSearchColumn.HotelId);

        
        
        //objRequested = objAllRequest.FindAllDistinct(ViewForRequestSearchColumn.HotelId);
            //Session["2"] = null;

        //}
        //else
        //{
        //    Session["2"] = "2";
        //}
        SearchResult1.BookingDataSource = objHotel;
        SearchResult1.RequestDataSource = lstRequestall.FindAllDistinct(ViewForRequestSearchColumn.HotelId);
        SearchResult1.RequestDataSourceAll = objRequested;
        HotelData = "[";
        if (objHotel.Count > 0 || objRequested.Count > 0)
        {
            foreach (BookingRequestViewList h in objHotel)
            {
                string IsPromoted = "";
                string strSpecialDeal = "";
                string strActualprice = "";
                //if (h.DdrPercent < 0 || h.DdrPercent > 0)
                //{

                strActualprice = string.Empty;
                strSpecialDeal = string.Empty;
                decimal ActualPrice = Convert.ToDecimal(h.ActualPkgPrice);
                decimal ActualHalfPrice = Convert.ToDecimal(h.ActualPkgPriceHalfDay);
                TList<PackageMaster> pkgmaster = objPackagePricingManager.GetStandardName(Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry));
                if (pkgmaster.Count > 0)
                {
                    #region Get Price from actualpackageprice by hotelid with discount for 1 Day
                    DateTime fromDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));
                    TList<SpecialPriceAndPromo> splDiscount = objBookingRequest.GetSpecialPriceandPromoByDate(Convert.ToInt32(h.HotelId), fromDate);
                    decimal DiscountPrice = Convert.ToDecimal(splDiscount[0].DdrPercent);
                    if (((Session["masterInput"] as BookingRequest)).propDuration == "1")
                    {
                        if (DiscountPrice < 0 && ((Session["masterInput"] as BookingRequest)).propDuration == "1")
                        {
                            IsPromoted = "YES";
                            if (((Session["masterInput"] as BookingRequest)).propDays == "0")
                            {
                                strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs(ActualPrice)));
                                TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(h.HotelId));
                                decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.FulldayPrice).Sum());
                                decimal Fulldaypkgprice = (Math.Abs((ActualPrice - getTotleFullDayPrice)));
                                decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * DiscountPrice / 100)));
                                strSpecialDeal = String.Format("{0:#,##,##0.0}", (ActualPrice - PkgDiscountPrice));
                            }
                            else
                            {
                                strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs(ActualHalfPrice)));
                                TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(h.HotelId));
                                decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.HalfdayPrice).Sum());
                                decimal Fulldaypkgprice = (Math.Abs((ActualHalfPrice - getTotleHalfDayPrice)));
                                decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * DiscountPrice / 100)));
                                strSpecialDeal = String.Format("{0:#,##,##0.0}", (ActualHalfPrice - PkgDiscountPrice));
                            }

                            //added this formula for checking minimum special discount
                            if (ViewState["MinConsider"] != null && Convert.ToInt32(ViewState["MinConsider"]) <= DiscountPrice)
                            {
                                strActualprice = strSpecialDeal;
                                IsPromoted = "NO";
                            }
                            else
                            {

                            }
                        }
                        //End Get Price from actualpackageprice by hotelid without discount
                        else if (DiscountPrice == 0 && ((Session["masterInput"] as BookingRequest)).propDuration == "1")
                        {
                            IsPromoted = "NO";
                            if (((Session["masterInput"] as BookingRequest)).propDays == "0")
                            {
                                strActualprice = String.Format("{0:#,##,##0.0}", Convert.ToDecimal(h.ActualPkgPrice));
                            }
                            else
                            {
                                strActualprice = String.Format("{0:#,##,##0.0}", Convert.ToDecimal(h.ActualPkgPriceHalfDay));
                            }
                        }
                        else if (DiscountPrice >= 0 && ((Session["masterInput"] as BookingRequest)).propDuration == "1")
                        {
                            IsPromoted = "NO";
                            if (((Session["masterInput"] as BookingRequest)).propDays == "0")
                            {
                                TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(h.HotelId));
                                decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.FulldayPrice).Sum());
                                decimal Fulldaypkgprice = (Math.Abs((ActualPrice - getTotleFullDayPrice)));
                                decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * DiscountPrice / 100)));
                                strActualprice = String.Format("{0:#,##,##0.0}", (ActualPrice + PkgDiscountPrice));
                            }
                            else
                            {
                                TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(h.HotelId));
                                decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.HalfdayPrice).Sum());
                                decimal Fulldaypkgprice = (Math.Abs((ActualHalfPrice - getTotleHalfDayPrice)));
                                decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * DiscountPrice / 100)));
                                strActualprice = String.Format("{0:#,##,##0.0}", (ActualHalfPrice + PkgDiscountPrice));
                            }
                        }
                    }
                    #endregion

                    #region Get Price from actualpackageprice by hotelid with discount for 2 Day
                    if (((Session["masterInput"] as BookingRequest)).propDuration == "2")
                    {
                        TList<SpecialPriceAndPromo> Spl = objBookingRequest.GetSpecialPriceandPromoByDate(Convert.ToInt32(h.HotelId), fromDate.AddDays(1));
                        decimal SecdayDiscountPercent = Convert.ToDecimal(Spl[0].DdrPercent);
                        if (((DiscountPrice < 0 && SecdayDiscountPercent < 0) && ((Session["masterInput"] as BookingRequest)).propDuration == "2") || ((DiscountPrice < 0 && SecdayDiscountPercent == 0) && ((Session["masterInput"] as BookingRequest)).propDuration == "2") || ((DiscountPrice == 0 && SecdayDiscountPercent < 0) && ((Session["masterInput"] as BookingRequest)).propDuration == "2"))
                        {
                            IsPromoted = "YES";
                            if (((Session["masterInput"] as BookingRequest)).propDays == "0" && ((Session["masterInput"] as BookingRequest)).propDay2 == "0")
                            {
                                strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs(ActualPrice + ActualPrice)));
                                TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(h.HotelId));
                                decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.FulldayPrice).Sum());
                                decimal Fulldaypkgprice = (Math.Abs((ActualPrice - getTotleFullDayPrice)));
                                decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * DiscountPrice / 100)));
                                decimal PkgDiscountPrice2Day = (Math.Abs((Fulldaypkgprice * SecdayDiscountPercent / 100)));
                                strSpecialDeal = String.Format("{0:#,##,##0.0}", (ActualPrice - PkgDiscountPrice) + (ActualPrice - PkgDiscountPrice2Day));
                            }
                            else if (((Session["masterInput"] as BookingRequest)).propDays != "0" && ((Session["masterInput"] as BookingRequest)).propDay2 == "0")
                            {
                                strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs(ActualHalfPrice + ActualPrice)));
                                TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(h.HotelId));
                                decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.FulldayPrice).Sum());
                                decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.HalfdayPrice).Sum());
                                decimal Fulldaypkgprice = (Math.Abs((ActualPrice - getTotleFullDayPrice)));
                                decimal Halfdaypkgprice = (Math.Abs((ActualHalfPrice - getTotleHalfDayPrice)));
                                decimal PkgDiscountPrice = (Math.Abs((Halfdaypkgprice * DiscountPrice / 100)));
                                decimal PkgDiscountPrice2Day = (Math.Abs((Fulldaypkgprice * SecdayDiscountPercent / 100)));
                                strSpecialDeal = String.Format("{0:#,##,##0.0}", (ActualHalfPrice - PkgDiscountPrice) + (ActualPrice - PkgDiscountPrice2Day));
                            }
                            else if (((Session["masterInput"] as BookingRequest)).propDays == "0" && ((Session["masterInput"] as BookingRequest)).propDay2 != "0")
                            {
                                strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs(ActualPrice + ActualHalfPrice)));
                                TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(h.HotelId));
                                decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.FulldayPrice).Sum());
                                decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.HalfdayPrice).Sum());
                                decimal Fulldaypkgprice = (Math.Abs((ActualPrice - getTotleFullDayPrice)));
                                decimal Halfdaypkgprice = (Math.Abs((ActualHalfPrice - getTotleHalfDayPrice)));
                                decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * DiscountPrice / 100)));
                                decimal PkgDiscountPrice2Day = (Math.Abs((Halfdaypkgprice * SecdayDiscountPercent / 100)));
                                strSpecialDeal = String.Format("{0:#,##,##0.0}", (ActualPrice - PkgDiscountPrice) + (ActualHalfPrice - PkgDiscountPrice2Day));
                            }
                            else if (((Session["masterInput"] as BookingRequest)).propDays != "0" && ((Session["masterInput"] as BookingRequest)).propDay2 != "0")
                            {
                                strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs(ActualHalfPrice + ActualHalfPrice)));
                                TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(h.HotelId));
                                decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.HalfdayPrice).Sum());
                                decimal Halfdaypkgprice = (Math.Abs((ActualHalfPrice - getTotleHalfDayPrice)));
                                decimal PkgDiscountPrice = (Math.Abs((Halfdaypkgprice * DiscountPrice / 100)));
                                decimal PkgDiscountPrice2Day = (Math.Abs((Halfdaypkgprice * SecdayDiscountPercent / 100)));
                                strSpecialDeal = String.Format("{0:#,##,##0.0}", (ActualHalfPrice - PkgDiscountPrice) + (ActualHalfPrice - PkgDiscountPrice2Day));
                            }

                            //added this formula for checking minimum special discount                    
                            if (ViewState["MinConsider"] != null && (Convert.ToInt32(ViewState["MinConsider"]) <= DiscountPrice || Convert.ToInt32(ViewState["MinConsider"]) <= SecdayDiscountPercent))
                            {
                                strActualprice = strSpecialDeal;
                                IsPromoted = "NO";
                            }
                            else
                            {

                            }
                        }
                        //End Get Price from actualpackageprice by hotelid without discount
                        else if (DiscountPrice == 0 && SecdayDiscountPercent == 0 && ((Session["masterInput"] as BookingRequest)).propDuration == "2")
                        {
                            IsPromoted = "NO";

                            if (((Session["masterInput"] as BookingRequest)).propDays == "0" && ((Session["masterInput"] as BookingRequest)).propDay2 == "0")
                            {
                                strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs(ActualPrice + ActualPrice)));
                            }
                            else if (((Session["masterInput"] as BookingRequest)).propDays != "0" && ((Session["masterInput"] as BookingRequest)).propDay2 != "0")
                            {
                                strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs(ActualHalfPrice + ActualHalfPrice)));
                            }
                            else
                            {
                                strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs(ActualPrice + ActualHalfPrice)));
                            }
                        }
                        else if ((DiscountPrice > 0 || SecdayDiscountPercent > 0) && ((Session["masterInput"] as BookingRequest)).propDuration == "2")
                        {
                            IsPromoted = "YES";
                            if (((Session["masterInput"] as BookingRequest)).propDays == "0" && ((Session["masterInput"] as BookingRequest)).propDay2 == "0")
                            {
                                TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(h.HotelId));
                                decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.FulldayPrice).Sum());
                                decimal Fulldaypkgprice = (Math.Abs((ActualPrice - getTotleFullDayPrice)));
                                decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * DiscountPrice / 100)));
                                decimal PkgDiscountPrice2Day = (Math.Abs((Fulldaypkgprice * SecdayDiscountPercent / 100)));
                                strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice) + ActualPrice)));
                                if (DiscountPrice > 0 && SecdayDiscountPercent < 0)
                                {
                                    //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice + PkgDiscountPrice) + ActualPrice)));
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice + PkgDiscountPrice) + (ActualPrice - PkgDiscountPrice2Day))));
                                }
                                else if (DiscountPrice > 0 && SecdayDiscountPercent == 0)
                                {
                                    //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice + PkgDiscountPrice) + ActualPrice)));
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice + PkgDiscountPrice) + (ActualPrice))));
                                }
                                else if (DiscountPrice < 0 && SecdayDiscountPercent > 0)
                                {
                                    //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice) + (ActualPrice + PkgDiscountPrice2Day))));
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice - PkgDiscountPrice) + (ActualPrice + PkgDiscountPrice2Day))));
                                }
                                else if (DiscountPrice == 0 && SecdayDiscountPercent > 0)
                                {
                                    //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice) + (ActualPrice + PkgDiscountPrice2Day))));
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice) + (ActualPrice + PkgDiscountPrice2Day))));
                                }
                                else if (DiscountPrice > 0 && SecdayDiscountPercent > 0)
                                {
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice + PkgDiscountPrice) + (ActualPrice + PkgDiscountPrice2Day))));
                                }
                                if (Convert.ToDecimal(strActualprice) <= Convert.ToDecimal(strSpecialDeal))
                                {
                                    strActualprice = strSpecialDeal;
                                }
                            }
                            else if (((Session["masterInput"] as BookingRequest)).propDays != "0" && ((Session["masterInput"] as BookingRequest)).propDay2 == "0")
                            {
                                TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(h.HotelId));
                                decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.FulldayPrice).Sum());
                                decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.HalfdayPrice).Sum());
                                decimal Fulldaypkgprice = (Math.Abs((ActualPrice - getTotleFullDayPrice)));
                                decimal Halfdaypkgprice = (Math.Abs((ActualHalfPrice - getTotleHalfDayPrice)));
                                decimal PkgDiscountPrice = (Math.Abs((Halfdaypkgprice * DiscountPrice / 100)));
                                decimal PkgDiscountPrice2Day = (Math.Abs((Fulldaypkgprice * SecdayDiscountPercent / 100)));
                                strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice) + ActualPrice)));
                                if (DiscountPrice > 0 && SecdayDiscountPercent < 0)
                                {
                                    //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice + PkgDiscountPrice) + ActualPrice)));
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice + PkgDiscountPrice) + (ActualPrice - PkgDiscountPrice2Day))));
                                }
                                else if (DiscountPrice > 0 && SecdayDiscountPercent == 0)
                                {
                                    //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice + PkgDiscountPrice) + ActualPrice)));
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice + PkgDiscountPrice) + (ActualPrice))));
                                }
                                else if (DiscountPrice < 0 && SecdayDiscountPercent > 0)
                                {
                                    //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice) + (ActualPrice + PkgDiscountPrice2Day))));
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice - PkgDiscountPrice) + (ActualPrice + PkgDiscountPrice2Day))));
                                }
                                else if (DiscountPrice == 0 && SecdayDiscountPercent > 0)
                                {
                                    //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice) + (ActualPrice + PkgDiscountPrice2Day))));
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice) + (ActualPrice + PkgDiscountPrice2Day))));
                                }
                                else if (DiscountPrice > 0 && SecdayDiscountPercent > 0)
                                {
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice + PkgDiscountPrice) + (ActualPrice + PkgDiscountPrice2Day))));
                                }
                                if (Convert.ToDecimal(strActualprice) <= Convert.ToDecimal(strSpecialDeal))
                                {
                                    strActualprice = strSpecialDeal;
                                }
                            }
                            else if (((Session["masterInput"] as BookingRequest)).propDays == "0" && ((Session["masterInput"] as BookingRequest)).propDay2 != "0")
                            {
                                TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(h.HotelId));
                                decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.FulldayPrice).Sum());
                                decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.HalfdayPrice).Sum());
                                decimal Fulldaypkgprice = (Math.Abs((ActualPrice - getTotleFullDayPrice)));
                                decimal Halfdaypkgprice = (Math.Abs((ActualHalfPrice - getTotleHalfDayPrice)));
                                decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * DiscountPrice / 100)));
                                decimal PkgDiscountPrice2Day = (Math.Abs((Halfdaypkgprice * SecdayDiscountPercent / 100)));
                                strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice) + ActualHalfPrice)));
                                if (DiscountPrice > 0 && SecdayDiscountPercent < 0)
                                {
                                    //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice + PkgDiscountPrice) + (ActualHalfPrice))));
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice + PkgDiscountPrice) + (ActualHalfPrice - PkgDiscountPrice2Day))));
                                }
                                else if (DiscountPrice > 0 && SecdayDiscountPercent == 0)
                                {
                                    //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice + PkgDiscountPrice) + (ActualHalfPrice))));
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice + PkgDiscountPrice) + (ActualHalfPrice))));
                                }
                                else if (DiscountPrice < 0 && SecdayDiscountPercent > 0)
                                {
                                    //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice) + (ActualHalfPrice + PkgDiscountPrice2Day))));
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice - PkgDiscountPrice) + (ActualHalfPrice + PkgDiscountPrice2Day))));
                                }
                                else if (DiscountPrice == 0 && SecdayDiscountPercent > 0)
                                {
                                    //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice) + (ActualHalfPrice + PkgDiscountPrice2Day))));
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice) + (ActualHalfPrice + PkgDiscountPrice2Day))));
                                }
                                else if (DiscountPrice > 0 && SecdayDiscountPercent > 0)
                                {
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice + PkgDiscountPrice) + (ActualHalfPrice + PkgDiscountPrice2Day))));
                                }
                                if (Convert.ToDecimal(strActualprice) <= Convert.ToDecimal(strSpecialDeal))
                                {
                                    strActualprice = strSpecialDeal;
                                }
                            }
                            else if (((Session["masterInput"] as BookingRequest)).propDays != "0" && ((Session["masterInput"] as BookingRequest)).propDay2 != "0")
                            {
                                TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(h.HotelId));
                                decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.HalfdayPrice).Sum());
                                decimal Halfdaypkgprice = (Math.Abs((ActualHalfPrice - getTotleHalfDayPrice)));
                                decimal PkgDiscountPrice = (Math.Abs((Halfdaypkgprice * DiscountPrice / 100)));
                                decimal PkgDiscountPrice2Day = (Math.Abs((Halfdaypkgprice * SecdayDiscountPercent / 100)));
                                strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice) + ActualHalfPrice)));
                                if (DiscountPrice > 0 && SecdayDiscountPercent < 0)
                                {
                                    //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice + PkgDiscountPrice) + (ActualHalfPrice))));
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice + PkgDiscountPrice) + (ActualHalfPrice - PkgDiscountPrice2Day))));
                                }
                                else if (DiscountPrice > 0 && SecdayDiscountPercent == 0)
                                {
                                    //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice + PkgDiscountPrice) + (ActualHalfPrice))));
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice + PkgDiscountPrice) + (ActualHalfPrice))));
                                }
                                else if (DiscountPrice < 0 && SecdayDiscountPercent > 0)
                                {
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice - PkgDiscountPrice) + (ActualHalfPrice + PkgDiscountPrice2Day))));
                                    //strSpecialDeal = String.Format("{0:#,##,##0.0}", (ActualHalfPrice - PkgDiscountPrice) + (ActualHalfPrice + PkgDiscountPrice2Day));
                                }
                                else if (DiscountPrice == 0 && SecdayDiscountPercent > 0)
                                {
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice) + (ActualHalfPrice + PkgDiscountPrice2Day))));
                                    //strSpecialDeal = String.Format("{0:#,##,##0.0}", (ActualHalfPrice) + (ActualHalfPrice + PkgDiscountPrice2Day));
                                }
                                else if (DiscountPrice > 0 && SecdayDiscountPercent > 0)
                                {
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice + PkgDiscountPrice) + (ActualHalfPrice + PkgDiscountPrice2Day))));
                                }
                                if (Convert.ToDecimal(strActualprice) <= Convert.ToDecimal(strSpecialDeal))
                                {
                                    strActualprice = strSpecialDeal;
                                }
                            }
                        }
                    }
                    #endregion
                }

                //}
                //else
                //{
                //    IsPromoted = "NO";
                //}

                if (HotelData == "[")
                {

                    HotelData += "['" + h.HotelName.Replace("'", "~") + "', " + h.Latitude + "," + h.Longitude + "," + 1 + ",'" + IsPromoted + "','" + h.HotelAddress.Replace(System.Environment.NewLine, string.Empty).Replace("'", "~") + "','" + h.Stars + "','" + strActualprice + "','" + strSpecialDeal + "']";
                }
                else
                {
                    HotelData = HotelData + ",[" + "'" + h.HotelName.Replace("'", "~") + "', " + h.Latitude + "," + h.Longitude + "," + 1 + ",'" + IsPromoted + "','" + h.HotelAddress.Replace(System.Environment.NewLine, string.Empty).Replace("'", "~") + "','" + h.Stars + "','" + strActualprice + "','" + strSpecialDeal + "']";
                }
            }
            foreach (ViewForRequestSearch rh in objRequested)
            {
                if (objHotel.Where(a => a.HotelId == rh.HotelId).FirstOrDefault() == null)
                {
                    string IsPromoted = "RE";
                    string strSpecialDeal = "No Price";
                    string strActualprice = "No Price";

                    if (HotelData == "[")
                    {
                        HotelData += "['" + rh.HotelName.Replace("'", "~") + "', " + rh.Latitude + "," + rh.Longitude + "," + 1 + ",'" + IsPromoted + "','" + rh.HotelAddress.Replace(System.Environment.NewLine, string.Empty).Replace("'", "~") + "','" + rh.Stars + "','" + strActualprice + "','" + strSpecialDeal + "']";
                    }
                    else
                    {
                        HotelData = HotelData + ",[" + "'" + rh.HotelName.Replace("'", "~") + "', " + rh.Latitude + "," + rh.Longitude + "," + 1 + ",'" + IsPromoted + "','" + rh.HotelAddress.Replace(System.Environment.NewLine, string.Empty).Replace("'", "~") + "','" + rh.Stars + "','" + strActualprice + "','" + strSpecialDeal + "']";
                    }

                }

            }
            HotelData += "]";
        }
        if (HotelData == "[")
        {
            HotelData = "[[]]";
        }
        
    }

    /// <summary>
    /// used this function to get center point of the city.
    /// </summary>
    /// <param name="cityID"></param>
    void GetCenterPoint(int cityID)
    {

        //Function to get main center point of the city.
        string strLatitudeOfCenter = "";
        string strLogitudeOfCenter = "";
        string strCenterPlace = "";
        string strCenterPoint = "";
        int intZoomLevel = 10;
        TList<MainPoint> objCenterPoint = objManageSearch.GetCenterPoint(cityID);
        if (objCenterPoint.Count > 0)
        {
            MainPoint objPoint = objCenterPoint.Find(a => a.IsCenter == true);
            strLatitudeOfCenter = objPoint.Latitude.ToString();
            strLogitudeOfCenter = objPoint.Longitude.ToString();
            //hdnMapLatitude.Value = objPoint.Latitude.ToString();
            //hdnMapLogitude.Value = objPoint.Longitude.ToString();
            strCenterPlace = objPoint.MainPointName;
            intZoomLevel = 12;
        }
        strCenterPoint = strLatitudeOfCenter + "," + strLogitudeOfCenter;
        ScriptManager.RegisterStartupScript(this, typeof(Page), "LoadMap3", "OnDemand('" + strCenterPoint + "','" + strCenterPlace + "','" + intZoomLevel + "');", true);
    }


    /// <summary>
    /// This method is taking all the data of language table
    /// </summary>
    /// <returns></returns>
    public TList<Language> GetData()
    {
        TList<Language> objlanguage = DataRepository.LanguageProvider.GetAll();
        return objlanguage;
    }



    /// <summary>
    /// This function used for basic search.
    /// </summary>
    void ManageSession()
    {
        string WhereClause = string.Empty;
        string WhereClauseday2 = string.Empty;
        if (hdnCountry.Value != "0")
        {
            if (hdnCountry.Value != "")
            {
                if (WhereClause.Length > 0)
                {
                    WhereClause += " and ";
                    WhereClauseday2 += " and ";
                }
                WhereClause += HotelColumn.CountryId + "=" + hdnCountry.Value;
                WhereClauseday2 += HotelColumn.CountryId + "=" + hdnCountry.Value;
            }
        }

        if (hdnCityDetail.Value != "0")
        {
            if (hdnCityDetail.Value != "")
            {
                if (WhereClause.Length > 0)
                {
                    WhereClause += " and ";
                    WhereClauseday2 += " and ";
                }
                WhereClause += HotelColumn.CityId + "=" + hdnCityDetail.Value;
                WhereClauseday2 += HotelColumn.CityId + "=" + hdnCityDetail.Value;
            }
        }
        if (drpDuration.SelectedValue == "1")
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                if (drpDays.SelectedValue == "0")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (drpDays.SelectedValue == "1")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (drpDays.SelectedValue == "2")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
            }
        }
        if (drpDuration.SelectedValue == "2")
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                WhereClauseday2 += " and ";
                if (drpDays.SelectedValue == "0")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (drpDays.SelectedValue == "1")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (drpDays.SelectedValue == "2")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }


                if (drpDay2.SelectedValue == "0")
                {
                    WhereClauseday2 += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (drpDay2.SelectedValue == "1")
                {
                    WhereClauseday2 += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (drpDay2.SelectedValue == "2")
                {
                    WhereClauseday2 += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
            }
        }

        if (txtBookingDate.Text != "")
        {
            DateTime fromDate = new DateTime(Convert.ToInt32("20" + txtBookingDate.Text.Split('/')[2]), Convert.ToInt32(txtBookingDate.Text.Split('/')[1]), Convert.ToInt32(txtBookingDate.Text.Split('/')[0]));

            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                WhereClauseday2 += " and ";
            }
            WhereClause += AvailabilityColumn.AvailabilityDate + "='" + fromDate + "'" + " and " + " (DATEDIFF(day, GETDATE(),'" + fromDate + "')) >= " + AvailabilityColumn.LeadTimeForMeetingRoom + "";
            WhereClauseday2 += AvailabilityColumn.AvailabilityDate + "='" + fromDate.AddDays(1) + "'" + " and " + " (DATEDIFF(day, GETDATE(),'" + fromDate.AddDays(1) + "')) >= " + AvailabilityColumn.LeadTimeForMeetingRoom + "";
        }


        if (txtParticipant.Text != "" && txtParticipant.Text != "0")
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                WhereClauseday2 += " and ";
            }
            WhereClause += txtParticipant.Text + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + "";
            WhereClauseday2 += txtParticipant.Text + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + "";
        }

        if (Request.QueryString["wl"] != null)
        {

            string wlMappedHotelID = string.Empty;
            TList<WhiteLabelMappingWithHotel> objMapHotel = objwlMapping.GetHotelMappingByWhiteLabelId(Convert.ToInt64(Request.QueryString["wl"]));
            if (objMapHotel.Count > 0)
            {
                int Intindex = 0;
                for (Intindex = 0; Intindex <= objMapHotel.Count - 1; Intindex++)
                {
                    if (wlMappedHotelID == string.Empty)
                    {
                        wlMappedHotelID = objMapHotel[Intindex].HotelId.ToString();
                    }
                    else
                    {
                        wlMappedHotelID += "," + objMapHotel[Intindex].HotelId.ToString();
                    }

                    Session["WLHotel"] = wlMappedHotelID;
                }

                if (WhereClause.Length > 0)
                {
                    WhereClause += " and ";
                    WhereClauseday2 += " and ";
                }
                WhereClause += "HotelId IN (" + wlMappedHotelID + ")";
                WhereClauseday2 += "HotelId IN (" + wlMappedHotelID + ")";
            }
        }
        else
        {
            Session.Remove("WLHotel");
        }

        //if (Session["Myzone"] != null)
        if (Convert.ToString(Session["zone"]) != "" && Session["zone"] != null)
        {
            int zoneid = Convert.ToInt32(Session["zone"]);
            WhereClause += " and ";
            WhereClause += HotelColumn.ZoneId + "=" + zoneid;
        }

        //Send all search session value
        Session["Where"] = WhereClause;
        if (drpDuration.SelectedValue == "2")
        {
            Session["Where2"] = WhereClauseday2;
        }

        //Maintain session value for all controls 
        objBookingRequest = new BookingRequest();
        objBookingRequest.propCountry = hdnCountry.Value;
        objBookingRequest.propCity = hdnCityDetail.Value;
        objBookingRequest.propDate = txtBookingDate.Text;
        objBookingRequest.propDuration = drpDuration.SelectedValue;
        objBookingRequest.propDays = drpDays.SelectedValue;
        objBookingRequest.propDay2 = drpDay2.SelectedValue;
        //objBookingRequest.propParticipants = txtParticipant.Text;
        if (txtParticipant.Text != "")
        {
            objBookingRequest.propParticipants = txtParticipant.Text;
        }
        else
        {
            objBookingRequest.propParticipants = "0";
        }
        Session["masterInput"] = objBookingRequest;
        //Dictionary<long, string> participants = new Dictionary<long, string>();
        //participants.Add(1, objBookingRequest.propParticipants);
        //Session["participants"] = participants;
    }

    #endregion

    #region Event
    /// <summary>
    /// This event Bind the city dropdown according to selected countryid.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void drpCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpCountry.SelectedValue != "")
        {
            //BindCityDropdownByCountryID(Convert.ToInt32(drpCountry.SelectedValue));
            MiscWebService objMisc = new MiscWebService();
            drpCity.DataSource = objMisc.GetCityByCountryIDForFrontend(Convert.ToInt64(drpCountry.SelectedValue));
            drpCity.DataTextField = "Name";
            drpCity.DataValueField = "id";
            drpCity.DataBind();
            drpCity.Items.Insert(0, new ListItem(GetKeyResult("SELECTCITY"), "0"));
        }
        imgMap.Visible = true;
        ScriptManager.RegisterStartupScript(this, typeof(Page), "HideMap", "HideMapImage();", true);
        // HideMapImage
        //showlocationdiv.Style.Add("display", "none");
    }

    protected void drpCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(drpCity.SelectedValue) != 0 && Convert.ToInt32(drpCountry.SelectedValue) != 0)
        {
            GetCityHotel();
            //GetCenterPoint(Convert.ToInt32(drpCity.SelectedValue));
            imgMap.Visible = false;
        }
        else
        {
            imgMap.Visible = true;
        }

    }

    protected void drpDuration_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpDuration.SelectedValue == "2")
        {
            divDay2.Style.Add("display", "block");
        }
        else
        {
            divDay2.Style.Add("display", "none");
        }
    }

    protected void btnSerchOnMap_Click(object sender, EventArgs e)
    {
        Session.Remove("Mycountry");
        Session.Remove("Mycity");
        Session.Remove("Zone");
        Session.Remove("MeetingRoomConfig");
        Session.Remove("MeetingRoom");
        Session.Remove("Hotel");
        Session.Remove("RequestedmeetingroomConfig");
        Session.Remove("Requestedmeetingroom");
        Session.Remove("Requestedhotel");
        Session["Country"] = hdnCountry.Value;
        Session["City"] = hdnCityDetail.Value;
        Session.Remove("masterInput");
        ManageSession();
        Session.Remove("RequestID");
        Session.Remove("Request");
        Session.Remove("SearchID");
        Session.Remove("Search");
        Session["participants"] = new Dictionary<long, Dictionary<long, string>>();
        Session["PostBack"] = null;
        //SearchResult1.BindSearchResult();
        //SearchResult1.fillfilter();

        if (Request.QueryString["wl"] == null)
        {
            if (l != null)
            {
                if (Request.RawUrl.ToLower().Contains("search-results/" + l.Name.ToLower()))
                {
                    Response.Redirect(Request.RawUrl);
                }
                else
                {
                    Response.Redirect(SiteRootPath + "search-results/" + l.Name.ToLower());
                }
            }
            else
            {
                if (Request.RawUrl.ToLower().Contains("search-results/english"))
                {
                    Response.Redirect(Request.RawUrl);
                }
                else
                {
                    Response.Redirect(SiteRootPath + "search-results/english");
                }
            }
        }
        else
        {
            string[] date = (txtBookingDate.Text).Split('/');
            string query = "wl=" + Request.QueryString["wl"].ToString() + "&country=" + drpCountry.SelectedValue + "&city=" + drpCity.SelectedValue + "&d=" + date[0] + "&m=" + date[1] + "&y=" + date[2] + "&delegate=" + txtParticipant.Text + "&duration=" + drpDuration.SelectedValue + "&day1=" + drpDays.SelectedValue + "&day2=" + drpDay2.SelectedValue + "";
            Response.Redirect("searchresult.aspx?" + query);
        }


        //ScriptManager.RegisterStartupScript(this, typeof(Page), "", "var TabbedPanels1 = new Spry.Widget.TabbedPanels('TabbedPanels2');", true);
    }

    protected void imgMapSearch_Click(object sender, ImageClickEventArgs e)
    {
        Session["hdnMapLatitude"] = null;
        Session["hdnMapLongitude"] = null;
        Session["Country"] = hdnCountry.Value;
        Session["City"] = hdnCityDetail.Value;
        ManageSession();
        Session["PostBack"] = null;
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "find-by-map/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "find-by-map/english");
        }
        //Response.Redirect("FindByMap.aspx");
    }

    #endregion


    protected void hypManageProfile_Click(object sender, EventArgs e)
    {
        Session["task"] = "Edit";
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "manage-profile/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "manage-profile/english");
        }
    }
    protected void hypChangepassword_Click(object sender, EventArgs e)
    {
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "change-password/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "change-password/english");
        }
        //Response.Redirect("ChangePassword.aspx");
    }

    protected void hypListBookings_Click(object sender, EventArgs e)
    {
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "list-bookings/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "list-bookings/english");
        }
        //Response.Redirect("ListBookings.aspx");
    }

    protected void hypListRequests_Click(object sender, EventArgs e)
    {
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "list-requests/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "list-requests/english");
        }
        //Response.Redirect("ListRequests.aspx");
    }
}