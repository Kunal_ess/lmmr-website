﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using LMMR.Entities;
using LMMR.Data;
using LMMR.Business;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using log4net;
using log4net.Config;
using System.Configuration;
using System.Xml;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
#endregion

public partial class ListRequests : BasePage
{

    #region variables
    VList<ViewBookingHotels> vlist;
    VList<Viewbookingrequest> vlistreq;
    ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
    HotelManager objHotelManager = new HotelManager();
    string Typelink = "", AdminUser = "", Hotelid = "0", userId = "0";
    ILog logger = log4net.LogManager.GetLogger(typeof(ListRequests));
    WizardLinkSettingManager ObjWizardManager = new WizardLinkSettingManager();
    Createbooking objBooking = null;
    BookingManager bm = new BookingManager();
    CurrencyManager cm = new CurrencyManager();
    HotelManager objHotel = new HotelManager();
    PackagePricingManager objPackagePricingManager = new PackagePricingManager();
    Hotel objRequestHotel = new Hotel();

    int countMr = 0;
    CreateRequest objRequest = null;
    public List<PackageItemDetails> SelectedPackage
    {
        get;
        set;
    }
    public List<RequestExtra> SelectedExtra
    {
        get;
        set;
    }
    public TList<PackageItems> AllPackageItem
    {
        get;
        set;
    }
    public TList<PackageItems> AllFoodAndBravrages
    {
        get;
        set;
    }
    public TList<PackageItems> AllEquipments
    {
        get;
        set;
    }
    public TList<PackageByHotel> AllPackageByHotel
    {
        get;
        set;
    }

    public string CurrencySign
    {
        get;
        set;
    }
    public XmlDocument XMLLanguage
    {
        get { return (XmlDocument)ViewState["Language"]; }
        set { ViewState["Language"] = value; }
    }
    #endregion

    #region Pageload
    /// <summary>
    /// Method to manage the page load functionality.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Session check  
            divbookingdetails.Visible = false;
            divprintpdfllink.Visible = false;
            if (Session["LanguageID"] == null)
            {
                Session["LanguageID"] = 1;
            }
            if (Session["CurrentRestUser"] != null)
            {
                Users objUsers = (Users)Session["CurrentRestUser"];
                lblLoginUserName.Text = objUsers.FirstName + " " + objUsers.LastName;
                objUsers.LastLogin = DateTime.Now;
                lstLoginTime.Text = DateTime.Now.ToLongDateString();
                AfterLogin(objUsers.UserId, Convert.ToInt32(objUsers.Usertype));
            }
            //userId = "475"; //temporary 
            userId = Convert.ToString(Session["CurrentRestUserID"].ToString());
            //Session check
            if (IsPostBack)
            {
                ApplyPaging();

            }

            else if (!IsPostBack)
            {
                Session["LinkID"] = "lnkRequests";
                btnSubmit.ValidationGroup = "popup";
                ViewState["Where"] = string.Empty;
                ViewState["Order"] = string.Empty;
                ViewState["SearchAlpha"] = "all";
                //FetchListofHotel();
                bindgrid();
                bindAllDDLs();
                BindURLs();

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region fetchlistofhotels
    /// <summary>
    /// Method to fetch list of hotel for the particular user
    /// </summary>

    protected void FetchListofHotel()
    {
        try
        {
            AdminUser = Convert.ToString(Session["CurrentUserID"]);

            Typelink = Convert.ToString(Request.QueryString["Type"]);

            //-------------- changed according Therirry comment in SRS v1.2
            Hotelid = Convert.ToString(Session["CurrentHotelID"].ToString());
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion
    //----------------------------new(24/5/12)------------------------------------

    /// <summary>
    /// This method displays the user profile area.
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="userType"></param>
    public void AfterLogin(long userID, int userType)
    {
        beforeLogin.Style.Add("display", "none");
        afterLogin.Style.Add("display", "block");
        Session["registerType"] = userType;
    }

    public void BindURLs()
    {
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            joinToday.HRef = SiteRootPath + "join-today/" + l.Name.ToLower();
        }
        else
        {
            joinToday.HRef = SiteRootPath + "join-today/english";
        }
    }

    /// <summary>
    /// This method binds the city dropdownlist.
    /// </summary>
    public void bindcityDDL()
    {
        string whereclause = "CreatorId in (" + userId + ") and booktype in ( 1,2) "; // book type for request and booking
        vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, String.Empty).FindAllDistinct(ViewbookingrequestColumn.HotelId);
        GridViewRow header = grdViewBooking.HeaderRow;
        DropDownList cityDDL = header.FindControl("cityDDL") as DropDownList;
        cityDDL.DataTextField = "City";
        cityDDL.DataValueField = "Id";
        cityDDL.DataSource = objViewBooking_Hotel.getAllCities(vlistreq);
        cityDDL.DataBind();
        cityDDL.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--Select City--", "0"));
    }

    /// <summary>
    /// This method binds the hotel DDL.
    /// </summary>
    public void bindHotelDDL()
    {
        string whereclause = "CreatorId in (" + userId + ") and booktype in ( 1,2)"; // book type for request and booking
        vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, String.Empty).FindAllDistinct(ViewbookingrequestColumn.HotelId);
        GridViewRow header = grdViewBooking.HeaderRow;
        DropDownList hotelDDL = header.FindControl("hotelDDL") as DropDownList;
        hotelDDL.DataTextField = "Name";
        hotelDDL.DataValueField = "Id";
        hotelDDL.DataSource = objViewBooking_Hotel.getAllHotels(vlistreq);
        hotelDDL.DataBind();
        hotelDDL.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--Select Hotel--", "0"));
    }

    /// <summary>
    /// This method binds the meetingRoom DDL.
    /// </summary>
    public void bindMeetingRoomDDL()
    {
        string whereclause = "CreatorId in (" + userId + ") and booktype in ( 1,2)"; // book type for request and booking
        vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, String.Empty).FindAllDistinct(ViewbookingrequestColumn.Name);
        GridViewRow header = grdViewBooking.HeaderRow;
        DropDownList meetingRoomDDL = header.FindControl("meetingRoomDDL") as DropDownList;
        meetingRoomDDL.DataTextField = "Name";
        meetingRoomDDL.DataValueField = "MainMeetingRoomId";
        meetingRoomDDL.DataSource = vlistreq;
        meetingRoomDDL.DataBind();
        meetingRoomDDL.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--Select Meeting Room--", "0"));
    }

    /// <summary>
    /// This method binds bookingDate DDL.
    /// </summary>
    public void bindbookingDateDDL()
    {
        GridViewRow header = grdViewBooking.HeaderRow;
        DropDownList bookingDateDDL = header.FindControl("bookingDateDDL") as DropDownList;
        string whereclause = "CreatorId in (" + userId + ") and booktype in ( 1,2)"; // book type for request and booking
        vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, String.Empty).FindAllDistinct(ViewbookingrequestColumn.BookingDate);
        List<string> dateList = new List<string>();
        foreach (Viewbookingrequest temp in vlistreq)
        {
            string dt = temp.BookingDate.ToString();
            string[] str = dt.Split(new char[] { ' ' });
            string[] formatted = Regex.Split(str[0], "/");
            string newStr = "";
            newStr += formatted[1] + "/";
            newStr += formatted[0] + "/";
            newStr += formatted[2];
            if (!dateList.Contains(newStr))
                dateList.Add(newStr);
        }
        bookingDateDDL.DataSource = dateList;
        //bookingDateDDL.DataTextField = "Display";
        //bookingDateDDL.DataValueField = "Display";
        //bookingDateDDL.DataSource = vlistreq.Select(a => new { Display = Convert.ToDateTime(a.BookingDate).ToString("dd/M/yyyy") });
        bookingDateDDL.DataBind();
        bookingDateDDL.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--Select Request Date--", "0"));
    }

    /// <summary>
    /// This method binds value DDl.
    /// </summary>
    //public void bindvalueDDL()
    //{
    //    string whereclause = "CreatorId in (" + userId + ") and BookType  in ( 1 ,2)"; // book type for request and booking
    //    vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, String.Empty).FindAllDistinct(ViewbookingrequestColumn.FinalTotalPrice);
    //    GridViewRow header = grdViewBooking.HeaderRow;
    //    DropDownList valueDDL = header.FindControl("valueDDL") as DropDownList;
    //    valueDDL.DataTextField = "FinalTotalPrice";
    //    valueDDL.DataValueField = "FinalTotalPrice";
    //    valueDDL.DataSource = vlistreq;
    //    valueDDL.DataBind();
    //    valueDDL.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--Select Value--", "0"));
    //}

    /// <summary>
    /// This method gets the city name according to the hotelID. 
    /// </summary>
    /// <param name="hotelId"></param>
    /// <returns></returns>
    public string getCityName(string hotelId)
    {
        return objViewBooking_Hotel.getCityName(hotelId);
    }

    /// <summary>
    /// This method gets the hotel name on the basis of hotelID.
    /// </summary>
    /// <param name="hotelId"></param>
    /// <returns></returns>
    public string getHotelName(string hotelId)
    {
        return objViewBooking_Hotel.GetbyHotelID(Convert.ToInt32(hotelId)).Name;
    }

    /// <summary>
    /// This method gets the cancellation limit for a particular hotel on the basis of its ID.
    /// </summary>
    /// <param name="hotelId"></param>
    /// <returns></returns>
    public string getCancellationLimit(int hotelId)
    {
        long policyId = objViewBooking_Hotel.getSpecialPolicyId(hotelId);
        int cancellationLimit;
        if (policyId != 0)
        {
            cancellationLimit = Convert.ToInt32(objViewBooking_Hotel.getPolicy(policyId).CancelationLimit);
        }
        else
        {
            long countryId = objViewBooking_Hotel.getHotelCountryId(hotelId);
            cancellationLimit = Convert.ToInt32(objViewBooking_Hotel.getDefaultPolicy(countryId).CancelationLimit);
        }
        return cancellationLimit.ToString();
    }

    /// <summary>
    /// This is the event handler for value DDL SelectedIndexChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void valueDDL_selectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList valueDDL = (DropDownList)grdViewBooking.HeaderRow.FindControl("valueDDL");
            string whereclause = "CreatorId in (" + userId + ") and booktype in ( 1,2) " + " and FinalTotalPrice = " + valueDDL.SelectedItem.Value; // book type for request and booking
            string orderby = ViewbookingrequestColumn.FinalTotalPrice + " DESC";
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, orderby);
            grdViewBooking.DataSource = vlistreq;
            Session["latestGridState"] = vlistreq;
            grdViewBooking.DataBind();
            bindAllDDLs();
            if (grdViewBooking.Rows.Count > 0)
            {
                divprintpdfllink.Visible = true;
            }

            divbookingdetails.Visible = false;
            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This is the event handler for bookingDate DDL SelectedIndexChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void bookingDateDDL_selectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList bookingDateDDL = (DropDownList)grdViewBooking.HeaderRow.FindControl("bookingDateDDL");
            string whereclause = "CreatorId in (" + userId + ") and booktype in (1,2)"; // book type for request and booking
            string orderby = ViewbookingrequestColumn.FinalTotalPrice + " DESC";
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, orderby);
            VList<Viewbookingrequest> filtered = new VList<Viewbookingrequest>();
            foreach (Viewbookingrequest temp in vlistreq)
            {
                string dt = temp.BookingDate.ToString();
                string[] str = dt.Split(new char[] { ' ' });
                string[] formatted = Regex.Split(str[0], "/");
                string newStr = "";
                newStr += formatted[1] + "/";
                newStr += formatted[0] + "/";
                newStr += formatted[2];
                if (bookingDateDDL.SelectedValue == newStr)
                {
                    filtered.Add(temp);
                }
            }
            grdViewBooking.DataSource = filtered;
            Session["latestGridState"] = filtered;
            grdViewBooking.DataBind();
            bindAllDDLs();

            if (grdViewBooking.Rows.Count > 0)
            {
                divprintpdfllink.Visible = true;
            }

            divbookingdetails.Visible = false;
            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This is the event handler for meetingRoom DDL SelectedIndexChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void meetingRoomDDL_selectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList meetingRoomDDL = (DropDownList)grdViewBooking.HeaderRow.FindControl("meetingRoomDDL");
            string whereclause = "CreatorId in (" + userId + ") and booktype in (1,2) " + " and Name = '" + meetingRoomDDL.SelectedItem.Text + "'"; // book type for request and booking
            string orderby = ViewbookingrequestColumn.FinalTotalPrice + " DESC";
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, orderby);
            grdViewBooking.DataSource = vlistreq;
            Session["latestGridState"] = vlistreq;
            grdViewBooking.DataBind();
            bindAllDDLs();

            if (grdViewBooking.Rows.Count > 0)
            {
                divprintpdfllink.Visible = true;
            }

            divbookingdetails.Visible = false;
            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This is the event handler for chkInvoiceSent checkbox CheckedChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkInvoiceSent_CheckedChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow gr in grdViewBooking.Rows)
        {
            CheckBox chkInvoiceSent = (CheckBox)gr.FindControl("chkInvoiceSent");
            if (chkInvoiceSent.Checked == true)
            {
                long bookingId = Convert.ToInt64(chkInvoiceSent.ToolTip);
                objViewBooking_Hotel.CancelBooking(bookingId);
            }
        }
        //CheckBox chkInvoiceSent=(CheckBox)grdViewBooking.ro
        //if()
    }

    /// <summary>
    /// This is the event handler for hotel DDL SelectedIndexChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void hotelDDL_selectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList hotelDDL = (DropDownList)grdViewBooking.HeaderRow.FindControl("hotelDDL");
            string whereclause = "CreatorId in (" + userId + ") and booktype in ( 1,2) " + " and HotelId = " + hotelDDL.SelectedItem.Value; // book type for request and booking
            string orderby = ViewbookingrequestColumn.FinalTotalPrice + " DESC";
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, orderby);
            grdViewBooking.DataSource = vlistreq;
            Session["latestGridState"] = vlistreq;
            grdViewBooking.DataBind();
            bindAllDDLs();

            if (grdViewBooking.Rows.Count > 0)
            {
                divprintpdfllink.Visible = true;
            }

            divbookingdetails.Visible = false;
            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }


    }

    /// <summary>
    /// This is the event handler for city DDL SelectedIndexChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void cityDDL_selectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList cityDDL = (DropDownList)grdViewBooking.HeaderRow.FindControl("cityDDL");
            string whereclause = "CreatorId in (" + userId + ") and BookType in (1,2) "; // book type for request and booking
            string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
            VList<Viewbookingrequest> filteredSet = new VList<Viewbookingrequest>();
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, orderby);
            foreach (Viewbookingrequest temp in vlistreq)
            {
                long cityId = objViewBooking_Hotel.getCityId(temp.HotelId);
                if (cityId == Convert.ToInt64(cityDDL.SelectedItem.Value))
                {
                    filteredSet.Add(temp);
                }
            }
            grdViewBooking.DataSource = filteredSet;
            Session["latestGridState"] = filteredSet;
            grdViewBooking.DataBind();
            bindAllDDLs();
            if (grdViewBooking.Rows.Count > 0)
            {
                divprintpdfllink.Visible = true;
            }

            divbookingdetails.Visible = false;
            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This method handles the paging functionality.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void PageChange(object sender, EventArgs e)
    {
        try
        {
            string whereclaus = String.Empty;
            string orderby = String.Empty;

            HtmlAnchor anc = (sender as HtmlAnchor);
            var d = anc.InnerHtml;
            //ViewState["SearchAlpha"] = d;
            if (d.Trim() == "all")
            {
                whereclaus = ViewbookingrequestColumn.HotelName + " LIKE '%' ";
                ViewState["SearchAlpha"] = d;
            }
            else
            {
                whereclaus = ViewbookingrequestColumn.HotelName + " LIKE '" + d + "%' ";
                ViewState["SearchAlpha"] = d;
            }
            ViewState["Where2"] = whereclaus;
            ViewState["CurrentPage"] = 0;
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
            if (ViewState["Where"] != null)
            {
                FilterGridWithHeader(ViewState["Where"].ToString(), ViewState["Order"].ToString(), string.Empty);
            }
            else
            {
                FilterGridWithHeader(ViewState["Where2"].ToString(), ViewState["Order"].ToString(), string.Empty);
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    protected void grvClientContract_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            grdViewBooking.PageIndex = e.NewPageIndex;
            ViewState["CurrentPage"] = e.NewPageIndex;
            FilterGridWithHeader(Convert.ToString(ViewState["Where"]), Convert.ToString(ViewState["Order"]), Convert.ToString(ViewState["ClientName"]));
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This method filters the grid.
    /// </summary>
    /// <param name="wherec"></param>
    /// <param name="order"></param>
    /// <param name="clientNameStarts"></param>
    public void FilterGridWithHeader(string wherec, string order, string clientNameStarts)
    {
        try
        {
            string whereclause;
            whereclause = "CreatorId in (" + userId + ") and BookType  in ( 1 ,2)";
            if (wherec != "")
            {
                whereclause = "CreatorId in (" + userId + ") and BookType  in ( 1 ,2)" + " and " + wherec; // book type for request and booking
            }
            if (ViewState["Where2"] != null)
            {
                if (!string.IsNullOrEmpty(ViewState["Where2"].ToString().Trim()))
                {
                    if (!string.IsNullOrEmpty(whereclause.Trim()))
                    {
                        whereclause += " and ";
                    }
                    whereclause += ViewState["Where2"];
                }
            }
            string orderby = order;
            orderby = ViewbookingrequestColumn.BookingDate + " DESC";
            grdViewBooking.PageIndex = Convert.ToInt32(ViewState["CurrentPage"] == null ? "0" : ViewState["CurrentPage"]);
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, orderby);
            grdViewBooking.DataSource = vlistreq;
            Session["latestGridState"] = vlistreq;
            grdViewBooking.DataBind();
            if (grdViewBooking.Rows.Count > 0)
            {
                divprintpdfllink.Visible = true;
            }
            bindAllDDLs();
            ApplyPaging();
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This method calls the binding methods of all the DDL's in the header.
    /// </summary>
    public void bindAllDDLs()
    {
        bindcityDDL();
        bindHotelDDL();
        bindMeetingRoomDDL();
        bindbookingDateDDL();
       // bindvalueDDL();
    }

    /// <summary>
    /// This is the event handler for the searching button's Clicked event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtGo_Click(object sender, EventArgs e)
    {
        try
        {
            ViewState["SearchAlpha"] = "all";
            string whereclause = "CreatorId in (" + userId + ") and BookType  in ( 1 ,2)";
            if (txtBookingID.Text != "")
            {
                whereclause += " and Id = " + txtBookingID.Text.Trim(); // book type for request and booking
            }
            if (txtFrom.Text != "" && txtTo.Text != "")
            {
                //whereclause += " and ArrivalDate between convert(DATETIME, '" + txtFrom.Text + " and DepartureDate= " + txtTo.Text;
                // whereclause += " and ArrivalDate between convert(DATETIME, '" + txtFrom.Text + "' and convert(DATETIME , '" + txtTo.Text + "'";
                whereclause += " and ArrivalDate between convert(DATETIME, '" + txtFrom.Text + "',103) and convert(DATETIME , '" + txtTo.Text + "',103) ";
            }
            else
            {
                if (!String.IsNullOrEmpty(txtFrom.Text))
                {
                    whereclause += "and ArrivalDate >= convert(DATETIME, '" + txtFrom.Text + "',103)";
                }
                if (!String.IsNullOrEmpty(txtTo.Text))
                {
                    whereclause += "and ArrivalDate <= convert(DATETIME, '" + txtTo.Text + "',103)";
                }
            }
            string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, orderby);
            grdViewBooking.DataSource = vlistreq;
            Session["latestGridState"] = vlistreq;
            grdViewBooking.DataBind();
            bindAllDDLs();
            if (grdViewBooking.Rows.Count > 0)
            {
                divprintpdfllink.Visible = true;
            }

            divbookingdetails.Visible = false;
            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This is the event handler for the chkCancel checkbox CheckedChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkCancel_CheckedChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow gr in grdViewBooking.Rows)
        {
            CheckBox chkCancel = (CheckBox)gr.FindControl("chkCancel");
            if (chkCancel.Checked == true)
            {
                objViewBooking_Hotel.CancelBooking(Convert.ToInt64(chkCancel.ToolTip));
            }
        }
    }

    /// <summary>
    /// This is the event handler for the hypManageProfile hyperlink Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void hypManageProfile_Click(object sender, EventArgs e)
    {
        //Session["task"] = "Edit";
        //Response.Redirect("Registration.aspx");

        Session["task"] = "Edit";
        //Response.Redirect("Registration.aspx");
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "manage-profile/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "manage-profile/english");
        }
    }

    /// <summary>
    /// This is the event handler for the hypChangepassword hyperlink Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void hypChangepassword_Click(object sender, EventArgs e)
    {
        //Response.Redirect("ChangePassword.aspx");
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "change-password/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "change-password/english");
        }
    }

    /// <summary>
    /// This is the event handler for the hypListBookings hyperlink Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void hypListBookings_Click(object sender, EventArgs e)
    {
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "list-bookings/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "list-bookings/english");
        }
    }

    /// <summary>
    /// This is the event handler for the hypListRequests hyperlink Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void hypListRequests_Click(object sender, EventArgs e)
    {
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "list-requests/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "list-requests/english");
        }
    }

    /// <summary>
    /// This is the event handler for lbtReset LinkButton Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtReset_Click(object sender, EventArgs e)
    {
        txtBookingID.Text = "";
        txtFrom.Text = "";
        txtTo.Text = "";
        bindgrid();
        bindAllDDLs();
        ViewState["SearchAlpha"] = "all";
    }


    // -------------------------------------------------------------------

    #region BindGrid
    /// <summary>
    /// Method to bind the grid
    /// </summary>
    /// <param name="linktype"></param>
    /// linktype = 1 the its new request 5- old request  

    protected void bindgrid()
    {
        try
        {
            Typelink = Convert.ToString(Request.QueryString["Type"]);
            //FetchListofHotel();
            int totalcount = 0;
            string whereclaus = "";
            whereclaus = "CreatorId in (" + userId + ") and BookType  in ( 1 ,2)"; ; // book type for request and booking
            string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclaus, orderby);
            grdViewBooking.DataSource = vlistreq;
            Session["latestGridState"] = vlistreq;
            grdViewBooking.DataBind();
            bindAllDDLs();
            if (grdViewBooking.Rows.Count > 0)
            {
                divprintpdfllink.Visible = true;
            }

            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region PageIndexChanging
    /// <summary>
    /// PageIndex Changing of grid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdViewBooking_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            grdViewBooking.PageIndex = e.NewPageIndex;
            ViewState["CurrentPage"] = e.NewPageIndex;
            bindgrid();
            bindAllDDLs();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region View Request Details
    /// <summary>
    /// Method to view details of Request would be seen
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lblRefNo_Click(object sender, EventArgs e)
    {
        //Changed The Code For Using Usercontrol:Pratik//
        try
        {
            divprintpdfllink.Visible = true;
            LinkButton lnk = (LinkButton)sender;
            //-- viewstate object
            ViewState["BookingID"] = lnk.Text;
            
            Booking obj = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(ViewState["BookingID"].ToString()));
            if (obj.BookType == 1)
            {
                requestDetails1.BindHotel(Convert.ToInt32(lnk.Text));
                
                requestDetails1.Visible = true;              
                divbookingdetails.Visible = true;                
                bookingDetails.Visible = false;
            }
            else if (obj.BookType == 2)
            {
                bookingDetails.BindBooking(Convert.ToInt32(lnk.Text));
                divbookingdetails.Visible = true;
                bookingDetails.Visible = true;
                requestDetails1.Visible = false;
            }








        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region fillbedroom details
    /// <summary>
    /// Method to get bedroom details
    /// </summary>
    protected void FillbedroomDetails()
    {
        try
        {
            // BookedBedRoom object
            TList<BookedBedRoom> objBookedBR = objViewBooking_Hotel.getbookedBedroom(Convert.ToInt64(Convert.ToString(ViewState["BookingID"])));

            foreach (BookedBedRoom br in objBookedBR)
            {

                //lblBedroomType.Text = Enum.GetName(typeof(BedRoomType), br.BedRoomIdSource.Types); // Enum.GetName();
                //lblChekOut.Text = String.Format("{0:dd/MM/yyyy}", br.CheckOut);
                //lblCheckIn.Text = String.Format("{0:dd/MM/yyyy}", br.CheckIn);
                //lblpersonName.Text = br.PersonName;
                //lblNote.Text = br.Note;

                //lblbedroomtotalprice.Text = String.Format("{0:#,###}", br.Total);

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region ItemDataBound
    /// <summary>
    /// ItemDataBound of repeater
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rpmain_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                Label lblbmrid = e.Item.FindControl("lblbmrid") as Label;
                Label lblPackageName = e.Item.FindControl("lblPackageName") as Label;
                Label lbltotalpackage = e.Item.FindControl("lbltotalpackage") as Label;
                Label lblextratotal = e.Item.FindControl("lblextratotal") as Label;
                GridView grdDetailspackage = e.Item.FindControl("grdDetailspackage") as GridView;
                GridView grdextra = e.Item.FindControl("grdextra") as GridView;
                GridView grdequipment = e.Item.FindControl("grdequipment") as GridView;

                //BuildPackageConfigure object
                TList<BuildPackageConfigure> obbuildpack = objViewBooking_Hotel.getPackageDetails(Convert.ToInt64(lblbmrid.Text));
                //BuildMeetingConfigure object
                TList<BuildMeetingConfigure> objBuildMeetingConfigure = objViewBooking_Hotel.getextra(Convert.ToInt64(lblbmrid.Text));

                grdextra.DataSource = objBuildMeetingConfigure.FindAll(a => a.PackageIdSource.IsExtra == true);
                grdextra.DataBind();
                grdequipment.DataSource = objBuildMeetingConfigure.FindAll(a => Convert.ToInt32(a.PackageIdSource.ItemType) == (int)ItemType.Equipment);
                grdequipment.DataBind();
                int cntextra = 0;
                foreach (BuildMeetingConfigure bmc in objBuildMeetingConfigure.FindAll(a => Convert.ToInt32(a.PackageIdSource.ItemType) == (int)ItemType.Equipment))
                {
                    cntextra += Convert.ToInt32(String.Format("{0:#,###}", bmc.TotalPrice));

                }
                lblextratotal.Text = Convert.ToString(cntextra);
                foreach (BuildPackageConfigure bpc in obbuildpack)
                {

                    lblPackageName.Text = bpc.PackageItemIdSource.PackageName;
                    lbltotalpackage.Text = String.Format("{0:#,###}", bpc.TotalPrice);

                    //BuildPackageConfigureDesc object
                    TList<BuildPackageConfigureDesc> obdesc = objViewBooking_Hotel.getpackagedetails(bpc.Id);
                    grdDetailspackage.DataSource = obdesc;
                    grdDetailspackage.DataBind();

                }


            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region Printdetails
    /// <summary>
    /// to print the details
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkPrint_Click(object sender, EventArgs e)
    {
        try
        {
            PrintHelper.PrintWebControl(Divdetails);
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region save as pdf
    /// <summary>
    /// method to save as pdf
    /// </summary>

    protected void lnkSavePDF_Click(object sender, EventArgs e)
    {
        try
        {
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=UserDetails.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            PrepareGridViewForExport(Divdetails);
            Divdetails.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region VerifyRenderingInServerForm
    /// <summary>
    /// method to VerifyRenderingInServerForm
    /// </summary>

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    #endregion

    #region PrepareGridViewForExport
    /// <summary>
    /// method to PrepareGridViewForExport
    /// </summary>
    private void PrepareGridViewForExport(Control gv)
    {

        try
        {
            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {

                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].GetType() == typeof(System.Web.UI.HtmlControls.HtmlAnchor))
                {

                    l.Text = (gv.Controls[i] as System.Web.UI.HtmlControls.HtmlAnchor).InnerText;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(DropDownList))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;

                    if ((gv.Controls[i] as DropDownList).SelectedItem.Text == "--Select City--")
                    {
                        l.Text = "City";
                    }
                    if ((gv.Controls[i] as DropDownList).SelectedItem.Text == "--Select Hotel--")
                    {
                        l.Text = "Hotel/Facility";
                    }
                    if ((gv.Controls[i] as DropDownList).SelectedItem.Text == "--Select Meeting Room--")
                    {
                        l.Text = "Meeting Room";
                    }
                    if ((gv.Controls[i] as DropDownList).SelectedItem.Text == "--Select Request Date--")
                    {
                        l.Text = "Request Date";
                    }
                    if ((gv.Controls[i] as DropDownList).SelectedItem.Text == "--Select Value--")
                    {
                        l.Text = "Value";
                    }

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(GridView))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareGridViewForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    // gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Repeater))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareGridViewForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Panel))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareGridViewForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Table))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareGridViewForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }


                else if (gv.Controls[i].GetType() == typeof(CheckBox))
                {

                    l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(HyperLink))
                {

                    l.Text = (gv.Controls[i] as HyperLink).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                //else if (gv.Controls[i].GetType() == typeof(Label))
                //{

                //    l.Text = (gv.Controls[i] as Label).Text;

                //    gv.Controls.Remove(gv.Controls[i]);

                //    gv.Controls.AddAt(i, l);

                //}
                else if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.Image))
                {

                    // l.Text = (gv.Controls[i] as Image).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareGridViewForExport(gv.Controls[i]);

                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region Bookingbind


    List<VatCollection> _VatCalculation = new List<VatCollection>();
    public List<VatCollection> VatCalculation
    {
        get
        {
            if (_VatCalculation == null)
            {
                _VatCalculation = new List<VatCollection>();
            }
            return _VatCalculation;
        }
        set
        {
            _VatCalculation = value;
        }
    }



    //public void BindHotel(long hotelID)
    //{
    //    try
    //    {
    //        rptHotel.DataSource = objRequest.HotelList.Where(a => a.MeetingroomList.Count > 0 && a.HotelID == hotelID);
    //        rptHotel.DataBind();

    //        lblBookedDays.Text = objRequest.Duration == 1 ? objRequest.Duration + " Day" : objRequest.Duration + " Days";

    //        //lblBookedDays.Text = objRequest.ArivalDate.ToString("dd MMM yyyy");
    //        //lblBookedDays1.Text = objRequest.DepartureDate.ToString("dd MMM yyyy");
    //        Int64 intHotelID = objRequest.HotelList[0].HotelID;
    //        if (objRequest.PackageID != 0)
    //        {
    //            pnlPackage.Visible = true;
    //            PackageMaster packagedetails = objPackagePricingManager.GetAllPackageName().Where(a => a.Id == objRequest.PackageID).FirstOrDefault();
    //            if (packagedetails != null)
    //            {
    //                lblPackageName.Text = packagedetails.PackageName;

    //                if (packagedetails.PackageName == "Standard")
    //                {
    //                    lblpackageDescription.Text = "Package includes main meeting room rental, note-pads, pencils, flipchart and mineral water Morning coffee break, Sandwich buffet with salads, Afternoon coffee break, Softdrinks, coffee and tea for half day packages, either morning or afternoon break is not included only as from min. 10 people.";

    //                }
    //                else if (packagedetails.PackageName == "Favourite")
    //                {
    //                    lblpackageDescription.Text = "Package includes main meeting room rental, note-pads, pencils, flipchart and mineral water Morning coffee break, 2-course menu, Afternoon coffee break, softdrinks, coffee and tea for half day packages, either morning or afternoon break is not included.";
    //                }
    //                else if (packagedetails.PackageName == "Elegant")
    //                {
    //                    lblpackageDescription.Text = "Package includes main meeting room rental, note-pads, pencils, flipchart and mineral water Morning coffee break, Hot & Cold buffet, Afternoon coffee break, softdrinks, coffee and tea for half day packages, either morning or afternoon break is not included only as from min. 20 people.";
    //                }
    //            }
    //            AllPackageItem = objPackagePricingManager.GetAllPackageItems();
    //            rptPackageItem.DataSource = objPackagePricingManager.GetPackageItemsByPackageID(objRequest.PackageID);
    //            rptPackageItem.DataBind();
    //            if (objRequest.ExtraList.Count > 0)
    //            {
    //                //Package Selection.

    //                pnlExtra.Visible = true;
    //                rptExtras.DataSource = objRequest.ExtraList;
    //                rptExtras.DataBind();//check AllPackageItem.Where(a => a.IsExtra == true)
    //            }
    //            else
    //            {
    //                pnlExtra.Visible = false;
    //            }
    //            pnlFoodAndBravrages.Visible = false;
    //        }
    //        else
    //        {
    //            pnlPackage.Visible = false;

    //            if (objRequest.BuildYourMeetingroomList.Count > 0)
    //            {
    //                AllFoodAndBravrages = objPackagePricingManager.GetAllFoodBeveragesItems(intHotelID);
    //                pnlFoodAndBravrages.Visible = true;
    //                rptFoodandBravragesDay.DataSource = objRequest.DaysList;
    //                rptFoodandBravragesDay.DataBind();
    //            }
    //            else
    //            {
    //                pnlFoodAndBravrages.Visible = false;
    //            }
    //        }


    //        if (objRequest.EquipmentList.Count > 0)
    //        {
    //            AllEquipments = objPackagePricingManager.GetAllEquipmentItems(intHotelID);
    //            pnlEquipment.Visible = true;
    //            rptEquipmentDay.DataSource = objRequest.DaysList;
    //            rptEquipmentDay.DataBind();
    //        }
    //        else
    //        {
    //            pnlEquipment.Visible = false;
    //        }
    //        if (objRequest.IsAccomodation)
    //        {
    //            pnlAccomodation.Visible = true;
    //            lblaccomodationQun.Text = Convert.ToString(objRequest.RequestAccomodationList.QuantityDouble);
    //        }
    //        else
    //        {
    //            pnlAccomodation.Visible = true;
    //        }
    //        lblSpecialRequest.Text = objRequest.SpecialRequest;
    //    }
    //    catch (Exception ex)
    //    {
    //        logger.Error(ex);

    //    }
    //}


    protected void rptPackageItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label txtQuantity = (Label)e.Item.FindControl("txtQuantity");
            HiddenField hdnItemId = (HiddenField)e.Item.FindControl("hdnItemId");
            PackageItemMapping pim = e.Item.DataItem as PackageItemMapping;
            if (pim != null)
            {
                PackageItems p = AllPackageItem.Where(a => a.Id == pim.ItemId).FirstOrDefault();
                if (p != null)
                {
                    hdnItemId.Value = Convert.ToString(p.Id);
                    lblItemName.Text = p.ItemName;
                    PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblDescription.Text = pdesc.ItemDescription;
                    }
                    else
                    {
                        lblDescription.Text = "";
                    }
                    PackageItemDetails pid = objRequest.PackageItemList.Where(a => a.ItemID == p.Id).FirstOrDefault();
                    if (pid != null)
                    {
                        txtQuantity.Text = Convert.ToString(pid.Quantity);
                    }
                    else
                    {
                        txtQuantity.Text = "0";
                    }
                }
            }
        }
    }

    protected void rptHotel_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblHotelName = (Label)e.Item.FindControl("lblHotelName");
            //Label lblRefNo = (Label)e.Item.FindControl("lblRefNo");
            Repeater rptMeetingroom = (Repeater)e.Item.FindControl("rptMeetingroom");
            BuildHotelsRequest b = e.Item.DataItem as BuildHotelsRequest;
            if (b != null)
            {
                Hotel objHtl = objHotel.GetHotelDetailsById(b.HotelID);
                lblHotelName.Text = objHtl.Name;
                rptMeetingroom.DataSource = b.MeetingroomList;
                rptMeetingroom.DataBind();
            }
        }
    }

    protected void rptMeetingroom_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //  Label lblIndex = (Label)e.Item.FindControl("lblIndex");
            Label lblMeetingRoomName = (Label)e.Item.FindControl("lblMeetingRoomName");
            Label lblConfigurationType = (Label)e.Item.FindControl("lblConfigurationType");
            Label lblMaxandMinCapacity = (Label)e.Item.FindControl("lblMaxandMinCapacity");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            Label lblIsMain = (Label)e.Item.FindControl("lblIsMain");
            BuildMeetingRoomRequest brm = e.Item.DataItem as BuildMeetingRoomRequest;
            if (brm != null)
            {
                // lblIndex.Text = (e.Item.ItemIndex + 1).ToString();                
                MeetingRoom objMeetingroom = objHotel.GetMeetingRoomDetailsById(brm.MeetingRoomID);
                MeetingRoomDesc objDesc = objMeetingroom.MeetingRoomDescCollection.Where(a => a.LanguageId == Convert.ToInt32(Session["LanguageID"])).FirstOrDefault();
                MeetingRoomConfig objMrConfig = objMeetingroom.MeetingRoomConfigCollection.Where(a => a.Id == brm.ConfigurationID).FirstOrDefault();
                lblMeetingRoomName.Text = objMeetingroom.Name;
                lblConfigurationType.Text = (objMrConfig.RoomShapeId == (int)RoomShape.Boardroom ? RoomShape.Boardroom.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Classroom ? RoomShape.Classroom.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Cocktail ? RoomShape.Cocktail.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.School ? RoomShape.School.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Theatre ? RoomShape.Theatre.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.UShape ? RoomShape.UShape.ToString() : RoomShape.Boardroom.ToString());
                lblMaxandMinCapacity.Text = objMrConfig.MinCapacity + " - " + objMrConfig.MaxCapicity;
                lblQuantity.Text = brm.Quantity.ToString();
                lblIsMain.Text = brm.IsMain == true ? "Main" : "Breakout";
            }
        }
    }

    protected void rptExtras_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            RequestExtra r = e.Item.DataItem as RequestExtra;
            if (r != null)
            {
                PackageItems objPackage = AllPackageItem.Where(a => a.IsExtra == true && a.Id == r.ItemID).FirstOrDefault();
                if (objPackage != null)
                {
                    lblItemName.Text = objPackage.ItemName;
                    PackageDescription pdesc = objPackage.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblDescription.Text = pdesc.ItemDescription;
                    }
                    else
                    {
                        lblDescription.Text = "";
                    }
                    lblQuantity.Text = Convert.ToString(r.Quantity);
                }
            }
        }
    }

    protected void rptFoodandBravragesDay_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblSelectDay = (Label)e.Item.FindControl("lblSelectDay");
            Repeater rptFoodandBravragesItem = (Repeater)e.Item.FindControl("rptFoodandBravragesItem");
            NumberOfDays nod = e.Item.DataItem as NumberOfDays;
            if (nod != null)
            {
                rptFoodandBravragesItem.DataSource = objRequest.BuildYourMeetingroomList.Where(a => a.SelectDay == nod.SelectedDay);
                rptFoodandBravragesItem.DataBind();
            }
        }
    }

    protected void rptFoodandBravragesItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {


        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            BuildYourMeetingroomRequest bm = e.Item.DataItem as BuildYourMeetingroomRequest;
            if (bm != null)
            {
                PackageItems p = AllFoodAndBravrages.Where(a => a.Id == bm.ItemID).FirstOrDefault();

                if (p != null)
                {
                    lblItemName.Text = p.ItemName;
                    PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblDescription.Text = pdesc.ItemDescription;
                    }
                    else
                    {
                        lblDescription.Text = "";
                    }
                    lblQuantity.Text = Convert.ToString(bm.Quantity);
                }
            }
        }
    }

    protected void rptEquipmentDay_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblSelectDay = (Label)e.Item.FindControl("lblSelectDay");
            Repeater rptEquipmentItem = (Repeater)e.Item.FindControl("rptEquipmentItem");
            NumberOfDays nod = e.Item.DataItem as NumberOfDays;
            if (nod != null)
            {
                lblSelectDay.Text = nod.SelectedDay.ToString();
                rptEquipmentItem.DataSource = objRequest.EquipmentList.Where(a => a.SelectedDay == nod.SelectedDay);
                rptEquipmentItem.DataBind();
            }
        }
    }

    protected void rptEquipmentItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            RequestEquipment re = e.Item.DataItem as RequestEquipment;
            if (re != null)
            {
                PackageItems p = AllEquipments.Where(a => a.Id == re.ItemID).FirstOrDefault();
                if (p != null)
                {
                    lblItemName.Text = p.ItemName;
                    PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblDescription.Text = pdesc.ItemDescription;
                    }
                    else
                    {
                        lblDescription.Text = "";
                    }
                    lblQuantity.Text = Convert.ToString(re.Quantity);
                }
            }
        }
    }

    #endregion

    #region Language
    //This is used for language conversion for static contants.
    public string GetKeyResult(string key)
    {
        //if (XMLLanguage == null)
        //{
        //    XMLLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
        //}
        //XmlNode nodes = XMLLanguage.SelectSingleNode("items/item[@key='" + Key + "']");
        //return nodes.InnerText;//
        return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    }
    #endregion

    #region Go for Calculation
    public void Calculate(Createbooking objCreateBook)
    {
        decimal TotalMeetingroomPrice = 0;
        decimal TotalPackagePrice = 0;
        decimal TotalBuildYourPackagePrice = 0;
        decimal TotalEquipmentPrice = 0;
        decimal TotalExtraPrice = 0;
        bool PackageSelected = false;
        VatCalculation = null;
        VatCalculation = new List<VatCollection>();
        if (objCreateBook != null)
        {
            foreach (BookedMR objb in objCreateBook.MeetingroomList)
            {
                foreach (BookedMrConfig objconfig in objb.MrDetails)
                {
                    TotalMeetingroomPrice = objconfig.NoOfParticepant * objconfig.MeetingroomPrice;
                    //Build mr
                    foreach (BuildYourMR bmr in objconfig.BuildManageMRLst)
                    {
                        TotalBuildYourPackagePrice += bmr.ItemPrice * bmr.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == bmr.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = bmr.vatpercent;
                            v.CalculatedPrice = bmr.ItemPrice * bmr.Quantity * bmr.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == bmr.vatpercent).FirstOrDefault().CalculatedPrice += bmr.ItemPrice * bmr.Quantity * bmr.vatpercent / 100;
                        }
                    }
                    PackageSelected = Convert.ToBoolean(objconfig.PackageID);
                    //Equipment
                    foreach (ManageEquipment eqp in objconfig.EquipmentLst)
                    {
                        TotalEquipmentPrice += eqp.ItemPrice * eqp.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = eqp.vatpercent;
                            v.CalculatedPrice = eqp.ItemPrice * eqp.Quantity * eqp.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault().CalculatedPrice += eqp.ItemPrice * eqp.Quantity * eqp.vatpercent / 100;
                        }
                    }
                    //Manage Extras
                    foreach (ManageExtras ext in objconfig.ManageExtrasLst)
                    {
                        TotalExtraPrice += ext.ItemPrice * ext.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == ext.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = ext.vatpercent;
                            v.CalculatedPrice = ext.ItemPrice * ext.Quantity * ext.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == ext.vatpercent).FirstOrDefault().CalculatedPrice += ext.ItemPrice * ext.Quantity * ext.vatpercent / 100;
                        }
                    }
                    //Manage Package Item
                    foreach (ManagePackageItem pck in objconfig.ManagePackageLst)
                    {
                        TotalPackagePrice += pck.ItemPrice * pck.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == pck.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = pck.vatpercent;
                            v.CalculatedPrice = pck.ItemPrice * pck.Quantity * pck.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == pck.vatpercent).FirstOrDefault().CalculatedPrice += pck.ItemPrice * pck.Quantity * pck.vatpercent / 100;
                        }
                    }

                    //if (PackageSelected)
                    //{
                    //    objBooking.TotalBookingPrice += TotalPackagePrice + TotalExtraPrice + TotalEquipmentPrice + TotalBuildYourPackagePrice;
                    //}
                    //else
                    //{
                    //    objBooking.TotalBookingPrice += TotalMeetingroomPrice + TotalEquipmentPrice + TotalBuildYourPackagePrice;
                    //}
                }
            }

        }
    }
    #endregion

    #region Process
    /// <summary>
    /// method to move to process
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Lnkmovetoprocessed_Click(object sender, EventArgs e)
    {
        try
        {
            //  changestatus();
            Booking objbooking = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(ViewState["BookingID"].ToString()));
            objbooking.RevenueReason = ddlreason.SelectedItem.Text; //decline reason is save din revenue reason
            objbooking.RequestStatus = 5;
            if (objViewBooking_Hotel.updatecheckComm(objbooking))
            {
                ddlreason.SelectedIndex = 0;
            }
            // txtrealvalue.Text = "";


            divbookingdetails.Visible = false;
            //divbutton.Visible = false;
            Response.Redirect("ViewRequest.aspx?type=" + Typelink);
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion


    #region RowDataBound
    /// <summary>
    /// RowDataBound of grid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdViewBooking_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            //foreach (GridViewRow gr in grdViewBooking.Rows)
            //{
            string rowID = String.Empty;
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
            {
                //e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#FF9'");
                //e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;");
                //ViewState["rowID"] = e.Row.RowIndex;


                GridViewRow gr = e.Row;
                LinkButton btnchkcommision = (LinkButton)gr.FindControl("btnchkcommision");
                Label lblArrivalDt = (Label)gr.FindControl("lblArrivalDt");
                LinkButton lblRefNo = (LinkButton)gr.FindControl("lblRefNo");
                TList<BookedMeetingRoom> bookedMR = objViewBooking_Hotel.getbookedmeetingroom(Convert.ToInt64(lblRefNo.Text));
                Label lblDepartureDt = (Label)gr.FindControl("lblDepartureDt");
                Label lblExpiryDt = (Label)gr.FindControl("lblExpiryDt");
                Label lblCityName = (Label)gr.FindControl("lblCityName");
                Label lblHotelName = (Label)gr.FindControl("lblHotelName");
                Label lblBookingDt = (Label)gr.FindControl("lblBookingDt");
                Label lblCancellationLimit = (Label)gr.FindControl("lblCancellationLimit");
                CheckBox chkCancel = (CheckBox)gr.FindControl("chkCancel");
                lblCityName.Text = getCityName(lblCityName.ToolTip);
                //lblCancellationLimit.Text = getCancellationLimit(Convert.ToInt32(lblCancellationLimit.ToolTip));


                #region calculation of finalamt after renvenue was adjusted
                //Booking bookcomm = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(btnchkcommision.CommandArgument));

                //Label lblFinalTotal = (Label)gr.FindControl("lblFinalTotal");
                //decimal finalamt = Convert.ToDecimal(bookcomm.FinalTotalPrice);

                ////if (bookcomm.RevenueAmount != null)
                ////{
                ////    finalamt = (decimal)bookcomm.RevenueAmount;
                ////    finalamt = Convert.ToDecimal(bookcomm.FinalTotalPrice) / (1 + finalamt / 100);
                ////}
                //lblFinalTotal.Text = String.Format("{0:#,###}", (Math.Round(finalamt, 2)));
                #endregion


            }
            //}

        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region Confirm Commsion
    /// <summary>
    /// when check commsion was confirmed
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>    

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            Booking objbooking = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(ViewState["ChkcommBookingID"].ToString()));
            if (Convert.ToString(ViewState["popup"]) == "1")
            {
                if (chkmeetingNotheld.Checked)
                {
                    objbooking.RequestStatus = (int)BookingRequestStatus.Cancel;
                    objbooking.RevenueReason = "Meeting was not held";
                }
                else
                {
                    #region check file upload
                    string strPlanName = string.Empty;
                    if (ulPlan.HasFile)
                    {
                        string fileExtension = Path.GetExtension(ulPlan.PostedFile.FileName.ToString());

                        if (fileExtension == ".xls" || fileExtension == ".xlsx" || fileExtension == ".doc" || fileExtension == ".docx" || fileExtension.ToLower() == ".pdf")
                        {
                            strPlanName = Path.GetFileName(ulPlan.FileName);
                        }
                        else
                        {
                            divmessage.InnerHtml = "Please select file in (word,excel,pdf) formats only";
                            divmessage.Attributes.Add("class", "error");
                            divmessage.Style.Add("display", "block");
                            modalcheckcomm.Show();
                            return;
                        }

                        if (ulPlan.PostedFile.ContentLength > 1048576)
                        {
                            divmessage.InnerHtml = "Filesize of Supporting Document is too large. Maximum file size permitted is 1 MB.";
                            divmessage.Attributes.Add("class", "error");
                            divmessage.Style.Add("display", "block");
                            modalcheckcomm.Show();
                            return;
                        }
                        if (string.IsNullOrEmpty(txtrealvalue.Text))
                        {
                            divmessage.InnerHtml = "Kindly upload the Supporting Document and the Netto value";
                            divmessage.Attributes.Add("class", "error");
                            divmessage.Style.Add("display", "block");
                            modalcheckcomm.Show();
                            return;
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(txtrealvalue.Text))
                            divmessage.InnerHtml = "Kindly upload the Supporting Document and the Netto value";
                        else
                            divmessage.InnerHtml = "Kindly upload the Supporting Document.";
                        divmessage.Attributes.Add("class", "error");
                        divmessage.Style.Add("display", "block");
                        modalcheckcomm.Show();
                        return;
                    }

                    #endregion

                    ulPlan.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "SupportDocNetto/") + strPlanName);
                    objbooking.RequestStatus = (int)BookingRequestStatus.Definite;
                    if (string.IsNullOrEmpty(txtrealvalue.Text))
                        txtrealvalue.Text = "0";
                    //  decimal netvalue = Convert.ToDecimal(txtrealvalue.Text);
                    // decimal revenue = (Convert.ToDecimal(objbooking.FinalTotalPrice) / (netvalue - 1)) * 100;
                    // objbooking.RevenueAmount = netvalue;
                    objbooking.FinalTotalPrice = Convert.ToDecimal(txtrealvalue.Text); // updated
                    objbooking.RevenueAmount = Convert.ToDecimal(txtrealvalue.Text); // updated
                    objbooking.RevenueReason = ddlreason.SelectedItem.Text;
                    objbooking.SupportingDocNetto = strPlanName;
                }


            }
            else
            {
                objbooking.RequestStatus = Convert.ToInt32(ddlstatus.SelectedValue);

            }
            if (objViewBooking_Hotel.updatecheckComm(objbooking))
            {

                FetchListofHotel();
                bindgrid();
                modalcheckcomm.Hide();
                ModalPopupCheck.Hide();

            }

        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        finally
        {
            txtrealvalue.Text = "";
            ddlreason.SelectedIndex = 0;
            ddlstatus.SelectedIndex = 0;

        }

    }
    #endregion
    #region Check Commission
    /// <summary>
    /// when check commsion was checked
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  
    protected void btnchkcommision_Click(object sender, EventArgs e)
    {
        try
        {

            LinkButton btnchkcomm = (LinkButton)sender;
            ViewState["ChkcommBookingID"] = btnchkcomm.CommandArgument;

            if (btnchkcomm.Text.ToLower() == "change")
            {
                divmessage.Style.Add("display", "none");
                ModalPopupCheck.Show();
                modalcheckcomm.Hide();
                ViewState["popup"] = "0";
            }
            else
            {
                //  txtrealvalue.ReadOnly = true;
                divmessage.Style.Add("display", "none");
                modalcheckcomm.Show();
                ModalPopupCheck.Hide();
                ViewState["popup"] = "1";
            }


        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion





    #region Decline
    /// <summary>
    /// decline a request
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  
    protected void lnkdecline_Click(object sender, EventArgs e)
    {
        try
        {
            Booking objbooking = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(ViewState["BookingID"].ToString()));


            objbooking.RevenueReason = "";

            objbooking.RequestStatus = 2;
            if (objViewBooking_Hotel.updatecheckComm(objbooking))
            {

                FetchListofHotel();
                bindgrid();
                Response.Redirect("ViewRequest.aspx?type=" + Typelink);
            }
            // txtrealvalue.Text = "";
            ddlreason.SelectedIndex = 0;
            bindgrid();
            divbookingdetails.Visible = false;
            //divbutton.Visible = false;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion


    #region Apply Paging
    /// <summary>
    /// Method to apply Paging in grid
    /// </summary>
    private void ApplyPaging()
    {
        try
        {
            GridViewRow row = grdViewBooking.TopPagerRow;
            if (row != null)
            {
                PlaceHolder ph;
                LinkButton lnkPaging;
                LinkButton lnkPrevPage;
                LinkButton lnkNextPage;
                lnkPrevPage = new LinkButton();
                lnkPrevPage.CssClass = "pre";
                lnkPrevPage.Width = Unit.Pixel(73);
                lnkPrevPage.CommandName = "Page";
                lnkPrevPage.CommandArgument = "prev";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPrevPage);
                if (grdViewBooking.PageIndex == 0)
                {
                    lnkPrevPage.Enabled = false;

                }
                for (int i = 1; i <= grdViewBooking.PageCount; i++)
                {
                    lnkPaging = new LinkButton();
                    if (ViewState["CurrentPage"] != null)
                    {
                        if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    else
                    {
                        if (i == 1)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    lnkPaging.Width = Unit.Pixel(16);
                    lnkPaging.Text = i.ToString();
                    lnkPaging.CommandName = "Page";
                    lnkPaging.CommandArgument = i.ToString();
                    if (i == grdViewBooking.PageIndex + 1)
                        ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkPaging);
                }
                lnkNextPage = new LinkButton();
                lnkNextPage.CssClass = "nex";
                lnkNextPage.Width = Unit.Pixel(42);
                lnkNextPage.CommandName = "Page";
                lnkNextPage.CommandArgument = "next";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkNextPage);
                ph = (PlaceHolder)row.FindControl("ph");
                if (grdViewBooking.PageIndex == grdViewBooking.PageCount - 1)
                {
                    lnkNextPage.Enabled = false;

                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion




    #region Cancel Button Click
    /// <summary>
    /// Modal popup cancel event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            divmessage.InnerHtml = "";
            divmessage.Style.Add("display", "none");
            modalcheckcomm.Hide();
            ModalPopupCheck.Hide();
            ddlstatus.SelectedIndex = 0;
            txtrealvalue.Text = "";
            chkmeetingNotheld.Checked = false;
            ddlreason.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    #endregion

    #region SelectedIndexChanged
    /// <summary>
    /// method to SelectedIndexChanged
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  
    protected void ddlstatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (grdViewBooking.Rows.Count > 0)
            {

                DropDownList ddl = (DropDownList)sender;

                foreach (GridViewRow row in grdViewBooking.Rows)
                {

                    if (((Label)row.FindControl("lblstatus")).ToolTip.IndexOf(ddl.SelectedValue) >= 0)
                        row.Visible = true;
                    else if ("All" == ddl.SelectedItem.Text)
                        row.Visible = true;
                    else
                        row.Visible = false;



                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }


    #endregion


    #region Search
    /// <summary>
    /// Search
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  
    //protected void lbtSearch_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        FetchListofHotel();
    //        int totalcount = 0;
    //        string whereclaus = "";
    //        if (Typelink == "1")
    //            whereclaus = "hotelid in (" + Hotelid + ") and requeststatus='" + Typelink + "' and BookType  in ( 1 ,2) "; // book type for request and booking
    //        else
    //            whereclaus = "hotelid in (" + Hotelid + ") and requeststatus not in (1) and BookType  in ( 1 ,2) "; // book type for request and booking

    //        if (!string.IsNullOrEmpty(txtFromdate.Text) && !string.IsNullOrEmpty(txtTodate.Text))
    //            whereclaus += " and arrivaldate between convert(DATETIME, '" + txtFromdate.Text + "',103) and convert(DATETIME, '" + txtTodate.Text + "',103) ";


    //        string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
    //        vlistreq = objViewBooking_Hotel.Bindgrid(whereclaus, orderby);
    //        grdViewBooking.DataSource = vlistreq;
    //        grdViewBooking.DataBind();

    //        #region Disableaction header
    //        if (Typelink == "5")
    //        {
    //            grdViewBooking.Columns[10].Visible = true;
    //            grdViewBooking.Columns[11].Visible = true;
    //        }
    //        else
    //        {
    //            grdViewBooking.Columns[10].Visible = false;
    //            grdViewBooking.Columns[11].Visible = false;
    //        }

    //        #endregion
    //        ApplyPaging();
    //    }
    //    catch (Exception ex)
    //    {
    //        logger.Error(ex);
    //    }
    //}
    #endregion



    #region sAVE AS PDF
    /// <summary>
    /// method to save as pdf
    /// </summary>
    protected void uiLinkButtonSaveAsPdfGrid_Click(object sender, EventArgs e)
    {
        try
        {
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=UserDetails.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            grdViewBooking.AllowPaging = false;
            grdViewBooking.DataSource = Session["latestGridState"];
            Control phhide = grdViewBooking.TopPagerRow.FindControl("ph");
            phhide.Visible = false;
            //bindgrid();
            //grdViewBooking.Columns[11].Visible = false;
            PrepareGridViewForExport(grdViewBooking);
            grdViewBooking.GridLines = GridLines.Both;
            grdViewBooking.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }


    }
    #endregion

    #region RowCreated
    /// <summary>
    /// method to RowCreated
    /// </summary>
    protected void grdViewBooking_RowCreated(object sender, GridViewRowEventArgs e)
    {
        try
        {
            string rowID = String.Empty;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#FF9'");
                //e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;");
                ViewState["rowID"] = e.Row.RowIndex;

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion
}