﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeMaster.master" AutoEventWireup="true"
    CodeFile="RequestStep1.aspx.cs" Inherits="RequestStep1" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntLeft" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMainbody" runat="Server">
    <!--today-date START HERE-->
    <div class="today-date">
        <asp:Panel ID="pnlLogin" runat="server" >
        <div class="today-date-left">
            <b><%= GetKeyResult("TODAY")%>:</b> <asp:Label ID="lblDateLogin" runat="server"></asp:Label>
        </div>
        <div class="today-date-right">
            <%= GetKeyResult("YOUARELOGGEDINAS")%>: <b><asp:Label ID="lblUserName" runat="server"></asp:Label></b>
        </div>
        </asp:Panel>
    </div>
    <!--today-date ENDS HERE-->
    <!--request-step-mainbody START HERE-->
    <div class="request-step-mainbody">
        <!--request-stepbody START HERE-->
        <div class="request-stepbody">
            <div class="request-step1">
                <span><%= GetKeyResult("STEP1")%>. </span><%= GetKeyResult("BASKETREQUEST")%>
            </div>
            <div class="request-step2">
                <span><%= GetKeyResult("STEP2")%>.</span> <%= GetKeyResult("EXTRAINFO")%>
            </div>
            <div class="request-step3">
                <span><%= GetKeyResult("STEP3")%>.</span> <%= GetKeyResult("RECAP")%>
            </div>
            <div class="request-step4">
                <span><%= GetKeyResult("STEP4")%>.</span> <%= GetKeyResult("SENDING")%> 
            </div>
        </div>
        <!--request-stepbody ENDS HERE-->
        <!--request-heading1 START HERE-->
        <div class="request-heading1">
            <h1>
                <span><%= GetKeyResult("YOURBASKETREQUEST")%></span></h1>
        </div>
        <!--request-heading1 ENDS HERE-->
        <div id="errormessage" class="error" style="display:none;" ></div>
        <!--request-date-body START HERE-->
        <div class="request-date-body">
            <div class="request-arrival-date">
                <span><%= GetKeyResult("ARRIVALDATE")%>:</span>
                <asp:Label ID="lblArrival" runat="server"></asp:Label>
            </div>
            <div class="request-departure-date">
                <span><%= GetKeyResult("DEPARTUREDATE")%>:</span> <asp:Label ID="lblDeparture" runat="server"></asp:Label>
            </div>
            <div class="request-participants">
                <%--<span>Participants:</span> 45--%>
            </div>
        </div>
        <asp:Repeater ID="rptDays" runat="server" OnItemDataBound="rptDays_ItemDataBound">
            <ItemTemplate>
                <div class="request-day-body">
                    <div class="request-day-box1">
                        <span><%= GetKeyResult("DURATION")%>:</span>&nbsp;&nbsp;
                        <asp:Label ID="lblDuration" runat="server"></asp:Label>
                    </div>
                    <div class="request-day-box2">
                        <div class="request-day-box2-box1">
                            <b><%= GetKeyResult("DAY")%> <asp:Label ID="lblDay" runat="server"></asp:Label>:</b></div>
                        <div id="Fullday">
                            <asp:DropDownList ID="drpSelectedTime" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div id="Fulldaytime">
                            <asp:DropDownList ID="drpStart" runat="server" ></asp:DropDownList>
                        </div>
                        <div id="Fulldaytime1">
                            <asp:DropDownList ID="drpEnd" runat="server" ></asp:DropDownList>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <!--request-date-body ENDS HERE-->
        <!--request-hotal-body START HERE-->
        <div class="request-hotal-body">
            <ul>
                <asp:Repeater ID="dtLstHotel" runat="server" 
                    OnItemDataBound="dtLstHotel_ItemDataBound" >
                    <ItemTemplate><asp:HiddenField ID="hdnHotelID" runat="server" />
                        <li>
                            <div class="request-hotal-body-inner">
                                <!--request-hotal-body-inner-top START HERE-->
                                <div class="request-hotal-body-inner-top">
                                    <div class="request-hotal-body-inner-top-left">
                                        <asp:Label ID="lblIndex" runat="server"></asp:Label>.</div>
                                    <div class="request-hotal-body-inner-top-mid">
                                        <h2>
                                            <asp:Label ID="lblHotelname" runat="server"></asp:Label></h2>
                                        <div class="h2star3">
                                            <%--<img src="<%= SiteRootPath %>images/star3.png">--%><asp:Image ID="imgStars" runat="server" /></div>
                                        <div class="small-heading">
                                            <asp:Label ID="lblAddress" runat="server"></asp:Label></div>
                                    </div>
                                    <div class="request-hotal-body-inner-top-right">
                                        <span class="long"><asp:HiddenField ID="hdnLong" runat="server" /></span>
                                        <span class="lat"><asp:HiddenField ID="hdnLat" runat="server" /></span>
                                        <a href="javascript:void(0);" class="showMap">
                                            <img align="absmiddle" src="<%= SiteRootPath %>images/map-icon.png"> <%= GetKeyResult("SHOWMAP")%></a></div>
                                </div>
                                <!--request-hotal-body-inner-top ENDS HERE-->
                                <!--request-hotal-inner-headingSTART HERE-->
                                <div class="request-hotal-inner-heading">
                                    <div class="request-hotal-inner-heading1">
                                        <%= GetKeyResult("MEETINGROOMS")%>
                                    </div>
                                    <div class="request-hotal-inner-heading2">
                                        <%= GetKeyResult("DESCRIPTION")%>
                                    </div>
                                    <div class="request-hotal-inner-heading3">
                                        <%= GetKeyResult("QTY")%>
                                    </div>
                                    <div class="request-hotal-inner-heading4">
                                        <%= GetKeyResult("CHOOSECATEGORY")%>
                                    </div>
                                </div>
                                <!--request-hotal-inner-heading ENDS HERE-->
                                <!--request-hotal-inner-detail START HERE-->
                                <div class="request-hotal-inner-detail">
                                    <ul>
                                        <asp:Repeater ID="dtLstMeetingroom" runat="server" OnItemDataBound="dtLstMeetingroom_ItemDataBound" onitemcommand="dtLstMeetingroom_ItemCommand" >
                                            <ItemTemplate><asp:HiddenField ID="hdnMeetingRoomID" runat="server" />
                                                <li>
                                                    <div class="request-hotal-inner-detail-heading">
                                                        <div class="request-hotal-inner-detail-heading-left">
                                                            <asp:Label ID="lblIndex" runat="server" ></asp:Label>. <asp:Label ID="lblMeetingRoomName" runat="server"></asp:Label>
                                                        </div>
                                                        <div class="request-hotal-inner-detail-heading-right">
                                                            <asp:ImageButton ID="btnDelete" runat="server" OnClientClick="return confirm('Do you want to delete this meeting room?');"
                                                                ImageUrl="~/images/delete-ol-btn.png" CssClass="ImageFloat" CommandName="DeleteMeetingroom"/>
                                                        </div>
                                                    </div>
                                                    <!--request-hotal-inner-detail-room START HERE-->
                                                    <div class="request-hotal-inner-detail-room">
                                                        <div class="request-hotal-inner-detail-room-inner">
                                                            <div class="request-hotal-inner-detail-room1">
                                                                <asp:Image ID="imgMrImage" runat="server" Width="69px" Height="69px" />
                                                            </div>
                                                            <div class="request-hotal-inner-detail-room2">
                                                                <p>
                                                                    <asp:Label ID="lblConfigurationType" runat="server"></asp:Label></p>
                                                                <p class="last">
                                                                    <asp:Label ID="lblMaxandMinCapacity" runat="server"></asp:Label></p>
                                                                <div>
                                                                    <asp:HyperLink ID="hypPlan" runat="server"><%= GetKeyResult("SEEPLAN")%></asp:HyperLink>
                                                                </div>
                                                            </div>
                                                            <div class="request-hotal-inner-detail-room3">
                                                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="inputbox" MaxLength="3"></asp:TextBox>
                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtQuantity"
                            ValidChars="0123456789" runat="server">
                        </asp:FilteredTextBoxExtender>
                                                            </div>
                                                            <div class="request-hotal-inner-detail-room4">
                                                                <p >
                                                                    <asp:RadioButton ID="rbtnMain" runat="server" GroupName="cat" onclick="return onMain(this);" />
                                                                    <asp:Label ID="lblMain" runat="server" ><%= GetKeyResult("MAIN")%>&nbsp;<img id="imgmain" src="Images/help-2.png" class="information" title='<%= GetKeyResult("MAINMEETINGROOMINFO")%>' /></asp:Label></p>
                                                                <p >
                                                                    <asp:RadioButton ID="rbtnBreakDown" runat="server" GroupName="cat" onclick="return onMain(this);" />
                                                                    <asp:Label ID="lblBreakDown" runat="server" ><%= GetKeyResult("BREAKOUT")%>&nbsp;<img  id="imgbreakout"  src="Images/help-2.png" class="information" title='<%= GetKeyResult("BREAKOUTMEETINGROOMINFO")%>' /></asp:Label></p>
                                                                    
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--request-hotal-inner-detail-room ENDS HERE-->
                                                </li>
                                            </ItemTemplate>
                                            <AlternatingItemTemplate><asp:HiddenField ID="hdnMeetingRoomID" runat="server" />
                                                <li>
                                                    <div class="request-hotal-inner-detail-heading1">
                                                        <div class="request-hotal-inner-detail-heading-left">
                                                            <asp:Label ID="lblIndex" runat="server" ></asp:Label>. <asp:Label ID="lblMeetingRoomName" runat="server"></asp:Label>
                                                        </div>
                                                        <div class="request-hotal-inner-detail-heading-right">
                                                            <asp:ImageButton ID="btnDelete" runat="server" OnClientClick="return confirm('Do you want to delete this meeting room?');"
                                                                ImageUrl="~/images/delete-ol-btn.png" CssClass="ImageFloat" CommandName="DeleteMeetingroom"/>
                                                        </div>
                                                    </div>
                                                    <!--request-hotal-inner-detail-room START HERE-->
                                                    <div class="request-hotal-inner-detail-room">
                                                        <div class="request-hotal-inner-detail-room-inner">
                                                            <div class="request-hotal-inner-detail-room1">
                                                                <asp:Image ID="imgMrImage" runat="server" Width="69px" Height="69px" />
                                                            </div>
                                                            <div class="request-hotal-inner-detail-room2">
                                                                <p>
                                                                    <asp:Label ID="lblConfigurationType" runat="server"></asp:Label></p>
                                                                <p class="last">
                                                                    <asp:Label ID="lblMaxandMinCapacity" runat="server"></asp:Label></p>
                                                                <div>
                                                                    <asp:HyperLink ID="hypPlan" runat="server"><%= GetKeyResult("SEEPLAN")%></asp:HyperLink>
                                                                </div>
                                                            </div>
                                                            <div class="request-hotal-inner-detail-room3">
                                                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="inputbox" MaxLength="3"></asp:TextBox>
                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtQuantity"
                            ValidChars="0123456789" runat="server">
                        </asp:FilteredTextBoxExtender>
                                                            </div>
                                                            <div class="request-hotal-inner-detail-room4">
                                                                <p >
                                                                    <asp:RadioButton ID="rbtnMain" runat="server" GroupName="cat2" onclick="return onMain(this);" />
                                                                    <asp:Label ID="lblMain" runat="server"><%= GetKeyResult("MAIN")%>&nbsp;<img  id="imgmain2" src="Images/help-2.png" class="information" title='<%= GetKeyResult("MAINMEETINGROOMINFO")%>' /></asp:Label></p>
                                                                <p>
                                                                    <asp:RadioButton ID="rbtnBreakDown" runat="server" GroupName="cat2" onclick="return onMain(this);" />
                                                                    <asp:Label ID="lblBreakDown" runat="server" ><%= GetKeyResult("BREAKOUT")%>&nbsp;<img  id="imgbreakout2" src="Images/help-2.png" class="information" title='<%= GetKeyResult("BREAKOUTMEETINGROOMINFO")%>' /></asp:Label></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--request-hotal-inner-detail-room ENDS HERE-->
                                                </li>
                                            </AlternatingItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </div>
                                <!--request-hotal-inner-detail ENDS HERE-->
                            </div>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
        <!--request-hotal-body ENDS HERE-->
    </div>
    <!--request-step-mainbody ENDS HERE-->
    <!--booking-heading START HERE-->
    <div class="xyz">
    </div>
    <!--booking-heading ENDS HERE-->

    <!-- Code added For WL -->
    <div id="divJoinToday-overlay">
    </div>
    <div id="divJoinToday">
        <div class="popup-top">
        </div>
        <div class="popup-mid">
            <div class="popup-mid-inner">
                <div class="error" id="errorPopup" runat="server">
                </div>
                <div>
                    <asp:RadioButton ID="rbregister" runat="server" GroupName="register" Text="Quick Register"
                        onclick="return registerORLogin();" Checked="true" /><br />
                    <asp:RadioButton ID="rblogin" runat="server" GroupName="register" Text="Using Lastminutemeetingroom.com Login credentials"
                        onclick="return registerORLogin();" />
                </div>
                <table cellspacing="5" id="tableRegister" style="margin-left: 20px;">
                    <tr>
                        <td>
                            <b>
                                <%= GetKeyResult("FIRSTNAME")%>:</b>
                        </td>
                        <td>
                            <asp:TextBox ID="txtFirstName" runat="server" value="" class="inputregister" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>
                                <%= GetKeyResult("LASTNAME")%>:</b>
                        </td>
                        <td>
                            <asp:TextBox ID="txtLastName" runat="server" value="" class="inputregister" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>
                                <%= GetKeyResult("COMPANY")%>:</b>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCompanyName" runat="server" value="" class="inputregister" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>
                                <%= GetKeyResult("EMAIL")%>:</b>
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmail" runat="server" value="" class="inputregister" MaxLength="250"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>
                                <%= GetKeyResult("PHONE")%>:</b>
                        </td>
                        <td>
                            <asp:TextBox ID="txtphoneNo" runat="server" value="" class="inputregister" MaxLength="15"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtphoneNo"
                                ValidChars="-0123456789">
                            </asp:FilteredTextBoxExtender>
                        </td>
                    </tr>
                </table>
                <div class="subscribe-btn1" id="divRegisterButton" style="padding-top: 0;">
                    <div class="save-cancel-btn1 button_section">
                        <asp:LinkButton ID="btnRegister" runat="server" CssClass="send-request-btn" OnClick="btnRegister_Click"><%= GetKeyResult("CONTINUE")%></asp:LinkButton>
                        &nbsp;or&nbsp;
                        <asp:LinkButton ID="lnkCnacel" runat="server" CssClass="cancelpop" OnClientClick="return closeRegisterORLoginPopUp();"><%=GetKeyResult("CANCEL") %></asp:LinkButton>
                    </div>
                </div>
                <table id="tableLogin" cellspacing="5" style="display: none; margin-left: 20px;">
                    <tr>
                        <td>
                            <b>
                                <%= GetKeyResult("LOGIN")%>:</b>
                        </td>
                        <td>
                            <asp:TextBox ID="txtLoginID" runat="server" value="" class="inputregister" MaxLength="250"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>
                                <%= GetKeyResult("PASSWORD")%>:</b>
                        </td>
                        <td>
                            <asp:TextBox ID="txtPassword" runat="server" value="" class="inputregister" MaxLength="250" TextMode="Password"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <div class="subscribe-btn" id="divLoginButton" style="display: none">
                    <div class="save-cancel-btn1 button_section">
                        <asp:LinkButton ID="btnLogin" runat="server" CssClass="select" OnClick="btnLogin_Click"
                            OnClientClick="javascript: _gaq.push(['_trackPageview', 'RegisterOK-Venue.aspx']);"><%=GetKeyResult("LOGIN")%></asp:LinkButton>
                        &nbsp;or&nbsp;
                        <asp:LinkButton ID="btnLoginCancel" runat="server" CssClass="cancelpop" OnClientClick="return closeRegisterORLoginPopUp();"><%=GetKeyResult("CANCEL") %></asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <div class="popup-bottom">
        </div>
        <script language="javascript" type="text/javascript">

            function showlogipopup(msg) {
                jQuery('#tableRegister').hide();
                jQuery('#tableLogin').show();
                jQuery('#divRegisterButton').hide();
                jQuery('#divLoginButton').show();
                if (jQuery("#<%= errorPopup.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= errorPopup.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                }
                jQuery("#<%= errorPopup.ClientID %>").html(msg);
                jQuery("#<%= errorPopup.ClientID %>").show();
            }

            function showregistrationpopup(msg) {
                jQuery('#tableLogin').hide();
                jQuery('#tableRegister').show();
                jQuery('#divRegisterButton').show();
                jQuery('#divLoginButton').hide();
                if (jQuery("#<%= errorPopup.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= errorPopup.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                }
                jQuery("#<%= errorPopup.ClientID %>").html(msg);
                jQuery("#<%= errorPopup.ClientID %>").show();

            }
            function registerORLogin() {
                if (jQuery('#<%=rbregister.ClientID %>:checked').val() == "rbregister") {
                    jQuery('#tableLogin').hide();
                    jQuery('#tableRegister').show();
                    jQuery('#divRegisterButton').show();
                    jQuery('#divLoginButton').hide();
                } else {
                    jQuery('#tableRegister').hide();
                    jQuery('#tableLogin').show();
                    jQuery('#divRegisterButton').hide();
                    jQuery('#divLoginButton').show();
                }

                if (jQuery("#<%= errorPopup.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= errorPopup.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                }
                jQuery("#<%= errorPopup.ClientID %>").html("");
                jQuery("#<%= errorPopup.ClientID %>").hide();
            }

            function openRegisterORLoginPopUp() {

                if (jQuery("#<%= errorPopup.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= errorPopup.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                }
                jQuery("#<%= errorPopup.ClientID %>").html("");
                jQuery("#<%= errorPopup.ClientID %>").hide();
                //jQuery("#Loding_overlay").show();
                //jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                jQuery("#divJoinToday").show();
                jQuery("#divJoinToday-overlay").show();
                return false;
            }

            function closeRegisterORLoginPopUp() {

                document.getElementsByTagName('html')[0].style.overflow = 'auto';
                document.getElementById('divJoinToday').style.display = 'none';
                document.getElementById('divJoinToday-overlay').style.display = 'none';
                jQuery('#<%=txtFirstName.ClientID %>').val('');
                jQuery('#<%=txtLastName.ClientID %>').val('');
                jQuery('#<%=txtCompanyName.ClientID %>').val('');
                jQuery('#<%=txtEmail.ClientID %>').val('');
                jQuery('#<%=txtphoneNo.ClientID %>').val('');
                jQuery('#<%=txtLoginID.ClientID %>').val('');
                jQuery('#<%=txtPassword.ClientID %>').val('');
                jQuery('#<%=rbregister.ClientID %>').attr('checked', true);
                registerORLogin();
                return false;
            }

            jQuery("#<%= btnRegister.ClientID %>").bind("click", function () {
                if (jQuery("#<%= errorPopup.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= errorPopup.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                }
                var isvalid = true;
                var errormessage = "";
                var FirstName = jQuery("#<%= txtFirstName.ClientID %>").val();
                if (FirstName.length <= 0 || FirstName == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>"
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("FIRSTNAMEERRORMESSAGE")%>';
                    isvalid = false;
                }

                var LastName = jQuery("#<%= txtLastName.ClientID %>").val();
                if (LastName.length <= 0 || LastName == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>"
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("LASTNAMEERRORMESSAGE")%>';
                    isvalid = false;
                }

                var emailp1 = jQuery("#<%= txtEmail.ClientID %>").val();
                if (emailp1.length <= 0 || emailp1 == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("EMAILIDERRORMESSAGE")%>';
                    isvalid = false;
                }

                else {
                    if (!validateEmail(emailp1)) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += '<%= GetKeyResultForJavaScript("EMAILVALADITYERRORMESSAGE")%>';
                        isvalid = false;
                    }
                }

                var Phone = jQuery("#<%=txtphoneNo.ClientID %>").val();
                if (Phone.length <= 0 || Phone == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>"
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("ERRORPHONE")%>';
                    isvalid = false;
                }

                if (!isvalid) {
                    jQuery("#<%= errorPopup.ClientID %>").show();
                    jQuery("#<%= errorPopup.ClientID %>").html(errormessage);
                    return false;
                }
                jQuery("#Loding_overlay").show();
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                jQuery("#divJoinToday").show();
                jQuery("#divJoinToday-overlay").show();
            });

            jQuery("#<%= btnLogin.ClientID %>").bind("click", function () {
                if (jQuery("#<%= errorPopup.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= errorPopup.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                }
                var isvalid = true;
                var errormessage = "";
                var Email = jQuery("#<%=txtLoginID.ClientID %>").val();
                if (Email.length == 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("EMAILISREQUIRED")%>';
                    isvalid = false;
                }
                else {
                    if (!validateEmail(Email)) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += '<%= GetKeyResultForJavaScript("ENTERVALIDEMAIL")%>';
                        isvalid = false;
                    }
                }

                var password = jQuery("#<%=txtPassword.ClientID %>").val();
                if (password.length == 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("PASSWORDISREQUIRED")%>';
                    isvalid = false;
                }
                if (!isvalid) {
                    jQuery("#<%= errorPopup.ClientID %>").show();
                    jQuery("#<%= errorPopup.ClientID %>").html(errormessage);
                    return false;
                }

                jQuery("#Loding_overlay").show();
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                jQuery("#divJoinToday").show();
                jQuery("#divJoinToday-overlay").show();
            });
            function validateEmail(txtEmail) {
                var a = txtEmail;
                var filter = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
                if (filter.test(a)) {
                    return true;
                }
                else {
                    return false;
                }
            }
        </script>
    </div>
    <!-- End WL Code -->
    <!--next button START HERE-->
    <div class="next-button">
        <%--<a href="#" class="step-pre-blue-btn">Previous step</a>--%> &nbsp;<asp:LinkButton 
            ID="lnkNext" runat="server" CssClass="step-next-blue-btn" 
            onclick="lnkNext_Click" ><%= GetKeyResult("NEXTSTEP")%></asp:LinkButton><%--<a href="#" class="step-next-blue-btn">Next
            step</a>--%>
    </div>
    <!--next button ENDS HERE-->
    <div id="maploc" style="background-color: #E5E3DF;border: 2px solid green;float: right;height: 176px;left: 923px;overflow: hidden;position: absolute;top: 242px;width: 249px;z-index: 50;display:none;" ></div>
    <input type="image" src="Images/close.png" style="float: right;position: absolute;z-index: 55;display:none;" class="close"/>
    <script language="javascript" type="text/javascript">
        var loginUser = '<%= Session["CurrentRestUserID"] %>';
        jQuery(document).ready(function () {

            jQuery("input:text").bind("focus", function () {
                var p = jQuery(this).val();
                if (parseInt(p) == 0) {
                    jQuery(this).val('');
                }
            });
            jQuery("#<%= lnkNext.ClientID %>").bind("click", function () {
                var strQu = getQuerystring('wl', 'no');
                if (strQu == 'no') {
                    var IsValid = true;
                    var errorMessage = "";
                    jQuery(".request-hotal-body").find("input:text").each(function () {
                        var checkminmax = jQuery(this).parent().parent().find(".last span").html();
                        var p = jQuery(this).val();
                        if (p == '' || p.length <= 0 || parseInt(p, 2) == 0 || isNaN(p)) {
                            if (errorMessage.length > 0) {
                                errorMessage += "<br/>";
                            }
                            errorMessage += '<%= GetKeyResultForJavaScript("PLEASEENTERVALIDVALUES")%>';
                            IsValid = false;
                        }
                        //                    else if (parseInt(p, 10) < parseInt(checkminmax.split('-')[0], 10) || parseInt(p,10) > parseInt(checkminmax.split('-')[1], 10)) {
                        //                        if (errorMessage.length > 0) {
                        //                            errorMessage += "<br/>";
                        //                        }
                        //                        errorMessage += '<%= GetKeyResult("PLEASEENTERVALUEBETWEEN")%>' + checkminmax.split('-')[0] + ' <%= GetKeyResult("AND")%> ' + checkminmax.split('-')[1] + '.';
                        //                        IsValid = false;
                        //                    }
                    });
                    if (!IsValid) {
                        //alert(errorMessage);
                        jQuery(".error").show();
                        jQuery(".error").html(errorMessage);
                        var offseterror = jQuery(".error").offset();
                        jQuery("body").scrollTop(offseterror.top);
                        jQuery("html").scrollTop(offseterror.top);
                        return false;
                    }

                    if (loginUser == '') {
                        alert('<%= GetKeyResultForJavaScript("LOGINFORREQUEST")%>.');
                        jQuery(".error").hide();
                        jQuery(".error").html('');
                        jQuery("body").scrollTop(0);
                        jQuery("html").scrollTop(0);
                    }
                    else {
                        jQuery(".error").hide();
                        jQuery(".error").html('');
                        jQuery("body").scrollTop(0);
                        jQuery("html").scrollTop(0);
                    }
                    return true;
                }
                else {

                    var IsValid = true;
                    var errorMessage = "";
                    jQuery(".request-hotal-body").find("input:text").each(function () {
                        var checkminmax = jQuery(this).parent().parent().find(".last span").html();
                        var p = jQuery(this).val();
                        if (p == '' || p.length <= 0 || parseInt(p, 2) == 0 || isNaN(p)) {
                            if (errorMessage.length > 0) {
                                errorMessage += "<br/>";
                            }
                            errorMessage += '<%= GetKeyResultForJavaScript("PLEASEENTERVALIDVALUES")%>';
                            IsValid = false;
                        }

                    });
                    if (!IsValid) {
                        //alert(errorMessage);
                        jQuery(".error").show();
                        jQuery(".error").html(errorMessage);
                        var offseterror = jQuery(".error").offset();
                        jQuery("body").scrollTop(offseterror.top);
                        jQuery("html").scrollTop(offseterror.top);
                        return false;
                    }

                    if (loginUser == '') {
                        openRegisterORLoginPopUp();
                        jQuery(".error").hide();
                        jQuery(".error").html('');
                        jQuery("body").scrollTop(0);
                        jQuery("html").scrollTop(0);
                        return false;
                    }
                    else {
                        jQuery(".error").hide();
                        jQuery(".error").html('');
                        jQuery("body").scrollTop(0);
                        jQuery("html").scrollTop(0);
                    }
                    return true;
                }
            });

        });

        function getQuerystring(key, default_) {
            if (default_ == null) default_ = "";
            key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
            var qs = regex.exec(window.location.href);
            if (qs == null)
                return default_;
            else
                return qs[1];
        }

        function onMain(sender) {
            var senderid = sender.id;
            var check = senderid.split('_');
            var last = check[check.length-1];
            if (parseInt(last, 10) == 0) {
                if (check[check.length - 2] == "rbtnMain") {
                    if (jQuery("#" + senderid.replace("rbtnMain_0", "rbtnBreakDown_1")) != undefined) {
                        var mystringB = "#" + senderid.replace("rbtnMain_0", "rbtnBreakDown_1");
                        jQuery("#" + senderid.replace("rbtnMain_0", "rbtnMain_1")).attr("checked", false);
                        jQuery(mystringB).attr("checked", true);
                        //alert(jQuery(mystringB).attr("checked"));
                        
                    }
                }
                else {
                    if (jQuery("#" + senderid.replace("rbtnBreakDown_0", "rbtnMain_1")) != undefined) {
                        //alert('hello');
                        jQuery("#" + senderid.replace("rbtnBreakDown_0", "rbtnBreakDown_1")).attr("checked", false);
                        jQuery("#" + senderid.replace("rbtnBreakDown_0", "rbtnMain_1")).attr("checked", true);
                        
                    }
                }
            }
            else {
                if (check[check.length - 2] == "rbtnMain") {
                    if (jQuery("#" + senderid.replace("rbtnMain_1", "rbtnBreakDown_0")) != undefined) {
                        var mystringB = "#" + senderid.replace("rbtnMain_1", "rbtnBreakDown_0");
                        jQuery("#" + senderid.replace("rbtnMain_1", "rbtnMain_0")).attr("checked", false);
                        jQuery(mystringB).attr("checked", true);
                        //alert(jQuery(mystringB).attr("checked"));
                        
                    }
                }
                else {
                    if (jQuery("#" + senderid.replace("rbtnBreakDown_1", "rbtnMain_0")) != undefined) {
                        jQuery("#" + senderid.replace("rbtnBreakDown_1", "rbtnBreakDown_0")).attr("checked", false);
                        jQuery("#" + senderid.replace("rbtnBreakDown_1", "rbtnMain_0")).attr("checked", true);
                        
                    }
                }
            }
        }
    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery(".showMap").each(function () {
                jQuery(this).bind("click",function () {
                    var offsetshowmap = jQuery(this).offset();
                    var long = jQuery(this).parent().find(".long").find("input:hidden").val();
                    var lat = jQuery(this).parent().find(".lat").find("input:hidden").val();
                    jQuery("#maploc").css({ "top": (offsetshowmap.top + 20) + "px", "left": (offsetshowmap.left - 10) + "px" }); 
                    jQuery("#maploc").show();
                    jQuery(".close").css({ "top": (offsetshowmap.top + 10) + "px", "left": (offsetshowmap.left + 230) + "px" });
                    jQuery(".close").show();
                    LoadMapLatLong(long, lat);
                });
            });
            jQuery(".close").bind("click", function () { jQuery("#maploc").hide(); jQuery(".close").hide(); return false; });
        });
            function LoadMapLatLong(long,lat) {

                var map = new google.maps.Map(document.getElementById('maploc'), {
                    zoom: 14,
                    center: new google.maps.LatLng(lat, long),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(lat, long),
                    map: map
                });

            }

//////            jQuery(document).ready(function () {
//////                //Select all anchor tag with rel set to tooltip
//////                jQuery('.tooltip').mouseover(function (e) {

//////                    //Grab the title attribute's value and assign it to a variable
//////                    var tip = jQuery(this).attr('alt');
//////                    //alert(tip);
//////                    //Remove the title attribute's to avoid the native tooltip from the browser
//////                    //jQuery(this).attr('alt', '');

//////                    //Append the tooltip template and its value
//////                    jQuery(this).append('<div id="tooltip"><div class="tipHeader"></div><div class="tipBody">' + tip + '</div><div class="tipFooter"></div></div>');

//////                    //Set the X and Y axis of the tooltip
//////                    jQuery('#tooltip').css('top', e.pageY + 10);
//////                    jQuery('#tooltip').css('left', e.pageX + 20);

//////                    //Show the tooltip with faceIn effect
//////                    jQuery('#tooltip').fadeIn('500');
//////                    jQuery('#tooltip').fadeTo('10', 0.8);

//////                }).mousemove(function (e) {

//////                    //Keep changing the X and Y axis for the tooltip, thus, the tooltip move along with the mouse
//////                    jQuery('#tooltip').css('top', e.pageY + 10);
//////                    jQuery('#tooltip').css('left', e.pageX + 20);

//////                }).mouseout(function () {

//////                    //Put back the title attribute's value
//////                    //jQuery(this).attr('alt', jQuery('.tipBody').html());

//////                    //Remove the appended tooltip template
//////                    jQuery(this).children('div#tooltip').remove();

//////                });
//////            });
        </script>
    
</asp:Content>
