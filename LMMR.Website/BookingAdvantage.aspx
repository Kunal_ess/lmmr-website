﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeMaster.master" AutoEventWireup="true"
    CodeFile="BookingAdvantage.aspx.cs" Inherits="BookingAdvantage" %>

<%@ Register Src="UserControl/Frontend/HotelOfTheWeek.ascx" TagName="HotelOfTheWeek"
    TagPrefix="uc1" %>
<%@ Register Src="UserControl/Frontend/Benefits.ascx" TagName="Benefits" TagPrefix="uc2" %>
<%@ Register Src="UserControl/Frontend/RecentlyJoinedHotel.ascx" TagName="RecentlyJoinedHotel"
    TagPrefix="uc3" %>
<%@ Register Src="UserControl/Frontend/NewsSubscriber.ascx" TagName="NewsSubscriber"
    TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntLeft" runat="Server">
    <uc1:HotelOfTheWeek ID="HotelOfTheWeek2" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMainbody" runat="Server">
    <div class="mainbody-left">
        <div class="banner">
            <iframe height="100%" src="<%= SiteRootPath %>SlideShow.aspx" width="100%"></iframe>
        </div>
        <div class="abutus-text">
            <asp:Label ID="lblBookingAdvantage" runat="server"></asp:Label>
            <%--<p>
                We offer you, the client, to search, analyse, compare and either
            </p>
            <p style="text-align: center">
                <%= GetKeyResult("BOOKONLINEORSENDAREQUESTS")%></p>
            <p>
                IMPORTANT TO KNOW: <span class="green">MAKE A BOOKING</span></p>
            <ul class="greenul">
                <li>You can search on <span class="green">a 60 day frame</span> into the future
                </li>
                <li><span class="green">No credit card details or payment</span> is requested while
                    making the booking(*)</li>
                <li>You can book any meeting room with or without Bedrooms, Food & Beverages and or
                    AV equipment</li>
                <li>You can easily see the <span class="green">added values and complimentary items</span>
                    offered by each venue</li>
                <li>You can easily <span class="green">see the discount</span> given by the venue for
                    that specific day(s)</li>
                <li>You get an <span class="green">immediate confirmation</span> in your Email box</li>
                <li>You will be contacted by the venue after the booking has been made in order to go
                    over the details and special requests.</li>
                <li>It is <span class="green">quick, easy and secure</span>.</li>
                <li>If you have a problem while making a booking, our <span class="green">Customer support
                    desk</span> is available every weekday from 08.00 to 23.00 in English, French and
                    Dutch language.</li>
                <li>Access to the “User area” in order to follow and or cancel your booking (**)</li>
                <li>It is free of charge</li>
            </ul>
            <p class="small">
                * (Except when booking time frame is between 7-0 days before arrival)</p>
            <p class="small">
                ** (within agreed cancellation policy of venue)</p>--%>
        </div>
    </div>
    <div class="mainbody-right">
        <div id="beforeLogin" runat="server">
            <div class="mainbody-right-call">
                <div class="need">
                    <span>
                        <%= GetKeyResult("NEEDHELP")%></span>
                    <br />
                    <%= GetKeyResult("CALLUS")%>
                    5/7
                </div>
                <div class="phone">
                    +32 2 344 25 50
                </div>
                <p style="font-size: 10px; color: White; text-align: center">
                    <%= GetKeyResult("CALLUSANDWEDOITFORYOU")%></p>
                <div class="mail">
                    <a href="http://needameetingroom.com/requestform/" onClick="javascript: _gaq.push(['_trackPageview', '/email']); window.open(this.href, 'popupwindow1','width=900,height=600,scrollbars,resizable,toolbar=NO'); &#13;&#10;return false;" Target="_blank"">
                        Send us your RFP</a>
                </div>
            </div>
            <div class="mainbody-right-join" id="divjoinus" runat="server">
                <div class="join">
                    <a id="jointoday" runat="server">
                        <%= GetKeyResult("JOINUSTODAY")%></a></div>
                <%= GetKeyResult("FORHOTELSMEETINGFACILITIESEVENT")%>
            </div>
            <div class="mainbody-right-you">
                <a href="http://www.youtube.com/watch?v=rkt3NIWecG8&feature=youtu.be" target="_blank">
                    <img src="<%= SiteRootPath %>images/youtube-icon.png" /></a>&nbsp;&nbsp;<a href="http://www.youtube.com/watch?v=U3y7d7yxmvE&feature=relmfu"
                        target="_blank"><img src="<%= SiteRootPath %>images/small-vedio.png" /></a>
            </div>
        </div>
        <div style="width: 216px; height: auto; overflow: hidden; margin: 0px auto; display: none"
            id="afterLogin" runat="server">
            <div class="mainbody-right-call">
                <div class="need">
                    <span>
                        <%= GetKeyResult("NEEDHELP")%></span>
                    <br />
                    <%= GetKeyResult("CALLUS")%>
                    5/7
                </div>
                <div class="phone">
                    +32 2 344 25 50
                </div>
                <p style="font-size: 10px; color: White; text-align: center">
                    CALL US and WE DO IT FOR YOU</p>
                <div class="mail">
                    <a href="http://needameetingroom.com/requestform/" onClick="javascript: _gaq.push(['_trackPageview', '/email']); window.open(this.href, 'popupwindow1','width=900,height=600,scrollbars,resizable,toolbar=NO'); &#13;&#10;return false;" Target="_blank"">
                        Send us your RFP</a>
                </div>
            </div>
            <!--start mainbody-right-afterlogin-body -->
            <div class="mainbody-right-afterlogin-body">
                <div class="mainbody-right-afterlogin-top">
                    <div class="mainbody-right-afterlogin-inner">
                        <b>
                            <%= GetKeyResult("TODAY")%>:</b>
                        <asp:Label ID="lstLoginTime" runat="server"></asp:Label>
                        <div class="afterlogin-welcome">
                            <span>
                                <%= GetKeyResult("WELCOME")%></span>
                            <br>
                            <asp:Label ID="lblLoginUserName" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="mainbody-right-afterlogin-bottom">
                    <div class="mainbody-right-afterlogin-bottom-inner">
                        <asp:LinkButton ID="hypManageProfile" runat="server" OnClick="hypManageProfile_Click"><%= GetKeyResult("MANAGEPROFILE")%></asp:LinkButton>
                        <br />
                        <asp:LinkButton ID="hypChangepassword" runat="server" OnClick="hypChangepassword_Click"><%= GetKeyResult("CHANGEPASSWORD")%></asp:LinkButton><br />
                        <asp:LinkButton ID="hypListBookings" runat="server" OnClick="hypListBookings_Click"
                            Visible="true"><%= GetKeyResult("MYBOOKINGS")%></asp:LinkButton><br />
                        <asp:LinkButton ID="hypListRequests" runat="server" OnClick="hypListRequests_Click"
                            Visible="true"><%= GetKeyResult("MYREQUESTS")%></asp:LinkButton>
                    </div>
                </div>
            </div>
            <!--end mainbody-right-afterlogin-body -->
        </div>
        <div class="mainbody-right-news">
            <uc4:NewsSubscriber ID="NewsSubscriber1" runat="server" />
        </div>
        <uc3:RecentlyJoinedHotel ID="RecentlyJoinedHotel1" runat="server" />
    </div>
    <div class="mainbody">
        <div class="mainbody-left">
        </div>
    </div>
</asp:Content>
