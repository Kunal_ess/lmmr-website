﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using LMMR.Business;

public partial class UserControl_SuperAdmin_LanguageTab : System.Web.UI.UserControl
{
    StringBuilder sbLanguage = new StringBuilder();
    CancellationPolicy objPolicy = new CancellationPolicy();
    PackagePricingManager objPricing = new PackagePricingManager();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //litLangugeTab.Text = LanguageTabTableau.CreateLanguageTabForTableau();
        }
    }

    /// <summary>
    /// this function used for create language tab. 
    /// </summary>
    public void CreateLanguageTab(int countryID)
    {
        int i = 0;
        int count = 0;
        ContentPlaceHolder Content = (ContentPlaceHolder)Page.Master.FindControl("ContentPlaceHolder1");
        var getLanguage = objPolicy.GetLanguageByCountry(countryID);
        HiddenField hdnPolicyID = (HiddenField)Content.FindControl("hdnPolicyID");
        sbLanguage.Append("<div class='superadmin-mainbody-sub6' style='border-bottom:1px solid #92BDDD;width:100%;' >");
        sbLanguage.Append("<ul>");
        for (i = 0; i <= getLanguage.Count - 1; i++)
        {

            var objAllDescriptionofPolicy = objPolicy.GetCancellationPolicylangByPolicyId(Convert.ToInt32(hdnPolicyID.Value));
            var getLanguageByID = objPolicy.GetLanguageByID(Convert.ToInt32(getLanguage[i].LanguageId));

            if (objAllDescriptionofPolicy.Count > 0)
            {
                if (getLanguageByID != null)
                {
                    if (objAllDescriptionofPolicy.Find(a => a.LanguageId == getLanguageByID.Id) != null)
                    {
                        count++;
                        if (count == 1)
                        {
                            sbLanguage.Append("<li><a href='#' class='select' id='liLanguageTab" + getLanguageByID.Id + "' onclick='GetLanguageID(" + getLanguageByID.Id + ")' >" + getLanguageByID.Name + "</a></li>");
                        }
                        else
                        {
                            sbLanguage.Append("<li><a href='#' id='liLanguageTab" + getLanguageByID.Id + "'  onclick='GetLanguageID(" + getLanguageByID.Id + ")' >" + getLanguageByID.Name + "</a></li>");
                        }
                    }
                    else
                    {

                        sbLanguage.Append("<li class='redtab'><a href='#' id='liLanguageTab" + getLanguageByID.Id + "'  onclick='GetLanguageID(" + getLanguageByID.Id + ")' >" + getLanguageByID.Name + "</a></li>");
                    }
                }
            }
            else
            {
                if (getLanguageByID != null)
                {
                    count++;
                    if (count == 1)
                    {
                        sbLanguage.Append("<li class='redtab'><a href='#' class='select' id='liLanguageTab" + getLanguageByID.Id + "' onclick='GetLanguageID(" + getLanguageByID.Id + ")' >" + getLanguageByID.Name + "</a></li>");
                    }
                    else
                    {
                        sbLanguage.Append("<li class='redtab'><a href='#' id='liLanguageTab" + getLanguageByID.Id + "'  onclick='GetLanguageID(" + getLanguageByID.Id + ")' >" + getLanguageByID.Name + "</a></li>");
                    }
                }
            }
        }
        sbLanguage.Append("</ul>");
        sbLanguage.Append("</div>");

        litLangugeTab.Text = sbLanguage.ToString();
    }

    /// <summary>
    /// This function used for create language tab for tableau.
    /// </summary>
    /// <param name="countryID"></param>
    /// <param name="recordID"></param>
    /// <returns></returns>
    public static void CreateLanguageTabForTableau(int countryID, int recordID, Label lit)
    {
        CancellationPolicy objPolicy = new CancellationPolicy();
        PackagePricingManager objPricing = new PackagePricingManager();
        StringBuilder sbLanguage = new StringBuilder();
        int i = 0;
        int count = 0;
        //ContentPlaceHolder Content = (ContentPlaceHolder)Page.Master.FindControl("ContentPlaceHolder2");
        var getLanguage = objPolicy.GetLanguageByCountry(countryID);
        //HiddenField hdnRecordID = (HiddenField)Content.FindControl("hdnRecordID");
        sbLanguage.Append("<div class='smalltab'>");
        sbLanguage.Append("<ul>");
        for (i = 0; i <= getLanguage.Count - 1; i++)
        {
            var objDescription = objPricing.GetPackageDescriptionByItemID(recordID);
            var getLanguageByID = objPolicy.GetLanguageByID(Convert.ToInt32(getLanguage[i].LanguageId));

            if (objDescription.Count > 0)
            {
                if (getLanguageByID != null)
                {
                    if (objDescription.Find(a => a.LanguageId == getLanguageByID.Id) != null)
                    {
                        count++;
                        if (count == 1)
                        {
                            sbLanguage.Append("<li><a href='#' class='select' id='liLanguageTab" + getLanguageByID.Id + "' onclick='GetLanguageID(" + getLanguageByID.Id + ")' >" + getLanguageByID.Name + "</a></li>");
                        }
                        else
                        {
                            sbLanguage.Append("<li><a href='#' id='liLanguageTab" + getLanguageByID.Id + "'  onclick='GetLanguageID(" + getLanguageByID.Id + ")' >" + getLanguageByID.Name + "</a></li>");
                        }
                    }
                    else
                    {

                        sbLanguage.Append("<li class='redtab'><a href='#' id='liLanguageTab" + getLanguageByID.Id + "'  onclick='GetLanguageID(" + getLanguageByID.Id + ")' >" + getLanguageByID.Name + "</a></li>");
                    }
                }
            }
            else
            {
                if (getLanguageByID != null)
                {
                    count++;
                    if (count == 1)
                    {
                        sbLanguage.Append("<li class='redtab'><a href='#' class='select' id='liLanguageTab" + getLanguageByID.Id + "' onclick='GetLanguageID(" + getLanguageByID.Id + ")' >" + getLanguageByID.Name + "</a></li>");
                    }
                    else
                    {
                        sbLanguage.Append("<li class='redtab'><a href='#' id='liLanguageTab" + getLanguageByID.Id + "'  onclick='GetLanguageID(" + getLanguageByID.Id + ")' >" + getLanguageByID.Name + "</a></li>");
                    }
                }
            }
        }
        sbLanguage.Append("</ul>");
        sbLanguage.Append("</div>");

        lit.Text = sbLanguage.ToString();
    }

    public static void CreateLanguageTabForTableauPackage(int countryID, int recordID, Label lit)
    {
        CancellationPolicy objPolicy = new CancellationPolicy();
        PackagePricingManager objPricing = new PackagePricingManager();
        StringBuilder sbLanguage = new StringBuilder();
        int i = 0;
        int count = 0;
        //ContentPlaceHolder Content = (ContentPlaceHolder)Page.Master.FindControl("ContentPlaceHolder2");
        var getLanguage = objPolicy.GetLanguageByCountry(countryID);
        //HiddenField hdnRecordID = (HiddenField)Content.FindControl("hdnRecordID");
        sbLanguage.Append("<div class='smalltab'>");
        sbLanguage.Append("<ul>");
        for (i = 0; i <= getLanguage.Count - 1; i++)
        {
            var objDescription = objPricing.GetPackageMasterDescription(recordID);
            var getLanguageByID = objPolicy.GetLanguageByID(Convert.ToInt32(getLanguage[i].LanguageId));

            if (objDescription.Count > 0)
            {
                if (getLanguageByID != null)
                {
                    if (objDescription.Find(a => a.LanguageId == getLanguageByID.Id) != null)
                    {
                        count++;
                        if (count == 1)
                        {
                            sbLanguage.Append("<li><a href='#' class='select' id='liLanguageTab" + getLanguageByID.Id + "' onclick='GetPackageLanguageID(" + getLanguageByID.Id + ")' >" + getLanguageByID.Name + "</a></li>");
                        }
                        else
                        {
                            sbLanguage.Append("<li><a href='#' id='liLanguageTab" + getLanguageByID.Id + "'  onclick='GetPackageLanguageID(" + getLanguageByID.Id + ")' >" + getLanguageByID.Name + "</a></li>");
                        }
                    }
                    else
                    {

                        sbLanguage.Append("<li class='redtab'><a href='#' id='liLanguageTab" + getLanguageByID.Id + "'  onclick='GetPackageLanguageID(" + getLanguageByID.Id + ")' >" + getLanguageByID.Name + "</a></li>");
                    }
                }
            }
            else
            {
                if (getLanguageByID != null)
                {
                    count++;
                    if (count == 1)
                    {
                        sbLanguage.Append("<li class='redtab'><a href='#' class='select' id='liLanguageTab" + getLanguageByID.Id + "' onclick='GetPackageLanguageID(" + getLanguageByID.Id + ")' >" + getLanguageByID.Name + "</a></li>");
                    }
                    else
                    {
                        sbLanguage.Append("<li class='redtab'><a href='#' id='liLanguageTab" + getLanguageByID.Id + "'  onclick='GetPackageLanguageID(" + getLanguageByID.Id + ")' >" + getLanguageByID.Name + "</a></li>");
                    }
                }
            }
        }
        sbLanguage.Append("</ul>");
        sbLanguage.Append("</div>");

        lit.Text = sbLanguage.ToString();
    }
}