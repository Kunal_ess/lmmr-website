﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StaticContent.ascx.cs"
    Inherits="UserControl_SuperAdmin_StaticContent" %>
<%@ Register Src="LanaguageTabWithoutCountry.ascx" TagName="LanaguageTabWithoutCountry"
    TagPrefix="uc1" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Button ID="btnLanguage" runat="server" Text="Button" OnClick="btnLanguage_Click"
    Style="display: none" />
<div class="superadmin-example-mainbody">
    <div class="superadmin-tabbody1">
        <div id="TabbedPanels2" class="TabbedPanels" runat="server">
            <uc1:LanaguageTabWithoutCountry ID="LanaguageTabWithoutCountry1" runat="server" />
            <br />
        </div>
        <div id="editor">
            <br />
            <asp:Label ID="lblContentDesc" runat="server" Font-Bold="true" Text="You can edit content from here :"
                Style="margin: 0 0 0 9px;"></asp:Label><br />
            <div class="TabbedPanelsContentGroup">
                <div class="TabbedPanelsContent">
                    <div class="tab-textarea1">
                        <CKEditor:CKEditorControl ID="ckeEditorEN" runat="server" Toolbar="Full"></CKEditor:CKEditorControl>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
