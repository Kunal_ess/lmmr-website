﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using LMMR.Business;
using LMMR.Entities;

public partial class UserControl_SuperAdmin_LanaguageTabWithoutCountry : System.Web.UI.UserControl
{
    StringBuilder sbLanguage = new StringBuilder();
    CancellationPolicy objPolicy = new CancellationPolicy();
    SuperAdminTaskManager objManager = new SuperAdminTaskManager();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// this function used for create language tab. 
    /// </summary>
    public void CreateLanguageTab(bool IsCMS)
    {
        int i = 0;
        int count = 0;
        ContentPlaceHolder Content = (ContentPlaceHolder)Page.Master.FindControl("ContentPlaceHolder1");
        var getLanguage = objPolicy.GetAllLanguage();
        HiddenField hdnRecordID = (HiddenField)Content.FindControl("hdnRecordId");
        HiddenField hdnContentTitle = (HiddenField)Content.FindControl("hdnContentTitle");
        sbLanguage.Append("<div class='superadmin-mainbody-sub6' style='border-bottom:1px solid #92BDDD;width:100%;' >");
        sbLanguage.Append("<ul>");
        for (i = 0; i <= getLanguage.Count - 1; i++)
        {
            TList<CmsDescription> objDescription = new TList<CmsDescription>();
            if (IsCMS)
            {
                objDescription = objManager.getCmsDescriptionOnCmsID(Convert.ToInt32(hdnRecordID.Value)).FindAll(a => a.ContentTitle == hdnContentTitle.Value);
            }
            else
            { 
            
            
            }
            var getLanguageByID = objPolicy.GetLanguageByID(Convert.ToInt32(getLanguage[i].Id));

            if (objDescription.Count > 0)
            {
                if (getLanguageByID != null)
                {
                    if (objDescription.Find(a => a.LanguageId == getLanguageByID.Id) != null)
                    {
                        count++;
                        if (count == 1)
                        {
                            sbLanguage.Append("<li><a  class='select' id='liLanguageTab" + getLanguageByID.Id + "' onclick='GetLanguageID(" + getLanguageByID.Id + ")' >" + getLanguageByID.Name + "</a></li>");
                        }
                        else
                        {
                            sbLanguage.Append("<li><a  id='liLanguageTab" + getLanguageByID.Id + "'  onclick='GetLanguageID(" + getLanguageByID.Id + ")' >" + getLanguageByID.Name + "</a></li>");
                        }
                    }
                    else
                    {

                        sbLanguage.Append("<li class='redtab'><a id='liLanguageTab" + getLanguageByID.Id + "'  onclick='GetLanguageID(" + getLanguageByID.Id + ")' >" + getLanguageByID.Name + "</a></li>");
                    }
                }
            }
            else
            {
                if (getLanguageByID != null)
                {
                    count++;
                    if (count == 1)
                    {
                        sbLanguage.Append("<li class='redtab'><a class='select' id='liLanguageTab" + getLanguageByID.Id + "' onclick='GetLanguageID(" + getLanguageByID.Id + ")' >" + getLanguageByID.Name + "</a></li>");
                    }
                    else
                    {
                        sbLanguage.Append("<li class='redtab'><a  id='liLanguageTab" + getLanguageByID.Id + "'  onclick='GetLanguageID(" + getLanguageByID.Id + ")' >" + getLanguageByID.Name + "</a></li>");
                    }
                }
            }
        }
        sbLanguage.Append("</ul>");
        sbLanguage.Append("</div>");

        litLangugeTab.Text = "";
        litLangugeTab.Text = sbLanguage.ToString();
    }

    public void CreateLanguageThree(bool IsCMS)
    {
        int i = 0;
        int count = 0;
        ContentPlaceHolder Content = (ContentPlaceHolder)Page.Master.FindControl("ContentPlaceHolder1");
        var getLanguage = objPolicy.GetAllLanguage().Where(a=>a.Name.ToLower().Trim() == "english" || a.Name.ToLower().Trim() == "french"||a.Name.ToLower().Trim() == "dutch").ToList();
        HiddenField hdnRecordID = (HiddenField)Content.FindControl("hdnRecordId");
        HiddenField hdnContentTitle = (HiddenField)Content.FindControl("hdnContentTitle");
        sbLanguage.Append("<div class='superadmin-mainbody-sub6' style='border-bottom:1px solid #92BDDD;width:100%;' >");
        sbLanguage.Append("<ul>");
        for (i = 0; i <= getLanguage.Count - 1; i++)
        {
            TList<CmsDescription> objDescription = new TList<CmsDescription>();
            if (IsCMS)
            {
                objDescription = objManager.getCmsDescriptionOnCmsID(Convert.ToInt32(hdnRecordID.Value)).FindAll(a => a.ContentTitle == hdnContentTitle.Value);
            }
            else
            {


            }
            var getLanguageByID = objPolicy.GetLanguageByID(Convert.ToInt32(getLanguage[i].Id));

            if (objDescription.Count > 0)
            {
                if (getLanguageByID != null)
                {
                    if (objDescription.Find(a => a.LanguageId == getLanguageByID.Id) != null)
                    {
                        count++;
                        if (count == 1)
                        {
                            sbLanguage.Append("<li><a  class='select' id='liLanguageTab" + getLanguageByID.Id + "' onclick='GetLanguageID(" + getLanguageByID.Id + ")' >" + getLanguageByID.Name + "</a></li>");
                        }
                        else
                        {
                            sbLanguage.Append("<li><a  id='liLanguageTab" + getLanguageByID.Id + "'  onclick='GetLanguageID(" + getLanguageByID.Id + ")' >" + getLanguageByID.Name + "</a></li>");
                        }
                    }
                    else
                    {

                        sbLanguage.Append("<li class='redtab'><a id='liLanguageTab" + getLanguageByID.Id + "'  onclick='GetLanguageID(" + getLanguageByID.Id + ")' >" + getLanguageByID.Name + "</a></li>");
                    }
                }
            }
            else
            {
                if (getLanguageByID != null)
                {
                    count++;
                    if (count == 1)
                    {
                        sbLanguage.Append("<li class='redtab'><a class='select' id='liLanguageTab" + getLanguageByID.Id + "' onclick='GetLanguageID(" + getLanguageByID.Id + ")' >" + getLanguageByID.Name + "</a></li>");
                    }
                    else
                    {
                        sbLanguage.Append("<li class='redtab'><a  id='liLanguageTab" + getLanguageByID.Id + "'  onclick='GetLanguageID(" + getLanguageByID.Id + ")' >" + getLanguageByID.Name + "</a></li>");
                    }
                }
            }
        }
        sbLanguage.Append("</ul>");
        sbLanguage.Append("</div>");

        litLangugeTab.Text = "";
        litLangugeTab.Text = sbLanguage.ToString();
    }
}