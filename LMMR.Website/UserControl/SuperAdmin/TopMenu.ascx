﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopMenu.ascx.cs" Inherits="UserControl_SuperAdmin_TopMenu" %>
<!--Main navigation start -->
            <div class="main_nav">
                <div id="subnavbar">
                    <ul id="subnav">
                        <li class="cat-item cat-item-1"><asp:LinkButton ID="hyperlink1" runat="server" PostBackUrl="~/Operator/ControlPanel.aspx">Dashboard</asp:LinkButton> </li>
                        <li class="cat-item cat-item-366"><asp:LinkButton ID="hyperlink2" runat="server" PostBackUrl="~/Operator/ClientContract.aspx">Clients-Contracts</asp:LinkButton></li>
                        <li class="cat-item cat-item-311"><asp:LinkButton ID="hyperlink3" runat="server" PostBackUrl="~/SuperAdmin/RankBookingRequests.aspx">Ranking</asp:LinkButton> </li>
                        <li class="cat-item cat-item-313"><asp:LinkButton ID="hyperlink4" runat="server" PostBackUrl="~/SuperAdmin/ReportManager.aspx">Reports</asp:LinkButton> </li>
                        <li class="cat-item cat-item-313"><asp:LinkButton ID="hyperlink5" runat="server" PostBackUrl="~/SuperAdmin/StaticPagesContents.aspx" >CMS</asp:LinkButton> </li>
                        <li class="cat-item cat-item-311"><asp:LinkButton ID="hyperlink6" runat="server" PostBackUrl="~/SuperAdmin/CountryCitySetting.aspx">Country-City Settings</asp:LinkButton> </li>
                        <li class="cat-item cat-item-313"><asp:LinkButton ID="hyperlink7" runat="server" PostBackUrl="~/SuperAdmin/Staffadmin.aspx">Staff accounts</asp:LinkButton> </li>
                        <li class="cat-item cat-item-313"><asp:LinkButton ID="hyperlink8" runat="server" PostBackUrl="~/SuperAdmin/EmailConfig.aspx">Email config</asp:LinkButton> </li>
                    </ul>
                </div>
            </div>
            <!--main navigation end -->
            <!--Main navigation start -->
            <div class="main_nav1">
                <div id="subnavbar1">
                    <ul id="subnav1">
                        <li class="cat-item"><asp:LinkButton ID="hyperlink9" runat="server" PostBackUrl="~/SuperAdmin/CancellationPolicy.aspx" >Cancellation</asp:LinkButton> </li>
                        <li class="cat-item"><asp:LinkButton ID="hyperlink10" runat="server" PostBackUrl="~/SuperAdmin/Tableau.aspx">Tableau</asp:LinkButton></li>
                        <li class="cat-item"><asp:LinkButton ID="hyperlink11" runat="server" PostBackUrl="~/Operator/HotelFacilityAccess.aspx">Hotel/Facility Access</asp:LinkButton></li>
                        <li class="cat-item"><asp:LinkButton ID="hyperlink13" runat="server" PostBackUrl="~/SuperAdmin/PriorityBasket.aspx">Priority Basket</asp:LinkButton></li>
                        <li class="cat-item"><asp:LinkButton ID="hyperlink12" runat="server" PostBackUrl="~/SuperAdmin/ManageCountryOthers.aspx">Other</asp:LinkButton></li>
                          <li class="cat-item"><asp:LinkButton ID="hyperlink14" runat="server" PostBackUrl="~/SuperAdmin/Log.aspx">Log</asp:LinkButton></li>
                          <li class="cat-item"><asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/SuperAdmin/Survey.aspx">Survey</asp:LinkButton></li>
                          <li class="cat-item"><asp:LinkButton ID="LinkButton2" runat="server" PostBackUrl="~/SuperAdmin/SurveyResponse.aspx">Review</asp:LinkButton></li>
                          <li class="cat-item"><asp:LinkButton ID="hyperlink16" runat="server" PostBackUrl="~/SuperAdmin/WhiteLabelManager.aspx">White Label</asp:LinkButton></li>
                    </ul>
                </div>
            </div>
            <!--main navigation end -->