﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportSearchPanel.ascx.cs"     Inherits="UserControl_SuperAdmin_ReportSearchPanel" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div class="search-operator-layout1" style="margin-bottom: 20px;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="630px">
                <div class="search-operator-layout-left1">
                    <div class="search-operator-from1">
                        <div class="commisions-top-new1 ">
                            <table width="100%" border="0" cellspacing="0" cellpadding="8">
                                <tr id="divProfile" runat="server">
                                    <td colspan="5" style="border-bottom: #92bede solid 1px">
                                        <asp:RadioButtonList ID="rdbProfile" runat="server" RepeatDirection="Horizontal">                                            
                                        </asp:RadioButtonList>                                    
                                    </td>                                    
                                </tr>
                                <tr id="trCountry" runat="server">
                                    <td style="border-bottom: #92bede solid 1px">
                                        <asp:DropDownList ID="drpSearchCountry" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpSearchCountry_SelectedIndexChanged"
                                            Style="width: 215px">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tblDateFromTo" runat="server">
                                            <tr>
                                                <td>
                                                    <b>Date:</b>
                                                </td>
                                                <td style="padding-left: 20px; text-align: right; padding-right: 3px;">
                                                    <b>From</b>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFromdate" runat="server" CssClass="inputbox1"></asp:TextBox><asp:HiddenField
                                                        ID="hdnfromdate" runat="server" />
                                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFromdate"
                                                        PopupButtonID="calFrom" Format="dd/MM/yy" OnClientDateSelectionChanged="dateChangeFrom">
                                                    </asp:CalendarExtender>
                                                    <%--<asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtFromdate"
                                                        ValidChars="/0123456789">
                                                    </asp:FilteredTextBoxExtender>--%>
                                                    <asp:Image ID="calFrom" runat="server" ImageUrl="~/Images/date-icon.png" align="absmiddle" />
                                                </td>
                                                <td>
                                                    <b>To</b>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTodate" runat="server" CssClass="inputbox1"></asp:TextBox>
                                                    <asp:HiddenField ID="hdntodate" runat="server" />
                                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTodate"
                                                        PopupButtonID="calTo" Format="dd/MM/yy" OnClientDateSelectionChanged="dateChangeTo">
                                                    </asp:CalendarExtender>
                                                    <%--<asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtTodate"
                                                        ValidChars="/0123456789">
                                                    </asp:FilteredTextBoxExtender>--%>
                                                    <asp:Image ID="calTo" runat="server" ImageUrl="~/Images/date-icon.png" align="absmiddle" />
                                                </td>
                                            </tr>
                                        </table>
                                        <table id="tblYear" runat="server">
                                            <tr>
                                                <td>
                                                    Year:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtYear" runat="server" MaxLength="4"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtYear"
                                                        ValidChars="0123456789">
                                                    </asp:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                        </table>                                        
                                    </td>
                                </tr>
                                <tr id="tblNewsLetter" runat="server">
                                <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                <table>
                                            <tr>
                                                <td>
                                                    Name :
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFlname" runat="server" MaxLength="25"></asp:TextBox>                                                    
                                                </td>
                                            </tr>
                                        </table>
                                </td>
                                </tr>
                                <tr id="trVenue" runat="server">
                                    <td id="divCity" runat="server" style="border-bottom: #92bede solid 1px">
                                        <asp:DropDownList ID="drpSearchCity" runat="server" AutoPostBack="True" CssClass="NoClassApply" OnSelectedIndexChanged="drpSearchCity_SelectedIndexChanged"
                                            Enabled="false" Style="width: 215px">
                                            <asp:ListItem Text="--Select city--" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td id="divVenue" runat="server" style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <div class="search-form1-left-new">
                                            Venue(s) :
                                        </div>
                                        <asp:TextBox ID="txtVenue" runat="server" class="inputbox" onkeyup="return GetHotelList(this);"
                                            OnTextChanged="txtVenue_TextChanged" AutoPostBack="true" MaxLength="100"></asp:TextBox>
                                    </td>
                                    <td id="divCompanyName" runat="server" style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <div class="search-form1-left-new">
                                        <asp:Label ID="lblcomname" runat="server">Company Name :</asp:Label>
                                            
                                        </div>
                                        <asp:TextBox ID="txtCompanyName" runat="server" class="inputbox" onkeyup="return GetHotelList(this);" MaxLength="100"
                                            OnTextChanged="txtCompanyName_TextChanged" AutoPostBack="true"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="Custom" runat="server" TargetControlID="txtCompanyName"
                                            FilterMode="InvalidChars" InvalidChars="!@#$%^&amp;*()~?><|\';:">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellspacing="0" cellpadding="8">
                                <tr runat="server" id="trVenueCheckBoxList" visible="false">
                                    <td style="border-bottom: #92bede solid 1px">
                                        <b>
                                        <asp:Label ID="lblVenue" runat="server" Text=""></asp:Label></b>
                                        <asp:CheckBoxList ID="chkchkListHotel" runat="server" RepeatDirection="Horizontal"
                                            RepeatColumns="3">
                                        </asp:CheckBoxList>
                                    </td>
                                </tr>
                                <tr runat="server" id="trEmptyVenueCheckBoxList" visible="false">
                                    <td style="border-bottom: #92bede solid 1px " align="center">
                                        <b>No record found</b>
                                    </td>
                                </tr>
                            </table>
                            <%--<table width="100%" border="0" cellspacing="0" cellpadding="8">
                                <tr>
                                    <td style="border-bottom: #92bede solid 1px">
                                        <b>Select country</b>
                                        <select name="select4" class="dropdown" style="width: 150px">
                                            <option value="opt1">Select country</option>
                                            <option value="opt2">Option 2</option>
                                            <option value="opt3">Option 3</option>
                                        </select>
                                    </td>
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <select name="select4" class="dropdown" style="width: 60px">
                                            <option value="opt1">Select country</option>
                                            <option value="opt2">Option 2</option>
                                            <option value="opt3">Option 3</option>
                                        </select>
                                    </td>
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <input type="text" class="inputregister" value="Enter nr here...">
                                    </td>
                                </tr>
                            </table>--%>
                        </div>
                    </div>
                </div>
            </td>
            <td align="left" valign="bottom">
                <div style="float: left; margin-left: 20px;" class="n-commisions">
                    <div class="n-btn">
                        <asp:LinkButton ID="lbtSearch" runat="server" OnClick="lbtSearch_Click" OnClientClick="return DataValidation()"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Generate Report</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                    </div>
                </div>
                <div style="float: left; margin-left: 10px;" class="n-commisions">
                    <div class="n-btn">
                        <asp:LinkButton ID="lnkClear" runat="server" OnClick="lnkClear_Click"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Clear</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript" language="javascript">
    /*jQuery(document).ready(function () {
        
        jQuery("#<%= chkchkListHotel.ClientID %>").find("input:checkbox").bind("click", function () {
            var v = jQuery(this).val();
            if (v == "0") {
                if (jQuery(this).is(":checked")) {
                    jQuery("#<%= chkchkListHotel.ClientID %>").find("input:checkbox").attr("checked", true);
                }
                else {
                    jQuery("#<%= chkchkListHotel.ClientID %>").find("input:checkbox").attr("checked", false);
                }
            }
            else {
                if (jQuery(this).is(":checked")) {
                    var p = jQuery("#<%= chkchkListHotel.ClientID %>").find("input:checkbox:checked").length;
                    if (p == jQuery("#<%= chkchkListHotel.ClientID %>").find("input:checkbox").length - 1) {
                        jQuery("#<%= chkchkListHotel.ClientID %>").find("input:checkbox:first").attr("checked", true);
                    }
                }
                else {
                    jQuery("#<%= chkchkListHotel.ClientID %>").find("input:checkbox:first").attr("checked", false);
                }
            }
        });
    });*/
    function dateChangeFrom(s, e) {
        var fromdatechange = jQuery("#<%= txtFromdate.ClientID %>").val();
        jQuery("#<%= hdnfromdate.ClientID %>").val(fromdatechange);
    }
    
    function dateChangeTo(s, e) {
        var todatechange = jQuery("#<%= txtTodate.ClientID %>").val();
        jQuery("#<%= hdntodate.ClientID %>").val(todatechange);
    }
    
    function DataValidation() {
        var fromdatechange = jQuery("#<%= txtFromdate.ClientID %>").val();
        var todatechange = jQuery("#<%= txtTodate.ClientID %>").val();
        var todayArr = todatechange.split('/');
        var formdayArr = fromdatechange.split('/');
        var fromdatecheck = new Date();
        var todatecheck = new Date();
        fromdatecheck.setFullYear(parseInt("20" + formdayArr[2],10), (parseInt(formdayArr[1],10) - 1), formdayArr[0]);
        todatecheck.setFullYear(parseInt("20" + todayArr[2],10), (parseInt(todayArr[1],10) - 1), todayArr[0]);
        if (fromdatecheck > todatecheck) {
            alert("From date must be less than To date.");
            return false;
        }
    }

    function GetHotelList(strHotelName) {

        jQuery.ajax({
            type: "POST",
            url: "ReportManager.aspx/GetHotel",
            data: "{'strHotelName':'" + strHotelName + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d != null) {
                    jQuery.each(data.d, function (key, value) {
                        $("#ddlCountry").append($("<option></option>").val(value.Id).html(value.Name));
                    });
                }
            }
        });
        return false;


    }
</script>
