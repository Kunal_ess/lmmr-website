﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AgenctClient.ascx.cs" Inherits="UserControl_Agency_AgenctClient" %>
 <style type="text/css">
     .style1
     {
         width: 100%;
     }
 </style>
 <div class="operator-mainbody">
 <div>
 <asp:Label ID="lblLoggingUser" runat="server" Text="" class="inputbox" Font-Bold="true" Font-Size="Small"></asp:Label>
 <asp:HiddenField ID="hdnUserId" runat="server" Value="0" />
 <asp:Panel ID="pnlBookingdtl" runat="server" Visible="false">
 <table class="style1" width="100%">
         <tr>
             <td width="20%">
                 <asp:Label ID="Label1" runat="server" Text="Last Booking" Font-Bold="true"></asp:Label></td>
                 <td width="30%">
                 <asp:Label ID="Label2" runat="server" Text="Last Request" Font-Bold="true"></asp:Label></td>
         </tr>
         <tr>
             <td width="20%">
                <span style="font-style:normal;font-size:small;">Boooking Id :</span> <asp:Label ID="lblBookingId" runat="server" Text=""></asp:Label></td>
                 <td width="30%">
                <span style="font-style:normal;font-size:small;">Request Id :</span>  <asp:Label ID="lblRequestId" runat="server" Text=""></asp:Label></td>
         </tr>
         <tr>
             <td width="20%">
                <span style="font-style:normal;font-size:small;">Boooking Date :</span> <asp:Label ID="lblBookingDate" runat="server" Text=""></asp:Label></td>
                 <td width="30%">
               <span style="font-style:normal;font-size:small;">Request Date :</span>  <asp:Label ID="lblRequestDate" runat="server" Text=""></asp:Label></td>
         </tr>
     </table>
 </asp:Panel>     
 <table>

 </table>
 </div>
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="630px">         
                <div class="search-operator-layout-left1">
                    <div class="search-operator-from1">
                        <div class="commisions-top-new1 ">
                            <table width="100%" border="0" cellspacing="0" cellpadding="8">                                                            
                                <tr>                                 
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px" width="60%">
                                        <div class="search-form1-left-new">
                                            Client Name : &nbsp;&nbsp;&nbsp;
                                        </div>                                        
                                        <asp:TextBox ID="txtClientName" runat="server" Text="" class="inputbox"></asp:TextBox>
                                    </td>   
                                    <td width="15%">
                                    <asp:LinkButton ID="lbtSearch" CssClass="WhiteBtn" runat="server" OnClick="lbtSearch_Click">Search</asp:LinkButton>
                                    </td>
                                     <td width="25%">
                                    <asp:LinkButton ID="lbtReset" CssClass="WhiteBtn" runat="server" OnClick="lbtReset_Click">Reset</asp:LinkButton>
                                    </td>                                
                                </tr>                                                                
                            </table>                            
                        </div>
                    </div>
                </div>           
            </td>
            <%--<td align="left" valign="bottom">
                <div style="float: left; margin-left: 20px;">
                    <div class="n-btn">
                        <asp:LinkButton ID="lbtSearch" runat="server" OnClick="lbtSearch_Click"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Search</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                    </div>
                </div>
                <div style="float: left; margin-left: 10px;">
                    <div class="n-btn">
                        <asp:LinkButton ID="lbtReset" runat="server" OnClick="lbtReset_Click"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Reset</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                    </div>
                </div>
            </td>--%>
        </tr>
    </table><br />
<table width="71%" cellspacing="1" cellpadding="5" border="0" bgcolor="#98BCD6">
  <tbody> 
  <tr bgcolor="#CCD8D8">
  <td valign="top" width="5%"></td>
  <td valign="top" width="15%">Last Name</td>
    <td valign="top" width="15%">First Name</td>
    <td valign="top" width="20%">Company/Dept Name</td>    
    <td valign="top" width="20%">Phone</td>    
  </tr> 
  <asp:GridView ID="grvClients" runat="server" Width="71%" border="0" CellPadding="5"
                    CellSpacing="1" AutoGenerateColumns="false" 
                    DataKeyNames="UserId" GridLines="None"
                    ShowHeader="false" ShowFooter="false" BackColor="#98BCD6" 
                    RowStyle-CssClass="con" AlternatingRowStyle-CssClass="con-dark"                    
                    EmptyDataRowStyle-BackColor="White" EmptyDataRowStyle-HorizontalAlign="Center"
                    HeaderStyle-BackColor="#98BCD6" onrowdatabound="grvClients_RowDataBound">
<AlternatingRowStyle CssClass="con-dark"></AlternatingRowStyle>
                    <Columns>
                        <asp:TemplateField ItemStyle-Width="5%">
                            <ItemTemplate>                                
                                <asp:CheckBox ID="rbselect" runat="server" 
                                    onclick="javascript:CheckOtherIsCheckedByGVID(this);" AutoPostBack="True" 
                                    oncheckedchanged="rbselect_CheckedChanged" /> 
                                <asp:HiddenField ID="hdnId" runat="server" Value='<%# Eval("UserId") %>'></asp:HiddenField>                    
                            </ItemTemplate>
                         </asp:TemplateField> 
                        <asp:TemplateField ItemStyle-Width="15%">
                            <ItemTemplate>                                
                                <%--<asp:LinkButton ID="lnbName" runat="server" Text='<%# Eval("LastName") %>' CommandArgument='<%# Eval("UserId") %>' CssClass="chk" OnClick="lnbName_Click"></asp:LinkButton>--%>
                                <asp:Label ID="lblLastName" runat="server" Text='<%# Eval("LastName") %>' CssClass="chk"></asp:Label>
                            </ItemTemplate>     
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="lblFirstName" runat="server" Text='<%# Eval("FirstName") %>' CssClass="chk1"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField> 
                        <asp:TemplateField ItemStyle-Width="20%">
                            <ItemTemplate>
                                <asp:Label ID="lblCompanyName" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>                                           
                        <asp:TemplateField ItemStyle-Width="20%">
                            <ItemTemplate>
                                <asp:Label ID="lblPhoneNumber" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField> 
                    </Columns>       

<EmptyDataRowStyle HorizontalAlign="Center" BackColor="White"></EmptyDataRowStyle>
                    <EmptyDataTemplate>
                        <table>
                            <tr>
                                <td colspan="3" align="center">
                                    <b>No record found !</b>
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>             

<HeaderStyle BackColor="#98BCD6"></HeaderStyle>

<RowStyle CssClass="con"></RowStyle>
                </asp:GridView>
</tbody></table> 
</div>

<script language="javascript" type="text/javascript">
    function CheckOtherIsCheckedByGVID(spanChk) {
        if (jQuery("#" + spanChk.id).is(":checked")) {
            //var id = jQuery("#" + spanChk.id).attr("Access");
            //alert(id);
            jQuery("#cntMainBody_AgentClient1_grvClients tr").find("td:first input").each(function () { jQuery(this).attr("checked", false); });
            jQuery("#cntMainBody_AgentClient1_grvClients tr").removeAttr("style");
            jQuery("#" + spanChk.id).attr("checked", true);
            //jQuery("#<%= lblLoggingUser.ClientID %>").text("hello");
            //alert(jQuery("#" + spanChk.id).parent().parent().find(".chk").html()); 

            //jQuery('#<%=lblLoggingUser.ClientID %>').html("You can start booking with this selected client : " + jQuery("#" + spanChk.id).parent().parent().find(".chk").html() + '-' + jQuery("#" + spanChk.id).parent().parent().find(".chk1").html());
            var HdnValue = jQuery("#" + spanChk.id).parent().parent().find("input:hidden").val();
            jQuery('#<%=hdnUserId.ClientID %>').val(HdnValue);
        }
        else {
            jQuery('#<%=hdnUserId.ClientID %>').val("0");
        }
    }
    </script>
     