﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopMenu.ascx.cs" Inherits="UserControl_Agency_TopMenu" %>
<ul id="subnav">
    <%--<li class="cat-item">
                                <asp:HyperLink ID="lnkEarnedCommisions" NavigateUrl="~/Agency/Commisions.aspx" runat="server">Earned Commisions</asp:HyperLink>
                            </li> --%>
    <%if (objUsers.Usertype == (int)LMMR.Business.Usertype.Agency && !objUsers.WorkAsAgentUser)
      { %>
    <li class="cat-item">
        <asp:HyperLink ID="lnkControlPanel" NavigateUrl="~/Agency/ControlPanel.aspx" onclick="callLoder();" runat="server">Control Panel</asp:HyperLink>
    </li>
    <li class="cat-item">
        <asp:HyperLink ID="lnkStatistics" NavigateUrl="~/Agency/StatisticsMaster.aspx" onclick="callLoder();" runat="server">Statistics</asp:HyperLink>
    </li>
    <li class="cat-item">

        <asp:HyperLink ID="lnkAgencyusers" NavigateUrl="~/Agency/Users.aspx" onclick="callLoder();" runat="server">Users</asp:HyperLink>
        
    </li>
    <li class="cat-item">

        <asp:HyperLink ID="lnkAgencySetting" NavigateUrl="~/Agency/AgencySetting.aspx" onclick="callLoder();" runat="server">Settings</asp:HyperLink>
    </li>
   
    <%} %>
    <%if (objUsers.Usertype == (int)LMMR.Business.Usertype.agencyuser || objUsers.WorkAsAgentUser)
      { %>
    <li class="cat-item">
        <asp:HyperLink ID="LinkButton2" NavigateUrl="~/Agency/ControlPanelUser.aspx" onclick="callLoder();" runat="server">Control Panel</asp:HyperLink>
    </li>
    <li class="cat-item">
        <asp:HyperLink ID="lnkClient" runat="server" onclick="callLoder();" NavigateUrl="~/Agency/AgencyClients.aspx">Client</asp:HyperLink>
    </li>
    <li class="cat-item">
        <asp:HyperLink ID="lnkAgencyClients" runat="server" onclick="return callLoder2();" NavigateUrl="#">New Booking/Request</asp:HyperLink><div style="display:none;">
        <asp:Button ID="lnkAgc" runat="server" OnClick="lnkAgencyClients_Click" /></div>
    </li>
     <li class="cat-item">

        <asp:HyperLink ID="HyperLink1" NavigateUrl="~/Agency/SendRfp.aspx" onclick="callLoder();" runat="server">Send RFP</asp:HyperLink>
    </li>
    <li class="cat-item">
        <asp:HyperLink ID="lnkSearchBooking" onclick="callLoder();" NavigateUrl="~/Agency/SearchBooking.aspx?type=0"
            runat="server">Search booking</asp:HyperLink>
    </li>
    <li class="cat-item">
        <asp:HyperLink ID="lnkSearchRequest" runat="server" onclick="callLoder();" NavigateUrl="~/Agency/SearchBooking.aspx?type=1">Search request</asp:HyperLink>
    </li>
    <li class="cat-item">
        <asp:HyperLink ID="LinkButton1" NavigateUrl="~/Agency/Statistics.aspx" onclick="callLoder();" runat="server">Statistics</asp:HyperLink>
    </li>
    
    <%} %>
    
</ul>
<script language="javascript">
    function callLoder() {
        jQuery('html').scrollTop(0);
        jQuery('body').scrollTop(0);
        document.getElementsByTagName('html')[0].style.overflow = 'hidden';
        //jQuery("#Loding_overlay").height(jQuery("document").height());
        jQuery("#Loding_overlay").show();
    }
    function callLoder2() {
        document.getElementById("<%= lnkAgc.ClientID %>").click();
        return false;
    }
</script>