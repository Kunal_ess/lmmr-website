﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchBookingPage.ascx.cs" Inherits="UserControl_Agency_SearchBookingPage" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:HiddenField ID="hdnManageBookingRequest" runat="server" />
<asp:HiddenField ID="hdnHotelId" runat="server" />
<asp:HiddenField ID="hdnMeetingRoomId" runat="server" />
<asp:HiddenField ID="hdnConfigurationId" runat="server" />
<asp:HiddenField ID="hdnRequestedHotelId" runat="server" />
<asp:HiddenField ID="hdnRequestedMeetingRoomId" runat="server" />
<asp:HiddenField ID="hdnRequestedConfigurationId" runat="server" />
<asp:HiddenField ID="hdnType" runat="server" />
<asp:HiddenField ID="hdnColId" runat="server" />
<asp:HiddenField ID="hdnType2" runat="server" />
<asp:HiddenField ID="hdnRequestCount" runat="server" />
<asp:HiddenField ID="hdnBookingCount" runat="server" />
<div class="resultbody">
    <!--found-results START HERE-->
    <asp:Panel ID="sortingPanel" runat="server">
        <div class="found-results">
            <div class="found">
                <%= GetKeyResult("WEFOUND")%>
                <span>
                    <asp:Label ID="lblResultCount" runat="server" Text=""></asp:Label></span>
                <%= GetKeyResult("VENUES")%>
                <a href="javascript:void(0);" id="a4" onclick="ShowRequestOnDemand('booking');" style="text-decoration:none;color:#97C63A">
                        "<%= GetKeyResult("BOOKONLINE")%>" </a>
            </div>
            <div class="sort">
                <%= GetKeyResult("SORTBY")%>:</div>
            <div class="stars">
                <asp:LinkButton ID="lbtPrice" runat="server" OnClick="lbtPrice_Click"><%= GetKeyResult("PRICE")%></asp:LinkButton></div>
            <div class="stars">
                <asp:LinkButton ID="lbtStars" runat="server" OnClick="lbtStars_Click"><%= GetKeyResult("STARS")%></asp:LinkButton></div>
                <div class="stars">
            <asp:LinkButton ID="lbtReview" runat="server" OnClick="lbtReview_Click"><%= GetKeyResult("REVIEW")%></asp:LinkButton></div>
            <div class="distance">
                <%= GetKeyResult("DISTANCETO")%>:
                <asp:DropDownList ID="drpDistanceBooking" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpDistanceBooking_SelectedIndexChanged"
                    Width="110px">
                </asp:DropDownList>
                <asp:DropDownList ID="drpDistanceRequest" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpDistanceRequest_SelectedIndexChanged"
                    Width="110px">
                </asp:DropDownList>
            </div>
            <%--<div class="guest-re">
                <a>
                    <%= GetKeyResult("GUESTREVIEW")%></a></div>--%>
        </div>
    </asp:Panel>
    <!--found-results ENDS HERE-->
    <!--pageing START HERE-->
    <%-- <div class="pageing">
            <a href="#" class="arrow-right select"></a>
            <div class="pageing-mid">
                <a href="#">Prevous</a> <a href="#" class="no">1</a><a href="#" class="no select">2</a><a
                    href="#" class="no">3</a> ... <a href="#" class="no">8</a> <a href="#">next</a></div>
            <a href="#" class="arrow-left"></a>
        </div>--%>
    <!--pageing ENDS HERE-->
    <!--found-results-mainbody START HERE-->
    <div class="bodyfor-icon">
        <div class="bodyfor-icon-left">
            <i><%= GetKeyResult("WEFOUND")%>
                <span>
                    <asp:Label ID="lblResultRequestCount" runat="server" Text=""></asp:Label></span>
                <%= GetKeyResult("VENUES")%> </i>&nbsp;
                    <a href="javascript:void(0);" id="a1" onclick="ShowRequestOnDemand('request');">
                        "<%= GetKeyResult("ONREQUEST")%>" </a>
        </div>
        <div class="bodyfor-icon-right">
            <img src="<%= SiteRootPath %>images/map-green.jpg" align="absmiddle" />
            <%= GetKeyResult("TOBOOK")%>
            <img src="<%= SiteRootPath %>images/map-red.jpg" align="absmiddle" />
            <%= GetKeyResult("TOBOOKWITHPROMATION")%>
            <img src="<%= SiteRootPath %>images/map-blue.jpg" align="absmiddle" />
            <%= GetKeyResult("SENDREQUEST")%>
        </div>
    </div>
    <!--found-results-mainbody START HERE--><div style="display:none;"><asp:Button ID="lnkRequestCall" runat="server" OnClick="lnkRequestCall_OnClick" Text="request" Visible="false"></asp:Button></div>
    <div id="TabbedPanels2" class="TabbedPanels">
        <div id="divTabs" runat="server">
            <ul class="TabbedPanelsTabGroup" style="overflow: hidden">
                <li class="TabbedPane   lsTab-green" style="float: left" tabindex="0" id="liTabBooking"
                    onclick="ShowRequestOnDemand('booking');"><a href="javascript:void(0);" class="select"
                        id="abooking">
                        <%= GetKeyResult("BOOKONLINE")%></a></li>
                <li class="TabbedPanelsTab2" tabindex="0" style="float: left" id="liTabRequest" onclick="ShowRequestOnDemand('request');">
                    <a href="javascript:void(0);" id="arequest">
                        <%= GetKeyResult("SENDREQUEST")%></a></li>
            </ul>
            <div class="priceslink">
                <asp:Image ID="infoIcon" runat="server" ImageUrl="~/Images/infoicon.png" CssClass="information" />
                <b>
                    <asp:Label ID="lblPriceDetail" runat="server" CssClass="information"></asp:Label></b>
            </div>
        </div>
        <div style="float:left;" class="merafocusdiv" >
        <div class="Divcover" id="divBookRequest" runat="server" visible="false" ></div>
        <asp:Panel ID="pnlBookRequest" runat="server">
            <div class="TabbedPanelsContentGroup">
                <div class="TabbedPanelsContent" id="divBooking">
                    <div class="found-results-mainbody">
                        <asp:GridView runat="server" ID="grvBooking" AutoGenerateColumns="false" Width="100%"
                            DataKeyNames="HotelId" AllowPaging="True" EmptyDataRowStyle-HorizontalAlign="Center"
                            BorderWidth="0" OnPageIndexChanging="grvBooking_PageIndexChanging" GridLines="None"
                            CellPadding="0" CellSpacing="0" ShowHeader="true" ShowFooter="true" PageSize="10"
                            OnRowDataBound="grvBooking_RowDataBound" OnRowCommand="grvBooking_RowCommand">
                            <%--<PagerStyle BackColor="White" ForeColor="White" HorizontalAlign="Right" Font-Overline="False"
                        Font-Strikeout="False" BorderWidth="0" />--%>
                            <EmptyDataTemplate>
                                <div class="bodyfor-icon1">
                                    <div class="bodyfor-icon-left1" style="text-align: center">
                                        <%= GetKeyResult("NOFOUNDMESSAGE1")%>
                                    </div>
                                    <div class="bodyfor-icon-left2" style="text-align: center">
                                        <%= GetKeyResult("NOFOUNDMESSAGE2")%>
                                    </div>
                                    <div class="bodyfor-icon-right1">
                                        <div class="bodyfor-icon-btn1">
                                            <a href="#" class="send-request-btn" onclick="return ShowRequestOnDemand('request');">
                                                <%= GetKeyResult("SENDREQUEST")%></a>
                                        </div>
                                    </div>
                                </div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                         <div class="Divcover" id="divcover" runat="server" visible="false"></div>
                                        <asp:Panel ID="pnlBooking" runat="server" BackColor="Gray">
                                            <ul>
                                                <li class="first">
                                                    <!--all-results START HERE-->
                                                    <div class="all-results">
                                                        <!--all-results-left START HERE-->
                                                        <div class="all-results-left">
                                                            <!--all-results-left-left START HERE-->
                                                            <div class="all-results-left-left">
                                                                <asp:Image ID="imgHotel" runat="server" Height="100" Width="100" /><br />
                                                                <asp:HyperLink ID="hypPhoto" runat="server" style="text-decoration:underline;"><%= GetKeyResult("PICTURES")%></asp:HyperLink>
                                                            </div>
                                                            <!--all-results-left-left START HERE-->
                                                            <!--all-results-left-right END HERE-->
                                                            <div class="all-results-left-right">
                                                                <div class="star">
                                                                    <asp:Image ID="imgStar" runat="server" />
                                                                </div>
                                                                <h3>
                                                                    <asp:HiddenField ID="hdnHotelId" runat="server" Value='<%#Eval("HotelId") %>' />
                                                                    <asp:Label ID="lblHotelName" runat="server" Text='<%#Eval("HotelName") %>'></asp:Label>
                                                                </h3>
                                                                <h4>
                                                                    <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label></h4>
                                                                <p>
                                                                    <asp:Label ID="lblDescription" runat="server" Text=""></asp:Label>
                                                                </p>
                                                                <div class="watch" id="divVideo" runat="server">
                                                                    <img src="<%= SiteRootPath %>images/r-video-icon.png" align="absmiddle" />
                                                                    <span>
                                                                        <asp:LinkButton ID="lnbVideo" runat="server" ForeColor="Black">Watch <%= GetKeyResult("VIDEO")%></asp:LinkButton>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <!--all-results-left-right END HERE-->
                                                        </div>
                                                        <!--all-results-left END HERE-->
                                                        <!--all-results-right START HERE-->
                                                        <div class="all-results-right">
                                                            <div class="results-top">
                                                                <div class="results-review">
                                                                        <p>
                                                                            <%= GetKeyResult("REVIEW")%><br /><asp:Label ID="lblReview" runat="server" Text=""></asp:Label></p>
                                                                        <span></span>
                                                                    </div>
                                                                <div class="results-from-airport">
                                                                    <span>
                                                                        <asp:Label ID="lblAirport" runat="server" Text=""></asp:Label></span>
                                                                </div>
                                                                <div class="results-price-dis" style="display: none" id="divDiscount" runat="server">
                                                                    <div class="results-price-dis-left">
                                                                        <img src="<%= SiteRootPath %>images/euro-red.png" />
                                                                    </div>
                                                                    <div class="results-price-dis-right">
                                                                        <span class="euro"><asp:Label ID="lblCurrencySign" runat="server" Text=""></asp:Label></span> <span><span class="dis-bg">
                                                                            <asp:Label ID="lblActPkgPrice" runat="server" Text=""></asp:Label>
                                                                            <asp:HiddenField ID="hdnDDRpercent" runat="server" Value='<%#Eval("DDRPercent") %>' />
                                                                        </span></span><b>
                                                                            <%= GetKeyResult("PPDAY")%></b>
                                                                        <p>
                                                                            <span><asp:Label ID="lblCurrencySign1" runat="server" Text=""></asp:Label>
                                                                                <asp:Label ID="lblDiscountPrice" runat="server" Text=""></asp:Label>
                                                                            </span>
                                                                        </p>
                                                                        <asp:HiddenField ID="hdnPkgPrice" runat="server" Value='<%#Eval("ActualPkgPrice") %>' />
                                                                        <asp:HiddenField ID="hdnPkgHalfPrice" runat="server" Value='<%#Eval("ActualPkgPriceHalfDay") %>' />
                                                                        <%--<p>Discount <span class="red">-7%</span></p>--%>
                                                                    </div>
                                                                </div>
                                                                <div class="results-price" style="display: none" id="divPkg" runat="server">
                                                                    <span class="euro"><asp:Label ID="lblCurrencySign2" runat="server" Text=""></asp:Label></span> <span>
                                                                        <asp:Label ID="lblActPAckagePrice" runat="server" Text=""></asp:Label></span>
                                                                    <b>
                                                                        <%= GetKeyResult("PPDAY")%></b>
                                                                </div>
                                                            </div>
                                                            <div class="results-bottom">
                                                                <asp:LinkButton ID="lnbBookOnline" runat="server" CommandName="Booking" CommandArgument='<%#Eval("HotelId") %>'
                                                                    CssClass="book-online-btn" OnClientClick="javascript: _gaq.push(['_trackPageview', 'Step1-BookOnline.aspx']);"><%= GetKeyResult("BOOKONLINE")%></asp:LinkButton>
                                                                <span><%= GetKeyResult("OR")%></span>
                                                                <asp:LinkButton ID="lnbRequestOnline" runat="server" CommandName="Request" CommandArgument='<%#Eval("HotelId") %>'
                                                                    CssClass="send-request-btn" OnClientClick="javascript: _gaq.push(['_trackPageview', 'Step1-SendRequest.aspx']);"><%= GetKeyResult("SENDAREQUEST")%></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                        <!--all-results-right  END HERE-->
                                                    </div>
                                                    <!--all-results END HERE-->
                                                    <!--all-results-bottom START HERE-->
                                                    <div class="all-results-bottom">
                                                        <div class="all-results-bottom-left">
                                                            <div class="all-results-bottom-left-online">
                                                                <span>
                                                                    <asp:Label ID="lblMeetingroomOnline" runat="server" Text=""></asp:Label></span>
                                                                <%= GetKeyResult("ONLINETOBOOK")%>
                                                            </div>
                                                            <div class="all-results-bottom-left-icon">
                                                                <asp:HyperLink ID="hypDedicated" runat="server" Visible="false">
                                                                    <asp:Image ID="Image4" ImageUrl="~/Uploads/FacilityIcon/meeting.png" runat="server" CssClass="information" 
                                                                        ToolTip="Dedicated meeting coordinator" /></asp:HyperLink>
                                                                <asp:HyperLink ID="hypClock" runat="server" Visible="false">
                                                                    <asp:Image ID="imgClock" runat="server" ImageUrl="~/Uploads/FacilityIcon/bar-resto.png" CssClass="information" 
                                                                        class="select" ToolTip="Restaurant" />
                                                                </asp:HyperLink>
                                                                <asp:HyperLink ID="hypDateIcon" runat="server" Visible="false">
                                                                    <asp:Image ID="imgDateIcon" ImageUrl="~/Uploads/FacilityIcon/daylight-in-all-meeting-rooms.png" CssClass="information" 
                                                                        runat="server" ToolTip="All meeting rooms have natural daylight" /></asp:HyperLink>
                                                                <asp:HyperLink ID="hyplockicon" runat="server" Visible="false">
                                                                    <asp:Image ID="Image1" ImageUrl="~/Uploads/FacilityIcon/bedroom-icon.png" runat="server" CssClass="information" 
                                                                        ToolTip="Bedroom Available" /></asp:HyperLink>
                                                                <asp:HyperLink ID="hypPrinticon" runat="server" Visible="false">
                                                                    <asp:Image ID="Image2" ImageUrl="~/Uploads/FacilityIcon/private-parking.png" runat="server" CssClass="information" 
                                                                        ToolTip="Free parking" /></asp:HyperLink>
                                                                <asp:HyperLink ID="hypchticon" runat="server" Visible="false">
                                                                    <asp:Image ID="Image3" ImageUrl="~/Uploads/FacilityIcon/wifi.png" runat="server" CssClass="information" 
                                                                        ToolTip="WIFI complimentary" /></asp:HyperLink>
                                                            </div>
                                                            <div class="all-results-bottom-left-map showmapdiv" style="position: relative; text-align: center;">
                                                                <span class="long">
                                                                    <asp:HiddenField ID="hdnLong" runat="server" Value='<%# Eval("Longitude") %>' />
                                                                </span><span class="lat">
                                                                    <asp:HiddenField ID="hdnLat" runat="server" Value='<%# Eval("latitude") %>' />
                                                                </span><a href="javascript:void(0);" class="showMap">
                                                                    <img align="absmiddle" src="<%= SiteRootPath %>images/map-icon.png">
                                                                    <%=GetKeyResult("SHOWMAP") %></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                                                        </div>
                                                        <div class="all-results-bottom-right">
                                                            <%--<a href="#" id="see_all">See all</a>  --%>
                                                            <asp:LinkButton ID="btnSeeAll" runat="server" CommandName="Popup" CommandArgument='<%#Eval("HotelId") %>'><%= GetKeyResult("SEEALLMEETINGROOM")%></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                    </div>
                                                    <div class="booking" id="divBmeeting" runat="server" visible="false" >
                                                        <!--booking-online START HERE-->
                                                        <div class="booking-online" id="divBookingMeeting" runat="server">
                                                            <h2>
                                                                <%= GetKeyResult("BOOKONLINE")%>
                                                                <span class="h2arrow">&nbsp;</span></h2>
                                                            <!--booking-online-body START HERE-->
                                                            <div class="booking-online-body">
                                                                <div class="shape">
                                                                    <ul>
                                                                        <li class="dark">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= GetKeyResult("THEATRE")%><br>
                                                                            <img src="<%= SiteRootPath %>images/theatre.gif" /></li>
                                                                        <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= GetKeyResult("SCHOOL")%><br>
                                                                            <img src="<%= SiteRootPath %>images/classroom.gif" /></li>
                                                                        <li class="dark">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= GetKeyResult("USHAPE")%><br>
                                                                            <img src="<%= SiteRootPath %>images/ushape.gif" /></li>
                                                                        <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= GetKeyResult("BOARDROOM")%><br>
                                                                            <img src="<%= SiteRootPath %>images/boardroom.gif" /></li>
                                                                        <li class="dark">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= GetKeyResult("COCKTAIL")%><br>
                                                                            <img src="<%= SiteRootPath %>images/cocktail.gif" /></li>
                                                                    </ul>
                                                                </div>
                                                                <!--Bind Booking for meeting room-->
                                                                <div class="booking-online-body-inner divgrvBookMeeting">
                                                                    <asp:GridView runat="server" ID="grvBookMeeting" AutoGenerateColumns="false" Width="100%"
                                                                        BorderWidth="0" GridLines="None" CellPadding="0" CellSpacing="0" ShowHeader="false"
                                                                        OnRowDataBound="grvBookMeeting_RowDataBound">
                                                                        <Columns>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <ul>
                                                                                        <!--meeting room START HERE-->
                                                                                        <li>
                                                                                            <div class="booking-online-body-inner-left">
                                                                                                <asp:Image ID="imgHotelImage" runat="server" Width="69" Height="69" />
                                                                                                <br />
                                                                                                <asp:HyperLink ID="hypMeetingPhoto" runat="server" style="text-decoration:underline;"><%= GetKeyResult("PICTURES")%></asp:HyperLink>
                                                                                                <br />
                                                                                                <asp:LinkButton ID="hypDescription" runat="server" CssClass="link" class="meetingroomdesc"><%= GetKeyResult("DESCRIPTION")%></asp:LinkButton>
                                                                                            </div>
                                                                                            <div class="booking-online-body-inner-right">
                                                                                                <div class="booking-online-body-inner-right-top">
                                                                                                    <asp:HiddenField ID="hdnMId" runat="server" Value='<%#Eval("MeetingRoomId") %>' />
                                                                                                    <asp:Label ID="lblMeetingRoomName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                                                                                </div>
                                                                                                <div class="booking-online-body-inner-right-bottom">
                                                                                                    <ul>
                                                                                                        <li class="firstli">
                                                                                                            <div class="firstli-inner">
                                                                                                                <img src="<%= SiteRootPath %>images/book-arrow1.png" align="absmiddle" /><asp:Label ID="lblRoomSize"
                                                                                                                    runat="server" Text='<%#Eval("Surface") + " sqm" %>'></asp:Label></div>
                                                                                                            <div class="firstli-inner">
                                                                                                                <img src="<%= SiteRootPath %>images/book-arrow2.png" align="absmiddle" /><asp:Label ID="lblHeight" runat="server"
                                                                                                                    Text='<%#Eval("Height") + " m" %>'></asp:Label></div>
                                                                                                            <div class="firstli-inner1">
                                                                                                                <asp:HyperLink ID="linkviewPlan" runat="server" Target="_blank"><%= GetKeyResult("FLOORPLAN")%></asp:HyperLink></div>
                                                                                                        </li>
                                                                                                        <li class="dark">
                                                                                                            <div class="meetingroom">
                                                                                                                <p>
                                                                                                                    <asp:Label ID="lblTheatreMin" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax"
                                                                                                                        runat="server" Text=""></asp:Label></p>
                                                                                                            </div>
                                                                                                            <asp:CheckBox ID="chkTheatre" runat="server" class="styledcheckbox" />
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <div class="meetingroom">
                                                                                                                <p>
                                                                                                                    <asp:Label ID="lblReceptionMin" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax"
                                                                                                                        runat="server" Text=""></asp:Label>
                                                                                                                </p>
                                                                                                            </div>
                                                                                                            <asp:CheckBox ID="chkSchool" runat="server" class="styledcheckbox" />
                                                                                                        </li>
                                                                                                        <li class="dark">
                                                                                                            <div class="meetingroom">
                                                                                                                <p>
                                                                                                                    <asp:Label ID="lblUShapeMin" runat="server" Text=""></asp:Label>
                                                                                                                    <asp:Label ID="lblUShapeMax" runat="server" Text=""></asp:Label></p>
                                                                                                            </div>
                                                                                                            <asp:CheckBox ID="chkUshape" runat="server" class="styledcheckbox" />
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <div class="meetingroom">
                                                                                                                <p>
                                                                                                                    <asp:Label ID="lblBanquetMin" runat="server" Text=""></asp:Label>
                                                                                                                    <asp:Label ID="lblBanquetMax" runat="server" Text=""></asp:Label></p>
                                                                                                            </div>
                                                                                                            <asp:CheckBox ID="chkBoardroom" runat="server" class="styledcheckbox" />
                                                                                                        </li>
                                                                                                        <li class="dark">
                                                                                                            <div class="meetingroom">
                                                                                                                <p>
                                                                                                                    <asp:Label ID="lblCocktailMin" runat="server" Text=""></asp:Label>
                                                                                                                    <asp:Label ID="lblCocktailMax" runat="server" Text=""></asp:Label></p>
                                                                                                            </div>
                                                                                                            <asp:CheckBox ID="chkCocktail" runat="server" class="styledcheckbox" />
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>
                                                                                        </li>
                                                                                        <!--meeting room END HERE-->
                                                                                    </ul>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                                <!--Bind booking for Secondary meeting room-->
                                                                <div class="booking-online-body-inner">
                                                                    <asp:Repeater ID="rptSecondaryMeeting" runat="server" OnItemDataBound="rptSecondaryMeeting_ItemDataBound">
                                                                        <ItemTemplate>
                                                                            <ul>
                                                                                <!--meeting room START HERE-->
                                                                                <li>
                                                                                    <div class="booking-online-body-inner-left">
                                                                                        <asp:Image ID="imgHotelImage" runat="server" Width="69" Height="69" />
                                                                                        <div class="booking-online-body-inner-left-text">
                                                                                            <asp:HyperLink ID="linkviewPlan" runat="server" Target="_blank"><%= GetKeyResult("FLOORPLAN")%></asp:HyperLink></div>
                                                                                    </div>
                                                                                    <div class="booking-online-body-inner-right" id="divRptId" runat="server">
                                                                                        <div class="booking-online-body-inner-right-top">
                                                                                            <asp:HiddenField ID="hdnMId" runat="server" Value='<%#Eval("MeetingRoomId") %>' />
                                                                                            <asp:Label ID="lblMeetingRoomName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                                                                        </div>
                                                                                        <asp:Repeater ID="rptDays" runat="server" OnItemDataBound="rptDays_ItemDataBound">
                                                                                            <ItemTemplate>
                                                                                                <div id="repDivItem" runat="server">
                                                                                                    <div class="booking-online-body-inner-right-top2">
                                                                                                        <div class="booking-online-body-inner-right-top1-left">
                                                                                                            <asp:Label ID="lblDate" runat="server" Text=""></asp:Label>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="booking-online-body-inner-right-bottom1">
                                                                                                        <ul id="divFullday" runat="server">
                                                                                                            <li class="processfirstli">
                                                                                                                <%= GetKeyResult("FULLDAY")%>
                                                                                                            </li>
                                                                                                            <li class="processdark">
                                                                                                                <asp:CheckBox ID="chkTheatre2" runat="server" />
                                                                                                                <asp:Label ID="lblTheatreMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax2"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                            <li class="process">
                                                                                                                <asp:CheckBox ID="chkSchool2" runat="server" />
                                                                                                                <asp:Label ID="lblReceptionMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax2"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                            <li class="processdark">
                                                                                                                <asp:CheckBox ID="chkUshape2" runat="server" />
                                                                                                                <asp:Label ID="lblUShapeMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblUShapeMax2"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                            <li class="process">
                                                                                                                <asp:CheckBox ID="chkBoardroom2" runat="server" />
                                                                                                                <asp:Label ID="lblBanquetMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblBanquetMax2"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                            <li class="processdark">
                                                                                                                <asp:CheckBox ID="chkCocktail2" runat="server" />
                                                                                                                <asp:Label ID="lblCocktailMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblCocktailMax2"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                    <div class="booking-online-body-inner-right-bottom1">
                                                                                                        <ul id="divMorning" runat="server">
                                                                                                            <li class="processfirstli">
                                                                                                                <%= GetKeyResult("MORNING")%></li>
                                                                                                            <li class="processdark">
                                                                                                                <asp:CheckBox ID="chkTheatre" runat="server" />
                                                                                                                <asp:Label ID="lblTheatreMin" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                            <li class="process">
                                                                                                                <asp:CheckBox ID="chkSchool" runat="server" />
                                                                                                                <asp:Label ID="lblReceptionMin" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                            <li class="processdark">
                                                                                                                <asp:CheckBox ID="chkUshape" runat="server" />
                                                                                                                <asp:Label ID="lblUShapeMin" runat="server" Text=""></asp:Label><asp:Label ID="lblUShapeMax"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                            <li class="process">
                                                                                                                <asp:CheckBox ID="chkBoardroom" runat="server" />
                                                                                                                <asp:Label ID="lblBanquetMin" runat="server" Text=""></asp:Label><asp:Label ID="lblBanquetMax"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                            <li class="processdark">
                                                                                                                <asp:CheckBox ID="chkCocktail" runat="server" />
                                                                                                                <asp:Label ID="lblCocktailMin" runat="server" Text=""></asp:Label><asp:Label ID="lblCocktailMax"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                    <div class="booking-online-body-inner-right-bottom1">
                                                                                                        <ul id="divAfternoon" runat="server">
                                                                                                            <li class="processfirstli">
                                                                                                                <%= GetKeyResult("AFTERNOON")%>
                                                                                                            </li>
                                                                                                            <li class="processdark">
                                                                                                                <asp:CheckBox ID="chkTheatre1" runat="server" />
                                                                                                                <asp:Label ID="lblTheatreMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax1"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                            <li class="process">
                                                                                                                <asp:CheckBox ID="chkSchool1" runat="server" />
                                                                                                                <asp:Label ID="lblReceptionMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax1"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                            <li class="processdark">
                                                                                                                <asp:CheckBox ID="chkUshape1" runat="server" />
                                                                                                                <asp:Label ID="lblUShapeMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblUShapeMax1"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                            <li class="process">
                                                                                                                <asp:CheckBox ID="chkBoardroom1" runat="server" />
                                                                                                                <asp:Label ID="lblBanquetMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblBanquetMax1"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                            <li class="processdark">
                                                                                                                <asp:CheckBox ID="chkCocktail1" runat="server" />
                                                                                                                <asp:Label ID="lblCocktailMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblCocktailMax1"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                            <AlternatingItemTemplate>
                                                                                                <div id="repDiv" runat="server">
                                                                                                    <div class="booking-online-body-inner-right-top2">
                                                                                                        <div class="booking-online-body-inner-right-top1-left">
                                                                                                            <asp:Label ID="lblDate" runat="server" Text=""></asp:Label>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="booking-online-body-inner-right-bottom1">
                                                                                                        <ul id="divFullday" runat="server">
                                                                                                            <li class="processfirstli">
                                                                                                                <%= GetKeyResult("FULLDAY")%>
                                                                                                            </li>
                                                                                                            <li class="processdark">
                                                                                                                <asp:CheckBox ID="chkTheatre2" runat="server" />
                                                                                                                <asp:Label ID="lblTheatreMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax2"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                            <li class="process">
                                                                                                                <asp:CheckBox ID="chkSchool2" runat="server" />
                                                                                                                <asp:Label ID="lblReceptionMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax2"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                            <li class="processdark">
                                                                                                                <asp:CheckBox ID="chkUshape2" runat="server" />
                                                                                                                <asp:Label ID="lblUShapeMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblUShapeMax2"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                            <li class="process">
                                                                                                                <asp:CheckBox ID="chkBoardroom2" runat="server" />
                                                                                                                <asp:Label ID="lblBanquetMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblBanquetMax2"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                            <li class="processdark">
                                                                                                                <asp:CheckBox ID="chkCocktail2" runat="server" />
                                                                                                                <asp:Label ID="lblCocktailMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblCocktailMax2"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                    <div class="booking-online-body-inner-right-bottom1">
                                                                                                        <ul id="divMorning" runat="server">
                                                                                                            <li class="processfirstli">
                                                                                                                <%= GetKeyResult("MORNING")%>
                                                                                                            </li>
                                                                                                            <li class="processdark">
                                                                                                                <asp:CheckBox ID="chkTheatre" runat="server" />
                                                                                                                <asp:Label ID="lblTheatreMin" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                            <li class="process">
                                                                                                                <asp:CheckBox ID="chkSchool" runat="server" />
                                                                                                                <asp:Label ID="lblReceptionMin" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                            <li class="processdark">
                                                                                                                <asp:CheckBox ID="chkUshape" runat="server" />
                                                                                                                <asp:Label ID="lblUShapeMin" runat="server" Text=""></asp:Label><asp:Label ID="lblUShapeMax"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                            <li class="process">
                                                                                                                <asp:CheckBox ID="chkBoardroom" runat="server" />
                                                                                                                <asp:Label ID="lblBanquetMin" runat="server" Text=""></asp:Label><asp:Label ID="lblBanquetMax"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                            <li class="processdark">
                                                                                                                <asp:CheckBox ID="chkCocktail" runat="server" />
                                                                                                                <asp:Label ID="lblCocktailMin" runat="server" Text=""></asp:Label><asp:Label ID="lblCocktailMax"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                    <div class="booking-online-body-inner-right-bottom1">
                                                                                                        <ul id="divAfternoon" runat="server">
                                                                                                            <li class="processfirstli">
                                                                                                                <%= GetKeyResult("AFTERNOON")%>
                                                                                                            </li>
                                                                                                            <li class="processdark">
                                                                                                                <asp:CheckBox ID="chkTheatre1" runat="server" />
                                                                                                                <asp:Label ID="lblTheatreMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax1"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                            <li class="process">
                                                                                                                <asp:CheckBox ID="chkSchool1" runat="server" />
                                                                                                                <asp:Label ID="lblReceptionMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax1"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                            <li class="processdark">
                                                                                                                <asp:CheckBox ID="chkUshape1" runat="server" />
                                                                                                                <asp:Label ID="lblUShapeMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblUShapeMax1"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                            <li class="process">
                                                                                                                <asp:CheckBox ID="chkBoardroom1" runat="server" />
                                                                                                                <asp:Label ID="lblBanquetMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblBanquetMax1"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                            <li class="processdark">
                                                                                                                <asp:CheckBox ID="chkCocktail1" runat="server" />
                                                                                                                <asp:Label ID="lblCocktailMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblCocktailMax1"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </AlternatingItemTemplate>
                                                                                        </asp:Repeater>
                                                                                    </div>
                                                                                </li>
                                                                                <!--meeting room END HERE-->
                                                                            </ul>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </div>
                                                            </div>
                                                            <!--booking-online-body END HERE-->
                                                            <!--booking-online-btn START HERE-->
                                                            <div class="booking-online-btn">
                                                                <span style="text-align: left; float: left; width: 650px; padding-left: 10px; margin-top: -10px;
                                                                    margin-bottom: 10px;">
                                                                    <%= GetKeyResult("XXYYNUMBER")%></span>
                                                                <asp:LinkButton ID="lnkAddtoShopping" runat="server" class="add-shopping-cart-btn"
                                                                    OnClientClick="return AddToShopping(this);_gaq.push(['_trackPageview', 'Step2-BookOnline.aspx']);"
                                                                    OnClick="lnkAddtoShopping_Click"><%= GetKeyResult("ADDTOSHOPPINGCART")%> </asp:LinkButton>
                                                                <span><%= GetKeyResult("OR")%></span>
                                                                <asp:LinkButton ID="lnkCancel" runat="server" CommandName="CancelPopup" OnClientClick="CloseDiv(); return true;"><%= GetKeyResult("CANCEL")%></asp:LinkButton>
                                                            </div>
                                                            <!--booking-online-btn END HERE-->
                                                        </div>
                                                        <!--booking-online END HERE-->
                                                        <!--booking-request START HERE-->
                                                        <div class="booking-request" id="divRequest" runat="server" visible="false" >
                                                            <h2>
                                                                <%= GetKeyResult("BOOKONREQUEST")%>
                                                            </h2>
                                                            <!--booking-request-body START HERE-->
                                                            <div class="booking-request-body">
                                                                <div class="shape">
                                                                    <ul>
                                                                        <li class="dark">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= GetKeyResult("THEATRE")%><br>
                                                                            <img src="<%= SiteRootPath %>images/theatre.gif" /></li>
                                                                        <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;School
                                                                            <br>
                                                                            <img src="<%= SiteRootPath %>images/classroom.gif" /></li>
                                                                        <li class="dark">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= GetKeyResult("USHAPE")%><br>
                                                                            <img src="<%= SiteRootPath %>images/ushape.gif" /></li>
                                                                        <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Boardroom
                                                                            <br>
                                                                            <img src="<%= SiteRootPath %>images/boardroom.gif" /></li>
                                                                        <li class="dark">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= GetKeyResult("COCKTAIL")%><br>
                                                                            <img src="<%= SiteRootPath %>images/cocktail.gif" /></li>
                                                                    </ul>
                                                                </div>
                                                                <!--Start Bind Request grid-->
                                                                <div class="divgrvRequestMeeting">
                                                                    <asp:GridView runat="server" ID="grvRequestMeeting" AutoGenerateColumns="false" Width="100%"
                                                                        BorderWidth="0" GridLines="None" CellPadding="0" CellSpacing="0" ShowHeader="false"
                                                                        OnRowDataBound="grvRequestMeeting_RowDataBound">
                                                                        <Columns>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <ul>
                                                                                        <!--meeting room START HERE-->
                                                                                        <li>
                                                                                            <div class="booking-online-body-inner-left">
                                                                                                <asp:Image ID="imgHotelImage" runat="server" Width="57" Height="57" /><br />
                                                                                                <asp:HyperLink ID="hypMeetingPhoto" runat="server" style="text-decoration:underline;"><%= GetKeyResult("PICTURES")%></asp:HyperLink><br />
                                                                                                <asp:LinkButton ID="hypDescription" runat="server" CssClass="link" class="meetingroomdesc">Description</asp:LinkButton>
                                                                                            </div>
                                                                                            <div class="booking-online-body-inner-right">
                                                                                                <div class="booking-online-body-inner-right-top">
                                                                                                    <asp:HiddenField ID="hdnMId" runat="server" Value='<%#Eval("MeetingRoomId") %>' />
                                                                                                    <asp:Label ID="lblMeetingRoomName" runat="server" Text='<%#Eval("MeetingRoomName") %>'></asp:Label>
                                                                                                </div>
                                                                                                <div class="booking-online-body-inner-right-bottom">
                                                                                                    <ul>
                                                                                                        <li class="firstli">
                                                                                                            <div class="firstli-inner">
                                                                                                                <img src="<%= SiteRootPath %>images/book-arrow1.png" align="absmiddle" /><asp:Label ID="lblRoomSize"
                                                                                                                    runat="server" Text='<%#Eval("Surface") + " sqm" %>'></asp:Label></div>
                                                                                                            <div class="firstli-inner">
                                                                                                                <img src="<%= SiteRootPath %>images/book-arrow2.png" align="absmiddle" /><asp:Label ID="lblHeight" runat="server"
                                                                                                                    Text='<%#Eval("Height") + " m" %>'></asp:Label></div>
                                                                                                            <div class="firstli-inner1">
                                                                                                                <asp:HyperLink ID="linkviewPlan" runat="server" Target="_blank"><%= GetKeyResult("FLOORPLAN")%></asp:HyperLink></div>
                                                                                                        </li>
                                                                                                        <li class="dark">
                                                                                                            <div class="meetingroom">
                                                                                                                <p>
                                                                                                                    <asp:Label ID="lblTheatreMin" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax"
                                                                                                                        runat="server" Text=""></asp:Label></p>
                                                                                                            </div>
                                                                                                            <asp:CheckBox ID="chkTheatre" runat="server" class="styledcheckbox" />
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <div class="meetingroom">
                                                                                                                <p>
                                                                                                                    <asp:Label ID="lblReceptionMin" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax"
                                                                                                                        runat="server" Text=""></asp:Label>
                                                                                                                </p>
                                                                                                            </div>
                                                                                                            <asp:CheckBox ID="chkSchool" runat="server" class="styledcheckbox" />
                                                                                                        </li>
                                                                                                        <li class="dark">
                                                                                                            <div class="meetingroom">
                                                                                                                <p>
                                                                                                                    <asp:Label ID="lblUShapeMin" runat="server" Text=""></asp:Label>
                                                                                                                    <asp:Label ID="lblUShapeMax" runat="server" Text=""></asp:Label></p>
                                                                                                            </div>
                                                                                                            <asp:CheckBox ID="chkUshape" runat="server" class="styledcheckbox" />
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <div class="meetingroom">
                                                                                                                <p>
                                                                                                                    <asp:Label ID="lblBanquetMin" runat="server" Text=""></asp:Label>
                                                                                                                    <asp:Label ID="lblBanquetMax" runat="server" Text=""></asp:Label></p>
                                                                                                            </div>
                                                                                                            <asp:CheckBox ID="chkBoardroom" runat="server" class="styledcheckbox" />
                                                                                                        </li>
                                                                                                        <li class="dark">
                                                                                                            <div class="meetingroom">
                                                                                                                <p>
                                                                                                                    <asp:Label ID="lblCocktailMin" runat="server" Text=""></asp:Label>
                                                                                                                    <asp:Label ID="lblCocktailMax" runat="server" Text=""></asp:Label></p>
                                                                                                            </div>
                                                                                                            <asp:CheckBox ID="chkCocktail" runat="server" class="styledcheckbox" />
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>
                                                                                        </li>
                                                                                        <!--meeting room END HERE-->
                                                                                    </ul>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                                <!--End Bind Request grid-->
                                                                <!--Start Bind Secondary Request grid-->
                                                                <asp:Repeater ID="rptRequestSecondary" runat="server" OnItemDataBound="rptRequestSecondary_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <ul>
                                                                            <!--meeting room START HERE-->
                                                                            <li>
                                                                                <div class="booking-online-body-inner-left">
                                                                                    <asp:Image ID="imgHotelImage" runat="server" Width="69" Height="69" />
                                                                                    <div class="booking-online-body-inner-left-text">
                                                                                        <asp:HyperLink ID="linkviewPlan" runat="server" Target="_blank"><%= GetKeyResult("FLOORPLAN")%></asp:HyperLink></div>
                                                                                </div>
                                                                                <div class="booking-online-body-inner-right" id="divRptId" runat="server">
                                                                                    <div class="booking-online-body-inner-right-top">
                                                                                        <asp:HiddenField ID="hdnMId" runat="server" Value='<%#Eval("MeetingRoomId") %>' />
                                                                                        <asp:Label ID="lblMeetingRoomName" runat="server" Text='<%#Eval("MeetingRoomName") %>'></asp:Label>
                                                                                    </div>
                                                                                    <asp:Repeater ID="rptDaysRequest" runat="server" OnItemDataBound="rptDaysRequest_ItemDataBound">
                                                                                        <ItemTemplate>
                                                                                            <div class="booking-online-body-inner-right-bottom1">
                                                                                                <ul id="divFullday" runat="server">
                                                                                                    <li class="processfirstli">
                                                                                                        <asp:Label ID="lblDate" runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="processdark">
                                                                                                        <asp:CheckBox ID="chkTheatre2" runat="server" />
                                                                                                        <asp:Label ID="lblTheatreMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax2"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="process">
                                                                                                        <asp:CheckBox ID="chkSchool2" runat="server" />
                                                                                                        <asp:Label ID="lblReceptionMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax2"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="processdark">
                                                                                                        <asp:CheckBox ID="chkUshape2" runat="server" />
                                                                                                        <asp:Label ID="lblUShapeMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblUShapeMax2"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="process">
                                                                                                        <asp:CheckBox ID="chkBoardroom2" runat="server" />
                                                                                                        <asp:Label ID="lblBanquetMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblBanquetMax2"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="processdark">
                                                                                                        <asp:CheckBox ID="chkCocktail2" runat="server" />
                                                                                                        <asp:Label ID="lblCocktailMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblCocktailMax2"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                            <div class="booking-online-body-inner-right-bottom1" style="display: none">
                                                                                                <ul id="divMorning" runat="server">
                                                                                                    <li class="processfirstli">
                                                                                                        <%= GetKeyResult("MORNING")%>
                                                                                                    </li>
                                                                                                    <li class="processdark">
                                                                                                        <asp:CheckBox ID="chkTheatre" runat="server" />
                                                                                                        <asp:Label ID="lblTheatreMin" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="process">
                                                                                                        <asp:CheckBox ID="chkSchool" runat="server" />
                                                                                                        <asp:Label ID="lblReceptionMin" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="processdark">
                                                                                                        <asp:CheckBox ID="chkUshape" runat="server" />
                                                                                                        <asp:Label ID="lblUShapeMin" runat="server" Text=""></asp:Label><asp:Label ID="lblUShapeMax"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="process">
                                                                                                        <asp:CheckBox ID="chkBoardroom" runat="server" />
                                                                                                        <asp:Label ID="lblBanquetMin" runat="server" Text=""></asp:Label><asp:Label ID="lblBanquetMax"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="processdark">
                                                                                                        <asp:CheckBox ID="chkCocktail" runat="server" />
                                                                                                        <asp:Label ID="lblCocktailMin" runat="server" Text=""></asp:Label><asp:Label ID="lblCocktailMax"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                            <div class="booking-online-body-inner-right-bottom1" style="display: none">
                                                                                                <ul id="divAfternoon" runat="server">
                                                                                                    <li class="processfirstli">
                                                                                                        <%= GetKeyResult("AFTERNOON")%>
                                                                                                    </li>
                                                                                                    <li class="processdark">
                                                                                                        <asp:CheckBox ID="chkTheatre1" runat="server" />
                                                                                                        <asp:Label ID="lblTheatreMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax1"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="process">
                                                                                                        <asp:CheckBox ID="chkSchool1" runat="server" />
                                                                                                        <asp:Label ID="lblReceptionMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax1"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="processdark">
                                                                                                        <asp:CheckBox ID="chkUshape1" runat="server" />
                                                                                                        <asp:Label ID="lblUShapeMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblUShapeMax1"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="process">
                                                                                                        <asp:CheckBox ID="chkBoardroom1" runat="server" />
                                                                                                        <asp:Label ID="lblBanquetMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblBanquetMax1"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="processdark">
                                                                                                        <asp:CheckBox ID="chkCocktail1" runat="server" />
                                                                                                        <asp:Label ID="lblCocktailMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblCocktailMax1"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                        <AlternatingItemTemplate>
                                                                                            <div id="repDiv" runat="server">
                                                                                                <div class="booking-online-body-inner-right-bottom1">
                                                                                                    <ul id="divFullday" runat="server">
                                                                                                        <li class="processfirstli">
                                                                                                            <asp:Label ID="lblDate" runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="processdark">
                                                                                                            <asp:CheckBox ID="chkTheatre2" runat="server" />
                                                                                                            <asp:Label ID="lblTheatreMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax2"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="process">
                                                                                                            <asp:CheckBox ID="chkSchool2" runat="server" />
                                                                                                            <asp:Label ID="lblReceptionMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax2"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="processdark">
                                                                                                            <asp:CheckBox ID="chkUshape2" runat="server" />
                                                                                                            <asp:Label ID="lblUShapeMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblUShapeMax2"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="process">
                                                                                                            <asp:CheckBox ID="chkBoardroom2" runat="server" />
                                                                                                            <asp:Label ID="lblBanquetMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblBanquetMax2"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="processdark">
                                                                                                            <asp:CheckBox ID="chkCocktail2" runat="server" />
                                                                                                            <asp:Label ID="lblCocktailMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblCocktailMax2"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                                <div class="booking-online-body-inner-right-bottom1" style="display: none">
                                                                                                    <ul id="divMorning" runat="server">
                                                                                                        <li class="processfirstli">
                                                                                                            <%= GetKeyResult("MORNING")%>
                                                                                                        </li>
                                                                                                        <li class="processdark">
                                                                                                            <asp:CheckBox ID="chkTheatre" runat="server" />
                                                                                                            <asp:Label ID="lblTheatreMin" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="process">
                                                                                                            <asp:CheckBox ID="chkSchool" runat="server" />
                                                                                                            <asp:Label ID="lblReceptionMin" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="processdark">
                                                                                                            <asp:CheckBox ID="chkUshape" runat="server" />
                                                                                                            <asp:Label ID="lblUShapeMin" runat="server" Text=""></asp:Label><asp:Label ID="lblUShapeMax"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="process">
                                                                                                            <asp:CheckBox ID="chkBoardroom" runat="server" />
                                                                                                            <asp:Label ID="lblBanquetMin" runat="server" Text=""></asp:Label><asp:Label ID="lblBanquetMax"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="processdark">
                                                                                                            <asp:CheckBox ID="chkCocktail" runat="server" />
                                                                                                            <asp:Label ID="lblCocktailMin" runat="server" Text=""></asp:Label><asp:Label ID="lblCocktailMax"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                                <div class="booking-online-body-inner-right-bottom1" style="display: none">
                                                                                                    <ul id="divAfternoon" runat="server">
                                                                                                        <li class="processfirstli">
                                                                                                            <%= GetKeyResult("AFTERNOON")%>
                                                                                                        </li>
                                                                                                        <li class="processdark">
                                                                                                            <asp:CheckBox ID="chkTheatre1" runat="server" />
                                                                                                            <asp:Label ID="lblTheatreMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax1"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="process">
                                                                                                            <asp:CheckBox ID="chkSchool1" runat="server" />
                                                                                                            <asp:Label ID="lblReceptionMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax1"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="processdark">
                                                                                                            <asp:CheckBox ID="chkUshape1" runat="server" />
                                                                                                            <asp:Label ID="lblUShapeMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblUShapeMax1"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="process">
                                                                                                            <asp:CheckBox ID="chkBoardroom1" runat="server" />
                                                                                                            <asp:Label ID="lblBanquetMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblBanquetMax1"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="processdark">
                                                                                                            <asp:CheckBox ID="chkCocktail1" runat="server" />
                                                                                                            <asp:Label ID="lblCocktailMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblCocktailMax1"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>
                                                                                        </AlternatingItemTemplate>
                                                                                    </asp:Repeater>
                                                                                </div>
                                                                            </li>
                                                                            <!--meeting room END HERE-->
                                                                        </ul>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                                <!--End Bind Secondary Request grid-->
                                                            </div>
                                                            <!--booking-request-body END HERE-->
                                                            <!--booking-request-btn START HERE-->
                                                            <div class="booking-request-btn">
                                                                <span style="text-align: left; float: left; width: 650px; padding-left: 10px;">
                                                                    <%= GetKeyResult("XXYYNUMBER")%></span>
                                                                <div class="booking-request-btn-basket">
                                                                    <asp:LinkButton ID="lnkButtonAddToBasket" runat="server" class="add-basket-btn" OnClick="lnkButtonAddToBasket_Click"
                                                                        OnClientClick="return AddToBasket(this);_gaq.push(['_trackPageview', 'Step2-SendRequest.aspx']);"><%= GetKeyResult("ADDTOBASKET")%></asp:LinkButton>
                                                                    <%= GetKeyResult("OR")%>
                                                                    <asp:LinkButton ID="lnkButtonCancel" runat="server" CommandName="CancelPopupRequest"
                                                                        OnClientClick="CloseDiv(); return true;"><%= GetKeyResult("CANCEL")%></asp:LinkButton>
                                                                </div>
                                                                <div class="booking-request-btn-close">
                                                                    <asp:LinkButton ID="lnbClose" runat="server" CommandName="CancelPopupRequest" OnClientClick="CloseDiv(); return true;"><img src="<%= SiteRootPath %>images/close-btn.png"/></asp:LinkButton>
                                                                </div>
                                                            </div>
                                                            <!--booking-request-btn END HERE-->
                                                        </div>
                                                        <!--booking-request END HERE-->
                                                    </div>                                                    
                                                </li>
                                            </ul>
                                        </asp:Panel>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle HorizontalAlign="Center"></EmptyDataRowStyle>
                            <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                            <PagerStyle HorizontalAlign="Right" BackColor="White" />
                            <PagerTemplate>
                                <tr>
                                    <td colspan="11" bgcolor="#ffffff">
                                        <div style="float: right;">
                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                            <asp:PlaceHolder ID="phBot" runat="server"></asp:PlaceHolder>
                                        </div>
                                    </td>
                                </tr>
                            </PagerTemplate>
                            <%--<PagerTemplate>
                        <div id="Paging" style="width: 100%; display: none;">
                            <table cellpadding="0" align="right">
                                <tr>
                                    <td style="vertical-align=middle; height: 22px;">
                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </PagerTemplate>--%>
                        </asp:GridView>
                        <div class="bodyfor-icon-left">
                            <%= GetKeyResult("FORMOREVENUESPLEASECLICKON")%>
                            <a href="javascript:void(0);" id="a2" onclick="ShowRequestOnDemand('request');">
                        "<%= GetKeyResult("SENDREQUEST")%>" </a>
                        </div>
                    </div>
                </div>
                <div class="TabbedPanelsContent" id="divRequest">
                    <div class="found-results-mainbody">
                        <asp:GridView runat="server" ID="grvRequest" AutoGenerateColumns="false" Width="100%"
                            ShowFooter="true" AllowPaging="true" PageSize="10" EmptyDataRowStyle-HorizontalAlign="Center"
                            BorderWidth="0" GridLines="None" CellPadding="0" OnPageIndexChanging="grvRequest_PageIndexChanging"
                            OnRowDataBound="grvRequest_RowDataBound" CellSpacing="0" ShowHeader="true" OnRowCommand="grvRequest_RowCommand">
                            <EmptyDataTemplate>
                                <div class="bodyfor-icon1">
                                    <div class="bodyfor-icon-left1" style="text-align: center">
                                        <%= GetKeyResult("DATANOTAVAILABLE")%>
                                    </div>
                                </div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                    <div class="Divcover" id="divcover" runat="server" visible="false"></div>
                                        <asp:Panel ID="pnlBooking" runat="server" BackColor="Gray">
                                            <ul>
                                                <li class="first">
                                                    <!--all-results START HERE-->
                                                    <div class="all-results">
                                                        <!--all-results-left START HERE-->
                                                        <div class="all-results-left">
                                                            <!--all-results-left-left START HERE-->
                                                            <div class="all-results-left-left">
                                                                <asp:Image ID="imgHotel" runat="server" Height="100" Width="100" /><br />
                                                                <asp:HyperLink ID="hypPhoto" runat="server" style="text-decoration:underline;"><%= GetKeyResult("PICTURES")%></asp:HyperLink>
                                                            </div>
                                                            <!--all-results-left-left START HERE-->
                                                            <!--all-results-left-right END HERE-->
                                                            <div class="all-results-left-right">
                                                                <div class="star">
                                                                    <asp:Image ID="imgStar" runat="server" />
                                                                </div>
                                                                <h3>
                                                                    <asp:HiddenField ID="hdnHotelId" runat="server" Value='<%#Eval("HotelId") %>' />
                                                                    <asp:Label ID="lblHotelName" runat="server" Text='<%#Eval("HotelName") %>'></asp:Label>
                                                                </h3>
                                                                <h4>
                                                                    <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label></h4>
                                                                <p>
                                                                    <asp:Label ID="lblDescription" runat="server" Text=""></asp:Label>
                                                                </p>
                                                                <div class="watch" id="divVideo" runat="server">
                                                                    <img src="<%= SiteRootPath %>images/r-video-icon.png" align="absmiddle" />
                                                                    <span>
                                                                        <asp:LinkButton ID="lnbVideo" runat="server" ForeColor="Black">Watch <%= GetKeyResult("VIDEO")%></asp:LinkButton>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <!--all-results-left-right END HERE-->
                                                        </div>
                                                        <!--all-results-left END HERE-->
                                                        <!--all-results-right START HERE-->
                                                        <div class="all-results-right">
                                                            <div class="results-top">
                                                                <div class="results-review">
                                                                        <p>
                                                                            <%= GetKeyResult("REVIEW")%><br /><asp:Label ID="lblReview" runat="server" Text=""></asp:Label></p>
                                                                        <span></span>
                                                                    </div>
                                                                <div class="results-from-airport">
                                                                    <span>
                                                                        <asp:Label ID="lblAirport" runat="server" Text=""></asp:Label></span>
                                                                </div>
                                                                <div class="results-price-dis" style="display: none" id="divDiscount" runat="server">
                                                                    <div class="results-price-dis-left">
                                                                        <img src="<%= SiteRootPath %>images/euro-red.png" />
                                                                    </div>
                                                                    <div class="results-price-dis-right">
                                                                        <span class="euro"></span> <span><span class="dis-bg">
                                                                            <%--<asp:Label ID="lblDiscountPrice" runat="server" Text='<%#Eval("DDRPercent") %>'></asp:Label>--%>
                                                                            <asp:HiddenField ID="hdnPkgPrice" runat="server" Value='<%#Eval("ActualPkgPrice") %>' />
                                                                            <asp:HiddenField ID="hdnPkgHalfPrice" runat="server" Value='<%#Eval("ActualPkgPriceHalfDay") %>' />
                                                                        </span></span><b>
                                                                            <%= GetKeyResult("PPDAY")%></b>
                                                                        <p>
                                                                            <%= GetKeyResult("FROM")%>
                                                                            <span>
                                                                                <asp:Label ID="lblActPkgPrice" runat="server" Text=""></asp:Label></span>
                                                                            <%= GetKeyResult("PPDAY")%></p>
                                                                        <%--<p>Discount <span class="red">-7%</span></p>--%>
                                                                    </div>
                                                                </div>
                                                                <div class="results-price" style="font-size: 11px; margin-top: -10px">
                                                                    <span class="euro"><asp:Label ID="lblCurrencySign" runat="server" Text=""></asp:Label></span> <span>
                                                                        <asp:Label ID="lblActPAckagePrice" runat="server" Text=""></asp:Label></span>
                                                                    <b>
                                                                        <%= GetKeyResult("PPDAY")%></b>
                                                                </div>
                                                            </div>
                                                            <div class="results-bottom" style="text-align: right;">
                                                                <asp:LinkButton ID="lnbRequestOnline" runat="server" OnClientClick="javascript: _gaq.push(['_trackPageview', 'Step1-SendRequest.aspx']);"
                                                                    CommandName="Request" CommandArgument='<%#Eval("HotelId") %>' CssClass="send-request-btn"
                                                                    Style="margin-right: 13px;"><%= GetKeyResult("SENDAREQUEST")%></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                        <!--all-results-right  END HERE-->
                                                    </div>
                                                    <!--all-results END HERE-->
                                                    <!--all-results-bottom START HERE-->
                                                    <div class="all-results-bottom">
                                                        <div class="all-results-bottom">
                                                            <div class="all-results-bottom-left">
                                                                <div class="all-results-bottom-left-online">
                                                                    <span>
                                                                        <asp:Label ID="lblMeetingroomOnline" runat="server" Text=""></asp:Label></span>
                                                                   <%= GetKeyResult("ONREQUEST")%> 
                                                                </div>
                                                                <div class="all-results-bottom-left-icon">
                                                                    <asp:HyperLink ID="hypDedicated" runat="server" Visible="false">
                                                                        <asp:Image ID="Image4" ImageUrl="~/Uploads/FacilityIcon/meeting.png" runat="server" CssClass="information" 
                                                                            ToolTip="Dedicated meeting coordinator" /></asp:HyperLink>
                                                                    <asp:HyperLink ID="hypClock" runat="server" Visible="false">
                                                                        <asp:Image ID="imgClock" runat="server" ImageUrl="~/Uploads/FacilityIcon/bar-resto.png" CssClass="information" 
                                                                            class="select" ToolTip="Restaurant" />
                                                                    </asp:HyperLink>
                                                                    <asp:HyperLink ID="hypDateIcon" runat="server" Visible="false">
                                                                        <asp:Image ID="imgDateIcon" ImageUrl="~/Uploads/FacilityIcon/daylight-in-all-meeting-rooms.png" CssClass="information" 
                                                                            runat="server" ToolTip="All meeting rooms have natural daylight" /></asp:HyperLink>
                                                                    <asp:HyperLink ID="hyplockicon" runat="server" Visible="false">
                                                                        <asp:Image ID="Image1" ImageUrl="~/Uploads/FacilityIcon/bedroom-icon.png" runat="server" CssClass="information" 
                                                                            ToolTip="Bedroom Available" /></asp:HyperLink>
                                                                    <asp:HyperLink ID="hypPrinticon" runat="server" Visible="false">
                                                                        <asp:Image ID="Image2" ImageUrl="~/Uploads/FacilityIcon/private-parking.png" runat="server" CssClass="information" 
                                                                            ToolTip="Free parking" /></asp:HyperLink>
                                                                    <asp:HyperLink ID="hypchticon" runat="server" Visible="false">
                                                                        <asp:Image ID="Image3" ImageUrl="~/Uploads/FacilityIcon/wifi.png" runat="server" CssClass="information" 
                                                                            ToolTip="WIFI complimentary" /></asp:HyperLink>
                                                                </div>
                                                                <div class="all-results-bottom-left-map showmapdiv" style="position: relative; text-align: center;">
                                                                    <span class="long">
                                                                        <asp:HiddenField ID="hdnLong" runat="server" Value='<%# Eval("Longitude") %>' />
                                                                    </span><span class="lat">
                                                                        <asp:HiddenField ID="hdnLat" runat="server" Value='<%# Eval("latitude") %>' />
                                                                    </span><a href="javascript:void(0);" class="showMap">
                                                                        <img align="absmiddle" src="<%= SiteRootPath %>images/map-icon.png">
                                                                        <%=GetKeyResult("SHOWMAP") %></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                                                            </div>
                                                        </div>
                                                        <!--booking-request START HERE-->
                                                        <div class="booking-request" id="divRequest" runat="server" visible="false" >
                                                            <h2>
                                                                <%=GetKeyResult("BOOKONREQUEST") %>
                                                            </h2>
                                                            <!--booking-request-body START HERE-->
                                                            <div class="booking-request-body">
                                                                <div class="shape">
                                                                    <ul>
                                                                        <li class="dark">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= GetKeyResult("THEATRE")%><br>
                                                                            <img src="<%= SiteRootPath %>images/theatre.gif" /></li>
                                                                        <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;School
                                                                            <br>
                                                                            <img src="<%= SiteRootPath %>images/classroom.gif" /></li>
                                                                        <li class="dark">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= GetKeyResult("USHAPE")%><br>
                                                                            <img src="<%= SiteRootPath %>images/ushape.gif" /></li>
                                                                        <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Boardroom
                                                                            <br>
                                                                            <img src="<%= SiteRootPath %>images/boardroom.gif" /></li>
                                                                        <li class="dark">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= GetKeyResult("COCKTAIL")%><br>
                                                                            <img src="<%= SiteRootPath %>images/cocktail.gif" /></li>
                                                                    </ul>
                                                                </div>
                                                                <!--Start Bind primary Request grid for Request Tab-->
                                                                <asp:GridView runat="server" ID="grvRequesttabMeeting" AutoGenerateColumns="false"
                                                                    Width="100%" BorderWidth="0" GridLines="None" CellPadding="0" CellSpacing="0"
                                                                    ShowHeader="false" OnRowDataBound="grvRequesttabMeeting_RowDataBound">
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <ul>
                                                                                    <!--meeting room START HERE-->
                                                                                    <li>
                                                                                        <div class="booking-online-body-inner-left">
                                                                                            <asp:Image ID="imgHotelImage" runat="server" Width="57" Height="57" />
                                                                                            <br />
                                                                                            <asp:HyperLink ID="hypMeetingPhoto" runat="server" style="text-decoration:underline;"><%= GetKeyResult("PICTURES")%></asp:HyperLink><br />
                                                                                            <asp:LinkButton ID="hypDescription" runat="server" CssClass="link" class="meetingroomdesc">Description</asp:LinkButton>
                                                                                        </div>
                                                                                        <div class="booking-online-body-inner-right">
                                                                                            <div class="booking-online-body-inner-right-top">
                                                                                                <asp:HiddenField ID="hdnMId" runat="server" Value='<%#Eval("MeetingRoomId") %>' />
                                                                                                <asp:Label ID="lblMeetingRoomName" runat="server" Text='<%#Eval("MeetingRoomName") %>'></asp:Label>
                                                                                            </div>
                                                                                            <div class="booking-online-body-inner-right-bottom">
                                                                                                <ul>
                                                                                                    <li class="firstli">
                                                                                                        <div class="firstli-inner">
                                                                                                            <img src="<%= SiteRootPath %>images/book-arrow1.png" align="absmiddle" /><asp:Label ID="lblRoomSize"
                                                                                                                runat="server" Text='<%#Eval("Surface") + " sqm" %>'></asp:Label></div>
                                                                                                        <div class="firstli-inner">
                                                                                                            <img src="<%= SiteRootPath %>images/book-arrow2.png" align="absmiddle" /><asp:Label ID="lblHeight" runat="server"
                                                                                                                Text='<%#Eval("Height") + " m" %>'></asp:Label></div>
                                                                                                        <div class="firstli-inner1">
                                                                                                            <asp:HyperLink ID="linkviewPlan" runat="server" Target="_blank"><%= GetKeyResult("FLOORPLAN")%></asp:HyperLink></div>
                                                                                                    </li>
                                                                                                    <li class="dark">
                                                                                                        <div class="meetingroom">
                                                                                                            <p>
                                                                                                                <asp:Label ID="lblTheatreMin" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax"
                                                                                                                    runat="server" Text=""></asp:Label></p>
                                                                                                        </div>
                                                                                                        <asp:CheckBox ID="chkTheatre" runat="server" class="styledcheckbox" />
                                                                                                    </li>
                                                                                                    <li>
                                                                                                        <div class="meetingroom">
                                                                                                            <p>
                                                                                                                <asp:Label ID="lblReceptionMin" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax"
                                                                                                                    runat="server" Text=""></asp:Label>
                                                                                                            </p>
                                                                                                        </div>
                                                                                                        <asp:CheckBox ID="chkSchool" runat="server" class="styledcheckbox" />
                                                                                                    </li>
                                                                                                    <li class="dark">
                                                                                                        <div class="meetingroom">
                                                                                                            <p>
                                                                                                                <asp:Label ID="lblUShapeMin" runat="server" Text=""></asp:Label>
                                                                                                                <asp:Label ID="lblUShapeMax" runat="server" Text=""></asp:Label></p>
                                                                                                        </div>
                                                                                                        <asp:CheckBox ID="chkUshape" runat="server" class="styledcheckbox" />
                                                                                                    </li>
                                                                                                    <li>
                                                                                                        <div class="meetingroom">
                                                                                                            <p>
                                                                                                                <asp:Label ID="lblBanquetMin" runat="server" Text=""></asp:Label>
                                                                                                                <asp:Label ID="lblBanquetMax" runat="server" Text=""></asp:Label></p>
                                                                                                        </div>
                                                                                                        <asp:CheckBox ID="chkBoardroom" runat="server" class="styledcheckbox" />
                                                                                                    </li>
                                                                                                    <li class="dark">
                                                                                                        <div class="meetingroom">
                                                                                                            <p>
                                                                                                                <asp:Label ID="lblCocktailMin" runat="server" Text=""></asp:Label>
                                                                                                                <asp:Label ID="lblCocktailMax" runat="server" Text=""></asp:Label></p>
                                                                                                        </div>
                                                                                                        <asp:CheckBox ID="chkCocktail" runat="server" class="styledcheckbox" />
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                    </li>
                                                                                    <!--meeting room END HERE-->
                                                                                </ul>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                                <!--End Bind primary Request grid for Request Tab-->
                                                                <!--Start Bind Secondary Request grid for request tab-->
                                                                <asp:Repeater ID="rptRequestTabSecondary" runat="server" OnItemDataBound="rptRequestTabSecondary_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <ul>
                                                                            <!--meeting room START HERE-->
                                                                            <li>
                                                                                <div class="booking-online-body-inner-left">
                                                                                    <asp:Image ID="imgHotelImage" runat="server" Width="69" Height="69" />
                                                                                    <div class="booking-online-body-inner-left-text">
                                                                                        <asp:HyperLink ID="linkviewPlan" runat="server" Target="_blank"><%= GetKeyResult("FLOORPLAN")%></asp:HyperLink></div>
                                                                                </div>
                                                                                <div class="booking-online-body-inner-right" id="divRptId" runat="server">
                                                                                    <div class="booking-online-body-inner-right-top">
                                                                                        <asp:HiddenField ID="hdnMId" runat="server" Value='<%#Eval("MeetingRoomId") %>' />
                                                                                        <asp:Label ID="lblMeetingRoomName" runat="server" Text='<%#Eval("MeetingRoomName") %>'></asp:Label>
                                                                                    </div>
                                                                                    <asp:Repeater ID="rptDaysTabRequest" runat="server" OnItemDataBound="rptDaysTabRequest_ItemDataBound">
                                                                                        <ItemTemplate>
                                                                                            <div class="booking-online-body-inner-right-bottom1">
                                                                                                <ul id="divFullday" runat="server">
                                                                                                    <li class="processfirstli">
                                                                                                        <asp:Label ID="lblDate" runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="processdark">
                                                                                                        <asp:CheckBox ID="chkTheatre2" runat="server" />
                                                                                                        <asp:Label ID="lblTheatreMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax2"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="process">
                                                                                                        <asp:CheckBox ID="chkSchool2" runat="server" />
                                                                                                        <asp:Label ID="lblReceptionMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax2"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="processdark">
                                                                                                        <asp:CheckBox ID="chkUshape2" runat="server" />
                                                                                                        <asp:Label ID="lblUShapeMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblUShapeMax2"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="process">
                                                                                                        <asp:CheckBox ID="chkBoardroom2" runat="server" />
                                                                                                        <asp:Label ID="lblBanquetMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblBanquetMax2"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="processdark">
                                                                                                        <asp:CheckBox ID="chkCocktail2" runat="server" />
                                                                                                        <asp:Label ID="lblCocktailMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblCocktailMax2"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                            <div class="booking-online-body-inner-right-bottom1" style="display: none">
                                                                                                <ul id="divMorning" runat="server">
                                                                                                    <li class="processfirstli">
                                                                                                        <%=GetKeyResult("MORNING") %>
                                                                                                    </li>
                                                                                                    <li class="processdark">
                                                                                                        <asp:CheckBox ID="chkTheatre" runat="server" />
                                                                                                        <asp:Label ID="lblTheatreMin" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="process">
                                                                                                        <asp:CheckBox ID="chkSchool" runat="server" />
                                                                                                        <asp:Label ID="lblReceptionMin" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="processdark">
                                                                                                        <asp:CheckBox ID="chkUshape" runat="server" />
                                                                                                        <asp:Label ID="lblUShapeMin" runat="server" Text=""></asp:Label><asp:Label ID="lblUShapeMax"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="process">
                                                                                                        <asp:CheckBox ID="chkBoardroom" runat="server" />
                                                                                                        <asp:Label ID="lblBanquetMin" runat="server" Text=""></asp:Label><asp:Label ID="lblBanquetMax"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="processdark">
                                                                                                        <asp:CheckBox ID="chkCocktail" runat="server" />
                                                                                                        <asp:Label ID="lblCocktailMin" runat="server" Text=""></asp:Label><asp:Label ID="lblCocktailMax"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                            <div class="booking-online-body-inner-right-bottom1" style="display: none">
                                                                                                <ul id="divAfternoon" runat="server">
                                                                                                    <li class="processfirstli">
                                                                                                        <%=GetKeyResult("AFTERNOON") %>
                                                                                                    </li>
                                                                                                    <li class="processdark">
                                                                                                        <asp:CheckBox ID="chkTheatre1" runat="server" />
                                                                                                        <asp:Label ID="lblTheatreMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax1"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="process">
                                                                                                        <asp:CheckBox ID="chkSchool1" runat="server" />
                                                                                                        <asp:Label ID="lblReceptionMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax1"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="processdark">
                                                                                                        <asp:CheckBox ID="chkUshape1" runat="server" />
                                                                                                        <asp:Label ID="lblUShapeMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblUShapeMax1"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="process">
                                                                                                        <asp:CheckBox ID="chkBoardroom1" runat="server" />
                                                                                                        <asp:Label ID="lblBanquetMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblBanquetMax1"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                    <li class="processdark">
                                                                                                        <asp:CheckBox ID="chkCocktail1" runat="server" />
                                                                                                        <asp:Label ID="lblCocktailMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblCocktailMax1"
                                                                                                            runat="server" Text=""></asp:Label>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                        <AlternatingItemTemplate>
                                                                                            <div id="repDiv" runat="server">
                                                                                                <div class="booking-online-body-inner-right-bottom1">
                                                                                                    <ul id="divFullday" runat="server">
                                                                                                        <li class="processfirstli">
                                                                                                            <asp:Label ID="lblDate" runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="processdark">
                                                                                                            <asp:CheckBox ID="chkTheatre2" runat="server" />
                                                                                                            <asp:Label ID="lblTheatreMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax2"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="process">
                                                                                                            <asp:CheckBox ID="chkSchool2" runat="server" />
                                                                                                            <asp:Label ID="lblReceptionMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax2"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="processdark">
                                                                                                            <asp:CheckBox ID="chkUshape2" runat="server" />
                                                                                                            <asp:Label ID="lblUShapeMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblUShapeMax2"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="process">
                                                                                                            <asp:CheckBox ID="chkBoardroom2" runat="server" />
                                                                                                            <asp:Label ID="lblBanquetMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblBanquetMax2"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="processdark">
                                                                                                            <asp:CheckBox ID="chkCocktail2" runat="server" />
                                                                                                            <asp:Label ID="lblCocktailMin2" runat="server" Text=""></asp:Label><asp:Label ID="lblCocktailMax2"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                                <div class="booking-online-body-inner-right-bottom1" style="display: none">
                                                                                                    <ul id="divMorning" runat="server">
                                                                                                        <li class="processfirstli">
                                                                                                            <%=GetKeyResult("MORNING") %>
                                                                                                        </li>
                                                                                                        <li class="processdark">
                                                                                                            <asp:CheckBox ID="chkTheatre" runat="server" />
                                                                                                            <asp:Label ID="lblTheatreMin" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="process">
                                                                                                            <asp:CheckBox ID="chkSchool" runat="server" />
                                                                                                            <asp:Label ID="lblReceptionMin" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="processdark">
                                                                                                            <asp:CheckBox ID="chkUshape" runat="server" />
                                                                                                            <asp:Label ID="lblUShapeMin" runat="server" Text=""></asp:Label><asp:Label ID="lblUShapeMax"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="process">
                                                                                                            <asp:CheckBox ID="chkBoardroom" runat="server" />
                                                                                                            <asp:Label ID="lblBanquetMin" runat="server" Text=""></asp:Label><asp:Label ID="lblBanquetMax"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="processdark">
                                                                                                            <asp:CheckBox ID="chkCocktail" runat="server" />
                                                                                                            <asp:Label ID="lblCocktailMin" runat="server" Text=""></asp:Label><asp:Label ID="lblCocktailMax"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                                <div class="booking-online-body-inner-right-bottom1" style="display: none">
                                                                                                    <ul id="divAfternoon" runat="server">
                                                                                                        <li class="processfirstli">
                                                                                                            <%=GetKeyResult("AFTERNOON") %>
                                                                                                        </li>
                                                                                                        <li class="processdark">
                                                                                                            <asp:CheckBox ID="chkTheatre1" runat="server" />
                                                                                                            <asp:Label ID="lblTheatreMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblTheatreMax1"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="process">
                                                                                                            <asp:CheckBox ID="chkSchool1" runat="server" />
                                                                                                            <asp:Label ID="lblReceptionMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblReceptionMax1"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="processdark">
                                                                                                            <asp:CheckBox ID="chkUshape1" runat="server" />
                                                                                                            <asp:Label ID="lblUShapeMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblUShapeMax1"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="process">
                                                                                                            <asp:CheckBox ID="chkBoardroom1" runat="server" />
                                                                                                            <asp:Label ID="lblBanquetMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblBanquetMax1"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                        <li class="processdark">
                                                                                                            <asp:CheckBox ID="chkCocktail1" runat="server" />
                                                                                                            <asp:Label ID="lblCocktailMin1" runat="server" Text=""></asp:Label><asp:Label ID="lblCocktailMax1"
                                                                                                                runat="server" Text=""></asp:Label>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>
                                                                                        </AlternatingItemTemplate>
                                                                                    </asp:Repeater>
                                                                                </div>
                                                                            </li>
                                                                            <!--meeting room END HERE-->
                                                                        </ul>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                                <!--End Bind Secondary Request grid for request tab-->
                                                            </div>
                                                            <!--booking-request-body END HERE-->
                                                            <!--booking-request-btn START HERE-->
                                                            <div class="booking-request-btn">
                                                                <span style="text-align: left; float: left; width: 650px; padding-left: 10px;">
                                                                    <%= GetKeyResult("XXYYNUMBER")%></span>
                                                                <div class="booking-request-btn-basket">
                                                                    <asp:LinkButton ID="lnkButtonAddToBasket" runat="server" class="add-basket-btn" OnClick="lnkButtonAddToBasket_Click"
                                                                        OnClientClick="return AddToBasket(this);_gaq.push(['_trackPageview', 'Step2-SendRequest.aspx']);"><%= GetKeyResult("ADDTOBASKET")%></asp:LinkButton>
                                                                    <%= GetKeyResult("OR")%>
                                                                    <asp:LinkButton ID="lnkButtonCancel" runat="server" CommandName="CancelPopupRequest"
                                                                        OnClientClick="CloseDiv(); return true;"><%= GetKeyResult("CANCEL")%></asp:LinkButton>
                                                                </div>
                                                                <div class="booking-request-btn-close">
                                                                    <asp:LinkButton ID="lnbClose" runat="server" CommandName="CancelPopupRequest" OnClientClick="CloseDiv(); return true;"><img src="<%= SiteRootPath %>images/close-btn.png"/></asp:LinkButton>
                                                                </div>
                                                            </div>                                                            
                                                        </div>                                                        
                                                    </div>                                                    
                                                </li>
                                            </ul>
                                        </asp:Panel>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle HorizontalAlign="Center"></EmptyDataRowStyle>
                            <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                            <PagerStyle HorizontalAlign="Right" BackColor="White" />
                            <PagerTemplate>
                                <tr>
                                    <td colspan="11" bgcolor="#ffffff">
                                        <div style="float: right;">
                                            <asp:PlaceHolder ID="phRequest" runat="server"></asp:PlaceHolder>
                                            <asp:PlaceHolder ID="phBottom" runat="server"></asp:PlaceHolder>
                                        </div>
                                    </td>
                                </tr>
                            </PagerTemplate>
                            <%--<PagerTemplate>
                        <div id="Paging" style="width: 100%; display: none;">
                            <table cellpadding="0" align="right">
                                <tr>
                                    <td style="vertical-align=middle; height: 22px;">
                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </PagerTemplate>--%>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </asp:Panel>
        </div>
    </div>
    <div id="divJoinToday-overlay">
    </div>
    <div id="divJoinToday">
        <div class="popup-top">
        </div>
        <div class="popup-mid">
            <div class="popup-mid-inner">
                <table cellspacing="10" style="margin-left: 20px">
                      <tr>                       
                         <td>
                            <%= GetKeyResult("WOULDYOULIKETOBOOKINGASECONDMEETINGROOM")%>
                        </td>
                      </tr>
                            <tr>                     
                                                                          <td>
                                <asp:RadioButton ID="rbYes" GroupName="checkout" runat="server" Style="float: left;" /><span
                                id="spanDefaultMessage1" runat="server" style="float: left; margin-left: 10px;">
                                <%= GetKeyResult("IWOULDNEEDIT")%>:</span> <span id="spanMessage1" runat="server"
                                    style="display: none; float: left; margin-left: 10px;"><%= GetKeyResult("PLEASESELECTNUMBEROFATTENDEES")%></span>
                            <asp:TextBox ID="txtSecparticipants" runat="server" CssClass="input-white-small"
                                MaxLength="3" Style="float: left; margin-left: 5px;"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtSecparticipants"
                                FilterType="Numbers">
                            </asp:FilteredTextBoxExtender>
                            &nbsp;&nbsp;<span id="spanDefaultMessage2" runat="server" style="float: left; margin-left: 5px;"><%= GetKeyResult("ATTENDEES")%></span>
                            <span id="spanMessage2" runat="server" style="display: none; float: left; margin-left: 24px;">
                                <%= GetKeyResult("FORSECONDMEETINGROOM")%> <%= GetKeyResult("PLEASECHOOSEBETWEEN")%> "<%= GetKeyResult("MIN")%> :
                                <asp:Label ID="lblMin" runat="server" Text="" Visible="false"></asp:Label>" <%= GetKeyResult("AND")%>
                                "<%= GetKeyResult("MAX")%> :
                                <asp:Label ID="lblMax" runat="server" Text="" Visible="false"></asp:Label>"</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="trRequest" runat="server" style="display: none;">
                                <asp:RadioButton ID="rbForRequest" GroupName="checkout" runat="server" Checked="false" />
                                <span style="margin-left: 10px;"><%= GetKeyResult("PLEASEPROCEEDANDTRANSFORMMYBOOKINGASREQUEST")%></span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButton ID="rbNo" GroupName="checkout" runat="server" Checked="false" />
                            &nbsp;&nbsp; <%=GetKeyResult("NOTHANKS")%>
                        </td>
                    </tr>
                </table>
                <div class="subscribe-btn1">
                    <div class="save-cancel-btn1 button_section">
                        <asp:LinkButton ID="btnContinue" runat="server" CssClass="send-request-btn" OnClick="btnContinue_Click"
                            OnClientClick="javascript: _gaq.push(['_trackPageview', 'Step3-BookOnline-Continue.aspx']);"><%=GetKeyResult("CONTINUE")%></asp:LinkButton>&nbsp;<%= GetKeyResult("OR")%>&nbsp;
                        <asp:LinkButton ID="lnkCnacel" runat="server" CssClass="cancelpop" Enabled="true"
                            OnClientClick="javascript:return ClosePopUp();_gaq.push(['_trackPageview', 'Step3-BookOnline-Cancel.aspx']);"><%=GetKeyResult("CANCEL") %></asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <div class="popup-bottom">
        </div>
    </div>
    <div id="divRequestPopUp-overlay">
    </div>
    <div id="divRequestPopUp">
        <div class="popup-top">
        </div>
        <div class="popup-mid">
            <div class="popup-mid-inner">
                <table cellspacing="10" style="margin-left: 20px">
                    <tr>
                        <td>
                            <%= GetKeyResult("WOULDYOULIKETOREQUESTASECONDMEETINGROOM")%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButton ID="rbRequestYes" GroupName="checkout" runat="server" Style="float: left;"
                                Checked="false" /><span id="span1" runat="server" style="float: left; margin-left: 10px;">
                                    <%=GetKeyResult("IWOULDNEEDIT")%> :</span>
                            <asp:TextBox ID="txtRequestedSecparticipants" runat="server" CssClass="input-white-small"
                                MaxLength="3" Style="float: left; margin-left: 5px;"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtRequestedSecparticipants"
                                FilterType="Numbers">
                            </asp:FilteredTextBoxExtender>
                            &nbsp;&nbsp;<span id="span3" runat="server" style="float: left; margin-left: 5px;"><%= GetKeyResult("ATTENDEES")%></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButton ID="rbRequestNo" GroupName="checkout" runat="server" Checked="true" />
                            &nbsp;&nbsp; <%=GetKeyResult("NOTHANKS")%>
                        </td>
                    </tr>
                </table>
                <div class="subscribe-btn1">
                    <div class="save-cancel-btn1 button_section">
                        <asp:LinkButton ID="btnRequestContinue" runat="server" CssClass="send-request-btn"
                            OnClick="btnRequestContinue_Click" OnClientClick="javascript: _gaq.push(['_trackPageview', 'Step3-SendRequest-Continue.aspx']);"><%=GetKeyResult("CONTINUE")%></asp:LinkButton>&nbsp;<%= GetKeyResult("OR")%>&nbsp;
                        <asp:LinkButton ID="btnRequestCancel" runat="server" CssClass="cancelpop" OnClientClick="javascript:return ClosePopUpRequestpopUp();_gaq.push(['_trackPageview', 'Step3-SendRequest-Cancel.aspx']);"><%=GetKeyResult("CANCEL") %></asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <div class="popup-bottom">
        </div>
    </div>
    <!--found-results-mainbody ENDS HERE-->
    <!--pageing START HERE-->
    <%--   <div class="pageing">
            <a href="#" class="arrow-right select"></a>
            <div class="pageing-mid">
                <a href="#">Prevous</a> <a href="#" class="no">1</a><a href="#" class="no select">2</a><a
                    href="#" class="no">3</a> ... <a href="#" class="no">8</a> <a href="#">next</a></div>
            <a href="#" class="arrow-left"></a>
        </div>--%>
    <!--pageing ENDS HERE-->
</div>

<script type="text/javascript" language="javascript">

    function Navigate(Id) {

        var url = '<%= SiteRootPath %>Video.aspx?HotelId=' + Id;
        var winName = '';
        var w = '580';
        var h = '335';
        var scroll = 'no';
        var popupWindow = null;
        LeftPosition = (screen.width) ? (screen.width - w) / 2 : 0;
        TopPosition = (screen.height) ? (screen.height - h) / 2 : 0;
        settings = 'scrollbars=yes,directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no, height=' + h + ',width=' + w + ',top=' + TopPosition + ',left=' + LeftPosition + ''
        popupWindow = window.open(url, winName, settings)
        return false;
    }

    function NavigateFacility(Id) {
        var url = '<%= SiteRootPath %>FacilitySearch.aspx?HotelId=' + Id;
        var winName = '';
        var w = '700';
        var h = '500';
        var scroll = 'no';
        var popupWindow = null;
        LeftPosition = (screen.width) ? (screen.width - w) / 2 : 0;
        TopPosition = (screen.height) ? (screen.height - h) / 2 : 0;
        settings = 'scrollbars=yes,directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no, height=' + h + ',width=' + w + ',top=' + TopPosition + ',left=' + LeftPosition + ''
        popupWindow = window.open(url, winName, settings)
        return false;
    }

    function CheckOne(obj, meetingroomID, configuretionID, btnId) {
        jQuery(".divgrvRequestMeeting").find("input:checkbox").prop("checked", false);
        //alert(jQuery("#divgrvRequestMeeting").find("input:checkbox").length);
        jQuery('#<%=hdnRequestedMeetingRoomId.ClientID %>').val("0");
        jQuery('#<%=hdnRequestedConfigurationId.ClientID %>').val("0");
        if (obj.checked == true) {
            jQuery("#" + obj.id).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().find("input:checkbox:checked").attr("checked", false);
            jQuery('#<%=hdnMeetingRoomId.ClientID %>').val(meetingroomID);
            jQuery('#<%=hdnConfigurationId.ClientID %>').val(configuretionID);
            obj.checked = true;
            //Below is for add to shopping cart checkbox visible/enable
            //jQuery("#" + btnId + "").show();
            //jQuery(jQuery("#" + btnId + "").siblings().get(0)).show();

            jQuery(".booking-online-btn").find(".add-shopping-cart-btn").focus();
        }
        else {
            //alert(jQuery("#" + obj.id).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().html());
            jQuery("#" + obj.id).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().find("input:checkbox:checked").attr("checked", false);
            //jQuery("#cntMainBody_SearchPanel1_grvBooking_grvBookMeeting_" + rowIndex).find("input:checkbox").attr("checked", "");
            jQuery('#<%=hdnMeetingRoomId.ClientID %>').val(0);
            jQuery('#<%=hdnConfigurationId.ClientID %>').val(0);
            //Below is for add to shopping cart checkbox visible/enable
            //jQuery("#" + btnId + "").hide();
            //jQuery(jQuery("#" + btnId + "").siblings().get(0)).hide();
        }
    }

    function CheckOneRequest(obj, meetingroomID, configuretionID, btnId) {
        jQuery(".divgrvBookMeeting").find("input:checkbox").prop("checked", false);
        //alert(jQuery("#divgrvBookMeeting").find("input:checkbox").length);
        jQuery('#<%=hdnMeetingRoomId.ClientID %>').val("0");
        jQuery('#<%=hdnConfigurationId.ClientID %>').val("0");
        if (obj.checked == true) {
            jQuery("#" + obj.id).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().find("input:checkbox:checked").attr("checked", false);
            jQuery('#<%=hdnRequestedMeetingRoomId.ClientID %>').val(meetingroomID);
            jQuery('#<%=hdnRequestedConfigurationId.ClientID %>').val(configuretionID);
            obj.checked = true;
            jQuery(".booking-request-btn").find(".booking-request-btn-basket").find(".add-basket-btn").focus();
        }
        else {
            jQuery("#" + obj.id).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().find("input:checkbox:checked").attr("checked", false);
            jQuery('#<%=hdnRequestedMeetingRoomId.ClientID %>').val(0);
            jQuery('#<%=hdnRequestedConfigurationId.ClientID %>').val(0);
        }
    }

    function CheckSecondary(obj, meetingroomID, configuretionID, Days, Days2, RptId, ColId, btnId, rowCountClass) {
        if (obj.checked == true) {
            jQuery('#<%=hdnMeetingRoomId.ClientID %>').val(meetingroomID);
            jQuery('#<%=hdnConfigurationId.ClientID %>').val(configuretionID);
            if (Days2 == 0) {
                jQuery('#<%=hdnType.ClientID %>').val(Days);
            }
            else {
                jQuery('#<%=hdnType2.ClientID %>').val(Days);
            }
            jQuery('#<%=hdnColId.ClientID %>').val(ColId);
            var baap = jQuery(obj).closest("ul").parent().parent();
            baap.find("input:checkbox").attr("checked", false);
            baap.closest("ul").siblings().find("input:checkbox").attr("checked", false);
            //jQuery("input:checkbox", ".repDivItem").attr("checked", false);
            //alert(jQuery("input:checkbox", ".repDivItem").length);
            //jQuery("#cntMainBody_SearchPanel1_grvBooking_rptSecondaryMeeting_0 input:checkbox:checked").attr("checked", "");            
            obj.checked = true;
            if (jQuery("." + rowCountClass) != undefined) {
                var p = jQuery("." + rowCountClass).find("li input:checkbox");
                //alert(p.length);
                jQuery("." + rowCountClass).find("li input:checkbox").each(function () { jQuery(this).attr("disabled", true); jQuery(this).attr("checked", false); });
                //jQuery("#repDiv").find("li input:checkbox").each(function () { jQuery(this).attr("checked", false); });
                jQuery("." + rowCountClass).find("li:nth-child(" + ColId + ") input:checkbox").attr("disabled", false);
            }
            jQuery("#" + obj.id).attr("checked", true);
            //Below is for add to shopping cart checkbox visible/enable
            //jQuery("#" + btnId + "").show();
            //jQuery(jQuery("#" + btnId + "").siblings().get(0)).show();
            jQuery(".booking-online-btn").find(".add-shopping-cart-btn").focus();
        }
        else {
            jQuery("#cntMainBody_SearchPanel1_grvBooking_rptSecondaryMeeting_0 input:checkbox:checked").attr("checked", "");
            jQuery("#" + RptId + " input:checkbox").attr("checked", false);
            jQuery('#<%=hdnMeetingRoomId.ClientID %>').val(0);
            jQuery('#<%=hdnConfigurationId.ClientID %>').val(0);
            jQuery('#<%=hdnType.ClientID %>').val();
            jQuery('#<%=hdnColId.ClientID %>').val();

            //Below is for add to shopping cart checkbox visible/enable
            //jQuery("#" + btnId + "").hide();
            //jQuery(jQuery("#" + btnId + "").siblings().get(0)).hide();
        }

    }
    ////    function CheckSecondary(obj, meetingroomID, configuretionID, Days, Days2, RptId, ColId, btnId, rowCountClass) {
    ////        if (obj.checked == true) {            
    ////            jQuery('#<%=hdnMeetingRoomId.ClientID %>').val(meetingroomID);
    ////            jQuery('#<%=hdnConfigurationId.ClientID %>').val(configuretionID);
    ////            if (Days2 == 0) {
    ////                jQuery('#<%=hdnType.ClientID %>').val(Days);
    ////            }
    ////            else {
    ////                jQuery('#<%=hdnType2.ClientID %>').val(Days);
    ////            }
    ////            jQuery('#<%=hdnColId.ClientID %>').val(ColId);
    ////            jQuery("input:checkbox", ".repDivItem").attr("checked", false);
    ////            //alert(jQuery("input:checkbox", ".repDivItem").length);
    ////            //jQuery("#cntMainBody_SearchPanel1_grvBooking_rptSecondaryMeeting_0 input:checkbox:checked").attr("checked", "");            
    ////            obj.checked = true;
    ////            if (jQuery("." + rowCountClass) != undefined) {
    ////                var p = jQuery("." + rowCountClass).find("li input:checkbox");
    ////                //alert(p.length);
    ////                jQuery("." + rowCountClass).find("li input:checkbox").each(function () { jQuery(this).attr("disabled", true); jQuery(this).attr("checked", false); });
    ////                //jQuery("#repDiv").find("li input:checkbox").each(function () { jQuery(this).attr("checked", false); });
    ////                jQuery("." + rowCountClass).find("li:nth-child(" + ColId + ") input:checkbox").attr("disabled", false);
    ////            }
    ////            jQuery("#" + obj.id).attr("checked", true);            
    ////            //Below is for add to shopping cart checkbox visible/enable
    ////            //jQuery("#" + btnId + "").show();
    ////            //jQuery(jQuery("#" + btnId + "").siblings().get(0)).show();
    ////        }
    ////        else {
    ////            jQuery("#cntMainBody_SearchPanel1_grvBooking_rptSecondaryMeeting_0 input:checkbox:checked").attr("checked", "");
    ////            jQuery("#" + RptId + " input:checkbox").attr("checked", false);
    ////            jQuery('#<%=hdnMeetingRoomId.ClientID %>').val(0);
    ////            jQuery('#<%=hdnConfigurationId.ClientID %>').val(0);
    ////            jQuery('#<%=hdnType.ClientID %>').val();
    ////            jQuery('#<%=hdnColId.ClientID %>').val();

    ////            //Below is for add to shopping cart checkbox visible/enable
    ////            //jQuery("#" + btnId + "").hide();
    ////            //jQuery(jQuery("#" + btnId + "").siblings().get(0)).hide();
    ////        }

    ////    }

    function CheckSecondaryRequest(obj, meetingroomID, configuretionID, Days, Days2, RptId, ColId, btnId, rowCountClass) {
        if (obj.checked == true) {
            jQuery('#<%=hdnRequestedMeetingRoomId.ClientID %>').val(meetingroomID);
            jQuery('#<%=hdnRequestedConfigurationId.ClientID %>').val(configuretionID);
            if (Days2 == 0) {
                jQuery('#<%=hdnType.ClientID %>').val(Days);
            }
            else {
                jQuery('#<%=hdnType2.ClientID %>').val(Days);
            }
            jQuery('#<%=hdnColId.ClientID %>').val(ColId);
            var baap = jQuery(obj).closest("ul").parent().parent();
            baap.find("input:checkbox").attr("checked", false);
            baap.closest("ul").siblings().find("input:checkbox").attr("checked", false);
            //jQuery("input:checkbox", ".repDivItem").attr("checked", false);
            //alert(jQuery("input:checkbox", ".repDivItem").length);
            //jQuery("#cntMainBody_SearchPanel1_grvBooking_rptSecondaryMeeting_0 input:checkbox:checked").attr("checked", "");            
            obj.checked = true;
            if (jQuery("." + rowCountClass) != undefined) {
                var p = jQuery("." + rowCountClass).find("li input:checkbox");
                //alert(p.length);
                jQuery("." + rowCountClass).find("li input:checkbox").each(function () { jQuery(this).attr("disabled", true); jQuery(this).attr("checked", false); });
                //jQuery("#repDiv").find("li input:checkbox").each(function () { jQuery(this).attr("checked", false); });
                jQuery("." + rowCountClass).find("li:nth-child(" + ColId + ") input:checkbox").attr("disabled", false);
            }
            jQuery("#" + obj.id).attr("checked", true);
            //Below is for add to shopping cart checkbox visible/enable
            //jQuery("#" + btnId + "").show();
            //jQuery(jQuery("#" + btnId + "").siblings().get(0)).show();
            jQuery(".booking-request-btn").find(".booking-request-btn-basket").find(".add-basket-btn").focus();
        }
        else {
            jQuery("#cntMainBody_SearchPanel1_grvBooking_rptSecondaryMeeting_0 input:checkbox:checked").attr("checked", "");
            jQuery("#" + RptId + " input:checkbox").attr("checked", false);
            jQuery('#<%=hdnRequestedMeetingRoomId.ClientID %>').val(0);
            jQuery('#<%=hdnRequestedConfigurationId.ClientID %>').val(0);
            jQuery('#<%=hdnType.ClientID %>').val();
            jQuery('#<%=hdnColId.ClientID %>').val();

            //Below is for add to shopping cart checkbox visible/enable
            //jQuery("#" + btnId + "").hide();
            //jQuery(jQuery("#" + btnId + "").siblings().get(0)).hide();
        }
    }

    function ClosePopUp() {
        document.getElementsByTagName('html')[0].style.overflow = 'auto';
        document.getElementById('divJoinToday').style.display = 'none';
        document.getElementById('divJoinToday-overlay').style.display = 'none';
        jQuery("#divJoinToday").hide();
        jQuery("#divJoinToday-overlay").hide();
        jQuery('#<%=trRequest.ClientID %>').hide();
        jQuery('#<%=spanMessage1.ClientID %>').hide();
        jQuery('#<%=spanMessage2.ClientID %>').hide();
        jQuery('#<%=spanDefaultMessage1.ClientID %>').show();
        jQuery('#<%=spanDefaultMessage2.ClientID %>').show();
        jQuery('#<%=rbNo.ClientID %>').attr("checked", true);
        return false;
    }

    function ClosePopUpRequestpopUp() {
        document.getElementsByTagName('html')[0].style.overflow = 'auto';
        document.getElementById('divRequestPopUp').style.display = 'none';
        document.getElementById('divRequestPopUp-overlay').style.display = 'none';
        jQuery('#<%=rbRequestNo.ClientID %>').attr("checked", true);
        return false;
    }

    function CloseDiv() {
        //jQuery('.booking').hide();
        //jQuery('<%=Session["IsSecondary"] %>').val="";
        //jQuery(".overlay2").removeClass("overlay2").removeClass("aspNetDisabled");
        jQuery('#<%=hdnMeetingRoomId.ClientID %>').val("0");
        jQuery('#<%=hdnConfigurationId.ClientID %>').val("0");
        jQuery('#<%=hdnRequestedMeetingRoomId.ClientID %>').val("0");
        jQuery('#<%=hdnRequestedConfigurationId.ClientID %>').val("0");
        //return false;
    }
    function CloseBookingDiv() {
        jQuery('.booking-online').hide();
        return false;
    }
    function CloseRequestDiv() {
        jQuery('.booking-request').hide();
        return false;
    }

    function AddToShopping(btnElement) {
        if (jQuery('#<%=hdnMeetingRoomId.ClientID %>').val() != 0 && jQuery('#<%=hdnConfigurationId.ClientID %>').val() != 0) {
            if (typeof (btnElement) == "undefined") {
            } else {
                var oneChecked = false;
                jQuery(btnElement).parent().siblings().find('div').find("input[type=checkbox]").each(function (index, item) {
                    if (jQuery(item).is(":checked")) {
                        oneChecked = true;
                        return false;
                    }
                });
                if (oneChecked == false) {
                    alert('<%=GetKeyResultForJavaScript("PLEASESELECTYOURSECONDRYMEETINGROOM")%>');
                    return false;
                }
            }
            if ('<%=Session["IsSecondary"] %>' == "NO" || '<%=Session["IsSecondary"] %>' == "") {

                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                jQuery("#divJoinToday").show();
                jQuery("#divJoinToday-overlay").show();
                document.getElementById('<%=txtSecparticipants.ClientID %>').value = "";
                jQuery("#cntMainBody_SearchPanel1_rbNo").attr("checked", true);
                return false;
            }
            else {
                if (confirm('<%=GetKeyResultForJavaScript("SECONDRYMEETINGROOMHASBEENADDEDTOYOURSHOPINGCART")%>')) {
                    jQuery('#<%=rbYes.ClientID %>').attr("checked", true);
                    return true;
                }
                else {
                    return false;
                }
            }
        }
        else {
            alert('<%=GetKeyResultForJavaScript("PLEASESELECTYOURMEETINGROOM")%>');
            return false;
        }
    }

    function AddToBasket(btnElement) {
        if (jQuery('#<%=hdnRequestedMeetingRoomId.ClientID %>').val() != 0 && jQuery('#<%=hdnRequestedConfigurationId.ClientID %>').val() != 0) {
            if (typeof (btnElement) == "undefined") {
            } else {
                var oneChecked = false;
                jQuery(btnElement).parent().parent().siblings().find('div').find("input[type=checkbox]").each(function (index, item) {
                    if (jQuery(item).is(":checked")) {
                        oneChecked = true;
                        return false;
                    }
                });
                if (oneChecked == false) {
                    alert('<%=GetKeyResultForJavaScript("PLEASESELECTYOURSECONDRYMEETINGROOM")%>');
                    return false;
                }
            }
            if ('<%=Session["IsRequestedSecondary"] %>' == "NO" || '<%=Session["IsRequestedSecondary"] %>' == "") {
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                jQuery("#divRequestPopUp").show();
                jQuery("#divRequestPopUp-overlay").show();
                document.getElementById('<%=txtRequestedSecparticipants.ClientID %>').value = "";
                jQuery('#<%=rbRequestNo.ClientID %>').attr("checked", true);
                return false;
            }
            else {
                if (confirm('<%=GetKeyResultForJavaScript("SECONDRYMEETINGROOMHASBEENADDEDTOYOURREQUESTBASKET")%> [<%=Session["RequestCount"] %> max] <%=GetKeyResultForJavaScript("SECONDRYMEETINGROOMHASBEENADDEDTOYOURREQUESTBASKET1")%>')) {
                    jQuery('#<%=rbRequestYes.ClientID %>').attr("checked", true);
                    return true;
                }
                else {
                    return false;
                }
            }
        }
        else {
            alert('<%=GetKeyResultForJavaScript("PLEASESELECTYOURMEETINGROOM")%>');
            return false;
        }
    }
</script>
<script type="text/javascript" language="javascript">
    jQuery(document).ready(function () {

        //Below is for add to shopping cart checkbox visible/enable
        //jQuery(jQuery('.booking-online-btn').find("a:even")).hide();
        //jQuery(jQuery('.booking-online-btn').find("span")).hide();


        jQuery("#cntMainBody_SearchPanel1_txtSecparticipants").bind("focus", function () {
            jQuery("#cntMainBody_SearchPanel1_rbYes").attr("checked", true);
            jQuery("#cntMainBody_SearchPanel1_rbNo").attr("checked", false);
            jQuery("#cntMainBody_SearchPanel1_rbYes").get(0).checked = true;
            jQuery("#cntMainBody_SearchPanel1_rbNo").get(0).checked = false;
        });
        //        jQuery("#cntMainBody_SearchPanel1_rbNo").bind("click", function () {
        //            if (jQuery(this).is(":checked")) {
        //                jQuery("#cntMainBody_SearchPanel1_txtSecparticipants").val('');
        //            }
        //        });

        jQuery("#cntMainBody_SearchPanel1_rbNo").click(function () {
            if (jQuery(this).is(":checked")) {
                jQuery("#cntMainBody_SearchPanel1_txtSecparticipants").val('');
                jQuery("#cntMainBody_SearchPanel1_rbYes").attr("checked", false);
                jQuery("#cntMainBody_SearchPanel1_rbYes").get(0).checked = false;
            }
            jQuery("#cntMainBody_SearchPanel1_rbNo").get(0).checked = true;
        });

        jQuery("#cntMainBody_SearchPanel1_txtRequestedSecparticipants").bind("focus", function () {
            jQuery("#cntMainBody_SearchPanel1_rbRequestYes").attr("checked", true);
            jQuery("#cntMainBody_SearchPanel1_rbRequestNo").attr("checked", false);
            jQuery("#cntMainBody_SearchPanel1_rbRequestYes").get(0).checked = true;
            jQuery("#cntMainBody_SearchPanel1_rbRequestNo").get(0).checked = false;
        });

        jQuery("#cntMainBody_SearchPanel1_rbRequestNo").click(function () {
            if (jQuery(this).is(":checked")) {
                jQuery("#cntMainBody_SearchPanel1_txtRequestedSecparticipants").val('');
                jQuery("#cntMainBody_SearchPanel1_rbRequestYes").attr("checked", false);
                jQuery("#cntMainBody_SearchPanel1_rbRequestYes").get(0).checked = false;
            }
            jQuery("#cntMainBody_SearchPanel1_rbRequestNo").get(0).checked = true;
        });

        jQuery("#<%= btnContinue.ClientID %>").bind("click", function () {
            if (document.getElementById('<%=rbYes.ClientID %>').checked == true) {
                if (document.getElementById('<%=txtSecparticipants.ClientID %>').value.length == "" || document.getElementById('<%=txtSecparticipants.ClientID %>').value == "") {
                    alert("You must enter the number of participants.");
                    return false;
                }
                else if (document.getElementById('<%=txtSecparticipants.ClientID %>').value > 500) {
                    alert('<%=GetKeyResultForJavaScript("PARTICIPENTSSHOULDNOTBEMORETHAN500")%>');
                    return false;
                }
                else {
                    jQuery("#divJoinToday").hide();
                    return true;
                }
            }
            else if (document.getElementById('<%=rbForRequest.ClientID %>').checked == true) {
                if (confirm('<%=GetKeyResultForJavaScript("YOUHAVECHOSEN3VANUESMAXPERREQUEST")%> <%=Session["RequestCount"] %> <%=GetKeyResultForJavaScript("YOUHAVECHOSEN3VANUESMAXPERREQUEST1")%>')) {
                    jQuery("#divJoinToday").hide();
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                if (confirm('<%=GetKeyResultForJavaScript("MEETINGROOMHASBEENADDEDTOYOURSHOOPINGCART")%>')) {
                    jQuery("#divJoinToday").hide();
                    return true;
                }
                else {
                    return false;
                }
            }
        });

        jQuery("#<%= btnRequestContinue.ClientID %>").bind("click", function () {
            if (document.getElementById('<%=rbRequestYes.ClientID %>').checked == true) {
                if (document.getElementById('<%=txtRequestedSecparticipants.ClientID %>').value.length == "" || document.getElementById('<%=txtRequestedSecparticipants.ClientID %>').value == "") {
                    alert('<%=GetKeyResultForJavaScript("YOUMUSTENTERTHENUMBEROFPARTICIPANTS")%>');
                    return false;
                }
                else if (document.getElementById('<%=txtRequestedSecparticipants.ClientID %>').value > 500) {
                    alert('<%=GetKeyResultForJavaScript("PARTICIPENTSSHOULDNOTBEMORETHAN500")%>');
                    return false;
                }
                else {
                    jQuery("#divRequestPopUp").hide();
                    return true;
                }
            }
            else {
                if (confirm('<%=GetKeyResultForJavaScript("PLEASESELECTANOTHEVENUE")%> [<%=Session["RequestCount"] %> max] <%=GetKeyResultForJavaScript("PLEASESELECTANOTHEVENUE1")%>')) {
                    jQuery("#divRequestPopUp").hide();
                    return true;
                }
                else {
                    return false;
                }
            }
        });

    });
</script>
<div id="basketSection" style="display: none">
    <div id="divshoppingcart" runat="server" style="display: none">
        <div class="shopping-cart-body">
            <div class="shopping-cart-heading">
                <%= GetKeyResult("SHOPPING")%><br />
                <%= GetKeyResult("CART")%>
            </div>
            <div class="shopping-cart-inner">
                <div class="shopping-cart-inner-left">
                    <h2>
                        <%= GetKeyResult("HOTELFACILITIESNAMES")%></h2>
                </div>
                <div class="shopping-cart-inner-right">
                    <%= GetKeyResult("ALL")%>
                    <asp:ImageButton ID="imgDel" ImageUrl="~/images/delete-ol-btn.png" OnClientClick="return confirm('Are you sure you want to delete it?')"
                        CommandArgument='<%#Eval("Id") %>' runat="server" OnClick="imgDel_Click" />
                </div>
            </div>
            <div class="shopping-cart-inner-ol">
                <ol>
                    <asp:ListView ID="lstViewAddToShopping" runat="server" ItemPlaceholderID="itemHolder"
                        OnItemDataBound="lstViewAddToShopping_ItemDataBound" DataKeyNames="Id" OnItemCommand="lstViewAddToShopping_ItemCommand">
                        <LayoutTemplate>
                            <asp:PlaceHolder ID="itemHolder" runat="server"></asp:PlaceHolder>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <li class="last">
                                <div class="shopping-cart-inner-ol-left">
                                    <%# Eval("Name") %></div>
                                <%--<div class="shopping-cart-inner-ol-right">
                                    <asp:ImageButton ID="imgDelete" ImageUrl="../../images/delete-ol-btn.png" CommandName="deletehotel"
                                        CommandArgument='<%#Eval("Id") %>' runat="server" />
                                </div>--%>
                                <ol>
                                    <asp:ListView ID="lstViewMeetingRoomAddToCart" runat="server" ItemPlaceholderID="MeetingroomHolder"
                                        DataKeyNames="Id" OnItemCommand="lstViewMeetingRoomAddToCart_ItemCommand" OnItemDataBound="lstViewMeetingRoomAddToCart_ItemDataBound">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder ID="MeetingroomHolder" runat="server"></asp:PlaceHolder>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <li>
                                                <div class="shopping-cart-inner-ol-left1">
                                                    <%# Eval("Name") %></div>
                                                <div class="shopping-cart-inner-ol-right1">
                                                    <asp:ImageButton ID="imgDelete" ImageUrl="../../images/delete-ol-btn.png" CommandName="deletemeetingroom"
                                                        OnClientClick="return confirm('Are you sure you want to delete it?')" CommandArgument='<%#Eval("Id") %>'
                                                        runat="server" Visible="true" />
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </ol>
                            </li>
                        </ItemTemplate>
                    </asp:ListView>
                </ol>
            </div>
        </div>
        <div class="con-with-check-btn">
            <asp:LinkButton ID="lbnContinue" runat="server" OnClick="lbnContinue_Click"> <%= GetKeyResult("CONTINUEYOURBOOKING")%></asp:LinkButton>
        </div>
    </div>
    <!--basket-request ENDS HERE-->
    <div class="basket-request" id="divRequestBasket" runat="server" style="display: none">
        <div class="basket-request-body">
            <div class="basket-request-heading">
                 <%= GetKeyResult("BASKET")%><br />
                <%= GetKeyResult("REQUEST")%>
            </div>
            <div class="basket-request-inner">
                <h2>
                    <%= GetKeyResult("VENUES")%></h2>
            </div>
            <div class="basket-request-inner-ol">
                <ol>
                    <asp:ListView ID="lstViewAddToBasket" runat="server" ItemPlaceholderID="itemHolderRequest"
                        OnItemDataBound="lstViewAddToBasket_ItemDataBound" DataKeyNames="Id" OnItemCommand="lstViewAddToBasket_ItemCommand">
                        <LayoutTemplate>
                            <asp:PlaceHolder ID="itemHolderRequest" runat="server"></asp:PlaceHolder>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <li class="last">
                                <div class="basket-request-inner-ol-left">
                                    <%# Eval("Name") %></div>
                                <div class="shopping-cart-inner-ol-right1">
                                    <asp:ImageButton ID="imgDel" CommandName="deletehotelroom" ImageUrl="../../images/delete-ol-btn.png"
                                        OnClientClick="return confirm('Are you sure you want to delete it?')" CommandArgument='<%#Eval("Id") %>'
                                        runat="server" />
                                </div>
                                <ol>
                                    <asp:ListView ID="lstViewMeetingRoomAddToBasket" runat="server" ItemPlaceholderID="RequestedMeetingroomHolder"
                                        OnItemCommand="lstViewMeetingRoomAddToBasket_ItemCommand">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder ID="RequestedMeetingroomHolder" runat="server"></asp:PlaceHolder>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <li>
                                                <div class="basket-request-inner-ol-left1">
                                                    <%# Eval("Name") %></div>
                                                <div class="basket-request-inner-ol-right1">
                                                    <asp:ImageButton ID="imgDelete" ImageUrl="../../images/delete-ol-btn.png" CommandName="deletemeetingroom"
                                                        OnClientClick="return confirm('Are you sure you want to delete it?')" CommandArgument='<%#Eval("Id") %>'
                                                        runat="server" />
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </ol>
                            </li>
                        </ItemTemplate>
                    </asp:ListView>
                </ol>
            </div>
        </div>
        <div class="con-with-req-btn">
            <a href="#"></a>
            <asp:LinkButton ID="lnbContinueRequest" runat="server" OnClick="lnbContinueRequest_Click"><%= GetKeyResult("CONTINUEYOURREQUEST")%></asp:LinkButton>
        </div>
    </div>
    <!--filter-form START HERE-->
    <div class="filter-form" id="filterBox" runat="server" style="display: block">
        <div class="filter-form-filter-results">
            <%= GetKeyResult("FILTER")%>
            <br />
            <%= GetKeyResult("RESULTS")%>
        </div>
        <div class="filter-form-inner">
            <div id="Area">
                <div id="divBzone">
                    <asp:DropDownList ID="zonesDDL" runat="server" CssClass="bigselect">
                    </asp:DropDownList>
                </div>
                <div id="divRzone">
                    <asp:DropDownList ID="zoneDDLrequest" runat="server" CssClass="bigselect">
                    </asp:DropDownList>
                </div>
            </div>
            <div id="Stars-Modern">
                <div id="Stars">
                    <label>
                        <%= GetKeyResult("STARS")%>
                        &nbsp;</label>
                    <asp:TextBox ID="txtStars" runat="server" CssClass="inputbox"></asp:TextBox>
                    <%--<input type="text" name="stars[]" value="N/A" id="txtStars" class="inputbox"/>--%>
                    <input type="button" onclick="var qty_el = document.getElementById('cntMainBody_SearchPanel1_txtStars'); var qty = qty_el.value; if(qty=='All') {qty=0;qty_el.value=0;} if( !isNaN( qty ) && qty<7) qty_el.value++;return false;"
                        class="button_up" />
                    <input type="button" onclick="var qty_el = document.getElementById('cntMainBody_SearchPanel1_txtStars'); var qty = qty_el.value; if(qty==0) {qty=0; qty_el.value='All';} if( !isNaN( qty ) && qty > 0 ){ qty_el.value--;if(qty_el.value==0) qty_el.value='All';};if(qty==0) qty_el.value='All'; return false;"
                        class="button_down" />
                </div>
                <div id="theme">
                    <%= GetKeyResult("THEME")%></div>
                <div id="Modern">
                    <asp:DropDownList ID="themeDDL" runat="server" CssClass="midselect1">
                        <asp:ListItem Value="0" Selected="True">--Select--</asp:ListItem>
                        <asp:ListItem Text="Luxury" Value="Luxury"></asp:ListItem>
                        <asp:ListItem Text="Chique" Value="Chique"></asp:ListItem>
                        <asp:ListItem Text="Trendy" Value="Trendy"></asp:ListItem>
                        <asp:ListItem Text="Modern" Value="Modern"></asp:ListItem>
                        <asp:ListItem Text="Fashion" Value="Fashion"></asp:ListItem>
                        <asp:ListItem Text="Budget" Value="Budget"></asp:ListItem>
                        <asp:ListItem Text="Rural" Value="Rural"></asp:ListItem>
                        <asp:ListItem Text="Classic" Value="Classic"></asp:ListItem>
                        <asp:ListItem Text="Golf Club" Value="Golf Club"></asp:ListItem>
                        <asp:ListItem Text="Castle" Value="Castle"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="daily-dudget">
                <div class="daily-dudgettext">
                    <%= GetKeyResult("DAILYBUDGET")%>
                    <br />
                    <span>(<%= GetKeyResult("PERMEETINGDELIGATE")%>)</span>
                </div>
                <div class="daily-dudgetimg">
                    <table style="width: 100%">
                        <tr>
                           <td align="left" style="font-size:10px;width:30px"><%= GetKeyResult("MIN")%>:20</td>
                            <td align="left" style="text-align: left;width:35px">
                                <asp:SliderExtender ID="SliderExtender1" runat="server" TargetControlID="txtDailyBudget"
                                    BoundControlID="sliderValue" Minimum="20" Maximum="200">
                                </asp:SliderExtender>
                               <asp:TextBox ID="txtDailyBudget" runat="server" Width="150px" Style="display: none;"> </asp:TextBox>
                            </td>
                            <td align="left" style="text-align: left;font-size:10px;width:40px">
                                <asp:Label ID="sliderValue" runat="server" CssClass="number"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="filter-button-body">
            <asp:LinkButton ID="lbtFilter" runat="server" CssClass="filter-button" OnClick="lbtFilter_Click"><%= GetKeyResult("FILTERYOURRESULTS")%></asp:LinkButton>&nbsp;&nbsp;
            <asp:LinkButton ID="lbtReset" runat="server" CssClass="reset-button-filter" OnClick="lbtReset_Click"><%= GetKeyResult("RESET")%></asp:LinkButton>
            <%--<input type="button" value="Filter your results" />--%>
        </div>
    </div>
</div>
<div id="maploc" style="background-color: #E5E3DF; border: 2px solid green; float: right;
    height: 176px; left: 923px; overflow: hidden; position: absolute; top: 242px;
    width: 249px; z-index: 50; display: none;">
</div>
<input type="image" src="<%= SiteRootPath %>Images/close.png" style="float: right;position: absolute;z-index: 55;display:none;" class="close"/>
<div id="divMeetingroomDesc" style="background-color: #fff; border: 2px solid gray; float: right;
    height: auto; left: 923px; overflow: hidden; position: absolute; top: 242px;
    width: 249px; z-index: 50; display: none;">
</div>
<script language="javascript" type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(".showMap").each(function () {
            jQuery(this).bind("click", function () {
                jQuery("#divMeetingroomDesc").hide();
                var offsetshowmap = jQuery(this).offset();
                var long = jQuery(this).parent().find(".long").find("input:hidden").val();
                var lat = jQuery(this).parent().find(".lat").find("input:hidden").val();
                jQuery("#maploc").css({ "top": (offsetshowmap.top + 20) + "px", "left": (offsetshowmap.left - 10) + "px" });
                jQuery("#maploc").show();
                jQuery(".close").css({ "top": (offsetshowmap.top + 10) + "px", "left": (offsetshowmap.left + 230) + "px" });
                jQuery(".close").show();
                LoadMapLatLong(long, lat);
                return false;
            });
        });
        jQuery(".close").bind("click", function () { jQuery("#maploc").hide(); jQuery("#divMeetingroomDesc").hide(); jQuery(".close").hide(); return false; });
        jQuery(".link").each(function () {
            jQuery(this).bind("click", function () {
                if (jQuery(this).attr('alt') != undefined) {
                    jQuery("#maploc").hide();
                    var offsetshowmap = jQuery(this).offset();
                    jQuery("#divMeetingroomDesc").css({ "top": (offsetshowmap.top + 20) + "px", "left": (offsetshowmap.left - 5) + "px" });
                    jQuery("#divMeetingroomDesc").show();
                    jQuery(".close").css({ "top": (offsetshowmap.top + 10) + "px", "left": (offsetshowmap.left + 230) + "px" });
                    jQuery(".close").show();
                    jQuery('#divMeetingroomDesc').html(jQuery(this).attr('alt'));
                    return false;
                }
            });
        });

    });
    function LoadMapLatLong(long, lat) {

        var map = new google.maps.Map(document.getElementById('maploc'), {
            zoom: 14,
            center: new google.maps.LatLng(lat, long),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, long),
            map: map
        });

    }
</script>
<script language="javascript" type="text/javascript">
    jQuery(document).ready(function () {
        var bookingRequest = jQuery('#<%=hdnManageBookingRequest.ClientID %>').val();
        if (bookingRequest == '0') {

            ShowRequestOnDemand('booking')
        }
        else {
            ShowRequestOnDemand('request')
        }
        //var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels2");
        var currentUrl = document.location.href;
        var pageName = currentUrl.split('/');
        var currentPage = pageName[currentUrl.split('/').length - 1];

        //        if (currentPage == 'FindByMap.aspx') {
        //            var str = jQuery("#basketSection").html();
        //            jQuery("#basketSection").html('');
        //            jQuery("#leftSection").html(str);
        //            jQuery("#filterBox").hide();
        //        }
        //        else {
        var str = jQuery("#basketSection").html();
        jQuery("#basketSection").html('');
        jQuery("#leftSection").html(str);

        //        }
    });
    //ShowRequestOnDemand("booking");


    function ShowRequestOnDemand(val) {
        if (val == "booking") {
            jQuery("#<%=hdnManageBookingRequest.ClientID %>").val('0');
            jQuery("#cntMainBody_SearchPanel1_drpDistanceBooking").show();
            jQuery("#cntMainBody_SearchPanel1_drpDistanceRequest").hide();
            jQuery(".Divcover").each(function () {
                var parentHeight = jQuery(this).parent().height();
                var parentWidth = jQuery(this).parent().width();
                jQuery(this).height(parentHeight);
                jQuery(this).width(parentWidth);
            });
            jQuery('#divBooking').show();
            jQuery('#divRequest').hide();
            jQuery("#TabbedPanels2").find("ul li:eq(0)").addClass("TabbedPanelsTab-green"); //TabbedPanelsTab-green
            jQuery("#TabbedPanels2").find("ul li:eq(1)").removeClass("TabbedPanelsTab2").addClass("TabbedPanelsTab2");
            jQuery("#abooking").addClass("select");
            jQuery("#arequest").removeClass("select");
            jQuery("#<%=hdnManageBookingRequest.ClientID %>").val('0');
            jQuery('#divBzone').show();
            jQuery('#divRzone').hide();
            //            var count = jQuery("#<%=hdnBookingCount.ClientID %>").val();
            //            jQuery("#<%=lblResultCount.ClientID %>").html(count);
            if (jQuery("#<%= divBookRequest.ClientID %>") != undefined) {
                var parentHeight = jQuery("#<%= divBookRequest.ClientID %>").parent().height();
                var parentWidth = jQuery("#<%= divBookRequest.ClientID %>").parent().width();
                jQuery("#<%= divBookRequest.ClientID %>").height(parentHeight);
                jQuery("#<%= divBookRequest.ClientID %>").width(parentWidth);
            }
            jQuery("#divBooking").find(".Divcover").each(function () {
                var parentHeight = jQuery(this).parent().height();
                var parentWidth = jQuery(this).parent().width();
                jQuery(this).height(parentHeight);
                jQuery(this).width(parentWidth);
            });
        }
        else {
            if (jQuery("#<%= lnkRequestCall.ClientID %>").attr("id") == undefined) {
                jQuery("#<%=hdnManageBookingRequest.ClientID %>").val('1');
                jQuery("#cntMainBody_SearchPanel1_drpDistanceBooking").hide();
                jQuery("#cntMainBody_SearchPanel1_drpDistanceRequest").show();
                jQuery(".Divcover").each(function () {
                    var parentHeight = jQuery(this).parent().height();
                    var parentWidth = jQuery(this).parent().width();
                    jQuery(this).height(parentHeight);
                    jQuery(this).width(parentWidth);
                });
                jQuery('#divBooking').hide();
                jQuery('#divRequest').show();
                jQuery("#TabbedPanels2").find("ul li:eq(1)").addClass("TabbedPanelsTab2");
                jQuery("#TabbedPanels2").find("ul li:eq(0)").removeClass("TabbedPanelsTab-green").addClass("TabbedPanelsTab-green");
                jQuery("#arequest").addClass("select");
                jQuery("#abooking").removeClass("select");
                jQuery('#divBzone').hide();
                jQuery('#divRzone').show();
                //            var count = jQuery("#<%=hdnRequestCount.ClientID %>").val();
                //            jQuery("#<%=lblResultCount.ClientID %>").html(count);
                if (jQuery("#<%= divBookRequest.ClientID %>") != undefined) {
                    var parentHeight = jQuery("#<%= divBookRequest.ClientID %>").parent().height();
                    var parentWidth = jQuery("#<%= divBookRequest.ClientID %>").parent().width();
                    jQuery("#<%= divBookRequest.ClientID %>").height(parentHeight);
                    jQuery("#<%= divBookRequest.ClientID %>").width(parentWidth);
                }
                jQuery("#divRequest").find(".Divcover").each(function () {
                    var parentHeight = jQuery(this).parent().height();
                    var parentWidth = jQuery(this).parent().width();
                    jQuery(this).height(parentHeight);
                    jQuery(this).width(parentWidth);
                });
            }
            else {
                document.getElementById("<%= lnkRequestCall.ClientID %>").click();
                return false;
            }
        }
    }
</script>
<script type="text/javascript" language="javascript">
    jQuery(document).ready(function () {


        //        jQuery('.link').bind("click", function () {
        //            jQuery('#meetingroomdesc').html(jQuery(this).attr('alt'));
        //            jQuery("body").scrollTop(0);
        //            jQuery("html").scrollTop(0);
        //            document.getElementsByTagName('html')[0].style.overflow = 'hidden';
        //            jQuery("#divpolicy").show();
        //            jQuery("#divpolicy-overlay").show();
        //            return false;
        //        });
        //        jQuery(".link").bind("click", function () {
        //            jQuery(".error").html("");
        //            jQuery(".error").hide();
        //            //jQuery("#Loding_overlay").show();
        //            jQuery("body").scrollTop(0);
        //            jQuery("html").scrollTop(0);
        //            document.getElementsByTagName('html')[0].style.overflow = 'hidden';
        //            jQuery("#divpolicy").show();
        //            jQuery("#divpolicy-overlay").show();
        //            return false;
        //       });

    });
    function Navigate2(val) {

        var url = '<%= SiteRootPath %>SlideShowPopup.aspx?id=' + val + '';
        var winName = 'myWindow';
        var w = '460';
        var h = '300';
        var scroll = 'no';
        var popupWindow = null;
        LeftPosition = (screen.width) ? (screen.width - w) / 2 : 0;
        TopPosition = (screen.height) ? (screen.height - h) / 2 : 0;
        settings = 'height=' + h + ',width=' + w + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=no,directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no';
        popupWindow = window.open(url, winName, settings)
        return false;
    }
    function NavigateMeetingPics(val) {

        var url = '<%= SiteRootPath %>SlideShowPopup.aspx?Mid=' + val + '';
        var winName = 'myWindow';
        var w = '460';
        var h = '300';
        var scroll = 'no';
        var popupWindow = null;
        LeftPosition = (screen.width) ? (screen.width - w) / 2 : 0;
        TopPosition = (screen.height) ? (screen.height - h) / 2 : 0;
        settings = 'height=' + h + ',width=' + w + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=no,directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no';
        popupWindow = window.open(url, winName, settings)
        return false;
    }

    function focusRequiredElement(elementID) {
        jQuery("html").scrollTop(parseInt(jQuery("#" + elementID).position().top, 10) - 140);
        jQuery("body").scrollTop(parseInt(jQuery("#" + elementID).position().top, 10) - 140);
    };

    function focusRequiredSecElement(elementID) {
        //alert(jQuery("." + elementID).position().top);
        jQuery("html").scrollTop(parseInt(jQuery("." + elementID).position().top, 10) - 180);
        jQuery("body").scrollTop(parseInt(jQuery("." + elementID).position().top, 10) - 180);
    };


</script>
<script language="javascript" type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(".Divcover").each(function () {
            var parentHeight = jQuery(this).parent().height();
            var parentWidth = jQuery(this).parent().width();
            jQuery(this).height(parentHeight);
            jQuery(this).width(parentWidth);
        });
    });
    
</script>