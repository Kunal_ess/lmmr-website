﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LeftSearchPanel.ascx.cs"
    Inherits="UserControl_Agency_LeftSearchPanel" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
    <%@ Register src="~/UserControl/Agency/SearchBookingPage.ascx" tagname="SearchPanel" tagprefix="uc2" %>
<!--left-inner-top START HERE-->
<div class="left-inner-top clearfix">
<asp:HiddenField ID="hdnSearchButtonClick" Value="0" runat="server" />
    <div class="left-inner-top-top">
        <div class="map">
            <asp:ImageButton ID="imgMapSearch" ImageUrl="~/images/map.png" runat="server" OnClick="imgMapSearch_Click"
                    OnClientClick="return IsCountryCitySelected();" />
        </div>
        <div class="logo">
            <a href="#">
                <img src="../images/logo-front.png" /></a>
        </div>
        <!--left-form START HERE-->
    </div>
    <script language="javascript" type="text/javascript">
        function onDropdownCountrySelected() {
            //alert(document.getElementById("<%=drpCountry.ClientID%>").value);
            var me = document.getElementById("<%=drpCountry.ClientID%>").value;
            MiscWebService.set_timeout = 60000;
            jQuery("#<%=drpCity.ClientID%>").find("option").remove()
            jQuery("#<%=drpCity.ClientID%>").append(jQuery("<option></option>").val("0").html('--Loading--'));
            MiscWebService.GetCityByCountryIDForFrontend(me, OnComplete, OnTimeOut, OnError);
        }
        function OnComplete(args) {
            jQuery("#<%=drpCity.ClientID%>").find("option").remove()
            jQuery("#<%=drpCity.ClientID%>").append(jQuery("<option></option>").val("0").html('--Select City--'));
            for (i = 0; i < args.length; i++) {
                //alert(args[i].id);
                jQuery("#<%=drpCity.ClientID%>").append(jQuery("<option></option>").val(args[i].id).html(args[i].Name));
            }
        }
        function OnTimeOut(args) {
            jQuery("#<%=drpCity.ClientID%>").find("option").remove()
            jQuery("#<%=drpCity.ClientID%>").append(jQuery("<option></option>").val("0").html('--Select City--'));
            alert("Service call timed out." + jQuery(args).text());
        }

        function OnError(args) {
            jQuery("#<%=drpCity.ClientID%>").find("option").remove()
            jQuery("#<%=drpCity.ClientID%>").append(jQuery("<option></option>").val("0").html('--Select City--'));
            alert("Error calling service method." + jQuery(args).text());
        }
        function onDropdownCitySelected() {
            var ids = jQuery("#<%= drpCity.ClientID %>").find("option:selected").val();
            jQuery("#<%= hdnCityDetail.ClientID %>").val(ids);
            var ids = jQuery("#<%= drpCountry.ClientID %>").find("option:selected").val();
            jQuery("#<%= hdnCountry.ClientID %>").val(ids);
            return false;
        }
        </script>
    <div class="left-inner-top-mid clearfix">
        <div class="left-form">
            <div id="Country"><asp:HiddenField ID="hdnCountry" runat="server" />
                <asp:DropDownList ID="drpCountry" runat="server" CssClass="bigselect" onchange="return onDropdownCountrySelected();" 
                        AutoPostBack="false">
                    </asp:DropDownList>
            </div>
            <div id="City"><asp:HiddenField ID="hdnCityDetail" runat="server" />
                <asp:DropDownList ID="drpCity" CssClass="bigselect" runat="server" onchange="return onDropdownCitySelected();"
                        AutoPostBack="false">
                    </asp:DropDownList>
            </div>
            <div class="location-fined-body" id="showlocationdiv" runat="server" visible="false">
                    <div id="location">
                        <div id="locationtext">
                            <asp:HiddenField ID="hdnMapCenterLatitude" runat="server" Value="0" />
                            <asp:HiddenField ID="hdnMapCenterLogitude" runat="server" Value="0" />
                            <asp:HiddenField ID="hdnMapLatitude" runat="server" Value="0" />
                            <asp:HiddenField ID="hdnMapLongitude" runat="server" Value="0" />
                            <asp:HiddenField ID="hdnMapcurrentLatitude" runat="server" Value="0" />
                            <asp:HiddenField ID="hdnMapcurrentLongitude" runat="server" Value="0" />
                            <asp:HiddenField ID="hdnPlaceName" runat="server" Value="0" />
                            <b><span id="actualLocation">
                                Actual location:</span><span id="yourlocation" style="display: none;">Change</span></b>
                            <br>
                            Latitude = <b><span id="selectLatitude"></span></b>
                            <br>
                            Longitude = <b><span id="selectLongitude"></span></b>
                        </div>
                        <div class="locationsearch">
                            <%--<img src="<%= SiteRootPath %>images/map-search.png" />--%>
                        </div>
                    </div>
                    <div id="radiusbody">
                        <div class="radiustext">
                            Search in radius of:</div>
                        <div id="radius">
                            <asp:DropDownList ID="drpRadius" runat="server" onchange="ToAddRadius();">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            <div id="Date">
                <div style="float:left;width:140px;">
                    <asp:TextBox ID="txtBookingDate" runat="server" value="dd/mm/yy" class="input-white-mid"></asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtBookingDate"
                        PopupButtonID="calFrom" Format="dd/MM/yy">
                    </asp:CalendarExtender>                    
                    <img id="calFrom" src="../Images/date-icon.png" class="datebutton" />
                    </div>
                    <div style="float:left;width:20px;padding-top:5px;">
                    <asp:Image ID="imgIcontext" runat="server" ImageUrl="~/Images/infoicon.png" CssClass="information" title="You can book online for the next 60 days. Requests can be made without any date restriction. You can also send us your request by mail or chat. We take care of your booking and requests. Easy-Fast and Free of Charge." />                  
                    </div>
            </div>
            <div id="Quantity-day">
                <div id="Quantity">
                        <label>
                            Duration &nbsp;</label><asp:DropDownList ID="drpDuration" runat="server" AutoPostBack="false"
                                OnSelectedIndexChanged="drpDuration_SelectedIndexChanged" onchange="ShowHideDay();">
                                <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                    </div>
                    <div id="daysname">
                        Day 1</div>
                    <div id="Day">
                        <asp:DropDownList ID="drpDays" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="dayone" id="divDay2" runat="server" style="display:none;">
                        <div id="daysonename">
                             Day 2</div>
                        <div id="Dayone">
                            <asp:DropDownList ID="drpDay2" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
            </div>
            <div id="Participants">
                <label>
                        Meeting Delegates &nbsp;</label>
                    <asp:TextBox ID="txtParticipant" runat="server" CssClass="input-white-small" MaxLength="3"></asp:TextBox>
                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtParticipant"
                        FilterType="Numbers">
                    </asp:FilteredTextBoxExtender>
            </div>
        </div>
    </div>
    <div class="left-inner-top-bottom clearfix">
        <asp:Button ID="btnSearch" runat="server" class="find-button" OnClientClick="return searchclick();"
                OnClick="btnSearch_Click" Text="Find your meeting room!"/>
    </div>
    <!--left-form ENDS HERE-->
</div>
<!--  left-inner-top ENDS HERE-->
<script language="javascript" type="text/javascript">
    function ShowHideDay() {

        var selectedDay = jQuery('#<%=drpDuration.ClientID %>').val();
        if (selectedDay == '2') {
            jQuery('#<%=drpDay2.ClientID %>').val('0');
            jQuery('#<%=divDay2.ClientID %>').show();

        }
        else {
            jQuery('#<%=divDay2.ClientID %>').hide();
        }

    }
    jQuery(document).ready(function () { jQuery("#<%=txtBookingDate.ClientID%>").attr("disabled", true); });
    function validate() {
        if (document.getElementById("<%=drpCountry.ClientID%>").value == 0) {
            alert('Please select country.');
            document.getElementById("<%=drpCountry.ClientID%>").focus();
            return false;
        }
        if (document.getElementById("<%=drpCity.ClientID%>").value == 0) {
            alert('Please select city.');
            document.getElementById("<%=drpCity.ClientID%>").focus();
            return false;
        }
        if (document.getElementById("<%=txtBookingDate.ClientID%>").value == "dd/mm/yy") {
            alert('Please enter date.');
            document.getElementById("<%=txtBookingDate.ClientID%>").focus();
            return false;
        }
        var no = document.getElementById("<%=txtParticipant.ClientID%>").value;
        if (no == "" || parseInt(no) <= 0) {
            alert('Please enter the number of Meeting delegates.');
            document.getElementById("<%=txtParticipant.ClientID%>").focus();
            return false;
        }
        else if (no > 500) {
            alert('Number of Meeting delegates must be less then or equal to 500.');
            document.getElementById("<%=txtParticipant.ClientID%>").focus();
            return false;

        }
        if(MessageForconfirm()==false)
        {
            return false;
        }
        jQuery("#<%=txtBookingDate.ClientID%>").attr("disabled", false);
    }
    function searchclick() {
            jQuery('#<%=hdnSearchButtonClick.ClientID %>').val('1');
            return validate();
        }
    function IsCountryCitySelected() {
        if (jQuery("#<%=drpCountry.ClientID%>").val() == "0" || jQuery("#<%=drpCity.ClientID%>").val() == "0") {
            alert('Country and city both are required for search.');
            return false;
        }
        jQuery("#<%=txtBookingDate.ClientID%>").attr("disabled", false);
    }    
    var currentPage = "<%=Request.Url.AbsoluteUri.Split('?')[0].Split('/')[Request.Url.AbsoluteUri.Split('?')[0].Split('/').Length -1] %>";
        function MessageForconfirm() {
            <% if(Session["BookingDone"]==null){ %>
            //alert(currentPage);
            if (currentPage.toLowerCase() == 'booking.aspx' || currentPage.toLowerCase() == 'bookingstep1.aspx' || currentPage.toLowerCase() == 'bookingstep2.aspx' || currentPage.toLowerCase() == 'bookingstep3.aspx' || currentPage.toLowerCase() == 'bookingstep4.aspx') {
                return confirm('You have a booking in progress. Would you like to leave and start a new search?');
            }
            if (currentPage.toLowerCase() == 'request.aspx' || currentPage.toLowerCase() == 'requeststep1.aspx' || currentPage.toLowerCase() == 'requeststep2.aspx' || currentPage.toLowerCase() == 'requeststep3.aspx') {
                return confirm('You have a request in progress. Would you like to leave and start a new search?');
            }
            <%} %>
        }
    </script>
    


    <%if (usercontroltype == "Map")
      { %>
    <script type="text/javascript">
        var geocoder;
        var map;
        var markersArray = [];
        var marker;
        var address;
        var addressByUser;
        var image;
        var circleArray;
        var infowindow = new google.maps.InfoWindow();
        var locations = <%= HotelData %>;
        
       function OnDemand(CenterPoint, PlaceName,zoomLevel) {
         jQuery('#map_canvas').show();
         jQuery('#mapMessgage').show();
            var cent = CenterPoint.split(',');
            map = new google.maps.Map(document.getElementById('map_canvas'), {
            zoom: parseFloat(zoomLevel),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scaleControl:true,
            scrollwheel:false
       });
            
            map.setCenter(new google.maps.LatLng(cent[0], cent[1]));
            
            image = new google.maps.MarkerImage('http://maps.google.com/mapfiles/ms/icons/yellow-dot.png');
            marker = new google.maps.Marker({
                map: map,
                title: PlaceName,
                position: new google.maps.LatLng(cent[0], cent[1]),
                icon: image
            });
//            infowindow.setContent(PlaceName);
//            infowindow.open(map, marker);
//            
            var radiusSelected = parseFloat(jQuery('#<%=drpRadius.ClientID %>').val());
            
              if(radiusSelected!=0 && radiusSelected!= null)
               {
                 circleArray = new google.maps.Circle({
                    map: map,
                    radius:parseFloat(radiusSelected),    // 1 miles in metres
                    center:new google.maps.LatLng(cent[0], cent[1]),
                    fillColor: '#AA0000'
                     });

                    map.fitBounds(circleArray.getBounds());
                    circleArray.setRadius(parseFloat(radiusSelected)*1000);
                    circleArray.setCenter(map.getCenter());
                    map.fitBounds(circleArray.getBounds());
                    map.circleRadius=parseFloat(radiusSelected);

               }

            google.maps.event.addListener(marker, 'mouseover', function () {
                infowindow.setContent('<table width="100%"><tr><td><b>Your actual location change :</b> '+ PlaceName + '</td></tr><tr><td><b>Latitude :</b> '+cent[0]+'</td></tr><tr><td><b> Longitude :</b> '+cent[1]+'</td></tr><tr><td></td></tr><tr><td><b>Please pin point on map</b></td></tr><table>');
                infowindow.open(map, this);
            });
            jQuery('#selectLatitude').html(cent[0]);
            jQuery('#selectLongitude').html(cent[1]);
            ShowHotelOfCity();
             
        }


       function ToAddRadius()
       {
     
        var radiusSelected = jQuery('#<%=drpRadius.ClientID %>').val();
        var varLatitude = jQuery('#<%=hdnMapLatitude.ClientID %>').val();
        var varLongitude = jQuery('#<%=hdnMapLongitude.ClientID %>').val();
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(varLatitude,varLongitude)
       });
         clearCircle();
              if(radiusSelected!=0 && radiusSelected!= null)
               {
                     circleArray = new google.maps.Circle({
                     map:map,
                     radius: parseFloat(radiusSelected),
                     center: new google.maps.LatLng(varLatitude,varLongitude),
                     fillColor: '#AA0000'

                     });
                     map.fitBounds(circleArray.getBounds());
                     circleArray.setRadius(parseFloat(radiusSelected)*1000);
                     circleArray.setCenter(map.getCenter());
                     map.fitBounds(circleArray.getBounds());
                     map.circleRadius=parseFloat(radiusSelected);
               }
       }


        //This function used for show all hotel of the city.
        function ShowHotelOfCity() {
            clearOverlays();
            markersArray.push(marker);
            for (i = 0; i < locations.length; i++) {
            
                if (locations[i][4] == 'YES') {
                    image = new google.maps.MarkerImage('http://maps.google.com/mapfiles/ms/icons/red-dot.png');
                }
                else if(locations[i][4] == 'RE'){
                    image = new google.maps.MarkerImage('http://maps.google.com/mapfiles/ms/icons/blue-dot.png');
                }
                else if(locations[i][4] == 'NO'){
                    image = new google.maps.MarkerImage('http://maps.google.com/mapfiles/ms/icons/green-dot.png');
                }
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: image
                });
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {
                       
                      var stars = null;
                      
                      if(locations[i][6]!=null)
                      {

                      if(locations[i][6]=='0')
                      {
                      stars ='  N/A';
                      }
                     else if(locations[i][6]=='1')
                      {
                      stars ='<img src="../Images/1.png" />';
                      }
                     else if(locations[i][6]=='2')
                      {
                      stars ='<img src="../Images/2.png" />';
                      //stars ="Images/2.png";
                      }
                      else if(locations[i][6]=='3')
                      {
                      stars ='<img src="../Images/3.png" />';
                      //stars ="Images/3.png";
                      }
                      else if(locations[i][6]=='4')
                      {
                      stars ='<img src="../Images/4.png" />';
                      //stars ="Images/4.png";
                      }
                      else if(locations[i][6]=='5')
                      {
                      stars ='<img src="../Images/5.png" />';
                      //stars ="Images/5.png";
                      }
                      else if(locations[i][6]=='6')
                      {
                      stars ='<img src="../Images/6.png" />';
                      //stars ="Images/6.png";
                      }
                      else if(locations[i][6]=='7'){
                      stars ='<img src="../Images/7.png" />';
                      //stars ="Images/7.png";
                      }
                      }
                      var htmlContent
                      if(locations[i][4] == 'YES')
                      {

                       htmlContent ='<table width="100%"><tr><td><b>Venue :</b> '+locations[i][0]+ '</td></tr><tr><td><b>Stars :</b>'+stars+'</td></tr><tr><td><b>Address :</b> '+locations[i][5]+'</td></tr><tr font><td><b>Special deals :</b> <span style="color:Red">€'+locations[i][8]+' pp/day</span> From €'+ locations[i][7]+'</td></tr></table>';

                      }
                      else
                      {
                      
                      htmlContent ='<table width="100%"><tr><td><b>Venue :</b> '+locations[i][0]+ '</td></tr><tr><td><b>Stars :</b>'+stars+'</td></tr><tr><td><b>Address :</b> '+locations[i][5]+'</td></tr></table>';
                     
                      }
                       infowindow.setContent(htmlContent);
                       infowindow.open(map, marker);

         }
                })(marker, i));
            }
            google.maps.event.addListener(map, 'click', function (event) {
                AddPoint(event.latLng);
            });
        }

        //Add referace point on click on map.
        function AddPoint(location) {
            geocoder = new google.maps.Geocoder();
            image = new google.maps.MarkerImage('http://maps.google.com/mapfiles/ms/icons/yellow-dot.png');
            geocoder.geocode({ 'latLng': location }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        
                     var latlog = location;
                     
                     if(latlog!=null)
                     {
                     var splitLogLat = latlog.toString().split(',');
                     jQuery('#<%=hdnMapcurrentLatitude.ClientID %>').val(splitLogLat[0].replace("(",""));
                     jQuery('#<%=hdnMapcurrentLongitude.ClientID %>').val(splitLogLat[1].replace(")",""));
                     }

                    if(Distance()<=20)
                    {
                    
                     var splitLogLat = latlog.toString().split(',');
                     jQuery('#selectLatitude').html(splitLogLat[0].replace("(",""));
                     jQuery('#selectLongitude').html(splitLogLat[1].replace(")",""));
                     jQuery('#<%=hdnMapLatitude.ClientID %>').val(splitLogLat[0].replace("(",""));
                     jQuery('#<%=hdnMapLongitude.ClientID %>').val(splitLogLat[1].replace(")",""));
                     jQuery('#<%=hdnPlaceName.ClientID %>').val(results[1].formatted_address);
                     jQuery('#yourlocation').show();
                     jQuery('#actualLocation').hide();
                     jQuery('#mapMessgage').hide()
                     
                        clearOverlays();
                        clearCircle();
                        marker = new google.maps.Marker({
                            position: location,
                            title: results[1].formatted_address,
                            map: map,
                            icon: image
                        });

                        infowindow.setContent('<table width="100%"><tr><td><b>Your location change :</b> '+results[1].formatted_address+ '</td></tr><tr><td><b>Latitude :</b> '+jQuery('#<%=hdnMapLatitude.ClientID %>').val()+'</td></tr><tr><td><b>Longitude :</b> '+jQuery('#<%=hdnMapLongitude.ClientID %>').val()+'</td></tr><tr><td></td></tr><tr><td><b>Please pin point on the map</b></td></tr></table>');
                        infowindow.open(map, marker);
                        markersArray.push(marker);
                        map.setCenter(location);
                        //click
                       google.maps.event.addListener(marker, 'mouseover', function () {
                       infowindow.setContent('<table width="100%"><tr><td><b>Your location change :</b> '+results[1].formatted_address+ '</td></tr><tr><td><b>Latitude :</b> '+jQuery('#<%=hdnMapLatitude.ClientID %>').val()+'</td></tr><tr><td><b>Longitude :</b> '+jQuery('#<%=hdnMapLongitude.ClientID %>').val()+'</td></tr><tr><td></td></tr><tr><td><b>Please pin point on the map</b></td></tr></table>');
                            infowindow.open(map, this);
                        });

                   var radiusSelected= jQuery('#<%=drpRadius.ClientID %>').val();
                      if(radiusSelected!=0 && radiusSelected!= null)
                       {
                         circleArray = new google.maps.Circle({
                            map: map,
                            radius:parseFloat(radiusSelected),    // 1 miles in metres
                            center:location,
                            fillColor: '#AA0000'
                        });
                        
                          map.fitBounds(circleArray.getBounds());
                          circleArray.setRadius(parseFloat(radiusSelected)*1000);
                          circleArray.setCenter(map.getCenter());
                          map.fitBounds(circleArray.getBounds());
                          map.circleRadius=parseFloat(radiusSelected);
                    }
                    }
                    else
                    {
                    alert('You are out of range.');
                    
                    }
                    }
                } else {
                    alert('GEO code fail'+":" + status);
                }
            });


        }


    function Distance() {
      var prevLat = jQuery("#<%=hdnMapCenterLatitude.ClientID %>").val();
      var prevLong = jQuery("#<%=hdnMapCenterLogitude.ClientID %>").val();
      var currLat = jQuery("#<%=hdnMapcurrentLatitude.ClientID %>").val();
      var currLong = jQuery("#<%=hdnMapcurrentLongitude.ClientID %>").val();
      // KNOWN CONSTANTS
      var degreesToRadians = Math.PI / 180;
      var earthRadius = 6371; // approximation in kilometers assuming earth to be spherical
      // CONVERT LATITUDE AND LONGITUDE VALUES TO RADIANS
      var previousRadianLat = prevLat * degreesToRadians;
      var previousRadianLong = prevLong * degreesToRadians;
      var currentRadianLat = currLat * degreesToRadians;
      var currentRadianLong = currLong * degreesToRadians;
     // CALCULATE RADIAN DELTA BETWEEN THE TWO POSITIONS
     var latitudeRadianDelta = currentRadianLat - previousRadianLat;
     var longitudeRadianDelta = currentRadianLong - previousRadianLong;
     var expr1 = (Math.sin(latitudeRadianDelta / 2) * Math.sin(latitudeRadianDelta / 2)) +
                (Math.cos(previousRadianLat) * Math.cos(currentRadianLat) * Math.sin(longitudeRadianDelta / 2) * Math.sin(longitudeRadianDelta / 2));
     var expr2 = 2 * Math.atan2(Math.sqrt(expr1), Math.sqrt(1 - expr1));
     var distanceValue = earthRadius * expr2;
     return distanceValue;
    }

        function clearOverlays() {
            if (markersArray) {
                for (i = 0; i < markersArray.length; i++) {
                    markersArray[i].setMap(null);
                }
                markersArray.length = 0;
            }

        }
        function clearCircle() {
            if (circleArray) {
                for (i in circleArray) {
                    circleArray.setMap(null);
                }
            }
        }
        function toggleBounce() {

            if (marker.getAnimation() != null) {
                marker.setAnimation(null);
            } else {
                marker.setAnimation(google.maps.Animation.BOUNCE);
            }
        }

       function ManageLoad(IsMessage)
       {
       
       if(IsMessage !='0')
       {
       
       jQuery('#mapMessgage').hide();
       jQuery('#yourlocation').show();
       jQuery('#actualLocation').hide();
       }
       else
       {
       jQuery('#actualLocation').hide();
       jQuery('#mapMessgage').show();
       jQuery('#actualLocation').show();
       }
       }

       function CityChanged()
       {
        jQuery('#<%=hdnPlaceName.ClientID %>').val('0'); 
       }
       
    </script>
    <%}
      else if(Session["IsSearchStart"]!=null)
      { %>
    <script type="text/javascript">
        var geocoder;
        var map;
        var markersArray = [];
        var marker;
        var address;
        var addressByUser;
        var image;
        var circleArray;
        var infowindow = new google.maps.InfoWindow();
        var locations = <%= HotelData %>;
        
        function OnDemand(CenterPoint, PlaceName,zoomLevel) {        
         jQuery('#map_canvas').show();
         jQuery('#mapMessgage').hide();
            var cent = CenterPoint.split(',');
            map = new google.maps.Map(document.getElementById('map_canvas'), {
            zoom: parseFloat(zoomLevel),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scaleControl:true,
            scrollwheel:false
        });
            
            map.setCenter(new google.maps.LatLng(cent[0], cent[1]));
            
             image = new google.maps.MarkerImage('http://maps.google.com/mapfiles/ms/icons/yellow-dot.png');
//            marker = new google.maps.Marker({
//                map: map,
//                title: PlaceName,
//                position: new google.maps.LatLng(cent[0], cent[1]),
//                icon: image
//            });

            jQuery('#selectLatitude').html(cent[0]);
            jQuery('#selectLongitude').html(cent[1]);
            ShowHotelOfCity();
             
        }

        //This function used for show all hotel of the city.
        function ShowHotelOfCity() {        
            clearOverlays();
            
            markersArray.push(marker);
            for (i = 0; i < locations.length; i++) {
                
            //alert(locations);
                if (locations[i][4] == 'YES') {
                    image = new google.maps.MarkerImage('http://maps.google.com/mapfiles/ms/icons/red-dot.png');
                }
                else if(locations[i][4] == 'RE'){
                    image = new google.maps.MarkerImage('http://maps.google.com/mapfiles/ms/icons/blue-dot.png');
                }
                else if(locations[i][4] == 'NO'){
                    image = new google.maps.MarkerImage('http://maps.google.com/mapfiles/ms/icons/green-dot.png');
                }
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: image
                });
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {
                       
                      var stars = null;
                      if(locations[i][6]!=null)
                      {

                      if(locations[i][6]=='0')
                      {
                      stars ='  N/A';
                      }
                     else if(locations[i][6]=='1')
                      {
                      stars ='<img src="../Images/1.png" />';
                      }
                     else if(locations[i][6]=='2')
                      {
                      stars ='<img src="../Images/2.png" />';
                      //stars ="Images/2.png";
                      }
                      else if(locations[i][6]=='3')
                      {
                      stars ='<img src="../Images/3.png" />';
                      //stars ="Images/3.png";
                      }
                      else if(locations[i][6]=='4')
                      {
                      stars ='<img src="../Images/4.png" />';
                      //stars ="Images/4.png";
                      }
                      else if(locations[i][6]=='5')
                      {
                      stars ='<img src="../Images/5.png" />';
                      //stars ="Images/5.png";
                      }
                      else if(locations[i][6]=='6')
                      {
                      stars ='<img src="../Images/6.png" />';
                      //stars ="Images/6.png";
                      }
                      else if(locations[i][6]=='7'){
                      stars ='<img src="../Images/7.png" />';
                      //stars ="Images/7.png";
                      }
                      }
                     var htmlContent
                      if(locations[i][4] == 'YES')
                      {

                      var hotelName = locations[i][0];
                      hotelName = hotelName.replace(/~/g,"'");
                      var hotelAddress = locations[i][5];
                      hotelAddress = hotelAddress.replace(/~/g,"'");
                       htmlContent ='<table width="100%"><tr><td><b>Venue :</b> '+ hotelName + '</td></tr><tr><td><b>Stars :</b>'+stars+'</td></tr><tr><td><b>Address :</b> '+hotelAddress+'</td></tr><tr font><td><b>Special deals :</b> <span style="color:Red">€'+locations[i][8]+' pp/day</span> From €'+ locations[i][7]+'</td></tr></table>';                       

                      }
                      else
                      {
                      var hotelName = locations[i][0];
                      hotelName = hotelName.replace(/~/g,"'");
                      var hotelAddress = locations[i][5];
                      hotelAddress = hotelAddress.replace(/~/g,"'");
                      htmlContent ='<table width="100%"><tr><td><b>Venue :</b> '+ hotelName + '</td></tr><tr><td><b>Stars :</b>'+stars+'</td></tr><tr><td><b>Address :</b> '+ hotelAddress + '</td></tr></table>';
                     
                      }
                       infowindow.setContent(htmlContent);
                       infowindow.open(map, marker);

         }
                })(marker, i));
                
            }
            google.maps.event.addListener(map, 'click', function (event) {
                AddPoint(event.latLng);
            });
        }

        function clearOverlays() {
            if (markersArray) {
                for (i = 0; i < markersArray.length; i++) {
                    markersArray[i].setMap(null);
                }
                markersArray.length = 0;
            }

        }
       
    </script>
    <%} %>