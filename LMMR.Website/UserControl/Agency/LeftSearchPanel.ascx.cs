﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using System.Text;
using AjaxControlToolkit;


    public partial class UserControl_Agency_LeftSearchPanel : System.Web.UI.UserControl
    {

        #region Variable and Properties
        public string propCountryID
        {
            get
            {
                return hdnCountry.Value;
            }
        }
        public string propCity
        {
            get
            {
                return hdnCityDetail.Value;
            }
        }
        public string propDate
        {
            get
            {
                return (txtBookingDate.Text == "dd/mm/yy" ? DateTime.Now.ToString("dd/MM/yy") : txtBookingDate.Text);
            }
        }
        public string propDuration
        {
            get
            {
                return drpDuration.SelectedValue;
            }
        }
        public string propDay1
        {
            get
            {
                return drpDays.SelectedValue;
            }
        }
        public string propDay2
        {
            get
            {
                return drpDay2.SelectedValue;
            }
        }
        public string propParticipant
        {
            get
            {
                return txtParticipant.Text;
            }
        }
        public string propRadious
        {
            get
            {
                return drpRadius.SelectedValue;
            }
        }
        public string propMapCenterLatitude
        {
            set
            {
                hdnMapCenterLatitude.Value = value;
            }
        }
        public string propMapCenterLogitude
        {
            set
            {
                hdnMapCenterLogitude.Value = value;
            }
        }
        public string propMapLatitude
        {
            set
            {
                hdnMapLatitude.Value = value;
            }
        }
        public string propMapLongitude
        {
            set
            {
                hdnMapLongitude.Value = value;
            }
        }
        public string propMapcurrentLatitude
        {
            set
            {
                hdnMapcurrentLatitude.Value = value;
            }
        }
        public string propMapcurrentLongitude
        {
            set
            {
                hdnMapcurrentLongitude.Value = value;
            }
        }
        public string propPlaceName
        {
            set
            {
                hdnPlaceName.Value = value;
            }
        }

        public VList<BookingRequestViewList> BookingDataSource
        {
            get;
            set;
        }
        public VList<ViewForRequestSearch> RequestDataSource
        {
            get;
            set;
        }
        public VList<ViewForRequestSearch> RequestDataSourceAll
        {
            get;
            set;
        }
        #endregion


        ManageMapSearch objManageSearch = new ManageMapSearch();
        PackagePricingManager objPackagePricingManager = new PackagePricingManager();
        BookingRequest objBookingRequest = null;
        public string HotelData
        {
            get;
            set;
        }
        ManageOthers objOthers = new ManageOthers();
        StringBuilder objSB = new StringBuilder();
        VList<BookingRequestViewList> Tempvlist;
        VList<BookingRequestViewList> vlist2day;
        public string usercontroltype
        {
            get
            {
                if (Session["SearchType"] == null)
                {
                    Session["SearchType"] = "Basic";
                }
                return Convert.ToString(Session["SearchType"]);
            }
            set { Session["SearchType"] = value; }
        }
        public int intTypeCMSid;

        public event EventHandler SearchButton;
        public event EventHandler MapSearch;

        public void CheckNexCondition()
        {
            HotelData = "[[]]";
            if (Convert.ToString(Session["masterInput"]) != "")
            {
                drpCountry.SelectedValue = ((Session["masterInput"] as BookingRequest)).propCountry;
                hdnCountry.Value = ((Session["masterInput"] as BookingRequest)).propCountry;
                BindCityDropdownByCountryID(Convert.ToInt32(drpCountry.SelectedValue));
                drpCity.SelectedValue = ((Session["masterInput"] as BookingRequest)).propCity;
                hdnCityDetail.Value = ((Session["masterInput"] as BookingRequest)).propCity;
                txtBookingDate.Text = ((Session["masterInput"] as BookingRequest)).propDate;
                drpDays.SelectedValue = ((Session["masterInput"] as BookingRequest)).propDays;
                drpDay2.SelectedValue = ((Session["masterInput"] as BookingRequest)).propDay2;
                drpDuration.SelectedValue = ((Session["masterInput"] as BookingRequest)).propDuration;
                if (drpDuration.SelectedValue == "2")
                {
                    divDay2.Style.Add("display", "block");
                }
                else
                {
                    divDay2.Style.Add("display", "none");
                }
                if (((Session["masterInput"] as BookingRequest)).propParticipants != "0")
                {
                    txtParticipant.Text = ((Session["masterInput"] as BookingRequest)).propParticipants;
                }
                else
                {
                    txtParticipant.Text = "";
                }
            }
            if (usercontroltype == "Map")
            {
                showlocationdiv.Visible = true;
                imgMapSearch.ImageUrl = "~/images/map-basic.png";
            }
            else
            {
                showlocationdiv.Visible = false;
                imgMapSearch.ImageUrl = "~/images/map.png";
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CalendarExtender1.StartDate = DateTime.Now;
                txtBookingDate.Text = "dd/mm/yy";
                BindCountryDropdown();
                BindCityDropdownByCountryID(Convert.ToInt32(string.IsNullOrEmpty(hdnCountry.Value) ? "0" : hdnCountry.Value));
                fillStaticData();
                CheckNexCondition();
                if (Session["ComeFromBookingAndRequestPage"] != null)
                {
                    GetCityHotel();
                    GetCenterPoint(Convert.ToInt32(string.IsNullOrEmpty(hdnCityDetail.Value) ? "0" : hdnCityDetail.Value));
                }
                hdnSearchButtonClick.Value = "0";
            }
            else
            {
                if ((txtBookingDate.Text == "" || txtBookingDate.Text == "dd/mm/yy") && drpCity.SelectedIndex != 0 && Session["IsSearchStart"]!=null)
                {
                    CheckNexCondition();
                    
                }
                //if (((Session["masterInput"] as BookingRequest)).propCity == hdnCityDetail.Value)
                //{
                GetCenterPoint(Convert.ToInt32(string.IsNullOrEmpty(hdnCityDetail.Value) ? "0" : hdnCityDetail.Value));
                    GetCityHotel();
                //    Session["CityHotel"] = "Yes";
                //}
                hdnSearchButtonClick.Value = "0";
            }
        }
        public void GetPageStartValues()
        {
            BindCountryDropdown();
            BindCityDropdownByCountryID(Convert.ToInt32(drpCountry.SelectedValue));
            fillStaticData();

            CheckNexCondition();
        }
        public void fillStaticData()
        {
            //imgIcontext.ToolTip = GetKeyResult("YOUCANONLYBOOKONLINEMESSAGE");
            drpRadius.Items.Add(new ListItem("--Select--", "0"));
            drpRadius.Items.Add(new ListItem("0.5 km", "0.5"));
            drpRadius.Items.Add(new ListItem("1 km", "1"));
            drpRadius.Items.Add(new ListItem("2 km", "2"));
            drpRadius.Items.Add(new ListItem("5 km", "5"));
            drpRadius.Items.Add(new ListItem("10 km", "10"));

            drpDays.Items.Add(new ListItem("Full day", "0"));
            drpDays.Items.Add(new ListItem("Morning", "1"));
            drpDays.Items.Add(new ListItem("Afternoon", "2"));

            drpDay2.Items.Add(new ListItem("Full day", "0"));
            drpDay2.Items.Add(new ListItem("Morning", "1"));
            drpDay2.Items.Add(new ListItem("Afternoon", "2"));

        }
        /// <summary>
        /// used this function to get center point of the city.
        /// </summary>
        /// <param name="cityID"></param>
        public void GetCenterPoint(int cityID)
        {
            if (cityID != 0)
            {
                //Function to get main center point of the city.
                string strLatitudeOfCenter = "";
                string strLogitudeOfCenter = "";
                string strCenterPlace = "";
                string strCenterPoint = "";
                int intZoomLevel = 10;
                TList<MainPoint> objCenterPoint = objManageSearch.GetCenterPoint(cityID);
                if (objCenterPoint.Count > 0)
                {
                    MainPoint objPoint = objCenterPoint.Find(a => a.IsCenter == true);
                    strLatitudeOfCenter = objPoint.Latitude.ToString();
                    strLogitudeOfCenter = objPoint.Longitude.ToString();
                    hdnMapLatitude.Value = objPoint.Latitude.ToString();
                    hdnMapLongitude.Value = objPoint.Longitude.ToString();
                    hdnMapCenterLatitude.Value = objPoint.Latitude.ToString();
                    hdnMapCenterLogitude.Value = objPoint.Longitude.ToString();
                    strCenterPlace = objPoint.MainPointName;
                    intZoomLevel = 12;
                }
                strCenterPoint = strLatitudeOfCenter + "," + strLogitudeOfCenter;
                Page.RegisterStartupScript("LoadMap3", "<script language='javascript' >jQuery(document).ready(function(){OnDemand('" + strCenterPoint + "','" + strCenterPlace + "','" + intZoomLevel + "');});</script>");
            }
        }

        /// <summary>
        /// Get All Hotel of the city.
        /// </summary>
        public void GetCityHotel()
        {
            string WhereClause = Convert.ToString(Session["Where"]);
            string orderby = string.Empty;
            if (Session["masterInput"] != null)
            {
                string whereclausRequest = string.Empty;
                int countparticipants = 0;
                DateTime BokingDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));
                if (Session["WLHotel"] != null)
                {
                    whereclausRequest = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + " and HotelId IN (" + Convert.ToString(Session["WLHotel"]) + ")";
                    countparticipants = Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants);
                }
                else
                {
                    whereclausRequest = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "'";// and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity;
                    countparticipants = Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants);
                }
                objBookingRequest = new BookingRequest();
                VList<BookingRequestViewList> objHotel = new VList<BookingRequestViewList>();

                //Get Minimum consider special as per country id
                TList<Others> objOther = objOthers.GetInfoByCountryID(Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry));
                if (objOther.Count > 0 && objOther[0].CountryId != null)
                {
                    ViewState["MinConsider"] = objOther[0].MinConsiderSpl;
                }
                else
                {
                    ViewState["MinConsider"] = null;
                }

                VList<ViewForRequestSearch> objRequested = new VList<ViewForRequestSearch>();
                //if (Session["2"] != null)
                //{
                //For day2 logic
                if (Convert.ToString(((Session["masterInput"] as BookingRequest)).propDuration) == "2")
                {
                    string whereclaus2 = Convert.ToString(Session["Where2"]);
                    vlist2day = objBookingRequest.GetBookingReqDetails(whereclaus2, orderby).FindAllDistinct(BookingRequestViewListColumn.HotelId);
                    Tempvlist = objBookingRequest.GetBookingReqDetails(WhereClause, orderby).FindAllDistinct(BookingRequestViewListColumn.HotelId);

                    for (int i = 0; i < Tempvlist.Count; i++)
                    {
                        BookingRequestViewList b = vlist2day.Where(a => a.HotelId == Tempvlist[i].HotelId && a.MeetingRoomId == Tempvlist[i].MeetingRoomId).FirstOrDefault();
                        if (b != null)
                        {
                            objHotel.Add(b);
                        }
                    }
                }
                else
                {
                    objHotel = objBookingRequest.GetBookingReqDetails(WhereClause, orderby).FindAllDistinct(BookingRequestViewListColumn.HotelId);
                    //VList<BookingRequestViewList> lstnew = new VList<BookingRequestViewList>();
                    //for (int i = 0; i < objHotel.Count; i++)
                    //{
                    //    if(lstnew.Where(u=>u.HotelId==objHotel[i].HotelId).Count() <= 0 )
                    //    {
                    //        lstnew.Add(objHotel[i]);
                    //    }
                    //}
                    //objHotel = lstnew;
                }

                VList<ViewForRequestSearch> lstRequestall = new VList<ViewForRequestSearch>();//
                objRequested = objBookingRequest.GetReqDetails(whereclausRequest, orderby);
                VList<ViewForRequestSearch> objAllRequest = objRequested.Copy();
                foreach (var p in objRequested)
                {
                    if (p.MinCapacity <= countparticipants && p.MaxCapicity >= countparticipants)
                    {
                        //if (lstRequestall.Where(u => u.HotelId == p.HotelId).Count() <= 0)
                        //{
                        lstRequestall.Add(p);
                        //}
                    }
                }
                //objRequested = lstRequestall;//.FindAllDistinct(ViewForRequestSearchColumn.HotelId);



                //objRequested = objAllRequest.FindAllDistinct(ViewForRequestSearchColumn.HotelId);
                //Session["2"] = null;

                //}
                //else
                //{
                //    Session["2"] = "2";
                //}
                BookingDataSource = objHotel;
                RequestDataSource = lstRequestall.FindAllDistinct(ViewForRequestSearchColumn.HotelId);
                RequestDataSourceAll = objRequested;
                HotelData = "[";
                if (objHotel.Count > 0 || objRequested.Count > 0)
                {
                    foreach (BookingRequestViewList h in objHotel)
                    {
                        string IsPromoted = "";
                        string strSpecialDeal = "";
                        string strActualprice = "";
                        //if (h.DdrPercent < 0 || h.DdrPercent > 0)
                        //{

                        strActualprice = string.Empty;
                        strSpecialDeal = string.Empty;
                        decimal ActualPrice = Convert.ToDecimal(h.ActualPkgPrice);
                        decimal ActualHalfPrice = Convert.ToDecimal(h.ActualPkgPriceHalfDay);
                        DateTime fromDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));
                        TList<SpecialPriceAndPromo> splDiscount = objBookingRequest.GetSpecialPriceandPromoByDate(Convert.ToInt32(h.HotelId), fromDate);
                        decimal DiscountPrice = Convert.ToDecimal(splDiscount[0].DdrPercent);
                        TList<PackageMaster> pkgmaster = objPackagePricingManager.GetStandardName(Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry));
                        if (pkgmaster.Count > 0)
                        {

                            #region Get Price from actualpackageprice by hotelid with discount for 1 Day
                            if (DiscountPrice < 0 && ((Session["masterInput"] as BookingRequest)).propDuration == "1")
                            {
                                IsPromoted = "YES";
                                if (((Session["masterInput"] as BookingRequest)).propDays == "0")
                                {
                                    strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs(ActualPrice)));
                                    TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(h.HotelId));
                                    decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.FulldayPrice).Sum());
                                    decimal Fulldaypkgprice = (Math.Abs((ActualPrice - getTotleFullDayPrice)));
                                    decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * DiscountPrice / 100)));
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (ActualPrice - PkgDiscountPrice));
                                }
                                else
                                {
                                    strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs(ActualHalfPrice)));
                                    TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(h.HotelId));
                                    decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.HalfdayPrice).Sum());
                                    decimal Fulldaypkgprice = (Math.Abs((ActualHalfPrice - getTotleHalfDayPrice)));
                                    decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * DiscountPrice / 100)));
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (ActualHalfPrice - PkgDiscountPrice));
                                }

                                //added this formula for checking minimum special discount
                                if (ViewState["MinConsider"] != null && Convert.ToInt32(ViewState["MinConsider"]) <= DiscountPrice)
                                {
                                    strActualprice = strSpecialDeal;
                                    IsPromoted = "NO";
                                }
                                else
                                {

                                }
                            }
                            //End Get Price from actualpackageprice by hotelid without discount
                            else if (DiscountPrice == 0 && ((Session["masterInput"] as BookingRequest)).propDuration == "1")
                            {
                                IsPromoted = "NO";
                                if (((Session["masterInput"] as BookingRequest)).propDays == "0")
                                {
                                    strActualprice = String.Format("{0:#,##,##0.0}", Convert.ToDecimal(h.ActualPkgPrice));
                                }
                                else
                                {
                                    strActualprice = String.Format("{0:#,##,##0.0}", Convert.ToDecimal(h.ActualPkgPriceHalfDay));
                                }
                            }
                            else if (DiscountPrice >= 0 && ((Session["masterInput"] as BookingRequest)).propDuration == "1")
                            {
                                IsPromoted = "NO";
                                if (((Session["masterInput"] as BookingRequest)).propDays == "0")
                                {
                                    TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(h.HotelId));
                                    decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.FulldayPrice).Sum());
                                    decimal Fulldaypkgprice = (Math.Abs((ActualPrice - getTotleFullDayPrice)));
                                    decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * DiscountPrice / 100)));
                                    strActualprice = String.Format("{0:#,##,##0.0}", (ActualPrice + PkgDiscountPrice));
                                }
                                else
                                {
                                    TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(h.HotelId));
                                    decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.HalfdayPrice).Sum());
                                    decimal Fulldaypkgprice = (Math.Abs((ActualHalfPrice - getTotleHalfDayPrice)));
                                    decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * DiscountPrice / 100)));
                                    strActualprice = String.Format("{0:#,##,##0.0}", (ActualHalfPrice + PkgDiscountPrice));
                                }
                            }
                            #endregion

                            #region Get Price from actualpackageprice by hotelid with discount for 2 Day
                            TList<SpecialPriceAndPromo> Spl = objBookingRequest.GetSpecialPriceandPromoByDate(Convert.ToInt32(h.HotelId), fromDate.AddDays(1));
                            decimal SecdayDiscountPercent = Convert.ToDecimal(Spl[0].DdrPercent);
                            if (((DiscountPrice < 0 && SecdayDiscountPercent < 0) && ((Session["masterInput"] as BookingRequest)).propDuration == "2") || ((DiscountPrice < 0 && SecdayDiscountPercent == 0) && ((Session["masterInput"] as BookingRequest)).propDuration == "2") || ((DiscountPrice == 0 && SecdayDiscountPercent < 0) && ((Session["masterInput"] as BookingRequest)).propDuration == "2"))
                            {
                                IsPromoted = "YES";
                                if (((Session["masterInput"] as BookingRequest)).propDays == "0" && ((Session["masterInput"] as BookingRequest)).propDay2 == "0")
                                {
                                    strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs(ActualPrice + ActualPrice)));
                                    TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(h.HotelId));
                                    decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.FulldayPrice).Sum());
                                    decimal Fulldaypkgprice = (Math.Abs((ActualPrice - getTotleFullDayPrice)));
                                    decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * DiscountPrice / 100)));
                                    decimal PkgDiscountPrice2Day = (Math.Abs((Fulldaypkgprice * SecdayDiscountPercent / 100)));
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (ActualPrice - PkgDiscountPrice) + (ActualPrice - PkgDiscountPrice2Day));
                                }
                                else if (((Session["masterInput"] as BookingRequest)).propDays != "0" && ((Session["masterInput"] as BookingRequest)).propDay2 == "0")
                                {
                                    strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs(ActualHalfPrice + ActualPrice)));
                                    TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(h.HotelId));
                                    decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.FulldayPrice).Sum());
                                    decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.HalfdayPrice).Sum());
                                    decimal Fulldaypkgprice = (Math.Abs((ActualPrice - getTotleFullDayPrice)));
                                    decimal Halfdaypkgprice = (Math.Abs((ActualHalfPrice - getTotleHalfDayPrice)));
                                    decimal PkgDiscountPrice = (Math.Abs((Halfdaypkgprice * DiscountPrice / 100)));
                                    decimal PkgDiscountPrice2Day = (Math.Abs((Fulldaypkgprice * SecdayDiscountPercent / 100)));
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (ActualHalfPrice - PkgDiscountPrice) + (ActualPrice - PkgDiscountPrice2Day));
                                }
                                else if (((Session["masterInput"] as BookingRequest)).propDays == "0" && ((Session["masterInput"] as BookingRequest)).propDay2 != "0")
                                {
                                    strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs(ActualPrice + ActualHalfPrice)));
                                    TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(h.HotelId));
                                    decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.FulldayPrice).Sum());
                                    decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.HalfdayPrice).Sum());
                                    decimal Fulldaypkgprice = (Math.Abs((ActualPrice - getTotleFullDayPrice)));
                                    decimal Halfdaypkgprice = (Math.Abs((ActualHalfPrice - getTotleHalfDayPrice)));
                                    decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * DiscountPrice / 100)));
                                    decimal PkgDiscountPrice2Day = (Math.Abs((Halfdaypkgprice * SecdayDiscountPercent / 100)));
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (ActualPrice - PkgDiscountPrice) + (ActualHalfPrice - PkgDiscountPrice2Day));
                                }
                                else if (((Session["masterInput"] as BookingRequest)).propDays != "0" && ((Session["masterInput"] as BookingRequest)).propDay2 != "0")
                                {
                                    strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs(ActualHalfPrice + ActualHalfPrice)));
                                    TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(h.HotelId));
                                    decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.HalfdayPrice).Sum());
                                    decimal Halfdaypkgprice = (Math.Abs((ActualHalfPrice - getTotleHalfDayPrice)));
                                    decimal PkgDiscountPrice = (Math.Abs((Halfdaypkgprice * DiscountPrice / 100)));
                                    decimal PkgDiscountPrice2Day = (Math.Abs((Halfdaypkgprice * SecdayDiscountPercent / 100)));
                                    strSpecialDeal = String.Format("{0:#,##,##0.0}", (ActualHalfPrice - PkgDiscountPrice) + (ActualHalfPrice - PkgDiscountPrice2Day));
                                }

                                //added this formula for checking minimum special discount                    
                                if (ViewState["MinConsider"] != null && (Convert.ToInt32(ViewState["MinConsider"]) <= DiscountPrice || Convert.ToInt32(ViewState["MinConsider"]) <= SecdayDiscountPercent))
                                {
                                    strActualprice = strSpecialDeal;
                                    IsPromoted = "NO";
                                }
                                else
                                {

                                }
                            }
                            //End Get Price from actualpackageprice by hotelid without discount
                            else if (DiscountPrice == 0 && SecdayDiscountPercent == 0 && ((Session["masterInput"] as BookingRequest)).propDuration == "2")
                            {
                                IsPromoted = "NO";

                                if (((Session["masterInput"] as BookingRequest)).propDays == "0" && ((Session["masterInput"] as BookingRequest)).propDay2 == "0")
                                {
                                    strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs(ActualPrice + ActualPrice)));
                                }
                                else if (((Session["masterInput"] as BookingRequest)).propDays != "0" && ((Session["masterInput"] as BookingRequest)).propDay2 != "0")
                                {
                                    strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs(ActualHalfPrice + ActualHalfPrice)));
                                }
                                else
                                {
                                    strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs(ActualPrice + ActualHalfPrice)));
                                }
                            }
                            else if ((DiscountPrice > 0 || SecdayDiscountPercent > 0) && ((Session["masterInput"] as BookingRequest)).propDuration == "2")
                            {
                                IsPromoted = "YES";
                                if (((Session["masterInput"] as BookingRequest)).propDays == "0" && ((Session["masterInput"] as BookingRequest)).propDay2 == "0")
                                {
                                    TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(h.HotelId));
                                    decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.FulldayPrice).Sum());
                                    decimal Fulldaypkgprice = (Math.Abs((ActualPrice - getTotleFullDayPrice)));
                                    decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * DiscountPrice / 100)));
                                    decimal PkgDiscountPrice2Day = (Math.Abs((Fulldaypkgprice * SecdayDiscountPercent / 100)));
                                    strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice) + ActualPrice)));
                                    if (DiscountPrice > 0 && SecdayDiscountPercent < 0)
                                    {
                                        //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice + PkgDiscountPrice) + ActualPrice)));
                                        strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice + PkgDiscountPrice) + (ActualPrice - PkgDiscountPrice2Day))));
                                    }
                                    else if (DiscountPrice > 0 && SecdayDiscountPercent == 0)
                                    {
                                        //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice + PkgDiscountPrice) + ActualPrice)));
                                        strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice + PkgDiscountPrice) + (ActualPrice))));
                                    }
                                    else if (DiscountPrice < 0 && SecdayDiscountPercent > 0)
                                    {
                                        //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice) + (ActualPrice + PkgDiscountPrice2Day))));
                                        strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice - PkgDiscountPrice) + (ActualPrice + PkgDiscountPrice2Day))));
                                    }
                                    else if (DiscountPrice == 0 && SecdayDiscountPercent > 0)
                                    {
                                        //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice) + (ActualPrice + PkgDiscountPrice2Day))));
                                        strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice) + (ActualPrice + PkgDiscountPrice2Day))));
                                    }
                                    else if (DiscountPrice > 0 && SecdayDiscountPercent > 0)
                                    {
                                        strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice + PkgDiscountPrice) + (ActualPrice + PkgDiscountPrice2Day))));
                                    }
                                    if (Convert.ToDecimal(strActualprice) <= Convert.ToDecimal(strSpecialDeal))
                                    {
                                        strActualprice = strSpecialDeal;
                                    }
                                }
                                else if (((Session["masterInput"] as BookingRequest)).propDays != "0" && ((Session["masterInput"] as BookingRequest)).propDay2 == "0")
                                {
                                    TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(h.HotelId));
                                    decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.FulldayPrice).Sum());
                                    decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.HalfdayPrice).Sum());
                                    decimal Fulldaypkgprice = (Math.Abs((ActualPrice - getTotleFullDayPrice)));
                                    decimal Halfdaypkgprice = (Math.Abs((ActualHalfPrice - getTotleHalfDayPrice)));
                                    decimal PkgDiscountPrice = (Math.Abs((Halfdaypkgprice * DiscountPrice / 100)));
                                    decimal PkgDiscountPrice2Day = (Math.Abs((Fulldaypkgprice * SecdayDiscountPercent / 100)));
                                    strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice) + ActualPrice)));
                                    if (DiscountPrice > 0 && SecdayDiscountPercent < 0)
                                    {
                                        //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice + PkgDiscountPrice) + ActualPrice)));
                                        strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice + PkgDiscountPrice) + (ActualPrice - PkgDiscountPrice2Day))));
                                    }
                                    else if (DiscountPrice > 0 && SecdayDiscountPercent == 0)
                                    {
                                        //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice + PkgDiscountPrice) + ActualPrice)));
                                        strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice + PkgDiscountPrice) + (ActualPrice))));
                                    }
                                    else if (DiscountPrice < 0 && SecdayDiscountPercent > 0)
                                    {
                                        //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice) + (ActualPrice + PkgDiscountPrice2Day))));
                                        strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice - PkgDiscountPrice) + (ActualPrice + PkgDiscountPrice2Day))));
                                    }
                                    else if (DiscountPrice == 0 && SecdayDiscountPercent > 0)
                                    {
                                        //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice) + (ActualPrice + PkgDiscountPrice2Day))));
                                        strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice) + (ActualPrice + PkgDiscountPrice2Day))));
                                    }
                                    else if (DiscountPrice > 0 && SecdayDiscountPercent > 0)
                                    {
                                        strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice + PkgDiscountPrice) + (ActualPrice + PkgDiscountPrice2Day))));
                                    }
                                    if (Convert.ToDecimal(strActualprice) <= Convert.ToDecimal(strSpecialDeal))
                                    {
                                        strActualprice = strSpecialDeal;
                                    }
                                }
                                else if (((Session["masterInput"] as BookingRequest)).propDays == "0" && ((Session["masterInput"] as BookingRequest)).propDay2 != "0")
                                {
                                    TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(h.HotelId));
                                    decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.FulldayPrice).Sum());
                                    decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.HalfdayPrice).Sum());
                                    decimal Fulldaypkgprice = (Math.Abs((ActualPrice - getTotleFullDayPrice)));
                                    decimal Halfdaypkgprice = (Math.Abs((ActualHalfPrice - getTotleHalfDayPrice)));
                                    decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * DiscountPrice / 100)));
                                    decimal PkgDiscountPrice2Day = (Math.Abs((Halfdaypkgprice * SecdayDiscountPercent / 100)));
                                    strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice) + ActualHalfPrice)));
                                    if (DiscountPrice > 0 && SecdayDiscountPercent < 0)
                                    {
                                        //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice + PkgDiscountPrice) + (ActualHalfPrice))));
                                        strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice + PkgDiscountPrice) + (ActualHalfPrice - PkgDiscountPrice2Day))));
                                    }
                                    else if (DiscountPrice > 0 && SecdayDiscountPercent == 0)
                                    {
                                        //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice + PkgDiscountPrice) + (ActualHalfPrice))));
                                        strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice + PkgDiscountPrice) + (ActualHalfPrice))));
                                    }
                                    else if (DiscountPrice < 0 && SecdayDiscountPercent > 0)
                                    {
                                        //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice) + (ActualHalfPrice + PkgDiscountPrice2Day))));
                                        strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice - PkgDiscountPrice) + (ActualHalfPrice + PkgDiscountPrice2Day))));
                                    }
                                    else if (DiscountPrice == 0 && SecdayDiscountPercent > 0)
                                    {
                                        //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice) + (ActualHalfPrice + PkgDiscountPrice2Day))));
                                        strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice) + (ActualHalfPrice + PkgDiscountPrice2Day))));
                                    }
                                    else if (DiscountPrice > 0 && SecdayDiscountPercent > 0)
                                    {
                                        strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualPrice + PkgDiscountPrice) + (ActualHalfPrice + PkgDiscountPrice2Day))));
                                    }
                                    if (Convert.ToDecimal(strActualprice) <= Convert.ToDecimal(strSpecialDeal))
                                    {
                                        strActualprice = strSpecialDeal;
                                    }
                                }
                                else if (((Session["masterInput"] as BookingRequest)).propDays != "0" && ((Session["masterInput"] as BookingRequest)).propDay2 != "0")
                                {
                                    TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(h.HotelId));
                                    decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.HalfdayPrice).Sum());
                                    decimal Halfdaypkgprice = (Math.Abs((ActualHalfPrice - getTotleHalfDayPrice)));
                                    decimal PkgDiscountPrice = (Math.Abs((Halfdaypkgprice * DiscountPrice / 100)));
                                    decimal PkgDiscountPrice2Day = (Math.Abs((Halfdaypkgprice * SecdayDiscountPercent / 100)));
                                    strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice) + ActualHalfPrice)));
                                    if (DiscountPrice > 0 && SecdayDiscountPercent < 0)
                                    {
                                        //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice + PkgDiscountPrice) + (ActualHalfPrice))));
                                        strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice + PkgDiscountPrice) + (ActualHalfPrice - PkgDiscountPrice2Day))));
                                    }
                                    else if (DiscountPrice > 0 && SecdayDiscountPercent == 0)
                                    {
                                        //strActualprice = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice + PkgDiscountPrice) + (ActualHalfPrice))));
                                        strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice + PkgDiscountPrice) + (ActualHalfPrice))));
                                    }
                                    else if (DiscountPrice < 0 && SecdayDiscountPercent > 0)
                                    {
                                        strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice - PkgDiscountPrice) + (ActualHalfPrice + PkgDiscountPrice2Day))));
                                        //strSpecialDeal = String.Format("{0:#,##,##0.0}", (ActualHalfPrice - PkgDiscountPrice) + (ActualHalfPrice + PkgDiscountPrice2Day));
                                    }
                                    else if (DiscountPrice == 0 && SecdayDiscountPercent > 0)
                                    {
                                        strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice) + (ActualHalfPrice + PkgDiscountPrice2Day))));
                                        //strSpecialDeal = String.Format("{0:#,##,##0.0}", (ActualHalfPrice) + (ActualHalfPrice + PkgDiscountPrice2Day));
                                    }
                                    else if (DiscountPrice > 0 && SecdayDiscountPercent > 0)
                                    {
                                        strSpecialDeal = String.Format("{0:#,##,##0.0}", (Math.Abs((ActualHalfPrice + PkgDiscountPrice) + (ActualHalfPrice + PkgDiscountPrice2Day))));
                                    }
                                    if (Convert.ToDecimal(strActualprice) <= Convert.ToDecimal(strSpecialDeal))
                                    {
                                        strActualprice = strSpecialDeal;
                                    }
                                }
                            }
                            #endregion
                        }

                        //}
                        //else
                        //{
                        //    IsPromoted = "NO";
                        //}

                        if (HotelData == "[")
                        {

                            HotelData += "['" + h.HotelName.Replace("'", "~") + "', " + h.Latitude + "," + h.Longitude + "," + 1 + ",'" + IsPromoted + "','" + h.HotelAddress.Replace(System.Environment.NewLine, string.Empty).Replace("'", "~") + "','" + h.Stars + "','" + strActualprice + "','" + strSpecialDeal + "']";
                        }
                        else
                        {
                            HotelData = HotelData + ",[" + "'" + h.HotelName.Replace("'", "~") + "', " + h.Latitude + "," + h.Longitude + "," + 1 + ",'" + IsPromoted + "','" + h.HotelAddress.Replace(System.Environment.NewLine, string.Empty).Replace("'", "~") + "','" + h.Stars + "','" + strActualprice + "','" + strSpecialDeal + "']";
                        }
                    }
                    foreach (ViewForRequestSearch rh in objRequested)
                    {
                        if (objHotel.Where(a => a.HotelId == rh.HotelId).FirstOrDefault() == null)
                        {
                            string IsPromoted = "RE";
                            string strSpecialDeal = "";
                            string strActualprice = "";

                            if (HotelData == "[")
                            {
                                HotelData += "['" + rh.HotelName.Replace("'", "~") + "', " + rh.Latitude + "," + rh.Longitude + "," + 1 + ",'" + IsPromoted + "','" + rh.HotelAddress.Replace(System.Environment.NewLine, string.Empty).Replace("'", "~") + "','" + rh.Stars + "','" + strActualprice + "','" + strSpecialDeal + "']";
                            }
                            else
                            {
                                HotelData = HotelData + ",[" + "'" + rh.HotelName.Replace("'", "~") + "', " + rh.Latitude + "," + rh.Longitude + "," + 1 + ",'" + IsPromoted + "','" + rh.HotelAddress.Replace(System.Environment.NewLine, string.Empty).Replace("'", "~") + "','" + rh.Stars + "','" + strActualprice + "','" + strSpecialDeal + "']";
                            }

                        }


                    }
                    HotelData += "]";
                }
                if (HotelData == "[")
                {
                    HotelData = "[[]]";
                }
            }
            else
            {
                HotelData = "[[]]";
            }
        }

        protected void drpCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (drpCountry.SelectedValue != "")
            {
                BindCityDropdownByCountryID(Convert.ToInt32(drpCountry.SelectedValue));
                //Session["Country"] = drpCountry.SelectedValue;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            SearchButton.Invoke(sender, e);
            #region Commented By Gaurav
            //if (Request.RawUrl.ToLower().Contains("bookingstep"))
            //{
            //    SearchTracer st = null;
            //    Createbooking objBooking = null;
            //    BookingManager bm = new BookingManager();
            //    if (objBooking == null)
            //    {
            //        if (Session["SerachID"] != null)
            //        {
            //            st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
            //            objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
            //            if (objBooking != null)
            //            {
            //                bm.ResetIsReserverFalse(objBooking);
            //            }
            //        }
            //    }
            //    Session.Remove("SearchID");
            //    Session.Remove("Search");
            //}
            //else
            //{
            //    Session.Remove("RequestID");
            //    Session.Remove("Request");
            //}
            //Session.Remove("MeetingRoomConfig");
            //Session.Remove("MeetingRoom");
            //Session.Remove("Hotel");
            //Session.Remove("RequestedmeetingroomConfig");
            //Session.Remove("Requestedmeetingroom");
            //Session.Remove("Requestedhotel");
            //Session["Country"] = drpCountry.SelectedValue;
            //Session["City"] = drpCity.SelectedValue;
            //Session.Remove("masterInput");
            //ManageSession();
            ////Response.Redirect("SearchResult.aspx");


            //Session.Remove("BookingDone");
            //Session["participants"] = new Dictionary<long, Dictionary<long, string>>();
            //Session["PostBack"] = null;
            //CheckNexCondition();
            #endregion
        }

        protected void drpCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(drpCity.SelectedValue) != 0 && Convert.ToInt32(drpCountry.SelectedValue) != 0)
            {

            }

        }

        protected void drpDuration_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (drpDuration.SelectedValue == "2")
            {
                divDay2.Visible = true;
            }
            else
            {
                divDay2.Visible = false;
            }
        }

        void BindCountryDropdown()
        {
            TList<Country> lstcountry = objManageSearch.GetCountryData();
            if (lstcountry.Count == 1)
            {
                drpCountry.DataTextField = "CountryName";
                drpCountry.DataValueField = "Id";
                drpCountry.DataSource = lstcountry;
                drpCountry.DataBind();
                //drpCountry.Items.Insert(0, new ListItem("--Select country--", "0"));
                drpCountry.SelectedIndex = 0;
            }
            else
            {
                drpCountry.DataTextField = "CountryName";
                drpCountry.DataValueField = "Id";
                drpCountry.DataSource = lstcountry;
                drpCountry.DataBind();
                drpCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));
            }
        }

        /// <summary>
        /// This function used for bind cities by countryID.
        /// </summary>
        /// <param name="intcountryID"></param>
        void BindCityDropdownByCountryID(int intcountryID)
        {
            if (intcountryID != 0)
            {
                drpCity.DataValueField = "Id";
                drpCity.DataTextField = "City";
                drpCity.DataSource = objManageSearch.GetCityData(Convert.ToInt32(drpCountry.SelectedItem.Value));
                drpCity.DataBind();
                drpCity.Items.Insert(0, new ListItem("--Select City--", "0"));
            }
            else
            {
                drpCity.Items.Clear();
                drpCity.Items.Insert(0, new ListItem("--Select City--", "0"));
            }
        }

        protected void imgMapSearch_Click(object sender, ImageClickEventArgs e)
        {
            
            if (usercontroltype == "Basic")
            {
                //Session["Country"] = drpCountry.SelectedValue;
                //Session["City"] = drpCity.SelectedValue;
                Session["IsMapSearch"] = "YES";
                usercontroltype = "Map";
                //GetCenterPoint(Convert.ToInt32(propCity));
                //ManageSession();
            }
            else
            {
                //Session["Country"] = drpCountry.SelectedValue;
                //Session["City"] = drpCity.SelectedValue;
                Session["IsMapSearch"] = "NO";
                usercontroltype = "Basic";
                //ManageSession();
            }
            MapSearch.Invoke(sender, e);
            //CheckNexCondition();
        }

        //void ManageSession()
        //{
        //    string WhereClause = string.Empty;
        //    string WhereClauseday2 = string.Empty;
        //    if (drpCountry.SelectedValue != "0")
        //    {
        //        if (drpCountry.SelectedValue != "")
        //        {
        //            if (WhereClause.Length > 0)
        //            {
        //                WhereClause += " and ";
        //                WhereClauseday2 += " and ";
        //            }
        //            WhereClause += HotelColumn.CountryId + "=" + drpCountry.SelectedValue;
        //            WhereClauseday2 += HotelColumn.CountryId + "=" + drpCountry.SelectedValue;
        //        }
        //    }

        //    if (drpCity.SelectedIndex != 0)
        //    {
        //        if (drpCity.SelectedValue != "")
        //        {
        //            if (WhereClause.Length > 0)
        //            {
        //                WhereClause += " and ";
        //                WhereClauseday2 += " and ";
        //            }
        //            WhereClause += HotelColumn.CityId + "=" + drpCity.SelectedValue;
        //            WhereClauseday2 += HotelColumn.CityId + "=" + drpCity.SelectedValue;
        //        }
        //    }
        //    if (drpDuration.SelectedValue == "1")
        //    {
        //        if (WhereClause.Length > 0)
        //        {
        //            WhereClause += " and ";
        //            if (drpDays.SelectedValue == "0")
        //            {
        //                WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
        //            }
        //            else if (drpDays.SelectedValue == "1")
        //            {
        //                WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
        //            }
        //            else if (drpDays.SelectedValue == "2")
        //            {
        //                WhereClause += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
        //            }
        //        }
        //    }
        //    if (drpDuration.SelectedValue == "2")
        //    {
        //        if (WhereClause.Length > 0)
        //        {
        //            WhereClause += " and ";
        //            WhereClauseday2 += " and ";
        //            if (drpDays.SelectedValue == "0")
        //            {
        //                WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
        //            }
        //            else if (drpDays.SelectedValue == "1")
        //            {
        //                WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
        //            }
        //            else if (drpDays.SelectedValue == "2")
        //            {
        //                WhereClause += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
        //            }


        //            if (drpDay2.SelectedValue == "0")
        //            {
        //                WhereClauseday2 += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
        //            }
        //            else if (drpDay2.SelectedValue == "1")
        //            {
        //                WhereClauseday2 += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
        //            }
        //            else if (drpDay2.SelectedValue == "2")
        //            {
        //                WhereClauseday2 += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
        //            }
        //        }
        //    }

        //    if (txtBookingDate.Text != "")
        //    {
        //        DateTime fromDate = new DateTime(Convert.ToInt32("20" + txtBookingDate.Text.Split('/')[2]), Convert.ToInt32(txtBookingDate.Text.Split('/')[1]), Convert.ToInt32(txtBookingDate.Text.Split('/')[0]));

        //        if (WhereClause.Length > 0)
        //        {
        //            WhereClause += " and ";
        //            WhereClauseday2 += " and ";
        //        }
        //        WhereClause += AvailabilityColumn.AvailabilityDate + "='" + fromDate + "'" + " and " + " (DATEDIFF(day, GETDATE(),'" + fromDate + "')) >= " + AvailabilityColumn.LeadTimeForMeetingRoom + "";
        //        WhereClauseday2 += AvailabilityColumn.AvailabilityDate + "='" + fromDate.AddDays(1) + "'" + " and " + " (DATEDIFF(day, GETDATE(),'" + fromDate.AddDays(1) + "')) >= " + AvailabilityColumn.LeadTimeForMeetingRoom + "";
        //    }


        //    if (txtParticipant.Text != "" && txtParticipant.Text != "0")
        //    {
        //        if (WhereClause.Length > 0)
        //        {
        //            WhereClause += " and ";
        //            WhereClauseday2 += " and ";
        //        }
        //        WhereClause += txtParticipant.Text + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + "";
        //        WhereClauseday2 += txtParticipant.Text + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + "";
        //    }

        //    //Send all search session value
        //    Session["Where"] = WhereClause;
        //    if (drpDuration.SelectedValue == "2")
        //    {
        //        Session["Where2"] = WhereClauseday2;
        //    }

        //    //Maintain session value for all controls 
        //    objBookingRequest = new BookingRequest();
        //    objBookingRequest.propCountry = drpCountry.SelectedValue;
        //    objBookingRequest.propCity = drpCity.SelectedValue;
        //    objBookingRequest.propDate = txtBookingDate.Text;
        //    objBookingRequest.propDuration = drpDuration.SelectedValue;
        //    objBookingRequest.propDays = drpDays.SelectedValue;
        //    objBookingRequest.propDay2 = drpDay2.SelectedValue;
        //    if (txtParticipant.Text != "")
        //    {
        //        objBookingRequest.propParticipants = txtParticipant.Text;
        //    }
        //    else
        //    {
        //        objBookingRequest.propParticipants = "0";
        //    }
        //    Session["masterInput"] = objBookingRequest;

        //    //Dictionary<long, string> participants = new Dictionary<long, string>();
        //    //participants.Add(1, objBookingRequest.propParticipants);
        //    //Session["participants"] = participants;
        //}
    }
