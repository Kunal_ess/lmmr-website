﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using System.Web.UI.HtmlControls;
using System.Configuration;
using log4net;
using log4net.Config;
using AjaxControlToolkit;
using System.Xml;
using System.Data;
using System.Web.Services;
#endregion
public partial class UserControl_Agency_SearchBookingPage : BaseUserControl
{
    #region variables
    public VList<BookingRequestViewList> BookingDataSource
    {
        get
        {
            if (Session["BookingDataSource"] == null)
            {
                return null;
            }
            else
            {
                return Session["BookingDataSource"] as VList<BookingRequestViewList>;
            }
        }
        set
        {
            Session["BookingDataSource"] = value;
        }
    }
    public VList<ViewForRequestSearch> RequestDataSource
    {
        get
        {
            if (Session["RequestDataSource"] == null)
            {
                return null;
            }
            else
            {
                return Session["RequestDataSource"] as VList<ViewForRequestSearch>;
            }
        }
        set
        {
            Session["RequestDataSource"] = value;
        }
    }
    public VList<ViewForRequestSearch> RequestDataSourceAll
    {
        get
        {
            if(Session["RequestDataSourceAll"]==null)
            {
                return null;
            }
            else 
            {
                return Session["RequestDataSourceAll"] as VList<ViewForRequestSearch>;
            }
        }
        set
        {
            Session["RequestDataSourceAll"]=value;
        }
    }
    public class Mapdate
    {
        public string Hotelbname;
        public string Logitude;
        public string Latitude;
        public string IsPromoted;

    }

    public TList<MeetingRoom> objmeetingroom = new TList<MeetingRoom>();
    public TList<Hotel> objhotel = new TList<Hotel>();
    public TList<MeetingRoomConfig> objConfig = new TList<MeetingRoomConfig>();

    public TList<MeetingRoom> objRequestedmeetingroom = new TList<MeetingRoom>();
    public TList<Hotel> objRequestedHotel = new TList<Hotel>();
    public TList<MeetingRoomConfig> objRequestedMeetingConfig = new TList<MeetingRoomConfig>();    

    public TList<PackageMaster> pkgmaster
    {
        get;
        set;
    }

    public VList<ViewMeetingRoomAvailability> BookingMeetingRoom
    {
        get;
        set;
    }
    public VList<ViewMeetingRoomAvailability> BookingMeetingRoomDay2
    {
        get;
        set;
    }
    public VList<ViewForRequestSearch> RequestMeetingRoom
    {
        get;
        set;
    }
    public VList<ViewFacilityIcon> ViewFacIcon
    {
        get;
        set;
    }

    ManageMapSearch objManageMapSearch = new ManageMapSearch();
    HotelManager objHotel = new HotelManager();
    PackagePricingManager objPackagePricingManager = new PackagePricingManager();
    ManageOthers objOthers = new ManageOthers();
    public string HotelData
    {
        get;
        set;
    }
    VList<BookingRequestViewList> vlist;
    VList<BookingRequestViewList> Tempvlist;
    VList<BookingRequestViewList> vlist2day;
    VList<ViewForRequestSearch> vlistRequest;
    VList<ViewForRequestSearch> vlistPriority;
    VList<ViewMeetingRoomForRequest> vlistMeetingRoomRequest;
    VList<BookingRequestViewList> FinalResult = new VList<BookingRequestViewList>();
    VList<ViewForRequestSearch> FinalResultRequest = new VList<ViewForRequestSearch>();
    BookingRequest objBookingRequest = new BookingRequest();
    MeetingRoomDescManager ObjMeetingRoomDesc = new MeetingRoomDescManager();
    MeetingRoomConfigManager ObjMeetingRoomConfigManager = new MeetingRoomConfigManager();
    HotelInfo ObjHotelinfo = new HotelInfo();
    Facilities objFacility = new Facilities();
    VList<ViewFacilityIcon> vicon;
    public decimal CurrentLat
    {
        get;
        set;
    }
    public decimal CurrentLong
    {
        get;
        set;
    }
    public string status = "";
    public string status2 = "";
    public DateTime BokingDate;
    CurrencyManager cm = new CurrencyManager();
    public XmlDocument XMLLanguage
    {
        get { return (XmlDocument)ViewState["Language"]; }
        set { ViewState["Language"] = value; }
    }
    public decimal CurrencyConvert
    {
        get { return Convert.ToDecimal(ViewState["CurrencyConvert"]); }
        set { ViewState["CurrencyConvert"] = value; }
    }
    public string UserCurrency
    {
        get { return Convert.ToString(ViewState["UserCurrency"]); }
        set { ViewState["UserCurrency"] = value; }
    }
    public string HotelCurrency
    {
        get { return Convert.ToString(ViewState["HotelCurrency"]); }
        set { ViewState["HotelCurrency"] = value; }
    }
    public string CurrencySign
    {
        get { return Convert.ToString(ViewState["CurrencySign"]); }
        set { ViewState["CurrencySign"] = value; }
    }
    string btnVar = "";

    //------------------------new variables by tariq------------------------//
    class DistanceCalculator
    {
        //Haversine's Formula
        public double FindDistance(CoOrdiantes position1, CoOrdiantes position2, DistanceUnit unit)
        {
            double CalculatedDistance = 0;

            //Choosing Earth's Radius
            double EarthRadius = (unit == DistanceUnit.Kilometers) ? 6371 : 3960;

            //Finding the delta of latitude i.e difference between two latitudes in radians.
            double dLat = GetRadianValue(position1.Latitude - position2.Latitude);

            //Finding the delta of latitude i.e difference between two latitudes in radians.
            double dLon = GetRadianValue(position1.Longitude - position2.Longitude);

            //Get rad of Lat1
            double rLat1 = GetRadianValue(position1.Latitude);

            //Get rad of Lat2
            double rLat2 = GetRadianValue(position2.Latitude);

            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) + Math.Cos(rLat1) * Math.Cos(rLat2) * Math.Sin(dLon / 2) * Math.Sin(dLon / 2);

            //Calculate the Angel of Corner Opposite
            //This also can be calculated with other formula mentioned below.
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            //double c = 2 * Math.Asin(Math.Min(1, Math.Sqrt(a)));

            CalculatedDistance = c * EarthRadius;

            return CalculatedDistance;
        }

        private double GetRadianValue(double value)
        {
            return (Math.PI / 180) * value;
        }
    }

    class CoOrdiantes
    {
        private double _Latitude;
        public double Latitude
        {
            get
            { return _Latitude; }
            set
            { _Latitude = value; }
        }

        private double _Longitude;
        public double Longitude
        {
            get
            { return _Longitude; }
            set
            { _Longitude = value; }
        }
    }

    enum DistanceUnit
    {
        Kilometers,
        Miles
    }

    public bool ShowBooking
    {
        get
        {
            return Session["ShowBooking"] == null ? true : Convert.ToBoolean(Session["ShowBooking"]);
        }
        set
        {
            Session["ShowBooking"] = value;
        }
    }
    #endregion

    #region Page load
    /// <summary>
    /// Page load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        infoIcon.ToolTip = GetKeyResult("SEARCHRESULTINFORMATIONTEXT");
        lblPriceDetail.Text = GetKeyResult("PRICEDETAIL");
        lblPriceDetail.ToolTip = GetKeyResult("SEARCHRESULTINFORMATIONTEXT");        
        if (!IsPostBack)
        {
            if (Session["masterInput"] != null)
            {
                //GetCurrency();
                ViewState["CurrentPage"] = null;
                ViewState["CurrentPageRequest"] = null;
                BindSearchResult();
                fillfilter();
                //Request["stars[]"] = "0";
            }
        }
        else
        {
            //if (Session["masterInput"] != null)
            //{
            //    GetCurrency();
            //}
            ViewState["CurrentPage"] = null;
            ViewState["CurrentPageRequest"] = null;
            ApplyPaging();
            ApplyPaginOnrequest();
        }

    }
    #endregion

    #region Fill Filter
    public void fillfilter()
    {
        BindDistanceToDDL();
        bindThemeDDL();
        bindZonesDDL();
        txtStars.Text = "All";
    }
    #endregion

    #region Get Currency per Country
    public void GetCurrency()
    {
        if (Session["CurrencyID"] != null)
        {
            Currency objUserCurrency = cm.GetCurrencyDetailsByID(Convert.ToInt64(Session["CurrencyID"]));
            UserCurrency = objUserCurrency.Currency;
            CurrencySign = objUserCurrency.CurrencySignature;
            //Currency objCurrency = cm.GetCurrencyDetailsByCountryID(Convert.ToInt32(((Session["masterInput"] as BookingRequest).propCountry)));
            TList<Others> objOther = objOthers.GetInfoByCountryID(Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry));
            Currency objCurrency = cm.GetCurrencyDetailsByID(Convert.ToInt64(objOther[0].CurrencyId));
            HotelCurrency = objCurrency.Currency;
            string currency = CurrencyManager.Currency(HotelCurrency, UserCurrency);
            CurrencyConvert = Convert.ToDecimal(currency == "N/A" || currency == "</HTML>" ? "1" : currency);

            pkgmaster = objPackagePricingManager.GetStandardName(Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry));
        }
    }
    #endregion


    #region Bind Search Result
    // Seach Booking and request algorithm according to the search criteria   
    public void BindSearchResult()
    {
        try
        {
            sortingPanel.Enabled = true;
            sortingPanel.CssClass = "";
            pnlBookRequest.Enabled = true;
            divBookRequest.Visible = false;
            pnlBookRequest.CssClass = "";
            objmeetingroom.Clear();
            objhotel.Clear();
            objConfig.Clear();
            objRequestedmeetingroom.Clear();
            objRequestedHotel.Clear();
            objRequestedMeetingConfig.Clear();
            hdnHotelId.Value = "";
            hdnMeetingRoomId.Value = "";
            hdnConfigurationId.Value = "";
            hdnRequestedHotelId.Value = "";
            hdnRequestedMeetingRoomId.Value = "";
            hdnRequestedConfigurationId.Value = "";
            grvRequest.Enabled = true;
            grvRequest.CssClass = "";
            ViewState["PopUpChk"] = null;//SerachID, Search
            ViewState["PrimarySelected"] = null;
            ViewState["RequestPaging"] = null;
            ViewState["RequestPagingBasket"] = null;
            ViewState["btnVar"] = null;
            Session["SerachID"] = null;
            Session["Search"] = null;
            Session["MeetingRoom"] = null;
            Session["meetingroomConfig"] = null;
            Session["hotel"] = null;
            Session["IsSecondary"] = "";
            Session["IsRequestedSecondary"] = "";
            Session["RequestedmeetingroomConfig"] = null;
            Session["Requestedmeetingroom"] = null;
            Session["Requestedhotel"] = null;
            Session["RequestCount"] = null;
            Session["SessionOrderBy"] = null;
            Session["SessionOrderByRequest"] = null;
            Session["OrderbyRequest"] = null;
            Session["OrderbyBooking"] = null;
            Session["DistanceArea"] = "0";
            Session["DistanceAreaRequest"] = "0";
            ResetFilter();
            divshoppingcart.Style.Add("display", "none");
            divRequestBasket.Style.Add("display", "none");
            filterBox.Style.Add("display", "block");
            GetCurrency();
            //Get Minimum consider special as per country id
            TList<Others> objOther = objOthers.GetInfoByCountryID(Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry));
            if (objOther.Count > 0 && objOther[0].CountryId != null)
            {
                ViewState["MinConsiderSpecial"] = objOther[0].MinConsiderSpl;
            }
            else
            {
                ViewState["MinConsiderSpecial"] = null;
            }

            grvBooking.Visible = true;
            ContentPlaceHolder searchContent = (ContentPlaceHolder)Page.Master.FindControl("cntLeftSearch");
            DropDownList drpRadius = (DropDownList)searchContent.FindControl("LeftSearchPanel1").FindControl("drpRadius");
            //if (ShowBooking)
            //{
                #region Booking Grid
                if (BookingDataSource != null)
                {
                    string whereclaus = Convert.ToString(Session["Where"]);
                    string orderby = "IsPriority DESC," + HotelColumn.BookingAlgo + " DESC";
                    if (Convert.ToString(((Session["masterInput"] as BookingRequest)).propDuration) == "2")
                    {
                        string whereclaus2 = Convert.ToString(Session["Where2"]);
                        vlist2day = objBookingRequest.GetBookingReqDetails(whereclaus2, orderby).FindAllDistinct(BookingRequestViewListColumn.HotelId); ;
                        Tempvlist = objBookingRequest.GetBookingReqDetails(whereclaus, orderby).FindAllDistinct(BookingRequestViewListColumn.HotelId); ;
                        vlist = new VList<BookingRequestViewList>();
                        for (int i = 0; i < Tempvlist.Count; i++)
                        {
                            BookingRequestViewList b = vlist2day.Where(a => a.HotelId == Tempvlist[i].HotelId && a.MeetingRoomId == Tempvlist[i].MeetingRoomId).FirstOrDefault();
                            if (b != null)
                            {
                                vlist.Add(b);
                            }
                        }
                    }
                    else
                    {
                        vlist = objBookingRequest.GetBookingReqDetails(whereclaus, orderby).FindAllDistinct(BookingRequestViewListColumn.HotelId); ;
                    }
                }
                else
                {
                    vlist = BookingDataSource;
                }
                BindBookingMeetingroom();



                if (drpRadius != null)
                {

                    if (drpRadius.SelectedValue != "0")
                    {
                        HiddenField hndActualLogitude = (HiddenField)searchContent.FindControl("LeftSearchPanel1").FindControl("hdnMapLongitude");
                        HiddenField hndActualLatitude = (HiddenField)searchContent.FindControl("LeftSearchPanel1").FindControl("hdnMapLatitude");
                        foreach (BookingRequestViewList objlist in vlist)
                        {
                            double actualLogitude = Convert.ToDouble(hndActualLogitude.Value);
                            double actuallatitude = Convert.ToDouble(hndActualLatitude.Value);
                            double hotelLogitude = Convert.ToDouble(objlist.Longitude);
                            double hotelLatitude = Convert.ToDouble(objlist.Latitude);
                            double hotelDistance = distance(actualLogitude, actuallatitude, hotelLogitude, hotelLatitude);

                            if (hotelDistance <= Convert.ToDouble(drpRadius.SelectedValue))
                            {
                                FinalResult.Add(objlist);
                            }
                        }
                    }

                    else
                    {
                        FinalResult = vlist;
                    }
                }
                else
                {
                    FinalResult = vlist;
                }
                grvBooking.DataSource = FinalResult;
                grvBooking.DataBind();
                //hdnBookingCount.Value = FinalResult.Count.ToString();
                if (FinalResult == null)
                {
                    lblResultCount.Text = "0";
                }
                else
                {
                    lblResultCount.Text = Convert.ToString(FinalResult == null ? "0" : Convert.ToString(FinalResult.Count));
                }
                //foreach (GridViewRow r in grvBooking.Rows)
                //{
                //    if (r.RowType == DataControlRowType.DataRow)
                //    {
                //        LinkButton lnbRequestOnline = (LinkButton)r.FindControl("lnbRequestOnline");
                //        LinkButton lnbBookOnline = (LinkButton)r.FindControl("lnbBookOnline");
                //        LinkButton btnSeeAll = (LinkButton)r.FindControl("btnSeeAll");
                //        btnSeeAll.Enabled = true;
                //        lnbRequestOnline.Enabled = true;
                //        lnbBookOnline.Enabled = true;
                //    }
                //}
                #endregion
                ApplyPaging();
            //}
            if (FinalResult == null)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "ShowRequest", "jQuery(document).ready(function(){try{ShowRequestOnDemand('request');}catch(err){}});", true);
                hdnManageBookingRequest.Value = "1";
                ShowBooking = false;
                lnkRequestCall.Visible = false;
            }
            else if (FinalResult.Count == 0)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "ShowRequest", "jQuery(document).ready(function(){try{ShowRequestOnDemand('request');}catch(err){}});", true);
                hdnManageBookingRequest.Value = "1";
                ShowBooking = false;
                lnkRequestCall.Visible = false;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "ShowRequest", "jQuery(document).ready(function(){try{ShowRequestOnDemand('booking');}catch(err){}});", true);
                hdnManageBookingRequest.Value = "0";
                ShowBooking = true;
                lnkRequestCall.Visible = true;
            }
            //grvRequest.Visible = true;// !ShowBooking;
            if (!ShowBooking)
            {
                #region Request Grid
                //This Condition for WL
                string whereclausRequest = string.Empty;
                if (Session["WLHotel"] != null)
                {
                    BokingDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));

                    if (Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) != 0 && Session["Zone"] == null)
                    {
                        whereclausRequest = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + " and HotelId IN (" + Convert.ToString(Session["WLHotel"]) + ")";
                    }
                    else if (Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) != 0 && Session["Zone"] != null)
                    {
                        whereclausRequest = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + " and " + HotelColumn.ZoneId + "=" + Convert.ToInt32(Session["Zone"]) + " and HotelId IN (" + Convert.ToString(Session["WLHotel"]) + ")";
                    }
                    else
                    {
                        whereclausRequest = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "'" + " and HotelId IN (" + Convert.ToString(Session["WLHotel"]) + ")";
                    }
                }
                else
                {
                    BokingDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));

                    if (Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) != 0 && Session["Zone"] == null)
                    {
                        whereclausRequest = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity;
                    }
                    else if (Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) != 0 && Session["Zone"] != null)
                    {
                        whereclausRequest = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + " and " + HotelColumn.ZoneId + "=" + Convert.ToInt32(Session["Zone"]);
                    }
                    else
                    {
                        whereclausRequest = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "'";
                    }
                }


                //string orderbyRequest = "IsPriority DESC," + HotelColumn.RequestAlgo + " DESC";
                string orderbyRequest = HotelColumn.RequestAlgo + " DESC";
                if (RequestDataSource == null)
                {
                    vlistRequest = objBookingRequest.GetReqDetails(whereclausRequest, orderbyRequest).FindAllDistinct(ViewForRequestSearchColumn.HotelId);
                }
                else
                {
                    vlistRequest = RequestDataSource;
                }
                vlistPriority = new VList<ViewForRequestSearch>();
                string whereCheck = "BRType = 0 and IsActive = 1 and '" + BokingDate + "' between FromDate and Todate";
                TList<PriorityBasket> objPriorityBasket = objBookingRequest.GetPriorityBasket(whereCheck, string.Empty);
                //Check record from priority basket for order priority
                for (int i = 0; i < objPriorityBasket.Count; i++)
                {
                    ViewForRequestSearch vrYes = vlistRequest.Where(a => a.HotelId == objPriorityBasket[i].HotelId).FirstOrDefault();
                    if (vrYes != null)
                    {
                        vlistPriority.Add(vrYes);
                    }
                }
                for (int j = 0; j < vlistRequest.Count; j++)
                {
                    ViewForRequestSearch vlistChcek = vlistPriority.Where(a => a.HotelId == vlistRequest[j].HotelId).FirstOrDefault();
                    if (vlistChcek == null)
                    {
                        vlistPriority.Add(vlistRequest[j]);
                    }
                }

                if (drpRadius != null)
                {
                    if (drpRadius.SelectedValue != "0")
                    {
                        HiddenField hndActualLogitude = (HiddenField)searchContent.FindControl("LeftSearchPanel1").FindControl("hdnMapLongitude");
                        HiddenField hndActualLatitude = (HiddenField)searchContent.FindControl("LeftSearchPanel1").FindControl("hdnMapLatitude");
                        foreach (ViewForRequestSearch objlist in vlistPriority)
                        {
                            double actualLogitude = Convert.ToDouble(hndActualLogitude.Value);
                            double actuallatitude = Convert.ToDouble(hndActualLatitude.Value);
                            double hotelLogitude = Convert.ToDouble(objlist.Longitude);
                            double hotelLatitude = Convert.ToDouble(objlist.Latitude);
                            double hotelDistance = distance(actualLogitude, actuallatitude, hotelLogitude, hotelLatitude);

                            if (hotelDistance <= Convert.ToDouble(drpRadius.SelectedValue))
                            {
                                if (FinalResult.Where(a => a.HotelId == objlist.HotelId).FirstOrDefault() == null)
                                {
                                    FinalResultRequest.Add(objlist);
                                }
                            }
                        }
                    }

                    else
                    {
                        FinalResultRequest = vlistPriority;
                    }
                }
                else
                {
                    FinalResultRequest = vlistPriority;
                }
                BindRequestMeetingroom();
                grvRequest.DataSource = FinalResultRequest;
                grvRequest.DataBind();
                //hdnRequestCount.Value = Convert.ToString(FinalResultRequest.Count);
                //lblResultCount.Text = Convert.ToString(FinalResultRequest.Count);
                lblResultRequestCount.Text = FinalResultRequest.Count.ToString();
                #endregion
                ApplyPaginOnrequest();
            }
            else
            {
                lblResultRequestCount.Text = Convert.ToString(RequestDataSource.Count);
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }

    public void BindResultforRequest()
    {
        try
        {
            #region Booking Grid
            ViewState["PopUpChk"] = null;
            ViewState["PrimarySelected"] = null;
            Session["IsSecondary"] = "";
            Session["IsRequestedSecondary"] = "";
            string whereclaus = Convert.ToString(Session["Where"]);
            string orderby;
            if (Session["OrderbyBooking"] != null)
            {
                orderby = Session["OrderbyBooking"].ToString();
            }
            else
            {
                orderby = "IsPriority DESC," + HotelColumn.BookingAlgo + " DESC";
            }

            if (Convert.ToString(((Session["masterInput"] as BookingRequest)).propDuration) == "2")
            {
                string whereclaus2 = Convert.ToString(Session["Where2"]);
                vlist2day = objBookingRequest.GetBookingReqDetails(whereclaus2, orderby).FindAllDistinct(BookingRequestViewListColumn.HotelId);
                Tempvlist = objBookingRequest.GetBookingReqDetails(whereclaus, orderby).FindAllDistinct(BookingRequestViewListColumn.HotelId);
                vlist = new VList<BookingRequestViewList>();
                for (int i = 0; i < Tempvlist.Count; i++)
                {
                    BookingRequestViewList b = vlist2day.Where(a => a.HotelId == Tempvlist[i].HotelId && a.MeetingRoomId == Tempvlist[i].MeetingRoomId).FirstOrDefault();
                    if (b != null)
                    {
                        vlist.Add(b);
                    }
                }
            }
            else
            {
                vlist = objBookingRequest.GetBookingReqDetails(whereclaus, orderby).FindAllDistinct(BookingRequestViewListColumn.HotelId);
            }

            BindBookingMeetingroom();

            ContentPlaceHolder searchContent = (ContentPlaceHolder)Page.Master.FindControl("cntLeftSearch");
            DropDownList drpRadius = (DropDownList)searchContent.FindControl("LeftSearchPanel1").FindControl("drpRadius");
            if (Session["SessionOrderBy"] != null)
            {
                vlist = (VList<BookingRequestViewList>)Session["SessionOrderBy"];
            }
            grvBooking.DataSource = vlist;
            grvBooking.DataBind();
            Session["SessionOrderBy"] = vlist;
            ViewState["RequestPaging"] = "Yes";
            lblResultCount.Text = vlist.Count.ToString();


            if (Convert.ToString(Session["hotel"]) != "")
            {

            }
            else
            {
                #region Hide items as per request basket
                foreach (GridViewRow r in grvBooking.Rows)
                {
                    if (r.RowType == DataControlRowType.DataRow)
                    {
                        LinkButton lnbRequestOnline = (LinkButton)r.FindControl("lnbRequestOnline");
                        LinkButton btnSeeAll = (LinkButton)r.FindControl("btnSeeAll");
                        btnSeeAll.Enabled = true;
                        lnbRequestOnline.Enabled = true;


                        Panel pnlBooking = (Panel)r.FindControl("pnlBooking");
                        System.Web.UI.HtmlControls.HtmlGenericControl divcover = ((System.Web.UI.HtmlControls.HtmlGenericControl)r.FindControl("divcover"));
                        HiddenField hdnId = (HiddenField)r.FindControl("hdnHotelId");
                        if (Convert.ToString(Session["Requestedhotel"]) != "")
                        {
                            objRequestedHotel = (TList<Hotel>)Session["Requestedhotel"];
                        }
                        Hotel checkhotel = objRequestedHotel.Where(a => a.Id == Convert.ToInt32(hdnId.Value)).FirstOrDefault();
                        if (checkhotel == null)
                        {
                            pnlBooking.Enabled = true;
                            divcover.Visible = false;
                        }
                        else
                        {
                            if (checkhotel.Id == Convert.ToInt64(hdnId.Value))
                            {
                                pnlBooking.Enabled = false;
                                divcover.Visible = true;
                                //pnlBooking.CssClass = "overlay2";
                            }
                            else
                            {
                                pnlBooking.Enabled = true;
                                divcover.Visible = false;
                            }
                        }
                    }
                }
                #endregion
            }

            #endregion

            #region Request Grid
            BokingDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));
            string whereclausRequest;
            if (Session["WLHotel"] != null)
            {
                if (Session["Zone"] != null)
                {
                    whereclausRequest = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + " and " + HotelColumn.ZoneId + "=" + Convert.ToInt32(Session["Zone"]) + " and HotelId IN (" + Convert.ToString(Session["WLHotel"]) + ")";
                }
                else
                {
                    whereclausRequest = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + " and HotelId IN (" + Convert.ToString(Session["WLHotel"]) + ")";
                }
            }
            else
            {
                if (Session["Zone"] != null)
                {
                    whereclausRequest = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + " and " + HotelColumn.ZoneId + "=" + Convert.ToInt32(Session["Zone"]);
                }
                else
                {
                    whereclausRequest = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity;
                }
            }
            //string orderbyRequest = "IsPriority DESC," + HotelColumn.RequestAlgo + " DESC";
            string orderbyRequest;
            if (Session["OrderbyRequest"] != null)
            {
                orderbyRequest = Session["OrderbyRequest"].ToString();
            }
            else
            {
                orderbyRequest = HotelColumn.RequestAlgo + " DESC";
            }
            vlistRequest = objBookingRequest.GetReqDetails(whereclausRequest, orderbyRequest).FindAllDistinct(ViewForRequestSearchColumn.HotelId);
            //For Priority Basket Order by
            vlistPriority = new VList<ViewForRequestSearch>();
            string whereCheck = "BRType = 0 and IsActive = 1 and '" + BokingDate + "' between FromDate and Todate";
            TList<PriorityBasket> objPriorityBasket = objBookingRequest.GetPriorityBasket(whereCheck, string.Empty);
            //Check record from priority basket for order priority
            for (int i = 0; i < objPriorityBasket.Count; i++)
            {
                ViewForRequestSearch vrYes = vlistRequest.Where(a => a.HotelId == objPriorityBasket[i].HotelId).FirstOrDefault();
                if (vrYes != null)
                {
                    vlistPriority.Add(vrYes);
                }
            }
            for (int j = 0; j < vlistRequest.Count; j++)
            {
                ViewForRequestSearch vlistChcek = vlistPriority.Where(a => a.HotelId == vlistRequest[j].HotelId).FirstOrDefault();
                if (vlistChcek == null)
                {
                    vlistPriority.Add(vlistRequest[j]);
                }
            }
            grvRequest.PageIndex = Convert.ToInt32(ViewState["CurrentPageRequest"] == null ? "0" : ViewState["CurrentPageRequest"]);
            if (Session["SessionOrderByRequest"] != null)
            {
                vlistPriority = (VList<ViewForRequestSearch>)Session["SessionOrderByRequest"];
            }
            BindRequestMeetingroom();
            grvRequest.DataSource = vlistPriority;
            grvRequest.DataBind();
            Session["SessionOrderByRequest"] = vlistPriority;
            ViewState["RequestPagingBasket"] = "Yes";
            lblResultRequestCount.Text = vlistPriority.Count.ToString();

            if (Convert.ToString(Session["hotel"]) != "")
            {
                pnlBookRequest.Enabled = false;
                divBookRequest.Visible = true;
                sortingPanel.Enabled = false;
                sortingPanel.CssClass = "overlay2";
            }
            else
            {
                #region Hide items as per request basket
                foreach (GridViewRow r in grvRequest.Rows)
                {
                    if (r.RowType == DataControlRowType.DataRow)
                    {
                        LinkButton lnbRequestOnline = (LinkButton)r.FindControl("lnbRequestOnline");
                        lnbRequestOnline.Enabled = true;

                        Panel pnlBooking = (Panel)r.FindControl("pnlBooking");
                        System.Web.UI.HtmlControls.HtmlGenericControl divcover = ((System.Web.UI.HtmlControls.HtmlGenericControl)r.FindControl("divcover"));
                        HiddenField hdnId = (HiddenField)r.FindControl("hdnHotelId");
                        if (Convert.ToString(Session["Requestedhotel"]) != "")
                        {
                            objRequestedHotel = (TList<Hotel>)Session["Requestedhotel"];
                        }
                        Hotel checkhotel = objRequestedHotel.Where(a => a.Id == Convert.ToInt32(hdnId.Value)).FirstOrDefault();
                        if (checkhotel == null)
                        {
                            pnlBooking.Enabled = true;
                            divcover.Visible = false;
                        }
                        else
                        {
                            if (checkhotel.Id == Convert.ToInt64(hdnId.Value))
                            {
                                pnlBooking.Enabled = false;
                                divcover.Visible = true;
                                //pnlBooking.CssClass = "overlay2";
                            }
                            else
                            {
                                pnlBooking.Enabled = true;
                                divcover.Visible = false;
                            }

                        }
                    }
                }
                #endregion
            }

            #endregion

            ApplyPaging();
            ApplyPaginOnrequest();

            if (hdnManageBookingRequest.Value == "1")
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "ShowRequest", "jQuery(document).ready(function(){try{ShowRequestOnDemand('request');}catch(err){}});", true);
                hdnManageBookingRequest.Value = "1";
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "ShowRequest", "jQuery(document).ready(function(){try{ShowRequestOnDemand('booking');}catch(err){}});", true);
                hdnManageBookingRequest.Value = "0";
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }

    public void BindBookingMeetingroom()
    {
        #region Check day 1 and 2
        string days = ((Session["masterInput"] as BookingRequest)).propDays;
        string day2 = ((Session["masterInput"] as BookingRequest)).propDay2;
        BokingDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));
        if (days == "0")
        {
            status = ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
        }
        else if (days == "1")
        {
            status = ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
        }
        else if (days == "2")
        {
            status = ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
        }

        //Status is for Day2
        if (day2 == "0")
        {
            status2 = ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
        }
        else if (day2 == "1")
        {
            status2 = ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
        }
        else if (day2 == "2")
        {
            status2 = ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
        }
        #endregion
        int Participant = Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants);
        string whereClauses = "AvailabilityDate='" + BokingDate + "' and " + status + " and " + Participant + " between " + "isnull(MinNo,0) and isnull(MaxNo,0)";
        BookingMeetingRoom = objBookingRequest.GetMeetingRoomAvailability(whereClauses).FindAllDistinct(ViewMeetingRoomAvailabilityColumn.MeetingRoomId);

        string whereclaus2day = "AvailabilityDate='" + BokingDate.AddDays(1) + "' and " + status2 + " and " + Participant + " between " + "isnull(MinNo,0) and isnull(MaxNo,0)";
        BookingMeetingRoomDay2 = objBookingRequest.GetMeetingRoomAvailability(whereclaus2day).FindAllDistinct(ViewMeetingRoomAvailabilityColumn.MeetingRoomId);

        ViewFacIcon = objFacility.GetFacilityIcon(string.Empty, String.Empty);

    }

    public void BindRequestMeetingroom()
    {
        //string whereRequest = "";
        RequestMeetingRoom = RequestDataSourceAll.FindAllDistinct(ViewForRequestSearchColumn.MeetingRoomId);//objBookingRequest.GetReqDetails(whereRequest, string.Empty).FindAllDistinct(ViewForRequestSearchColumn.MeetingRoomId);
        ViewFacIcon = objFacility.GetFacilityIcon(string.Empty, String.Empty);
    }
    #endregion

    #region Calculate Distance
    public double distance(double X1, double Y1, double X2, double Y2)
    {
        double R = 6371;
        double dLat = this.toRadian(Y2 - Y1);
        double dLon = this.toRadian(X2 - X1);
        double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) + Math.Cos(this.toRadian(Y1)) * Math.Cos(this.toRadian(Y2)) * Math.Sin(dLon / 2) * Math.Sin(dLon / 2);
        double c = 2 * Math.Asin(Math.Min(1, Math.Sqrt(a)));
        double d = R * c;

        return d;
    }

    private double toRadian(double val)
    {
        return (Math.PI / 180) * val;
    }
    #endregion


    #region Get Hotel details
    //Get Hotel details and show for booking gridview
    protected void grvBooking_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (Session["masterInput"] != null)
            {
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
                {
                    BookingRequestViewList brvl = e.Row.DataItem as BookingRequestViewList;
                    HiddenField hdnId = (HiddenField)e.Row.FindControl("hdnHotelId");
                    Label lblAddress = (Label)e.Row.FindControl("lblAddress");
                    Label lblDescription = (Label)e.Row.FindControl("lblDescription");
                    Label lblMeetingroomOnline = (Label)e.Row.FindControl("lblMeetingroomOnline");
                    Label lblReview = (Label)e.Row.FindControl("lblReview");
                    Image imgStar = (Image)e.Row.FindControl("imgStar");
                    Image imgHotel = (Image)e.Row.FindControl("imgHotel");

                    Hotel htl = ObjHotelinfo.GetHotelByHotelID(Convert.ToInt32(hdnId.Value));

                    string whereclausedesc = "Hotel_Id=" + htl.Id + " and " + "Language_Id=" + Session["LanguageID"] + "";
                    TList<HotelDesc> lsthdesc = ObjHotelinfo.GetHotelDesc(whereclausedesc, String.Empty);
                    HyperLink hypPhoto = (HyperLink)e.Row.FindControl("hypPhoto");
                    hypPhoto.NavigateUrl = SiteRootPath + "/SlideShowPopup.aspx?id=" + htl.Id;
                    hypPhoto.Attributes.Add("onclick", "return Navigate2(" + htl.Id + ");");
                    if (lsthdesc.Count > 0)
                    {
                        lblDescription.Text = (lsthdesc[0].Description == null ? "" : lsthdesc[0].Description);
                        lblAddress.Text = (lsthdesc[0].Address == null ? "" : lsthdesc[0].Address);
                    }
                    else
                    {
                        int englishID = Convert.ToInt32(ObjHotelinfo.GetAllRequiredLanguage().Find(a => a.Name == "English").Id);
                        string wherecls = "Hotel_Id=" + htl.Id + " and " + "Language_Id=" + englishID + "";
                        TList<HotelDesc> lstEng = ObjHotelinfo.GetHotelDesc(wherecls, String.Empty);
                        lblDescription.Text = (lstEng[0].Description == null ? "" : lstEng[0].Description);
                        lblAddress.Text = (lstEng[0].Address == null ? "" : lstEng[0].Address);
                    }

                    #region For static hotel star
                    if (htl.Stars == 1)
                    {
                        imgStar.ImageUrl = "~/Images/1.png";
                    }
                    else if (htl.Stars == 2)
                    {
                        imgStar.ImageUrl = "~/Images/2.png";
                    }
                    else if (htl.Stars == 3)
                    {
                        imgStar.ImageUrl = "~/Images/3.png";
                    }
                    else if (htl.Stars == 4)
                    {
                        imgStar.ImageUrl = "~/Images/4.png";
                    }
                    else if (htl.Stars == 5)
                    {
                        imgStar.ImageUrl = "~/Images/5.png";
                    }
                    else if (htl.Stars == 6)
                    {
                        imgStar.ImageUrl = "~/Images/6.png";
                    }
                    else if (htl.Stars == 7)
                    {
                        imgStar.ImageUrl = "~/Images/7.png";
                    }
                    else if (htl.Stars == 0)
                    {
                        imgStar.Visible = false;
                    }
                    #endregion

                    if (htl.Logo != null)
                    {
                        imgHotel.ImageUrl = ConfigurationManager.AppSettings["FilePath"] + "/HotelImage/" + htl.Logo;
                    }
                    TList<HotelPhotoVideoGallary> ObjHotelImageAltertext = objBookingRequest.GetAlternateTextPictureVideo(Convert.ToInt32(htl.Id));
                    if (ObjHotelImageAltertext.Count > 0)
                    {
                        imgHotel.ToolTip = ObjHotelImageAltertext[0].AlterText;
                    }

                    System.Web.UI.HtmlControls.HtmlGenericControl divVideo = ((System.Web.UI.HtmlControls.HtmlGenericControl)e.Row.FindControl("divVideo"));
                    TList<HotelPhotoVideoGallary> ObjHotelImageVideo = objBookingRequest.GetAllHotelPictureVideo(Convert.ToInt32(htl.Id));
                    if (ObjHotelImageVideo.Count == 0)
                    {
                        divVideo.Visible = false;
                    }
                    LinkButton button = (LinkButton)e.Row.FindControl("lnbVideo");
                    button.Attributes.Add("onclick", string.Format("return Navigate('{0}')", hdnId.Value));

                    if (Convert.ToString(((Session["masterInput"] as BookingRequest)).propDuration) == "2")
                    {
                        VList<ViewMeetingRoomAvailability> VMeeting = new VList<ViewMeetingRoomAvailability>();
                        ListBase<ViewMeetingRoomAvailability> vMeetingroom1 = BookingMeetingRoom.FindAll(a => a.HotelId == Convert.ToInt64(hdnId.Value));
                        for (int i = 0; i < vMeetingroom1.Count; i++)
                        {
                            ViewMeetingRoomAvailability b = BookingMeetingRoomDay2.Where(a => a.HotelId == vMeetingroom1[i].HotelId && a.MeetingRoomId == vMeetingroom1[i].MeetingRoomId).FirstOrDefault();
                            if (b != null)
                            {
                                VMeeting.Add(b);
                            }
                        }
                        lblMeetingroomOnline.Text = Convert.ToString(VMeeting.Count);
                    }
                    else
                    {
                        lblMeetingroomOnline.Text = Convert.ToString(BookingMeetingRoom.Where(a => a.HotelId == Convert.ToInt64(hdnId.Value)).Count());
                    }

                    //Show count number for online book

                    Label lblActPkgPrice = (Label)e.Row.FindControl("lblActPkgPrice");
                    Label lblActPAckagePrice = (Label)e.Row.FindControl("lblActPAckagePrice");
                    Label lblDiscountPrice = (Label)e.Row.FindControl("lblDiscountPrice");
                    HiddenField hdnPkgPrice = (HiddenField)e.Row.FindControl("hdnPkgPrice");
                    HiddenField hdnPkgHalfPrice = (HiddenField)e.Row.FindControl("hdnPkgHalfPrice");
                    HiddenField hdnDDRpercent = (HiddenField)e.Row.FindControl("hdnDDRpercent");
                    //decimal DiscountPrice = Convert.ToDecimal(hdnDDRpercent.Value);
                    decimal ActualPrice = Convert.ToDecimal(hdnPkgPrice.Value);
                    decimal ActualHalfPrice = Convert.ToDecimal(hdnPkgHalfPrice.Value);

                    DateTime fromDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));
                    TList<SpecialPriceAndPromo> splDiscount = objBookingRequest.GetSpecialPriceandPromoByDate(Convert.ToInt32(hdnId.Value), fromDate);
                    decimal DiscountPrice = Convert.ToDecimal(splDiscount[0].DdrPercent);

                    #region Currency set
                    Label lblCurrencySign = (Label)e.Row.FindControl("lblCurrencySign");
                    Label lblCurrencySign1 = (Label)e.Row.FindControl("lblCurrencySign1");
                    Label lblCurrencySign2 = (Label)e.Row.FindControl("lblCurrencySign2");
                    lblCurrencySign.Text = CurrencySign;
                    lblCurrencySign1.Text = CurrencySign;
                    lblCurrencySign2.Text = CurrencySign;
                    #endregion


                    if (pkgmaster.Count > 0)
                    {
                        #region Get Price from actualpackageprice by hotelid with discount for 1 Day

                        if (DiscountPrice < 0 && ((Session["masterInput"] as BookingRequest)).propDuration == "1")
                        {
                            System.Web.UI.HtmlControls.HtmlGenericControl mydiv = ((System.Web.UI.HtmlControls.HtmlGenericControl)e.Row.FindControl("divDiscount"));
                            System.Web.UI.HtmlControls.HtmlGenericControl divPkg = ((System.Web.UI.HtmlControls.HtmlGenericControl)e.Row.FindControl("divPkg"));
                            if (((Session["masterInput"] as BookingRequest)).propDays == "0")
                            {
                                lblActPkgPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(ActualPrice * Math.Round(CurrencyConvert, 2), 2)));
                                TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(hdnId.Value));
                                decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.FulldayPrice).Sum());
                                decimal Fulldaypkgprice = (Math.Abs((ActualPrice - getTotleFullDayPrice)));
                                decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * DiscountPrice / 100)));
                                lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round((ActualPrice - PkgDiscountPrice) * Math.Round(CurrencyConvert, 2), 2)));
                            }
                            else
                            {
                                lblActPkgPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(ActualHalfPrice * Math.Round(CurrencyConvert, 2), 2)));
                                TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(hdnId.Value));
                                decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.HalfdayPrice).Sum());
                                decimal Fulldaypkgprice = (Math.Abs((ActualHalfPrice - getTotleHalfDayPrice)));
                                decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * DiscountPrice / 100)));
                                lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round((ActualHalfPrice - PkgDiscountPrice) * Math.Round(CurrencyConvert, 2), 2)));
                            }

                            //added this formula for checking minimum special discount
                            if (ViewState["MinConsiderSpecial"] != null && Convert.ToInt32(ViewState["MinConsiderSpecial"]) <= DiscountPrice)
                            {
                                mydiv.Style.Add("display", "none");
                                divPkg.Style.Add("display", "block");
                                lblActPAckagePrice.Text = lblDiscountPrice.Text;
                            }
                            else
                            {
                                mydiv.Style.Add("display", "block");
                                divPkg.Style.Add("display", "none");
                            }
                        }
                        //End Get Price from actualpackageprice by hotelid without discount
                        else if (DiscountPrice == 0 && ((Session["masterInput"] as BookingRequest)).propDuration == "1")
                        {
                            System.Web.UI.HtmlControls.HtmlGenericControl mydiv = ((System.Web.UI.HtmlControls.HtmlGenericControl)e.Row.FindControl("divPkg"));
                            mydiv.Style.Add("display", "block");
                            if (((Session["masterInput"] as BookingRequest)).propDays == "0")
                            {
                                lblActPAckagePrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(Convert.ToDecimal(hdnPkgPrice.Value) * Math.Round(CurrencyConvert, 2), 2)));
                            }
                            else
                            {
                                lblActPAckagePrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(Convert.ToDecimal(hdnPkgHalfPrice.Value) * Math.Round(CurrencyConvert, 2), 2)));
                            }
                            lblDiscountPrice.Text = lblActPAckagePrice.Text;
                        }
                        else if (DiscountPrice >= 0 && ((Session["masterInput"] as BookingRequest)).propDuration == "1")
                        {
                            System.Web.UI.HtmlControls.HtmlGenericControl mydiv = ((System.Web.UI.HtmlControls.HtmlGenericControl)e.Row.FindControl("divPkg"));
                            mydiv.Style.Add("display", "block");
                            if (((Session["masterInput"] as BookingRequest)).propDays == "0")
                            {
                                TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(hdnId.Value));
                                decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.FulldayPrice).Sum());
                                decimal Fulldaypkgprice = (Math.Abs((ActualPrice - getTotleFullDayPrice)));
                                decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * DiscountPrice / 100)));
                                lblActPAckagePrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round((ActualPrice + PkgDiscountPrice) * Math.Round(CurrencyConvert, 2), 2)));
                            }
                            else
                            {
                                TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(hdnId.Value));
                                decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.HalfdayPrice).Sum());
                                decimal Fulldaypkgprice = (Math.Abs((ActualHalfPrice - getTotleHalfDayPrice)));
                                decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * DiscountPrice / 100)));
                                lblActPAckagePrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round((ActualHalfPrice + PkgDiscountPrice) * Math.Round(CurrencyConvert, 2), 2)));
                            }
                            lblDiscountPrice.Text = lblActPAckagePrice.Text;
                        }
                        #endregion

                        #region Get Price from actualpackageprice by hotelid with discount for 2 Day
                        TList<SpecialPriceAndPromo> Spl = objBookingRequest.GetSpecialPriceandPromoByDate(Convert.ToInt32(hdnId.Value), fromDate.AddDays(1));
                        decimal SecdayDiscountPercent = Convert.ToDecimal(Spl[0].DdrPercent);
                        if (((DiscountPrice < 0 && SecdayDiscountPercent < 0) && ((Session["masterInput"] as BookingRequest)).propDuration == "2") || ((DiscountPrice < 0 && SecdayDiscountPercent == 0) && ((Session["masterInput"] as BookingRequest)).propDuration == "2") || ((DiscountPrice == 0 && SecdayDiscountPercent < 0) && ((Session["masterInput"] as BookingRequest)).propDuration == "2"))
                        {
                            System.Web.UI.HtmlControls.HtmlGenericControl mydiv = ((System.Web.UI.HtmlControls.HtmlGenericControl)e.Row.FindControl("divDiscount"));
                            System.Web.UI.HtmlControls.HtmlGenericControl divPkg = ((System.Web.UI.HtmlControls.HtmlGenericControl)e.Row.FindControl("divPkg"));
                            if (((Session["masterInput"] as BookingRequest)).propDays == "0" && ((Session["masterInput"] as BookingRequest)).propDay2 == "0")
                            {
                                lblActPkgPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round((ActualPrice + ActualPrice) * Math.Round(CurrencyConvert, 2), 2)));
                                TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(hdnId.Value));
                                decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.FulldayPrice).Sum());
                                decimal Fulldaypkgprice = (Math.Abs((ActualPrice - getTotleFullDayPrice)));
                                decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * DiscountPrice / 100)));
                                decimal PkgDiscountPrice2Day = (Math.Abs((Fulldaypkgprice * SecdayDiscountPercent / 100)));
                                lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(((ActualPrice - PkgDiscountPrice) + (ActualPrice - PkgDiscountPrice2Day)) * Math.Round(CurrencyConvert, 2), 2)));
                            }
                            else if (((Session["masterInput"] as BookingRequest)).propDays != "0" && ((Session["masterInput"] as BookingRequest)).propDay2 == "0")
                            {
                                lblActPkgPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round((ActualHalfPrice + ActualPrice) * Math.Round(CurrencyConvert, 2), 2)));
                                TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(hdnId.Value));
                                decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.FulldayPrice).Sum());
                                decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.HalfdayPrice).Sum());
                                decimal Fulldaypkgprice = (Math.Abs((ActualPrice - getTotleFullDayPrice)));
                                decimal Halfdaypkgprice = (Math.Abs((ActualHalfPrice - getTotleHalfDayPrice)));
                                decimal PkgDiscountPrice = (Math.Abs((Halfdaypkgprice * DiscountPrice / 100)));
                                decimal PkgDiscountPrice2Day = (Math.Abs((Fulldaypkgprice * SecdayDiscountPercent / 100)));
                                lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(((ActualHalfPrice - PkgDiscountPrice) + (ActualPrice - PkgDiscountPrice2Day)) * Math.Round(CurrencyConvert, 2), 2)));
                            }
                            else if (((Session["masterInput"] as BookingRequest)).propDays == "0" && ((Session["masterInput"] as BookingRequest)).propDay2 != "0")
                            {
                                lblActPkgPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round((ActualPrice + ActualHalfPrice) * Math.Round(CurrencyConvert, 2), 2)));
                                TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(hdnId.Value));
                                decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.FulldayPrice).Sum());
                                decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.HalfdayPrice).Sum());
                                decimal Fulldaypkgprice = (Math.Abs((ActualPrice - getTotleFullDayPrice)));
                                decimal Halfdaypkgprice = (Math.Abs((ActualHalfPrice - getTotleHalfDayPrice)));
                                decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * DiscountPrice / 100)));
                                decimal PkgDiscountPrice2Day = (Math.Abs((Halfdaypkgprice * SecdayDiscountPercent / 100)));
                                lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(((ActualPrice - PkgDiscountPrice) + (ActualHalfPrice - PkgDiscountPrice2Day)) * Math.Round(CurrencyConvert, 2), 2)));
                            }
                            else if (((Session["masterInput"] as BookingRequest)).propDays != "0" && ((Session["masterInput"] as BookingRequest)).propDay2 != "0")
                            {
                                lblActPkgPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round((ActualHalfPrice + ActualHalfPrice) * Math.Round(CurrencyConvert, 2), 2)));
                                TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(hdnId.Value));
                                decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.HalfdayPrice).Sum());
                                decimal Halfdaypkgprice = (Math.Abs((ActualHalfPrice - getTotleHalfDayPrice)));
                                decimal PkgDiscountPrice = (Math.Abs((Halfdaypkgprice * DiscountPrice / 100)));
                                decimal PkgDiscountPrice2Day = (Math.Abs((Halfdaypkgprice * SecdayDiscountPercent / 100)));
                                lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(((ActualHalfPrice - PkgDiscountPrice) + (ActualHalfPrice - PkgDiscountPrice2Day)) * Math.Round(CurrencyConvert, 2), 2)));
                            }

                            //added this formula for checking minimum special discount
                            if (ViewState["MinConsiderSpecial"] != null && (Convert.ToInt32(ViewState["MinConsiderSpecial"]) <= DiscountPrice || Convert.ToInt32(ViewState["MinConsiderSpecial"]) <= SecdayDiscountPercent))
                            {
                                mydiv.Style.Add("display", "none");
                                divPkg.Style.Add("display", "block");
                                lblActPAckagePrice.Text = lblDiscountPrice.Text;
                            }
                            else
                            {
                                mydiv.Style.Add("display", "block");
                                divPkg.Style.Add("display", "none");
                            }
                        }
                        //End Get Price from actualpackageprice by hotelid without discount
                        else if (DiscountPrice == 0 && SecdayDiscountPercent == 0 && ((Session["masterInput"] as BookingRequest)).propDuration == "2")
                        {
                            System.Web.UI.HtmlControls.HtmlGenericControl mydiv = ((System.Web.UI.HtmlControls.HtmlGenericControl)e.Row.FindControl("divPkg"));
                            mydiv.Style.Add("display", "block");
                            if (((Session["masterInput"] as BookingRequest)).propDays == "0" && ((Session["masterInput"] as BookingRequest)).propDay2 == "0")
                            {
                                lblActPAckagePrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round((ActualPrice + ActualPrice) * Math.Round(CurrencyConvert, 2), 2)));
                            }
                            else if (((Session["masterInput"] as BookingRequest)).propDays != "0" && ((Session["masterInput"] as BookingRequest)).propDay2 != "0")
                            {
                                lblActPAckagePrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round((ActualHalfPrice + ActualHalfPrice) * Math.Round(CurrencyConvert, 2), 2)));
                            }
                            else
                            {
                                lblActPAckagePrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round((ActualPrice + ActualHalfPrice) * Math.Round(CurrencyConvert, 2), 2)));
                            }
                            lblDiscountPrice.Text = lblActPAckagePrice.Text;
                        }
                        else if ((DiscountPrice > 0 || SecdayDiscountPercent > 0) && ((Session["masterInput"] as BookingRequest)).propDuration == "2")
                        {
                            System.Web.UI.HtmlControls.HtmlGenericControl mydiv = ((System.Web.UI.HtmlControls.HtmlGenericControl)e.Row.FindControl("divDiscount"));
                            mydiv.Style.Add("display", "block");
                            if (((Session["masterInput"] as BookingRequest)).propDays == "0" && ((Session["masterInput"] as BookingRequest)).propDay2 == "0")
                            {
                                TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(hdnId.Value));
                                decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.FulldayPrice).Sum());
                                decimal Fulldaypkgprice = (Math.Abs((ActualPrice - getTotleFullDayPrice)));
                                decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * DiscountPrice / 100)));
                                decimal PkgDiscountPrice2Day = (Math.Abs((Fulldaypkgprice * SecdayDiscountPercent / 100)));
                                lblActPkgPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round((ActualPrice + ActualPrice) * Math.Round(CurrencyConvert, 2), 2)));
                                if (DiscountPrice > 0 && SecdayDiscountPercent < 0)
                                {
                                    lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(((ActualPrice + PkgDiscountPrice) + (ActualPrice - PkgDiscountPrice2Day)) * Math.Round(CurrencyConvert, 2), 2)));
                                }
                                else if (DiscountPrice > 0 && SecdayDiscountPercent == 0)
                                {
                                    lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(((ActualPrice + PkgDiscountPrice) + (ActualPrice)) * Math.Round(CurrencyConvert, 2), 2)));
                                }
                                else if (DiscountPrice < 0 && SecdayDiscountPercent > 0)
                                {
                                    lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(((ActualPrice - PkgDiscountPrice) + (ActualPrice + PkgDiscountPrice2Day)) * Math.Round(CurrencyConvert, 2), 2)));
                                }
                                else if (DiscountPrice == 0 && SecdayDiscountPercent > 0)
                                {
                                    lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(((ActualPrice) + (ActualPrice + PkgDiscountPrice2Day)) * Math.Round(CurrencyConvert, 2), 2)));
                                }
                                else if (DiscountPrice > 0 && SecdayDiscountPercent > 0)
                                {
                                    lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(((ActualPrice + PkgDiscountPrice) + (ActualPrice + PkgDiscountPrice2Day)) * Math.Round(CurrencyConvert, 2), 2)));
                                }
                                if (Convert.ToDecimal(lblActPkgPrice.Text) <= Convert.ToDecimal(lblDiscountPrice.Text))
                                {
                                    mydiv.Style.Add("display", "none");
                                    System.Web.UI.HtmlControls.HtmlGenericControl mydiv1 = ((System.Web.UI.HtmlControls.HtmlGenericControl)e.Row.FindControl("divPkg"));
                                    mydiv1.Style.Add("display", "block");
                                    lblActPAckagePrice.Text = lblDiscountPrice.Text;
                                }
                            }
                            else if (((Session["masterInput"] as BookingRequest)).propDays != "0" && ((Session["masterInput"] as BookingRequest)).propDay2 == "0")
                            {
                                TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(hdnId.Value));
                                decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.FulldayPrice).Sum());
                                decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.HalfdayPrice).Sum());
                                decimal Fulldaypkgprice = (Math.Abs((ActualPrice - getTotleFullDayPrice)));
                                decimal Halfdaypkgprice = (Math.Abs((ActualHalfPrice - getTotleHalfDayPrice)));
                                decimal PkgDiscountPrice = (Math.Abs((Halfdaypkgprice * DiscountPrice / 100)));
                                decimal PkgDiscountPrice2Day = (Math.Abs((Fulldaypkgprice * SecdayDiscountPercent / 100)));
                                lblActPkgPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round((ActualHalfPrice + ActualPrice) * Math.Round(CurrencyConvert, 2), 2)));
                                if (DiscountPrice > 0 && SecdayDiscountPercent < 0)
                                {
                                    lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(((ActualHalfPrice + PkgDiscountPrice) + (ActualPrice - PkgDiscountPrice2Day)) * Math.Round(CurrencyConvert, 2), 2)));
                                }
                                else if (DiscountPrice > 0 && SecdayDiscountPercent == 0)
                                {
                                    lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(((ActualHalfPrice + PkgDiscountPrice) + (ActualPrice)) * Math.Round(CurrencyConvert, 2), 2)));
                                }
                                else if (DiscountPrice < 0 && SecdayDiscountPercent > 0)
                                {
                                    lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(((ActualHalfPrice - PkgDiscountPrice) + (ActualPrice + PkgDiscountPrice2Day)) * Math.Round(CurrencyConvert, 2), 2)));
                                }
                                else if (DiscountPrice == 0 && SecdayDiscountPercent > 0)
                                {
                                    lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(((ActualHalfPrice) + (ActualPrice + PkgDiscountPrice2Day)) * Math.Round(CurrencyConvert, 2), 2)));
                                }
                                else if (DiscountPrice > 0 && SecdayDiscountPercent > 0)
                                {
                                    lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(((ActualHalfPrice + PkgDiscountPrice) + (ActualPrice + PkgDiscountPrice2Day)) * Math.Round(CurrencyConvert, 2), 2)));
                                }
                                if (Convert.ToDecimal(lblActPkgPrice.Text) <= Convert.ToDecimal(lblDiscountPrice.Text))
                                {
                                    mydiv.Style.Add("display", "none");
                                    System.Web.UI.HtmlControls.HtmlGenericControl mydiv1 = ((System.Web.UI.HtmlControls.HtmlGenericControl)e.Row.FindControl("divPkg"));
                                    mydiv1.Style.Add("display", "block");
                                    lblActPAckagePrice.Text = lblDiscountPrice.Text;
                                }
                            }
                            else if (((Session["masterInput"] as BookingRequest)).propDays == "0" && ((Session["masterInput"] as BookingRequest)).propDay2 != "0")
                            {
                                TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(hdnId.Value));
                                decimal getTotleFullDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.FulldayPrice).Sum());
                                decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.HalfdayPrice).Sum());
                                decimal Fulldaypkgprice = (Math.Abs((ActualPrice - getTotleFullDayPrice)));
                                decimal Halfdaypkgprice = (Math.Abs((ActualHalfPrice - getTotleHalfDayPrice)));
                                decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * DiscountPrice / 100)));
                                decimal PkgDiscountPrice2Day = (Math.Abs((Halfdaypkgprice * SecdayDiscountPercent / 100)));
                                lblActPkgPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round((ActualPrice + ActualHalfPrice) * Math.Round(CurrencyConvert, 2), 2)));
                                if (DiscountPrice > 0 && SecdayDiscountPercent < 0)
                                {
                                    lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(((ActualPrice + PkgDiscountPrice) + (ActualHalfPrice - PkgDiscountPrice2Day)) * Math.Round(CurrencyConvert, 2), 2)));
                                }
                                else if (DiscountPrice > 0 && SecdayDiscountPercent == 0)
                                {
                                    lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(((ActualPrice + PkgDiscountPrice) + (ActualHalfPrice)) * Math.Round(CurrencyConvert, 2), 2)));
                                }
                                else if (DiscountPrice < 0 && SecdayDiscountPercent > 0)
                                {
                                    lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(((ActualPrice - PkgDiscountPrice) + (ActualHalfPrice + PkgDiscountPrice2Day)) * Math.Round(CurrencyConvert, 2), 2)));
                                }
                                else if (DiscountPrice == 0 && SecdayDiscountPercent > 0)
                                {
                                    lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(((ActualPrice) + (ActualHalfPrice + PkgDiscountPrice2Day)) * Math.Round(CurrencyConvert, 2), 2)));
                                }
                                else if (DiscountPrice > 0 && SecdayDiscountPercent > 0)
                                {
                                    lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(((ActualPrice + PkgDiscountPrice) + (ActualHalfPrice + PkgDiscountPrice2Day)) * Math.Round(CurrencyConvert, 2), 2)));
                                }
                                if (Convert.ToDecimal(lblActPkgPrice.Text) <= Convert.ToDecimal(lblDiscountPrice.Text))
                                {
                                    mydiv.Style.Add("display", "none");
                                    System.Web.UI.HtmlControls.HtmlGenericControl mydiv1 = ((System.Web.UI.HtmlControls.HtmlGenericControl)e.Row.FindControl("divPkg"));
                                    mydiv1.Style.Add("display", "block");
                                    lblActPAckagePrice.Text = lblDiscountPrice.Text;
                                }
                            }
                            else if (((Session["masterInput"] as BookingRequest)).propDays != "0" && ((Session["masterInput"] as BookingRequest)).propDay2 != "0")
                            {
                                TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(hdnId.Value));
                                decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.HalfdayPrice).Sum());
                                decimal Halfdaypkgprice = (Math.Abs((ActualHalfPrice - getTotleHalfDayPrice)));
                                decimal PkgDiscountPrice = (Math.Abs((Halfdaypkgprice * DiscountPrice / 100)));
                                decimal PkgDiscountPrice2Day = (Math.Abs((Halfdaypkgprice * SecdayDiscountPercent / 100)));
                                lblActPkgPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round((ActualHalfPrice + ActualHalfPrice) * Math.Round(CurrencyConvert, 2), 2)));
                                if (DiscountPrice > 0 && SecdayDiscountPercent < 0)
                                {
                                    lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(((ActualHalfPrice + PkgDiscountPrice) + (ActualHalfPrice - PkgDiscountPrice2Day)) * Math.Round(CurrencyConvert, 2), 2)));
                                }
                                else if (DiscountPrice > 0 && SecdayDiscountPercent == 0)
                                {
                                    lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(((ActualHalfPrice + PkgDiscountPrice) + (ActualHalfPrice)) * Math.Round(CurrencyConvert, 2), 2)));
                                }
                                else if (DiscountPrice < 0 && SecdayDiscountPercent > 0)
                                {
                                    lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(((ActualHalfPrice - PkgDiscountPrice) + (ActualHalfPrice + PkgDiscountPrice2Day)) * Math.Round(CurrencyConvert, 2), 2)));
                                }
                                else if (DiscountPrice == 0 && SecdayDiscountPercent > 0)
                                {
                                    lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(((ActualHalfPrice) + (ActualHalfPrice + PkgDiscountPrice2Day)) * Math.Round(CurrencyConvert, 2), 2)));
                                }
                                else if (DiscountPrice > 0 && SecdayDiscountPercent > 0)
                                {
                                    lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(((ActualHalfPrice + PkgDiscountPrice) + (ActualHalfPrice + PkgDiscountPrice2Day)) * Math.Round(CurrencyConvert, 2), 2)));
                                }
                                if (Convert.ToDecimal(lblActPkgPrice.Text) <= Convert.ToDecimal(lblDiscountPrice.Text))
                                {
                                    mydiv.Style.Add("display", "none");
                                    System.Web.UI.HtmlControls.HtmlGenericControl mydiv1 = ((System.Web.UI.HtmlControls.HtmlGenericControl)e.Row.FindControl("divPkg"));
                                    mydiv1.Style.Add("display", "block");
                                    lblActPAckagePrice.Text = lblDiscountPrice.Text;
                                }
                            }
                        }
                        #endregion
                    }

                    #region Comment the calculation code to get average price which was done earlier
                    //if (((Session["masterInput"] as BookingRequest)).propDuration == "2")
                    //{
                    //    TList<SpecialPriceAndPromo> Spl = objBookingRequest.GetSpecialPriceandPromoByDate(Convert.ToInt32(hdnId.Value), fromDate.AddDays(1));
                    //    if (Convert.ToDecimal(Spl[0].DdrPercent) > 0)
                    //    {
                    //        lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Abs(Convert.ToDecimal(lblDiscountPrice.Text) / 2)));
                    //        return;
                    //    }

                    //    decimal day2Perc = (Math.Abs((Convert.ToDecimal(lblActPkgPrice.Text) * Convert.ToDecimal(Spl[0].DdrPercent) / 100)));
                    //    decimal average = (Convert.ToDecimal(lblDiscountPrice.Text) + day2Perc) / 2;
                    //    lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", average);
                    //////FYI
                    //lblActPkgPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Abs(ActualHalfPrice)));
                    //TList<PackageByHotel> lstPackagePrice = objPackagePricingManager.GetAllPackagePrice(Convert.ToInt32(hdnId.Value));
                    //decimal getTotleHalfDayPrice = Convert.ToDecimal(lstPackagePrice.Where(u => Convert.ToInt64(u.PackageId) == pkgmaster[0].Id).Select(u => u.HalfdayPrice).Sum());
                    //decimal Fulldaypkgprice = (Math.Abs((ActualHalfPrice - getTotleHalfDayPrice)));
                    //decimal PkgDiscountPrice = (Math.Abs((Fulldaypkgprice * DiscountPrice / 100)));
                    //lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (ActualHalfPrice - PkgDiscountPrice));
                    //}
                    ////if (Convert.ToDecimal(lblDiscountPrice.Text) < 0)
                    ////{
                    ////    #region Currency Conversion
                    ////    Currency objUserCurrency = cm.GetCurrencyDetailsByID(Convert.ToInt64(Session["CurrencyID"]));
                    ////    UserCurrency = objUserCurrency.Currency;
                    ////    CurrencySign = objUserCurrency.CurrencySignature;                        
                    ////    Currency objcurrency = cm.GetCurrencyDetailsByID(htl.CurrencyId);
                    ////    HotelCurrency = objcurrency.Currency;
                    ////    string currency = CurrencyManager.Currency(HotelCurrency, UserCurrency);
                    ////    CurrencyConvert = Convert.ToDecimal(currency == "N/A" ? "1" : currency);
                    ////    #endregion

                    ////    System.Web.UI.HtmlControls.HtmlGenericControl mydiv = ((System.Web.UI.HtmlControls.HtmlGenericControl)e.Row.FindControl("divDiscount"));
                    ////    mydiv.Style.Add("display", "block");
                    ////    lblActPkgPrice.Text = String.Format("{0:#,##,##0.0}", objBookingRequest.GetActualpkgPricebyHotelid(Convert.ToInt32(hdnId.Value))[0].ActualFullDayPrice * CurrencyConvert);
                    ////    lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Abs((Convert.ToDecimal(lblActPkgPrice.Text) * Convert.ToDecimal(lblDiscountPrice.Text) / 100) * CurrencyConvert)));

                    ////    if (((Session["masterInput"] as BookingRequest)).propDuration == "2")
                    ////    {
                    ////        TList<SpecialPriceAndPromo> Spl = objBookingRequest.GetSpecialPriceandPromoByDate(Convert.ToInt32(hdnId.Value), fromDate.AddDays(1));
                    ////        if (Convert.ToDecimal(Spl[0].DdrPercent) > 0)
                    ////        {
                    ////            //lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", (Math.Abs((Convert.ToDecimal(lblDiscountPrice.Text) / 2) * CurrencyConvert)));
                    ////            lblDiscountPrice.Text = Math.Round((Convert.ToDecimal(lblDiscountPrice.Text) / 2) * CurrencyConvert, 2).ToString();                                
                    ////            return;
                    ////        }

                    ////        //decimal day2Perc = (Math.Abs((Convert.ToDecimal(lblActPkgPrice.Text) * Convert.ToDecimal(Spl[0].DdrPercent) / 100) * CurrencyConvert));
                    ////        decimal day2Perc =Convert.ToDecimal( Math.Round((Convert.ToDecimal(lblActPkgPrice.Text) * Convert.ToDecimal(Spl[0].DdrPercent) / 100) * CurrencyConvert, 2).ToString());
                    ////        //decimal average = (Convert.ToDecimal((lblDiscountPrice.Text) + day2Perc) / 2) * CurrencyConvert; 
                    ////        decimal average = Convert.ToDecimal(Math.Round((Convert.ToDecimal((lblDiscountPrice.Text) + day2Perc) / 2) * CurrencyConvert, 2).ToString());
                    ////        //lblDiscountPrice.Text = String.Format("{0:#,##,##0.0}", average);
                    ////        lblDiscountPrice.Text = Math.Round(average, 2).ToString();
                    ////    }
                    ////}
                    //////Get Price from actualpackageprice by hotelid without discount
                    ////else
                    ////{
                    ////    #region Currency Conversion
                    ////    Currency objUserCurrency = cm.GetCurrencyDetailsByID(Convert.ToInt64(Session["CurrencyID"]));
                    ////    UserCurrency = objUserCurrency.Currency;
                    ////    CurrencySign = objUserCurrency.CurrencySignature;
                    ////    Currency objcurrency = cm.GetCurrencyDetailsByID(htl.CurrencyId);
                    ////    HotelCurrency = objcurrency.Currency;
                    ////    string currency = CurrencyManager.Currency(HotelCurrency, UserCurrency);
                    ////    CurrencyConvert = Convert.ToDecimal(currency == "N/A" ? "1" : currency);
                    ////    #endregion

                    ////    System.Web.UI.HtmlControls.HtmlGenericControl mydiv = ((System.Web.UI.HtmlControls.HtmlGenericControl)e.Row.FindControl("divPkg"));
                    ////    mydiv.Style.Add("display", "block");
                    ////    lblActPAckagePrice.Text = String.Format("{0:#,##,##0.0}", objBookingRequest.GetActualpkgPricebyHotelid(Convert.ToInt32(hdnId.Value))[0].ActualFullDayPrice * CurrencyConvert);
                    ////}
                    #endregion

                    #region Get Facility images, Navigate to Facility icon
                    HyperLink hypClock = (HyperLink)e.Row.FindControl("hypClock");
                    hypClock.Attributes.Add("onclick", string.Format("NavigateFacility('{0}')", hdnId.Value));
                    HyperLink hypDateIcon = (HyperLink)e.Row.FindControl("hypDateIcon");
                    hypDateIcon.Attributes.Add("onclick", string.Format("NavigateFacility('{0}')", hdnId.Value));
                    HyperLink hyplockicon = (HyperLink)e.Row.FindControl("hyplockicon");
                    hyplockicon.Attributes.Add("onclick", string.Format("NavigateFacility('{0}')", hdnId.Value));
                    HyperLink hypPrinticon = (HyperLink)e.Row.FindControl("hypPrinticon");
                    hypPrinticon.Attributes.Add("onclick", string.Format("NavigateFacility('{0}')", hdnId.Value));
                    HyperLink hypchticon = (HyperLink)e.Row.FindControl("hypchticon");
                    hypchticon.Attributes.Add("onclick", string.Format("NavigateFacility('{0}')", hdnId.Value));
                    HyperLink hypDedicated = (HyperLink)e.Row.FindControl("hypDedicated");
                    hypDedicated.Attributes.Add("onclick", string.Format("NavigateFacility('{0}')", hdnId.Value));

                    //string whereclaus = "Hotel_Id=" + hdnId.Value + " and Icon='private-parking.png'";
                    //lblMeetingroomOnline.Text = Convert.ToString(BookingMeetingRoom.Where(a => a.HotelId == Convert.ToInt64(hdnId.Value)).Count());
                    //vicon = ViewFacIcon.Where(a => a.HotelId == Convert.ToInt64(hdnId.Value)).Count());
                    if (ViewFacIcon.Where(a => a.HotelId == Convert.ToInt64(hdnId.Value) && a.Icon == "private-parking.png").Count() > 0)
                    {
                        hypPrinticon.Visible = true;
                    }

                    //string whereclaus1 = "Hotel_Id=" + hdnId.Value + " and Icon='bar-resto.png'";
                    //vicon = objFacility.GetFacilityIcon(whereclaus1, String.Empty);
                    if (ViewFacIcon.Where(a => a.HotelId == Convert.ToInt64(hdnId.Value) && a.Icon == "bar-resto.png").Count() > 0)
                    {
                        hypClock.Visible = true;
                    }

                    //string whereclaus2 = "Hotel_Id=" + hdnId.Value + " and Icon='wifi.png'";
                    //vicon = objFacility.GetFacilityIcon(whereclaus2, String.Empty);
                    if (ViewFacIcon.Where(a => a.HotelId == Convert.ToInt64(hdnId.Value) && a.Icon == "wifi.png").Count() > 0)
                    {
                        hypchticon.Visible = true;
                    }

                    //string whereclaus3 = "Hotel_Id=" + hdnId.Value + " and Icon='daylight-in-all-meeting-rooms.png'";
                    //vicon = objFacility.GetFacilityIcon(whereclaus3, String.Empty);
                    if (ViewFacIcon.Where(a => a.HotelId == Convert.ToInt64(hdnId.Value) && a.Icon == "daylight-in-all-meeting-rooms.png").Count() > 0)
                    {
                        hypDateIcon.Visible = true;
                    }

                    //string whereDedicated = "Hotel_Id=" + hdnId.Value + " and Icon='meeting.png'";
                    //vicon = objFacility.GetFacilityIcon(whereDedicated, String.Empty);
                    if (ViewFacIcon.Where(a => a.HotelId == Convert.ToInt64(hdnId.Value) && a.Icon == "meeting.png").Count() > 0)
                    {
                        hypDedicated.Visible = true;
                    }

                    string whereclaus4 = "Id=" + hdnId.Value + " and " + "IsBedroomAvailable=1";
                    TList<Hotel> lsthLink = ObjHotelinfo.GetHotelbyCondition(whereclaus4, String.Empty);
                    if (lsthLink.Count > 0)
                    {
                        hyplockicon.Visible = true;
                    }
                    #endregion

                    #region Start bind from airport
                    if (drpDistanceBooking.SelectedIndex != 0 && drpDistanceBooking.SelectedIndex != -1)
                    {
                        int selectedArea = Convert.ToInt32(drpDistanceBooking.SelectedItem.Value);
                        CoOrdiantes selectedarea = new CoOrdiantes();
                        DistanceCalculator calculator = new DistanceCalculator();
                        selectedarea.Latitude = Convert.ToDouble(ObjHotelinfo.getById(selectedArea).Latitude);
                        selectedarea.Longitude = Convert.ToDouble(ObjHotelinfo.getById(selectedArea).Longitude);

                        Label lblAirport = (Label)e.Row.FindControl("lblAirport");
                        double distance;

                        CoOrdiantes hotelCord = new CoOrdiantes();
                        hotelCord.Latitude = Convert.ToDouble(brvl.Latitude);
                        hotelCord.Longitude = Convert.ToDouble(brvl.Longitude);
                        distance = calculator.FindDistance(hotelCord, selectedarea, DistanceUnit.Kilometers);
                        lblAirport.Text = Math.Round(distance, 2).ToString() + " KM";
                    }
                    #endregion

                    #region Review servey response
                    TList<ServeyResponse> objResponse = objBookingRequest.GetServeyResponse(Convert.ToInt32(brvl.HotelId));
                    if (objResponse.Count > 0)
                    {
                        string whereSurvey = ServeyResponseColumn.ServeyId + " in (";
                        foreach (ServeyResponse cl in objResponse)
                        {
                            whereSurvey += cl.ServeyId + ",";
                        }
                        if (whereSurvey.Length > 0)
                        {
                            whereSurvey = whereSurvey.Substring(0, whereSurvey.Length - 1);
                        }
                        whereSurvey += ")";

                        TList<Serveyresult> objResult = objBookingRequest.GetServeyresult(whereSurvey);
                        if (objResult.Count > 0)
                        {
                            string result = Convert.ToString(objResult.Select(u => u.Rating).Sum());
                            string FinalReview = String.Format("{0:#,##,##0.0}", Convert.ToInt32(result) / objResult.FindAllDistinct(ServeyresultColumn.ServeyId).Count);
                            lblReview.Text = String.Format("{0:#,##,##0.0}", Convert.ToDecimal(Convert.ToDecimal(FinalReview) / 10));
                        }
                    }
                    #endregion
                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
        //ScriptManager.RegisterStartupScript(this, typeof(Page), "ShowRequest", "ShowRequestOnDemand('booking');", true);
    }


    //Get Hotel details and show for Request gridview
    protected void grvRequest_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (Session["masterInput"] != null)
            {
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
                {
                    BookingRequestViewList brvl = e.Row.DataItem as BookingRequestViewList;
                    HiddenField hdnId = (HiddenField)e.Row.FindControl("hdnHotelId");
                    Label lblAddress = (Label)e.Row.FindControl("lblAddress");
                    Label lblDescription = (Label)e.Row.FindControl("lblDescription");
                    Label lblMeetingroomOnline = (Label)e.Row.FindControl("lblMeetingroomOnline");
                    Label lblReview = (Label)e.Row.FindControl("lblReview");
                    Image imgStar = (Image)e.Row.FindControl("imgStar");
                    Image imgHotel = (Image)e.Row.FindControl("imgHotel");
                    Hotel htl = ObjHotelinfo.GetHotelByHotelID(Convert.ToInt32(hdnId.Value));
                    HyperLink hypPhoto = (HyperLink)e.Row.FindControl("hypPhoto");
                    hypPhoto.NavigateUrl = "/SlideShowPopup.aspx?id=" + htl.Id;
                    hypPhoto.Attributes.Add("onclick", "return Navigate2(" + htl.Id + ");");
                    string whereclausedesc = "Hotel_Id=" + htl.Id + " and " + "Language_Id=" + Session["LanguageID"] + "";
                    TList<HotelDesc> lsthdesc = ObjHotelinfo.GetHotelDesc(whereclausedesc, String.Empty);
                    if (lsthdesc.Count > 0)
                    {
                        lblDescription.Text = (lsthdesc[0].Description == null ? "" : lsthdesc[0].Description);
                        lblAddress.Text = (lsthdesc[0].Address == null ? "" : lsthdesc[0].Address);
                    }
                    else
                    {
                        int englishID = Convert.ToInt32(ObjHotelinfo.GetAllRequiredLanguage().Find(a => a.Name == "English").Id);
                        string wherecls = "Hotel_Id=" + htl.Id + " and " + "Language_Id=" + englishID + "";
                        TList<HotelDesc> lstEng = ObjHotelinfo.GetHotelDesc(wherecls, String.Empty);
                        lblDescription.Text = (lstEng[0].Description == null ? "" : lstEng[0].Description);
                        lblAddress.Text = (lstEng[0].Address == null ? "" : lstEng[0].Address);
                    }


                    #region for static hotel star
                    if (htl.Stars == 1)
                    {
                        imgStar.ImageUrl = "~/Images/1.png";
                    }
                    else if (htl.Stars == 2)
                    {
                        imgStar.ImageUrl = "~/Images/2.png";
                    }
                    else if (htl.Stars == 3)
                    {
                        imgStar.ImageUrl = "~/Images/3.png";
                    }
                    else if (htl.Stars == 4)
                    {
                        imgStar.ImageUrl = "~/Images/4.png";
                    }
                    else if (htl.Stars == 5)
                    {
                        imgStar.ImageUrl = "~/Images/5.png";
                    }
                    else if (htl.Stars == 6)
                    {
                        imgStar.ImageUrl = "~/Images/6.png";
                    }
                    else if (htl.Stars == 7)
                    {
                        imgStar.ImageUrl = "~/Images/7.png";
                    }
                    else if (htl.Stars == 0)
                    {
                        imgStar.Visible = false;
                    }
                    #endregion

                    if (htl.Logo != null)
                    {
                        imgHotel.ImageUrl = ConfigurationManager.AppSettings["FilePath"] + "/HotelImage/" + htl.Logo;
                    }

                    TList<HotelPhotoVideoGallary> ObjHotelImageAltertext = objBookingRequest.GetAlternateTextPictureVideo(Convert.ToInt32(htl.Id));
                    if (ObjHotelImageAltertext.Count > 0)
                    {
                        imgHotel.ToolTip = ObjHotelImageAltertext[0].AlterText;
                    }

                    System.Web.UI.HtmlControls.HtmlGenericControl divVideo = ((System.Web.UI.HtmlControls.HtmlGenericControl)e.Row.FindControl("divVideo"));
                    TList<HotelPhotoVideoGallary> ObjHotelImageVideo = objBookingRequest.GetAllHotelPictureVideo(Convert.ToInt32(htl.Id));
                    if (ObjHotelImageVideo.Count == 0)
                    {
                        divVideo.Visible = false;
                    }
                    LinkButton button = (LinkButton)e.Row.FindControl("lnbVideo");
                    button.Attributes.Add("onclick", string.Format("Navigate('{0}')", hdnId.Value));

                    //Show count number for online request                                      
                    //string whereClauses = "HotelId='" + htl.Id + "'";
                    //lblMeetingroomOnline.Text = Convert.ToString(Convert.ToInt32(objBookingRequest.GetReqDetails(whereClauses, string.Empty).FindAllDistinct(ViewForRequestSearchColumn.MeetingRoomId).Count));
                    lblMeetingroomOnline.Text = Convert.ToString(RequestMeetingRoom.Where(a => a.HotelId == Convert.ToInt64(hdnId.Value)).Count());
                    //Show count number for online request

                    Label lblActPkgPrice = (Label)e.Row.FindControl("lblActPkgPrice");
                    Label lblActPAckagePrice = (Label)e.Row.FindControl("lblActPAckagePrice");
                    Label lblDiscountPrice = (Label)e.Row.FindControl("lblDiscountPrice");
                    HiddenField hdnPkgPrice = (HiddenField)e.Row.FindControl("hdnPkgPrice");
                    HiddenField hdnPkgHalfPrice = (HiddenField)e.Row.FindControl("hdnPkgHalfPrice");
                    decimal ActualPrice = Convert.ToDecimal(hdnPkgPrice.Value);
                    decimal ActualHalfPrice = Convert.ToDecimal(hdnPkgHalfPrice.Value);

                    DateTime fromDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));

                    #region Currency set
                    Label lblCurrencySign = (Label)e.Row.FindControl("lblCurrencySign");
                    lblCurrencySign.Text = CurrencySign;
                    #endregion

                    #region Get Price from actualpackageprice by hotelid for 1 Day
                    if (((Session["masterInput"] as BookingRequest)).propDuration == "1")
                    {
                        if (((Session["masterInput"] as BookingRequest)).propDays == "0")
                        {
                            lblActPAckagePrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(Convert.ToDecimal(hdnPkgPrice.Value) * Math.Round(CurrencyConvert, 2), 2)));
                        }
                        else if (((Session["masterInput"] as BookingRequest)).propDays != "0")
                        {
                            lblActPAckagePrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round(Convert.ToDecimal(hdnPkgHalfPrice.Value) * Math.Round(CurrencyConvert, 2), 2)));
                        }
                    }
                    #endregion

                    #region Get Price from actualpackageprice by hotelid for 2 Day
                    if (((Session["masterInput"] as BookingRequest)).propDuration == "2")
                    {
                        if (((Session["masterInput"] as BookingRequest)).propDays == "0" && ((Session["masterInput"] as BookingRequest)).propDay2 == "0")
                        {
                            lblActPAckagePrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round((ActualPrice + ActualPrice) * Math.Round(CurrencyConvert, 2), 2)));
                        }
                        else if (((Session["masterInput"] as BookingRequest)).propDays != "0" && ((Session["masterInput"] as BookingRequest)).propDay2 != "0")
                        {
                            lblActPAckagePrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round((ActualHalfPrice + ActualHalfPrice) * Math.Round(CurrencyConvert, 2), 2)));
                        }
                        else
                        {
                            lblActPAckagePrice.Text = String.Format("{0:#,##,##0.0}", (Math.Round((ActualPrice + ActualHalfPrice) * Math.Round(CurrencyConvert, 2), 2)));
                        }
                    }
                    #endregion

                    //Get Facility images
                    //Navigate to Facility icon
                    HyperLink hypClock = (HyperLink)e.Row.FindControl("hypClock");
                    hypClock.Attributes.Add("onclick", string.Format("NavigateFacility('{0}')", hdnId.Value));
                    HyperLink hypDateIcon = (HyperLink)e.Row.FindControl("hypDateIcon");
                    hypDateIcon.Attributes.Add("onclick", string.Format("NavigateFacility('{0}')", hdnId.Value));
                    HyperLink hyplockicon = (HyperLink)e.Row.FindControl("hyplockicon");
                    hyplockicon.Attributes.Add("onclick", string.Format("NavigateFacility('{0}')", hdnId.Value));
                    HyperLink hypPrinticon = (HyperLink)e.Row.FindControl("hypPrinticon");
                    hypPrinticon.Attributes.Add("onclick", string.Format("NavigateFacility('{0}')", hdnId.Value));
                    HyperLink hypchticon = (HyperLink)e.Row.FindControl("hypchticon");
                    hypchticon.Attributes.Add("onclick", string.Format("NavigateFacility('{0}')", hdnId.Value));
                    HyperLink hypDedicated = (HyperLink)e.Row.FindControl("hypDedicated");
                    hypDedicated.Attributes.Add("onclick", string.Format("NavigateFacility('{0}')", hdnId.Value));


                    //string whereclaus = "Hotel_Id=" + hdnId.Value + " and Icon='private-parking.png'";                    
                    //vicon = ViewFacIcon.Where(a => a.HotelId == Convert.ToInt64(hdnId.Value)).Count());
                    if (ViewFacIcon.Where(a => a.HotelId == Convert.ToInt64(hdnId.Value) && a.Icon == "private-parking.png").Count() > 0)
                    {
                        hypPrinticon.Visible = true;
                    }

                    //string whereclaus1 = "Hotel_Id=" + hdnId.Value + " and Icon='bar-resto.png'";
                    //vicon = objFacility.GetFacilityIcon(whereclaus1, String.Empty);
                    if (ViewFacIcon.Where(a => a.HotelId == Convert.ToInt64(hdnId.Value) && a.Icon == "bar-resto.png").Count() > 0)
                    {
                        hypClock.Visible = true;
                    }

                    //string whereclaus2 = "Hotel_Id=" + hdnId.Value + " and Icon='wifi.png'";
                    //vicon = objFacility.GetFacilityIcon(whereclaus2, String.Empty);
                    if (ViewFacIcon.Where(a => a.HotelId == Convert.ToInt64(hdnId.Value) && a.Icon == "wifi.png").Count() > 0)
                    {
                        hypchticon.Visible = true;
                    }

                    //string whereclaus3 = "Hotel_Id=" + hdnId.Value + " and Icon='daylight-in-all-meeting-rooms.png'";
                    //vicon = objFacility.GetFacilityIcon(whereclaus3, String.Empty);
                    if (ViewFacIcon.Where(a => a.HotelId == Convert.ToInt64(hdnId.Value) && a.Icon == "daylight-in-all-meeting-rooms.png").Count() > 0)
                    {
                        hypDateIcon.Visible = true;
                    }

                    //string whereDedicated = "Hotel_Id=" + hdnId.Value + " and Icon='meeting.png'";
                    //vicon = objFacility.GetFacilityIcon(whereDedicated, String.Empty);
                    if (ViewFacIcon.Where(a => a.HotelId == Convert.ToInt64(hdnId.Value) && a.Icon == "meeting.png").Count() > 0)
                    {
                        hypDedicated.Visible = true;
                    }

                    string whereclaus4 = "Id=" + hdnId.Value + " and " + "IsBedroomAvailable=1";
                    TList<Hotel> lsthLink = ObjHotelinfo.GetHotelbyCondition(whereclaus4, String.Empty);
                    if (lsthLink.Count > 0)
                    {
                        hyplockicon.Visible = true;
                    }
                    //End Facility Images      

                    ////Start bind from airport  
                    if (drpDistanceRequest.SelectedIndex != 0 && drpDistanceRequest.SelectedIndex != -1)
                    {
                        int selectedArea = Convert.ToInt32(drpDistanceRequest.SelectedItem.Value);
                        CoOrdiantes selectedarea = new CoOrdiantes();
                        DistanceCalculator calculator = new DistanceCalculator();
                        selectedarea.Latitude = Convert.ToDouble(ObjHotelinfo.getById(selectedArea).Latitude);
                        selectedarea.Longitude = Convert.ToDouble(ObjHotelinfo.getById(selectedArea).Longitude);

                        Label lblAirport = (Label)e.Row.FindControl("lblAirport");
                        double distance;

                        CoOrdiantes hotelCord = new CoOrdiantes();
                        hotelCord.Latitude = Convert.ToDouble(htl.Latitude);
                        hotelCord.Longitude = Convert.ToDouble(htl.Longitude);
                        distance = calculator.FindDistance(hotelCord, selectedarea, DistanceUnit.Kilometers);
                        lblAirport.Text = Math.Round(distance, 2).ToString() + " KM";
                    }
                    ////end bind from airport

                    #region Review servey response
                    TList<ServeyResponse> objResponse = objBookingRequest.GetServeyResponse(Convert.ToInt32(htl.Id));
                    if (objResponse.Count > 0)
                    {
                        string whereSurvey = ServeyResponseColumn.ServeyId + " in (";
                        foreach (ServeyResponse cl in objResponse)
                        {
                            whereSurvey += cl.ServeyId + ",";
                        }
                        if (whereSurvey.Length > 0)
                        {
                            whereSurvey = whereSurvey.Substring(0, whereSurvey.Length - 1);
                        }
                        whereSurvey += ")";

                        TList<Serveyresult> objResult = objBookingRequest.GetServeyresult(whereSurvey);
                        if (objResult.Count > 0)
                        {
                            string result = Convert.ToString(objResult.Select(u => u.Rating).Sum());
                            string FinalReview = String.Format("{0:#,##,##0.0}", Convert.ToInt32(result) / objResult.FindAllDistinct(ServeyresultColumn.ServeyId).Count);
                            lblReview.Text = String.Format("{0:#,##,##0.0}", Convert.ToDecimal(Convert.ToDecimal(FinalReview) / 10));
                        }
                    }
                    #endregion
                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
        //ScriptManager.RegisterStartupScript(this, typeof(Page), "ShowRequest", "ShowRequestOnDemand('booking');", true);
    }
    #endregion

    #region Apply Paging
    //Apply paging for Booking gridview
    public void ApplyPaging()
    {
        GridViewRow row = grvBooking.TopPagerRow;
        GridViewRow rowBottom = grvBooking.BottomPagerRow;
        if (row != null)
        {
            PlaceHolder ph;
            LinkButton lnkPaging;
            LinkButton lnkPrevPage;
            LinkButton lnkNextPage;
            lnkPrevPage = new LinkButton();
            lnkPrevPage.CssClass = "pre";
            lnkPrevPage.Width = Unit.Pixel(73);
            lnkPrevPage.CommandName = "Page";
            lnkPrevPage.CommandArgument = "prev";
            ph = (PlaceHolder)row.FindControl("ph");
            ph.Controls.Add(lnkPrevPage);
            if (grvBooking.PageIndex == 0)
            {
                lnkPrevPage.Enabled = false;

            }
            for (int i = 1; i <= grvBooking.PageCount; i++)
            {
                lnkPaging = new LinkButton();
                if (ViewState["CurrentPage"] != null)
                {
                    if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                    {
                        lnkPaging.CssClass = "pag2";
                    }
                    else
                    {
                        lnkPaging.CssClass = "pag";
                    }
                }
                else
                {
                    if (i == 1)
                    {
                        lnkPaging.CssClass = "pag2";
                    }
                    else
                    {
                        lnkPaging.CssClass = "pag";
                    }
                }
                lnkPaging.Width = Unit.Pixel(16);
                lnkPaging.Text = i.ToString();
                lnkPaging.CommandName = "Page";
                lnkPaging.CommandArgument = i.ToString();
                if (i == grvBooking.PageIndex + 1)
                    ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPaging);
            }
            lnkNextPage = new LinkButton();
            lnkNextPage.CssClass = "nex";
            lnkNextPage.Width = Unit.Pixel(42);
            lnkNextPage.CommandName = "Page";
            lnkNextPage.CommandArgument = "next";
            ph = (PlaceHolder)row.FindControl("ph");
            ph.Controls.Add(lnkNextPage);
            ph = (PlaceHolder)row.FindControl("ph");
            if (grvBooking.PageIndex == grvBooking.PageCount - 1)
            {
                lnkNextPage.Enabled = false;

            }
        }

        if (rowBottom != null)
        {
            PlaceHolder phBot;
            LinkButton lnkPaging;
            LinkButton lnkPrevPage;
            LinkButton lnkNextPage;
            lnkPrevPage = new LinkButton();
            lnkPrevPage.CssClass = "pre";
            lnkPrevPage.Width = Unit.Pixel(73);
            lnkPrevPage.CommandName = "Page";
            lnkPrevPage.CommandArgument = "prev";
            phBot = (PlaceHolder)rowBottom.FindControl("phBot");
            phBot.Controls.Add(lnkPrevPage);
            if (grvBooking.PageIndex == 0)
            {
                lnkPrevPage.Enabled = false;

            }
            for (int i = 1; i <= grvBooking.PageCount; i++)
            {
                lnkPaging = new LinkButton();
                if (ViewState["CurrentPage"] != null)
                {
                    if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                    {
                        lnkPaging.CssClass = "pag2";
                    }
                    else
                    {
                        lnkPaging.CssClass = "pag";
                    }
                }
                else
                {
                    if (i == 1)
                    {
                        lnkPaging.CssClass = "pag2";
                    }
                    else
                    {
                        lnkPaging.CssClass = "pag";
                    }
                }
                lnkPaging.Width = Unit.Pixel(16);
                lnkPaging.Text = i.ToString();
                lnkPaging.CommandName = "Page";
                lnkPaging.CommandArgument = i.ToString();
                if (i == grvBooking.PageIndex + 1)
                    phBot = (PlaceHolder)rowBottom.FindControl("phBot");
                phBot.Controls.Add(lnkPaging);
            }
            lnkNextPage = new LinkButton();
            lnkNextPage.CssClass = "nex";
            lnkNextPage.Width = Unit.Pixel(42);
            lnkNextPage.CommandName = "Page";
            lnkNextPage.CommandArgument = "next";
            phBot = (PlaceHolder)rowBottom.FindControl("phBot");
            phBot.Controls.Add(lnkNextPage);
            phBot = (PlaceHolder)rowBottom.FindControl("phBot");
            if (grvBooking.PageIndex == grvBooking.PageCount - 1)
            {
                lnkNextPage.Enabled = false;

            }
        }

    }

    public void ApplyPaginOnrequest()
    {
        GridViewRow row = grvRequest.TopPagerRow;
        GridViewRow rowBottom = grvRequest.BottomPagerRow;
        if (row != null)
        {
            PlaceHolder phRequest;
            LinkButton lnkPaging;
            LinkButton lnkPrevPage;
            LinkButton lnkNextPage;
            lnkPrevPage = new LinkButton();
            lnkPrevPage.CssClass = "pre";
            lnkPrevPage.Width = Unit.Pixel(73);
            lnkPrevPage.CommandName = "Page";
            lnkPrevPage.CommandArgument = "prev";
            phRequest = (PlaceHolder)row.FindControl("phRequest");
            phRequest.Controls.Add(lnkPrevPage);
            if (grvRequest.PageIndex == 0)
            {
                lnkPrevPage.Enabled = false;

            }
            for (int i = 1; i <= grvRequest.PageCount; i++)
            {
                lnkPaging = new LinkButton();
                if (ViewState["CurrentPageRequest"] != null)
                {
                    if (Convert.ToInt32(ViewState["CurrentPageRequest"]) + 1 == i)
                    {
                        lnkPaging.CssClass = "pag2";
                    }
                    else
                    {
                        lnkPaging.CssClass = "pag";
                    }
                }
                else
                {
                    if (i == 1)
                    {
                        lnkPaging.CssClass = "pag2";
                    }
                    else
                    {
                        lnkPaging.CssClass = "pag";
                    }
                }
                lnkPaging.Width = Unit.Pixel(16);
                lnkPaging.Text = i.ToString();
                lnkPaging.CommandName = "Page";
                lnkPaging.CommandArgument = i.ToString();
                if (i == grvRequest.PageIndex + 1)
                    phRequest = (PlaceHolder)row.FindControl("phRequest");
                phRequest.Controls.Add(lnkPaging);
            }
            lnkNextPage = new LinkButton();
            lnkNextPage.CssClass = "nex";
            lnkNextPage.Width = Unit.Pixel(42);
            lnkNextPage.CommandName = "Page";
            lnkNextPage.CommandArgument = "next";
            phRequest = (PlaceHolder)row.FindControl("phRequest");
            phRequest.Controls.Add(lnkNextPage);
            phRequest = (PlaceHolder)row.FindControl("phRequest");
            if (grvRequest.PageIndex == grvRequest.PageCount - 1)
            {
                lnkNextPage.Enabled = false;

            }
        }
        if (rowBottom != null)
        {
            PlaceHolder phBottom;
            LinkButton lnkPaging;
            LinkButton lnkPrevPage;
            LinkButton lnkNextPage;
            lnkPrevPage = new LinkButton();
            lnkPrevPage.CssClass = "pre";
            lnkPrevPage.Width = Unit.Pixel(73);
            lnkPrevPage.CommandName = "Page";
            lnkPrevPage.CommandArgument = "prev";
            phBottom = (PlaceHolder)rowBottom.FindControl("phBottom");
            phBottom.Controls.Add(lnkPrevPage);
            if (grvRequest.PageIndex == 0)
            {
                lnkPrevPage.Enabled = false;

            }
            for (int i = 1; i <= grvRequest.PageCount; i++)
            {
                lnkPaging = new LinkButton();
                if (ViewState["CurrentPageRequest"] != null)
                {
                    if (Convert.ToInt32(ViewState["CurrentPageRequest"]) + 1 == i)
                    {
                        lnkPaging.CssClass = "pag2";
                    }
                    else
                    {
                        lnkPaging.CssClass = "pag";
                    }
                }
                else
                {
                    if (i == 1)
                    {
                        lnkPaging.CssClass = "pag2";
                    }
                    else
                    {
                        lnkPaging.CssClass = "pag";
                    }
                }
                lnkPaging.Width = Unit.Pixel(16);
                lnkPaging.Text = i.ToString();
                lnkPaging.CommandName = "Page";
                lnkPaging.CommandArgument = i.ToString();
                if (i == grvRequest.PageIndex + 1)
                    phBottom = (PlaceHolder)rowBottom.FindControl("phBottom");
                phBottom.Controls.Add(lnkPaging);
            }
            lnkNextPage = new LinkButton();
            lnkNextPage.CssClass = "nex";
            lnkNextPage.Width = Unit.Pixel(42);
            lnkNextPage.CommandName = "Page";
            lnkNextPage.CommandArgument = "next";
            phBottom = (PlaceHolder)rowBottom.FindControl("phBottom");
            phBottom.Controls.Add(lnkNextPage);
            phBottom = (PlaceHolder)rowBottom.FindControl("phBottom");
            if (grvRequest.PageIndex == grvRequest.PageCount - 1)
            {
                lnkNextPage.Enabled = false;

            }
        }
    }
    #endregion

    #region Pageindex for Booking Gridview
    //This event is used to maintain paging for booking
    protected void grvBooking_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grvBooking.PageIndex = e.NewPageIndex;
        ViewState["CurrentPage"] = e.NewPageIndex;
        if (Session["SessionOrderBy"] != null)
        {
            BindBookingMeetingroom();
            grvBooking.DataSource = Session["SessionOrderBy"];
            grvBooking.DataBind();
            ApplyPaging();
            if (ViewState["RequestPaging"] != null)
            {
                FogDisableBooking();
            }
        }
        else
        {
            BindSearchResult();
        }
    }

    protected void grvRequest_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grvRequest.PageIndex = e.NewPageIndex;
        ViewState["CurrentPageRequest"] = e.NewPageIndex;
        if (Convert.ToString(Session["Requestedhotel"]) != "")
        {
            objRequestedHotel = (TList<Hotel>)Session["Requestedhotel"];
        }

        if (Session["SessionOrderByRequest"] != null)
        {
            BindRequestMeetingroom();
            grvRequest.DataSource = Session["SessionOrderByRequest"];
            grvRequest.DataBind();
            ApplyPaginOnrequest();
            if (ViewState["RequestPagingBasket"] != null)
            {
                FogDisableBooking();
                FogDisableRequest();
            }
        }
        else
        {
            BindResultforRequest();
        }
    }
    #endregion

    //#region Event Rendor
    //public override void VerifyRenderingInServerForm(Control control)
    //{
    //    /* Verifies that the control is rendered */
    //}
    //#endregion

    #region Bind Meetingroom for Booking and Request after click on hotel
    //Bind Meetingroom for booking and request after clicking see all
    protected void grvBooking_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            Control commandSource = e.CommandSource as Control;
            if (ViewState["LinkButtonClick"] == null)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () { focusRequiredElement('" + commandSource.ClientID + "');});", true);
            }

            ViewState["LinkButtonClick"] = null;

            if (Session["masterInput"] != null)
            {
                string days = ((Session["masterInput"] as BookingRequest)).propDays;
                string day2 = ((Session["masterInput"] as BookingRequest)).propDay2;
                BokingDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));
                if (days == "0")
                {
                    status = ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (days == "1")
                {
                    status = ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (days == "2")
                {
                    status = ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                //Status is for Day2
                if (day2 == "0")
                {
                    status2 = ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (day2 == "1")
                {
                    status2 = ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (day2 == "2")
                {
                    status2 = ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }

                if (e.CommandName == "Popup" && e.CommandArgument != null)
                {
                    hdnManageBookingRequest.Value = "0";
                    Session["IsSecondary"] = "";
                    int hotelid = Convert.ToInt32(e.CommandArgument.ToString());
                    hdnHotelId.Value = Convert.ToString(hotelid);
                    hdnRequestedHotelId.Value = Convert.ToString(hotelid);
                    GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    GridView gv = (GridView)row.FindControl("grvBookMeeting");
                    GridView gvr = (GridView)row.FindControl("grvRequestMeeting");
                    LinkButton lnkAddtoShopping = (LinkButton)row.FindControl("lnkAddtoShopping");
                    btnVar = lnkAddtoShopping.ClientID;
                    ViewState["btnVar"] = lnkAddtoShopping.ClientID;
                    GetBasket();

                    System.Web.UI.HtmlControls.HtmlGenericControl mydiv = ((System.Web.UI.HtmlControls.HtmlGenericControl)row.FindControl("divBmeeting"));
                    //mydiv.Style.Add("display", "block");
                    mydiv.Visible = true;
                    System.Web.UI.HtmlControls.HtmlGenericControl divRequest = ((System.Web.UI.HtmlControls.HtmlGenericControl)row.FindControl("divRequest"));
                    divRequest.Visible = true;
                    ViewState["CurrentHotel"] = hotelid;

                    int Participant = Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants);
                    VList<ViewMeetingRoomAvailability> VMeeting = new VList<ViewMeetingRoomAvailability>();
                    string whereclaus = "HotelId='" + hotelid + "' and AvailabilityDate='" + BokingDate + "' and " + status + " and " + Participant + " between " + "isnull(MinNo,0) and isnull(MaxNo,0)";
                    string whereclaus2;
                    if (Convert.ToString(((Session["masterInput"] as BookingRequest)).propDuration) == "2")
                    {
                        whereclaus2 = "HotelId='" + hotelid + "' and AvailabilityDate='" + BokingDate.AddDays(1) + "' and " + status2 + " and " + Participant + " between " + "isnull(MinNo,0) and isnull(MaxNo,0)";
                        VList<ViewMeetingRoomAvailability> VMeeting2 = objBookingRequest.GetMeetingRoomAvailability(whereclaus2).FindAllDistinct(ViewMeetingRoomAvailabilityColumn.MeetingRoomId);
                        VList<ViewMeetingRoomAvailability> VMeeting1 = objBookingRequest.GetMeetingRoomAvailability(whereclaus).FindAllDistinct(ViewMeetingRoomAvailabilityColumn.MeetingRoomId);
                        for (int i = 0; i < VMeeting1.Count; i++)
                        {
                            ViewMeetingRoomAvailability b = VMeeting2.Where(a => a.HotelId == VMeeting1[i].HotelId && a.MeetingRoomId == VMeeting1[i].MeetingRoomId).FirstOrDefault();
                            if (b != null)
                            {
                                VMeeting.Add(b);
                            }
                        }
                    }
                    else
                    {
                        VMeeting = objBookingRequest.GetMeetingRoomAvailability(whereclaus).FindAllDistinct(ViewMeetingRoomAvailabilityColumn.MeetingRoomId);
                    }
                    //Meeting room bind for request grid
                    gv.DataSource = VMeeting;
                    gv.DataBind();

                    //Meeting room bind for request grid
                    string whereclsreq = "HotelId='" + hotelid + "'";
                    gvr.DataSource = objBookingRequest.GetMeetingRoomRequest(whereclsreq, string.Empty);
                    gvr.DataBind();
                    foreach (GridViewRow r in grvBooking.Rows)
                    {
                        if (r.RowType == DataControlRowType.DataRow)
                        {
                            Panel pnlBooking = (Panel)r.FindControl("pnlBooking");
                            System.Web.UI.HtmlControls.HtmlGenericControl divcover = ((System.Web.UI.HtmlControls.HtmlGenericControl)r.FindControl("divcover"));
                            HiddenField hdnId = (HiddenField)r.FindControl("hdnHotelId");
                            if (ViewState["CurrentHotel"] == null)
                            {
                                pnlBooking.Enabled = true;
                                divcover.Visible = false;
                            }
                            else
                            {
                                if (Convert.ToInt64(ViewState["CurrentHotel"]) != Convert.ToInt64(hdnId.Value))
                                {
                                    pnlBooking.Enabled = false;
                                    divcover.Visible = true;
                                    //pnlBooking.CssClass = "overlay2";
                                }
                                else
                                {
                                    pnlBooking.Enabled = true;
                                    divcover.Visible = false;
                                }
                            }
                        }
                    }
                    InsertStatistics(hotelid);
                }

                if (e.CommandName == "Request" && e.CommandArgument != null)
                {
                    hdnManageBookingRequest.Value = "0";
                    //Session["IsSecondary"] = "";
                    int hotelid = Convert.ToInt32(e.CommandArgument.ToString());
                    hdnRequestedHotelId.Value = Convert.ToString(hotelid);
                    GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    GridView gvr = (GridView)row.FindControl("grvRequestMeeting");
                    LinkButton lnkAddtoShopping = (LinkButton)row.FindControl("lnkAddtoShopping");
                    LinkButton lnbBookOnline = (LinkButton)row.FindControl("lnbBookOnline");
                    btnVar = lnkAddtoShopping.ClientID;
                    ViewState["btnVar"] = lnkAddtoShopping.ClientID;
                    GetBasket();

                    System.Web.UI.HtmlControls.HtmlGenericControl mydiv = ((System.Web.UI.HtmlControls.HtmlGenericControl)row.FindControl("divBmeeting"));
                    //mydiv.Style.Add("display", "block");
                    mydiv.Visible = true;
                    System.Web.UI.HtmlControls.HtmlGenericControl divBookingMeeting = ((System.Web.UI.HtmlControls.HtmlGenericControl)row.FindControl("divBookingMeeting"));
                    divBookingMeeting.Style.Add("display", "none");
                    System.Web.UI.HtmlControls.HtmlGenericControl divRequest = ((System.Web.UI.HtmlControls.HtmlGenericControl)row.FindControl("divRequest"));
                    //divRequest.Style.Add("display", "block");
                    divRequest.Visible = true;
                    ViewState["CurrentHotel"] = hotelid;


                    string whereclaus = "HotelId='" + hotelid + "'";
                    gvr.DataSource = objBookingRequest.GetMeetingRoomRequest(whereclaus, string.Empty);
                    gvr.DataBind();
                    foreach (GridViewRow r in grvBooking.Rows)
                    {
                        if (r.RowType == DataControlRowType.DataRow)
                        {
                            Panel pnlBooking = (Panel)r.FindControl("pnlBooking");
                            System.Web.UI.HtmlControls.HtmlGenericControl divcover = ((System.Web.UI.HtmlControls.HtmlGenericControl)r.FindControl("divcover"));
                            HiddenField hdnId = (HiddenField)r.FindControl("hdnHotelId");
                            if (ViewState["CurrentHotel"] == null)
                            {
                                pnlBooking.Enabled = true;
                                divcover.Visible = false;
                            }
                            else
                            {
                                if (Convert.ToInt64(ViewState["CurrentHotel"]) != Convert.ToInt64(hdnId.Value))
                                {
                                    pnlBooking.Enabled = false;
                                    divcover.Visible = true;
                                    //pnlBooking.CssClass = "overlay2";
                                    lnbBookOnline.Enabled = false;
                                }
                                else
                                {
                                    pnlBooking.Enabled = true;
                                    divcover.Visible = false;
                                }
                            }
                        }
                    }
                    InsertStatistics(hotelid);
                }

                if (e.CommandName == "Booking" && e.CommandArgument != null)
                {
                    //objRequestedHotel= (TList<Hotel>)Session["Requestedhotel"];
                    //if (objRequestedHotel.Count > 0)
                    // {
                    //     MessageBox("You need to continue with request basket or delete request basket for further booking");
                    //     return;
                    // }
                    hdnManageBookingRequest.Value = "0";
                    Session["IsSecondary"] = "";
                    int hotelid = Convert.ToInt32(e.CommandArgument.ToString());
                    hdnHotelId.Value = Convert.ToString(hotelid);
                    GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    GridView gv = (GridView)row.FindControl("grvBookMeeting");
                    LinkButton lnkAddtoShopping = (LinkButton)row.FindControl("lnkAddtoShopping");
                    LinkButton lnbRequestOnline = (LinkButton)row.FindControl("lnbRequestOnline");
                    LinkButton btnSeeAll = (LinkButton)row.FindControl("btnSeeAll");
                    btnVar = lnkAddtoShopping.ClientID;
                    ViewState["btnVar"] = lnkAddtoShopping.ClientID;

                    System.Web.UI.HtmlControls.HtmlGenericControl mydiv = ((System.Web.UI.HtmlControls.HtmlGenericControl)row.FindControl("divBmeeting"));
                    mydiv.Visible = true;
                    //mydiv.Style.Add("display", "block");
                    System.Web.UI.HtmlControls.HtmlGenericControl divBookingMeeting = ((System.Web.UI.HtmlControls.HtmlGenericControl)row.FindControl("divBookingMeeting"));
                    divBookingMeeting.Style.Add("display", "block");
                    System.Web.UI.HtmlControls.HtmlGenericControl divRequest = ((System.Web.UI.HtmlControls.HtmlGenericControl)row.FindControl("divRequest"));
                    //divRequest.Style.Add("display", "none");
                    divRequest.Visible = false;
                    ViewState["CurrentHotel"] = hotelid;

                    int Participant = Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants);
                    VList<ViewMeetingRoomAvailability> VMeeting = new VList<ViewMeetingRoomAvailability>();
                    string whereclaus = "HotelId='" + hotelid + "' and AvailabilityDate='" + BokingDate + "' and " + status + " and " + Participant + " between " + "isnull(MinNo,0) and isnull(MaxNo,0)";
                    string whereclaus2;
                    if (Convert.ToString(((Session["masterInput"] as BookingRequest)).propDuration) == "2")
                    {
                        whereclaus2 = "HotelId='" + hotelid + "' and AvailabilityDate='" + BokingDate.AddDays(1) + "' and " + status2 + " and " + Participant + " between " + "isnull(MinNo,0) and isnull(MaxNo,0)";
                        VList<ViewMeetingRoomAvailability> VMeeting2 = objBookingRequest.GetMeetingRoomAvailability(whereclaus2).FindAllDistinct(ViewMeetingRoomAvailabilityColumn.MeetingRoomId);
                        VList<ViewMeetingRoomAvailability> VMeeting1 = objBookingRequest.GetMeetingRoomAvailability(whereclaus).FindAllDistinct(ViewMeetingRoomAvailabilityColumn.MeetingRoomId);
                        for (int i = 0; i < VMeeting1.Count; i++)
                        {
                            ViewMeetingRoomAvailability b = VMeeting2.Where(a => a.HotelId == VMeeting1[i].HotelId && a.MeetingRoomId == VMeeting1[i].MeetingRoomId).FirstOrDefault();
                            if (b != null)
                            {
                                VMeeting.Add(b);
                            }
                        }
                    }
                    else
                    {
                        VMeeting = objBookingRequest.GetMeetingRoomAvailability(whereclaus).FindAllDistinct(ViewMeetingRoomAvailabilityColumn.MeetingRoomId);
                    }
                    //Meeting room bind for request grid
                    gv.DataSource = VMeeting;
                    gv.DataBind();
                    foreach (GridViewRow r in grvBooking.Rows)
                    {
                        if (r.RowType == DataControlRowType.DataRow)
                        {
                            Panel pnlBooking = (Panel)r.FindControl("pnlBooking");
                            System.Web.UI.HtmlControls.HtmlGenericControl divcover = ((System.Web.UI.HtmlControls.HtmlGenericControl)r.FindControl("divcover"));
                            HiddenField hdnId = (HiddenField)r.FindControl("hdnHotelId");
                            if (ViewState["CurrentHotel"] == null)
                            {
                                pnlBooking.Enabled = true;
                                divcover.Visible = false;
                            }
                            else
                            {
                                if (Convert.ToInt64(ViewState["CurrentHotel"]) != Convert.ToInt64(hdnId.Value))
                                {
                                    pnlBooking.Enabled = false;
                                    divcover.Visible = true;
                                    //pnlBooking.CssClass = "overlay2";
                                    lnbRequestOnline.Enabled = false;
                                    btnSeeAll.Enabled = false;
                                }
                                else
                                {
                                    pnlBooking.Enabled = true;
                                    divcover.Visible = false;
                                }
                            }
                        }
                    }
                    InsertStatistics(hotelid);
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "ShowRequest", "ClosePopUp();", true);
                }
                if (e.CommandName == "CancelPopup")
                {
                    BindResultforRequest();
                }
                if (e.CommandName == "CancelPopupRequest")
                {
                    if (Convert.ToString(Session["Requestedhotel"]) != "")
                    {
                        objRequestedHotel = (TList<Hotel>)Session["Requestedhotel"];
                    }
                    GetBasket();
                    if (objRequestedHotel.Count == Convert.ToInt32(Session["RequestCount"]))
                    {
                        pnlBookRequest.Enabled = false;
                        divBookRequest.Visible = true;
                        sortingPanel.Enabled = false;
                        sortingPanel.CssClass = "overlay2";
                        ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () {alert(' " + GetKeyResult("YOUHAVECHOSEN3VANUESMAXPERREQUEST") + " " + Session["RequestCount"] + " " + GetKeyResult("YOUHAVECHOSEN3VANUESMAXPERREQUEST1") + "');});", true);
                    }
                    else
                    {
                        BindResultforRequest();
                        sortingPanel.Enabled = true;
                        sortingPanel.CssClass = "";
                    }
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    protected void grvRequest_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            Control commandSource = e.CommandSource as Control;
            //if (ViewState["LinkRequestClick"] == null)
            //{
            //    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () { focusRequiredElement('" + commandSource.ClientID + "');});", true);
            //}
            //ViewState["LinkRequestClick"] = null;
            if (Session["masterInput"] != null)
            {
                BokingDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));

                if (e.CommandName == "Request" && e.CommandArgument != null)
                {
                    hdnManageBookingRequest.Value = "1";
                    //Session["IsSecondary"] = "";
                    int hotelid = Convert.ToInt32(e.CommandArgument.ToString());
                    hdnRequestedHotelId.Value = Convert.ToString(hotelid);
                    GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    GridView gvr = (GridView)row.FindControl("grvRequesttabMeeting");
                    LinkButton lnkButtonAddToBasket = (LinkButton)row.FindControl("lnkButtonAddToBasket");
                    btnVar = lnkButtonAddToBasket.ClientID;
                    ViewState["btnVar"] = lnkButtonAddToBasket.ClientID;
                    GetBasket();

                    System.Web.UI.HtmlControls.HtmlGenericControl divRequest = ((System.Web.UI.HtmlControls.HtmlGenericControl)row.FindControl("divRequest"));
                    //divRequest.Style.Add("display", "block");
                    divRequest.Visible = true;
                    ViewState["CurrentHotel"] = hotelid;

                    string whereclausRequest = "HotelId='" + hotelid + "'";
                    vlistMeetingRoomRequest = objBookingRequest.GetMeetingRoomRequest(whereclausRequest, string.Empty);
                    gvr.DataSource = vlistMeetingRoomRequest.FindAllDistinct(ViewMeetingRoomForRequestColumn.MeetingRoomId);
                    gvr.DataBind();
                    foreach (GridViewRow r in grvRequest.Rows)
                    {
                        if (r.RowType == DataControlRowType.DataRow)
                        {
                            Panel pnlBooking = (Panel)r.FindControl("pnlBooking");
                            System.Web.UI.HtmlControls.HtmlGenericControl divcover = ((System.Web.UI.HtmlControls.HtmlGenericControl)r.FindControl("divcover"));
                            HiddenField hdnId = (HiddenField)r.FindControl("hdnHotelId");
                            if (ViewState["CurrentHotel"] == null)
                            {
                                pnlBooking.Enabled = true;
                                divcover.Visible = false;
                            }
                            else
                            {
                                if (Convert.ToInt64(ViewState["CurrentHotel"]) != Convert.ToInt64(hdnId.Value))
                                {
                                    pnlBooking.Enabled = false;
                                    divcover.Visible = true;
                                    //pnlBooking.CssClass = "overlay2";
                                }
                                else
                                {
                                    pnlBooking.Enabled = true;
                                    divcover.Visible = false;
                                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () { focusRequiredElement('" + commandSource.ClientID + "');});", true);
                                }
                            }

                        }
                    }
                    InsertStatistics(hotelid);
                }
                if (e.CommandName == "CancelPopupRequest")
                {
                    if (Convert.ToString(Session["Requestedhotel"]) != "")
                    {
                        objRequestedHotel = (TList<Hotel>)Session["Requestedhotel"];
                    }
                    GetBasket();
                    if (objRequestedHotel.Count == Convert.ToInt32(Session["RequestCount"]))
                    {
                        pnlBookRequest.Enabled = false;
                        divBookRequest.Visible = true;
                        sortingPanel.Enabled = false;
                        sortingPanel.CssClass = "overlay2";
                        ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () {alert(' " + GetKeyResult("YOUHAVECHOSEN3VANUESMAXPERREQUEST") + " " + Session["RequestCount"] + " " + GetKeyResult("YOUHAVECHOSEN3VANUESMAXPERREQUEST1") + "');});", true);
                        hdnManageBookingRequest.Value = "1";
                    }
                    else
                    {
                        BindResultforRequest();
                        sortingPanel.Enabled = true;
                        sortingPanel.CssClass = "";
                        hdnManageBookingRequest.Value = "1";
                    }
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    #region Bind Meetingroom Configuration after click on hotel In Booking Tab
    //Bind Meetingroom Configuration for booking after clicking see all
    protected void grvBookMeeting_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (Session["masterInput"] != null)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
            {
                //for image,plan,room size and height from meeting table
                Image hotelImage = (Image)e.Row.FindControl("imgHotelImage");
                hotelImage.ImageUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "/MeetingRoomImage/" + DataBinder.Eval(e.Row.DataItem, "Picture");
                HyperLink PlanView = (HyperLink)e.Row.FindControl("linkviewPlan");

                if (DataBinder.Eval(e.Row.DataItem, "MRPlan") != "")
                {
                    PlanView.NavigateUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "Plan/" + DataBinder.Eval(e.Row.DataItem, "MRPlan");
                }
                else
                {
                    PlanView.Visible = false;
                }


                HiddenField hdnMId = (HiddenField)e.Row.FindControl("hdnMId");
                LinkButton lnkDescription = (LinkButton)e.Row.FindControl("hypDescription");

                //Redirect to slideshow for Meetingroom pocture
                HyperLink hypMeetingPhoto = (HyperLink)e.Row.FindControl("hypMeetingPhoto");
                hypMeetingPhoto.NavigateUrl = "/SlideShowPopup.aspx?Mid=" + hdnMId.Value;
                hypMeetingPhoto.Attributes.Add("onclick", "return NavigateMeetingPics(" + hdnMId.Value + ");");
                //Redirect to slideshow for Meetingroom pocture

                string whereclauseMeeting = "Meetingroom_Id=" + hdnMId.Value + " and " + "Language_Id=" + Session["LanguageID"] + "";
                TList<MeetingRoomDesc> lstdesc = ObjMeetingRoomDesc.GetMeetingRoomDescByMeetingRoomLanguageId(whereclauseMeeting);
                if (lstdesc.Count > 0)
                {
                    lnkDescription.Attributes.Add("alt", lstdesc[0].Description == null ? "" : lstdesc[0].Description);
                }
                else
                {
                    int englishID = Convert.ToInt32(ObjHotelinfo.GetAllRequiredLanguage().Find(a => a.Name == "English").Id);
                    string wherecls = "Meetingroom_Id=" + hdnMId.Value + " and " + "Language_Id=" + englishID + "";
                    TList<MeetingRoomDesc> lstEng = ObjMeetingRoomDesc.GetMeetingRoomDescByMeetingRoomLanguageId(wherecls);
                    lnkDescription.Attributes.Add("alt", lstEng[0].Description == null ? "" : lstEng[0].Description);
                }

                //TList<MeetingRoomConfig> lst = ObjMeetingRoomConfigManager.GetConfigByMeetingRoomIDParticipant(Convert.ToInt32(hdnMId.Value), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants));
                TList<MeetingRoomConfig> lst = ObjMeetingRoomConfigManager.GetConfigByMeetingRoomID(Convert.ToInt32(hdnMId.Value));

                int intMinEnumValue = ObjMeetingRoomConfigManager.GetMinMeetingRoomShapeID();
                int intMaxEnumValue = ObjMeetingRoomConfigManager.GetMaxMeetingRoomShapeID();
                int Participant = Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants);
                int intIndex;
                for (intIndex = intMinEnumValue; intIndex <= intMaxEnumValue; intIndex++)
                {
                    var getRow = lst.Find(a => a.RoomShapeId == intIndex);
                    if (getRow != null)
                    {

                        if (getRow.RoomShapeId == 1)
                        {
                            Label lblTheatreMin = (Label)e.Row.FindControl("lblTheatreMin");
                            Label lblTheatreMax = (Label)e.Row.FindControl("lblTheatreMax");
                            CheckBox chkTheatre = (CheckBox)e.Row.FindControl("chkTheatre");
                            chkTheatre.Attributes.Add("onclick", "javascript:CheckOne(this,'" + hdnMId.Value + "','" + getRow.Id + "','" + btnVar + "');");
                            lblTheatreMin.Text = getRow.MinCapacity.ToString() + " - ";
                            lblTheatreMax.Text = getRow.MaxCapicity.ToString();
                            if (Participant < Convert.ToInt32(getRow.MinCapacity.ToString()))
                            {
                                chkTheatre.Visible = false;
                            }
                            else if (Participant > Convert.ToInt32(getRow.MaxCapicity.ToString()))
                            {
                                chkTheatre.Visible = false;
                            }
                        }
                        else if (getRow.RoomShapeId == 2)
                        {

                            Label lblReceptionMin = (Label)e.Row.FindControl("lblReceptionMin");
                            Label lblReceptionMax = (Label)e.Row.FindControl("lblReceptionMax");
                            CheckBox chkSchool = (CheckBox)e.Row.FindControl("chkSchool");
                            chkSchool.Attributes.Add("onclick", "javascript:CheckOne(this,'" + hdnMId.Value + "','" + getRow.Id + "','" + btnVar + "');");
                            lblReceptionMin.Text = getRow.MinCapacity.ToString() + " - ";
                            lblReceptionMax.Text = getRow.MaxCapicity.ToString();
                            if (Participant < Convert.ToInt32(getRow.MinCapacity.ToString()))
                            {
                                chkSchool.Visible = false;
                            }
                            else if (Participant > Convert.ToInt32(getRow.MaxCapicity.ToString()))
                            {
                                chkSchool.Visible = false;
                            }


                        }
                        else if (getRow.RoomShapeId == 3)
                        {

                            Label lblUShapeMin = (Label)e.Row.FindControl("lblUShapeMin");
                            Label lblUShapeMax = (Label)e.Row.FindControl("lblUShapeMax");
                            CheckBox chkUshape = (CheckBox)e.Row.FindControl("chkUshape");
                            chkUshape.Attributes.Add("onclick", "javascript:CheckOne(this,'" + hdnMId.Value + "','" + getRow.Id + "','" + btnVar + "');");
                            lblUShapeMin.Text = getRow.MinCapacity.ToString() + " - ";
                            lblUShapeMax.Text = getRow.MaxCapicity.ToString();
                            if (Participant < Convert.ToInt32(getRow.MinCapacity.ToString()))
                            {
                                chkUshape.Visible = false;
                            }
                            else if (Participant > Convert.ToInt32(getRow.MaxCapicity.ToString()))
                            {
                                chkUshape.Visible = false;
                            }

                        }
                        else if (getRow.RoomShapeId == 4)
                        {

                            Label lblBanquetMin = (Label)e.Row.FindControl("lblBanquetMin");
                            Label lblBanquetMax = (Label)e.Row.FindControl("lblBanquetMax");
                            CheckBox chkBoardroom = (CheckBox)e.Row.FindControl("chkBoardroom");
                            chkBoardroom.Attributes.Add("onclick", "javascript:CheckOne(this,'" + hdnMId.Value + "','" + getRow.Id + "','" + btnVar + "');");
                            lblBanquetMin.Text = getRow.MinCapacity.ToString() + " - ";
                            lblBanquetMax.Text = getRow.MaxCapicity.ToString();
                            if (Participant < Convert.ToInt32(getRow.MinCapacity.ToString()))
                            {
                                chkBoardroom.Visible = false;
                            }
                            else if (Participant > Convert.ToInt32(getRow.MaxCapicity.ToString()))
                            {
                                chkBoardroom.Visible = false;
                            }
                        }
                        else if (getRow.RoomShapeId == 5)
                        {

                            Label lblCocktailMin = (Label)e.Row.FindControl("lblCocktailMin");
                            Label lblCocktailMax = (Label)e.Row.FindControl("lblCocktailMax");
                            CheckBox chkCocktail = (CheckBox)e.Row.FindControl("chkCocktail");
                            chkCocktail.Attributes.Add("onclick", "javascript:CheckOne(this,'" + hdnMId.Value + "','" + getRow.Id + "','" + btnVar + "');");
                            lblCocktailMin.Text = getRow.MinCapacity.ToString() + " - ";
                            lblCocktailMax.Text = getRow.MaxCapicity.ToString();
                            if (Participant < Convert.ToInt32(getRow.MinCapacity.ToString()))
                            {
                                chkCocktail.Visible = false;
                            }
                            else if (Participant > Convert.ToInt32(getRow.MaxCapicity.ToString()))
                            {
                                chkCocktail.Visible = false;
                            }
                        }
                    }
                }
            }
        }
    }
    #endregion

    #region Bind Meetingroom Configuration for request after click on hotel In Booking Tab
    //Bind Meetingroom Configuration for request after clicking see all
    protected void grvRequestMeeting_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
        {
            //for image,plan,room size and height from meeting table
            Image hotelImage = (Image)e.Row.FindControl("imgHotelImage");
            hotelImage.ImageUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "/MeetingRoomImage/" + DataBinder.Eval(e.Row.DataItem, "Picture");
            HyperLink PlanView = (HyperLink)e.Row.FindControl("linkviewPlan");
            if (DataBinder.Eval(e.Row.DataItem, "MRPlan") != "")
            {
                PlanView.NavigateUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "Plan/" + DataBinder.Eval(e.Row.DataItem, "MRPlan");
            }
            else
            {
                PlanView.Visible = false;
            }


            HiddenField hdnMId = (HiddenField)e.Row.FindControl("hdnMId");
            LinkButton lnkDescription = (LinkButton)e.Row.FindControl("hypDescription");

            //Redirect to slideshow for Meetingroom pocture
            HyperLink hypMeetingPhoto = (HyperLink)e.Row.FindControl("hypMeetingPhoto");
            hypMeetingPhoto.NavigateUrl = "/SlideShowPopup.aspx?Mid=" + hdnMId.Value;
            hypMeetingPhoto.Attributes.Add("onclick", "return NavigateMeetingPics(" + hdnMId.Value + ");");
            //Redirect to slideshow for Meetingroom pocture


            string whereclauseMeeting = "Meetingroom_Id=" + hdnMId.Value + " and " + "Language_Id=" + Session["LanguageID"] + "";
            TList<MeetingRoomDesc> lstdesc = ObjMeetingRoomDesc.GetMeetingRoomDescByMeetingRoomLanguageId(whereclauseMeeting);
            if (lstdesc.Count > 0)
            {
                lnkDescription.Attributes.Add("alt", lstdesc[0].Description == null ? "" : lstdesc[0].Description);
            }
            else
            {
                int englishID = Convert.ToInt32(ObjHotelinfo.GetAllRequiredLanguage().Find(a => a.Name == "English").Id);
                string wherecls = "Meetingroom_Id=" + hdnMId.Value + " and " + "Language_Id=" + englishID + "";
                TList<MeetingRoomDesc> lstEng = ObjMeetingRoomDesc.GetMeetingRoomDescByMeetingRoomLanguageId(wherecls);
                lnkDescription.Attributes.Add("alt", lstEng[0].Description == null ? "" : lstEng[0].Description);
            }

            ////Redirect to slideshow for Meetingroom pocture
            //HyperLink hypMeetingPhoto = (HyperLink)e.Row.FindControl("hypMeetingPhoto");
            //hypMeetingPhoto.NavigateUrl = "/slideShow.aspx?Mid=" + hdnMId.Value;
            //hypMeetingPhoto.Attributes.Add("onclick", "return NavigateMeetingPics(" + hdnMId.Value + ");");
            ////Redirect to slideshow for Meetingroom pocture

            //TList<MeetingRoomConfig> lst = ObjMeetingRoomConfigManager.GetConfigByMeetingRoomIDParticipant(Convert.ToInt32(hdnMId.Value), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants));
            TList<MeetingRoomConfig> lst = ObjMeetingRoomConfigManager.GetConfigByMeetingRoomID(Convert.ToInt32(hdnMId.Value));

            int intMinEnumValue = ObjMeetingRoomConfigManager.GetMinMeetingRoomShapeID();
            int intMaxEnumValue = ObjMeetingRoomConfigManager.GetMaxMeetingRoomShapeID();
            int intIndex;
            for (intIndex = intMinEnumValue; intIndex <= intMaxEnumValue; intIndex++)
            {
                var getRow = lst.Find(a => a.RoomShapeId == intIndex);
                if (getRow != null)
                {

                    if (getRow.RoomShapeId == 1)
                    {
                        Label lblTheatreMin = (Label)e.Row.FindControl("lblTheatreMin");
                        Label lblTheatreMax = (Label)e.Row.FindControl("lblTheatreMax");
                        CheckBox chkTheatre = (CheckBox)e.Row.FindControl("chkTheatre");
                        chkTheatre.Attributes.Add("onclick", "javascript:CheckOneRequest(this,'" + hdnMId.Value + "','" + getRow.Id + "','" + btnVar + "');");
                        lblTheatreMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblTheatreMax.Text = getRow.MaxCapicity.ToString();
                        if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                        {
                            lblTheatreMin.Text = "";
                            lblTheatreMax.Text = "";
                            chkTheatre.Visible = false;
                        }
                    }
                    else if (getRow.RoomShapeId == 2)
                    {

                        Label lblReceptionMin = (Label)e.Row.FindControl("lblReceptionMin");
                        Label lblReceptionMax = (Label)e.Row.FindControl("lblReceptionMax");
                        CheckBox chkSchool = (CheckBox)e.Row.FindControl("chkSchool");
                        chkSchool.Attributes.Add("onclick", "javascript:CheckOneRequest(this,'" + hdnMId.Value + "','" + getRow.Id + "','" + btnVar + "');");
                        lblReceptionMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblReceptionMax.Text = getRow.MaxCapicity.ToString();
                        if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                        {
                            lblReceptionMin.Text = "";
                            lblReceptionMax.Text = "";
                            chkSchool.Visible = false;
                        }


                    }
                    else if (getRow.RoomShapeId == 3)
                    {

                        Label lblUShapeMin = (Label)e.Row.FindControl("lblUShapeMin");
                        Label lblUShapeMax = (Label)e.Row.FindControl("lblUShapeMax");
                        CheckBox chkUshape = (CheckBox)e.Row.FindControl("chkUshape");
                        chkUshape.Attributes.Add("onclick", "javascript:CheckOneRequest(this,'" + hdnMId.Value + "','" + getRow.Id + "','" + btnVar + "');");
                        lblUShapeMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblUShapeMax.Text = getRow.MaxCapicity.ToString();
                        if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                        {
                            lblUShapeMin.Text = "";
                            lblUShapeMax.Text = "";
                            chkUshape.Visible = false;
                        }

                    }
                    else if (getRow.RoomShapeId == 4)
                    {

                        Label lblBanquetMin = (Label)e.Row.FindControl("lblBanquetMin");
                        Label lblBanquetMax = (Label)e.Row.FindControl("lblBanquetMax");
                        CheckBox chkBoardroom = (CheckBox)e.Row.FindControl("chkBoardroom");
                        chkBoardroom.Attributes.Add("onclick", "javascript:CheckOneRequest(this,'" + hdnMId.Value + "','" + getRow.Id + "','" + btnVar + "');");
                        lblBanquetMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblBanquetMax.Text = getRow.MaxCapicity.ToString();
                        if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                        {
                            lblBanquetMin.Text = "";
                            lblBanquetMax.Text = "";
                            chkBoardroom.Visible = false;
                        }
                    }
                    else if (getRow.RoomShapeId == 5)
                    {

                        Label lblCocktailMin = (Label)e.Row.FindControl("lblCocktailMin");
                        Label lblCocktailMax = (Label)e.Row.FindControl("lblCocktailMax");
                        CheckBox chkCocktail = (CheckBox)e.Row.FindControl("chkCocktail");
                        chkCocktail.Attributes.Add("onclick", "javascript:CheckOneRequest(this,'" + hdnMId.Value + "','" + getRow.Id + "','" + btnVar + "');");
                        lblCocktailMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblCocktailMax.Text = getRow.MaxCapicity.ToString();
                        if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                        {
                            lblCocktailMin.Text = "";
                            lblCocktailMax.Text = "";
                            chkCocktail.Visible = false;
                        }
                    }
                }
            }
        }
    }
    #endregion

    #region Bind Meetingroom Configuration for request tab after click on hotel In Request Tab
    //Bind Meetingroom Configuration for request after clicking see all
    protected void grvRequesttabMeeting_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
        {
            //for image,plan,room size and height from meeting table
            Image hotelImage = (Image)e.Row.FindControl("imgHotelImage");
            hotelImage.ImageUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "/MeetingRoomImage/" + DataBinder.Eval(e.Row.DataItem, "Picture");
            HyperLink PlanView = (HyperLink)e.Row.FindControl("linkviewPlan");
            if (DataBinder.Eval(e.Row.DataItem, "MRPlan") != "")
            {
                PlanView.NavigateUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "Plan/" + DataBinder.Eval(e.Row.DataItem, "MRPlan");
            }
            else
            {
                PlanView.Visible = false;
            }


            HiddenField hdnMId = (HiddenField)e.Row.FindControl("hdnMId");
            LinkButton lnkDescription = (LinkButton)e.Row.FindControl("hypDescription");

            //Redirect to slideshow for Meetingroom picture
            HyperLink hypMeetingPhoto = (HyperLink)e.Row.FindControl("hypMeetingPhoto");
            hypMeetingPhoto.NavigateUrl = SiteRootPath + "/SlideShowPopup.aspx?Mid=" + hdnMId.Value;
            hypMeetingPhoto.Attributes.Add("onclick", "return NavigateMeetingPics(" + hdnMId.Value + ");");
            //Redirect to slideshow for Meetingroom picture

            string whereclauseMeeting = "Meetingroom_Id=" + hdnMId.Value + " and " + "Language_Id=" + Session["LanguageID"] + "";
            TList<MeetingRoomDesc> lstdesc = ObjMeetingRoomDesc.GetMeetingRoomDescByMeetingRoomLanguageId(whereclauseMeeting);
            if (lstdesc.Count > 0)
            {
                lnkDescription.Attributes.Add("alt", lstdesc[0].Description == null ? "" : lstdesc[0].Description);
            }
            else
            {
                int englishID = Convert.ToInt32(ObjHotelinfo.GetAllRequiredLanguage().Find(a => a.Name == "English").Id);
                string wherecls = "Meetingroom_Id=" + hdnMId.Value + " and " + "Language_Id=" + englishID + "";
                TList<MeetingRoomDesc> lstEng = ObjMeetingRoomDesc.GetMeetingRoomDescByMeetingRoomLanguageId(wherecls);
                lnkDescription.Attributes.Add("alt", lstEng[0].Description == null ? "" : lstEng[0].Description);
            }


            //TList<MeetingRoomConfig> lst = ObjMeetingRoomConfigManager.GetConfigByMeetingRoomIDParticipant(Convert.ToInt32(hdnMId.Value), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants));
            TList<MeetingRoomConfig> lst = ObjMeetingRoomConfigManager.GetConfigByMeetingRoomID(Convert.ToInt32(hdnMId.Value));

            int intMinEnumValue = ObjMeetingRoomConfigManager.GetMinMeetingRoomShapeID();
            int intMaxEnumValue = ObjMeetingRoomConfigManager.GetMaxMeetingRoomShapeID();
            int intIndex;
            for (intIndex = intMinEnumValue; intIndex <= intMaxEnumValue; intIndex++)
            {
                var getRow = lst.Find(a => a.RoomShapeId == intIndex);
                if (getRow != null)
                {

                    if (getRow.RoomShapeId == 1)
                    {
                        Label lblTheatreMin = (Label)e.Row.FindControl("lblTheatreMin");
                        Label lblTheatreMax = (Label)e.Row.FindControl("lblTheatreMax");
                        CheckBox chkTheatre = (CheckBox)e.Row.FindControl("chkTheatre");
                        chkTheatre.Attributes.Add("onclick", "javascript:CheckOneRequest(this,'" + hdnMId.Value + "','" + getRow.Id + "','" + btnVar + "');");
                        lblTheatreMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblTheatreMax.Text = getRow.MaxCapicity.ToString();
                        if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                        {
                            lblTheatreMin.Text = "";
                            lblTheatreMax.Text = "";
                            chkTheatre.Visible = false;
                        }
                    }
                    else if (getRow.RoomShapeId == 2)
                    {

                        Label lblReceptionMin = (Label)e.Row.FindControl("lblReceptionMin");
                        Label lblReceptionMax = (Label)e.Row.FindControl("lblReceptionMax");
                        CheckBox chkSchool = (CheckBox)e.Row.FindControl("chkSchool");
                        chkSchool.Attributes.Add("onclick", "javascript:CheckOneRequest(this,'" + hdnMId.Value + "','" + getRow.Id + "','" + btnVar + "');");
                        lblReceptionMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblReceptionMax.Text = getRow.MaxCapicity.ToString();
                        if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                        {
                            lblReceptionMin.Text = "";
                            lblReceptionMax.Text = "";
                            chkSchool.Visible = false;
                        }


                    }
                    else if (getRow.RoomShapeId == 3)
                    {

                        Label lblUShapeMin = (Label)e.Row.FindControl("lblUShapeMin");
                        Label lblUShapeMax = (Label)e.Row.FindControl("lblUShapeMax");
                        CheckBox chkUshape = (CheckBox)e.Row.FindControl("chkUshape");
                        chkUshape.Attributes.Add("onclick", "javascript:CheckOneRequest(this,'" + hdnMId.Value + "','" + getRow.Id + "','" + btnVar + "');");
                        lblUShapeMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblUShapeMax.Text = getRow.MaxCapicity.ToString();
                        if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                        {
                            lblUShapeMin.Text = "";
                            lblUShapeMax.Text = "";
                            chkUshape.Visible = false;
                        }

                    }
                    else if (getRow.RoomShapeId == 4)
                    {

                        Label lblBanquetMin = (Label)e.Row.FindControl("lblBanquetMin");
                        Label lblBanquetMax = (Label)e.Row.FindControl("lblBanquetMax");
                        CheckBox chkBoardroom = (CheckBox)e.Row.FindControl("chkBoardroom");
                        chkBoardroom.Attributes.Add("onclick", "javascript:CheckOneRequest(this,'" + hdnMId.Value + "','" + getRow.Id + "','" + btnVar + "');");
                        lblBanquetMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblBanquetMax.Text = getRow.MaxCapicity.ToString();
                        if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                        {
                            lblBanquetMin.Text = "";
                            lblBanquetMax.Text = "";
                            chkBoardroom.Visible = false;
                        }
                    }
                    else if (getRow.RoomShapeId == 5)
                    {

                        Label lblCocktailMin = (Label)e.Row.FindControl("lblCocktailMin");
                        Label lblCocktailMax = (Label)e.Row.FindControl("lblCocktailMax");
                        CheckBox chkCocktail = (CheckBox)e.Row.FindControl("chkCocktail");
                        chkCocktail.Attributes.Add("onclick", "javascript:CheckOneRequest(this,'" + hdnMId.Value + "','" + getRow.Id + "','" + btnVar + "');");
                        lblCocktailMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblCocktailMax.Text = getRow.MaxCapicity.ToString();
                        if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                        {
                            lblCocktailMin.Text = "";
                            lblCocktailMax.Text = "";
                            chkCocktail.Visible = false;
                        }
                    }
                }
            }
        }
    }
    #endregion



    #region Bind Shopping Cart
    //Bind Shopping Cart
    protected void lstViewAddToShopping_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (Session["masterInput"] != null)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem && e.Item.DataItem != null)
            {
                bookingCartItem = 0;
                objmeetingroom = (TList<MeetingRoom>)Session["meetingroom"];
                ListView lstView = (ListView)e.Item.FindControl("lstViewMeetingRoomAddToCart");
                lstView.DataSource = objmeetingroom;
                lstView.DataBind();
            }
        }
    }
    #endregion

    #region Remove hotel and meeting room from shopping cart listview
    //Remove hotel and meeting room from shopping cart listview
    protected void lstViewAddToShopping_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        if (Session["masterInput"] != null)
        {
            int intHotelID = 0;
            switch (e.CommandName.Trim())
            {
                case "deletehotel":
                    intHotelID = Convert.ToInt32(e.CommandArgument);
                    Hotel d = objhotel.Find(a => a.Id == intHotelID);
                    objhotel.Remove(d);
                    if (objhotel.Count > 0)
                    {
                        divshoppingcart.Style.Add("display", "none");
                    }
                    lstViewAddToShopping.DataSource = objhotel;
                    lstViewAddToShopping.DataBind();
                    break;
            }
        }
    }
    #endregion

    #region Remove hotel and meeting room from shopping cart listview
    //Remove hotel and meeting room from shopping cart listview
    protected void lstViewMeetingRoomAddToCart_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        if (Session["masterInput"] != null)
        {
            int intmeetingroom = 0;
            switch (e.CommandName.Trim())
            {
                case "deletemeetingroom":
                    intmeetingroom = Convert.ToInt32(e.CommandArgument);
                    objmeetingroom = (TList<MeetingRoom>)Session["MeetingRoom"];
                    if (objmeetingroom.Count == 1)
                    {
                        BindSearchResult();
                    }
                    MeetingRoom d = objmeetingroom.Find(a => a.Id == intmeetingroom);
                    objmeetingroom.Remove(d);
                    if (objmeetingroom.Count == 0) { divshoppingcart.Style.Add("display", "none"); }
                    lstViewAddToShopping.DataSource = (TList<Hotel>)Session["hotel"];
                    lstViewAddToShopping.DataBind();
                    break;
            }
        }
    }
    #endregion

    #region Remove hotel and meeting room from shopping cart listview
    //Remove hotel and all meeting room from shopping cart listview
    protected void imgDel_Click(object sender, ImageClickEventArgs e)
    {
        BindSearchResult();
        //objhotel.Clear();
        //objmeetingroom.Clear();
        //divshoppingcart.Visible = false;
        //pnlBookRequest.Enabled = true;
        //pnlBookRequest.CssClass = "";
        //foreach (GridViewRow r in grvBooking.Rows)
        //{
        //    if (r.RowType == DataControlRowType.DataRow)
        //    {
        //        Panel pnlBooking = (Panel)r.FindControl("pnlBooking");
        //        pnlBooking.Enabled = true;
        //        pnlBooking.CssClass = "";
        //        ViewState["CurrentHotel"] = null;
        //        Session["IsSecondary"] = "";
        //        btnVar = "";
        //        ViewState["btnVar"] = null;
        //        System.Web.UI.HtmlControls.HtmlGenericControl myBooking = ((System.Web.UI.HtmlControls.HtmlGenericControl)r.FindControl("divBmeeting"));
        //        myBooking.Style.Add("display", "none");
        //        hdnHotelId.Value = "";
        //        hdnMeetingRoomId.Value = "";
        //        hdnConfigurationId.Value = "";

        //    }
        //}
    }
    #endregion

    #region Bind secondary meeting room for booking gridview
    //Bind secondary meeting room for booking gridview
    int countRowSecondary = 1;
    int countRowSecondaryItem = 1;
    protected void rptDays_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ViewMeetingRoomAvailability v = e.Item.DataItem as ViewMeetingRoomAvailability;
            TList<MeetingRoomConfig> lst = ObjMeetingRoomConfigManager.GetConfigByMeetingRoomID(v.MeetingRoomId);
            DateTime fromDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));
            Label lblDate = (Label)e.Item.FindControl("lblDate");

            System.Web.UI.HtmlControls.HtmlControl divFullday = ((System.Web.UI.HtmlControls.HtmlControl)e.Item.FindControl("divFullday"));
            System.Web.UI.HtmlControls.HtmlControl divMorning = ((System.Web.UI.HtmlControls.HtmlControl)e.Item.FindControl("divMorning"));
            System.Web.UI.HtmlControls.HtmlControl divAfternoon = ((System.Web.UI.HtmlControls.HtmlControl)e.Item.FindControl("divAfternoon"));

            if (e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var divContainer = e.Item.FindControl("repDiv") as HtmlGenericControl;
                divContainer.Attributes.Add("class", "repDiv" + countRowSecondary);
                countRowSecondary++;
                lblDate.Text = String.Format("{0:dd/MM/yyyy}", fromDate.AddDays(1).ToShortDateString());
                if (((Session["masterInput"] as BookingRequest)).propDay2 != null)
                {
                    if (Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDay2) == 1)
                    {
                        divFullday.Attributes.Add("class", "redul");
                        divAfternoon.Attributes.Add("class", "redul");
                    }
                    else if (Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDay2) == 2)
                    {
                        divMorning.Attributes.Add("class", "redul");
                        divFullday.Attributes.Add("class", "redul");
                    }
                    else
                    {
                        divFullday.Attributes.Add("class", "");
                        divMorning.Attributes.Add("class", "");
                        divAfternoon.Attributes.Add("class", "");
                    }
                }
            }
            else
            {
                var divContainerItem = e.Item.FindControl("repDivItem") as HtmlGenericControl;
                divContainerItem.Attributes.Add("class", "repDivItem");// + countRowSecondaryItem);
                countRowSecondaryItem++;
                if (Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDays) == 1)
                {
                    divFullday.Attributes.Add("class", "redul");
                    divAfternoon.Attributes.Add("class", "redul");
                }
                else if (Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDays) == 2)
                {
                    divMorning.Attributes.Add("class", "redul");
                    divFullday.Attributes.Add("class", "redul");
                }
                else
                {
                    divFullday.Attributes.Add("class", "");
                    divMorning.Attributes.Add("class", "");
                    divAfternoon.Attributes.Add("class", "");
                }
                lblDate.Text = String.Format("{0:dd/MM/yyyy}", fromDate.ToShortDateString());
            }


            int Participant = Convert.ToInt32(txtSecparticipants.Text);
            bool ChkRed = false;
            int intMinEnumValue = ObjMeetingRoomConfigManager.GetMinMeetingRoomShapeID();
            int intMaxEnumValue = ObjMeetingRoomConfigManager.GetMaxMeetingRoomShapeID();
            int intIndex;
            for (intIndex = intMinEnumValue; intIndex <= intMaxEnumValue; intIndex++)
            {

                var getRow = lst.Find(a => a.RoomShapeId == intIndex);
                if (getRow != null)
                {

                    if (getRow.RoomShapeId == 1)
                    {
                        Label lblTheatreMin = (Label)e.Item.FindControl("lblTheatreMin");
                        Label lblTheatreMax = (Label)e.Item.FindControl("lblTheatreMax");
                        CheckBox chkTheatre = (CheckBox)e.Item.FindControl("chkTheatre");
                        Label lblTheatreMin1 = (Label)e.Item.FindControl("lblTheatreMin1");
                        Label lblTheatreMax1 = (Label)e.Item.FindControl("lblTheatreMax1");
                        CheckBox chkTheatre1 = (CheckBox)e.Item.FindControl("chkTheatre1");
                        Label lblTheatreMin2 = (Label)e.Item.FindControl("lblTheatreMin2");
                        Label lblTheatreMax2 = (Label)e.Item.FindControl("lblTheatreMax2");
                        CheckBox chkTheatre2 = (CheckBox)e.Item.FindControl("chkTheatre2");
                        chkTheatre.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','1','" + e.Item.ItemIndex + "','" + rptid + "','2','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondary + "','" + "repDivItem" + countRowSecondaryItem + "');");
                        chkTheatre1.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','2','" + e.Item.ItemIndex + "','" + rptid + "','2','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondary + "','" + "repDivItem" + countRowSecondaryItem + "');");
                        chkTheatre2.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','0','" + e.Item.ItemIndex + "','" + rptid + "','2','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondary + "','" + "repDivItem" + countRowSecondaryItem + "');");
                        lblTheatreMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblTheatreMax.Text = getRow.MaxCapicity.ToString();
                        lblTheatreMin1.Text = getRow.MinCapacity.ToString() + " - ";
                        lblTheatreMax1.Text = getRow.MaxCapicity.ToString();
                        lblTheatreMin2.Text = getRow.MinCapacity.ToString() + " - ";
                        lblTheatreMax2.Text = getRow.MaxCapicity.ToString();
                        if (Participant < Convert.ToInt32(getRow.MinCapacity.ToString()))
                        {
                            chkTheatre.Visible = false;
                            chkTheatre1.Visible = false;
                            chkTheatre2.Visible = false;
                        }
                        else if (Participant > Convert.ToInt32(getRow.MaxCapicity.ToString()))
                        {
                            chkTheatre.Visible = false;
                            chkTheatre1.Visible = false;
                            chkTheatre2.Visible = false;
                        }
                    }
                    else if (getRow.RoomShapeId == 2)
                    {

                        Label lblReceptionMin = (Label)e.Item.FindControl("lblReceptionMin");
                        Label lblReceptionMax = (Label)e.Item.FindControl("lblReceptionMax");
                        CheckBox chkSchool = (CheckBox)e.Item.FindControl("chkSchool");
                        Label lblReceptionMin1 = (Label)e.Item.FindControl("lblReceptionMin1");
                        Label lblReceptionMax1 = (Label)e.Item.FindControl("lblReceptionMax1");
                        CheckBox chkSchool1 = (CheckBox)e.Item.FindControl("chkSchool1");
                        Label lblReceptionMin2 = (Label)e.Item.FindControl("lblReceptionMin2");
                        Label lblReceptionMax2 = (Label)e.Item.FindControl("lblReceptionMax2");
                        CheckBox chkSchool2 = (CheckBox)e.Item.FindControl("chkSchool2");
                        chkSchool.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','1','" + e.Item.ItemIndex + "','" + rptid + "','3','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondary + "','" + "repDivItem" + countRowSecondaryItem + "');");
                        chkSchool1.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','2','" + e.Item.ItemIndex + "','" + rptid + "','3','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondary + "','" + "repDivItem" + countRowSecondaryItem + "');");
                        chkSchool2.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','0','" + e.Item.ItemIndex + "','" + rptid + "','3','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondary + "','" + "repDivItem" + countRowSecondaryItem + "');");
                        lblReceptionMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblReceptionMax.Text = getRow.MaxCapicity.ToString();
                        lblReceptionMin1.Text = getRow.MinCapacity.ToString() + " - ";
                        lblReceptionMax1.Text = getRow.MaxCapicity.ToString();
                        lblReceptionMin2.Text = getRow.MinCapacity.ToString() + " - ";
                        lblReceptionMax2.Text = getRow.MaxCapicity.ToString();
                        if (Participant < Convert.ToInt32(getRow.MinCapacity.ToString()))
                        {
                            chkSchool.Visible = false;
                            chkSchool1.Visible = false;
                            chkSchool2.Visible = false;
                        }
                        else if (Participant > Convert.ToInt32(getRow.MaxCapicity.ToString()))
                        {
                            chkSchool.Visible = false;
                            chkSchool1.Visible = false;
                            chkSchool2.Visible = false;
                        }


                    }
                    else if (getRow.RoomShapeId == 3)
                    {

                        Label lblUShapeMin = (Label)e.Item.FindControl("lblUShapeMin");
                        Label lblUShapeMax = (Label)e.Item.FindControl("lblUShapeMax");
                        CheckBox chkUshape = (CheckBox)e.Item.FindControl("chkUshape");
                        Label lblUShapeMin1 = (Label)e.Item.FindControl("lblUShapeMin1");
                        Label lblUShapeMax1 = (Label)e.Item.FindControl("lblUShapeMax1");
                        CheckBox chkUshape1 = (CheckBox)e.Item.FindControl("chkUshape1");
                        Label lblUShapeMin2 = (Label)e.Item.FindControl("lblUShapeMin2");
                        Label lblUShapeMax2 = (Label)e.Item.FindControl("lblUShapeMax2");
                        CheckBox chkUshape2 = (CheckBox)e.Item.FindControl("chkUshape2");
                        chkUshape.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','1','" + e.Item.ItemIndex + "','" + rptid + "','4','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondary + "','" + "repDivItem" + countRowSecondaryItem + "');");
                        chkUshape1.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','2','" + e.Item.ItemIndex + "','" + rptid + "','4','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondary + "','" + "repDivItem" + countRowSecondaryItem + "');");
                        chkUshape2.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','0','" + e.Item.ItemIndex + "','" + rptid + "','4','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondary + "','" + "repDivItem" + countRowSecondaryItem + "');");
                        lblUShapeMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblUShapeMax.Text = getRow.MaxCapicity.ToString();
                        lblUShapeMin1.Text = getRow.MinCapacity.ToString() + " - ";
                        lblUShapeMax1.Text = getRow.MaxCapicity.ToString();
                        lblUShapeMin2.Text = getRow.MinCapacity.ToString() + " - ";
                        lblUShapeMax2.Text = getRow.MaxCapicity.ToString();
                        if (Participant < Convert.ToInt32(getRow.MinCapacity.ToString()))
                        {
                            chkUshape.Visible = false;
                            chkUshape1.Visible = false;
                            chkUshape2.Visible = false;
                        }
                        else if (Participant > Convert.ToInt32(getRow.MaxCapicity.ToString()))
                        {
                            chkUshape.Visible = false;
                            chkUshape1.Visible = false;
                            chkUshape2.Visible = false;
                        }

                    }
                    else if (getRow.RoomShapeId == 4)
                    {

                        Label lblBanquetMin = (Label)e.Item.FindControl("lblBanquetMin");
                        Label lblBanquetMax = (Label)e.Item.FindControl("lblBanquetMax");
                        CheckBox chkBoardroom = (CheckBox)e.Item.FindControl("chkBoardroom");
                        Label lblBanquetMin1 = (Label)e.Item.FindControl("lblBanquetMin1");
                        Label lblBanquetMax1 = (Label)e.Item.FindControl("lblBanquetMax1");
                        CheckBox chkBoardroom1 = (CheckBox)e.Item.FindControl("chkBoardroom1");
                        Label lblBanquetMin2 = (Label)e.Item.FindControl("lblBanquetMin2");
                        Label lblBanquetMax2 = (Label)e.Item.FindControl("lblBanquetMax2");
                        CheckBox chkBoardroom2 = (CheckBox)e.Item.FindControl("chkBoardroom2");
                        chkBoardroom.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','1','" + e.Item.ItemIndex + "','" + rptid + "','5','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondary + "','" + "repDivItem" + countRowSecondaryItem + "');");
                        chkBoardroom1.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','2','" + e.Item.ItemIndex + "','" + rptid + "','5','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondary + "','" + "repDivItem" + countRowSecondaryItem + "');");
                        chkBoardroom2.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','0','" + e.Item.ItemIndex + "','" + rptid + "','5','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondary + "','" + "repDivItem" + countRowSecondaryItem + "');");
                        lblBanquetMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblBanquetMax.Text = getRow.MaxCapicity.ToString();
                        lblBanquetMin1.Text = getRow.MinCapacity.ToString() + " - ";
                        lblBanquetMax1.Text = getRow.MaxCapicity.ToString();
                        lblBanquetMin2.Text = getRow.MinCapacity.ToString() + " - ";
                        lblBanquetMax2.Text = getRow.MaxCapicity.ToString();
                        if (Participant < Convert.ToInt32(getRow.MinCapacity.ToString()))
                        {
                            chkBoardroom.Visible = false;
                            chkBoardroom1.Visible = false;
                            chkBoardroom2.Visible = false;
                        }
                        else if (Participant > Convert.ToInt32(getRow.MaxCapicity.ToString()))
                        {
                            chkBoardroom.Visible = false;
                            chkBoardroom1.Visible = false;
                            chkBoardroom2.Visible = false;
                        }
                    }
                    else if (getRow.RoomShapeId == 5)
                    {

                        Label lblCocktailMin = (Label)e.Item.FindControl("lblCocktailMin");
                        Label lblCocktailMax = (Label)e.Item.FindControl("lblCocktailMax");
                        CheckBox chkCocktail = (CheckBox)e.Item.FindControl("chkCocktail");
                        Label lblCocktailMin1 = (Label)e.Item.FindControl("lblCocktailMin1");
                        Label lblCocktailMax1 = (Label)e.Item.FindControl("lblCocktailMax1");
                        CheckBox chkCocktail1 = (CheckBox)e.Item.FindControl("chkCocktail1");
                        Label lblCocktailMin2 = (Label)e.Item.FindControl("lblCocktailMin2");
                        Label lblCocktailMax2 = (Label)e.Item.FindControl("lblCocktailMax2");
                        CheckBox chkCocktail2 = (CheckBox)e.Item.FindControl("chkCocktail2");
                        chkCocktail.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','1','" + e.Item.ItemIndex + "','" + rptid + "','6','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondary + "','" + "repDivItem" + countRowSecondaryItem + "');");
                        chkCocktail1.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','2','" + e.Item.ItemIndex + "','" + rptid + "','6','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondary + "','" + "repDivItem" + countRowSecondaryItem + "');");
                        chkCocktail2.Attributes.Add("onclick", "javascript:CheckSecondary(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','0','" + e.Item.ItemIndex + "','" + rptid + "','6','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondary + "','" + "repDivItem" + countRowSecondaryItem + "');");
                        lblCocktailMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblCocktailMax.Text = getRow.MaxCapicity.ToString();
                        lblCocktailMin1.Text = getRow.MinCapacity.ToString() + " - ";
                        lblCocktailMax1.Text = getRow.MaxCapicity.ToString();
                        lblCocktailMin2.Text = getRow.MinCapacity.ToString() + " - ";
                        lblCocktailMax2.Text = getRow.MaxCapicity.ToString();
                        if (Participant < Convert.ToInt32(getRow.MinCapacity.ToString()))
                        {
                            chkCocktail.Visible = false;
                            chkCocktail1.Visible = false;
                            chkCocktail2.Visible = false;
                        }
                        else if (Participant > Convert.ToInt32(getRow.MaxCapicity.ToString()))
                        {
                            chkCocktail.Visible = false;
                            chkCocktail1.Visible = false;
                            chkCocktail2.Visible = false;
                        }
                    }
                }
            }
        }
    }
    public string rptid
    {
        get;
        set;
    }
    protected void rptSecondaryMeeting_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        //try
        //{
        if (Session["masterInput"] != null)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //for image,plan,room size and height from meeting table
                Image hotelImage = (Image)e.Item.FindControl("imgHotelImage");
                hotelImage.ImageUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "/MeetingRoomImage/" + DataBinder.Eval(e.Item.DataItem, "Picture");
                HyperLink PlanView = (HyperLink)e.Item.FindControl("linkviewPlan");
                if (DataBinder.Eval(e.Item.DataItem, "MRPlan") != "")
                {
                    PlanView.NavigateUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "Plan/" + DataBinder.Eval(e.Item.DataItem, "MRPlan");
                }
                else
                {
                    PlanView.Visible = false;
                }
                System.Web.UI.HtmlControls.HtmlControl divRptId = ((System.Web.UI.HtmlControls.HtmlControl)e.Item.FindControl("divRptId"));
                rptid = divRptId.ClientID;
                Repeater rptDays = (Repeater)e.Item.FindControl("rptDays");
                HiddenField hdnMId = (HiddenField)e.Item.FindControl("hdnMId");


                rptDays.DataSource = MyMeetingroom.Where(a => a.MeetingRoomId == Convert.ToInt64(hdnMId.Value));
                rptDays.DataBind();

            }
        }
        //}
        //catch (Exception ex)
        //{
        //    logger.Error(ex);
        //}

    }

    //Bind secondary meeting room for request gridview 
    int countRowSecondaryRequest = 1;
    protected void rptDaysRequest_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ViewMeetingRoomForRequest v = e.Item.DataItem as ViewMeetingRoomForRequest;
            TList<MeetingRoomConfig> lst = ObjMeetingRoomConfigManager.GetConfigByMeetingRoomID(v.MeetingRoomId);
            DateTime fromDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));
            Label lblDate = (Label)e.Item.FindControl("lblDate");
            System.Web.UI.HtmlControls.HtmlControl divFullday = ((System.Web.UI.HtmlControls.HtmlControl)e.Item.FindControl("divFullday"));
            System.Web.UI.HtmlControls.HtmlControl divMorning = ((System.Web.UI.HtmlControls.HtmlControl)e.Item.FindControl("divMorning"));
            System.Web.UI.HtmlControls.HtmlControl divAfternoon = ((System.Web.UI.HtmlControls.HtmlControl)e.Item.FindControl("divAfternoon"));
            if (e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var divContainer = e.Item.FindControl("repDiv") as HtmlGenericControl;
                divContainer.Attributes.Add("class", "repDiv" + countRowSecondaryRequest);
                countRowSecondaryRequest++;
                lblDate.Text = String.Format("{0:dd/MM/yyyy}", fromDate.AddDays(1).ToShortDateString());
                //if (((Session["masterInput"] as BookingRequest)).propDay2 != null)
                //{
                //    if (Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDay2) == 1)
                //    {
                //        divFullday.Attributes.Add("class", "redul");
                //        divAfternoon.Attributes.Add("class", "redul");
                //    }
                //    else if (Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDay2) == 2)
                //    {
                //        divMorning.Attributes.Add("class", "redul");
                //        divFullday.Attributes.Add("class", "redul");
                //    }
                //    else if (Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDay2) == 0)
                //    {
                //        divFullday.Attributes.Add("class", "");
                //        divMorning.Attributes.Add("class", "");
                //        divAfternoon.Attributes.Add("class", "");
                //    }
                //}
            }
            else
            {
                //if (Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDays) == 1)
                //{
                //    divFullday.Attributes.Add("class", "redul");
                //    divAfternoon.Attributes.Add("class", "redul");
                //}
                //else if (Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDays) == 2)
                //{
                //    divMorning.Attributes.Add("class", "redul");
                //    divFullday.Attributes.Add("class", "redul");
                //}
                //else
                //{
                //    divFullday.Attributes.Add("class", "");
                //    divMorning.Attributes.Add("class", "");
                //    divAfternoon.Attributes.Add("class", "");
                //}                
                lblDate.Text = String.Format("{0:dd/MM/yyyy}", fromDate.ToShortDateString());
            }


            int Participant = Convert.ToInt32(txtRequestedSecparticipants.Text);

            bool ChkRed = false;
            int intMinEnumValue = ObjMeetingRoomConfigManager.GetMinMeetingRoomShapeID();
            int intMaxEnumValue = ObjMeetingRoomConfigManager.GetMaxMeetingRoomShapeID();
            int intIndex;
            for (intIndex = intMinEnumValue; intIndex <= intMaxEnumValue; intIndex++)
            {

                var getRow = lst.Find(a => a.RoomShapeId == intIndex);
                if (getRow != null)
                {

                    if (getRow.RoomShapeId == 1)
                    {
                        Label lblTheatreMin = (Label)e.Item.FindControl("lblTheatreMin");
                        Label lblTheatreMax = (Label)e.Item.FindControl("lblTheatreMax");
                        CheckBox chkTheatre = (CheckBox)e.Item.FindControl("chkTheatre");
                        Label lblTheatreMin1 = (Label)e.Item.FindControl("lblTheatreMin1");
                        Label lblTheatreMax1 = (Label)e.Item.FindControl("lblTheatreMax1");
                        CheckBox chkTheatre1 = (CheckBox)e.Item.FindControl("chkTheatre1");
                        Label lblTheatreMin2 = (Label)e.Item.FindControl("lblTheatreMin2");
                        Label lblTheatreMax2 = (Label)e.Item.FindControl("lblTheatreMax2");
                        CheckBox chkTheatre2 = (CheckBox)e.Item.FindControl("chkTheatre2");
                        chkTheatre.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','1','" + e.Item.ItemIndex + "','" + rptid + "','2','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequest + "');");
                        chkTheatre1.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','2','" + e.Item.ItemIndex + "','" + rptid + "','2','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequest + "');");
                        chkTheatre2.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','0','" + e.Item.ItemIndex + "','" + rptid + "','2','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequest + "');");
                        lblTheatreMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblTheatreMax.Text = getRow.MaxCapicity.ToString();
                        lblTheatreMin1.Text = getRow.MinCapacity.ToString() + " - ";
                        lblTheatreMax1.Text = getRow.MaxCapicity.ToString();
                        lblTheatreMin2.Text = getRow.MinCapacity.ToString() + " - ";
                        lblTheatreMax2.Text = getRow.MaxCapicity.ToString();
                        if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                        {
                            lblTheatreMin.Text = "";
                            lblTheatreMax.Text = "";
                            lblTheatreMin1.Text = "";
                            lblTheatreMax1.Text = "";
                            lblTheatreMin2.Text = "";
                            lblTheatreMax2.Text = "";
                            chkTheatre.Visible = false;
                            chkTheatre1.Visible = false;
                            chkTheatre2.Visible = false;
                        }
                    }
                    else if (getRow.RoomShapeId == 2)
                    {

                        Label lblReceptionMin = (Label)e.Item.FindControl("lblReceptionMin");
                        Label lblReceptionMax = (Label)e.Item.FindControl("lblReceptionMax");
                        CheckBox chkSchool = (CheckBox)e.Item.FindControl("chkSchool");
                        Label lblReceptionMin1 = (Label)e.Item.FindControl("lblReceptionMin1");
                        Label lblReceptionMax1 = (Label)e.Item.FindControl("lblReceptionMax1");
                        CheckBox chkSchool1 = (CheckBox)e.Item.FindControl("chkSchool1");
                        Label lblReceptionMin2 = (Label)e.Item.FindControl("lblReceptionMin2");
                        Label lblReceptionMax2 = (Label)e.Item.FindControl("lblReceptionMax2");
                        CheckBox chkSchool2 = (CheckBox)e.Item.FindControl("chkSchool2");
                        chkSchool.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','1','" + e.Item.ItemIndex + "','" + rptid + "','3','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequest + "');");
                        chkSchool1.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','2','" + e.Item.ItemIndex + "','" + rptid + "','3','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequest + "');");
                        chkSchool2.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','0','" + e.Item.ItemIndex + "','" + rptid + "','3','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequest + "');");
                        lblReceptionMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblReceptionMax.Text = getRow.MaxCapicity.ToString();
                        lblReceptionMin1.Text = getRow.MinCapacity.ToString() + " - ";
                        lblReceptionMax1.Text = getRow.MaxCapicity.ToString();
                        lblReceptionMin2.Text = getRow.MinCapacity.ToString() + " - ";
                        lblReceptionMax2.Text = getRow.MaxCapicity.ToString();
                        if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                        {
                            lblReceptionMin.Text = "";
                            lblReceptionMax.Text = "";
                            lblReceptionMin1.Text = "";
                            lblReceptionMax1.Text = "";
                            lblReceptionMin2.Text = "";
                            lblReceptionMax2.Text = "";
                            chkSchool.Visible = false;
                            chkSchool1.Visible = false;
                            chkSchool2.Visible = false;
                        }

                    }
                    else if (getRow.RoomShapeId == 3)
                    {

                        Label lblUShapeMin = (Label)e.Item.FindControl("lblUShapeMin");
                        Label lblUShapeMax = (Label)e.Item.FindControl("lblUShapeMax");
                        CheckBox chkUshape = (CheckBox)e.Item.FindControl("chkUshape");
                        Label lblUShapeMin1 = (Label)e.Item.FindControl("lblUShapeMin1");
                        Label lblUShapeMax1 = (Label)e.Item.FindControl("lblUShapeMax1");
                        CheckBox chkUshape1 = (CheckBox)e.Item.FindControl("chkUshape1");
                        Label lblUShapeMin2 = (Label)e.Item.FindControl("lblUShapeMin2");
                        Label lblUShapeMax2 = (Label)e.Item.FindControl("lblUShapeMax2");
                        CheckBox chkUshape2 = (CheckBox)e.Item.FindControl("chkUshape2");
                        chkUshape.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','1','" + e.Item.ItemIndex + "','" + rptid + "','4','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequest + "');");
                        chkUshape1.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','2','" + e.Item.ItemIndex + "','" + rptid + "','4','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequest + "');");
                        chkUshape2.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','0','" + e.Item.ItemIndex + "','" + rptid + "','4','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequest + "');");
                        lblUShapeMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblUShapeMax.Text = getRow.MaxCapicity.ToString();
                        lblUShapeMin1.Text = getRow.MinCapacity.ToString() + " - ";
                        lblUShapeMax1.Text = getRow.MaxCapicity.ToString();
                        lblUShapeMin2.Text = getRow.MinCapacity.ToString() + " - ";
                        lblUShapeMax2.Text = getRow.MaxCapicity.ToString();
                        if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                        {
                            lblUShapeMin.Text = "";
                            lblUShapeMax.Text = "";
                            lblUShapeMin1.Text = "";
                            lblUShapeMax1.Text = "";
                            lblUShapeMin2.Text = "";
                            lblUShapeMax2.Text = "";
                            chkUshape.Visible = false;
                            chkUshape1.Visible = false;
                            chkUshape2.Visible = false;
                        }

                    }
                    else if (getRow.RoomShapeId == 4)
                    {

                        Label lblBanquetMin = (Label)e.Item.FindControl("lblBanquetMin");
                        Label lblBanquetMax = (Label)e.Item.FindControl("lblBanquetMax");
                        CheckBox chkBoardroom = (CheckBox)e.Item.FindControl("chkBoardroom");
                        Label lblBanquetMin1 = (Label)e.Item.FindControl("lblBanquetMin1");
                        Label lblBanquetMax1 = (Label)e.Item.FindControl("lblBanquetMax1");
                        CheckBox chkBoardroom1 = (CheckBox)e.Item.FindControl("chkBoardroom1");
                        Label lblBanquetMin2 = (Label)e.Item.FindControl("lblBanquetMin2");
                        Label lblBanquetMax2 = (Label)e.Item.FindControl("lblBanquetMax2");
                        CheckBox chkBoardroom2 = (CheckBox)e.Item.FindControl("chkBoardroom2");
                        chkBoardroom.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','1','" + e.Item.ItemIndex + "','" + rptid + "','5','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequest + "');");
                        chkBoardroom1.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','2','" + e.Item.ItemIndex + "','" + rptid + "','5','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequest + "');");
                        chkBoardroom2.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','0','" + e.Item.ItemIndex + "','" + rptid + "','5','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequest + "');");
                        lblBanquetMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblBanquetMax.Text = getRow.MaxCapicity.ToString();
                        lblBanquetMin1.Text = getRow.MinCapacity.ToString() + " - ";
                        lblBanquetMax1.Text = getRow.MaxCapicity.ToString();
                        lblBanquetMin2.Text = getRow.MinCapacity.ToString() + " - ";
                        lblBanquetMax2.Text = getRow.MaxCapicity.ToString();
                        if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                        {
                            lblBanquetMin.Text = "";
                            lblBanquetMax.Text = "";
                            lblBanquetMin1.Text = "";
                            lblBanquetMax1.Text = "";
                            lblBanquetMin2.Text = "";
                            lblBanquetMax2.Text = "";
                            chkBoardroom.Visible = false;
                            chkBoardroom1.Visible = false;
                            chkBoardroom2.Visible = false;
                        }
                    }
                    else if (getRow.RoomShapeId == 5)
                    {

                        Label lblCocktailMin = (Label)e.Item.FindControl("lblCocktailMin");
                        Label lblCocktailMax = (Label)e.Item.FindControl("lblCocktailMax");
                        CheckBox chkCocktail = (CheckBox)e.Item.FindControl("chkCocktail");
                        Label lblCocktailMin1 = (Label)e.Item.FindControl("lblCocktailMin1");
                        Label lblCocktailMax1 = (Label)e.Item.FindControl("lblCocktailMax1");
                        CheckBox chkCocktail1 = (CheckBox)e.Item.FindControl("chkCocktail1");
                        Label lblCocktailMin2 = (Label)e.Item.FindControl("lblCocktailMin2");
                        Label lblCocktailMax2 = (Label)e.Item.FindControl("lblCocktailMax2");
                        CheckBox chkCocktail2 = (CheckBox)e.Item.FindControl("chkCocktail2");
                        chkCocktail.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','1','" + e.Item.ItemIndex + "','" + rptid + "','6','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequest + "');");
                        chkCocktail1.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','2','" + e.Item.ItemIndex + "','" + rptid + "','6','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequest + "');");
                        chkCocktail2.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','0','" + e.Item.ItemIndex + "','" + rptid + "','6','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequest + "');");
                        lblCocktailMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblCocktailMax.Text = getRow.MaxCapicity.ToString();
                        lblCocktailMin1.Text = getRow.MinCapacity.ToString() + " - ";
                        lblCocktailMax1.Text = getRow.MaxCapicity.ToString();
                        lblCocktailMin2.Text = getRow.MinCapacity.ToString() + " - ";
                        lblCocktailMax2.Text = getRow.MaxCapicity.ToString();
                        if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                        {
                            lblCocktailMin.Text = "";
                            lblCocktailMax.Text = "";
                            lblCocktailMin1.Text = "";
                            lblCocktailMax1.Text = "";
                            lblCocktailMin2.Text = "";
                            lblCocktailMax2.Text = "";
                            chkCocktail.Visible = false;
                            chkCocktail1.Visible = false;
                            chkCocktail2.Visible = false;
                        }
                    }
                }
            }
        }
    }

    protected void rptRequestSecondary_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        //try
        //{
        if (Session["masterInput"] != null)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //for image,plan,room size and height from meeting table
                Image hotelImage = (Image)e.Item.FindControl("imgHotelImage");
                hotelImage.ImageUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "/MeetingRoomImage/" + DataBinder.Eval(e.Item.DataItem, "Picture");
                HyperLink PlanView = (HyperLink)e.Item.FindControl("linkviewPlan");
                if (DataBinder.Eval(e.Item.DataItem, "MRPlan") != "")
                {
                    PlanView.NavigateUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "Plan/" + DataBinder.Eval(e.Item.DataItem, "MRPlan");
                }
                else
                {
                    PlanView.Visible = false;
                }
                System.Web.UI.HtmlControls.HtmlControl divRptId = ((System.Web.UI.HtmlControls.HtmlControl)e.Item.FindControl("divRptId"));
                rptid = divRptId.ClientID;
                Repeater rptDays = (Repeater)e.Item.FindControl("rptDaysRequest");
                HiddenField hdnMId = (HiddenField)e.Item.FindControl("hdnMId");


                rptDays.DataSource = MyMeetingroomforRequest.Where(a => a.MeetingRoomId == Convert.ToInt64(hdnMId.Value));
                rptDays.DataBind();

            }
        }
        //}
        //catch (Exception ex)
        //{
        //    logger.Error(ex);
        //}

    }

    //Bind secondary meeting room for Request Tab
    int countRowSecondaryRequestTab = 1;
    protected void rptDaysTabRequest_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ViewMeetingRoomForRequest v = e.Item.DataItem as ViewMeetingRoomForRequest;
            TList<MeetingRoomConfig> lst = ObjMeetingRoomConfigManager.GetConfigByMeetingRoomID(v.MeetingRoomId);
            DateTime fromDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));
            Label lblDate = (Label)e.Item.FindControl("lblDate");
            System.Web.UI.HtmlControls.HtmlControl divFullday = ((System.Web.UI.HtmlControls.HtmlControl)e.Item.FindControl("divFullday"));
            System.Web.UI.HtmlControls.HtmlControl divMorning = ((System.Web.UI.HtmlControls.HtmlControl)e.Item.FindControl("divMorning"));
            System.Web.UI.HtmlControls.HtmlControl divAfternoon = ((System.Web.UI.HtmlControls.HtmlControl)e.Item.FindControl("divAfternoon"));
            if (e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var divContainer = e.Item.FindControl("repDiv") as HtmlGenericControl;
                divContainer.Attributes.Add("class", "repDiv" + countRowSecondaryRequestTab);
                countRowSecondaryRequestTab++;
                lblDate.Text = String.Format("{0:dd/MM/yyyy}", fromDate.AddDays(1).ToShortDateString());
                //if (((Session["masterInput"] as BookingRequest)).propDay2 != null)
                //{
                //    if (Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDay2) == 1)
                //    {
                //        divFullday.Attributes.Add("class", "redul");
                //        divAfternoon.Attributes.Add("class", "redul");
                //    }
                //    else if (Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDay2) == 2)
                //    {
                //        divMorning.Attributes.Add("class", "redul");
                //        divFullday.Attributes.Add("class", "redul");
                //    }
                //    else
                //    {
                //        divFullday.Attributes.Add("class", "");
                //        divMorning.Attributes.Add("class", "");
                //        divAfternoon.Attributes.Add("class", "");
                //    }
                //}
            }
            else
            {
                //if (Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDays) == 1)
                //{
                //    divFullday.Attributes.Add("class", "redul");
                //    divAfternoon.Attributes.Add("class", "redul");
                //}
                //else if (Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDays) == 2)
                //{
                //    divMorning.Attributes.Add("class", "redul");
                //    divFullday.Attributes.Add("class", "redul");
                //}
                //else
                //{
                //    divFullday.Attributes.Add("class", "");
                //    divMorning.Attributes.Add("class", "");
                //    divAfternoon.Attributes.Add("class", "");
                //}                
                lblDate.Text = String.Format("{0:dd/MM/yyyy}", fromDate.ToShortDateString());
            }


            int Participant = Convert.ToInt32(txtRequestedSecparticipants.Text);

            bool ChkRed = false;
            int intMinEnumValue = ObjMeetingRoomConfigManager.GetMinMeetingRoomShapeID();
            int intMaxEnumValue = ObjMeetingRoomConfigManager.GetMaxMeetingRoomShapeID();
            int intIndex;
            for (intIndex = intMinEnumValue; intIndex <= intMaxEnumValue; intIndex++)
            {

                var getRow = lst.Find(a => a.RoomShapeId == intIndex);
                if (getRow != null)
                {

                    if (getRow.RoomShapeId == 1)
                    {
                        Label lblTheatreMin = (Label)e.Item.FindControl("lblTheatreMin");
                        Label lblTheatreMax = (Label)e.Item.FindControl("lblTheatreMax");
                        CheckBox chkTheatre = (CheckBox)e.Item.FindControl("chkTheatre");
                        Label lblTheatreMin1 = (Label)e.Item.FindControl("lblTheatreMin1");
                        Label lblTheatreMax1 = (Label)e.Item.FindControl("lblTheatreMax1");
                        CheckBox chkTheatre1 = (CheckBox)e.Item.FindControl("chkTheatre1");
                        Label lblTheatreMin2 = (Label)e.Item.FindControl("lblTheatreMin2");
                        Label lblTheatreMax2 = (Label)e.Item.FindControl("lblTheatreMax2");
                        CheckBox chkTheatre2 = (CheckBox)e.Item.FindControl("chkTheatre2");
                        chkTheatre.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','1','" + e.Item.ItemIndex + "','" + rptid + "','2','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequestTab + "');");
                        chkTheatre1.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','2','" + e.Item.ItemIndex + "','" + rptid + "','2','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequestTab + "');");
                        chkTheatre2.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','0','" + e.Item.ItemIndex + "','" + rptid + "','2','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequestTab + "');");
                        lblTheatreMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblTheatreMax.Text = getRow.MaxCapicity.ToString();
                        lblTheatreMin1.Text = getRow.MinCapacity.ToString() + " - ";
                        lblTheatreMax1.Text = getRow.MaxCapicity.ToString();
                        lblTheatreMin2.Text = getRow.MinCapacity.ToString() + " - ";
                        lblTheatreMax2.Text = getRow.MaxCapicity.ToString();
                        if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                        {
                            lblTheatreMin.Text = "";
                            lblTheatreMax.Text = "";
                            lblTheatreMin1.Text = "";
                            lblTheatreMax1.Text = "";
                            lblTheatreMin2.Text = "";
                            lblTheatreMax2.Text = "";
                            chkTheatre.Visible = false;
                            chkTheatre1.Visible = false;
                            chkTheatre2.Visible = false;
                        }
                    }
                    else if (getRow.RoomShapeId == 2)
                    {

                        Label lblReceptionMin = (Label)e.Item.FindControl("lblReceptionMin");
                        Label lblReceptionMax = (Label)e.Item.FindControl("lblReceptionMax");
                        CheckBox chkSchool = (CheckBox)e.Item.FindControl("chkSchool");
                        Label lblReceptionMin1 = (Label)e.Item.FindControl("lblReceptionMin1");
                        Label lblReceptionMax1 = (Label)e.Item.FindControl("lblReceptionMax1");
                        CheckBox chkSchool1 = (CheckBox)e.Item.FindControl("chkSchool1");
                        Label lblReceptionMin2 = (Label)e.Item.FindControl("lblReceptionMin2");
                        Label lblReceptionMax2 = (Label)e.Item.FindControl("lblReceptionMax2");
                        CheckBox chkSchool2 = (CheckBox)e.Item.FindControl("chkSchool2");
                        chkSchool.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','1','" + e.Item.ItemIndex + "','" + rptid + "','3','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequestTab + "');");
                        chkSchool1.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','2','" + e.Item.ItemIndex + "','" + rptid + "','3','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequestTab + "');");
                        chkSchool2.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','0','" + e.Item.ItemIndex + "','" + rptid + "','3','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequestTab + "');");
                        lblReceptionMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblReceptionMax.Text = getRow.MaxCapicity.ToString();
                        lblReceptionMin1.Text = getRow.MinCapacity.ToString() + " - ";
                        lblReceptionMax1.Text = getRow.MaxCapicity.ToString();
                        lblReceptionMin2.Text = getRow.MinCapacity.ToString() + " - ";
                        lblReceptionMax2.Text = getRow.MaxCapicity.ToString();
                        if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                        {
                            lblReceptionMin.Text = "";
                            lblReceptionMax.Text = "";
                            lblReceptionMin1.Text = "";
                            lblReceptionMax1.Text = "";
                            lblReceptionMin2.Text = "";
                            lblReceptionMax2.Text = "";
                            chkSchool.Visible = false;
                            chkSchool1.Visible = false;
                            chkSchool2.Visible = false;
                        }

                    }
                    else if (getRow.RoomShapeId == 3)
                    {

                        Label lblUShapeMin = (Label)e.Item.FindControl("lblUShapeMin");
                        Label lblUShapeMax = (Label)e.Item.FindControl("lblUShapeMax");
                        CheckBox chkUshape = (CheckBox)e.Item.FindControl("chkUshape");
                        Label lblUShapeMin1 = (Label)e.Item.FindControl("lblUShapeMin1");
                        Label lblUShapeMax1 = (Label)e.Item.FindControl("lblUShapeMax1");
                        CheckBox chkUshape1 = (CheckBox)e.Item.FindControl("chkUshape1");
                        Label lblUShapeMin2 = (Label)e.Item.FindControl("lblUShapeMin2");
                        Label lblUShapeMax2 = (Label)e.Item.FindControl("lblUShapeMax2");
                        CheckBox chkUshape2 = (CheckBox)e.Item.FindControl("chkUshape2");
                        chkUshape.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','1','" + e.Item.ItemIndex + "','" + rptid + "','4','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequestTab + "');");
                        chkUshape1.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','2','" + e.Item.ItemIndex + "','" + rptid + "','4','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequestTab + "');");
                        chkUshape2.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','0','" + e.Item.ItemIndex + "','" + rptid + "','4','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequestTab + "');");
                        lblUShapeMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblUShapeMax.Text = getRow.MaxCapicity.ToString();
                        lblUShapeMin1.Text = getRow.MinCapacity.ToString() + " - ";
                        lblUShapeMax1.Text = getRow.MaxCapicity.ToString();
                        lblUShapeMin2.Text = getRow.MinCapacity.ToString() + " - ";
                        lblUShapeMax2.Text = getRow.MaxCapicity.ToString();
                        if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                        {
                            lblUShapeMin.Text = "";
                            lblUShapeMax.Text = "";
                            lblUShapeMin1.Text = "";
                            lblUShapeMax1.Text = "";
                            lblUShapeMin2.Text = "";
                            lblUShapeMax2.Text = "";
                            chkUshape.Visible = false;
                            chkUshape1.Visible = false;
                            chkUshape2.Visible = false;
                        }
                    }
                    else if (getRow.RoomShapeId == 4)
                    {

                        Label lblBanquetMin = (Label)e.Item.FindControl("lblBanquetMin");
                        Label lblBanquetMax = (Label)e.Item.FindControl("lblBanquetMax");
                        CheckBox chkBoardroom = (CheckBox)e.Item.FindControl("chkBoardroom");
                        Label lblBanquetMin1 = (Label)e.Item.FindControl("lblBanquetMin1");
                        Label lblBanquetMax1 = (Label)e.Item.FindControl("lblBanquetMax1");
                        CheckBox chkBoardroom1 = (CheckBox)e.Item.FindControl("chkBoardroom1");
                        Label lblBanquetMin2 = (Label)e.Item.FindControl("lblBanquetMin2");
                        Label lblBanquetMax2 = (Label)e.Item.FindControl("lblBanquetMax2");
                        CheckBox chkBoardroom2 = (CheckBox)e.Item.FindControl("chkBoardroom2");
                        chkBoardroom.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','1','" + e.Item.ItemIndex + "','" + rptid + "','5','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequestTab + "');");
                        chkBoardroom1.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','2','" + e.Item.ItemIndex + "','" + rptid + "','5','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequestTab + "');");
                        chkBoardroom2.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','0','" + e.Item.ItemIndex + "','" + rptid + "','5','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequestTab + "');");
                        lblBanquetMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblBanquetMax.Text = getRow.MaxCapicity.ToString();
                        lblBanquetMin1.Text = getRow.MinCapacity.ToString() + " - ";
                        lblBanquetMax1.Text = getRow.MaxCapicity.ToString();
                        lblBanquetMin2.Text = getRow.MinCapacity.ToString() + " - ";
                        lblBanquetMax2.Text = getRow.MaxCapicity.ToString();
                        if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                        {
                            lblBanquetMin.Text = "";
                            lblBanquetMax.Text = "";
                            lblBanquetMin1.Text = "";
                            lblBanquetMax1.Text = "";
                            lblBanquetMin2.Text = "";
                            lblBanquetMax2.Text = "";
                            chkBoardroom.Visible = false;
                            chkBoardroom1.Visible = false;
                            chkBoardroom2.Visible = false;
                        }
                    }
                    else if (getRow.RoomShapeId == 5)
                    {

                        Label lblCocktailMin = (Label)e.Item.FindControl("lblCocktailMin");
                        Label lblCocktailMax = (Label)e.Item.FindControl("lblCocktailMax");
                        CheckBox chkCocktail = (CheckBox)e.Item.FindControl("chkCocktail");
                        Label lblCocktailMin1 = (Label)e.Item.FindControl("lblCocktailMin1");
                        Label lblCocktailMax1 = (Label)e.Item.FindControl("lblCocktailMax1");
                        CheckBox chkCocktail1 = (CheckBox)e.Item.FindControl("chkCocktail1");
                        Label lblCocktailMin2 = (Label)e.Item.FindControl("lblCocktailMin2");
                        Label lblCocktailMax2 = (Label)e.Item.FindControl("lblCocktailMax2");
                        CheckBox chkCocktail2 = (CheckBox)e.Item.FindControl("chkCocktail2");
                        chkCocktail.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','1','" + e.Item.ItemIndex + "','" + rptid + "','6','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequestTab + "');");
                        chkCocktail1.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','2','" + e.Item.ItemIndex + "','" + rptid + "','6','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequestTab + "');");
                        chkCocktail2.Attributes.Add("onclick", "javascript:CheckSecondaryRequest(this,'" + v.MeetingRoomId + "','" + getRow.Id + "','0','" + e.Item.ItemIndex + "','" + rptid + "','6','" + ViewState["btnVar"] + "','" + "repDiv" + countRowSecondaryRequestTab + "');");
                        lblCocktailMin.Text = getRow.MinCapacity.ToString() + " - ";
                        lblCocktailMax.Text = getRow.MaxCapicity.ToString();
                        lblCocktailMin1.Text = getRow.MinCapacity.ToString() + " - ";
                        lblCocktailMax1.Text = getRow.MaxCapicity.ToString();
                        lblCocktailMin2.Text = getRow.MinCapacity.ToString() + " - ";
                        lblCocktailMax2.Text = getRow.MaxCapicity.ToString();
                        if (getRow.MinCapacity.ToString() == "0" && getRow.MaxCapicity.ToString() == "0")
                        {
                            lblCocktailMin.Text = "";
                            lblCocktailMax.Text = "";
                            lblCocktailMin1.Text = "";
                            lblCocktailMax1.Text = "";
                            lblCocktailMin2.Text = "";
                            lblCocktailMax2.Text = "";
                            chkCocktail.Visible = false;
                            chkCocktail1.Visible = false;
                            chkCocktail2.Visible = false;
                        }
                    }
                }
            }
        }
    }

    protected void rptRequestTabSecondary_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        //try
        //{
        if (Session["masterInput"] != null)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //for image,plan,room size and height from meeting table
                Image hotelImage = (Image)e.Item.FindControl("imgHotelImage");
                hotelImage.ImageUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "/MeetingRoomImage/" + DataBinder.Eval(e.Item.DataItem, "Picture");
                HyperLink PlanView = (HyperLink)e.Item.FindControl("linkviewPlan");
                if (DataBinder.Eval(e.Item.DataItem, "MRPlan") != "")
                {
                    PlanView.NavigateUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "Plan/" + DataBinder.Eval(e.Item.DataItem, "MRPlan");
                }
                else
                {
                    PlanView.Visible = false;
                }
                System.Web.UI.HtmlControls.HtmlControl divRptId = ((System.Web.UI.HtmlControls.HtmlControl)e.Item.FindControl("divRptId"));
                rptid = divRptId.ClientID;
                Repeater rptDays = (Repeater)e.Item.FindControl("rptDaysTabRequest");
                HiddenField hdnMId = (HiddenField)e.Item.FindControl("hdnMId");


                rptDays.DataSource = MyMeetingroomforRequest.Where(a => a.MeetingRoomId == Convert.ToInt64(hdnMId.Value));
                rptDays.DataBind();

            }
        }
        //}
        //catch (Exception ex)
        //{
        //    logger.Error(ex);
        //}

    }
    #endregion

    #region Continue with checkout for booking
    //Send variable to booking step1 (Continue with checkout)
    protected void lbnContinue_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["masterInput"] != null)
            {
                if (Convert.ToString(Session["hotel"]) != "")
                {
                    objmeetingroom = (TList<MeetingRoom>)Session["meetingroom"];
                    objhotel = (TList<Hotel>)Session["hotel"];
                    objConfig = (TList<MeetingRoomConfig>)Session["meetingroomConfig"];

                    DateTime fromDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));
                    Createbooking objCreateBooking = new Createbooking();
                    objCreateBooking.HotelID = Convert.ToInt32(hdnHotelId.Value);
                    //Convert.ToInt32(hdnMeetingRoomId.Value)
                    //Set All the required Field
                    Hotel objHotelDetail = new HotelManager().GetHotelDetailsById(objCreateBooking.HotelID);
                    objCreateBooking.ArivalDate = fromDate;
                    objCreateBooking.DepartureDate = fromDate.AddDays(Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDuration) - 1);
                    if (Session["CurrentUserSeleted"] != null)
                    {
                        objCreateBooking.CurrentUserId = Convert.ToInt64(Session["CurrentUserSeleted"]);
                    }
                    else
                    {
                        objCreateBooking.CurrentUserId = 0;
                    }
                    objCreateBooking.Duration = Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDuration);
                    //List of Selected meetingroom details
                    objCreateBooking.MeetingroomList = new List<BookedMR>();
                    for (int i = 0; i < objmeetingroom.Count; i++)
                    {
                        BookedMR objBookedMR = new BookedMR();
                        if (i == 0)
                        {

                            objBookedMR.MRId = objmeetingroom[0].Id;
                            objBookedMR.MrConfigId = objConfig[0].Id;
                            //Set Configure for all Meetingroom
                            objBookedMR.MrDetails = new List<BookedMrConfig>();
                            for (int j = 0; j < Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDuration); j++)
                            {
                                BookedMrConfig objBookedMrConfig = new BookedMrConfig();
                                objBookedMrConfig.NoOfParticepant = Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants);
                                if (j == 0)
                                {
                                    objBookedMrConfig.SelectedDay = 1;
                                    objBookedMrConfig.SelectedTime = Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDays);//FullDay 0 | Morning 1| afrenoon 2
                                    objBookedMrConfig.FromTime = objBookedMrConfig.SelectedTime == 0 ? objHotelDetail.RtFFrom : (objBookedMrConfig.SelectedTime == 1 ? objHotelDetail.RtMFrom : (objBookedMrConfig.SelectedTime == 2 ? objHotelDetail.RtAFrom : objHotelDetail.RtFFrom));
                                    objBookedMrConfig.ToTime = objBookedMrConfig.SelectedTime == 0 ? objHotelDetail.RtFTo : (objBookedMrConfig.SelectedTime == 1 ? objHotelDetail.RtMTo : (objBookedMrConfig.SelectedTime == 2 ? objHotelDetail.RtATo : objHotelDetail.RtFTo));
                                }
                                else
                                {
                                    objBookedMrConfig.SelectedDay = 2;
                                    objBookedMrConfig.SelectedTime = Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDay2);
                                    objBookedMrConfig.FromTime = objBookedMrConfig.SelectedTime == 0 ? objHotelDetail.RtFFrom : (objBookedMrConfig.SelectedTime == 1 ? objHotelDetail.RtMFrom : (objBookedMrConfig.SelectedTime == 2 ? objHotelDetail.RtAFrom : objHotelDetail.RtFFrom));
                                    objBookedMrConfig.ToTime = objBookedMrConfig.SelectedTime == 0 ? objHotelDetail.RtFTo : (objBookedMrConfig.SelectedTime == 1 ? objHotelDetail.RtMTo : (objBookedMrConfig.SelectedTime == 2 ? objHotelDetail.RtATo : objHotelDetail.RtFTo));
                                }
                                objBookedMR.MrDetails.Add(objBookedMrConfig);
                            }
                        }
                        if (i == 1)
                        {
                            objBookedMR.MRId = objmeetingroom[1].Id;
                            objBookedMR.MrConfigId = objConfig[1].Id;
                            //Set Configure for all Meetingroom
                            objBookedMR.MrDetails = new List<BookedMrConfig>();
                            for (int j = 0; j < Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDuration); j++)
                            {
                                BookedMrConfig objBookedMrConfig = new BookedMrConfig();
                                if (txtSecparticipants.Text != null || txtSecparticipants.Text != "")
                                {
                                    objBookedMrConfig.NoOfParticepant = Convert.ToInt32(txtSecparticipants.Text);
                                }
                                if (j == 0)
                                {
                                    if (!string.IsNullOrEmpty(hdnType.Value))
                                    {
                                        objBookedMrConfig.SelectedDay = 1;
                                        //objBookedMrConfig.SelectedTime = Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDays);//FullDay 0 | Morning 1| afrenoon 2
                                        objBookedMrConfig.SelectedTime = Convert.ToInt32(hdnType.Value);//FullDay 0 | Morning 1| afrenoon 2
                                        objBookedMrConfig.FromTime = objBookedMrConfig.SelectedTime == 0 ? objHotelDetail.RtFFrom : (objBookedMrConfig.SelectedTime == 1 ? objHotelDetail.RtMFrom : (objBookedMrConfig.SelectedTime == 2 ? objHotelDetail.RtAFrom : objHotelDetail.RtFFrom));
                                        objBookedMrConfig.ToTime = objBookedMrConfig.SelectedTime == 0 ? objHotelDetail.RtFTo : (objBookedMrConfig.SelectedTime == 1 ? objHotelDetail.RtMTo : (objBookedMrConfig.SelectedTime == 2 ? objHotelDetail.RtATo : objHotelDetail.RtFTo));
                                    }
                                    if (!string.IsNullOrEmpty(hdnType.Value))
                                    {
                                        objBookedMR.MrDetails.Add(objBookedMrConfig);
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(hdnType2.Value))
                                    {
                                        objBookedMrConfig.SelectedDay = 2;
                                        objBookedMrConfig.SelectedTime = Convert.ToInt32(hdnType2.Value);//FullDay 0 | Morning 1| afrenoon 2
                                        objBookedMrConfig.FromTime = objBookedMrConfig.SelectedTime == 0 ? objHotelDetail.RtFFrom : (objBookedMrConfig.SelectedTime == 1 ? objHotelDetail.RtMFrom : (objBookedMrConfig.SelectedTime == 2 ? objHotelDetail.RtAFrom : objHotelDetail.RtFFrom));
                                        objBookedMrConfig.ToTime = objBookedMrConfig.SelectedTime == 0 ? objHotelDetail.RtFTo : (objBookedMrConfig.SelectedTime == 1 ? objHotelDetail.RtMTo : (objBookedMrConfig.SelectedTime == 2 ? objHotelDetail.RtATo : objHotelDetail.RtFTo));
                                    }
                                    if (!string.IsNullOrEmpty(hdnType2.Value))
                                    {
                                        objBookedMR.MrDetails.Add(objBookedMrConfig);
                                    }
                                }

                            }
                        }
                        objCreateBooking.MeetingroomList.Add(objBookedMR);
                    }

                    //For Statistics
                    Viewstatistics objview = new Viewstatistics();
                    Statistics ST = new Statistics();
                    ST.HotelId = Convert.ToInt32(hdnHotelId.Value);
                    ST.SensitiveBooking = 1;
                    ST.StatDate = System.DateTime.Now;
                    objview.InsertVisitwithBookingid(ST);
                    //For Statistics

                    //After Complete this part send this object in term of Session to the Bokking 1st Step Page.
                    //Session["Search"] = objCreateBooking;
                    SearchTracer st = new SearchTracer();
                    st = new SearchTracer();
                    st.SearchId = Guid.NewGuid().ToString();

                    if (Session["CurrentAgencyUserID"] != null)
                    {
                        st.UserId = Convert.ToInt64(Session["CurrentAgencyUserID"]);
                    }
                    else
                    {
                        st.UserId = null;
                    }
                    st.CurrentStep = 1;
                    st.IsBooking = true;
                    st.CurrentDay = 1;
                    Session["Search"] = objCreateBooking;
                    string serailstring = TrailManager.XmlSerialize(objCreateBooking);
                    st.SearchObject = serailstring;
                    if (new BookingManager().SaveSearch(st))
                    {
                        Session["SerachID"] = st.SearchId;
                        Response.Redirect("~/Agency/Booking.aspx");
                    }
                }
                else
                {
                    Response.Redirect("~/login.aspx", true);
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    #region Continue with checkout for request
    //Send variable to booking step1 (Continue with checkout)
    protected void lnbContinueRequest_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["masterInput"] != null)
            {
                if (Convert.ToString(Session["Requestedhotel"]) != "")
                {
                    objRequestedmeetingroom = (TList<MeetingRoom>)Session["Requestedmeetingroom"];
                    objRequestedHotel = (TList<Hotel>)Session["Requestedhotel"];
                    objRequestedMeetingConfig = (TList<MeetingRoomConfig>)Session["RequestedmeetingroomConfig"];

                    DateTime fromDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));

                    CreateRequest objPrepareRequest = new CreateRequest();
                    objPrepareRequest.RequestID = "";
                    objPrepareRequest.Duration = Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDuration);
                    objPrepareRequest.ArivalDate = fromDate;
                    objPrepareRequest.DepartureDate = objPrepareRequest.ArivalDate.AddDays(objPrepareRequest.Duration - 1);
                    if (Session["CurrentUserSeleted"] != null)
                    {
                        objPrepareRequest.CurrentUserId = Convert.ToInt64(Session["CurrentUserSeleted"]);
                    }
                    else
                    {
                        objPrepareRequest.CurrentUserId = 0;
                    }
                    if (objPrepareRequest.DaysList == null)
                    {
                        objPrepareRequest.DaysList = new List<NumberOfDays>();
                    }
                    for (int i = 0; i < objPrepareRequest.Duration; i++)
                    {
                        NumberOfDays objNoDays = new NumberOfDays();
                        objNoDays.SelectedDay = i + 1;
                        if (i == 0)
                        {
                            objNoDays.SelectTime = Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDays);
                        }
                        else
                        {
                            objNoDays.SelectTime = Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDay2);
                        }
                        objNoDays.StartTime = objNoDays.SelectTime == 0 ? "00:00 am" : (objNoDays.SelectTime == 1 ? "00:00 am" : "12:00 pm");
                        objNoDays.EndTime = objNoDays.SelectTime == 0 ? "11:30 pm" : (objNoDays.SelectTime == 1 ? "12:00 pm" : "11:30 pm"); ;
                        objPrepareRequest.DaysList.Add(objNoDays);
                    }
                    if (objPrepareRequest.HotelList == null)
                    {
                        objPrepareRequest.HotelList = new List<BuildHotelsRequest>();
                    }

                    Dictionary<long, Dictionary<long, string>> participants = Session["participants"] as Dictionary<long, Dictionary<long, string>>;
                    foreach (Hotel h in objRequestedHotel)
                    {
                        BuildHotelsRequest obj1Hotel = new BuildHotelsRequest();
                        obj1Hotel.HotelID = h.Id;
                        if (obj1Hotel.MeetingroomList == null)
                        {
                            obj1Hotel.MeetingroomList = new List<BuildMeetingRoomRequest>();
                        }
                        int cou = 0;
                        var values = participants[h.Id];
                        foreach (MeetingRoom m in objRequestedmeetingroom.Where(a => a.HotelId == h.Id))
                        {
                            BuildMeetingRoomRequest obj1mr1 = new BuildMeetingRoomRequest();
                            obj1mr1.ConfigurationID = objRequestedMeetingConfig.Where(a => a.MeetingRoomId == m.Id).FirstOrDefault().Id;
                            obj1mr1.MeetingRoomID = m.Id;

                            obj1mr1.Quantity = Convert.ToInt32(values[m.Id]);
                            obj1mr1.IsMain = cou == 0 ? true : false;
                            obj1Hotel.MeetingroomList.Add(obj1mr1);
                            cou++;
                        }
                        objPrepareRequest.HotelList.Add(obj1Hotel);
                    }
                    #region Commented By GAurav
                    //BuildMeetingRoomRequest obj1mr1 = new BuildMeetingRoomRequest();
                    //obj1mr1.ConfigurationID = 1176;
                    //obj1mr1.MeetingRoomID = 245;
                    //obj1mr1.Quantity = 50;
                    //obj1mr1.IsMain = true;
                    //obj1Hotel.MeetingroomList.Add(obj1mr1);
                    //BuildMeetingRoomRequest obj1mr2 = new BuildMeetingRoomRequest();
                    //obj1mr2.ConfigurationID = 1181;
                    //obj1mr2.MeetingRoomID = 246;
                    //obj1mr2.Quantity = 55;
                    //obj1mr2.IsMain = false;
                    //obj1Hotel.MeetingroomList.Add(obj1mr2);
                    //objPrepareRequest.HotelList.Add(obj1Hotel);

                    //BuildHotelsRequest obj2Hotel = new BuildHotelsRequest();
                    //obj2Hotel.HotelID = 31;
                    //if (obj2Hotel.MeetingroomList == null)
                    //{
                    //    obj2Hotel.MeetingroomList = new List<BuildMeetingRoomRequest>();
                    //}
                    //BuildMeetingRoomRequest obj2mr1 = new BuildMeetingRoomRequest();
                    //obj2mr1.ConfigurationID = 1502;
                    //obj2mr1.MeetingRoomID = 310;
                    //obj2mr1.Quantity = 50;
                    //obj2mr1.IsMain = true;
                    //obj2Hotel.MeetingroomList.Add(obj2mr1);
                    //BuildMeetingRoomRequest obj2mr2 = new BuildMeetingRoomRequest();
                    //obj2mr2.ConfigurationID = 1508;
                    //obj2mr2.MeetingRoomID = 311;
                    //obj2mr2.Quantity = 55;
                    //obj2mr2.IsMain = false;
                    //obj2Hotel.MeetingroomList.Add(obj2mr2);
                    //objPrepareRequest.HotelList.Add(obj2Hotel);

                    //BuildHotelsRequest obj3Hotel = new BuildHotelsRequest();
                    //obj3Hotel.HotelID = 46;
                    //if (obj3Hotel.MeetingroomList == null)
                    //{
                    //    obj3Hotel.MeetingroomList = new List<BuildMeetingRoomRequest>();
                    //}
                    //BuildMeetingRoomRequest obj3mr1 = new BuildMeetingRoomRequest();
                    //obj3mr1.ConfigurationID = 1838;
                    //obj3mr1.MeetingRoomID = 377;
                    //obj3mr1.Quantity = 50;
                    //obj3mr1.IsMain = true;
                    //obj3Hotel.MeetingroomList.Add(obj3mr1);
                    //BuildMeetingRoomRequest obj3mr2 = new BuildMeetingRoomRequest();
                    //obj3mr2.ConfigurationID = 1844;
                    //obj3mr2.MeetingRoomID = 378;
                    //obj3mr2.Quantity = 55;
                    //obj3mr2.IsMain = false;
                    //obj3Hotel.MeetingroomList.Add(obj3mr2);
                    //objPrepareRequest.HotelList.Add(obj3Hotel);
                    #endregion
                    SearchTracer st = null;
                    if (st != null)
                    {
                        //objSearch.SearchId = Convert.ToString(Session["SerachID"]);
                    }
                    else
                    {
                        st = new SearchTracer();
                        st.SearchId = Guid.NewGuid().ToString();
                    }

                    if (Session["CurrentAgencyUserID"] != null)
                    {
                        st.UserId = Convert.ToInt64(Session["CurrentAgencyUserID"]);
                    }
                    else
                    {
                        st.UserId = null;
                    }

                    Session["Request"] = objPrepareRequest;

                    string serailstring = TrailManager.XmlSerialize(objPrepareRequest);
                    st.SearchObject = serailstring;

                    st.CurrentStep = 1;
                    st.IsBooking = false;
                    st.CurrentDay = 1;
                    if (new BookingManager().SaveSearch(st))
                    {
                        Session["RequestID"] = st.SearchId;
                        Response.Redirect("~/Agency/Request.aspx");
                    }
                }
                else
                {
                    Response.Redirect("~/login.aspx", true);
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    //------------------------new implementations by tariq---------------------

    #region Binds the distanceTo dropdownlist.
    public void BindDistanceToDDL()
    {
        TList<MainPoint> temp = ObjHotelinfo.getAreasByCityId(Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity));
        drpDistanceBooking.DataValueField = "MainPointID";
        drpDistanceBooking.DataTextField = "MainPointName";
        drpDistanceBooking.DataSource = temp;
        drpDistanceBooking.DataBind();
        drpDistanceBooking.Items.Insert(0, new ListItem("CLEAR", "0"));

        drpDistanceRequest.DataValueField = "MainPointID";
        drpDistanceRequest.DataTextField = "MainPointName";
        drpDistanceRequest.DataSource = temp;
        drpDistanceRequest.DataBind();
        drpDistanceRequest.Items.Insert(0, new ListItem("CLEAR", "0"));
    }
    #endregion

    #region Binds the Themes dropdownlist.
    public void bindThemeDDL()
    {
        themeDDL.SelectedIndex = 0;
        //themeDDL.Items.Add(new ListItem("--Select--", "0"));
        //themeDDL.Items.Add(new ListItem("Luxury", "Luxury"));
        //themeDDL.Items.Add(new ListItem("Chique", "Chique"));
        //themeDDL.Items.Add(new ListItem("Trendy", "Trendy"));
        //themeDDL.Items.Add(new ListItem("Modern", "Modern"));
        //themeDDL.Items.Add(new ListItem("Fashion", "Fashion"));
        //themeDDL.Items.Add(new ListItem("Budget", "Budget"));
        //themeDDL.Items.Add(new ListItem("Rural", "Rural"));
        //themeDDL.Items.Add(new ListItem("Classic", "Classic"));
        //themeDDL.Items.Add(new ListItem("Golf Club", "Golf Club"));
        //themeDDL.Items.Add(new ListItem("Castle", "Castle"));
    }
    #endregion

    /// <summary>
    /// Binds the Zones dropdownlist.
    /// </summary>
    public void bindZonesDDL()
    {
        TList<Zone> zones = new TList<Zone>();
        string whereclaus = Convert.ToString(Session["Where"]);
        if (Convert.ToString(((Session["masterInput"] as BookingRequest)).propDuration) == "2")
        {
            string whereclaus2 = Convert.ToString(Session["Where2"]);
            vlist2day = objBookingRequest.GetBookingReqDetails(whereclaus2, string.Empty).FindAllDistinct(BookingRequestViewListColumn.HotelId);
            Tempvlist = objBookingRequest.GetBookingReqDetails(whereclaus, string.Empty).FindAllDistinct(BookingRequestViewListColumn.HotelId);
            vlist = new VList<BookingRequestViewList>();
            for (int i = 0; i < Tempvlist.Count; i++)
            {
                BookingRequestViewList b = vlist2day.Where(a => a.HotelId == Tempvlist[i].HotelId && a.MeetingRoomId == Tempvlist[i].MeetingRoomId).FirstOrDefault();
                if (b != null)
                {
                    vlist.Add(b);
                }
            }
        }
        else
        {
            vlist = objBookingRequest.GetBookingReqDetails(whereclaus, string.Empty).FindAllDistinct(BookingRequestViewListColumn.HotelId);
        }

        foreach (BookingRequestViewList temp in vlist.FindAllDistinct("ZoneId"))
        {
            zones.Add(ObjHotelinfo.getZoneById(temp.ZoneId));
        }
        zonesDDL.DataTextField = "Zone";
        zonesDDL.DataValueField = "Id";
        zonesDDL.DataSource = zones;
        zonesDDL.DataBind();
        if (Session["Zone"] != null)
        {
            zonesDDL.SelectedValue = Session["Zone"].ToString();
        }

        zonesDDL.Items.Insert(0, new ListItem("--Select zone--", "0"));

        TList<Zone> reqZone = new TList<Zone>();
        BokingDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));
        string whereclausReq = string.Empty;
        if (Session["Zone"] != null)
        {

            whereclausReq = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + " and " + HotelColumn.ZoneId + "=" + Convert.ToInt32(Session["Zone"]);

        }
        else
        {
            whereclausReq = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity;
        }
        vlistRequest = objBookingRequest.GetReqDetails(whereclausReq, string.Empty).FindAllDistinct(ViewForRequestSearchColumn.HotelId);
        foreach (ViewForRequestSearch temp in vlistRequest.FindAllDistinct("ZoneId"))
        {
            reqZone.Add(ObjHotelinfo.getZoneById(temp.ZoneId));
        }
        zoneDDLrequest.DataTextField = "Zone";
        zoneDDLrequest.DataValueField = "Id";
        zoneDDLrequest.DataSource = reqZone;
        zoneDDLrequest.DataBind();
        if (Session["Zone"] != null)
        {
            zoneDDLrequest.SelectedValue = Session["Zone"].ToString();
        }

        zoneDDLrequest.Items.Insert(0, new ListItem("--Select zone--", "0"));


    }

    /// <summary>
    /// Binds the grid on the basis of ordering and other conditions.
    /// </summary>
    /// <param name="orderBy"></param>
    /// <param name="whereclause"></param>
    private void BindSearchResultOnOrder(string orderBy, string whereclause)
    {
        try
        {
            string whereclaus;
            string orderby;
            if (string.IsNullOrEmpty(whereclause))
            {
                whereclaus = Convert.ToString(Session["Where"]);
            }
            else
            {
                whereclaus = whereclause;
            }

            if (string.IsNullOrEmpty(orderBy))
            {
                orderby = "IsPriority DESC," + HotelColumn.BookingAlgo + " DESC";
            }
            else
            {
                orderby = orderBy;
            }

            if (Convert.ToString(((Session["masterInput"] as BookingRequest)).propDuration) == "2")
            {
                string whereclaus2 = Convert.ToString(Session["Where2"]);
                vlist2day = objBookingRequest.GetBookingReqDetails(whereclaus2, orderby).FindAllDistinct(BookingRequestViewListColumn.HotelId);
                Tempvlist = objBookingRequest.GetBookingReqDetails(whereclaus, orderby).FindAllDistinct(BookingRequestViewListColumn.HotelId);
                vlist = new VList<BookingRequestViewList>();
                for (int i = 0; i < Tempvlist.Count; i++)
                {
                    BookingRequestViewList b = vlist2day.Where(a => a.HotelId == Tempvlist[i].HotelId && a.MeetingRoomId == Tempvlist[i].MeetingRoomId).FirstOrDefault();
                    if (b != null)
                    {
                        vlist.Add(b);
                    }
                }
            }
            else
            {
                vlist = objBookingRequest.GetBookingReqDetails(whereclaus, orderby).FindAllDistinct(BookingRequestViewListColumn.HotelId);
            }

            BindBookingMeetingroom();

            if (vlist.Count > 0)
            {
                grvBooking.PageIndex = Convert.ToInt32(ViewState["CurrentPage"] == null ? "0" : ViewState["CurrentPage"]);
                grvBooking.DataSource = vlist.FindAllDistinct(BookingRequestViewListColumn.HotelId);
                grvBooking.DataBind();
                Session["SessionOrderBy"] = vlist;
                lblResultCount.Text = vlist.FindAllDistinct(BookingRequestViewListColumn.HotelId).Count.ToString();
                ApplyPaging();
                hdnManageBookingRequest.Value = "0";
            }
            else
            {
                grvBooking.DataSource = vlist;
                grvBooking.DataBind();
                lblResultCount.Text = vlist.FindAllDistinct(BookingRequestViewListColumn.HotelId).Count.ToString();
                hdnManageBookingRequest.Value = "0";
            }

            if (Convert.ToString(Session["Requestedhotel"]) != "" || Convert.ToString(Session["hotel"]) != "")
            {
                ClearSession();
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }

    /// <summary>
    /// Binds the grid for request on the basis of ordering and other conditions.
    /// </summary>
    /// <param name="orderBy"></param>
    /// <param name="whereclause"></param>
    private void BindSearchResultForRequestOnOrder(string orderBy, string whereclause)
    {
        try
        {
            string whereclaus;
            string orderby;
            BokingDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));
            if (string.IsNullOrEmpty(whereclause))
            {
                if (Session["Zone"] != null)
                {
                    whereclaus = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + " and " + HotelColumn.ZoneId + "=" + Convert.ToInt32(Session["Zone"]);
                }
                else
                {
                    whereclaus = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity;
                }

            }
            else
            {
                whereclaus = whereclause;
            }

            if (string.IsNullOrEmpty(orderBy))
            {
                orderby = HotelColumn.RequestAlgo + " DESC";
            }
            else
            {
                orderby = orderBy;
            }

            vlistRequest = objBookingRequest.GetReqDetails(whereclaus, orderby).FindAllDistinct(ViewForRequestSearchColumn.HotelId);
            BindRequestMeetingroom();
            #region Check record from priority basket for order priority.. Commented priority basket after changes by Thierry
            //vlistPriority = new VList<ViewForRequestSearch>();
            //string whereCheck = "BRType = 0 and IsActive = 1 and '" + BokingDate + "' between FromDate and Todate";
            //TList<PriorityBasket> objPriorityBasket = objBookingRequest.GetPriorityBasket(whereCheck, string.Empty);
            //for (int i = 0; i < objPriorityBasket.Count; i++)
            //{
            //    ViewForRequestSearch vrYes = vlistRequest.Where(a => a.HotelId == objPriorityBasket[i].HotelId).FirstOrDefault();
            //    if (vrYes != null)
            //    {
            //        vlistPriority.Add(vrYes);
            //    }
            //}
            //for (int j = 0; j < vlistRequest.Count; j++)
            //{
            //    ViewForRequestSearch vlistChcek = vlistPriority.Where(a => a.HotelId == vlistRequest[j].HotelId).FirstOrDefault();
            //    if (vlistChcek == null)
            //    {
            //        vlistPriority.Add(vlistRequest[j]);
            //    }
            //}
            #endregion
            if (Convert.ToString(((Session["masterInput"] as BookingRequest)).propDuration) == "2" && (orderby == "ActualPkgPrice ASC" || orderby == "ActualPkgPriceHalfDay ASC") && (((Session["masterInput"] as BookingRequest)).propDays != "0" || ((Session["masterInput"] as BookingRequest)).propDay2 != "0"))
            {
                #region Sorting by price
                DataTable dt = new DataTable();
                dt.Columns.Add("hotelId", typeof(Int64));
                dt.Columns.Add("pkgPrice", typeof(decimal));
                grvRequest.AllowPaging = false;
                grvRequest.DataSource = vlistRequest;
                grvRequest.DataBind();

                foreach (GridViewRow r in grvRequest.Rows)
                {
                    if (r.RowType == DataControlRowType.DataRow)
                    {
                        DataRow dr = dt.NewRow();
                        Label lblActPAckagePrice = (Label)r.FindControl("lblActPAckagePrice");
                        HiddenField hdnId = (HiddenField)r.FindControl("hdnHotelId");
                        dr["hotelId"] = hdnId.Value;
                        dr["pkgPrice"] = lblActPAckagePrice.Text;
                        dt.Rows.Add(dr);
                    }
                }
                grvRequest.AllowPaging = true;
                DataView dv = new DataView();
                dv = dt.DefaultView;
                dv.Sort = "pkgPrice";
                // creating a sorted list
                VList<ViewForRequestSearch> sortedVList = new VList<ViewForRequestSearch>();

                for (int i = 0; i < dv.Count; i++)
                {
                    ViewForRequestSearch b = vlistRequest.Where(a => a.HotelId == Convert.ToInt32(dv[i]["hotelId"])).FirstOrDefault();
                    if (b != null)
                    {
                        sortedVList.Add(b);
                    }
                }
                #endregion
                #region Show/Hide for 2 day
                if (vlistRequest.Count > 0)
                {
                    grvRequest.PageIndex = Convert.ToInt32(ViewState["CurrentPageRequest"] == null ? "0" : ViewState["CurrentPageRequest"]);
                    grvRequest.DataSource = sortedVList;
                    grvRequest.DataBind();
                    Session["SessionOrderByRequest"] = sortedVList;
                    ApplyPaginOnrequest();
                    lblResultRequestCount.Text = sortedVList.FindAllDistinct(ViewForRequestSearchColumn.HotelId).Count.ToString();
                    hdnManageBookingRequest.Value = "1";
                }
                else
                {
                    //ScriptManager.RegisterStartupScript(this, typeof(Page), "ShowRequest", "ShowRequestOnDemand('request');", true);                
                    grvRequest.DataSource = sortedVList;
                    grvRequest.DataBind();
                    grvRequest.EmptyDataTemplate.ToString();
                    lblResultRequestCount.Text = sortedVList.FindAllDistinct(ViewForRequestSearchColumn.HotelId).Count.ToString();
                    hdnManageBookingRequest.Value = "1";
                }
                #endregion
            }
            else
            {
                #region Show/Hide for 1-2 day
                if (vlistRequest.Count > 0)
                {
                    grvRequest.PageIndex = Convert.ToInt32(ViewState["CurrentPageRequest"] == null ? "0" : ViewState["CurrentPageRequest"]);
                    grvRequest.DataSource = vlistRequest;
                    grvRequest.DataBind();
                    Session["SessionOrderByRequest"] = vlistRequest;
                    ApplyPaginOnrequest();
                    lblResultRequestCount.Text = vlistRequest.FindAllDistinct(ViewForRequestSearchColumn.HotelId).Count.ToString();
                    hdnManageBookingRequest.Value = "1";
                }
                else
                {
                    //ScriptManager.RegisterStartupScript(this, typeof(Page), "ShowRequest", "ShowRequestOnDemand('request');", true);                
                    grvRequest.DataSource = vlistRequest;
                    grvRequest.DataBind();
                    grvRequest.EmptyDataTemplate.ToString();
                    lblResultRequestCount.Text = vlistRequest.FindAllDistinct(ViewForRequestSearchColumn.HotelId).Count.ToString();
                    hdnManageBookingRequest.Value = "1";
                }
                #endregion
            }    

            if (Convert.ToString(Session["Requestedhotel"]) != "" || Convert.ToString(Session["hotel"]) != "")
            {
                ClearSession();
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }

    // -------  New development by tariq dated : 23/jan/2013-------START-----------//

    /// <summary>
    /// This method sorts the results on the basis of review score.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtReview_Click(object sender, EventArgs e)
    {
        if (hdnManageBookingRequest.Value == "0")   // For booking.
        {
            #region For 1 day or 2 day availability
            string whereclaus;
            if (ViewState["FilterWhere"] != null)
            {
                whereclaus = Convert.ToString(ViewState["FilterWhere"]);
            }
            else
            {
                whereclaus = Convert.ToString(Session["Where"]);
            }

            if (Convert.ToString(((Session["masterInput"] as BookingRequest)).propDuration) == "2")
            {
                string whereclaus2 = Convert.ToString(Session["Where2"]);
                vlist2day = objBookingRequest.GetBookingReqDetails(whereclaus2, string.Empty).FindAllDistinct(BookingRequestViewListColumn.HotelId);
                Tempvlist = objBookingRequest.GetBookingReqDetails(whereclaus, string.Empty).FindAllDistinct(BookingRequestViewListColumn.HotelId);
                vlist = new VList<BookingRequestViewList>();
                for (int i = 0; i < Tempvlist.Count; i++)
                {
                    BookingRequestViewList b = vlist2day.Where(a => a.HotelId == Tempvlist[i].HotelId && a.MeetingRoomId == Tempvlist[i].MeetingRoomId).FirstOrDefault();
                    if (b != null)
                    {
                        vlist.Add(b);
                    }
                }
            }
            else
            {
                vlist = objBookingRequest.GetBookingReqDetails(whereclaus, string.Empty).FindAllDistinct(BookingRequestViewListColumn.HotelId);
            }
            BindBookingMeetingroom();
            #endregion

            #region Sorting by review
            DataTable dt = new DataTable();
            dt.Columns.Add("hotelId", typeof(Int64));
            dt.Columns.Add("reviewScore", typeof(decimal));
            grvBooking.AllowPaging = false;
            grvBooking.DataSource = vlist;
            grvBooking.DataBind();

            foreach (GridViewRow r in grvBooking.Rows)
            {
                if (r.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = dt.NewRow();
                    Label lblReview = (Label)r.FindControl("lblReview");
                    HiddenField hdnId = (HiddenField)r.FindControl("hdnHotelId");
                    dr["hotelId"] = hdnId.Value;
                    dr["reviewScore"] = (string.IsNullOrEmpty(lblReview.Text) ? "0" : lblReview.Text);
                    dt.Rows.Add(dr);
                }
            }
            grvBooking.AllowPaging = true;
            DataView dv = new DataView();
            dv = dt.DefaultView;
            dv.Sort = "reviewScore DESC";
            // creating a sorted list
            VList<BookingRequestViewList> sortedVList = new VList<BookingRequestViewList>();

            for (int i = 0; i < dv.Count; i++)
            {
                BookingRequestViewList b = vlist.Where(a => a.HotelId == Convert.ToInt32(dv[i]["hotelId"])).FirstOrDefault();
                if (b != null)
                {
                    sortedVList.Add(b);
                }
            }

            #endregion
            grvBooking.PageIndex = Convert.ToInt32(ViewState["CurrentPage"] == null ? "0" : ViewState["CurrentPage"]);
            grvBooking.DataSource = sortedVList;
            grvBooking.DataBind();
            Session["SessionOrderBy"] = sortedVList;
            ApplyPaging();
            lblResultCount.Text = sortedVList.FindAllDistinct(BookingRequestViewListColumn.HotelId).Count.ToString();
            if (Convert.ToString(Session["Requestedhotel"]) != "" || Convert.ToString(Session["hotel"]) != "")
            {
                ClearSession();
            }
        }
        else if (hdnManageBookingRequest.Value == "1")   // For request.
        {
            #region data binding

            string whereclaus;
            BokingDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));

            if (Session["WLHotel"] != null)
            {
                if (Session["Zone"] != null)
                {
                    whereclaus = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + " and " + HotelColumn.ZoneId + "=" + Convert.ToInt32(Session["Zone"]) + " and HotelId IN (" + Convert.ToString(Session["WLHotel"]) + ")";
                }
                else
                {
                    whereclaus = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + " and HotelId IN (" + Convert.ToString(Session["WLHotel"]) + ")";
                }
            }
            else
            {
                if (Session["Zone"] != null)
                {
                    whereclaus = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + " and " + HotelColumn.ZoneId + "=" + Convert.ToInt32(Session["Zone"]);
                }
                else
                {
                    whereclaus = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity;
                }
            }

            vlistRequest = new VList<ViewForRequestSearch>();
            vlistRequest = objBookingRequest.GetReqDetails(whereclaus, "").FindAllDistinct(ViewForRequestSearchColumn.HotelId);
            BindRequestMeetingroom();

            #endregion

            #region Sorting by review
            DataTable dt = new DataTable();
            dt.Columns.Add("hotelId", typeof(Int64));
            dt.Columns.Add("reviewScore", typeof(decimal));
            grvRequest.AllowPaging = false;
            grvRequest.DataSource = vlistRequest;
            grvRequest.DataBind();

            foreach (GridViewRow r in grvRequest.Rows)
            {
                if (r.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = dt.NewRow();
                    Label lblReview = (Label)r.FindControl("lblReview");
                    HiddenField hdnId = (HiddenField)r.FindControl("hdnHotelId");
                    dr["hotelId"] = hdnId.Value;
                    dr["reviewScore"] = (string.IsNullOrEmpty(lblReview.Text) ? "0" : lblReview.Text);
                    dt.Rows.Add(dr);
                }
            }
            grvRequest.AllowPaging = true;
            DataView dv = new DataView();
            dv = dt.DefaultView;
            dv.Sort = "reviewScore DESC";
            // creating a sorted list
            VList<ViewForRequestSearch> sortedVList = new VList<ViewForRequestSearch>();

            for (int i = 0; i < dv.Count; i++)
            {
                ViewForRequestSearch b = vlistRequest.Where(a => a.HotelId == Convert.ToInt32(dv[i]["hotelId"])).FirstOrDefault();
                if (b != null)
                {
                    sortedVList.Add(b);
                }
            }

            #endregion

            if (sortedVList.Count > 0)
            {
                grvRequest.PageIndex = Convert.ToInt32(ViewState["CurrentPageRequest"] == null ? "0" : ViewState["CurrentPageRequest"]);
                grvRequest.DataSource = sortedVList;
                grvRequest.DataBind();
                Session["SessionOrderByRequest"] = sortedVList;
                ApplyPaginOnrequest();
                lblResultRequestCount.Text = sortedVList.FindAllDistinct(ViewForRequestSearchColumn.HotelId).Count.ToString();
                hdnManageBookingRequest.Value = "1";
            }
            else
            {
                //ScriptManager.RegisterStartupScript(this, typeof(Page), "ShowRequest", "ShowRequestOnDemand('request');", true);                
                grvRequest.DataSource = sortedVList;
                grvRequest.DataBind();
                grvRequest.EmptyDataTemplate.ToString();
                lblResultRequestCount.Text = sortedVList.FindAllDistinct(ViewForRequestSearchColumn.HotelId).Count.ToString();
                hdnManageBookingRequest.Value = "1";
            }

            if (Convert.ToString(Session["Requestedhotel"]) != "" || Convert.ToString(Session["hotel"]) != "")
            {
                ClearSession();
            }
        }
    }

    // -------  New development by tariq dated : 23/jan/2013-------END-----------//

    /// <summary>
    /// This method sorts the results on the basis of price.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtPrice_Click(object sender, EventArgs e)
    {
        if (hdnManageBookingRequest.Value == "0")
        {
            #region For 1 day or 2 day availability
            string whereclaus;
            if (ViewState["FilterWhere"] != null)
            {
                whereclaus = Convert.ToString(ViewState["FilterWhere"]);
            }
            else
            {
                whereclaus = Convert.ToString(Session["Where"]);
            }

            if (Convert.ToString(((Session["masterInput"] as BookingRequest)).propDuration) == "2")
            {
                string whereclaus2 = Convert.ToString(Session["Where2"]);
                vlist2day = objBookingRequest.GetBookingReqDetails(whereclaus2, string.Empty).FindAllDistinct(BookingRequestViewListColumn.HotelId);
                Tempvlist = objBookingRequest.GetBookingReqDetails(whereclaus, string.Empty).FindAllDistinct(BookingRequestViewListColumn.HotelId);
                vlist = new VList<BookingRequestViewList>();
                for (int i = 0; i < Tempvlist.Count; i++)
                {
                    BookingRequestViewList b = vlist2day.Where(a => a.HotelId == Tempvlist[i].HotelId && a.MeetingRoomId == Tempvlist[i].MeetingRoomId).FirstOrDefault();
                    if (b != null)
                    {
                        vlist.Add(b);
                    }
                }
            }
            else
            {
                vlist = objBookingRequest.GetBookingReqDetails(whereclaus, string.Empty).FindAllDistinct(BookingRequestViewListColumn.HotelId);
            }
            BindBookingMeetingroom();
            #endregion

            #region Sorting by price
            DataTable dt = new DataTable();
            dt.Columns.Add("hotelId", typeof(Int64));
            dt.Columns.Add("pkgPrice", typeof(decimal));
            grvBooking.AllowPaging = false;
            grvBooking.DataSource = vlist;
            grvBooking.DataBind();

            foreach (GridViewRow r in grvBooking.Rows)
            {
                if (r.RowType == DataControlRowType.DataRow)
                {
                    DataRow dr = dt.NewRow();
                    Label lblDiscountPrice = (Label)r.FindControl("lblDiscountPrice");
                    HiddenField hdnId = (HiddenField)r.FindControl("hdnHotelId");
                    dr["hotelId"] = hdnId.Value;
                    dr["pkgPrice"] = lblDiscountPrice.Text;
                    dt.Rows.Add(dr);
                }
            }
            grvBooking.AllowPaging = true;
            DataView dv = new DataView();
            dv = dt.DefaultView;
            dv.Sort = "pkgPrice";
            // creating a sorted list
            VList<BookingRequestViewList> sortedVList = new VList<BookingRequestViewList>();

            for (int i = 0; i < dv.Count; i++)
            {
                BookingRequestViewList b = vlist.Where(a => a.HotelId == Convert.ToInt32(dv[i]["hotelId"])).FirstOrDefault();
                if (b != null)
                {
                    sortedVList.Add(b);
                }
            }

            #endregion

            grvBooking.PageIndex = Convert.ToInt32(ViewState["CurrentPage"] == null ? "0" : ViewState["CurrentPage"]);
            grvBooking.DataSource = sortedVList;
            grvBooking.DataBind();
            Session["SessionOrderBy"] = sortedVList;
            ApplyPaging();
            lblResultCount.Text = sortedVList.FindAllDistinct(BookingRequestViewListColumn.HotelId).Count.ToString();

            if (Convert.ToString(Session["Requestedhotel"]) != "" || Convert.ToString(Session["hotel"]) != "")
            {
                ClearSession();
            }
        }
        if (hdnManageBookingRequest.Value == "1")
        {
            string orderby;
            if (((Session["masterInput"] as BookingRequest)).propDays == "0")
            {
                orderby = "ActualPkgPrice ASC";
            }
            else
            {
                orderby = "ActualPkgPriceHalfDay ASC";
            }
            Session["OrderbyRequest"] = orderby;
            if (ViewState["FilterWhereRequest"] != null)
            {
                BindSearchResultForRequestOnOrder(orderby, Convert.ToString(ViewState["FilterWhereRequest"]));
            }
            else
            {
                BindSearchResultForRequestOnOrder(orderby, string.Empty);
            }
        }
    }

    /// <summary>
    /// This method sorts the results on the basis of stars.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtStars_Click(object sender, EventArgs e)
    {
        string orderby = "Stars DESC";
        if (hdnManageBookingRequest.Value == "0")
        {
            Session["OrderbyBooking"] = orderby;
            if (ViewState["FilterWhere"] != null)
            {
                BindSearchResultOnOrder(orderby, Convert.ToString(ViewState["FilterWhere"]));
            }
            else
            {
                BindSearchResultOnOrder(orderby, string.Empty);
            }
        }
        if (hdnManageBookingRequest.Value == "1")
        {
            Session["OrderbyRequest"] = orderby;
            if (ViewState["FilterWhereRequest"] != null)
            {
                BindSearchResultForRequestOnOrder(orderby, Convert.ToString(ViewState["FilterWhereRequest"]));
            }
            else
            {
                BindSearchResultForRequestOnOrder(orderby, string.Empty);
            }
        }
    }

    /// <summary>
    /// Event handler for distanceTo dropdownlist selected index changed.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void drpDistanceBooking_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (hdnManageBookingRequest.Value == "0")
        {
            if (drpDistanceBooking.SelectedItem.Value != "0")
            {
                int selectedArea = Convert.ToInt32(drpDistanceBooking.SelectedItem.Value);
                Session["DistanceArea"] = Convert.ToInt32(drpDistanceBooking.SelectedItem.Value);
                CoOrdiantes selectedarea = new CoOrdiantes();
                DistanceCalculator calculator = new DistanceCalculator();
                selectedarea.Latitude = Convert.ToDouble(ObjHotelinfo.getById(selectedArea).Latitude);
                selectedarea.Longitude = Convert.ToDouble(ObjHotelinfo.getById(selectedArea).Longitude);
                string whereclaus;
                if (ViewState["FilterWhere"] != null)
                {
                    whereclaus = Convert.ToString(ViewState["FilterWhere"]);
                }
                else
                {
                    whereclaus = Convert.ToString(Session["Where"]);
                }

                if (Convert.ToString(((Session["masterInput"] as BookingRequest)).propDuration) == "2")
                {
                    string whereclaus2 = Convert.ToString(Session["Where2"]);
                    vlist2day = objBookingRequest.GetBookingReqDetails(whereclaus2, string.Empty).FindAllDistinct(BookingRequestViewListColumn.HotelId);
                    Tempvlist = objBookingRequest.GetBookingReqDetails(whereclaus, string.Empty).FindAllDistinct(BookingRequestViewListColumn.HotelId);
                    vlist = new VList<BookingRequestViewList>();
                    for (int i = 0; i < Tempvlist.Count; i++)
                    {
                        BookingRequestViewList b = vlist2day.Where(a => a.HotelId == Tempvlist[i].HotelId && a.MeetingRoomId == Tempvlist[i].MeetingRoomId).FirstOrDefault();
                        if (b != null)
                        {
                            vlist.Add(b);
                        }
                    }
                }
                else
                {
                    vlist = objBookingRequest.GetBookingReqDetails(whereclaus, string.Empty).FindAllDistinct(BookingRequestViewListColumn.HotelId);
                }
                BindBookingMeetingroom();

                DataTable dt = new DataTable();
                dt.Columns.Add("hotelId", typeof(Int64));
                dt.Columns.Add("distance", typeof(double));
                double distance;
                CoOrdiantes hotel = new CoOrdiantes();
                foreach (BookingRequestViewList temp in vlist)
                {
                    hotel.Latitude = Convert.ToDouble(temp.Latitude);
                    hotel.Longitude = Convert.ToDouble(temp.Longitude);
                    distance = calculator.FindDistance(hotel, selectedarea, DistanceUnit.Kilometers);
                    DataRow dr = dt.NewRow();
                    dr["hotelId"] = temp.HotelId;
                    dr["distance"] = distance;
                    dt.Rows.Add(dr);
                }
                // now sorting on the basis of distance
                DataView dv = new DataView();
                dv = dt.DefaultView;
                dv.Sort = "distance";

                // creating a sorted list
                VList<BookingRequestViewList> sortedVList = new VList<BookingRequestViewList>();
                foreach (DataRowView dr in dv)
                {
                    foreach (BookingRequestViewList temp in vlist)
                    {
                        if (Convert.ToInt64(dr["hotelId"]) == temp.HotelId)
                        {
                            sortedVList.Add(temp);
                            break;
                        }
                    }
                }

                // binding with sorted list
                grvBooking.PageIndex = Convert.ToInt32(ViewState["CurrentPage"] == null ? "0" : ViewState["CurrentPage"]);
                grvBooking.DataSource = sortedVList;
                grvBooking.DataBind();
                Session["SessionOrderBy"] = sortedVList;
                ApplyPaging();
                lblResultCount.Text = sortedVList.FindAllDistinct(BookingRequestViewListColumn.HotelId).Count.ToString();

                if (Convert.ToString(Session["Requestedhotel"]) != "" || Convert.ToString(Session["hotel"]) != "")
                {
                    ClearSession();
                }
            }
            else
            {
                ViewState["FilterWhere"] = null;
                ViewState["FilterWhereRequest"] = null;
                Session["SessionOrderBy"] = null;
                Session["SessionOrderByRequest"] = null;
                Session["OrderbyRequest"] = null;
                Session["OrderbyBooking"] = null;
                BindResultforRequest();
                ClearDistanceRequest();
                ResetFilter();
            }
        }
    }

    protected void drpDistanceRequest_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (hdnManageBookingRequest.Value == "1")
        {
            if (drpDistanceRequest.SelectedItem.Value != "0")
            {
                int selectedArea = Convert.ToInt32(drpDistanceRequest.SelectedItem.Value);
                Session["DistanceAreaRequest"] = Convert.ToInt32(drpDistanceRequest.SelectedItem.Value);
                CoOrdiantes selectedarea = new CoOrdiantes();
                DistanceCalculator calculator = new DistanceCalculator();
                selectedarea.Latitude = Convert.ToDouble(ObjHotelinfo.getById(selectedArea).Latitude);
                selectedarea.Longitude = Convert.ToDouble(ObjHotelinfo.getById(selectedArea).Longitude);
                BokingDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));
                string whereclaus;
                if (ViewState["FilterWhereRequest"] != null)
                {
                    whereclaus = Convert.ToString(ViewState["FilterWhereRequest"]);
                }
                else
                {
                    if (Session["Zone"] != null)
                    {
                        whereclaus = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + " and " + HotelColumn.ZoneId + "=" + Convert.ToInt32(Session["Zone"]);
                    }
                    else
                    {
                        whereclaus = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity;
                    }
                }
                vlistRequest = objBookingRequest.GetReqDetails(whereclaus, string.Empty).FindAllDistinct(ViewForRequestSearchColumn.HotelId);
                BindRequestMeetingroom();

                DataTable dt = new DataTable();
                dt.Columns.Add("hotelId", typeof(Int64));
                dt.Columns.Add("distance", typeof(double));
                double distance;
                CoOrdiantes hotel = new CoOrdiantes();
                foreach (ViewForRequestSearch temp in vlistRequest)
                {
                    hotel.Latitude = Convert.ToDouble(temp.Latitude);
                    hotel.Longitude = Convert.ToDouble(temp.Longitude);
                    distance = calculator.FindDistance(hotel, selectedarea, DistanceUnit.Kilometers);
                    DataRow dr = dt.NewRow();
                    dr["hotelId"] = temp.HotelId;
                    dr["distance"] = distance;
                    dt.Rows.Add(dr);
                }
                // now sorting on the basis of distance
                DataView dv = new DataView();
                dv = dt.DefaultView;
                dv.Sort = "distance";

                // creating a sorted list                
                VList<ViewForRequestSearch> sortedVList = new VList<ViewForRequestSearch>();
                foreach (DataRowView dr in dv)
                {
                    foreach (ViewForRequestSearch temp in vlistRequest)
                    {
                        if (Convert.ToInt64(dr["hotelId"]) == temp.HotelId)
                        {
                            sortedVList.Add(temp);
                            break;
                        }
                    }
                }

                // binding with sorted list
                grvRequest.PageIndex = Convert.ToInt32(ViewState["CurrentPageRequest"] == null ? "0" : ViewState["CurrentPageRequest"]);
                grvRequest.DataSource = sortedVList;
                grvRequest.DataBind();
                Session["SessionOrderByRequest"] = sortedVList;
                ApplyPaginOnrequest();
                lblResultRequestCount.Text = sortedVList.FindAllDistinct(ViewForRequestSearchColumn.HotelId).Count.ToString();

                if (Convert.ToString(Session["Requestedhotel"]) != "" || Convert.ToString(Session["hotel"]) != "")
                {
                    ClearSession();
                }
            }
            else
            {
                ViewState["FilterWhere"] = null;
                ViewState["FilterWhereRequest"] = null;
                Session["SessionOrderBy"] = null;
                Session["SessionOrderByRequest"] = null;
                Session["OrderbyRequest"] = null;
                Session["OrderbyBooking"] = null;
                BindResultforRequest();
                ClearDistanceBooking();
                ResetFilter();
            }
        }
    }



    /// <summary>
    /// This method filters the results on the basis of various
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtFilter_Click(object sender, EventArgs e)
    {
        string whereclause;
        if (hdnManageBookingRequest.Value == "0")
        {
            whereclause = Convert.ToString(Session["Where"]);
            if (zonesDDL.SelectedItem.Value != "0") //if zone has been selected
                whereclause += " and ZoneId='" + zonesDDL.SelectedItem.Value + "'";
            if (themeDDL.SelectedItem.Value != "0") //if theme has been selected
                whereclause += " and Theme='" + themeDDL.SelectedItem.Value + "'";
            if (txtStars.Text != "All") //if stars have been selected
            {
                whereclause += " and Stars='" + txtStars.Text + "'";
            }
            if (txtDailyBudget.Text != "20") //if budget entered
                whereclause += " and ActualPkgPrice between 0 and '" + txtDailyBudget.Text + "'";
            ViewState["FilterWhere"] = whereclause;
            BindSearchResultOnOrder(string.Empty, whereclause);
            //ClearDistanceBooking();
        }
        else
        {
            BokingDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));
            if (Session["Zone"] != null)
            {
                whereclause = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + " and " + HotelColumn.ZoneId + "=" + Convert.ToInt32(Session["Zone"]);
            }
            else
            {
                whereclause = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity;
            }
            if (zoneDDLrequest.SelectedItem.Value != "0") //if zone has been selected
                whereclause += " and ZoneId='" + zoneDDLrequest.SelectedItem.Value + "'";
            if (themeDDL.SelectedItem.Value != "0") //if theme has been selected
                whereclause += " and Theme='" + themeDDL.SelectedItem.Value + "'";
            if (txtStars.Text != "All") //if stars have been selected
                whereclause += " and Stars='" + txtStars.Text + "'";
            if (txtDailyBudget.Text != "20") //if budget enetered
                whereclause += " and ActualPkgPrice between 0 and '" + txtDailyBudget.Text + "'";
            ViewState["FilterWhereRequest"] = whereclause;
            BindSearchResultForRequestOnOrder(string.Empty, whereclause);
            //ClearDistanceRequest();
        }
    }

    /// <summary>
    /// This method reset the filtered results
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtReset_Click(object sender, EventArgs e)
    {
        #region Commented after changes on Filer and sorting which was changes by Client
        //string whereclause;
        //ResetFilter();  
        //if (hdnManageBookingRequest.Value == "0")
        //{
        //    whereclause = Convert.ToString(Session["Where"]);
        //    BindSearchResultOnOrder(string.Empty, whereclause);
        //    ClearDistanceBooking();
        //}
        //else
        //{
        //    BokingDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));
        //    if (Session["Zone"] != null)
        //    {
        //        whereclause = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + " and " + HotelColumn.ZoneId + "=" + Convert.ToInt32(Session["Zone"]);
        //    }
        //    else
        //    {
        //        whereclause = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity;
        //    }
        //    BindSearchResultForRequestOnOrder(string.Empty, whereclause);
        //    ClearDistanceRequest();
        //}
        #endregion
        ResetFilter();
        ViewState["FilterWhere"] = null;
        ViewState["FilterWhereRequest"] = null;
        Session["SessionOrderBy"] = null;
        Session["SessionOrderByRequest"] = null;
        Session["OrderbyRequest"] = null;
        Session["OrderbyBooking"] = null;
        BindResultforRequest();
    }

    int bookingCartItem = 0;
    protected void lstViewMeetingRoomAddToCart_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem && e.Item.DataItem != null)
        {
            if (bookingCartItem == 0)
            {
                ImageButton deleteItem = e.Item.FindControl("imgDelete") as ImageButton;
                if (deleteItem != null)
                {
                    deleteItem.Visible = false;
                }
            }
            bookingCartItem++;
        }
    }


    #region Add to shoppingcart for booking
    public VList<ViewMeetingRoomAvailability> MyMeetingroom
    {
        get;
        set;
    }
    public VList<ViewMeetingRoomForRequest> MyMeetingroomforRequest
    {
        get;
        set;
    }

    //This event is used for Add to shopping cart for Normal and secondary booking
    protected void btnContinue_Click(object sender, EventArgs e)
    {
        try
        {
            //objmeetingroom.Clear();
            //objhotel.Clear();
            //objConfig.Clear();
            if (rbNo.Checked == true)
            {
                Session["IsSecondary"] = "NO";
                divshoppingcart.Style.Add("display", "block");
                divRequestBasket.Style.Add("display", "none");
                filterBox.Style.Add("display", "none");
                MeetingRoomConfig objmeetconfg = ObjMeetingRoomConfigManager.GetMeetingroomConfigByID(Convert.ToInt32(hdnConfigurationId.Value));
                objConfig.Add(objmeetconfg);
                Session["meetingroomConfig"] = objConfig;
                MeetingRoom objmeet = ObjMeetingRoomDesc.GetMeetingRoomByID(Convert.ToInt32(hdnMeetingRoomId.Value));
                objmeetingroom.Add(objmeet);
                Session["meetingroom"] = objmeetingroom;
                Hotel getHotel = ObjHotelinfo.GetHotelByHotelID(Convert.ToInt32(hdnHotelId.Value));
                objhotel.Add(getHotel);
                Session["hotel"] = objhotel;
                lstViewAddToShopping.DataSource = objhotel;
                lstViewAddToShopping.DataBind();
                pnlBookRequest.Enabled = false;
                divBookRequest.Visible = true;
                //pnlBookRequest.CssClass = "overlay2";
                sortingPanel.Enabled = false;
                sortingPanel.CssClass = "overlay2";
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () {alert('" + GetKeyResult("YOUHAVECHOSENVANUESMAXPERBOOKING") + "');});", true);

            }
            else if (rbYes.Checked == true)
            {
                bool CheckNumberOfParticipants = true;
                bool CheckNumberOfMinimumParticipants = true;
                bool CheckAvailability = true;
                string status = "";
                string days, day2;
                DateTime fromDate;
                if (Session["masterInput"] != null)
                {
                    days = ((Session["masterInput"] as BookingRequest)).propDays;
                    day2 = ((Session["masterInput"] as BookingRequest)).propDay2;
                    fromDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));
                    if (days == "0")
                    {
                        status = ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                    }
                    else if (days == "1")
                    {
                        status = ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                    }
                    else if (days == "2")
                    {
                        status = ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                    }

                    //Status is for Day2
                    if (day2 == "0")
                    {
                        status2 = ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                    }
                    else if (day2 == "1")
                    {
                        status2 = ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                    }
                    else if (day2 == "2")
                    {
                        status2 = ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                    }


                    if (Convert.ToString(((Session["masterInput"] as BookingRequest)).propDuration) == "2")
                    {
                        var value = Convert.ToInt32(txtSecparticipants.Text);
                        string whereclaus = "HotelId='" + Convert.ToInt32(hdnHotelId.Value) + "' and AvailabilityDate ='" + fromDate + "' and " + status + " and MeetingRoomId not in('" + Convert.ToInt32(hdnMeetingRoomId.Value) + "')";// + " and " + Convert.ToInt32(value) + " between " + "isnull(MinNo,0) and isnull(MaxNo,0)"                
                        string whereclaus2 = "HotelId='" + Convert.ToInt32(hdnHotelId.Value) + "' and AvailabilityDate='" + fromDate.AddDays(1) + "' and " + status2 + " and MeetingRoomId not in('" + Convert.ToInt32(hdnMeetingRoomId.Value) + "')";

                        VList<ViewMeetingRoomAvailability> VMeeting2 = objBookingRequest.GetMeetingRoomAvailability(whereclaus2).FindAllDistinct(ViewMeetingRoomAvailabilityColumn.MeetingRoomId);
                        VList<ViewMeetingRoomAvailability> VMeeting1 = objBookingRequest.GetMeetingRoomAvailability(whereclaus).FindAllDistinct(ViewMeetingRoomAvailabilityColumn.MeetingRoomId);
                        VList<ViewMeetingRoomAvailability> VMeeting = new VList<ViewMeetingRoomAvailability>();
                        for (int j = 0; j < VMeeting1.Count; j++)
                        {
                            ViewMeetingRoomAvailability b = VMeeting2.Where(a => a.HotelId == VMeeting1[j].HotelId && a.MeetingRoomId == VMeeting1[j].MeetingRoomId).FirstOrDefault();
                            if (b != null)
                            {
                                VMeeting.Add(b);
                            }
                        }

                        var data = VMeeting;
                        if (data.Count == 0)
                        {
                            //Start Added by Manas for secondary meeting room
                            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "alert('" + GetKeyResult("SECONDARYMEETINGROOM") + "');", true);
                            Session["IsSecondary"] = "NO";
                            divshoppingcart.Style.Add("display", "block");
                            divRequestBasket.Style.Add("display", "none");
                            filterBox.Style.Add("display", "none");
                            MeetingRoomConfig objmeetconfg = ObjMeetingRoomConfigManager.GetMeetingroomConfigByID(Convert.ToInt32(hdnConfigurationId.Value));
                            objConfig.Add(objmeetconfg);
                            Session["meetingroomConfig"] = objConfig;
                            MeetingRoom objmeet = ObjMeetingRoomDesc.GetMeetingRoomByID(Convert.ToInt32(hdnMeetingRoomId.Value));
                            objmeetingroom.Add(objmeet);
                            Session["meetingroom"] = objmeetingroom;
                            Hotel getHotel = ObjHotelinfo.GetHotelByHotelID(Convert.ToInt32(hdnHotelId.Value));
                            objhotel.Add(getHotel);
                            Session["hotel"] = objhotel;
                            lstViewAddToShopping.DataSource = objhotel;
                            lstViewAddToShopping.DataBind();
                            pnlBookRequest.Enabled = false;
                            //pnlBookRequest.CssClass = "overlay2";
                            divBookRequest.Visible = true;
                            sortingPanel.Enabled = false;
                            sortingPanel.CssClass = "overlay2";
                            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () {alert('" + GetKeyResult("MEETINGROOMHASBEENADDEDTOYOURSHOOPINGCART") + "');});", true);
                            //End Added by Manas for secondary meeting room
                            return;   
                        }
                        var getResult = data.Where(u => u.MinNo <= value && u.MaxNo >= value);
                        if (getResult.Count() <= 0 && Convert.ToInt32(ViewState["PrimarySelected"]) != 1)
                        {
                            CheckNumberOfParticipants = false;
                            CheckNumberOfMinimumParticipants = false;
                            var min = data.OrderBy(u => u.MinNo).FirstOrDefault();
                            var max = data.OrderBy(u => u.MaxNo).LastOrDefault();
                            lblMax.Text = Convert.ToString(Convert.ToInt32(max.MaxNo));
                            lblMin.Text = Convert.ToString(Convert.ToInt32(min.MinNo));
                        }
                    }
                    else
                    {
                        var value = Convert.ToInt32(txtSecparticipants.Text.Trim());

                        string whereclaus = "HotelId='" + Convert.ToInt32(hdnHotelId.Value) + "' and AvailabilityDate='" + fromDate + "' and " + status + " and MeetingRoomId not in('" + Convert.ToInt32(hdnMeetingRoomId.Value) + "')";// + " and " + Convert.ToInt32(value) + " between " + "isnull(MinNo,0) and isnull(MaxNo,0)" 
                        var data = objBookingRequest.GetMeetingRoomAvailability(whereclaus);
                        if (data.Count == 0)
                        {
                            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "alert('" + GetKeyResult("SECONDARYMEETINGROOM") + "');", true);
                            Session["IsSecondary"] = "NO";
                            divshoppingcart.Style.Add("display", "block");
                            divRequestBasket.Style.Add("display", "none");
                            filterBox.Style.Add("display", "none");
                            MeetingRoomConfig objmeetconfg = ObjMeetingRoomConfigManager.GetMeetingroomConfigByID(Convert.ToInt32(hdnConfigurationId.Value));
                            objConfig.Add(objmeetconfg);
                            Session["meetingroomConfig"] = objConfig;
                            MeetingRoom objmeet = ObjMeetingRoomDesc.GetMeetingRoomByID(Convert.ToInt32(hdnMeetingRoomId.Value));
                            objmeetingroom.Add(objmeet);
                            Session["meetingroom"] = objmeetingroom;
                            Hotel getHotel = ObjHotelinfo.GetHotelByHotelID(Convert.ToInt32(hdnHotelId.Value));
                            objhotel.Add(getHotel);
                            Session["hotel"] = objhotel;
                            lstViewAddToShopping.DataSource = objhotel;
                            lstViewAddToShopping.DataBind();
                            pnlBookRequest.Enabled = false;
                            //pnlBookRequest.CssClass = "overlay2";
                            divBookRequest.Visible = true;
                            sortingPanel.Enabled = false;
                            sortingPanel.CssClass = "overlay2";
                            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () {alert('" + GetKeyResult("MEETINGROOMHASBEENADDEDTOYOURSHOOPINGCART") + "');});", true);
                            return;
                        }
                        var getResult = data.Where(u => u.MinNo <= value && u.MaxNo >= value);
                        if (getResult.Count() <= 0 && Convert.ToInt32(ViewState["PrimarySelected"]) != 1)
                        {
                            CheckNumberOfParticipants = false;
                            CheckNumberOfMinimumParticipants = false;
                            var min = data.OrderBy(u => u.MinNo).FirstOrDefault();
                            var max = data.OrderBy(u => u.MaxNo).LastOrDefault();
                            lblMax.Text = Convert.ToString(max.MaxNo);
                            lblMin.Text = Convert.ToString(min.MinNo);
                        }
                    }

                    if (CheckNumberOfParticipants && CheckNumberOfMinimumParticipants)
                    {
                        objConfig = (TList<MeetingRoomConfig>)Session["meetingroomConfig"];
                        if (objConfig == null)
                        {
                            objConfig = new TList<MeetingRoomConfig>();
                        }
                        objmeetingroom = (TList<MeetingRoom>)Session["meetingroom"];
                        if (objmeetingroom == null)
                        {
                            objmeetingroom = new TList<MeetingRoom>();
                        }
                        objhotel = (TList<Hotel>)Session["hotel"];
                        if (objhotel == null)
                        {
                            objhotel = new TList<Hotel>();
                        }

                        if (ViewState["PopUpChk"] == null)
                        {
                            ViewState["PopUpChk"] = 1;
                        }
                        else
                        {
                            ViewState["PopUpChk"] = 2;
                        }
                        MeetingRoomConfig objmeetconfg = ObjMeetingRoomConfigManager.GetMeetingroomConfigByID(Convert.ToInt32(hdnConfigurationId.Value));
                        objConfig.Add(objmeetconfg);
                        Session["meetingroomConfig"] = objConfig;
                        //objConfig = (TList<MeetingRoomConfig>)Session["meetingroomConfig"];
                        int meetingroomId = Convert.ToInt32(hdnMeetingRoomId.Value);
                        var IsMeetingExist = objmeetingroom.Find(a => a.Id == meetingroomId);
                        if (IsMeetingExist == null)
                        {
                            MeetingRoom objmeet = ObjMeetingRoomDesc.GetMeetingRoomByID(Convert.ToInt32(hdnMeetingRoomId.Value));
                            objmeetingroom.Add(objmeet);
                            Session["meetingroom"] = objmeetingroom;
                            //objmeetingroom = (TList<MeetingRoom>)Session["meetingroom"];
                        }
                        int hotelID = Convert.ToInt32(hdnHotelId.Value);
                        var IsHotelExist = objhotel.Find(a => a.Id == hotelID);
                        if (IsHotelExist == null)
                        {
                            Hotel getHotel = ObjHotelinfo.GetHotelByHotelID(Convert.ToInt32(hdnHotelId.Value));
                            //objhotel = (TList<Hotel>)Session["hotel"];
                            objhotel.Add(getHotel);
                            Session["hotel"] = objhotel;
                        }

                        lstViewAddToShopping.DataSource = (TList<Hotel>)Session["hotel"];
                        lstViewAddToShopping.DataBind();

                        for (int i = 0; i < grvBooking.Rows.Count; i++) // select status in drop downlist box of nested grid view
                        {
                            Repeater childgrid = (Repeater)grvBooking.Rows[i].FindControl("rptSecondaryMeeting");
                            GridView grvBookMeeting = (GridView)grvBooking.Rows[i].FindControl("grvBookMeeting");
                            ObjMeetingRoomDesc = new MeetingRoomDescManager();

                            if (Session["masterInput"] != null)
                            {
                                string whereclaus = "HotelId='" + Convert.ToInt32(hdnHotelId.Value) + "' and AvailabilityDate='" + fromDate + "' and " + status + " and MeetingRoomId not in('" + Convert.ToInt32(hdnMeetingRoomId.Value) + "')" + " and " + Convert.ToInt32(txtSecparticipants.Text) + " between " + "isnull(MinNo,0) and isnull(MaxNo,0)";
                                string whereclaus2;
                                if (Convert.ToString(((Session["masterInput"] as BookingRequest)).propDuration) == "2")
                                {
                                    whereclaus2 = "HotelId='" + Convert.ToInt32(hdnHotelId.Value) + "' and AvailabilityDate='" + fromDate.AddDays(1) + "' and " + status2 + " and MeetingRoomId not in('" + Convert.ToInt32(hdnMeetingRoomId.Value) + "')" + " and " + Convert.ToInt32(txtSecparticipants.Text) + " between " + "isnull(MinNo,0) and isnull(MaxNo,0)";
                                    VList<ViewMeetingRoomAvailability> VMeeting2 = objBookingRequest.GetAllOnlineMeetingRoomSecondary(whereclaus2, 2).FindAllDistinct(ViewMeetingRoomAvailabilityColumn.MeetingRoomId);
                                    VList<ViewMeetingRoomAvailability> VMeeting1 = objBookingRequest.GetAllOnlineMeetingRoomSecondary(whereclaus, 2).FindAllDistinct(ViewMeetingRoomAvailabilityColumn.MeetingRoomId);
                                    MyMeetingroom = new VList<ViewMeetingRoomAvailability>();
                                    for (int j = 0; j < VMeeting1.Count; j++)
                                    {
                                        ViewMeetingRoomAvailability b = VMeeting2.Where(a => a.HotelId == VMeeting1[j].HotelId && a.MeetingRoomId == VMeeting1[j].MeetingRoomId).FirstOrDefault();
                                        if (b != null)
                                        {
                                            MyMeetingroom.Add(b);
                                            MyMeetingroom.Add(VMeeting1[j]);
                                        }
                                    }
                                    //MyMeetingroom = objBookingRequest.GetAllOnlineMeetingRoomSecondary(whereclaus, 2);
                                    childgrid.DataSource = MyMeetingroom.FindAllDistinct(ViewMeetingRoomAvailabilityColumn.MeetingRoomId);
                                    childgrid.DataBind();
                                    ViewState["PrimarySelected"] = 1;
                                }
                                else
                                {
                                    MyMeetingroom = objBookingRequest.GetMeetingRoomAvailability(whereclaus);
                                    childgrid.DataSource = MyMeetingroom.FindAllDistinct(ViewMeetingRoomAvailabilityColumn.MeetingRoomId);
                                    childgrid.DataBind();
                                    ViewState["PrimarySelected"] = 1;
                                }
                            }
                            grvBookMeeting.Visible = false;
                            System.Web.UI.HtmlControls.HtmlGenericControl divRequest = ((System.Web.UI.HtmlControls.HtmlGenericControl)grvBooking.Rows[i].FindControl("divRequest"));
                            divRequest.Visible = false;
                            if (Session["IsSecondary"].ToString() == "YES")
                            {
                                divshoppingcart.Style.Add("display", "block");
                                divRequestBasket.Style.Add("display", "none");
                                filterBox.Style.Add("display", "none");
                                if (Convert.ToString(ViewState["PopUpChk"]) == "2")
                                {
                                    pnlBookRequest.Enabled = false;
                                    divBookRequest.Visible = true;
                                    //pnlBookRequest.CssClass = "overlay2";
                                    sortingPanel.Enabled = false;
                                    sortingPanel.CssClass = "overlay2";
                                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () {alert('" + GetKeyResult("YOUHAVECHOSENVANUESMAXPERBOOKING") + "');});", true);
                                    //Control commandSource = sender as Control;
                                    //ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () { focusRequiredElement('" + commandSource.ClientID + "');});", true);
                                    return;
                                }
                                else
                                {
                                    if (ViewState["LinkButtonClick"] == null)
                                    {
                                        ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () { focusRequiredSecElement('booking'); });", true);
                                    }
                                }
                            }
                            else
                            {
                                divshoppingcart.Style.Add("display", "block");
                                divRequestBasket.Style.Add("display", "none");
                                filterBox.Style.Add("display", "none");
                                Session["IsSecondary"] = "YES";
                                grvRequest.Enabled = false;
                                grvRequest.CssClass = "overlay2";
                                sortingPanel.Enabled = false;
                                sortingPanel.CssClass = "overlay2";
                            }
                        }
                    }
                    else
                    {
                        rbYes.Checked = true;
                        trRequest.Style.Add("display", "block");
                        spanMessage1.Style.Add("display", "block");
                        spanMessage2.Style.Add("display", "block");
                        lblMax.Visible = true;
                        lblMin.Visible = true;
                        spanDefaultMessage1.Style.Add("display", "none");
                        spanDefaultMessage2.Style.Add("display", "none");
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "ShowRequest", "AddToShopping();", true);
                    }
                }
            }
            else if (rbForRequest.Checked == true)
            {

                //MeetingRoomConfig objmeetconfg = ObjMeetingRoomConfigManager.GetMeetingroomConfigByID(Convert.ToInt32(hdnConfigurationId.Value));
                //objConfig.Add(objmeetconfg);
                //Session["meetingroomConfig"] = objConfig;
                //MeetingRoom objmeet = ObjMeetingRoomDesc.GetMeetingRoomByID(Convert.ToInt32(hdnMeetingRoomId.Value));
                //objmeetingroom.Add(objmeet);
                //Session["meetingroom"] = objmeetingroom;
                //Hotel getHotel = ObjHotelinfo.GetHotelByHotelID(Convert.ToInt32(hdnHotelId.Value));
                //objhotel.Add(getHotel);
                //Session["hotel"] = objhotel;
                //lstViewAddToShopping.DataSource = objhotel;
                //lstViewAddToShopping.DataBind();
                //pnlBookRequest.Enabled = false;
                //pnlBookRequest.CssClass = "overlay2";
                //sortingPanel.Enabled = false;
                //sortingPanel.CssClass = "overlay2";
                Session["IsRequestedSecondary"] = "NO";

                objRequestedMeetingConfig = (TList<MeetingRoomConfig>)Session["RequestedmeetingroomConfig"];
                if (objRequestedMeetingConfig == null)
                {
                    objRequestedMeetingConfig = new TList<MeetingRoomConfig>();
                }
                objRequestedmeetingroom = (TList<MeetingRoom>)Session["Requestedmeetingroom"];
                if (objRequestedmeetingroom == null)
                {
                    objRequestedmeetingroom = new TList<MeetingRoom>();
                }
                objRequestedHotel = (TList<Hotel>)Session["Requestedhotel"];
                if (objRequestedHotel == null)
                {
                    objRequestedHotel = new TList<Hotel>();
                }
                Session["IsSecondary"] = "NO";
                divshoppingcart.Style.Add("display", "none");
                divRequestBasket.Style.Add("display", "block");
                filterBox.Style.Add("display", "block");
                MeetingRoomConfig objmeetconfg = ObjMeetingRoomConfigManager.GetMeetingroomConfigByID(Convert.ToInt32(hdnConfigurationId.Value));
                objRequestedMeetingConfig.Add(objmeetconfg);
                Session["RequestedmeetingroomConfig"] = objRequestedMeetingConfig;
                MeetingRoom objmeet = ObjMeetingRoomDesc.GetMeetingRoomByID(Convert.ToInt32(hdnMeetingRoomId.Value));
                objRequestedmeetingroom.Add(objmeet);
                Session["Requestedmeetingroom"] = objRequestedmeetingroom;
                Hotel getHotel = ObjHotelinfo.GetHotelByHotelID(Convert.ToInt32(hdnHotelId.Value));
                objRequestedHotel.Add(getHotel);
                Session["Requestedhotel"] = objRequestedHotel;
                lstViewAddToBasket.DataSource = objRequestedHotel;
                lstViewAddToBasket.DataBind();
                if (objRequestedHotel.Count == Convert.ToInt32(Session["RequestCount"]))
                {
                    pnlBookRequest.Enabled = false;
                    divBookRequest.Visible = true;
                    //pnlBookRequest.CssClass = "overlay2";
                    sortingPanel.Enabled = false;
                    sortingPanel.CssClass = "overlay2";
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () {alert(' " + GetKeyResult("YOUHAVECHOSEN3VANUESMAXPERREQUEST") + " " + Session["RequestCount"] + " " + GetKeyResult("YOUHAVECHOSEN3VANUESMAXPERREQUEST1") + "');});", true);
                    //Control commandSource = sender as Control;
                    //ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () { focusRequiredElement('" + commandSource.ClientID + "');});", true);
                }
                else
                {
                    BindResultforRequest();
                }

                Dictionary<long, Dictionary<long, string>> participants = Session["participants"] as Dictionary<long, Dictionary<long, string>>;
                if (participants == null)
                {
                    participants = new Dictionary<long, Dictionary<long, string>>();
                }

                if (!participants.ContainsKey(getHotel.Id))
                {
                    //KeyValuePair<long, Dictionary<long, string>> pair = new KeyValuePair<long, Dictionary<long, string>>();

                    Dictionary<long, string> value = new Dictionary<long, string>();
                    value.Add(objmeet.Id, ((Session["masterInput"] as BookingRequest)).propParticipants);

                    participants.Add(getHotel.Id, value);
                }
                else
                {
                    var value = participants[getHotel.Id];
                    if (value.ContainsKey(objmeet.Id))
                    {
                        if ((value.Count == 1))
                        {
                            //participants[objmeet.Id] = txtRequestedSecparticipants.Text.Trim();
                            value[objmeet.Id] = ((Session["masterInput"] as BookingRequest)).propParticipants;
                        }
                        else
                        {
                            //participants[objmeet.Id] = ((Session["masterInput"] as BookingRequest)).propParticipants;
                            value[objmeet.Id] = txtSecparticipants.Text.Trim();
                        }
                    }
                    else
                    {
                        if ((value.Count == 0))
                        {
                            value.Add(objmeet.Id, ((Session["masterInput"] as BookingRequest)).propParticipants);
                        }
                        else if ((objRequestedmeetingroom.IndexOf(objmeet) == 0))
                        {
                            value[objmeet.Id] = ((Session["masterInput"] as BookingRequest)).propParticipants;
                        }
                        else
                        {
                            //participants.Add(objmeet.Id, ((Session["masterInput"] as BookingRequest)).propParticipants);
                            value.Add(objmeet.Id, txtSecparticipants.Text.Trim());
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    protected void lnkAddtoShopping_Click(object sender, EventArgs e)
    {
        ViewState["LinkButtonClick"] = "Click";
        btnContinue_Click(null, null);
        Control commandSource = sender as Control;
        //ViewState["LinkButtonClick"] = null;
        //ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () { focusRequiredElement('" + commandSource.ClientID + "');});", true);
    }

    //This event is used for Add to shopping cart for Normal and secondary request
    protected void btnRequestContinue_Click(object sender, EventArgs e)
    {
        if (rbRequestNo.Checked == true)
        {

            Session["IsRequestedSecondary"] = "NO";

            objRequestedMeetingConfig = (TList<MeetingRoomConfig>)Session["RequestedmeetingroomConfig"];
            if (objRequestedMeetingConfig == null)
            {
                objRequestedMeetingConfig = new TList<MeetingRoomConfig>();
            }
            objRequestedmeetingroom = (TList<MeetingRoom>)Session["Requestedmeetingroom"];
            if (objRequestedmeetingroom == null)
            {
                objRequestedmeetingroom = new TList<MeetingRoom>();
            }
            objRequestedHotel = (TList<Hotel>)Session["Requestedhotel"];
            if (objRequestedHotel == null)
            {
                objRequestedHotel = new TList<Hotel>();
            }
            Dictionary<long, Dictionary<long, string>> participants = Session["participants"] as Dictionary<long, Dictionary<long, string>>;
            if (participants == null)
            {
                participants = new Dictionary<long, Dictionary<long, string>>();
            }


            divRequestBasket.Style.Add("display", "block");
            filterBox.Style.Add("display", "block");
            if (objRequestedmeetingroom.FirstOrDefault(u => u.Id == Convert.ToInt32(hdnRequestedMeetingRoomId.Value)) == null)
            {
                MeetingRoomConfig objmeetconfg = ObjMeetingRoomConfigManager.GetMeetingroomConfigByID(Convert.ToInt32(hdnRequestedConfigurationId.Value));
                objRequestedMeetingConfig.Add(objmeetconfg);
                Session["RequestedmeetingroomConfig"] = objRequestedMeetingConfig;
                MeetingRoom objmeet = ObjMeetingRoomDesc.GetMeetingRoomByID(Convert.ToInt32(hdnRequestedMeetingRoomId.Value));
                objRequestedmeetingroom.Add(objmeet);

                Session["Requestedmeetingroom"] = objRequestedmeetingroom;
                Hotel getHotel = ObjHotelinfo.GetHotelByHotelID(Convert.ToInt32(hdnRequestedHotelId.Value));
                objRequestedHotel.Add(getHotel);


                if (!participants.ContainsKey(getHotel.Id))
                {
                    //KeyValuePair<long, Dictionary<long, string>> pair = new KeyValuePair<long, Dictionary<long, string>>();

                    Dictionary<long, string> value = new Dictionary<long, string>();
                    value.Add(objmeet.Id, ((Session["masterInput"] as BookingRequest)).propParticipants);

                    participants.Add(getHotel.Id, value);
                }
                else
                {
                    var value = participants[getHotel.Id];
                    if (value.ContainsKey(objmeet.Id))
                    {
                        if ((value.Count == 1))
                        {
                            //participants[objmeet.Id] = txtRequestedSecparticipants.Text.Trim();
                            value[objmeet.Id] = ((Session["masterInput"] as BookingRequest)).propParticipants;
                        }
                        else
                        {
                            //participants[objmeet.Id] = ((Session["masterInput"] as BookingRequest)).propParticipants;
                            value[objmeet.Id] = txtRequestedSecparticipants.Text.Trim();
                        }

                    }
                    else
                    {
                        if ((value.Count == 0))
                        {
                            value.Add(objmeet.Id, ((Session["masterInput"] as BookingRequest)).propParticipants);
                        }
                        else if ((objRequestedmeetingroom.IndexOf(objmeet) == 0))
                        {
                            value[objmeet.Id] = ((Session["masterInput"] as BookingRequest)).propParticipants;
                        }
                        else
                        {
                            //participants.Add(objmeet.Id, ((Session["masterInput"] as BookingRequest)).propParticipants);
                            value.Add(objmeet.Id, txtRequestedSecparticipants.Text.Trim());
                        }
                    }
                }
            }


            Session["participants"] = participants;
            Session["Requestedhotel"] = objRequestedHotel;
            lstViewAddToBasket.DataSource = objRequestedHotel;
            lstViewAddToBasket.DataBind();
            if (objRequestedHotel.Count == Convert.ToInt32(Session["RequestCount"]))
            {
                pnlBookRequest.Enabled = false;
                divBookRequest.Visible = true;
                sortingPanel.Enabled = false;
                sortingPanel.CssClass = "overlay2";
                filterBox.Style.Add("display", "none");
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () {alert(' " + GetKeyResult("YOUHAVECHOSEN3VANUESMAXPERREQUEST") + " " + Session["RequestCount"] + " " + GetKeyResult("YOUHAVECHOSEN3VANUESMAXPERREQUEST1") + "');});", true);
            }
            else
            {
                BindResultforRequest();
            }
        }
        else if (rbRequestYes.Checked == true)
        {
            if (ViewState["PopUpChk"] == null)
            {
                ViewState["PopUpChk"] = 1;
            }
            else
            {
                ViewState["PopUpChk"] = 2;
            }

            divRequestBasket.Style.Add("display", "block");
            filterBox.Style.Add("display", "block");
            objRequestedMeetingConfig = (TList<MeetingRoomConfig>)Session["RequestedmeetingroomConfig"];
            if (objRequestedMeetingConfig == null)
            {
                objRequestedMeetingConfig = new TList<MeetingRoomConfig>();
            }
            objRequestedmeetingroom = (TList<MeetingRoom>)Session["Requestedmeetingroom"];
            if (objRequestedmeetingroom == null)
            {
                objRequestedmeetingroom = new TList<MeetingRoom>();
            }
            objRequestedHotel = (TList<Hotel>)Session["Requestedhotel"];
            if (objRequestedHotel == null)
            {
                objRequestedHotel = new TList<Hotel>();
            }

            MeetingRoomConfig objmeetconfg = ObjMeetingRoomConfigManager.GetMeetingroomConfigByID(Convert.ToInt32(hdnRequestedConfigurationId.Value));
            objRequestedMeetingConfig.Add(objmeetconfg);
            Session["RequestedmeetingroomConfig"] = objRequestedMeetingConfig;
            long meetingroomId = Convert.ToInt64(hdnRequestedMeetingRoomId.Value);
            MeetingRoom objmeet = objRequestedmeetingroom.FirstOrDefault(a => a.Id == meetingroomId);
            if (objmeet == null)
            {
                objmeet = ObjMeetingRoomDesc.GetMeetingRoomByID(Convert.ToInt32(hdnRequestedMeetingRoomId.Value));

                objRequestedmeetingroom.Add(objmeet);
                Session["Requestedmeetingroom"] = objRequestedmeetingroom;
                meetingroomId = objmeet.Id;

            }
            long hotelID = Convert.ToInt64(hdnRequestedHotelId.Value);
            Hotel getHotel = objRequestedHotel.FirstOrDefault(a => a.Id == hotelID);
            if (getHotel == null)
            {
                getHotel = ObjHotelinfo.GetHotelByHotelID(Convert.ToInt32(hdnRequestedHotelId.Value));
                objRequestedHotel.Add(getHotel);
                hotelID = getHotel.Id;
                Session["Requestedhotel"] = objRequestedHotel;
            }

            Dictionary<long, Dictionary<long, string>> participants = Session["participants"] as Dictionary<long, Dictionary<long, string>>;
            if (participants == null)
            {
                participants = new Dictionary<long, Dictionary<long, string>>();
            }

            if (!participants.ContainsKey(getHotel.Id))
            {
                //KeyValuePair<long, Dictionary<long, string>> pair = new KeyValuePair<long, Dictionary<long, string>>();

                Dictionary<long, string> value = new Dictionary<long, string>();
                value.Add(objmeet.Id, ((Session["masterInput"] as BookingRequest)).propParticipants);

                participants.Add(getHotel.Id, value);
            }
            else
            {
                var value = participants[getHotel.Id];
                if (value.ContainsKey(objmeet.Id))
                {
                    if ((value.Count == 1))
                    {
                        //participants[objmeet.Id] = txtRequestedSecparticipants.Text.Trim();
                        value[objmeet.Id] = ((Session["masterInput"] as BookingRequest)).propParticipants;
                    }
                    else
                    {
                        //participants[objmeet.Id] = ((Session["masterInput"] as BookingRequest)).propParticipants;
                        value[objmeet.Id] = txtRequestedSecparticipants.Text.Trim();
                    }

                }
                else
                {
                    if ((value.Count == 0))
                    {
                        value.Add(objmeet.Id, ((Session["masterInput"] as BookingRequest)).propParticipants);
                    }
                    else if ((objRequestedmeetingroom.IndexOf(objmeet) == 0))
                    {
                        value[objmeet.Id] = ((Session["masterInput"] as BookingRequest)).propParticipants;
                    }
                    else
                    {
                        //participants.Add(objmeet.Id, ((Session["masterInput"] as BookingRequest)).propParticipants);
                        value.Add(objmeet.Id, txtRequestedSecparticipants.Text.Trim());
                    }
                }
            }

            Session["participants"] = participants;

            lstViewAddToBasket.DataSource = (TList<Hotel>)Session["Requestedhotel"];
            lstViewAddToBasket.DataBind();

            BokingDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));


            if (hdnManageBookingRequest.Value == "0")
            {
                for (int i = 0; i < grvBooking.Rows.Count; i++) // select status in drop downlist box of nested grid view
                {
                    Repeater childgrid = (Repeater)grvBooking.Rows[i].FindControl("rptRequestSecondary");
                    GridView grvRequestMeeting = (GridView)grvBooking.Rows[i].FindControl("grvRequestMeeting");
                    ObjMeetingRoomDesc = new MeetingRoomDescManager();

                    if (Session["masterInput"] != null)
                    {
                        if (Convert.ToString(((Session["masterInput"] as BookingRequest)).propDuration) == "2")
                        {
                            //string whereclausRequest = "HotelId='" + Convert.ToInt32(hdnRequestedHotelId.Value) + "' and AvailabilityDate='" + BokingDate + "' and MeetingRoomId not in('" + Convert.ToInt32(hdnRequestedMeetingRoomId.Value) + "')";
                            string whereclausRequest = "HotelId='" + Convert.ToInt32(hdnRequestedHotelId.Value) + "' and MeetingRoomId not in('" + Convert.ToInt32(hdnRequestedMeetingRoomId.Value) + "')";
                            MyMeetingroomforRequest = objBookingRequest.GetReqDetailsForSecondary(whereclausRequest, 2);
                            if (MyMeetingroomforRequest.Count == 0)
                            {
                                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "alert('" + GetKeyResult("SECONDARYMEETINGROOM") + "');", true);
                                if (Convert.ToString(Session["Requestedhotel"]) != "")
                                {
                                    objRequestedHotel = (TList<Hotel>)Session["Requestedhotel"];
                                }

                                if (objRequestedHotel.Count == Convert.ToInt32(Session["RequestCount"]))
                                {
                                    pnlBookRequest.Enabled = false;
                                    divBookRequest.Visible = true;
                                    sortingPanel.Enabled = false;
                                    sortingPanel.CssClass = "overlay2";
                                    filterBox.Style.Add("display", "none");
                                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () {alert(' " + GetKeyResult("YOUHAVECHOSEN3VANUESMAXPERREQUEST") + " " + Session["RequestCount"] + " " + GetKeyResult("YOUHAVECHOSEN3VANUESMAXPERREQUEST1") + "');});", true);
                                    return;
                                }
                                else
                                {
                                    BindResultforRequest();
                                    return;
                                }
                            }
                            else
                            {
                                childgrid.DataSource = MyMeetingroomforRequest.FindAllDistinct(ViewMeetingRoomForRequestColumn.MeetingRoomId);
                                childgrid.DataBind();
                            }
                        }
                        else
                        {
                            string whereclausRequest = "HotelId='" + Convert.ToInt32(hdnRequestedHotelId.Value) + "' and MeetingRoomId not in('" + Convert.ToInt32(hdnRequestedMeetingRoomId.Value) + "')";
                            MyMeetingroomforRequest = objBookingRequest.GetMeetingRoomRequest(whereclausRequest, string.Empty);
                            if (MyMeetingroomforRequest.Count == 0)
                            {
                                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "alert('" + GetKeyResult("SECONDARYMEETINGROOM") + "');", true);
                                if (Convert.ToString(Session["Requestedhotel"]) != "")
                                {
                                    objRequestedHotel = (TList<Hotel>)Session["Requestedhotel"];
                                }

                                if (objRequestedHotel.Count == Convert.ToInt32(Session["RequestCount"]))
                                {
                                    pnlBookRequest.Enabled = false;
                                    divBookRequest.Visible = true;
                                    sortingPanel.Enabled = false;
                                    sortingPanel.CssClass = "overlay2";
                                    filterBox.Style.Add("display", "none");
                                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () {alert(' " + GetKeyResult("YOUHAVECHOSEN3VANUESMAXPERREQUEST") + " " + Session["RequestCount"] + " " + GetKeyResult("YOUHAVECHOSEN3VANUESMAXPERREQUEST1") + "');});", true);
                                    return;
                                }
                                else
                                {
                                    BindResultforRequest();
                                    return;
                                }
                            }
                            else
                            {
                                childgrid.DataSource = MyMeetingroomforRequest.FindAllDistinct(ViewMeetingRoomForRequestColumn.MeetingRoomId);
                                childgrid.DataBind();
                            }
                        }
                    }
                    grvRequestMeeting.Visible = false;
                    System.Web.UI.HtmlControls.HtmlGenericControl divBookingMeeting = ((System.Web.UI.HtmlControls.HtmlGenericControl)grvBooking.Rows[i].FindControl("divBookingMeeting"));
                    divBookingMeeting.Visible = false;
                    if (Session["IsRequestedSecondary"] != null)
                    {
                        if (Session["IsRequestedSecondary"].ToString() == "YES")
                        {
                            divRequestBasket.Style.Add("display", "block");
                            filterBox.Style.Add("display", "block");
                            if (Convert.ToString(ViewState["PopUpChk"]) == "2")
                            {
                                if (objRequestedHotel.Count == Convert.ToInt32(Session["RequestCount"]))
                                {
                                    pnlBookRequest.Enabled = false;
                                    divBookRequest.Visible = true;
                                    sortingPanel.Enabled = false;
                                    sortingPanel.CssClass = "overlay2";
                                    filterBox.Style.Add("display", "none");
                                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () {alert(' " + GetKeyResult("YOUHAVECHOSEN3VANUESMAXPERREQUEST") + " " + Session["RequestCount"] + " " + GetKeyResult("YOUHAVECHOSEN3VANUESMAXPERREQUEST1") + "');});", true);
                                    //Control commandSource = sender as Control;
                                    //ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () { focusRequiredElement('" + commandSource.ClientID + "');});", true);
                                    return;
                                }
                                else
                                {
                                    BindResultforRequest();
                                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () { focusRequiredSecElement('booking-request'); });", true);
                                    return;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () { focusRequiredSecElement('booking-request'); });", true);
                            }
                        }
                        else
                        {
                            Session["IsRequestedSecondary"] = "YES";
                            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () { focusRequiredSecElement('booking-request'); });", true);
                        }
                    }
                }
                sortingPanel.Enabled = false;
                sortingPanel.CssClass = "overlay2";
            }
            else
            {
                for (int i = 0; i < grvRequest.Rows.Count; i++) // select status in drop downlist box of nested grid view
                {
                    Repeater childgrid = (Repeater)grvRequest.Rows[i].FindControl("rptRequestTabSecondary");
                    GridView grvRequestMeeting = (GridView)grvRequest.Rows[i].FindControl("grvRequesttabMeeting");
                    ObjMeetingRoomDesc = new MeetingRoomDescManager();

                    if (Session["masterInput"] != null)
                    {
                        if (Convert.ToString(((Session["masterInput"] as BookingRequest)).propDuration) == "2")
                        {
                            //string whereclausRequest = "HotelId='" + Convert.ToInt32(hdnRequestedHotelId.Value) + "' and AvailabilityDate='" + BokingDate + "' and MeetingRoomId not in('" + Convert.ToInt32(hdnRequestedMeetingRoomId.Value) + "')";
                            string whereclausRequest = "HotelId='" + Convert.ToInt32(hdnRequestedHotelId.Value) + "' and MeetingRoomId not in('" + Convert.ToInt32(hdnRequestedMeetingRoomId.Value) + "')";
                            MyMeetingroomforRequest = objBookingRequest.GetReqDetailsForSecondary(whereclausRequest, 2);
                            if (MyMeetingroomforRequest.Count == 0)
                            {
                                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "alert('" + GetKeyResult("SECONDARYMEETINGROOM") + "');", true);
                                if (Convert.ToString(Session["Requestedhotel"]) != "")
                                {
                                    objRequestedHotel = (TList<Hotel>)Session["Requestedhotel"];
                                }

                                if (objRequestedHotel.Count == Convert.ToInt32(Session["RequestCount"]))
                                {
                                    pnlBookRequest.Enabled = false;
                                    divBookRequest.Visible = true;
                                    sortingPanel.Enabled = false;
                                    sortingPanel.CssClass = "overlay2";
                                    filterBox.Style.Add("display", "none");
                                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () {alert(' " + GetKeyResult("YOUHAVECHOSEN3VANUESMAXPERREQUEST") + " " + Session["RequestCount"] + " " + GetKeyResult("YOUHAVECHOSEN3VANUESMAXPERREQUEST1") + "');});", true);
                                    return;
                                }
                                else
                                {
                                    BindResultforRequest();
                                    return;
                                }
                            }
                            else
                            {
                                childgrid.DataSource = MyMeetingroomforRequest.FindAllDistinct(ViewMeetingRoomForRequestColumn.MeetingRoomId);
                                childgrid.DataBind();
                            }
                        }
                        else
                        {
                            string whereclausRequest = "HotelId='" + Convert.ToInt32(hdnRequestedHotelId.Value) + "' and MeetingRoomId not in('" + Convert.ToInt32(hdnRequestedMeetingRoomId.Value) + "')";
                            MyMeetingroomforRequest = objBookingRequest.GetMeetingRoomRequest(whereclausRequest, string.Empty);
                            if (MyMeetingroomforRequest.Count == 0)
                            {
                                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "alert('" + GetKeyResult("SECONDARYMEETINGROOM") + "');", true);
                                if (Convert.ToString(Session["Requestedhotel"]) != "")
                                {
                                    objRequestedHotel = (TList<Hotel>)Session["Requestedhotel"];
                                }

                                if (objRequestedHotel.Count == Convert.ToInt32(Session["RequestCount"]))
                                {
                                    pnlBookRequest.Enabled = false;
                                    divBookRequest.Visible = true;
                                    sortingPanel.Enabled = false;
                                    sortingPanel.CssClass = "overlay2";
                                    filterBox.Style.Add("display", "none");
                                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () {alert(' " + GetKeyResult("YOUHAVECHOSEN3VANUESMAXPERREQUEST") + " " + Session["RequestCount"] + " " + GetKeyResult("YOUHAVECHOSEN3VANUESMAXPERREQUEST1") + "');});", true);
                                    return;
                                }
                                else
                                {
                                    BindResultforRequest();
                                    return;
                                }
                            }
                            else
                            {
                                childgrid.DataSource = MyMeetingroomforRequest.FindAllDistinct(ViewMeetingRoomForRequestColumn.MeetingRoomId);
                                childgrid.DataBind();
                            }
                        }
                    }
                    grvRequestMeeting.Visible = false;
                    if (Session["IsRequestedSecondary"] != null)
                    {
                        if (Session["IsRequestedSecondary"].ToString() == "YES")
                        {
                            divRequestBasket.Style.Add("display", "block");
                            filterBox.Style.Add("display", "block");
                            if (Convert.ToString(ViewState["PopUpChk"]) == "2")
                            {
                                if (objRequestedHotel.Count == Convert.ToInt32(Session["RequestCount"]))
                                {
                                    pnlBookRequest.Enabled = false;
                                    divBookRequest.Visible = true;
                                    sortingPanel.Enabled = false;
                                    sortingPanel.CssClass = "overlay2";
                                    filterBox.Style.Add("display", "none");
                                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () {alert(' " + GetKeyResult("YOUHAVECHOSEN3VANUESMAXPERREQUEST") + " " + Session["RequestCount"] + " " + GetKeyResult("YOUHAVECHOSEN3VANUESMAXPERREQUEST1") + "');});", true);
                                    return;
                                }
                                else
                                {
                                    BindResultforRequest();
                                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () { focusRequiredSecElement('booking-request'); });", true);
                                    return;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () { focusRequiredSecElement('booking-request'); });", true);
                            }
                        }
                        else
                        {
                            Session["IsRequestedSecondary"] = "YES";
                        }
                    }
                }
                sortingPanel.Enabled = false;
                sortingPanel.CssClass = "overlay2";
            }
        }
    }

    protected void lnkButtonAddToBasket_Click(object sender, EventArgs e)
    {
        ViewState["LinkRequestClick"] = "click";
        btnRequestContinue_Click(null, null);
        Control commandSource = sender as Control;
        ViewState["LinkRequestClick"] = null;
        if (objRequestedHotel.Count == Convert.ToInt32(Session["RequestCount"]))
        {
            sortingPanel.Enabled = false;
            sortingPanel.CssClass = "overlay2";
        }
        else
        {
            sortingPanel.Enabled = true;
            sortingPanel.CssClass = "";
        }
        //ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () { focusRequiredElement('" + commandSource.ClientID + "');});", true);
    }
    #endregion

    #region Bind Request Basket
    //Bind Shopping Cart
    protected void lstViewAddToBasket_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (Session["masterInput"] != null)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {

                ListViewDataItem currentItem = (ListViewDataItem)e.Item;
                DataKey currentDataKey = this.lstViewAddToBasket.DataKeys[currentItem.DataItemIndex];
                long intHotelID = Convert.ToInt64(currentDataKey.Value);
                ListView lstView = (ListView)e.Item.FindControl("lstViewMeetingRoomAddToBasket");
                objRequestedmeetingroom = (TList<MeetingRoom>)Session["Requestedmeetingroom"];
                var result = objRequestedmeetingroom.Where(a => a.HotelId == intHotelID);
                lstView.DataSource = result;
                lstView.DataBind();
                result = null;
            }
        }
    }

    protected void lstViewMeetingRoomAddToBasket_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        if (Session["masterInput"] != null)
        {
            long intmeetingroom = 0;
            switch (e.CommandName.Trim())
            {
                case "deletemeetingroom":
                    intmeetingroom = Convert.ToInt64(e.CommandArgument);
                    objRequestedmeetingroom = (TList<MeetingRoom>)Session["Requestedmeetingroom"];
                    TList<Hotel> objHotel = (TList<Hotel>)Session["Requestedhotel"];
                    if (objRequestedmeetingroom.Count == 1)
                    {
                        BindSearchResult();
                    }
                    MeetingRoom d = objRequestedmeetingroom.Find(a => a.Id == intmeetingroom);
                    objRequestedmeetingroom.Remove(d);

                    if (objRequestedmeetingroom.Count == 0)
                    {
                        objHotel.Clear();
                        divRequestBasket.Style.Add("display", "none");
                        return;
                    }
                    Dictionary<long, Dictionary<long, string>> participants = Session["participants"] as Dictionary<long, Dictionary<long, string>>;
                    if (participants != null)
                    {
                        foreach (Dictionary<long, string> value in participants.Values)
                        {
                            if (value.ContainsKey(intmeetingroom))
                            {
                                value.Remove(intmeetingroom);
                            }
                        }
                    }
                    int intIndex = 0;
                    for (intIndex = 0; intIndex <= objHotel.Count - 1; intIndex++)
                    {
                        long intHotelId = Convert.ToInt64(objHotel[intIndex].Id);
                        if (objRequestedmeetingroom.Where(a => a.HotelId == intHotelId).Count() <= 0)
                        {
                            Hotel h = objHotel.Find(a => a.Id == intHotelId);
                            objHotel.Remove(h);
                            BindResultforRequest();
                        }
                    }
                    lstViewAddToBasket.DataSource = objHotel;
                    lstViewAddToBasket.DataBind();
                    break;
            }
        }
    }

    //Remove hotel and meeting room from shopping cart listview
    protected void lstViewAddToBasket_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        if (Session["masterInput"] != null)
        {
            long intHotelID = 0;
            switch (e.CommandName.Trim())
            {
                case "deletehotelroom":
                    intHotelID = Convert.ToInt64(e.CommandArgument);
                    Dictionary<long, Dictionary<long, string>> participants = Session["participants"] as Dictionary<long, Dictionary<long, string>>;
                    if (participants != null)
                    {
                        if (participants.ContainsKey(intHotelID))
                        {
                            participants.Remove(intHotelID);
                        }
                    }

                    TList<Hotel> objHotel = (TList<Hotel>)Session["Requestedhotel"];
                    if (objHotel.Count == 1)
                    {
                        BindSearchResult();
                        Hotel d = objHotel.Find(a => a.Id == intHotelID);
                        objHotel.Remove(d);
                    }
                    else
                    {
                        TList<MeetingRoom> objRequestedmeetingroom = (TList<MeetingRoom>)Session["Requestedmeetingroom"];
                        if (objRequestedmeetingroom != null)
                        {
                            TList<MeetingRoom> mee = objRequestedmeetingroom.FindAll(a => a.HotelId == intHotelID);
                            foreach (MeetingRoom m in mee)
                            {
                                objRequestedmeetingroom.Remove(m);
                            }
                        }
                        Hotel d = objHotel.Find(a => a.Id == intHotelID);
                        objHotel.Remove(d);
                        BindResultforRequest();
                    }


                    if (objHotel.Count == 0)
                    {
                        divRequestBasket.Style.Add("display", "none");

                    }

                    lstViewAddToBasket.DataSource = objHotel;
                    lstViewAddToBasket.DataBind();
                    break;
            }
        }
    }
    #endregion

    public bool MessageBox(string msg)
    {
        Label lbl = new Label();
        lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
        if (lbl.Text == "OK")
        {
            //return ;

        }
        else
        {

        }
        Page.Controls.Add(lbl);
        return true;
    }

    #region statistics
    public void InsertStatistics(int HotelId)
    {
        Viewstatistics objview = new Viewstatistics();
        Statistics ST = new Statistics();
        ST.HotelId = HotelId;
        ST.Views = 1;
        ST.StatDate = System.DateTime.Now;
        objview.InsertVisitwithBookingid(ST);
    }
    #endregion

    #region Get request basket count as per country id
    public void GetBasket()
    {
        int RequestCount = 0;
        TList<Others> objOthers = objBookingRequest.GetNoBasketbyHotelId(Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry));
        if (objOthers.Count > 0)
        {
            if (objOthers[0].MaxRequestBasket != null)
            {
                RequestCount = Convert.ToInt32(objOthers[0].MaxRequestBasket);
                if (Convert.ToInt32(objOthers[0].MaxRequestBasket) == 0)
                {
                    if (l != null)
                    {
                        Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
                    }
                    else
                    {
                        Response.Redirect(SiteRootPath + "default/english");
                    }
                }
            }
            else
            {
                RequestCount = 3;
            }
        }
        else
        {
            RequestCount = 3;
        }
        Session["RequestCount"] = RequestCount;
    }
    #endregion

    #region Other Related Function

    public void FogDisableBooking()
    {
        foreach (GridViewRow r in grvBooking.Rows)
        {
            if (r.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnbRequestOnline = (LinkButton)r.FindControl("lnbRequestOnline");
                LinkButton btnSeeAll = (LinkButton)r.FindControl("btnSeeAll");
                btnSeeAll.Enabled = true;
                lnbRequestOnline.Enabled = true;


                Panel pnlBooking = (Panel)r.FindControl("pnlBooking");
                System.Web.UI.HtmlControls.HtmlGenericControl divcover = ((System.Web.UI.HtmlControls.HtmlGenericControl)r.FindControl("divcover"));
                HiddenField hdnId = (HiddenField)r.FindControl("hdnHotelId");
                if (Convert.ToString(Session["Requestedhotel"]) != "")
                {
                    objRequestedHotel = (TList<Hotel>)Session["Requestedhotel"];
                }
                Hotel checkhotel = objRequestedHotel.Where(a => a.Id == Convert.ToInt32(hdnId.Value)).FirstOrDefault();
                if (checkhotel == null)
                {
                    pnlBooking.Enabled = true;
                    divcover.Visible = false;
                }
                else
                {
                    if (checkhotel.Id == Convert.ToInt64(hdnId.Value))
                    {
                        pnlBooking.Enabled = false;
                        divcover.Visible = true;
                        //pnlBooking.CssClass = "overlay2";
                    }
                    else
                    {
                        pnlBooking.Enabled = true;
                        divcover.Visible = false;
                    }
                }
            }
        }
    }

    public void FogDisableRequest()
    {
        foreach (GridViewRow r in grvRequest.Rows)
        {
            if (r.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnbRequestOnline = (LinkButton)r.FindControl("lnbRequestOnline");
                lnbRequestOnline.Enabled = true;

                Panel pnlBooking = (Panel)r.FindControl("pnlBooking");
                System.Web.UI.HtmlControls.HtmlGenericControl divcover = ((System.Web.UI.HtmlControls.HtmlGenericControl)r.FindControl("divcover"));
                HiddenField hdnId = (HiddenField)r.FindControl("hdnHotelId");
                if (Convert.ToString(Session["Requestedhotel"]) != "")
                {
                    objRequestedHotel = (TList<Hotel>)Session["Requestedhotel"];
                }
                Hotel checkhotel = objRequestedHotel.Where(a => a.Id == Convert.ToInt32(hdnId.Value)).FirstOrDefault();
                if (checkhotel == null)
                {
                    pnlBooking.Enabled = true;
                    divcover.Visible = false;
                }
                else
                {
                    if (checkhotel.Id == Convert.ToInt64(hdnId.Value))
                    {
                        pnlBooking.Enabled = false;
                        divcover.Visible = true;
                        //pnlBooking.CssClass = "overlay2";
                    }
                    else
                    {
                        pnlBooking.Enabled = true;
                        divcover.Visible = false;
                    }

                }
            }
        }
    }

    public void ClearSession()
    {
        foreach (GridViewRow r in grvBooking.Rows)
        {
            if (r.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnbRequestOnline = (LinkButton)r.FindControl("lnbRequestOnline");
                LinkButton btnSeeAll = (LinkButton)r.FindControl("btnSeeAll");
                btnSeeAll.Enabled = true;
                lnbRequestOnline.Enabled = true;


                Panel pnlBooking = (Panel)r.FindControl("pnlBooking");
                System.Web.UI.HtmlControls.HtmlGenericControl divcover = ((System.Web.UI.HtmlControls.HtmlGenericControl)r.FindControl("divcover"));
                HiddenField hdnId = (HiddenField)r.FindControl("hdnHotelId");
                if (Convert.ToString(Session["Requestedhotel"]) != "")
                {
                    objRequestedHotel = (TList<Hotel>)Session["Requestedhotel"];
                }
                Hotel checkhotel = objRequestedHotel.Where(a => a.Id == Convert.ToInt32(hdnId.Value)).FirstOrDefault();
                if (checkhotel == null)
                {
                    pnlBooking.Enabled = true;
                    divcover.Visible = false;
                }
                else
                {
                    if (checkhotel.Id == Convert.ToInt64(hdnId.Value))
                    {
                        pnlBooking.Enabled = false;
                        divcover.Visible = true;
                        //pnlBooking.CssClass = "overlay2";
                    }
                    else
                    {
                        pnlBooking.Enabled = true;
                        divcover.Visible = false;
                    }
                }
            }
        }


        foreach (GridViewRow r in grvRequest.Rows)
        {
            if (r.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnbRequestOnline = (LinkButton)r.FindControl("lnbRequestOnline");
                lnbRequestOnline.Enabled = true;

                Panel pnlBooking = (Panel)r.FindControl("pnlBooking");
                System.Web.UI.HtmlControls.HtmlGenericControl divcover = ((System.Web.UI.HtmlControls.HtmlGenericControl)r.FindControl("divcover"));
                HiddenField hdnId = (HiddenField)r.FindControl("hdnHotelId");
                if (Convert.ToString(Session["Requestedhotel"]) != "")
                {
                    objRequestedHotel = (TList<Hotel>)Session["Requestedhotel"];
                }
                Hotel checkhotel = objRequestedHotel.Where(a => a.Id == Convert.ToInt32(hdnId.Value)).FirstOrDefault();
                if (checkhotel == null)
                {
                    pnlBooking.Enabled = true;
                    divcover.Visible = false;
                }
                else
                {
                    if (checkhotel.Id == Convert.ToInt64(hdnId.Value))
                    {
                        pnlBooking.Enabled = false;
                        divcover.Visible = true;
                        //pnlBooking.CssClass = "overlay2";
                    }
                    else
                    {
                        pnlBooking.Enabled = true;
                        divcover.Visible = false;
                    }

                }
            }
        }
    }

    public void ClearDistanceBooking()
    {
        if (drpDistanceBooking.SelectedValue != "0")
        {
            foreach (GridViewRow r in grvBooking.Rows)
            {
                if (r.RowType == DataControlRowType.DataRow)
                {
                    Label lblAirport = (Label)r.FindControl("lblAirport");
                    lblAirport.Text = "";
                }
            }
        }
        drpDistanceBooking.SelectedIndex = -1;
    }

    public void ClearDistanceRequest()
    {
        if (drpDistanceRequest.SelectedValue != "0")
        {
            foreach (GridViewRow r in grvRequest.Rows)
            {
                if (r.RowType == DataControlRowType.DataRow)
                {
                    Label lblAirport = (Label)r.FindControl("lblAirport");
                    lblAirport.Text = "";
                }
            }
        }
        drpDistanceRequest.SelectedIndex = -1;
    }

    public void ResetFilter()
    {
        zonesDDL.SelectedIndex = -1; //reset zone 
        zoneDDLrequest.SelectedIndex = -1;
        themeDDL.SelectedIndex = -1; //reset theme 
        txtStars.Text = "All"; //reset stars 
        txtDailyBudget.Text = "20";//reset dailybudget 
        drpDistanceBooking.SelectedIndex = -1;
        drpDistanceRequest.SelectedIndex = -1;
    }
    #endregion

    protected void lnkRequestCall_OnClick(object sender, EventArgs e)
    {

        ScriptManager.RegisterStartupScript(this, typeof(Page), "ShowRequest", "jQuery(document).ready(function(){try{ShowRequestOnDemand('request');}catch(err){}});", true);
        hdnManageBookingRequest.Value = "1";
        ShowBooking = false;
        lnkRequestCall.Visible = false;
        grvRequest.Visible = true;
        ContentPlaceHolder searchContent = (ContentPlaceHolder)Page.Master.FindControl("cntLeftSearch");
        DropDownList drpRadius = (DropDownList)searchContent.FindControl("LeftSearchPanel1").FindControl("drpRadius");
        if (grvRequest.Visible)
        {
            #region Request Grid
            //This Condition for WL
            string whereclausRequest = string.Empty;
            if (Session["WLHotel"] != null)
            {
                BokingDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));

                if (Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) != 0 && Session["Zone"] == null)
                {
                    whereclausRequest = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + " and HotelId IN (" + Convert.ToString(Session["WLHotel"]) + ")";
                }
                else if (Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) != 0 && Session["Zone"] != null)
                {
                    whereclausRequest = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + " and " + HotelColumn.ZoneId + "=" + Convert.ToInt32(Session["Zone"]) + " and HotelId IN (" + Convert.ToString(Session["WLHotel"]) + ")";
                }
                else
                {
                    whereclausRequest = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "'" + " and HotelId IN (" + Convert.ToString(Session["WLHotel"]) + ")";
                }
            }
            else
            {
                BokingDate = new DateTime(Convert.ToInt32("20" + ((Session["masterInput"] as BookingRequest)).propDate.Split('/')[2]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[1]), Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propDate.Split('/')[0]));

                if (Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) != 0 && Session["Zone"] == null)
                {
                    whereclausRequest = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity;
                }
                else if (Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) != 0 && Session["Zone"] != null)
                {
                    whereclausRequest = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "' and " + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propParticipants) + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + " and " + HotelColumn.ZoneId + "=" + Convert.ToInt32(Session["Zone"]);
                }
                else
                {
                    whereclausRequest = "CountryId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCountry) + "' and CityId='" + Convert.ToInt32(((Session["masterInput"] as BookingRequest)).propCity) + "'";
                }
            }


            //string orderbyRequest = "IsPriority DESC," + HotelColumn.RequestAlgo + " DESC";
            string orderbyRequest = HotelColumn.RequestAlgo + " DESC";
            if (RequestDataSource == null)
            {
                vlistRequest = objBookingRequest.GetReqDetails(whereclausRequest, orderbyRequest).FindAllDistinct(ViewForRequestSearchColumn.HotelId);
            }
            else
            {
                vlistRequest = RequestDataSource;
            }
            vlistPriority = new VList<ViewForRequestSearch>();
            string whereCheck = "BRType = 0 and IsActive = 1 and '" + BokingDate + "' between FromDate and Todate";
            TList<PriorityBasket> objPriorityBasket = objBookingRequest.GetPriorityBasket(whereCheck, string.Empty);
            //Check record from priority basket for order priority
            for (int i = 0; i < objPriorityBasket.Count; i++)
            {
                ViewForRequestSearch vrYes = vlistRequest.Where(a => a.HotelId == objPriorityBasket[i].HotelId).FirstOrDefault();
                if (vrYes != null)
                {
                    vlistPriority.Add(vrYes);
                }
            }
            for (int j = 0; j < vlistRequest.Count; j++)
            {
                ViewForRequestSearch vlistChcek = vlistPriority.Where(a => a.HotelId == vlistRequest[j].HotelId).FirstOrDefault();
                if (vlistChcek == null)
                {
                    vlistPriority.Add(vlistRequest[j]);
                }
            }

            if (drpRadius != null)
            {
                if (drpRadius.SelectedValue != "0")
                {
                    HiddenField hndActualLogitude = (HiddenField)searchContent.FindControl("LeftSearchPanel1").FindControl("hdnMapLongitude");
                    HiddenField hndActualLatitude = (HiddenField)searchContent.FindControl("LeftSearchPanel1").FindControl("hdnMapLatitude");
                    foreach (ViewForRequestSearch objlist in vlistPriority)
                    {
                        double actualLogitude = Convert.ToDouble(hndActualLogitude.Value);
                        double actuallatitude = Convert.ToDouble(hndActualLatitude.Value);
                        double hotelLogitude = Convert.ToDouble(objlist.Longitude);
                        double hotelLatitude = Convert.ToDouble(objlist.Latitude);
                        double hotelDistance = distance(actualLogitude, actuallatitude, hotelLogitude, hotelLatitude);

                        if (hotelDistance <= Convert.ToDouble(drpRadius.SelectedValue))
                        {
                            if (FinalResult.Where(a => a.HotelId == objlist.HotelId).FirstOrDefault() == null)
                            {
                                FinalResultRequest.Add(objlist);
                            }
                        }
                    }
                }

                else
                {
                    FinalResultRequest = vlistPriority;
                }
            }
            else
            {
                FinalResultRequest = vlistPriority;
            }
            BindRequestMeetingroom();
            grvRequest.DataSource = FinalResultRequest;
            grvRequest.DataBind();
            //hdnRequestCount.Value = Convert.ToString(FinalResultRequest.Count);
            //lblResultCount.Text = Convert.ToString(FinalResultRequest.Count);
            lblResultRequestCount.Text = FinalResultRequest.Count.ToString();
            #endregion
            ApplyPaginOnrequest();
        }
        //ltrScript.Text = "<script language='javascript' type='text/javascript'>jQuery(document).ready(function () { " + ScriptForImage + "});</script>";
    }
}