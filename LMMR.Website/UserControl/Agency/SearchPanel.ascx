﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchPanel.ascx.cs" Inherits="UserControl_Operator_SearchPanel" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div class="search-operator-layout1" style="margin-bottom: 20px;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="630px">
                <div class="search-operator-layout-left1">
                    <div class="search-operator-from1">
                        <div class="commisions-top-new1 ">
                            <table width="100%" border="0" cellspacing="0" cellpadding="8">
                                <tr>
                                    <td style="border-bottom: #92bede solid 1px">
                                        <asp:DropDownList ID="drpSearchCountry" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpSearchCountry_SelectedIndexChanged"
                                            Style="width: 215px">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <b>Date:</b>
                                                </td>
                                                <td style="padding-left: 20px; text-align: right; padding-right: 3px;">
                                                    <b>From</b>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFromdate" runat="server" CssClass="inputbox1"></asp:TextBox><asp:HiddenField
                                                        ID="hdnfromdate" runat="server" />
                                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFromdate"
                                                        PopupButtonID="calFrom" Format="dd/MM/yy" OnClientDateSelectionChanged="dateChangeFrom">
                                                    </asp:CalendarExtender>
                                                    <asp:Image ID="calFrom" runat="server" ImageUrl="~/Images/date-icon.png" align="absmiddle" />
                                                </td>
                                                <td>
                                                    <b>To</b>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTodate" runat="server" CssClass="inputbox1"></asp:TextBox>
                                                    <asp:HiddenField ID="hdntodate" runat="server" />
                                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTodate"
                                                        PopupButtonID="calTo" Format="dd/MM/yy" OnClientDateSelectionChanged="dateChangeTo">
                                                    </asp:CalendarExtender>
                                                    <asp:Image ID="calTo" runat="server" ImageUrl="~/Images/date-icon.png" align="absmiddle" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trVenue" runat="server" visible="false">
                                    <td style="border-bottom: #92bede solid 1px">
                                        <asp:DropDownList ID="drpSearchCity" runat="server" AutoPostBack="false" CssClass="NoClassApply"
                                            Enabled="false" Style="width: 215px">
                                            <asp:ListItem Text="--Select city--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <div class="search-form1-left-new">
                                            Venue(s) :
                                        </div>
                                        <asp:TextBox ID="txtVenue" runat="server" class="inputbox"></asp:TextBox>
                                    </td>
                                </tr>
                                <%--<tr>
                                    <td style="border-bottom: #92bede solid 1px">
                                        <select name="select4" class="dropdown" style="width: 215px">
                                            <option value="opt1">Select country</option>
                                            <option value="opt2">Option 2</option>
                                            <option value="opt3">Option 3</option>
                                        </select>
                                    </td>
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <div class="search-form1-left-new">
                                            Invoice number:
                                        </div>
                                        <input type="text" class="inputregister" value="Enter nr here...">
                                    </td>
                                </tr>--%>
                            </table>
                            <%--<table width="100%" border="0" cellspacing="0" cellpadding="8">
                                <tr>
                                    <td style="border-bottom: #92bede solid 1px">
                                        <b>Select country</b>
                                        <select name="select4" class="dropdown" style="width: 150px">
                                            <option value="opt1">Select country</option>
                                            <option value="opt2">Option 2</option>
                                            <option value="opt3">Option 3</option>
                                        </select>
                                    </td>
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <select name="select4" class="dropdown" style="width: 60px">
                                            <option value="opt1">Select country</option>
                                            <option value="opt2">Option 2</option>
                                            <option value="opt3">Option 3</option>
                                        </select>
                                    </td>
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <input type="text" class="inputregister" value="Enter nr here...">
                                    </td>
                                </tr>
                            </table>--%>
                        </div>
                    </div>
                </div>
            </td>
            <td align="left" valign="bottom">
                <div style="float: left; margin-left: 20px;" class="n-commisions">
                    <div class="n-btn">
                        <asp:LinkButton ID="lbtSearch" runat="server" OnClick="lbtSearch_Click" OnClientClick="return DataValidation()"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Search</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                    </div>
                </div>
                <div style="float: left; margin-left: 10px;" class="n-commisions">
                    <div class="n-btn">
                        <asp:LinkButton ID="lnkClear" runat="server" OnClick="lnkClear_Click"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Clear</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript" language="javascript">
    function dateChangeFrom(s, e) {
        var fromdatechange = jQuery("#<%= txtFromdate.ClientID %>").val();
        jQuery("#<%= hdnfromdate.ClientID %>").val(fromdatechange);
    }
    function dateChangeTo(s, e) {
        var todatechange = jQuery("#<%= txtTodate.ClientID %>").val();
        jQuery("#<%= hdntodate.ClientID %>").val(todatechange);
    }

    function DataValidation() {
        var fromdatechange = jQuery("#<%= txtFromdate.ClientID %>").val();
        var todatechange = jQuery("#<%= txtTodate.ClientID %>").val();
        var todayArr = todatechange.split('/');
        var formdayArr = fromdatechange.split('/');
        var fromdatecheck = new Date();
        var todatecheck = new Date();
        fromdatecheck.setFullYear(parseInt("20" + formdayArr[2]), (parseInt(formdayArr[1]) - 1), formdayArr[0]);
        todatecheck.setFullYear(parseInt("20" + todayArr[2]), (parseInt(todayArr[1]) - 1), todayArr[0]);
        if (fromdatecheck > todatecheck) {
            alert("From date must be less than To date."); 
            return false;
        }
    }
</script>
