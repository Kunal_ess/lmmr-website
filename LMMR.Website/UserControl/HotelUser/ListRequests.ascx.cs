﻿

#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using LMMR.Entities;
using LMMR.Data;
using LMMR.Business;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using log4net;
using log4net.Config;
using System.Configuration;
using System.Xml;

#endregion

public partial class UserControl_HotelUser_ListRequests : System.Web.UI.UserControl
{

    #region variables
    VList<ViewBookingHotels> vlist;
    VList<Viewbookingrequest> vlistreq;
    ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
    HotelManager objHotelManager = new HotelManager();
    string Typelink = "", AdminUser = "", Hotelid = "0", userId = "0";
    ILog logger = log4net.LogManager.GetLogger(typeof(UserControl_HotelUser_ListRequests));
    WizardLinkSettingManager ObjWizardManager = new WizardLinkSettingManager();
    Createbooking objBooking = null;
    BookingManager bm = new BookingManager();
    CurrencyManager cm = new CurrencyManager();
    HotelManager objHotel = new HotelManager();
    PackagePricingManager objPackagePricingManager = new PackagePricingManager();
    Hotel objRequestHotel = new Hotel();

    int countMr = 0;
    CreateRequest objRequest = null;
    public List<PackageItemDetails> SelectedPackage
    {
        get;
        set;
    }
    public List<RequestExtra> SelectedExtra
    {
        get;
        set;
    }
    public TList<PackageItems> AllPackageItem
    {
        get;
        set;
    }
    public TList<PackageItems> AllFoodAndBravrages
    {
        get;
        set;
    }
    public TList<PackageItems> AllEquipments
    {
        get;
        set;
    }
    public TList<PackageItems> AllOthers
    {
        get;
        set;
    }
    public TList<PackageByHotel> AllPackageByHotel
    {
        get;
        set;
    }

    public string CurrencySign
    {
        get;
        set;
    }
    public XmlDocument XMLLanguage
    {
        get { return (XmlDocument)ViewState["Language"]; }
        set { ViewState["Language"] = value; }
    }
    #endregion

    #region Pageload
    /// <summary>
    /// Method to manage the page load functionality.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    #endregion

    #region fetchlistofhotels
    /// <summary>
    /// Method to fetch list of hotel for the particular user
    /// </summary>

    protected void FetchListofHotel()
    {
        try
        {
            AdminUser = Convert.ToString(Session["CurrentUserID"]);

            Typelink = Convert.ToString(Request.QueryString["Type"]);

            //-------------- changed according Therirry comment in SRS v1.2
            Hotelid = Convert.ToString(Session["CurrentHotelID"].ToString());
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion
    //----------------------------new(24/5/12)------------------------------------

    /// <summary>
    /// This method displays the user profile area.
    /// </summary>
    /// <param name="userID"></param>
    /// <param name="userType"></param>
    
    /// <summary>
    /// This method binds the city dropdownlist.
    /// </summary>
    

    /// <summary>
    /// This method binds the hotel DDL.
    /// </summary>
    

    /// <summary>
    /// This method binds the meetingRoom DDL.
    /// </summary>
    

    /// <summary>
    /// This method binds bookingDate DDL.
    /// </summary>
    

    /// <summary>
    /// This method binds value DDl.
    /// </summary>
    //public void bindvalueDDL()
    //{
    //    string whereclause = "CreatorId in (" + userId + ") and booktype = 1"; // book type for request and booking
    //    vlistreq = objViewBooking_Hotel.Bindgrid(whereclause, String.Empty).FindAllDistinct(ViewbookingrequestColumn.FinalTotalPrice);
    //    GridViewRow header = grdViewBooking.HeaderRow;
    //    DropDownList valueDDL = header.FindControl("valueDDL") as DropDownList;
    //    valueDDL.DataTextField = "FinalTotalPrice";
    //    valueDDL.DataValueField = "FinalTotalPrice";
    //    valueDDL.DataSource = vlistreq;
    //    valueDDL.DataBind();
    //    valueDDL.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--Select Value--", "0"));
    //}

    /// <summary>
    /// This method gets the city name according to the hotelID. 
    /// </summary>
    /// <param name="hotelId"></param>
    /// <returns></returns>
    public string getCityName(string hotelId)
    {
        return objViewBooking_Hotel.getCityName(hotelId);
    }

    /// <summary>
    /// This method gets the hotel name on the basis of hotelID.
    /// </summary>
    /// <param name="hotelId"></param>
    /// <returns></returns>
    public string getHotelName(string hotelId)
    {
        return objViewBooking_Hotel.GetbyHotelID(Convert.ToInt32(hotelId)).Name;
    }

    /// <summary>
    /// This method gets the cancellation limit for a particular hotel on the basis of its ID.
    /// </summary>
    /// <param name="hotelId"></param>
    /// <returns></returns>
    public string getCancellationLimit(int hotelId)
    {
        long policyId = objViewBooking_Hotel.getSpecialPolicyId(hotelId);
        int cancellationLimit;
        if (policyId != 0)
        {
            cancellationLimit = Convert.ToInt32(objViewBooking_Hotel.getPolicy(policyId).CancelationLimit);
        }
        else
        {
            long countryId = objViewBooking_Hotel.getHotelCountryId(hotelId);
            cancellationLimit = Convert.ToInt32(objViewBooking_Hotel.getDefaultPolicy(countryId).CancelationLimit);
        }
        return cancellationLimit.ToString();
    }

    /// <summary>
    /// This is the event handler for value DDL SelectedIndexChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    

    /// <summary>
    /// This is the event handler for bookingDate DDL SelectedIndexChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    

    /// <summary>
    /// This is the event handler for meetingRoom DDL SelectedIndexChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    

    /// <summary>
    /// This is the event handler for chkInvoiceSent checkbox CheckedChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    

    /// <summary>
    /// This is the event handler for hotel DDL SelectedIndexChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    

    /// <summary>
    /// This is the event handler for city DDL SelectedIndexChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    

    /// <summary>
    /// This method handles the paging functionality.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    

    /// <summary>
    /// This method filters the grid.
    /// </summary>
    /// <param name="wherec"></param>
    /// <param name="order"></param>
    /// <param name="clientNameStarts"></param>
   
    /// <summary>
    /// This is the event handler for the hypManageProfile hyperlink Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>


    /// <summary>
    /// This is the event handler for the hypListBookings hyperlink Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
  

    /// <summary>
    /// This is the event handler for lbtReset LinkButton Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
  


    // -------------------------------------------------------------------

    

    

    

    #region fillbedroom details
    /// <summary>
    /// Method to get bedroom details
    /// </summary>
    protected void FillbedroomDetails()
    {
        try
        {
            // BookedBedRoom object
            TList<BookedBedRoom> objBookedBR = objViewBooking_Hotel.getbookedBedroom(Convert.ToInt64(Convert.ToString(ViewState["BookingID"])));

            foreach (BookedBedRoom br in objBookedBR)
            {

                //lblBedroomType.Text = Enum.GetName(typeof(BedRoomType), br.BedRoomIdSource.Types); // Enum.GetName();
                //lblChekOut.Text = String.Format("{0:dd/MM/yyyy}", br.CheckOut);
                //lblCheckIn.Text = String.Format("{0:dd/MM/yyyy}", br.CheckIn);
                //lblpersonName.Text = br.PersonName;
                //lblNote.Text = br.Note;

                //lblbedroomtotalprice.Text = String.Format("{0:#,###}", br.Total);

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }
    #endregion

    #region ItemDataBound
    /// <summary>
    /// ItemDataBound of repeater
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rpmain_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                Label lblbmrid = e.Item.FindControl("lblbmrid") as Label;
                Label lblPackageName = e.Item.FindControl("lblPackageName") as Label;
                Label lbltotalpackage = e.Item.FindControl("lbltotalpackage") as Label;
                Label lblextratotal = e.Item.FindControl("lblextratotal") as Label;
                GridView grdDetailspackage = e.Item.FindControl("grdDetailspackage") as GridView;
                GridView grdextra = e.Item.FindControl("grdextra") as GridView;
                GridView grdequipment = e.Item.FindControl("grdequipment") as GridView;

                //BuildPackageConfigure object
                TList<BuildPackageConfigure> obbuildpack = objViewBooking_Hotel.getPackageDetails(Convert.ToInt64(lblbmrid.Text));
                //BuildMeetingConfigure object
                TList<BuildMeetingConfigure> objBuildMeetingConfigure = objViewBooking_Hotel.getextra(Convert.ToInt64(lblbmrid.Text));

                grdextra.DataSource = objBuildMeetingConfigure.FindAll(a => a.PackageIdSource.IsExtra == true);
                grdextra.DataBind();
                grdequipment.DataSource = objBuildMeetingConfigure.FindAll(a => Convert.ToInt32(a.PackageIdSource.ItemType) == (int)ItemType.Equipment);
                grdequipment.DataBind();
                int cntextra = 0;
                foreach (BuildMeetingConfigure bmc in objBuildMeetingConfigure.FindAll(a => Convert.ToInt32(a.PackageIdSource.ItemType) == (int)ItemType.Equipment))
                {
                    cntextra += Convert.ToInt32(String.Format("{0:#,###}", bmc.TotalPrice));

                }
                lblextratotal.Text = Convert.ToString(cntextra);
                foreach (BuildPackageConfigure bpc in obbuildpack)
                {

                    lblPackageName.Text = bpc.PackageItemIdSource.PackageName;
                    lbltotalpackage.Text = String.Format("{0:#,###}", bpc.TotalPrice);

                    //BuildPackageConfigureDesc object
                    TList<BuildPackageConfigureDesc> obdesc = objViewBooking_Hotel.getpackagedetails(bpc.Id);
                    grdDetailspackage.DataSource = obdesc;
                    grdDetailspackage.DataBind();

                }


            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }
    #endregion





    

    #region PrepareGridViewForExport
    /// <summary>
    /// method to PrepareGridViewForExport
    /// </summary>
    private void PrepareGridViewForExport(Control gv)
    {

        try
        {
            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {

                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].GetType() == typeof(System.Web.UI.HtmlControls.HtmlAnchor))
                {

                    l.Text = (gv.Controls[i] as System.Web.UI.HtmlControls.HtmlAnchor).InnerText;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(DropDownList))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;

                    if ((gv.Controls[i] as DropDownList).SelectedItem.Text == "--Select City--")
                    {
                        l.Text = "City";
                    }
                    if ((gv.Controls[i] as DropDownList).SelectedItem.Text == "--Select Hotel--")
                    {
                        l.Text = "Hotel/Facility";
                    }
                    if ((gv.Controls[i] as DropDownList).SelectedItem.Text == "--Select Meeting Room--")
                    {
                        l.Text = "Meeting Room";
                    }
                    if ((gv.Controls[i] as DropDownList).SelectedItem.Text == "--Select Request Date--")
                    {
                        l.Text = "Booking Date";
                    }
                    if ((gv.Controls[i] as DropDownList).SelectedItem.Text == "--Select Value--")
                    {
                        l.Text = "Value";
                    }

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(GridView))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareGridViewForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    // gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Repeater))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareGridViewForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Panel))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareGridViewForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Table))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareGridViewForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }


                else if (gv.Controls[i].GetType() == typeof(CheckBox))
                {

                    l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(HyperLink))
                {

                    l.Text = (gv.Controls[i] as HyperLink).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                //else if (gv.Controls[i].GetType() == typeof(Label))
                //{

                //    l.Text = (gv.Controls[i] as Label).Text;

                //    gv.Controls.Remove(gv.Controls[i]);

                //    gv.Controls.AddAt(i, l);

                //}
                else if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.Image))
                {

                    // l.Text = (gv.Controls[i] as Image).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareGridViewForExport(gv.Controls[i]);

                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }
    #endregion

    #region Bookingbind


    List<VatCollection> _VatCalculation = new List<VatCollection>();
    public List<VatCollection> VatCalculation
    {
        get
        {
            if (_VatCalculation == null)
            {
                _VatCalculation = new List<VatCollection>();
            }
            return _VatCalculation;
        }
        set
        {
            _VatCalculation = value;
        }
    }



    public void BindHotel(int bookingId)
    {
        try
        {
            
            objRequest = objViewBooking_Hotel.Reqgetxml(Convert.ToInt64(bookingId));
            rptHotel.DataSource = objRequest.HotelList.Where(a => a.MeetingroomList.Count > 0);
            rptHotel.DataBind();
            //Bind Booking Details

            Int64 intHotelID = objRequest.HotelList[0].HotelID;
            // Hotel object
            Hotel htl = objViewBooking_Hotel.GetbyHotelID((Convert.ToInt32(bookingId)));

            //ViewMeetingroom object
            List<ViewMeetingroom> vlistmr = objViewBooking_Hotel.viewMR(Convert.ToInt64(Convert.ToString(bookingId)));


            // Users object
            Users obj = objViewBooking_Hotel.GetUser(Convert.ToInt64(Convert.ToString(bookingId)));
            TList<UserDetails> objDetails = objViewBooking_Hotel.getuserdetails(obj.UserId);
            //lblConatctpersonEmail.HRef = "mailto:" + obj.EmailId;
            lblConatctpersonEmail.Text = obj.EmailId;
            lblContactPerson.Text = obj.FirstName + " " + obj.LastName;
            if (objDetails.Count > 0)
            {
                lblContactAddress.Text = objDetails[0].Address;
                lblContactPhone.Text = objDetails[0].Phone;
            }

            // FillbedroomDetails();

            lblFromDt.Text = objRequest.ArivalDate.ToString("dd MMM yyyy");

            lblToDate.Text = objRequest.DepartureDate.ToString("dd MMM yyyy");
            lblBookedDays.Text = objRequest.Duration == 1 ? objRequest.Duration + " Day" : objRequest.Duration + " Days";
            
            divbookingdetails.Visible = true;
            //lblBookedDays.Text = objRequest.ArivalDate.ToString("dd MMM yyyy");
            //lblBookedDays1.Text = objRequest.DepartureDate.ToString("dd MMM yyyy");
           
            if (objRequest.PackageID != 0)
            {
                pnlPackage.Visible = true;
                PackageMaster packagedetails = objPackagePricingManager.GetAllPackageName().Where(a => a.Id == objRequest.PackageID).FirstOrDefault();
                if (packagedetails != null)
                {
                    lblPackageName.Text = packagedetails.PackageName;

                    if (packagedetails.PackageName == "Standard")
                    {
                        lblpackageDescription.Text = GetKeyResult("STANDARDDESCRIPTION"); // "Package includes main meeting room rental, note-pads, pencils, flipchart and mineral water Morning coffee break, Sandwich buffet with salads, Afternoon coffee break, Softdrinks, coffee and tea for half day packages, either morning or afternoon break is not included only as from min. 10 people.";

                    }
                    else if (packagedetails.PackageName == "Favourite")
                    {
                        lblpackageDescription.Text = GetKeyResult("FAVOURITEDESCRIPTION"); // "Package includes main meeting room rental, note-pads, pencils, flipchart and mineral water Morning coffee break, 2-course menu, Afternoon coffee break, softdrinks, coffee and tea for half day packages, either morning or afternoon break is not included.";
                    }
                    else if (packagedetails.PackageName == "Elegant")
                    {
                        lblpackageDescription.Text = GetKeyResult("ELEGANTDESCRIPTION"); // "Package includes main meeting room rental, note-pads, pencils, flipchart and mineral water Morning coffee break, Hot & Cold buffet, Afternoon coffee break, softdrinks, coffee and tea for half day packages, either morning or afternoon break is not included only as from min. 20 people.";
                    }
                }
                AllPackageItem = objPackagePricingManager.GetAllPackageItems();
                rptPackageItem.DataSource = objPackagePricingManager.GetPackageItemsByPackageID(objRequest.PackageID);
                rptPackageItem.DataBind();
                if (objRequest.ExtraList.Count > 0)
                {
                    //Package Selection.

                    pnlExtra.Visible = true;
                    rptExtras.DataSource = objRequest.ExtraList;
                    rptExtras.DataBind();//check AllPackageItem.Where(a => a.IsExtra == true)
                }
                else
                {
                    pnlExtra.Visible = false;
                }
                pnlFoodAndBravrages.Visible = false;
            }
            else
            {
                pnlPackage.Visible = false;

                if (objRequest.BuildYourMeetingroomList.Count > 0)
                {
                    AllFoodAndBravrages = objPackagePricingManager.GetAllFoodBeveragesItems(intHotelID);
                    pnlFoodAndBravrages.Visible = true;
                    rptFoodandBravragesDay.DataSource = objRequest.DaysList;
                    rptFoodandBravragesDay.DataBind();
                }
                else
                {
                    pnlFoodAndBravrages.Visible = false;
                }
            }


            if (objRequest.EquipmentList.Count > 0)
            {
                AllEquipments = objPackagePricingManager.GetAllEquipmentItems(intHotelID);
                pnlEquipment.Visible = true;
                rptEquipmentDay.DataSource = objRequest.DaysList;
                rptEquipmentDay.DataBind();
            }
            else
            {
                pnlEquipment.Visible = false;
            }
            if (objRequest.OthersList.Count > 0)
            {
                AllOthers = objPackagePricingManager.GetAllOtherItems(intHotelID);
                pnlOthers.Visible = true;
                rptOthersDay.DataSource = objRequest.DaysList;
                rptOthersDay.DataBind();
            }
            else
            {
                pnlOthers.Visible = false;
            }
            if (objRequest.IsAccomodation)
            {
                pnlAccomodation.Visible = true;
                rptDays2.DataSource = objRequest.RequestAccomodationList;
                rptDays2.DataBind();
                rptSingleQuantity.DataSource = objRequest.RequestAccomodationList;
                rptSingleQuantity.DataBind();
                rptDoubleQuantity.DataSource = objRequest.RequestAccomodationList;
                rptDoubleQuantity.DataBind();
                if (objRequest.RequestAccomodationList.Count < 6)
                {
                    ltrExtendDays.Text = "<td colspan='" + (6 - objRequest.RequestAccomodationList.Count) + "' style='border-top: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;padding: 5px;' >&nbsp;</td>";
                    ltrExtendSQuantity.Text = "<td colspan='" + (6 - objRequest.RequestAccomodationList.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
                    ltrExtendDQuantity.Text = "<td colspan='" + (6 - objRequest.RequestAccomodationList.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
                }
                //lblaccomodationQun.Text = Convert.ToString(objRequest.RequestAccomodationList.QuantityDouble);
            }
            else
            {
                pnlAccomodation.Visible = true;
            }
            lblSpecialRequest.Text = objRequest.SpecialRequest;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }


    protected void rptPackageItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label txtQuantity = (Label)e.Item.FindControl("txtQuantity");
            //HiddenField hdnItemId = (HiddenField)e.Item.FindControl("hdnItemId");
            PackageItemMapping pim = e.Item.DataItem as PackageItemMapping;
            if (pim != null)
            {
                PackageItems p = AllPackageItem.Where(a => a.Id == pim.ItemId).FirstOrDefault();
                if (p != null)
                {
                    //hdnItemId.Value = Convert.ToString(p.Id);
                    lblItemName.Text = p.ItemName;
                    PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblDescription.Text = pdesc.ItemDescription;
                    }
                    else
                    {
                        lblDescription.Text = "";
                    }
                    PackageItemDetails pid = objRequest.PackageItemList.Where(a => a.ItemID == p.Id).FirstOrDefault();
                    if (pid != null)
                    {
                        txtQuantity.Text = Convert.ToString(pid.Quantity);
                    }
                    else
                    {
                        txtQuantity.Text = "0";
                    }
                }
            }
        }
    }

    protected void rptHotel_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblHotelName = (Label)e.Item.FindControl("lblHotelName");
            //Label lblRefNo = (Label)e.Item.FindControl("lblRefNo");
            Repeater rptMeetingroom = (Repeater)e.Item.FindControl("rptMeetingroom");
            BuildHotelsRequest b = e.Item.DataItem as BuildHotelsRequest;
            if (b != null)
            {
                Hotel objHtl = objHotel.GetHotelDetailsById(b.HotelID);
                lblHotelName.Text = objHtl.Name;
                rptMeetingroom.DataSource = b.MeetingroomList;
                rptMeetingroom.DataBind();
            }
        }
    }

    protected void rptMeetingroom_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //  Label lblIndex = (Label)e.Item.FindControl("lblIndex");
            Label lblMeetingRoomName = (Label)e.Item.FindControl("lblMeetingRoomName");
            Label lblConfigurationType = (Label)e.Item.FindControl("lblConfigurationType");
            Label lblMaxandMinCapacity = (Label)e.Item.FindControl("lblMaxandMinCapacity");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            Label lblIsMain = (Label)e.Item.FindControl("lblIsMain");
            BuildMeetingRoomRequest brm = e.Item.DataItem as BuildMeetingRoomRequest;
            if (brm != null)
            {
                // lblIndex.Text = (e.Item.ItemIndex + 1).ToString();                
                MeetingRoom objMeetingroom = objHotel.GetMeetingRoomDetailsById(brm.MeetingRoomID);
                MeetingRoomDesc objDesc = objMeetingroom.MeetingRoomDescCollection.Where(a => a.LanguageId == Convert.ToInt32(Session["LanguageID"])).FirstOrDefault();
                MeetingRoomConfig objMrConfig = objMeetingroom.MeetingRoomConfigCollection.Where(a => a.Id == brm.ConfigurationID).FirstOrDefault();
                lblMeetingRoomName.Text = objMeetingroom.Name;
                lblConfigurationType.Text = (objMrConfig.RoomShapeId == (int)RoomShape.Boardroom ? RoomShape.Boardroom.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Classroom ? RoomShape.Classroom.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Cocktail ? RoomShape.Cocktail.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.School ? RoomShape.School.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Theatre ? RoomShape.Theatre.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.UShape ? RoomShape.UShape.ToString() : RoomShape.Boardroom.ToString());
                lblMaxandMinCapacity.Text = objMrConfig.MinCapacity + " - " + objMrConfig.MaxCapicity;
                lblQuantity.Text = brm.Quantity.ToString();
                lblIsMain.Text = brm.IsMain == true ? "Main" : "Breakout";
            }
        }
    }

    protected void rptExtras_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            RequestExtra r = e.Item.DataItem as RequestExtra;
            if (r != null)
            {
                PackageItems objPackage = AllPackageItem.Where(a => a.IsExtra == true && a.Id == r.ItemID).FirstOrDefault();
                if (objPackage != null)
                {
                    lblItemName.Text = objPackage.ItemName;
                    PackageDescription pdesc = objPackage.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblDescription.Text = pdesc.ItemDescription;
                    }
                    else
                    {
                        lblDescription.Text = "";
                    }
                    lblQuantity.Text = Convert.ToString(r.Quantity);
                }
            }
        }
    }

    protected void rptFoodandBravragesDay_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblSelectDay = (Label)e.Item.FindControl("lblSelectDay");
            Repeater rptFoodandBravragesItem = (Repeater)e.Item.FindControl("rptFoodandBravragesItem");
            NumberOfDays nod = e.Item.DataItem as NumberOfDays;
            if (nod != null)
            {
                lblSelectDay.Text = nod.SelectedDay.ToString();
                rptFoodandBravragesItem.DataSource = objRequest.BuildYourMeetingroomList.Where(a => a.SelectDay == nod.SelectedDay);
                rptFoodandBravragesItem.DataBind();
            }
        }
    }

    protected void rptFoodandBravragesItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {


        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            BuildYourMeetingroomRequest bm = e.Item.DataItem as BuildYourMeetingroomRequest;
            if (bm != null)
            {
                PackageItems p = AllFoodAndBravrages.Where(a => a.Id == bm.ItemID).FirstOrDefault();

                if (p != null)
                {
                    lblItemName.Text = p.ItemName;
                    PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblDescription.Text = pdesc.ItemDescription;
                    }
                    else
                    {
                        lblDescription.Text = "";
                    }
                    lblQuantity.Text = Convert.ToString(bm.Quantity);
                }
            }
        }
    }

    protected void rptEquipmentDay_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblSelectDay = (Label)e.Item.FindControl("lblSelectDay");
            Repeater rptEquipmentItem = (Repeater)e.Item.FindControl("rptEquipmentItem");
            NumberOfDays nod = e.Item.DataItem as NumberOfDays;
            if (nod != null)
            {
                lblSelectDay.Text = nod.SelectedDay.ToString();
                rptEquipmentItem.DataSource = objRequest.EquipmentList.Where(a => a.SelectedDay == nod.SelectedDay);
                rptEquipmentItem.DataBind();
            }
        }
    }

    protected void rptEquipmentItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            RequestEquipment re = e.Item.DataItem as RequestEquipment;
            if (re != null)
            {
                PackageItems p = AllEquipments.Where(a => a.Id == re.ItemID).FirstOrDefault();
                if (p != null)
                {
                    lblItemName.Text = p.ItemName;
                    PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblDescription.Text = pdesc.ItemDescription;
                    }
                    else
                    {
                        lblDescription.Text = "";
                    }
                    lblQuantity.Text = Convert.ToString(re.Quantity);
                }
            }
        }
    }

    protected void rptOthersDay_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblSelectDay = (Label)e.Item.FindControl("lblSelectDay");
            Repeater rptOthersItem = (Repeater)e.Item.FindControl("rptOthersItem");
            NumberOfDays nod = e.Item.DataItem as NumberOfDays;
            if (nod != null)
            {
                lblSelectDay.Text = nod.SelectedDay.ToString();
                rptOthersItem.DataSource = objRequest.OthersList.Where(a => a.SelectedDay == nod.SelectedDay);
                rptOthersItem.DataBind();
            }
        }
    }

    protected void rptOthersItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            RequestOthers re = e.Item.DataItem as RequestOthers;
            if (re != null)
            {
                PackageItems p = AllOthers.Where(a => a.Id == re.ItemID).FirstOrDefault();
                if (p != null)
                {
                    lblItemName.Text = p.ItemName;
                    PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblDescription.Text = pdesc.ItemDescription;
                    }
                    else
                    {
                        lblDescription.Text = "";
                    }
                    lblQuantity.Text = Convert.ToString(re.Quantity);
                }
            }
        }
    }
    #endregion

    #region Language
    //This is used for language conversion for static contants.
    public string GetKeyResult(string key)
    {
        //if (XMLLanguage == null)
        //{
        //    XMLLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
        //}
        //XmlNode nodes = XMLLanguage.SelectSingleNode("items/item[@key='" + Key + "']");
        //return nodes.InnerText;//
        return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    }
    #endregion

    #region Go for Calculation
    public void Calculate(Createbooking objCreateBook)
    {
        decimal TotalMeetingroomPrice = 0;
        decimal TotalPackagePrice = 0;
        decimal TotalBuildYourPackagePrice = 0;
        decimal TotalEquipmentPrice = 0;
        decimal TotalExtraPrice = 0;
        bool PackageSelected = false;
        VatCalculation = null;
        VatCalculation = new List<VatCollection>();
        if (objCreateBook != null)
        {
            foreach (BookedMR objb in objCreateBook.MeetingroomList)
            {
                foreach (BookedMrConfig objconfig in objb.MrDetails)
                {
                    TotalMeetingroomPrice = objconfig.NoOfParticepant * objconfig.MeetingroomPrice;
                    //Build mr
                    foreach (BuildYourMR bmr in objconfig.BuildManageMRLst)
                    {
                        TotalBuildYourPackagePrice += bmr.ItemPrice * bmr.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == bmr.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = bmr.vatpercent;
                            v.CalculatedPrice = bmr.ItemPrice * bmr.Quantity * bmr.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == bmr.vatpercent).FirstOrDefault().CalculatedPrice += bmr.ItemPrice * bmr.Quantity * bmr.vatpercent / 100;
                        }
                    }
                    PackageSelected = Convert.ToBoolean(objconfig.PackageID);
                    //Equipment
                    foreach (ManageEquipment eqp in objconfig.EquipmentLst)
                    {
                        TotalEquipmentPrice += eqp.ItemPrice * eqp.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = eqp.vatpercent;
                            v.CalculatedPrice = eqp.ItemPrice * eqp.Quantity * eqp.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault().CalculatedPrice += eqp.ItemPrice * eqp.Quantity * eqp.vatpercent / 100;
                        }
                    }
                    //Manage Extras
                    foreach (ManageExtras ext in objconfig.ManageExtrasLst)
                    {
                        TotalExtraPrice += ext.ItemPrice * ext.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == ext.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = ext.vatpercent;
                            v.CalculatedPrice = ext.ItemPrice * ext.Quantity * ext.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == ext.vatpercent).FirstOrDefault().CalculatedPrice += ext.ItemPrice * ext.Quantity * ext.vatpercent / 100;
                        }
                    }
                    //Manage Package Item
                    foreach (ManagePackageItem pck in objconfig.ManagePackageLst)
                    {
                        TotalPackagePrice += pck.ItemPrice * pck.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == pck.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = pck.vatpercent;
                            v.CalculatedPrice = pck.ItemPrice * pck.Quantity * pck.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == pck.vatpercent).FirstOrDefault().CalculatedPrice += pck.ItemPrice * pck.Quantity * pck.vatpercent / 100;
                        }
                    }

                    //if (PackageSelected)
                    //{
                    //    objBooking.TotalBookingPrice += TotalPackagePrice + TotalExtraPrice + TotalEquipmentPrice + TotalBuildYourPackagePrice;
                    //}
                    //else
                    //{
                    //    objBooking.TotalBookingPrice += TotalMeetingroomPrice + TotalEquipmentPrice + TotalBuildYourPackagePrice;
                    //}
                }
            }

        }
    }
    #endregion

    


    #region RowDataBound
    /// <summary>
    /// RowDataBound of grid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdViewBooking_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            //foreach (GridViewRow gr in grdViewBooking.Rows)
            //{
            string rowID = String.Empty;
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
            {
                //e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#FF9'");
                //e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;");
                //ViewState["rowID"] = e.Row.RowIndex;


                GridViewRow gr = e.Row;
                LinkButton btnchkcommision = (LinkButton)gr.FindControl("btnchkcommision");
                Label lblArrivalDt = (Label)gr.FindControl("lblArrivalDt");
                LinkButton lblRefNo = (LinkButton)gr.FindControl("lblRefNo");
                TList<BookedMeetingRoom> bookedMR = objViewBooking_Hotel.getbookedmeetingroom(Convert.ToInt64(lblRefNo.Text));
                Label lblDepartureDt = (Label)gr.FindControl("lblDepartureDt");
                Label lblExpiryDt = (Label)gr.FindControl("lblExpiryDt");
                Label lblCityName = (Label)gr.FindControl("lblCityName");
                Label lblHotelName = (Label)gr.FindControl("lblHotelName");
                Label lblBookingDt = (Label)gr.FindControl("lblBookingDt");
                Label lblCancellationLimit = (Label)gr.FindControl("lblCancellationLimit");
                CheckBox chkCancel = (CheckBox)gr.FindControl("chkCancel");
                lblCityName.Text = getCityName(lblCityName.ToolTip);
                //lblCancellationLimit.Text = getCancellationLimit(Convert.ToInt32(lblCancellationLimit.ToolTip));


                #region calculation of finalamt after renvenue was adjusted
                Booking bookcomm = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(btnchkcommision.CommandArgument));

                Label lblFinalTotal = (Label)gr.FindControl("lblFinalTotal");
                decimal finalamt = Convert.ToDecimal(bookcomm.FinalTotalPrice);

                //if (bookcomm.RevenueAmount != null)
                //{
                //    finalamt = (decimal)bookcomm.RevenueAmount;
                //    finalamt = Convert.ToDecimal(bookcomm.FinalTotalPrice) / (1 + finalamt / 100);
                //}
                lblFinalTotal.Text = String.Format("{0:#,###}", (Math.Round(finalamt, 2)));
                #endregion


            }
            //}

        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }
    #endregion

    //#region Confirm Commsion
    ///// <summary>
    ///// when check commsion was confirmed
    ///// </summary>
    ///// <param name="sender"></param>
    ///// <param name="e"></param>    

    //protected void btnSubmit_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        Booking objbooking = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(ViewState["ChkcommBookingID"].ToString()));
    //        if (Convert.ToString(ViewState["popup"]) == "1")
    //        {
    //            if (chkmeetingNotheld.Checked)
    //            {
    //                objbooking.RequestStatus = (int)BookingRequestStatus.Cancel;
    //                objbooking.RevenueReason = "Meeting was not held";
    //            }
    //            else
    //            {
    //                #region check file upload
    //                string strPlanName = string.Empty;
    //                if (ulPlan.HasFile)
    //                {
    //                    string fileExtension = Path.GetExtension(ulPlan.PostedFile.FileName.ToString());

    //                    if (fileExtension == ".xls" || fileExtension == ".xlsx" || fileExtension == ".doc" || fileExtension == ".docx" || fileExtension.ToLower() == ".pdf")
    //                    {
    //                        strPlanName = Path.GetFileName(ulPlan.FileName);
    //                    }
    //                    else
    //                    {
    //                        divmessage.InnerHtml = "Please select file in (word,excel,pdf) formats only";
    //                        divmessage.Attributes.Add("class", "error");
    //                        divmessage.Style.Add("display", "block");
    //                        modalcheckcomm.Show();
    //                        return;
    //                    }

    //                    if (ulPlan.PostedFile.ContentLength > 1048576)
    //                    {
    //                        divmessage.InnerHtml = "Filesize of Supporting Document is too large. Maximum file size permitted is 1 MB.";
    //                        divmessage.Attributes.Add("class", "error");
    //                        divmessage.Style.Add("display", "block");
    //                        modalcheckcomm.Show();
    //                        return;
    //                    }
    //                    if (string.IsNullOrEmpty(txtrealvalue.Text))
    //                    {
    //                        divmessage.InnerHtml = "Kindly upload the Supporting Document and the Netto value";
    //                        divmessage.Attributes.Add("class", "error");
    //                        divmessage.Style.Add("display", "block");
    //                        modalcheckcomm.Show();
    //                        return;
    //                    }
    //                }
    //                else
    //                {
    //                    if (string.IsNullOrEmpty(txtrealvalue.Text))
    //                        divmessage.InnerHtml = "Kindly upload the Supporting Document and the Netto value";
    //                    else
    //                        divmessage.InnerHtml = "Kindly upload the Supporting Document.";
    //                    divmessage.Attributes.Add("class", "error");
    //                    divmessage.Style.Add("display", "block");
    //                    modalcheckcomm.Show();
    //                    return;
    //                }

    //                #endregion

    //                ulPlan.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "SupportDocNetto/") + strPlanName);
    //                objbooking.RequestStatus = (int)BookingRequestStatus.Definite;
    //                if (string.IsNullOrEmpty(txtrealvalue.Text))
    //                    txtrealvalue.Text = "0";
    //                //  decimal netvalue = Convert.ToDecimal(txtrealvalue.Text);
    //                // decimal revenue = (Convert.ToDecimal(objbooking.FinalTotalPrice) / (netvalue - 1)) * 100;
    //                // objbooking.RevenueAmount = netvalue;
    //                objbooking.FinalTotalPrice = Convert.ToDecimal(txtrealvalue.Text); // updated
    //                objbooking.RevenueAmount = Convert.ToDecimal(txtrealvalue.Text); // updated
    //                objbooking.RevenueReason = ddlreason.SelectedItem.Text;
    //                objbooking.SupportingDocNetto = strPlanName;
    //            }


    //        }
    //        else
    //        {
    //            objbooking.RequestStatus = Convert.ToInt32(ddlstatus.SelectedValue);

    //        }
    //        if (objViewBooking_Hotel.updatecheckComm(objbooking))
    //        {

    //            FetchListofHotel();
    //            bindgrid();
    //            modalcheckcomm.Hide();
    //            ModalPopupCheck.Hide();

    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        logger.Error(ex);
    //    }
    //    finally
    //    {
    //        txtrealvalue.Text = "";
    //        ddlreason.SelectedIndex = 0;
    //        ddlstatus.SelectedIndex = 0;

    //    }

    //}
    //#endregion
    //#region Check Commission
    ///// <summary>
    ///// when check commsion was checked
    ///// </summary>
    ///// <param name="sender"></param>
    ///// <param name="e"></param>  
    //protected void btnchkcommision_Click(object sender, EventArgs e)
    //{
    //    try
    //    {

    //        LinkButton btnchkcomm = (LinkButton)sender;
    //        ViewState["ChkcommBookingID"] = btnchkcomm.CommandArgument;

    //        if (btnchkcomm.Text.ToLower() == "change")
    //        {
    //            divmessage.Style.Add("display", "none");
    //            ModalPopupCheck.Show();
    //            modalcheckcomm.Hide();
    //            ViewState["popup"] = "0";
    //        }
    //        else
    //        {
    //            //  txtrealvalue.ReadOnly = true;
    //            divmessage.Style.Add("display", "none");
    //            modalcheckcomm.Show();
    //            ModalPopupCheck.Hide();
    //            ViewState["popup"] = "1";
    //        }


    //    }
    //    catch (Exception ex)
    //    {
    //        logger.Error(ex);
    //    }
    //}
    //#endregion





    


    




    

    


    #region Search
    /// <summary>
    /// Search
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  
    //protected void lbtSearch_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        FetchListofHotel();
    //        int totalcount = 0;
    //        string whereclaus = "";
    //        if (Typelink == "1")
    //            whereclaus = "hotelid in (" + Hotelid + ") and requeststatus='" + Typelink + "' and booktype = 1 "; // book type for request and booking
    //        else
    //            whereclaus = "hotelid in (" + Hotelid + ") and requeststatus not in (1) and booktype = 1 "; // book type for request and booking

    //        if (!string.IsNullOrEmpty(txtFromdate.Text) && !string.IsNullOrEmpty(txtTodate.Text))
    //            whereclaus += " and arrivaldate between convert(DATETIME, '" + txtFromdate.Text + "',103) and convert(DATETIME, '" + txtTodate.Text + "',103) ";


    //        string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
    //        vlistreq = objViewBooking_Hotel.Bindgrid(whereclaus, orderby);
    //        grdViewBooking.DataSource = vlistreq;
    //        grdViewBooking.DataBind();

    //        #region Disableaction header
    //        if (Typelink == "5")
    //        {
    //            grdViewBooking.Columns[10].Visible = true;
    //            grdViewBooking.Columns[11].Visible = true;
    //        }
    //        else
    //        {
    //            grdViewBooking.Columns[10].Visible = false;
    //            grdViewBooking.Columns[11].Visible = false;
    //        }

    //        #endregion
    //        ApplyPaging();
    //    }
    //    catch (Exception ex)
    //    {
    //        logger.Error(ex);
    //    }
    //}
    #endregion



    

    #region RowCreated
    /// <summary>
    /// method to RowCreated
    /// </summary>
    protected void grdViewBooking_RowCreated(object sender, GridViewRowEventArgs e)
    {
        try
        {
            string rowID = String.Empty;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#FF9'");
                //e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;");
                ViewState["rowID"] = e.Row.RowIndex;

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    #region New Accommodation
    protected void rptDays2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblDay = (Label)e.Item.FindControl("lblDay");
            RequestAccomodation a = e.Item.DataItem as RequestAccomodation;
            lblDay.Text = a.Checkin.ToString("dd/MM/yyyy");
        }
    }
    protected void rptSingleQuantity_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label txtQuantitySDay = (Label)e.Item.FindControl("txtQuantitySDay");
            //HiddenField hdnDate = (HiddenField)e.Item.FindControl("hdnDate");
            RequestAccomodation a = e.Item.DataItem as RequestAccomodation;
            txtQuantitySDay.Text = a.QuantitySingle.ToString();
            //hdnDate.Value = a.Checkin.ToString();
        }
    }
    protected void rptDoubleQuantity_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label txtQuantityDDay = (Label)e.Item.FindControl("txtQuantityDDay");
            //HiddenField hdnDate = (HiddenField)e.Item.FindControl("hdnDate");
            RequestAccomodation a = e.Item.DataItem as RequestAccomodation;
            txtQuantityDDay.Text = a.QuantityDouble.ToString();
            //hdnDate.Value = a.Checkin.ToString();
        }
    }
    #endregion
}