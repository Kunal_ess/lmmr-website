﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RequestDetails.ascx.cs" Inherits="UserControl_HotelUser_RequestDetails" %>
<div id="divbookingdetails" align="left" runat="server">
    <div class="booking-details" id="Divdetails" runat="server">
        <!--request-step3-body ENDS HERE-->
      <table width="100%" cellpadding="1" cellspacing="1" border="0" class="one">
            <tr>
                <td>
                    <b>Contact name:</b>
                </td>
                <td colspan="2">
                    <asp:Label ID="lblContactPerson" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    <b>Phone No:</b>
                </td>
                <td>
                    <asp:Label ID="lblContactPhone" runat="server" Text=""></asp:Label>
                </td>
                <td >
                    <b>Email :</b> <asp:label id="lblConatctpersonEmail" runat="server"></asp:label>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Date from:</b>
                </td>
                <td colspan="2">
                    <asp:Label ID="lblFromDt" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    <b>To:</b>
                </td>
                <td>
                    <asp:Label ID="lblToDate" runat="server" Text=""></asp:Label>
                </td>
                <td >
                    <b>Duration:</b>
                    <asp:Label ID="lblBookedDays" runat="server" Text=""></asp:Label>
                    <asp:Label ID="lblBookedDays1" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Contact Address</b>
                </td>
                <td colspan="5">
                    <asp:Label ID="lblContactAddress" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <asp:Repeater ID="rptHotel" runat="server" OnItemDataBound="rptHotel_ItemDataBound">
                <ItemTemplate>
                    <tr bgcolor="#E3F0F1">
                        <td colspan="6">
                            <h1 class="first">
                                <asp:Label ID="lblHotelName" runat="server"></asp:Label></h1>
                        </td>
                    </tr>
                    <tr bgcolor="#CCD8D8" valign="top">
                        <td align="left" valign="top" colspan="2">
                            <b>Meeting rooms </b>
                        </td>
                        <td align="left" valign="top" colspan="2">
                            <b>Descriptions</b>
                        </td>
                        <td align="center" valign="top">
                            <b>Qty</b>
                        </td>
                        <td align="left" valign="top">
                            <b>Category </b>
                        </td>
                    </tr>
                    <asp:Repeater ID="rptMeetingroom" runat="server" OnItemDataBound="rptMeetingroom_ItemDataBound">
                        <ItemTemplate>
                            <tr bgcolor="#E3F0F1">
                                <td align="left" colspan="2">
                                    <asp:Label ID="lblMeetingRoomName" runat="server"></asp:Label>
                                </td>
                                <td align="left" colspan="2">
                                    <p>
                                        <asp:Label ID="lblConfigurationType" runat="server">Conference</asp:Label></p>
                                    <p>
                                        <asp:Label ID="lblMaxandMinCapacity" runat="server">50-60</asp:Label></p>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblIsMain" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </ItemTemplate>
            </asp:Repeater>
            <asp:Panel ID="pnlPackage" runat="server">
                <tr bgcolor="#f1f8f8">
                    <td colspan="6">
                        <h1>
                            <span>Package Requested</span></h1>
                    </td>
                </tr>
                <tr bgcolor="#ccd8d8" valign="top">
                    <td colspan="2">
                        &nbsp;
                    </td>
                    <td colspan="4">
                        <b>Description</b>
                    </td>
                </tr>
                <tr bgcolor="#e3f0f1">
                    <td colspan="2">
                        <asp:Label ID="lblPackageName" runat="server"></asp:Label>
                    </td>
                    <td colspan="4">
                        <asp:Label ID="lblpackageDescription" runat="server"></asp:Label>
                    </td>
                </tr>
                <asp:Repeater ID="rptPackageItem" runat="server" OnItemDataBound="rptPackageItem_ItemDataBound">
                    <HeaderTemplate>
                        <tr  bgcolor="#ccd8d8" valign="top">
                            <td colspan="2">
                            </td>
                            <td colspan="3">
                                <b>Description</b>
                            </td>
                            <td>
                                <b>Qty</b>
                            </td>
                        </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr bgcolor="#e3f0f1">
                            <td colspan="2">
                                <asp:Label ID="lblItemName" runat="server"></asp:Label>
                                    
                            </td>
                            <td colspan="3">
                                <asp:Label ID="lblDescription" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtQuantity" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
               
                <asp:Panel ID="pnlExtra" runat="server" Width="100%">
                    <tr bgcolor="#f1f8f8">
                        <td colspan="6">
                            <h1>
                                Extra's</h1>
                        </td>
                    </tr>
                    <asp:Repeater ID="rptExtras" runat="server" OnItemDataBound="rptExtras_ItemDataBound">
                        <HeaderTemplate>
                            <tr bgcolor="#ccd8d8" valign="top">
                                <td colspan="2">
                                </td>
                                <td colspan="3">
                                    <b>Description </b>
                                </td>
                                <td>
                                    <b>Qty </b>
                                </td>
                            </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr bgcolor="#e3f0f1">
                                <td colspan="2">
                                    <asp:Label ID="lblItemName" runat="server"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </asp:Panel>
                <asp:Panel ID="pnlFoodAndBravrages" runat="server">
                    <tr bgcolor="#f1f8f8">
                        <td colspan="6">
                            <h1 style="overflow: hidden">
                                <span>Build your meeting</span></h1>
                        </td>
                    </tr>
                    <tr bgcolor="#ccd8d8" valign="top">
                        <td colspan="2">
                            &nbsp;
                        </td>
                        <td colspan="3">
                            <b>Description </b>
                        </td>
                        <td>
                          <b>  Qty</b>
                        </td>
                    </tr>
                    <asp:Repeater ID="rptFoodandBravragesDay" runat="server" OnItemDataBound="rptFoodandBravragesDay_ItemDataBound">
                        <ItemTemplate>
                            <tr bgcolor="#f1f8f8">
                                <td colspan="6">
                                    <h3>
                                        <b>Day</b>
                                        <asp:Label ID="lblSelectDay" runat="server"></asp:Label></h3>
                                </td>
                            </tr>
                            <asp:Repeater ID="rptFoodandBravragesItem" runat="server" OnItemDataBound="rptFoodandBravragesItem_ItemDataBound">
                                <ItemTemplate>
                                    <tr bgcolor="#e3f0f1">
                                        <td colspan="2">
                                            <asp:Label ID="lblItemName" runat="server"></asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ItemTemplate>
                    </asp:Repeater>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pnlEquipment" runat="server">
                <tr bgcolor="#f1f8f8">
                    <td colspan="6">
                        <h1>
                            Equipment</h1>
                    </td>
                </tr>
                <tr bgcolor="#ccd8d8" valign="top">
                    <td colspan="2">
                        &nbsp;
                    </td>
                    <td colspan="3">
                        <b>Description </b>
                    </td>
                    <td>
                        <b>Qty </b>
                    </td>
                </tr>
                <asp:Repeater ID="rptEquipmentDay" runat="server" OnItemDataBound="rptEquipmentDay_ItemDataBound">
                    <ItemTemplate>
                            <tr bgcolor="#f1f8f8">
                                <td colspan="6">
                                    <h3>
                                        Day
                                        <asp:Label ID="lblSelectDay" runat="server"></asp:Label></h3>
                                </td>
                            </tr>
                            <asp:Repeater ID="rptEquipmentItem" runat="server" OnItemDataBound="rptEquipmentItem_ItemDataBound">
                                <ItemTemplate>
                                    <tr bgcolor="#e3f0f1">
                                        <td colspan="2">
                                            <asp:Label ID="lblItemName" runat="server"></asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                    </ItemTemplate>
                </asp:Repeater>
            </asp:Panel>
          
                  <asp:Panel ID="pnlAccomodation" runat="server">
                  <tr>
              <td colspan="6">
                      <table width="100%" bgcolor="#9cc6ca" cellspacing="1" cellpadding="0">
                          <tr bgcolor="#E3F0F1">
                              <td>
                                  <h1>
                                      Accommodation</h1>
                                  <br />
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                      <tr bgcolor="#d4d9cc">
                                          <td colspan="2" style="border-top: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;
                                              padding: 5px">
                                              Date
                                          </td>
                                          <asp:Repeater ID="rptDays2" runat="server" OnItemDataBound="rptDays2_ItemDataBound">
                                              <ItemTemplate>
                                                  <td valign="top"  align="center" style="border-left: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;
                                                      border-top: #bfd2a5 solid 1px; padding: 5px">
                                                      <asp:Label ID="lblDay" runat="server"></asp:Label>
                                                  </td>
                                              </ItemTemplate>
                                          </asp:Repeater>
                                          <asp:Literal ID="ltrExtendDays" runat="server"></asp:Literal>
                                      </tr>
                                      <tr bgcolor="#cee5ad">
                                          <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                              Quantity
                                          </td>
                                          <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                              Single
                                          </td>
                                          <asp:Repeater ID="rptSingleQuantity" runat="server" OnItemDataBound="rptSingleQuantity_ItemDataBound">
                                              <ItemTemplate>
                                                  <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                                      <asp:HiddenField ID="hdnDate" runat="server" />
                                                      <asp:Label ID="txtQuantitySDay" runat="server">0</asp:Label>
                                                  </td>
                                              </ItemTemplate>
                                          </asp:Repeater>
                                          <asp:Literal ID="ltrExtendSQuantity" runat="server"></asp:Literal>
                                      </tr>
                                      <tr bgcolor="#cee5ad">
                                          <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                              &nbsp;
                                          </td>
                                          <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                              Double
                                          </td>
                                          <asp:Repeater ID="rptDoubleQuantity" runat="server" OnItemDataBound="rptDoubleQuantity_ItemDataBound">
                                              <ItemTemplate>
                                                  <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                                      <asp:HiddenField ID="hdnDate" runat="server" />
                                                      <asp:Label ID="txtQuantityDDay" runat="server">0</asp:Label>
                                                  </td>
                                              </ItemTemplate>
                                          </asp:Repeater>
                                          <asp:Literal ID="ltrExtendDQuantity" runat="server"></asp:Literal>
                                      </tr>
                                  </table>
                              </td>
                          </tr>
                      </table>
                      </td>
          </tr>
                  </asp:Panel>
              
             <tr>
                <td colspan="6">
                    <h1>
                        Special request</h1>
                </td>
            </tr>
            <tr bgcolor="#E3F0F1">
                <td colspan="6">
                    <asp:Label ID="lblSpecialRequest" runat="server">No special request</asp:Label>
                </td>
            </tr>
        </table> 
                
</div>
</div>
