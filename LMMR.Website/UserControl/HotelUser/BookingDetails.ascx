﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BookingDetails.ascx.cs" Inherits="UserControl_HotelUser_BookingDetails" %>

<div id="divbookingdetails" align="left" runat="server" style="width: 100%;">
    <div class="booking-details" id="Divdetails" runat="server" style="width: 100%;">
        <table width="100%" cellpadding="4" bgcolor="#fffff" cellspacing="1" border="0">
            <tr>
                <td colspan="2">
                    <b>Contact name:</b>
                </td>
                <td colspan="1">
                    <asp:Label ID="lblContactPerson" runat="server" Text=""></asp:Label>
                </td>
                <td colspan="1">
                    <b>Phone No:</b>
                </td>
                <td colspan="1">
                    <asp:Label ID="lblContactPhone" runat="server" Text=""></asp:Label>
                </td>
                <td colspan="2">
                    <b>Email :</b> <a id="lblConatctpersonEmail" runat="server" href="#"></a>
                </td>
            </tr>
            <tr >
                <td colspan="2">
                    <b>Date from:</b>
                </td>
                <td colspan="1">
                    <asp:Label ID="lblFromDt" runat="server" Text=""></asp:Label>
                </td>
                <td colspan="1">
                    <b>To:</b>
                </td>
                <td colspan="1">
                    <asp:Label ID="lblToDate" runat="server" Text=""></asp:Label>
                </td>
                <td colspan="2">
                    <b>Duration:</b>
                    <asp:Label ID="lblBookedDays" runat="server" Text=""></asp:Label>
                    <asp:Label ID="lblBookedDays1" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
            <td colspan="2">
                    <b>Contact Address:</b>
                </td>
                <td colspan="2">
                    <asp:Label ID="lblContactAddress" runat="server" Text=""></asp:Label>
                </td>
                <td><strong><asp:Label ID="lblCName" runat="server">Company Name:</asp:Label></strong></td>
                <td colspan="2"><asp:Label ID="lblCompanyName" runat="server" ></asp:Label></td>
                
                
            </tr>
            <asp:Repeater ID="rptMeetingRoom" runat="server" OnItemDataBound="rptMeetingRoom_ItemDataBound">
                <ItemTemplate>
                    <tr bgcolor="#ecf2e3">
                        <td colspan="7">
                            <h1><b>Meeting room 1</b></h1>
                        </td>
                    </tr>
                    <asp:Repeater ID="rptMeetingRoomConfigure" runat="server" OnItemDataBound="rptMeetingRoomConfigure_ItemDataBound">
                        <ItemTemplate>
                            <tr bgcolor="#ecf2e3">
                                <td colspan="7">
                                    <h3>
                                        <b>Day</b>
                                        <asp:Label ID="lblSelectedDay" runat="server">1</asp:Label></h3>
                                </td>
                            </tr>
                            <tr bgcolor="#d4d9cc" style="font-weight: bold">
                                <td>
                                </td>
                                <td align="left">
                                    <b>Description</b>
                                </td>
                                <td align="center" >
                                    <asp:Label ID="lblPriceIfNotPackage" runat="server"><b>Price</b></asp:Label>
                                </td>
                                <td align="center">
                                  <b>  Start</b>
                                </td>
                                <td align="center">
                                   <b> End</b>
                                </td>
                                <td align="center">
                                   <b> Qty</b>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblTotalIfNotPackage" runat="server"><b>Total</b></asp:Label>
                                </td>
                            </tr>
                            <tr bgcolor="#ecf2e3">
                                <td valign="top">
                                    "<asp:Label ID="lblMeetingRoomName" runat="server">Ambasador</asp:Label>"
                                </td>
                                <td align="left" valign="top">
                                    <p>
                                        <asp:Label ID="lblMeetingRoomType" runat="server">Conference</asp:Label></p>
                                    <p>
                                        <asp:Label ID="lblMinMaxCapacity" runat="server">50-60</asp:Label></p>
                                </td>
                                <td align="center" valign="top" >
                                    <asp:Label ID="lblMeetingRoomActualPrice" runat="server"></asp:Label>
                                </td>
                                <td align="center" valign="top">
                                    <asp:Label ID="lblStart" runat="server">08:00</asp:Label>
                                </td>
                                <td align="center" valign="top">
                                    <asp:Label ID="lblEnd" runat="server">12:15</asp:Label>
                                </td>
                                <td align="center" valign="top">
                                    <asp:Label ID="lblNumberOfParticepant" runat="server">32</asp:Label>
                                </td>
                                <td align="center" valign="top" runat="server">
                                    
                                    <asp:Label ID="lblTotalMeetingRoomPrice" runat="server"> 600</asp:Label>
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td colspan="7" align="right" >
                                    <asp:Label ID="lblTotal2IfPackageIsNotAvailable" runat="server">Total:</asp:Label> &nbsp; &nbsp; <b>
                                        <asp:Label ID="lblTotalFinalMeetingRoomPrice" runat="server"> 600</asp:Label></b>
                                </td>
                            </tr>
                            <asp:Panel ID="pnlIsPackageSelected" runat="server">
                                <!--booking-step3-packages-mainbody START HERE-->
                                <tr bgcolor="#ecf2e3">
                                    <td valign="top" colspan="7">
                                        <h1><b>
                                            Packages</b>
                                        </h1>
                                    </td>
                                </tr>
                                <tr bgcolor="#d4d9cc">
                                    <td colspan="2">
                                    </td>
                                    <td align="left" valign="top" colspan="4">
                                        <strong>Description </strong>
                                    </td>
                                    <td align="center" valign="top">
                                        <strong>Total</strong>
                                    </td>
                                </tr>
                                <tr bgcolor="#ecf2e3">
                                    <td valign="top" colspan="2">
                                        <b><asp:Label ID="lblSelectedPackage" runat="server">DDr2</asp:Label></b>
                                    </td>
                                    <td valign="top" align="left" colspan="4">
                                        <p>
                                            <asp:Label ID="lblPackageDescription" runat="server">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam mollis, leo nec consequat
                                                    vehicula, felis augue scelerisque nunc,</asp:Label>
                                        </p>
                                    </td>
                                    <td align="center" valign="top">
                                        
                                        <asp:Label ID="lblPackagePrice" runat="server"> 600</asp:Label>
                                    </td>
                                </tr>
                                <asp:Repeater ID="rptPackageItem" runat="server" OnItemDataBound="rptPackageItem_ItemDataBound">
                                   <HeaderTemplate>
                                        <tr bgcolor="#d4d9cc">
                                            <td colspan="3">
                                            </td>
                                            <td align="left" valign="top">
                                              <b>  Starting Time</b>
                                            </td>
                                            <td align="left" valign="top">
                                               <b> Starting Time</b>
                                            </td>
                                            <td align="left" valign="top">
                                               <b> Qty</b>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr bgcolor="#ecf2e3">
                                            <td valign="top" colspan="3">
                                                
                                                <asp:Label ID="lblPackageItem" runat="server">Welcome cofee break</asp:Label>
                                            </td>
                                            <td align="left" valign="top">
                                                <asp:Label ID="lblFromTime" runat="server"></asp:Label>
                                            </td>
                                            <td align="left" valign="top">
                                                <asp:Label ID="lblToTime" runat="server"></asp:Label>
                                            </td>
                                            <td align="left" valign="top">
                                                <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Panel ID="pnlIsExtra" runat="server">
                                    <tr bgcolor="#ecf2e3">
                                        <td colspan="7" valign="top">
                                           <h1> <b><asp:Label ID="lblextratitle" runat="server" Text=" Extra's"></asp:Label></b></h1>
                                        </td>
                                    </tr>
                                    <asp:Repeater ID="rptExtra" runat="server" OnItemDataBound="rptExtra_ItemDataBound">
                                        <HeaderTemplate>
                                            <tr bgcolor="#d4d9cc">
                                                <td colspan="3">
                                                </td>
                                                <td width="15%">
                                                  <b>  From</b>
                                                </td>
                                                <td width="15%">
                                                   <b> To</b>
                                                </td>
                                                <td width="15%">
                                                   <b> Qty</b>
                                                </td>
                                                <td>
                                                  <b>  Total</b>
                                                </td>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr bgcolor="#ecf2e3">
                                                <td valign="top" colspan="3">
                                                    <asp:Label ID="lblPackageItem" runat="server">Welcome cofee break</asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblFrom" runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblTo" runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblQuntity" runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                   
                                                    <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <tr bgcolor="#ecf2e3">
                                        <td align="right" colspan="7">
                                            
                                            <asp:Label runat="server" ID="lblExtraPrice"> 120</asp:Label>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <tr>
                                    <td colspan="7" align="right" bgcolor="#ffffff">
                                        Total:&nbsp; &nbsp; <b>
                                            <asp:Label ID="lblTotalPackagePrice" runat="server">720</asp:Label></b>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="pnlBuildMeeting" runat="server">
                                <!--booking-step3-BuildMeeting-mainbody START HERE-->
                                <tr bgcolor="#ecf2e3">
                                    <td colspan="7">
                                        <h3>
                                            Build Your Meeting<br />
                                        </h3>
                                    </td>
                                </tr>
                                <asp:Repeater ID="rptBuildMeeting" runat="server" OnItemDataBound="rptBuildMeeting_ItemDataBound">
                                    <HeaderTemplate>
                                        <tr bgcolor="#d4d9cc">
                                            <td colspan="2">
                                            </td>
                                            <td align="left" colspan="3">
                                               <b> Description</b>
                                            </td>
                                            <td align="center">
                                               <b> Qty</b>
                                            </td>
                                            <td align="center">
                                               <b> Total</b>
                                            </td>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr bgcolor="#ecf2e3">
                                            <td align="left" valign="top" colspan="2">
                                                <asp:Label ID="lblItemName" runat="server"></asp:Label>
                                            </td>
                                            <td align="left" valign="top" colspan="3">
                                                <p>
                                                    <asp:Label ID="lblItemDescription" runat="server"></asp:Label>
                                                </p>
                                            </td>
                                            <td align="center" valign="top">
                                                <asp:Label ID="lblQuantity" runat="server">1</asp:Label>
                                            </td>
                                            <td align="center" valign="top">
                                                
                                                <asp:Label ID="lblTotalItemPrice" runat="server">5</asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <tr bgcolor="#ffffff" align="right">
                                    <td colspan="7">
                                        Total:&nbsp; &nbsp; <b>
                                            <asp:Label ID="lblTotalBuildPackagePrice" runat="server"> 30</asp:Label></b>
                                    </td>
                                </tr>
                            </asp:Panel>
                            
                            <!--booking-step3-BuildMeeting ENDS HERE-->
                            <asp:Panel ID="pnlEquipment" runat="server">
                                <!--booking-step3-equipment-mainbody START HERE-->
                                <tr bgcolor="#ecf2e3">
                                    <td colspan="7">
                                        <h1><b>
                                            Equipment</b></h1>
                                    </td>
                                </tr>
                                <asp:Repeater ID="rptEquipment" runat="server" OnItemDataBound="rptEquipment_ItemDataBound">
                                    <HeaderTemplate>
                                        <tr bgcolor="#d4d9cc">
                                            <td colspan="2">
                                            </td>
                                            <td align="left" colspan="3">
                                                <b>Description</b>
                                            </td>
                                            <td align="center">
                                                <b>Qty</b>
                                            </td>
                                            <td align="center">
                                                <b>Total</b>
                                            </td>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr bgcolor="#ecf2e3">
                                            <td colspan="2">
                                                <asp:Label ID="lblItemName" runat="server"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <p>
                                                    <asp:Label ID="lblItemDescription" runat="server"></asp:Label>
                                                </p>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblQuantity" runat="server">1</asp:Label>
                                            </td>
                                            <td align="center">
                                                
                                                <asp:Label ID="lblTotalPrice" runat="server">5</asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <tr bgcolor="#ffffff">
                                    <td colspan="7" align="right">
                                        Total:&nbsp; &nbsp; <b>
                                            <asp:Label ID="lblTotalEquipmentPrice" runat="server">30</asp:Label></b>
                                    </td>
                                </tr>
                                <!--booking-step3-equipment ENDS HERE-->
                            </asp:Panel>

                            <asp:Panel ID="pnlOthers" runat="server">
                                <!--booking-step3-equipment-mainbody START HERE-->
                                <tr bgcolor="#ecf2e3">
                                    <td colspan="7">
                                        <h1><b>
                                            Others</b></h1>
                                    </td>
                                </tr>
                                <asp:Repeater ID="rptOthers" runat="server" OnItemDataBound="rptOthers_ItemDataBound">
                                    <HeaderTemplate>
                                        <tr bgcolor="#d4d9cc">
                                            <td colspan="2">
                                            </td>
                                            <td align="left" colspan="3">
                                                <b>Description</b>
                                            </td>
                                            <td align="center">
                                                <b>Qty</b>
                                            </td>
                                            <td align="center">
                                                <b>Total</b>
                                            </td>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr bgcolor="#ecf2e3">
                                            <td colspan="2">
                                                <asp:Label ID="lblItemName" runat="server"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <p>
                                                    <asp:Label ID="lblItemDescription" runat="server"></asp:Label>
                                                </p>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                
                                                <asp:Label ID="lblTotalPrice" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <tr bgcolor="#ffffff">
                                    <td colspan="7" align="right">
                                        Total:&nbsp; &nbsp; <b>
                                            <asp:Label ID="lblTotalOthersPrice" runat="server"></asp:Label></b>
                                    </td>
                                </tr>
                                <!--booking-step3-equipment ENDS HERE-->
                            </asp:Panel>
                        </ItemTemplate>
                    </asp:Repeater>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr bgcolor="#ecf2e3">
                        <td colspan="7">
                            <h1>
                               <b> Meeting room 2</b></h1>
                        </td>
                    </tr>
                    <asp:Repeater ID="rptMeetingRoomConfigure" runat="server" OnItemDataBound="rptMeetingRoomConfigure_ItemDataBound">
                        <ItemTemplate>
                            <tr bgcolor="#ecf2e3">
                                <td colspan="7">
                                    <h3>
                                        <b>Day</b>
                                        <asp:Label ID="lblSelectedDay" runat="server">1</asp:Label></h3>
                                </td>
                            </tr>
                            <tr bgcolor="#d4d9cc" style="font-weight: bold">
                                <td>
                                </td>
                                <td align="left">
                                    <b>Description</b>
                                </td>
                                <td align="center" >
                                    <asp:Label ID="lblPriceIfNotPackage" runat="server"><b>Price</b></asp:Label>
                                </td>
                                <td align="center">
                                  <b>  Start</b>
                                </td>
                                <td align="center">
                                   <b> End</b>
                                </td>
                                <td align="center">
                                   <b> Qty</b>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblTotalIfNotPackage" runat="server"><b>Total</b></asp:Label>
                                </td>
                            </tr>
                            <tr bgcolor="#ecf2e3">
                                <td valign="top">
                                    "<asp:Label ID="lblMeetingRoomName" runat="server">Ambasador</asp:Label>"
                                </td>
                                <td align="left" valign="top">
                                    <p>
                                        <asp:Label ID="lblMeetingRoomType" runat="server">Conference</asp:Label></p>
                                    <p>
                                        <asp:Label ID="lblMinMaxCapacity" runat="server">50-60</asp:Label></p>
                                </td>
                                <td align="center" valign="top" >
                                    <asp:Label ID="lblMeetingRoomActualPrice" runat="server"></asp:Label>
                                </td>
                                <td align="center" valign="top">
                                    <asp:Label ID="lblStart" runat="server">08:00</asp:Label>
                                </td>
                                <td align="center" valign="top">
                                    <asp:Label ID="lblEnd" runat="server">12:15</asp:Label>
                                </td>
                                <td align="center" valign="top">
                                    <asp:Label ID="lblNumberOfParticepant" runat="server">32</asp:Label>
                                </td>
                                <td id="Td1" align="center" valign="top" runat="server">
                                    
                                    <asp:Label ID="lblTotalMeetingRoomPrice" runat="server"> 600</asp:Label>
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td colspan="7" align="right" >
                                    <asp:Label ID="lblTotal2IfPackageIsNotAvailable" runat="server">Total:</asp:Label> &nbsp; &nbsp; <b>
                                        <asp:Label ID="lblTotalFinalMeetingRoomPrice" runat="server"> 600</asp:Label></b>
                                </td>
                            </tr>
                            <asp:Panel ID="pnlIsBreakdown" runat="server">
                                <tr bgcolor="#ecf2e3">
                                    <td colspan="7" align="right">
                                        <h3>
                                            This meeting room is breakout.</h3>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="pnlNotIsBreakdown" runat="server">
                                <asp:Panel ID="pnlIsPackageSelected" runat="server">
                                    <!--booking-step3-packages-mainbody START HERE-->
                                    <tr bgcolor="#ecf2e3">
                                        <td valign="top" colspan="7">
                                            <h1><b>
                                            Packages</b>
                                        </h1>
                                        </td>
                                    </tr>
                                    <tr bgcolor="#d4d9cc">
                                        <td colspan="2">
                                        </td>
                                        <td align="left" valign="top" colspan="4">
                                            <strong>Description </strong>
                                        </td>
                                        <td align="center" valign="top">
                                            <strong>Total</strong>
                                        </td>
                                    </tr>
                                    <tr bgcolor="#ecf2e3">
                                        <td valign="top" colspan="2">
                                            <asp:Label ID="lblSelectedPackage" runat="server">DDr2</asp:Label>
                                        </td>
                                        <td valign="top" align="left" colspan="4">
                                            <p>
                                                <asp:Label ID="lblPackageDescription" runat="server">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam mollis, leo nec consequat
                                                    vehicula, felis augue scelerisque nunc,</asp:Label>
                                            </p>
                                        </td>
                                        <td align="center" valign="top">
                                            
                                            <asp:Label ID="lblPackagePrice" runat="server"> 600</asp:Label>
                                        </td>
                                    </tr>
                                    <asp:Repeater ID="rptPackageItem" runat="server" OnItemDataBound="rptPackageItem_ItemDataBound">
                                        <HeaderTemplate>
                                            <tr bgcolor="#d4d9cc">
                                                <td colspan="3">
                                                </td>
                                                <td align="left" valign="top">
                                                    <b>Starting Time</b>
                                                </td>
                                                <td align="left" valign="top">
                                                   <b> Starting Time</b>
                                                </td>
                                                <td align="left" valign="top">
                                                    <b>Qty</b>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr bgcolor="#ecf2e3">
                                                <td valign="top" colspan="3">
                                                    <asp:HiddenField ID="hdnItemID" runat="server" />
                                                    <asp:Label ID="lblPackageItem" runat="server">Welcome cofee break</asp:Label>
                                                </td>
                                                <td align="left" valign="top">
                                                    <asp:Label ID="lblFromTime" runat="server"></asp:Label>
                                                </td>
                                                <td align="left" valign="top">
                                                    <asp:Label ID="lblToTime" runat="server"></asp:Label>
                                                </td>
                                                <td align="left" valign="top">
                                                    <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <asp:Panel ID="pnlIsExtra" runat="server">
                                        <tr bgcolor="#ecf2e3">
                                            <td colspan="7" valign="top">
                                                <asp:Label ID="lblextratitle" runat="server" Text=" Extra's"></asp:Label>
                                            </td>
                                        </tr>
                                        <asp:Repeater ID="rptExtra" runat="server" OnItemDataBound="rptExtra_ItemDataBound">
                                            <HeaderTemplate>
                                                <tr bgcolor="#d4d9cc">
                                                    <td colspan="3">
                                                    </td>
                                                    <td width="15%">
                                                       <b> From</b>
                                                    </td>
                                                    <td width="15%">
                                                       <b> To</b>
                                                    </td>
                                                    <td width="15%">
                                                      <b>  Qty</b>
                                                    </td>
                                                    <td>
                                                       <b> Total</b>
                                                    </td>
                                                </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr bgcolor="#ecf2e3">
                                                    <td valign="top" colspan="3">
                                                        <asp:Label ID="lblPackageItem" runat="server">Welcome cofee break</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblFrom" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblTo" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblQuntity" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        
                                                        <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <tr bgcolor="#ecf2e3">
                                            <td align="right" colspan="7">
                                                <span class="currencyClass"></span>
                                                <asp:Label runat="server" ID="lblExtraPrice">120</asp:Label>
                                            </td>
                                        </tr>
                                    </asp:Panel>
                                    <tr>
                                        <td colspan="7" align="right" bgcolor="#ffffff">
                                            Total:&nbsp; &nbsp; <b>
                                                <asp:Label ID="lblTotalPackagePrice" runat="server">720</asp:Label></b>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlBuildMeeting" runat="server">
                                    <!--booking-step3-BuildMeeting-mainbody START HERE-->
                                    <tr bgcolor="#ecf2e3">
                                        <td colspan="7">
                                            <h3>
                                                Build Your Meeting<br />
                                            </h3>
                                        </td>
                                    </tr>
                                    <asp:Repeater ID="rptBuildMeeting" runat="server" OnItemDataBound="rptBuildMeeting_ItemDataBound">
                                        <HeaderTemplate>
                                            <tr bgcolor="#d4d9cc">
                                                <td colspan="2">
                                                </td>
                                                <td align="left" colspan="3">
                                                    <b>Description</b>
                                                </td>
                                                <td align="center">
                                                   <b> Qty</b>
                                                </td>
                                                <td align="center">
                                                   <b> Total</b>
                                                </td>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr bgcolor="#ecf2e3">
                                                <td align="left" valign="top" colspan="2">
                                                    <asp:Label ID="lblItemName" runat="server"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" colspan="3">
                                                    <p>
                                                        <asp:Label ID="lblItemDescription" runat="server"></asp:Label>
                                                    </p>
                                                </td>
                                                <td align="center" valign="top">
                                                    <asp:Label ID="lblQuantity" runat="server">1</asp:Label>
                                                </td>
                                                <td align="center" valign="top">
                                                    <span class="currencyClass"></span>
                                                    <asp:Label ID="lblTotalItemPrice" runat="server">&#8364; 5</asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <tr bgcolor="#ffffff" align="right">
                                        <td colspan="7">
                                            Total:&nbsp; &nbsp; <b>
                                                <asp:Label ID="lblTotalBuildPackagePrice" runat="server"> 30</asp:Label></b>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <!--booking-step3-BuildMeeting ENDS HERE-->
                                
                                <asp:Panel ID="pnlEquipment" runat="server">
                                    <!--booking-step3-equipment-mainbody START HERE-->
                                    <tr bgcolor="#ecf2e3">
                                        <td colspan="7">
                                            <h3>
                                                Equipment</h3>
                                        </td>
                                    </tr>
                                    <asp:Repeater ID="rptEquipment" runat="server" OnItemDataBound="rptEquipment_ItemDataBound">
                                        <HeaderTemplate>
                                            <tr bgcolor="#d4d9cc">
                                                <td colspan="2">
                                                </td>
                                                <td align="left" colspan="3">
                                                    <b>Description</b>
                                                </td>
                                                <td align="center">
                                                    <b>Qty</b>
                                                </td>
                                                <td align="center">
                                                    <b>Total</b>
                                                </td>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr bgcolor="#ecf2e3">
                                                <td colspan="2">
                                                    <asp:Label ID="lblItemName" runat="server"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <p>
                                                        <asp:Label ID="lblItemDescription" runat="server"></asp:Label>
                                                    </p>
                                                </td>
                                                <td align="center">
                                                    <asp:Label ID="lblQuantity" runat="server">1</asp:Label>
                                                </td>
                                                <td align="center">
                                                    <span class="currencyClass"></span>
                                                    <asp:Label ID="lblTotalPrice" runat="server"> 5</asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <tr bgcolor="#ffffff">
                                        <td colspan="7" align="right">
                                            Total:&nbsp; &nbsp; <b>
                                                <asp:Label ID="lblTotalEquipmentPrice" runat="server"> 30</asp:Label></b>
                                        </td>
                                    </tr>
                                    <!--booking-step3-equipment ENDS HERE-->
                                </asp:Panel>

                                <asp:Panel ID="pnlOthers" runat="server">
                                <!--booking-step3-equipment-mainbody START HERE-->
                                <tr bgcolor="#ecf2e3">
                                    <td colspan="7">
                                        <h1><b>
                                            Others</b></h1>
                                    </td>
                                </tr>
                                <asp:Repeater ID="rptOthers" runat="server" OnItemDataBound="rptOthers_ItemDataBound">
                                    <HeaderTemplate>
                                        <tr bgcolor="#d4d9cc">
                                            <td colspan="2">
                                            </td>
                                            <td align="left" colspan="3">
                                                <b>Description</b>
                                            </td>
                                            <td align="center">
                                                <b>Qty</b>
                                            </td>
                                            <td align="center">
                                                <b>Total</b>
                                            </td>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr bgcolor="#ecf2e3">
                                            <td colspan="2">
                                                <asp:Label ID="lblItemName" runat="server"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <p>
                                                    <asp:Label ID="lblItemDescription" runat="server"></asp:Label>
                                                </p>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                
                                                <asp:Label ID="lblTotalPrice" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <tr bgcolor="#ffffff">
                                    <td colspan="7" align="right">
                                        Total:&nbsp; &nbsp; <b>
                                            <asp:Label ID="lblTotalOthersPrice" runat="server"></asp:Label></b>
                                    </td>
                                </tr>
                                <!--booking-step3-equipment ENDS HERE-->
                            </asp:Panel>
                            </asp:Panel>
                        </ItemTemplate>
                    </asp:Repeater>
                </AlternatingItemTemplate>
            </asp:Repeater>
            <tr bgcolor="#ecf2e3">
                <td colspan="7">
                    <asp:Panel ID="pnlAccomodation" runat="server">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td><h1>
                <b><%= GetKeyResult("ACCOMODATION")%></b></h1>  </td></tr></table>
                      
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <asp:Repeater ID="rptAccomodationDaysManager" runat="server" OnItemDataBound="rptAccomodationDaysManager_ItemDataBound">
                        <HeaderTemplate>
                            <tr bgcolor="#d4d9cc">
                                <td colspan="2" style="border-top: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;
                                    padding: 0px">
                                    <%= GetKeyResult("BEDROOMDATE")%>
                                </td>
                                <asp:Repeater ID="rptDays2" runat="server" OnItemDataBound="rptDays2_ItemDataBound" >
                                    <ItemTemplate>
                                        <td valign="top" style="border-left: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;
                                            border-top: #bfd2a5 solid 1px; padding: 0px">
                                            <asp:Label ID="lblDay" runat="server"></asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendDays" runat="server" ></asp:Literal>
                                <td valign="top" style="border-left: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;
                                    border-top: #bfd2a5 solid 1px; padding: 0px; width: 70px">
                                    <%= GetKeyResult("TOTAL")%>
                                </td>
                            </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td colspan="8" style="padding: 0px">
                                    <strong>&quot;<asp:Label ID="lblBedroomType" runat="server"></asp:Label>&quot;<asp:HiddenField ID="hdnBedroomId" runat="server" /> </strong>
                                </td>
                                <td valign="top" style="padding: 0px" align="center">
                                    
                                </td>
                            </tr>
                            <tr bgcolor="#cee5ad">
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                  <%= GetKeyResult("PRICE")%>  
                                </td>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("SINGLE")%>/<%= GetKeyResult("DOUBLE")%>
                                </td>
                                <asp:Repeater ID="rptPrice" runat="server" OnItemDataBound="rptPrice_ItemDataBound">
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                            <asp:Label ID="lblPriceDay" runat="server"></asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendPrice" runat="server"></asp:Literal>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                                <asp:Repeater ID="rptNoOfDays" runat="server" OnItemDataBound="rptNoOfDays_ItemDataBound">
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                            <asp:Label ID="lblAvailableRoomDay" runat="server">3</asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendNoDays" runat="server"></asp:Literal>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr bgcolor="#cee5ad">
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("YOURQTY")%>
                                </td>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("SINGLE")%>
                                </td>
                                <asp:Repeater ID="rptSingleQuantity" runat="server" OnItemDataBound="rptSingleQuantity_ItemDataBound" >
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                            <asp:Label ID="lblQuantitySDay" runat="server"></asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendSQuantity" runat="server"></asp:Literal>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr bgcolor="#cee5ad">
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("DOUBLE")%>
                                </td>
                                <asp:Repeater ID="rptDoubleQuantity" runat="server" OnItemDataBound="rptDoubleQuantity_ItemDataBound">
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                            <asp:Label ID="lblQuantityDDay" runat="server"></asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendDQuantity" runat="server"></asp:Literal>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">&nbsp;</td>                                
                                <td colspan="4" style="padding: 0px;height:45px;">
                                    &nbsp;
                                </td>
                                <td valign="top" style="padding: 0px" align="center">
                                     <asp:Label ID="lblTotalAccomodation" CssClass="price" runat="server">0.00</asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr>
                                <td colspan="8" style="padding: 0px">
                                    <strong>&quot;<asp:Label ID="lblBedroomType" runat="server"></asp:Label>&quot; <asp:HiddenField ID="hdnBedroomId" runat="server" /></strong>
                                </td>
                                <td valign="top" style="padding: 0px" align="center">
                                    
                                </td>
                            </tr>
                            <tr bgcolor="#cee5ad">
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("PRICE")%>
                                </td>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("SINGLE")%>/<%= GetKeyResult("DOUBLE")%>
                                </td>
                                <asp:Repeater ID="rptPrice" runat="server" OnItemDataBound="rptPrice_ItemDataBound">
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                            <asp:Label ID="lblPriceDay" runat="server"></asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendPrice" runat="server"></asp:Literal>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                                <asp:Repeater ID="rptNoOfDays" runat="server" OnItemDataBound="rptNoOfDays_ItemDataBound">
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                            <asp:Label ID="lblAvailableRoomDay" runat="server">3</asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendNoDays" runat="server"></asp:Literal>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr bgcolor="#cee5ad">
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("YOURQTY")%>
                                </td>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("SINGLE")%>
                                </td>
                                <asp:Repeater ID="rptSingleQuantity" runat="server" OnItemDataBound="rptSingleQuantity_ItemDataBound" >
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                            <asp:Label ID="lblQuantitySDay" runat="server" ></asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendSQuantity" runat="server"></asp:Literal>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr bgcolor="#cee5ad">
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("DOUBLE")%>
                                </td>
                                <asp:Repeater ID="rptDoubleQuantity" runat="server" OnItemDataBound="rptDoubleQuantity_ItemDataBound">
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                            <asp:Label ID="lblQuantityDDay" runat="server"></asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendDQuantity" runat="server"></asp:Literal>
                                <td valign="top" align="center" style="padding: 0px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">&nbsp;</td>
                                <td colspan="4" style="padding: 0px;height:45px;">
                                    &nbsp;
                                </td>
                                <td valign="top" style="padding: 0px" align="center">
                                    <asp:Label ID="lblTotalAccomodation" CssClass="price" runat="server">0.00</asp:Label>
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                    <tr>
                        <td colspan="9" style="padding: 0px;" class="special-request-textareabox">
                            <asp:Label ID="lblNoteBedroom" runat="server" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="padding: 0px">
                            &nbsp;
                        </td>
                        <td valign="top" style="padding: 0px">
                            &nbsp;
                        </td>
                        <td valign="top" style="padding: 0px">
                            &nbsp;
                        </td>
                        <td valign="top" style="padding: 0px">
                            &nbsp;
                        </td>
                        <td valign="top" style="padding: 0px">
                            &nbsp;
                        </td>
                        <td colspan="3" valign="top" style="padding: 0px" align="right">
                            <%= GetKeyResult("ACCOMODATION")%> :
                        </td>
                        <td valign="top" style="padding: 0px">
                           <asp:Label ID="lblTotalAccomodation" CssClass="price" runat="server">0.00</asp:Label>
                        </td>
                    </tr>
                </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr bgcolor="#ecf2e3">
                <td colspan="7">
                    <h3>
                        Special request</h3>
                </td>
            </tr>
            <tr bgcolor="#ecf2e3">
                <td colspan="7">
                    <asp:Label ID="lblSpecialRequest" runat="server">No Special Requests</asp:Label>
                </td>
            </tr>
            <tr bgcolor="#ffffff" valign="top">
                <td colspan="7" align="right">
                    Final Total:
                    <asp:Label ID="lblFinalTotal" runat="server">1120</asp:Label>
                    <!--special-request ENDS HERE-->
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    <p>
                        <asp:LinkButton ID="tAndCPopUp" runat="server" Visible="false"><%= GetKeyResult("CANCELLATIONTEXT")%>.</asp:LinkButton></p>
                </td>
            </tr>
        </table>
    </div>
    <div id="Nettotalview" runat="server" style="display: none;">
    <table width="100%" bgcolor="#ffffff" cellspacing="1" cellpadding="4">
    <tr>
    <td align="right" >Net Total: <span class="currencyClass"></span>&#8364;
            <asp:Label ID="lblNetTotal" runat="server"> 1120</asp:Label></td>
    </tr>
    </table>

    </div>
    <asp:Repeater ID="rptVatList" runat="server" OnItemDataBound="rptVatList_ItemDataBound">
        <ItemTemplate>
            <div class="final-total-inner" style="display: none;">
                <div class="final-total-inner-left">
                    <%= ("VAT")%>
                    <asp:Label ID="lblPercentage" runat="server"><%#Eval("VATPercent","{0:0.00}") %></asp:Label>%:
                </div>
                <div class="final-total-inner-right">
                    <span class="currencyClass"></span>
                    <asp:Label ID="lblVatPrice" runat="server"></asp:Label>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>
