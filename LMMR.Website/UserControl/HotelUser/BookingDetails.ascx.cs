﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using LMMR.Entities;
using LMMR.Data;
using LMMR.Business;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Configuration;
using log4net;
using log4net.Config;
using System.Xml;
using System.Web.UI.HtmlControls;
#endregion

public partial class UserControl_HotelUser_BookingDetails : System.Web.UI.UserControl
{

    #region variables
    VList<ViewBookingHotels> vlist;
    VList<Viewbookingrequest> vlistreq;
    ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
    HotelManager objHotelManager = new HotelManager();
    string Typelink = "", AdminUser = "", Hotelid = "0";
    ILog logger = log4net.LogManager.GetLogger(typeof(UserControl_HotelUser_BookingDetails));
    WizardLinkSettingManager ObjWizardManager = new WizardLinkSettingManager();
    Createbooking objBooking = null;
    BookingManager bm = new BookingManager();
    CurrencyManager cm = new CurrencyManager();
    HotelManager objHotel = new HotelManager();
    public string CurrencySign
    {
        get;
        set;
    }
    public XmlDocument XMLLanguage
    {
        get { return (XmlDocument)ViewState["Language"]; }
        set { ViewState["Language"] = value; }
    }
    public string UserCurrency
    {
        get { return "1"; }
        set { ViewState["UserCurrency"] = "1"; }
    }
    public string HotelCurrency
    {
        get { return "1"; }
        set { ViewState["HotelCurrency"] = "1"; }
    }
    public decimal CurrencyConvert
    {
        get { return 1; }
        set { ViewState["CurrencyConvert"] = 1; }
    }
    public decimal CurrentLat
    {
        get;
        set;
    }
    public decimal CurrentLong
    {
        get;
        set;
    }
    public Int64 CurrentMeetingRoomID
    {
        get;
        set;
    }
    public Int64 CurrentMeetingRoomConfigureID
    {
        get;
        set;
    }
    public int CurrentPackageType
    {
        get;
        set;
    }
    public List<ManagePackageItem> CurrentPackageItem
    {
        get;
        set;
    }
    public TList<PackageItems> objHp
    {
        get;
        set;
    }
    public int CurrentDayTime
    {
        get;
        set;
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        //bindDetails();
        //BindBooking();
        Nettotalview.Visible = false;
        rptVatList.Visible = false;
       
    }

    


    protected void rptVatList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblVatPrice = (Label)e.Item.FindControl("lblVatPrice");
            VatCollection v = e.Item.DataItem as VatCollection;
            if (v != null)
            {
                lblVatPrice.Text = CurrencySign + " " + Math.Round(v.CalculatedPrice, 2).ToString();
            }
        }
    }

    protected void rptPackageItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPackageItem = (Label)e.Item.FindControl("lblPackageItem");
            Label lblFromTime = (Label)e.Item.FindControl("lblFromTime");
            Label lblToTime = (Label)e.Item.FindControl("lblToTime");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            PackageItems p = e.Item.DataItem as PackageItems;
            if (p != null)
            {
                ManagePackageItem mpi = CurrentPackageItem.Where(a => a.ItemId == p.Id).FirstOrDefault();
                if (mpi != null)
                {
                    lblPackageItem.Text = p.ItemName;
                    
                    if (p.ItemName.ToLower().Contains("coffee break(s) morning and/or afternoon"))//|| p.ItemName.ToLower().Contains("morning / afternoon") || p.ItemName.ToLower().Contains("morning/ afternoon") || p.ItemName.ToLower().Contains("morning /afternoon") || p.ItemName.ToLower().Contains("morning / afternoon coffee break")
                    {
                        if (CurrentDayTime == 0 || CurrentDayTime == 1)
                        {
                            lblFromTime.Text = mpi.FromTime;
                            lblFromTime.Visible = true;
                        }
                        else
                        {
                            lblFromTime.Text = "NA";
                            //lblFromTime.Visible = false;
                        }
                        if (CurrentDayTime == 0 || CurrentDayTime == 2)
                        {
                            lblToTime.Text = mpi.ToTime;
                            lblToTime.Visible = true;
                        }
                        else
                        {
                            lblToTime.Text = "NA";
                            //lblToTime.Visible = false;
                        }
                    }
                    else
                    {
                        lblFromTime.Text = mpi.FromTime;
                        lblToTime.Visible = false;
                    }
                    //lblToTime.Text = mpi.ToTime;
                    lblQuantity.Text = Convert.ToString(mpi.Quantity);
                }
            }
        }
    }


    protected void rptMeetingRoomConfigure_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            BookedMrConfig objBookedMrConfig = e.Item.DataItem as BookedMrConfig;
            //Bind Meetingroom//
            MeetingRoom objMeetingroom = objHotel.GetMeetingRoomDetailsById(CurrentMeetingRoomID);
            MeetingRoomDesc objDesc = objMeetingroom.MeetingRoomDescCollection.Where(a => a.LanguageId == Convert.ToInt32(Session["LanguageID"])).FirstOrDefault();
            MeetingRoomConfig objMrConfig = objMeetingroom.MeetingRoomConfigCollection.Where(a => a.Id == CurrentMeetingRoomConfigureID).FirstOrDefault();

            Label lblMeetingRoomName = (Label)e.Item.FindControl("lblMeetingRoomName");
            Label lblMeetingRoomType = (Label)e.Item.FindControl("lblMeetingRoomType");
            Label lblMinMaxCapacity = (Label)e.Item.FindControl("lblMinMaxCapacity");
            Label lblMeetingRoomActualPrice = (Label)e.Item.FindControl("lblMeetingRoomActualPrice");
            Label lblTotalMeetingRoomPrice = (Label)e.Item.FindControl("lblTotalMeetingRoomPrice");
            Label lblTotalFinalMeetingRoomPrice = (Label)e.Item.FindControl("lblTotalFinalMeetingRoomPrice");
            lblMeetingRoomName.Text = objMeetingroom.Name;
            lblMeetingRoomType.Text = Enum.GetName(typeof(RoomShape), objMrConfig.RoomShapeId);
            lblMinMaxCapacity.Text = objMrConfig.MinCapacity + " - " + objMrConfig.MaxCapicity;
            lblMeetingRoomActualPrice.Text = CurrencySign + " " + Math.Round(objBookedMrConfig.MeetingroomPrice , 2).ToString();
            lblTotalFinalMeetingRoomPrice.Text = CurrencySign + " " + Math.Round(objBookedMrConfig.MeetingroomPrice , 2).ToString();
            decimal intDiscountMR = objBookedMrConfig.MeetingroomDiscount;
            lblTotalMeetingRoomPrice.Text = CurrencySign + " " + Math.Round(objBookedMrConfig.MeetingroomPrice , 2).ToString();

            //Bind Other details//
            Label lblSelectedDay = (Label)e.Item.FindControl("lblSelectedDay");
            Label lblStart = (Label)e.Item.FindControl("lblStart");
            Label lblEnd = (Label)e.Item.FindControl("lblEnd");
            Label lblNumberOfParticepant = (Label)e.Item.FindControl("lblNumberOfParticepant");
            lblSelectedDay.Text = objBookedMrConfig.SelectedDay.ToString();
            CurrentDayTime = objBookedMrConfig.SelectedTime;
            //lblTotalMeetingRoomPrice.Text = Math.Round(objBookedMrConfig.MeetingroomPrice , 2).ToString();
            lblStart.Text = objBookedMrConfig.FromTime;
            lblEnd.Text = objBookedMrConfig.ToTime;
            lblNumberOfParticepant.Text = objBookedMrConfig.NoOfParticepant.ToString();
            Panel pnlNotIsBreakdown = (Panel)e.Item.FindControl("pnlNotIsBreakdown");
            Panel pnlIsBreakdown = (Panel)e.Item.FindControl("pnlIsBreakdown");
            if (objBookedMrConfig.IsBreakdown == true)
            {
                if (pnlIsBreakdown == null)
                {
                }
                else
                {
                    pnlIsBreakdown.Visible = true;
                    pnlNotIsBreakdown.Visible = false;
                }
            }
            else
            {
                if (pnlIsBreakdown == null)
                {
                }
                else
                {
                    pnlIsBreakdown.Visible = false;
                    pnlNotIsBreakdown.Visible = true;
                }
            }
            #region Package and Equipment Collection
            //Bind Package//
            Panel pnlIsPackageSelected = (Panel)e.Item.FindControl("pnlIsPackageSelected");
            Panel pnlBuildMeeting = (Panel)e.Item.FindControl("pnlBuildMeeting");
            HtmlTableCell divpriceMeetingroom = (HtmlTableCell)e.Item.FindControl("divpriceMeetingroom");
            HtmlTableCell divpriceMeetingroom1 = (HtmlTableCell)e.Item.FindControl("divpriceMeetingroom1");
            HtmlTableCell divpriceMeetingroom2 = (HtmlTableCell)e.Item.FindControl("divpriceMeetingroom2");
            HtmlTableCell divpriceMeetingroom3 = (HtmlTableCell)e.Item.FindControl("divpriceMeetingroom3");
            HtmlTableCell divpriceMeetingroom4 = (HtmlTableCell)e.Item.FindControl("divpriceMeetingroom4");
            if (objBookedMrConfig.PackageID == 0)
            {
                pnlIsPackageSelected.Visible = false;
                if (objBookedMrConfig.BuildManageMRLst.Count > 0)
                {
                    pnlBuildMeeting.Visible = true;
                    Repeater rptBuildMeeting = (Repeater)e.Item.FindControl("rptBuildMeeting");
                    Label lblTotalBuildPackagePrice = (Label)e.Item.FindControl("lblTotalBuildPackagePrice");
                    
                    CurrentPackageType = objBookedMrConfig.SelectedTime;
                    rptBuildMeeting.DataSource = objBookedMrConfig.BuildManageMRLst;
                    rptBuildMeeting.DataBind();
                    lblTotalBuildPackagePrice.Text = CurrencySign + " " + Math.Round((objBookedMrConfig.BuildPackagePriceTotal - (objBookedMrConfig.MeetingroomPrice)) * CurrencyConvert, 2).ToString();
                }
                else
                {
                    pnlBuildMeeting.Visible = false;
                }
                //divpriceMeetingroom.Visible = true;
                //divpriceMeetingroom1.Visible = true;
                //divpriceMeetingroom2.Visible = true;
                //divpriceMeetingroom3.Visible = true;
                //divpriceMeetingroom4.Visible = true;
            }
            else
            {
                pnlIsPackageSelected.Visible = true;
                pnlBuildMeeting.Visible = false;
                Label lblSelectedPackage = (Label)e.Item.FindControl("lblSelectedPackage");
                Label lblPackageDescription = (Label)e.Item.FindControl("lblPackageDescription");
                Label lblPackagePrice = (Label)e.Item.FindControl("lblPackagePrice");
                Label lblTotalPackagePrice = (Label)e.Item.FindControl("lblTotalPackagePrice");
                
                lblPackagePrice.Text = CurrencySign + " " + Math.Round(objBookedMrConfig.PackagePriceTotal * CurrencyConvert, 2).ToString();
                Repeater rptPackageItem = (Repeater)e.Item.FindControl("rptPackageItem");
                Label lblTotal2IfPackageIsNotAvailable = (Label)e.Item.FindControl("lblTotal2IfPackageIsNotAvailable");
                //Label lblTotalFinalMeetingRoomPrice2= (Label)e.Item.FindControl("lblTotalFinalMeetingRoomPrice2");
                //Label lblTotalMeetingRoomPrice2= (Label)e.Item.FindControl("lblTotalMeetingRoomPrice2");
                //Label lblMeetingRoomActualPrice2 = (Label)e.Item.FindControl("lblMeetingRoomActualPrice2");
                Label lblTotalIfNotPackage= (Label)e.Item.FindControl("lblTotalIfNotPackage");
                Label lblPriceIfNotPackage= (Label)e.Item.FindControl("lblPriceIfNotPackage");
                PackageByHotel p = objHotel.GetPackageDetailsByHotel(objBooking.HotelID).FindAllDistinct(PackageByHotelColumn.PackageId).Where(a => a.PackageId == objBookedMrConfig.PackageID).FirstOrDefault();
                if (p != null)
                {

                    CurrentPackageItem = objBookedMrConfig.ManagePackageLst;
                    rptPackageItem.DataSource = objHotel.GetPackageItemDetailsByPackageID(Convert.ToInt64(p.PackageId));
                    rptPackageItem.DataBind();

                    lblSelectedPackage.Text = p.PackageIdSource.PackageName;
                    if (p.PackageIdSource.PackageName.ToLower() == "favourite")
                    {
                        lblPackageDescription.Text = GetKeyResult("FAVOURITEDESCRIPTION");
                    }
                    if (p.PackageIdSource.PackageName.ToLower() == "elegant")
                    {
                        lblPackageDescription.Text = GetKeyResult("ELEGANTDESCRIPTION");
                    }
                    if (p.PackageIdSource.PackageName.ToLower() == "standard")
                    {
                        lblPackageDescription.Text = GetKeyResult("STANDARDDESCRIPTION");
                    }

                    lblTotal2IfPackageIsNotAvailable.Visible = false;
                    lblTotalFinalMeetingRoomPrice.Visible = false;
                    lblTotalMeetingRoomPrice.Visible = false;
                    lblMeetingRoomActualPrice.Visible = false;
                    lblTotalIfNotPackage.Visible = false;
                    lblPriceIfNotPackage.Visible = false;
                }
                else
                {
                    lblTotal2IfPackageIsNotAvailable.Visible = true;
                    lblTotalFinalMeetingRoomPrice.Visible = true;
                    lblTotalMeetingRoomPrice.Visible = true;
                    lblMeetingRoomActualPrice.Visible = true;
                    lblTotalIfNotPackage.Visible = true;
                    lblPriceIfNotPackage.Visible = true;
                }
                Panel pnlIsExtra = (Panel)e.Item.FindControl("pnlIsExtra");
                Label lblextratitle = (Label)e.Item.FindControl("lblextratitle");
                Label lblExtraPrice = (Label)e.Item.FindControl("lblExtraPrice");
                if (objBookedMrConfig.ManageExtrasLst.Count > 0)
                {
                    Repeater rptExtra = (Repeater)e.Item.FindControl("rptExtra");
                    objHp = objHotel.GetIsExtraItemDetailsByHotel(objBooking.HotelID).FindAllDistinct(PackageItemsColumn.Id);
                    rptExtra.DataSource = objBookedMrConfig.ManageExtrasLst;
                    rptExtra.DataBind();
                    pnlIsExtra.Visible = true;

                    lblExtraPrice.Text = CurrencySign + " " + Math.Round(objBookedMrConfig.ExtraPriceTotal * CurrencyConvert, 2).ToString();
                }
                else
                {
                    lblExtraPrice.Visible = false;

                    lblextratitle.Visible = false;
                    pnlIsExtra.Visible = false;

                }

                lblTotalPackagePrice.Text = CurrencySign + " " + Math.Round((objBookedMrConfig.PackagePriceTotal + objBookedMrConfig.ExtraPriceTotal) * CurrencyConvert, 2).ToString();
            }
            Panel pnlEquipment = (Panel)e.Item.FindControl("pnlEquipment");
            if (objBookedMrConfig.EquipmentLst.Count > 0)
            {
                pnlEquipment.Visible = true;
                Repeater rptEquipment = (Repeater)e.Item.FindControl("rptEquipment");
                rptEquipment.DataSource = objBookedMrConfig.EquipmentLst;
                rptEquipment.DataBind();
                Label lblTotalEquipmentPrice = (Label)e.Item.FindControl("lblTotalEquipmentPrice");
                lblTotalEquipmentPrice.Text = CurrencySign + " " + Math.Round(objBookedMrConfig.EquipmentPriceTotal * CurrencyConvert, 2).ToString();
            }
            else
            {
                pnlEquipment.Visible = false;
            }
            Panel pnlOthers = (Panel)e.Item.FindControl("pnlOthers");
            if (objBookedMrConfig.BuildOthers.Count > 0)
            {
                pnlOthers.Visible = true;
                Repeater rptOthers = (Repeater)e.Item.FindControl("rptOthers");
                rptOthers.DataSource = objBookedMrConfig.BuildOthers;
                rptOthers.DataBind();
                Label lblTotalOthersPrice = (Label)e.Item.FindControl("lblTotalOthersPrice");
                lblTotalOthersPrice.Text = CurrencySign + " " + Math.Round(objBookedMrConfig.OtherPriceTotal * CurrencyConvert, 2).ToString();
            }
            else
            {
                pnlOthers.Visible = false;
            }
            #endregion
        }
    }

    #region Language
    //This is used for language conversion for static contants.
    public string GetKeyResult(string key)
    {
        //if (XMLLanguage == null)
        //{
        //    XMLLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
        //}
        //XmlNode nodes = XMLLanguage.SelectSingleNode("items/item[@key='" + Key + "']");
        //return nodes.InnerText;//
        return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    }
    #endregion

    #region New Accomodation
    protected void rptAccomodationDaysManager_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Header)
        {
            Repeater rptDays2 = (Repeater)e.Item.FindControl("rptDays2");
            Literal ltrExtendDays = (Literal)e.Item.FindControl("ltrExtendDays");
            long bedid = (from d in objBooking.ManageAccomodationLst select d.BedroomId).FirstOrDefault();
            List<Accomodation> distinctNames = (from d in objBooking.ManageAccomodationLst where d.BedroomId == bedid select d).ToList();
            rptDays2.DataSource = distinctNames;
            rptDays2.DataBind();
            if (distinctNames.Count < 6)
            {
                ltrExtendDays.Text = "<td colspan='" + (6 - distinctNames.Count) + "' style='border-top: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;padding: 5px;' >&nbsp;</td>";
            }
        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblBedroomType = (Label)e.Item.FindControl("lblBedroomType");
            HiddenField hdnBedroomId = (HiddenField)e.Item.FindControl("hdnBedroomId");
            Repeater rptPrice = (Repeater)e.Item.FindControl("rptPrice");
            Repeater rptNoOfDays = (Repeater)e.Item.FindControl("rptNoOfDays");
            Repeater rptSingleQuantity = (Repeater)e.Item.FindControl("rptSingleQuantity");
            Repeater rptDoubleQuantity = (Repeater)e.Item.FindControl("rptDoubleQuantity");
            Literal ltrExtendPrice = (Literal)e.Item.FindControl("ltrExtendPrice");
            Literal ltrExtendNoDays = (Literal)e.Item.FindControl("ltrExtendNoDays");
            Literal ltrExtendSQuantity = (Literal)e.Item.FindControl("ltrExtendSQuantity");
            Literal ltrExtendDQuantity = (Literal)e.Item.FindControl("ltrExtendDQuantity");
            Label lblTotalAccomodation = (Label)e.Item.FindControl("lblTotalAccomodation");
            BedroomManage bedm = e.Item.DataItem as BedroomManage;
            lblBedroomType.Text = Enum.GetName(typeof(BedRoomType), bedm.bedroomType);
            hdnBedroomId.Value = bedm.BedroomID.ToString();
            List<Accomodation> lstaccomo = (from d in objBooking.ManageAccomodationLst where d.BedroomId == bedm.BedroomID select d).ToList();
            rptPrice.DataSource = lstaccomo;
            rptPrice.DataBind();
            rptNoOfDays.DataSource = lstaccomo;
            rptNoOfDays.DataBind();
            rptSingleQuantity.DataSource = lstaccomo;
            rptSingleQuantity.DataBind();
            rptDoubleQuantity.DataSource = lstaccomo;
            rptDoubleQuantity.DataBind();
            if (lstaccomo.Count < 6)
            {
                ltrExtendNoDays.Text = "<td colspan='" + (6 - lstaccomo.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
                ltrExtendPrice.Text = "<td colspan='" + (6 - lstaccomo.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
                ltrExtendSQuantity.Text = "<td colspan='" + (6 - lstaccomo.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
                ltrExtendDQuantity.Text = "<td colspan='" + (6 - lstaccomo.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
            }
            lblTotalAccomodation.Text = CurrencySign + " " + String.Format("{0:#,##,#0.00}", Math.Round((lstaccomo.Sum(a => a.QuantityDouble > 0 ? a.QuantityDouble * a.RoomPriceDouble * CurrencyConvert : 0) + lstaccomo.Sum(a => a.QuantitySingle > 0 ? a.QuantitySingle * a.RoomPriceSingle * CurrencyConvert : 0)), 2));
        }
    }

    protected void rptDays2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblDay = (Label)e.Item.FindControl("lblDay");
            Accomodation a = e.Item.DataItem as Accomodation;
            lblDay.Text = a.DateRequest.ToString("dd/MM/yyyy");
        }
    }

    protected void rptPrice_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPriceDay = (Label)e.Item.FindControl("lblPriceDay");
            Accomodation a = e.Item.DataItem as Accomodation;
            lblPriceDay.Text = CurrencySign + " " + Math.Round(a.RoomPriceSingle * Math.Round(CurrencyConvert, 2), 2).ToString("#,##,##0.00") + " / " + Math.Round(a.RoomPriceDouble * Math.Round(CurrencyConvert, 2), 2).ToString("#,##,##0.00");
        }
    }

    protected void rptNoOfDays_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblAvailableRoomDay = (Label)e.Item.FindControl("lblAvailableRoomDay");
            Accomodation a = e.Item.DataItem as Accomodation;
            lblAvailableRoomDay.Text = a.RoomAvailable.ToString();
        }
    }

    protected void rptSingleQuantity_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblQuantitySDay = (Label)e.Item.FindControl("lblQuantitySDay");
            //HiddenField hdnDate = (HiddenField)e.Item.FindControl("hdnDate");
            //HiddenField hdnPrice = (HiddenField)e.Item.FindControl("hdnPrice");
            Accomodation a = e.Item.DataItem as Accomodation;
            if (a != null)
            {
                lblQuantitySDay.Text = a.QuantitySingle.ToString();
                lblQuantitySDay.Attributes.Add("roomtype", Enum.GetName(typeof(BedRoomType), a.BedroomType));
                lblQuantitySDay.Attributes.Add("maxvalue", a.RoomAvailable.ToString());
                lblQuantitySDay.Attributes.Add("dateavailable", a.DateRequest.ToString("dd/MM/yyyy"));
                lblQuantitySDay.Attributes.Add("price", Math.Round(a.RoomPriceSingle * CurrencyConvert, 2).ToString());
                //hdnDate.Value = a.DateRequest.ToString();
                //hdnPrice.Value = a.RoomPriceSingle.ToString();
            }
        }
    }

    protected void rptDoubleQuantity_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblQuantityDDay = (Label)e.Item.FindControl("lblQuantityDDay");
            HiddenField hdnDate = (HiddenField)e.Item.FindControl("hdnDate");
            HiddenField hdnPrice = (HiddenField)e.Item.FindControl("hdnPrice");
            Accomodation a = e.Item.DataItem as Accomodation;
            if (a != null)
            {
                if (lblQuantityDDay != null)
                {
                    lblQuantityDDay.Text = a.QuantityDouble.ToString();
                    lblQuantityDDay.Attributes.Add("roomtype", Enum.GetName(typeof(BedRoomType), a.BedroomType));
                    lblQuantityDDay.Attributes.Add("maxvalue", a.RoomAvailable.ToString());
                    lblQuantityDDay.Attributes.Add("dateavailable", a.DateRequest.ToString("dd/MM/yyyy"));
                    lblQuantityDDay.Attributes.Add("price", Math.Round(a.RoomPriceDouble * CurrencyConvert, 2).ToString());
                    //hdnDate.Value = a.DateRequest.ToString();
                    //hdnPrice.Value = a.RoomPriceDouble.ToString();
                }
            }
        }
    }
    #endregion

    protected void rptMeetingRoom_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            BookedMR objBookedMr = e.Item.DataItem as BookedMR;
            CurrentMeetingRoomID = objBookedMr.MRId;
            CurrentMeetingRoomConfigureID = objBookedMr.MrConfigId;

            Repeater rptMeetingRoomConfigure = (Repeater)e.Item.FindControl("rptMeetingRoomConfigure");
            rptMeetingRoomConfigure.DataSource = objBookedMr.MrDetails;
            rptMeetingRoomConfigure.DataBind();
        }
    }

    protected void rptBuildMeeting_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            BuildYourMR objBuildYourMR = e.Item.DataItem as BuildYourMR;
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblItemDescription = (Label)e.Item.FindControl("lblItemDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            Label lblTotalItemPrice = (Label)e.Item.FindControl("lblTotalItemPrice");
            PackageItems p = objHotel.GetItemDetailsByHotelandType(objBooking.HotelID, ItemType.FoodBeverages).FindAllDistinct(PackageItemsColumn.Id).Where(a => a.Id == objBuildYourMR.ItemId).FirstOrDefault();
            if (p != null)
            {
                lblItemName.Text = p.ItemName;
                lblItemDescription.Text = p.PackageDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64(Session["LanguageID"])).FirstOrDefault().ItemDescription;
                lblQuantity.Text = objBuildYourMR.Quantity.ToString();
                lblTotalItemPrice.Text = CurrencySign + " " + Math.Round(objBuildYourMR.ItemPrice * objBuildYourMR.Quantity, 2).ToString();
                //Package Hotel pricing
                //PackageByHotel objp = p.PackageByHotelCollection.Where(a => a.HotelId == objBooking.HotelID && a.PackageId == null).FirstOrDefault();
                //if(objp!=null)
                //{
                //     = Math.Round((CurrentPackageType == 0 ? Convert.ToDecimal(objp.FulldayPrice) : Convert.ToDecimal(objp.HalfdayPrice)) * objBuildYourMR.Quantity * CurrencyConvert,2).ToString();
                //}
            }
        }
    }

    protected void rptEquipment_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ManageEquipment objManageEquipment = e.Item.DataItem as ManageEquipment;
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblItemDescription = (Label)e.Item.FindControl("lblItemDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            Label lblTotalPrice = (Label)e.Item.FindControl("lblTotalPrice");
            PackageItems p = objHotel.GetItemDetailsByHotelandType(objBooking.HotelID, ItemType.Equipment).FindAllDistinct(PackageItemsColumn.Id).Where(a => a.Id == objManageEquipment.ItemId).FirstOrDefault();
            if (p != null)
            {
                lblItemName.Text = p.ItemName;
                lblItemDescription.Text = p.PackageDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64(Session["LanguageID"])).FirstOrDefault().ItemDescription;
                lblQuantity.Text = objManageEquipment.Quantity.ToString();
                lblTotalPrice.Text = CurrencySign + " " + Math.Round(objManageEquipment.ItemPrice * objManageEquipment.Quantity, 2).ToString();
                //Package Hotel pricing
                //PackageByHotel objp = p.PackageByHotelCollection.Where(a => a.HotelId == objBooking.HotelID && a.PackageId == null).FirstOrDefault();
                //if (objp != null)
                //{
                //    lblTotalPrice.Text = Math.Round((CurrentPackageType == 0 ? Convert.ToDecimal(objp.FulldayPrice) : Convert.ToDecimal(objp.HalfdayPrice)) * objManageEquipment.Quantity * CurrencyConvert,2).ToString();
                //}
            }
        }
    }

    protected void rptOthers_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ManageOtherItems objManageEquipment = e.Item.DataItem as ManageOtherItems;
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblItemDescription = (Label)e.Item.FindControl("lblItemDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            Label lblTotalPrice = (Label)e.Item.FindControl("lblTotalPrice");
            //PackageItems p = objHotel.GetItemDetailsByHotelandType(objBooking.HotelID, ItemType.Equipment).FindAllDistinct(PackageItemsColumn.Id).Where(a => a.Id == objManageEquipment.ItemId).FirstOrDefault();
            PackageItems p = objHotel.GetItemDetailsByHotelandType(objBooking.HotelID, ItemType.Others).FindAllDistinct(PackageItemsColumn.Id).Where(a => a.Id == objManageEquipment.ItemId).FirstOrDefault();
            if (p != null)
            {
                lblItemName.Text = p.ItemName;
                lblItemDescription.Text = p.PackageDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64(Session["LanguageID"])).FirstOrDefault().ItemDescription;
                lblQuantity.Text = objManageEquipment.Quantity.ToString();
                lblTotalPrice.Text = CurrencySign + " " + Math.Round(objManageEquipment.ItemPrice * objManageEquipment.Quantity, 2).ToString();
                //Package Hotel pricing
                //PackageByHotel objp = p.PackageByHotelCollection.Where(a => a.HotelId == objBooking.HotelID && a.PackageId == null).FirstOrDefault();
                //if (objp != null)
                //{
                //    lblTotalPrice.Text = Math.Round((CurrentPackageType == 0 ? Convert.ToDecimal(objp.FulldayPrice) : Convert.ToDecimal(objp.HalfdayPrice)) * objManageEquipment.Quantity * CurrencyConvert,2).ToString();
                //}
            }
        }
    }

    protected void rptExtra_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPackageItem = (Label)e.Item.FindControl("lblPackageItem");
            Label lblFrom = (Label)e.Item.FindControl("lblFrom");
            Label lblTo = (Label)e.Item.FindControl("lblTo");
            Label lblQuntity = (Label)e.Item.FindControl("lblQuntity");
            Label lblTotal = (Label)e.Item.FindControl("lblTotal");
            ManageExtras mngExt = e.Item.DataItem as ManageExtras;
            if (mngExt != null)
            {
                PackageItems p = objHp.Where(a => a.Id == mngExt.ItemId).FirstOrDefault();
                if (p != null)
                {
                    lblPackageItem.Text = p.ItemName;
                    lblFrom.Text = mngExt.FromTime;
                   // lblTo.Text = mngExt.ToTime;
                    lblTotal.Text =CurrencySign + " " + Math.Round(mngExt.Quantity * mngExt.ItemPrice, 2).ToString("#,##,##0.00");
                    lblQuntity.Text = mngExt.Quantity.ToString();
                }
            }
            else
            {
                lblQuntity.Text = "0";
            }
        }
    }



    #region PrepareGridViewForExportDIV
    /// <summary>
    /// method to PrepareGridViewForExport a div
    /// </summary>
    private void PrepareDivForExport(Control gv)
    {
        try
        {

            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {

                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].GetType() == typeof(System.Web.UI.HtmlControls.HtmlAnchor))
                {

                    l.Text = (gv.Controls[i] as System.Web.UI.HtmlControls.HtmlAnchor).InnerText;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(DropDownList))
                {

                    l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(GridView))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    // gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Repeater))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Panel))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Table))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }


                else if (gv.Controls[i].GetType() == typeof(CheckBox))
                {

                    l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(HyperLink))
                {

                    l.Text = (gv.Controls[i] as HyperLink).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                //else if (gv.Controls[i].GetType() == typeof(Label))
                //{

                //    l.Text = (gv.Controls[i] as Label).Text;

                //    gv.Controls.Remove(gv.Controls[i]);

                //    gv.Controls.AddAt(i, l);

                //}
                else if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.Image))
                {

                    // l.Text = (gv.Controls[i] as Image).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareDivForExport(gv.Controls[i]);

                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
        //PrepareGridViewForExport();
        //PrepareGridViewForExport();

    }


    #endregion

    

    public void BindBooking(int bookingId)
    {
        // Users object
        Users obj = objViewBooking_Hotel.GetUser(Convert.ToInt64(bookingId));

        //UserDetails object
        TList<UserDetails> objDetails = objViewBooking_Hotel.getuserdetails(obj.UserId);
        lblConatctpersonEmail.HRef = "mailto:" + obj.EmailId;
        lblConatctpersonEmail.InnerText = obj.EmailId;
        lblContactPerson.Text = obj.FirstName + " " + obj.LastName;
        if (objDetails.Count > 0)
        {
            lblContactAddress.Text = objDetails[0].Address;
            lblContactPhone.Text = objDetails[0].Phone;
            if (obj.Usertype == (int)Usertype.Company)
            {
                lblCName.Visible = true;
                lblCompanyName.Visible = true;
                lblCompanyName.Text = string.IsNullOrEmpty(objDetails[0].CompanyName) ? "" : objDetails[0].CompanyName;
            }
            else
            {
                lblCName.Visible = false;
                lblCompanyName.Visible = false;
            }
        }
        else
        {
            lblCName.Visible = false;
            lblCompanyName.Visible = false;
        }
        objBooking = objViewBooking_Hotel.getxml(Convert.ToInt64(bookingId));
        ////Bind Booking Details
        //BindBooking();
        Hotel objHotelDetails = objHotel.GetHotelDetailsById(objBooking.HotelID);
        Currency objcurrency = cm.GetCurrencyDetailsByID(objHotelDetails.CurrencyId);
        HotelCurrency = objcurrency.Currency;
        CurrencySign = objcurrency.CurrencySignature;
        //divbookingdetails.Visible = true;


        lblFromDt.Text = objBooking.ArivalDate.ToString("dd MMM yyyy");
        lblToDate.Text = objBooking.DepartureDate.ToString("dd MMM yyyy");
        lblBookedDays.Text = objBooking.Duration == 1 ? objBooking.Duration + " Day" : objBooking.Duration + " Days";

        // BindHotelDetails(objBooking.HotelID);

        rptMeetingRoom.DataSource = objBooking.MeetingroomList;
        rptMeetingRoom.DataBind();
        BindAccomodation();
        lblSpecialRequest.Text = objBooking.SpecialRequest;
        Calculate(objBooking);
        lblNetTotal.Text = CurrencySign + " " + Math.Round((objBooking.TotalBookingPrice - VatCalculation.Sum(a => a.CalculatedPrice)), 2).ToString();
        rptVatList.DataSource = VatCalculation;
        rptVatList.DataBind();
        lblFinalTotal.Text = CurrencySign + " " + Math.Round(objBooking.TotalBookingPrice, 2).ToString();
        //tAndCPopUp.OnClientClick = "return open_win2('Policy.aspx?hid=" + objBooking.HotelID + "')";
        //tAndCPopUp.OnClientClick = "return open_win2('Policy.aspx?hid=" + objBooking.HotelID + "')";
        //TimeSpan t = DateTime.Now.Subtract(DateTime.Now.AddDays(7));
        //if (t.Days < 7)
        //{
        //    lblCancelation.Visible = true;
        //    lblCancelation.Text = GetKeyResult("THISBOOKINGCANBECANCELED") + " <b>" + GetKeyResult("FREE") + "</b> " + GetKeyResult("OFCHANGEUNTIL") + " <b> " + DateTime.Now.AddDays(7).ToString("dd MMM yyyy") + "</b>";
        //}
        //else
        //{
        //    lblCancelation.Visible = false;
        //}
    }
   
    public void BindAccomodation()
    {
        if (objBooking.NoAccomodation)
        {
            pnlAccomodation.Visible = false;
        }
        else
        {
            if (objBooking.ManageAccomodationLst.Count > 0)
            {
                List<BedroomManage> distinctNames = (from d in objBooking.ManageAccomodationLst select new BedroomManage { BedroomID = d.BedroomId, bedroomType = d.BedroomType }).ToList();
                List<BedroomManage> disListNew = distinctNames.Distinct(new DistinctItemComparer()).ToList();
                lblTotalAccomodation.Text = CurrencySign + " " + string.Format("{0:#,##,##0.00}", objBooking.AccomodationPriceTotal);
                rptAccomodationDaysManager.DataSource = disListNew;
                rptAccomodationDaysManager.DataBind();
                pnlAccomodation.Visible = true;
                lblNoteBedroom.Text = objBooking.BedroomNote;
            }
            else
            {
                pnlAccomodation.Visible = false;
            }
        }
    }

    public void bindDetails()
    {
        //BindBooking();
    }

    List<VatCollection> _VatCalculation = new List<VatCollection>();
    public List<VatCollection> VatCalculation
    {
        get
        {
            if (_VatCalculation == null)
            {
                _VatCalculation = new List<VatCollection>();
            }
            return _VatCalculation;
        }
        set
        {
            _VatCalculation = value;
        }
    }


    #region Go for Calculation
    public void Calculate(Createbooking objCreateBook)
    {
        decimal TotalMeetingroomPrice = 0;
        decimal TotalPackagePrice = 0;
        decimal TotalBuildYourPackagePrice = 0;
        decimal TotalEquipmentPrice = 0;
        decimal TotalExtraPrice = 0;
        decimal TotalOthersPrice = 0;
        bool PackageSelected = false;
        VatCalculation = null;
        VatCalculation = new List<VatCollection>();
        if (objCreateBook != null)
        {
            foreach (BookedMR objb in objCreateBook.MeetingroomList)
            {
                foreach (BookedMrConfig objconfig in objb.MrDetails)
                {
                    TotalMeetingroomPrice = objconfig.NoOfParticepant * objconfig.MeetingroomPrice;
                    //if (VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault() == null)
                    //{
                    //    VatCollection v = new VatCollection();
                    //    v.VATPercent = objconfig.MeetingroomVAT;
                    //    v.CalculatedPrice = objconfig.MeetingroomPrice * objconfig.MeetingroomVAT / 100;
                    //    VatCalculation.Add(v);
                    //}
                    //else
                    //{
                    //    VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault().CalculatedPrice += objconfig.MeetingroomPrice * objconfig.MeetingroomVAT / 100;
                    //}
                    //Build mr
                    foreach (BuildYourMR bmr in objconfig.BuildManageMRLst)
                    {
                        TotalBuildYourPackagePrice += bmr.ItemPrice * bmr.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == bmr.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = bmr.vatpercent;
                            v.CalculatedPrice = (bmr.ItemPrice - (bmr.ItemPrice / ((100 + bmr.vatpercent) / 100))) * bmr.Quantity;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == bmr.vatpercent).FirstOrDefault().CalculatedPrice += (bmr.ItemPrice - (bmr.ItemPrice / ((100 + bmr.vatpercent) / 100))) * bmr.Quantity;
                        }
                    }
                    PackageSelected = Convert.ToBoolean(objconfig.PackageID);
                    //Equipment
                    foreach (ManageEquipment eqp in objconfig.EquipmentLst)
                    {
                        TotalEquipmentPrice += eqp.ItemPrice * eqp.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = eqp.vatpercent;
                            v.CalculatedPrice = (eqp.ItemPrice - (eqp.ItemPrice / ((100 + eqp.vatpercent) / 100))) * eqp.Quantity;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault().CalculatedPrice += (eqp.ItemPrice - (eqp.ItemPrice / ((100 + eqp.vatpercent) / 100))) * eqp.Quantity;
                        }
                    }
                    foreach (ManageOtherItems eqp in objconfig.BuildOthers)
                    {
                        TotalOthersPrice += eqp.ItemPrice * eqp.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = eqp.vatpercent;
                            v.CalculatedPrice = (eqp.ItemPrice - (eqp.ItemPrice / ((100 + eqp.vatpercent) / 100))) * eqp.Quantity;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault().CalculatedPrice += (eqp.ItemPrice - (eqp.ItemPrice / ((100 + eqp.vatpercent) / 100))) * eqp.Quantity;
                        }
                    }
                    //Manage Extras
                    foreach (ManageExtras ext in objconfig.ManageExtrasLst)
                    {
                        TotalExtraPrice += ext.ItemPrice * ext.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == ext.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = ext.vatpercent;
                            v.CalculatedPrice = (ext.ItemPrice - (ext.ItemPrice / ((100 + ext.vatpercent) / 100))) * ext.Quantity;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == ext.vatpercent).FirstOrDefault().CalculatedPrice += (ext.ItemPrice - (ext.ItemPrice / ((100 + ext.vatpercent) / 100))) * ext.Quantity;
                        }
                    }
                    //Manage Package Item
                    decimal restmeetingroomprice = 0;
                    decimal itemtotalprice = 0;
                    foreach (ManagePackageItem pck in objconfig.ManagePackageLst)
                    {
                        itemtotalprice += pck.ItemPrice;
                        TotalPackagePrice += pck.ItemPrice * pck.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == pck.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = pck.vatpercent;
                            v.CalculatedPrice = (pck.ItemPrice - (pck.ItemPrice / ((100 + pck.vatpercent) / 100))) * pck.Quantity;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == pck.vatpercent).FirstOrDefault().CalculatedPrice += (pck.ItemPrice - (pck.ItemPrice / ((100 + pck.vatpercent) / 100))) * pck.Quantity;
                        }
                    }
                    restmeetingroomprice = (objconfig.PackagePricePerPerson - itemtotalprice) * objconfig.NoOfParticepant;
                    if (PackageSelected)
                    {
                        if (VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = objconfig.MeetingroomVAT;
                            v.CalculatedPrice = restmeetingroomprice - (restmeetingroomprice / ((100 + objconfig.MeetingroomVAT) / 100));
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault().CalculatedPrice += restmeetingroomprice - (restmeetingroomprice / ((100 + objconfig.MeetingroomVAT) / 100));
                        }
                    }
                    else
                    {
                        if (VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = objconfig.MeetingroomVAT;
                            v.CalculatedPrice = objconfig.MeetingroomPrice - (objconfig.MeetingroomPrice / ((100 + objconfig.MeetingroomVAT) / 100));
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault().CalculatedPrice += objconfig.MeetingroomPrice - (objconfig.MeetingroomPrice / ((100 + objconfig.MeetingroomVAT) / 100));
                        }
                    }
                }
            }

        }
    }
    #endregion

   



}

class DistinctItemComparer : IEqualityComparer<BedroomManage>
{

    public bool Equals(BedroomManage x, BedroomManage y)
    {
        return x.BedroomID == y.BedroomID &&
            x.bedroomType == y.bedroomType;
    }

    public int GetHashCode(BedroomManage obj)
    {
        return obj.BedroomID.GetHashCode() ^
            obj.bedroomType.GetHashCode();
    }
}