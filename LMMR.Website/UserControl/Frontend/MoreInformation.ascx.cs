﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Data;
using LMMR.Entities;

public partial class MoreInformation : BaseUserControl
{
    ManageCMSContent mcs = new ManageCMSContent();
    //SuperAdminTaskManager manager = new SuperAdminTaskManager();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindMoreInfo();
        }
    }

    public void BindMoreInfo()
    {
        Cms cmsObj = PropCMS.Find(a => a.CmsType.ToLower() == "staticpages");
        if (cmsObj != null)
        {
            rptMoreInformation.DataSource = PropCMSDESCRIPTION.Where(a => a.CmsId == cmsObj.Id && a.LanguageId == Convert.ToInt64(Session["LanguageID"]) && a.IsActive == true).Select(a => new newString() { name = a.ContentTitle }).Distinct();
            rptMoreInformation.DataBind();
            if (rptMoreInformation.Items.Count <= 0)
            {
                rptMoreInformation.DataSource = mcs.GetCMSContent(cmsObj.Id, Convert.ToInt64(Session["LanguageID"])).Select(a => new newString() { name = a.ContentTitle }).Distinct();
                rptMoreInformation.DataBind();
            }
        }
    }

    protected void rptMoreInformation_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            newString c = e.Item.DataItem as newString;
            if (c != null)
            {
                HyperLink hypLink = (HyperLink)e.Item.FindControl("hypLink");
                hypLink.Text = GetKeyResult(c.name.ToUpper().Replace(" ","-"));
                if (l != null)
                {
                    hypLink.NavigateUrl = SiteRootPath +  c.name.ToLower().Replace(" ", "-") + "/" + l.Name.ToLower();
                }
                else
                {
                    hypLink.NavigateUrl = SiteRootPath + c.name.ToLower().Replace(" ", "-") + "/" + l.Name.ToLower();
                }
            }
            //HyperLink hypLink = (HyperLink)e.Item.FindControl("hypLink");
            //hypLink.Text = e.Item.DataItem.ToString();
        }
    }
}
public class newString
{
    public string name
    {
        get;
        set;
    }
}