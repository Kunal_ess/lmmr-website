﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MoreInformation.ascx.cs"
    Inherits="MoreInformation" %>
<asp:Repeater ID="rptMoreInformation" runat="server" 
    onitemdatabound="rptMoreInformation_ItemDataBound">
    <HeaderTemplate>
        <div style="line-height: 3;">
            <h3 style="color:#FFFFFF;" >
                More Information</h3>
            <ul>
    </HeaderTemplate>
    <ItemTemplate>
        <li><asp:HyperLink ID="hypLink" runat="server" ></asp:HyperLink></li>
    </ItemTemplate>
    <FooterTemplate>
        </ul> </div>
    </FooterTemplate>
</asp:Repeater>
