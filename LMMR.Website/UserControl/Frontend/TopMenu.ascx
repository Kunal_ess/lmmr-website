﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopMenu.ascx.cs" Inherits="UserControl_Frontend_TopMenu" %>
<div class="nav">
    <!--Main navigation start -->
    <div id="subnavbar">
        <div id="divSubNav" runat="server">
            <ul id="subnav">
                <li class="cat-item" id="liHome" runat="server">
                    <asp:LinkButton ID="lnkBtnHome" runat="server"><%= GetKeyResult("HOME")%></asp:LinkButton>
                </li>
                <li class="cat-item">
                    <asp:LinkButton ID="lnkBtnAbout" runat="server"><%= GetKeyResult("ABOUTUS")%></asp:LinkButton>
                </li>
                <li class="cat-item">
                    <asp:LinkButton ID="lnkBtnContactUs" runat="server"><%= GetKeyResult("CONTACTUS")%></asp:LinkButton>
                </li>
                <li class="cat-item">
                    <asp:LinkButton ID="lnkBtnJoinToday" runat="server"><%= GetKeyResult("JOINTODAY")%></asp:LinkButton>
                </li>
                <li class="cat-item">
                    <asp:LinkButton ID="lnkBtnLogin" runat="server"><%= GetKeyResult("LOGIN")%></asp:LinkButton>
                    <asp:LinkButton ID="lnkBtnLogout" runat="server" OnClick="lnkBtnLogout_Click"><%= GetKeyResult("LOGOUT")%></asp:LinkButton>
                </li>
            </ul>
        </div>
    </div>
    <!--main navigation end -->
</div>
<div class="nav-form">
    <div id="divLanguage" runat="server">
        <div id="Euro">
            <asp:DropDownList ID="drpCurrency" runat="server" CssClass="smallselect" AutoPostBack="true"
                OnSelectedIndexChanged="drpCurrency_SelectedIndexChanged">
            </asp:DropDownList>
            <%--<select name="test" class="smallselect">
                <option>Euro</option>
                <option value="Euro1">Euro1</option>
                <option value="Euro2">Euro2</option>
                <option value="Euro3">Euro3</option>
            </select>--%>
        </div>
        <div id="English">
            <asp:DropDownList ID="DDLlanguage" CssClass="midselect" runat="server" AutoPostBack="true"
                OnSelectedIndexChanged="DDLlanguage_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
    </div>
</div>
<asp:Literal ID="ltrtestscript" runat="server"></asp:Literal>
<asp:Literal ID="Literal1" runat="server"></asp:Literal>
<script language="javascript" type="text/javascript">
    //    function showItem() {
    //        var selected = jQuery("#<%=DDLlanguage.ClientID %>").val();
    //        alert(jQuery("#<%=DDLlanguage.ClientID %>").val());
    //        if (selected == '4') {
    //            alert("Will be available soon.");
    //            //jQuery("#<%=DDLlanguage.ClientID %>").val('1');
    //            return false;
    //        }
    //        return true;
    //    }

    //    function ShowCurrencyMessage() {
    //        var selected = jQuery("#<%=drpCurrency.ClientID %>").val();
    //        if (selected == '1') {
    //            //alert("Will be available soon.");
    //            //jQuery("#<%=drpCurrency.ClientID %>").val('2')
    //        }
    //        //return false;
    //    }
</script>
