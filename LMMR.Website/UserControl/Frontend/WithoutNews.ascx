﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WithoutNews.ascx.cs" Inherits="UserControl_Frontend_WithoutNews" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style type="text/css">
    .modalBackground
    {
        background-color: #CCCCFF;
        filter: alpha(opacity=40);
        opacity: 0.5;
    }
    .water
    {
        color: Gray;
    }
    .style1
    {
        height: 84px;
    }
</style>
<!--wapper-inner START HERE-->
<div class="mainbody-right-news">
    <div class="mainbody-right-news-top">
    </div>
    <div class="mainbody-right-news-mid">
        <div class="mainbody-right-news-inner">
            <h2>
                <%= GetKeyResult("NEWSLETTER")%></h2>
            <div class="subsribe-bodymain">
                <div id="divmessagenews" class="error" runat="server" style="display:none">
                </div>
                <div class="subsribe-body">
                    <asp:TextBox ID="txtemail" runat="server" class="inputbox" />
                    <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtemail">
                    </asp:TextBoxWatermarkExtender>
                    <asp:Button ID="btnsub" runat="server" CssClass="subsribe" />
                </div>
            </div>
        </div>
    </div>
    <div class="mainbody-right-news-bottom">
    </div>
</div>
<!--mainbody-right-news ENDS HERE-->
<div id="subscribe-overlay">
</div>
<div id="subscribe">
    <div class="popup-top">
    </div>
    <div class="popup-mid">
        <div class="popup-mid-inner">
            <div class="error" id="errorPopupnews" runat="server">
            </div>
            <table cellspacing="10" style="margin-left: 50px">
                <tr>
                    <td>
                        <%=GetKeyResult("FIRSTNAME") %>&nbsp;<asp:DropDownList ID="titleDDL" runat="server">
                            <asp:ListItem Text="Mr." Value="Mr."></asp:ListItem>
                            <asp:ListItem Text="Miss." Value="Miss"></asp:ListItem>
                            <asp:ListItem Text="Mrs." Value="Mrs."></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:TextBox ID="txtFirstnam" runat="server" value="" class="inputregister"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%=GetKeyResult("LASTNAME") %>
                    </td>
                    <td>
                        <asp:TextBox ID="txtlastnam" runat="server" value="" class="inputregister"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%=GetKeyResult("EMAIL")%>
                    </td>
                    <td>
                        <asp:Label ID="Lblemailid" runat="server"></asp:Label><asp:HiddenField ID="hdnemail"
                            runat="server" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="subscribe-btn">
            <div class="save-cancel-btn1 button_section">
                <asp:LinkButton ID="btnSubscribe" runat="server" CssClass="select" OnClick="btnSubscribe_Click"><%=GetKeyResult("SAVE") %></asp:LinkButton>
                &nbsp;or&nbsp;
                <asp:LinkButton ID="lnkCnacel" runat="server" CssClass="cancelpop" OnClientClick="return CloseNewsSubPopUp();"><%=GetKeyResult("CANCEL") %></asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="popup-bottom">
    </div>
</div>
<!--wapper-bottom ENDS HERE-->
<script language="javascript" type="text/javascript">
    function CloseNewsSubPopUp() {
        jQuery('#<%=txtFirstnam.ClientID %>').val('');
        jQuery('#<%=txtlastnam.ClientID %>').val('');
        document.getElementsByTagName('html')[0].style.overflow = 'auto';
        document.getElementById('subscribe').style.display = 'none';
        document.getElementById('subscribe-overlay').style.display = 'none';
        document.getElementById('<%=divmessagenews.ClientID %>').style.display = 'none';
        return false;
    }
    jQuery(document).ready(function () {
       
        jQuery("#<%= divmessagenews.ClientID %>").hide();
        document.getElementById('subscribe').style.display = 'none';
        jQuery("#errorPopupnews").hide();
        jQuery("#<%= btnsub.ClientID %>").bind("click", function () {
            if (jQuery("#<%= divmessagenews.ClientID %>").hasClass("succesfuly")) {
                jQuery("#<%= divmessagenews.ClientID %>").removeClass("succesfuly").addClass("error").hide();
            }

            var isvalid = true;
            var errormessage = "";


            var emailp1 = jQuery("#<%= txtemail.ClientID %>").val();
            if (emailp1.length <= 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }

                errormessage += '<%= GetKeyResultForJavaScript("ERROR")%>';
                isvalid = false;
            }
            else {
                if (!validateEmail(emailp1)) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }

                    errormessage += '<%= GetKeyResultForJavaScript("ERROR")%>';
                    isvalid = false;
                }

            }
            if (!isvalid) {

                jQuery("#<%= divmessagenews.ClientID %>").show();
                jQuery("#<%= divmessagenews.ClientID %>").html(errormessage);
                //                jQuery(".error").show();
                //                jQuery(".error").html(errormessage);
                return false;
            }
            jQuery(".error").html("");
            jQuery(".error").hide();
            //jQuery("#Loding_overlay").show();
            jQuery("body").scrollTop(0);
            jQuery("html").scrollTop(0);
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            jQuery("#subscribe").show();
            jQuery("#subscribe-overlay").show();
            jQuery("#<%= Lblemailid.ClientID %>").html(emailp1);
            jQuery("#<%= hdnemail.ClientID %>").val(emailp1);
            return false;
        });

    });
    function validateEmail(txtEmail) {
        var a = txtEmail;
        var filter = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
        if (filter.test(a)) {
            return true;
        }
        else {
            return false;
        }
    }

    jQuery(document).ready(function () {
        jQuery("#<%= errorPopupnews.ClientID %>").hide();

        jQuery("#<%=btnSubscribe.ClientID %>").bind("click", function () {
            if (jQuery("#<%= errorPopupnews.ClientID %>").hasClass("succesfuly")) {
                jQuery("#<%= errorPopupnews.ClientID %>").removeClass("succesfuly").addClass("error").hide();
            }

            var isvalid = true;
            var errormessage = "";
            var firstName = jQuery("#<%= txtFirstnam.ClientID %>").val().trim();
            // alert(firstName.length);
            if (firstName.length <= 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += '<%= GetKeyResultForJavaScript("ERRORFIRSTNAME")%>';
                isvalid = false;
            }
            var lastName = jQuery("#<%= txtlastnam.ClientID %>").val().trim();
            if (lastName.length <= 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += '<%= GetKeyResultForJavaScript("ERRORLASTNAME")%>';
                isvalid = false;
            }
            if (!isvalid) {

                //jQuery(".error").show();
                //jQuery(".error").html(errormessage);
                jQuery("#<%=errorPopupnews.ClientID %>").show();
                jQuery("#<%=errorPopupnews.ClientID %>").html(errormessage);

                return false;
            }

        });
    });

</script>
