﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NewsSubscriber.ascx.cs"
    Inherits="UserControl_Frontend_NewsSubscriber" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style type="text/css">
    .modalBackground
    {
        background-color: #CCCCFF;
        filter: alpha(opacity=40);
        opacity: 0.5;
    }
    .water
    {
        color: Gray;
    }
    .style2
    {
        height: 64px;
    }
</style>
<!--wapper-inner START HERE-->
<div class="mainbody-right-news">
    <div class="mainbody-right-news-top">
    </div>
    <div class="mainbody-right-news-mid">
        <div class="mainbody-right-news-inner">
            <h2>
                <%= GetKeyResult("NEWS")%></h2>
            <ul>
                <div style="height: 150px; overflow-y: scroll; overflow-x: hidden;">
                    <asp:Panel ID="panel1" runat="server">
                        <%-- <marquee direction="up" behavior="alternate" height="100" onmouseover="this.stop()"
                        onmouseout="this.start()" scrollamount="5" id="Marquee1" runat="server">--%>
                        <asp:GridView ID="grdvwnews" runat="server" AutoGenerateColumns="False" GridLines="None"
                            ShowHeader="False" OnRowDataBound="grdvwnews_RowDataBound">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <li>
                                            <%--<asp:HyperLink ID="HynNews" runat="server" Target="_blank"></asp:HyperLink>--%>
                                            <asp:LinkButton ID="HynNews" runat="server"></asp:LinkButton>
                                            <div class="date">
                                                <asp:Label ID="lbldate" runat="server"></asp:Label>
                                            </div>
                                        </li>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <%--</marquee>--%>
                    </asp:Panel>
                </div>
            </ul>
            <div class="subsribe-bodymain">
                <div id="divmessagenews" class="error" runat="server" style="border 1px solid red;
                    display: none;">
                </div>
                <div class="subsribe-body">
                    <asp:TextBox ID="txtemail" runat="server" class="inputbox" />
                    <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtemail">
                    </asp:TextBoxWatermarkExtender>
                    <asp:Button ID="btnsub" runat="server" CssClass="subsribe" />
                </div>
            </div>
        </div>
    </div>
    <div class="mainbody-right-news-bottom">
    </div>
</div>
<!--mainbody-right-news ENDS HERE-->
<div id="subscribe-overlay">
</div>
<div id="subscribe">
    <div class="popup-top">
    </div>
    <div class="popup-mid">
        <div class="popup-mid-inner">
            <div class="error" id="errorPopupnews" runat="server">
            </div>
            <table cellspacing="10" style="margin-left: 50px">
                <tr>
                    <td>
                        <%=GetKeyResult("FIRSTNAME") %>&nbsp;<asp:DropDownList ID="titleDDL" runat="server">
                            <asp:ListItem Text="Mr." Value="Mr."></asp:ListItem>
                            <asp:ListItem Text="Miss." Value="Miss"></asp:ListItem>
                            <asp:ListItem Text="Mrs." Value="Mrs."></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:TextBox ID="txtFirstnam" runat="server" value="" class="inputregister" MaxLength="30"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%=GetKeyResult("LASTNAME") %>
                    </td>
                    <td>
                        <asp:TextBox ID="txtlastnam" runat="server" value="" class="inputregister" MaxLength="30"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        <%=GetKeyResult("EMAIL")%>
                    </td>
                    <td class="style2">
                        <asp:Label ID="Lblemailid" runat="server"></asp:Label><asp:HiddenField ID="hdnemail"
                            runat="server" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="subscribe-btn">
            <div class="save-cancel-btn1 button_section">
                <asp:LinkButton ID="btnSubscribe" runat="server" CssClass="select" OnClientClick="javascript: _gaq.push(['_trackPageview', 'SubscribeNewsOK.aspx']);" OnClick="btnSubscribe_Click" ><%=GetKeyResult("SAVE") %></asp:LinkButton>
                &nbsp;or&nbsp;
                <asp:LinkButton ID="lnkCnacel" runat="server" CssClass="cancelpop" OnClientClick="return CloseNewsSubPopUp();"><%=GetKeyResult("CANCEL") %></asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="popup-bottom">
    </div>
</div>
<!--wapper-bottom ENDS HERE-->

<script type="text/javascript">
    function open_win(pageurl) {
        var url = pageurl;
        var winName = 'myWindow';
        var w = '700';
        var h = '500';
        var scroll = 'no';
        var popupWindow = null;
        LeftPosition = (screen.width) ? (screen.width - w) / 2 : 0;
        TopPosition = (screen.height) ? (screen.height - h) / 2 : 0;
        settings = 'height=' + h + ',width=' + w + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=no,directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no';
        popupWindow = window.open(url, winName, settings)
        return false;
    }
</script>
<script language="javascript" type="text/javascript">

    jQuery(document).ready(function () {
        jQuery("#<%= divmessagenews.ClientID %>").hide();
        document.getElementById('subscribe').style.display = 'none';
        jQuery("#<%= btnsub.ClientID %>").bind("click", function () {
            if (jQuery("#<%= divmessagenews.ClientID %>").hasClass("succesfuly")) {
                jQuery("#<%= divmessagenews.ClientID %>").removeClass("succesfuly").addClass("error").hide();
            }

            var isvalid = true;
            var errormessage = "";
            var emailp1 = jQuery("#<%= txtemail.ClientID %>").val();
            if (emailp1.length <= 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }

                errormessage += '<%= GetKeyResultForJavaScript("ERROR")%>';
                isvalid = false;
            }
            else {
                if (!validateEmail(emailp1)) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }

                    errormessage += '<%= GetKeyResultForJavaScript("ERROR")%>';
                    isvalid = false;
                }

            }
            if (!isvalid) {

                jQuery("#<%= divmessagenews.ClientID %>").show();
                jQuery("#<%= divmessagenews.ClientID %>").html(errormessage);
                //                jQuery(".error").show();
                //                jQuery(".error").html(errormessage);
                return false;
            }
            jQuery(".error").html("");
            jQuery(".error").hide();
            //jQuery("#Loding_overlay").show();
            jQuery("body").scrollTop(0);
            jQuery("html").scrollTop(0);
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            jQuery("#subscribe").show();
            jQuery("#subscribe-overlay").show();
            jQuery("#<%= Lblemailid.ClientID %>").html(emailp1);
            jQuery("#<%= hdnemail.ClientID %>").val(emailp1);
            return false;
        });

    });

    function validateEmail(txtEmail) {
        var a = txtEmail;
        var filter = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
        if (filter.test(a)) {
            return true;
        }
        else {
            return false;
        }
    }

    jQuery(document).ready(function () {
        jQuery("#<%= errorPopupnews.ClientID %>").hide();

        jQuery("#<%=btnSubscribe.ClientID %>").bind("click", function () {
            if (jQuery("#<%= errorPopupnews.ClientID %>").hasClass("succesfuly")) {
                jQuery("#<%= errorPopupnews.ClientID %>").removeClass("succesfuly").addClass("error").hide();
            }

            var isvalid = true;
            var errormessage = "";
            var firstName = jQuery("#<%= txtFirstnam.ClientID %>").val().trim();
            // alert(firstName.length);
            if (firstName.length <= 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += '<%= GetKeyResultForJavaScript("ERRORFIRSTNAME")%>';
                isvalid = false;
            }
            var lastName = jQuery("#<%= txtlastnam.ClientID %>").val().trim();
            if (lastName.length <= 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += '<%= GetKeyResultForJavaScript("ERRORLASTNAME")%>';
                isvalid = false;
            }
            if (!isvalid) {

                //jQuery(".error").show();
                //jQuery(".error").html(errormessage);

                jQuery("#<%=errorPopupnews.ClientID %>").show();
                jQuery("#<%=errorPopupnews.ClientID %>").html(errormessage);

                return false;
            }

        });
    });

    function CloseNewsSubPopUp() {
        jQuery('#<%=txtFirstnam.ClientID %>').val('');
        jQuery('#<%=txtlastnam.ClientID %>').val('');
        document.getElementsByTagName('html')[0].style.overflow = 'auto';
        document.getElementById('subscribe').style.display = 'none';
        document.getElementById('subscribe-overlay').style.display = 'none';
        document.getElementById('<%=divmessagenews.ClientID %>').style.display = 'none';
        return false;
    }

</script>
