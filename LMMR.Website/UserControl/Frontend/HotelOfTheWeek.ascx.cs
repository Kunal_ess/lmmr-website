﻿#region Using
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using System.Linq;
#endregion

public partial class UserControl_Frontend_HotelOfTheWeek : BaseUserControl
{
    #region Variables and Properties
    public int intTypeCMSid;
    public int intMinSpl;
    ManageCMSContent objManageCMSContent = new ManageCMSContent();
    //SuperAdminTaskManager manager = new SuperAdminTaskManager();
    public string TimeForSlider
    {
        get;
        set;
    }
    #endregion

    #region PageLoad
    /// <summary>
    /// This method is call on the Page load and Initlize all the components of the page to its initial stage.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
       if (!Page.IsPostBack)
        {
            Others o = DataRepository.OthersProvider.GetAll().FirstOrDefault();
            TimeForSlider = (o.ScrollHotelOfWeek.Value * 1000).ToString();
            BindHotelOftheWeek();
            GetMinConsiderSpll();
        }
    }
    #endregion

    #region Functions
    /// <summary>
    /// This method bind the Hotel of The week with gridview
    /// </summary>
    public void BindHotelOftheWeek()
    {
        Cms cmsObj = PropCMS.Find(a => a.CmsType.ToLower() == "hoteloftheweek");
        if (cmsObj != null)
        {
            gvHotelOfWeek.DataSource = PropCMSDESCRIPTION.Where(a => a.CmsId == cmsObj.Id && a.LanguageId == Convert.ToInt64(Session["LanguageID"]) && a.IsActive == true).OrderByDescending(u => u.UpdatedDate);
            gvHotelOfWeek.DataBind();
            if (gvHotelOfWeek.Items.Count <= 0)
            {
                gvHotelOfWeek.DataSource = PropCMSDESCRIPTION.Where(a => a.CmsId == cmsObj.Id && a.LanguageId == Convert.ToInt64("1") && a.IsActive == true);
                gvHotelOfWeek.DataBind();
            }
        }
        else
        {
            Cms cmsObj2 = manager.getCmsEntityOnType("hoteloftheweek");
            if (cmsObj != null)
            {
                gvHotelOfWeek.DataSource = objManageCMSContent.GetCMSContent(cmsObj2.Id, Convert.ToInt64(Session["LanguageID"])).OrderByDescending(u => u.UpdatedDate);
                gvHotelOfWeek.DataBind();
                if (gvHotelOfWeek.Items.Count <= 0)
                {
                    gvHotelOfWeek.DataSource = objManageCMSContent.GetCMSContent(cmsObj2.Id, Convert.ToInt64("1"));
                    gvHotelOfWeek.DataBind();
                }
            }
        }
        //Cms cmsObj = manager.getCmsEntityOnType("HoteloftheWeek");
        //if (cmsObj != null)
        //{
        //    gvHotelOfWeek.DataSource = objManageCMSContent.GetCMSContent(cmsObj.Id, Convert.ToInt64(Session["LanguageID"]));
        //    gvHotelOfWeek.DataBind();
        //}
    }

    ///// <summary>
    ///// This method return the pass the key value through GetResult Function 
    ///// </summary>
    ///// <param name="Key"></param>
    ///// <returns></returns>
    //public string GetKeyResult(string key)
    //{
    //    return  System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    //}

    /// <summary>
    /// This method bind the marquee scroll smount with others table
    /// </summary>
    /// <returns></returns>

    public void GetMinConsiderSpll()
    {

        TList<Others> getScrllamount = objManageCMSContent.GetMinConsiderSpl();

        if (getScrllamount != null)
        {
            intMinSpl = 50;
        }
    }


    #endregion

    #region events
    /// <summary>
    /// This event is used for page indexing in gridview
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //protected void gvHotelOfWeek_PageIndexChanging1(object sender, GridViewPageEventArgs e)
    //{
    //    gvHotelOfWeek.PageIndex = e.NewPageIndex;
    //    BindHotelOftheWeek();
    //}
    protected void gvHotelOfWeek_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CmsDescription obj = e.Item.DataItem as CmsDescription;
            if (obj != null)
            {
                Image imgNewsIcon = (Image)e.Item.FindControl("imgNewsIcon");
                if (obj.ContentImage != null)
                {
                    imgNewsIcon.ImageUrl = ConfigurationManager.AppSettings["FilePath"] + "HotelOfTheWeek/" + obj.ContentImage;
                }

                Label lblHotelName = (Label)e.Item.FindControl("lblHotelName");
                if (obj.ContentTitle != null)
                {
                    lblHotelName.Text = obj.ContentTitle.Split(',')[0].ToString();
                }

                Label lblDesc = (Label)e.Item.FindControl("lblDesc");
                if (obj.ContentsDesc != null)
                {
                    lblDesc.Text = obj.ContentsDesc.ToString();
                }

                Label lbldate = (Label)e.Item.FindControl("lbldate");
                if (obj.UpdatedDate != null)
                {
                    //lbldate.Text = obj.UpdatedDate.Value.ToString("MM/dd/yyyy").Replace("-", "/");
                   //bldate.Text = obj.ContentShortDesc;
                    lbldate.Text = String.Format("{0:dd.MM.yyyy}",obj.FromDate) + "-" + String.Format("{0:dd.MM.yyyy}",obj.ToDate);
                }

            }
        }
    }
    #endregion




}