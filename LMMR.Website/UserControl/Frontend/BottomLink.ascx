﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BottomLink.ascx.cs" Inherits="UserControl_Frontend_BottomLink" %>
<%@ Register Src="~/UserControl/Frontend/MoreInformation.ascx" TagName="MoreInformation"
    TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:HiddenField ID="hdnPageUrl" runat="server" />
<uc1:moreinformation id="moreinfo1" runat="server">
</uc1:moreinformation>
<asp:Repeater ID="datalstBottomlink" runat="server"
    OnItemDataBound="datalstBottomlink_ItemDataBound">
    <HeaderTemplate>
            <ul>
    </HeaderTemplate>
    <ItemTemplate>
            <li>
                <asp:LinkButton ID="lnkBtn" runat="server"><%# Eval("ContentsDesc") %></asp:LinkButton>
                <%--<asp:HyperLink ID="hypButtomLink" runat="server" Text='<%# Eval("ContentsDesc") %>'></asp:HyperLink>--%>
            </li>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
    </FooterTemplate>
</asp:Repeater>
<div id="divButtomLink-overlay">
</div>
<div id="divButtomLink" class="clearfix">
    <div class="popup-top">
    </div>
    <div class="popup-mid">
        <div class="popup-mid-inner">
            <div class="error" id="errorPopup" style="display: none" runat="server">
            </div>
            <table cellspacing="10" style="margin-left: 20px">
                <tr>
                    <td colspan="2">
                        <%= GetKeyResult("PLEASECHOOSEFIRSTDATEANDNUMBEROFMEETINGDELEGATES")%>
                    </td>
                </tr>
                <tr>
                    <td align="right" width="40%">
                        <%= GetKeyResult("ARRIVALDATE")%>:
                    </td>
                    <td width="60%">
                        <asp:TextBox ID="txtBookingDate1" runat="server" value="dd/mm/yy" class="input-white-mid"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender12" runat="server" TargetControlID="txtBookingDate1"
                            PopupButtonID="calFrom1" Format="dd/MM/yy">
                        </asp:CalendarExtender>
                        <img id="calFrom1" src="<%=SiteRootPath %>Images/date-icon.png" class="datebutton" />
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <%= GetKeyResult("NUMBEROFPARTICIPANT")%> :
                    </td>
                    <td>
                        <asp:TextBox ID="txtNumberofParticipant" runat="server" value="" class="inputboxsmall"
                            MaxLength="3" Style="text-align: left;"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" TargetControlID="txtNumberofParticipant"
                            ValidChars="0123456789" runat="server">
                        </asp:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
            <div class="subscribe-btn1">
                <div class="save-cancel-btn1 button_section">
                    <asp:LinkButton ID="btnContinue" runat="server" CssClass="send-request-btn" OnClick="btnContinue_click"><%= GetKeyResult("CONTINUE")%></asp:LinkButton>&nbsp;or&nbsp;
                    <asp:LinkButton ID="lnkCnacel" runat="server" CssClass="cancelpop" OnClientClick="return ClosePopUpBottom();"><%= GetKeyResult("CANCEL")%></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="popup-bottom">
    </div>
</div>
<script language="javascript" type="text/javascript">
    function ClosePopUpBottom() {
        document.getElementsByTagName('html')[0].style.overflow = 'auto';
        document.getElementById('divButtomLink').style.display = 'none';
        document.getElementById('divButtomLink-overlay').style.display = 'none';
        return false;
    }

    function OpenPopUp(url) {
        jQuery("body").scrollTop(0);
        jQuery("html").scrollTop(0);
        jQuery("#<%= errorPopup.ClientID %>").html("");
        jQuery("#<%= errorPopup.ClientID %>").hide();
        document.getElementsByTagName('html')[0].style.overflow = 'hidden';
        jQuery("#divButtomLink").show();
        jQuery("#divButtomLink-overlay").show();
        jQuery("#<%=hdnPageUrl.ClientID %>").val(url);
        jQuery("#<%=txtNumberofParticipant.ClientID %>").val('');
        jQuery("#<%=txtBookingDate1.ClientID %>").val('');
        return false;
    }
    jQuery("#<%= btnContinue.ClientID %>").bind("click", function () {
        if (jQuery("#<%= errorPopup.ClientID %>").hasClass("succesfuly")) {
            jQuery("#<%= errorPopup.ClientID %>").removeClass("succesfuly").addClass("error").hide();
        }
        var isvalid = true;
        var errormessage = "";

        var txtBookingDate = jQuery("#<%=txtBookingDate1.ClientID %>").val();
        if (txtBookingDate.length <= 0 || txtBookingDate == "" || txtBookingDate == "dd/mm/yy") {
            if (errormessage.length > 0) {
                errormessage += "<br/>"
            }
            errormessage += '<%= GetKeyResultForJavaScript("MSGENTERDATE")%>';
            isvalid = false;
        }

        var txtNumberofParticipant = jQuery("#<%=txtNumberofParticipant.ClientID %>").val();
      
        if (txtNumberofParticipant.length <= 0 || txtNumberofParticipant == "" || parseInt(txtNumberofParticipant) <= 0) {
            if (errormessage.length > 0) {
                errormessage += "<br/>"
            }
            errormessage += '<%= GetKeyResultForJavaScript("MSGENTERPARTICIPANT")%>';
            isvalid = false;
        }

        if (!isvalid) {
            jQuery("#<%= errorPopup.ClientID %>").show();
            jQuery("#<%= errorPopup.ClientID %>").html(errormessage);
            return false;
        }
        jQuery("#<%= errorPopup.ClientID %>").html("");
        jQuery("#<%= errorPopup.ClientID %>").hide();
        jQuery("body").scrollTop(0);
        jQuery("html").scrollTop(0);
        document.getElementsByTagName('html')[0].style.overflow = 'hidden';
        jQuery("#divButtomLink").show();
        jQuery("#divButtomLink-overlay").show();
    });
    jQuery("#<%=txtBookingDate1.ClientID %>").prop("readonly", "readonly");
</script>
