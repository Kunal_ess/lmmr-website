﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HotelOfTheWeek.ascx.cs"
    Inherits="UserControl_Frontend_HotelOfTheWeek" %>
<div class="left-inner-bottom">
    <h2>
        <%= GetKeyResult("MEETINGSPECIAL")%></h2>
    <h4>
        <%= GetKeyResult("MESSAGEHOTELOFTHEWEEKHEADER")%></h4>
    <ul id="myupslider">
        <!--<marquee direction="up" behavior="continue" height="400" onmouseover="this.stop()"
            onmouseout="this.start()" scrollamount="5" id="Marquee1" runat="server">-->
        <asp:Repeater ID="gvHotelOfWeek" runat="server" OnItemDataBound="gvHotelOfWeek_ItemDataBound">
            <ItemTemplate>
                <li>
                    <div class="left">
                        <asp:Image ID="imgNewsIcon" runat="server" Width="57" Height="57" />
                    </div>
                    <div class="right">
                        <b>
                            <asp:Label ID="lblHotelName" runat="server" Text=""></asp:Label>: </b>
                        <p>
                            <asp:Label ID="lblDesc" runat="server"></asp:Label></p>
                        <p class="date">
                            <%= GetKeyResult("BOOKABLEBETWEEN")%>
                            <asp:Label ID="lbldate" runat="server"></asp:Label></p>
                    </div>
                </li>
            </ItemTemplate>
        </asp:Repeater>
        <!--</marquee>-->
    </ul>
    <script language="javascript" type="text/javascript">
        var timespan = <%= TimeForSlider %>;
        jQuery(document).ready(function () {
            callme();
            jQuery("#myupslider").hover(function () { jQuery("#myupslider li").stop(); }, function () {
                var topMove = -1 * (jQuery("#myupslider li:eq(0)").height() + 25);
                jQuery("#myupslider li").animate({ top: topMove }, "slow", 'linear', function () { jQuery("#myupslider li").css({ "top": "0px" }); jQuery("#myupslider li:first").insertAfter("#myupslider li:last"); jQuery("#myupslider li").stop(); callme(); });
            });
        });

        function callme() {
            //alert(jQuery("#myupslider li:eq(0)").html());
            var topMove =-1* (jQuery("#myupslider li:eq(0)").height() + 25);
            jQuery("#myupslider li").animate({ top: topMove }, timespan, 'linear', function () { jQuery("#myupslider li").css({ "top": "0px" }); jQuery("#myupslider li:first").insertAfter("#myupslider li:last"); jQuery("#myupslider li").stop(); callme(); });
        }
    </script>
</div>
