﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LinkMedia.ascx.cs" Inherits="UserControl_Frontend_LinkMedia" %>
<div class="footer-menu">
    <ul>
        <%--  <asp:Repeater ID="RepeaterBottomLink" runat="server" OnItemDataBound="RepeaterBottomLink_ItemDataBound">
                <ItemTemplate>--%>
        <li>
            <asp:LinkButton ID="lbtCompanyInfo" runat="server"></asp:LinkButton>
            <%--<asp:HyperLink ID="hypBottomLink" runat="server" Target="_blank"></asp:HyperLink>--%>
        </li>
        <li>
            <asp:LinkButton ID="lbtForInvesters" runat="server"></asp:LinkButton>
        </li>
        <li>
            <asp:LinkButton ID="lbtPrivacyStatement" runat="server"></asp:LinkButton>
        </li>
        <li>
            <asp:LinkButton ID="lbtLegalAspects" runat="server"></asp:LinkButton>
        </li>
        <li>
            <asp:LinkButton ID="lbtTermsNCOnditions" runat="server"></asp:LinkButton>
        </li>
        <%--    </ItemTemplate>
            </asp:Repeater>--%>
    </ul>
</div>
<script type="text/javascript" language="javascript">

    function open_win(pageurl) {

        var url = pageurl;
        var winName = 'myWindow';
        var w = '700';
        var h = '500';
        var scroll = 'no';
        var popupWindow = null;
        LeftPosition = (screen.width) ? (screen.width - w) / 2 : 0;
        TopPosition = (screen.height) ? (screen.height - h) / 2 : 0;
        settings = 'height=' + h + ',width=' + w + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=no,directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no';
        popupWindow = window.open(url, winName, settings)
        return false;

    }
</script>
<div class="footer-icon">
    <asp:Repeater ID="rptrSocialNetworking" runat="server" OnItemDataBound="rptrSocialNetworking_ItemDataBound">
        <ItemTemplate>
            <asp:HyperLink ID="hypSocialSite" runat="server" Height="30px" Width="30px" Target="_blank">
            </asp:HyperLink>
        </ItemTemplate>
    </asp:Repeater>
    <%--<asp:HyperLink ID="HynFacebook" runat="server" Text="Facebook" NavigateUrl="http://www.facebook.com/lastminutemeetingroom"
        Target="_blank" ><div  class="face">&nbsp;</div></asp:HyperLink>
    <asp:HyperLink ID="Hyplnktwitter" runat="server" Text="twitter" NavigateUrl="https://twitter.com/#%21/LMMRCOM"
        Target="_blank" ><div class="twitter">&nbsp;</div></asp:HyperLink>
    <asp:HyperLink ID="Hyplnkgoogle" runat="server" Visible="false" Enabled="false" Text="google" NavigateUrl="http://www.google.com/"
        Target="_blank" ><div  class="google">&nbsp;</div></asp:HyperLink>
    <asp:HyperLink ID="Hyplnklinkd" runat="server" Text="linked" NavigateUrl="http://www.linkedin.com/company/2526599"
        Target="_blank" ><div  class="linked">&nbsp;</div></asp:HyperLink>
    <asp:HyperLink ID="Hyplnkyoutube" runat="server" Text="you" NavigateUrl="http://www.youtube.com/watch?v=kZY9JoCJRAU"
        Target="_blank" ><div class="you">&nbsp;</div></asp:HyperLink>--%>
</div>
