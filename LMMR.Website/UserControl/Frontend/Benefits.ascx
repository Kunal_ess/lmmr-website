﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Benefits.ascx.cs" Inherits="UserControl_Frontend_Benefits" %>
<div class="bottom">
    <ul>
        <asp:Repeater ID="DataLsticons" runat="server" >
            <ItemTemplate>
                <li style="float: left;">
                 
                        <asp:Image ID="imgNewsIcon" runat="server"  ImageUrl='<%# "~/Images/" + Eval("ContentImage")%>' align="absmiddle" style=" margin-right:10px; width:27px; height:27px;" />
                            <asp:Label ID="LblDescription" runat="server" Text='<%#Eval("ContentShortDesc")%>'></asp:Label>
                   
                </li>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <li>
                 
                        <asp:Image ID="imgNewsIcon" runat="server"  ImageUrl='<%# "~/Images/" + Eval("ContentImage")%>' align="absmiddle" style=" margin-right:10px; width:27px; height:27px;" />
                            <asp:Label ID="LblDescription" runat="server" Text='<%#Eval("ContentShortDesc")%>'></asp:Label>
                   
                </li>
            </AlternatingItemTemplate>
        </asp:Repeater>
    </ul>
</div>
