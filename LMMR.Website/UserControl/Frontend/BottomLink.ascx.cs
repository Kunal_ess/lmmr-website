﻿#region Using Directives
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using System.Linq;
#endregion

public partial class UserControl_Frontend_BottomLink : BaseUserControl
{
    #region Variables and Properties
    public int intTypeCMSid;
    ManageCMSContent objManageCMSContent = new ManageCMSContent();
    //SuperAdminTaskManager manager = new SuperAdminTaskManager();
    #endregion

    #region PageLoad
    /// <summary>
    /// This method is call on the Page load and Initlize all the components of the page to its initial stage.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
       if (!Page.IsPostBack)
        {
            CalendarExtender12.StartDate = DateTime.Now.Date;
            CalendarExtender12.EndDate = DateTime.Now.Date.AddMonths(2);
            BindBottomLink();
        }
    }
    ///// <summary>
    ///// This method return the value in Getresult function
    ///// </summary>
    ///// <param name="Key"></param>
    ///// <returns></returns>
    //public string GetKeyResult(string key)
    //{
    //    return  System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    //}
    protected void btnContinue_click(object sender, EventArgs e)
    {
        Session["Date"] = txtBookingDate1.Text;
        Session["NumberOfParticipant"] = txtNumberofParticipant.Text;
        string[] urlArray = hdnPageUrl.Value.Split('/');
        int countryID = 0;
        if (urlArray.Length >= 6)
        {
            try
            {
                countryID = Convert.ToInt32(objManageCMSContent.GetCountryIdByName(urlArray[5]).Id);
            }
            catch
            {
                countryID = 0;
            }
        }
        int cityID = 0;
        if (urlArray.Length >= 7)
        {
            try
            {
                cityID = Convert.ToInt32(objManageCMSContent.GetCityIdByName(urlArray[6]).Id);
            }
            catch
            {
                cityID = 0;
            }
        }
        int zoneID = 0;
        if (urlArray.Length >= 8)
        {
            try
            {
                zoneID = Convert.ToInt32(objManageCMSContent.GetZoneIdByName(cityID,urlArray[7]).Id);
            }
            catch
            {
                zoneID = 0;
            }
        }
        //lnkBtn.PostBackUrl = "~/SearchResult.aspx?country=" + countryID + "&city=" + cityID + "";
        Session["Mycountry"] = countryID==0?null:countryID.ToString();
        Session["Mycity"] = cityID==0?null:cityID.ToString();
        Session["Myzone"] = zoneID==0?null:zoneID.ToString();
        //string QueryString = "";
        //if (countryID != 0)
        //{
        //    if(QueryString.Length >0)
        //    {
        //        QueryString += "&";
        //    }
        //    QueryString += "country=" + countryID;
        //}
        //if (cityID != 0)
        //{
        //    if (QueryString.Length > 0)
        //    {
        //        QueryString += "&";
        //    }
        //    QueryString += "city=" + cityID;
        //}
        //if (zoneID != 0)
        //{
        //    if (QueryString.Length > 0)
        //    {
        //        QueryString += "&";
        //    }
        //    QueryString += "zone=" + zoneID;
        //}
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        Session["PostBack"] = null;
        if (l != null)
        {
            Response.Redirect(hdnPageUrl.Value+"/"+l.Name.ToLower());
        }
        else
        {
            Response.Redirect(hdnPageUrl.Value+"/english");
        }
    }
    #endregion

    #region Functions
    /// <summary>
    /// This method bind the Bottom Link with Datalist
    /// </summary>
    void BindBottomLink()
    {
        Cms cmsObj = PropCMS.Find(a => a.CmsType.ToLower() == "bottompart");
        if (cmsObj != null)
        {
            datalstBottomlink.DataSource = PropCMSDESCRIPTION.Where(a => a.CmsId == cmsObj.Id && a.LanguageId == Convert.ToInt64(Session["LanguageID"]) && a.IsActive == true);
            datalstBottomlink.DataBind();
            if (datalstBottomlink.Items.Count <= 0)
            {
                datalstBottomlink.DataSource = PropCMSDESCRIPTION.Where(a => a.CmsId == cmsObj.Id && a.LanguageId == Convert.ToInt64("1") && a.IsActive == true);
                datalstBottomlink.DataBind();
            }
        }
        else
        {
            Cms cmsObj2 = manager.getCmsEntityOnType("bottompart");
            if (cmsObj != null)
            {
                datalstBottomlink.DataSource = objManageCMSContent.GetCMSContent(cmsObj2.Id, Convert.ToInt64(Session["LanguageID"]));
                datalstBottomlink.DataBind();
                if (datalstBottomlink.Items.Count <= 0)
                {
                    datalstBottomlink.DataSource = objManageCMSContent.GetCMSContent(cmsObj2.Id, Convert.ToInt64("1"));
                    datalstBottomlink.DataBind();
                }
            }
        }
        if (l.Name.ToLower() == "english" || l.Name.ToLower() == "french" || l.Name.ToLower() == "dutch")
        {
            moreinfo1.Visible = true;
        }
        else
        {
            moreinfo1.Visible = false;
        }
    }
    #endregion
    protected void datalstBottomlink_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CmsDescription currentItem = e.Item.DataItem as CmsDescription;
            if (currentItem != null)
            {
                //HyperLink h = (HyperLink)e.Item.FindControl("hypButtomLink");
                // h.NavigateUrl = SiteRootPath + currentItem.PageUrl;

                LinkButton lnkBtn = (LinkButton)e.Item.FindControl("lnkBtn");
                lnkBtn.OnClientClick = "return OpenPopUp('" + SiteRootPath + currentItem.PageUrl + "');";

            }
        }
    }
}