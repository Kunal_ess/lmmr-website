﻿#region Using Directives
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using log4net;
using log4net.Config;
using System.IO;
using System.Linq;
#endregion

public partial class UserControl_Frontend_NewsSubscriber : BaseUserControl
{
    #region Variables and Properties
    public int intTypeCMSid;
    public int intLanguageID;
    ManageCMSContent objManageCMSContent = new ManageCMSContent();
    NewsLetterSubscriber objNewsSubscriber = new NewsLetterSubscriber();
    SendMails objSendmail = new SendMails();
    EmailConfigManager em = new EmailConfigManager();
    //SuperAdminTaskManager manager = new SuperAdminTaskManager();
    #endregion

    #region PageLoad
    /// <summary>
    /// This method is call on the Page load and Initlize all the components of the page to its initial stage.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            TextBoxWatermarkExtender1.WatermarkText = GetKeyResult("ENTERYOUREMAIL");
            BindNewsGridView();
            btnsub.Text = GetKeyResult("SUBSCRIBE");
        }
    }
    #endregion

    #region Functions

    /// <summary>
    /// this method bind the all news with gridview.
    /// </summary>
    void BindNewsGridView()
    {
        Cms cmsObj = PropCMS.Find(a => a.CmsType.ToLower() == "news");
        if (cmsObj != null)
        {
            grdvwnews.DataSource = PropCMSDESCRIPTION.Where(a => a.CmsId == cmsObj.Id && a.LanguageId == Convert.ToInt64(Session["LanguageID"]) && a.IsActive == true).OrderByDescending(u => u.UpdatedDate);
            grdvwnews.DataBind();
            if (grdvwnews.Rows.Count<=0)
            {
                grdvwnews.DataSource = PropCMSDESCRIPTION.Where(a => a.CmsId == cmsObj.Id && a.LanguageId == Convert.ToInt64("1") && a.IsActive == true);
                grdvwnews.DataBind();
            }
        }
        else
        {
            intTypeCMSid = 2;
            Cms cmsObj2 = manager.getCmsEntityOnType("news");
            if (cmsObj2 != null)
            {
                grdvwnews.DataSource = objManageCMSContent.GetCMSContent(cmsObj2.Id, Convert.ToInt64(Session["LanguageID"])).OrderByDescending(u => u.UpdatedDate);
                grdvwnews.DataBind();
                if (grdvwnews.Rows.Count <= 0)
                {
                    grdvwnews.DataSource = objManageCMSContent.GetCMSContent(cmsObj2.Id, Convert.ToInt64("1"));
                    grdvwnews.DataBind();
                }
            }
        }
    }

    /// <summary>
    /// This function used for clear news subscription form.
    /// </summary>
    public void Clearform()
    {
        txtemail.Text = GetKeyResult("ENTERYOUREMAIL");
        Lblemailid.Text = "";
        txtFirstnam.Text = "";
        txtlastnam.Text = "";
    }
    #endregion

    #region events
    /// <summary>
    /// In this evevt Mail is sent to the user who want to subscribe news .
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSubscribe_Click(object sender, EventArgs e)
    {
        if (!objManageCMSContent.IsMailExist(hdnemail.Value))
        {
            Page.RegisterStartupScript("msg", "<script language='javascript'>alert('Your email id already exists.');</script>");
            Clearform();
        }
        else
        {
            objNewsSubscriber.SubscriberEmailid = hdnemail.Value;
            objNewsSubscriber.FirstName = titleDDL.SelectedItem.Text + txtFirstnam.Text;
            objNewsSubscriber.LastName = txtlastnam.Text;
            string message = string.Empty;
            if (objManageCMSContent.AddSubscription(objNewsSubscriber))
            {
                message = GetKeyResult("THANKYOUFORSUBSCRIPTION");// "Thank you for your subscription. Stay tuned on our monthly Meeting Advise or Newsletter";
                EmailConfig eConfig = em.GetByName("New subscriber welcome");
                if (eConfig.IsActive)
                {
                    objSendmail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
                    objSendmail.ToEmail = hdnemail.Value;
                    objSendmail.Bcc = ConfigurationManager.AppSettings["WatchEMailID"];
                    string bodymsg = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
                    EmailConfigMapping emap2 = new EmailConfigMapping();
                    emap2 = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                    bodymsg = bodymsg.Replace("@CONTENTS", emap2.EmailContents);
                    // string bodymsg = File.ReadAllText(Server.MapPath("~/EmailTemplets/NEWSubscribe.html"));
                    EmailValueCollection objEmailValues = new EmailValueCollection();

                    objSendmail.Subject = "Request for news subscription";
                    foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailFornewssubscribe(txtFirstnam.Text, txtlastnam.Text))
                    {
                        bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                    }
                    objSendmail.Body = bodymsg;
                    objSendmail.SendMail();
                }
                Response.Redirect("~/SubscribeNewsOK.aspx");
                Clearform();

                //Page.RegisterStartupScript("aa", "<script language='javascript'>alert('" + message + "');</script>");
            }
            else
            {
                message = "Your email id already exists!";
                Page.RegisterStartupScript("aa", "<script language='javascript'>alert('" + message + "');</script>");

            }
        }
        txtemail.Text = "";
        errorPopupnews.Style.Add("display", "none");
        errorPopupnews.Attributes.Add("class", "error");
    }

    /// <summary>
    /// This event used for gridview row bound.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdvwnews_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CmsDescription obj = e.Row.DataItem as CmsDescription;
            if (obj != null && obj.IsActive == true)
            {
                LinkButton HynNews = (LinkButton)e.Row.FindControl("HynNews");
                HynNews.Text = obj.ContentsDesc.ToString();
                //HynNews.PostBackUrl = ConfigurationManager.AppSettings["NewsFile"] + "NewsFile/" + obj.PageUrl.ToString();
               // string filepath = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Replace("~/", "") + "NewsFile/" + obj.PageUrl.ToString();
                string filepath = "";
                if (string.IsNullOrEmpty(obj.PageUrl))
                {
                    filepath = "";
                }
                else
                {
                    filepath = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Replace("~/", "") + "NewsFile/" + obj.PageUrl.ToString();
                }
                HynNews.OnClientClick = "return open_win('" + filepath + "')";
                Label lbldate = (Label)e.Row.FindControl("lbldate");
                if (lbldate != null)
                {
                    string date = String.Format("{0:MMMM-dd-yyyy}", obj.UpdatedDate);

                    if (!string.IsNullOrEmpty(date))
                    {
                        lbldate.Text = date;
                    }
                }

            }

        }
    }
    #endregion


}