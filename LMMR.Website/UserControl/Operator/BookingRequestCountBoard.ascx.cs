﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Entities;
using LMMR.Business;

public partial class UserControl_Operator_BookingRequestCountBoard : System.Web.UI.UserControl
{
    ViewBooking_Hotel objViewBooikng = new ViewBooking_Hotel();

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            lblCurrentYearBooking.Text = GetByYear().ToString();
            lblCurrentMonthBooking.Text = GetByMonth().ToString();
            lblTodayBooking.Text = GetByToday().ToString();
            lblTodayRequest.Text = TotalRequest().ToString();
            lblTotalTentativeRequest.Text = GetTotalTentativeRequest().ToString();
            lblToNoactionRequest.Text = GetTotalNoActionRequest().ToString();
        }

    }

    public int GetByYear()
    {
        DateTime startDate = GetFirstDayOfTheYear();
        DateTime endDate = DateTime.Today;
        string fromDate = string.Format("{0:yyyy-MM-dd}", startDate);
        string toDate = string.Format("{0:yyyy-MM-dd}", endDate);
        string whereclaus = string.Empty;
        whereclaus = "booktype = 0 and BookingDate BETWEEN '" + fromDate + "' And '" + toDate + "'";
        string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
        VList<Viewbookingrequest> objViewBooking = new VList<Viewbookingrequest>();
        objViewBooking = objViewBooikng.Bindgrid(whereclaus, orderby).FindAllDistinct(ViewbookingrequestColumn.HotelId);
        return objViewBooking.Count; ;
    }

    static DateTime GetFirstDayOfTheYear()
    {
        return FirstDayOfYear(DateTime.Today);
    }
    static DateTime FirstDayOfYear(DateTime y)
    {
        return new DateTime(y.Year, 1, 1);
    }

    public int GetByMonth()
    {
        DateTime baseDate = DateTime.Today;
        DateTime startDate = baseDate.AddDays(1 - baseDate.Day);
        DateTime endDate = DateTime.Today;
        string fromDate = string.Format("{0:yyyy-MM-dd}", startDate);
        string toDate = string.Format("{0:yyyy-MM-dd}", endDate);
        string whereclaus = string.Empty;
        whereclaus = "booktype = 0 and BookingDate BETWEEN '" + fromDate + "' And '" + toDate + "'";
        string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
        VList<Viewbookingrequest> objViewBooking = new VList<Viewbookingrequest>();
        objViewBooking = objViewBooikng.Bindgrid(whereclaus, orderby).FindAllDistinct(ViewbookingrequestColumn.HotelId);
        return objViewBooking.Count; ;
    }

    public int GetByToday()
    {
        DateTime endDate = DateTime.Today;
        string today = string.Format("{0:yyyy-MM-dd}", endDate);
        string whereclaus = string.Empty;
        whereclaus = "booktype = 0 and BookingDate = '" + today + "'";
        string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
        VList<Viewbookingrequest> objViewBooking = new VList<Viewbookingrequest>();
        objViewBooking = objViewBooikng.Bindgrid(whereclaus, orderby).FindAllDistinct(ViewbookingrequestColumn.HotelId);
        return objViewBooking.Count;
    }

    public int TotalRequest()
    {
        string whereclaus = string.Empty;
        whereclaus = "booktype = 1";
        string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
        VList<Viewbookingrequest> objViewBooking = new VList<Viewbookingrequest>();
        objViewBooking = objViewBooikng.Bindgrid(whereclaus, orderby).FindAllDistinct(ViewbookingrequestColumn.HotelId);
        return objViewBooking.Count;
    }

    public int GetTotalTentativeRequest()
    {
        string whereclaus = string.Empty;
        whereclaus = "booktype = 1 and RequestStatus = 5";
        string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
        VList<Viewbookingrequest> objViewBooking = new VList<Viewbookingrequest>();
        objViewBooking = objViewBooikng.Bindgrid(whereclaus, orderby).FindAllDistinct(ViewbookingrequestColumn.HotelId);
        return objViewBooking.Count;
    }

    public int GetTotalNoActionRequest()
    {
        string whereclaus = string.Empty;
        whereclaus = "booktype = 1 and RequestStatus = 1 or RequestStatus =8";
        string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
        VList<Viewbookingrequest> objViewBooking = new VList<Viewbookingrequest>();
        objViewBooking = objViewBooikng.Bindgrid(whereclaus, orderby).FindAllDistinct(ViewbookingrequestColumn.HotelId);
        return objViewBooking.Count;
    }
}