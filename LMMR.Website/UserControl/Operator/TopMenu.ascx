﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopMenu.ascx.cs" Inherits="UserControl_Operator_TopMenu" %>
<!--Main navigation start -->
                <div class="main_nav">
                    <div id="subnavbar">
                        <ul id="subnav">
                            <li class="cat-item cat-item-1"><asp:LinkButton ID="hypControlPanel" runat="server" PostBackUrl="~/Operator/ControlPanel.aspx">Control Panel</asp:LinkButton></li>
                            <li class="cat-item cat-item-366"><asp:LinkButton ID="hypClientContract" runat="server" PostBackUrl="~/Operator/ClientContract.aspx">Clients-Contracts</asp:LinkButton></li>
                            <li class="cat-item cat-item-311"><asp:LinkButton ID="hypSearch" PostBackUrl="~/Operator/SearchDetails.aspx" runat="server" >Search bookings</asp:LinkButton></li>
                            <li class="cat-item cat-item-313"><asp:LinkButton ID="hypseachreq" PostBackUrl="~/Operator/RequestSearchDetails.aspx"  runat="server" >Search request</asp:LinkButton></li>
                            <li class="cat-item cat-item-313"><asp:LinkButton ID="HyperLink3" runat="server" PostBackUrl="~/Operator/AvailabilityandSpecialMeetingroom.aspx">Availability & Special monitoring</asp:LinkButton></li>
                        </ul>
                    </div>
                </div>
                <!--main navigation end -->
                <!--Main navigation start -->
                <div class="main_nav1">
                    <div id="subnavbar1">
                        <ul id="subnav1">
                            <li class="cat-item"><asp:LinkButton ID="HyperLink4" runat="server" PostBackUrl="~/Operator/CheckCommission.aspx" >Commissions</asp:LinkButton></li>
                            <li class="cat-item"><asp:LinkButton ID="HyperLink5" runat="server" PostBackUrl="~/Operator/newHotelInvoice.aspx">Invoices</asp:LinkButton></li>
                            <li class="cat-item"><asp:LinkButton ID="HyperLink6" runat="server" PostBackUrl="~/Operator/CancelTransfer.aspx">Cancel & Transfer</asp:LinkButton></li>
                            <li class="cat-item"><asp:LinkButton ID="HyperLink7" runat="server" PostBackUrl="~/Operator/HotelFacilityAccess.aspx">Hotel/Facility Access</asp:LinkButton>
                            </li>
                            <li class="cat-item"><asp:LinkButton ID="HyperLink8" runat="server" PostBackUrl="~/Operator/Usermenu.aspx">Users</asp:LinkButton> </li>
                          <%--  <li class="cat-item"><asp:LinkButton ID="HyperLink9" runat="server" PostBackUrl="~/Operator/AddNewCity.aspx">Add City</asp:LinkButton> </li>
                            <li class="cat-item"><asp:LinkButton ID="HyperLink10" runat="server" PostBackUrl="~/Operator/AddZone.aspx">Add Zone</asp:LinkButton> </li>--%>
                            <li class="cat-item"><asp:LinkButton ID="HyperLink11" runat="server" PostBackUrl="~/Operator/NewAdminLink.aspx">Add Admin</asp:LinkButton> </li>
                        </ul>
                    </div>
                </div>
                <!--main navigation end -->