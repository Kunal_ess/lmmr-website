﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchPanel.ascx.cs" Inherits="UserControl_Operator_SearchPanel" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div class="search-operator-layout1" style="margin-bottom: 20px;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="630px">
                <%--  <asp:UpdatePanel ID="upsearch" runat="server">
               <ContentTemplate>--%>
                <div class="search-operator-layout-left1">
                    <div class="search-operator-from1">
                        <div class="commisions-top-new1 ">
                            <table width="100%" border="0" cellspacing="0" cellpadding="8">
                                <tr id="trCancel" runat="server" visible="false">
                                    <td style="border-bottom: #92bede solid 1px" runat="server" id="tdRadio">
                                        <asp:RadioButtonList ID="rdbBooking" runat="server" Style="width: 215px" RepeatDirection="Horizontal"
                                            AutoPostBack="True" OnSelectedIndexChanged="rdbBooking_SelectedIndexChanged">
                                            <asp:ListItem Value="0" Selected="True">Booking</asp:ListItem>
                                            <asp:ListItem Value="1">Request</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px" runat="server"
                                        id="tdBookingType">
                                        <div class="search-form1-left-new">
                                            <asp:Label ID="lblBookingType" runat="server" Text="Booking Number"></asp:Label>
                                            :
                                        </div>
                                        <asp:TextBox ID="txtNumber" runat="server" class="inputbox" MaxLength="10"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtNumber"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr id="trSearch" runat="server" visible="false">
                                    <td style="border-bottom: #92bede solid 1px" runat="server" id="tdcountry">
                                        <asp:DropDownList ID="drpSearchCountry" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpSearchCountry_SelectedIndexChanged"
                                            Style="width: 215px">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px" runat="server"
                                        id="tdDate">
                                        <table  border="0" cellspacing="0" cellpadding="0" style="max-width:400px; width:380px;">
                                            <tr>
                                                <td>
                                                    <b>
                                                        <asp:Label ID="lbldate" runat="server" Text="Date:"></asp:Label></b>
                                                </td>
                                                <td style="padding-left: 20px; text-align: right; padding-right: 3px;">
                                                    <b>From</b>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFromdate" runat="server" CssClass="inputbox1"></asp:TextBox><asp:HiddenField
                                                        ID="hdnfromdate" runat="server" />
                                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFromdate"
                                                        PopupButtonID="calFrom" Format="dd/MM/yy" OnClientDateSelectionChanged="dateChangeFrom">
                                                    </asp:CalendarExtender>
                                                    <asp:Image ID="calFrom" runat="server" ImageUrl="~/Images/date-icon.png" align="absmiddle" />
                                                </td>
                                                <td>
                                                    <b>To</b>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTodate" runat="server" CssClass="inputbox1"></asp:TextBox>
                                                    <asp:HiddenField ID="hdntodate" runat="server" />
                                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTodate"
                                                        PopupButtonID="calTo" Format="dd/MM/yy" OnClientDateSelectionChanged="dateChangeTo">
                                                    </asp:CalendarExtender>
                                                    <asp:Image ID="calTo" runat="server" ImageUrl="~/Images/date-icon.png" align="absmiddle" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trVenue" runat="server" visible="false">
                                    <td style="border-bottom: #92bede solid 1px" runat="server" id="tdCity">
                                        <asp:DropDownList ID="drpSearchCity" runat="server" CssClass="NoClassApply" AutoPostBack="true"
                                            OnSelectedIndexChanged="drpSearchCity_SelectedIndexChanged" Enabled="false" Style="width: 215px">
                                            <asp:ListItem Text="--Select city--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px" id="tdVenue"
                                        runat="server">
                                        <div class="search-form1-left-new">
                                            Venue(s) :
                                        </div>
                                        <asp:TextBox ID="txtVenue" runat="server" class="inputbox" MaxLength="100"></asp:TextBox>
                                        <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtVenue"
                                            MinimumPrefixLength="3" EnableCaching="true" CompletionSetCount="1" CompletionInterval="1000"
                                            ServiceMethod="GetHotel" CompletionListCssClass="AutoExtender">
                                        </asp:AutoCompleteExtender>
                                    </td>
                                </tr>
                                <tr id="trMultiplecity" runat="server" visible="false">
                                    <td id="tdMultiplecity" runat="server" style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <div class="search-form1-left-new">
                                            <asp:ListBox ID="lstcity" runat="server" SelectionMode="Multiple" AutoPostBack="false"
                                                CssClass="NoClassApply" Enabled="false" Style="width: 215px">
                                                <asp:ListItem Text="---- Select city ----" Value=""></asp:ListItem>
                                            </asp:ListBox>
                                        </div>
                                    </td>
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <div class="search-form1-left-new">
                                            <table width="100%">
                                                <tr id="trclientname" runat="server" visible="false">
                                                    
                                                    <td valign="top">
                                                        <div class="search-form1-left-new">
                                                           <asp:RadioButtonList ID="rbtcientvenue" runat="server" RepeatDirection="Horizontal">
                                                           <asp:ListItem Text="Venue" Value="2" Selected="True" /> 
                                                           <asp:ListItem Text="Client" Value="1"  />
                                                           </asp:RadioButtonList>
                                                           
                                                            <%--<asp:TextBox ID="txtclientname" runat="server" class="inputbox"></asp:TextBox>--%>
                                                        </div>
                                                    </td>
                                                     <td valign="middle" colspan="2">
                                                        <div class="search-form1-left-new">
                                                            <asp:TextBox ID="txtclientname" runat="server" class="inputbox" Width="100%" MaxLength="100"></asp:TextBox>
                                                        </div>
                                                    </td>
                                                    
                                                </tr>
                                                
                                            </table>
                                        </div>
                                    </td>
                                    
                                </tr>
                                <tr id="trHotelname" runat="server" visible="false">
                                    <td style="border-bottom: #92bede solid 1px">
                                        <asp:DropDownList ID="drpHotelName" runat="server" CssClass="NoClassApply" AutoPostBack="true"
                                            OnSelectedIndexChanged="drpHotelName_SelectedIndexChanged" Style="width: 215px">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="border-bottom: #92bede solid 1px">
                                        <asp:DropDownList ID="drpMeetingRoom" runat="server" CssClass="NoClassApply" Enabled="false"
                                            Style="width: 215px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="trUsername" runat="server" visible="false">
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <div class="search-form1-left-new">
                                            User's name:
                                        </div>
                                    </td>
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <div class="search-form1-left-new">
                                            <asp:TextBox ID="TxtUsername" runat="server" class="inputbox"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr id="trcompany" runat="server" visible="false">
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <div class="search-form1-left-new">
                                            Company:
                                        </div>
                                    </td>
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <div class="search-form1-left-new">
                                            <asp:TextBox ID="TxtCompany" runat="server" class="inputbox"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr id="trtype" runat="server" visible="false">
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <div class="search-form1-left-new">
                                            Type :
                                        </div>
                                    </td>
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <div class="search-form1-left-new">
                                            <asp:DropDownList ID="drpUserType" runat="server" Style="width: 215px" CssClass="NoClassApply">
                                                <asp:ListItem Text="All" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Meet 2 Solution" Value="7"></asp:ListItem>
                                                <asp:ListItem Text="Company" Value="9"></asp:ListItem>
                                                <asp:ListItem Text="Private" Value="10"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                </tr>
                                <tr id="trstfuser" runat="server" visible="false">
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <div class="search-form1-left-new">
                                            Name:
                                        </div>
                                    </td>
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <div class="search-form1-left-new">
                                            <asp:TextBox ID="txtstfuser" runat="server" class="inputbox"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                                <%--<tr>
                                    <td style="border-bottom: #92bede solid 1px">
                                        <select name="select4" class="dropdown" style="width: 215px">
                                            <option value="opt1">Select country</option>
                                            <option value="opt2">Option 2</option>
                                            <option value="opt3">Option 3</option>
                                        </select>
                                    </td>
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <div class="search-form1-left-new">
                                            Invoice number:
                                        </div>
                                        <input type="text" class="inputregister" value="Enter nr here...">
                                    </td>
                                </tr>--%>
                            </table>
                            <%--<table width="100%" border="0" cellspacing="0" cellpadding="8">
                                <tr>
                                    <td style="border-bottom: #92bede solid 1px">
                                        <b>Select country</b>
                                        <select name="select4" class="dropdown" style="width: 150px">
                                            <option value="opt1">Select country</option>
                                            <option value="opt2">Option 2</option>
                                            <option value="opt3">Option 3</option>
                                        </select>
                                    </td>
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <select name="select4" class="dropdown" style="width: 60px">
                                            <option value="opt1">Select country</option>
                                            <option value="opt2">Option 2</option>
                                            <option value="opt3">Option 3</option>
                                        </select>
                                    </td>
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <input type="text" class="inputregister" value="Enter nr here...">
                                    </td>
                                </tr>
                            </table>--%>
                        </div>
                    </div>
                </div>
                <%-- </ContentTemplate>
               </asp:UpdatePanel>--%>
            </td>
            <td align="left" valign="bottom">
                <div style="float: left; margin-left: 20px;" class="n-commisions">
                    <div class="n-btn">
                        <asp:LinkButton ID="lbtSearch" runat="server" OnClick="lbtSearch_Click" OnClientClick="return DataValidation()"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Search</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                    </div>
                </div>
                <div style="float: left; margin-left: 10px;" class="n-commisions">
                    <div class="n-btn">
                        <asp:LinkButton ID="lnkClear" runat="server" OnClick="lnkClear_Click"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Clear</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript" language="javascript">
    //function valid(){
    //    if (document.getElementById(TxtUsername).value != "" && document.getElementById(TxtCompany).value != "") {
    //        alert("Select Only One");
    //        return false;

    //    }
    //}
    function dateChangeFrom(s, e) {
        var fromdatechange = jQuery("#<%= txtFromdate.ClientID %>").val();
        jQuery("#<%= hdnfromdate.ClientID %>").val(fromdatechange);
    }
    function dateChangeTo(s, e) {
        var todatechange = jQuery("#<%= txtTodate.ClientID %>").val();
        jQuery("#<%= hdntodate.ClientID %>").val(todatechange);
    }

    function DataValidation() {
        var fromdatechange = jQuery("#<%= txtFromdate.ClientID %>").val();
        var todatechange = jQuery("#<%= txtTodate.ClientID %>").val();
        //        var Username = jQuery("#<%= TxtUsername.ClientID %>").val();
        //        var Company = jQuery("#<%= TxtCompany.ClientID %>").val();
        //        if (Username !="" && Company !="") {
        //            alert("Please Search By Only One");
        //            return false;
        //        } 
        var todayArr = todatechange.split('/');
        var formdayArr = fromdatechange.split('/');
        var fromdatecheck = new Date();
        var todatecheck = new Date();

        fromdatecheck.setFullYear(parseInt("20" + formdayArr[2], 10), (parseInt(formdayArr[1], 10) - 1), formdayArr[0]);
        todatecheck.setFullYear(parseInt("20" + todayArr[2], 10), (parseInt(todayArr[1], 10) - 1), todayArr[0]);
        if (fromdatecheck > todatecheck) {
            alert("From date must be less than To date.");
            return false;
        }

    }

    
</script>
<script language="javascript" type="text/javascript">
    jQuery(document).ready(function () {
        jQuery("#<%= txtFromdate.ClientID %>").attr("disabled", true);
        jQuery("#<%= txtTodate.ClientID %>").attr("disabled", true);
        jQuery("#<%= lbtSearch.ClientID %>").bind("click", function () {
            jQuery("#<%= txtFromdate.ClientID %>").attr("disabled", false);
            jQuery("#<%= txtTodate.ClientID %>").attr("disabled", false);
        });
    });
     
</script>
