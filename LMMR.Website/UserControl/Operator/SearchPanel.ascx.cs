﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;

public partial class UserControl_Operator_SearchPanel : System.Web.UI.UserControl
{
    #region Variable
    ClientContract objClient = new ClientContract();
    ViewBooking_Hotel objViewBooking = new ViewBooking_Hotel();
    public event EventHandler SearchButtonClick;
    public event EventHandler CancelButtonClick;
    public event EventHandler rdbBookingClick;
    
    #endregion

    #region Properties
    public long propCountryID
    {
        get
        {

            if (drpSearchCountry.SelectedValue == "")
            {
                return 0;
            }
            else
            {
                return Convert.ToInt64(drpSearchCountry.SelectedValue);
            }
        }

    }
    public long Usertype
    {
        get
        {

            if (drpUserType.SelectedValue == "")
            {
                return 0;
            }
            else
            {
                return Convert.ToInt64(drpUserType.SelectedValue);
            }
        }

    }
    public string Username
    {
        get
        {
            return Convert.ToString(TxtUsername.Text);
        }
    }
    
    public string stfname
    {
        get
        {
            return Convert.ToString(txtstfuser.Text);
        }
    }
    public string Company
    {
        get
        {
            return Convert.ToString(TxtCompany.Text);
        }
    }
    public DateTime MinFromDate
    {
        get;
        set;
    }
    public DateTime MinToDate
    {
        get;
        set;
    }
    public DateTime MaxFromDate
    {
        get;
        set;
    }
    public DateTime MaxToDate
    {
        get;
        set;
    }
    public long propCityID
    {
        get
        {
            if (drpSearchCity.SelectedValue == "")
            {
                return 0;
            }
            else
            {
                return Convert.ToInt64(drpSearchCity.SelectedValue);
            }
        }
    }
    public string propMultiCityID
    {
        get
        {
            if (lstcity.SelectedValue == "")
            {
                return "0";
            }
            else
            {
                string ids = "1";
                foreach (ListItem li in lstcity.Items)
                {
                    if (li.Selected)
                    {
                        ids += ", " + li.Value.ToString();
                    }
                }

                return ids;

            }
        }
    }
    public DateTime propFromDate
    {
        get
        {
            if (txtFromdate.Text != "" && txtFromdate.Text != "dd/mm/yy")
            {

                //DateTime startdate = new DateTime(Convert.ToInt32("20" + hdnfromdate.Value.Split('/')[2]), Convert.ToInt32(hdnfromdate.Value.Split('/')[1]), Convert.ToInt32(hdnfromdate.Value.Split('/')[0]));
                DateTime startdate = new DateTime(Convert.ToInt32("20" + txtFromdate.Text.Split('/')[2]), Convert.ToInt32(txtFromdate.Text.Split('/')[1]), Convert.ToInt32(txtFromdate.Text.Split('/')[0]));
                return startdate;
            }
            else
            {
                if (!Request.Url.AbsolutePath.ToLower().Contains("availabilityandspecialmeetingroom.aspx"))
                {
                    DateTime baseDate = DateTime.Today;
                    return baseDate.AddDays(1 - baseDate.Day);
                }
                else
                {
                    return DateTime.Now;
                }
            }
        }
    }
    public DateTime propToDate
    {
        get
        {
            if (txtTodate.Text != "" && txtTodate.Text != "dd/mm/yy")
            {
                //DateTime enddate = new DateTime(Convert.ToInt32("20" + hdntodate.Value.Split('/')[2]), Convert.ToInt32(hdntodate.Value.Split('/')[1]), Convert.ToInt32(hdntodate.Value.Split('/')[0]));
                DateTime enddate = new DateTime(Convert.ToInt32("20" + txtTodate.Text.Split('/')[2]), Convert.ToInt32(txtTodate.Text.Split('/')[1]), Convert.ToInt32(txtTodate.Text.Split('/')[0]));
                return enddate;
            }
            else
            {
                if (!Request.Url.AbsolutePath.ToLower().Contains("availabilityandspecialmeetingroom.aspx"))
                {
                    return DateTime.Today.Date;
                }
                else
                {
                    return DateTime.Now.AddDays(60);
                }
            }
        }
    }
    public string propDateFrom
    {
        get
        {
            if (txtFromdate.Text != "" && txtFromdate.Text != "dd/mm/yy")
            {
                DateTime startdate = new DateTime(Convert.ToInt32("20" + txtFromdate.Text.Split('/')[2]), Convert.ToInt32(txtFromdate.Text.Split('/')[1]), Convert.ToInt32(txtFromdate.Text.Split('/')[0]));
                return "Yes";
            }
            else
            {
                return "No";
            }
        }
    }
    public string propDateTo
    {
        get
        {
            if (txtTodate.Text != "" && txtTodate.Text != "dd/mm/yy")
            {

                DateTime enddate = new DateTime(Convert.ToInt32("20" + txtTodate.Text.Split('/')[2]), Convert.ToInt32(txtTodate.Text.Split('/')[1]), Convert.ToInt32(txtTodate.Text.Split('/')[0]));
                return "Yes";
            }
            else
            {
                return "No";
            }
        }
    }
    public string Venus
    {
        get
        {
            return Convert.ToString(txtVenue.Text);
        }
    }
    public long propHotelId
    {
        get
        {

            if (drpHotelName.SelectedValue == "")
            {
                return 0;
            }
            else
            {
                return Convert.ToInt64(drpHotelName.SelectedValue);
            }
        }

    }
    public long propMeetingRoomId
    {
        get
        {

            if (drpMeetingRoom.SelectedValue == "")
            {
                return 0;
            }
            else
            {
                return Convert.ToInt64(drpMeetingRoom.SelectedValue);
            }
        }

    }
    public string propBookRefNumber
    {
        get
        {
            return Convert.ToString(txtNumber.Text);
        }
    }
    public int propBookRefType
    {
        get
        {

            if (rdbBooking.SelectedValue == "")
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(rdbBooking.SelectedValue);
            }
        }

        set
        {
            propBookRefType = value;

        }

    }

    public int proprbtvenueclient
    {
        get
        {

            if (rbtcientvenue.SelectedValue == "")
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(rbtcientvenue.SelectedValue);
            }
        }

        set
        {
            propBookRefType = value;

        }

    }
    public int propBookRef
    {
        get;
        set;
    }
    public string propclientname
    {
        get
        {
            return Convert.ToString(txtclientname.Text);
        }

    }
    public bool Isvalidation { get; set; }
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Url.AbsolutePath.ToLower().Contains("controlpanel.aspx"))
            {
                trVenue.Visible = true;
                trSearch.Visible = true;
                tdcountry.ColSpan = 2;
                tdDate.Visible = false;

                // Hide show your div according your page.
            }
            else if (Request.Url.AbsolutePath.Contains("usermenu.aspx") || Request.Url.AbsolutePath.Contains("Usermenu.aspx"))
            {
                trcompany.Visible = true;
                trUsername.Visible = true;
                trtype.Visible = true;

            }
            else if (Request.Url.AbsolutePath.ToLower().Contains("superadmin/staffadmin.aspx"))
            {

                trstfuser.Visible = true;

            }

            else if (Request.Url.AbsolutePath.ToLower().Contains("availabilityandspecialmeetingroom.aspx"))
            {
                trSearch.Visible = true;
                trVenue.Visible = true;
                if (MaxFromDate != null)
                {
                    CalendarExtender1.EndDate = MaxFromDate;
                }
                if (MinFromDate != null)
                {
                    CalendarExtender1.StartDate = MinFromDate;
                }
                if (MaxToDate != null)
                {
                    CalendarExtender2.EndDate = MaxToDate;
                }
                if (MinToDate != null)
                {
                    CalendarExtender2.StartDate = MinToDate;
                }
            }
            else if (Request.Url.AbsolutePath.ToLower().Contains("canceltransfer.aspx"))
            {
                trCancel.Visible = true;
                trVenue.Visible = true;
                tdVenue.Visible = false;
                trHotelname.Visible = true;
                rdbBooking.SelectedValue = "0";
                trSearch.Visible = true;
                tdcountry.ColSpan = 2;
                tdCity.ColSpan = 2;
                tdDate.Visible = false;
                FillHotelMeetingRoom();
            }
            else if (Request.RawUrl.ToLower().Contains("searchdetails.aspx"))
            {
                trVenue.Visible = false;
                tdVenue.Visible = false;
                trHotelname.Visible = false;
                trSearch.Visible = true;
                trclientname.Visible = true;
                trMultiplecity.Visible = true;
                tdDate.Visible = true;

                lbldate.Text = "Arrival Date:";
                FillHotelMeetingRoom();
                tdDate.Visible = true;
                txtFromdate.Text = Convert.ToString(propFromDate);
                txtTodate.Text = Convert.ToString(propToDate);
                FillCountryForSearch();


            }
            else if (Request.RawUrl.ToLower().Contains("searchdetails.aspx"))
            {
                trVenue.Visible = false;
                tdVenue.Visible = false;
                trHotelname.Visible = false;
                trSearch.Visible = true;
                trclientname.Visible = true;
                trMultiplecity.Visible = true;
                tdDate.Visible = true;
                lbldate.Text = "Arrival Date:";
                FillHotelMeetingRoom();
                tdDate.Visible = true;
                txtFromdate.Text = Convert.ToString(propFromDate);
                txtTodate.Text = Convert.ToString(propToDate);
                FillCountryForSearch();

            }
            else if (Request.RawUrl.ToLower().Contains("searchbooking.aspx?type=0"))
            {
                trVenue.Visible = false;
                tdVenue.Visible = false;
                trHotelname.Visible = false;
                trSearch.Visible = true;
                trclientname.Visible = true;
                trMultiplecity.Visible = true;
                tdDate.Visible = true;
                tdcountry.Visible = false;
                tdMultiplecity.Visible = false;
                lbldate.Text = "Arrival Date:";
                FillHotelMeetingRoom();
                tdDate.Visible = true;
                txtFromdate.Text = Convert.ToString(propFromDate);
                txtTodate.Text = Convert.ToString(propToDate);
                FillCountryForSearch();


            }
            else if (Request.RawUrl.ToLower().Contains("searchbooking.aspx?type=1"))
            {
                trVenue.Visible = false;
                tdVenue.Visible = false;
                trHotelname.Visible = false;
                trSearch.Visible = true;
                trclientname.Visible = true;
                trMultiplecity.Visible = true;
                tdDate.Visible = true;
                tdcountry.Visible = false;
                tdMultiplecity.Visible = false;
                lbldate.Text = "Arrival Date:";
                FillHotelMeetingRoom();
                tdDate.Visible = true;
                txtFromdate.Text = Convert.ToString(propFromDate);
                txtTodate.Text = Convert.ToString(propToDate);
                FillCountryForSearch();

            }
            else if (Request.Url.AbsolutePath.ToLower().Contains("superadmin/prioritybasket.aspx"))
            {
                trVenue.Visible = true;
                tdVenue.Visible = false;
                trHotelname.Visible = false;
                trSearch.Visible = true;
                trCancel.Visible = true;
                tdBookingType.Visible = false;
                tdRadio.ColSpan = 2;
                tdDate.Visible = true;
                tdDate.Visible = true;
                txtFromdate.Text = Convert.ToString(propDateFrom);
                txtTodate.Text = Convert.ToString(propDateTo);

            }
            else if (Request.Url.AbsolutePath.ToLower().Contains("checkcommission.aspx"))
            {
                trCancel.Visible = false;
                trVenue.Visible = true;
                tdVenue.Visible = false;
                trHotelname.Visible = false;
                trSearch.Visible = true;
                tdcountry.ColSpan = 2;
                tdCity.ColSpan = 2;
                tdDate.Visible = false;
                FillHotelMeetingRoom();
            }

            FillCountryForSearch();
            txtFromdate.Text = "dd/mm/yy";
            txtTodate.Text = "dd/mm/yy";
        }
        else
        {
            if (propDateFrom == "Yes")
            {

            }
            else
            {
                txtFromdate.Text = "dd/mm/yy";
                txtTodate.Text = "dd/mm/yy";
            }
        }
    }
    #endregion
    /// <summary>
    /// Selected Index Change of Dropdown Country list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 
    protected void drpSearchCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillCityFroSearch(Convert.ToInt32(drpSearchCountry.SelectedValue));
       // this.Page.FindControl("divgraph").Visible = false;
    }
    /// <summary>
    /// Reset all the filter values
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkClear_Click(object sender, EventArgs e)
    {
        Session["command"] = null;
        txtstfuser.Text = "";
        txtTodate.Text = "dd/mm/yy";
        txtFromdate.Text = "dd/mm/yy";
        hdnfromdate.Value = "dd/mm/yy";
        hdntodate.Value = "dd/mm/yy";
        drpSearchCountry.SelectedValue = "0";
        //rdbBooking.SelectedValue = "0";
        drpHotelName.SelectedValue = "0";
        drpMeetingRoom.SelectedValue = "0";
        drpMeetingRoom.Enabled = false;
        FillCityFroSearch(0);
        txtVenue.Text = "";
        TxtCompany.Text = "";
        txtclientname.Text = "";
        TxtUsername.Text = "";
        drpUserType.SelectedValue = "1";
        txtNumber.Text = "";
        CancelButtonClick.Invoke(sender, e);
    }
    /// <summary>
    /// This event fire for event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtSearch_Click(object sender, EventArgs e)
    {
        Session["command"] = null;
        string from = txtFromdate.Text;
        string to = txtTodate.Text;
        SearchButtonClick.Invoke(sender, e);
    }
    /// <summary>
    /// Bind all the country Table in dropdown.
    /// </summary>
    protected void FillCountryForSearch()
    {
        //get list of countries.
        TList<Country> objCountry = objClient.GetByAllCountry();
        if (objCountry.Count > 0)
        {
            drpSearchCountry.DataValueField = "Id";
            drpSearchCountry.DataTextField = "CountryName";
            drpSearchCountry.DataSource = objCountry.OrderBy(a => a.CountryName);
            drpSearchCountry.DataBind();
            drpSearchCountry.Items.Insert(0, new ListItem("--Select country--", "0"));
            lstcity.Enabled = false;
        }
        else
        {
            drpSearchCountry.Items.Insert(0, new ListItem("--Select country--", "0"));
            lstcity.Enabled = false;
        }

    }
    /// <summary>
    /// This function to used for city.
    /// </summary>
    /// <param name="intcountryID"></param>
    protected void FillCityFroSearch(int intcountryID)
    {
        if (intcountryID > 0)
        {
            drpSearchCity.DataValueField = "Id";
            drpSearchCity.DataTextField = "City";
            drpSearchCity.DataSource = objClient.GetCityByCountry(Convert.ToInt32(drpSearchCountry.SelectedValue)).OrderBy(a => a.City);
            drpSearchCity.DataBind();
            drpSearchCity.Items.Insert(0, new ListItem("--Select city--", ""));
            drpSearchCity.Enabled = true;


            lstcity.DataValueField = "Id";
            lstcity.DataTextField = "City";
            lstcity.DataSource = objClient.GetCityByCountry(Convert.ToInt32(drpSearchCountry.SelectedValue)).OrderBy(a => a.City);
            lstcity.DataBind();
         // lstcity.Items.Insert(0, new ListItem("--Select city--", ""));
            lstcity.Enabled = true;
        }
        else
        {
            drpSearchCity.Items.Clear();
            drpSearchCity.Items.Insert(0, new ListItem("--Select city--", ""));
            drpSearchCity.Enabled = false;
            lstcity.Items.Clear();
           lstcity.Items.Insert(0, new ListItem("--Select city--", ""));
            lstcity.Enabled = false;
        }
    }
    protected void rdbBooking_SelectedIndexChanged(object sender, EventArgs e)
    {
        rdbBookingClick.Invoke(sender, e);
        //FillHotelMeetingRoom();
        if (Request.Url.AbsolutePath.ToLower().Contains("canceltransfer.aspx"))
        {
            FillHotelMeetingRoom();
            txtNumber.Text = "";
        }
        else
        {
            txtFromdate.Text = "dd/mm/yy";
            txtTodate.Text = "dd/mm/yy";
        }
        drpSearchCountry.SelectedValue = "0";
        drpMeetingRoom.Enabled = false;
        if (drpSearchCity.SelectedValue != "")
        {
            drpSearchCity.Items.Clear();
            drpSearchCity.Items.Insert(0, new ListItem("--Select city--", ""));
            drpSearchCity.Enabled = false;
        }
    }
    /// <summary>
    /// Bind all the hotel name and meeting room Table as per booking/request for CancelTrasfer
    /// </summary>
    public void FillHotelMeetingRoom()
    {
        if (rdbBooking.SelectedValue == "0")
        {
            lblBookingType.Text = "Booking Number";
            VList<Viewbookingrequest> vlistBook;
            string whereclaus = string.Empty;
            if (drpSearchCity.SelectedValue == "")
            {
                whereclaus = "booktype = 0";
            }
            else
            {
                whereclaus = "booktype = 0 and CityId='" + drpSearchCity.SelectedValue + "'";
            }
            vlistBook = objViewBooking.Bindgrid(whereclaus, String.Empty);
            drpHotelName.DataValueField = "HotelId";
            drpHotelName.DataTextField = "HotelName";
            drpHotelName.DataSource = vlistBook.FindAllDistinct(ViewbookingrequestColumn.HotelId).OrderBy(a => a.HotelName);
            drpHotelName.DataBind();
            drpHotelName.Items.Insert(0, new ListItem("--Select Hotel Name--", "0"));
            //drpMeetingRoom.Items.Insert(0, new ListItem("--Select Meeting Room--", "0"));
        }
        else
        {
            lblBookingType.Text = "Request Number";
            VList<Viewbookingrequest> vlistRequest;
            string whereclaus = string.Empty;
            if (drpSearchCity.SelectedValue == "")
            {
               // whereclaus = "requeststatus in (5,7,2) and booktype = 1";
                whereclaus = "requeststatus in (5,7,2) and booktype in ( 1,2)";
            }
            else
            {
                whereclaus = "requeststatus in (5,7,2) and booktype in ( 1,2) and CityId='" + drpSearchCity.SelectedValue + "'";
            }            
            vlistRequest = objViewBooking.Bindgrid(whereclaus, String.Empty);
            drpHotelName.DataValueField = "HotelId";
            drpHotelName.DataTextField = "HotelName";
            drpHotelName.DataSource = vlistRequest.FindAllDistinct(ViewbookingrequestColumn.HotelId).OrderBy(a => a.HotelName);
            drpHotelName.DataBind();
            drpHotelName.Items.Insert(0, new ListItem("--Select Hotel Name--", "0"));
            //drpMeetingRoom.Items.Insert(0, new ListItem("--Select Meeting Room--", "0"));
            
        }
        if (drpMeetingRoom.Items.Count == 0)
        {
            drpMeetingRoom.Items.Insert(0, new ListItem("--Select Meeting Room--", "0"));
        }
        drpMeetingRoom.Enabled = false;

        ////--------
        //if (propBookRef == 0)
        //{
        //   // lblBookingType.Text = "Booking Number";
        //    VList<Viewbookingrequest> vlistBook;
        //    string whereclaus = string.Empty;
        //    if (drpSearchCity.SelectedValue == "")
        //    {
        //        whereclaus = "booktype = 0   and ( IsComissionDone=0 or  IsComissionDone is null) and InvoiceId= null";
        //    }
        //    else
        //    {
        //        whereclaus = "booktype = 0   and ( IsComissionDone=0 or  IsComissionDone is null) and InvoiceId= null and CityId='" + drpSearchCity.SelectedValue + "'";
        //    }
        //    vlistBook = objViewBooking.Bindgrid(whereclaus, String.Empty);
        //    drpHotelName.DataValueField = "HotelId";
        //    drpHotelName.DataTextField = "HotelName";
        //    drpHotelName.DataSource = vlistBook.FindAllDistinct(ViewbookingrequestColumn.HotelId).OrderBy(a => a.HotelName);
        //    drpHotelName.DataBind();
        //    drpHotelName.Items.Insert(0, new ListItem("--Select Hotel Name--", "0"));
        //    drpMeetingRoom.Items.Insert(0, new ListItem("--Select Meeting Room--", "0"));
        //}
        //else
        //{
        //   // lblBookingType.Text = "Request Number";
        //    VList<Viewbookingrequest> vlistRequest;
        //    string whereclaus = string.Empty;
        //    if (drpSearchCity.SelectedValue == "")
        //    {
        //        whereclaus = "requeststatus in (7)  and ( IsComissionDone=0 or  IsComissionDone is null) and booktype = 1 ";
        //    }
        //    else
        //    {
        //        whereclaus = "requeststatus in (7)  and ( IsComissionDone=0 or  IsComissionDone is null) and booktype = 1 and CityId='" + drpSearchCity.SelectedValue + "'";
        //    }
        //    vlistRequest = objViewBooking.Bindgrid(whereclaus, String.Empty);
        //    drpHotelName.DataValueField = "HotelId";
        //    drpHotelName.DataTextField = "HotelName";
        //    drpHotelName.DataSource = vlistRequest.FindAllDistinct(ViewbookingrequestColumn.HotelId).OrderBy(a => a.HotelName);
        //    drpHotelName.DataBind();
        //    drpHotelName.Items.Insert(0, new ListItem("--Select Hotel Name--", "0"));
        //    drpMeetingRoom.Items.Insert(0, new ListItem("--Select Meeting Room--", "0"));
        //}

    }

    protected void drpHotelName_SelectedIndexChanged(object sender, EventArgs e)
    {
        drpMeetingRoom.Enabled = true;
        if (rdbBooking.SelectedValue == "0")
        {
            lblBookingType.Text = "Booking Number";
            VList<Viewbookingrequest> vlistBook;
            string whereclaus = "booktype = 0 and HotelId='" + drpHotelName.SelectedValue + "'";
            vlistBook = objViewBooking.Bindgrid(whereclaus, String.Empty);
            if (vlistBook.Count > 0)
            {               
                drpMeetingRoom.DataValueField = "MainMeetingRoomId";
                drpMeetingRoom.DataTextField = "Name";
                drpMeetingRoom.DataSource = vlistBook.FindAllDistinct(ViewbookingrequestColumn.MainMeetingRoomId).OrderBy(a => a.Name);
                drpMeetingRoom.DataBind();
                drpMeetingRoom.Items.Insert(0, new ListItem("--Select Meeting Room--", "0"));                
            }
            else
            {
                drpMeetingRoom.SelectedValue = "0";
                drpMeetingRoom.Enabled = false;
            }
        }
        else
        {
            lblBookingType.Text = "Request Number";
            VList<Viewbookingrequest> vlistRequest;
            string whereclaus = "requeststatus in (5,7,2) and booktype in ( 1,2) and HotelId='" + drpHotelName.SelectedValue + "'";
            vlistRequest = objViewBooking.Bindgrid(whereclaus, String.Empty);
            if (vlistRequest.Count > 0)
            {
                drpMeetingRoom.DataValueField = "MainMeetingRoomId";
                drpMeetingRoom.DataTextField = "Name";
                drpMeetingRoom.DataSource = vlistRequest.FindAllDistinct(ViewbookingrequestColumn.MainMeetingRoomId).OrderBy(a => a.Name);
                drpMeetingRoom.DataBind();
                drpMeetingRoom.Items.Insert(0, new ListItem("--Select Meeting Room--", "0"));
            }
            else
            {
                drpMeetingRoom.SelectedValue = "0";
                drpMeetingRoom.Enabled = false;
            }
        }
        //if (propBookRef == 0)
        //{
        //    // lblBookingType.Text = "Booking Number";
           
        //    VList<Viewbookingrequest> vlistBook;
        //    string whereclaus = "booktype = 0   and ( IsComissionDone=0 or  IsComissionDone is null) and InvoiceId= null and HotelId='" + drpHotelName.SelectedValue + "'";
        //    vlistBook = objViewBooking.Bindgrid(whereclaus, String.Empty);
        //    if (vlistBook.Count > 0)
        //    {
        //        drpMeetingRoom.DataValueField = "MainMeetingRoomId";
        //        drpMeetingRoom.DataTextField = "Name";
        //        drpMeetingRoom.DataSource = vlistBook.FindAllDistinct(ViewbookingrequestColumn.MainMeetingRoomId).OrderBy(a => a.Name);
        //        drpMeetingRoom.DataBind();
        //        drpMeetingRoom.Items.Insert(0, new ListItem("--Select Meeting Room--", "0"));
        //    }
        //    else
        //    {
        //        drpMeetingRoom.SelectedValue = "0";
        //        drpMeetingRoom.Enabled = false;
        //    }
        //}
        //else
        //{
        //    VList<Viewbookingrequest> vlistRequest;
        //    string whereclaus = " requeststatus in (7)  and ( IsComissionDone=0 or  IsComissionDone is null) and booktype = 1  and HotelId='" + drpHotelName.SelectedValue + "'";
        //    vlistRequest = objViewBooking.Bindgrid(whereclaus, String.Empty);
        //    if (vlistRequest.Count > 0)
        //    {
        //        drpMeetingRoom.DataValueField = "MainMeetingRoomId";
        //        drpMeetingRoom.DataTextField = "Name";
        //        drpMeetingRoom.DataSource = vlistRequest.FindAllDistinct(ViewbookingrequestColumn.MainMeetingRoomId).OrderBy(a => a.Name);
        //        drpMeetingRoom.DataBind();
        //        drpMeetingRoom.Items.Insert(0, new ListItem("--Select Meeting Room--", "0"));
        //    }
        //    else
        //    {
        //        drpMeetingRoom.SelectedValue = "0";
        //        drpMeetingRoom.Enabled = false;
        //    }
        //}
    }

    protected void drpSearchCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillHotelMeetingRoom();
    }

    
}