﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Entities;
using LMMR.Data;
using LMMR.Business;
using System.Text;

public partial class BookingRequests : System.Web.UI.Page
{
    #region variables   
    LanguageManager lm = new LanguageManager();
    public class Mapdate
    {
        public string Hotelbname;
        public string Logitude;
        public string Latitude;
        public string IsPromoted;

    }
    ManageMapSearch objManageMapSearch = new ManageMapSearch();
    public string HotelData
    {
        get;
        set;
    }
    VList<BookingRequestViewList> vlist;
    BookingRequest objBookingRequest = new BookingRequest();
    HotelInfo ObjHotelinfo = new HotelInfo();
    #endregion

    #region Pageload
    /// <summary>
    /// Method to manage the page load functionality.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LanguageID"] == null)
        {
            Session["LanguageID"] = 1;
        }
        if (!Page.IsPostBack)
        {
            DDLlanguage.DataTextField = "Name";
            DDLlanguage.DataValueField = "ID";
            DDLlanguage.DataSource = lm.GetData();
            DDLlanguage.DataBind();
            DDLlanguage.SelectedValue = Convert.ToString(Session["LanguageID"]);
            BindCountryDropdown();
            BindCityDropdownByCountryID(0);
            ScriptManager.RegisterStartupScript(this, typeof(Page), "LoadMap1", "DefaultMap('europe');", true);

            //grvBooking.DataSource = ObjHotelinfo.GetAllHotel();
            //grvBooking.DataBind();

            //grvRequest.DataSource = ObjHotelinfo.GetAllHotel();
            //grvRequest.DataBind();
        }
        HotelData = "[[]]";
    }
    #endregion

    #region function
    void GetAllHotelByCity(int cityID, int countryID)
    {
        StringBuilder objSB = new StringBuilder();
        TList<Hotel> objHotel = objManageMapSearch.GetHotelByCity(cityID, countryID);
        List<Mapdate> objmapdat = new List<Mapdate>();
        HotelData = "[";
        if (objHotel.Count > 0)
        {
            foreach (Hotel h in objHotel)
            {
                if (HotelData == "[")
                {

                    HotelData += "['" + h.Name + "', " + h.Latitude + "," + h.Longitude + "," + 1 + ",'No']";
                }
                else
                {
                    HotelData = HotelData + ",[" + "'" + h.Name + "', " + h.Latitude + "," + h.Longitude + "," + 1 + ",'Special']";
                }
            }
            HotelData += "]";
        }

    }
    /// <summary>
    /// This method is taking all the data of language table
    /// </summary>
    /// <returns></returns>
    //public TList<Language> GetData()
    //{
    //    TList<Language> objlanguage = DataRepository.LanguageProvider.GetAll();
    //    return objlanguage;
    //}

    /// <summary>
    /// This method is passing the key value
    /// </summary>
    /// <param name="Key"></param>
    /// <returns></returns>
    public string GetKeyResult(string Key)
    {
        return ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), Key);
    }

    /// <summary>
    /// This function used for bind all country.
    /// </summary>
    void BindCountryDropdown()
    {
        drpCountry.DataTextField = "CountryName";
        drpCountry.DataValueField = "Id";
        drpCountry.DataSource = objManageMapSearch.GetCountryData();
        drpCountry.DataBind();
        drpCountry.Items.Insert(0, new ListItem("--Select country--", "0"));
    }

    /// <summary>
    /// This function used for bind cities by countryID.
    /// </summary>
    /// <param name="intcountryID"></param>
    void BindCityDropdownByCountryID(int intcountryID)
    {
        if (intcountryID != 0)
        {
            drpCity.DataValueField = "Id";
            drpCity.DataTextField = "City";
            drpCity.DataSource = objManageMapSearch.GetCityData(Convert.ToInt32(drpCountry.SelectedItem.Value));
            drpCity.DataBind();
            drpCity.Items.Insert(0, new ListItem("--Select city--", "0"));
        }
        else
        {
            drpCity.Items.Clear();
            drpCity.Items.Insert(0, new ListItem("--Select city--", "0"));
        }
    }
    #endregion

    #region events
    /// <summary>
    /// this events is used for index changing of dropdown list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DDLlanguage_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["LanguageID"] = DDLlanguage.SelectedValue;
        Response.Redirect(Request.RawUrl, false);
    }

    /// <summary>
    /// This event Bind the city dropdown according to selected countryid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void drpCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpCountry.SelectedValue != "0")
        {
            BindCityDropdownByCountryID(Convert.ToInt32(drpCountry.SelectedValue));
            ScriptManager.RegisterStartupScript(this, typeof(Page), "LoadMap2", "DefaultMap('" + drpCountry .SelectedItem.Text + "');", true);
        }
    }

    /// <summary>
    /// This Value event will be fire on city.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void drpCity_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (Convert.ToInt32(drpCity.SelectedValue) != 0 && Convert.ToInt32(drpCountry.SelectedValue) != 0)
        {

            GetAllHotelByCity(Convert.ToInt32(drpCity.SelectedValue), Convert.ToInt32(drpCountry.SelectedValue));

            //Function to get main center point of the city.
            string strLatitudeOfCenter = "";
            string strLogitudeOfCenter = "";
            string strCenterPlace = "";
            string strCenterPoint = "";
            int intZoomLevel = 10;
            TList<MainPoint> objCenterPoint = objManageMapSearch.GetCenterPoint(Convert.ToInt32(drpCity.SelectedValue));
            if (objCenterPoint.Count > 0)
            {
                strLatitudeOfCenter = objCenterPoint.FirstOrDefault().Latitude.ToString();
                strLogitudeOfCenter = objCenterPoint.FirstOrDefault().Longitude.ToString();
                hdnMapLatitude.Value = objCenterPoint.FirstOrDefault().Latitude.ToString();
                hdnMapLogitude.Value = objCenterPoint.FirstOrDefault().Longitude.ToString();
                strCenterPlace = objCenterPoint.FirstOrDefault().MainPointName;
                intZoomLevel = 12;
            }

            strCenterPoint = strLatitudeOfCenter + "," + strLogitudeOfCenter;
            ScriptManager.RegisterStartupScript(this, typeof(Page), "LoadMap3", "OnDemand('" + strCenterPoint + "','" + strCenterPlace + "','" + intZoomLevel + "');", true);
        }
    }

    /// <summary>
    /// This event will be fire on select of radius.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void drpRadius_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(drpCity.SelectedValue) != 0 && Convert.ToInt32(drpCountry.SelectedValue) != 0)
        {

            GetAllHotelByCity(Convert.ToInt32(drpCity.SelectedValue), Convert.ToInt32(drpCountry.SelectedValue));
            string strLatitudeOfCenter = "";
            string strLogitudeOfCenter = "";
            string strCenterPlace = "";
            string strCenterPoint = "";
            int intZoomLevel = 10;

            //if (hdnMapLatitude.Value != "0" && hdnMapLogitude.Value != "0")
            //{
            //    strLatitudeOfCenter = hdnMapLatitude.Value;
            //    strLogitudeOfCenter = hdnMapLogitude.Value;
            //    strCenterPlace = "You are !";
            //    intZoomLevel = 22;
            //}

            TList<MainPoint> objCenterPoint = objManageMapSearch.GetCenterPoint(Convert.ToInt32(drpCity.SelectedValue));
            if (objCenterPoint.Count > 0)
            {
                strLatitudeOfCenter = objCenterPoint.FirstOrDefault().Latitude.ToString();
                strLogitudeOfCenter = objCenterPoint.FirstOrDefault().Longitude.ToString();
                hdnMapLatitude.Value = objCenterPoint.FirstOrDefault().Latitude.ToString();
                hdnMapLogitude.Value = objCenterPoint.FirstOrDefault().Longitude.ToString();
                strCenterPlace = objCenterPoint.FirstOrDefault().MainPointName;
                intZoomLevel = 12;
            }

            strCenterPoint = strLatitudeOfCenter + "," + strLogitudeOfCenter;
            ScriptManager.RegisterStartupScript(this, typeof(Page), "LoadMap4", "OnDemand('" + strCenterPoint + "','" + strCenterPlace + "','" + intZoomLevel + "');", true);
        }
    }

    #endregion
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        DateTime fromDate = new DateTime(Convert.ToInt32("20" + txtBookingDate.Text.Split('/')[2]), Convert.ToInt32(txtBookingDate.Text.Split('/')[1]), Convert.ToInt32(txtBookingDate.Text.Split('/')[0]));
        string whereclaus = "CountryId='" + drpCountry.SelectedValue + "' and CityId='" + drpCity.SelectedValue + "' and SpecialPriceDate = '" + fromDate + "'";
        //WHERE     (SpecialPriceAndPromo.SpecialPriceDate = '2012-04-16 T00:00:00.000') AND (50 BETWEEN MeetingRoomConfig.MinCapacity AND 
        //MeetingRoomConfig.MaxCapicity) AND (DATEDIFF(day, Availability.AvailabilityDate, GETDATE()) >= Availability.LeadTimeForMeetingRoom)
        //vlist = objBookingRequest.GetBookingReqDetails(whereclaus);
        //grvBooking.DataSource = vlist;
        //grvBooking.DataBind();
    }
}