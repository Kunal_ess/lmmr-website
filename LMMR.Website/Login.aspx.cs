﻿#region Using Directives
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using log4net;
using log4net.Config;
using System.IO;
using System.IO;
using System.Linq;
#endregion

public partial class Login : BasePage
{
    #region Variables and Properties
    UserManager objUserManager = new UserManager();
    HotelManager objHotelManager = new HotelManager();
    SendMails objSendmail = new SendMails();
    ManageCMSContent obj = new ManageCMSContent();
    EmailConfigManager em = new EmailConfigManager();
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["ListBookings"] = null;
                //if (!HttpContext.Current.Request.IsSecureConnection)
                //{
                //    Response.Redirect(Request.Url.AbsoluteUri.Replace("http://", "https://"), true);
                //    return;
                //}
                divError.Attributes.Add("class", "error");
                divError.Style.Add("display", "none");
                recaptcha.Visible = false;
                if (Request.Cookies["UName"] != null)
                    txtUserName.Text = Request.Cookies["UName"].Value;
                if (Request.Cookies["PWD"] != null)
                    txtPassword.Attributes.Add("value", Request.Cookies["PWD"].Value);
                if (Request.Cookies["UName"] != null && Request.Cookies["PWD"] != null)
                    chkRememberPassword.Checked = true;
                if (Session["Active"] != null)
                {
                    if (Session["Active"].ToString() == "Y")
                    {
                        divError.Style.Add("display", "block");
                        divError.Attributes.Add("class", "succesfuly");
                        divError.InnerHtml = GetKeyResult("ACCOUNTACTIVATEDPLEASELOGINNOW");
                        Session.Remove("Active");
                    }
                    else if (Session["Active"].ToString()=="ACTIVATION_MAIL_REUSE_DETECTED")
                    {
                        divError.Style.Add("display", "block");
                        divError.Attributes.Add("class", "error");
                        divError.InnerHtml = GetKeyResult("ACTIVATIONMAILREUSEDETECTED");
                        Session.Remove("Active");
                    }
                }
                //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                if (l != null)
                {
                    hypForgotPassword.NavigateUrl = SiteRootPath + "forgot-password/" + l.Name.ToLower();
                }
                else
                {
                    hypForgotPassword.NavigateUrl = SiteRootPath + "forgot-password/english";
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Events
    /// <summary>
    /// click of login button then this event call.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        try
        {
            if (recaptcha.Visible)
            {
                recaptcha.Validate();
                if (recaptcha.IsValid)
                {
                    loginuser();
                }
                else
                {
                    divError.Style.Add("display", "block");
                    divError.InnerHtml = GetKeyResult("PLEASEENTERVALIDCAPTCHATEXT");
                }
            }
            else
            {
                loginuser();
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// Click of cancel button then this event call and reset all the fields.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnReset_Click(object sender, EventArgs e)
    {
        try
        {
            txtUserName.Text = "";
            txtPassword.Text = "";
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This is the Event Handler for Agency Link Click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //protected void agencyLink_Click(object sender, EventArgs e)
    //{
    //    Session["task"] = "register";
    //    Session["registerType"] = Convert.ToInt32(Usertype.Agency);
    //    //Response.Redirect("Registration.aspx", false);
    //    //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
    //    if (l != null)
    //    {
    //        Response.Redirect(SiteRootPath + "registration-as-agency/" + l.Name.ToLower());
    //    }
    //    else
    //    {
    //        Response.Redirect(SiteRootPath + "registration-as-agency/english");
    //    }
    //    return;
    //}

    /// <summary>
    /// This is the Event Handler for Company Link Click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void companyLink_Click(object sender, EventArgs e)
    {
        Session["task"] = "register";
        Session["registerType"] = Convert.ToInt32(Usertype.Company);
        //Response.Redirect("Registration.aspx", false);
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "registration-as-company/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "registration-as-company/english");
        }
        return;
    }

    /// <summary>
    /// This is the Event Handler for User Link Click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void userLink_Click(object sender, EventArgs e)
    {
        Session["task"] = "register";
        Session["registerType"] = Convert.ToInt32(Usertype.privateuser);
        //Response.Redirect("Registration.aspx", false);
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "registration/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "registration/english");
        }
        return;
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        EmailConfig eConfig = em.GetByName(" Corporate solution 2-Meet");
        EmailValueCollection objEmailValues = new EmailValueCollection();
        if (eConfig.IsActive)
        {
            //Send mail to the operator.
            string bodymsg = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
            EmailConfigMapping emap2 = new EmailConfigMapping();
            emap2 = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
            bodymsg = bodymsg.Replace("@CONTENTS", emap2.EmailContents);
            objSendmail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
            objSendmail.Bcc = ConfigurationManager.AppSettings["WatchEMailID"];
            Users objoperator = new HotelManager().GetFirstOperatorInDataBase();
            if (objoperator != null)
            {
                objSendmail.ToEmail = objoperator.EmailId;
            }
            //string bodymsgOperator = File.ReadAllText(Server.MapPath("~/EmailTemplets/JoinTodayForOperatorSuperAdmin.html"));
            EmailValueCollection objEmailValuesOperator = new EmailValueCollection();
            Users objUser = (Users)Session["CurrentUser"];
            objSendmail.Subject = "Request for corporate solution 2-Meet";
            foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailToCoporate("Operator", txtcompanyname.Text, txtEmail.Text,txtphoneNo.Text,txtfirstname.Text + txtlastname.Text))
            {
                bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
            }
            string status;
           objSendmail.Body = bodymsg;
           status= objSendmail.SendMail();
           if (status == "Send Mail")
           {
               Page.RegisterStartupScript("msg", "<script language='javascript'>alert('Mail Send successfully');</script>");
               
           }
           else
           {
               
               Page.RegisterStartupScript("msg", "<script language='javascript'>alert(" + "'" + status + "'" + ");</script>");
              
           }
        }
        

    }
    #endregion

    #region Method
    /// <summary>
    /// For login this method is call and check all the login functionality inside this method.
    /// </summary>
    protected void loginuser()
    {
        try
        {
            //Code For Remember me
            if (chkRememberPassword.Checked == true)
            {
                Response.Cookies["UName"].Value = txtUserName.Text;
                Response.Cookies["PWD"].Value = txtPassword.Text;
                Response.Cookies["UName"].Expires = DateTime.Now.AddMonths(2);
                Response.Cookies["PWD"].Expires = DateTime.Now.AddMonths(2);
            }
            else
            {
                Response.Cookies["UName"].Expires = DateTime.Now.AddMonths(-1);
                Response.Cookies["PWD"].Expires = DateTime.Now.AddMonths(-1);
            }
            //Check for Login
            Users objUser = objUserManager.LoginUser(txtUserName.Text.Trim().Replace("'", ""), txtPassword.Text.Trim().Replace("'", ""));
            if (objUser == null)
            {
                //If No user found send message of Incorrect Login
                divError.Attributes.Add("class", "error");
                divError.Style.Add("display", "block");
                divError.InnerHtml = GetKeyResult("INVALIDLOGINDETAILSPLEASETRYAGAIN");
                if (ViewState["count"] == null)
                {
                    ViewState["count"] = 1;
                }
                else
                {
                    ViewState["count"] = Convert.ToInt32(ViewState["count"]) + 1;
                }
                recaptcha.Visible = true;
                if (Convert.ToInt32(ViewState["count"]) >= 3)
                {
                    divError.Attributes.Add("class", "error");
                    divError.Style.Add("display", "block");
                    divError.InnerHtml += "<br />" + GetKeyResult("PLEASECLICKFORGETPASSWORDTORETRIVEYOURPASSWORD");
                }
            }
            else
            {
                //If Correct User Found then Chack type of user.
                Session["TimeOut"] = objHotelManager.GetSessionTimeoutValue();
                ViewState["count"] = 0;
                if (objUser.Usertype == (int)Usertype.HotelUser || objUser.Usertype == (int)Usertype.HotelGroupUser || objUser.Usertype == (int)Usertype.HotelClient || objUser.Usertype == (int)Usertype.SuperMan)
                {
                    //If Session is null then Move to login 1. Check Hotel Exist if Yes the login otherwise logout and remove session.
                    if (Session["CurrentUser"] == null)
                    {
                        Session["CurrentUserID"] = objUser.UserId;
                        Session["CurrentUser"] = objUser;
                        TList<Hotel> htlList = objHotelManager.GetHotelByClientId(Session["CurrentUserID"] == null ? 0 : Convert.ToInt64(Session["CurrentUserID"]));
                        if (htlList.Count > 0)
                        {
                            logger.Debug("Login User ID:" + objUser.UserId + " | Login User:" + objUser.FirstName + " | Login UserType:" + objUser.Usertype);
                            Response.Redirect("~/HotelUser/HotelDashboard.aspx", false);
                            return;
                        }
                        else
                        {
                            divError.Attributes.Add("class", "error");
                            divError.Style.Add("display", "block");
                            divError.InnerHtml = GetKeyResult("YOUDONTHAVEANYACTIVEHOTEL");
                            Session.Remove("CurrentUserID");
                            Session.Remove("CurrentUser");
                        }
                    }
                    else
                    {
                        //If Session Exist the Check the same user login or other user login if Other user login the send message for same userlogin else login the user with remain the session
                        Users objOldUser = (Users)Session["CurrentUser"];
                        if (objOldUser.UserId == objUser.UserId)
                        {
                            TList<Hotel> htlList = objHotelManager.GetHotelByClientId(Session["CurrentUserID"] == null ? 0 : Convert.ToInt64(Session["CurrentUserID"]));
                            if (htlList.Count > 0)
                            {
                                Response.Redirect("~/HotelUser/HotelDashboard.aspx", false);
                                return;
                            }
                            else
                            {
                                divError.Attributes.Add("class", "error");
                                divError.Style.Add("display", "block");
                                divError.InnerHtml = GetKeyResult("YOUDONTHAVEANYACTIVEHOTEL");
                                Session.Remove("CurrentUsererID");
                                Session.Remove("CurrentUser");
                            }
                        }
                        else
                        {
                            divError.Attributes.Add("class", "error");
                            divError.Style.Add("display", "block");
                            divError.InnerHtml = GetKeyResult("PLEASECLOSEPREVIOUSLOGIN");
                        }
                    }
                }
                else if (objUser.Usertype == (int)Usertype.Operator)
                {
                    //If User type is Operator the 
                    logger.Debug("Login as Hotel operator user.");
                    //If Session not exist then move to Login.
                    if (Session["CurrentOperator"] == null)
                    {
                        Session["CurrentOperatorID"] = objUser.UserId;
                        Session["CurrentOperator"] = objUser;
                        //Response.StatusCode = 301;
                        //Response.AddHeader("Location", SiteRootPath+"Operator/ControlPanel.aspx");
                        //Response.End();
                        Response.Redirect("~/Operator/ControlPanel.aspx", false);
                        return;
                    }
                    else
                    {
                        //If Session Exist the Check the same user login or other user login if Other user login the send message for same userlogin else login the user with remain the session
                        Users objOldUser = (Users)Session["CurrentOperator"];
                        if (objOldUser.UserId == objUser.UserId)
                        {
                            Response.Redirect("~/Operator/ControlPanel.aspx", false);
                            return;
                        }
                        else
                        {
                            divError.Attributes.Add("class", "error");
                            divError.Style.Add("display", "block");
                            divError.InnerHtml = GetKeyResult("PLEASECLOSEPREVIOUSLOGIN");
                        }
                    }
                }
                else if (objUser.Usertype == (int)Usertype.Superadmin)
                {
                    //If User type is Operator the 
                    logger.Debug("Login as Hotel operator user.");
                    //If Session not exist then move to Login.
                    if (Session["CurrentSuperAdmin"] == null)
                    {
                        Session["CurrentSuperAdminID"] = objUser.UserId;
                        Session["CurrentSuperAdmin"] = objUser;
                        //Add these two session id to enter superadmin clientcontract session
                        Session["CurrentOperatorID"] = objUser.UserId;
                        Session["CurrentOperator"] = objUser;
                        Response.Redirect("~/Operator/ControlPanel.aspx", false);
                        return;
                    }
                    else
                    {
                        //If Session Exist the Check the same user login or other user login if Other user login the send message for same userlogin else login the user with remain the session
                        Users objOldUser = (Users)Session["CurrentSuperAdmin"];
                        if (objOldUser.UserId == objUser.UserId)
                        {
                            //Add these two session id to enter superadmin clientcontract session
                            Session["CurrentOperatorID"] = objUser.UserId;
                            Session["CurrentOperator"] = objUser;
                            Response.Redirect("~/Operator/ControlPanel.aspx", false);
                            return;
                        }
                        else
                        {
                            divError.Attributes.Add("class", "error");
                            divError.Style.Add("display", "block");
                            divError.InnerHtml = GetKeyResult("PLEASECLOSEPREVIOUSLOGIN");
                        }
                    }
                }
                else if (objUser.Usertype == (int)Usertype.Salesperson)
                {
                    //If User type is Operator the 
                    logger.Debug("Login as Hotel SalesPerson user.");
                    //If Session not exist then move to Login.
                    if (Session["CurrentSalesPerson"] == null)
                    {
                        Session["CurrentSalesPersonID"] = objUser.UserId;
                        Session["CurrentSalesPerson"] = objUser;
                        Response.Redirect("~/Operator/ClientContract.aspx", false);
                        return;
                    }
                    else
                    {
                        //If Session Exist the Check the same user login or other user login if Other user login the send message for same userlogin else login the user with remain the session
                        Users objOldUser = (Users)Session["CurrentSalesPerson"];
                        if (objOldUser.UserId == objUser.UserId)
                        {
                            //Add these two session id to enter superadmin clientcontract session
                            Session["CurrentSalesPersonID"] = objUser.UserId;
                            Session["CurrentSalesPerson"] = objUser;
                            Response.Redirect("~/Operator/ClientContract.aspx", false);
                            return;
                        }
                        else
                        {
                            divError.Attributes.Add("class", "error");
                            divError.Style.Add("display", "block");
                            divError.InnerHtml = GetKeyResult("PLEASECLOSEPREVIOUSLOGIN");
                        }
                    }
                }

                else if (objUser.Usertype == (int)Usertype.Agency || objUser.Usertype == (int)Usertype.agencyuser)
                {
                    Session["CurrentAgencyUserID"] = objUser.UserId;
                    Session["CurrentAgencyUser"] = objUser;
                    Session["LanguageID"] = "1";
                    if (Session["SerachID"] != null)
                    {
                        BookingManager bm = new BookingManager();
                        SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
                        Createbooking objbooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
                        objbooking.CurrentUserId = objUser.UserId;
                        st.UserId = objUser.UserId;
                        st.SearchObject = TrailManager.XmlSerialize(objbooking);
                        if (bm.SaveSearch(st))
                        {
                            if (st.CurrentStep == 1)
                            {
                                if (objUser.Usertype == (int)Usertype.Agency)
                                {
                                    objUser.WorkAsAgentUser = true;
                                    Session["CurrentAgencyUser"] = objUser;
                                }
                                Response.Redirect("~/Agency/booking.aspx");
                            }
                            else if (st.CurrentStep == 2)
                            {
                                bm.ResetIsReserverTrue(objbooking);
                                if (objUser.Usertype == (int)Usertype.Agency)
                                {
                                    objUser.WorkAsAgentUser = true;
                                    Session["CurrentAgencyUser"] = objUser;
                                }
                                Response.Redirect("~/Agency/booking.aspx");
                                return;
                            }
                            else
                            {
                                if (objUser.Usertype == (int)Usertype.agencyuser)
                                {
                                    Response.Redirect("~/Agency/controlpanelUser.aspx");
                                }
                                else if (objUser.Usertype == (int)Usertype.Agency)
                                {
                                    Response.Redirect("~/Agency/controlpanel.aspx");
                                }
                            }
                        }
                        else
                        {
                            divError.Attributes.Add("class", "error");
                            divError.Style.Add("display", "block");
                            divError.InnerHtml = "Some error appear while redirecting to the search page.";
                        }
                    }
                    else if (Session["RequestID"] != null)
                    {
                        BookingManager bm = new BookingManager();
                        SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["RequestID"]));
                        CreateRequest objRequest = (CreateRequest)TrailManager.XmlDeserialize(typeof(CreateRequest), st.SearchObject);
                        objRequest.CurrentUserId = objUser.UserId;
                        st.UserId = objUser.UserId;
                        st.SearchObject = TrailManager.XmlSerialize(objRequest);
                        if (bm.SaveSearch(st))
                        {
                            if (st.CurrentStep == 1)
                            {
                                Response.Redirect("~/Agency/request.aspx");
                                return;
                            }
                            else if (st.CurrentStep == 2)
                            {
                                Response.Redirect("~/Agency/Request.aspx");
                                return;
                            }
                            else
                            {
                                Response.Redirect("~/Agency/controlpanel.aspx");
                                return;
                            }
                        }
                        else
                        {
                            divError.Attributes.Add("class", "error");
                            divError.Style.Add("display", "block");
                            divError.InnerHtml = "Some error appear while redirecting to the request page.";
                        }
                    }
                    else
                    {
                        if (Session["CurrentAgencyUserID"] == null)
                        {
                            Session["CurrentAgencyUserID"] = objUser.UserId;
                            Session["CurrentAgencyUser"] = objUser;
                            Session["LanguageID"] = 1;
                            if (objUser.Usertype == (int)Usertype.agencyuser)
                            {
                                Response.Redirect("~/Agency/controlpanelUser.aspx");
                            }
                            else if (objUser.Usertype == (int)Usertype.Agency)
                            {
                                Response.Redirect("~/Agency/controlpanel.aspx");
                            }
                            return;
                        }
                        else
                        {
                            //If Session Exist the Check the same user login or other user login if Other user login the send message for same userlogin else login the user with remain the session
                            Users objOldUser = (Users)Session["CurrentAgencyUser"];
                            if (objOldUser.UserId == objUser.UserId)
                            {
                                if (objUser.Usertype == (int)Usertype.agencyuser)
                                {
                                    Response.Redirect("~/Agency/controlpanelUser.aspx");
                                }
                                else if (objUser.Usertype == (int)Usertype.Agency)
                                {
                                    Response.Redirect("~/Agency/controlpanel.aspx");
                                }
                                return;
                            }
                            else
                            {
                                divError.Attributes.Add("class", "error");
                                divError.Style.Add("display", "block");
                                divError.InnerHtml = GetKeyResult("PLEASECLOSEPREVIOUSLOGIN");
                            }
                        }
                    }
                }
                else if (objUser.Usertype == (int)Usertype.Company || objUser.Usertype == (int)Usertype.privateuser)
                {
                    Session["CurrentRestUserID"] = objUser.UserId;
                    Session["CurrentRestUser"] = objUser;
                    Session["LanguageID"] = objUser.UserDetailsCollection.FirstOrDefault().LanguageId;
                    if (Session["SerachID"] != null)
                    {
                        BookingManager bm = new BookingManager();
                        SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
                        Createbooking objbooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
                        objbooking.CurrentUserId = objUser.UserId;
                        st.UserId = objUser.UserId;
                        st.SearchObject = TrailManager.XmlSerialize(objbooking);
                        if (bm.SaveSearch(st))
                        {
                            if (st.CurrentStep == 1)
                            {
                                //Response.Redirect("BookingStep1.aspx", false);
                                //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                                if (l != null)
                                {
                                    Response.Redirect(SiteRootPath + "booking-step1/" + l.Name.ToLower());
                                }
                                else
                                {
                                    Response.Redirect(SiteRootPath + "booking-step1/english");
                                }
                                return;
                            }
                            else if (st.CurrentStep == 2)
                            {
                                bm.ResetIsReserverTrue(objbooking);
                                //Response.Redirect("BookingStep2.aspx", false);
                                //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                                if (l != null)
                                {
                                    Response.Redirect(SiteRootPath + "booking-step2/" + l.Name.ToLower());
                                }
                                else
                                {
                                    Response.Redirect(SiteRootPath + "booking-step2/english");
                                }
                                return;
                            }
                            else if (st.CurrentStep == 3)
                            {
                                //Response.Redirect("BookingStep3.aspx", false);
                                //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                                Session.Remove("SerachID");
                                Session.Remove("Serach");
                                if (l != null)
                                {
                                    Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
                                }
                                else
                                {
                                    Response.Redirect(SiteRootPath + "default/english");
                                }
                                return;
                            }
                            else if (st.CurrentStep == 4)
                            {
                                //Response.Redirect("BookingStep4.aspx", false);
                                //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                                Session.Remove("SerachID");
                                Session.Remove("Serach");
                                if (l != null)
                                {
                                    Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
                                }
                                else
                                {
                                    Response.Redirect(SiteRootPath + "default/english");
                                }
                                return;
                            }
                        }
                        else
                        {
                            divError.Attributes.Add("class", "error");
                            divError.Style.Add("display", "block");
                            divError.InnerHtml = "Some error appear while redirecting to the search page.";
                        }
                    }
                    else if (Session["RequestID"] != null)
                    {
                        BookingManager bm = new BookingManager();
                        SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["RequestID"]));
                        CreateRequest objRequest = (CreateRequest)TrailManager.XmlDeserialize(typeof(CreateRequest), st.SearchObject);
                        objRequest.CurrentUserId = objUser.UserId;
                        st.UserId = objUser.UserId;
                        st.SearchObject = TrailManager.XmlSerialize(objRequest);
                        if (bm.SaveSearch(st))
                        {
                            if (st.CurrentStep == 1)
                            {
                                //Response.Redirect("RequestStep1.aspx", false);
                                //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                                if (l != null)
                                {
                                    Response.Redirect(SiteRootPath + "request-step1/" + l.Name.ToLower());
                                }
                                else
                                {
                                    Response.Redirect(SiteRootPath + "request-step1/english");
                                }
                                return;
                            }
                            else if (st.CurrentStep == 2)
                            {
                                //Response.Redirect("RequestStep2.aspx", false);
                                //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                                if (l != null)
                                {
                                    Response.Redirect(SiteRootPath + "request-step2/" + l.Name.ToLower());
                                }
                                else
                                {
                                    Response.Redirect(SiteRootPath + "request-step2/english");
                                }
                                return;
                            }
                            else if (st.CurrentStep == 3)
                            {
                                //Response.Redirect("RequestStep3.aspx", false);
                                //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                                if (l != null)
                                {
                                    Response.Redirect(SiteRootPath + "request-step3/" + l.Name.ToLower());
                                }
                                else
                                {
                                    Response.Redirect(SiteRootPath + "request-step3/english");
                                }
                                return;
                            }
                            else if (st.CurrentStep == 4)
                            {
                                //Response.Redirect("RequestStep4.aspx", false);
                                //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                                if (l != null)
                                {
                                    Response.Redirect(SiteRootPath + "request-step4/" + l.Name.ToLower());
                                }
                                else
                                {
                                    Response.Redirect(SiteRootPath + "request-step4/english");
                                }
                                return;
                            }
                        }
                        else
                        {
                            divError.Attributes.Add("class", "error");
                            divError.Style.Add("display", "block");
                            divError.InnerHtml = "Some error appear while redirecting to the request page.";
                        }
                    }
                    else
                    {

                        if (Session["CurrentRestUserID"] == null)
                        {
                            Session["CurrentRestUserID"] = objUser.UserId;
                            Session["CurrentRestUser"] = objUser;
                        }
                        //Response.Redirect("Default.aspx", false);
                        Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                        if (l != null)
                        {
                            Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
                        }
                        else
                        {
                            Response.Redirect(SiteRootPath + "default/english");
                        }
                        return;
                    }
                }
                else
                {
                    divError.Attributes.Add("class", "error");
                    divError.Style.Add("display", "block");
                    divError.InnerHtml = "Right now you are not able to login.";
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            divError.Attributes.Add("class", "error");
            divError.Style.Add("display", "block");
            divError.InnerHtml = "Error occur:" + ex.Message;
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This method returns the resultant string for the passed key
    /// </summary>
    /// <param name="key"></param>
    /// <returns>string</returns>
    public string GetKeyResult(string key)
    {
        return System.Net.WebUtility.HtmlEncode(System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key)));
    }
    #endregion
    protected void agencyLink_Click(object sender, EventArgs e)
    {
        txtfirstname.Text = "";
        txtlastname.Text = "";
        txtEmail.Text = "";
        txtphoneNo.Text = "";
        txtcompanyname.Text = "";
    }
}