﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;


public partial class meeting_rooms : BasePage
{
    ManageCMSContent mcs = new ManageCMSContent();
    ManageCMSContent obj = new ManageCMSContent();
    public void BindURLs()
    {
        ////Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            jointoday.HRef = SiteRootPath + "jointoday/" + l.Name.ToLower();
        }
        else
        {
            jointoday.HRef = SiteRootPath + "jointoday/english";
        }

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentRestUser"] != null)
        {
            Users objUsers = (Users)Session["CurrentRestUser"];
            lblLoginUserName.Text = objUsers.FirstName + " " + objUsers.LastName;
            objUsers.LastLogin = DateTime.Now;
            lstLoginTime.Text = DateTime.Now.ToLongDateString();
            AfterLogin(objUsers.UserId, Convert.ToInt32(objUsers.Usertype));
        }

        if (Session["status"] != null)
        {
            Page.RegisterStartupScript("msg", "<script language='javascript'>alert(" + "'" + Session["status"] + "'" + ");</script>");
            Session["status"] = null;
        }
        if (Session["passwordChangeStatus"] != null)
        {
            Page.RegisterStartupScript("msg", "<script language='javascript'>alert(" + "'" + Session["passwordChangeStatus"] + "'" + ");</script>");
            Session["passwordChangeStatus"] = null;
        }
        if (!Page.IsPostBack)
        {
            BindPage();
        }
        BindURLs();
    }

    public void BindPage()
    {
        Cms cmsObj2 = new SuperAdminTaskManager().getCmsEntityOnType("StaticPages");
        CmsDescription c = null;
        if (Request.RawUrl.ToLower().Contains("meeting-rooms"))
        {
            c = mcs.GetCMSContent(cmsObj2.Id, Convert.ToInt64(Session["LanguageID"])).Where(a => a.ContentTitle.ToLower() == "meeting rooms").FirstOrDefault();
            if (c == null)
            {
                c = mcs.GetCMSContent(cmsObj2.Id, Convert.ToInt64("1")).Where(a => a.ContentTitle.ToLower() == "meeting rooms").FirstOrDefault();
            }
        }
        else if (Request.RawUrl.ToLower().Contains("conference-venues"))
        {
            c = mcs.GetCMSContent(cmsObj2.Id, Convert.ToInt64(Session["LanguageID"])).Where(a => a.ContentTitle.ToLower() == "conference venues").FirstOrDefault();
            if (c == null)
            {
                c = mcs.GetCMSContent(cmsObj2.Id, Convert.ToInt64("1")).Where(a => a.ContentTitle.ToLower() == "conference venues").FirstOrDefault();
            }
        }
        else if (Request.RawUrl.ToLower().Contains("conference-rooms"))
        {
            c = mcs.GetCMSContent(cmsObj2.Id, Convert.ToInt64(Session["LanguageID"])).Where(a => a.ContentTitle.ToLower() == "conference rooms").FirstOrDefault();
            if (c == null)
            {
                c = mcs.GetCMSContent(cmsObj2.Id, Convert.ToInt64("1")).Where(a => a.ContentTitle.ToLower() == "conference rooms").FirstOrDefault();
            }
        }
        else if (Request.RawUrl.ToLower().Contains("meeting-venues"))
        {
            c = mcs.GetCMSContent(cmsObj2.Id, Convert.ToInt64(Session["LanguageID"])).Where(a => a.ContentTitle.ToLower() == "meeting venues").FirstOrDefault();
            if (c == null)
            {
                c = mcs.GetCMSContent(cmsObj2.Id, Convert.ToInt64("1")).Where(a => a.ContentTitle.ToLower() == "meeting venues").FirstOrDefault();
            }
        }
        else if (Request.RawUrl.ToLower().Contains("meeting-space"))
        {
            c = mcs.GetCMSContent(cmsObj2.Id, Convert.ToInt64(Session["LanguageID"])).Where(a => a.ContentTitle.ToLower() == "meeting space").FirstOrDefault();
            if (c == null)
            {
                c = mcs.GetCMSContent(cmsObj2.Id, Convert.ToInt64("1")).Where(a => a.ContentTitle.ToLower() == "meeting space").FirstOrDefault();
            }
        }
        else if (Request.RawUrl.ToLower().Contains("meeting-hotels"))
        {
            c = mcs.GetCMSContent(cmsObj2.Id, Convert.ToInt64(Session["LanguageID"])).Where(a => a.ContentTitle.ToLower() == "meeting hotels").FirstOrDefault();
            if (c == null)
            {
                c = mcs.GetCMSContent(cmsObj2.Id, Convert.ToInt64("1")).Where(a => a.ContentTitle.ToLower() == "meeting hotels").FirstOrDefault();
            }
        }
        else if (Request.RawUrl.ToLower().Contains("conference-centre"))
        {
            c = mcs.GetCMSContent(cmsObj2.Id, Convert.ToInt64(Session["LanguageID"])).Where(a => a.ContentTitle.ToLower() == "conference centre").FirstOrDefault();
            if (c == null)
            {
                c = mcs.GetCMSContent(cmsObj2.Id, Convert.ToInt64("1")).Where(a => a.ContentTitle.ToLower() == "conference centre").FirstOrDefault();
            }
        }
        else if (Request.RawUrl.ToLower().Contains("conference-hotel"))
        {
            c = mcs.GetCMSContent(cmsObj2.Id, Convert.ToInt64(Session["LanguageID"])).Where(a => a.ContentTitle.ToLower() == "conference hotel").FirstOrDefault();
            if (c == null)
            {
                c = mcs.GetCMSContent(cmsObj2.Id, Convert.ToInt64("1")).Where(a => a.ContentTitle.ToLower() == "conference hotel").FirstOrDefault();
            }
        }
        else if (Request.RawUrl.ToLower().Contains("business-hotel"))
        {
            c = mcs.GetCMSContent(cmsObj2.Id, Convert.ToInt64(Session["LanguageID"])).Where(a => a.ContentTitle.ToLower() == "business hotel").FirstOrDefault();
            if (c == null)
            {
                c = mcs.GetCMSContent(cmsObj2.Id, Convert.ToInt64("1")).Where(a => a.ContentTitle.ToLower() == "business hotel").FirstOrDefault();
            }
        }
        else if (Request.RawUrl.ToLower().Contains("conference-hall"))
        {
            c = mcs.GetCMSContent(cmsObj2.Id, Convert.ToInt64(Session["LanguageID"])).Where(a => a.ContentTitle.ToLower() == "conference hall").FirstOrDefault();
            if (c == null)
            {
                c = mcs.GetCMSContent(cmsObj2.Id, Convert.ToInt64("1")).Where(a => a.ContentTitle.ToLower() == "conference hall").FirstOrDefault();
            }
        }
        if (c != null)
        {
            lblAboutUs.Text = "";
            //lblAboutUs.Text += "<h1>"+c.ContentShortDesc+"</h1><br/>";
            lblAboutUs.Text += c.ContentsDesc;
        }
    }

    public void AfterLogin(long userID, int userType)
    {
        beforeLogin.Style.Add("display", "none");
        afterLogin.Style.Add("display", "block");
        Session["registerType"] = userType;
        //hypManageProfile.NavigateUrl = "Registration.aspx";
    }

    protected void hypManageProfile_Click(object sender, EventArgs e)
    {
        Session["task"] = "Edit";
        ////Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "manage-profile/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "manage-profile/english");
        }
        //Response.Redirect("Registration.aspx");
    }
    protected void hypChangepassword_Click(object sender, EventArgs e)
    {
        ////Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "change-password/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "change-password/english");
        }
        //Response.Redirect("ChangePassword.aspx");
    }

    protected void hypListBookings_Click(object sender, EventArgs e)
    {
        ////Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "list-bookings/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "list-bookings/english");
        }
        //Response.Redirect("ListBookings.aspx");
    }

    protected void hypListRequests_Click(object sender, EventArgs e)
    {
        ////Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "list-requests/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "list-requests/english");
        }
        //Response.Redirect("ListRequests.aspx");
    }
}