﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Entities;
using LMMR.Business;

public partial class FrontEnd_activation : BasePage
{
    NewUser objNewUser = new NewUser();

    protected void Page_Load(object sender, EventArgs e)
    {
        long userId = Convert.ToInt64(Page.Request.QueryString["activate"]);
        Users getUser = objNewUser.GetUserByID(userId);
        if (getUser != null)
        {
            if (getUser.LastLogin == null)
            {
                getUser.IsActive = true;
                getUser.LastLogin = DateTime.Now;
                objNewUser.UpdateUser(getUser);
                Session["Active"] = "Y";
                ////Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "login/english");
                }
            }
            else
            {
                Session["Active"] = "ACTIVATION_MAIL_REUSE_DETECTED";
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "login/english");
                }
            }
        }
        else
        {

            Session["Active"] = "N";
            ////Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
            if (l != null)
            {
                Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
            }
            else
            {
                Response.Redirect(SiteRootPath + "login/english");
            }        
        }
     
    }
}