﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;

public partial class Default2 : BasePage
{
    ManageCMSContent obj = new ManageCMSContent();
    public void BindURLs()
    {
        ////Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            jointoday.HRef = SiteRootPath + "jointoday/" + l.Name.ToLower();
        }
        else
        {
            jointoday.HRef = SiteRootPath + "jointoday/english";
        }
         
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentRestUser"] != null)
        {
            Users objUsers = (Users)Session["CurrentRestUser"];
            lblLoginUserName.Text = objUsers.FirstName + " " + objUsers.LastName;
            objUsers.LastLogin = DateTime.Now;
            lstLoginTime.Text = DateTime.Now.ToLongDateString();
            AfterLogin(objUsers.UserId, Convert.ToInt32(objUsers.Usertype));
        }

        if (Session["status"] != null)
        {
            Page.RegisterStartupScript("msg", "<script language='javascript'>alert(" + "'" + Session["status"] + "'" + ");</script>");
            Session["status"] = null;
        }
        if (Session["passwordChangeStatus"] != null)
        {
            Page.RegisterStartupScript("msg", "<script language='javascript'>alert(" + "'" + Session["passwordChangeStatus"] + "'" + ");</script>");
            Session["passwordChangeStatus"] = null;
        }

        if (!IsPostBack)
        {
            GetAboutUsData();   
        }
        BindURLs();
    }

    public void AfterLogin(long userID, int userType)
    {
        beforeLogin.Style.Add("display", "none");
        afterLogin.Style.Add("display", "block");
        Session["registerType"] = userType;
        //hypManageProfile.NavigateUrl = "Registration.aspx";
    }

    public void GetAboutUsData()
    {
        Cms cmsObj = new SuperAdminTaskManager().getCmsEntityOnType("Aboutus");
        if (cmsObj != null)
        {
            TList<CmsDescription> getDesc = obj.GetCMSContent(cmsObj.Id, Convert.ToInt64(Session["LanguageID"]));
            if (getDesc.Count > 0)
            {
                lblAboutUs.Text = getDesc[0].ContentsDesc;

            }
            else
            {
                lblAboutUs.Text = "About Us Description Not Found";
            }
        }
        else
        {
            lblAboutUs.Text = "About Us Not Found";
        }
    }

    protected void hypManageProfile_Click(object sender, EventArgs e)
    {
        Session["task"] = "Edit";
        ////Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "manage-profile/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "manage-profile/english");
        }
        //Response.Redirect("Registration.aspx");
    }
    protected void hypChangepassword_Click(object sender, EventArgs e)
    {
        ////Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "change-password/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "change-password/english");
        }
        //Response.Redirect("ChangePassword.aspx");
    }

    protected void hypListBookings_Click(object sender, EventArgs e)
    {
        ////Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "list-bookings/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "list-bookings/english");
        }
        //Response.Redirect("ListBookings.aspx");
    }

    protected void hypListRequests_Click(object sender, EventArgs e)
    {
        ////Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "list-requests/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "list-requests/english");
        }
        //Response.Redirect("ListRequests.aspx");
    }
}