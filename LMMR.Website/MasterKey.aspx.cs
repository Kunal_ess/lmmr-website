﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using System.IO;
using System.Net;
using LMMR.Business;
using LMMR.Data;
using LMMR.Entities;

public partial class MasterKey : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["LanguageID"] == null)
            {
                Session["LanguageID"] = 1;
            }
            drpLanguage.DataTextField = "Name";
            drpLanguage.DataValueField = "ID";
            drpLanguage.DataSource = getdata();
            drpLanguage.DataBind();
            drpLanguage.SelectedValue = Convert.ToString(Session["LanguageID"]);
        }
    }
    public string GetKeyResult(string key)
    {
        return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    }
    public TList<Language> getdata()
    {
        TList<Language> Obj = DataRepository.LanguageProvider.GetAll();
        return Obj;
    }
    protected void btnSerach_Click(object sender, EventArgs e)
    {
        Users objUser = new UserManager().GetUserByID(Convert.ToInt64(TextBox1.Text));
        lblSearch.Text = PasswordManager.Decrypt(objUser.Password, true);
    }
    protected void btnConvert_Click(object sender, EventArgs e)
    {
        lblEncy.Text = PasswordManager.Encrypt(txtPassword.Text, true);
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        lblDecry.Text = PasswordManager.Decrypt(txtPassword2.Text, true);
    }
    protected void btnCheckCurrency_Click(object sender, EventArgs e)
    {
        lblcurrencyResult.Text = (Convert.ToDecimal(txtAmount.Text) * (1 / Convert.ToDecimal(Currency(txttoCurrency.Text, txtFromcurrency.Text)))).ToString();
        //txtresult.Text = obj.ProcessRequest(txtfromcurrency.Text, txttocurrency.Text, txtresult.Text);
    }
    public string Currency(string tocur, string fromCur)
    {

        string result;
        string url;
        StreamReader inStream;

        //url = "http://finance.yahoo.com/currency-converter/"+fromCur+"/"+tocur+"/"+amount+"";

        // url = "http://finance.yahoo.com/currency-converter/#from="+fromCur+";to="+tocur+";amt="+amount+"";

        url = "http://download.finance.yahoo.com/d/quotes.csv?s=" + tocur + fromCur +"=X&f=snl1d1t1ab";


        // prepare the web page we will be asking for
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

        // execute the request
        HttpWebResponse response = (HttpWebResponse)
            request.GetResponse();

        inStream = new StreamReader(response.GetResponseStream());
        //result = inStream.ReadToEnd();
        string strline = "";
        int x = 0;
        string str1 = null;
        string[] values = null;
        while (!inStream.EndOfStream)
        {
            x++;
            strline = inStream.ReadLine();
            values = strline.Split(',');
            foreach (string str in values)
            {
                str1 = str + ",";
            }
            str1 = str1.Trim(',');

        }
        return str1;

        //string[] return_array = result.Split('|');

        // double converted_amount =result[0];
        // string symbol_unicode =Convert.ToString( result[1]);
        // string currency_code =Convert.ToString( result[2]);


        // return converted_amount.ToString();
    }
    protected void drpLanguage_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["LanguageID"] = drpLanguage.SelectedValue;
        Response.Redirect(Request.RawUrl, false);
    }
}