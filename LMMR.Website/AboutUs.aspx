﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeMaster.master" AutoEventWireup="true"
    CodeFile="AboutUs.aspx.cs" Inherits="Default2" %>

<%@ Register Src="UserControl/Frontend/HotelOfTheWeek.ascx" TagName="HotelOfTheWeek"
    TagPrefix="uc1" %>
<%@ Register Src="UserControl/Frontend/Benefits.ascx" TagName="Benefits" TagPrefix="uc2" %>
<%@ Register Src="UserControl/Frontend/RecentlyJoinedHotel.ascx" TagName="RecentlyJoinedHotel"
    TagPrefix="uc3" %>
<%@ Register Src="UserControl/Frontend/NewsSubscriber.ascx" TagName="NewsSubscriber"
    TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntLeft" runat="Server">
    <uc1:HotelOfTheWeek ID="HotelOfTheWeek2" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMainbody" runat="Server">
    <div class="mainbody-left">
        <div class="banner">
            <iframe height="100%" src="<%= SiteRootPath %>SlideShow.aspx" width="100%"></iframe>
        </div>
        <div class="abutus-text">
                <asp:Label runat="server" ID="lblAboutUs" ></asp:Label>
        </div>
    </div>
    <div class="mainbody-right">
        <div id="beforeLogin" runat="server">
            <div class="mainbody-right-call">
                <div class="need">
                    <span>
                        <%= GetKeyResult("NEEDHELP")%></span>
                    <br />
                    <%= GetKeyResult("CALLUS")%>
                    5/7
                </div>
                <div class="phone">
                    +32 2 344 25 50
                </div>
                <p style="font-size: 10px; color: White; text-align: center">
                    <%= GetKeyResult("CALLUSANDWEDOITFORYOU")%></p>
                <div class="mail">
                    <a href="http://needameetingroom.com/requestform/" onClick="javascript: _gaq.push(['_trackPageview', '/email']); window.open(this.href, 'popupwindow1','width=900,height=600,scrollbars,resizable,toolbar=NO'); &#13;&#10;return false;" Target="_blank"">
                        Send us your RFP</a>
                </div>
            </div>
            <div class="mainbody-right-join" id="divjoinus" runat="server">
                <div class="join">
                    <a id="jointoday" runat="server">
                        <%= GetKeyResult("JOINUSTODAY")%></a></div>
                <%= GetKeyResult("FORHOTELSMEETINGFACILITIESEVENT")%>
            </div>
            <div class="mainbody-right-you">
                <a href="http://www.youtube.com/watch?v=rkt3NIWecG8&feature=youtu.be" target="_blank">
                    <img src="<%= SiteRootPath %>images/youtube-icon.png" /></a>&nbsp;&nbsp;<a href="http://www.youtube.com/watch?v=U3y7d7yxmvE&feature=relmfu"
                        target="_blank"><img src="<%= SiteRootPath %>images/small-vedio.png" /></a>
            </div>
        </div>
        <div style="width: 216px; height: auto; overflow: hidden; margin: 0px auto; display: none"
            id="afterLogin" runat="server">
            <div class="mainbody-right-call">
                <div class="need">
                    <span>
                        <%= GetKeyResult("NEEDHELP")%></span>
                    <br />
                    <%= GetKeyResult("CALLUS")%>
                    5/7
                </div>
                <div class="phone">
                    +32 2 344 25 50
                </div>
                <p style="font-size: 10px; color: White; text-align: center">
                    <%= GetKeyResult("CALLUSANDWEDOITFORYOU")%></p>
                <div class="mail">
                    <a href="http://needameetingroom.com/requestform/" onClick="javascript: _gaq.push(['_trackPageview', '/email']); window.open(this.href, 'popupwindow1','width=900,height=600,scrollbars,resizable,toolbar=NO'); &#13;&#10;return false;" Target="_blank"">
                        Send us your RFP</a>
                </div>
            </div>
            <!--start mainbody-right-afterlogin-body -->
            <div class="mainbody-right-afterlogin-body">
                <div class="mainbody-right-afterlogin-top">
                    <div class="mainbody-right-afterlogin-inner">
                        <b>
                            <%= GetKeyResult("TODAY")%>:</b>
                        <asp:Label ID="lstLoginTime" runat="server"></asp:Label>
                        <div class="afterlogin-welcome">
                            <span>
                                <%= GetKeyResult("WELCOME")%></span>
                            <br>
                            <asp:Label ID="lblLoginUserName" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="mainbody-right-afterlogin-bottom">
                    <div class="mainbody-right-afterlogin-bottom-inner">
                        <asp:LinkButton ID="hypManageProfile" runat="server" OnClick="hypManageProfile_Click"><%= GetKeyResult("MANAGEPROFILE")%></asp:LinkButton>
                        <br>
                        <asp:LinkButton ID="hypChangepassword" runat="server" OnClick="hypChangepassword_Click"><%= GetKeyResult("CHANGEPASSWORD")%></asp:LinkButton><br />
                        <asp:LinkButton ID="hypListBookings" runat="server" OnClick="hypListBookings_Click"
                            Visible="true"><%= GetKeyResult("MYBOOKINGS")%></asp:LinkButton><br />
                        <asp:LinkButton ID="hypListRequests" runat="server" OnClick="hypListRequests_Click"
                            Visible="true"><%= GetKeyResult("MYREQUESTS")%></asp:LinkButton>
                    </div>
                </div>
            </div>
            <!--end mainbody-right-afterlogin-body -->
        </div>
        <div class="mainbody-right-news">
            <uc4:NewsSubscriber ID="NewsSubscriber1" runat="server" />
        </div>
        <uc3:RecentlyJoinedHotel ID="RecentlyJoinedHotel1" runat="server" />
    </div>
    <div class="mainbody">
        <div class="mainbody-left">
        </div>
    </div>
</asp:Content>
