﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Home" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntLeftSearch" Runat="Server">
 
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cntLeftBottom" Runat="Server">
    <div class="left-inner-bottom"> 
            <h2>Meeting specials</h2>
            <ul>
            <li>
            <div class="left"><img src="<%= SiteRootPath %>images/leftimage.png" width="57" height="57" /></div>
            <div class="right">
            <p>Adipiscing elit. Morbi ac sapien erat, nec semper mauris.</p>
            <p class="date">Valid untill 01/06/2012</p>            
            </div>            
            </li>
            <li>
            <div class="left"><img src="<%= SiteRootPath %>images/leftimage.png" width="57" height="57" /></div>
            <div class="right">
            <p>Adipiscing elit. Morbi ac sapien erat, nec semper mauris.</p>
            <p class="date">Valid untill 01/06/2012</p>            
            </div>            
            </li>
             <li>
            <div class="left"><img src="<%= SiteRootPath %>images/leftimage.png" width="57" height="57" /></div>
            <div class="right">
            <p>Adipiscing elit. Morbi ac sapien erat, nec semper mauris.</p>
            <p class="date">Valid untill 01/06/2012</p>            
            </div>            
            </li>
            <li>
            <div class="left"><img src="<%= SiteRootPath %>images/leftimage.png" width="57" height="57" /></div>
            <div class="right">
            <p>Adipiscing elit. Morbi ac sapien erat, nec semper mauris.</p>
            <p class="date">Valid untill 01/06/2012</p>            
            </div>            
            </li>
            </ul>
            </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cntTopNavigation" Runat="Server">
    <div class="nav">        
 <!--Main navigation start -->
					<div id="subnavbar">
					  <ul id="subnav">
						<li class="cat-item"><a href="#">Home</a></li>
						<li class="cat-item"><a href="#">About us</a></li>
                        <li class="cat-item"><a href="#">Contact us</a></li>
                        <li class="cat-item"><a href="#">Join today</a></li>
                        <li class="cat-item"><a href="Login.aspx">Login</a></li>
                        					
					  </ul>
					</div>
					<!--main navigation end -->
            </div>
            <div class="nav-form">        
            
                <div id="Euro">
                <select name="test">
                <option>Euro</option>
                <option value="Euro1">Euro1</option>
                <option value="Euro2">Euro2</option>
                <option value="Euro3">Euro3</option>
                </select>
                </div>
                
                <div id="English">
                <select name="test">
                <option>English</option>
                <option value="English1">English1</option>
                <option value="English2">English2</option>
                <option value="English3">English3</option>
                </select>
                </div>

            </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cntMainBody" Runat="Server">
    <div class="mainbody"> 
        	<!--mainbody-left START HERE-->
            <div class="mainbody-left">       
            <!--banner START HERE-->
            <div class="banner">        
            <img src="<%= SiteRootPath %>images/banner.png"/>
            </div>
            <div class="video">        
                <div class="videoleftbody">
                     <div class="videobody">      
                     <img src="<%= SiteRootPath %>images/con-img1.png"/>
                     </div>
                     <div class="video-button">      
                     <a href="#"><img src="<%= SiteRootPath %>images/for-guest.png"/></a>
                     </div>
                </div>
                <div class="videorightbody"> 
                     <div class="videobody">          
                     <img src="<%= SiteRootPath %>images/con-img2.png"/>
                     </div>
                     <div class="video-button">      
                     <a href="#"><img src="<%= SiteRootPath %>images/for-hotels.png"/></a>
                     </div>
                </div>
            </div>
            <!--banner ENDS HERE-->
            <div class="mid">        
                <div class="left">        
                <h2>How to book online?</h2>
                <p>Aenean porta, tellus vel accumsan fermentum, diam arcu condimentum purus, hendrerit posuere purus mauris nec urna. Nam id elit dolor. </p>
                <p><a href="#">Read more</a></p>
                </div>
                <div class="right">        
                <h2>How to send a request?</h2>
                <p>Vivamus et felis felis, nec mattis diam. Sed luctus mollis nisi, sed blandit urna pulvinar ut. Integer male suada vesti bulum erat, at posuere mauris. </p>
                <p><a href="#">Read more</a></p>
                </div>
            </div>
            <!--bottom ENDS HERE-->
            </div>
            <!--mainbody-left ENDS HERE-->
            <!--mainbody-right START HERE-->
            <div class="mainbody-right"> 
            	<!--mainbody-right-call START HERE-->
                <div class="mainbody-right-call"> 
                    <div class="need">  
                    <span>Need help?</span>
                    <br />
                    Call us 5/7                    
                    </div> 
                    <div class="phone"> 
                   +32 2 344 25 50 </div>
                    <p style="font-size:10px;color:White;text-align:center">  <%= GetKeyResult("CALLUSANDWEDOITFORYOU")%></p>
                    <div class="mail"> 
                    <a href="#">Contact us via mail</a>
                    </div>                
                </div> 
                <!--mainbody-right-call ENDS HERE-->
                <!--mainbody-right-join START HERE-->
                <div class="mainbody-right-join">
                <div class="join"><a href="#">Join us today !</a></div>
                (For Hotels, Meeting facilities, Event locations)
                </div>
                <!--mainbody-right-join ENDS HERE-->
                <!--mainbody-right-you START HERE-->
                <div class="mainbody-right-you">
				<img src="<%= SiteRootPath %>images/youtube.png" />
                </div>
                <!--mainbody-right-you ENDS HERE-->
                <!--mainbody-right-news START HERE-->
                <div class="mainbody-right-news">
                <div class="mainbody-right-news-top"></div>
                    <div class="mainbody-right-news-mid">
                        <div class="mainbody-right-news-inner">
                        <h2>News</h2>
                        <ul>
                        <li>
                        <div><a href="#">Morbi ac sapien erat, nec semper mauris. Donec quis malesuada est. Nullam aliquet ultricies.</a></div>
                        <div class="date">September 24 - 2011</div>                        
                        </li>
                        <li>
                        <div><a href="#">Morbi ac sapien erat, nec semper mauris. Donec quis malesuada est. Nullam aliquet ultricies.</a></div>
                        <div class="date">September 24 - 2011</div>                        
                        </li>
                        </ul>
                        <div class="subsribe-body"><input type="text" value="Enter your email"  class="inputbox" onclick="this.value=''" /> <input type="button"  value="Subsribe" class="subsribe" /></div>
                        </div>
                    </div>
                <div class="mainbody-right-news-bottom"></div>
                </div>
                <!--mainbody-right-news ENDS HERE-->
                
                <!--mainbody-right-recently-joined START HERE-->
                <div class="mainbody-right-recently-joined">
                <h2>Hotels recently joined</h2>
                <ul>
                <li>
                <div><a href="#">Morbi ac sapien erat, nec sem</a></div>
                <div class="date">Joined today</div>                        
                </li>
                <li>
                <div><a href="#">Semper mauris. </a></div>
                <div class="date">Joined today</div>                        
                </li>
                <li>
                <div><a href="#">Donec quis malesuada est.</a></div>
                <div class="date">Joined yesterday</div>                        
                </li>
                <li>
                <div><a href="#">Nullam aliquet ultricies purus </a></div>
                <div class="date">Joined August 13 2011</div>                        
                </li>
                <li>
                <div><a href="#">At vulputate. </a></div>
                <div class="date">Joined August 13 2011</div>                        
                </li>
                </ul>
                </div>
                <!--mainbody-right-recently-joined ENDS HERE-->
                
                
                
                
                
                            
            </div>
            <!--mainbody-right ENDS HERE-->
        </div>
       

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cntMediaLink" Runat="Server">
    <div class="footer-menu"> 
        <ul>
        <li><a href="#">Company Information</a></li>
        <li><a href="#">For Investors</a></li>
        <li><a href="#">Privacy statement</a></li>
        <li><a href="#">Legal aspects</a></li>
        <li><a href="#">Terms and conditions</a></li>
        <li><a href="#">Join Us</a></li>
        </ul>
        </div>
        <div class="footer-icon"> 
        <a href="#" class="face">&nbsp;</a><a href="#" class="twitter">&nbsp;</a><a href="#" class="google">&nbsp;</a><a href="#" class="linked">&nbsp;</a><a href="#" class="you">&nbsp;</a>
        </div>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cntFotter" Runat="Server">
    <ul>
         <li><a href="#">Lorem ipsum dolor sit amet,</a></li>
         <li><a href="#">consectetur adipiscing elit.</a></li>
         <li><a href="#">Morbi ac sapien erat, nec semper</a></li>
         <li><a href="#">Donec quis malesuada est. Nullam </a></li>
         <li><a href="#">ultricies purus at vulputate. Duis</a></li>
         <li><a href="#">egestas bibendum sapien. Fusce </a></li>
         <li><a href="#">turpis eget dui iaculis non </a></li>
         <li><a href="#">ultricies purus at vulputate. Duis</a></li>
         <li><a href="#">Lorem ipsum dolor sit amet,</a></li>
         <li><a href="#">consectetur adipiscing elit.</a></li>
         <li><a href="#">Morbi ac sapien erat, nec semper</a></li>
         <li><a href="#">Donec quis malesuada est. Nullam </a></li>
         <li><a href="#">ultricies purus at vulputate. Duis</a></li>
         <li><a href="#">egestas bibendum sapien. Fusce </a></li>
         <li><a href="#">turpis eget dui iaculis non </a></li>
         <li><a href="#">ultricies purus at vulputate. Duis</a></li>
         <li><a href="#">Lorem ipsum dolor sit amet,</a></li>
         <li><a href="#">consectetur adipiscing elit.</a></li>
         <li><a href="#">Morbi ac sapien erat, nec semper</a></li>
         <li><a href="#">Donec quis malesuada est. Nullam </a></li>
         <li><a href="#">ultricies purus at vulputate. Duis</a></li>
         <li><a href="#">egestas bibendum sapien. Fusce </a></li>
         <li><a href="#">turpis eget dui iaculis non </a></li>
         <li><a href="#">ultricies purus at vulputate. Duis</a></li>
         <li><a href="#">Lorem ipsum dolor sit amet,</a></li>
         <li><a href="#">consectetur adipiscing elit.</a></li>
         <li><a href="#">Morbi ac sapien erat, nec semper</a></li>
         <li><a href="#">Donec quis malesuada est. Nullam </a></li>
         <li><a href="#">ultricies purus at vulputate. Duis</a></li>
         <li><a href="#">egestas bibendum sapien. Fusce </a></li>
         <li><a href="#">turpis eget dui iaculis non </a></li>
         <li><a href="#">ultricies purus at vulputate. Duis</a></li>
         </ul>
</asp:Content>

