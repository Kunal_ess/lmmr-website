﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GoogleMapMultiColor.aspx.cs" Inherits="Test_GoogleMapMultiColor" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Google Map By Pranayesh</title>
    <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="map" style="width: 700px; height: 600px;">
    </div>
    <script type="text/javascript">
        var locations = [
      ['Bondi Beach', -33.890542, 151.274856, 4, 'Special'],
      ['Coogee Beach', -33.923036, 151.259052, 3, 'Special'],
      ['Cronulla Beach', -34.028249, 151.157507, 5, 'Center'],
      ['Manly Beach', -33.80010128657071, 151.28747820854187, 2, 'Special'],
      ['Maroubra Beach', -33.950198, 151.259302, 1, 'No']
    ];
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            //center: new google.maps.LatLng(-33.92, 151.25),
            center: new google.maps.LatLng(-34.028249, 151.157507),
            mapTypeId: google.maps.MapTypeId.ROADMAP

        });

        var infowindow = new google.maps.InfoWindow();

        var marker, i;
        var image;
        for (i = 0; i < locations.length; i++) {
            if (locations[i][4] == 'Special' && locations[i][4] != 'Center') {
                image = new google.maps.MarkerImage('http://maps.google.com/mapfiles/ms/icons/green-dot.png');
            }
            else if (locations[i][4] == 'No' && locations[i][4] != 'Center') {
                image = new google.maps.MarkerImage('http://maps.google.com/mapfiles/ms/icons/pink-dot.png');
            }
            else {
                image = new google.maps.MarkerImage('http://maps.google.com/mapfiles/ms/icons/blue-dot.png');
            }

            var shadow = new google.maps.MarkerImage('../Images/shadow50.png',
            // The shadow image is larger in the horizontal dimension
            // while the position and offset are the same as for the main image.
             new google.maps.Size(37, 34),
             new google.maps.Point(10, 5),
             new google.maps.Point(0, 34));


            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map,
                shadow: shadow,
                icon: image
            });

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));

        }
        // Add circle overlay and bind to marker
        var circle = new google.maps.Circle({
            map: map,
            radius: 16093,    // 10 miles in metres
            fillColor: '#AA0000'
        });
        circle.bindTo('center', marker, 'position');

    </script>
    </form>
</body>
</html>
