﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Entities;
using LMMR.Business;

public partial class Policy : System.Web.UI.Page
{

    CancellationPolicy objCancellation = new CancellationPolicy();
    HotelManager objHotelManager = new HotelManager();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["hid"] != null)
            {
                BindPolicy(Convert.ToInt32(Request.QueryString["hid"]));
            }
        }
    }

    public void BindPolicy(Int32 HotelID)
    {
        long? PolicyID = 0;
        PolicyHotelMapping objMapping = objCancellation.GetPolicyByHotelID(Convert.ToInt32(HotelID)).FirstOrDefault();
        if (objMapping != null)
        {
            PolicyID = objMapping.PolicyId;
        }
        else
        {
            int countryID = Convert.ToInt32(objHotelManager.GetHotelDetailsById(Convert.ToInt32(HotelID)).CountryId);
            CancelationPolicy objPolicy = objCancellation.GetPolicyByCountry(countryID).Find(a => a.DefaultCountry == true);
            PolicyID = objPolicy.Id;
        }

        CancelationPolicy GetPolicyByID = objCancellation.GetPolicyByID(Convert.ToInt32(PolicyID));
        if (GetPolicyByID != null)
        {
            int languageID = 0;
            if (Session["LanguageID"] == null)
            {
                languageID = Convert.ToInt32(objCancellation.GetLanguage().Find(a => a.Name == "English").Id);
            }
            else
            {
                languageID = Convert.ToInt32(Session["LanguageID"]);
            }
            TList<CancelationPolicyLanguage> objCancellationPolicy = objCancellation.GetPolicySectionsByPolicyID(Convert.ToInt32(PolicyID));
            CancelationPolicyLanguage objLanguage = objCancellationPolicy.Find(a => a.LanguageId == languageID);
            if (objLanguage == null)
            {
                languageID = Convert.ToInt32(objCancellation.GetLanguage().Find(a => a.Name == "English").Id);
                objLanguage = objCancellationPolicy.Find(a => a.LanguageId == languageID);
            }
            lblName.Text = GetPolicyByID.PolicyName;
            lblCalcellationLimit.Text = GetPolicyByID.CancelationLimit.ToString();
            lblTimeFrameMax1.Text = GetPolicyByID.TimeFrameMax1.ToString();
            lblTimeFrameMax2.Text = GetPolicyByID.TimeFrameMax2.ToString();
            lblTimeFrameMax3.Text = GetPolicyByID.TimeFrameMax3.ToString();
            lblTimeFrameMax4.Text = GetPolicyByID.TimeFramMax4.ToString();
            lblTimeFrameMin1.Text = GetPolicyByID.TimeFrameMin1.ToString();
            lblTimeFrameMin2.Text = GetPolicyByID.TimeFrameMin2.ToString();
            lblTimeFrameMin3.Text = GetPolicyByID.TimeFrameMin3.ToString();
            lblTimeFrameMin4.Text = GetPolicyByID.TimeFrameMin4.ToString();

            lblSection1.Text = objLanguage.PolicySection1;
            lblSection2.Text = objLanguage.PolicySection2;
            lblSection3.Text = objLanguage.PolicySection3;
            lblSection4.Text = objLanguage.PolicySection4;


        }
        else
        {
            // divpolicy.Style.Add("display", "none");
            //divMessage.Style.Add("display", "block");
        }

    }
}