﻿#region NameSpaces

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using log4net;
using log4net.Config;
using System.IO;
using System.Configuration;
using LMMR.Data;
using System.Globalization;



#endregion

public partial class SurveyDetails : System.Web.UI.Page
{
    #region variable declaration
    Surveydata obj = new Surveydata();
    surveybookingrequest objsurveybookingrequest = new surveybookingrequest();
    ServeyAnswer insertsurvey = new ServeyAnswer();
    TList<ServeyAnswer> objsurvey = new TList<ServeyAnswer>();
    ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
    string status;

    bool status1;
    #endregion
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            divmessage.Visible = false;
            lbl1.Text = objsurveybookingrequest.GetSurveyNameByid(1);
            lbl2.Text = objsurveybookingrequest.GetSurveyNameByid(2);
            lbl3.Text = objsurveybookingrequest.GetSurveyNameByid(3);
            lbl4.Text = objsurveybookingrequest.GetSurveyNameByid(4);
            lbl5.Text = objsurveybookingrequest.GetSurveyNameByid(5);
            lbl6.Text = objsurveybookingrequest.GetSurveyNameByid(6);
            lbl7.Text = objsurveybookingrequest.GetSurveyNameByid(7);
            lbl8.Text = objsurveybookingrequest.GetSurveyNameByid(8);
            lbl9.Text = objsurveybookingrequest.GetSurveyNameByid(9);
            lbl10.Text = objsurveybookingrequest.GetSurveyNameByid(10);
            lblcomments.Text  = objsurveybookingrequest.GetSurveyNameByid(11);
        }

        
    }
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl + "/";
        }
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {

        BindRatings();
        
        if (status == "Thank You For The Survey")
        {
            divsurvey.Visible = true;

            divmessage.Visible = true;
            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "succesfuly");
            divmessage.InnerHtml = status;


        }
        else if (status == "Information could not be saved.")
        {
            divsurvey.Visible = true;
            divmessage.Visible = true;
            divmessage.Attributes.Add("class", "error");
            divmessage.Style.Add("display", "block");
            divmessage.InnerHtml = status;

        }
        else
        {
            divsurvey.Visible = true;
            divmessage.Visible = true;
            divmessage.Attributes.Add("class", "error");
            divmessage.Style.Add("display", "block");
            divmessage.InnerHtml = status;
        }
    }

    public void BindRatings()
    {
        int Userid = Convert.ToInt32(Request.QueryString["UserID"]);
        int venueid = Convert.ToInt32(Request.QueryString["venue"]);
        int bookingID = Convert.ToInt32(Request.QueryString["bookingID"]);
        Booking onjbooking = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(bookingID));
        if (onjbooking.IsSurveyDone == false)
        {
            onjbooking.IsSurveyDone = true;
            status1 = DataRepository.BookingProvider.Update(onjbooking);
            if (status1 == true)
            {
                Serveyresult res = new Serveyresult();
                ServeyResponse response = new ServeyResponse();
                response.UserId = Userid;
                response.VanueId = venueid;
                response.BookingId = bookingID;

                response.BookingType = Convert.ToInt32(onjbooking.BookType);
                if (commentsText.Text != "")
                {

                    response.AdditionalComment = commentsText.Text;
                    status = obj.insertSurvey(response);
                    if (status == "Thank You For The Survey")
                    {
                        res.QuestionId = 4;
                        res.AnswerId = 11;
                        res.ServeyId = response.ServeyId;
                        status = obj.insertSurvey(res);
                    }
                    else
                    {
                        divsurvey.Visible = true;
                        divmessage.Visible = true;
                        divmessage.Attributes.Add("class", "error");
                        divmessage.Style.Add("display", "block");
                        divmessage.InnerHtml = status;
                    }


                }
                else
                {
                    status = obj.insertSurvey(response);
                }
                if (status == "Thank You For The Survey")
                {

                    if (rbt_State_Venue.SelectedValue != "" || rbt_expectations.SelectedValue != "" || rbt_convenience.SelectedValue != "")
                    {

                        if (rbt_State_Venue.SelectedValue != "")
                        {

                            res.QuestionId = 1;
                            res.AnswerId = 1;
                            res.Rating = Convert.ToInt32(rbt_State_Venue.SelectedValue);
                            res.ServeyId = response.ServeyId;
                            status = obj.insertSurvey(res);

                        }


                        if (rbt_expectations.SelectedValue != "")
                        {

                            res.QuestionId = 1;
                            res.AnswerId = 2;
                            res.Rating = Convert.ToInt32(rbt_expectations.SelectedValue);
                            res.ServeyId = response.ServeyId;
                            status = obj.insertSurvey(res);


                        }


                        if (rbt_convenience.SelectedValue != "")
                        {

                            res.QuestionId = 1;
                            res.AnswerId = 3;
                            res.Rating = Convert.ToInt32(rbt_convenience.SelectedValue);
                            res.ServeyId = response.ServeyId;
                            status = obj.insertSurvey(res);


                        }


                    }

                    if (rbt_Availability_staff.SelectedValue != "" || rbt_Staff_Kindness.SelectedValue != "" || rbt_Staff_Professionalism.SelectedValue != "")
                    {
                        if (rbt_Availability_staff.SelectedValue != "")
                        {

                            res.QuestionId = 2;
                            res.AnswerId = 4;
                            res.Rating = Convert.ToInt32(rbt_Availability_staff.SelectedValue);
                            res.ServeyId = response.ServeyId;
                            status = obj.insertSurvey(res);

                        }


                        if (rbt_Staff_Kindness.SelectedValue != "")
                        {

                            res.QuestionId = 2;
                            res.AnswerId = 5;
                            res.Rating = Convert.ToInt32(rbt_Staff_Kindness.SelectedValue);
                            res.ServeyId = response.ServeyId;
                            status = obj.insertSurvey(res);

                        }
                        else
                        {
                            //Response.Write("Staff Kindness Did Not Rate");
                            //Response.Write("<br/>");
                        }

                        if (rbt_Staff_Professionalism.SelectedValue != "")
                        {

                            res.QuestionId = 2;
                            res.AnswerId = 6;
                            res.Rating = Convert.ToInt32(rbt_Staff_Professionalism.SelectedValue);
                            res.ServeyId = response.ServeyId;
                            status = obj.insertSurvey(res);

                        }
                        else
                        {
                            //Response.Write("Staff Professionalism Did Not Rate");
                            //Response.Write("<br/>");
                        }

                    }
                    else
                    {
                        //Response.Write("Your satisfaction regarding service Did Not Rate");
                        //Response.Write("<br/>");

                    }
                    if (rbt_Quality_of_food.SelectedValue != "" || rbt_Quality_of_coffee_breaks.SelectedValue != "" || rbt_Original_and_Creative.SelectedValue != "" || rbt_service_speed.SelectedValue != "")
                    {
                        if (rbt_Quality_of_food.SelectedValue != "")
                        {

                            res.QuestionId = 3;
                            res.AnswerId = 7;
                            res.Rating = Convert.ToInt32(rbt_Quality_of_food.SelectedValue);
                            res.ServeyId = response.ServeyId;
                            status = obj.insertSurvey(res);

                        }
                        else
                        {
                            //Response.Write("Quality Of Food Did Not Rate");
                            //Response.Write("<br/>");
                        }

                        if (rbt_Quality_of_coffee_breaks.SelectedValue != "")
                        {

                            res.QuestionId = 3;
                            res.AnswerId = 8;
                            res.Rating = Convert.ToInt32(rbt_Quality_of_coffee_breaks.SelectedValue);
                            res.ServeyId = response.ServeyId;
                            status = obj.insertSurvey(res);

                        }
                        else
                        {
                            //Response.Write("Quality Of Cofee Breaks Did Not Rate");
                            //Response.Write("<br/>");
                        }

                        if (rbt_Original_and_Creative.SelectedValue != "")
                        {

                            res.QuestionId = 3;
                            res.AnswerId = 9;
                            res.Rating = Convert.ToInt32(rbt_Original_and_Creative.SelectedValue);
                            res.ServeyId = response.ServeyId;
                            status = obj.insertSurvey(res);

                        }
                        else
                        {
                            //Response.Write("Orginal Or Creativity  Did Not Rate");
                            //Response.Write("<br/>");
                        }
                        if (rbt_service_speed.SelectedValue != "")
                        {

                            res.QuestionId = 3;
                            res.AnswerId = 10;
                            res.Rating = Convert.ToInt32(rbt_service_speed.SelectedValue);
                            res.ServeyId = response.ServeyId;
                            status = obj.insertSurvey(res);

                        }
                    }
                    
                }
                else
                {
                    divsurvey.Visible = true;
                    divmessage.Visible = true;
                    divmessage.Attributes.Add("class", "error");
                    divmessage.Style.Add("display", "block");
                    divmessage.InnerHtml = status;
                }
            }
            else
            {
                divsurvey.Visible = true;
                divmessage.Visible = true;
                divmessage.Attributes.Add("class", "error");
                divmessage.Style.Add("display", "block");
                divmessage.InnerHtml = "Survey did not Save!";
            }

        }
        else
        {
            status = "Survey already done for the Hotel";
            divsurvey.Visible = true;
            divmessage.Visible = true;
            divmessage.Attributes.Add("class", "error");
            divmessage.Style.Add("display", "block");
            divmessage.InnerHtml = status;
        }
    }
}