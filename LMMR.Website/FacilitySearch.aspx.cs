﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using log4net;
using log4net.Config;
#endregion

public partial class FacilitySearch : System.Web.UI.Page
{
    #region variable declaration
    Facilities objFaclt = new Facilities();
    int hotelPid = 0;
    WizardLinkSettingManager Objwizard = new WizardLinkSettingManager();
    DashboardManager Objdash = new DashboardManager();
    HotelManager objHotelManager = new HotelManager();
    HotelInfo ObjHotelinfo = new HotelInfo();
    ILog logger = log4net.LogManager.GetLogger(typeof(FacilitySearch));
    #endregion

    #region PageLoad
    /// <summary>
    /// On page load we manage all the containts and Apply paging if there is a post back.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {            
            if (!IsPostBack)
            {
                if (Convert.ToString(Request.QueryString.Get("HotelId")) != null)
                {
                    hotelPid = Convert.ToInt32(Request.QueryString.Get("HotelId"));
                    //Call function to bind all selected facilities
                    InsertStatistics(hotelPid);
                    BindAllStaticFacilities();
                }                               
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Additional Methods   
    /// <summary>
    //Bind all facilities by FacilityTypeId
    /// </summary>   
    public void BindAllStaticFacilities()
    {
        try
        {
            chkGeneral.DataValueField = "Id";
            chkGeneral.DataTextField = "FacilityName";
            chkGeneral.DataSource = objFaclt.GetFacilityGnl();
            chkGeneral.DataBind();
            if (objFaclt.GetFacilityGnl().Count > 0)
            {
                for (int i = 0; i < chkGeneral.Items.Count; i++)
                {
                    TList<HotelFacilities> htlfac = objFaclt.GetFacilitiesByHotelID(Convert.ToInt32(chkGeneral.Items[i].Value), hotelPid);
                    if (htlfac.Count > 0)
                    {
                        chkGeneral.Items[i].Selected = true;
                    }

                }
            }
            chkActivities.DataValueField = "Id";
            chkActivities.DataTextField = "FacilityName";
            chkActivities.DataSource = objFaclt.GetFacilityAct();
            chkActivities.DataBind();
            if (objFaclt.GetFacilityAct().Count > 0)
            {
                for (int i = 0; i < chkActivities.Items.Count; i++)
                {
                    TList<HotelFacilities> htlfac = objFaclt.GetFacilitiesByHotelID(Convert.ToInt32(chkActivities.Items[i].Value), hotelPid);
                    if (htlfac.Count > 0)
                    {
                        chkActivities.Items[i].Selected = true;
                    }

                }
            }

            chkServices.DataValueField = "Id";
            chkServices.DataTextField = "FacilityName";
            chkServices.DataSource = objFaclt.GetFacilitySer();
            chkServices.DataBind();
            if (objFaclt.GetFacilitySer().Count > 0)
            {
                for (int i = 0; i < chkServices.Items.Count; i++)
                {
                    TList<HotelFacilities> htlfac = objFaclt.GetFacilitiesByHotelID(Convert.ToInt32(chkServices.Items[i].Value), hotelPid);
                    if (htlfac.Count > 0)
                    {
                        chkServices.Items[i].Selected = true;
                    }
                }
            }


            chkMeeting.DataValueField = "Id";
            chkMeeting.DataTextField = "FacilityName";
            chkMeeting.DataSource = objFaclt.GetFacilityMeeting();
            chkMeeting.DataBind();
            if (objFaclt.GetFacilityMeeting().Count > 0)
            {
                for (int i = 0; i < chkMeeting.Items.Count; i++)
                {
                    TList<HotelFacilities> htlfac = objFaclt.GetFacilitiesByHotelID(Convert.ToInt32(chkMeeting.Items[i].Value), hotelPid);
                    if (htlfac.Count > 0)
                    {
                        chkMeeting.Items[i].Selected = true;
                    }
                }
            }


            chkBedrooms.DataValueField = "Id";
            chkBedrooms.DataTextField = "FacilityName";
            chkBedrooms.DataSource = objFaclt.GetFacilityBedroom();
            chkBedrooms.DataBind();
            if (objFaclt.GetFacilityBedroom().Count > 0)
            {
                for (int i = 0; i < chkBedrooms.Items.Count; i++)
                {
                    TList<HotelFacilities> htlfac = objFaclt.GetFacilitiesByHotelID(Convert.ToInt32(chkBedrooms.Items[i].Value), hotelPid);
                    if (htlfac.Count > 0)
                    {
                        chkBedrooms.Items[i].Selected = true;
                    }
                }
            }

        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }   
    #endregion   

    #region statistics
    public void InsertStatistics(int HotelId)
    {
        Viewstatistics objview = new Viewstatistics();
        Statistics ST = new Statistics();
        ST.HotelId = HotelId;
        ST.Views = 1;
        ST.StatDate = System.DateTime.Now;
        objview.InsertVisitwithBookingid(ST);
    }
    #endregion
}