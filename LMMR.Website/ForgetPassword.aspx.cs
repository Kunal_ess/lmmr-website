﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using log4net;
using log4net.Config;
#endregion


public partial class ForgetPassword : BasePage
{
    #region Variables and Properties
    PasswordManager objPasswordManager = new PasswordManager();
    UserManager objUserManager = new UserManager();
    #endregion

    #region PageLoad
    /// <summary>
    /// This is a page load event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            divError.Attributes.Add("class", "Error");
            divError.Style.Add("display", "none");
            //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
            if (l != null)
            {
                btnReset.PostBackUrl = SiteRootPath + "login/" + l.Name.ToLower();
            }
            else
            {
                btnReset.PostBackUrl = SiteRootPath + "login/english";
            }
        }
    }
    #endregion

    /// <summary>
    /// This method is passing the key value
    /// </summary>
    /// <param name="Key"></param>
    /// <returns></returns>
    public string GetKeyResult(string key)
    {
        return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    }

    #region Events
    /// <summary>
    /// Submit email for forget password.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Users objUser = objUserManager.CheckUseractivation(txtEmail.Text.Trim().Replace("'", ""));
        if (objUser != null)
        {
            string[] message = objPasswordManager.ForgetPassword(txtEmail.Text.Trim().Replace("'", ""));
            divError.InnerHtml = GetKeyResult(message[0].Replace(" ", "").ToUpper());
            divError.Attributes.Add("class", message[1]);
            divError.Style.Add("display", "block");
        }
        else
        {
            divError.InnerHtml = "Sorry, your account is not activated !!";            
            divError.Style.Add("display", "block");
            divError.Attributes.Add("class", "error");
        }

    }

    /// <summary>
    /// This is the Event Handler for Agency Link Click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void agencyLink_Click(object sender, EventArgs e)
    {
        Session["task"] = "register";
        Session["registerType"] = Convert.ToInt32(Usertype.Agency);
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "registration-as-agent/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "registration-as-agent/english");
        }
        //Response.Redirect("Registration.aspx", false);
        return;
    }

    /// <summary>
    /// This is the Event Handler for Company Link Click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void companyLink_Click(object sender, EventArgs e)
    {
        Session["task"] = "register";
        Session["registerType"] = Convert.ToInt32(Usertype.Company);
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "registration-as-company/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "registration-as-company/english");
        }
        //Response.Redirect("Registration.aspx", false);
        return;
    }

    /// <summary>
    /// This is the Event Handler for User Link Click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void userLink_Click(object sender, EventArgs e)
    {
        Session["task"] = "register";
        Session["registerType"] = Convert.ToInt32(Usertype.privateuser);
        //Response.Redirect("Registration.aspx", false);
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "registration/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "registration/english");
        }
        return;
    }
    #endregion
}