﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using System.Configuration;
using log4net;
using log4net.Config;
#endregion


public partial class Operator_ClientContract : System.Web.UI.Page
{
    #region variable decleration
    ILog logger = log4net.LogManager.GetLogger(typeof(Operator_ClientContract));
    ClientContract objClient = new ClientContract();
    City objCity = new City();
    public VList<ViewClientContract> vlist;
    public TList<Country> TlistCountry;
    ViewClientContract objViewContract = new ViewClientContract();
    PictureVideoManagementManager objPicture = new PictureVideoManagementManager();
    HotelManager objHotelManager = new HotelManager();
    HotelInfo ObjHotelinfo = new HotelInfo();
    public string HeaderFilter = string.Empty;
    EmailConfigManager em = new EmailConfigManager();
    #endregion

    #region PageLoad
    /// <summary>
    /// On page load we manage all the containts and Apply paging if there is a post back.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Check session exist or not
            if (Session["CurrentSalesPersonID"] == null)
            {
                Response.Redirect("~/login.aspx",true);
            }

            //Check condition for Page postback.
            if (!IsPostBack)
            {
                FillCountryForSearch();
                uiButtonModify.Visible = false;
                uiButtonDelete.Visible = false;
                divcontract.Visible = false;
                ViewState["Where"] = string.Empty;
                ViewState["Order"] = string.Empty;
                ViewState["SearchAlpha"] = "all";
                uiTextBoxCompanyClient.ReadOnly = true;
                uiTextBoxFacility.ReadOnly = true;
                FilterGridWithHeader(string.Empty, string.Empty, string.Empty);
                divmessage.Style.Add("display", "none");
                divmessage.Attributes.Add("class", "error");
                divMessageOnGridHeader.Style.Add("display", "none");
                divMessageOnGridHeader.Attributes.Add("class", "error");
                txtFromdate.Text = "dd/mm/yy";
                txtTodate.Text = "dd/mm/yy";
                hdnfromdate.Value = "dd/mm/yy";
                hdntodate.Value = "dd/mm/yy";
                RequestGoOnline();
            }
            else
            {
                ApplyPaging();
                txtFromdate.Text = hdnfromdate.Value;
                txtTodate.Text = hdntodate.Value;
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    #region Additional Methods
    /// <summary>
    /// Bind all hotels to the dropdown drpHotelName.
    /// </summary>
    public void BindAllHotel(TList<Hotel> objHotel)
    {
        try
        {
            //Bind all hotel Orderby name.
            if (objHotel.Count > 0)
            {
                drpHotelName.DataSource = objHotel.OrderBy(a => a.Name);
                drpHotelName.DataTextField = "Name";
                drpHotelName.DataValueField = "Id";
                drpHotelName.DataBind();
            }
            else
            {
                drpHotelName.Items.Clear();
            }
            drpHotelName.Items.Insert(0, new ListItem("Select Hotel", ""));
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }


    /// <summary>
    /// Bind all the country Table in dropdown.
    /// </summary>
    protected void FillCountryForSearch()
    {
        try
        {
            //Bind country Dropdown for search 
            TList<Country> objCountry = objClient.GetByAllCountry();
            uiDropDownListCountryforLoad.DataValueField = "Id";
            uiDropDownListCountryforLoad.DataTextField = "CountryName";
            uiDropDownListCountryforLoad.DataSource = objCountry.OrderBy(a=>a.CountryName);
            uiDropDownListCountryforLoad.DataBind();
            uiDropDownListCountryforLoad.Items.Insert(0, new ListItem("Select Country", ""));
            
            drpCountry.DataValueField = "Id";
            drpCountry.DataTextField = "CountryName";
            drpCountry.DataSource = objCountry.OrderBy(a=>a.CountryName);
            drpCountry.DataBind();
            drpCountry.Items.Insert(0, new ListItem("Select Country", ""));
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }


    /// <summary>
    /// Bind all the Sales person from User Tables
    /// </summary>
    protected void FillStaff()
    {
        try
        {
            //Bind all Sales person
            drpSalesperson.DataValueField = "UserId";
            drpSalesperson.DataTextField = "FirstName";
            drpSalesperson.DataSource = objClient.GetAllStaffAccount().OrderBy(a=>a.FirstName);
            drpSalesperson.DataBind();
            drpSalesperson.Items.Insert(0, new ListItem("Select Sales Person", ""));
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    /// <summary>
    /// Bind all group name from group Tables
    /// </summary>
    protected void FillGroup()
    {
        try
        {
            //Bind all Sales person
            drpGroup.DataValueField = "Id";
            drpGroup.DataTextField = "GroupName";
            drpGroup.DataSource = objClient.GetAllGroupName().OrderBy(a => a.Id);
            drpGroup.DataBind();
           
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }


    /// <summary>
    /// Bind Currency dropdown by Country ID
    /// </summary>
    protected void FillCurrency()
    {
        try
        {
            //Bind Currency by country
            if (drpCountry.SelectedIndex != 0)
            {
                drpCurrency.DataValueField = "Id";
                drpCurrency.DataTextField = "Currency";
                if (objClient.GetCurrencyByCountry(Convert.ToInt64(drpCountry.SelectedValue)) != null)
                {
                    drpCurrency.DataSource = objClient.GetCurrencyByCountry(Convert.ToInt64(drpCountry.SelectedValue)).OrderBy(a => a.Currency);
                    drpCurrency.DataBind();
                    drpCurrency.Enabled = true;
                }
            }
            else
            {
                drpCurrency.Items.Clear();
                drpCurrency.Items.Insert(0, new ListItem("Select Currency", ""));
                drpCurrency.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }


    /// <summary>
    /// Bind City dropdown by Country
    /// </summary>
    protected void FillCity()
    {
        try
        {
            //Bind City according to country
            if (drpCountry.SelectedIndex != 0)
            {
                TList<City> tCity = objClient.GetCityByCountry(Convert.ToInt32(drpCountry.SelectedValue));
                drpCity.DataValueField = "Id";
                drpCity.DataTextField = "City";
                drpCity.DataSource = tCity.OrderBy(a=>a.City);
                drpCity.DataBind();
            }
            drpCity.Items.Insert(0, new ListItem("Select City", ""));
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }


    /// <summary>
    /// Bind Zone dropdown by Country
    /// </summary>
    protected void FillZone()
    {
        try
        {
            //Bind Zone according to city
            if (drpCity.SelectedIndex != 0)
            {
                TList<Zone> tZone = objClient.GetZoneByCityId(Convert.ToInt32(drpCity.SelectedItem.Value));
                drpZone.DataValueField = "Id";
                drpZone.DataTextField = "Zone";
                drpZone.DataSource = tZone.OrderBy(a=>a.Zone);
                drpZone.DataBind();
            }
            drpZone.Items.Insert(0, new ListItem("Select Zone", ""));
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }


    /// <summary>
    /// Bind Client dropdown 
    /// </summary>
    protected void FillClient()
    {
        try
        {
            //Bind Client from the record
            drpClientName.Items.Clear();
            drpClientName.DataValueField = "UserId";
            drpClientName.DataTextField = "FirstName";
            drpClientName.DataSource = objClient.GetAllClients().OrderBy(a=>a.FirstName);
            drpClientName.DataBind();
            drpClientName.Items.Insert(0, new ListItem("Select Client", ""));
            drpClientName.Items.Add(new ListItem("Others", "Others"));
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }   

    /// <summary>
    /// Get count of request goonline 
    /// </summary>
    protected void RequestGoOnline()
    {
        try
        {
           string whereclause = HotelColumn.RequestGoOnline + "=1" + " and (" + HotelColumn.GoOnline + " is null" + " or " + HotelColumn.GoOnline + "!=1)"  + "and " + HotelColumn.IsRemoved + "=0" + " and " + "IsActive=1";           
           TList<Hotel> htlList = ObjHotelinfo.GetHotelbyCondition(whereclause, String.Empty);
           lblRequestonline.Text = "You have " + Convert.ToString(htlList.Count)  + " request to go online. " + "<br/>" + " Please check the description of those hotels";
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    #region BindGrid
    /// <summary>
    /// Bind grid with filter condition
    /// </summary>
    /// <param name="wherec">This is the string value which contains value of Viewstate Where condition</param>
    /// <param name="order">This is the string value which contains value of Viewstate Order condition</param>
    public void FilterGridWithHeader(string wherec,string order, string clientNameStarts)
    {
        try
        {
            int totalcount = 0;
            string whereclaus = wherec;
            string whereclauseforHotel = wherec;
            string orderby = order;
            
            if (drpHotelName.SelectedIndex != 0)
            {
                if (!string.IsNullOrEmpty(whereclauseforHotel.Trim()))
                {
                    whereclauseforHotel += " and ";
                }
                whereclauseforHotel += HotelColumn.Id + "=" + drpHotelName.SelectedValue + " ";
            }
            string selectedHotelName = drpHotelName.SelectedValue;
            if (drpActive.SelectedIndex != 0)
            {
                if (!string.IsNullOrEmpty(whereclauseforHotel.Trim()))
                {
                    whereclauseforHotel += " and ";
                }
                whereclauseforHotel += HotelColumn.IsActive + "=1 ";
            }
            if (ViewState["Where2"] != null)
            {
                if (!string.IsNullOrEmpty(ViewState["Where2"].ToString().Trim()))
                {
                    if (!string.IsNullOrEmpty(whereclauseforHotel.Trim()))
                    {
                        whereclauseforHotel += " and ";
                    }
                    whereclauseforHotel += ViewState["Where2"];
                }
            }

            if (!string.IsNullOrEmpty(whereclauseforHotel.Trim()))
            {
                whereclauseforHotel += " and ";
            }
            whereclauseforHotel += HotelColumn.IsRemoved + "=0";

            TList<Hotel> THotel = objHotelManager.GetHotelByWhereAndOrderby(whereclauseforHotel, order,clientNameStarts);
            grvClientContract.PageIndex = Convert.ToInt32(ViewState["CurrentPage"] == null ? "0" : ViewState["CurrentPage"]);
            grvClientContract.DataSource = THotel;
            grvClientContract.DataBind();
            ApplyPaging();
            BindAllHotel(THotel);
            drpHotelName.SelectedValue = selectedHotelName;
            
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    /// <summary>
    /// Alphabat paging applied using this methods
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void PageChange(object sender, EventArgs e)
    {
        try
        {
            int totalcount = 0;
            string whereclaus = String.Empty;
            string orderby = String.Empty;

            HtmlAnchor anc = (sender as HtmlAnchor);
            var d = anc.InnerHtml;
            //ViewState["SearchAlpha"] = d;
            if (d.Trim() == "all")
                {
                whereclaus = HotelColumn.Name + " LIKE '%' ";
                ViewState["SearchAlpha"] = d;
            }
            else
            {
                whereclaus = HotelColumn.Name + " LIKE '" + d + "%' ";
                ViewState["SearchAlpha"] = d;
            }
            ViewState["Where2"] = whereclaus;
            ViewState["CurrentPage"] = 0;

            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
            divMessageOnGridHeader.Style.Add("display", "none");
            divMessageOnGridHeader.Attributes.Add("class", "error");
            if (ViewState["Where"] != null)
            {
                FilterGridWithHeader(ViewState["Where"].ToString(), ViewState["Order"].ToString(), string.Empty);
            }
            else
            {
                FilterGridWithHeader(ViewState["Where2"].ToString(), ViewState["Order"].ToString(), string.Empty);
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }


    /// <summary>
    /// Grid Row databound method to bind the data row.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grvClientContract_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
            {
                Hotel data = e.Row.DataItem as Hotel;
                Label lblContractId = (Label)e.Row.FindControl("lblContractId");
                Label lblCountryName = (Label)e.Row.FindControl("lblCountryName");
                Label lblCity = (Label)e.Row.FindControl("lblCity");
                LinkButton lnbName = (LinkButton)e.Row.FindControl("lnbName");
                Label lblFirstName = (Label)e.Row.FindControl("lblFirstName");
                Label lblContactPerson = (Label)e.Row.FindControl("lblContactPerson");
                Label lblPhone = (Label)e.Row.FindControl("lblPhone");
                Label uiLabelEmail = (Label)e.Row.FindControl("uiLabelEmail");
                Label lblStaffName = (Label)e.Row.FindControl("lblStaffName");
                HyperLink linkviewPlan = (HyperLink)e.Row.FindControl("linkviewPlan");                

                lblContractId.Text = (data.ContractId == null ? "" : data.ContractId);
                Country objCountry = objHotelManager.GetByCountryID(data.CountryId);//;
                lblCountryName.Text = (objCountry.CountryName == null ? "" : objCountry.CountryName);
                City objCity = objHotelManager.GetByCityID(data.CityId);
                if (objCity != null)
                {
                    lblCity.Text = (objCity.City == null ? "" : objCity.City);
                }
                lnbName.Text = (data.Name == null ? "" : data.Name);
                Users objusers = objHotelManager.GetByUserID(Convert.ToInt64(data.ClientId));
                lblFirstName.Text = (objusers.FirstName == null ? "" : objusers.FirstName);
                lblContactPerson.Text = (data.ContactPerson == null ? "" : data.ContactPerson);
                lblPhone.Text = (data.Phone == null ? "" : (data.PhoneExt == null ? "" : "(+" + data.PhoneExt + ")") + "" + data.Phone);
                uiLabelEmail.Text = (data.Email == null ? "" : data.Email);
                Users objusers2 = objHotelManager.GetByUserID(data.StaffId);
                lblStaffName.Text = (objusers2.FirstName == null ? "" : objusers2.FirstName);

                CheckBox uiCheckBoxIsActive = (CheckBox)e.Row.FindControl("uiCheckBoxIsActive");
                uiCheckBoxIsActive.Attributes.Add("AccessKey", data.Id.ToString() + "+" + Convert.ToString(data.Email) + "+" + data.ClientId.ToString() + "+" + Convert.ToString(data.Password) + "");

                if (data.IsActive == true)
                {
                    uiCheckBoxIsActive.Checked = true;
                }
                else
                {
                    uiCheckBoxIsActive.Checked = false;
                }
                if (data.HotelPlan != null)
                {
                    linkviewPlan.Visible = true;
                    linkviewPlan.NavigateUrl = ConfigurationManager.AppSettings["FilePath"] + "HotelImage/" + data.HotelPlan;
                }
                else
                {
                    linkviewPlan.Visible = false;
                }

                //Added on 10th april for search on basis of goonline/request/not requested                
                if (data.GoOnline == true)
                {
                    LinkButton imgEye = (LinkButton)e.Row.FindControl("hypEye");
                    Image imgURL = (Image)e.Row.FindControl("imgEye");
                    imgURL.ImageUrl = "~/Images/access-green.png";
                    imgEye.Visible = true;
                    
                }
                if (data.RequestGoOnline == true && data.GoOnline != true)
                {
                    LinkButton imgEye = (LinkButton)e.Row.FindControl("hypEye");
                    Image imgURL = (Image)e.Row.FindControl("imgEye");
                    imgURL.ImageUrl = "~/Images/access-red.png";
                    imgEye.Visible = true;                    
                }                
                //Added on 10th april for search on basis of goonline/request/not requested
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }


    /// <summary>
    /// Page Index change to bind the grid.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grvClientContract_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            grvClientContract.PageIndex = e.NewPageIndex;
            ViewState["CurrentPage"] = e.NewPageIndex;
            FilterGridWithHeader(Convert.ToString(ViewState["Where"]), Convert.ToString(ViewState["Order"]), Convert.ToString(ViewState["ClientName"]));
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
            divMessageOnGridHeader.Style.Add("display", "none");
            divMessageOnGridHeader.Attributes.Add("class", "error");
            divcontract.Visible = false;

        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }


    /// <summary>
    /// Apply Numaric paging to the grid
    /// </summary>
    private void ApplyPaging()
    {
        try
        {
            GridViewRow row = grvClientContract.TopPagerRow;
            if (row != null)
            {
                PlaceHolder ph;
                LinkButton lnkPaging;
                LinkButton lnkPrevPage;
                LinkButton lnkNextPage;
                lnkPrevPage = new LinkButton();
                lnkPrevPage.CssClass = "pre";
                lnkPrevPage.Width = Unit.Pixel(73);
                lnkPrevPage.CommandName = "Page";
                lnkPrevPage.CommandArgument = "prev";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPrevPage);
                if (grvClientContract.PageIndex == 0)
                {
                    lnkPrevPage.Enabled = false;

                }
                for (int i = 1; i <= grvClientContract.PageCount; i++)
                {
                    lnkPaging = new LinkButton();
                    if (ViewState["CurrentPage"] != null)
                    {
                        if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    else
                    {
                        if (i == 1)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    lnkPaging.Width = Unit.Pixel(16);
                    lnkPaging.Text = i.ToString();
                    lnkPaging.CommandName = "Page";
                    lnkPaging.CommandArgument = i.ToString();
                    if (i == grvClientContract.PageIndex + 1)
                        ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkPaging);
                }
                lnkNextPage = new LinkButton();
                lnkNextPage.CssClass = "nex";
                lnkNextPage.Width = Unit.Pixel(42);
                lnkNextPage.CommandName = "Page";
                lnkNextPage.CommandArgument = "next";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkNextPage);
                ph = (PlaceHolder)row.FindControl("ph");
                if (grvClientContract.PageIndex == grvClientContract.PageCount - 1)
                {
                    lnkNextPage.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    #region Search Methods
    /// <summary>
    /// Method call when client checkbox check change.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkClient_CheckedChanged(object sender, EventArgs e)
    {
        if (chkClient.Checked)
        {
            uiTextBoxCompanyClient.Text = "";
            uiTextBoxCompanyClient.ReadOnly = false;
        }
        else
        {
            uiTextBoxCompanyClient.Text = "";
            uiTextBoxCompanyClient.ReadOnly = true;
        }
        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "CallOnchangeCountry();", true);
    }


    /// <summary>
    /// Method call when contract checkbox check change.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkContracts_CheckedChanged(object sender, EventArgs e)
    {
        if (chkContracts.Checked)
        {
            uiTextBoxFacility.Text = "";
            uiTextBoxFacility.ReadOnly = false;
        }
        else
        {
            uiTextBoxFacility.Text = "";
            uiTextBoxFacility.ReadOnly = true;
        }
        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "CallOnchangeCountry();", true);
    }


    /// <summary>
    /// Selected Index Change of Dropdown Country list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uiDropDownListCountryforLoad_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (uiDropDownListCountryforLoad.SelectedIndex > 0)
            {
                divpagedetailsM.Visible = true;
                divcontract.Visible = false;
                uiButtonDelete.Visible = false;
                uiButtonModify.Visible = false;
                uiDropDownListCityForLoad.DataValueField = "Id";
                uiDropDownListCityForLoad.DataTextField = "City";
                uiDropDownListCityForLoad.DataSource = objClient.GetCityByCountry(Convert.ToInt32(uiDropDownListCountryforLoad.SelectedItem.Value)).OrderBy(a=>a.City);
                uiDropDownListCityForLoad.DataBind();
                uiDropDownListCityForLoad.Items.Insert(0, new ListItem("Select City", ""));
                uiDropDownListCityForLoad.Enabled = true;
                divAlphabeticPaging.Visible = true;
            }
            else
            {
                uiDropDownListCityForLoad.Items.Clear();
                uiDropDownListCityForLoad.Items.Insert(0, new ListItem("Select City", ""));
                uiDropDownListCityForLoad.Enabled = false;
            }
            ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "CallOnchangeCountry();", true);
            logger.Debug("Change Country");
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }


    /// <summary>
    /// Search according to search panel
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtSearch_Click(object sender, EventArgs e)
    {
        try
        {
            string WhereClause = string.Empty;
            if (uiDropDownListCountryforLoad.SelectedIndex != 0)
            {
                if (uiDropDownListCountryforLoad.SelectedValue != "")
                {
                    if (WhereClause.Length > 0)
                    {
                        WhereClause += " and ";
                    }
                    WhereClause += HotelColumn.CountryId + "=" + uiDropDownListCountryforLoad.SelectedValue;
                    
                }
            }
            
            if (uiDropDownListCityForLoad.SelectedIndex != 0)
            {
                if (uiDropDownListCityForLoad.SelectedValue != "")
                {
                    if (WhereClause.Length > 0)
                    {
                        WhereClause += " and ";
                    }
                    WhereClause += HotelColumn.CityId + "=" + uiDropDownListCityForLoad.SelectedValue;
                    
                }

            }
            
            if (hdnfromdate.Value != "" && hdnfromdate.Value != "dd/mm/yy" && hdntodate.Value != "" && hdntodate.Value != "dd/mm/yy")
            {
                DateTime fromDate = new DateTime(Convert.ToInt32("20" + hdnfromdate.Value.Split('/')[2]), Convert.ToInt32(hdnfromdate.Value.Split('/')[1]), Convert.ToInt32(hdnfromdate.Value.Split('/')[0]));
                DateTime todate = new DateTime(Convert.ToInt32("20" + hdntodate.Value.Split('/')[2]), Convert.ToInt32(hdntodate.Value.Split('/')[1]), Convert.ToInt32(hdntodate.Value.Split('/')[0]),23,59,59);
                if (WhereClause.Length > 0)
                {
                    WhereClause += " and ";
                }
                WhereClause += HotelColumn.CreationDate + " between '" + fromDate + "' and '" + todate + "'";
            }

            if (chkClient.Checked)
            {
                if (uiTextBoxCompanyClient.Text != "")
                {
                    ViewState["ClientName"] = uiTextBoxCompanyClient.Text.Replace("'", "");
                    AlphaList.Visible = true;
                }
                else
                {
                    ViewState["ClientName"] = null;
                    AlphaList.Visible = true;
                }
            }
            else
            {
                ViewState["ClientName"] = null;
                AlphaList.Visible = true;
            }
            if (chkContracts.Checked)
            {
                if (WhereClause.Length > 0)
                {
                    WhereClause += " and ";
                }
                WhereClause += HotelColumn.Name + " Like '" + uiTextBoxFacility.Text.Trim().Replace("'", "") + "%'";
                AlphaList.Visible = false;
            }

            //Added on 10th april for search on basis of goonline/request/not requested
            if (rdbStatus.SelectedValue == "0")
            {
                if (WhereClause.Length > 0)
                {
                    WhereClause += " and ";
                }
                WhereClause += HotelColumn.RequestGoOnline + " is null" + " and (" + HotelColumn.GoOnline + " is null" + " or " + HotelColumn.GoOnline + "<>1)";                
            }
            else if (rdbStatus.SelectedValue == "1")
            {
                if (WhereClause.Length > 0)
                {
                    WhereClause += " and ";
                }
                WhereClause += HotelColumn.RequestGoOnline + "=1" + " and (" + HotelColumn.GoOnline + " is null" + " or " + HotelColumn.GoOnline + "!=1)";                
            }
            else if (rdbStatus.SelectedValue == "2")
            {
                if (WhereClause.Length > 0)
                {
                    WhereClause += " and ";
                }
                WhereClause += HotelColumn.GoOnline + "=1";                
            }
            //Added on 10th april for search on basis of goonline/request/not requested


            ViewState["Where"] = WhereClause;
            ViewState["Where2"] = null;
            ViewState["CurrentPage"] = 0;
            
            drpHotelName.SelectedIndex = 0;
            drpActive.SelectedIndex = 0;
            FilterGridWithHeader(Convert.ToString(ViewState["Where"]), Convert.ToString(ViewState["Order"]), Convert.ToString(ViewState["ClientName"]));
            
            ViewState["SearchAlpha"] = "all";
            txtFromdate.Text = hdnfromdate.Value;
            txtTodate.Text = hdntodate.Value;
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
            divMessageOnGridHeader.Style.Add("display", "none");
            divMessageOnGridHeader.Attributes.Add("class", "error");
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }


    /// <summary>
    /// Reset all the filter values
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkClear_Click(object sender, EventArgs e)
    {
        try
        {
            uiDropDownListCountryforLoad.SelectedIndex = 0;
            uiDropDownListCityForLoad.Items.Clear();
            uiDropDownListCityForLoad.Items.Insert(0, new ListItem("Select City", ""));
            uiDropDownListCityForLoad.Enabled = false;
            uiTextBoxCompanyClient.Text = "";
            uiTextBoxCompanyClient.ReadOnly = true;
            uiTextBoxFacility.Text = "";
            uiTextBoxFacility.ReadOnly = true;
            chkClient.Checked = false;
            chkContracts.Checked = false;
            rdbStatus.SelectedIndex = -1;
            
            drpHotelName.SelectedIndex = 0;
            drpActive.SelectedIndex = 0;
            divcontract.Visible = false;
            ViewState["SearchAlpha"] = "all";
            ViewState["CurrentPage"] = 0;
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
            divMessageOnGridHeader.Style.Add("display", "none");
            divMessageOnGridHeader.Attributes.Add("class", "error");
            
            ViewState["Where2"] = null;
            ViewState["Where"] = null;
            ViewState["ClientName"] = null;
            AlphaList.Visible = true;
            FilterGridWithHeader(string.Empty, string.Empty,string.Empty);
            txtTodate.Text = "dd/mm/yy";
            txtFromdate.Text = "dd/mm/yy";
            hdnfromdate.Value = "dd/mm/yy";
            hdntodate.Value = "dd/mm/yy";
            
            drpHotelName.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }


    /// <summary>
    /// Time of Adding data We need to select Client. If "others" is selected then show and hide the divs below.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void drpClientName_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (drpClientName.SelectedItem.Text == "Others")
            {
                DivVatNumber.Visible = true;
                DivUserFirstName.Visible = true;
            }
            else
            {
                DivVatNumber.Visible = false;
                DivUserFirstName.Visible = false;
            }
            string height = string.Empty;
            if (drpClientName.SelectedItem.Text == "Others")
            {
                height = "0";
            }
            else
            {
                height = "1";
            }
            ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "CallWhenYouWant('" + drpZone.ClientID + "','" + height + "');", true);
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }


    /// <summary>
    /// Grid Filter According to Hotel Name
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void drpHotelName_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            ViewState["CurrentPage"] = 0;
            ViewState["SearchAlpha"] = "all";
            ViewState["Where2"] = "";
            ViewState["Where"] = "";
            FilterGridWithHeader(Convert.ToString(ViewState["Where"]), Convert.ToString(ViewState["Order"]), Convert.ToString(ViewState["ClientName"]));            

            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
            divMessageOnGridHeader.Style.Add("display", "none");
            divMessageOnGridHeader.Attributes.Add("class", "error");
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }


    /// <summary>
    /// Grid Is Active Dropdown is to be set for filter in grid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void drpActive_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            ViewState["CurrentPage"] = 0;
            ViewState["SearchAlpha"] = "all";
            ViewState["Where2"] = "";
            
            FilterGridWithHeader(Convert.ToString(ViewState["Where"]), Convert.ToString(ViewState["Order"]), Convert.ToString(ViewState["ClientName"]));

            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
            divMessageOnGridHeader.Style.Add("display", "none");
            divMessageOnGridHeader.Attributes.Add("class", "error");
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }


    /// <summary>
    /// Order By Creation Date
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void creationdate_Click(object sender, EventArgs e)
    {
        try
        {
            if (orderimg.ImageUrl.Contains("/sortdescending.gif"))
            {
                orderimg.ImageUrl = "~/Images/sortascending.gif";
                ViewState["Order"] = HotelColumn.CreationDate + " ASC";
            }
            else
            {
                orderimg.ImageUrl = "~/Images/sortdescending.gif";
                ViewState["Order"] = HotelColumn.CreationDate + " DESC";
            }
            FilterGridWithHeader(Convert.ToString(ViewState["Where"]), Convert.ToString(ViewState["Order"]), Convert.ToString(ViewState["ClientName"]));
            divMessageOnGridHeader.Attributes.Add("class", "error");
            divMessageOnGridHeader.Style.Add("display", "none");
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    #region Add Contracts
    /// <summary>
    /// Add panel country dropdown bind city according to country
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void drpCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (drpCountry.SelectedIndex > 0)
            {
                //Get Currency id on the basis of countryid
                Country cunt = objHotelManager.GetByCountryID(Convert.ToInt32(drpCountry.SelectedValue));
                //Bind currency on the basis of country
                FillCurrency();
                drpCity.DataValueField = "Id";
                drpCity.DataTextField = "City";
                drpCity.DataSource = objClient.GetCityByCountry(Convert.ToInt32(drpCountry.SelectedItem.Value)).OrderBy(a => a.City);
                drpCity.DataBind();
                drpCity.Items.Insert(0, new ListItem("Select City", ""));
                drpCity.Enabled = true;

                string height = string.Empty;
                if (drpClientName.SelectedItem.Text == "Others")
                {
                    height = "0";
                }
                else
                {
                    height = "1";

                }
                ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "CallWhenYouWant('" + drpCountry.ClientID + "','" + height + "');", true);
            }
            else
            {
                FillCurrency();
                drpCity.Items.Clear();
                drpCity.Items.Insert(0, new ListItem("Select City", ""));
                drpCity.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }


    /// <summary>
    /// Add panel city Dropdown selected index change
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void drpCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //Bind Zone on basis of CityId        
            drpZone.DataValueField = "Id";
            drpZone.DataTextField = "Zone";
            drpZone.DataSource = objClient.GetZoneByCityId(Convert.ToInt32(drpCity.SelectedItem.Value)).OrderBy(a=>a.Zone);
            drpZone.DataBind();
            drpZone.Items.Insert(0, new ListItem("Select Zone", ""));
            drpZone.Enabled = true;
            string height = string.Empty;
            if (drpClientName.SelectedItem.Text == "Others")
            {
                height = "0";
            }
            else
            {
                height = "1";

            }
            ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "CallWhenYouWant('" + drpCity.ClientID + "','" + height + "');", true);
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }


    /// <summary>
    /// Button save click check all the details of hotel and add hotel.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            //Check for Duplicate Email
            if (objClient.CheckEmailExist(txtEmailAccount.Text.Trim().Replace("'","")))
            {
                divmessage.InnerHtml = "Email Id already exist in records.";
                divmessage.Attributes.Add("class", "error");
                divmessage.Style.Add("display", "block");
                string height = string.Empty;
                txtHotelName.Focus();
                if (drpClientName.SelectedItem.Text == "Others")
                {
                    height = "0";
                }
                else
                {
                    height = "1";
                }
                ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "CallWhenYouWant('" + drpZone.ClientID + "','" + height + "');", true);
                return;
            }
            string strPlanName = "";
            string strLogo = "";
            if (ScanFile.HasFile)
            {
                if (ScanFile.PostedFile.ContentLength > 1048576)
                {
                    divmessage.InnerHtml = "Filesize of plan is too large. Maximum file size permitted is 1 MB.";
                    divmessage.Attributes.Add("class", "error");
                    divmessage.Style.Add("display", "block");
                    txtHotelName.Focus();
                    string height = string.Empty;
                    if (drpClientName.SelectedItem.Text == "Others")
                    {
                        height = "0";
                    }
                    else
                    {
                        height = "1";
                    }
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "CallWhenYouWant('" + drpZone.ClientID + "','" + height + "');", true);
                    return;
                }
                else
                {
                    strPlanName = Path.GetFileName(ScanFile.FileName);
                    ScanFile.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"] + "HotelImage/") + strPlanName);
                }
            }
            if (uiFileUploadLogo.HasFile)
            {
                if (uiFileUploadLogo.PostedFile.ContentLength > 1048576)
                {
                    divmessage.InnerHtml = "Filesize of image is too large. Maximum file size permitted is 1 MB.";
                    divmessage.Attributes.Add("class", "error");
                    divmessage.Style.Add("display", "block");
                    txtHotelName.Focus();
                    string height = string.Empty;
                    if (drpClientName.SelectedItem.Text == "Others")
                    {
                        height = "0";
                    }
                    else
                    {
                        height = "1";
                    }
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "CallWhenYouWant('" + drpZone.ClientID + "','" + height + "');", true);
                    return;
                }
                else
                {
                    Guid objID = Guid.NewGuid();
                    strLogo = Path.GetFileName(uiFileUploadLogo.FileName).Replace(" ", "").Replace("%20", "").Trim();
                    strLogo = System.Text.RegularExpressions.Regex.Replace(strLogo, @"[^a-zA-Z 0-9'.@]", string.Empty).Trim();
                    strLogo = objID + strLogo;
                    uiFileUploadLogo.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"] + "HotelImage/") + strLogo);
                }
            }
            Int64 recentUserID = 0;
            int ranid = RandomNumber(10000000, 90000000);
            if (drpClientName.SelectedIndex != 0)
            {
                decimal starvalue = Math.Ceiling(Convert.ToDecimal(Rating1.CurrentRating));
                int starRate = (int)starvalue;

                Int64 recentid = 0;
                //User Object
                
                Users objOwnerUser = new Users();
                //User Derails Object
                
                UserDetails objOwnerUserDetails = new UserDetails();
                Users objHClient = new Users();
                bool AddotherUser = false;
                if (drpClientName.SelectedItem.Text == "Others")
                {
                    objOwnerUser.FirstName = uiTextBoxUserFirstName.Text.Trim().Replace("'", "");
                    objOwnerUser.Usertype = (int)Usertype.HotelClient;
                    objOwnerUser.IsActive = true;
                    objOwnerUserDetails.Name = uiTextBoxUserFirstName.Text.Trim().Replace("'", "");
                    objOwnerUserDetails.VatNo = uiTextBoxVATNumber.Text.Replace("'", "");
                    AddotherUser = true;
                }
                else
                {
                    objHClient.ParentId = Convert.ToInt64(drpClientName.SelectedValue);
                }
                objHClient.FirstName = txtContactPerson.Text.Trim().Replace("'", "");
                objHClient.Usertype = (int)Usertype.HotelUser;
                objHClient.EmailId = txtEmailAccount.Text.Replace("'", "");
                objHClient.IsActive = true;
                UserDetails objUserDetails = new UserDetails();
                objUserDetails.Name = txtContactPerson.Text.Trim().Replace("'", "");
                //Hotel Object
                Hotel newHotel = new Hotel();
                newHotel.Name = txtHotelName.Text.Trim().Replace("'", "");
                newHotel.HotelAddress = txtHotelAddress.Text.Trim().Replace("'", "");
                newHotel.ContractId = Convert.ToString(ranid);
                newHotel.CountryId = Convert.ToInt32(drpCountry.SelectedValue);
                newHotel.CurrencyId = Convert.ToInt32(drpCurrency.SelectedValue);
                newHotel.CityId = Convert.ToInt32(drpCity.SelectedValue);
                newHotel.ZoneId = Convert.ToInt32(drpZone.SelectedValue);
                if (drpClientName.SelectedItem.Text != "Others")
                {
                    newHotel.ClientId = Convert.ToInt64(drpClientName.SelectedValue);
                }
                newHotel.StaffId = Convert.ToInt32(drpSalesperson.SelectedValue);
                newHotel.Latitude = lblLatitude.Text.Replace("'", "");
                newHotel.Longitude = lblLongitude.Text.Replace("'", "");
                newHotel.PhoneExt = drpExtPhone.SelectedItem.Text.Replace("'", "");
                newHotel.Phone = txtHotelPhone.Text.Replace("'", "");
                newHotel.ContractValue = Convert.ToDecimal(txtContractValue.Text.Replace("'", ""));
                newHotel.OperatorChoice = drpOperatorChoice.SelectedItem.Text.Replace("'", "");
                newHotel.HotelPlan = strPlanName;
                newHotel.Stars = starRate;
                newHotel.GroupId = Convert.ToInt32(drpGroup.SelectedValue);
                newHotel.ContactPerson = txtContactPerson.Text.Replace("'", "");
                newHotel.Logo = strLogo;
                newHotel.Email = txtEmailAccount.Text.Replace("'", "");
                newHotel.AccountingOfficer=txtAccountingOfficer.Text.Replace("'", "");
                newHotel.IsActive = false;

                

                //HotelPhotoVideoGallary Object
                HotelPhotoVideoGallary objHotelPicture = new HotelPhotoVideoGallary();
                objHotelPicture.FileType = (int)UploadFileType.Image;
                objHotelPicture.ImageName = strLogo;
                objHotelPicture.FileName = null;
                objHotelPicture.AlterText = null;
                objHotelPicture.IsMain = true;
                objHotelPicture.OrderNumber = 1;

                string[] message = objHotelManager.AddNewContract(objHClient, newHotel, objUserDetails, objHotelPicture, AddotherUser, objOwnerUser, objOwnerUserDetails);
                ViewState["CurrentPage"] = 0;
                ViewState["SearchAlpha"] = "all";
                ViewState["Where2"] = "";
                ViewState["Where"] = "";
                ClearAllFileds();
                if (message[1] == "error")
                {
                    divcontract.Visible = true;
                    divmessage.Attributes.Add("class", message[1]);
                    divmessage.Style.Add("display", "block");
                    divmessage.InnerHtml = message[0];
                    divMessageOnGridHeader.Attributes.Add("class", "error");
                    divMessageOnGridHeader.Style.Add("display", "none");
                }
                else
                {
                    divcontract.Visible = false;
                    divmessage.Attributes.Add("class", "error");
                    divmessage.Style.Add("display", "none");
                    divMessageOnGridHeader.InnerHtml = message[0];
                    divMessageOnGridHeader.Attributes.Add("class", message[1]);
                    divMessageOnGridHeader.Style.Add("display", "block");
                    FilterGridWithHeader(string.Empty, string.Empty, string.Empty);
                    //BindAllHotel();
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    private int RandomNumber(int min, int max)
    {
        Random random = new Random();
        return random.Next(min, max);
    }
    #endregion

    #region Modify Contract
    /// <summary>
    /// Grid row command for all modification methods
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grvClientContract_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (Convert.ToString(e.CommandArgument) != "next" && Convert.ToString(e.CommandArgument) != "prev")
            {
                int id;
                id = Convert.ToInt32(e.CommandArgument);
                uiHiddenFieldPrimaryKeyId.Value = id.ToString();
                int totalcount = 0;
                
                string orderby = "";
                Hotel objHotel = objHotelManager.GetHotelDetailsById(Convert.ToInt64(id));                
                if (objHotel != null)
                {
                    txtHotelName.Focus();
                    confirmEmail.Visible = false;
                    divcontract.Visible = true;
                    btnSave.Visible = false;
                    uiButtonModify.Visible = true;
                    uiButtonDelete.Visible = true;
                    string clientis = string.Empty;
                    if (objHotel.CountryId != null)
                    {
                        drpCountry.SelectedValue = Convert.ToString(objHotel.CountryId);
                        clientis = drpCountry.ClientID;
                    }
                    FillCurrency();
                    drpCurrency.SelectedValue = Convert.ToString(objHotel.CurrencyId == null ? "" : objHotel.CurrencyId.ToString());
                    FillCity();
                    if (objHotel.CityId != null)
                    {
                        drpCity.SelectedValue = Convert.ToString(objHotel.CityId);
                        drpCity.Enabled = true;
                        clientis = drpCity.ClientID;
                    }
                    FillZone();
                    if (objHotel.ZoneId != null)
                    {
                        drpZone.SelectedValue = Convert.ToString(objHotel.ZoneId);
                        clientis = drpZone.ClientID;
                        drpZone.Enabled = true;
                    }
                    FillClient();
                    if (objHotel.ClientId != null)
                    {
                        drpClientName.SelectedValue = Convert.ToString(objHotel.ClientId);
                    }
                    string height = string.Empty;
                    if (drpClientName.SelectedItem.Text == "Others")
                    {
                        height = "1";
                    }
                    else
                    {
                        height = "0";
                    }
                    drpClientName.Enabled = false;
                    FillStaff();
                    if (objHotel.StaffId != null)
                    {
                        drpSalesperson.SelectedValue = Convert.ToString(objHotel.StaffId);
                    }


                    FillGroup();
                    if (objHotel.GroupId != null)
                    {
                        drpGroup.SelectedValue = Convert.ToString(objHotel.GroupId);
                    }



                    if (objHotel.Name != null)
                    {
                        txtHotelName.Text = Convert.ToString(objHotel.Name);
                    }
                    if (objHotel.HotelAddress != null)
                    {
                        txtHotelAddress.Text = Convert.ToString(objHotel.HotelAddress);
                    }
                    Rating1.CurrentRating = Convert.ToInt16(objHotel.Stars);
                    lblLatitude.Text = Convert.ToString(objHotel.Latitude);
                    lblLongitude.Text = Convert.ToString(objHotel.Longitude);
                    txtContactPerson.Text = Convert.ToString(objHotel.ContactPerson);
                    txtEmailAccount.Text = Convert.ToString(objHotel.Email);
                    txtEmailAccount.Enabled = false;
                    txtAccountingOfficer.Text = Convert.ToString(objHotel.AccountingOfficer);
                    drpExtPhone.SelectedValue = objHotel.PhoneExt;
                    txtHotelPhone.Text = Convert.ToString(objHotel.Phone);
                    txtContractValue.Text = Convert.ToString(objHotel.ContractValue);
                    drpOperatorChoice.SelectedValue = Convert.ToString(objHotel.OperatorChoice);
                    uiHiddenFieldFileUpload.Value = Convert.ToString(objHotel.HotelPlan);
                    uiHiddenFieldlogo.Value = Convert.ToString(objHotel.Logo);
                    
                    TList<Users> objusers = objHotelManager.GetUserByParentid(Convert.ToInt32(objHotel.ClientId));
                    if (objusers.Count > 0)
                    {
                        TList<UserDetails> objuserdtls = objHotelManager.GetVATnumberUserID(Convert.ToInt32(objHotel.ClientId));
                        DivVatNumber.Visible = true;
                        DivUserFirstName.Visible = false;
                        uiTextBoxVATNumber.Text = objuserdtls[0].VatNo;
                    }                    
                    else
                    {
                        DivVatNumber.Visible = false;
                        DivUserFirstName.Visible = false;                        
                    }

                    ViewState["mode"] = "U";
                    Page.RegisterStartupScript("aa", "<script language='javascript' >jQuery(document).ready(function(){CallWhenYouWant('Modify','" + height + "');});</script>'");
                }
                divmessage.Style.Add("display", "none");
                divmessage.Attributes.Add("class", "error");
                divMessageOnGridHeader.Style.Add("display", "none");
                divMessageOnGridHeader.Attributes.Add("class", "error");
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }


    /// <summary>
    /// Button Modify Click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uiButtonModify_Click(object sender, EventArgs e)
    {
        try
        {
            string strLogo = string.Empty;
            string strPlanName = string.Empty;
            if (Convert.ToInt32(uiHiddenFieldPrimaryKeyId.Value) != 0)
            {

                if (ScanFile.HasFile)
                {
                    if (ScanFile.PostedFile.ContentLength > 1048576)
                    {
                        divmessage.InnerHtml = "Filesize of scanned file is too large. Maximum file size permitted is 1 MB.";
                        divmessage.Attributes.Add("class", "error");
                        divmessage.Style.Add("display", "block");
                        txtHotelName.Focus();
                        string height = string.Empty;
                        if (drpClientName.SelectedItem.Text == "Others")
                        {
                            height = "0";
                        }
                        else
                        {
                            height = "1";
                        }
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "CallWhenYouWant('" + drpZone.ClientID + "','" + height + "');", true);
                        return;
                    }
                    else
                    {
                        strPlanName = Path.GetFileName(ScanFile.FileName);
                        ScanFile.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"] + "HotelImage/") + strPlanName);
                    }
                }
                if (uiFileUploadLogo.HasFile)
                {
                    if (uiFileUploadLogo.PostedFile.ContentLength > 1048576)
                    {
                        divmessage.InnerHtml = "Filesize of logo image is too large. Maximum file size permitted is 1 MB.";
                        divmessage.Attributes.Add("class", "error");
                        divmessage.Style.Add("display", "block");
                        txtHotelName.Focus();
                        string height = string.Empty;
                        if (drpClientName.SelectedItem.Text == "Others")
                        {
                            height = "0";
                        }
                        else
                        {
                            height = "1";
                        }
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "CallWhenYouWant('" + drpZone.ClientID + "','" + height + "');", true);
                        return;
                    }
                    else
                    {
                        Guid objID = Guid.NewGuid();
                        strLogo = Path.GetFileName(uiFileUploadLogo.FileName).Replace(" ","").Replace("%20","").Trim();            
                        strLogo = System.Text.RegularExpressions.Regex.Replace(strLogo, @"[^a-zA-Z 0-9'.@]", string.Empty).Trim();
                        strLogo = objID + strLogo;
                        uiFileUploadLogo.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"] + "HotelImage/") + strLogo);
                    }
                }
                
                //For update function without logo
                if (String.IsNullOrEmpty(strLogo))
                {                    
                    ViewState["FileName"] = uiHiddenFieldlogo.Value;
                }
                else
                {
                    ViewState["FileName"] = strLogo;
                }

                int hotelcount = 0;
                string whereuser = "";
                whereuser = " emailid like '" + txtEmailAccount.Text.Replace("'", "") + "'";
               TList< Users> objuserupdate = DataRepository.UsersProvider.GetPaged(whereuser, string.Empty, 0, int.MaxValue, out hotelcount);

                foreach (Users u in objuserupdate)
                {
                    u.FirstName = txtContactPerson.Text.Replace("'", "");
                    DataRepository.UsersProvider.Update(u);
               
                }

                Hotel objHotal = objHotelManager.GetHotelDetailsById(Convert.ToInt32(uiHiddenFieldPrimaryKeyId.Value));
                objHotal.CountryId = Convert.ToInt32(drpCountry.SelectedValue);
                objHotal.CurrencyId = Convert.ToInt32(drpCurrency.SelectedValue);
                objHotal.CityId = Convert.ToInt32(drpCity.SelectedValue);
                objHotal.ZoneId = Convert.ToInt32(drpZone.SelectedValue);
                objHotal.ClientId = Convert.ToInt32(drpClientName.SelectedValue);
                objHotal.StaffId = Convert.ToInt32(drpSalesperson.SelectedValue);
                objHotal.Name = txtHotelName.Text.Replace("'", "");
                objHotal.HotelAddress = txtHotelAddress.Text.Replace("'", "");
                int starRate = Rating1.CurrentRating;
                objHotal.Stars = starRate;
                objHotal.Latitude = lblLatitude.Text.Replace("'", "");
                objHotal.Longitude = lblLongitude.Text.Replace("'", "");
                objHotal.ContactPerson = txtContactPerson.Text.Replace("'", "");
                objHotal.Email = txtEmailAccount.Text.Replace("'", "");
                objHotal.AccountingOfficer = txtAccountingOfficer.Text.Replace("'", "");
                objHotal.PhoneExt = drpExtPhone.SelectedItem.Text.Replace("'", "");
                objHotal.Phone = txtHotelPhone.Text.Replace("'", "");
                objHotal.ContractValue = Convert.ToDecimal(txtContractValue.Text.Replace("'", ""));
                objHotal.OperatorChoice = drpOperatorChoice.SelectedItem.Text.Replace("'", "");
                objHotal.GroupId = Convert.ToInt32(drpGroup.SelectedValue);
                if (String.IsNullOrEmpty(strPlanName))
                {
                    objHotal.HotelPlan = uiHiddenFieldFileUpload.Value;
                }
                else
                {
                    objHotal.HotelPlan = strPlanName;
                }
                if (String.IsNullOrEmpty(strLogo))
                {
                    objHotal.Logo = uiHiddenFieldlogo.Value;
                }
                else
                {
                    objHotal.Logo = strLogo;
                }

                HotelPhotoVideoGallary objHotalvidioMain = new HotelPhotoVideoGallary();
                string imagename;
                imagename = ViewState["FileName"].ToString();
                int totalcount = 0;
                string whereclaus = "HotelId = " + uiHiddenFieldPrimaryKeyId.Value + " and IsMain=1";
                string orderby = "";
                TList<HotelPhotoVideoGallary> vlisthotelpict = objHotelManager.GetByPhotoVideoByCondition(whereclaus);

                Int64 pictureId;
                if (vlisthotelpict.Count > 0)
                {
                    pictureId = vlisthotelpict.FirstOrDefault().Id;
                }
                else
                {
                    pictureId = 0;
                }
                HotelPhotoVideoGallary objHotalvidio = objHotelManager.GetPhotoVideoByID(pictureId);
                if (objHotalvidio != null)
                {
                    objHotalvidio.FileType = 0;
                    objHotalvidio.ImageName = imagename;
                    objHotalvidio.IsMain = true;
                }


                int hotelcount1 = 0;
                string whereuser1 = "";
                whereuser1 = "UserId=" + objHotal.ClientId;
                TList<UserDetails> objuserdtlsupdate = DataRepository.UserDetailsProvider.GetPaged(whereuser1, string.Empty, 0, int.MaxValue, out hotelcount1);

                foreach (UserDetails u in objuserdtlsupdate)
                {
                    u.VatNo = uiTextBoxVATNumber.Text.Replace("'", "");
                    DataRepository.UserDetailsProvider.Update(u);
                }

                string[] Message = objHotelManager.UpdateContract(objHotal, objHotalvidio);
                ViewState["CurrentPage"] = 0;
                ViewState["SearchAlpha"] = "all";
                ViewState["Where2"] = "";
                ViewState["Where"] = "";
                FilterGridWithHeader(string.Empty, string.Empty,string.Empty);
                divcontract.Visible = false;
                divMessageOnGridHeader.InnerHtml = Message[0];
                divMessageOnGridHeader.Attributes.Add("class", Message[1]);
                divMessageOnGridHeader.Style.Add("display", "block");
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    #region Add New Click and Clear all fields
    protected void uiButtonNewContract_Click(object sender, EventArgs e)
    {
        ClearAllFileds();
        confirmEmail.Visible = true;
        txtConfirmEmail.Text = "";
        txtHotelName.Focus();
        btnSave.Visible = true;
        uiButtonModify.Visible = false;
        uiButtonDelete.Visible = false;
        DivVatNumber.Visible = false;
        DivUserFirstName.Visible = false;
        divmessage.Style.Add("display", "none");
        divmessage.Attributes.Add("class", "error");
        divMessageOnGridHeader.Style.Add("display", "none");
        divMessageOnGridHeader.Attributes.Add("class", "error");
        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){CallWhenYouWant(null,'1');});", true);
    }

    public void ClearAllFileds()
    {
        divcontract.Visible = true;
        drpClientName.Enabled = true;
        txtEmailAccount.Enabled = true;
        
        uiButtonDelete.Visible = false;
        uiButtonModify.Visible = false;
        DivVatNumber.Visible = false;
        DivUserFirstName.Visible = false;
        
        drpCountry.SelectedIndex = 0;
        drpCurrency.Items.Clear();
        drpCurrency.Items.Insert(0, new ListItem("Select Currency", ""));
        drpCurrency.Enabled = false;
        drpCity.Items.Clear();
        drpCity.Items.Insert(0, new ListItem("Select City", ""));
        drpCity.Enabled = false;
        drpZone.Items.Clear();
        drpZone.Items.Insert(0, new ListItem("Select Zone", ""));
        drpZone.Enabled = false;
        FillClient();
        FillStaff();
        FillGroup();
        uiTextBoxVATNumber.Text = "";
        uiTextBoxUserFirstName.Text = "";
        drpSalesperson.SelectedIndex = 0;
        txtHotelName.Text = "";
        txtHotelAddress.Text = "";
        Rating1.CurrentRating = 0;
        lblLatitude.Text = "";
        lblLongitude.Text = "";
        txtContactPerson.Text = "";
        txtEmailAccount.Text = "";
        txtHotelPhone.Text = "";
        txtContractValue.Text = "";
        drpExtPhone.SelectedIndex = 0;
        drpOperatorChoice.SelectedIndex = 0;
        uiHiddenFieldFileUpload.Value = "";
        uiHiddenFieldlogo.Value = "";
        drpOperatorChoice.SelectedIndex = 0;
        DivUserFirstName.Visible = true;
        DivVatNumber.Visible = true;
        ViewState["mode"] = "A";
        scriptRunTime.Text = @"<script type='text/javascript' src='http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=6.3'></script>";
    }
    #endregion

    #region Delete Contract

    /// <summary>
    /// Button delete click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uiButtonDelete_Click(object sender, EventArgs e)
    {
        try
        {
            if (Convert.ToInt32(uiHiddenFieldPrimaryKeyId.Value) != 0)
            {
                DeleteHotalContract(Convert.ToInt32(uiHiddenFieldPrimaryKeyId.Value));
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }


    /// <summary>
    /// Delete Client Contract 
    /// </summary>
    /// <param name="Hotelid"></param>
    /// <returns></returns>
    public bool DeleteHotalContract(Int32 Hotelid)
    {
        try
        {
            if (objHotelManager.DeleteContract(Hotelid))
            {
                divcontract.Visible = false;
                divMessageOnGridHeader.InnerHtml = "Contract deleted successfully.";
                divMessageOnGridHeader.Attributes.Add("class", "succesfuly");
                divMessageOnGridHeader.Style.Add("display", "block");
                ViewState["CurrentPage"] = 0;
                ViewState["SearchAlpha"] = "all";
                ViewState["Where2"] = "";
                ViewState["Where"] = "";
                FilterGridWithHeader(string.Empty, string.Empty,string.Empty);
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            return false;
        }
    }

    #endregion

    #region Checkbox isActive

    /// <summary>
    /// If grid hotel is active check box change then get the "Accesskey" atribute and then find the email exist on the client of the Hotel.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uiCheckBoxIsActive_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox ch = sender as CheckBox;
            var data = ch.Attributes["AccessKey"];
            var arr = data.Split('+');
            if (ch.Checked == true)
            {
                if (Convert.ToInt32(arr[0]) != 0 && arr[0] != null)
                {
                    string genereatedPassword;
                    genereatedPassword = RandomPassword.Generate(8, 10);
                    if (objHotelManager.ChangeHotelActiveStatus(Convert.ToInt32(arr[0]), true))
                    {
                        if (updateUserPassword(Convert.ToInt32(arr[2]), genereatedPassword, arr[1]))
                        {
                            EmailConfig eConfig = em.GetByName("Login details for venus");
                            if (eConfig.IsActive)
                            {
                                string bodymsg = File.ReadAllText(Server.MapPath("~/EmailTemplets/EmailTempletSample.html"));
                                EmailConfigMapping emap = new EmailConfigMapping();
                                emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                                bodymsg = bodymsg.Replace("@CONTENTS", emap.EmailContents);
                                SendMails objSendmail = new SendMails();
                                objSendmail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
                                objSendmail.ToEmail = arr[1];

                                //string bodymsg = File.ReadAllText(Server.MapPath("~/EmailTemplets/LoginDetailsMailTemplet.html"));
                                EmailValueCollection objEmailValues = new EmailValueCollection();

                                objSendmail.Subject = "Login Details for Last Minute Meeting Room Property User";
                                foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForLoginDetailsOfClientContract(arr[1], genereatedPassword, "Last Minute Meeting Room Admin"))
                                {
                                    bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                                }
                                objSendmail.Body = bodymsg;
                                Users objUser = (Users)Session["CurrentSalesPerson"];
                                objSendmail.cc = objUser.EmailId;
                                objSendmail.Bcc = ConfigurationManager.AppSettings["WatchEMailID"];
                                if (objSendmail.SendMail() == "Send Mail")
                                {
                                    divMessageOnGridHeader.InnerHtml = "Contract activated successfully. Activation Mail sent to the Hotel User.";
                                    divMessageOnGridHeader.Attributes.Add("class", "succesfuly");
                                    divMessageOnGridHeader.Style.Add("display", "block");
                                    divcontract.Visible = false;
                                }
                                else
                                {
                                    divmessage.Style.Add("display", "none");
                                    divmessage.Attributes.Add("class", "error");
                                    divMessageOnGridHeader.Style.Add("display", "none");
                                    divMessageOnGridHeader.Attributes.Add("class", "error");
                                    divcontract.Visible = false;
                                }
                            }
                            else
                            {
                                divMessageOnGridHeader.InnerHtml = "The mail functionality is not working yet.";
                                divMessageOnGridHeader.Attributes.Add("class", "error");
                                divMessageOnGridHeader.Style.Add("display", "block");
                                divcontract.Visible = false;
                            }
                        }
                        else
                        {
                            divMessageOnGridHeader.InnerHtml = "Contract activated successfully.";
                            divMessageOnGridHeader.Attributes.Add("class", "succesfuly");
                            divMessageOnGridHeader.Style.Add("display", "block");
                            divcontract.Visible = false;
                        }
                    }
                    else
                    {
                        divMessageOnGridHeader.InnerHtml = "Error occured while change activation state.";
                        divMessageOnGridHeader.Attributes.Add("class", "error");
                        divMessageOnGridHeader.Style.Add("display", "block");
                        divcontract.Visible = false;
                    }
                }
            }

            if (ch.Checked == false)
            {
                if (Convert.ToInt32(arr[0]) != 0 && arr[0] != null)
                {

                    if (objHotelManager.ChangeHotelActiveStatus(Convert.ToInt32(arr[0]), false))
                    {
                        divMessageOnGridHeader.InnerHtml = "Contract deactivated successfully.";
                        divMessageOnGridHeader.Attributes.Add("class", "succesfuly");
                        divMessageOnGridHeader.Style.Add("display", "block");
                        divmessage.Style.Add("display", "none");
                        divmessage.Attributes.Add("class", "error");
                    }
                    else
                    {
                        divMessageOnGridHeader.InnerHtml = "Error occured while change activation state.";
                        divMessageOnGridHeader.Attributes.Add("class", "error");
                        divMessageOnGridHeader.Style.Add("display", "block");
                        divmessage.Style.Add("display", "none");
                        divmessage.Attributes.Add("class", "error");
                    }
                    divcontract.Visible = false;
                }
                else
                {
                    divmessage.Style.Add("display", "none");
                    divmessage.Attributes.Add("class", "error");
                    divMessageOnGridHeader.Style.Add("display", "none");
                    divMessageOnGridHeader.Attributes.Add("class", "error");
                    divcontract.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }


    /// <summary>
    /// Change the user password according to Userid and email
    /// </summary>
    /// <param name="UserId"></param>
    /// <param name="password"></param>
    /// <param name="email"></param>
    /// <returns></returns>
    public bool updateUserPassword(Int32 UserId, string password, string email)
    {
        
            return PasswordManager.UpdateClientPasswordByEmail(email, password);
        
    }

    #endregion
    
    #region Cancel all Modify and Saving option
    /// <summary>
    /// Button Cancel click work redirect to the same page again.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        divMessageOnGridHeader.Attributes.Add("class", "error");
        divMessageOnGridHeader.Style.Add("display", "none");
        Response.Redirect("~/Operator/ClientContract.aspx", true);
    }
    #endregion


    protected void hypEye_Click(object sender, EventArgs e)
    {        
        try
        {
            LinkButton lnkId = (LinkButton)sender;
            Hotel objhotel = objHotelManager.GetHotelDetailsById(Convert.ToInt64(lnkId.CommandName));
            Users objUser = (Users)Session["CurrentSalesPerson"];
            Session["CurrentUserID"] = objUser.UserId;
            Session["CurrentHotelID"] = objhotel.Id;
            Session["CurrentUser"] = Session["CurrentSalesPerson"];
            Session["Operator"] = "1";
            Session["OType"] = "Client";
            Response.Redirect("Watch.aspx");
            //lblFacilityName.Text = objhotel.Name;
            //lblContractNumber.Text = objhotel.ContractId;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    protected void lnb_Click(object sender, EventArgs e)
    {
        pnlFrame.Visible = false;
        Session.Remove("Operator");
    }

}