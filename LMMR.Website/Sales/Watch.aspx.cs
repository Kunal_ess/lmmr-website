﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Entities;
using LMMR.Business;

public partial class Operator_Watch : System.Web.UI.Page
{
    HotelManager objHotelManager = new HotelManager();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["CurrentHotelID"] != null)
            {
                Hotel objhotel = objHotelManager.GetHotelDetailsById(Convert.ToInt32(Session["CurrentHotelID"]));
                lblFacilityName.Text = objhotel.Name;
                lblContractNumber.Text = objhotel.ContractId;
            }

            if (Session["IsPictureManagment"] != null)
            {
                if (Session["IsPictureManagment"].ToString() == "1")
                {
                    OperatorFrame.Attributes.Add("src", "../HotelUser/PictureVideoManagement.aspx");
                    Session.Remove("IsPictureManagment");
                }

            }
        }
    }

    protected void lnb_Click(object sender, EventArgs e)
    {
        //pnlFrame.Visible = false;
        Session.Remove("Operator");
        if (Convert.ToString(Session["OType"]) == "Facility")
        {
            Response.Redirect("HotelFacilityAccess.aspx");
        }
        else
        {
            Response.Redirect("ClientContract.aspx");
        }
        
    }

}