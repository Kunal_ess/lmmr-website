﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using System.Configuration;
using log4net;
using log4net.Config;
using System.Xml;
#endregion

public partial class BookingStep1 : BasePage
{
    #region Variables and Properties

    HotelManager objHotel = new HotelManager();
    Hotel objBookedHotel = new Hotel();
    int countMr = 0;
    Createbooking objBooking = null;
    BookingManager bm = new BookingManager();
    CurrencyManager cm = new CurrencyManager();
    NewUser newUserObject = new NewUser();
    UserManager objUserManager = new UserManager();
    //public XmlDocument XMLLanguage
    //{
    //    get { return (XmlDocument)ViewState["Language"]; }
    //    set { ViewState["Language"] = value; }
    //}
    public decimal CurrencyConvert
    {
        get { return Convert.ToDecimal(ViewState["CurrencyConvert"]); }
        set { ViewState["CurrencyConvert"] = value; }
    }
    public string UserCurrency
    {
        get { return Convert.ToString(ViewState["UserCurrency"]); }
        set { ViewState["UserCurrency"] = value; }
    }
    public decimal CurrentLat
    {
        get;
        set;
    }
    public decimal CurrentLong
    {
        get;
        set;
    }
    public string HotelCurrency
    {
        get { return Convert.ToString(ViewState["HotelCurrency"]); }
        set { ViewState["HotelCurrency"] = value; }
    }
    BookingRequest objBookingRequest = new BookingRequest();
    #endregion

    #region Page Load
    /// <summary>
    /// Page load 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                Session["CheckBedroomAvailability"] = null;
                //Check search is available or not
                CheckSearchAvailable();
                BindMeetingroom();
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Additional Method Bind Meetingroom search
    /// <summary>
    /// Check is Search available
    /// </summary>
    public void CheckSearchAvailable()
    {
        //if user login then Move inside the if loop
        if (Session["CurrentRestUserID"] != null)
        {
            if (Session["SerachID"] != null)
            {
                SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
                if (st != null)
                {
                    if ((st.IsSentAsRequest == null ? false : st.IsSentAsRequest) == true)
                    {
                        divWarnning.InnerHtml = GetKeyResult("YOURBOOKINGISCONVERTINTOREQUEST");// "Your booking is now converted into request.";
                        divWarnning.Attributes.Add("style", "display:block;");
                    }
                    if (st.CurrentStep == 1)
                    {
                        objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
                        //Bind all the meetingroom details
                        //BindMeetingroom();
                    }
                    else if (st.CurrentStep == 2)
                    {
                        if (Request.QueryString["wl"] == null)
                        {
                            //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                            if (l != null)
                            {
                                Response.Redirect(SiteRootPath + "booking-step2/" + l.Name.ToLower());
                            }
                            else
                            {
                                Response.Redirect(SiteRootPath + "booking-step2/english");
                            }

                            //Response.Redirect("BookingStep2.aspx", false);
                        }
                        else
                        {
                            Response.Redirect(SiteRootPath + "bookingstep2.aspx?" + Request.RawUrl.Split('?')[1]);
                        }
                    }
                    else if (st.CurrentStep == 3)
                    {
                        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                        if (Request.QueryString["wl"] == null)
                        {
                            if (l != null)
                            {
                                Response.Redirect(SiteRootPath + "bookingstep3/" + l.Name.ToLower());
                            }
                            else
                            {
                                Response.Redirect(SiteRootPath + "bookingstep3/english");
                            }
                            //Response.Redirect("BookingStep3.aspx", false);
                        }
                        else
                        {
                            Response.Redirect(SiteRootPath + "bookingstep3.aspx?" + Request.RawUrl.Split('?')[1]);
                        }
                    }
                    else if (st.CurrentStep == 4)
                    {
                        if (Request.QueryString["wl"] == null)
                        {
                            //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                            if (l != null)
                            {
                                Response.Redirect(SiteRootPath + "bookingstep3/" + l.Name.ToLower());
                            }
                            else
                            {
                                Response.Redirect(SiteRootPath + "bookingstep3/english");
                            }
                            //Response.Redirect("BookingStep4.aspx", false);
                        }
                        else
                        {
                            Response.Redirect(SiteRootPath + "bookingstep3.aspx?" + Request.RawUrl.Split('?')[1]);
                        }
                    }
                }
                else
                {
                    if (Request.QueryString["wl"] == null)
                    {
                        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                        if (l != null)
                        {
                            Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
                        }
                        else
                        {
                            Response.Redirect(SiteRootPath + "default/english");
                        }
                        //Response.Redirect("Default.aspx", false);
                    }
                }
            }
        }
        else if (Session["Search"] != null)
        {
            objBooking = Session["Search"] as Createbooking;
            //BindMeetingroom();
        }
        else
        {
            //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
            if (l != null)
            {
                Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
            }
            else
            {
                Response.Redirect(SiteRootPath + "default/english");
            }
            //Response.Redirect("Default.aspx", false);
        }
    }

    /// <summary>
    /// Get the meetingroom details 
    /// </summary>
    public void BindMeetingroom()
    {
        //try
        //{
        if (objBooking == null)
        {
            objBooking = Session["Search"] as Createbooking;
        }
        if (Session["CurrentRestUser"] != null)
        {
            //Bind Login users details
            Users objUsers = Session["CurrentRestUser"] as Users;
            pnlLogin.Visible = true;
            lblDateLogin.Text = DateTime.Now.ToString("dd MMM yyyy");
            lblUserName.Text = objUsers.FirstName + " " + objUsers.LastName;
        }
        else
        {
            pnlLogin.Visible = false;
        }
        //Bind Booking Details
        lblArrival.Text = objBooking.ArivalDate.ToString("dd MMM yyyy");
        lblDeparture.Text = objBooking.DepartureDate.ToString("dd MMM yyyy");
        lblDuration.Text = objBooking.Duration == 1 ? objBooking.Duration + GetKeyResult("DAY") : objBooking.Duration + GetKeyResult("DAY");

        //Bind Hotel details
        objBookedHotel = objHotel.GetHotelDetailsByIdandLanguage(objBooking.HotelID, Convert.ToInt64(Session["LanguageID"]));
        //objBookedHotel.HotelDescCollection.Add(objHotel.GetHotelDescriptionByHotelAndLanguageId(objBooking.HotelID, Convert.ToInt32(Session["LanguageID"])));
        lblHotelname.Text = objBookedHotel.Name;
        lblFullFrom.Text = objBookedHotel.RtFFrom;
        lblFullTo.Text = objBookedHotel.RtFTo;
        lblMorningFrom.Text = objBookedHotel.RtMFrom;
        lblMorningTo.Text = objBookedHotel.RtMTo;
        lblAfternoonFrom.Text = objBookedHotel.RtAFrom;
        lblAfternoonTo.Text = objBookedHotel.RtATo;

        imgHotel.ImageUrl = (string.IsNullOrEmpty(objBookedHotel.Logo) ? ConfigurationManager.AppSettings["FilePath"] + "HotelImage/default_hotel_logo.png" : ConfigurationManager.AppSettings["FilePath"] + "HotelImage/" + objBookedHotel.Logo);
        TList<HotelPhotoVideoGallary> ObjHotelImageAltertext = objBookingRequest.GetAlternateTextPictureVideo(Convert.ToInt32(objBooking.HotelID));
        if (ObjHotelImageAltertext.Count > 0)
        {
            imgHotel.ToolTip = ObjHotelImageAltertext[0].AlterText;
        }
        Currency objUserCurrency = cm.GetCurrencyDetailsByID(Convert.ToInt64(Session["CurrencyID"]));
        UserCurrency = objUserCurrency.Currency;
        Currency objCurrency = cm.GetCurrencyDetailsByID(objBookedHotel.CurrencyId);
        HotelCurrency = objCurrency.Currency;
        string currency = CurrencyManager.Currency(HotelCurrency, UserCurrency);
        CurrencyConvert = Convert.ToDecimal(currency == "N/A" ? "1" : currency);
        CurrentLat = Convert.ToDecimal(objBookedHotel.Latitude);
        CurrentLong = Convert.ToDecimal(objBookedHotel.Longitude);
        //for static hotel star
        if (objBookedHotel.Stars == 1)
        {
            imgstars.ImageUrl = "~/Images/1.png";
        }
        else if (objBookedHotel.Stars == 2)
        {
            imgstars.ImageUrl = "~/Images/2.png";
        }
        else if (objBookedHotel.Stars == 3)
        {
            imgstars.ImageUrl = "~/Images/3.png";
        }
        else if (objBookedHotel.Stars == 4)
        {
            imgstars.ImageUrl = "~/Images/4.png";
        }
        else if (objBookedHotel.Stars == 5)
        {
            imgstars.ImageUrl = "~/Images/5.png";
        }
        else if (objBookedHotel.Stars == 6)
        {
            imgstars.ImageUrl = "~/Images/6.png";
        }
        else if (objBookedHotel.Stars == 7)
        {
            imgstars.ImageUrl = "~/Images/7.png";
        }

        //Get description by language.
        HotelDesc objDesc = objBookedHotel.HotelDescCollection.Where(a => a.LanguageId == Convert.ToInt32(Session["LanguageID"])).FirstOrDefault();//objHotel.GetHotelDescriptionByHotelAndLanguageId(objBookedHotel.Id, Convert.ToInt32(Session["LanguageID"]));
        if (objDesc != null)
        {
            lblDescription.Text = objDesc.Description;
            lblAddress.Text = objDesc.Address;
        }
        else
        {
            lblDescription.Text = "";
            lblAddress.Text = objBookedHotel.HotelAddress;
        }
        //Bind with meetingroom details
        countMr = objBooking.MeetingroomList.Count;
        if (countMr == 0)
        {
            //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
            if (l != null)
            {
                Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
            }
            else
            {
                Response.Redirect(SiteRootPath + "default/english");
            }
            //Response.Redirect("default.aspx", false);
        }
        else
        {
            dtLstMeetingroom.DataSource = objBooking.MeetingroomList;
            dtLstMeetingroom.DataBind();
        }
        if (bm.CheckMeetingroomReserved(objBooking))
        {
            //Page.RegisterStartupScript("aa", "<script language='javascript' type='text/javascript'>outFromPage();</script>");
        }
        //}
        //catch (Exception ex)
        //{
        //    logger.Error(ex);
        //}
    }
    #endregion

    #region Event
    /// <summary>
    /// Bind Meeting room details get by Search.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dtLstMeetingroom_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //Create object of BookedMR
            BookedMR objBooked = e.Item.DataItem as BookedMR;
            if (objBooked != null)
            {
                //Get the components present in Repeater.
                Label lblMeetingRoomName = (Label)e.Item.FindControl("lblMeetingRoomName");
                Image imgMrImage = (Image)e.Item.FindControl("imgMrImage");
                Label lblConfigurationType = (Label)e.Item.FindControl("lblConfigurationType");
                Label lblMaxandMinCapacity = (Label)e.Item.FindControl("lblMaxandMinCapacity");
                Repeater rptDays = (Repeater)e.Item.FindControl("rptDays");
                HyperLink hypPlan = (HyperLink)e.Item.FindControl("hypPlan");
                ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");

                //Get details of Meeting room according to the meeting room Id with Deep Load
                MeetingRoom objMeetingroom = objHotel.GetMeetingRoomDetailsById(objBooked.MRId);
                MeetingRoomDesc objDesc = objMeetingroom.MeetingRoomDescCollection.Where(a => a.LanguageId == Convert.ToInt32(Session["LanguageID"])).FirstOrDefault();
                MeetingRoomConfig objMrConfig = objMeetingroom.MeetingRoomConfigCollection.Where(a => a.Id == objBooked.MrConfigId).FirstOrDefault();
                if (objMeetingroom != null)
                {
                    //Fill All meeting room details.
                    lblMeetingRoomName.Text = objMeetingroom.Name;
                    imgMrImage.ImageUrl = (string.IsNullOrEmpty(objMeetingroom.Picture) ? ConfigurationManager.AppSettings["FilePath"] + "HotelImage/default_hotel_logo.png" : ConfigurationManager.AppSettings["FilePath"] + "MeetingRoomImage/" + objMeetingroom.Picture);
                    if (objMrConfig != null)
                    {
                        //Bind the child repeater with the datails of days
                        lblConfigurationType.Text = (objMrConfig.RoomShapeId == (int)RoomShape.Boardroom ? RoomShape.Boardroom.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Classroom ? RoomShape.Classroom.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Cocktail ? RoomShape.Cocktail.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.School ? RoomShape.School.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Theatre ? RoomShape.Theatre.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.UShape ? RoomShape.UShape.ToString() : RoomShape.Boardroom.ToString());
                        lblMaxandMinCapacity.Text = objMrConfig.MinCapacity + " - " + objMrConfig.MaxCapicity;
                        rptDays.DataSource = objBooked.MrDetails;
                        rptDays.DataBind();
                    }
                    if (objMeetingroom.MrPlan != "")
                    {
                        hypPlan.NavigateUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "Plan/" + objMeetingroom.MrPlan;
                        hypPlan.ToolTip = objMeetingroom.MrPlan;
                        hypPlan.Target = "_blank";
                        hypPlan.Text = GetKeyResult("SEEPLAN");
                        hypPlan.Visible = true;
                    }
                    else
                    {
                        hypPlan.Visible = false;
                        hypPlan.Text = GetKeyResult("SEEPLAN");
                    }
                }
                if (countMr == 1)
                {
                    btnDelete.Visible = false;
                }
                else
                {
                    btnDelete.OnClientClick = "return confirm('" + GetKeyResult("DOYOUWANTTODELETETHISMEETINGROOM") + "?');";
                }
            }
        }
    }

    /// <summary>
    /// Bind child repeater of the days.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rptDays_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //Get the details of the Booking Configure object.
            BookedMrConfig objBookedMrConfig = e.Item.DataItem as BookedMrConfig;
            //Get the controls of the child repeater
            HiddenField hdnHotelArivalTime = (HiddenField)e.Item.FindControl("hdnHotelArivalTime");
            HiddenField hdnHotelDepartureTime = (HiddenField)e.Item.FindControl("hdnHotelDepartureTime");

            Label lblDayDuration = (Label)e.Item.FindControl("lblDayDuration");
            //Label lblTiming = (Label)e.Item.FindControl("lblTiming");
            DropDownList drpFrom = (DropDownList)e.Item.FindControl("drpFrom");
            DropDownList drpTo = (DropDownList)e.Item.FindControl("drpTo");
            TextBox txtQuantity = (TextBox)e.Item.FindControl("txtQuantity");
            if (objBookedMrConfig != null)
            {
                //Bind all the details of the database.
                lblDayDuration.Text = objBookedMrConfig.SelectedTime == 0 ? GetKeyResult("FULLDAY") : objBookedMrConfig.SelectedTime == 1 ? GetKeyResult("MORNING") : objBookedMrConfig.SelectedTime == 2 ? GetKeyResult("AFTERNOON") : GetKeyResult("FULLDAY");
                BindDropdown(drpFrom, objBookedMrConfig.FromTime, objBookedMrConfig.ToTime);
                BindDropdown(drpTo, objBookedMrConfig.FromTime, objBookedMrConfig.ToTime);
                drpFrom.Items.FindByText(objBookedMrConfig.FromTime).Selected = true;
                drpTo.Items.FindByText(objBookedMrConfig.ToTime).Selected = true;
                txtQuantity.Text = Convert.ToString(objBookedMrConfig.NoOfParticepant);
                hdnHotelArivalTime.Value = objBookedMrConfig.SelectedTime == 0 ? objBookedHotel.RtFFrom : objBookedMrConfig.SelectedTime == 1 ? objBookedHotel.RtMFrom : objBookedMrConfig.SelectedTime == 2 ? objBookedHotel.RtAFrom : objBookedHotel.RtFFrom;
                hdnHotelDepartureTime.Value = objBookedMrConfig.SelectedTime == 0 ? objBookedHotel.RtFTo : objBookedMrConfig.SelectedTime == 1 ? objBookedHotel.RtMTo : objBookedMrConfig.SelectedTime == 2 ? objBookedHotel.RtATo : objBookedHotel.RtFTo;
            }
        }
    }

    /// <summary>
    /// Next button event collect all information and send to next page. step2
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkNext_Click(object sender, EventArgs e)
    {
        NextProcess(); 
    }

    /// <summary>
    /// This function used to proceed for next step.
    /// </summary>
    public void NextProcess()
    {
        SearchTracer st = null;
        if (objBooking == null)
        {
            if (Session["SerachID"] == null)
            {
                objBooking = Session["Search"] as Createbooking;
            }
            else
            {
                st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
                objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
            }
        }
        //Insert Value in Search Variable.
        if (objBooking != null)
        {
            for (int i = 0; i < dtLstMeetingroom.Items.Count; i++)
            {
                Repeater repChild = (Repeater)dtLstMeetingroom.Items[i].FindControl("rptDays");
                for (int j = 0; j < repChild.Items.Count; j++)
                {
                    TextBox txtQuantity = (TextBox)repChild.Items[j].FindControl("txtQuantity");
                    DropDownList drpFrom = (DropDownList)repChild.Items[j].FindControl("drpFrom");
                    DropDownList drpTo = (DropDownList)repChild.Items[j].FindControl("drpTo");
                    objBooking.MeetingroomList[i].MrDetails[j].NoOfParticepant = Convert.ToInt32(txtQuantity.Text);
                    objBooking.MeetingroomList[i].MrDetails[j].FromTime = drpFrom.SelectedItem.Text;
                    objBooking.MeetingroomList[i].MrDetails[j].ToTime = drpTo.SelectedItem.Text;
                    //if (objBooking.MeetingroomList[i].MrDetails[j].PackageID <= 0)
                    //{
                    //    objBooking.MeetingroomList[i].MrDetails[j].PackageID = 0;
                    //}
                    if (objBooking.MeetingroomList[i].MrDetails[j].ManageExtrasLst == null)
                    {
                        objBooking.MeetingroomList[i].MrDetails[j].ManageExtrasLst = new List<ManageExtras>();
                    }
                    if (objBooking.MeetingroomList[i].MrDetails[j].ManagePackageLst == null)
                    {
                        objBooking.MeetingroomList[i].MrDetails[j].ManagePackageLst = new List<ManagePackageItem>();
                    }
                    if (objBooking.MeetingroomList[i].MrDetails[j].EquipmentLst == null)
                    {
                        objBooking.MeetingroomList[i].MrDetails[j].EquipmentLst = new List<ManageEquipment>();
                    }
                    if (objBooking.MeetingroomList[i].MrDetails[j].BuildManageMRLst == null)
                    {
                        objBooking.MeetingroomList[i].MrDetails[j].BuildManageMRLst = new List<BuildYourMR>();
                    }
                }
            }
            //Manage Accomodation
            #region Manage Accomodation No more in use in this version
            //if (objBooking.ManageAccomodationLst==null || objBooking.ManageAccomodationLst.Count <= 0)
            //{
            //    VList<ViewFindAvailableBedroom> lstBedroom = objHotel.GetAvailabilityBedroomListByHotelID(objBooking.HotelID, objBooking.ArivalDate, objBooking.DepartureDate, objBooking.Duration);
            //    foreach (ViewFindAvailableBedroom b in lstBedroom.FindAllDistinct(ViewFindAvailableBedroomColumn.Id))
            //    {
            //        Accomodation objAccomodation = new Accomodation();
            //        objAccomodation.BedroomId = b.Id;
            //        objAccomodation.Quantity = 1;
            //        RoomManage objRoomManage = new RoomManage();
            //        objRoomManage.CheckInTime = objBooking.ArivalDate.ToString("dd/MM/yyyy");
            //        objRoomManage.checkoutTime = objBooking.DepartureDate.ToString("dd/MM/yyyy");
            //        if (objAccomodation.RoomManageLst == null)
            //        {
            //            objAccomodation.RoomManageLst = new List<RoomManage>();
            //        }
            //        objAccomodation.RoomManageLst.Add(objRoomManage);
            //        if (objBooking.ManageAccomodationLst == null)
            //        {
            //            objBooking.ManageAccomodationLst = new List<Accomodation>();
            //        }
            //        objBooking.ManageAccomodationLst.Add(objAccomodation);
            //    }
            //    objBooking.AccomodationDiffCheckinChecoutTime = false;
            //}
            #endregion

            #region New Version of Accomodation
            if (objBooking.ManageAccomodationLst == null || objBooking.ManageAccomodationLst.Count <= 0)
            {
                if (objBooking.ManageAccomodationLst == null)
                {
                    objBooking.ManageAccomodationLst = new List<Accomodation>();
                }
                VList<ViewFindAvailableBedroom> lstBedroom = objHotel.GetAvailabilityBedroomListByHotelID(objBooking.HotelID, (objBooking.Duration == 1 ? objBooking.ArivalDate.AddDays(-2) : objBooking.ArivalDate.AddDays(-1)), (objBooking.Duration == 1 ? objBooking.ArivalDate.AddDays(2) : objBooking.DepartureDate.AddDays(2)), objBooking.Duration);
                foreach (ViewFindAvailableBedroom b in lstBedroom.FindAllDistinct(ViewFindAvailableBedroomColumn.RoomId))
                {
                    VList<ViewFindAvailableBedroom> singleBedroom = lstBedroom.FindAll(ViewFindAvailableBedroomColumn.RoomId, b.RoomId);
                    foreach (ViewFindAvailableBedroom v in singleBedroom)
                    {
                        Accomodation objAccomodation = new Accomodation();
                        objAccomodation.BedroomId = v.RoomId;
                        objAccomodation.BedroomType = Convert.ToInt32(v.Types);
                        objAccomodation.DateRequest = Convert.ToDateTime(v.AvailabilityDate == null ? DateTime.Now : v.AvailabilityDate);
                        objAccomodation.QuantityDouble = 0;
                        objAccomodation.QuantitySingle = 0;
                        if (v.MorningStatus == (int)AvailabilityStatus.CLOSED)
                        {
                            objAccomodation.RoomAvailable = 0;
                        }
                        else
                        {
                            objAccomodation.RoomAvailable = Convert.ToInt64(v.NumberOfRoomsAvailable) - Convert.ToInt64(v.NumberOfRoomBooked) < 0 ? 0 : Convert.ToInt64(v.NumberOfRoomsAvailable) - Convert.ToInt64(v.NumberOfRoomBooked);
                        }
                        objAccomodation.RoomPriceDouble = Convert.ToDecimal(v.PriceOfTheDayDouble == 0 ? v.PriceDouble : v.PriceOfTheDayDouble);
                        objAccomodation.RoomPriceSingle = Convert.ToDecimal(v.PriceOfTheDaySingle == 0 ? v.PriceSingle : v.PriceOfTheDaySingle);
                        objAccomodation.Vat = 0;
                        objBooking.ManageAccomodationLst.Add(objAccomodation);
                    }
                }
                objBooking.NoAccomodation = false;
            }
            #endregion

            if (st != null)
            {
                //objSearch.SearchId = Convert.ToString(Session["SerachID"]);
            }
            else
            {
                st = new SearchTracer();
                st.SearchId = Guid.NewGuid().ToString();
            }

            if (Session["CurrentRestUserID"] != null)
            {
                st.UserId = Convert.ToInt64(Session["CurrentRestUserID"]);
                objBooking.CurrentUserId = st.UserId.Value;
            }
            else
            {
                st.UserId = null;
                objBooking.CurrentUserId = 0;
            }
            st.CurrentStep = 2;
            st.IsBooking = true;
            st.CurrentDay = 1;
            if (Request.QueryString["wl"] != null)
            {
                st.ChannelBy = "wl";
                st.ChannelId = Convert.ToString(Request.QueryString["wl"]);
                objBooking.BookType = "wl";
                objBooking.ChannelID = Convert.ToString(Request.QueryString["wl"]);
            }
            Session["Search"] = objBooking;
            string serailstring = TrailManager.XmlSerialize(objBooking);
            st.SearchObject = serailstring;
            if (bm.SaveSearch(st))
            {
                Session["SerachID"] = st.SearchId;

                //This condition added for white label
                if (Request.QueryString["wl"] == null)
                {
                    if (Session["CurrentRestUserID"] != null)
                    {
                        bm.ResetIsReserverTrue(objBooking);
                        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                        if (l != null)
                        {
                            Response.Redirect(SiteRootPath + "booking-step2/" + l.Name.ToLower());
                        }
                        else
                        {
                            Response.Redirect(SiteRootPath + "booking-step2/english");
                        }
                        //Response.Redirect("BookingStep2.aspx", false);
                    }
                    else
                    {
                        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                        if (l != null)
                        {
                            Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                        }
                        else
                        {
                            Response.Redirect(SiteRootPath + "login/english");
                        }
                        //Response.Redirect("login.aspx");
                    }
                }
                else
                {
                    if (Session["CurrentRestUserID"] != null)
                    {
                        bm.ResetIsReserverTrue(objBooking);
                        Response.Redirect(SiteRootPath + "bookingstep2.aspx?" + Request.RawUrl.Split('?')[1]);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "openRegisterORLoginPopUp", "openRegisterORLoginPopUp();", true);
                    }
                }
            }
            else
            {
                //Error Message No Search Record Updated. 
            }
        }
        else
        {
            //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
            if (Request.QueryString["wl"] == null)
            {
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "default/english");
                }
            }
            //Response.Redirect("default.aspx");
        }
    
    }
    private void Master_ButtonClick(object sender, EventArgs e)
    {
        // This Method will be Called.
        MasterNextProcess();
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        // Create an event handler for the master page's contentCallEvent event
        this.Master.contentCallEvent += new EventHandler(Master_ButtonClick);
    }
    public void MasterNextProcess()
    {
        SearchTracer st = null;
        if (objBooking == null)
        {
            if (Session["SerachID"] == null)
            {
                objBooking = Session["Search"] as Createbooking;
            }
            else
            {
                st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
                objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
            }
        }
        //Insert Value in Search Variable.
        if (objBooking != null)
        {
            for (int i = 0; i < dtLstMeetingroom.Items.Count; i++)
            {
                Repeater repChild = (Repeater)dtLstMeetingroom.Items[i].FindControl("rptDays");
                for (int j = 0; j < repChild.Items.Count; j++)
                {
                    TextBox txtQuantity = (TextBox)repChild.Items[j].FindControl("txtQuantity");
                    DropDownList drpFrom = (DropDownList)repChild.Items[j].FindControl("drpFrom");
                    DropDownList drpTo = (DropDownList)repChild.Items[j].FindControl("drpTo");
                    objBooking.MeetingroomList[i].MrDetails[j].NoOfParticepant = Convert.ToInt32(txtQuantity.Text);
                    objBooking.MeetingroomList[i].MrDetails[j].FromTime = drpFrom.SelectedItem.Text;
                    objBooking.MeetingroomList[i].MrDetails[j].ToTime = drpTo.SelectedItem.Text;
                    //if (objBooking.MeetingroomList[i].MrDetails[j].PackageID <= 0)
                    //{
                    //    objBooking.MeetingroomList[i].MrDetails[j].PackageID = 0;
                    //}
                    if (objBooking.MeetingroomList[i].MrDetails[j].ManageExtrasLst == null)
                    {
                        objBooking.MeetingroomList[i].MrDetails[j].ManageExtrasLst = new List<ManageExtras>();
                    }
                    if (objBooking.MeetingroomList[i].MrDetails[j].ManagePackageLst == null)
                    {
                        objBooking.MeetingroomList[i].MrDetails[j].ManagePackageLst = new List<ManagePackageItem>();
                    }
                    if (objBooking.MeetingroomList[i].MrDetails[j].EquipmentLst == null)
                    {
                        objBooking.MeetingroomList[i].MrDetails[j].EquipmentLst = new List<ManageEquipment>();
                    }
                    if (objBooking.MeetingroomList[i].MrDetails[j].BuildManageMRLst == null)
                    {
                        objBooking.MeetingroomList[i].MrDetails[j].BuildManageMRLst = new List<BuildYourMR>();
                    }
                }
            }
            //Manage Accomodation

            #region New Version of Accomodation
            if (objBooking.ManageAccomodationLst == null || objBooking.ManageAccomodationLst.Count <= 0)
            {
                if (objBooking.ManageAccomodationLst == null)
                {
                    objBooking.ManageAccomodationLst = new List<Accomodation>();
                }
                VList<ViewFindAvailableBedroom> lstBedroom = objHotel.GetAvailabilityBedroomListByHotelID(objBooking.HotelID, (objBooking.Duration == 1 ? objBooking.ArivalDate.AddDays(-2) : objBooking.ArivalDate.AddDays(-1)), (objBooking.Duration == 1 ? objBooking.ArivalDate.AddDays(2) : objBooking.DepartureDate.AddDays(2)), objBooking.Duration);
                foreach (ViewFindAvailableBedroom b in lstBedroom.FindAllDistinct(ViewFindAvailableBedroomColumn.RoomId))
                {
                    VList<ViewFindAvailableBedroom> singleBedroom = lstBedroom.FindAll(ViewFindAvailableBedroomColumn.RoomId, b.RoomId);
                    foreach (ViewFindAvailableBedroom v in singleBedroom)
                    {
                        Accomodation objAccomodation = new Accomodation();
                        objAccomodation.BedroomId = v.RoomId;
                        objAccomodation.BedroomType = Convert.ToInt32(v.Types);
                        objAccomodation.DateRequest = Convert.ToDateTime(v.AvailabilityDate == null ? DateTime.Now : v.AvailabilityDate);
                        objAccomodation.QuantityDouble = 0;
                        objAccomodation.QuantitySingle = 0;
                        if (v.MorningStatus == (int)AvailabilityStatus.CLOSED)
                        {
                            objAccomodation.RoomAvailable = 0;
                        }
                        else
                        {
                            objAccomodation.RoomAvailable = Convert.ToInt64(v.NumberOfRoomsAvailable) - Convert.ToInt64(v.NumberOfRoomBooked) < 0 ? 0 : Convert.ToInt64(v.NumberOfRoomsAvailable) - Convert.ToInt64(v.NumberOfRoomBooked);
                        }
                        objAccomodation.RoomPriceDouble = Convert.ToDecimal(v.PriceOfTheDayDouble == 0 ? v.PriceDouble : v.PriceOfTheDayDouble);
                        objAccomodation.RoomPriceSingle = Convert.ToDecimal(v.PriceOfTheDaySingle == 0 ? v.PriceSingle : v.PriceOfTheDaySingle);
                        objAccomodation.Vat = 0;
                        objBooking.ManageAccomodationLst.Add(objAccomodation);
                    }
                }
                objBooking.NoAccomodation = false;
            }
            #endregion

            if (st != null)
            {
                //objSearch.SearchId = Convert.ToString(Session["SerachID"]);
            }
            else
            {
                st = new SearchTracer();
                st.SearchId = Guid.NewGuid().ToString();
            }

            if (Session["CurrentRestUserID"] != null)
            {
                st.UserId = Convert.ToInt64(Session["CurrentRestUserID"]);
                objBooking.CurrentUserId = st.UserId.Value;
            }
            else
            {
                st.UserId = null;
                objBooking.CurrentUserId = 0;
            }
            //st.CurrentStep = 2;
            //st.IsBooking = true;
            //st.CurrentDay = 1;
            //if (Request.QueryString["wl"] != null)
            //{
            //    st.ChannelBy = "wl";
            //    st.ChannelId = Convert.ToString(Request.QueryString["wl"]);
            //    objBooking.BookType = "wl";
            //    objBooking.ChannelID = Convert.ToString(Request.QueryString["wl"]);
            //}
            Session["Search"] = objBooking;
            string serailstring = TrailManager.XmlSerialize(objBooking);
            st.SearchObject = serailstring;
            if (bm.SaveSearch(st))
            {
                Session["SerachID"] = st.SearchId;

                //This condition added for white label
                if (Request.QueryString["wl"] == null)
                {
                    if (Session["CurrentRestUserID"] != null)
                    {
                        bm.ResetIsReserverTrue(objBooking);
                        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                        //if (l != null)
                        //{
                        //    Response.Redirect(SiteRootPath + "booking-step2/" + l.Name.ToLower());
                        //}
                        //else
                        //{
                        //    Response.Redirect(SiteRootPath + "booking-step2/english");
                        //}
                        //Response.Redirect("BookingStep2.aspx", false);
                    }
                    else
                    {
                        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                        //if (l != null)
                        //{
                        //    Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                        //}
                        //else
                        //{
                        //    Response.Redirect(SiteRootPath + "login/english");
                        //}
                        //Response.Redirect("login.aspx");
                    }
                }
                else
                {
                    if (Session["CurrentRestUserID"] != null)
                    {
                        bm.ResetIsReserverTrue(objBooking);
                        //Response.Redirect(SiteRootPath + "bookingstep2.aspx?" + Request.RawUrl.Split('?')[1]);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "openRegisterORLoginPopUp", "openRegisterORLoginPopUp();", true);
                    }
                }
            }
            else
            {
                //Error Message No Search Record Updated. 
            }
        }
        else
        {
            //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
            if (Request.QueryString["wl"] == null)
            {
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "default/english");
                }
            }
            //Response.Redirect("default.aspx");
        }

    }
    /// <summary>
    /// Meeting room repeater item command event
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtLstMeetingroom_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (objBooking == null)
        {
            objBooking = Session["Search"] as Createbooking;
        }
        //Delete Meetingroom
        if (e.CommandName == "DeleteMeetingroom")
        {
            SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
            if (st != null)
            {
                objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
            }
            else
            {
                st = new SearchTracer();
                objBooking = (Createbooking)Session["Search"];
                if (Session["SerachID"] == null)
                {
                    Session["SerachID"] = Guid.NewGuid().ToString();
                    st.SearchId = Convert.ToString(Session["SerachID"]);
                }
                else
                {
                    st.SearchId = Convert.ToString(Session["SerachID"]);
                }
            }
            BookedMR objBookMR = objBooking.MeetingroomList.Find(a => a.MRId == Convert.ToInt64(e.CommandArgument));
            objBooking.MeetingroomList.Remove(objBookMR);
            if (Request.QueryString["wl"] != null)
            {
                st.ChannelBy = "wl";
                st.ChannelId = Convert.ToString(Request.QueryString["wl"]);
                objBooking.BookType = "wl";
                objBooking.ChannelID = Convert.ToString(Request.QueryString["wl"]);
            }
            st.SearchObject = TrailManager.XmlSerialize(objBooking);
            bm.SaveSearch(st);
            BindMeetingroom();
        }
    }
    #endregion

    #region Language
    //This is used for language conversion for static contants.
    public string GetKeyResult(string key)
    {
        //if (XMLLanguage == null)
        //{
        //    XMLLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
        //}
        //XmlNode nodes = XMLLanguage.SelectSingleNode("items/item[@key='" + Key + "']");
        //return nodes.InnerText;
        return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    }
    #endregion

    #region Step of Time
    private string[] arrTime = { "00:00 am", "00:15 am", "00:30 am", "00:45 am", "01:00 am", "01:15 am", "01:30 am", "01:45 am", "02:00 am", "02:15 am", "02:30 am", "02:45 am", "03:00 am", "03:15 am", "03:30 am", "03:45 am", "04:00 am", "04:15 am", "04:30 am", "04:45 am", "05:00 am", "05:15 am", "05:30 am", "05:45 am", "06:00 am", "06:15 am", "06:30 am", "06:45 am", "07:00 am", "07:15 am", "07:30 am", "07:45 am", "08:00 am", "08:15 am", "08:30 am", "08:45 am", "09:00 am", "09:15 am", "09:30 am", "09:45 am", "10:00 am", "10:15 am", "10:30 am", "10:45 am", "11:00 am", "11:15 am", "11:30 am", "11:45 am", "12:00 pm", "12:15 pm", "12:30 pm", "12:45 pm", "01:00 pm", "01:15 pm", "01:30 pm", "01:45 pm", "02:00 pm", "02:15 pm", "02:30 pm", "02:45 pm", "03:00 pm", "03:15 pm", "03:30 pm", "03:45 pm", "04:00 pm", "04:15 pm", "04:30 pm", "04:45 pm", "05:00 pm", "05:15 pm", "05:30 pm", "05:45 pm", "06:00 pm", "06:15 pm", "06:30 pm", "06:45 pm", "07:00 pm", "07:15 pm", "07:30 pm", "07:45 pm", "08:00 pm", "08:15 pm", "08:30 pm", "08:45 pm", "09:00 pm", "09:15 pm", "09:30 pm", "09:45 pm", "10:00 pm", "10:15 pm", "10:30 pm", "10:45 pm", "11:00 pm", "11:15 pm", "11:30 pm", "11:45 pm", "12:00 pm", "12:15 pm", "12:30 pm", "12:45 pm" };
    public void BindDropdown(DropDownList drplst, string start, string end)
    {
        drplst.Items.Clear();
        bool bstart = false;
        bool bend = false;
        for (int i = 0; i < arrTime.Length; i++)
        {
            if (arrTime[i] == start)
            {
                drplst.Items.Add(new ListItem(arrTime[i], (i + 1).ToString()));
                bstart = true;
            }
            else if (arrTime[i] == end)
            {
                drplst.Items.Add(new ListItem(arrTime[i], (i + 1).ToString()));
                bend = true;
            }
            else if (bstart && !bend)
            {
                drplst.Items.Add(new ListItem(arrTime[i], (i + 1).ToString()));
            }
            else if (bstart && bend)
            {
                break;
            }
        }
    }
    #endregion

    protected void btnRegister_Click(object sender, EventArgs e)
    {

        Users objUser = new Users();
        UserDetails objUserDetail = new UserDetails();
        objUser.EmailId = txtEmail.Text;
        objUser.Password = "123456";
        objUser.FirstName = txtFirstName.Text;
        objUser.LastName = txtLastName.Text;
        objUser.IsfromWl = true;
        objUserDetail.CompanyName = txtCompanyName.Text;
        objUserDetail.Phone = txtphoneNo.Text.Trim();
        string result = newUserObject.register(objUser, objUserDetail);
        if (result == "Please activate you account by clicking on the link from the email message.")
        {

            Session["CurrentRestUserID"] = objUser.UserId;
            Session["CurrentRestUser"] = objUser;
            NextProcess();
            errorPopup.Attributes.Add("class", "error");
            errorPopup.Style.Add("display", "none");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "openRegisterORLoginPopUp", "openRegisterORLoginPopUp();", true);
            ScriptManager.RegisterStartupScript(this, typeof(Page), "showregistration", "showregistrationpopup('" + result + "');", true);
        }
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        Users objUser = objUserManager.LoginUser(txtLoginID.Text.Trim().Replace("'", ""), txtPassword.Text.Trim().Replace("'", ""));
        if (objUser == null)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "openRegisterORLoginPopUp", "openRegisterORLoginPopUp();", true);
            ScriptManager.RegisterStartupScript(this, typeof(Page), "showlogin", "showlogipopup('Invalid login / password');", true);

        }
        else
        {
            Session["CurrentRestUserID"] = objUser.UserId;
            Session["CurrentRestUser"] = objUser;
            NextProcess();
            errorPopup.Attributes.Add("class", "error");
            errorPopup.Style.Add("display", "none");
        }

    }
}