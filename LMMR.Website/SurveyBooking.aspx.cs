﻿#region NameSpaces

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using log4net;
using log4net.Config;
using System.IO;
using System.Configuration;
using LMMR.Data;
using System.Globalization;



#endregion

public partial class SurveyBooking : System.Web.UI.Page
{
    #region variable declaration
    Surveydata obj = new Surveydata();
    surveybookingrequest objsurveybookingrequest = new surveybookingrequest();
    ServeyAnswer insertsurvey = new ServeyAnswer();
    TList<ServeyAnswer> objsurvey = new TList<ServeyAnswer>();
    ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
    TList<ServeyQuestion> objsurveydesc = new TList<ServeyQuestion>();
    string status;
    string substatus;

    bool status1;
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            divmessage.Visible = false;
            BindHeadrepeter();
            int venueId = Convert.ToInt32(Request.QueryString["venue"]);
            int bookingId = Convert.ToInt32(Request.QueryString["bookingid"]);
            HotelInfo info= new HotelInfo();            
            lblVenueName.Text = "Venue Name : " + (info.GetHotelByHotelID(venueId) == null ? "" : info.GetHotelByHotelID(venueId).Name);
            lblDate.Text = "Meeting Date : " + (info.GetBookingByBookingID(bookingId) == null ? "" : Convert.ToDateTime(info.GetBookingByBookingID(bookingId).ArrivalDate).ToString("dd/MM/yy"));            
        }
    }

    public void BindHeadrepeter()
    {
        objsurveydesc = objsurveybookingrequest.GetAllsurveyDescription();
        rptheadQuestion.DataSource = objsurveydesc;
        rptheadQuestion.DataBind();
    }
    protected void rptheadQuestion_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ServeyQuestion surveydesc = e.Item.DataItem as ServeyQuestion;
            Label lblHeader = (Label)e.Item.FindControl("lblHeader");
            HiddenField hdfsurveyId = (HiddenField)e.Item.FindControl("hdfsurveyId");
            Repeater rptsubquestions = (Repeater)e.Item.FindControl("rptsubquestions");
            if (surveydesc != null)
            {
                lblHeader.Text = surveydesc.QuestionName;
                hdfsurveyId.Value = surveydesc.QuestionId.ToString();
                rptsubquestions.DataSource = objsurveybookingrequest.Getsurveybooking().Where(a => a.QuestionId == surveydesc.QuestionId).ToList();
                rptsubquestions.DataBind();
            }
        }
    }

    protected void rptsubquestions_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ServeyAnswer surveyanswer = e.Item.DataItem as ServeyAnswer;
            Label lblsubquestions = (Label)e.Item.FindControl("lblsubquestions");
            HiddenField hdfSubQsurveyId = (HiddenField)e.Item.FindControl("hdfSubQsurveyId");
            RadioButtonList rbtsurvey = (RadioButtonList)e.Item.FindControl("rbtsurvey");
            TextBox commentsText = (TextBox)e.Item.FindControl("commentsText");
            if (surveyanswer != null)
            {
                lblsubquestions.Text = surveyanswer.Answer;
                if (hdfSubQsurveyId.Value =="4")
                {
                    rbtsurvey.Visible = false;
                    commentsText.Visible = true;
                    hdfcommentstxt.Value = commentsText.Text;
                }
                else
                {
                    rbtsurvey.Visible = true;
                    commentsText.Visible = false;
                   // hdfcommentstxt.Value = "";
                }
                hdfSubQsurveyId.Value = surveyanswer.AnswerId.ToString();
            }
        }
    }
    protected void rptsubquestionscreated(object sender, RepeaterItemEventArgs e)
    {

    }

    protected void btn_submit_Click(object sender, EventArgs e)
    {
        int Userid = Convert.ToInt32(Request.QueryString["UserID"]);
        int venueid = Convert.ToInt32(Request.QueryString["venue"]);
        int bookingID = Convert.ToInt32(Request.QueryString["bookingID"]);
        //int bookingID = 17;
        Booking onjbooking = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(bookingID));
        if (onjbooking.IsSurveyDone == false)//Orginally 
        {
            onjbooking.IsSurveyDone = true;
            status1 = DataRepository.BookingProvider.Update(onjbooking);
            if (status1 == true)
            {
                Serveyresult res = new Serveyresult();
                ServeyResponse response = new ServeyResponse();
                response.UserId = Userid;
                response.VanueId = venueid;
                response.BookingId = bookingID;
                response.BookingType = Convert.ToInt32(onjbooking.BookType);

                if (hdfcommentstxt.Value != "")
                {
                    response.AdditionalComment = hdfcommentstxt.Value;
                    status = obj.insertSurvey(response);
                }
                else
                {
                    status = obj.insertSurvey(response);
                }

                if (status == "Thank You For The Survey")
                {
                    foreach (RepeaterItem item in rptheadQuestion.Items)
                    {
                        Repeater rptsubquestions = (Repeater)item.FindControl("rptsubquestions");

                        HiddenField hdfsurveyId = (HiddenField)item.FindControl("hdfsurveyId");
                        foreach (RepeaterItem childitem in rptsubquestions.Items)
                        {
                            HiddenField hdfSubQsurveyId = (HiddenField)childitem.FindControl("hdfSubQsurveyId");
                            RadioButtonList rbtsurvey = (RadioButtonList)childitem.FindControl("rbtsurvey");

                            TextBox commentsText = (TextBox)childitem.FindControl("commentsText");
                            ServeyResponse s = DataRepository.ServeyResponseProvider.GetByServeyId(Convert.ToInt32(response.ServeyId));
                            s.AdditionalComment=commentsText.Text;
                            DataRepository.ServeyResponseProvider.Update(s);
                            TList<ServeyAnswer> objans = objsurveybookingrequest.GetbyQuestionIdBooking(Convert.ToInt32(hdfSubQsurveyId.Value));
                            foreach (ServeyAnswer sa in objans)
                            {
                                if (Convert.ToInt32(hdfsurveyId.Value) != 4)
                                {
                                    res.QuestionId = Convert.ToInt32(hdfsurveyId.Value);
                                    res.AnswerId = sa.AnswerId;
                                    res.Rating = Convert.ToInt32(rbtsurvey.SelectedValue);
                                    res.ServeyId = response.ServeyId;
                                    substatus = obj.insertSurvey(res);
                                }
                                

                            }
                            if (substatus == "Thank You For The Survey")
                            {

                                divsurvey.Visible = true;
                                divmessage.Visible = true;
                                divmessage.Style.Add("display", "block");
                                divmessage.Attributes.Add("class", "succesfuly");
                                divmessage.InnerHtml = status;

                            }
                        }
                    }
                }
                else
                {
                    divsurvey.Visible = true;
                    divmessage.Visible = true;
                    divmessage.Attributes.Add("class", "error");
                    divmessage.Style.Add("display", "block");
                    divmessage.InnerHtml = "Survey did not Save!";

                }


            }
            else
            {
                divsurvey.Visible = true;
                divmessage.Visible = true;
                divmessage.Attributes.Add("class", "error");
                divmessage.Style.Add("display", "block");
                divmessage.InnerHtml = "Survey did not Save!";

            }
        }
        else
        {
            status = "Survey already done";
            divsurvey.Visible = true;
            divmessage.Visible = true;
            divmessage.Attributes.Add("class", "error");
            divmessage.Style.Add("display", "block");
            divmessage.InnerHtml = status;
        }


    }
}