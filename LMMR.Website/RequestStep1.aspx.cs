﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using log4net;
using System.Configuration;

public partial class RequestStep1 : BasePage
{
    #region Variables and Properties
    HotelManager objHotel = new HotelManager();
    Hotel objRequestHotel = new Hotel();
    int countMr = 0;
    CreateRequest objRequest = null;
    BookingManager bm = new BookingManager();
    NewUser newUserObject = new NewUser();
    UserManager objUserManager = new UserManager();
    //CreateRequest objRequest = null;
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                //PreapareSearch();
                if (Session["CurrentRestUser"] != null)
                {
                    //Bind Login users details
                    Users objUsers = Session["CurrentRestUser"] as Users;
                    pnlLogin.Visible = true;
                    lblDateLogin.Text = DateTime.Now.ToString("dd MMM yyyy");
                    lblUserName.Text = objUsers.FirstName + " " + objUsers.LastName;
                }
                else
                {
                    pnlLogin.Visible = false;
                }
                CheckSearchAvailable();
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion
    
    #region Additional Method Bind Meetingroom search
    public void CheckSearchAvailable()
    {
        //if user login then Move inside the if loop
        if (Session["CurrentRestUserID"] != null)
        {            
            if (Session["RequestID"] != null)
            {
                SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["RequestID"]));
                if (st != null)
                {
                    if (st.CurrentStep == 1)
                    {
                        objRequest = (CreateRequest)TrailManager.XmlDeserialize(typeof(CreateRequest), st.SearchObject);
                        BindHotel();
                    }
                    else if (st.CurrentStep == 2)
                    {
                        if (Request.QueryString["wl"] == null)
                        {
                            //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                            if (l != null)
                            {
                                Response.Redirect(SiteRootPath + "request-step2/" + l.Name.ToLower());
                            }
                            else
                            {
                                Response.Redirect(SiteRootPath + "request-step2/english");
                            }
                            //Response.Redirect("RequestStep2.aspx", false);
                        }
                        else
                        {
                            Response.Redirect(SiteRootPath + "requeststep2.aspx?" + Request.RawUrl.Split('?')[1]);
                        }
                    }
                    else if (st.CurrentStep == 3)
                    {
                        if (Request.QueryString["wl"] == null)
                        {
                            //Response.Redirect("RequestStep3.aspx", false);
                            //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                            if (l != null)
                            {
                                Response.Redirect(SiteRootPath + "request-step3/" + l.Name.ToLower());
                            }
                            else
                            {
                                Response.Redirect(SiteRootPath + "request-step3/english");
                            }
                        }
                        else
                        {
                            Response.Redirect(SiteRootPath + "requeststep3.aspx?" + Request.RawUrl.Split('?')[1]);
                        }
                    }
                    else if (st.CurrentStep == 4)
                    {
                        //Response.Redirect("RequestStep4.aspx", false);
                        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                        if (Request.QueryString["wl"] == null)
                        {
                            if (l != null)
                            {
                                Response.Redirect(SiteRootPath + "request-step4/" + l.Name.ToLower());
                            }
                            else
                            {
                                Response.Redirect(SiteRootPath + "request-step4/english");
                            }
                        }
                        else
                        {
                            Response.Redirect(SiteRootPath + "requeststep4.aspx?" + Request.RawUrl.Split('?')[1]);
                        }
                    }
                }
                else
                {
                    //Response.Redirect("Default.aspx", false);
                    //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                    if (l != null)
                    {
                        Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
                    }
                    else
                    {
                        Response.Redirect(SiteRootPath + "default/english");
                    }
                }
            }
            else if (Session["Request"] != null) // vaibhav : search id session gets null id back button of browser pressed or an error occurs
            {
                objRequest = Session["Request"] as CreateRequest;
                BindHotel();
            }
        }
        else if (Session["Request"] != null)
        {
            objRequest = Session["Request"] as CreateRequest;
            BindHotel();
        }
        else
        {
            //Response.Redirect("Default.aspx", false);
            //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
            if (l != null)
            {
                Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
            }
            else
            {
                Response.Redirect(SiteRootPath + "default/english");
            }
        }
    }
    public void BindHotel()
    {
        try
        {
            if (objRequest == null)
            {
                objRequest = Session["Request"] as CreateRequest;
            }
            if (objRequest == null)
            {
                //Response.Redirect("~/Default.aspx");
                //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "default/english");
                }
            }
            else
            {
                rptDays.DataSource = objRequest.DaysList;
                rptDays.DataBind();
                List<BuildHotelsRequest> objHotelsrequest = objRequest.HotelList.Where(a => a.MeetingroomList.Count > 0).ToList();
                if (objHotelsrequest.Count > 0)
                {
                    dtLstHotel.DataSource = objHotelsrequest;
                    dtLstHotel.DataBind();
                }
                else
                {
                    Response.Redirect("Default.aspx", false);
                }
                lblArrival.Text = objRequest.ArivalDate.ToString("dd MMM yyyy");
                lblDeparture.Text = objRequest.DepartureDate.ToString("dd MMM yyyy");
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Prepare Search step

    
    //Prepare Search for Session to recive(this part is recived by Search Last step)
    public void PreapareSearch()
    {
        CreateRequest objPrepareRequest = new CreateRequest();
        objPrepareRequest.RequestID = "";
        objPrepareRequest.Duration = 2;
        objPrepareRequest.ArivalDate = DateTime.Now.AddDays(8);
        objPrepareRequest.DepartureDate = objPrepareRequest.ArivalDate.AddDays(objPrepareRequest.Duration-1);
        objPrepareRequest.CurrentUserId = 0;
        if(objPrepareRequest.DaysList == null)
        {
            objPrepareRequest.DaysList = new List<NumberOfDays>();
        }
        for (int i = 0; i < objPrepareRequest.Duration; i++)
        {
            NumberOfDays objNoDays = new NumberOfDays();
            objNoDays.SelectedDay = i + 1;
            objNoDays.SelectTime = 0;
            objNoDays.StartTime = objNoDays.SelectTime == 0 ? "00:00 am" : (objNoDays.SelectTime == 1 ? "00:00 am" : "12:00 pm");
            objNoDays.EndTime = objNoDays.SelectTime == 0 ? "11:30 pm" : (objNoDays.SelectTime == 1 ? "12:00 pm" : "11:30 pm"); ;
            objPrepareRequest.DaysList.Add(objNoDays);
        }
        if (objPrepareRequest.HotelList == null)
        {
            objPrepareRequest.HotelList = new List<BuildHotelsRequest>();
        }
        BuildHotelsRequest obj1Hotel = new BuildHotelsRequest();
        obj1Hotel.HotelID = 11;
        if (obj1Hotel.MeetingroomList == null)
        {
            obj1Hotel.MeetingroomList = new List<BuildMeetingRoomRequest>();
        }
        BuildMeetingRoomRequest obj1mr1 = new BuildMeetingRoomRequest();
        obj1mr1.ConfigurationID = 1176;
        obj1mr1.MeetingRoomID = 245;
        obj1mr1.Quantity = 50;
        obj1mr1.IsMain = true;
        obj1Hotel.MeetingroomList.Add(obj1mr1);
        BuildMeetingRoomRequest obj1mr2 = new BuildMeetingRoomRequest();
        obj1mr2.ConfigurationID = 1181;
        obj1mr2.MeetingRoomID = 246;
        obj1mr2.Quantity = 55;
        obj1mr2.IsMain = false;
        obj1Hotel.MeetingroomList.Add(obj1mr2);
        objPrepareRequest.HotelList.Add(obj1Hotel);

        BuildHotelsRequest obj2Hotel = new BuildHotelsRequest();
        obj2Hotel.HotelID = 31;
        if (obj2Hotel.MeetingroomList == null)
        {
            obj2Hotel.MeetingroomList = new List<BuildMeetingRoomRequest>();
        }
        BuildMeetingRoomRequest obj2mr1 = new BuildMeetingRoomRequest();
        obj2mr1.ConfigurationID = 1502;
        obj2mr1.MeetingRoomID = 310;
        obj2mr1.Quantity = 50;
        obj2mr1.IsMain = true;
        obj2Hotel.MeetingroomList.Add(obj2mr1);
        BuildMeetingRoomRequest obj2mr2 = new BuildMeetingRoomRequest();
        obj2mr2.ConfigurationID = 1508;
        obj2mr2.MeetingRoomID = 311;
        obj2mr2.Quantity = 55;
        obj2mr2.IsMain = false;
        obj2Hotel.MeetingroomList.Add(obj2mr2);
        objPrepareRequest.HotelList.Add(obj2Hotel);

        BuildHotelsRequest obj3Hotel = new BuildHotelsRequest();
        obj3Hotel.HotelID = 46;
        if (obj3Hotel.MeetingroomList == null)
        {
            obj3Hotel.MeetingroomList = new List<BuildMeetingRoomRequest>();
        }
        BuildMeetingRoomRequest obj3mr1 = new BuildMeetingRoomRequest();
        obj3mr1.ConfigurationID = 1838;
        obj3mr1.MeetingRoomID = 377;
        obj3mr1.Quantity = 50;
        obj3mr1.IsMain = true;
        obj3Hotel.MeetingroomList.Add(obj3mr1);
        BuildMeetingRoomRequest obj3mr2 = new BuildMeetingRoomRequest();
        obj3mr2.ConfigurationID = 1844;
        obj3mr2.MeetingRoomID = 378;
        obj3mr2.Quantity = 55;
        obj3mr2.IsMain = false;
        obj3Hotel.MeetingroomList.Add(obj3mr2);
        objPrepareRequest.HotelList.Add(obj3Hotel);

        Session["Request"] = objPrepareRequest;
    }
    #endregion

    #region Event
    protected void rptDays_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblDuration = (Label)e.Item.FindControl("lblDuration");
            Label lblDay = (Label)e.Item.FindControl("lblDay");
            DropDownList drpSelectedTime = (DropDownList)e.Item.FindControl("drpSelectedTime");
            if (drpSelectedTime.Items.Count <= 0)
            {
                drpSelectedTime.Items.Insert(0, new ListItem(GetKeyResult("FULLDAY"), "0"));
                drpSelectedTime.Items.Insert(1, new ListItem(GetKeyResult("MORNING"), "1"));
                drpSelectedTime.Items.Insert(2, new ListItem(GetKeyResult("AFTERNOON"), "2"));
            }
            DropDownList drpStart = (DropDownList)e.Item.FindControl("drpStart");
            BindDropdown(drpStart);
            DropDownList drpEnd = (DropDownList)e.Item.FindControl("drpEnd");
            BindDropdown(drpEnd);
            NumberOfDays objNoofDay = e.Item.DataItem as NumberOfDays;
            if (objNoofDay != null)
            {
                lblDuration.Text = objRequest.Duration==1?objRequest.Duration + GetKeyResult("DAY"):objRequest.Duration + GetKeyResult("DAY");
                lblDay.Text = objNoofDay.SelectedDay.ToString();
                drpSelectedTime.Items.FindByValue(objNoofDay.SelectTime.ToString()).Selected = true;
                if (drpStart.Items.FindByText(objNoofDay.StartTime) != null)
                {
                    drpStart.Items.FindByText(objNoofDay.StartTime).Selected = true;
                }
                if (drpEnd.Items.FindByText(objNoofDay.EndTime) != null)
                {
                    drpEnd.Items.FindByText(objNoofDay.EndTime).Selected = true;
                }
            }
        }
    }
    /// <summary>
    /// Bind Meeting room details get by Search.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dtLstMeetingroom_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblIndex = (Label)e.Item.FindControl("lblIndex");
            Label lblMeetingRoomName = (Label)e.Item.FindControl("lblMeetingRoomName");
            ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");
            Image imgMrImage = (Image)e.Item.FindControl("imgMrImage");
            Label lblConfigurationType = (Label)e.Item.FindControl("lblConfigurationType");
            Label lblMaxandMinCapacity = (Label)e.Item.FindControl("lblMaxandMinCapacity");
            HyperLink hypPlan = (HyperLink)e.Item.FindControl("hypPlan");
            RadioButton rbtnMain = (RadioButton)e.Item.FindControl("rbtnMain");
            RadioButton rbtnBreakDown = (RadioButton)e.Item.FindControl("rbtnBreakDown");
            Label lblMain = (Label)e.Item.FindControl("lblMain");
            Label lblBreakDown = (Label)e.Item.FindControl("lblBreakDown");
            TextBox txtQuantity = (TextBox)e.Item.FindControl("txtQuantity");
            HiddenField hdnMeetingRoomID = (HiddenField)e.Item.FindControl("hdnMeetingRoomID");

            BuildMeetingRoomRequest objb = e.Item.DataItem as BuildMeetingRoomRequest;
            if (objb != null)
            {
                lblIndex.Text = (e.Item.ItemIndex + 1).ToString();
                hdnMeetingRoomID.Value = Convert.ToString(objb.MeetingRoomID);
                MeetingRoom objMeetingroom = objHotel.GetMeetingRoomDetailsById(objb.MeetingRoomID);
                MeetingRoomDesc objDesc = objMeetingroom.MeetingRoomDescCollection.Where(a => a.LanguageId == Convert.ToInt32(Session["LanguageID"])).FirstOrDefault();
                MeetingRoomConfig objMrConfig = objMeetingroom.MeetingRoomConfigCollection.Where(a => a.Id == objb.ConfigurationID).FirstOrDefault();
                if (objMeetingroom != null)
                {
                    //Fill All meeting room details.
                    lblMeetingRoomName.Text = objMeetingroom.Name;
                    imgMrImage.ImageUrl = (string.IsNullOrEmpty(objMeetingroom.Picture) ? ConfigurationManager.AppSettings["FilePath"] + "HotelImage/default_hotel_logo.png" : ConfigurationManager.AppSettings["FilePath"] + "MeetingRoomImage/" + objMeetingroom.Picture);
                    if (objMrConfig != null)
                    {
                        //Bind the child repeater with the datails of days
                        lblConfigurationType.Text = (objMrConfig.RoomShapeId == (int)RoomShape.Boardroom ? RoomShape.Boardroom.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Classroom ? RoomShape.Classroom.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Cocktail ? RoomShape.Cocktail.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.School ? RoomShape.School.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Theatre ? RoomShape.Theatre.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.UShape ? RoomShape.UShape.ToString() : RoomShape.Boardroom.ToString());
                        lblMaxandMinCapacity.Text = objMrConfig.MinCapacity + " - " + objMrConfig.MaxCapicity;
                    }
                    if (objMeetingroom.MrPlan != "")
                    {
                        hypPlan.NavigateUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "Plan/" + objMeetingroom.MrPlan;
                        hypPlan.ToolTip = objMeetingroom.MrPlan;
                        hypPlan.Target = "_blank";
                        hypPlan.Text = "See plan";
                        hypPlan.Visible = true;
                    }
                    else
                    {
                        hypPlan.Visible = false;
                        hypPlan.Text = "See Plan";
                    }
                    
                    rbtnMain.Checked = objb.IsMain;
                    rbtnBreakDown.Checked = !objb.IsMain;
                    txtQuantity.Text = Convert.ToString(objb.Quantity);
                    if (countMeetingRoom == 1)
                    {
                        rbtnMain.Visible = false;
                        rbtnBreakDown.Visible = false;
                        lblMain.Visible = false;
                        lblBreakDown.Visible = false;
                    }
                    btnDelete.CommandArgument = objMeetingroom.Id + "," + objMeetingroom.HotelId;
                }
            }
        }
    }
    HotelManager objHotelManager = new HotelManager();
    public int countMeetingRoom
    {
        get;
        set;
    }
    /// <summary>
    /// Bind Hotel detail get by search
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dtLstHotel_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater dtLstMeetingroom = (Repeater)e.Item.FindControl("dtLstMeetingroom");
            Label lblIndex = (Label)e.Item.FindControl("lblIndex");
            Label lblHotelname = (Label)e.Item.FindControl("lblHotelname");
            Image imgStars = (Image)e.Item.FindControl("imgStars");
            Label lblAddress = (Label)e.Item.FindControl("lblAddress");
            HiddenField hdnLong = (HiddenField)e.Item.FindControl("hdnLong");
            HiddenField hdnLat = (HiddenField)e.Item.FindControl("hdnLat");
            HiddenField hdnHotelID = (HiddenField)e.Item.FindControl("hdnHotelID");
            //Bind with meetingroom details
            BuildHotelsRequest objReq = e.Item.DataItem as BuildHotelsRequest;
            if (objReq != null)
            {
                lblIndex.Text = (e.Item.ItemIndex + 1).ToString();
                Hotel objHotel = objHotelManager.GetHotelDetailsById(objReq.HotelID);
                hdnHotelID.Value = Convert.ToString(objReq.HotelID);
                lblHotelname.Text = objHotel.Name;
                HotelDesc objDesc = objHotel.HotelDescCollection.Where(a=>a.LanguageId == Convert.ToInt64(Session["LanguageID"])).FirstOrDefault();
                if (objDesc == null)
                {
                    lblAddress.Text = objHotel.HotelAddress;
                }
                else
                {
                    lblAddress.Text = objDesc.Address;
                }
                hdnLong.Value = objHotel.Longitude;
                hdnLat.Value = objHotel.Latitude;
                //for static hotel star
                if (objHotel.Stars == 1)
                {
                    imgStars.ImageUrl = "~/Images/1.png";
                }
                else if (objHotel.Stars == 2)
                {
                    imgStars.ImageUrl = "~/Images/2.png";
                }
                else if (objHotel.Stars == 3)
                {
                    imgStars.ImageUrl = "~/Images/3.png";
                }
                else if (objHotel.Stars == 4)
                {
                    imgStars.ImageUrl = "~/Images/4.png";
                }
                else if (objHotel.Stars == 5)
                {
                    imgStars.ImageUrl = "~/Images/5.png";
                }
                else if (objHotel.Stars == 6)
                {
                    imgStars.ImageUrl = "~/Images/6.png";
                }
                else if (objHotel.Stars == 7)
                {
                    imgStars.ImageUrl = "~/Images/7.png";
                }
                countMeetingRoom = objReq.MeetingroomList.Count;
                dtLstMeetingroom.DataSource = objReq.MeetingroomList;
                dtLstMeetingroom.DataBind();
            }
        }
    }

    #endregion

    #region Step of Time
    private string[] arrTime = { "00:00 am", "00:15 am", "00:30 am", "00:45 am", "01:00 am", "01:15 am", "01:30 am", "01:45 am", "02:00 am", "02:15 am", "02:30 am", "02:45 am", "03:00 am", "03:15 am", "03:30 am", "03:45 am", "04:00 am", "04:15 am", "04:30 am", "04:45 am", "05:00 am", "05:15 am", "05:30 am", "05:45 am", "06:00 am", "06:15 am", "06:30 am", "06:45 am", "07:00 am", "07:15 am", "07:30 am", "07:45 am", "08:00 am", "08:15 am", "08:30 am", "08:45 am", "09:00 am", "09:15 am", "09:30 am", "09:45 am", "10:00 am", "10:15 am", "10:30 am", "10:45 am", "11:00 am", "11:15 am", "11:30 am", "11:45 am", "12:00 pm", "12:15 pm", "12:30 pm", "12:45 pm", "01:00 pm", "01:15 pm", "01:30 pm", "01:45 pm", "02:00 pm", "02:15 pm", "02:30 pm", "02:45 pm", "03:00 pm", "03:15 pm", "03:30 pm", "03:45 pm", "04:00 pm", "04:15 pm", "04:30 pm", "04:45 pm", "05:00 pm", "05:15 pm", "05:30 pm", "05:45 pm", "06:00 pm", "06:15 pm", "06:30 pm", "06:45 pm", "07:00 pm", "07:15 pm", "07:30 pm", "07:45 pm", "08:00 pm", "08:15 pm", "08:30 pm", "08:45 pm", "09:00 pm", "09:15 pm", "09:30 pm", "09:45 pm", "10:00 pm", "10:15 pm", "10:30 pm", "10:45 pm", "11:00 pm", "11:15 pm", "11:30 pm", "11:45 pm", "12:00 pm", "12:15 pm", "12:30 pm", "12:45 pm" };
    public void BindDropdown(DropDownList drplst)
    {
        drplst.Items.Clear();
        for (int i = 0; i < arrTime.Length; i++)
        {
            drplst.Items.Add(new ListItem(arrTime[i], (i + 1).ToString()));
        }
    }
    #endregion

    public void fillStaticData()
    {
        for (int i = 0; i < rptDays.Items.Count; i++)
        {
            DropDownList drpSelectedTime = (DropDownList)rptDays.Items[i].FindControl("drpSelectedTime");
            drpSelectedTime.Items.Add(new ListItem("Full Day", "0"));
            drpSelectedTime.Items.Add(new ListItem("Morning", "1"));
            drpSelectedTime.Items.Add(new ListItem("Afternoon", "2"));
        }
    }
    protected void dtLstMeetingroom_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "DeleteMeetingroom")
        {
            SearchTracer st = null;
            if (objRequest == null)
            {
                if (Session["RequestID"] == null)
                {
                    objRequest = Session["Request"] as CreateRequest;
                }
                else
                {
                    st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["RequestID"]));
                    objRequest = (CreateRequest)TrailManager.XmlDeserialize(typeof(CreateRequest), st.SearchObject);
                }
            }
            string allval = e.CommandArgument.ToString();
            BuildMeetingRoomRequest objMeetingRoom = objRequest.HotelList.Where(a => a.HotelID == Convert.ToInt64(allval.Split(',')[1])).FirstOrDefault().MeetingroomList.Where(a => a.MeetingRoomID == Convert.ToInt64(allval.Split(',')[0])).FirstOrDefault();
            objRequest.HotelList.Where(a => a.HotelID == Convert.ToInt64(allval.Split(',')[1])).FirstOrDefault().MeetingroomList.Remove(objMeetingRoom);
            //if (objRequest.HotelList.Where(a => a.HotelID == Convert.ToInt64(allval.Split(',')[1])).FirstOrDefault().MeetingroomList.Count > 0)
            //{
            //    objRequest.HotelList.Where(a => a.HotelID == Convert.ToInt64(allval.Split(',')[1])).FirstOrDefault().MeetingroomList[0].IsMain = true;
            //}
            if (Request.QueryString["wl"] != null)
            {
                st.ChannelBy = "wl";
                st.ChannelId = Convert.ToString(Request.QueryString["wl"]);
                objRequest.BookType = "wl";
                objRequest.ChannelID = Convert.ToString(Request.QueryString["wl"]);
            }
            Session["Request"] = objRequest;
            if (st != null)
            {
                st.SearchObject = TrailManager.XmlSerialize(objRequest);
                bm.SaveSearch(st);
            }
            BindHotel();
        }
    }
    protected void lnkNext_Click(object sender, EventArgs e)
    {
        NextProcess();
    }

    public void NextProcess()
    {
        SearchTracer st = null;
        if (objRequest == null)
        {
            if (Session["RequestID"] == null)
            {
                objRequest = Session["Request"] as CreateRequest;
            }
            else
            {
                st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["RequestID"]));
                objRequest = (CreateRequest)TrailManager.XmlDeserialize(typeof(CreateRequest), st.SearchObject);
            }
        }
        for (int i = 0; i < rptDays.Items.Count; i++)
        {
            Label lblDuration = (Label)rptDays.Items[i].FindControl("lblDuration");
            Label lblDay = (Label)rptDays.Items[i].FindControl("lblDay");
            DropDownList drpStart = (DropDownList)rptDays.Items[i].FindControl("drpStart");
            DropDownList drpEnd = (DropDownList)rptDays.Items[i].FindControl("drpEnd");
            DropDownList drpSelectedTime = (DropDownList)rptDays.Items[i].FindControl("drpSelectedTime");
            objRequest.DaysList.Where(a => a.SelectedDay == Convert.ToInt32(lblDay.Text)).FirstOrDefault().SelectTime = Convert.ToInt32(drpSelectedTime.SelectedValue);
            objRequest.DaysList.Where(a => a.SelectedDay == Convert.ToInt32(lblDay.Text)).FirstOrDefault().StartTime = drpStart.SelectedItem.Text;
            objRequest.DaysList.Where(a => a.SelectedDay == Convert.ToInt32(lblDay.Text)).FirstOrDefault().EndTime = drpEnd.SelectedItem.Text;
        }
        for (int j = 0; j < dtLstHotel.Items.Count; j++)
        {
            HiddenField hdnHotelID = (HiddenField)dtLstHotel.Items[j].FindControl("hdnHotelID");
            Repeater dtLstMeetingroom = (Repeater)dtLstHotel.Items[j].FindControl("dtLstMeetingroom");
            for (int k = 0; k < dtLstMeetingroom.Items.Count; k++)
            {
                RadioButton rbtnMain = (RadioButton)dtLstMeetingroom.Items[k].FindControl("rbtnMain");
                RadioButton rbtnBreakDown = (RadioButton)dtLstMeetingroom.Items[k].FindControl("rbtnBreakDown");
                TextBox txtQuantity = (TextBox)dtLstMeetingroom.Items[k].FindControl("txtQuantity");
                HiddenField hdnMeetingRoomID = (HiddenField)dtLstMeetingroom.Items[k].FindControl("hdnMeetingRoomID");
                objRequest.HotelList.Where(a => a.HotelID == Convert.ToInt64(hdnHotelID.Value)).FirstOrDefault().MeetingroomList.Where(a => a.MeetingRoomID == Convert.ToInt64(hdnMeetingRoomID.Value)).FirstOrDefault().Quantity = Convert.ToInt32(string.IsNullOrEmpty(txtQuantity.Text) ? "0" : txtQuantity.Text);
                objRequest.HotelList.Where(a => a.HotelID == Convert.ToInt64(hdnHotelID.Value)).FirstOrDefault().MeetingroomList.Where(a => a.MeetingRoomID == Convert.ToInt64(hdnMeetingRoomID.Value)).FirstOrDefault().IsMain = rbtnMain.Checked;
            }
        }


        if (st != null)
        {
            //objSearch.SearchId = Convert.ToString(Session["SerachID"]);
        }
        else
        {
            st = new SearchTracer();
            st.SearchId = Guid.NewGuid().ToString();
        }

        if (Session["CurrentRestUserID"] != null)
        {
            st.UserId = Convert.ToInt64(Session["CurrentRestUserID"]);
            objRequest.CurrentUserId = st.UserId.Value;
        }
        else
        {
            st.UserId = null;
            objRequest.CurrentUserId = 0;
        }

        Session["Request"] = objRequest;
        if (Request.QueryString["wl"] != null)
        {
            st.ChannelBy = "wl";
            st.ChannelId = Convert.ToString(Request.QueryString["wl"]);
            objRequest.BookType = "wl";
            objRequest.ChannelID = Convert.ToString(Request.QueryString["wl"]);
        }
        string serailstring = TrailManager.XmlSerialize(objRequest);
        st.SearchObject = serailstring;

        st.CurrentStep = 2;
        st.IsBooking = false;
        st.CurrentDay = 1;
        if (bm.SaveSearch(st))
        {
            Session["RequestID"] = st.SearchId;
            if (Request.QueryString["wl"] == null)
            {
                if (Session["CurrentRestUserID"] != null)
                {
                    //Response.Redirect("RequestStep2.aspx",false);
                    //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                    if (l != null)
                    {
                        Response.Redirect(SiteRootPath + "request-step2/" + l.Name.ToLower());
                    }
                    else
                    {
                        Response.Redirect(SiteRootPath + "request-step2/english");
                    }
                }
                else
                {
                    //Response.Redirect("login.aspx", false);
                    //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                    if (l != null)
                    {
                        Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                    }
                    else
                    {
                        Response.Redirect(SiteRootPath + "login/english");
                    }
                }
            }
            else
            {
                if (Session["CurrentRestUserID"] != null)
                {
                    Response.Redirect(SiteRootPath + "requeststep2.aspx?" + Request.RawUrl.Split('?')[1]);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "openRegisterORLoginPopUp", "openRegisterORLoginPopUp();", true);
                }
            }
        }
        else
        {
            //Error Message No Search Record Updated. 
        }
    }
    #region Language
    //This is used for language conversion for static contants.
    public string GetKeyResult(string key)
    {
        //if (XMLLanguage == null)
        //{
        //    XMLLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
        //}
        //XmlNode nodes = XMLLanguage.SelectSingleNode("items/item[@key='" + Key + "']");
        //return nodes.InnerText;//
        return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    }
    #endregion



    protected void btnRegister_Click(object sender, EventArgs e)
    {

        Users objUser = new Users();
        UserDetails objUserDetail = new UserDetails();
        objUser.EmailId = txtEmail.Text;
        objUser.Password = "123456";
        objUser.FirstName = txtFirstName.Text;
        objUser.LastName = txtLastName.Text;
        objUser.IsfromWl = true;
        objUserDetail.CompanyName = txtCompanyName.Text;
        objUserDetail.Phone = txtphoneNo.Text.Trim();
        string result = newUserObject.register(objUser, objUserDetail);
        if (result == "Please activate you account by clicking on the link from the email message.")
        {

            Session["CurrentRestUserID"] = objUser.UserId;
            Session["CurrentRestUser"] = objUser;
            NextProcess();
            errorPopup.Attributes.Add("class", "error");
            errorPopup.Style.Add("display", "none");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "openRegisterORLoginPopUp", "openRegisterORLoginPopUp();", true);
            ScriptManager.RegisterStartupScript(this, typeof(Page), "showregistration", "showregistrationpopup('" + result + "');", true);
        }
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        Users objUser = objUserManager.LoginUser(txtLoginID.Text.Trim().Replace("'", ""), txtPassword.Text.Trim().Replace("'", ""));
        if (objUser == null)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "openRegisterORLoginPopUp", "openRegisterORLoginPopUp();", true);
            ScriptManager.RegisterStartupScript(this, typeof(Page), "showlogin", "showlogipopup('Invalid login / password');", true);

        }
        else
        {
            Session["CurrentRestUserID"] = objUser.UserId;
            Session["CurrentRestUser"] = objUser;
            NextProcess();
            errorPopup.Attributes.Add("class", "error");
            errorPopup.Style.Add("display", "none");
        }

    }
}