﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using System.IO;
using System.Configuration;
using log4net;
using log4net.Config;
using System.Xml;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Net.Mail;
using System.Net.Mime;
using System.Web.UI.HtmlControls;

public partial class BookingStep4 : BasePage
{
    #region Variable
    CreditCardManager objCreditCardManager = new CreditCardManager();

    Int64 intHotelID = 0;
    Int64 intUserID = 0;
    BookingManager bm = new BookingManager();
    Createbooking objBooking = null;
    ManageWhiteLabel objwlManager = new ManageWhiteLabel();
    public string FaxNumber
    {
        get { return Convert.ToString(ViewState["FaxNumber"]); }
        set { ViewState["FaxNumber"] = value; }
    }



    VList<ViewBookingHotels> vlist;
    VList<Viewbookingrequest> vlistreq;
    ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
    HotelManager objHotelManager = new HotelManager();
    string Typelink = "", AdminUser = "", Hotelid = "0";

    WizardLinkSettingManager ObjWizardManager = new WizardLinkSettingManager();

    CurrencyManager cm = new CurrencyManager();
    HotelManager objHotel = new HotelManager();
    public string CurrencySign
    {
        get;
        set;
    }
    public XmlDocument XMLLanguage
    {
        get { return (XmlDocument)ViewState["Language"]; }
        set { ViewState["Language"] = value; }
    }
    public string UserCurrency
    {
        get { return "1"; }
        set { ViewState["UserCurrency"] = "1"; }
    }
    public string HotelCurrency
    {
        get { return "1"; }
        set { ViewState["HotelCurrency"] = "1"; }
    }
    public decimal CurrencyConvert
    {
        get { return 1; }
        set { ViewState["CurrencyConvert"] = 1; }
    }
    public decimal CurrentLat
    {
        get;
        set;
    }
    public decimal CurrentLong
    {
        get;
        set;
    }
    public Int64 CurrentMeetingRoomID
    {
        get;
        set;
    }
    public Int64 CurrentMeetingRoomConfigureID
    {
        get;
        set;
    }
    public int CurrentPackageType
    {
        get;
        set;
    }
    public List<ManagePackageItem> CurrentPackageItem
    {
        get;
        set;
    }
    public TList<PackageItems> objHp
    {
        get;
        set;
    }
    public bool IsConvertToRequest
    {
        get
        {
            return (ViewState["IsConvertToRequest"] == null ? false : Convert.ToBoolean(ViewState["IsConvertToRequest"]));
        }
        set
        {
            ViewState["IsConvertToRequest"] = value;
        }
    }
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!HttpContext.Current.Request.IsSecureConnection)
        //{
        //    Response.Redirect(Request.Url.AbsoluteUri.Replace("http://", "https://"), true);
        //    return;
        //}

        if (Session["CurrentRestUserID"] == null)
        {
            //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
            if (Request.QueryString["wl"] == null)
            {
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "default/english");
                }
                //Response.Redirect("Login.aspx");
            }
            else
            {

            }
        }
        if (!IsPostBack)
        {
            try
            {
                Users objUsers = Session["CurrentRestUser"] as Users;
                intUserID = objUsers.UserId;
                //Bind Login users details
                lblLoginTime.Text = DateTime.Now.ToString("dd MMM yyyy");
                lblLoginUser.Text = objUsers.FirstName + " " + objUsers.LastName;
                if (Session["SerachID"] != null)
                {
                    SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
                    objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
                    if ((st.IsSentAsRequest == null ? false : st.IsSentAsRequest) == true)
                    {
                        divWarnning.InnerHtml = GetKeyResult("YOURBOOKINGISCONVERTINTOREQUEST");//"Your booking is now converted into request.";
                        divWarnning.Attributes.Add("style", "display:block;");
                        IsConvertToRequest = true;
                    }
                    else
                    {
                        IsConvertToRequest = false;
                    }
                    BindBooking();
                    DateTime startTime = DateTime.Now;
                    DateTime endTime = Convert.ToDateTime(objBooking.ArivalDate);
                    TimeSpan span = endTime.Subtract(startTime);
                    intHotelID = objBooking.HotelID;
                    Hotel objhotels = new HotelManager().GetHotelDetailsById(intHotelID);
                    if (objhotels != null)
                    {
                        FaxNumber = objhotels.FaxNumber;
                    }
                    //if (span.Days < 7) //changes by Manas 
                    if (span.Days < 7 && (st.IsSentAsRequest == null ? false : st.IsSentAsRequest) == false)
                    {
                        pnlcreditcard.Visible = true;
                        BindYear();
                        BindCountry();
                        BindCreditCard();
                        divmessage.Style.Add("display", "none");
                        divmessage.Attributes.Add("class", "error");

                        //This is static value for now.
                        // This values will be get by logged user.
                        lblName.Text = objUsers.FirstName;
                        lblEmail.Text = objUsers.EmailId;
                    }
                    else
                    {
                        pnlcreditcard.Visible = false;
                        #region bookingpdf


                        Divdetails.Visible = false;
                        Divdetails.Visible = true;

                        StringWriter sw = new StringWriter();
                        HtmlTextWriter hw = new HtmlTextWriter(sw);
                        PrepareDivForExport(Divdetails);
                        Divdetails.RenderControl(hw);
                        StringReader sr = new StringReader(sw.ToString());
                        Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);

                        Divdetails.Visible = false;
                        var dirPath = Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "Temp/");
                        var filePath = dirPath + Guid.NewGuid().ToString().Replace("-", string.Empty) + ".pdf";

                        try
                        {
                            FileStream fileStream = new FileStream(filePath, FileMode.Create);
                            PdfWriter.GetInstance(pdfDoc, fileStream);
                            pdfDoc.Open();
                            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                            htmlparser.Parse(sr);
                            fileStream.Flush();
                            pdfDoc.Close();
                            fileStream.Close();
                            fileStream.Dispose();
                        }
                        catch (Exception ex)
                        {
                            Divdetails.Visible = false;
                            divmessage.Attributes.Add("class", "error");
                            divmessage.InnerHtml = ex.ToString();
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                        Divdetails.Visible = false;



                        if (DoneBooking(filePath))
                        {
                            divmessage.Attributes.Add("class", "succesfuly");
                            if (IsConvertToRequest)
                            {
                                divmessage.InnerHtml = GetKeyResult("REQUESTDONESUCCESSFUL") + " (" + BookingIds + ").";
                            }
                            else
                            {
                                divmessage.InnerHtml = GetKeyResult("BOOKINGDONESUCCESSFUL") + " (" + BookingIds + ").";
                            }
                            st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
                            st.IsProcessDone = true;
                            bm.SaveSearch(st);
                            DirectoryInfo dir = new DirectoryInfo(dirPath);
                            DateTime dateTime = DateTime.Now.AddMinutes(-10);
                            foreach (var file in dir.GetFiles().Where(u => u.CreationTime < dateTime).OrderBy(u => u.CreationTime))
                            {
                                try
                                {
                                    file.Delete();
                                }
                                catch(Exception ex)
                                {
                                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                                }
                            }
                            Session.Remove("SearchID");
                            Session.Remove("Search");

                            Session["BookingDone"] = "done";
                        }
                        else
                        {
                            Divdetails.Visible = false;
                            divmessage.Attributes.Add("class", "error");
                            divmessage.InnerHtml = GetKeyResult("BOOKINGNOTDONESUCCESSFUL");
                        }
                        #endregion
                        Session["SerachID"] = null;
                    }
                }
                else
                {
                    //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                    if (l != null)
                    {
                        Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
                    }
                    else
                    {
                        Response.Redirect(SiteRootPath + "default/english");
                    }
                    //Response.Redirect("default.aspx");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                Divdetails.Visible = false;
                divmessage.Attributes.Add("class", "error");
                divmessage.InnerHtml = ex.ToString();
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
    }
    #endregion

    #region VerifyRenderingInServerForm
    /// <summary>
    /// method to VerifyRenderingInServerForm
    /// </summary>

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    #endregion

    #region PrepareGridViewForExportDIV
    /// <summary>
    /// method to PrepareGridViewForExport a div
    /// </summary>
    private void PrepareDivForExport(Control gv)
    {
        try
        {

            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {

                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].GetType() == typeof(System.Web.UI.HtmlControls.HtmlAnchor))
                {

                    l.Text = (gv.Controls[i] as System.Web.UI.HtmlControls.HtmlAnchor).InnerText;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(DropDownList))
                {

                    l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(GridView))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    // gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Repeater))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Panel))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Table))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }


                else if (gv.Controls[i].GetType() == typeof(CheckBox))
                {

                    l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(HyperLink))
                {

                    l.Text = (gv.Controls[i] as HyperLink).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                //else if (gv.Controls[i].GetType() == typeof(Label))
                //{

                //    l.Text = (gv.Controls[i] as Label).Text;

                //    gv.Controls.Remove(gv.Controls[i]);

                //    gv.Controls.AddAt(i, l);

                //}
                else if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.Image))
                {

                    // l.Text = (gv.Controls[i] as Image).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareDivForExport(gv.Controls[i]);

                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        //PrepareGridViewForExport();
        //PrepareGridViewForExport();

    }


    #endregion

    #region Bookingbind


    List<VatCollection> _VatCalculation = new List<VatCollection>();
    public List<VatCollection> VatCalculation
    {
        get
        {
            if (_VatCalculation == null)
            {
                _VatCalculation = new List<VatCollection>();
            }
            return _VatCalculation;
        }
        set
        {
            _VatCalculation = value;
        }
    }

    public void BindBooking()
    {
        //lblArivalDate.Text = objBooking.ArivalDate.ToString("dd MMM yyyy");
        //lblDepartureDate.Text = objBooking.DepartureDate.ToString("dd MMM yyyy");
        //lblDuration.Text = objBooking.Duration == 1 ? objBooking.Duration + " Day" : objBooking.Duration + " Days";
        Users u = objHotelManager.GetByUserID(objBooking.CurrentUserId);
        lblName.Text = u.FirstName;

        UserDetails ud = u.UserDetailsCollection.Where(a => a.UserId == u.UserId).FirstOrDefault();
        if (ud != null)
        {
            lblContactAddress.Text = ud.Address;
            lblContactPhone.Text = ud.Phone;
            lblContactPerson.Text = u.FirstName + " " + u.LastName;
            lblConatctpersonEmail.Text = u.EmailId;
            if (u.Usertype == (int)Usertype.Company)
            {
                lblCName.Visible = true;
                lblCompanyName.Visible = true;
                lblCompanyName.Text = string.IsNullOrEmpty(ud.CompanyName) ? "" : ud.CompanyName;
            }
            else
            {
                lblCName.Visible = false;
                lblCompanyName.Visible = false;
            }
        }
        else
        {
            lblCName.Visible = false;
            lblCompanyName.Visible = false;
        }
        lblFromDt.Text = objBooking.ArivalDate.ToString("dd MMM yyyy");
        lblToDate.Text = objBooking.DepartureDate.ToString("dd MMM yyyy");
        lblBookedDays.Text = objBooking.Duration == 1 ? objBooking.Duration + GetKeyResult("DAY") : objBooking.Duration + GetKeyResult("DAY");

        Hotel objHotelDetails = objHotel.GetHotelDetailsById(objBooking.HotelID);
        Currency objcurrency = cm.GetCurrencyDetailsByID(objHotelDetails.CurrencyId);
        HotelCurrency = objcurrency.Currency;
        CurrencySign = objcurrency.CurrencySignature;

        BindHotelDetails(objBooking.HotelID);
        //imgLogo.Src = SiteRootPath + "Images/logo.png";
        rptMeetingRoom.DataSource = objBooking.MeetingroomList;
        rptMeetingRoom.DataBind();
        BindAccomodation();
        lblSpecialRequest.Text = objBooking.SpecialRequest;
        Calculate(objBooking);
        lblNetTotal.Text = Math.Round((objBooking.TotalBookingPrice - VatCalculation.Sum(a => a.CalculatedPrice)) * CurrencyConvert, 2).ToString();
        lblNetTotal.Visible = false;
        //rptVatList.DataSource = VatCalculation;
        //rptVatList.DataBind();
        lblFinalTotal.Text = CurrencySign + Math.Round(objBooking.TotalBookingPrice * CurrencyConvert, 2).ToString("#,##,##0.00");

    }

    public void BindHotelDetails(Int64 Hotelid)
    {
        Hotel objHotelDetails = objHotel.GetHotelDetailsById(Hotelid);
        lblHotel.Text = objHotelDetails.Name;
        lblhoteladd.Text = objHotelDetails.HotelAddress;
        lblFax.Text = objHotelDetails.CountryCodeFax + "-" + objHotelDetails.FaxNumber;
        lblTel.Text = objHotelDetails.PhoneExt + "-" + objHotelDetails.PhoneNumber;
        lblEmail.Text = objHotelDetails.Email;

        //Currency objUserCurrency = cm.GetCurrencyDetailsByID(Convert.ToInt64(Session["CurrencyID"]));
        //UserCurrency = objUserCurrency.Currency;
        //CurrencySign = objUserCurrency.CurrencySignature;
        //Currency objcurrency = cm.GetCurrencyDetailsByID(objHotelDetails.CurrencyId);
        //HotelCurrency = objcurrency.Currency;
        //string currency = CurrencyManager.Currency(HotelCurrency, UserCurrency);
        //CurrencyConvert = Convert.ToDecimal(currency == "N/A" ? "1" : currency);
        //CurrentLat = Convert.ToDecimal(objHotelDetails.Latitude);
        //CurrentLong = Convert.ToDecimal(objHotelDetails.Longitude);
        ////if (objHotelDetails.Stars == 1)
        //{
        //    imgHotelStars.ImageUrl = "~/Images/1.png";
        //}
        //else if (objHotelDetails.Stars == 2)
        //{
        //    imgHotelStars.ImageUrl = "~/Images/2.png";
        //}
        //else if (objHotelDetails.Stars == 3)
        //{
        //    imgHotelStars.ImageUrl = "~/Images/3.png";
        //}
        //else if (objHotelDetails.Stars == 4)
        //{
        //    imgHotelStars.ImageUrl = "~/Images/4.png";
        //}
        //else if (objHotelDetails.Stars == 5)
        //{
        //    imgHotelStars.ImageUrl = "~/Images/5.png";
        //}
        //else if (objHotelDetails.Stars == 6)
        //{
        //    imgHotelStars.ImageUrl = "~/Images/6.png";
        //}
        //else if (objHotelDetails.Stars == 7)
        //{
        //    imgHotelStars.ImageUrl = "~/Images/7.png";
        //}
    }
    public void BindAccomodation()
    {
        if (objBooking.NoAccomodation)
        {
            pnlAccomodation.Visible = false;
        }
        else
        {
            if (objBooking.ManageAccomodationLst.Count > 0)
            {
                List<BedroomManage> distinctNames = (from d in objBooking.ManageAccomodationLst select new BedroomManage { BedroomID = d.BedroomId, bedroomType = d.BedroomType }).ToList();
                List<BedroomManage> disListNew = distinctNames.Distinct(new DistinctItemComparer()).ToList();
                lblTotalAccomodation.Text = CurrencySign + " " +string.Format("{0:#,##,##0.00}", objBooking.AccomodationPriceTotal);
                rptAccomodationDaysManager.DataSource = disListNew;
                rptAccomodationDaysManager.DataBind();
                pnlAccomodation.Visible = true;
                lblNoteBedroom.Text = objBooking.BedroomNote;
            }
            else
            {
                pnlAccomodation.Visible = false;
            }
        }
    }
    protected void rptExtra_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPackageItem = (Label)e.Item.FindControl("lblPackageItem");
            Label lblFrom = (Label)e.Item.FindControl("lblFrom");
            Label lblTo = (Label)e.Item.FindControl("lblTo");
            Label lblQuntity = (Label)e.Item.FindControl("lblQuntity");
            Label lblTotal = (Label)e.Item.FindControl("lblTotal");
            ManageExtras mngExt = e.Item.DataItem as ManageExtras;
            if (mngExt != null)
            {
                PackageItems p = objHp.Where(a => a.Id == mngExt.ItemId).FirstOrDefault();
                PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                if (p != null)
                {
                    if (pdesc != null)
                    {
                        lblPackageItem.Text = pdesc.ItemName;
                    }
                    else
                    {
                        lblPackageItem.Text = p.ItemName;
                    }
                    lblFrom.Text = mngExt.FromTime;
                    // lblTo.Text = mngExt.ToTime;
                    lblTotal.Text = CurrencySign + Math.Round(mngExt.Quantity * mngExt.ItemPrice, 2).ToString("#,##,##0.00");
                    lblQuntity.Text = mngExt.Quantity.ToString();
                }
            }
            else
            {
                lblQuntity.Text = "0";
            }
        }
    }
    protected void rptMeetingRoom_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            BookedMR objBookedMr = e.Item.DataItem as BookedMR;
            CurrentMeetingRoomID = objBookedMr.MRId;
            CurrentMeetingRoomConfigureID = objBookedMr.MrConfigId;

            Repeater rptMeetingRoomConfigure = (Repeater)e.Item.FindControl("rptMeetingRoomConfigure");
            rptMeetingRoomConfigure.DataSource = objBookedMr.MrDetails;
            rptMeetingRoomConfigure.DataBind();
        }
    }

    protected void rptPackageItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPackageItem = (Label)e.Item.FindControl("lblPackageItem");
            Label lblFromTime = (Label)e.Item.FindControl("lblFromTime");
            Label lblToTime = (Label)e.Item.FindControl("lblToTime");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            PackageItems p = e.Item.DataItem as PackageItems;
            PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
            if (p != null)
            {
                ManagePackageItem mpi = CurrentPackageItem.Where(a => a.ItemId == p.Id).FirstOrDefault();
                if (mpi != null)
                {
                    if (pdesc != null)
                    {
                        lblPackageItem.Text = pdesc.ItemName;
                    }
                    else
                    {
                        lblPackageItem.Text = p.ItemName;
                    }
                    if (p.ItemName.ToLower().Contains("coffee break(s) morning and/or afternoon"))//|| p.ItemName.ToLower().Contains("morning / afternoon") || p.ItemName.ToLower().Contains("morning/ afternoon") || p.ItemName.ToLower().Contains("morning /afternoon") || p.ItemName.ToLower().Contains("morning / afternoon coffee break")
                    {
                        if (CurrentDayTime == 0 || CurrentDayTime == 1)
                        {
                            lblFromTime.Text = mpi.FromTime;
                            lblFromTime.Visible = true;
                        }
                        else
                        {
                            lblFromTime.Text = "NA";
                            //lblFromTime.Visible = false;
                        }
                        if (CurrentDayTime == 0 || CurrentDayTime == 2)
                        {
                            lblToTime.Text = mpi.ToTime;
                            lblToTime.Visible = true;
                        }
                        else
                        {
                            lblToTime.Text = "NA";
                            //lblToTime.Visible = false;
                        }
                    }
                    else
                    {
                        lblToTime.Visible = false;
                        lblFromTime.Text = mpi.FromTime;
                    }

                    //lblToTime.Text = mpi.ToTime;
                    lblQuantity.Text = Convert.ToString(mpi.Quantity);
                }
            }
        }
    }
    protected void rptVatList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblVatPrice = (Label)e.Item.FindControl("lblVatPrice");
            VatCollection v = e.Item.DataItem as VatCollection;
            if (v != null)
            {
                lblVatPrice.Text = CurrencySign + Math.Round(v.CalculatedPrice, 2).ToString("#,##,##0.00");
            }
        }
    }

    protected void rptAccomodation_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Accomodation objAccomodation = e.Item.DataItem as Accomodation;

            Label lblBedroomName = (Label)e.Item.FindControl("lblBedroomName");
            Label lblBedroomType = (Label)e.Item.FindControl("lblBedroomType");
            Label lblBedroomPrice = (Label)e.Item.FindControl("lblBedroomPrice");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            Label lblTotalBedroomPrice = (Label)e.Item.FindControl("lblTotalBedroomPrice");
            if (objAccomodation != null)
            {
                //BedRoom objBedroom = objHotel.GetBedRoomDetailsByBedroomID(objAccomodation.BedroomId);
                //lblBedroomName.Text = Enum.GetName(typeof(BedRoomType), objBedroom.Types);
                //lblBedroomType.Text = objAccomodation.RoomType;
                //lblBedroomPrice.Text = Math.Round(Convert.ToDecimal(objAccomodation.RoomType == "Single" ? objBedroom.PriceSingle : objBedroom.PriceDouble) * CurrencyConvert, 2).ToString();
                //lblQuantity.Text = objAccomodation.Quantity.ToString();
                //lblTotalBedroomPrice.Text = Math.Round(Convert.ToDecimal(objAccomodation.RoomType == "Single" ? objBedroom.PriceSingle : objBedroom.PriceDouble) * CurrencyConvert * objAccomodation.Quantity, 2).ToString();
                //Repeater rptRoomManage = (Repeater)e.Item.FindControl("rptRoomManage");
                //rptRoomManage.DataSource = objAccomodation.RoomManageLst;
                //rptRoomManage.DataBind();
            }
        }
    }
    protected void rptRoomManage_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            RoomManage objRoomManage = e.Item.DataItem as RoomManage;
            Label lblRoomIndex = (Label)e.Item.FindControl("lblRoomIndex");
            Label lblAllotmantName = (Label)e.Item.FindControl("lblAllotmantName");
            Label lblCheckIn = (Label)e.Item.FindControl("lblCheckIn");
            Label lblCheckOut = (Label)e.Item.FindControl("lblCheckOut");
            Label lblNote = (Label)e.Item.FindControl("lblNote");
            lblRoomIndex.Text = (e.Item.ItemIndex + 1).ToString();
            lblAllotmantName.Text = objRoomManage.RoomAssignedTo;
            lblCheckIn.Text = objRoomManage.CheckInTime;
            lblCheckOut.Text = objRoomManage.checkoutTime;
            lblNote.Text = objRoomManage.Notes;
        }
    }


    public int CurrentDayTime
    {
        get;
        set;
    }
    protected void rptMeetingRoomConfigure_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            BookedMrConfig objBookedMrConfig = e.Item.DataItem as BookedMrConfig;
            //Bind Meetingroom//
            MeetingRoom objMeetingroom = objHotel.GetMeetingRoomDetailsById(CurrentMeetingRoomID);
            MeetingRoomDesc objDesc = objMeetingroom.MeetingRoomDescCollection.Where(a => a.LanguageId == Convert.ToInt32(Session["LanguageID"])).FirstOrDefault();
            MeetingRoomConfig objMrConfig = objMeetingroom.MeetingRoomConfigCollection.Where(a => a.Id == CurrentMeetingRoomConfigureID).FirstOrDefault();

            Label lblMeetingRoomName = (Label)e.Item.FindControl("lblMeetingRoomName");
            Label lblMeetingRoomType = (Label)e.Item.FindControl("lblMeetingRoomType");
            Label lblMinMaxCapacity = (Label)e.Item.FindControl("lblMinMaxCapacity");
            Label lblMeetingRoomActualPrice = (Label)e.Item.FindControl("lblMeetingRoomActualPrice");
            Label lblTotalMeetingRoomPrice = (Label)e.Item.FindControl("lblTotalMeetingRoomPrice");
            Label lblTotalFinalMeetingRoomPrice = (Label)e.Item.FindControl("lblTotalFinalMeetingRoomPrice");
            lblMeetingRoomName.Text = objMeetingroom.Name;
            lblMeetingRoomType.Text = Enum.GetName(typeof(RoomShape), objMrConfig.RoomShapeId);
            lblMinMaxCapacity.Text = objMrConfig.MinCapacity + " - " + objMrConfig.MaxCapicity;
            lblMeetingRoomActualPrice.Text = CurrencySign + Math.Round(objBookedMrConfig.MeetingroomPrice, 2).ToString("#,##,##0.00");
            lblTotalFinalMeetingRoomPrice.Text = CurrencySign + Math.Round(objBookedMrConfig.MeetingroomPrice, 2).ToString("#,##,##0.00");
            decimal intDiscountMR = objBookedMrConfig.MeetingroomDiscount;
            lblTotalMeetingRoomPrice.Text = CurrencySign + Math.Round(objBookedMrConfig.MeetingroomPrice, 2).ToString("#,##,##0.00");

            //Bind Other details//
            Label lblSelectedDay = (Label)e.Item.FindControl("lblSelectedDay");
            Label lblStart = (Label)e.Item.FindControl("lblStart");
            Label lblEnd = (Label)e.Item.FindControl("lblEnd");
            Label lblNumberOfParticepant = (Label)e.Item.FindControl("lblNumberOfParticepant");
            lblSelectedDay.Text = objBookedMrConfig.SelectedDay.ToString();
            CurrentDayTime = objBookedMrConfig.SelectedTime;
            //lblTotalMeetingRoomPrice.Text = Math.Round(objBookedMrConfig.MeetingroomPrice , 2).ToString();
            lblStart.Text = objBookedMrConfig.FromTime;
            lblEnd.Text = objBookedMrConfig.ToTime;
            lblNumberOfParticepant.Text = objBookedMrConfig.NoOfParticepant.ToString();
            Panel pnlNotIsBreakdown = (Panel)e.Item.FindControl("pnlNotIsBreakdown");
            Panel pnlIsBreakdown = (Panel)e.Item.FindControl("pnlIsBreakdown");
            if (objBookedMrConfig.IsBreakdown == true)
            {
                if (pnlIsBreakdown == null)
                {
                }
                else
                {
                    pnlIsBreakdown.Visible = true;
                    pnlNotIsBreakdown.Visible = false;
                }
            }
            else
            {
                if (pnlIsBreakdown == null)
                {
                }
                else
                {
                    pnlIsBreakdown.Visible = false;
                    pnlNotIsBreakdown.Visible = true;
                }
            }
            #region Package and Equipment Collection
            //Bind Package//
            Panel pnlIsPackageSelected = (Panel)e.Item.FindControl("pnlIsPackageSelected");
            Panel pnlBuildMeeting = (Panel)e.Item.FindControl("pnlBuildMeeting");
            HtmlTableCell divpriceMeetingroom = (HtmlTableCell)e.Item.FindControl("divpriceMeetingroom");
            HtmlTableCell divpriceMeetingroom1 = (HtmlTableCell)e.Item.FindControl("divpriceMeetingroom1");
            HtmlTableCell divpriceMeetingroom2 = (HtmlTableCell)e.Item.FindControl("divpriceMeetingroom2");
            HtmlTableCell divpriceMeetingroom3 = (HtmlTableCell)e.Item.FindControl("divpriceMeetingroom3");
            HtmlTableCell divpriceMeetingroom4 = (HtmlTableCell)e.Item.FindControl("divpriceMeetingroom4");
            if (objBookedMrConfig.PackageID == 0)
            {
                pnlIsPackageSelected.Visible = false;
                if (objBookedMrConfig.BuildManageMRLst.Count > 0)
                {
                    pnlBuildMeeting.Visible = true;
                    Repeater rptBuildMeeting = (Repeater)e.Item.FindControl("rptBuildMeeting");
                    Label lblTotalBuildPackagePrice = (Label)e.Item.FindControl("lblTotalBuildPackagePrice");

                    CurrentPackageType = objBookedMrConfig.SelectedTime;
                    rptBuildMeeting.DataSource = objBookedMrConfig.BuildManageMRLst;
                    rptBuildMeeting.DataBind();
                    lblTotalBuildPackagePrice.Text = CurrencySign + Math.Round((objBookedMrConfig.BuildPackagePriceTotal - (objBookedMrConfig.MeetingroomPrice)) * CurrencyConvert, 2).ToString("#,##,##0.00");
                }
                else
                {
                    pnlBuildMeeting.Visible = false;
                }
                //divpriceMeetingroom.Visible = true;
                //divpriceMeetingroom1.Visible = true;
                //divpriceMeetingroom2.Visible = true;
                //divpriceMeetingroom3.Visible = true;
                //divpriceMeetingroom4.Visible = true;
            }
            else
            {
                pnlIsPackageSelected.Visible = true;
                pnlBuildMeeting.Visible = false;
                Label lblSelectedPackage = (Label)e.Item.FindControl("lblSelectedPackage");
                Label lblPackageDescription = (Label)e.Item.FindControl("lblPackageDescription");
                Label lblPackagePrice = (Label)e.Item.FindControl("lblPackagePrice");
                Label lblTotalPackagePrice = (Label)e.Item.FindControl("lblTotalPackagePrice");

                lblPackagePrice.Text = CurrencySign + Math.Round(objBookedMrConfig.PackagePriceTotal * CurrencyConvert, 2).ToString("#,##,##0.00");
                Repeater rptPackageItem = (Repeater)e.Item.FindControl("rptPackageItem");
                Label lblTotal2IfPackageIsNotAvailable = (Label)e.Item.FindControl("lblTotal2IfPackageIsNotAvailable");
                //Label lblTotalFinalMeetingRoomPrice2= (Label)e.Item.FindControl("lblTotalFinalMeetingRoomPrice2");
                //Label lblTotalMeetingRoomPrice2= (Label)e.Item.FindControl("lblTotalMeetingRoomPrice2");
                //Label lblMeetingRoomActualPrice2 = (Label)e.Item.FindControl("lblMeetingRoomActualPrice2");
                Label lblTotalIfNotPackage = (Label)e.Item.FindControl("lblTotalIfNotPackage");
                Label lblPriceIfNotPackage = (Label)e.Item.FindControl("lblPriceIfNotPackage");
                PackageByHotel p = objHotel.GetPackageDetailsByHotel(objBooking.HotelID).FindAllDistinct(PackageByHotelColumn.PackageId).Where(a => a.PackageId == objBookedMrConfig.PackageID).FirstOrDefault();
                if (p != null)
                {

                    CurrentPackageItem = objBookedMrConfig.ManagePackageLst;
                    rptPackageItem.DataSource = objHotel.GetPackageItemDetailsByPackageID(Convert.ToInt64(p.PackageId));
                    rptPackageItem.DataBind();

                    lblSelectedPackage.Text = p.PackageIdSource.PackageName;
                    //if (p.PackageIdSource.PackageName.ToLower() == "favourite")
                    //{
                    //    lblPackageDescription.Text = GetKeyResult("FAVOURITEDESCRIPTION");
                    //}
                    //if (p.PackageIdSource.PackageName.ToLower() == "elegant")
                    //{
                    //    lblPackageDescription.Text = GetKeyResult("ELEGANTDESCRIPTION");
                    //}
                    //if (p.PackageIdSource.PackageName.ToLower() == "standard")
                    //{
                    //    lblPackageDescription.Text = GetKeyResult("STANDARDDESCRIPTION");
                    //}
                    objHotel.GetDescriptionofPackageByPackage(p.PackageIdSource);
                    PackageMasterDescription des = p.PackageIdSource.PackageMasterDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64(Session["LanguageID"])).FirstOrDefault();
                    if (des == null)
                    {
                        des = p.PackageIdSource.PackageMasterDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64("1")).FirstOrDefault();
                    }
                    if (des != null)
                    {
                        lblPackageDescription.Text = des.Description;
                    }
                    lblTotal2IfPackageIsNotAvailable.Visible = false;
                    lblTotalFinalMeetingRoomPrice.Visible = false;
                    lblTotalMeetingRoomPrice.Visible = false;
                    lblMeetingRoomActualPrice.Visible = false;
                    lblTotalIfNotPackage.Visible = false;
                    lblPriceIfNotPackage.Visible = false;
                }
                else
                {
                    lblTotal2IfPackageIsNotAvailable.Visible = true;
                    lblTotalFinalMeetingRoomPrice.Visible = true;
                    lblTotalMeetingRoomPrice.Visible = true;
                    lblMeetingRoomActualPrice.Visible = true;
                    lblTotalIfNotPackage.Visible = true;
                    lblPriceIfNotPackage.Visible = true;
                }
                Panel pnlIsExtra = (Panel)e.Item.FindControl("pnlIsExtra");
                Label lblextratitle = (Label)e.Item.FindControl("lblextratitle");
                Label lblExtraPrice = (Label)e.Item.FindControl("lblExtraPrice");
                if (objBookedMrConfig.ManageExtrasLst.Count > 0)
                {
                    Repeater rptExtra = (Repeater)e.Item.FindControl("rptExtra");
                    objHp = objHotel.GetIsExtraItemDetailsByHotel(objBooking.HotelID).FindAllDistinct(PackageItemsColumn.Id);
                    rptExtra.DataSource = objBookedMrConfig.ManageExtrasLst;
                    rptExtra.DataBind();
                    pnlIsExtra.Visible = true;

                    lblExtraPrice.Text = CurrencySign + Math.Round(objBookedMrConfig.ExtraPriceTotal * CurrencyConvert, 2).ToString("#,##,##0.00");
                }
                else
                {
                    lblExtraPrice.Visible = false;

                    lblextratitle.Visible = false;
                    pnlIsExtra.Visible = false;

                }

                lblTotalPackagePrice.Text = CurrencySign + Math.Round((objBookedMrConfig.PackagePriceTotal + objBookedMrConfig.ExtraPriceTotal) * CurrencyConvert, 2).ToString("#,##,##0.00");
            }
            Panel pnlEquipment = (Panel)e.Item.FindControl("pnlEquipment");
            if (objBookedMrConfig.EquipmentLst.Count > 0)
            {
                pnlEquipment.Visible = true;
                Repeater rptEquipment = (Repeater)e.Item.FindControl("rptEquipment");
                rptEquipment.DataSource = objBookedMrConfig.EquipmentLst;
                rptEquipment.DataBind();
                Label lblTotalEquipmentPrice = (Label)e.Item.FindControl("lblTotalEquipmentPrice");
                lblTotalEquipmentPrice.Text = CurrencySign + Math.Round(objBookedMrConfig.EquipmentPriceTotal * CurrencyConvert, 2).ToString("#,##,##0.00");
            }
            else
            {
                pnlEquipment.Visible = false;
            }
            Panel pnlOthers = (Panel)e.Item.FindControl("pnlOthers");
            if (objBookedMrConfig.BuildOthers.Count > 0)
            {
                pnlOthers.Visible = true;
                Repeater rptOthers = (Repeater)e.Item.FindControl("rptOthers");
                rptOthers.DataSource = objBookedMrConfig.BuildOthers;
                rptOthers.DataBind();
                Label lblTotalOthersPrice = (Label)e.Item.FindControl("lblTotalOthersPrice");
                lblTotalOthersPrice.Text = CurrencySign + Math.Round(objBookedMrConfig.OtherPriceTotal * CurrencyConvert, 2).ToString("#,##,##0.00");
            }
            else
            {
                pnlOthers.Visible = false;
            }
            #endregion
        }
    }

    protected void rptBuildMeeting_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            BuildYourMR objBuildYourMR = e.Item.DataItem as BuildYourMR;
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblItemDescription = (Label)e.Item.FindControl("lblItemDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            Label lblTotalItemPrice = (Label)e.Item.FindControl("lblTotalItemPrice");
            PackageItems p = objHotel.GetItemDetailsByHotelandType(objBooking.HotelID, ItemType.FoodBeverages).FindAllDistinct(PackageItemsColumn.Id).Where(a => a.Id == objBuildYourMR.ItemId).FirstOrDefault();
            if (p != null)
            {
                PackageDescription pds = p.PackageDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64(Session["LanguageID"])).FirstOrDefault();
                if (pds != null)
                {
                    lblItemName.Text = pds.ItemName;
                    lblItemDescription.Text = pds.ItemDescription;
                }
                else
                {
                    lblItemName.Text = p.ItemName;
                    lblItemDescription.Text = "";
                }
                lblQuantity.Text = objBuildYourMR.Quantity.ToString();
                lblTotalItemPrice.Text = CurrencySign + Math.Round(objBuildYourMR.ItemPrice * objBuildYourMR.Quantity, 2).ToString("#,##,##0.00");
                //Package Hotel pricing
                //PackageByHotel objp = p.PackageByHotelCollection.Where(a => a.HotelId == objBooking.HotelID && a.PackageId == null).FirstOrDefault();
                //if(objp!=null)
                //{
                //     = Math.Round((CurrentPackageType == 0 ? Convert.ToDecimal(objp.FulldayPrice) : Convert.ToDecimal(objp.HalfdayPrice)) * objBuildYourMR.Quantity * CurrencyConvert,2).ToString();
                //}
            }
        }
    }

    protected void rptEquipment_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ManageEquipment objManageEquipment = e.Item.DataItem as ManageEquipment;
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblItemDescription = (Label)e.Item.FindControl("lblItemDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            Label lblTotalPrice = (Label)e.Item.FindControl("lblTotalPrice");
            PackageItems p = objHotel.GetItemDetailsByHotelandType(objBooking.HotelID, ItemType.Equipment).FindAllDistinct(PackageItemsColumn.Id).Where(a => a.Id == objManageEquipment.ItemId).FirstOrDefault();
            if (p != null)
            {
                PackageDescription pds = p.PackageDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64(Session["LanguageID"])).FirstOrDefault();
                if (pds != null)
                {
                    lblItemName.Text = pds.ItemName;
                    lblItemDescription.Text = pds.ItemDescription;
                }
                else
                {
                    lblItemName.Text = p.ItemName;
                    lblItemDescription.Text = "";
                }
                lblQuantity.Text = objManageEquipment.Quantity.ToString();
                lblTotalPrice.Text = CurrencySign + Math.Round(objManageEquipment.ItemPrice * objManageEquipment.Quantity, 2).ToString("#,##,##0.00");
                //Package Hotel pricing
                //PackageByHotel objp = p.PackageByHotelCollection.Where(a => a.HotelId == objBooking.HotelID && a.PackageId == null).FirstOrDefault();
                //if (objp != null)
                //{
                //    lblTotalPrice.Text = Math.Round((CurrentPackageType == 0 ? Convert.ToDecimal(objp.FulldayPrice) : Convert.ToDecimal(objp.HalfdayPrice)) * objManageEquipment.Quantity * CurrencyConvert,2).ToString();
                //}
            }
        }
    }

    protected void rptOthers_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ManageOtherItems objManageEquipment = e.Item.DataItem as ManageOtherItems;
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblItemDescription = (Label)e.Item.FindControl("lblItemDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            Label lblTotalPrice = (Label)e.Item.FindControl("lblTotalPrice");
            PackageItems p = objHotel.GetItemDetailsByHotelandType(objBooking.HotelID, ItemType.Others).FindAllDistinct(PackageItemsColumn.Id).Where(a => a.Id == objManageEquipment.ItemId).FirstOrDefault();
            if (p != null)
            {
                PackageDescription pds = p.PackageDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64(Session["LanguageID"])).FirstOrDefault();
                if (pds != null)
                {
                    lblItemName.Text = pds.ItemName;
                    lblItemDescription.Text = pds.ItemDescription;
                }
                else
                {
                    lblItemName.Text = p.ItemName;
                    lblItemDescription.Text = "";
                }
                lblQuantity.Text = objManageEquipment.Quantity.ToString();
                lblTotalPrice.Text = CurrencySign + Math.Round(objManageEquipment.ItemPrice * objManageEquipment.Quantity, 2).ToString("#,##,##0.00");
            }
        }
    }



    #region Go for Calculation
    public void Calculate(Createbooking objCreateBook)
    {
        decimal TotalMeetingroomPrice = 0;
        decimal TotalPackagePrice = 0;
        decimal TotalBuildYourPackagePrice = 0;
        decimal TotalEquipmentPrice = 0;
        decimal TotalExtraPrice = 0;
        decimal TotalOthersPrice = 0;
        bool PackageSelected = false;
        VatCalculation = null;
        VatCalculation = new List<VatCollection>();
        if (objCreateBook != null)
        {
            foreach (BookedMR objb in objCreateBook.MeetingroomList)
            {
                foreach (BookedMrConfig objconfig in objb.MrDetails)
                {
                    TotalMeetingroomPrice = objconfig.NoOfParticepant * objconfig.MeetingroomPrice;
                    //if (VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault() == null)
                    //{
                    //    VatCollection v = new VatCollection();
                    //    v.VATPercent = objconfig.MeetingroomVAT;
                    //    v.CalculatedPrice = objconfig.MeetingroomPrice * objconfig.MeetingroomVAT / 100;
                    //    VatCalculation.Add(v);
                    //}
                    //else
                    //{
                    //    VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault().CalculatedPrice += objconfig.MeetingroomPrice * objconfig.MeetingroomVAT / 100;
                    //}
                    //Build mr
                    foreach (BuildYourMR bmr in objconfig.BuildManageMRLst)
                    {
                        TotalBuildYourPackagePrice += bmr.ItemPrice * bmr.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == bmr.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = bmr.vatpercent;
                            v.CalculatedPrice = (bmr.ItemPrice - (bmr.ItemPrice / ((100 + bmr.vatpercent) / 100))) * bmr.Quantity;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == bmr.vatpercent).FirstOrDefault().CalculatedPrice += (bmr.ItemPrice - (bmr.ItemPrice / ((100 + bmr.vatpercent) / 100))) * bmr.Quantity;
                        }
                    }
                    PackageSelected = Convert.ToBoolean(objconfig.PackageID);
                    //Equipment
                    foreach (ManageEquipment eqp in objconfig.EquipmentLst)
                    {
                        TotalEquipmentPrice += eqp.ItemPrice * eqp.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = eqp.vatpercent;
                            v.CalculatedPrice = (eqp.ItemPrice - (eqp.ItemPrice / ((100 + eqp.vatpercent) / 100))) * eqp.Quantity;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault().CalculatedPrice += (eqp.ItemPrice - (eqp.ItemPrice / ((100 + eqp.vatpercent) / 100))) * eqp.Quantity;
                        }
                    }
                    foreach (ManageOtherItems eqp in objconfig.BuildOthers)
                    {
                        TotalOthersPrice += eqp.ItemPrice * eqp.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = eqp.vatpercent;
                            v.CalculatedPrice = (eqp.ItemPrice - (eqp.ItemPrice / ((100 + eqp.vatpercent) / 100))) * eqp.Quantity;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault().CalculatedPrice += (eqp.ItemPrice - (eqp.ItemPrice / ((100 + eqp.vatpercent) / 100))) * eqp.Quantity;
                        }
                    }
                    //Manage Extras
                    foreach (ManageExtras ext in objconfig.ManageExtrasLst)
                    {
                        TotalExtraPrice += ext.ItemPrice * ext.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == ext.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = ext.vatpercent;
                            v.CalculatedPrice = (ext.ItemPrice - (ext.ItemPrice / ((100 + ext.vatpercent) / 100))) * ext.Quantity;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == ext.vatpercent).FirstOrDefault().CalculatedPrice += (ext.ItemPrice - (ext.ItemPrice / ((100 + ext.vatpercent) / 100))) * ext.Quantity;
                        }
                    }
                    //Manage Package Item
                    decimal restmeetingroomprice = 0;
                    decimal itemtotalprice = 0;
                    foreach (ManagePackageItem pck in objconfig.ManagePackageLst)
                    {
                        itemtotalprice += pck.ItemPrice;
                        TotalPackagePrice += pck.ItemPrice * pck.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == pck.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = pck.vatpercent;
                            v.CalculatedPrice = (pck.ItemPrice - (pck.ItemPrice / ((100 + pck.vatpercent) / 100))) * pck.Quantity;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == pck.vatpercent).FirstOrDefault().CalculatedPrice += (pck.ItemPrice - (pck.ItemPrice / ((100 + pck.vatpercent) / 100))) * pck.Quantity;
                        }
                    }
                    restmeetingroomprice = (objconfig.PackagePricePerPerson - itemtotalprice) * objconfig.NoOfParticepant;
                    if (PackageSelected)
                    {
                        if (VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = objconfig.MeetingroomVAT;
                            v.CalculatedPrice = restmeetingroomprice - (restmeetingroomprice / ((100 + objconfig.MeetingroomVAT) / 100));
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault().CalculatedPrice += restmeetingroomprice - (restmeetingroomprice / ((100 + objconfig.MeetingroomVAT) / 100));
                        }
                    }
                    else
                    {
                        if (VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = objconfig.MeetingroomVAT;
                            v.CalculatedPrice = objconfig.MeetingroomPrice - (objconfig.MeetingroomPrice / ((100 + objconfig.MeetingroomVAT) / 100));
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault().CalculatedPrice += objconfig.MeetingroomPrice - (objconfig.MeetingroomPrice / ((100 + objconfig.MeetingroomVAT) / 100));
                        }
                    }
                }
            }

        }
    }
    #endregion
    #endregion

    #region Function
    /// <summary>
    /// Bind year
    /// </summary>
    public void BindYear()
    {
        int intIndex = 0;
        for (intIndex = DateTime.Today.Year; intIndex <= DateTime.Today.AddYears(15).Year; intIndex++)
        {
            dropdownYear.Items.Add(intIndex.ToString());
        }
    }

    /// <summary>
    /// Bind All Country.
    /// </summary>
    public void BindCountry()
    {
        dropdownCountry.DataTextField = "CountryName";
        dropdownCountry.DataValueField = "Id";
        dropdownCountry.DataSource = objCreditCardManager.GetCountry().OrderBy(a => a.CountryName);
        dropdownCountry.DataBind();
        try
        {
            dropdownCountry.Items.FindByText("Belgium").Selected = true;
        }
        catch(Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        //    dropdownCountry.Items.Insert(0, new ListItem("--Select country--", "0"));
    }

    /// <summary>
    /// This function used to get credit card type for specific hotel.
    /// </summary>
    public void BindCreditCard()
    {
        string strCreditCardType = objCreditCardManager.GetCreditCard(intHotelID).CreditCardType;
        if (strCreditCardType != null)
        {
            string[] creditCardType = strCreditCardType.Split(',');
            if (creditCardType.Length > 0)
            {
                for (int i = 0; i <= creditCardType.Length - 2; i++)
                {
                    dropdownCreditCardType.Items.Add(creditCardType[i].ToString());
                }
                //   dropdownCreditCardType.Items.Insert(0, new ListItem("--Select credit card type--", "0"));
            }
        }
    }


    private Int64 BookingIds = 0;
    public bool DoneBooking(string Filename)
    {
        bool iswl = false;
        string wlLogo = string.Empty;
        if (Request.QueryString["wl"] != null)
        {
            WhiteLabel objwl = objwlManager.GetWhiteLabelByID(Convert.ToInt64(Request.QueryString["wl"]));
            if (objwl != null)
            {
                wlLogo = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Replace("~/", "") + "TargetSiteLogo/" + objwl.TargetSiteLogo; ;
            }
            iswl = true;
        }

        return bm.CreateBooking(objBooking, ref BookingIds, Filename, Convert.ToDecimal(string.IsNullOrEmpty(lblNetTotal.Text.Trim()) ? "0" : lblNetTotal.Text.Trim()), iswl, wlLogo, (IsConvertToRequest==true?BookType.ConvertToRequest:BookType.Booking));
    }
    #endregion

    #region Event
    protected void lnkbtnSend_Click(object sender, EventArgs e)
    {
        SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
        objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
        BindBooking();
        string msg = string.Empty;
        CreditCardDetail obj1 = new CreditCardDetail();
        obj1.UserId = objBooking.CurrentUserId;
        obj1.Name = lblName.Text;
        obj1.Email = lblEmail.Text;
        obj1.Address = txtAddress.Text;
        obj1.PostalCode = txtPostalcode.Text;
        obj1.CountryId = Convert.ToInt32(dropdownCountry.SelectedValue);
        obj1.ContactNumber = txtTelphone.Text;
        obj1.CardType = dropdownCreditCardType.SelectedItem.Text;
        obj1.CardName = txtCreditCardName.Text;
        obj1.CardNumber = txtCreditcardnumber.Text;
        obj1.CvcNumber = txtCvcNumber.Text;
        obj1.ExpiryMonthYear = dropdownMonth.SelectedValue + "/" + dropdownYear.SelectedValue;
        msg = objCreditCardManager.SaveCreditCardDeatil(obj1);
        if (msg == "Detail send successfully.")
        {
            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "succesfuly");
            divmessage.InnerHtml = msg;
        }
        else
        {
            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "error");
            divmessage.InnerHtml = msg;
        }
        #region bookingpdf



        Divdetails.Visible = false;
        Divdetails.Visible = true;

        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        PrepareDivForExport(Divdetails);
        Divdetails.RenderControl(hw);
        StringReader sr = new StringReader(sw.ToString());
        Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);

        Divdetails.Visible = false;
        var dirPath = Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "Temp/");
        var filePath = dirPath + Guid.NewGuid().ToString().Replace("-", string.Empty) + ".pdf";

        try
        {
            FileStream fileStream = new FileStream(filePath, FileMode.Create);
            PdfWriter.GetInstance(pdfDoc, fileStream);
            pdfDoc.Open();
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            htmlparser.Parse(sr);
            fileStream.Flush();
            pdfDoc.Close();
            fileStream.Close();
            fileStream.Dispose();
        }
        catch (Exception ex)
        {
            Divdetails.Visible = false;
            divmessage.Attributes.Add("class", "error");
            divmessage.InnerHtml = ex.ToString();
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        Divdetails.Visible = false;
        if (DoneBooking(filePath))
        {

            DirectoryInfo dir = new DirectoryInfo(dirPath);
            DateTime dateTime = DateTime.Now.AddMinutes(-10);
            foreach (var file in dir.GetFiles().Where(u => u.CreationTime < dateTime).OrderBy(u => u.CreationTime))
            {
                try
                {
                    file.Delete();
                }
                catch(Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }

            Users objUsers = Session["CurrentRestUser"] as Users;
            intUserID = objUsers.UserId;
            StreamWriter SW;
            SW = File.CreateText(Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "BookingFinalize/" + FaxNumber + "_" + DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + "_" + DateTime.Now.Hour + "_" + DateTime.Now.Minute + "_" + DateTime.Now.Second + ".txt"));
            SW.WriteLine(FaxNumber + "_" + DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + "_" + DateTime.Now.Hour + "_" + DateTime.Now.Minute + "_" + DateTime.Now.Second);
            SW.WriteLine("*******************************************");
            SW.WriteLine("Hotel ID : " + objBooking.HotelID);
            HotelManager hm = new HotelManager();
            SW.WriteLine("Hotel Name : " + hm.GetHotelDetailsById(objBooking.HotelID).Name);
            SW.WriteLine("Booking ID : " + BookingIds);
            SW.WriteLine("*******************************************");
            SW.WriteLine("Fax Number : " + FaxNumber);
            SW.WriteLine("User ID : " + obj1.UserId);
            SW.WriteLine("User Name : " + lblName.Text);
            SW.WriteLine("Email : " + lblEmail.Text);
            SW.WriteLine("Address : " + txtAddress.Text);
            SW.WriteLine("Postal Code : " + txtPostalcode.Text);
            SW.WriteLine("Country : " + dropdownCountry.SelectedItem.Text);
            SW.WriteLine("Contact Number : " + txtTelphone.Text);
            SW.WriteLine("Card Type : " + dropdownCreditCardType.SelectedItem.Text);
            SW.WriteLine("Card Name : " + txtCreditCardName.Text);
            SW.WriteLine("Card Number : " + txtCreditcardnumber.Text);
            SW.WriteLine("Cvc Number : " + txtCvcNumber.Text);
            SW.WriteLine("Expiry Date : " + obj1.ExpiryMonthYear);
            SW.Close();
            st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
            st.IsProcessDone = true;
            bm.SaveSearch(st);
            divmessage.Attributes.Add("class", "succesfuly");
            if (IsConvertToRequest)
            {
                divmessage.InnerHtml = GetKeyResult("REQUESTDONESUCCESSFUL") + " (" + BookingIds + ").";
            }
            else
            {
                divmessage.InnerHtml = GetKeyResult("BOOKINGDONESUCCESSFUL") + "(" + BookingIds + ").";
            }
            pnlcreditcard.Visible = false;
            Session["BookingDone"] = "done";
            Session["SerachID"] = null;
        }
        else
        {
            divmessage.Attributes.Add("class", "error");
            divmessage.InnerHtml = GetKeyResult("BOOKINGNOTDONESUCCESSFUL");
        }
        #endregion


    }

    protected void lnkbtnCancel_Click(object sender, EventArgs e)
    {
        SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
        //objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
        st.CurrentStep = 3;
        //st.CurrentDay = objBooking.Duration;
        //st.SearchObject = TrailManager.XmlSerialize(objBooking);
        if (bm.SaveSearch(st))
        {
            if (Request.QueryString["wl"] == null)
            {
                //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "booking-step3/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "booking-step3/english");
                }
                //Response.Redirect("bookingstep3.aspx", false);
            }
            else
            {
                Response.Redirect(SiteRootPath + "bookingstep3.aspx?" + Request.RawUrl.Split('?')[1]);
            }
        }
    }
    #endregion

    #region New Accomodation
    protected void rptAccomodationDaysManager_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Header)
        {
            Repeater rptDays2 = (Repeater)e.Item.FindControl("rptDays2");
            Literal ltrExtendDays = (Literal)e.Item.FindControl("ltrExtendDays");
            long bedid = (from d in objBooking.ManageAccomodationLst select d.BedroomId).FirstOrDefault();
            List<Accomodation> distinctNames = (from d in objBooking.ManageAccomodationLst where d.BedroomId == bedid select d).ToList();
            rptDays2.DataSource = distinctNames;
            rptDays2.DataBind();
            if (distinctNames.Count < 6)
            {
                ltrExtendDays.Text = "<td colspan='" + (6 - distinctNames.Count) + "' style='border-top: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;padding: 5px;' >&nbsp;</td>";
            }
        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblBedroomType = (Label)e.Item.FindControl("lblBedroomType");
            HiddenField hdnBedroomId = (HiddenField)e.Item.FindControl("hdnBedroomId");
            Repeater rptPrice = (Repeater)e.Item.FindControl("rptPrice");
            Repeater rptNoOfDays = (Repeater)e.Item.FindControl("rptNoOfDays");
            Repeater rptSingleQuantity = (Repeater)e.Item.FindControl("rptSingleQuantity");
            Repeater rptDoubleQuantity = (Repeater)e.Item.FindControl("rptDoubleQuantity");
            Literal ltrExtendPrice = (Literal)e.Item.FindControl("ltrExtendPrice");
            Literal ltrExtendNoDays = (Literal)e.Item.FindControl("ltrExtendNoDays");
            Literal ltrExtendSQuantity = (Literal)e.Item.FindControl("ltrExtendSQuantity");
            Literal ltrExtendDQuantity = (Literal)e.Item.FindControl("ltrExtendDQuantity");
            Label lblTotalAccomodation = (Label)e.Item.FindControl("lblTotalAccomodation");
            BedroomManage bedm = e.Item.DataItem as BedroomManage;
            lblBedroomType.Text = Enum.GetName(typeof(BedRoomType), bedm.bedroomType);
            hdnBedroomId.Value = bedm.BedroomID.ToString();
            List<Accomodation> lstaccomo = (from d in objBooking.ManageAccomodationLst where d.BedroomId == bedm.BedroomID select d).ToList();
            rptPrice.DataSource = lstaccomo;
            rptPrice.DataBind();
            rptNoOfDays.DataSource = lstaccomo;
            rptNoOfDays.DataBind();
            rptSingleQuantity.DataSource = lstaccomo;
            rptSingleQuantity.DataBind();
            rptDoubleQuantity.DataSource = lstaccomo;
            rptDoubleQuantity.DataBind();
            if (lstaccomo.Count < 6)
            {
                ltrExtendNoDays.Text = "<td colspan='" + (6 - lstaccomo.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
                ltrExtendPrice.Text = "<td colspan='" + (6 - lstaccomo.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
                ltrExtendSQuantity.Text = "<td colspan='" + (6 - lstaccomo.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
                ltrExtendDQuantity.Text = "<td colspan='" + (6 - lstaccomo.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
            }
            lblTotalAccomodation.Text = CurrencySign + " " + String.Format("{0:#,##,#0.00}", Math.Round((lstaccomo.Sum(a => a.QuantityDouble > 0 ? a.QuantityDouble * a.RoomPriceDouble * CurrencyConvert : 0) + lstaccomo.Sum(a => a.QuantitySingle > 0 ? a.QuantitySingle * a.RoomPriceSingle * CurrencyConvert : 0)), 2));
        }
    }

    protected void rptDays2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblDay = (Label)e.Item.FindControl("lblDay");
            Accomodation a = e.Item.DataItem as Accomodation;
            lblDay.Text = a.DateRequest.ToString("dd/MM/yyyy");
        }
    }

    protected void rptPrice_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPriceDay = (Label)e.Item.FindControl("lblPriceDay");
            Accomodation a = e.Item.DataItem as Accomodation;
            lblPriceDay.Text = CurrencySign + " " + Math.Round(a.RoomPriceSingle * Math.Round(CurrencyConvert, 2), 2).ToString("#,##,##0.00") + " / " + Math.Round(a.RoomPriceDouble * Math.Round(CurrencyConvert, 2), 2).ToString("#,##,##0.00");
        }
    }

    protected void rptNoOfDays_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblAvailableRoomDay = (Label)e.Item.FindControl("lblAvailableRoomDay");
            Accomodation a = e.Item.DataItem as Accomodation;
            lblAvailableRoomDay.Text = a.RoomAvailable.ToString();
        }
    }

    protected void rptSingleQuantity_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblQuantitySDay = (Label)e.Item.FindControl("lblQuantitySDay");
            //HiddenField hdnDate = (HiddenField)e.Item.FindControl("hdnDate");
            //HiddenField hdnPrice = (HiddenField)e.Item.FindControl("hdnPrice");
            Accomodation a = e.Item.DataItem as Accomodation;
            if (a != null)
            {
                lblQuantitySDay.Text = a.QuantitySingle.ToString();
                lblQuantitySDay.Attributes.Add("roomtype", Enum.GetName(typeof(BedRoomType), a.BedroomType));
                lblQuantitySDay.Attributes.Add("maxvalue", a.RoomAvailable.ToString());
                lblQuantitySDay.Attributes.Add("dateavailable", a.DateRequest.ToString("dd/MM/yyyy"));
                lblQuantitySDay.Attributes.Add("price", Math.Round(a.RoomPriceSingle * CurrencyConvert, 2).ToString());
                //hdnDate.Value = a.DateRequest.ToString();
                //hdnPrice.Value = a.RoomPriceSingle.ToString();
            }
        }
    }

    protected void rptDoubleQuantity_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblQuantityDDay = (Label)e.Item.FindControl("lblQuantityDDay");
            HiddenField hdnDate = (HiddenField)e.Item.FindControl("hdnDate");
            HiddenField hdnPrice = (HiddenField)e.Item.FindControl("hdnPrice");
            Accomodation a = e.Item.DataItem as Accomodation;
            if (a != null)
            {
                if (lblQuantityDDay != null)
                {
                    lblQuantityDDay.Text = a.QuantityDouble.ToString();
                    lblQuantityDDay.Attributes.Add("roomtype", Enum.GetName(typeof(BedRoomType), a.BedroomType));
                    lblQuantityDDay.Attributes.Add("maxvalue", a.RoomAvailable.ToString());
                    lblQuantityDDay.Attributes.Add("dateavailable", a.DateRequest.ToString("dd/MM/yyyy"));
                    lblQuantityDDay.Attributes.Add("price", Math.Round(a.RoomPriceDouble * CurrencyConvert, 2).ToString());
                    //hdnDate.Value = a.DateRequest.ToString();
                    //hdnPrice.Value = a.RoomPriceDouble.ToString();
                }
            }
        }
    }
    #endregion

}

class DistinctItemComparer : IEqualityComparer<BedroomManage>
{

    public bool Equals(BedroomManage x, BedroomManage y)
    {
        return x.BedroomID == y.BedroomID &&
            x.bedroomType == y.bedroomType;
    }

    public int GetHashCode(BedroomManage obj)
    {
        return obj.BedroomID.GetHashCode() ^
            obj.bedroomType.GetHashCode();
    }
}