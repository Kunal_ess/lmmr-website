﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using log4net;
using log4net.Config;
#endregion

public partial class ChangePassword : BasePage
{
    #region Variable declaration

    PasswordManager objPassword = new PasswordManager();
    int intUserID = 0;
    
    #endregion

    #region Page Load

    protected void Page_Load(object sender, EventArgs e)
    {
        
        try
        {
            if (Session["CurrentRestUserID"] != null)
            {
                Session["LinkID"] = "lnkchangePassword";
                intUserID = Convert.ToInt32(Session["CurrentRestUserID"]);
                if (!IsPostBack)
                {
                    divmessage.Style.Add("display", "none");
                    divmessage.Attributes.Add("class", "error");
                }
            }
            else
            {
                //Response.Redirect("~/login.aspx", true);
                //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "login/english");
                }
            }
            filStaticContent();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }

    #endregion

    #region Events

    /// <summary>
    /// This method is call on the click of save button.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            //update pssword here.
            string strMsg = objPassword.UpdatePassword(intUserID, txtOldPassword.Text, txtNewPassword.Text.Trim().Replace("'", ""));
            if (strMsg == "Password update Successfully")
            {
                Session["passwordChangeStatus"] = strMsg;
                //Response.Redirect("Default.aspx");
                //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "default/english");
                }
            }
            else if (strMsg == "Please enter correct old password")
            {
                divmessage.InnerHtml = GetKeyResult("PLEASEENTERCORRECTOLDPASSWORD");
                divmessage.Attributes.Add("class", "error");
                divmessage.Style.Add("display", "block");
            }
            else
            {
                divmessage.InnerHtml = GetKeyResult(strMsg.Replace(" ","").ToUpper());
                divmessage.Attributes.Add("class", "error");
                divmessage.Style.Add("display", "block");
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This method is used to reset the all fields.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        //Response.Redirect("Default.aspx");
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "default/english");
        }
    }

    #endregion

    /// <summary>
    /// This method returns the resultant string for the passed key
    /// </summary>
    /// <param name="key"></param>
    /// <returns>string</returns>
    public string GetKeyResult(string key)
    {
        return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    }

    public void filStaticContent()
    {
        btnSave.Text = GetKeyResult("SAVE");
        btnCancel.Text = GetKeyResult("CANCEL");
    }
}