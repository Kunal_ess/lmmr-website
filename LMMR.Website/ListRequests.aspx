﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeMaster.master" AutoEventWireup="true"
    CodeFile="ListRequests.aspx.cs" Inherits="ListRequests" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/UserControl/HotelUser/ListRequests.ascx" TagName="requestDetails1" TagPrefix="uc2" %>
<%@ Register Src="~/UserControl/HotelUser/BookingDetails.ascx" TagName="BookingDetails" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntLeft" runat="Server">
    <style type="text/css">
        .modalBackground
        {
            background-color: #CCCCFF;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }
    </style>
    <link href="../css/stylew423.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .LinkPaging
        {
            width: 20;
            background-color: White;
            border: Solid 1 Black;
            text-align: center;
            margin-left: 8;
        }
    </style>
    <asp:UpdateProgress ID="uprog" runat="server" AssociatedUpdatePanelID="updTest">
        <ProgressTemplate>
            <div id="Loding_overlay" style="display: block;">
                <img src="<%= SiteRootPath %>Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
                Loading...
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="Loding_overlay" style="display: block;">
                <img src="<%= SiteRootPath %>Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
                Loading...
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="contract-list11">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <%--<td width="5%"><a href="#" class="pro">Profile</a></td>--%>
                <td width="14%">
                    <b>To retrieve your request</b>
                </td>
                <td width="11%">
                    <asp:TextBox ID="txtBookingID" runat="server" MaxLength="10" CssClass="dateinputf"></asp:TextBox>
                    <asp:FilteredTextBoxExtender ID="txtBookingIDExtender" runat="server" TargetControlID="txtBookingID"
                        FilterType="Numbers">
                    </asp:FilteredTextBoxExtender>
                </td>
                <td width="67%">
                    <asp:LinkButton ID="lbtGo" runat="server" CssClass="gobtn" OnClick="lbtGo_Click"><%= GetKeyResult("GO")%></asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbtReset"
                    runat="server" CssClass="gobtn" onclick="lbtReset_Click"><%= GetKeyResult("RESET")%></asp:LinkButton>
                </td>
                <%--<td width="62%"><a href="#" class="pro">Future booking</a></td>--%>
            </tr>
        </table>
    </div>
    <div class="contract-list11">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <%--<td width="12%">
                    <img src="Images/pdf-icon.png" align="absmiddle" />
                    save as pdf
                </td>--%>
                <td width="9%">
                    <b>Arrival date :</b>
                </td>
                <td width="4%">
                    from
                </td>
                <td width="11%">
                    <asp:TextBox ID="txtFrom" runat="server" MaxLength="20" CssClass="dateinputf"></asp:TextBox>
                </td>
                <td width="4%">
                    <input type="image" src="<%= SiteRootPath %>Images/dateicon.png" id="calFrom2" tabindex="1" /><asp:CalendarExtender
                        ID="calExTO" runat="server" TargetControlID="txtFrom" PopupButtonID="calFrom2"
                        Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                </td>
                <td width="2%">
                    to
                </td>
                <td width="11%">
                    <asp:TextBox ID="txtTo" runat="server" MaxLength="20" CssClass="dateinputf"></asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTo"
                        PopupButtonID="calTo" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                </td>
                <td width="47%">
                    <input type="image" src="<%= SiteRootPath %>Images/dateicon.png" id="calTo" />
                </td>
            </tr>
        </table>
    </div>
    <div class="contract-list01">
        <div class="contract-list-right" style="width: 676px;">
            <ul runat="server" id="AlphaList">
                <li id="Li1" runat="server"><a href="#" class="select" runat="server" id="all" onserverclick="PageChange">all</a></li>
                <li id="Li2" runat="server"><a href="#" runat="server" id="a" onserverclick="PageChange">a</a></li>
                <li id="Li3" runat="server"><a href="#" runat="server" id="b" onserverclick="PageChange">b</a></li>
                <li id="Li4" runat="server"><a href="#" runat="server" id="c" onserverclick="PageChange">c</a></li>
                <li id="Li5" runat="server"><a href="#" runat="server" id="d" onserverclick="PageChange">d</a></li>
                <li id="Li6" runat="server"><a href="#" runat="server" id="e" onserverclick="PageChange">e</a></li>
                <li id="Li7" runat="server"><a href="#" runat="server" id="f" onserverclick="PageChange">f</a></li>
                <li id="Li8" runat="server"><a href="#" runat="server" id="g" onserverclick="PageChange">g</a></li>
                <li id="Li9" runat="server"><a href="#" runat="server" id="h" onserverclick="PageChange">h</a></li>
                <li id="Li10" runat="server"><a href="#" runat="server" id="i" onserverclick="PageChange">i</a></li>
                <li id="Li11" runat="server"><a href="#" runat="server" id="j" onserverclick="PageChange">j</a></li>
                <li id="Li12" runat="server"><a href="#" runat="server" id="k" onserverclick="PageChange">k</a></li>
                <li id="Li13" runat="server"><a href="#" runat="server" id="l" onserverclick="PageChange">l</a></li>
                <li id="Li14" runat="server"><a href="#" runat="server" id="m" onserverclick="PageChange">m</a></li>
                <li id="Li15" runat="server"><a href="#" runat="server" id="n" onserverclick="PageChange">n</a></li>
                <li id="Li16" runat="server"><a href="#" runat="server" id="o" onserverclick="PageChange">o</a></li>
                <li id="Li17" runat="server"><a href="#" runat="server" id="p" onserverclick="PageChange">p</a></li>
                <li id="Li18" runat="server"><a href="#" runat="server" id="q" onserverclick="PageChange">q</a></li>
                <li id="Li19" runat="server"><a href="#" runat="server" id="r" onserverclick="PageChange">r</a></li>
                <li id="Li20" runat="server"><a href="#" runat="server" id="s" onserverclick="PageChange">s</a></li>
                <li id="Li21" runat="server"><a href="#" runat="server" id="t" onserverclick="PageChange">t</a></li>
                <li id="Li22" runat="server"><a href="#" runat="server" id="u" onserverclick="PageChange">u</a></li>
                <li id="Li23" runat="server"><a href="#" runat="server" id="v" onserverclick="PageChange">v</a></li>
                <li id="Li24" runat="server"><a href="#" runat="server" id="w" onserverclick="PageChange">w</a></li>
                <li id="Li25" runat="server"><a href="#" runat="server" id="x" onserverclick="PageChange">x</a></li>
                <li id="Li26" runat="server"><a href="#" runat="server" id="y" onserverclick="PageChange">y</a></li>
                <li id="Li27" runat="server"><a href="#" runat="server" id="z" onserverclick="PageChange">z</a></li>
            </ul>
        </div>
    </div>
    <div id="booking-details" class="booking-details">
      
        <asp:UpdatePanel runat="server" ID="updTest">
            <ContentTemplate>
                <asp:HiddenField ID="hdnSelectedRowID" runat="server" Value="0" />
                <ul class="message">
                    <li>
                <div id="divGrid">
                    <asp:GridView ID="grdViewBooking" runat="server" Width="973px" AutoGenerateColumns="False"
                        CellPadding="0" CellSpacing="1" OnRowDataBound="grdViewBooking_RowDataBound" RowStyle-HorizontalAlign="Center" RowStyle-VerticalAlign="Middle"
                        EditRowStyle-VerticalAlign="Top" OnPageIndexChanging="grdViewBooking_PageIndexChanging" EmptyDataRowStyle-Font-Bold="true"
                        EmptyDataRowStyle-HorizontalAlign="Center" AllowPaging="true" GridLines="None" PageSize="20" CssClass="cofig" BackColor="#ffffff"
                        ShowHeader="true" ShowHeaderWhenEmpty="true" OnRowCreated="grdViewBooking_RowCreated">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblRefNo" runat="server" Text="No."></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lblRefNo" runat="server" ToolTip='<%# Eval("HotelID") %>' CommandName="high"
                                        Text='<%# Eval("Id") %>' CommandArgument='<%# ((GridViewRow) Container).RowIndex %>'
                                        ForeColor="#339966" OnClick="lblRefNo_Click"></asp:LinkButton>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:LinkButton ID="lblRefNo" runat="server" ToolTip='<%# Eval("HotelID") %>' CommandName="high"
                                        Text='<%# Eval("Id") %>' CommandArgument='<%# ((GridViewRow) Container).RowIndex %>'
                                        ForeColor="#339966" OnClick="lblRefNo_Click"></asp:LinkButton>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1"  HorizontalAlign="Center"/>
                                <ItemStyle CssClass="con1-earned" />
                                <HeaderTemplate>
                                    <asp:DropDownList ID="cityDDL" runat="server" CssClass="smallselect" AutoPostBack="true"
                                        OnSelectedIndexChanged="cityDDL_selectedIndexChanged">
                                    </asp:DropDownList>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCityName" runat="server" ToolTip='<%# Eval("HotelId") %>'></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblCityName" runat="server" ToolTip='<%# Eval("HotelId") %>'></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con1-earned" />
                                <HeaderTemplate>
                                    <asp:DropDownList ID="hotelDDL" runat="server" CssClass="midselect" AutoPostBack="true"
                                        OnSelectedIndexChanged="hotelDDL_selectedIndexChanged">
                                    </asp:DropDownList>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblHotelName" runat="server" ToolTip='<%# Eval("HotelId") %>' Text='<%# Eval("HotelName") %>'></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblHotelName" runat="server" ToolTip='<%# Eval("HotelId") %>' Text='<%# Eval("HotelName") %>'></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row2" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con1-earned" />
                                <HeaderTemplate>
                                    <asp:DropDownList ID="meetingRoomDDL" runat="server" CssClass="midselect" AutoPostBack="true"
                                        OnSelectedIndexChanged="meetingRoomDDL_selectedIndexChanged">
                                    </asp:DropDownList>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblMeetingRoom" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblMeetingRoom" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con1-earned" />
                                <HeaderTemplate>
                                    <asp:DropDownList ID="bookingDateDDL" runat="server" CssClass="midselect" AutoPostBack="true"
                                        OnSelectedIndexChanged="bookingDateDDL_selectedIndexChanged">
                                    </asp:DropDownList>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblBookingDt" runat="server" Text='<%# Eval("BookingDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblBookingDt" runat="server" Text='<%# Eval("BookingDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con1-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblArrival" runat="server" Text="Arrival"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblArrivalDt" runat="server" Text='<%# String.Format("{0:dd/MM/yyyy}",Eval("ArrivalDate"))%>'></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblArrivalDt" runat="server" Text='<%# String.Format("{0:dd/MM/yyyy}",Eval("ArrivalDate"))%>'></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" HorizontalAlign="Center" />
                                <ItemStyle CssClass="con1-earned" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblDeparture" runat="server" Text="Departure"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblDepartureDt" runat="server" Text='<%#  String.Format("{0:dd/MM/yyyy}", Eval("DepartureDate"))%>' ToolTip='<%# Eval("DepartureDate")%>'>
                                    </asp:Label>
                                    <asp:LinkButton ID="btnchkcommision" runat="server" ForeColor="#339966" CommandArgument='<%# Eval("Id") %>'
                                        OnClick="btnchkcommision_Click"></asp:LinkButton>
                                        
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblDepartureDt" runat="server" Text='<%#  String.Format("{0:dd/MM/yyyy}", Eval("DepartureDate"))%>'
                                        ToolTip='<%# Eval("DepartureDate")%>'></asp:Label>
                                        <asp:LinkButton ID="btnchkcommision" runat="server" ForeColor="#339966" CommandArgument='<%# Eval("Id") %>'
                                        OnClick="btnchkcommision_Click"></asp:LinkButton>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                           <%-- <asp:TemplateField>
                                <HeaderStyle CssClass="heading-earned-row1" />
                                <ItemStyle CssClass="con1-earned" />
                                <HeaderTemplate>
                                    <asp:DropDownList ID="valueDDL" runat="server" CssClass="smallselect" OnSelectedIndexChanged="valueDDL_selectedIndexChanged"
                                        AutoPostBack="true">
                                    </asp:DropDownList>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblFinalTotal" runat="server" Text='<%# String.Format("{0:###,###,###}",Eval("Finaltotalprice")) %>'></asp:Label>
                                    <asp:LinkButton ID="btnchkcommision" runat="server" ForeColor="#339966" CommandArgument='<%# Eval("Id") %>'
                                        OnClick="btnchkcommision_Click"></asp:LinkButton>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblFinalTotal" runat="server" Text='<%# String.Format("{0:###,###,###}",Eval("Finaltotalprice")) %>'></asp:Label>
                                    <asp:LinkButton ID="btnchkcommision" runat="server" ForeColor="#339966" CommandArgument='<%# Eval("Id") %>'
                                        OnClick="btnchkcommision_Click"></asp:LinkButton>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>--%>
                            
                        </Columns>
                        <EmptyDataTemplate>
                            <tr>
                                <td colspan="11" align="center">
                                    "No record Found!"
                                </td>
                            </tr>
                        </EmptyDataTemplate>
                        <PagerSettings Mode="NumericFirstLast" Position="Top" />
                        <PagerStyle HorizontalAlign="Right" BackColor="White" />
                        <PagerTemplate>
                                    <div style="float: right; width: auto;">
                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                    </div>
                        </PagerTemplate>
                    </asp:GridView>
                </div>  
                </li>
                </ul>
                <div class="booking-details" id="divprintpdfllink" runat="server">
                    <ul>
                        <li class="value3" style="width: 973px;">
                            <div class="col9" style="width: 970px;">
                                <img src="Images/print.png" />&nbsp; <a id="uiLinkButtonPrintGrid" style="cursor: pointer;"
                                    onclick="javascript:Button1_onclick('divGrid');">Print</a> &bull;
                                <img src="Images/pdf.png" />&nbsp;<asp:LinkButton ID="uiLinkButtonSaveAsPdfGrid"
                                    runat="server" OnClick="uiLinkButtonSaveAsPdfGrid_Click">Save as PDF</asp:LinkButton>
                            </div>
                        </li>
                    </ul>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="uiLinkButtonSaveAsPdfGrid" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="divbookingdetails" runat="server" visible="false">
                <h1 class="new">
                    Request details</h1>
                <br />
                <div class="booking-details" id="divprint" runat="server">
                    <ul>
                        <li class="value3">
                            <div class="col9" style="width: 973px">
                                <img src="Images/print.png" />&nbsp; <a id="ADetails" style="cursor: pointer;" onclick="javascript:Button1_onclick('<%= Divdetails.ClientID%>');">
                                    Print</a> &bull;
                                <img src="Images/pdf.png" />&nbsp;<asp:LinkButton ID="lnkSavePDF" runat="server"
                                    OnClick="lnkSavePDF_Click">Save as PDF</asp:LinkButton>
                            </div>
                        </li>
                    </ul>
                </div>
                <div id="Divdetails" runat="server"><%--style="float:left;width:978px"--%>
                    <uc2:requestDetails1 ID="requestDetails1" runat="server">
                    </uc2:requestDetails1>
                    <uc1:BookingDetails ID="bookingDetails" runat="server"></uc1:BookingDetails>
                </div>
              
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lnkSavePDF" />
        </Triggers>
    </asp:UpdatePanel>
    <br />
    <div>
        <asp:LinkButton ID="lnkbtn" runat="server"></asp:LinkButton>
        <asp:ModalPopupExtender ID="modalcheckcomm" TargetControlID="lnkbtn" BackgroundCssClass="modalBackground"
            PopupControlID="pnlchkCommission" runat="server">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlchkCommission" BorderColor="#999999" Width="757px" BorderWidth="5"
            Style="display: none; padding-top: 7px;" runat="server" BackColor="White">
            <div class="popup-mid-inner-body" align="center">
                Here you can confirm Request revenue or adjust it.
            </div>
            <div id="divmessage" runat="server">
            </div>
            <div class="popup-mid-inner-body1">
                <asp:UpdatePanel ID="upcheckcomm" runat="server">
                    <ContentTemplate>
                        <table width="100%" cellspacing="0" cellpadding="3" align="center">
                            <tr>
                                <td class="style1">
                                    <table width="100%">
                                        <tr>
                                            <td align="center" colspan="2">
                                                <b>Adjust Revenue (if needed)</b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                Insert Netto (VAT excluded) :
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtrealvalue" runat="server" MaxLength="7"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtenderrealvalue" runat="server"
                                                    TargetControlID="txtrealvalue" ValidChars="." FilterMode="ValidChars" FilterType="Numbers,Custom">
                                                </asp:FilteredTextBoxExtender>
                                                <asp:RequiredFieldValidator ID="reqtxtvalue" runat="server" ControlToValidate="txtrealvalue"
                                                    ValidationGroup="popup" ErrorMessage="*"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                Reason :
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlreason" runat="server" CssClass="NoClassApply">
                                                    <asp:ListItem>Increase number of delegates</asp:ListItem>
                                                    <asp:ListItem>Decrease number of delegates</asp:ListItem>
                                                    <asp:ListItem>Changes in package</asp:ListItem>
                                                    <asp:ListItem>Changes in F&B</asp:ListItem>
                                                    <asp:ListItem>Changes in Duration</asp:ListItem>
                                                    <asp:ListItem>Changes in room nights</asp:ListItem>
                                                    <asp:ListItem>No-Show</asp:ListItem>
                                                    <asp:ListItem>Cancelled</asp:ListItem>
                                                </asp:DropDownList>
                                                <%--<asp:TextBox ID="txtreason" runat="server"></asp:TextBox>--%>
                                                <%-- <asp:RequiredFieldValidator
                            ID="reqreason" runat="server" ControlToValidate="ddlreason" ValidationGroup="popup" InitialValue="0" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" valign="top">
                                                Supporting document :
                                            </td>
                                            <td>
                                                <asp:FileUpload ID="ulPlan" runat="server" /><br />
                                                <span style="font-family: Arial; font-size: Smaller; color: Gray">Supporting document
                                                    file format: PDF/Word/Excel & max size 1MB. </span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr valign="bottom">
                                <td align="center" valign="bottom">
                                    <br />
                                    <asp:CheckBox ID="chkmeetingNotheld" runat="server" onclick="callme()" Text="The meeting was not held" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div>
                <div class="popup-mid-inner-body2_i">
                </div>
                <div class="booking-details" style="width: 760px;">
                    <ul>
                        <li class="value10">
                            <div class="col21" style="width: 752px;">
                                <div class="button_section">
                                    <asp:LinkButton ID="btnSubmit" runat="server" Text="Save" ValidationGroup="popup"
                                        CssClass="select" OnClick="btnSubmit_Click" />
                                    <span>or</span>
                                    <asp:LinkButton ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div>
        <asp:LinkButton ID="lnkcheck" runat="server"></asp:LinkButton>
        <asp:ModalPopupExtender ID="ModalPopupCheck" TargetControlID="lnkcheck" BackgroundCssClass="modalBackground"
            PopupControlID="pnlcheck" runat="server">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlcheck" BorderColor="Black" Width="800px" BorderWidth="1" Style="display: none;"
            runat="server" BackColor="White">
            <div class="popup-mid-inner-body" align="center">
            </div>
            <div class="popup-mid-inner-body1">
                <table width="100%" cellspacing="0" cellpadding="3" align="center">
                    <tr>
                        <td colspan="2">
                            <div class="warning">
                                Please first update status to Definative or Cancel and then press "Check Commission"
                                button</div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Status
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlstatus" Width="200px" runat="server">
                                <asp:ListItem Value="7">Definative</asp:ListItem>
                                <asp:ListItem Value="2">Cancel</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </div>
            <div>
                <div class="popup-mid-inner-body2_i">
                </div>
                <div class="booking-details" style="width: 760px;" id="div5">
                    <ul>
                        <li class="value10">
                            <div class="col21" style="width: 752px;">
                                <div class="button_section">
                                    <asp:LinkButton ID="lnkchecksubmit" runat="server" Text="Save" CssClass="select" /><%--OnClick="btnSubmit_Click"--%>
                                    <span>or</span>
                                    <asp:LinkButton ID="lnkcheckcancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </asp:Panel>
    </div>
    <script language="javascript" type="text/javascript">

        function Button1_onclick(strid) {

            var prtContent = document.getElementById(strid);
            var WinPrint = window.open('', '', 'letf=0,top=0,width=800,height=800,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();


        }


        function callme() {
            if (jQuery("#contentbody_chkmeetingNotheld").is(":checked")) {

                jQuery("#contentbody_txtrealvalue").val('');
                jQuery("#contentbody_txtrealvalue").attr("disabled", true);
                jQuery("#contentbody_ddlreason").attr("disabled", true);
                jQuery("#contentbody_ulPlan").val('');
                jQuery("#contentbody_ulPlan").attr("disabled", true);

            }
            else {

                jQuery("#contentbody_txtrealvalue").val('');
                jQuery("#contentbody_txtrealvalue").attr("disabled", false);
                jQuery("#contentbody_ddlreason").attr("disabled", false);
                jQuery("#contentbody_ulPlan").val('');
                jQuery("#contentbody_ulPlan").attr("disabled", false);
            }
        }

        

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMainbody" runat="Server">
    <div class="mainbody-left">
        <div class="banner">
            <iframe src="<%= SiteRootPath %>SlideShow.aspx" width="100%" height="100%"></iframe>
        </div>
    </div>
    <div class="mainbody-right">
        <div id="beforeLogin" runat="server">
            <div class="mainbody-right-call">
                <div class="need">
                    <span>
                        <%= GetKeyResult("NEEDHELP")%></span>
                    <br />
                    <%= GetKeyResult("CALLUS")%>
                    5/7
                </div>
                <div class="phone">
                    +32 2 344 25 50 </div>
                <p style="font-size: 10px; color: White; text-align: center">
                    <%= GetKeyResult("CALLUSANDWEDOITFORYOU")%></p>
                <div class="mail">
                        <a href="http://needameetingroom.com/requestform/" onClick="javascript: _gaq.push(['_trackPageview', '/email']); window.open(this.href, 'popupwindow1','width=900,height=600,scrollbars,resizable,toolbar=NO'); &#13;&#10;return false;" Target="_blank"">
                        Send us your RFP</a>
                    </div>
            </div>
            <div class="mainbody-right-join" id="divjoinus" runat="server">
               <div class="join">
                    <a runat="server" id="joinToday">
                        <%= GetKeyResult("JOINUSTODAY")%></a></div>
                <%= GetKeyResult("FORHOTELSMEETINGFACILITIESEVENT")%>
            </div>
            <div class="mainbody-right-you">
                <a href="http://www.youtube.com/watch?v=rkt3NIWecG8&feature=youtu.be" target="_blank">
                    <img src="<%= SiteRootPath %>images/youtube-icon.png" /></a>&nbsp;&nbsp;<a href="http://www.youtube.com/watch?v=U3y7d7yxmvE&feature=relmfu"
                        target="_blank"><img src="<%= SiteRootPath %>images/small-vedio.png" /></a>
            </div>
        </div>
        <div style="width: 216px; height: auto; overflow: hidden; margin: 0px auto; display: none"
            id="afterLogin" runat="server">
            <div class="mainbody-right-call">
                <div class="need">
                    <span>
                        <%= GetKeyResult("NEEDHELP")%></span>
                    <br />
                    <%= GetKeyResult("CALLUS")%>
                    5/7
                </div>
                <div class="phone">
                    +32 2 344 25 50 </div>
                <p style="font-size: 10px; color: White; text-align: center">
                    CALL US and WE DO IT FOR YOU</p>
                <div class="mail">
                        <a href="http://needameetingroom.com/requestform/" onClick="javascript: _gaq.push(['_trackPageview', '/email']); window.open(this.href, 'popupwindow1','width=900,height=600,scrollbars,resizable,toolbar=NO'); &#13;&#10;return false;" Target="_blank"">
                        Send us your RFP</a>
                    </div>
            </div>
            <!--start mainbody-right-afterlogin-body -->
            <div class="mainbody-right-afterlogin-body">
                <div class="mainbody-right-afterlogin-top">
                    <div class="mainbody-right-afterlogin-inner">
                        <b>Today :</b>
                        <asp:Label ID="lstLoginTime" runat="server"></asp:Label>
                        <div class="afterlogin-welcome">
                            <span>Welcome</span>
                            <br>
                            <asp:Label ID="lblLoginUserName" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="mainbody-right-afterlogin-bottom">
                    <div class="mainbody-right-afterlogin-bottom-inner">
                        <asp:LinkButton ID="hypManageProfile" runat="server" OnClick="hypManageProfile_Click">Manage profile</asp:LinkButton> <br/>
			            <asp:LinkButton ID="hypChangepassword" runat="server" OnClick="hypChangepassword_Click">Change password</asp:LinkButton><br/>
                        <asp:LinkButton ID="hypListBookings" runat="server" OnClick="hypListBookings_Click" Visible="true">My bookings</asp:LinkButton><br/>
                        <asp:LinkButton ID="hypListRequests" runat="server" OnClick="hypListRequests_Click" Visible="true">My requests</asp:LinkButton>
                    </div>
                </div>
            </div>
            <!--end mainbody-right-afterlogin-body -->
        </div>
    </div>
    <script language="javascript" type="text/javascript">

        function Button1_onclick(strid) {

            var prtContent = document.getElementById(strid);
            var WinPrint = window.open('', '', 'letf=0,top=0,width=800,height=800,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();


        }


    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#<%= txtFrom.ClientID %>").attr("disabled", true);
            jQuery("#<%= txtTo.ClientID %>").attr("disabled", true);
            jQuery("#<%= lbtGo.ClientID%>").bind("click", function () {
                jQuery(".error").html("");
                jQuery(".error").hide();
                jQuery("#<%= txtFrom.ClientID %>").attr("disabled", false);
                jQuery("#<%= txtTo.ClientID %>").attr("disabled", false);
                jQuery("#Loding_overlay").show();
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                var fromdate = jQuery("#<%= txtFrom.ClientID %>").val();
                var todate = jQuery("#<%= txtTo.ClientID %>").val();
                if (fromdate.length != 0 && todate.length != 0) {
                    var todayArr = todate.split('/');
                    var formdayArr = fromdate.split('/');
                    var fromdatecheck = new Date();
                    var todatecheck = new Date();
                    fromdatecheck.setFullYear(parseInt("20" + formdayArr[2]), (parseInt(formdayArr[1]) - 1), formdayArr[0]);
                    todatecheck.setFullYear(parseInt("20" + todayArr[2]), (parseInt(todayArr[1]) - 1), todayArr[0]);
                    if (fromdatecheck > todatecheck) {
                        alert('\"From date\" must be earlier or equal to \"To date\".');
                        return false;
                    }
                }
            });
        });
    </script>
    <script language="javascript" type="text/javascript">
                        jQuery(document).ready(function () {
                            <% if (ViewState["SearchAlpha"] != null) {%>
                                jQuery('#<%= AlphaList.ClientID %> li a').removeClass('select');
                                jQuery('#cntLeftBottom_cntLeft_<%= ViewState["SearchAlpha"]%>').addClass('select');
                            <% }%>
                        });
    </script>

    
</asp:Content>
