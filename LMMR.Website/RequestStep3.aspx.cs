﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LMMR.Business;
using LMMR.Entities;
using log4net;
using System.Configuration;

public partial class RequestStep3 : BasePage
{
    #region Variables and Properties
    HotelManager objHotel = new HotelManager();
    PackagePricingManager objPackagePricingManager = new PackagePricingManager();
    Hotel objRequestHotel = new Hotel();
    BookingManager bm = new BookingManager();
    int countMr = 0;
    CreateRequest objRequest = null;
    
    public List<PackageItemDetails> SelectedPackage
    {
        get;
        set;
    }
    public List<RequestExtra> SelectedExtra
    {
        get;
        set;
    }
    public TList<PackageItems> AllPackageItem
    {
        get;
        set;
    }
    public TList<PackageItems> AllFoodAndBravrages
    {
        get;
        set;
    }
    public TList<PackageItems> AllEquipments
    {
        get;
        set;
    }
    public TList<PackageItems> AllOthersItem
    {
        get;
        set;
    }
    public TList<PackageItemMapping> AllPackageItemMapping
    {
        get;
        set;
    }
    
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["CurrentRestUserID"] == null)
            {
                if (Request.QueryString["wl"] == null)
                {
                    //Response.Redirect("Login.aspx");
                    //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                    if (l != null)
                    {
                        Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                    }
                    else
                    {
                        Response.Redirect(SiteRootPath + "login/english");
                    }
                }
            }
            if (!Page.IsPostBack)
            {
                Users objUsers = Session["CurrentRestUser"] as Users;
                //Bind Login users details
                lblDateLogin.Text = DateTime.Now.ToString("dd MMM yyyy");
                lblUserName.Text = objUsers.FirstName + " " + objUsers.LastName;
                CheckSearchAvailable();
                hypTermsandCondition.NavigateUrl = ConfigurationManager.AppSettings["FilePath"] + "MediaFile/TermsAndConditions.pdf";
                hypTermsandCondition.Target = "_blank";
                hypTermsandCondition.Attributes.Add("onclick", "return open_win('" + SiteRootPath + hypTermsandCondition.NavigateUrl.Replace("~/", "") + "');");
            }
            //hypTermsandCondition.NavigateUrl = ConfigurationManager.AppSettings["FilePath"] + "MediaFile/Termsandconditions.pdf";
            //hypTermsandCondition.Target = "_blank";
            //hypTermsandCondition.Attributes.Add("onclick", "return open_win('" + hypTermsandCondition.NavigateUrl.Replace("~/","") + "');");
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Additional methods
    /// <summary>
    /// Check Availability of Serach according To Serach ID.
    /// </summary>
    public void CheckSearchAvailable()
    {
        if (Session["RequestID"] != null)
        {
            SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["RequestID"]));
            if (st != null)
            {
                if (st.CurrentStep == 1)
                {
                    if (Request.QueryString["wl"] == null)
                    {
                        //Response.Redirect("RequestStep1.aspx", false);
                        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                        if (l != null)
                        {
                            Response.Redirect(SiteRootPath + "request-step1/" + l.Name.ToLower());
                        }
                        else
                        {
                            Response.Redirect(SiteRootPath + "request-step1/english");
                        }
                    }
                    else
                    {
                        Response.Redirect(SiteRootPath + "requeststep1.aspx?" + Request.RawUrl.Split('?')[1]);
                    }
                }
                else if (st.CurrentStep == 2)
                {
                    if (Request.QueryString["wl"] == null)
                    {
                        //Response.Redirect("RequestStep2.aspx", false);
                        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                        if (l != null)
                        {
                            Response.Redirect(SiteRootPath + "request-step2/" + l.Name.ToLower());
                        }
                        else
                        {
                            Response.Redirect(SiteRootPath + "request-step2/english");
                        }
                    }
                    else
                    {
                        Response.Redirect(SiteRootPath + "requeststep2.aspx?" + Request.RawUrl.Split('?')[1]);
                    }
                }
                else if (st.CurrentStep == 3)
                {
                    objRequest = (CreateRequest)TrailManager.XmlDeserialize(typeof(CreateRequest), st.SearchObject);
                    //Bind Booking Details
                    BindHotel();
                }
                else if (st.CurrentStep == 4)
                {
                    if (Request.QueryString["wl"] == null)
                    {
                        //Response.Redirect("RequestStep4.aspx", false);
                        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                        if (l != null)
                        {
                            Response.Redirect(SiteRootPath + "request-step4/" + l.Name.ToLower());
                        }
                        else
                        {
                            Response.Redirect(SiteRootPath + "request-step4/english");
                        }
                    }
                    else
                    {
                        Response.Redirect(SiteRootPath + "requeststep4.aspx?" + Request.RawUrl.Split('?')[1]);
                    }
                }
            }
        }
        else
        {
            if (Request.QueryString["wl"] == null)
            {
                //Response.Redirect("Login.aspx",false);
                //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "login/english");
                }
            }
        }
    }
    public void BindHotel()
    {
        try
        {
            rptHotel.DataSource = objRequest.HotelList.Where(a => a.MeetingroomList.Count > 0);
            rptHotel.DataBind();
            lblArrival.Text = objRequest.ArivalDate.ToString("dd MMM yyyy");
            lblDeparture.Text = objRequest.DepartureDate.ToString("dd MMM yyyy");
            Int64 intHotelID = objRequest.HotelList[0].HotelID;
            if (objRequest.PackageID != 0)
            {
                pnlPackage.Visible = true;
                PackageMaster packagedetails = objPackagePricingManager.GetAllPackageName().Where(a => a.Id == objRequest.PackageID).FirstOrDefault();
                if (packagedetails != null)
                {
                    lblPackageName.Text = packagedetails.PackageName;
                    //if (packagedetails.PackageName == "Standard")
                    //{
                    //    lblpackageDescription.Text = GetKeyResult("STANDARDDESCRIPTION"); //"Package includes main meeting room rental, note-pads, pencils, flipchart and mineral water Morning coffee break, Sandwich buffet with salads, Afternoon coffee break, Softdrinks, coffee and tea for half day packages, either morning or afternoon break is not included only as from min. 10 people.";

                    //}
                    //else if (packagedetails.PackageName == "Favourite")
                    //{
                    //    lblpackageDescription.Text = GetKeyResult("FAVOURITEDESCRIPTION"); //"Package includes main meeting room rental, note-pads, pencils, flipchart and mineral water Morning coffee break, 2-course menu, Afternoon coffee break, softdrinks, coffee and tea for half day packages, either morning or afternoon break is not included.";
                    //}
                    //else if (packagedetails.PackageName == "Elegant")
                    //{
                    //    lblpackageDescription.Text = GetKeyResult("ELEGANTDESCRIPTION"); //"Package includes main meeting room rental, note-pads, pencils, flipchart and mineral water Morning coffee break, Hot & Cold buffet, Afternoon coffee break, softdrinks, coffee and tea for half day packages, either morning or afternoon break is not included only as from min. 20 people.";
                    //}
                    //else
                    //{
                    //    lblpackageDescription.Text = "";
                    //}
                    objHotel.GetDescriptionofPackageByPackage(packagedetails);
                    PackageMasterDescription des = packagedetails.PackageMasterDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64(Session["LanguageID"])).FirstOrDefault();
                    if (des == null)
                    {
                        des = packagedetails.PackageMasterDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64("1")).FirstOrDefault();
                    }
                    if (des != null)
                    {
                        lblpackageDescription.Text = des.Description;
                    }
                }
                AllPackageItem = objPackagePricingManager.GetAllPackageItems();
                rptPackageItem.DataSource = objPackagePricingManager.GetPackageItemsByPackageID(objRequest.PackageID);
                rptPackageItem.DataBind();
                if (objRequest.ExtraList.Count > 0)
                {
                    //Package Selection.
                    
                    pnlExtra.Visible = true;
                    rptExtras.DataSource = objRequest.ExtraList;
                    rptExtras.DataBind();//check AllPackageItem.Where(a => a.IsExtra == true)
                }
                else
                {
                    pnlExtra.Visible = false;
                }
                pnlFoodAndBravrages.Visible = false;
            }
            else
            {
                pnlPackage.Visible = false;
                
                if (objRequest.BuildYourMeetingroomList.Count > 0)
                {
                    AllFoodAndBravrages = objPackagePricingManager.GetAllFoodBeveragesItems(intHotelID);
                    pnlFoodAndBravrages.Visible = true;
                    rptFoodandBravragesDay.DataSource = objRequest.DaysList;
                    rptFoodandBravragesDay.DataBind();
                }
                else
                {
                    pnlFoodAndBravrages.Visible = false;
                }
            }

            lblDuration.Text = Convert.ToString(objRequest.Duration == 1 ? objRequest.Duration + GetKeyResult("DAY") : objRequest.Duration + GetKeyResult("DAY"));
            if (objRequest.EquipmentList.Count > 0)
            {
                AllEquipments = objPackagePricingManager.GetAllEquipmentItems(intHotelID);
                pnlEquipment.Visible = true;
                rptEquipmentDay.DataSource = objRequest.DaysList;
                rptEquipmentDay.DataBind();
            }
            else
            {
                pnlEquipment.Visible = false;
            }
            if (objRequest.OthersList.Count > 0)
            {
                AllOthersItem = objPackagePricingManager.GetAllOtherItems(intHotelID);
                pnlOthers.Visible = true;
                rptOthersDay.DataSource = objRequest.DaysList;
                rptOthersDay.DataBind();
            }
            else
            {
                pnlOthers.Visible = false;
            }
            if (objRequest.IsAccomodation)
            {
                pnlAccomodation.Visible = true;
                rptDays2.DataSource = objRequest.RequestAccomodationList;
                rptDays2.DataBind();
                rptSingleQuantity.DataSource = objRequest.RequestAccomodationList;
                rptSingleQuantity.DataBind();
                rptDoubleQuantity.DataSource = objRequest.RequestAccomodationList;
                rptDoubleQuantity.DataBind();
                if (objRequest.RequestAccomodationList.Count < 6)
                {
                    ltrExtendDays.Text = "<td colspan='" + (6 - objRequest.RequestAccomodationList.Count) + "' style='border-top: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;padding: 5px;' >&nbsp;</td>";
                    ltrExtendSQuantity.Text = "<td colspan='" + (6 - objRequest.RequestAccomodationList.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
                    ltrExtendDQuantity.Text = "<td colspan='" + (6 - objRequest.RequestAccomodationList.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
                }
                //lblaccomodationQun.Text = Convert.ToString(objRequest.RequestAccomodationList[0].QuantityDouble);
            }
            else
            {
                pnlAccomodation.Visible = false;
            }
            lblSpecialRequest.Text = objRequest.SpecialRequest;    
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }


    protected void rptPackageItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label txtQuantity = (Label)e.Item.FindControl("txtQuantity");
            HiddenField hdnItemId = (HiddenField)e.Item.FindControl("hdnItemId");
            //AllPackageItem
            PackageItemMapping pim = e.Item.DataItem as PackageItemMapping;
            if (pim != null)
            {
                PackageItems p = AllPackageItem.Where(a => a.Id == pim.ItemId).FirstOrDefault();
                if (p != null)
                {
                    hdnItemId.Value = Convert.ToString(p.Id);
                    
                    PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblDescription.Text = pdesc.ItemDescription;
                        lblItemName.Text = pdesc.ItemName;
                    }
                    else
                    {
                        lblDescription.Text = "";
                        lblItemName.Text = p.ItemName;
                    }
                        PackageItemDetails pid = objRequest.PackageItemList.Where(a => a.ItemID == p.Id).FirstOrDefault();
                        if (pid != null)
                        {
                            txtQuantity.Text = Convert.ToString(pid.Quantity);
                        }
                        else
                        {
                            txtQuantity.Text = "0";
                        }
                }
            }
        }
    }
    #endregion
    #region New Accommodation
    protected void rptDays2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblDay = (Label)e.Item.FindControl("lblDay");
            RequestAccomodation a = e.Item.DataItem as RequestAccomodation;
            lblDay.Text = a.Checkin.ToString("dd/MM/yyyy");
        }
    }
    protected void rptSingleQuantity_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label txtQuantitySDay = (Label)e.Item.FindControl("txtQuantitySDay");
            HiddenField hdnDate = (HiddenField)e.Item.FindControl("hdnDate");
            RequestAccomodation a = e.Item.DataItem as RequestAccomodation;
            txtQuantitySDay.Text = a.QuantitySingle.ToString();
            hdnDate.Value = a.Checkin.ToString();
        }
    }
    protected void rptDoubleQuantity_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label txtQuantityDDay = (Label)e.Item.FindControl("txtQuantityDDay");
            HiddenField hdnDate = (HiddenField)e.Item.FindControl("hdnDate");
            RequestAccomodation a = e.Item.DataItem as RequestAccomodation;
            txtQuantityDDay.Text = a.QuantityDouble.ToString();
            hdnDate.Value = a.Checkin.ToString();
        }
    }
    #endregion
    protected void rptHotel_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblIndex = (Label)e.Item.FindControl("lblIndex");
            Label lblHotelName = (Label)e.Item.FindControl("lblHotelName");
            Repeater rptMeetingroom = (Repeater)e.Item.FindControl("rptMeetingroom");
            BuildHotelsRequest b = e.Item.DataItem as BuildHotelsRequest;
            if (b != null)
            {
                lblIndex.Text = (e.Item.ItemIndex + 1).ToString();
                Hotel objHtl = objHotel.GetHotelDetailsById(b.HotelID);
                lblHotelName.Text = objHtl.Name;
                rptMeetingroom.DataSource = b.MeetingroomList;
                rptMeetingroom.DataBind();
            }
        }
    }

    protected void rptMeetingroom_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblIndex = (Label)e.Item.FindControl("lblIndex");
            Label lblMeetingRoomName = (Label)e.Item.FindControl("lblMeetingRoomName");
            Label lblConfigurationType = (Label)e.Item.FindControl("lblConfigurationType");
            Label lblMaxandMinCapacity = (Label)e.Item.FindControl("lblMaxandMinCapacity");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            Label lblIsMain = (Label)e.Item.FindControl("lblIsMain");            
            BuildMeetingRoomRequest brm = e.Item.DataItem as BuildMeetingRoomRequest;
            if (brm != null)
            {
                lblIndex.Text = (e.Item.ItemIndex + 1).ToString();                
                MeetingRoom objMeetingroom = objHotel.GetMeetingRoomDetailsById(brm.MeetingRoomID);
                MeetingRoomDesc objDesc = objMeetingroom.MeetingRoomDescCollection.Where(a => a.LanguageId == Convert.ToInt32(Session["LanguageID"])).FirstOrDefault();
                MeetingRoomConfig objMrConfig = objMeetingroom.MeetingRoomConfigCollection.Where(a => a.Id == brm.ConfigurationID).FirstOrDefault();
                lblMeetingRoomName.Text = objMeetingroom.Name;
                lblConfigurationType.Text = (objMrConfig.RoomShapeId == (int)RoomShape.Boardroom ? RoomShape.Boardroom.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Classroom ? RoomShape.Classroom.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Cocktail ? RoomShape.Cocktail.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.School ? RoomShape.School.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Theatre ? RoomShape.Theatre.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.UShape ? RoomShape.UShape.ToString() : RoomShape.Boardroom.ToString());
                lblMaxandMinCapacity.Text = objMrConfig.MinCapacity + " - " + objMrConfig.MaxCapicity;
                lblQuantity.Text = brm.Quantity.ToString();
                lblIsMain.Text = brm.IsMain == true ? GetKeyResult("MAIN") : GetKeyResult("BREAKOUT");
            }
        }
    }

    protected void rptExtras_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            RequestExtra r = e.Item.DataItem as RequestExtra;
            if (r != null)
            {
                PackageItems objPackage = AllPackageItem.Where(a => a.IsExtra == true && a.Id == r.ItemID).FirstOrDefault();
                if (objPackage != null)
                {
                    
                    PackageDescription pdesc = objPackage.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblDescription.Text = pdesc.ItemDescription;
                        lblItemName.Text = pdesc.ItemName;
                    }
                    else
                    {
                        lblDescription.Text = "";
                        lblItemName.Text = objPackage.ItemName;
                    }
                    lblQuantity.Text = Convert.ToString(r.Quantity);
                }
            }
        }
    }

    protected void rptFoodandBravragesDay_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblSelectDay = (Label)e.Item.FindControl("lblSelectDay");
            Repeater rptFoodandBravragesItem = (Repeater)e.Item.FindControl("rptFoodandBravragesItem");
            NumberOfDays nod = e.Item.DataItem as NumberOfDays;
            if (nod != null)
            {
                lblSelectDay.Text = nod.SelectedDay.ToString();
                rptFoodandBravragesItem.DataSource = objRequest.BuildYourMeetingroomList.Where(a=>a.SelectDay == nod.SelectedDay);
                rptFoodandBravragesItem.DataBind();
            }
        }
    }

    protected void rptFoodandBravragesItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            BuildYourMeetingroomRequest bm = e.Item.DataItem as BuildYourMeetingroomRequest;
            if (bm != null)
            {
                PackageItems p = AllFoodAndBravrages.Where(a => a.Id == bm.ItemID).FirstOrDefault();
                if (p != null)
                {
                    
                    PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblDescription.Text = pdesc.ItemDescription;
                        lblItemName.Text = pdesc.ItemName;
                    }
                    else
                    {
                        lblDescription.Text = "";
                        lblItemName.Text = p.ItemName;
                    }
                    lblQuantity.Text = Convert.ToString(bm.Quantity);
                }
            }
        }
    }

    protected void rptEquipmentDay_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblSelectDay = (Label)e.Item.FindControl("lblSelectDay");
            Repeater rptEquipmentItem = (Repeater)e.Item.FindControl("rptEquipmentItem");
            NumberOfDays nod = e.Item.DataItem as NumberOfDays;
            if (nod != null)
            {
                lblSelectDay.Text = nod.SelectedDay == 1 ? GetKeyResult("DAY") + nod.SelectedDay : GetKeyResult("DAY") + nod.SelectedDay;
                rptEquipmentItem.DataSource = objRequest.EquipmentList.Where(a => a.SelectedDay == nod.SelectedDay);
                rptEquipmentItem.DataBind();
            }
        }
    }

    protected void rptEquipmentItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            RequestEquipment re = e.Item.DataItem as RequestEquipment;
            if (re != null)
            {
                PackageItems p = AllEquipments.Where(a => a.Id == re.ItemID).FirstOrDefault();
                if (p != null)
                {
                    
                    PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblDescription.Text = pdesc.ItemDescription;
                        lblItemName.Text = pdesc.ItemName;
                    }
                    else
                    {
                        lblDescription.Text = "";
                        lblItemName.Text = p.ItemName;
                    }
                    lblQuantity.Text = Convert.ToString(re.Quantity);
                }
            }
        }
    }

    protected void rptOthersDay_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblSelectDay = (Label)e.Item.FindControl("lblSelectDay");
            Repeater rptOthersItem = (Repeater)e.Item.FindControl("rptOthersItem");
            NumberOfDays nod = e.Item.DataItem as NumberOfDays;
            if (nod != null)
            {
                lblSelectDay.Text = nod.SelectedDay == 1 ? GetKeyResult("DAY") + nod.SelectedDay : GetKeyResult("DAY") + nod.SelectedDay;
                rptOthersItem.DataSource = objRequest.OthersList.Where(a => a.SelectedDay == nod.SelectedDay);
                rptOthersItem.DataBind();
            }
        }
    }

    protected void rptOthersItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            RequestOthers re = e.Item.DataItem as RequestOthers;
            if (re != null)
            {
                PackageItems p = AllOthersItem.Where(a => a.Id == re.ItemID).FirstOrDefault();
                if (p != null)
                {
                    
                    PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblDescription.Text = pdesc.ItemDescription;
                        lblItemName.Text = pdesc.ItemName;
                    }
                    else
                    {
                        lblDescription.Text = "";
                        lblItemName.Text = p.ItemName;
                    }
                    lblQuantity.Text = Convert.ToString(re.Quantity);
                }
            }
        }
    }

    protected void lnkPrevious_Click(object sender, EventArgs e)
    {
        SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["RequestID"]));
        objRequest = (CreateRequest)TrailManager.XmlDeserialize(typeof(CreateRequest), st.SearchObject);
        st.CurrentStep = 2;
        st.CurrentDay = 1;
        st.SearchObject = TrailManager.XmlSerialize(objRequest);
        if (bm.SaveSearch(st))
        {
            if (Request.QueryString["wl"] == null)
            {
                //Response.Redirect("RequestStep2.aspx",false);
                //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "request-step2/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "request-step2/english");
                }
            }
            else
            {
                Response.Redirect(SiteRootPath + "requeststep2.aspx?" + Request.RawUrl.Split('?')[1]);
            }
        }
    }

    protected void lnkConfirm_Click(object sender, EventArgs e)
    {
        SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["RequestID"]));
        objRequest = (CreateRequest)TrailManager.XmlDeserialize(typeof(CreateRequest), st.SearchObject);
        st.CurrentStep = 4;
        st.CurrentDay = 1;
        st.SearchObject = TrailManager.XmlSerialize(objRequest);
        if (bm.SaveSearch(st))
        {
            if (Request.QueryString["wl"] == null)
            {
                //Response.Redirect("RequestStep4.aspx", false);
                //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "request-step4/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "request-step4/english");
                }
            }
            else
            {
                Response.Redirect(SiteRootPath + "requeststep4.aspx?" + Request.RawUrl.Split('?')[1]);
            }
        }
    }
    #region Language
    //This is used for language conversion for static contants.
    //public string GetKeyResult(string key)
    //{
    //    //if (XMLLanguage == null)
    //    //{
    //    //    XMLLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
    //    //}
    //    //XmlNode nodes = XMLLanguage.SelectSingleNode("items/item[@key='" + Key + "']");
    //    //return nodes.InnerText;//
    //    return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    //}
    #endregion
}