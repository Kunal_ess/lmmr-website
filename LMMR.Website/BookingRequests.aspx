﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeFile="BookingRequests.aspx.cs" Inherits="BookingRequests" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="UserControl/Frontend/BottomLink.ascx" TagName="BottomLink" TagPrefix="uc1" %>
<%@ Register Src="UserControl/Frontend/LinkMedia.ascx" TagName="LinkMedia" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntLeftSearch" runat="Server">
    <!--left-inner-top START HERE-->
    <div class="left-inner-top-map">
        <div class="map">
            <a href="#">
                <img src="images/map.png" /></a>
        </div>
        <div class="logo">
            <a href="#">
                <img src="images/logo-front.png" /></a>
        </div>
        <!--left-form START HERE-->
        <div class="left-form">
            <div id="Country">
                <asp:DropDownList ID="drpCountry" runat="server" CssClass="bigselect" OnSelectedIndexChanged="drpCountry_SelectedIndexChanged"
                    AutoPostBack="True">
                </asp:DropDownList>
            </div>
            <div id="City">
                <asp:DropDownList ID="drpCity" CssClass="bigselect" runat="server" AutoPostBack="True"
                    OnSelectedIndexChanged="drpCity_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div id="location">
                <div id="locationtext">
                    <asp:HiddenField ID="hdnMapLatitude" runat="server" Value="0" />
                    <asp:HiddenField ID="hdnMapLogitude" runat="server" Value="0" />
                    <%= GetKeyResult("YOURLOCATIONCHANGE")%><br>
                    <%= GetKeyResult("LATITUDE")%> = <b><span id="selectLatitude"></span></b>
                    <br>
                    <%= GetKeyResult("LONGITUDE")%> = <b><span id="selectLogitude"></span></b>
                </div>
                <div class="locationsearch">
                    <img src="images/map-search.png" />
                </div>
            </div>
            <div id="radiusbody">
                <div class="radiustext">
                    <%= GetKeyResult("SEARCHINRADIUSOFKM")%>:</div>
                <div id="radius">
                    <asp:DropDownList ID="drpRadius" runat="server" OnSelectedIndexChanged="drpRadius_SelectedIndexChanged"
                        onchange="ToAddRadius();">
                        <asp:ListItem Selected="True" Text="Select" Value="0"></asp:ListItem>
                        <asp:ListItem Text="0.5 km" Value="0.310685"></asp:ListItem>
                        <asp:ListItem Text="1 km" Value="0.621371"></asp:ListItem>
                        <asp:ListItem Text="2 km" Value="1.24274"></asp:ListItem>
                        <asp:ListItem Text="5 km" Value="3.10685"></asp:ListItem>
                        <asp:ListItem Text="10 km" Value="6.21371"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div id="Date">
                <%-- <input name="AnotherDate" class="dateinput">
                <input type="button" value="select" class="datebutton" onclick="displayDatePicker('AnotherDate', this);">--%>
                <asp:TextBox ID="txtBookingDate" runat="server" value="dd/mm/yy" class="dateinput"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtBookingDate"
                    PopupButtonID="calFrom" Format="dd/MM/yy">
                </asp:CalendarExtender>
            </div>
            <div id="Quantity-day">
                <div id="Quantity">
                    <label>
                        <%= GetKeyResult("DURATION")%>&nbsp;</label>
                    <asp:DropDownList ID="DropDownList1" runat="server">
                        <asp:ListItem Text="1"></asp:ListItem>
                        <asp:ListItem Text="2"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div id="daysname">
                    <%= GetKeyResult("DAYSUSERCONTROL")%></div>
                <div id="Day">
                    <asp:DropDownList ID="drpDays" runat="server">
                        <asp:ListItem Text="Full Day" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Morning" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Afternoon" Value="2"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div id="Participants">
                <label>
                     <%= GetKeyResult("MEETINGDELEGATES")%> &nbsp;</label>
                <asp:TextBox ID="txtParticipant" runat="server" CssClass="inputbox" MaxLength="3"></asp:TextBox>
            </div>
            <asp:Button ID="btnSearch" runat="server" Text="Find your meeting room !" class="find-button"
                OnClick="btnSearch_Click" />
        </div>
        <!--left-form ENDS HERE-->
    </div>
    <!--left-inner-top ENDS HERE-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cntLeftBottom" runat="Server">
    <!--filter-form START HERE-->
    <div class="filter-form">
        <div class="filter-form-filter-results">
            <%= GetKeyResult("FILTER")%>
            <br>
            <%= GetKeyResult("RESULTS")%>
        </div>
        <div class="filter-form-inner">
            <div id="Area">
                <select name="test" class="bigselect">
                    <option>Area</option>
                    <option value="Area1">Area1</option>
                    <option value="Area2">Area2</option>
                    <option value="Area3">Area3</option>
                </select>
            </div>
            <div id="location1">
                <div id="location1text">
                    <%= GetKeyResult("YOURLOCATIONCHANGE")%> { <a href="#">change</a> }<br>
                    <%= GetKeyResult("LATITUDE")%> = <b>51.05175436468443</b><br>
                    <%= GetKeyResult("LONGITUDE")%> = <b>3.72711181640625</b>
                </div>
                <div class="locationsearch">
                    <img src="images/map-search.png" />
                </div>
            </div>
            <div id="radiusbody1">
                <div class="radiustext">
                    <%= GetKeyResult("SEARCHINRADIUSOF")%>:</div>
                <div id="radius1">
                    <select name="test" class="midselect1">
                        <option>0,5 km</option>
                        <option value="0,5 km">0,5 km</option>
                        <option value="6,10 km">6,10 km</option>
                        <option value="11,15 km">11,15 km</option>
                    </select>
                </div>
            </div>
            <div id="Stars-Modern">
                <div id="Stars">
                    <label>
                        <%= GetKeyResult("STARS")%> &nbsp;</label><input type="text" value="1" name="stars[]" id="stars74" class="inputbox">
                    <input type="button" onclick="var qty_el = document.getElementById('stars74'); var qty = qty_el.value; if( !isNaN( qty )) qty_el.value++;return false;"
                        class="button_up">
                    <input type="button" onclick="var qty_el = document.getElementById('stars74'); var qty = qty_el.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) qty_el.value--;return false;"
                        class="button_down">
                </div>
                <div id="theme">
                    <%= GetKeyResult("THEME")%></div>
                <div id="Modern">
                    <select name="test" class="midselect1">
                        <option>Modern</option>
                        <option value="Modern1">Modern1</option>
                        <option value="Modern2">Modern2</option>
                        <option value="Modern3">Modern3</option>
                    </select>
                </div>
            </div>
            <div class="daily-dudget">
                <div class="daily-dudgettext">
                    <%= GetKeyResult("DAILYBUDGET")%>
                    <br>
                    <span>(<%= GetKeyResult("PERPERSON")%>)<span>
                </div>
                <img src="images/results-para.png" align="absmiddle" />
            </div>
            <input type="button" value="Filter your results" class="filter-button" />
        </div>
    </div>
    <!--filter-form ENDS HERE-->
    </span></span>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cntTopNavigation" runat="Server">
    <!--nav-body START HERE-->
    <div class="nav">
        <!--Main navigation start -->
        <div id="subnavbar">
            <ul id="subnav">
                <li class="cat-item"><a href="Default.aspx">
                    <%= GetKeyResult("HOME")%></a></li>
                <li class="cat-item"><a href="AboutUs.aspx">
                    <%= GetKeyResult("ABOUTUS")%></a></li>
                <li class="cat-item"><a href="ContactUs.aspx">
                    <%= GetKeyResult("CONTACTUS")%></a></li>
                <li class="cat-item"><a href="JoinToday.aspx">
                    <%= GetKeyResult("JOINTODAY")%></a></li>
                <li class="cat-item"><a href="Login.aspx">
                    <%= GetKeyResult("LOGIN")%></a></li>
            </ul>
        </div>
        <!--main navigation end -->
    </div>
    <div class="nav-form">
        <div id="Euro">
            <select name="test" class="smallselect">
                <option>Euro</option>
                <option value="Euro1">Euro1</option>
                <option value="Euro2">Euro2</option>
                <option value="Euro3">Euro3</option>
            </select>
        </div>
        <div id="English">
            <asp:DropDownList ID="DDLlanguage" CssClass="midselect" runat="server" AutoPostBack="True"
                OnSelectedIndexChanged="DDLlanguage_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
    </div>
    <!--nav-body ENDS HERE-->
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cntMainBody" runat="Server">
    <!--mainbody START HERE-->
    <div class="mainbody">
        <!--mainbody-left START HERE-->
        <div class="mainbody-left">
            <!--banner START HERE-->
            <div class="banner">
                <div id="map_canvas" style="width: 460px; height: 300px; float: left;">
                </div>
            </div>
            <!--banner ENDS HERE-->
        </div>
        <!--mainbody-left ENDS HERE-->
        <!--mainbody-right START HERE-->
        <div class="mainbody-right">
            <!--mainbody-right-call START HERE-->
            <div class="mainbody-right-call">
                <div class="need">
                    <span><%= GetKeyResult("NEEDHELP")%></span>
                    <br />
                    <%= GetKeyResult("CALLUS")%>
                     5/7
                </div>
                <div class="phone">
                    +32 2 344 25 50 </div>
             <p style="font-size:10px;color:White;text-align:center"><%= GetKeyResult("CALLUSANDWEDOITFORYOU")%></p>
                <div class="mail">
                    <a href="http://needameetingroom.com/requestform/" onClick="javascript: _gaq.push(['_trackPageview', '/email']); window.open(this.href, 'popupwindow1','width=900,height=600,scrollbars,resizable,toolbar=NO'); &#13;&#10;return false;" Target="_blank"">
                        Send us your RFP</a>
                </div>
            </div>
            <!--mainbody-right-call ENDS HERE-->
            <!--mainbody-right-join START HERE-->
            <div class="mainbody-right-join">
                <div class="join">
                    <a href="#"><%= GetKeyResult("JOIN")%></a></div>
                <%= GetKeyResult("FORHOTELSMEETINGFACILITIESEVENT")%>
            </div>
            <!--mainbody-right-join ENDS HERE-->
            <!--mainbody-right-news START HERE-->
            <div class="mainbody-right-news">
                <div class="mainbody-right-news-top">
                </div>
                <div class="mainbody-right-news-mid">
                    <div class="mainbody-right-news-inner-result">
                        <h2 class="newsletter">
                            <%= GetKeyResult("NEWSLETTER")%></h2>
                        <div class="subsribe-body">
                            <input type="text" value="Enter your email" class="inputbox" onclick="this.value=''" />
                            <input type="button" value="Subsribe" class="subsribe" /></div>
                    </div>
                </div>
                <div class="mainbody-right-news-bottom">
                </div>
            </div>
            <!--mainbody-right-news ENDS HERE-->
        </div>
        <!--mainbody-right ENDS HERE-->
    </div>
    <!--mainbody ENDS HERE-->
    <!--resultbody START HERE-->
    <div class="resultbody">
        <!--found-results START HERE-->
        <div class="found-results">
            <div class="found">
                We found <span>226</span> results</div>
            <div class="sort">
                <%= GetKeyResult("SORTBY")%></div>
            <div class="price">
                <a href="#" class="pricelink"><%= GetKeyResult("PRICE")%></a></div>
            <div class="popularity">
                <a href="#"><%= GetKeyResult("POPULARITY")%></a></div>
            <div class="stars">
                <a href="#"><%= GetKeyResult("STARS")%></a></div>
            <div class="distance">
                <a href="#"><%= GetKeyResult("DISTANCETO")%></a> <span>Airport</span> <a href="#">
                    <img src="images/results-found-arrow.png" align="absmiddle" /></a></div>
            <div class="guest-re">
                <a href="#"><%= GetKeyResult("GUESTREVIEW")%></a></div>
        </div>
        <!--found-results ENDS HERE-->
        <!--pageing START HERE-->
        <div class="pageing">
            <a href="#" class="arrow-right select"></a>
            <div class="pageing-mid">
                <a href="#"><%= GetKeyResult("PREVIOUS")%></a> <a href="#" class="no">1</a><a href="#" class="no select">2</a><a
                    href="#" class="no">3</a> ... <a href="#" class="no">8</a> <a href="#"><%= GetKeyResult("NEXT")%></a></div>
            <a href="#" class="arrow-left"></a>
        </div>
        <!--pageing ENDS HERE-->
        <!--found-results-mainbody START HERE-->
        <div id="TabbedPanels2" class="TabbedPanels">
            <ul class="TabbedPanelsTabGroup">
                <li class="TabbedPanelsTab-green" tabindex="0"><%= GetKeyResult("BOOKONLINE")%></li>
                <li class="TabbedPanelsTab" tabindex="0"><%= GetKeyResult("SENDAREQUEST")%></li>
            </ul>
            <div class="TabbedPanelsContentGroup">
                <div class="TabbedPanelsContent">
                    <div class="found-results-mainbody">
                        <asp:GridView runat="server" ID="grvBooking" AutoGenerateColumns="false" Width="100%"
                            AllowPaging="True" EmptyDataRowStyle-HorizontalAlign="Center" BorderWidth="0"
                            GridLines="None" CellPadding="0" CellSpacing="0" ShowHeader="false">
                            <%--<PagerStyle BackColor="White" ForeColor="White" HorizontalAlign="Right" Font-Overline="False"
                        Font-Strikeout="False" BorderWidth="0" />--%>
                            <%-- <EmptyDataTemplate>
                        <div class="booking-details" style="width: 960px;" id="divLastDetails" runat="server">
                            <ul>
                                <li class="value10">
                                    <div class="col21" style="width: 960px;">
                                        <div class="button_section">
                                            Search result not found.
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </EmptyDataTemplate>--%>
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <ul>
                                            <li class="first">
                                                <!--all-results START HERE-->
                                                <div class="all-results">
                                                    <!--all-results-left START HERE-->
                                                    <div class="all-results-left">
                                                        <!--all-results-left-left START HERE-->
                                                        <div class="all-results-left-left">
                                                            <img src="images/r-img1.png" />
                                                        </div>
                                                        <!--all-results-left-left START HERE-->
                                                        <!--all-results-left-right END HERE-->
                                                        <div class="all-results-left-right">
                                                            <div class="star">
                                                                <img src="images/r-star.png" />
                                                            </div>
                                                            <h3>
                                                                <asp:Label ID="lblHotelName" runat="server" Text='<%#Eval("HotelName") %>'></asp:Label>
                                                            </h3>
                                                            <h4>
                                                                Rue de Laffayete 132, 1000 Bruxelles - Belgium</h4>
                                                            <p>
                                                                Aenean porta, tellus vel accumsan fermentum, diam arcu condimentum purus, hendrerit
                                                                posuere.
                                                            </p>
                                                            <div class="watch">
                                                                <img src="images/r-video-icon.png" align="absmiddle" />
                                                                <span>Watch Video</span>
                                                            </div>
                                                        </div>
                                                        <!--all-results-left-right END HERE-->
                                                    </div>
                                                    <!--all-results-left END HERE-->
                                                    <!--all-results-right START HERE-->
                                                    <div class="all-results-right">
                                                        <div class="results-top">
                                                            <div class="results-review">
                                                                <p>
                                                                    Review</p>
                                                                <span>7,8</span>
                                                            </div>
                                                            <div class="results-from-airport">
                                                                <p>
                                                                    From airport</p>
                                                                <span>0,9 km</span>
                                                            </div>
                                                            <div class="results-price-dis">
                                                                <div class="results-price-dis-left">
                                                                    <img src="images/euro-red.png" />
                                                                </div>
                                                                <div class="results-price-dis-right">
                                                                    <span class="euro">€</span> <span><span class="dis-bg">40</span></span> <b>pp/day</b>
                                                                    <p>
                                                                        From <span>€ 29</span> pp/day</p>
                                                                    <p>
                                                                        Discount <span class="red">-7%</span></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="results-bottom">
                                                            <a href="#" class="book-online-btn">Book online</a> <span>or</span> <a href="#" class="send-request-btn">
                                                                Send a request</a>
                                                        </div>
                                                    </div>
                                                    <!--all-results-right  END HERE-->
                                                </div>
                                                <!--all-results END HERE-->
                                                <!--all-results-bottom START HERE-->
                                                <div class="all-results-bottom">
                                                    <div class="all-results-bottom-left">
                                                        <div class="all-results-bottom-left-online">
                                                            <span>2</span> online to book
                                                        </div>
                                                        <div class="all-results-bottom-left-icon">
                                                            <a href="#">
                                                                <img src="images/watch-icon.png" class="select" /></a><a href="#"><img src="images/date-icon.png" /></a><a
                                                                    href="#"><img src="images/lock-icon.png" /></a><a href="#"><img src="images/print-icon.png" /></a><a
                                                                        href="#"><img src="images/chhoot-icon.png" /></a><a href="#"><img src="images/last-icon.png" /></a>
                                                        </div>
                                                        <div class="all-results-bottom-left-map">
                                                            <a href="#">
                                                                <img src="images/map-icon.png" align="absmiddle" /></a> Show map
                                                        </div>
                                                    </div>
                                                    <div class="all-results-bottom-right">
                                                        <a href="#">See all</a></div>
                                                </div>
                                                <!--all-results-bottom END HERE-->
                                            </li>
                                        </ul>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle HorizontalAlign="Center"></EmptyDataRowStyle>
                            <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                            <PagerStyle HorizontalAlign="Right" BackColor="White" />
                            <%--<PagerTemplate>
                        <div id="Paging" style="width: 100%; display: none;">
                            <table cellpadding="0" align="right">
                                <tr>
                                    <td style="vertical-align=middle; height: 22px;">
                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </PagerTemplate>--%>
                        </asp:GridView>
                    </div>
                </div>
            </div>
            <div class="TabbedPanelsContentGroup">
                <div class="TabbedPanelsContent">
                    <div class="TabbedPanelsContent">
                        <div class="found-results-mainbody">
                            <asp:GridView runat="server" ID="grvRequest" AutoGenerateColumns="false" Width="100%"
                                AllowPaging="True" EmptyDataRowStyle-HorizontalAlign="Center" BorderWidth="0"
                                GridLines="None" CellPadding="0" CellSpacing="0" ShowHeader="false">
                                <%--<PagerStyle BackColor="White" ForeColor="White" HorizontalAlign="Right" Font-Overline="False"
                        Font-Strikeout="False" BorderWidth="0" />--%>
                                <%-- <EmptyDataTemplate>
                        <div class="booking-details" style="width: 960px;" id="divLastDetails" runat="server">
                            <ul>
                                <li class="value10">
                                    <div class="col21" style="width: 960px;">
                                        <div class="button_section">
                                            Search result not found.
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </EmptyDataTemplate>--%>
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <ul>
                                                <li class="first">
                                                    <!--all-results START HERE-->
                                                    <div class="all-results">
                                                        <!--all-results-left START HERE-->
                                                        <div class="all-results-left">
                                                            <!--all-results-left-left START HERE-->
                                                            <div class="all-results-left-left">
                                                                <img src="images/r-img1.png" />
                                                            </div>
                                                            <!--all-results-left-left START HERE-->
                                                            <!--all-results-left-right END HERE-->
                                                            <div class="all-results-left-right">
                                                                <div class="star">
                                                                    <img src="images/r-star.png" />
                                                                </div>
                                                                <h3>
                                                                    <asp:Label ID="lblHotelName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                                                </h3>
                                                                <h4>
                                                                    Rue de Laffayete 132, 1000 Bruxelles - Belgium</h4>
                                                                <p>
                                                                    Aenean porta, tellus vel accumsan fermentum, diam arcu condimentum purus, hendrerit
                                                                    posuere.
                                                                </p>
                                                                <div class="watch">
                                                                    <img src="images/r-video-icon.png" align="absmiddle" />
                                                                    <span>Watch Video</span>
                                                                </div>
                                                            </div>
                                                            <!--all-results-left-right END HERE-->
                                                        </div>
                                                        <!--all-results-left END HERE-->
                                                        <!--all-results-right START HERE-->
                                                        <div class="all-results-right">
                                                            <div class="results-top">
                                                                <div class="results-review">
                                                                    <p>
                                                                        Review</p>
                                                                    <span>700,800</span>
                                                                </div>
                                                                <div class="results-from-airport">
                                                                    <p>
                                                                        From airport</p>
                                                                    <span>0,9000 km</span>
                                                                </div>
                                                                <div class="results-price-dis">
                                                                    <div class="results-price-dis-left">
                                                                        <img src="images/euro-red.png" />
                                                                    </div>
                                                                    <div class="results-price-dis-right">
                                                                        <span class="euro">€</span> <span><span class="dis-bg">400</span></span> <b>pp/day</b>
                                                                        <p>
                                                                            From <span>€ 2900</span> pp/day</p>
                                                                        <p>
                                                                            Discount <span class="red">-100%</span></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="results-bottom" style="text-align: right;">
                                                                <a href="#" class="send-request-btn" style="margin-right: 13px;">Send a request</a>
                                                            </div>
                                                        </div>
                                                        <!--all-results-right  END HERE-->
                                                    </div>
                                                    <!--all-results END HERE-->
                                                    <!--all-results-bottom START HERE-->
                                                    <div class="all-results-bottom">
                                                        <div class="all-results-bottom-left">
                                                            <div class="all-results-bottom-left-online">
                                                                <span>2</span> online to book
                                                            </div>
                                                            <div class="all-results-bottom-left-icon">
                                                                <a href="#">
                                                                    <img src="images/watch-icon.png" class="select" /></a><a href="#"><img src="images/date-icon.png" /></a><a
                                                                        href="#"><img src="images/lock-icon.png" /></a><a href="#"><img src="images/print-icon.png" /></a><a
                                                                            href="#"><img src="images/chhoot-icon.png" /></a><a href="#"><img src="images/last-icon.png" /></a>
                                                            </div>
                                                            <div class="all-results-bottom-left-map">
                                                                <a href="#">
                                                                    <img src="images/map-icon.png" align="absmiddle" /></a> Show map
                                                            </div>
                                                        </div>
                                                        <div class="all-results-bottom-right">
                                                            <a href="#">See all</a></div>
                                                    </div>
                                                    <!--all-results-bottom END HERE-->
                                                </li>
                                            </ul>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Center"></EmptyDataRowStyle>
                                <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                                <PagerStyle HorizontalAlign="Right" BackColor="White" />
                                <%--<PagerTemplate>
                        <div id="Paging" style="width: 100%; display: none;">
                            <table cellpadding="0" align="right">
                                <tr>
                                    <td style="vertical-align=middle; height: 22px;">
                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </PagerTemplate>--%>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels2");
        </script>
        <!--found-results-mainbody ENDS HERE-->
        <!--pageing START HERE-->
        <div class="pageing">
            <a href="#" class="arrow-right select"></a>
            <div class="pageing-mid">
                <a href="#"><%= GetKeyResult("PREVIOUS")%></a> <a href="#" class="no">1</a><a href="#" class="no select">2</a><a
                    href="#" class="no">3</a> ... <a href="#" class="no">8</a> <a href="#"><%= GetKeyResult("NEXT")%></a></div>
            <a href="#" class="arrow-left"></a>
        </div>
        <!--pageing ENDS HERE-->
    </div>
    <!--resultbody ENDS HERE-->
    <script type="text/javascript">
        var geocoder;
        var map;
        var markersArray = [];
        var marker;
        var address;
        var addressByUser;
        var image;
        var circleArray;
        var infowindow = new google.maps.InfoWindow();
        var locations = <%= HotelData %>;
        
        function OnDemand(CenterPoint, PlaceName,zoomLevel) {
            var cent = CenterPoint.split(',');
            map = new google.maps.Map(document.getElementById('map_canvas'), {
            zoom: parseInt(zoomLevel),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
            
            map.setCenter(new google.maps.LatLng(cent[0], cent[1]));
            
            image = new google.maps.MarkerImage('http://maps.google.com/mapfiles/ms/icons/blue-dot.png');
            marker = new google.maps.Marker({
                map: map,
                title: PlaceName,
                position: new google.maps.LatLng(cent[0], cent[1]),
                icon: image
            });
            infowindow.setContent(PlaceName);
            infowindow.open(map, marker);
            
            var radiusSelected= jQuery('#<%=drpRadius.ClientID %>').val();
            
              if(radiusSelected!=0 && radiusSelected!= null)
               {
                     circleArray = new google.maps.Circle({
                     position: new google.maps.LatLng(cent[0], cent[1]),
                     radius: parseInt(radiusSelected), 
                     fillColor: '#AA0000'
                     });
                 circleArray.bindTo('center', marker, 'position');
               }

            google.maps.event.addListener(marker, 'mouseover', function () {
                infowindow.setContent(PlaceName);
                infowindow.open(map, this);
            });
            jQuery('#selectLatitude').html(cent[0]);
            jQuery('#selectLogitude').html(cent[1]);
            ShowHotelOfCity();
            
        }


       function ToAddRadius()
       {
     
        var radiusSelected = jQuery('#<%=drpRadius.ClientID %>').val();
        var varLatitude = jQuery('#<%=hdnMapLatitude.ClientID %>').val();
        var varLogitude = jQuery('#<%=hdnMapLogitude.ClientID %>').val();
        
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(varLatitude,varLogitude)
       });
         clearCircle();
              if(radiusSelected!=0 && radiusSelected!= null)
               {
                     map.setZoom(20);
                     map.setCenter(new google.maps.LatLng(varLatitude,varLogitude));
                     circleArray = new google.maps.Circle({
                     map:map,
                     radius: parseInt(radiusSelected), 
                     fillColor: '#AA0000'
                     });
                    circleArray.bindTo('center', marker, 'position');
                 //circleArray.bindTo('center', marker, 'position');
               }
       }


        //This function used for show all hotel of the city.
        function ShowHotelOfCity() {
            clearOverlays();
            markersArray.push(marker);
            for (i = 0; i < locations.length; i++) {
                if (locations[i][4] == 'Special') {
                    image = new google.maps.MarkerImage('http://maps.google.com/mapfiles/ms/icons/green-dot.png');
                }
                else {
                    image = new google.maps.MarkerImage('http://maps.google.com/mapfiles/ms/icons/red-dot.png');
                }
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: image
                });
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {
                        //infowindow.setContent(locations[i][0]);
                       var contentString = '<div style="width:250px; padding:10px; background:#000; morgin-top:5px;color:#fff">'+locations[i][1]+','+ locations[i][2]+'</div>';
                       infowindow.setContent(contentString);
                       infowindow.open(map, marker);

//            infoBubble = new InfoBubble({
//            map: map,
//            content: '<div class="phoneytext">Some label</div>',
//            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
//            shadowStyle: 1,
//            padding: 0,
//            backgroundColor: 'rgb(57,57,57)',
//            borderRadius: 4,
//            arrowSize: 10,
//            borderWidth: 1,
//            borderColor: '#2c2c2c',
//            disableAutoPan: true,
//            hideCloseButton: true,
//            arrowPosition: 30,
//            backgroundClassName: 'phoney',
//            arrowStyle: 2
//        }); 
//        infoBubble.open(map, marker); 
         }
                })(marker, i));
            }
            google.maps.event.addListener(map, 'click', function (event) {
                AddPoint(event.latLng);
            });
        }

        function DefaultMap(addressbyuser) {
        map = new google.maps.Map(document.getElementById('map_canvas'), {
            zoom: 3,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        address=addressbyuser;
        geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    clearOverlays();
                    map.setCenter(results[0].geometry.location);
                    marker = new google.maps.Marker({
                        map: map,
                        title: results[0]['formatted_address'],
                        position: results[0].geometry.location
                    });
                     
                     var latlog = results[0].geometry.location;
                     if(latlog!=null)
                     {
                     var splitLogLat = latlog.toString().split(',');
                     jQuery('#selectLatitude').html(splitLogLat[0].replace("(",""));
                     jQuery('#selectLogitude').html(splitLogLat[1].replace(")",""));
                     }
                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);
                    markersArray.push(marker);
                    var radiusSelected= jQuery('#<%=drpRadius.ClientID %>').val();
                       if(radiusSelected!=0 && radiusSelected!= null)
                       {
                            circleArray = new google.maps.Circle({
                            map: map,
                            radius: parseInt(radiusSelected),    // 1 miles in metres
                            fillColor: '#AA0000'
                        });
                        circleArray.bindTo('center', marker, 'position');
                    }

                } else {
                    alert('<%= GetKeyResult("GEOCODENTSUCCESS")%>: ' + status);
                }
            });
        }


        //Add referace point on click on map.
        function AddPoint(location) {
            geocoder = new google.maps.Geocoder();
            image = new google.maps.MarkerImage('http://maps.google.com/mapfiles/ms/icons/blue-dot.png');
            geocoder.geocode({ 'latLng': location }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        clearOverlays();
                        clearCircle();
                     var latlog = location;
                     if(latlog!=null)
                     {
                     var splitLogLat = latlog.toString().split(',');
                     jQuery('#selectLatitude').html(splitLogLat[0].replace("(",""));
                     jQuery('#selectLogitude').html(splitLogLat[1].replace(")",""));
                     jQuery('#<%=hdnMapLatitude.ClientID %>').val(splitLogLat[0].replace("(",""));
                     jQuery('#<%=hdnMapLogitude.ClientID %>').val(splitLogLat[1].replace(")",""));
                     }
                        marker = new google.maps.Marker({
                            position: location,
                            title: results[1].formatted_address,
                            map: map,
                            icon: image
                        });
                        infowindow.setContent(results[1].formatted_address);
                        infowindow.open(map, marker);
                        markersArray.push(marker);
                        map.setCenter(location);
                        //click
                        google.maps.event.addListener(marker, 'mouseover', function () {
                            infowindow.setContent(results[1].formatted_address);
                            infowindow.open(map, this);
                        });

                   var radiusSelected= jQuery('#<%=drpRadius.ClientID %>').val();
                      if(radiusSelected!=0 && radiusSelected!= null)
                       {
                            circleArray = new google.maps.Circle({
                            map: map,
                            radius:parseInt(radiusSelected),    // 1 miles in metres
                            fillColor: '#AA0000'
                        });
                        circleArray.bindTo('center', marker, 'position');
                    }

                    }
                } else {
                    
                    alert('<%= GetKeyResult("GEOCODEFAIL")%>: '+ status);
                }
            });

        }

        function clearOverlays() {
            if (markersArray) {
                for (i = 0; i < markersArray.length; i++) {
                    markersArray[i].setMap(null);
                }
                markersArray.length = 0;
            }

        }
        function clearCircle() {
            if (circleArray) {
                for (i in circleArray) {
                    circleArray.setMap(null);
                }
            }
        }
        function toggleBounce() {

            if (marker.getAnimation() != null) {
                marker.setAnimation(null);
            } else {
                marker.setAnimation(google.maps.Animation.BOUNCE);
            }
        }
        
    </script>
        
    
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cntMediaLink" runat="Server">
    <uc2:LinkMedia ID="LinkMedia1" runat="server" />
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cntFotter" runat="Server">
    <uc1:BottomLink ID="BottomLink1" runat="server" />
</asp:Content>
