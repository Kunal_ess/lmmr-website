﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using System.Configuration;
using log4net;
using log4net.Config;
#endregion

public partial class Operator_NewAdminLink : System.Web.UI.Page
{
    #region Variable
    ILog logger = log4net.LogManager.GetLogger(typeof(Operator_NewAdminLink));
    ClientContract objClient = new ClientContract();
    HotelInfo ObjHotelinfo = new HotelInfo();
    HotelManager objHotelManager = new HotelManager();
    EmailConfigManager em = new EmailConfigManager();
    #endregion

    #region PageLoad
    /// <summary>
    /// On page load we manage all the containts and Apply paging if there is a post back.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Check session exist or not
            if (Session["CurrentOperatorID"] == null)
            {
                Response.Redirect("~/login.aspx", true);
            }

            //Check condition for Page postback.
            if (!IsPostBack)
            {
                FillClient();
                divmessage.Style.Add("display", "none");
                divmessage.Attributes.Add("class", "error");
                BindGroupClients();
            }            
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    #region Bind groupclient Admin link
    public void BindGroupClients()
    {
        TList<Users> user = objClient.GetAllGroupClients();
        grvAdminLink.DataSource = user;
        grvAdminLink.DataBind();        
    }
    #endregion

    #region Bind group Client dropdown
    protected void FillClient()
    {
        try
        {
            //Bind Client from the record
            //drpClientName.Items.Clear();
            //drpClientName.DataValueField = "UserId";
            //drpClientName.DataTextField = "FirstName";
            //drpClientName.DataSource = objClient.GetAllClients().FindAllDistinct(UsersColumn.UserId).OrderBy(a => a.FirstName);
            //drpClientName.DataBind();
            //drpClientName.Items.Insert(0, new ListItem("Select Client", ""));            
 
            chkHotelName.Items.Clear();
            chkHotelName.DataValueField = "Id";
            chkHotelName.DataTextField = "Name";
            chkHotelName.DataSource = objHotelManager.GetAllHotel().OrderBy(a=>a.Name);
            chkHotelName.DataBind();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    //protected void drpClientName_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        //Bind hotelname as per client id
    //        chkHotelName.Items.Clear();
    //        chkHotelName.DataValueField = "Id";
    //        chkHotelName.DataTextField = "Name";
    //        chkHotelName.DataSource = objClient.GetHotelByClientId(Convert.ToInt32(drpClientName.SelectedValue));
    //        chkHotelName.DataBind();
    //        if (objClient.GetHotelByClientId(Convert.ToInt32(drpClientName.SelectedValue)).Count == 0)
    //        {
    //            lblMessage.Text = "There is no hotel available for this client";
    //        }
    //        else
    //        {
    //            lblMessage.Text = "";
    //            chkHotelName.Visible = true;
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        logger.Error(ex);
    //    }
    //}
    #endregion

    #region For create new admin link
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (objClient.CheckEmailExistAdminLink(txtEmailAccount.Text.Trim().Replace("'", "")))
        {
            divmessage.InnerHtml = "Email Id already exist in records.";
            divmessage.Attributes.Add("class", "error");
            divmessage.Style.Add("display", "block");
            return;
        }
        else
        {
            Users newUser = new Users();
            UserDetails details = new UserDetails();
            HotelOwnerLink hotelowner = new HotelOwnerLink();
            NewUser newUserObject = new NewUser();
            newUser.FirstName = txtContactPerson.Text;
            newUser.EmailId = txtEmailAccount.Text;
            newUser.Usertype = 6;            
            details.Phone = txtPhoneNumber.Text;

            newUserObject.RegisterAdminOwner(newUser, details);


            for (int i = 0; i < chkHotelName.Items.Count; i++)
            {
                if (chkHotelName.Items[i].Selected)
                {
                    newUserObject.InsertHotelOwner(Convert.ToInt32(newUser.UserId), Convert.ToInt32(chkHotelName.Items[i].Value));
                }
            }

            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "succesfuly");
            divmessage.InnerHtml = "New admin has been created successfully";
            divMessageOnGridHeader.Style.Add("display", "none");
            divMessageOnGridHeader.Attributes.Add("class", "succesfuly");
            divMessageOnGridHeader.Attributes.Add("class", "error");
            BindGroupClients();
            txtPhoneNumber.Text = "";
            txtEmailAccount.Text = "";
            txtContactPerson.Text = "";
            chkHotelName.SelectedIndex = -1;
            //drpClientName.SelectedIndex = -1;
            //chkHotelName.Visible = false;
        }
    }
    #endregion

    #region Clear all fileds
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtPhoneNumber.Text = "";
        txtEmailAccount.Text = "";
        txtContactPerson.Text="";
        chkHotelName.SelectedIndex = -1;
        //drpClientName.SelectedIndex = -1;
       // lblMessage.Text = "";
        divmessage.Style.Add("display", "none");
        divmessage.Attributes.Add("class", "error");
        divMessageOnGridHeader.Style.Add("display", "none");
        divMessageOnGridHeader.Attributes.Add("class", "error");      
    }
    #endregion

    #region Bind all client and hotel name for the specific admin link
    protected void lnbHotelName_Click(object sender, EventArgs e)
    {
        try
        {

            LinkButton lnbHotelName = (LinkButton)sender;
            ViewState["ChkcommBookingID"] = lnbHotelName.CommandArgument;
            ModalPopupCheck.Show();
            TList<HotelOwnerLink> ownerlink = objClient.GetHotelOwnerLink(Convert.ToInt32(ViewState["ChkcommBookingID"]));
            grvHotelowner.DataSource = ownerlink;
            grvHotelowner.DataBind();
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    #region Hide popup control for cancel button
    protected void lnkCheckcancel_Click(object sender, EventArgs e)
    {
        try
        {
            ModalPopupCheck.Hide();            
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    #region Show active/deactive button for admin users
    protected void grvAdminLink_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
        {
            Users data = e.Row.DataItem as Users;
            Label lblPhoneNumber = (Label)e.Row.FindControl("lblPhoneNumber");
            TList<UserDetails> objDtls = ObjHotelinfo.GetUserDetails(Convert.ToInt32(data.UserId));
            lblPhoneNumber.Text = objDtls[0].Phone;

            CheckBox uiCheckBoxIsActive = (CheckBox)e.Row.FindControl("uiCheckBoxIsActive");
            uiCheckBoxIsActive.Attributes.Add("AccessKey", data.UserId.ToString() + "+" + Convert.ToString(data.Password) + "+" + Convert.ToString(data.EmailId) + "");

            if (data.IsActive == true)
            {
                uiCheckBoxIsActive.Checked = true;
            }
            else
            {
                uiCheckBoxIsActive.Checked = false;
            }
        }       
    }
    #endregion

    #region Sending mail function for login id and password
    protected void uiCheckBoxIsActive_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox ch = sender as CheckBox;
            var data = ch.Attributes["AccessKey"];
            var arr = data.Split('+');
            if (ch.Checked == true)
            {
                if (Convert.ToInt32(arr[0]) != 0 && arr[0] != null)
                {
                    string genereatedPassword;
                    genereatedPassword = RandomPassword.Generate(8, 10);
                    if (objHotelManager.ChangeHotelAdminActiveStatus(Convert.ToInt32(arr[0]), true))
                    {
                        if (updateUserPassword(Convert.ToInt32(arr[0]), genereatedPassword, arr[2]))
                        {
                            EmailConfig eConfig = em.GetByName("Activation of admin from operator");
                            if (eConfig.IsActive)
                            {
                                string bodymsg = File.ReadAllText(Server.MapPath("~/EmailTemplets/EmailTempletSample.html"));
                                EmailConfigMapping emap = new EmailConfigMapping();
                                emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                                bodymsg = bodymsg.Replace("@CONTENTS", emap.EmailContents);
                                SendMails objSendmail = new SendMails();
                                objSendmail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
                                objSendmail.ToEmail = arr[2];                                
                                //string bodymsg = File.ReadAllText(Server.MapPath("~/EmailTemplets/LoginDetailsMailTemplet.html"));
                                EmailValueCollection objEmailValues = new EmailValueCollection();

                                objSendmail.Subject = "Login Details for Last Minute Meeting Room admin link";                                
                                foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForLoginDetailsOfClientContract(arr[2], genereatedPassword, "Last Minute Meeting Room Admin"))
                                {
                                    bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                                }
                                objSendmail.Body = bodymsg;
                                Users objUser = (Users)Session["CurrentOperator"];
                                objSendmail.cc = objUser.EmailId;
                                objSendmail.Bcc = ConfigurationManager.AppSettings["WatchEMailID"];
                                if (objSendmail.SendMail() == "Send Mail")
                                {
                                    divMessageOnGridHeader.InnerHtml = "Admin activated successfully. Activation Mail sent to the specified email id";
                                    divMessageOnGridHeader.Attributes.Add("class", "succesfuly");
                                    divMessageOnGridHeader.Style.Add("display", "block");
                                    divmessage.Attributes.Add("class", "error");
                                    divmessage.Attributes.Add("class", "succesfuly");
                                    divmessage.Style.Add("display", "none");                                    
                                }
                                else
                                {
                                    divmessage.Style.Add("display", "none");
                                    divmessage.Attributes.Add("class", "error");
                                    divMessageOnGridHeader.Style.Add("display", "none");
                                    divMessageOnGridHeader.Attributes.Add("class", "error");                                    
                                }
                            }
                            else
                            {
                                divMessageOnGridHeader.InnerHtml = "The mail functionality is not working yet.";
                                divMessageOnGridHeader.Attributes.Add("class", "error");
                                divMessageOnGridHeader.Style.Add("display", "block");                                
                            }
                        }
                        else
                        {
                            divMessageOnGridHeader.InnerHtml = "Admin link activated successfully.";
                            divMessageOnGridHeader.Attributes.Add("class", "succesfuly");
                            divMessageOnGridHeader.Style.Add("display", "block");
                            divmessage.Attributes.Add("class", "error");
                            divmessage.Style.Add("display", "none");
                        }
                    }
                    else
                    {
                        divMessageOnGridHeader.InnerHtml = "Error occured while change activation state.";
                        divMessageOnGridHeader.Attributes.Add("class", "error");
                        divMessageOnGridHeader.Style.Add("display", "block");
                        divmessage.Attributes.Add("class", "error");
                        divmessage.Style.Add("display", "none");
                    }
                }
            }

            if (ch.Checked == false)
            {
                if (Convert.ToInt32(arr[0]) != 0 && arr[0] != null)
                {

                    if (objHotelManager.ChangeHotelAdminActiveStatus(Convert.ToInt32(arr[0]), false))
                    {
                        divMessageOnGridHeader.InnerHtml = "Admin link deactivated successfully.";
                        divMessageOnGridHeader.Attributes.Add("class", "succesfuly");
                        divMessageOnGridHeader.Style.Add("display", "block");
                        divmessage.Style.Add("display", "none");
                        divmessage.Attributes.Add("class", "error");
                    }
                    else
                    {
                        divMessageOnGridHeader.InnerHtml = "Error occured while change activation state.";
                        divMessageOnGridHeader.Attributes.Add("class", "error");
                        divMessageOnGridHeader.Style.Add("display", "block");
                        divmessage.Style.Add("display", "none");
                        divmessage.Attributes.Add("class", "error");
                    }                   
                }
                else
                {
                    divmessage.Style.Add("display", "none");
                    divmessage.Attributes.Add("class", "error");
                    divMessageOnGridHeader.Style.Add("display", "none");
                    divMessageOnGridHeader.Attributes.Add("class", "error");                    
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }
    #endregion

    #region Change the user password according to Userid and email
    public bool updateUserPassword(Int32 UserId, string password, string email)
    {

        return PasswordManager.UpdateClientPasswordByEmail(email, password);

    }
    #endregion    

    #region Modify record of priority basket
    protected void lnbModify_Click(object sender, EventArgs e)
    {
        //DataKey currentDataKey = this.grvPriorityBasket.DataKeys[grv.DataItemIndex];        
    }
    #endregion
}