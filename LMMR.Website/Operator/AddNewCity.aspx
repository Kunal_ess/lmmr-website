﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Operator/Operator.master" AutoEventWireup="true" CodeFile="AddNewCity.aspx.cs" Inherits="Operator_AddNewCity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <div class="main_container1">
        <!-- start left section-->
        <div class="left_container2">
            <div class="search">
                <%--<div class="hotel_logo"><img src="../images/hotel_logo.png" alt="Hotel Logo"></div>	--%>
                <!-- start drop down search -->
                <div class="rowElem">
                    <asp:DropDownList ID="drpCountry" runat="server"
                        CssClass="NoClassApply" Width="160">
                    </asp:DropDownList>
                </div>
                <!-- end drop down search -->
            </div>
        </div>
        <!-- end left section-->
        <!-- start right section-->
        <div class="right_container1">
            <div class="superadmin-mainbody">
                <!-- end logout-today-->
                <div class="superadmin-mainbody-inner">
                    <div class="commisions-top-main clearfix">
                        <div class="commisions-top ">
                            <div id="divmessage" runat="server">
                            </div>
                            <table width="100%" border="0" cellspacing="5" cellpadding="0">
                               
                                <tr>
                                    <td>
                                        Enter City Name
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCityName" runat="server" MaxLength="20"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <br />
                            <br />
                            <asp:LinkButton ID="lbtSave" runat="server" CssClass="select" OnClick="lbtSave_Click"
                                OnClientClick="return validate();">Save</asp:LinkButton>
                            <asp:LinkButton ID="lbtReset" runat="server" CssClass="select" OnClick="lbtReset_Click">Reset</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end right section-->
    </div>
<script language="javascript" type="text/javascript">
    function validate() {
        var CountryDropdownList = document.getElementById('<%=drpCountry.ClientID %>');
        var SelectedText = CountryDropdownList.options[CountryDropdownList.selectedIndex].text;
        if (SelectedText == "Select Country") {
            alert("Please select country.");
            return false;
        }
        
        var cityName = jQuery("#<%= txtCityName.ClientID %>").val();
        if (cityName.length <= 0) {
            alert("Please enter city.");
            return false;
        }
    }

</script>
</asp:Content>

