﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Operator/Operator.master" AutoEventWireup="true" CodeFile="HotelFacilityAccess.aspx.cs" Inherits="Operator_HotelFacilityAccess" %>
<%@ Register Src="~/HotelUser/usercontrols/LeftHotelDetails.ascx" TagName="LeftHotelDetails"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Literal ID="scriptRunTime" runat="server"></asp:Literal> 
   <%--<div class="contract-list" id="divAlphabeticPaging" runat="server" style="margin-top: 20px;">
                <div class="contract-list-left" style="width: 278px;">
                    <h2>
                        Hotel/Facility Access</h2>
                </div>                
            </div>--%>
    <div class="search-operator-layout1" style="margin-bottom: 20px;">
    <table width="100%" border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td width="630px">
           <%--  <asp:UpdatePanel ID="upsearch" runat="server">
               <ContentTemplate>--%>
                <div class="search-operator-layout-left1">
                    <div class="search-operator-from1">
                        <div class="commisions-top-new1 ">
                            <table width="100%" border="0" cellspacing="0" cellpadding="8">                            
                                <tr id="trSearch" runat="server">
                                    <td style="border-bottom: #92bede solid 1px" runat="server" id="tdcountry">
                                   
                                     <asp:DropDownList ID="drpCountry" runat="server" AutoPostBack="True" onselectedindexchanged="drpCountry_SelectedIndexChanged"
                                            Style="width: 215px">
                                        </asp:DropDownList>
                                    
                                       
                                    </td>                                    
                                </tr>
                                <tr id="trVenue" runat="server">
                                    <td style="border-bottom: #92bede solid 1px" runat="server" id="tdCity">
                                        <asp:DropDownList ID="drpCity" runat="server" AutoPostBack="true" onselectedindexchanged="drpCity_SelectedIndexChanged"  CssClass="NoClassApply"
                                            Enabled="false" Style="width: 215px">
                                            <asp:ListItem Text="--Select city--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>                                                               
                                </tr>                               
                                <tr id="trHotelname" runat="server">
                                    <td style="border-bottom: #92bede solid 1px" >
                                        <asp:DropDownList ID="drpHotelList" runat="server" AutoPostBack="false" CssClass="NoClassApply"
                                            Style="width: 215px">                                            
                                        </asp:DropDownList>
                                    </td>                                                             
                                </tr>                                                             
                            </table>                            
                        </div>
                    </div>
                </div>
               <%-- </ContentTemplate>
               </asp:UpdatePanel>--%>
            </td>
            <td align="left" valign="bottom">
                <div style="float: left; margin-left: 20px;" class="n-commisions">
                    <div class="n-btn">
                        <asp:LinkButton ID="lnbSearch" runat="server" OnClick="lnbSearch_Click"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Go to hotel screen</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                    </div>
                </div>  
                <div style="float: left; margin-left: 10px;" class="n-commisions">
                    <div class="n-btn">
                        <asp:LinkButton ID="lnkClear" runat="server" OnClick="lnkClear_Click"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Clear</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                    </div>
                </div>              
            </td>
        </tr>
        <tr>
        <td width="630px" align="left">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           
            <img src="../Images/help.jpg" />&nbsp;&nbsp;<strong>Note : Kindly select the hotel to go 'Hotel screen'</strong>
        </td>
        </tr>
    </table>
</div>
   <div class="main_container1">   
   <!-- start left section-->		  
		  <!-- end left section-->
		  <div class="search-booking">          		  		  
		  <div class="search-booking-rowmain clearfix" >
		  
          <asp:Panel ID="pnlFrame" runat="server" Width="100%" Visible="false">
               <div style="width: 1000px; position: absolute; left: auto; top: 112px; z-index: 100;
                        overflow: hidden; border: 15px solid #333; background: #999;">
                     <%--<a href="#" onclick="window.close();" style=" display:block; clear:both; float:right; color:#000;font-size: 16px; font-weight: bold; margin-right: 4px; padding: 2px;text-decoration: none;">X</a>--%>
                       &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="Label2" runat="server" Text="Hotel Name : "
                            ForeColor="White" Font-Bold="true"></asp:Label><asp:Label ID="lblFacilityName" runat="server"
                                Text="" ForeColor="White" Font-Bold="true"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label3" runat="server" Text="Contract Number : " ForeColor="White"
                            Font-Bold="true"></asp:Label><asp:Label ID="lblContractNumber" runat="server" Text=""
                                ForeColor="White" Font-Bold="true"></asp:Label>
                     <asp:LinkButton runat="server" ID="lnb" 
                         style=" display:block; clear:both; float:right; color:#000;font-size: 16px; font-weight: bold; margin-right: 4px; padding: 2px;text-decoration: none;" 
                         onclick="lnb_Click">X</asp:LinkButton>
                     <iframe scrolling="auto" src="../HotelUser/HotelDashboard.aspx" width="998px" height="870px"
                            frameborder="1" title="Hotel Details" scrolling="yes">Your browser does not support
                            IFRAMEs. </iframe>
                                </div> 
          </asp:Panel>                 
              </div>
		  
                        

</div>
</div>
</asp:Content>

