﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Operator/Operator.master" AutoEventWireup="true" CodeFile="Usermenu.aspx.cs" Inherits="Operator_Usermenu" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControl/Operator/SearchPanel.ascx" TagName="SearchPanel"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/HotelUser/BookingDetails.ascx" TagName="BookingDetails"
    TagPrefix="uc2" %>
<%@ Register src="../UserControl/Operator/BookingRequestCountBoard.ascx" tagname="BookingRequestCountBoard" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
    function disableBackButton() {
        window.history.forward();
    }
    disableBackButton();
    window.onload = new function () { disableBackButton() }
    window.onpageshow = new function (evt) { if (evt != undefined) { if (evt.persisted) disableBackButton(); } }
    window.onunload = new function () { void (0) }
    </script>
    <style type="text/css">
        .modalBackground
        {
            background-color: #CCCCFF;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }
    </style>
    <link href="../css/style.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .LinkPaging
        {
            width: 20;
            background-color: White;
            border: Solid 1 Black;
            text-align: center;
            margin-left: 8;
        }
    </style>
    <%--<asp:UpdateProgress ID="updatesearch1" runat="server" AssociatedUpdatePanelID="UpdatePanelse">
        <ProgressTemplate>
            <div id="Loding_overlay" style="display: block;">
                <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
                Loading...</div>
        </ProgressTemplate>
    </asp:UpdateProgress>--%>
    
   <%--<asp:UpdatePanel ID="UpdatePanelse" runat="server">
        <ContentTemplate>--%>
         
    
            <div class="operator-mainbody">
              
            
    <uc1:SearchPanel ID="SearchPanel1" runat="server" />
    <div id="divmessage" runat="server">
    </div>
        <div class="contract-list">
                   <div class="contract-list" id="divAlphabeticPaging" runat="server" style="margin-top:0px;">
                <div class="contract-list-left" style="width: 278px;">
                    <h2>
                        Users</h2>
                </div>
                <div class="contract-list-right" style="width: 676px;">
                    <ul runat="server" id="AlphaList">
                        <li id="Li1" runat="server"><a href="#" class="select" runat="server" id="all" onserverclick="PageChange">all</a></li>
                        <li id="Li2" runat="server"><a href="#" runat="server" id="a" onserverclick="PageChange">a</a></li>
                        <li id="Li3" runat="server"><a href="#" runat="server" id="b" onserverclick="PageChange">b</a></li>
                        <li id="Li4" runat="server"><a href="#" runat="server" id="c" onserverclick="PageChange">c</a></li>
                        <li id="Li5" runat="server"><a href="#" runat="server" id="d" onserverclick="PageChange">d</a></li>
                        <li id="Li6" runat="server"><a href="#" runat="server" id="e" onserverclick="PageChange">e</a></li>
                        <li id="Li7" runat="server"><a href="#" runat="server" id="f" onserverclick="PageChange">f</a></li>
                        <li id="Li8" runat="server"><a href="#" runat="server" id="g" onserverclick="PageChange">g</a></li>
                        <li id="Li9" runat="server"><a href="#" runat="server" id="h" onserverclick="PageChange">h</a></li>
                        <li id="Li10" runat="server"><a href="#" runat="server" id="i" onserverclick="PageChange">i</a></li>
                        <li id="Li11" runat="server"><a href="#" runat="server" id="j" onserverclick="PageChange">j</a></li>
                        <li id="Li12" runat="server"><a href="#" runat="server" id="k" onserverclick="PageChange">k</a></li>
                        <li id="Li13" runat="server"><a href="#" runat="server" id="l" onserverclick="PageChange">l</a></li>
                        <li id="Li14" runat="server"><a href="#" runat="server" id="m" onserverclick="PageChange">m</a></li>
                        <li id="Li15" runat="server"><a href="#" runat="server" id="n" onserverclick="PageChange">n</a></li>
                        <li id="Li16" runat="server"><a href="#" runat="server" id="o" onserverclick="PageChange">o</a></li>
                        <li id="Li17" runat="server"><a href="#" runat="server" id="p" onserverclick="PageChange">p</a></li>
                        <li id="Li18" runat="server"><a href="#" runat="server" id="q" onserverclick="PageChange">q</a></li>
                        <li id="Li19" runat="server"><a href="#" runat="server" id="r" onserverclick="PageChange">r</a></li>
                        <li id="Li20" runat="server"><a href="#" runat="server" id="s" onserverclick="PageChange">s</a></li>
                        <li id="Li21" runat="server"><a href="#" runat="server" id="t" onserverclick="PageChange">t</a></li>
                        <li id="Li22" runat="server"><a href="#" runat="server" id="u" onserverclick="PageChange">u</a></li>
                        <li id="Li23" runat="server"><a href="#" runat="server" id="v" onserverclick="PageChange">v</a></li>
                        <li id="Li24" runat="server"><a href="#" runat="server" id="w" onserverclick="PageChange">w</a></li>
                        <li id="Li25" runat="server"><a href="#" runat="server" id="x" onserverclick="PageChange">x</a></li>
                        <li id="Li26" runat="server"><a href="#" runat="server" id="y" onserverclick="PageChange">y</a></li>
                        <li id="Li27" runat="server"><a href="#" runat="server" id="z" onserverclick="PageChange">z</a></li>
                    </ul>
                </div>
            </div>
                 

                </div> 
                <div class="pageing-operator">
                    <div class="pageing-operator-new">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="left" valign="bottom">
                                    <div style="float: left; margin-left: 20px;" class="n-commisions">
                                        <div class="n-btn">
                                            <asp:LinkButton ID="lnkAddNew" runat="server" OnClick="lnkAddNew_Click"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Add new </div><div class="n-btn-right">
                            </div></asp:LinkButton>
                                        </div>
                                    </div>
                                    <asp:Panel ID="pnlActivate" runat="server">
                                        <div style="float: left; margin-left: 10px;" class="n-commisions">
                                            <div class="n-btn">
                                                <asp:LinkButton ID="lnkmodify" runat="server" OnClick="lnkmodify_Click" OnClientClick="javascript:return TestCheckBox1();"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Modify</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                                            </div>
                                        </div>
                                        <div style="float: left; margin-left: 10px;" class="n-commisions">
                                            <div class="n-btn">
                                                <asp:LinkButton ID="lnkdelete" runat="server" OnClientClick="javascript:return TestCheckBox(); "
                                                    OnClick="lnkdelete_Click"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Delete</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </td>
                                <td align="right">
                                    Total Result :
                                    <asp:Label ID="lblTotalBooking" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="float: right;" id="pageing">
                    </div>
                </div>
                <div class="search-booking-rowmain clearfix" style="width: 100%">
                    <asp:GridView ID="grdViewBooking" runat="server" Width="100%" Height="100%" border="0"
                        CellPadding="0" CellSpacing="1" AutoGenerateColumns="false" GridLines="None"
                        ShowHeader="true" ShowHeaderWhenEmpty="true" ShowFooter="false" BackColor="#98BCD6"
                        RowStyle-BackColor="#FFFFFF" AlternatingRowStyle-BackColor="#E3F0F1" PageSize="10"
                        AllowPaging="true" EmptyDataRowStyle-BackColor="White" EmptyDataRowStyle-HorizontalAlign="Center"
                        HeaderStyle-BackColor="#98BCD6" OnPageIndexChanging="grdViewBooking_PageIndexChanging"
                        OnRowDataBound="grdViewBooking_RowDataBound" EmptyDataText="No record Found!">
                        <Columns>
                            <asp:TemplateField ControlStyle-Width="3%" ItemStyle-Width="3%">
                                <HeaderStyle BackColor="#CCD8D8" HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkField" onclick="javascript:CheckOtherIsCheckedByGVID(this);" />
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkField" onclick="javascript:CheckOtherIsCheckedByGVID(this);" />
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ControlStyle-Width="5%" ItemStyle-Width="5%">
                                <HeaderStyle BackColor="#CCD8D8" HorizontalAlign="Center" />
                                <HeaderTemplate>
                                    User's id
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblRefNo" runat="server" ForeColor="#339966"></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblRefNo" runat="server" ForeColor="#339966"></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ControlStyle-Width="80%" ItemStyle-Width="5%">
                                <HeaderStyle BackColor="#CCD8D8" HorizontalAlign="Center" />
                                <HeaderTemplate>
                                    User's login
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblEmailId" runat="server"></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblEmailId" runat="server"></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ControlStyle-Width="80%" ItemStyle-Width="5%">
                                <HeaderStyle BackColor="#CCD8D8" HorizontalAlign="Center" />
                                <HeaderTemplate>
                                    User's Type
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblUserType" runat="server"></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblUserType" runat="server"></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ControlStyle-Width="100%" ItemStyle-Width="5%">
                                <HeaderStyle BackColor="#CCD8D8" HorizontalAlign="Center" />
                                <HeaderTemplate>
                                    Registration
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbllCreatedDt" runat="server"> </asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lbllCreatedDt" runat="server"></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ControlStyle-Width="100%" ItemStyle-Width="5%">
                                <HeaderStyle BackColor="#CCD8D8" HorizontalAlign="Center" />
                                <HeaderTemplate>
                                    Last login
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbllastLogin" runat="server"></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lbllastLogin" runat="server"></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ControlStyle-Width="100%" ItemStyle-Width="7%">
                                <HeaderStyle BackColor="#CCD8D8" HorizontalAlign="Center" />
                                <HeaderTemplate>
                                    Name
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblName" runat="server"></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblName" runat="server"></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ControlStyle-Width="70%" ItemStyle-Width="5%">
                                <HeaderStyle BackColor="#CCD8D8" HorizontalAlign="Center" />
                                <HeaderTemplate>
                                    First name
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblFirstName" runat="server"></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblFirstName" runat="server"></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ControlStyle-Width="1%" ItemStyle-Width="2%">
                                <HeaderStyle BackColor="#CCD8D8" HorizontalAlign="Center" />
                                <HeaderTemplate>
                                    Company
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblcompany" runat="server"></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblcompany" runat="server"></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ControlStyle-Width="70%" ItemStyle-Width="5%">
                                <HeaderStyle BackColor="#CCD8D8" HorizontalAlign="Center" />
                                <HeaderTemplate>
                                    City
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblcity" runat="server"></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblcity" runat="server"></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ControlStyle-Width="70%" ItemStyle-Width="5%">
                                <HeaderStyle BackColor="#CCD8D8" HorizontalAlign="Center" />
                                <HeaderTemplate>
                                    No.of booking
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblnobooking" runat="server"></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblnobooking" runat="server"></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ControlStyle-Width="70%" ItemStyle-Width="5%">
                                <HeaderStyle BackColor="#CCD8D8" HorizontalAlign="Center" />
                                <HeaderTemplate>
                                    Contact phone
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblcontact" runat="server"></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblcontact" runat="server"></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ControlStyle-Width="100%" ItemStyle-Width="5%">
                                <HeaderStyle BackColor="#CCD8D8" HorizontalAlign="Center" />
                                <HeaderTemplate>
                                    Status
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblnew" runat="server"></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblnew" runat="server"></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ControlStyle-Width="100%" ItemStyle-Width="5%">
                                <HeaderStyle BackColor="#CCD8D8" HorizontalAlign="Center" />
                                <HeaderTemplate>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LnkIsactive" runat="server" ForeColor="#339966" CommandArgument='<%# Eval("UserId") %>'
                                        OnClick="LnkIsactive_Click"></asp:LinkButton>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:LinkButton ID="LnkIsactive" runat="server" ForeColor="#339966" CommandArgument='<%# Eval("UserId") %>'
                                        OnClick="LnkIsactive_Click"></asp:LinkButton>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <table>
                                <tr>
                                    <td colspan="3" align="center">
                                        <b>No record found !</b>
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                        <PagerStyle HorizontalAlign="Right" BackColor="White" CssClass="displayNone" />
                        <PagerTemplate>
                            <div id="Paging" style="width: 100%; display: none;">
                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                            </div>
                        </PagerTemplate>
                    </asp:GridView>
                </div>
                
                <!-- end contract-list-->
                
            </div>
        <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
    <script language="javascript" type="text/javascript">
        function CheckOtherIsCheckedByGVID(spanChk) {
            var IsChecked = spanChk.checked;
            var CurrentRdbID = spanChk.id;
            var Chk = spanChk;
            Parent = document.getElementById('ContentPlaceHolder1_grdViewBooking');
            var items = Parent.getElementsByTagName('input');

            for (i = 0; i < items.length; i++) {

                if (items[i].id != CurrentRdbID && items[i].type == "checkbox") {

                    if (items[i].checked) {

                        items[i].checked = false;

                        items[i].parentElement.parentElement.style.backgroundColor = 'white';

                        items[i].parentElement.parentElement.style.color = 'black';
                    }
                }
            }
        }


                
    </script>
     <script language="javascript" type="text/javascript">
                        jQuery(document).ready(function () {
                            <% if (ViewState["SearchAlpha"] != null) {%>
                                jQuery('#<%= AlphaList.ClientID %> li a').removeClass('select');
                                jQuery('#ContentPlaceHolder1_<%= ViewState["SearchAlpha"]%>').addClass('select');
                            <% }%>
                        });
    </script>
    <script language="javascript" type="text/javascript">

        jQuery(document).ready(function () {
            if (jQuery("#Paging") != undefined) {
                var inner = jQuery("#Paging").html();
                jQuery("#pageing").html(inner);
                jQuery("#Paging").html("");
            }

        });

        jQuery(document).ready(function () {
            if (jQuery("#PagingRequest") != undefined) {
                var inner = jQuery("#PagingRequest").html();
                jQuery("#PageingRequest").html(inner);
                jQuery("#PagingRequest").html("");
            }

        });
        function TestCheckBox() {
            if (jQuery("#ContentPlaceHolder1_grdViewBooking").find("input:checkbox:checked").length > 0) {

            }
            else {
                alert("Please select at least one User");
                return false;
            }


            return confirm('Are you sure you want to delete this User?');



        }
        function TestCheckBox1() {
            if (jQuery("#ContentPlaceHolder1_grdViewBooking").find("input:checkbox:checked").length > 0) {

            }
            else {
                alert("Please select at least one User");
                return false;
            }

        }
                       
    </script>
</asp:Content>
