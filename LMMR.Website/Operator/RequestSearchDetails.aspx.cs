﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LMMR.Business;
using LMMR.Entities;
using System.Text.RegularExpressions;
using log4net;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Configuration;

public partial class Operator_RequestSearchDetails : System.Web.UI.Page
{
    #region Variable
    ILog logger = log4net.LogManager.GetLogger(typeof(Operator_RequestSearchDetails));
    VList<Viewbookingrequest> Vlistreq = new VList<Viewbookingrequest>();
      ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
    ViewBooking_Hotel objViewBooking = new ViewBooking_Hotel();
    HotelManager objHotelManager = new HotelManager();
    #endregion
    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        SearchPanel1.propBookRef = 1;
        SearchPanel1.SearchButtonClick += new EventHandler(Search_Button);
        SearchPanel1.CancelButtonClick += new EventHandler(Cancel_Button);
        SearchPanel1.rdbBookingClick += new EventHandler(Booking_SelectedIndex);
        
        if (!IsPostBack)
        {
            ViewState["SearchAlpha"] = "all";
            BindBookingDetails();
        }

       
        ApplyPaging();
    }
    #endregion

    #region search
    protected void Cancel_Button(object sender, EventArgs e)
    {
        ViewState["SearchAlpha"] = "all";
        BindBookingDetails();
        ApplyPaging();
    }
    protected void Search_Button(object sender, EventArgs e)
    {
        ViewState["SearchAlpha"] = "all";
        BindBookingDetails();
        ApplyPaging();
    }
    protected void Booking_SelectedIndex(object sender, EventArgs e)
    {
        string whereclaus = string.Empty;
        string orderby = "Id DESC";
        if (SearchPanel1.propBookRef == 0)
        {
            lblType.Text = "Booking#";
            lblDate.Text = "Booking date";
            whereclaus += "booktype = 0";
        }
        else
        {
            lblType.Text = "Request#";
            lblDate.Text = "Request date";
            whereclaus += "booktype in ( 1,2)";// "booktype = 1";
        }
        Vlistreq = objViewBooking.Bindgrid(whereclaus, orderby);
        grvBookingDetails.DataSource = Vlistreq;
        grvBookingDetails.DataBind();
        lblTotalBooking.Text = Convert.ToString(Vlistreq.Count);
        Session["WhereClause"] = Vlistreq;
        Session["WhereClauseTemp"] = Vlistreq;
        ApplyPaging();
        DivrequestDetail.Visible = false;
        divbookingdetails.Visible = false;
    }

    private void ApplyPaging()
    {
        try
        {
            GridViewRow row = grvBookingDetails.TopPagerRow;
            if (row != null)
            {
                PlaceHolder ph;
                LinkButton lnkPaging;
                LinkButton lnkPrevPage;
                LinkButton lnkNextPage;
                lnkPrevPage = new LinkButton();
                lnkPrevPage.CssClass = "pre";
                lnkPrevPage.Width = Unit.Pixel(73);
                lnkPrevPage.CommandName = "Page";
                lnkPrevPage.CommandArgument = "prev";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPrevPage);
                if (grvBookingDetails.PageIndex == 0)
                {
                    lnkPrevPage.Enabled = false;

                }
                for (int i = 1; i <= grvBookingDetails.PageCount; i++)
                {
                    lnkPaging = new LinkButton();
                    if (ViewState["CurrentPage"] != null)
                    {
                        if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    else
                    {
                        if (i == 1)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    lnkPaging.Width = Unit.Pixel(16);
                    lnkPaging.Text = i.ToString();
                    lnkPaging.CommandName = "Page";
                    lnkPaging.CommandArgument = i.ToString();
                    if (i == grvBookingDetails.PageIndex + 1)
                        ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkPaging);
                }
                lnkNextPage = new LinkButton();
                lnkNextPage.CssClass = "nex";
                lnkNextPage.Width = Unit.Pixel(42);
                lnkNextPage.CommandName = "Page";
                lnkNextPage.CommandArgument = "next";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkNextPage);
                ph = (PlaceHolder)row.FindControl("ph");
                if (grvBookingDetails.PageIndex == grvBookingDetails.PageCount - 1)
                {
                    lnkNextPage.Enabled = false;
                }
            }
            DivrequestDetail.Visible = false;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    protected void grvBookingDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            grvBookingDetails.PageIndex = e.NewPageIndex;
            ViewState["CurrentPage"] = e.NewPageIndex;
            grvBookingDetails.DataSource = Session["WhereClauseTemp"];
            grvBookingDetails.DataBind();
            ApplyPaging();
        }
        catch (Exception ex)
        {

        }
    }
    #region PageAlpha
    public void PageChange(object sender, EventArgs e)
    {
        try
        {
            HtmlAnchor anc = (sender as HtmlAnchor);
            anc.Style.Add("class", "select");
            var alpha = anc.InnerHtml;
            string get = alpha.Trim();
            ViewState["SearchAlpha"] = get;
            VList<Viewbookingrequest> objBooking = new VList<Viewbookingrequest>();
            objBooking = (VList<Viewbookingrequest>)Session["WhereClause"];
            ViewState["CurrentPage"] = null;
            if (get != "all")
            {
                Session["WhereClauseTemp"] = objBooking.FindAll(a => a.HotelName.ToLower().StartsWith(get.ToLower()));
                grvBookingDetails.DataSource = Session["WhereClauseTemp"];
                grvBookingDetails.DataBind();
                lblTotalBooking.Text = Convert.ToString(grvBookingDetails.Rows.Count);
                ApplyPaging();
            }
            else
            {
                Session["WhereClauseTemp"] = objBooking;
                grvBookingDetails.DataSource = objBooking;
                grvBookingDetails.DataBind();
                lblTotalBooking.Text = Convert.ToString(grvBookingDetails.Rows.Count);
                ApplyPaging();
            }

        }
        catch (Exception ex)
        {
            //logger.Error(ex);
        }
    }
    #endregion
    #endregion

    #region BindGrid for Booking
    /// <summary>
    /// Method to bind the grid
    /// </summary>    
    protected void BindBookingDetails()
    {
        try
        {
            string whereclaus = string.Empty;
            string orderby = "Id DESC";
            //if (!string.IsNullOrEmpty(SearchPanel1.propBookRefNumber))
            //{
            //    if (!string.IsNullOrEmpty(whereclaus.Trim()))
            //    {
            //        whereclaus += " and ";
            //    }
            //    whereclaus += ViewbookingrequestColumn.Id + "=" + SearchPanel1.propBookRefNumber + " ";
            //}
            //if (SearchPanel1.propHotelId != 0)
            //{
            //    if (!string.IsNullOrEmpty(whereclaus.Trim()))
            //    {
            //        whereclaus += " and ";
            //    }
            //    whereclaus += ViewbookingrequestColumn.HotelId + "=" + SearchPanel1.propHotelId + " ";
            //}
            //if (SearchPanel1.propMeetingRoomId != 0)
            //{
            //    if (!string.IsNullOrEmpty(whereclaus.Trim()))
            //    {
            //        whereclaus += " and ";
            //    }
            //    whereclaus += ViewbookingrequestColumn.MainMeetingRoomId + "=" + SearchPanel1.propMeetingRoomId + " ";
            //}

            //country
            if (SearchPanel1.propCountryID != 0)
            {
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += ViewbookingrequestColumn.CountryId + "=" + SearchPanel1.propCountryID + " ";
            }
            //--- city
            if (SearchPanel1.propMultiCityID != "0")
            {
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += ViewbookingrequestColumn.CityId + " in (" + SearchPanel1.propMultiCityID + ") ";
            }

            //--- venue
            if (!string.IsNullOrEmpty(SearchPanel1.Venus))
            {
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += ViewbookingrequestColumn.HotelName + " like '" + SearchPanel1.Venus + "%' ";
            }

            //--- arrival date
            if (SearchPanel1.propFromDate != DateTime.MinValue && SearchPanel1.propToDate != DateTime.MinValue)
            {
                if (whereclaus.Length > 0)
                {
                    whereclaus += " and ";
                }
                whereclaus += ViewbookingrequestColumn.ArrivalDate + ">=  '" + SearchPanel1.propFromDate + "' " + " and " + ViewbookingrequestColumn.ArrivalDate + "<= '" + SearchPanel1.propToDate + "' ";
                //  whereclaus += ViewbookingrequestColumn.ArrivalDate + " >='" + SearchPanel1.propFromDate + "'  and   ViewbookingrequestColumn.ArrivalDate >='" + SearchPanel1.propToDate + "',103) ";
            }

            //--- client
            if (!string.IsNullOrEmpty(SearchPanel1.propclientname))
            {
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += "(";
                if (SearchPanel1.proprbtvenueclient == 1)
                {
                    whereclaus += ViewbookingrequestColumn.Contact + " like '" + SearchPanel1.propclientname + "%'";
                }
                else if (SearchPanel1.proprbtvenueclient == 2)
                {
                    whereclaus += ViewbookingrequestColumn.HotelName + " like '" + SearchPanel1.propclientname + "%'  ";
                }


                whereclaus += ")";
            }
            if (SearchPanel1.propBookRef == 0)
            {
                lblType.Text = "Booking#";
                lblDate.Text = "Booking date";
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += "booktype = 0";
            }
            else
            {
                lblType.Text = "Request#";
                lblDate.Text = "Request date";
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += "booktype in ( 1,2 )";// "booktype = 1";
            }
            Vlistreq = objViewBooking.Bindgrid(whereclaus, orderby);
            grvBookingDetails.DataSource = Vlistreq;//.OrderBy(a => a.Id);
            grvBookingDetails.DataBind();
            lblTotalBooking.Text = Convert.ToString(Vlistreq.Count);
            Session["WhereClause"] = Vlistreq;
            Session["WhereClauseTemp"] = Vlistreq;
            //  ApplyPaging();
            DivrequestDetail.Visible = false;
            divbookingdetails.Visible = false;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    //#region Reset for Cancel button
    //public void ResetCancel()
    //{
    //    BindBookingDetails();
    //}
    //#endregion

    #region Change booking/request status
    /// <summary>
    /// when check commsion was checked
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  
    protected void btnchkcommision_Click(object sender, EventArgs e)
    {
        try
        {

            LinkButton btnchkcomm = (LinkButton)sender;
            ViewState["ChkcommBookingID"] = btnchkcomm.CommandArgument;

            if (btnchkcomm.Text.ToLower() == "change status")
            {
                ModalPopupCheck.Show();
                if (SearchPanel1.propBookRef == 1)
                {
                    drpStatus.Visible = false;
                    ddlstatus.Visible = true;
                    ViewState["popup"] = "1";
                }
                else
                {
                    ViewState["popup"] = "0";
                    drpStatus.Visible = true;
                    ddlstatus.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    protected void grvBookingDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
            {
                Label lblCity = (Label)e.Row.FindControl("lblCity");
                HiddenField hdnCityId = (HiddenField)e.Row.FindControl("hdnCityId");
                City objCity = objHotelManager.GetByCityID(Convert.ToInt32(hdnCityId.Value));
                lblCity.Text = objCity.City;



                Label lblArrivalDt = (Label)e.Row.FindControl("lblArrivalDt");
                LinkButton lblRefNo = (LinkButton)e.Row.FindControl("lblRefNo");
                Label lblDepartureDt = (Label)e.Row.FindControl("lblDepartureDt");
                Label lblExpiryDt = (Label)e.Row.FindControl("lblExpiryDt");

                Label lblBookingDt = (Label)e.Row.FindControl("lblBookingDt");
                Label lblstatus = (Label)e.Row.FindControl("lblstatus");
                DateTime startTime = DateTime.Now;
                HyperLink PlanView = (HyperLink)e.Row.FindControl("linkviewPlan");
                HiddenField hdf = ((HiddenField)e.Row.FindControl("hdfImage"));
                string depdt = lblDepartureDt.Text;
                Label lblComm = (Label)e.Row.FindControl("lblComm");

                #region Bindrequeststaus
                lblstatus.Text = Enum.GetName(typeof(BookingRequestStatus), Convert.ToInt32(lblstatus.ToolTip));
                #endregion



                if (lblComm.Text.ToLower() == "false" || string.IsNullOrEmpty(lblComm.Text))
                {
                    //((System.Web.UI.WebControls.Image)e.Row.FindControl("imgCheck")).Visible = false;
                    PlanView.Visible = false;
                }
                else
                {
                    //((System.Web.UI.WebControls.Image)e.Row.FindControl("imgCheck")).Visible = true;
                    if (hdf.Value != "")
                    {
                        PlanView.NavigateUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "SupportDocNetto/" + hdf.Value;
                    }
                    else
                    {
                        PlanView.Visible = false;
                    }
                }




                #region frozen
                if (lblstatus.Text == BookingRequestStatus.Frozen.ToString())
                {
                    //  lblRefNo.Enabled = false;
                    e.Row.BackColor = System.Drawing.Color.LightGray;
                }
                if (lblstatus.Text == BookingRequestStatus.Expired.ToString())
                {
                    //  lblRefNo.Enabled = false;
                    e.Row.BackColor = System.Drawing.Color.LightGray;
                }
                #endregion



            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            Booking objbooking = objViewBooking.getparticularbookingdetails(Convert.ToInt64(ViewState["ChkcommBookingID"].ToString()));
            if (Convert.ToString(ViewState["popup"]) == "1")
            {
                objbooking.RequestStatus = Convert.ToInt32(ddlstatus.SelectedValue);
                if (objViewBooking.updatecheckComm(objbooking))
                {
                    ModalPopupCheck.Hide();
                    BindBookingDetails();
                }
            }
            else if (Convert.ToString(ViewState["popup"]) == "0")
            {
                objViewBooking.CancelBooking(Convert.ToInt64(ViewState["ChkcommBookingID"].ToString()));
                ModalPopupCheck.Hide();
                BindBookingDetails();
            }


        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }

    #region Cancel Button Click
    /// <summary>
    /// Modal popup cancel event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            ModalPopupCheck.Hide();
            ddlstatus.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    #endregion
    protected void grvBookingDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (Convert.ToString(e.CommandName) == "ViewBookingDetail")
            {
                Booking obj=        objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(e.CommandArgument));
                if (obj.BookType == 2)
                {
              
                    divbookingdetails.Visible = true;
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + divbookingdetails.ClientID + "');});", true);
                    DivrequestDetail.Visible = false;
                    ucViewBookingDetail.BindBooking(Convert.ToInt32(e.CommandArgument));
                }
                else  if (obj.BookType == 1)
                {
                    requestDetails.BindHotel(Convert.ToInt32(e.CommandArgument));
                    DivrequestDetail.Visible = true;
                    divbookingdetails.Visible = false;
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + DivrequestDetail.ClientID + "');});", true);
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    #region PDF
    protected void lnkSavePDF_Click(object sender, EventArgs e)
    {
        try
        {
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=UserDetails.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            PrepareGridViewForExport(Divdetails);
            Divdetails.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }




    }

    #region VerifyRenderingInServerForm
    /// <summary>
    /// method to VerifyRenderingInServerForm
    /// </summary>

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    #endregion

    #region PrepareGridViewForExport
    /// <summary>
    /// method to PrepareGridViewForExport
    /// </summary>
    private void PrepareGridViewForExport(Control gv)
    {
        try
        {
            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {

                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(DropDownList))
                {

                    l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(CheckBox))
                {

                    l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(HyperLink))
                {

                    l.Text = (gv.Controls[i] as HyperLink).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.Image))
                {

                    // l.Text = (gv.Controls[i] as Image).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareGridViewForExport(gv.Controls[i]);

                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }
    #endregion
    #region PrepareGridViewForExportDIV
    /// <summary>
    /// method to PrepareGridViewForExport a div
    /// </summary>
    private void PrepareDivForExport(Control gv)
    {
        try
        {

            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {

                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].GetType() == typeof(System.Web.UI.HtmlControls.HtmlAnchor))
                {

                    l.Text = (gv.Controls[i] as System.Web.UI.HtmlControls.HtmlAnchor).InnerText;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(DropDownList))
                {

                    l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(GridView))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    // gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Repeater))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Panel))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Table))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }


                else if (gv.Controls[i].GetType() == typeof(CheckBox))
                {

                    l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(HyperLink))
                {

                    l.Text = (gv.Controls[i] as HyperLink).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                //else if (gv.Controls[i].GetType() == typeof(Label))
                //{

                //    l.Text = (gv.Controls[i] as Label).Text;

                //    gv.Controls.Remove(gv.Controls[i]);

                //    gv.Controls.AddAt(i, l);

                //}
                else if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.Image))
                {

                    // l.Text = (gv.Controls[i] as Image).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareDivForExport(gv.Controls[i]);

                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
        //PrepareGridViewForExport();
        //PrepareGridViewForExport();

    }


    #endregion

    protected void lnkSaveRequestPdf_Click(object sender, EventArgs e)
    {
        try
        {
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=UserDetails.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            PrepareGridViewForExport(DivdetailsRequest);
            DivdetailsRequest.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }
    #endregion
}