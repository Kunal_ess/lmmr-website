<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TestDrop.aspx.cs" Inherits="Operator_TestDrop" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>operator-layout</title>
    <link href="../css/style.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqtransform.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" src="../js/jquery.jqtransform.js"></script>
    <script language="javascript">
        $(function () {
            $('form').jqTransform({ imgPath: 'jqtransformplugin/img/' });
        });
    </script>
</head>
<body class="bg">
    <div class="main_container1">
        <!-- start left section-->
        <div class="left_container1">
            <div class="main_logo_new">
                <img src="../images/logo.png" alt="Last Minute"></div>
        </div>
        <!-- end left section-->
        <!-- start right section-->
        <div class="right_container1">
            <!--Main navigation start -->
            <div class="main_nav">
                <div id="subnavbar">
                    <ul id="subnav">
                        <li class="cat-item cat-item-1"><a href="#">Control Panel</a> </li>
                        <li class="cat-item cat-item-366"><a href="#" class="select">Clients-Contracts</a></li>
                        <li class="cat-item cat-item-311"><a href="#">Search bookings</a> </li>
                        <li class="cat-item cat-item-313"><a href="#">Search requests</a> </li>
                        <li class="cat-item cat-item-313"><a href="#">Availability & Special monitoring</a>
                        </li>
                    </ul>
                </div>
            </div>
            <!--main navigation end -->
            <!--Main navigation start -->
            <div class="main_nav1">
                <div id="subnavbar1">
                    <ul id="subnav1">
                        <li class="cat-item"><a href="#">Commisions</a> </li>
                        <li class="cat-item"><a href="#">Cancel & Transfer</a></li>
                        <li class="cat-item"><a href="#">Hotel/Facility Access</a> </li>
                        <li class="cat-item"><a href="#">Users</a> </li>
                    </ul>
                </div>
            </div>
            <!--main navigation end -->
        </div>
        <!-- end right section-->
        <!--main_body start -->
        <div class="main_body">
            <!--logout-today start -->
            <div class="logout-today">
                <div class="today">
                    <span>Today:</span>16 october 2011</div>
                <div class="logout">
                    You are logged in as: <span>Super Admin</span> � <a href="#">log out</a></div>
            </div>
            <!-- end logout-today-->
            <!--search-operator-layout start -->
            <div class="search-operator-layout">
                <div class="search-operator-layout-left">
                    <div class="search-operator-from">
                        <form method="POST">
                        <div class="search-operator-from-left">
                            <div class="search-form-left">
                                <div class="rowElem">
                                    <select name="select4" class="dropdown">
                                        <option value="opt1">Select country</option>
                                        <option value="opt2">Option 2</option>
                                        <option value="opt3">Option 3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="search-form-left">
                                <div class="rowElem">
                                    <select name="select4" class="dropdown">
                                        <option value="opt1">Select City</option>
                                        <option value="opt2">Option 2</option>
                                        <option value="opt3">Option 3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="search-form-left">
                                <input type="checkbox" name="chbox" id=""><label>Clients</label>
                            </div>
                            <div class="search-form-left">
                                <input type="checkbox" name="chbox" id=""><label>Contracts</label>
                            </div>
                        </div>
                        <div class="search-operator-from-right">
                            <div class="search-form1">
                                <div class="search-form1-left">
                                    &nbsp;
                                </div>
                                <div class="search-form1-right">
                                    <input type="text" value="Search � Type at least 3 letters..." class="inputbox" />
                                </div>
                            </div>
                            <div class="search-form1">
                                <div class="search-form1-left">
                                    Company client:
                                </div>
                                <div class="search-form1-right">
                                    <input type="text" value="Enter nr here..." class="inputbox" />
                                </div>
                            </div>
                            <div class="search-form1">
                                <div class="search-form1-left">
                                    Facility (hotel):
                                </div>
                                <div class="search-form1-right">
                                    <input type="text" value="Enter nr here..." class="inputbox" />
                                </div>
                            </div>
                            <div class="search-form1">
                                <div class="search-form1-left">
                                    Contract date: &nbsp; From
                                </div>
                                <div class="search-form1-right1">
                                    <input type="text" value="dd/mm/yy" class="inputbox1" />
                                </div>
                                <div class="search-form1-right2">
                                    <img src="../images/date-icon.png">
                                </div>
                                <div class="search-form1-right3">
                                    To
                                </div>
                                <div class="search-form1-right1">
                                    <input type="text" value="dd/mm/yy" class="inputbox1" />
                                </div>
                                <div class="search-form1-right2">
                                    <img src="../images/date-icon.png">
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
                <div class="search-operator-layout-right">
                    <b>Lorem ipsum dolor sit amet.</b><br>
                    Consectetur adipiscing elit. Fusce lectus justo, varius a aliquet at, pretium dictum
                    sapien. Duis fermentum aliquet eros, nec porta enim posuere ut.
                    <div class="search-btn-body">
                        <a class="Search-btn" href="#">Search</a></div>
                </div>
            </div>
            <!-- end search-operator-layout-->
            <!--contract-list start -->
            <div class="contract-list">
                <div class="contract-list-left">
                    <h2>
                        Contract list</h2>
                </div>
                <div class="contract-list-right">
                    <ul>
                        <li><a href="#">a</a></li><li><a href="#" class="select">b</a></li><li><a href="#">c</a></li><li>
                            <a href="#">d</a></li><li><a href="#">e</a></li><li><a href="#">f</a></li><li><a
                                href="#">g</a></li><li><a href="#">h</a></li><li><a href="#">i</a></li><li><a href="#">
                                    j</a></li><li><a href="#">k</a></li><li><a href="#">l</a></li><li><a href="#">m</a></li><li>
                                        <a href="#">n</a></li><li><a href="#">o</a></li><li><a href="#">p</a></li><li><a
                                            href="#">q</a></li><li><a href="#">r</a></li><li><a href="#">s</a></li><li><a href="#">
                                                t</a></li><li><a href="#">u</a></li><li><a href="#">v</a></li><li><a href="#">w</a></li><li>
                                                    <a href="#">x</a></li><li><a href="#">y</a></li><li><a href="#">z</a></li>
                    </ul>
                </div>
            </div>
            <!-- end contract-list-->
            <!--pageing-operator start -->
            <div class="pageing-operator">
                <div class="pageing-operator-left">
                    <a class="Add-contract-btn" href="#">Add new contract</a> <a class="Modify-btn" href="#">
                        Modify</a><a class="Delete-btn" href="#">Delete</a></div>
                <div class="pageing-operator-right">
                    <div class="pagination">
                        <ul class="pager">
                            <li class="perv"><a href="#" style="width: auto;">Prevous</a></li>
                            <li><a href="#">1</a> </li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#" class="select">4</a></li>
                            <li><a href="#">5</a></li>
                            <li class="perv"><a href="#" class="select">Next</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- end pageing-operator-->
            <!--Contract-body start -->
            <div class="contract-body">
                <div class="contract-body-heading">
                    <div class="contract-heading-box1">
                        <span>Contract nr.</span>
                    </div>
                    <div class="contract-heading-box2">
                        <div class="contract-heading-box2-top">
                            <form>
                            <div class="rowElem">
                                <select name="select4" class="dropdown">
                                    <option value="opt1">country</option>
                                    <option value="opt2">Option 2</option>
                                    <option value="opt3">Option 3</option>
                                </select>
                            </div>
                            </form>
                        </div>
                        <br>
                        <br>
                        <div class="contract-heading-box2-bottom">
                            <form>
                            <div class="rowElem">
                                <select name="select4" class="dropdown">
                                    <option value="opt1">Select city</option>
                                    <option value="opt2">Option 2</option>
                                    <option value="opt3">Option 3</option>
                                </select>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div class="contract-heading-box3">
                        <form>
                        <div class="rowElem">
                            <select name="select4" class="dropdown">
                                <option value="opt1">Facility/Hotel name</option>
                                <option value="opt2">Option 2</option>
                                <option value="opt3">Option 3</option>
                            </select>
                        </div>
                        </form>
                    </div>
                    <div class="contract-heading-box4">
                        <span>Client (company)</span><br>
                        Contact person
                    </div>
                    <div class="contract-heading-box5">
                        <span>Contact phone</span><br>
                        Contact email
                    </div>
                    <div class="contract-heading-box6">
                        <span>Sales agent</span><br>
                        Signed date
                    </div>
                    <div class="contract-heading-box7">
                        <span>Value</span>
                    </div>
                    <div class="contract-heading-box8">
                        <span>Scan</span>
                    </div>
                    <div class="contract-heading-box9">
                        <form>
                        <div class="rowElem">
                            <select name="select4" class="dropdown">
                                <option value="opt1">only active</option>
                                <option value="opt2">Option 2</option>
                                <option value="opt3">Option 3</option>
                            </select>
                        </div>
                        </form>
                    </div>
                </div>
                <div class="contract-body-box">
                    <ul>
                        <li>
                            <div class="contract-li-box1">
                                <span>12798765</span>
                            </div>
                            <div class="contract-li-box2">
                                <span>Belgium</span><br>
                                Antwerpen
                            </div>
                            <div class="contract-li-box3">
                                <a href="#">Bepart hotels</a>
                            </div>
                            <div class="contract-li-box4">
                                <span>SC xxx company SRL</span><br>
                                Enric Van Muller
                            </div>
                            <div class="contract-li-box5">
                                <span>+32 498 989 345</span><br>
                                <a href="#">Muller@xxxcompany...</a>
                            </div>
                            <div class="contract-li-box6">
                                <span>John dohle</span><br>
                                today
                            </div>
                            <div class="contract-li-box7">
                                <span>� 30.500</span>
                            </div>
                            <div class="contract-li-box8">
                                <img src="../images/pdf-icon.png">
                            </div>
                            <div class="contract-li-box9">
                                <form>
                                <input type="checkbox" checked="checked" name="chbox" id="">
                                </form>
                            </div>
                        </li>
                        <li class="light">
                            <div class="contract-li-box1">
                                <span>15796548</span>
                            </div>
                            <div class="contract-li-box2">
                                <span>France</span><br>
                                Lion
                            </div>
                            <div class="contract-li-box3">
                                <a href="#">Parck hotel</a>
                            </div>
                            <div class="contract-li-box4">
                                <span>Conference building SPRL</span><br>
                                Pieter Corneels
                            </div>
                            <div class="contract-li-box5">
                                <span>+32 9 346 80 52</span><br>
                                <a href="#">Pieter.Corneels@conf...</a>
                            </div>
                            <div class="contract-li-box6">
                                <span>another agent</span><br>
                                20/08
                            </div>
                            <div class="contract-li-box7">
                                <span>� 12.000</span>
                            </div>
                            <div class="contract-li-box8">
                                <img src="../images/pdf-icon.png">
                            </div>
                            <div class="contract-li-box9">
                                <form>
                                <input type="checkbox" checked="checked" name="chbox" id="">
                                </form>
                            </div>
                        </li>
                        <li>
                            <div class="contract-li-box1">
                                <span>25648357</span>
                            </div>
                            <div class="contract-li-box2">
                                <span>Belgium</span><br>
                                Brucelles
                            </div>
                            <div class="contract-li-box3">
                                <a href="#">Parck hotel</a>
                            </div>
                            <div class="contract-li-box4">
                                <span>Majestic building business SRL</span><br>
                                Ruben van Maele
                            </div>
                            <div class="contract-li-box5">
                                <span>+32 2 364 89 04</span><br>
                                <a href="#">Ruben@majesticbuild...</a>
                            </div>
                            <div class="contract-li-box6">
                                <span>sales agent</span><br>
                                19/08
                            </div>
                            <div class="contract-li-box7">
                                <span>� 12.000</span>
                            </div>
                            <div class="contract-li-box8">
                                <img src="../images/pdf-icon.png">
                            </div>
                            <div class="contract-li-box9">
                                <form>
                                <input type="checkbox" checked="checked" name="chbox" id="">
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- end search-operator-layout-->
            <!--contract hotal start -->
            <div class="contract-hotal">
                <div class="contract-hotal-left">
                    <h2>
                        New contract for Hotel/Facility</h2>
                </div>
                <div class="contract-hotal-right">
                    <a class="save-btn" href="#">Save</a> <span>or</span> <a href="#" class="cancel-btn">
                        cancel</a>
                </div>
            </div>
            <!-- end contract-hotal-->
            <!--contract hotal body start -->
            <div class="contract-hotal-body">
                <!--contract hotal body start -->
                <div class="contract-hotal-body-left">
                    <form method="POST">
                    <div class="contract-form1">
                        <div class="contract-form-left">
                            Contract nr.
                        </div>
                        <div class="contract-form-right">
                            CRO-2897875
                        </div>
                    </div>
                    <div class="contract-form">
                        <div class="contract-form-left">
                            Country
                        </div>
                        <div class="contract-form-right">
                            <div class="rowElem">
                                <select name="select4" class="dropdown">
                                    <option value="opt1">Croatia</option>
                                    <option value="opt2">Option 2</option>
                                    <option value="opt3">Option 3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="contract-form-dark">
                        <div class="contract-form-left">
                            Currency
                        </div>
                        <div class="contract-form-right">
                            <div class="rowElem">
                                <select name="select5" class="dropdown">
                                    <option value="opt1">Select currency</option>
                                    <option value="opt2">Option 2</option>
                                    <option value="opt3">Option 3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="contract-form">
                        <div class="contract-form-left">
                            City
                        </div>
                        <div class="contract-form-right">
                            <div class="rowElem">
                                <select name="select4" class="dropdown">
                                    <option value="opt1">Select City</option>
                                    <option value="opt2">Option 2</option>
                                    <option value="opt3">Option 3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="contract-form-dark">
                        <div class="contract-form-left">
                            Zone
                        </div>
                        <div class="contract-form-right">
                            <div class="rowElem">
                                <select name="select5" class="dropdown">
                                    <option value="opt1">Select zone</option>
                                    <option value="opt2">Option 2</option>
                                    <option value="opt3">Option 3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="contract-form">
                        <div class="contract-form-left">
                            Client name (company)
                        </div>
                        <div class="contract-form-right">
                            <div class="rowElem">
                                <select name="select4" class="dropdown">
                                    <option value="opt1">Select client</option>
                                    <option value="opt2">Option 2</option>
                                    <option value="opt3">Option 3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="contract-form-dark">
                        <div class="contract-form-left">
                            Sales Person
                        </div>
                        <div class="contract-form-right">
                            <div class="rowElem">
                                <select name="select5" class="dropdown">
                                    <option value="opt1">Select sales person</option>
                                    <option value="opt2">Option 2</option>
                                    <option value="opt3">Option 3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="contract-form">
                        <div class="contract-form-left">
                            Hotel name
                        </div>
                        <div class="contract-form-right">
                            <input type="text" value="Pegasus 3 stars hotel" class="inputbox" />
                        </div>
                    </div>
                    <div class="contract-form-dark">
                        <div class="contract-form-left">
                            Hotel Address
                        </div>
                        <div class="contract-form-right">
                            <input type="text" value="Pegasus 3 stars hotel" class="inputbox" />
                        </div>
                        <div class="contract-form-left">
                            &nbsp;
                        </div>
                        <div class="contract-form-right">
                            <input type="text" value="Pegasus 3 stars hotel" class="inputbox" />
                        </div>
                        <div class="contract-form-left">
                            Latitude/Longitude
                        </div>
                        <div class="contract-form-right" style="padding: 9px 0px 10px 5px;">
                            50 9090 / 42 0030
                        </div>
                    </div>
                    <div class="contract-form">
                        <div class="contract-form-left">
                            Hotel Contact person
                        </div>
                        <div class="contract-form-right">
                            <input type="text" value="Enter hotel contact person" class="inputbox" />
                        </div>
                    </div>
                    <div class="contract-form-dark">
                        <div class="contract-form-left">
                            Hotel email account
                        </div>
                        <div class="contract-form-right">
                            <input type="text" value="Enter hotel email account" class="inputbox" />
                        </div>
                    </div>
                    <div class="contract-form">
                        <div class="contract-form-left">
                            Hotel Phone
                        </div>
                        <div class="contract-form-right">
                            <input type="text" value="Enter hotel phone number" class="inputbox" />
                        </div>
                    </div>
                    <div class="contract-form-dark">
                        <div class="contract-form-left">
                            Contract value
                        </div>
                        <div class="contract-form-right">
                            <input type="text" value="Enter contract value" class="inputbox" />
                        </div>
                    </div>
                    <div class="contract-form">
                        <div class="contract-form-left">
                            Scanned file
                        </div>
                        <div class="contract-form-right">
                            <div class="browse-cancel-btn">
                                <a class="browse-btn" href="#">Browse</a> <span>or</span> <a href="#" class="cancel-btn">
                                    cancel</a>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <!-- end contract hotal body-->
                <!--contract hotal body start -->
                <div class="contract-map-body">
                    <img src="../images/operator-layout-map.png" />
                </div>
                <!-- end contract hotal body-->
            </div>
            <!-- end contract hotal body-->
            <!--save-cancel-btn-body start -->
            <div class="save-cancel-btn-body">
                <div class="save-cancel-btn">
                    <a class="save-btn" href="#">Save</a> <span>or</span> <a href="#" class="cancel-btn">
                        cancel</a>
                </div>
            </div>
            <!-- end save-cancel-btn-body-->
        </div>
        <!-- end main_body-->
    </div>
</body>
</html>
