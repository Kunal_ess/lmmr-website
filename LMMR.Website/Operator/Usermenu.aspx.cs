﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LMMR.Business;
using LMMR.Entities;
using System.Text.RegularExpressions;
using log4net;
using LMMR.Data;
using System.Configuration;
using System.IO;

public partial class Operator_Usermenu : System.Web.UI.Page
    {
        TList<Users> tuser = new TList<Users>();
        Usersdata ud = new Usersdata();
        UserManager um = new UserManager();
        NewUser newUserObject = new NewUser();
        EmailConfigManager em = new EmailConfigManager();
        string status;
        //string status1;
        string whereclaus = String.Empty;
        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            SearchPanel1.SearchButtonClick += new EventHandler(Search_Button);
            SearchPanel1.CancelButtonClick += new EventHandler(Cancel_Button);



            if (!IsPostBack)
            {
                ViewState["SearchAlpha"] = "all";
                ViewState["Where"] = string.Empty;
                ViewState["Order"] = string.Empty;
                ViewState["companysearch"] = string.Empty;
                bindhotel();
                if (Session["Update"] == null)
                {
                    divmessage.Style.Add("display", "none");
                    divmessage.Attributes.Add("class", "error");
                    divmessage.Attributes.Add("class", "succesfuly");
                }
                else
                {
                    divmessage.Visible = true;
                    if (Session["Update"] == "Information updated successfully.")
                    {
                        status = "User updated successfully ";
                        divmessage.Visible = true;
                        divmessage.Style.Add("display", "block");
                        divmessage.Attributes.Add("class", "succesfuly");
                        divmessage.InnerHtml = status.ToString();
                    }
                    else if (Session["Update"] == "Please activate you account by clicking on the link from the email message.")
                    {
                        status = "User added successfully ";
                        divmessage.Visible = true;
                        divmessage.Style.Add("display", "block");
                        divmessage.Attributes.Add("class", "succesfuly");
                        divmessage.InnerHtml = status.ToString();
                    }
                    else
                    {
                        status = Session["Update"].ToString();
                        divmessage.Visible = true;
                        divmessage.Style.Add("display", "block");
                        divmessage.Attributes.Add("class", "error");
                        divmessage.InnerHtml = status.ToString();
                    }

                }

                Session["Update"] = null;



                //FilterGridWithHeader(string.Empty, string.Empty, string.Empty);



            }

            else
            {
                Session["Update"] = null;
                ApplyPaging();

            }
        }
        #endregion

        #region Buttons Usercontrol
        protected void Search_Button(object sender, EventArgs e)
        {
            ViewState["SearchAlpha"] = "all";
            bindhotel();

        }

        protected void Cancel_Button(object sender, EventArgs e)
        {
            ViewState["SearchAlpha"] = "all";
            bindhotel();

            divmessage.Visible = false;
        }
        #endregion

        #region Bind GridView
        public void bindhotel()
        {

            string whereclaus = string.Empty;
            string orderby = "CreatedDate DESC";
            int totalcount = 0;
            int intCount = 1;
            Users u = new Users();
            UserDetails ud = new UserDetails();
            Booking book = new Booking();
            int countBooking = u.BookingCollectionGetByCreatorId.FindAll(a => a.BookType == 0).Count();
            //string test = string.Format("{0:dd MMM yyyy}", lbllCreatedDt.Text);
            if (SearchPanel1.Username != "" && SearchPanel1.Company != "" && SearchPanel1.Usertype == 7)
            {
                whereclaus = "Usertype = 7 and LastName Like '" + SearchPanel1.Username + "%'";
                ViewState["ClientName"] = SearchPanel1.Username.Replace("'", "");
            }
            else if (SearchPanel1.Username != "" && SearchPanel1.Company != "" && SearchPanel1.Usertype == 9)
            {
                whereclaus = "Usertype = 9 and LastName Like '" + SearchPanel1.Username + "%'";
                ViewState["ClientName"] = SearchPanel1.Username.Replace("'", "");
            }
            else if (SearchPanel1.Username != "" && SearchPanel1.Company != "" && SearchPanel1.Usertype == 10)
            {
                whereclaus = "Usertype = 10 and LastName Like '" + SearchPanel1.Username + "%'";
                ViewState["ClientName"] = SearchPanel1.Username.Replace("'", "");
            }
            else if (SearchPanel1.Username != "" && SearchPanel1.Company != "" && SearchPanel1.Usertype == 1)
            {
                whereclaus = "Usertype In (7,9,10) and LastName Like '" + SearchPanel1.Username + "%'";
                ViewState["ClientName"] = SearchPanel1.Username.Replace("'", "");
            }
            else if (SearchPanel1.Username != "" && SearchPanel1.Company != "")
            {
                whereclaus = "Usertype In (7,9,10) and LastName Like '" + SearchPanel1.Username + "%'";
                ViewState["ClientName"] = SearchPanel1.Username.Replace("'", "");
            }
            else if (SearchPanel1.Username != "" && SearchPanel1.Usertype == 7)
            {
                whereclaus = "Usertype = 7 and LastName Like '" + SearchPanel1.Username + "%'";
                ViewState["ClientName"] = SearchPanel1.Username.Replace("'", "");
            }
            else if (SearchPanel1.Username != "" && SearchPanel1.Usertype == 9)
            {
                whereclaus = "Usertype = 9 and LastName Like '" + SearchPanel1.Username + "%'";
                ViewState["ClientName"] = SearchPanel1.Username.Replace("'", "");
            }
            else if (SearchPanel1.Username != "" && SearchPanel1.Usertype == 10)
            {
                whereclaus = "Usertype = 10 and LastName Like '" + SearchPanel1.Username + "%'";
                ViewState["ClientName"] = SearchPanel1.Username.Replace("'", "");
            }
            else if (SearchPanel1.Username != "" && SearchPanel1.Usertype == 1)
            {
                whereclaus = "Usertype In (7,9,10) and LastName Like '" + SearchPanel1.Username + "%'";
                ViewState["ClientName"] = SearchPanel1.Username.Replace("'", "");
            }
            else if (SearchPanel1.Company != "" && SearchPanel1.Usertype == 7)
            {
                whereclaus = "Usertype = 7 ";
                ViewState["ClientName"] = SearchPanel1.Username.Replace("'", "");
            }
            else if (SearchPanel1.Company != "" && SearchPanel1.Usertype == 9)
            {
                whereclaus = "Usertype = 9 ";
            }
            else if (SearchPanel1.Company != "" && SearchPanel1.Usertype == 10)
            {
                whereclaus = "Usertype = 10 ";
                ViewState["ClientName"] = SearchPanel1.Username.Replace("'", "");
            }
            else if (SearchPanel1.Company != "" && SearchPanel1.Usertype == 1)
            {
                whereclaus = "Usertype In (7,9,10) ";
                ViewState["ClientName"] = SearchPanel1.Username.Replace("'", "");
            }

            else if (SearchPanel1.Username != "")
            {


                whereclaus = " LastName Like '" + SearchPanel1.Username + "%' and  Usertype In (7,9,10) ";
                ViewState["ClientName"] = SearchPanel1.Username.Replace("'", "");
                //and Usertype = '" + SearchPanel1.Usertype + "'  
            }
            else if (SearchPanel1.Company != "")
            {
                whereclaus = "Usertype In (7,9,10)";
                ViewState["ClientName"] = SearchPanel1.Username.Replace("'", "");
            }
            else
            {
                if (SearchPanel1.Usertype == 7)
                {
                    whereclaus = "Usertype = 7  ";
                    ViewState["ClientName"] = null;
                }
                else if (SearchPanel1.Usertype == 9)
                {
                    whereclaus = "Usertype = 9  ";
                    ViewState["ClientName"] = null;
                }
                else if (SearchPanel1.Usertype == 10)
                {
                    whereclaus = "Usertype = 10  ";
                    ViewState["ClientName"] = null;
                }
                else
                {
                    whereclaus = "Usertype In (7,9,10)";
                    ViewState["ClientName"] = null;
                }


            }
            ViewState["Where"] = whereclaus;
            ViewState["Where2"] = null;
            ViewState["CurrentPage"] = 0;


            FilterGridWithHeader(Convert.ToString(ViewState["Where"]), Convert.ToString(ViewState["Order"]), Convert.ToString(ViewState["ClientName"]));
            //tuser = um.GetUserDetails(whereclaus, orderby);//DataRepository.UsersProvider.GetPaged(whereclaus, orderby, 0, int.MaxValue, out totalcount);
            //lblTotalBooking.Text = tuser.Count.ToString();
            //if (SearchPanel1.Company != "")
            //{
            //    ApplyPaging();
            //    grdViewBooking.DataSource = tuser.Where(a => a.UserDetailsCollection.FirstOrDefault() != null ? (a.UserDetailsCollection.FirstOrDefault().CompanyName != null ? a.UserDetailsCollection.FirstOrDefault().CompanyName.ToLower().StartsWith(SearchPanel1.Company.ToLower()) : false) : false).ToList();
            //    lblTotalBooking.Text = tuser.Where(a => a.UserDetailsCollection.FirstOrDefault() != null ? (a.UserDetailsCollection.FirstOrDefault().CompanyName != null ? a.UserDetailsCollection.FirstOrDefault().CompanyName.ToLower().StartsWith(SearchPanel1.Company.ToLower()) : false) : false).Count().ToString();
            //    grdViewBooking.DataBind();


            //}
            //else
            //{
            //    ApplyPaging();
            //    grdViewBooking.DataSource = tuser;
            //    grdViewBooking.DataBind();

            //}


        }
        #endregion

        #region BindGrid
        /// <summary>
        /// Bind grid with filter condition
        /// </summary>
        /// <param name="wherec">This is the string value which contains value of Viewstate Where condition</param>
        /// <param name="order">This is the string value which contains value of Viewstate Order condition</param>
        public void FilterGridWithHeader(string wherec, string order, string clientNameStarts)
        {
            try
            {
                int totalcount = 0;
                string whereclaus = wherec;
                string whereclauseforHotel = wherec;
                string orderby = order;
                if (ViewState["Where2"] != null)
                {
                    if (!string.IsNullOrEmpty(ViewState["Where2"].ToString().Trim()))
                    {
                        if (!string.IsNullOrEmpty(whereclauseforHotel.Trim()))
                        {
                            whereclauseforHotel += " and ";
                        }
                        whereclauseforHotel += ViewState["Where2"];
                    }
                }
                string orderbydate = "CreatedDate DESC";
                tuser = um.GetUserDetails(whereclauseforHotel, orderbydate);
                //DataRepository.UsersProvider.GetPaged(whereclaus, orderby, 0, int.MaxValue, out totalcount);

                if (SearchPanel1.Company != "")
                {
                    ApplyPaging();
                    if (ViewState["companysearch"] != null)
                    {
                        string namecomp = ViewState["companysearch"].ToString();
                        grdViewBooking.DataSource = tuser.Where(a => a.UserDetailsCollection.FirstOrDefault() != null ? (a.UserDetailsCollection.FirstOrDefault().CompanyName != null ? ((a.UserDetailsCollection.FirstOrDefault().CompanyName.ToLower().StartsWith(SearchPanel1.Company.ToLower())) && (a.UserDetailsCollection.FirstOrDefault().CompanyName.ToLower().StartsWith(namecomp.ToLower()))) : false) : false).ToList();
                        lblTotalBooking.Text = tuser.Where(a => a.UserDetailsCollection.FirstOrDefault() != null ? (a.UserDetailsCollection.FirstOrDefault().CompanyName != null ? ((a.UserDetailsCollection.FirstOrDefault().CompanyName.ToLower().StartsWith(SearchPanel1.Company.ToLower())) && (a.UserDetailsCollection.FirstOrDefault().CompanyName.ToLower().StartsWith(namecomp.ToLower()))) : false) : false).Count().ToString();
                        grdViewBooking.PageIndex = Convert.ToInt32(ViewState["CurrentPage"] == null ? "0" : ViewState["CurrentPage"]);
                        grdViewBooking.DataBind();
                        ApplyPaging();
                    }
                    else
                    {
                        grdViewBooking.DataSource = tuser.Where(a => a.UserDetailsCollection.FirstOrDefault() != null ? (a.UserDetailsCollection.FirstOrDefault().CompanyName != null ? a.UserDetailsCollection.FirstOrDefault().CompanyName.ToLower().StartsWith(SearchPanel1.Company.ToLower()) : false) : false).ToList();
                        lblTotalBooking.Text = tuser.Where(a => a.UserDetailsCollection.FirstOrDefault() != null ? (a.UserDetailsCollection.FirstOrDefault().CompanyName != null ? a.UserDetailsCollection.FirstOrDefault().CompanyName.ToLower().StartsWith(SearchPanel1.Company.ToLower()) : false) : false).Count().ToString();
                        grdViewBooking.PageIndex = Convert.ToInt32(ViewState["CurrentPage"] == null ? "0" : ViewState["CurrentPage"]);
                        grdViewBooking.DataBind();
                        ApplyPaging();
                    }



                }
                else
                {

                    grdViewBooking.DataSource = tuser;
                    lblTotalBooking.Text = tuser.Count.ToString();
                    grdViewBooking.PageIndex = Convert.ToInt32(ViewState["CurrentPage"] == null ? "0" : ViewState["CurrentPage"]);
                    grdViewBooking.DataBind();
                    ApplyPaging();
                }

                //BindAllHotel(THotel);
                //drpHotelName.SelectedValue = selectedHotelName;


            }
            catch (Exception ex)
            {
                //logger.Error(ex);
            }
        }

        /// <summary>
        /// Alphabat paging applied using this methods
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void PageChange(object sender, EventArgs e)
        {
            try
            {
                int totalcount = 0;
                //string whereclaus = String.Empty;
                string orderby = String.Empty;
                DropDownList drpUserType = SearchPanel1.FindControl("drpUserType") as DropDownList;
                HtmlAnchor anc = (sender as HtmlAnchor);
                var d = anc.InnerHtml;
                //ViewState["SearchAlpha"] = d;
                if ((SearchPanel1.Username != "") && (SearchPanel1.Company != "") && (SearchPanel1.Usertype == 1))
                {
                    if (d.Trim() == "all")
                    {
                        whereclaus = UsersColumn.LastName + " LIKE '" + SearchPanel1.Username + "%' and Usertype In(7,9,10) ";
                        ViewState["SearchAlpha"] = d;
                    }
                    else
                    {
                        whereclaus = UsersColumn.LastName + " LIKE '" + d + "%' and " + UsersColumn.LastName + " LIKE '" + SearchPanel1.Username + "%' and  Usertype In(7,9,10) ";
                        ViewState["SearchAlpha"] = d;
                    }
                }
                else if ((SearchPanel1.Username != "") && (SearchPanel1.Company != "") && (SearchPanel1.Usertype == 7))
                {
                    if (d.Trim() == "all")
                    {
                        whereclaus = UsersColumn.LastName + " LIKE '" + SearchPanel1.Username + "%' and Usertype=7 ";
                        ViewState["SearchAlpha"] = d;
                    }
                    else
                    {
                        whereclaus = UsersColumn.LastName + " LIKE '" + d + "%' and " + UsersColumn.LastName + " LIKE '" + SearchPanel1.Username + "%' and  Usertype=7 ";
                        ViewState["SearchAlpha"] = d;
                    }
                }
                else if ((SearchPanel1.Username != "") && (SearchPanel1.Company != "") && (SearchPanel1.Usertype == 9))
                {
                    if (d.Trim() == "all")
                    {
                        whereclaus = UsersColumn.LastName + " LIKE '" + SearchPanel1.Username + "%' and Usertype=9 ";
                        ViewState["SearchAlpha"] = d;
                    }
                    else
                    {
                        whereclaus = UsersColumn.LastName + " LIKE '" + d + "%' and " + UsersColumn.LastName + " LIKE '" + SearchPanel1.Username + "%' and Usertype=9 ";
                        ViewState["SearchAlpha"] = d;
                    }
                }
                else if ((SearchPanel1.Username != "") && (SearchPanel1.Company != "") && (SearchPanel1.Usertype == 10))
                {
                    if (d.Trim() == "all")
                    {
                        whereclaus = UsersColumn.LastName + " LIKE '" + SearchPanel1.Username + "%' and Usertype=10 ";
                        ViewState["SearchAlpha"] = d;
                    }
                    else
                    {
                        whereclaus = UsersColumn.LastName + " LIKE '" + d + "%' and " + UsersColumn.LastName + " LIKE '" + SearchPanel1.Username + "%' and  Usertype=10 ";
                        ViewState["SearchAlpha"] = d;
                    }
                }

                else if ((SearchPanel1.Username != "") && (SearchPanel1.Usertype == 1))
                {
                    if (d.Trim() == "all")
                    {
                        whereclaus = UsersColumn.LastName + " LIKE '" + SearchPanel1.Username + "%'  and Usertype In(7,9,10) ";
                        ViewState["SearchAlpha"] = d;
                    }
                    else
                    {
                        whereclaus = UsersColumn.LastName + " LIKE '" + d + "%' and " + UsersColumn.LastName + " LIKE '" + SearchPanel1.Username + "%'  and  Usertype In(7,9,10) ";
                        ViewState["SearchAlpha"] = d;
                    }
                }
                else if ((SearchPanel1.Username != "") && (SearchPanel1.Usertype == 7))
                {
                    if (d.Trim() == "all")
                    {
                        whereclaus = UsersColumn.LastName + " LIKE '" + SearchPanel1.Username + "%'  and Usertype=7";
                        ViewState["SearchAlpha"] = d;
                    }
                    else
                    {
                        whereclaus = UsersColumn.LastName + " LIKE '" + d + "%' and " + UsersColumn.LastName + " LIKE '" + SearchPanel1.Username + "%'  and  Usertype=7";
                        ViewState["SearchAlpha"] = d;
                    }
                }
                else if ((SearchPanel1.Username != "") && (SearchPanel1.Usertype == 9))
                {
                    if (d.Trim() == "all")
                    {
                        whereclaus = UsersColumn.LastName + " LIKE '" + SearchPanel1.Username + "%'  and Usertype=9";
                        ViewState["SearchAlpha"] = d;
                    }
                    else
                    {
                        whereclaus = UsersColumn.LastName + " LIKE '" + d + "%' and " + UsersColumn.LastName + " LIKE '" + SearchPanel1.Username + "%'  and Usertype=9";
                        ViewState["SearchAlpha"] = d;
                    }
                }
                else if ((SearchPanel1.Username != "") && (SearchPanel1.Usertype == 10))
                {
                    if (d.Trim() == "all")
                    {
                        whereclaus = UsersColumn.LastName + " LIKE '" + SearchPanel1.Username + "%'  and Usertype=10";
                        ViewState["SearchAlpha"] = d;
                    }
                    else
                    {
                        whereclaus = UsersColumn.LastName + " LIKE '" + d + "%' and " + UsersColumn.LastName + " LIKE '" + SearchPanel1.Username + "%'  and  Usertype=10";
                        ViewState["SearchAlpha"] = d;
                    }
                }
                else if ((SearchPanel1.Company != "") && (SearchPanel1.Usertype == 1))
                {
                    if (d.Trim() == "all")
                    {
                        whereclaus = "Usertype In (7,9,10)";
                        ViewState["SearchAlpha"] = d;
                    }
                    else
                    {
                        whereclaus = "Usertype In (7,9,10)";
                        ViewState["companysearch"] = d;
                        ViewState["SearchAlpha"] = d;
                    }
                }
                else if ((SearchPanel1.Company != "") && (SearchPanel1.Usertype == 7))
                {
                    if (d.Trim() == "all")
                    {
                        whereclaus = "Usertype=7";
                        ViewState["SearchAlpha"] = d;
                    }
                    else
                    {
                        whereclaus = "Usertype=7";
                        ViewState["companysearch"] = d;
                        ViewState["SearchAlpha"] = d;
                    }
                }
                else if ((SearchPanel1.Company != "") && (SearchPanel1.Usertype == 9))
                {
                    if (d.Trim() == "all")
                    {
                        whereclaus = "Usertype=9";
                        ViewState["SearchAlpha"] = d;
                    }
                    else
                    {
                        whereclaus = "Usertype=9";
                        ViewState["companysearch"] = d;
                        ViewState["SearchAlpha"] = d;
                    }
                }
                else if ((SearchPanel1.Company != "") && (SearchPanel1.Usertype == 10))
                {
                    if (d.Trim() == "all")
                    {
                        whereclaus = "Usertype=10";
                        ViewState["SearchAlpha"] = d;
                    }
                    else
                    {
                        whereclaus = "Usertype=10";
                        ViewState["companysearch"] = d;
                        ViewState["SearchAlpha"] = d;
                    }
                }
                else
                {
                    if (d.Trim() == "all")
                    {
                        string selectedType = drpUserType.SelectedItem.Value;
                        if (selectedType == "1")
                        {
                            whereclaus = UsersColumn.LastName + " LIKE '%' and " + UsersColumn.Usertype + " in (7,9,10)";
                        }
                        else
                        {
                            whereclaus = UsersColumn.LastName + " LIKE '%' and Usertype=" + drpUserType.SelectedItem.Value;
                        }

                        ViewState["SearchAlpha"] = d;
                    }
                    else
                    {
                        string selectedType = drpUserType.SelectedItem.Value;
                        if (selectedType == "1")
                        {
                            whereclaus = UsersColumn.LastName + " LIKE '" + d + "%' and " + UsersColumn.Usertype + " in (7,9,10) ";
                        }
                        else
                        {
                            whereclaus = UsersColumn.LastName + " LIKE '" + d + "%' and Usertype=" + drpUserType.SelectedItem.Value;
                        }
                        ViewState["SearchAlpha"] = d;
                    }
                }

                ViewState["Where2"] = whereclaus;
                ViewState["CurrentPage"] = 0;

                divmessage.Style.Add("display", "none");
                divmessage.Attributes.Add("class", "error");
                //divMessageOnGridHeader.Style.Add("display", "none");
                //divMessageOnGridHeader.Attributes.Add("class", "error");
                if (ViewState["Where"] == null)
                {
                    FilterGridWithHeader(ViewState["Where"].ToString(), ViewState["Order"].ToString(), string.Empty);
                }
                else
                {
                    FilterGridWithHeader(ViewState["Where2"].ToString(), ViewState["Order"].ToString(), string.Empty);
                }
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
            }
        }




        /// <summary>
        /// Apply Numaric paging to the grid
        /// </summary>
        private void ApplyPaging()
        {
            try
            {
                GridViewRow row = grdViewBooking.TopPagerRow;
                if (row != null)
                {
                    PlaceHolder ph;
                    LinkButton lnkPaging;
                    LinkButton lnkPrevPage;
                    LinkButton lnkNextPage;
                    lnkPrevPage = new LinkButton();
                    lnkPrevPage.CssClass = "pre";
                    lnkPrevPage.Width = Unit.Pixel(73);
                    lnkPrevPage.CommandName = "Page";
                    lnkPrevPage.CommandArgument = "prev";
                    ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkPrevPage);
                    if (grdViewBooking.PageIndex == 0)
                    {
                        lnkPrevPage.Enabled = false;

                    }
                    for (int i = 1; i <= grdViewBooking.PageCount; i++)
                    {
                        lnkPaging = new LinkButton();
                        if (ViewState["CurrentPage"] != null)
                        {
                            if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                            {
                                lnkPaging.CssClass = "pag2";
                            }
                            else
                            {
                                lnkPaging.CssClass = "pag";
                            }
                        }
                        else
                        {
                            if (i == 1)
                            {
                                lnkPaging.CssClass = "pag2";
                            }
                            else
                            {
                                lnkPaging.CssClass = "pag";
                            }
                        }
                        lnkPaging.Width = Unit.Pixel(16);
                        lnkPaging.Text = i.ToString();
                        lnkPaging.CommandName = "Page";
                        lnkPaging.CommandArgument = i.ToString();
                        if (i == grdViewBooking.PageIndex + 1)
                            ph = (PlaceHolder)row.FindControl("ph");
                        ph.Controls.Add(lnkPaging);
                    }
                    lnkNextPage = new LinkButton();
                    lnkNextPage.CssClass = "nex";
                    lnkNextPage.Width = Unit.Pixel(42);
                    lnkNextPage.CommandName = "Page";
                    lnkNextPage.CommandArgument = "next";
                    ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkNextPage);
                    ph = (PlaceHolder)row.FindControl("ph");
                    if (grdViewBooking.PageIndex == grdViewBooking.PageCount - 1)
                    {
                        lnkNextPage.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
            }
        }
        #endregion

        #region PageIndexChanging
        /// <summary>
        /// PageIndex Changing of grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdViewBooking_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {

                grdViewBooking.PageIndex = e.NewPageIndex;
                ViewState["CurrentPage"] = e.NewPageIndex;
                FilterGridWithHeader(Convert.ToString(ViewState["Where"]), Convert.ToString(ViewState["Order"]), Convert.ToString(ViewState["ClientName"]));
                divmessage.Style.Add("display", "none");
                divmessage.Attributes.Add("class", "error");

            }
            catch (Exception ex)
            {
                //logger.Error(ex);
            }
        }
        #endregion

        #region RowDataBound
        /// <summary>
        /// RowDataBound of grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdViewBooking_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
            {
                try
                {


                    GridViewRow gr = e.Row;

                    Label lblUserType = (Label)gr.FindControl("lblUserType");
                    Label lbllCreatedDt = (Label)gr.FindControl("lbllCreatedDt");
                    Label lbllastLogin = (Label)gr.FindControl("lbllastLogin");
                    Label lblName = (Label)gr.FindControl("lblName");
                    Label lblFirstName = (Label)gr.FindControl("lblFirstName");
                    Label lblcity = (Label)gr.FindControl("lblcity");
                    LinkButton LnkIsactive = (LinkButton)gr.FindControl("LnkIsactive");
                    Label lblnew = (Label)gr.FindControl("lblnew");
                    Label lblEmailId = (Label)gr.FindControl("lblEmailId");
                    Label lblnobooking = (Label)gr.FindControl("lblnobooking");
                    Label userid = (Label)gr.FindControl("lblRefNo");
                    Label lblcompany = (Label)gr.FindControl("lblcompany");
                    Label lblcontact = (Label)gr.FindControl("lblcontact");
                    Users u = e.Row.DataItem as Users;
                    UserDetails ud = u.UserDetailsCollection.FirstOrDefault();
                    Booking b = u.BookingCollectionGetByCreatorId.FirstOrDefault();
                    int countBooking = u.BookingCollectionGetByCreatorId.FindAll(a => a.CreatorId == u.UserId && u.UserId == a.CreatorId && a.BookType == 0).Count();
                    lblnobooking.Text = countBooking.ToString();
                    userid.Text = u.UserId.ToString();
                    lblEmailId.Text = u.EmailId.Trim().Replace("'", "");
                    lblUserType.Text = u.Usertype.ToString();
                    lbllCreatedDt.Text = string.Format("{0:dd MMM yyyy}", u.CreatedDate);
                    lbllastLogin.Text = string.Format("{0:dd MMM yyyy}", u.LastLogin);
                    lblName.Text = u.LastName;
                    lblFirstName.Text = u.FirstName;
                    LnkIsactive.Text = u.IsActive.ToString();

                    if (ud != null)
                    {

                        lblcompany.Text = string.IsNullOrEmpty(ud.CompanyName) ? "" : ud.CompanyName;
                        lblcity.Text = string.IsNullOrEmpty(ud.CityName) ? "" : ud.CityName;
                        lblcontact.Text = string.IsNullOrEmpty(ud.Phone) ? "" : ud.Phone;
                    }


                    if (LnkIsactive.Text == "True")
                    {
                        LnkIsactive.Text = "Deactive";
                    }
                    else if (LnkIsactive.Text == "False")
                    {
                        LnkIsactive.Text = "Active";
                    }


                    if (lbllCreatedDt.Text == "")
                    {
                        lbllCreatedDt.Text = "N/A";
                    }
                    if (lbllastLogin.Text == "")
                    {
                        lbllastLogin.Text = "N/A";
                    }
                    if (lblName.Text == "")
                    {
                        lblName.Text = "N/A";
                    }
                    if (lblFirstName.Text == "")
                    {
                        lblFirstName.Text = "N/A";
                    }
                    if (lblcity.Text == "")
                    {
                        lblcity.Text = "N/A";
                    }
                    if (lblcompany.Text == "")
                    {
                        lblcompany.Text = "N/A";
                    }
                    if (lblcontact.Text == "")
                    {
                        lblcontact.Text = "N/A";
                    }
                    if (lblUserType.Text == "7")
                    {
                        lblUserType.Text = "Meet 2 Solution";
                    }
                    else if (lblUserType.Text == "9")
                    {
                        lblUserType.Text = "Company";
                    }
                    else if (lblUserType.Text == "10")
                    {
                        lblUserType.Text = "Private";
                    }

                    if (LnkIsactive.Text == "Active")
                    {
                        lblnew.Text = "New";
                    }
                    else if (LnkIsactive.Text == "Deactive")
                    {
                        lblnew.Text = "Active";
                    }
                    //if (lblUserType.Text == "Company")
                    //{
                    //    LnkIsactive.Enabled = true;
                    //}
                    //else
                    //{
                    //    LnkIsactive.Enabled = false;
                    //}

                }

                catch (Exception EX)
                {
                    lblTotalBooking.Text = EX.ToString();
                }
            }
        }
        #endregion

        #region Redirect to Add User
        protected void lnkAddNew_Click(object sender, EventArgs e)
        {

            Session["task"] = "register";
            if (SearchPanel1.Usertype == 9)
            {
                Session["registerType"] = Convert.ToInt32(Usertype.Company);
                Session["superadmin"] = "SuperAdmin";
                Response.Redirect("~/Registration.aspx");
            }
            else if (SearchPanel1.Usertype == 10)
            {
                Session["registerType"] = Convert.ToInt32(Usertype.privateuser);
                Session["superadmin"] = "SuperAdmin";
                Response.Redirect("~/Registration.aspx");
            }
            else if (SearchPanel1.Usertype == 7)
            {
                Session["registerType"] = Convert.ToInt32(Usertype.Agency);
                Session["superadmin"] = "SuperAdmin";
                Response.Redirect("~/Registration.aspx");
                //divmessage.Visible = true;
                //status = "Available Soon (Next version).";
                //divmessage.Style.Add("display", "block");
                //divmessage.Attributes.Add("class", "succesfuly");
                //divmessage.InnerHtml = status;

            }
            else
            {
                Session["registerType"] = Convert.ToInt32(Usertype.privateuser);
                Session["superadmin"] = "SuperAdmin";
                Response.Redirect("~/Registration.aspx");

            }



        }
        #endregion

        #region Link Active OR Deactive
        protected void LnkIsactive_Click(object sender, EventArgs e)
        {

            LinkButton lnk = (LinkButton)sender;
            foreach (GridViewRow gr in grdViewBooking.Rows)
            {
                if (gr.FindControl("LnkIsactive") == lnk)
                {
                    Label lblRefNo = (Label)gr.FindControl("lblRefNo");
                    string userId = lblRefNo.Text;
                    break;
                }
            }
            long userid = Convert.ToInt64(lnk.CommandArgument);
            string where = "Userid='" + userid + "'";
            Users userupdate = new Users();
            userupdate = um.GetUserByID(userid);
            //UserDetails userdetails = userupdate.UserDetailsCollection.FirstOrDefault();
            int count1 = 0;
            string whereUserdtl = "UserId=" + userupdate.UserId;
            TList<UserDetails> userdetails = DataRepository.UserDetailsProvider.GetPaged(whereUserdtl, string.Empty, 0, int.MaxValue, out count1);
            status = ud.update(userupdate);
            if (status == "Information updated successfully.")
            {
                if (lnk.Text == "Active")
                {
                    divmessage.Style.Add("display", "none");
                    divmessage.Attributes.Add("class", "error");
                    if (userupdate.Usertype == 7)
                    {
                        if (um.ChangeuserActiveStatus(Convert.ToInt32(userupdate.UserId), true))
                        {
                            string genereatedPassword;
                            genereatedPassword = RandomPassword.Generate(8, 10);
                            if (updateUserPassword(Convert.ToInt32(userupdate.UserId), genereatedPassword, userupdate.EmailId))
                            {
                                EmailConfig eConfig = em.GetByName("Registration Mail Agency");
                                if (eConfig.IsActive)
                                {
                                    SendMails mail = new SendMails();
                                    mail.FromEmail = ConfigurationManager.AppSettings["ClientSupportEmailID"];
                                    mail.ToEmail = userupdate.EmailId;
                                    mail.Bcc = ConfigurationManager.AppSettings["WatchEMailID"];
                                    //string bodymsg = File.ReadAllText(Server.MapPath("~/EmailTemplets/registrationEmailTemplate.htm"));
                                    string bodymsg = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
                                    EmailConfigMapping emap = new EmailConfigMapping();
                                    if (userdetails.Count > 0)
                                    {
                                        if (userdetails[0].LanguageId == 0)
                                        {
                                            emap = null;
                                        }
                                        else
                                        {
                                            emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == userdetails[0].LanguageId).FirstOrDefault();
                                        }
                                    }
                                    if (emap != null)
                                    {
                                        bodymsg = bodymsg.Replace("@CONTENTS", emap.EmailContents);
                                    }
                                    else
                                    {
                                        emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                                        bodymsg = bodymsg.Replace("@CONTENTS", emap.EmailContents);
                                    }
                                    EmailValueCollection objEmailValues = new EmailValueCollection();
                                    mail.Subject = "Meet 2 Solution Registration";
                                    string strActivationLink = "activation.aspx?activate=" + userupdate.UserId;
                                    foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForAgencyRegistration(userupdate.FirstName + " " + userupdate.LastName, userupdate.EmailId, genereatedPassword, ConfigurationManager.AppSettings["Sender"]))
                                    {
                                        bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                                    }
                                    mail.Body = bodymsg;
                                    mail.SendMail();

                                    status = "User Activated successfully. Activation Mail sent to the User";
                                    divmessage.Style.Add("display", "block");
                                    divmessage.Attributes.Add("class", "succesfuly");
                                    divmessage.InnerHtml = status;


                                    //mail.SendMail();

                                }
                                else
                                {
                                    divmessage.InnerHtml = "The mail functionality is not working yet.";
                                    divmessage.Attributes.Add("class", "error");
                                    divmessage.Style.Add("display", "block");

                                }

                            }
                            else
                            {
                                status = "User Activated successfully.";

                                divmessage.Style.Add("display", "block");
                                divmessage.Attributes.Add("class", "succesfuly");
                                divmessage.InnerHtml = status;
                            }
                        }
                        else
                        {
                            divmessage.InnerHtml = "Information could not be saved.Please contact Administrator";
                            divmessage.Attributes.Add("class", "error");
                            divmessage.Style.Add("display", "block");
                        }

                    }
                    else
                    {
                        if (um.ChangeuserActiveStatus(Convert.ToInt32(userupdate.UserId), true))
                        {
                            status = "User Activated successfully.";
                            divmessage.Style.Add("display", "block");
                            divmessage.Attributes.Add("class", "succesfuly");
                            divmessage.InnerHtml = status;
                        }
                        else
                        {
                            divmessage.Attributes.Add("class", "error");
                            divmessage.Style.Add("display", "block");
                            divmessage.InnerHtml = status;
                        }

                    }

                    ViewState["Where1"] = ViewState["Where"].ToString();

                    //ViewState["CurrentPage"] = 0;
                    //divMessageOnGridHeader.Style.Add("display", "none");
                    //divMessageOnGridHeader.Attributes.Add("class", "error");
                    if (ViewState["Where1"] == null)
                    {
                        ViewState["SearchAlpha"] = ViewState["SearchAlpha"];
                        ViewState["Where3"] = ViewState["Where2"];
                        //ViewState["CurrentPage"] = 0;
                        FilterGridWithHeader(ViewState["Where3"].ToString(), ViewState["Order"].ToString(), string.Empty);

                    }
                    else
                    {

                        ViewState["SearchAlpha"] = ViewState["SearchAlpha"];
                        ViewState["ClientName"] = null;

                        FilterGridWithHeader(Convert.ToString(ViewState["Where1"]), Convert.ToString(ViewState["Order"]), Convert.ToString(ViewState["ClientName"]));

                    }


                }
                else if (lnk.Text == "Deactive")
                {
                    if (um.ChangeuserActiveStatus(Convert.ToInt32(userupdate.UserId), false))
                    {
                        divmessage.Style.Add("display", "none");
                        divmessage.Attributes.Add("class", "error");
                        ViewState["Where1"] = ViewState["Where"].ToString();
                        if (ViewState["Where1"] == null)
                        {
                            ViewState["SearchAlpha"] = ViewState["SearchAlpha"];
                            ViewState["Where3"] = ViewState["Where2"];
                            //ViewState["CurrentPage"] = 0;
                            FilterGridWithHeader(ViewState["Where3"].ToString(), ViewState["Order"].ToString(), string.Empty);

                        }
                        else
                        {

                            ViewState["SearchAlpha"] = ViewState["SearchAlpha"];
                            ViewState["ClientName"] = null;

                            FilterGridWithHeader(Convert.ToString(ViewState["Where1"]), Convert.ToString(ViewState["Order"]), Convert.ToString(ViewState["ClientName"]));

                        }
                        status = "User Deactivated successfully.";
                        divmessage.Style.Add("display", "block");
                        divmessage.Attributes.Add("class", "succesfuly");
                        divmessage.InnerHtml = status;
                    }
                    else
                    {
                        divmessage.InnerHtml = "Information could not be saved.Please contact Administrator";
                        divmessage.Attributes.Add("class", "error");
                        divmessage.Style.Add("display", "block");
                        ViewState["Where1"] = ViewState["Where"].ToString();
                        if (ViewState["Where1"] == null)
                        {
                            ViewState["SearchAlpha"] = ViewState["SearchAlpha"];
                            ViewState["Where3"] = ViewState["Where2"];
                            //ViewState["CurrentPage"] = 0;
                            FilterGridWithHeader(ViewState["Where3"].ToString(), ViewState["Order"].ToString(), string.Empty);

                        }
                        else
                        {

                            ViewState["SearchAlpha"] = ViewState["SearchAlpha"];
                            ViewState["ClientName"] = null;

                            FilterGridWithHeader(Convert.ToString(ViewState["Where1"]), Convert.ToString(ViewState["Order"]), Convert.ToString(ViewState["ClientName"]));

                        }
                    }


                }

            }
        }
        #endregion

        public bool updateUserPassword(Int32 UserId, string password, string email)
        {

            return PasswordManager.UpdateClientPasswordByEmail(email, password);

        }

        #region Link Update User
        protected void lnkmodify_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow gvr in grdViewBooking.Rows)
            {
                CheckBox chk = (CheckBox)gvr.FindControl("chkField");
                if (chk.Checked == true)
                {
                    Label lbt = (Label)gvr.FindControl("lblRefNo");
                    Label lblUserType = (Label)gvr.FindControl("lblUserType");
                    string userId = lbt.Text;
                    string usertype = lblUserType.Text;
                    Session["CurrentUserID"] = userId.ToString();
                    Session["CurrentUserID"] = userId.ToString();
                    if (usertype == "Meet 2 Solution")
                    {
                        Session["registerType"] = Convert.ToInt32(Usertype.Agency);
                        Session["superadmin"] = "SuperAdmin";
                        Session["task"] = "Edit";
                        Response.Redirect("~/Registration.aspx");
                        //divmessage.Visible = true;
                        //status = "Available Soon (Next version) .";
                        //divmessage.Style.Add("display", "block");
                        //divmessage.Attributes.Add("class", "succesfuly");
                        //divmessage.InnerHtml = status;
                    }
                    else if (usertype == "Company")
                    {
                        Session["registerType"] = Convert.ToInt32(Usertype.Company);
                        Session["superadmin"] = "SuperAdmin";
                        Session["task"] = "Edit";
                        Response.Redirect("~/Registration.aspx");
                    }
                    else if (usertype == "Private")
                    {
                        Session["registerType"] = Convert.ToInt32(Usertype.privateuser);
                        Session["superadmin"] = "SuperAdmin";
                        Session["task"] = "Edit";
                        Response.Redirect("~/Registration.aspx");
                    }
                    //Session["registerType"] = Convert.ToInt32(Usertype.Agency);
                    break;

                }


                //LinkButton lblRefNo = (LinkButton)gvr.FindControl("lblRefNo");
                //string userId = lblRefNo.Text;
                //}
            }

            //string t = Session["test"].ToString();

        }
        #endregion

        #region Checkbox select Ony Select One
        public static int iVal = -1;
        public static int jVal = -2;
        protected void chkField_CheckedChanged(object sender, EventArgs e)
        {
            int i;
            CheckBox chk;
            for (i = 0; i <= grdViewBooking.Rows.Count - 1; i++)
            {
                chk = (CheckBox)grdViewBooking.Rows[i].FindControl("chkField");
                if (chk.Checked == true)
                {
                    if (jVal != i)
                    {
                        iVal = i;
                        jVal = i;
                        if (jVal == i)
                        {
                            break;
                        }
                    }
                }
            }
            for (i = 0; i <= grdViewBooking.Rows.Count - 1; i++)
            {
                chk = (CheckBox)grdViewBooking.Rows[i].FindControl("chkField");
                if (i == iVal)
                {
                    chk.Checked = true;
                }
                else
                {
                    chk.Checked = false;
                }
            }
        }
        #endregion

        #region Delete User
        protected void lnkdelete_Click(object sender, EventArgs e)
        {
            bool isStatus = false;
            foreach (GridViewRow gvr in grdViewBooking.Rows)
            {
                CheckBox chk = (CheckBox)gvr.FindControl("chkField");
                if (chk.Checked == true)
                {
                    Label lblReferenceNumber = (Label)gvr.FindControl("lblRefNo");
                    string userId = lblReferenceNumber.Text;
                    if (!string.IsNullOrEmpty(userId))
                    {
                        Users objUser = um.GetUserByID(Convert.ToInt64(userId));
                        UserDetails objUserDetail = um.getUserbyid(Convert.ToInt64(userId)).FirstOrDefault();
                        if (objUser != null)
                        {
                            objUser.IsRemoved = true;
                            objUser.IsActive = false;

                            if (objUserDetail != null)
                            {
                                objUserDetail.IsDeleted = true;
                            }
                            //Call Delete Function...
                            if (!ud.DeleteUser(objUser, objUserDetail))
                            {
                                divmessage.Attributes.Add("class", "error");
                                divmessage.Style.Add("display", "block");
                                divmessage.InnerHtml = "Error please contact to technical supprt.";
                                isStatus = false;
                                return;
                            }
                            else
                            {
                                isStatus = true;
                            }
                        }
                    }
                    else
                    {
                        divmessage.Attributes.Add("class", "error");
                        divmessage.Style.Add("display", "block");
                        divmessage.InnerHtml = "Error please contact to technical supprt.";

                    }
                    //Label lbt = (Label)gvr.FindControl("lblRefNo");
                    //string userId = lbt.Text;
                    //long deleteuserbyid = Convert.ToInt64(userId);
                    //UserDetails userdetailid = new UserDetails();
                    //userdetailid = um.getUserbyid(deleteuserbyid).FirstOrDefault();
                    //string status;
                    //status = ud.Delete(userdetailid);
                    //if (status == "Information Deleted successfully.")
                    //{
                    //    //bindhotel();
                    //    ViewState["Where1"] = ViewState["Where"].ToString();

                    //    //ViewState["CurrentPage"] = e.NewPageIndex;
                    //    //divMessageOnGridHeader.Style.Add("display", "none");
                    //    //divMessageOnGridHeader.Attributes.Add("class", "error");
                    //    if (ViewState["Where1"] == null)
                    //    {
                    //        ViewState["SearchAlpha"] = ViewState["SearchAlpha"];
                    //        ViewState["Where3"] = ViewState["Where2"];
                    //        //ViewState["CurrentPage"] = 0;
                    //        FilterGridWithHeader(ViewState["Where3"].ToString(), ViewState["Order"].ToString(), string.Empty);

                    //    }
                    //    else
                    //    {

                    //        ViewState["SearchAlpha"] = ViewState["SearchAlpha"];
                    //        ViewState["ClientName"] = null;

                    //        FilterGridWithHeader(Convert.ToString(ViewState["Where1"]), Convert.ToString(ViewState["Order"]), Convert.ToString(ViewState["ClientName"]));

                    //    }
                    //    status = "User deleted successfully.";
                    //    divmessage.Style.Add("display", "block");
                    //    divmessage.Attributes.Add("class", "succesfuly");
                    //    divmessage.InnerHtml = status;
                    //}
                    //else
                    //{

                    //    //bindhotel();
                    //    ViewState["Where1"] = ViewState["Where"].ToString();

                    //    //ViewState["CurrentPage"] = 0;
                    //    //divMessageOnGridHeader.Style.Add("display", "none");
                    //    //divMessageOnGridHeader.Attributes.Add("class", "error");
                    //    if (ViewState["Where1"] == null)
                    //    {
                    //        ViewState["SearchAlpha"] = ViewState["SearchAlpha"];
                    //        ViewState["Where3"] = ViewState["Where2"];
                    //        //ViewState["CurrentPage"] = 0;
                    //        FilterGridWithHeader(ViewState["Where3"].ToString(), ViewState["Order"].ToString(), string.Empty);

                    //    }
                    //    else
                    //    {

                    //        ViewState["SearchAlpha"] = ViewState["SearchAlpha"];
                    //        ViewState["ClientName"] = null;

                    //        FilterGridWithHeader(Convert.ToString(ViewState["Where1"]), Convert.ToString(ViewState["Order"]), Convert.ToString(ViewState["ClientName"]));

                    //    }
                    //    divmessage.Attributes.Add("class", "error");
                    //    divmessage.Style.Add("display", "block");
                    //    divmessage.InnerHtml = status;
                    //}


                    //break;

                }
            }
            if (isStatus)
            {
                divmessage.Style.Add("display", "block");
                divmessage.Attributes.Add("class", "succesfuly");
                divmessage.InnerHtml = "User deleted successfully.";
            }
            else
            {
                divmessage.Attributes.Add("class", "error");
                divmessage.Style.Add("display", "block");
                divmessage.InnerHtml = "Error please contact to technical support.";
            }
            if (ViewState["Where"] != null)
            {
                ViewState["Where1"] = ViewState["Where"].ToString();
                if (ViewState["Where1"] == null)
                {
                    ViewState["SearchAlpha"] = ViewState["SearchAlpha"];
                    ViewState["Where3"] = ViewState["Where2"];
                    FilterGridWithHeader(ViewState["Where3"].ToString(), ViewState["Order"].ToString(), string.Empty);
                }
                else
                {
                    ViewState["SearchAlpha"] = ViewState["SearchAlpha"];
                    ViewState["ClientName"] = null;
                    FilterGridWithHeader(Convert.ToString(ViewState["Where1"]), Convert.ToString(ViewState["Order"]), Convert.ToString(ViewState["ClientName"]));
                }
            }
            else
            {
                FilterGridWithHeader(string.Empty, string.Empty, string.Empty);
            }
        }
        #endregion
    }