﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using System.IO;
using System.Configuration;
using log4net;
using log4net.Config;
using System.Xml;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Net.Mail;
using System.Net.Mime;
using Microsoft.Practices.EnterpriseLibrary.Data;
#endregion

public partial class Operator_InvoiceGenerate : System.Web.UI.Page
{
    #region variable declaration
    FinanceInvoice objFinance = new FinanceInvoice();
    VList<Viewbookingrequest> vlistreq;
    ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
    HotelInfo ObjHotelinfo = new HotelInfo();
    ILog logger = log4net.LogManager.GetLogger(typeof(Operator_InvoiceGenerate));
    HotelManager objHotel = new HotelManager();
    ClientContract objClient = new ClientContract();
    ManageOthers objOthers = new ManageOthers();
    ManageWhiteLabel objWhite = new ManageWhiteLabel();
    //EmailConfigManager em = new EmailConfigManager();
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl + "/";
        }
    }
    #endregion

    #region PageLoad
    /// <summary>
    /// On page load we manage all the containts and Apply paging if there is a post back.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        #region For Invoice Generation
        if (Convert.ToString(Request.QueryString["HotelId"]) != null && Convert.ToString(Request.QueryString["Id"]) != null)
        {
            lblTotalValue.Text = "0.00";
            lblTotalRequest.Text = "0.00";
            lblTotalBooking.Text = "0.00";
            lblFinalTotal.Text = "0.00";
            lblTotalVat.Text = "0.00";

            string whereclause = "HotelId='" + Request.QueryString["HotelId"] + "' and " + "Id='" + Request.QueryString["Id"] + "' ";
            TList<Invoice> lstInvoice = objFinance.GetInvoicebyCondition(whereclause, String.Empty);

            DateTime fromDate = (DateTime)lstInvoice[0].DateFrom;
            DateTime toDate = (DateTime)lstInvoice[0].DateTo;
            DateTime invoiceDate = (DateTime)lstInvoice[0].DueDate;
            lblFrom.Text = fromDate.ToString("dd/MM/yyyy");
            lblTo.Text = toDate.ToString("dd/MM/yyyy");
            lblInvoiceDate.Text = invoiceDate.ToString("dd/MM/yyyy");
            lblInvoiceNumber.Text = lstInvoice[0].InvoiceNumber;
            lblUniqueNumber.Text = lstInvoice[0].InvoiceNumber;

            //Get Hotel client details
            Hotel hotel = ObjHotelinfo.GetHotelByHotelID(Convert.ToInt32(Request.QueryString["HotelId"]));
            lblClientAddress.Text = hotel.HotelAddress;
            lblClientPhone.Text = hotel.PhoneNumber;
            lblClientCode.Text = hotel.ContractId;
            lblVenueName.Text = hotel.Name;
            TList<UserDetails> userdtls = ObjHotelinfo.GetUserDetails(Convert.ToInt32(hotel.ClientId));
            if (userdtls.Count > 0)
            {
                lblClientVat.Text = userdtls[0].VatNo;
            }

            // For Booking
            string whereclaus = "hotelid in (" + lstInvoice[0].HotelId + ") and booktype = 0 and DepartureDate between '" + lstInvoice[0].DateFrom + "' and '" + lstInvoice[0].DateTo + "' and requeststatus=4";
            //string whereclaus = "hotelid in (113) and booktype = 0 and DepartureDate between '" + vl.DateFrom + "' and '" + vl.DateTo + "'";
            string orderby = ViewbookingrequestColumn.DepartureDate + " DESC";
            VList<Viewbookingrequest> vlistBooking = objViewBooking_Hotel.Bindgrid(whereclaus, orderby);
            grvBooking.DataSource = vlistBooking;
            grvBooking.DataBind();

            //For Request
            string whereclausreq = "hotelid in (" + lstInvoice[0].HotelId + ") and Booktype in (1,2) and DepartureDate between '" + lstInvoice[0].DateFrom + "' and '" + lstInvoice[0].DateTo + "' and requeststatus=4 and IsComissionDone=1";
            //string whereclausreq = "hotelid in (113) and booktype = 1 and DepartureDate between '" + vl.DateFrom + "' and '" + vl.DateTo + "'";
            string orderbyreq = ViewbookingrequestColumn.DepartureDate + " DESC";
            VList<Viewbookingrequest> vlistreq = objViewBooking_Hotel.Bindgrid(whereclausreq, orderbyreq);
            grvRequest.DataSource = vlistreq;
            grvRequest.DataBind();
            Divdetails.Visible = true;

            if (vlistBooking.Count == 0 && vlistreq.Count == 0)
            {
                divTotal.Visible = false;
            }
            else
            {
                divTotal.Visible = true;
            }

            lblTotalValue.Text = String.Format("{0:#,##,##0.00}", Convert.ToDecimal(lblTotalBooking.Text) + Convert.ToDecimal(lblTotalRequest.Text));
            if (ObjHotelinfo.GetCountryByID(Convert.ToInt32(hotel.CountryId)) == "Belgium")
            {
                lblTotalVat.Text = String.Format("{0:#,##,##0.00}", Convert.ToDecimal(lblTotalValue.Text) * 21 / 100);
                lblFinalTotal.Text = Convert.ToString(Convert.ToDecimal(lblTotalValue.Text) + Convert.ToDecimal(lblTotalVat.Text));
            }
            else
            {
                lblFinalTotal.Text = Convert.ToString(Convert.ToDecimal(lblTotalValue.Text) + Convert.ToDecimal(lblTotalVat.Text));
            }


            #region bookingpdf
            ContentPlaceHolder1_Image1.Src = SiteRootPath + "Uploads/InvoiceFile/header.jpg";
            ContentPlaceHolder1_Image1.Width = 1170;
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            PrepareDivForExport(Divdetails);
            Divdetails.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
            //ContentPlaceHolder1_Image1.Src = ConfigurationManager.AppSettings["FilePath"] + "InvoiceFile/header.jpg";            
            var dirPath = Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "InvoiceFile/");
            var filePath = dirPath + lstInvoice[0].InvoiceNumber + ".pdf";

            FileStream fileStream = new FileStream(filePath, FileMode.Create);
            PdfWriter.GetInstance(pdfDoc, fileStream);
            pdfDoc.Open();
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            htmlparser.Parse(sr);
            fileStream.Flush();
            pdfDoc.Close();
            fileStream.Close();
            fileStream.Dispose();


            //System.IO.File.Create(filePath);           
            Invoice objInv = objFinance.GetInvoiceByID(Convert.ToInt32(lstInvoice[0].Id));
            objInv.InvoiceFile = lstInvoice[0].InvoiceNumber + ".pdf";
            objInv.TotalAmount = Convert.ToDecimal(lblFinalTotal.Text);
            objFinance.UpdateInvoice(objInv);
            #endregion

            #region Sending mail for autometic invoice
            //TList<Others> othVerify = objFinance.GetInvoiceVerify();
            //if (Convert.ToByte(othVerify[0].InvoiceVerify) == 1)
            //{
            //    string hotelEmail = ObjHotelinfo.getHotelEmail(Convert.ToInt64(Request.QueryString["HotelId"]));
            //    string secondaryEmail = ObjHotelinfo.getSecondaryEmail(Convert.ToInt64(Request.QueryString["HotelId"]));
            //    if (hotelEmail != null)
            //    {
            //        bool sentStatus = sendMail(hotelEmail, lstInvoice[0].InvoiceFile);
            //        if (sentStatus == true)
            //        {
            //            Invoice objInv = objFinance.GetInvoiceByID(Convert.ToInt32(lstInvoice[0].Id));
            //            objInv.InvoiceSent = true;
            //            objFinance.UpdateInvoice(objInv);
            //        }
            //    }
            //    if (secondaryEmail != null)
            //        sendMail(secondaryEmail, lstInvoice[0].InvoiceFile);
            //}
            #endregion
        }
        #endregion
    }
    #endregion




    #region Get Booking/Request Information after clicking on Invoice Number
    /// <summary>
    //Start Get all information of booking details when clicking on Invoice Number and calculate net amount as per booking id
    /// </summary> 
    protected void grvBooking_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblId = (Label)e.Row.FindControl("lblId");
                // the replace check image wherver renvue is done
                Booking bookcomm = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(lblId.Text));


                #region Calculate commission as per date difference
                Label lblBookingDate = (Label)e.Row.FindControl("lblBookingDate");
                Label lblDepartureDt = (Label)e.Row.FindControl("lblDepartureDt");
                HiddenField hdnArrivalDate = (HiddenField)e.Row.FindControl("hdnArrivalDate");
                Label lblCommision = (Label)e.Row.FindControl("lblCommision");

                DateTime fromDate = new DateTime(Convert.ToInt32(lblBookingDate.Text.Split('/')[2]), Convert.ToInt32(lblBookingDate.Text.Split('/')[1]), Convert.ToInt32(lblBookingDate.Text.Split('/')[0]));
                DateTime todate = new DateTime(Convert.ToInt32(hdnArrivalDate.Value.Split('/')[2]), Convert.ToInt32(hdnArrivalDate.Value.Split('/')[1]), Convert.ToInt32(hdnArrivalDate.Value.Split('/')[0]), 23, 59, 59);
                TimeSpan ts = todate - fromDate;
                int days = ts.Days;
                #endregion

                #region Dynamic set invoice
                if (bookcomm.ChannelBy != null)
                {
                    lblCommision.Text = Convert.ToString(Convert.ToInt32(objWhite.GetWhiteLabelByID(Convert.ToInt64(bookcomm.ChannelId)).Commission));
                }
                else
                {
                    VList<ViewForSetInvoice> objView = objOthers.GetCommissionDetailsForHotelId(Convert.ToInt32(bookcomm.HotelId));
                    if (objView.Count > 0)
                    {
                        for (int i = 0; i < objView.Count; i++)
                        {
                            if (days >= objView[i].FromDay && days <= objView[i].ToDay)
                            {
                                lblCommision.Text = objView[i].CommissionPercentage.ToString();
                                break;
                            }
                        }
                    }
                    else
                    {
                        TList<InvoiceCommission> objComm = objOthers.GetInvoiceCommission();
                        for (int i = 0; i < objComm.Count; i++)
                        {
                            if (days >= objComm[i].FromDay && days <= objComm[i].ToDay)
                            {
                                lblCommision.Text = objComm[i].DefaultCommission.ToString();
                                break;
                            }
                            else
                            {
                                lblCommision.Text = "8";
                                break;
                            }
                        }
                    }
                }
                #endregion

                #region Calculate the final value i.e. confirmed value multiply lmmr commission
                Label lblFinalValue = (Label)e.Row.FindControl("lblFinalValue");
                Label lblRevenueAmount = (Label)e.Row.FindControl("lblRevenueAmount");

                
                if (lblRevenueAmount.Text != "" && lblRevenueAmount.Text != null)
                {
                    lblFinalValue.Text = String.Format("{0:#,##,##0.00}", Math.Round((Convert.ToDecimal(lblCommision.Text) * Convert.ToDecimal(lblRevenueAmount.Text) / 100), 2));
                }
                else
                {
                    lblFinalValue.Text = "0";
                }
                #endregion

                lblTotalBooking.Text = String.Format("{0:#,##,##0.00}", Math.Round((Convert.ToDecimal(lblFinalValue.Text) + Convert.ToDecimal(lblTotalBooking.Text)), 2));

                bookcomm.Accomodation = Convert.ToInt32(lblCommision.Text);
                DataRepository.BookingProvider.Update(bookcomm);


            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    /// <summary>
    //Start Get all information of request details when clicking on Invoice Number and calculate net amount as per request id
    /// </summary> 
    protected void grvRequest_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblId = (Label)e.Row.FindControl("lblId");
                // the replace check image wherver renvue is done
                Booking bookcomm = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(lblId.Text));

                #region Calculate commission as per date difference
                Label lblBookingDate = (Label)e.Row.FindControl("lblBookingDate");
                Label lblDepartureDt = (Label)e.Row.FindControl("lblDepartureDt");
                HiddenField hdnArrivalDate = (HiddenField)e.Row.FindControl("hdnArrivalDate");
                Label lblCommision = (Label)e.Row.FindControl("lblCommision");

                DateTime fromDate = new DateTime(Convert.ToInt32(lblBookingDate.Text.Split('/')[2]), Convert.ToInt32(lblBookingDate.Text.Split('/')[1]), Convert.ToInt32(lblBookingDate.Text.Split('/')[0]));
                DateTime todate = new DateTime(Convert.ToInt32(hdnArrivalDate.Value.Split('/')[2]), Convert.ToInt32(hdnArrivalDate.Value.Split('/')[1]), Convert.ToInt32(hdnArrivalDate.Value.Split('/')[0]), 23, 59, 59);
                TimeSpan ts = todate - fromDate;
                int days = ts.Days;
                #endregion

                #region Dynamic set invoice
                if (bookcomm.ChannelBy != null)
                {
                    lblCommision.Text = Convert.ToString(Convert.ToInt32(objWhite.GetWhiteLabelByID(Convert.ToInt64(bookcomm.ChannelId)).Commission));
                }
                else
                {
                    VList<ViewForSetInvoice> objView = objOthers.GetCommissionDetailsForHotelId(Convert.ToInt32(bookcomm.HotelId));
                    if (objView.Count > 0)
                    {
                        for (int i = 0; i < objView.Count; i++)
                        {
                            if (days >= objView[i].FromDay && days <= objView[i].ToDay)
                            {
                                lblCommision.Text = objView[i].CommissionPercentage.ToString();
                                break;
                            }
                        }
                    }
                    else
                    {
                        TList<InvoiceCommission> objComm = objOthers.GetInvoiceCommission();
                        for (int i = 0; i < objComm.Count; i++)
                        {
                            if (days >= objComm[i].FromDay && days <= objComm[i].ToDay)
                            {
                                lblCommision.Text = objComm[i].DefaultCommission.ToString();
                                break;
                            }
                            else
                            {
                                lblCommision.Text = "8";
                                break;
                            }
                        }
                    }
                }
                #endregion

                #region Calculate the final value i.e. confirmed value multiply lmmr commission
                Label lblFinalValue = (Label)e.Row.FindControl("lblFinalValue");
                Label lblRevenueAmount = (Label)e.Row.FindControl("lblRevenueAmount");
                if (lblRevenueAmount.Text != "" && lblRevenueAmount.Text != null)
                {
                    lblFinalValue.Text = String.Format("{0:#,##,##0.00}", Math.Round((Convert.ToDecimal(lblCommision.Text) * Convert.ToDecimal(lblRevenueAmount.Text) / 100), 2));
                }
                else
                {
                    lblFinalValue.Text = "0";
                }

                #endregion
                lblTotalRequest.Text = String.Format("{0:#,##,##0.00}", Math.Round((Convert.ToDecimal(lblFinalValue.Text) + Convert.ToDecimal(lblTotalRequest.Text)), 2));

                bookcomm.Accomodation = Convert.ToInt32(lblCommision.Text);
                DataRepository.BookingProvider.Update(bookcomm);
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    //Rendor Method
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    #region PrepareGridViewForExportDIV
    /// <summary>
    /// method to PrepareGridViewForExport a div
    /// </summary>
    private void PrepareDivForExport(Control gv)
    {
        try
        {

            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {

                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].GetType() == typeof(System.Web.UI.HtmlControls.HtmlAnchor))
                {

                    l.Text = (gv.Controls[i] as System.Web.UI.HtmlControls.HtmlAnchor).InnerText;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(DropDownList))
                {

                    l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(GridView))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    // gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Repeater))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Panel))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Table))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }


                else if (gv.Controls[i].GetType() == typeof(CheckBox))
                {

                    l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(HyperLink))
                {

                    l.Text = (gv.Controls[i] as HyperLink).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                //else if (gv.Controls[i].GetType() == typeof(Label))
                //{

                //    l.Text = (gv.Controls[i] as Label).Text;

                //    gv.Controls.Remove(gv.Controls[i]);

                //    gv.Controls.AddAt(i, l);

                //}
                else if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.Image))
                {

                    // l.Text = (gv.Controls[i] as Image).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareDivForExport(gv.Controls[i]);

                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
        //PrepareGridViewForExport();
        //PrepareGridViewForExport();

    }


    #endregion

    #region Mail sending Function
    //public bool sendMail(string hotelname, string email, string attachment)
    //{
    //    string previousMonth = DateTime.Now.AddMonths(-1).ToString("MMMM") + "-" + DateTime.Now.AddMonths(-1).ToString("yyyy");
    //    EmailConfig eConfig = em.GetByName("Operator invoice");
    //    if (eConfig.IsActive)
    //    {
    //        SendMails mail = new SendMails();
    //        mail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
    //        mail.ToEmail = email;
    //        mail.Attachment.Add(new System.Net.Mail.Attachment(Server.MapPath((ConfigurationManager.AppSettings["FilePath"].ToString() + "InvoiceFile/")) + attachment));
    //        //string bodymsg = File.ReadAllText(Server.MapPath("~/EmailTemplets/OperatorInvoice.htm"));
    //        string bodymsg = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
    //        EmailConfigMapping emap2 = new EmailConfigMapping();
    //        emap2 = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
    //        bodymsg = bodymsg.Replace("@CONTENTS", emap2.EmailContents);

    //        EmailValueCollection objEmailValues = new EmailValueCollection();
    //        mail.Subject = "Invoice";
    //        foreach (KeyValuePair<string, string> strKey in objEmailValues.InvoiceEmail(hotelname, email, ConfigurationManager.AppSettings["Sender"], previousMonth))
    //        {
    //            bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
    //        }
    //        mail.Body = bodymsg;
    //        string success = mail.SendMail();
    //        if (success == "Send Mail")
    //            return true;
    //        return false;
    //    }
    //    else
    //    {
    //        return false;
    //    }
    //}
    #endregion

}
