﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Operator/Operator.master" AutoEventWireup="true"
    CodeFile="SearchBookingRequest.aspx.cs" Inherits="Operator_ControlPanel" MaintainScrollPositionOnPostback="false" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControl/Operator/SearchPanel.ascx" TagName="SearchPanel"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/HotelUser/BookingDetails.ascx" TagName="BookingDetails"
    TagPrefix="uc2" %>
<%@ Register Src="../UserControl/HotelUser/RequestDetails.ascx" TagName="RequestDetails"
    TagPrefix="uc4" %>
<%@ Register Src="../UserControl/Operator/BookingRequestCountBoard.ascx" TagName="BookingRequestCountBoard"
    TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:SearchPanel ID="SearchPanel1" runat="server" />
    
    <div><asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label></div>
      
    <!--main-body start -->
    <div class="operator-mainbody" id="divlist" runat="server">
        <!--contract-list start -->
        <div class="contract-list">
            <div class="contract-list-right">
                <ul runat="server" id="AlphaList">
                    <li id="Li1" runat="server"><a href="#" class="select" runat="server" id="all" onserverclick="PageChange">
                        all</a></li>
                    <li id="Li3" runat="server"><a href="#" runat="server" id="b" onserverclick="PageChange">
                        b</a></li>
                    <li id="Li4" runat="server"><a href="#" runat="server" id="c" onserverclick="PageChange">
                        c</a></li>
                    <li id="Li5" runat="server"><a href="#" runat="server" id="d" onserverclick="PageChange">
                        d</a></li>
                    <li id="Li6" runat="server"><a href="#" runat="server" id="e" onserverclick="PageChange">
                        e</a></li>
                    <li id="Li7" runat="server"><a href="#" runat="server" id="f" onserverclick="PageChange">
                        f</a></li>
                    <li id="Li8" runat="server"><a href="#" runat="server" id="g" onserverclick="PageChange">
                        g</a></li>
                    <li id="Li9" runat="server"><a href="#" runat="server" id="h" onserverclick="PageChange">
                        h</a></li>
                    <li id="Li10" runat="server"><a href="#" runat="server" id="i" onserverclick="PageChange">
                        i</a></li>
                    <li id="Li11" runat="server"><a href="#" runat="server" id="j" onserverclick="PageChange">
                        j</a></li>
                    <li id="Li12" runat="server"><a href="#" runat="server" id="k" onserverclick="PageChange">
                        k</a></li>
                    <li id="Li13" runat="server"><a href="#" runat="server" id="l" onserverclick="PageChange">
                        l</a></li>
                    <li id="Li14" runat="server"><a href="#" runat="server" id="m" onserverclick="PageChange">
                        m</a></li>
                    <li id="Li15" runat="server"><a href="#" runat="server" id="n" onserverclick="PageChange">
                        n</a></li>
                    <li id="Li16" runat="server"><a href="#" runat="server" id="o" onserverclick="PageChange">
                        o</a></li>
                    <li id="Li17" runat="server"><a href="#" runat="server" id="p" onserverclick="PageChange">
                        p</a></li>
                    <li id="Li18" runat="server"><a href="#" runat="server" id="q" onserverclick="PageChange">
                        q</a></li>
                    <li id="Li19" runat="server"><a href="#" runat="server" id="r" onserverclick="PageChange">
                        r</a></li>
                    <li id="Li20" runat="server"><a href="#" runat="server" id="s" onserverclick="PageChange">
                        s</a></li>
                    <li id="Li21" runat="server"><a href="#" runat="server" id="t" onserverclick="PageChange">
                        t</a></li>
                    <li id="Li22" runat="server"><a href="#" runat="server" id="u" onserverclick="PageChange">
                        u</a></li>
                    <li id="Li23" runat="server"><a href="#" runat="server" id="v" onserverclick="PageChange">
                        v</a></li>
                    <li id="Li24" runat="server"><a href="#" runat="server" id="w" onserverclick="PageChange">
                        w</a></li>
                    <li id="Li25" runat="server"><a href="#" runat="server" id="x" onserverclick="PageChange">
                        x</a></li>
                    <li id="Li26" runat="server"><a href="#" runat="server" id="y" onserverclick="PageChange">
                        y</a></li>
                    <li id="Li27" runat="server"><a href="#" runat="server" id="z" onserverclick="PageChange">
                        z</a></li>
                    <li id="Li2" runat="server"><a href="#" runat="server" id="a" onserverclick="PageChange">
                        a</a></li>
                </ul>
            </div>
        </div>
        <div class="pageing-operator-new" id="DivBookingText" runat="server">
            
        </div>
        <asp:Panel ID="pnlbooking" runat="server">
            <!-- end contract-list-->
            <div class="pageing-operator">
                <div style="float: right;" id="pageing">
                </div>
            </div>
            <div class="search-booking-rowmain clearfix" style="width: 960px">
                <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#98BCD6">
                    <tr bgcolor="#CCD8D8">
                        <td valign="top" width="15%">
                            City
                        </td>
                        <td valign="top" width="30%">
                            Facility / Hotal name
                        </td>
                        <td valign="top" width="55%">
                            Number of booking
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="grdViewHotelList" runat="server" Width="100%" border="0" CellPadding="5"
                    CellSpacing="1" AutoGenerateColumns="false" DataKeyNames="HotelId" OnRowDataBound="grdViewHotelList_RowDataBound"
                    GridLines="None" ShowHeader="false" ShowFooter="false" OnPageIndexChanging="grdViewHotelList_PageIndexChanging"
                    BackColor="#98BCD6" RowStyle-BackColor="#FFFFFF" OnRowCommand="grdViewHotelList_RowCommand"
                    PageSize="2" AllowPaging="true" EmptyDataRowStyle-BackColor="White" EmptyDataRowStyle-HorizontalAlign="Center">
                    <Columns>
                        <asp:TemplateField ItemStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblCity"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="30%">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkHotelName" runat="server" CommandName="ViewBooking" CommandArgument='<%# Eval("HotelId") %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="55%">
                            <ItemTemplate>
                                <asp:Label runat="server" Text="Label" ID="lblNumberOfBooking"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <table>
                            <tr>
                                <td colspan="3" align="center">
                                    <b>No record found !</b>
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                    <PagerStyle HorizontalAlign="Right" BackColor="White" CssClass="displayNone" />
                    <PagerTemplate>
                        <div id="Paging" style="width: 100%; display: none;">
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                        </div>
                    </PagerTemplate>
                </asp:GridView>
                <%--<div class="pageing-operator">
                <div class="pageing-operator-right">
                    <div class="pagination">
                        <ul class="pager">
                            <li class="perv"><a style="width: auto;" href="#">Prevous</a></li>
                            <li><a href="#">1</a> </li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a class="select" href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li class="perv"><a class="select" href="#">Next</a></li>
                        </ul>
                    </div>
                </div>
            </div>--%>
            </div>
            <br />
            <div>
                <asp:Panel ID="pnlBookingGrid" runat="server" Visible="false">
                    <asp:GridView ID="grdViewBooking" runat="server" Width="80%" border="0" CellPadding="5"
                        CellSpacing="1" AutoGenerateColumns="false" OnRowDataBound="grdViewBooking_RowDataBound"
                        BackColor="#98BCD6" RowStyle-BackColor="#FFFFFF" AlternatingRowStyle-BackColor="#E3F0F1"
                        CssClass="controlPanelbooking" GridLines="None" DataKeyNames="Id" OnRowCommand="grdViewBooking_RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="Ref no" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkRefno" runat="server" CommandName="ViewBookingDetail" CommandArgument='<%#Eval("Id") %>'></asp:LinkButton>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:LinkButton ID="lnkRefno" runat="server"></asp:LinkButton>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Meeting room" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblMeetingroom" runat="server" Text="Label"></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblMeetingroom" runat="server" Text="Label"></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Booking date" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblBookingDate" runat="server" Text="Label"></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblBookingDate" runat="server" Text="Label"></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Company" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblCompany" runat="server" Text="Label"></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblCompany" runat="server" Text="Label"></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Contact name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkContactName" runat="server" CommandName="ViewContactDatail"
                                        CommandArgument='<%#Eval("Id") %>'></asp:LinkButton>
                                    <%--<asp:Label ID="lblContactName" runat="server" Text="Label"></asp:Label>--%>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:LinkButton ID="lnkContactName" runat="server" CommandName="ViewContactDatail"
                                        CommandArgument='<%#Eval("Id") %>'></asp:LinkButton>
                                    <%--<asp:Label ID="lblContactName" runat="server" Text="Label"></asp:Label>--%>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Start" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblStart" runat="server" Text="Label"></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblStart" runat="server" Text="Label"></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="End" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblEnd" runat="server" Text="Label"></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblEnd" runat="server" Text="Label"></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblTotal" runat="server" Text="Label"></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblTotal" runat="server" Text="Label"></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
                <br />
                <div id="divbookingdetails" align="left" runat="server" visible="false" style="margin-left: 109px;">
                    <h1 class="new" style="font: left; width: 760px">
                        Booking details</h1>
                    <div class="booking-details" id="divprint" runat="server">
                        <ul>
                            <li class="value3">
                                <div class="col9">
                                    <img src="../Images/print.png" />&nbsp; <a id="ADetails" style="cursor: pointer;"
                                        onclick="javascript:Button1_onclick('<%= Divdetails.ClientID%>');">Print</a>
                                    &bull;
                                    <img src="../Images/pdf.png" />&nbsp;
                                    <asp:LinkButton ID="lnkSavePDF" runat="server" OnClick="lnkSavePDF_Click">Save as PDF</asp:LinkButton>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div id="Divdetails" runat="server" class="meinnewbody">
                        <uc2:BookingDetails ID="ucViewBookingDetail" runat="server" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlRequest" runat="server" Visible="false">
            <!-- end contract-list-->
            <div class="pageing-operator">
                <div style="float: right;" id="PageingRequest">
                </div>
            </div>
            <div class="search-booking-rowmain clearfix" style="width: 960px">
                <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#98BCD6">
                    <tr bgcolor="#CCD8D8">
                        <td valign="top" width="15%">
                            City
                        </td>
                        <td valign="top" width="30%">
                            Facility / Hotal name
                        </td>
                        <td valign="top" width="55%">
                            Number of Requests
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="grdViewHotelListRequest" runat="server" Width="100%" border="0"
                    CellPadding="5" CellSpacing="1" AutoGenerateColumns="false" DataKeyNames="HotelId"
                    OnRowDataBound="grdViewHotelListRequest_RowDataBound" GridLines="None" ShowHeader="false"
                    ShowFooter="false" BackColor="#98BCD6" RowStyle-BackColor="#FFFFFF" OnRowCommand="grdViewHotelListRequest_RowCommand"
                    PageSize="2" AllowPaging="true" EmptyDataRowStyle-BackColor="White" EmptyDataRowStyle-HorizontalAlign="Center"
                    OnPageIndexChanging="grdViewHotelListRequest_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField ItemStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblCity"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="30%">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkHotelName" runat="server" CommandName="ViewRequest" CommandArgument='<%# Eval("HotelId") %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="55%">
                            <ItemTemplate>
                                <asp:Label runat="server" Text="Label" ID="lblNumberOfRequest"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <table>
                            <tr>
                                <td colspan="3" align="center">
                                    <b>No record found !</b>
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                    <PagerStyle HorizontalAlign="Right" BackColor="White" CssClass="displayNone" />
                    <PagerTemplate>
                        <div id="PagingRequest" style="width: 100%; display: none;">
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                        </div>
                    </PagerTemplate>
                </asp:GridView>
            </div>
            <br />
            <div>
                <asp:Panel ID="pnlRequestGrid" runat="server" Visible="false">
                    <asp:GridView ID="grdViewRequest" runat="server" Width="80%" border="0" CellPadding="5"
                        CellSpacing="1" AutoGenerateColumns="false" OnRowDataBound="grdViewRequest_RowDataBound"
                        BackColor="#98BCD6" RowStyle-BackColor="#FFFFFF" AlternatingRowStyle-BackColor="#E3F0F1"
                        CssClass="controlPanelbooking" GridLines="None" DataKeyNames="Id" OnRowCommand="grdViewRequest_RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="Ref no" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkRefno" runat="server" CommandName="ViewBookingDetail" CommandArgument='<%#Eval("Id") %>'></asp:LinkButton>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:LinkButton ID="lnkRefno" runat="server"></asp:LinkButton>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Meeting room" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblMeetingroom" runat="server" Text="Label"></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblMeetingroom" runat="server" Text="Label"></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Booking date" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblBookingDate" runat="server" Text="Label"></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblBookingDate" runat="server" Text="Label"></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Company" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblCompany" runat="server" Text="Label"></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblCompany" runat="server" Text="Label"></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Contact name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkContactName" runat="server" CommandName="ViewContactDatail"
                                        CommandArgument='<%#Eval("Id") %>'></asp:LinkButton>
                                    <%--<asp:Label ID="lblContactName" runat="server" Text="Label"></asp:Label>--%>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:LinkButton ID="lnkContactName" runat="server" CommandName="ViewContactDatail"
                                        CommandArgument='<%#Eval("Id") %>'></asp:LinkButton>
                                    <%--<asp:Label ID="lblContactName" runat="server" Text="Label"></asp:Label>--%>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Start" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblStart" runat="server" Text="Label"></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblStart" runat="server" Text="Label"></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="End" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblEnd" runat="server" Text="Label"></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblEnd" runat="server" Text="Label"></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblTotal" runat="server" Text="Label"></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblTotal" runat="server" Text="Label"></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
                <br />
                <div id="DivrequestDetail" align="left" runat="server" visible="false" style="margin-left: 109px;">
                    <h1 class="new" style="font: left; width: 760px">
                        Request details</h1>
                    <div class="booking-details" id="div3" runat="server">
                        <ul>
                            <li class="value3">
                                <div class="col9">
                                    <img src="../Images/print.png" />&nbsp; <a id="A1" style="cursor: pointer;" onclick="javascript:Button2_onclick('<%= DivdetailsRequest.ClientID%>');">
                                        Print</a> &bull;
                                    <img src="../Images/pdf.png" />&nbsp;
                                    <asp:LinkButton ID="lnkSaveRequestPdf" runat="server" OnClick="lnkSaveRequestPdf_Click">Save as PDF</asp:LinkButton>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div id="DivdetailsRequest" runat="server" class="meinnewbody">
                        <uc4:RequestDetails ID="requestDetails" runat="server" />
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <script language="javascript" type="text/javascript">


     function Button1_onclick(strid) {

            var prtContent = document.getElementById(strid);
            var WinPrint = window.open('', '', 'letf=0,top=0,width=800,height=800,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();


        }

        function Button2_onclick(strid) {

            var prtContent = document.getElementById(strid);
            var WinPrint = window.open('', '', 'letf=0,top=0,width=800,height=800,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();


        }

         function OpenContactPopUp()
          {
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                jQuery("#Contactpopup").show();
                jQuery("#Contactpopup-overlay").show();  
                return false;       
           }

           function CloseContactPopUp() {
            document.getElementsByTagName('html')[0].style.overflow = 'auto';
            document.getElementById('Contactpopup').style.display = 'none';
            document.getElementById('Contactpopup-overlay').style.display = 'none';
            return false;
        }
        jQuery(document).ready(function () {
            if (jQuery("#Paging") != undefined) {
                var inner = jQuery("#Paging").html();
                jQuery("#pageing").html(inner);
                jQuery("#Paging").html("");
            }

        });

        jQuery(document).ready(function () {
            if (jQuery("#PagingRequest") != undefined) {
                var inner = jQuery("#PagingRequest").html();
                jQuery("#PageingRequest").html(inner);
                jQuery("#PagingRequest").html("");
            }

        });

        jQuery(document).ready(function () {
                            <% if (ViewState["SearchAlpha"] != null) {%>
                                jQuery('#<%= AlphaList.ClientID %> li a').removeClass('select');
                                jQuery('#ContentPlaceHolder1_<%= ViewState["SearchAlpha"]%>').addClass('select');
                            <% }%>
                        });
                       
    </script>
    <div id="divPopUp" runat="server" style="display: none">
        <div id="Contactpopup-overlay">
        </div>
        <div id="Contactpopup">
            <div class="popup-top">
            </div>
            <div class="popup-mid">
                <div style="position: absolute; right: 4px; top: 5px;">
                    <asp:ImageButton ID="imgBtnCloaseContactPopUp" ImageUrl="~/Images/close-black.png"
                        runat="server" OnClientClick="return CloseContactPopUp();" />
                </div>
                <div class="popup-mid-inner">
                    <table cellspacing="10">
                        <tr>
                            <td>
                                <b>Contact name :</b>
                            </td>
                            <td>
                                <asp:Label ID="lblContactName" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Phone no. </b>
                            </td>
                            <td>
                                <asp:Label ID="lblPhoneNumber" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Email : </b>
                            </td>
                            <td>
                                <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Arrival date : </b>
                            </td>
                            <td>
                                <asp:Label ID="lblFrom" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Departure date : </b>
                            </td>
                            <td>
                                <asp:Label ID="lblTo" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Duration :</b>
                            </td>
                            <td>
                                <asp:Label ID="lblDuration" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Contact address :</b>
                            </td>
                            <td>
                                <asp:Label ID="lbladdress" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="popup-bottom">
            </div>
        </div>
    </div>
</asp:Content>
