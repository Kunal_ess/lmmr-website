﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LMMR.Business;
using LMMR.Entities;
using System.Text.RegularExpressions;
using log4net;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Web.UI.DataVisualization.Charting;

public partial class Operator_ControlPanel : System.Web.UI.Page
{
    #region Variable
    ILog logger = log4net.LogManager.GetLogger(typeof(Operator_ControlPanel));
    HotelManager objHotelManager = new HotelManager();
    ViewBooking_Hotel objViewHotelBooikng = new ViewBooking_Hotel();
    VList<Viewbookingrequest> Vlistreq = new VList<Viewbookingrequest>();
    VList<Viewbookingrequest> vlistFoCount = new VList<Viewbookingrequest>();
    HotelInfo objHotelInfo = new HotelInfo();
    Createbooking objBooking = null;
    CreateRequest objRequest = null;
    VList<Viewbookingrequest> objBookingView = new VList<Viewbookingrequest>();
    VList<Viewbookingrequest> objBookingViewTemp = new VList<Viewbookingrequest>();
    VList<Viewbookingrequest> objRequestView = new VList<Viewbookingrequest>();
    VList<Viewbookingrequest> objRequestViewTemp = new VList<Viewbookingrequest>();
    string hotelid = "0";
    Viewstatistics objViewstatistics = new Viewstatistics();
    List<Fetchdata> fdata = new List<Fetchdata>();
    #endregion
    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        SearchPanel1.SearchButtonClick += new EventHandler(Search_Button);
        SearchPanel1.CancelButtonClick += new EventHandler(Cancel_Button);
        if (!IsPostBack)
        {
            ViewState["SearchAlpha"] = "all";
            ManageDashBoard();
            BindLstViewHotel("MONTH");
            pnlRequest.Visible = false;
            ViewState["IsBooking"] = "YES";

        }
        else
        {
            //FirePrviousCommand();
        }
        ApplyPaging();
        ApplyPagingRequest();
        ApplyPagingForBookingList();
        ApplyPagingForRequestList();

    }
    #endregion
    #region search
    protected void Cancel_Button(object sender, EventArgs e)
    {
        Session["command"] = null;
        if (ViewState["IsBooking"].ToString() == "YES")
        {
            ViewState["SearchAlpha"] = "all";
            ViewState["CurrentPage"] = null;
            ManageDashBoard();
            DivBookingText.Visible = true;
            if (ViewState["Period"] != null)
            {
                BindLstViewHotel(ViewState["Period"].ToString());
            }
            else
            {
                BindLstViewHotel("MONTH");
            }
            ApplyPaging();
            trTotalRequest.BgColor = "#ffffff";
            trNoAction.BgColor = "#ffffff";
            trOnlineRequest.BgColor = "#ffffff";
            trTentative.BgColor = "#ffffff";
            divbookingdetails.Visible = false;
        }
        else
        {

            ViewState["SearchAlpha"] = "all";
            grdViewHotelListRequest.PageIndex = 0;
            ViewState["CurrentPageRequest"] = null;
            ManageDashBoard();
            if (ViewState["RequestType"] != null)
            {
                BindLstViewHotelRequest(ViewState["RequestType"].ToString());
            }
            else
            {
                BindLstViewHotelRequest("ONLINE");
            }
            //ApplyPagingRequest();
            trTodayBooking.BgColor = "#ffffff";
            trCurrentMonth.BgColor = "#ffffff";
            trCurrentYearBooking.BgColor = "#ffffff";
            DivrequestDetail.Visible = false;
        }

    }
    protected void Search_Button(object sender, EventArgs e)
    {
        Session["command"] = null;
        if (ViewState["IsBooking"].ToString() != "YES")
        {
            grdViewHotelListRequest.PageIndex = 0;
            ViewState["CurrentPageRequest"] = null;

            if (ViewState["RequestType"] != null)
            {
                ManageDashBoard();
                BindLstViewHotelRequest(ViewState["RequestType"].ToString());
            }
            else
            {
                ManageDashBoard();
                BindLstViewHotelRequest("ONLINE");
            }
            //ApplyPagingRequest();
        }
        else
        {
            ViewState["CurrentPage"] = null;

            if (ViewState["Period"] != null)
            {
                ManageDashBoard();
                BindLstViewHotel(ViewState["Period"].ToString());
            }
            else
            {
                ManageDashBoard();
                BindLstViewHotel("MONTH");
            }
            //ApplyPaging();
        }

    }
    #endregion
    #region Booking
    #region Function
    public VList<Viewbookingrequest> GetFinalBookingData(string strPeriod)
    {
        if (strPeriod == "YEAR")
        {
            Vlistreq = GetByYear();
            lblTotalBooking.Text = Session["TotalBookingByYear"].ToString();
            lblFromDate.Visible = true;
        }
        else if (strPeriod == "TODAY")
        {
            Vlistreq = GetByToday();
            lblTotalBooking.Text = Session["TotalBookingByToday"].ToString();
            lblFromDate.Visible = false;
        }
        else if (strPeriod == "MONTH")
        {
            Vlistreq = GetByMonth();
            lblTotalBooking.Text = Session["TotalBookingByMonth"].ToString();
            lblFromDate.Visible = true;
        }
        Session["ListOfHotelBooking"] = Vlistreq;
        Session["ListOfHotelTemp"] = Vlistreq;
        return Vlistreq;
    }
    private void ApplyPaging()
    {
        try
        {
            pnlBookingGrid.Visible = false;
            GridViewRow row = grdViewHotelList.TopPagerRow;
            if (row != null)
            {
                PlaceHolder ph;
                LinkButton lnkPaging;
                LinkButton lnkPrevPage;
                LinkButton lnkNextPage;
                lnkPrevPage = new LinkButton();
                lnkPrevPage.CssClass = "pre";
                lnkPrevPage.Width = Unit.Pixel(73);
                lnkPrevPage.CommandName = "Page";
                lnkPrevPage.CommandArgument = "prev";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPrevPage);
                if (grdViewHotelList.PageIndex == 0)
                {
                    lnkPrevPage.Enabled = false;

                }

                if (ViewState["CurrentPage"] == null)
                {
                    ViewState["CurrentPage"] = "0";
                }

                for (int i = 1; i <= grdViewHotelList.PageCount; i++)
                {
                    lnkPaging = new LinkButton();
                    if (ViewState["CurrentPage"] != null)
                    {
                        if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    else
                    {
                        if (i == 1)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    lnkPaging.Width = Unit.Pixel(16);
                    lnkPaging.Text = i.ToString();
                    lnkPaging.CommandName = "Page";
                    lnkPaging.CommandArgument = i.ToString();
                    if (i == grdViewHotelList.PageIndex + 1)
                        ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkPaging);
                }
                lnkNextPage = new LinkButton();
                lnkNextPage.CssClass = "nex";
                lnkNextPage.Width = Unit.Pixel(42);
                lnkNextPage.CommandName = "Page";
                lnkNextPage.CommandArgument = "next";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkNextPage);
                ph = (PlaceHolder)row.FindControl("ph");
                if (grdViewHotelList.PageIndex == grdViewHotelList.PageCount - 1)
                {
                    lnkNextPage.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    private void ApplyPagingForBookingList()
    {
        try
        {
            GridViewRow row = grdViewBooking.TopPagerRow;
            if (row != null)
            {
                PlaceHolder ph;
                LinkButton lnkPaging;
                LinkButton lnkPrevPage;
                LinkButton lnkNextPage;
                lnkPrevPage = new LinkButton();
                lnkPrevPage.CssClass = "pre";
                lnkPrevPage.Width = Unit.Pixel(73);
                lnkPrevPage.CommandName = "Page";
                lnkPrevPage.CommandArgument = "prev";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPrevPage);
                if (grdViewBooking.PageIndex == 0)
                {
                    lnkPrevPage.Enabled = false;

                }

                if (ViewState["CurrentPageBookingList"] == null)
                {
                    ViewState["CurrentPageBookingList"] = "0";
                }

                for (int i = 1; i <= grdViewBooking.PageCount; i++)
                {
                    lnkPaging = new LinkButton();
                    if (ViewState["CurrentPageBookingList"] != null)
                    {
                        if (Convert.ToInt32(ViewState["CurrentPageBookingList"]) + 1 == i)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    else
                    {
                        if (i == 1)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    lnkPaging.Width = Unit.Pixel(16);
                    lnkPaging.Text = i.ToString();
                    lnkPaging.CommandName = "Page";
                    lnkPaging.CommandArgument = i.ToString();
                    if (i == grdViewBooking.PageIndex + 1)
                        ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkPaging);
                }
                lnkNextPage = new LinkButton();
                lnkNextPage.CssClass = "nex";
                lnkNextPage.Width = Unit.Pixel(42);
                lnkNextPage.CommandName = "Page";
                lnkNextPage.CommandArgument = "next";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkNextPage);
                ph = (PlaceHolder)row.FindControl("ph");
                if (grdViewBooking.PageIndex == grdViewBooking.PageCount - 1)
                {
                    lnkNextPage.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    private void ApplyPagingForRequestList()
    {
        try
        {
            GridViewRow row = grdViewRequest.TopPagerRow;
            if (row != null)
            {
                PlaceHolder ph;
                LinkButton lnkPaging;
                LinkButton lnkPrevPage;
                LinkButton lnkNextPage;
                lnkPrevPage = new LinkButton();
                lnkPrevPage.CssClass = "pre";
                lnkPrevPage.Width = Unit.Pixel(73);
                lnkPrevPage.CommandName = "Page";
                lnkPrevPage.CommandArgument = "prev";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPrevPage);
                if (grdViewRequest.PageIndex == 0)
                {
                    lnkPrevPage.Enabled = false;

                }

                if (ViewState["CurrentPageRequestList"] == null)
                {
                    ViewState["CurrentPageRequestList"] = "0";
                }

                for (int i = 1; i <= grdViewRequest.PageCount; i++)
                {
                    lnkPaging = new LinkButton();
                    if (ViewState["CurrentPageRequestList"] != null)
                    {
                        if (Convert.ToInt32(ViewState["CurrentPageRequestList"]) + 1 == i)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    else
                    {
                        if (i == 1)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    lnkPaging.Width = Unit.Pixel(16);
                    lnkPaging.Text = i.ToString();
                    lnkPaging.CommandName = "Page";
                    lnkPaging.CommandArgument = i.ToString();
                    if (i == grdViewRequest.PageIndex + 1)
                        ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkPaging);
                }
                lnkNextPage = new LinkButton();
                lnkNextPage.CssClass = "nex";
                lnkNextPage.Width = Unit.Pixel(42);
                lnkNextPage.CommandName = "Page";
                lnkNextPage.CommandArgument = "next";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkNextPage);
                ph = (PlaceHolder)row.FindControl("ph");
                if (grdViewRequest.PageIndex == grdViewRequest.PageCount - 1)
                {
                    lnkNextPage.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    public void BindLstViewHotel(string strPeriod)
    {
        VList<Viewbookingrequest> obj = GetFinalBookingData(strPeriod);
        ViewState["GetTotalBookingCount"] = "0";
        grdViewHotelList.DataSource = obj;
        grdViewHotelList.DataBind();
        divgraph.Style.Add("display", "none");
        divlist.Style.Add("display", "block");
    }
    public void grdViewBookingBind(long intHotelID)
    {
        string whereclaus = string.Empty;

        if (ViewState["Period"] != null)
        {
            if (ViewState["Period"].ToString() == "TODAY")
            {
                DateTime Todaydate = DateTime.Today;
                string startdate = string.Format("{0:yyyy-MM-dd}", Todaydate);
                string enddate = string.Format("{0:yyyy-MM-dd}", Todaydate.AddDays(1));
                string today = string.Format("{0:dd MMM yyyy}", Todaydate);
                if (SearchPanel1.Venus != "")
                {
                    whereclaus = "HotelId='" + intHotelID + "' and booktype = 0 and BookingDate Between  '" + startdate + "' and '" + enddate + "' and HotelName Like '" + SearchPanel1.Venus + "%'";
                }
                else
                {
                    whereclaus = "HotelId='" + intHotelID + "' and booktype = 0 and BookingDate Between  '" + startdate + "' and '" + enddate + "'";
                }
            }
            else if (ViewState["Period"].ToString() == "MONTH")
            {
                DateTime baseDate = DateTime.Today;
                DateTime startDate = baseDate.AddDays(1 - baseDate.Day);
                DateTime endDate = DateTime.Today;

                string fromDate = string.Format("{0:yyyy-MM-dd}", startDate);
                string toDate = string.Format("{0:yyyy-MM-dd}", endDate.AddDays(1));

                if (SearchPanel1.Venus != "")
                {
                    whereclaus = "HotelId='" + intHotelID + "' and booktype = 0 and BookingDate BETWEEN '" + fromDate + "' And '" + toDate + "' and HotelName Like '" + SearchPanel1.Venus + "%'";
                }
                else
                {
                    whereclaus = "HotelId='" + intHotelID + "' and booktype = 0 and BookingDate BETWEEN '" + fromDate + "' And '" + toDate + "'";
                }
            }
            else if (ViewState["Period"].ToString() == "YEAR")
            {

                DateTime startDate = GetFirstDayOfTheYear();
                DateTime endDate = DateTime.Today;
                string fromDate = string.Format("{0:yyyy-MM-dd}", startDate);
                string toDate = string.Format("{0:yyyy-MM-dd}", endDate.AddDays(1));

                if (SearchPanel1.Venus != "")
                {
                    whereclaus = "HotelId='" + intHotelID + "' and booktype = 0 and BookingDate BETWEEN '" + fromDate + "' And '" + toDate + "' and HotelName Like '" + SearchPanel1.Venus + "%'";
                }
                else
                {
                    whereclaus = "HotelId='" + intHotelID + "' and booktype = 0 and BookingDate BETWEEN '" + fromDate + "' And '" + toDate + "'";
                }

            }
        }
        else
        {
            DateTime baseDate = DateTime.Today;
            DateTime startDate = baseDate.AddDays(1 - baseDate.Day);
            DateTime endDate = DateTime.Today;

            string fromDate = string.Format("{0:yyyy-MM-dd}", startDate);
            string toDate = string.Format("{0:yyyy-MM-dd}", endDate.AddDays(1));

            if (SearchPanel1.Venus != "")
            {
                whereclaus = "HotelId='" + intHotelID + "' and booktype = 0 and BookingDate BETWEEN '" + fromDate + "' And '" + toDate + "' and HotelName Like '" + SearchPanel1.Venus + "%'";
            }
            else
            {
                whereclaus = "HotelId='" + intHotelID + "' and booktype = 0 and BookingDate BETWEEN '" + fromDate + "' And '" + toDate + "'";
            }
        }

        string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
        VList<Viewbookingrequest> objViewBooking = new VList<Viewbookingrequest>();
        objViewBooking = objViewHotelBooikng.Bindgrid(whereclaus, orderby);
        grdViewBooking.DataSource = objViewBooking;
        grdViewBooking.DataBind();
        ApplyPagingForBookingList();
    }
    #endregion
    #region Event
    protected void grdViewHotelList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
        {
            Viewbookingrequest data = e.Row.DataItem as Viewbookingrequest;
            Label lblCity = (Label)e.Row.FindControl("lblCity");
            lblCity.Text = objViewHotelBooikng.getCityName(Convert.ToString(data.HotelId));
            LinkButton lblHotel = (LinkButton)e.Row.FindControl("lnkHotelName");
            lblHotel.Text = objViewHotelBooikng.GetHotelName(data.HotelId);
            Label lblNumberOfBooking = (Label)e.Row.FindControl("lblNumberOfBooking");
            vlistFoCount = (VList<Viewbookingrequest>)Session["AllBooking"];
            lblNumberOfBooking.Text = vlistFoCount.Where(a => a.HotelId == data.HotelId).Count().ToString();
            if (ViewState["GetTotalBookingCount"].ToString() == "0")
            {
                ViewState["GetTotalBookingCount"] = lblNumberOfBooking.Text;
            }
            else
            {
                ViewState["GetTotalBookingCount"] = Convert.ToInt64(ViewState["GetTotalBookingCount"]) + Convert.ToInt64(lblNumberOfBooking.Text);
            }
        }
    }
    protected void grdViewHotelList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (Convert.ToString(e.CommandName) == "ViewBooking")
        {
            grdViewBookingBind(Convert.ToInt64(e.CommandArgument));
            ViewState["HotelIdForPagingIndex"] = e.CommandArgument;
            pnlBookingGrid.Visible = true;
            divbookingdetails.Visible = false;
            ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + pnlBookingGrid.ClientID + "');});", true);
        }

    }
    protected void grdViewBooking_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
        {
            Viewbookingrequest data = e.Row.DataItem as Viewbookingrequest;
            LinkButton lnkRefno = (LinkButton)e.Row.FindControl("lnkRefno");
            lnkRefno.Text = data.Id.ToString();
            Label lblMeetingroom = (Label)e.Row.FindControl("lblMeetingroom");
            lblMeetingroom.Text = data.Name;
            Label lblBookingDate = (Label)e.Row.FindControl("lblBookingDate");
            lblBookingDate.Text = string.Format("{0:dd MMM yyyy}", data.BookingDate);
            Label lblCompany = (Label)e.Row.FindControl("lblCompany");
            lblCompany.Text = data.Usertype;
            LinkButton lnkContactName = (LinkButton)e.Row.FindControl("lnkContactName");
            lnkContactName.Text = data.Contact;
            Label lblStart = (Label)e.Row.FindControl("lblStart");
            lblStart.Text = string.Format("{0:dd MMM yyyy}", data.ArrivalDate);
            Label lblEnd = (Label)e.Row.FindControl("lblEnd");
            lblEnd.Text = string.Format("{0:dd MMM yyyy}", data.DepartureDate);
            Label lblTotal = (Label)e.Row.FindControl("lblTotal");
            lblTotal.Text = string.Format("{0:###,###,###}", data.FinalTotalPrice);
        }
    }
    protected void grdViewBooking_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (Convert.ToString(e.CommandName) == "ViewBookingDetail")
        {
            divbookingdetails.Visible = true;
            pnlBookingGrid.Visible = true;
            ucViewBookingDetail.BindBooking(Convert.ToInt32(e.CommandArgument));
            ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + divbookingdetails.ClientID + "');});", true);

        }
        else if (Convert.ToString(e.CommandName) == "ViewContactDatail")
        {
            Users obj = objViewHotelBooikng.GetUser(Convert.ToInt32(e.CommandArgument));
            UserDetails objDetails = objViewHotelBooikng.getuserdetails(obj.UserId).FirstOrDefault();
            objBooking = objViewHotelBooikng.getxml(Convert.ToInt64(e.CommandArgument));
            lblContactName.Text = obj.FirstName + "  " + obj.LastName;
            lblEmail.Text = obj.EmailId;
            lblPhoneNumber.Text = objDetails.Phone;
            if (string.IsNullOrEmpty(objDetails.Address) || string.IsNullOrWhiteSpace(objDetails.Address))
            {
                lbladdress.Text = "N/A";

            }
            else
            {
                lbladdress.Text = objDetails.Address;
            }
            lblFrom.Text = objBooking.ArivalDate.ToString("dd MMM yyyy");
            lblTo.Text = objBooking.DepartureDate.ToString("dd MMM yyyy");
            lblDuration.Text = objBooking.Duration == 1 ? objBooking.Duration + " Day" : objBooking.Duration + " Days";
            divPopUp.Style.Add("display", "block");
            ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "OpenContactPopUp();", true);
            pnlBookingGrid.Visible = true;
        }
    }
    protected void grdViewHotelList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            divbookingdetails.Visible = false;
            pnlBookingGrid.Visible = false;
            grdViewHotelList.PageIndex = e.NewPageIndex;
            ViewState["CurrentPage"] = e.NewPageIndex;
            ViewState["GetTotalBookingCount"] = "0";
            grdViewHotelList.DataSource = Session["ListOfHotelTemp"];
            grdViewHotelList.DataBind();
            ApplyPaging();
        }
        catch (Exception ex)
        {

        }
    }

    #endregion
    #endregion
    #region Request
    #region Event
    protected void grdViewHotelListRequest_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
        {
            Viewbookingrequest data = e.Row.DataItem as Viewbookingrequest;
            Label lblCity = (Label)e.Row.FindControl("lblCity");
            lblCity.Text = objViewHotelBooikng.getCityName(Convert.ToString(data.HotelId));
            LinkButton lblHotel = (LinkButton)e.Row.FindControl("lnkHotelName");
            lblHotel.Text = objViewHotelBooikng.GetHotelName(data.HotelId);
            Label lblNumberOfBooking = (Label)e.Row.FindControl("lblNumberOfRequest");
            vlistFoCount = (VList<Viewbookingrequest>)Session["AllRequest"];
            lblNumberOfBooking.Text = vlistFoCount.Where(a => a.HotelId == data.HotelId).Count().ToString();
            if (ViewState["GetTotalRequestCount"].ToString() == "0")
            {
                ViewState["GetTotalRequestCount"] = lblNumberOfBooking.Text;
            }
            else
            {
                ViewState["GetTotalRequestCount"] = Convert.ToInt64(ViewState["GetTotalRequestCount"]) + Convert.ToInt64(lblNumberOfBooking.Text);
            }
        }
    }
    protected void grdViewHotelListRequest_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (Convert.ToString(e.CommandName) == "ViewRequest")
        {
            grdViewRequestBind(Convert.ToInt64(e.CommandArgument));
            ViewState["HotelIdForPagingIndexRequest"] = e.CommandArgument;
            pnlRequestGrid.Visible = true;
            DivrequestDetail.Visible = false;
            ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + pnlRequestGrid.ClientID + "');});", true);
        }

    }
    protected void grdViewRequest_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
        {
            Viewbookingrequest data = e.Row.DataItem as Viewbookingrequest;
            LinkButton lnkRefno = (LinkButton)e.Row.FindControl("lnkRefno");
            lnkRefno.Text = data.Id.ToString();
            Label lblMeetingroom = (Label)e.Row.FindControl("lblMeetingroom");
            lblMeetingroom.Text = data.Name;
            Label lblBookingDate = (Label)e.Row.FindControl("lblBookingDate");
            lblBookingDate.Text = string.Format("{0:dd MMM yyyy}", data.BookingDate);
            Label lblCompany = (Label)e.Row.FindControl("lblCompany");
            lblCompany.Text = data.Usertype;
            LinkButton lnkContactName = (LinkButton)e.Row.FindControl("lnkContactName");
            lnkContactName.Text = data.Contact;
            Label lblStart = (Label)e.Row.FindControl("lblStart");
            lblStart.Text = string.Format("{0:dd MMM yyyy}", data.ArrivalDate);
            Label lblEnd = (Label)e.Row.FindControl("lblEnd");
            lblEnd.Text = string.Format("{0:dd MMM yyyy}", data.DepartureDate);
            Label lblTotal = (Label)e.Row.FindControl("lblTotal");
            lblTotal.Text = string.Format("{0:###,###,###}", data.FinalTotalPrice);
        }
    }
    protected void grdViewRequest_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        if (Convert.ToString(e.CommandName) == "ViewBookingDetail")
        {
            ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
            Booking obj = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(e.CommandArgument));
            if (obj.BookType == 1)
            {
                requestDetails.BindHotel(Convert.ToInt32(e.CommandArgument));
                BookingDetails1.Visible = false;
                requestDetails.Visible = true;
                pnlRequestGrid.Visible = true;
                DivrequestDetail.Visible = true;
                ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + DivrequestDetail.ClientID + "');});", true);
            }
            else if (obj.BookType == 2)
            {
                BookingDetails1.Visible = true;
                BookingDetails1.BindBooking(Convert.ToInt32(e.CommandArgument));
                requestDetails.Visible = false;
                pnlRequestGrid.Visible = true;
                DivrequestDetail.Visible = true;
                ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + DivrequestDetail.ClientID + "');});", true);
            }
        }
        else if (Convert.ToString(e.CommandName) == "ViewContactDatail")
        {
            pnlRequestGrid.Visible = true;
            Users obj = objViewHotelBooikng.GetUser(Convert.ToInt32(e.CommandArgument));
            UserDetails objDetails = objViewHotelBooikng.getuserdetails(obj.UserId).FirstOrDefault();
            objRequest = objViewHotelBooikng.Reqgetxml(Convert.ToInt64(e.CommandArgument));
            lblContactName.Text = obj.FirstName + "  " + obj.LastName;
            lblEmail.Text = obj.EmailId;
            lblPhoneNumber.Text = objDetails.Phone;
            if (string.IsNullOrEmpty(objDetails.Address) || string.IsNullOrWhiteSpace(objDetails.Address))
            {
                lbladdress.Text = "N/A";

            }
            else
            {
                lbladdress.Text = objDetails.Address;
            }
            lblFrom.Text = objRequest.ArivalDate.ToString("dd MMM yyyy");
            lblTo.Text = objRequest.DepartureDate.ToString("dd MMM yyyy");
            lblDuration.Text = objRequest.Duration == 1 ? objRequest.Duration + " Day" : objRequest.Duration + " Days";
            divPopUp.Style.Add("display", "block");
            ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "OpenContactPopUp();", true);
        }
    }
    protected void grdViewHotelListRequest_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            pnlRequestGrid.Visible = false;
            DivrequestDetail.Visible = false;
            grdViewHotelListRequest.PageIndex = e.NewPageIndex;
            ViewState["CurrentPageRequest"] = e.NewPageIndex;
            ViewState["GetTotalRequestCount"] = "0";
            grdViewHotelListRequest.DataSource = Session["ListOfHotelRequestTemp"];
            grdViewHotelListRequest.DataBind();
            ApplyPagingRequest();
        }
        catch (Exception ex)
        {

        }

    }
    #endregion
    #region Function
    private void ApplyPagingRequest()
    {
        try
        {
            pnlRequestGrid.Visible = false;
            GridViewRow row = grdViewHotelListRequest.TopPagerRow;
            if (row != null)
            {
                PlaceHolder ph;
                LinkButton lnkPaging;
                LinkButton lnkPrevPage;
                LinkButton lnkNextPage;
                lnkPrevPage = new LinkButton();
                lnkPrevPage.CssClass = "pre";
                lnkPrevPage.Width = Unit.Pixel(73);
                lnkPrevPage.CommandName = "Page";
                lnkPrevPage.CommandArgument = "prev";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPrevPage);
                if (grdViewHotelListRequest.PageIndex == 0)
                {
                    lnkPrevPage.Enabled = false;

                }

                if (ViewState["CurrentPageRequest"] == null)
                {
                    ViewState["CurrentPageRequest"] = "0";
                }

                for (int i = 1; i <= grdViewHotelListRequest.PageCount; i++)
                {
                    lnkPaging = new LinkButton();
                    if (ViewState["CurrentPageRequest"] != null)
                    {
                        if (Convert.ToInt32(ViewState["CurrentPageRequest"]) + 1 == i)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    else
                    {
                        if (i == 1)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    lnkPaging.Width = Unit.Pixel(16);
                    lnkPaging.Text = i.ToString();
                    lnkPaging.CommandName = "Page";
                    lnkPaging.CommandArgument = i.ToString();
                    if (i == grdViewHotelListRequest.PageIndex + 1)
                        ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkPaging);
                }
                lnkNextPage = new LinkButton();
                lnkNextPage.CssClass = "nex";
                lnkNextPage.Width = Unit.Pixel(42);
                lnkNextPage.CommandName = "Page";
                lnkNextPage.CommandArgument = "next";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkNextPage);
                ph = (PlaceHolder)row.FindControl("ph");
                if (grdViewHotelListRequest.PageIndex == grdViewHotelListRequest.PageCount - 1)
                {
                    lnkNextPage.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    public VList<Viewbookingrequest> GetFinalRequestData(string strRequestType)
    {
        if (strRequestType == "ONLINE")
        {
            Vlistreq = GetOnlineRequest();
            lblTotalRequest.Text = Session["TotalRequestByOnline"].ToString();
        }
        else if (strRequestType == "TENTATIVE")
        {
            Vlistreq = GetTotalTentativeRequest();
            lblTotalRequest.Text = Session["TotalRequestByTentative"].ToString();
        }
        else if (strRequestType == "NOACTION")
        {
            Vlistreq = GetNoActionRequest();
            lblTotalRequest.Text = Session["TotalRequestByNoAction"].ToString();
        }
        else if (strRequestType == "TODAY")
        {
            Vlistreq = GetTodayRequest();
            lblTotalRequest.Text = Session["TotalRequestByToday"].ToString();
        }
        Session["ListOfHotelRequest"] = Vlistreq;
        Session["ListOfHotelRequestTemp"] = Vlistreq;
        return Vlistreq;

    }
    public void grdViewRequestBind(long intHotelID)
    {
        string whereclaus = string.Empty;
        if (ViewState["RequestType"] != null)
        {
            if (ViewState["RequestType"].ToString() == "TODAY")
            {
                DateTime Todaydate = DateTime.Today;
                string startdate = string.Format("{0:yyyy-MM-dd}", Todaydate);
                string enddate = string.Format("{0:yyyy-MM-dd}", Todaydate.AddDays(1));
                //string today = string.Format("{0:yyyy/MM/dd}", Todaydate);

                whereclaus = "HotelId='" + intHotelID + "' and Booktype in (1,2) and BookingDate Between  '" + startdate + "' and '" + enddate + "'";
            }
            else if (ViewState["RequestType"].ToString() == "ONLINE")
            {
                whereclaus = "HotelId='" + intHotelID + "' and Booktype in (1,2)";
            }
            else if (ViewState["RequestType"].ToString() == "TENTATIVE")
            {

                whereclaus = "HotelId='" + intHotelID + "' and RequestStatus = 5 and Booktype in (1,2)";

            }
            else if (ViewState["RequestType"].ToString() == "NOACTION")
            {
                whereclaus = "HotelId='" + intHotelID + "' and RequestStatus = 1 and Booktype in (1,2)";

            }
        }
        else
        {

            DateTime Todaydate = DateTime.Today;
            string startdate = string.Format("{0:yyyy-MM-dd}", Todaydate);
            string enddate = string.Format("{0:yyyy-MM-dd}", Todaydate.AddDays(1));
            whereclaus = "HotelId='" + intHotelID + "' and Booktype in (1,2) and BookingDate Between  '" + startdate + "' and '" + enddate + "'";
            //string today = string.Format("{0:yyyy/MM/dd}", Todaydate);
            //whereclaus = "HotelId='" + intHotelID + "' and Booktype in (1,2) and BookingDate =  '" + today + "'";
        }
        string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
        VList<Viewbookingrequest> objViewRequest = new VList<Viewbookingrequest>();
        objViewRequest = objViewHotelBooikng.Bindgrid(whereclaus, orderby);
        grdViewRequest.DataSource = objViewRequest;
        grdViewRequest.DataBind();
        ApplyPagingForRequestList();
    }
    public void BindLstViewHotelRequest(string strRequestType)
    {
        VList<Viewbookingrequest> obj = GetFinalRequestData(strRequestType);
        ViewState["GetTotalRequestCount"] = "0";
        grdViewHotelListRequest.DataSource = obj;
        grdViewHotelListRequest.DataBind();
        ApplyPagingRequest();
        lblTotalBooking.Text = obj.Count.ToString();

    }
    #endregion
    #endregion
    #region PageAlpha
    public void PageChange(object sender, EventArgs e)
    {
        try
        {
            if (ViewState["IsBooking"].ToString() == "YES")
            {
                pnlBookingGrid.Visible = false;
                divbookingdetails.Visible = false;
                HtmlAnchor anc = (sender as HtmlAnchor);
                anc.Style.Add("class", "select");
                var alpha = anc.InnerHtml;
                string get = alpha.Trim();
                ViewState["SearchAlpha"] = get;
                VList<Viewbookingrequest> objshortBooking = new VList<Viewbookingrequest>();
                objshortBooking = (VList<Viewbookingrequest>)Session["ListOfHotelBooking"];
                ViewState["CurrentPage"] = null;
                if (get != "all")
                {

                    ViewState["GetTotalBookingCount"] = "0";
                    //Session["ListOfHotelTemp"] = objshortBooking.FindAll(a => a.HotelName.ToLower().StartsWith(get.ToLower()));
                    VList<Viewbookingrequest> objBookingCount = (VList<Viewbookingrequest>)objshortBooking.FindAll(a => a.HotelName.ToLower().StartsWith(get.ToLower()));
                    Session["ListOfHotelTemp"] = objBookingCount;
                    grdViewHotelList.DataSource = Session["ListOfHotelTemp"];
                    grdViewHotelList.DataBind();
                    ApplyPaging();
                    lblTotalBooking.Text = ViewState["GetTotalBookingCount"].ToString();
                }
                else
                {
                    ViewState["GetTotalBookingCount"] = "0";
                    Session["ListOfHotelTemp"] = objshortBooking;
                    grdViewHotelList.DataSource = objshortBooking;
                    grdViewHotelList.DataBind();
                    lblTotalBooking.Text = objshortBooking.Count.ToString();
                    ApplyPaging();
                    lblTotalBooking.Text = ViewState["GetTotalBookingCount"].ToString();
                }
                pnlBookingGrid.Visible = false;
            }
            else
            {
                pnlRequestGrid.Visible = false;
                DivrequestDetail.Visible = false;
                HtmlAnchor anc = (sender as HtmlAnchor);
                anc.Style.Add("class", "select");
                var alpha = anc.InnerHtml;
                string get = alpha.Trim();
                ViewState["SearchAlpha"] = get;
                VList<Viewbookingrequest> objshortBooking = new VList<Viewbookingrequest>();
                objshortBooking = (VList<Viewbookingrequest>)Session["ListOfHotelRequest"];
                ViewState["CurrentPageRequest"] = null;
                grdViewHotelListRequest.PageIndex = 0;
                if (get != "all")
                {
                    ViewState["GetTotalRequestCount"] = "0";
                    Session["ListOfHotelRequestTemp"] = objshortBooking.FindAll(a => a.HotelName.ToLower().StartsWith(get.ToLower()));
                    grdViewHotelListRequest.DataSource = Session["ListOfHotelRequestTemp"];
                    grdViewHotelListRequest.DataBind();
                    ApplyPagingRequest();
                    lblTotalRequest.Text = ViewState["GetTotalRequestCount"].ToString();
                }
                else
                {
                    ViewState["GetTotalRequestCount"] = "0";
                    Session["ListOfHotelRequestTemp"] = objshortBooking;
                    grdViewHotelListRequest.DataSource = objshortBooking;
                    grdViewHotelListRequest.DataBind();
                    ApplyPagingRequest();
                    lblTotalRequest.Text = ViewState["GetTotalRequestCount"].ToString();
                }
                pnlRequestGrid.Visible = false;
            }
        }
        catch (Exception ex)
        {
            //logger.Error(ex);
            lblMessage.Text = ex.ToString();
        }
    }
    #endregion
    #region BookingStatus
    #region Event
    protected void lnkViewTodayBooking_Click(object sender, EventArgs e)
    {
        Session["command"] = "lnkViewTodayBooking_Click";
        divbookingdetails.Visible = false;
        DivBookingText.Visible = true;
        DivRequestText.Visible = false;
        grdViewHotelList.PageIndex = 0;
        ViewState["CurrentPage"] = null;
        ViewState["SearchAlpha"] = "all";
        pnlRequest.Visible = false;
        pnlbooking.Visible = true;
        ViewState["Period"] = "TODAY";
        ViewState["IsBooking"] = "YES";
        BindLstViewHotel("TODAY");
        ApplyPaging();
        trTodayBooking.BgColor = "#E3F0F1";
        trCurrentMonth.BgColor = "#ffffff";
        trCurrentYearBooking.BgColor = "#ffffff";
        trTotalRequest.BgColor = "#ffffff";
        trTentative.BgColor = "#ffffff";
        trNoAction.BgColor = "#ffffff";
        trOnlineRequest.BgColor = "#ffffff";
        divgraph.Style.Add("display", "none");
        divlist.Style.Add("display", "block");

    }
    protected void lnkViewCurrentMonth_Click(object sender, EventArgs e)
    {
        Session["command"] = "lnkViewCurrentMonth_Click";
        divbookingdetails.Visible = false;
        DivBookingText.Visible = true;
        DivRequestText.Visible = false;
        grdViewHotelList.PageIndex = 0;
        ViewState["CurrentPage"] = null;
        ViewState["SearchAlpha"] = "all";
        pnlRequest.Visible = false;
        pnlbooking.Visible = true;
        ViewState["Period"] = "MONTH";
        ViewState["IsBooking"] = "YES";
        BindLstViewHotel("MONTH");
        ApplyPaging();
        trTodayBooking.BgColor = "#ffffff";
        trCurrentMonth.BgColor = "#E3F0F1";
        trCurrentYearBooking.BgColor = "#ffffff";
        trTotalRequest.BgColor = "#ffffff";
        trTentative.BgColor = "#ffffff";
        trNoAction.BgColor = "#ffffff";
        trOnlineRequest.BgColor = "#ffffff";
        divgraph.Style.Add("display", "none");
        divlist.Style.Add("display", "block");
    }
    protected void lnkCurrentYear_Click(object sender, EventArgs e)
    {
        Session["command"] = "lnkCurrentYear_Click";
        divbookingdetails.Visible = false;
        DivBookingText.Visible = true;
        DivRequestText.Visible = false;
        grdViewHotelList.PageIndex = 0;
        ViewState["CurrentPage"] = null;
        ViewState["SearchAlpha"] = "all";
        pnlRequest.Visible = false;
        pnlbooking.Visible = true;
        ViewState["Period"] = "YEAR";
        ViewState["IsBooking"] = "YES";
        BindLstViewHotel("YEAR");
        ApplyPaging();
        trTodayBooking.BgColor = "#ffffff";
        trCurrentMonth.BgColor = "#ffffff";
        trCurrentYearBooking.BgColor = "#E3F0F1";
        trTotalRequest.BgColor = "#ffffff";
        trTentative.BgColor = "#ffffff";
        trNoAction.BgColor = "#ffffff";
        trOnlineRequest.BgColor = "#ffffff";
        divgraph.Style.Add("display", "none");
        divlist.Style.Add("display", "block");
    }
    #endregion
    #region Function
    static DateTime GetFirstDayOfTheYear()
    {
        return FirstDayOfYear(DateTime.Today);
    }
    static DateTime FirstDayOfYear(DateTime y)
    {
        return new DateTime(y.Year, 1, 1);
    }
    public VList<Viewbookingrequest> GetByYear()
    {
        DateTime startDate = GetFirstDayOfTheYear();
        DateTime endDate = DateTime.Today;
        string fromDate = string.Format("{0:yyyy-MM-dd}", startDate);
        string toDate = string.Format("{0:yyyy-MM-dd}", endDate.AddDays(1));
        lblFromDate.Text = string.Format("{0:dd MMM yyyy}", startDate);
        lblToDate.Text = string.Format("{0:dd MMM yyyy}", endDate);

        string whereclaus = string.Empty;
        if (SearchPanel1.Venus != "")
        {
            whereclaus = "booktype = 0 and BookingDate BETWEEN '" + fromDate + "' And '" + toDate + "' and HotelName Like '" + SearchPanel1.Venus + "%'";
        }
        else
        {
            whereclaus = "booktype = 0 and BookingDate BETWEEN '" + fromDate + "' And '" + toDate + "'";
        }


        string orderby = ViewbookingrequestColumn.BookingDate + " DESC";

        objBookingView = objViewHotelBooikng.Bindgrid(whereclaus, orderby);
        Session["TotalBookingByYear"] = objBookingView.Count;

        if (objBookingView.Count > 0)
        {
            var list = objBookingView.Select(a => Convert.ToDecimal(a.RevenueAmount));
            Session["TotalrevenueByYear"] = list.Sum();// objBookingView.Sum(a => a.RevenueAmount);
        }

        else
            Session["TotalrevenueByYear"] = 0;

        Session["AllBooking"] = objBookingView;

        objBookingView = objBookingView.FindAllDistinct(ViewbookingrequestColumn.HotelId);
        objBookingViewTemp = objBookingView.Copy();
        foreach (Viewbookingrequest obj in objBookingView)
        {
            bool IsRemove = false;
            Hotel objHotel = objHotelInfo.GetHotelByHotelID(Convert.ToInt32(obj.HotelId));

            if (objHotel != null)
            {
                if (SearchPanel1.propCountryID != 0)
                {
                    if (objHotel.CountryId == SearchPanel1.propCountryID)
                    {
                        IsRemove = false;
                    }
                    else
                    {
                        IsRemove = true;
                    }
                }
                if (SearchPanel1.propCityID != 0)
                {
                    if (objHotel.CityId == SearchPanel1.propCityID)
                    {
                        IsRemove = false;
                    }
                    else
                    {
                        IsRemove = true;
                    }
                }

                if (IsRemove)
                {
                    VList<Viewbookingrequest> objList = new VList<Viewbookingrequest>();
                    objList = (VList<Viewbookingrequest>)Session["AllBooking"];
                    VList<Viewbookingrequest> objListBookingTemp = objList.Copy();

                    if (objList.Count > 0)
                    {
                        for (int i = 0; i <= objList.Count - 1; i++)
                        {
                            Viewbookingrequest r = objListBookingTemp.Find(a => a.HotelId == obj.HotelId);
                            objListBookingTemp.Remove(r);
                        }
                        Session["AllBooking"] = objListBookingTemp;
                        Session["TotalBookingByYear"] = objListBookingTemp.Count;
                    }

                    Viewbookingrequest d = objBookingViewTemp.Find(a => a.Id == obj.Id);
                    objBookingViewTemp.Remove(d);
                }
            }

        }


        #region fetch hotel id

        foreach (Viewbookingrequest obj in objBookingViewTemp)
        {
            hotelid += " ," + obj.HotelId;

        }
        #endregion
        return objBookingViewTemp;
    }
    public VList<Viewbookingrequest> GetByMonth()
    {
        VList<Viewbookingrequest> objView = new VList<Viewbookingrequest>();
        DateTime baseDate = DateTime.Today;
        DateTime startDate = baseDate.AddDays(1 - baseDate.Day);
        DateTime endDate = DateTime.Today;

        string fromDate = string.Format("{0:yyyy-MM-dd}", startDate);
        string toDate = string.Format("{0:yyyy-MM-dd}", endDate.AddDays(1));

        lblFromDate.Text = string.Format("{0:dd MMM yyyy}", startDate);
        lblToDate.Text = string.Format("{0:dd MMM yyyy}", endDate);

        string whereclaus = string.Empty;

        if (SearchPanel1.Venus != "")
        {
            whereclaus = "booktype = 0 and BookingDate BETWEEN '" + fromDate + "' And '" + toDate + "' and HotelName Like '" + SearchPanel1.Venus + "%'";
        }
        else
        {
            whereclaus = "booktype = 0 and BookingDate BETWEEN '" + fromDate + "' And '" + toDate + "'";
        }
        string orderby = ViewbookingrequestColumn.BookingDate + " DESC";

        objBookingView = objViewHotelBooikng.Bindgrid(whereclaus, orderby);
        Session["TotalBookingByMonth"] = objBookingView.Count;
        Session["AllBooking"] = objBookingView;
        objBookingView = objBookingView.FindAllDistinct(ViewbookingrequestColumn.HotelId);
        objBookingViewTemp = objBookingView.Copy();

        foreach (Viewbookingrequest obj in objBookingView)
        {
            bool IsRemove = false;
            Hotel objHotel = objHotelInfo.GetHotelByHotelID(Convert.ToInt32(obj.HotelId));

            if (objHotel != null)
            {
                if (SearchPanel1.propCountryID != 0)
                {
                    if (objHotel.CountryId == SearchPanel1.propCountryID)
                    {
                        IsRemove = false;
                    }
                    else
                    {
                        IsRemove = true;
                    }
                }
                if (SearchPanel1.propCityID != 0)
                {
                    if (objHotel.CityId == SearchPanel1.propCityID)
                    {
                        IsRemove = false;
                    }
                    else
                    {
                        IsRemove = true;
                    }
                }

                if (IsRemove)
                {
                    VList<Viewbookingrequest> objList = new VList<Viewbookingrequest>();
                    objList = (VList<Viewbookingrequest>)Session["AllBooking"];
                    VList<Viewbookingrequest> objListBookingTemp = objList.Copy();

                    if (objList.Count > 0)
                    {
                        for (int i = 0; i <= objList.Count - 1; i++)
                        {
                            Viewbookingrequest r = objListBookingTemp.Find(a => a.HotelId == obj.HotelId);
                            objListBookingTemp.Remove(r);
                        }
                        Session["AllBooking"] = objListBookingTemp;
                        Session["TotalBookingByMonth"] = objListBookingTemp.Count;
                    }


                    Viewbookingrequest d = objBookingViewTemp.Find(a => a.Id == obj.Id);
                    objBookingViewTemp.Remove(d);
                }
            }

        }
        #region fetch hotel id

        foreach (Viewbookingrequest obj in objBookingViewTemp)
        {
            hotelid += " ," + obj.HotelId;


        }
        Session["fetchHotelId"] = hotelid;
        #endregion
        return objBookingViewTemp;
    }
    public VList<Viewbookingrequest> GetByToday()
    {
        VList<Viewbookingrequest> objView = new VList<Viewbookingrequest>();
        DateTime Todaydate = DateTime.Today;

        string fromDate = string.Format("{0:yyyy-MM-dd}", Todaydate);
        string toDate = string.Format("{0:yyyy-MM-dd}", Todaydate.AddDays(1));
        lblToDate.Text = string.Format("{0:dd MMM yyyy}", Todaydate);
        string whereclaus = string.Empty;

        if (SearchPanel1.Venus != "")
        {
            whereclaus = "booktype = 0 and BookingDate BETWEEN '" + fromDate + "' And '" + toDate + "' and HotelName Like '" + SearchPanel1.Venus + "%'";
        }
        else
        {
            whereclaus = "booktype = 0 and BookingDate BETWEEN '" + fromDate + "' And '" + toDate + "'";
        }
        string orderby = ViewbookingrequestColumn.BookingDate + " DESC";

        objBookingView = objViewHotelBooikng.Bindgrid(whereclaus, orderby);
        Session["TotalBookingByToday"] = objBookingView.Count;
        if (objBookingView.Count > 0)
        {
            var list = objBookingView.Select(a => a.RevenueAmount);
            Session["totalrevenueByToday"] = list.Sum();// objBookingView.Sum(a => a.RevenueAmount);
        }
        else
            Session["totalrevenueByToday"] = 0;
        Session["AllBooking"] = objBookingView;
        objBookingView = objBookingView.FindAllDistinct(ViewbookingrequestColumn.HotelId);
        objBookingViewTemp = objBookingView.Copy();
        foreach (Viewbookingrequest obj in objBookingView)
        {
            bool IsRemove = false;
            Hotel objHotel = objHotelInfo.GetHotelByHotelID(Convert.ToInt32(obj.HotelId));

            if (objHotel != null)
            {
                if (SearchPanel1.propCountryID != 0)
                {
                    if (objHotel.CountryId == SearchPanel1.propCountryID)
                    {
                        IsRemove = false;
                    }
                    else
                    {
                        IsRemove = true;
                    }
                }
                if (SearchPanel1.propCityID != 0)
                {
                    if (objHotel.CityId == SearchPanel1.propCityID)
                    {
                        IsRemove = false;
                    }
                    else
                    {
                        IsRemove = true;
                    }
                }

                if (IsRemove)
                {
                    VList<Viewbookingrequest> objList = new VList<Viewbookingrequest>();
                    objList = (VList<Viewbookingrequest>)Session["AllBooking"];
                    VList<Viewbookingrequest> objListBookingTemp = objList.Copy();

                    if (objList.Count > 0)
                    {
                        for (int i = 0; i <= objList.Count - 1; i++)
                        {
                            Viewbookingrequest r = objListBookingTemp.Find(a => a.HotelId == obj.HotelId);
                            objListBookingTemp.Remove(r);
                        }
                        Session["AllBooking"] = objListBookingTemp;
                        Session["TotalBookingByToday"] = objListBookingTemp.Count;
                    }


                    Viewbookingrequest d = objBookingViewTemp.Find(a => a.Id == obj.Id);
                    objBookingViewTemp.Remove(d);
                }
            }

        }
        #region fetch hotel id

        foreach (Viewbookingrequest obj in objBookingViewTemp)
        {
            hotelid += " ," + obj.HotelId;

        }
        Session["fetchHotelId"] = hotelid;
        #endregion
        return objBookingViewTemp;
    }
    public void ManageDashBoard()
    {
        GetByYear();
        GetByMonth();
        GetByToday();
        lblTodayBooking.Text = Session["TotalBookingByToday"].ToString();
        lblCurrentMonthBooking.Text = Session["TotalBookingByMonth"].ToString();
        lblCurrentYearBooking.Text = Session["TotalBookingByYear"].ToString();
        GetNoActionRequest();
        GetOnlineRequest();
        GetTodayRequest();
        GetTotalTentativeRequest();
        lblOnlineRequest.Text = Session["TotalRequestByOnline"].ToString();
        lblTodayRequest.Text = Session["TotalRequestByToday"].ToString();
        lblToNoactionRequest.Text = Session["TotalRequestByNoAction"].ToString();
        lblTotalTentativeRequest.Text = Session["TotalRequestByTentative"].ToString();
        divgraph.Style.Add("display", "none");
        divlist.Style.Add("display", "block");
    }
    #endregion
    #endregion
    #region RequestStatus
    protected void lnkTodayRequest_Click(object sender, EventArgs e)
    {
        Session["command"] = "lnkTodayRequest_Click";
        DivrequestDetail.Visible = false;
        grdViewHotelListRequest.PageIndex = 0;
        ViewState["CurrentPageRequest"] = null;
        DivRequestText.Visible = true;
        DivBookingText.Visible = false;
        ViewState["IsBooking"] = "NO";
        ViewState["RequestType"] = "TODAY";
        ViewState["SearchAlpha"] = "all";
        pnlbooking.Visible = false;
        pnlRequest.Visible = true;
        trTotalRequest.BgColor = "#E3F0F1";
        trTentative.BgColor = "#ffffff";
        trNoAction.BgColor = "#ffffff";
        trOnlineRequest.BgColor = "#ffffff";
        trTodayBooking.BgColor = "#ffffff";
        trCurrentMonth.BgColor = "#ffffff";
        trCurrentYearBooking.BgColor = "#ffffff";
        BindLstViewHotelRequest("TODAY");
        //ApplyPagingRequest();
        divgraph.Style.Add("display", "none");
        divlist.Style.Add("display", "block");
    }
    protected void lnkTotalTentative_Click(object sender, EventArgs e)
    {
        Session["command"] = "lnkTotalTentative_Click";
        DivrequestDetail.Visible = false;
        grdViewHotelListRequest.PageIndex = 0;
        ViewState["CurrentPageRequest"] = null;
        DivBookingText.Visible = false;
        DivRequestText.Visible = true;
        ViewState["IsBooking"] = "NO";
        ViewState["SearchAlpha"] = "all";
        ViewState["RequestType"] = "TENTATIVE";
        pnlbooking.Visible = false;
        pnlRequest.Visible = true;
        trTotalRequest.BgColor = "#ffffff";
        trTentative.BgColor = "#E3F0F1";
        trNoAction.BgColor = "#ffffff";
        trOnlineRequest.BgColor = "#ffffff";

        trTodayBooking.BgColor = "#ffffff";
        trCurrentMonth.BgColor = "#ffffff";
        trCurrentYearBooking.BgColor = "#ffffff";
        BindLstViewHotelRequest("TENTATIVE");
        //ApplyPagingRequest();
        divgraph.Style.Add("display", "none");
        divlist.Style.Add("display", "block");
    }
    protected void lnkTotalNoAction_Click(object sender, EventArgs e)
    {
        Session["command"] = "lnkTotalNoAction_Click";
        DivrequestDetail.Visible = false;
        grdViewHotelListRequest.PageIndex = 0;
        ViewState["CurrentPageRequest"] = null;
        DivBookingText.Visible = false;
        DivRequestText.Visible = true;
        ViewState["IsBooking"] = "NO";
        ViewState["SearchAlpha"] = "all";
        ViewState["RequestType"] = "NOACTION";
        pnlbooking.Visible = false;
        pnlRequest.Visible = true;
        trTotalRequest.BgColor = "#ffffff";
        trTentative.BgColor = "#ffffff";
        trNoAction.BgColor = "#E3F0F1";
        trOnlineRequest.BgColor = "#ffffff";
        trTodayBooking.BgColor = "#ffffff";
        trCurrentMonth.BgColor = "#ffffff";
        trCurrentYearBooking.BgColor = "#ffffff";
        BindLstViewHotelRequest("NOACTION");
        //ApplyPagingRequest();
        divgraph.Style.Add("display", "none");
        divlist.Style.Add("display", "block");

    }
    protected void lnkOnlineRequest_Click(object sender, EventArgs e)
    {
        Session["command"] = "lnkOnlineRequest_Click";
        DivrequestDetail.Visible = false;
        grdViewHotelListRequest.PageIndex = 0;
        ViewState["CurrentPageRequest"] = null;
        DivBookingText.Visible = false;
        DivRequestText.Visible = true;
        ViewState["IsBooking"] = "NO";
        ViewState["SearchAlpha"] = "all";
        ViewState["RequestType"] = "ONLINE";
        pnlbooking.Visible = false;
        pnlRequest.Visible = true;
        trTotalRequest.BgColor = "#ffffff";
        trTentative.BgColor = "#ffffff";
        trNoAction.BgColor = "#ffffff";
        trOnlineRequest.BgColor = "#E3F0F1";

        trTodayBooking.BgColor = "#ffffff";
        trCurrentMonth.BgColor = "#ffffff";
        trCurrentYearBooking.BgColor = "#ffffff";
        BindLstViewHotelRequest("ONLINE");
        //ApplyPagingRequest();
        divgraph.Style.Add("display", "none");
        divlist.Style.Add("display", "block");
    }

    public VList<Viewbookingrequest> GetTodayRequest()
    {
        string whereclaus = string.Empty;
        DateTime Todaydate = DateTime.Today;
        string today = string.Format("{0:yyyy-MM-dd}", Todaydate);
        string tomorrow = string.Format("{0:yyyy-MM-dd}", Todaydate.AddDays(1));
        if (SearchPanel1.Venus != "")
        {
            whereclaus = "Booktype in (1,2) and BookingDate Between '" + today + "' and '" + tomorrow + "' and HotelName Like '" + SearchPanel1.Venus + "%'";
        }
        else
        {
            whereclaus = "Booktype in (1,2) and BookingDate Between '" + today + "' and '" + tomorrow + "'";
        }

        string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
        objRequestView = objViewHotelBooikng.Bindgrid(whereclaus, orderby);
        Session["TotalRequestByToday"] = objRequestView.Count;
        Session["AllRequest"] = objRequestView;
        objRequestView = objRequestView.FindAllDistinct(ViewbookingrequestColumn.HotelId);
        objRequestViewTemp = objRequestView.Copy();
        foreach (Viewbookingrequest obj in objRequestView)
        {
            bool IsRemove = false;
            Hotel objHotel = objHotelInfo.GetHotelByHotelID(Convert.ToInt32(obj.HotelId));

            if (objHotel != null)
            {
                if (SearchPanel1.propCountryID != 0)
                {
                    if (objHotel.CountryId == SearchPanel1.propCountryID)
                    {
                        IsRemove = false;
                    }
                    else
                    {
                        IsRemove = true;
                    }
                }
                if (SearchPanel1.propCityID != 0)
                {
                    if (objHotel.CityId == SearchPanel1.propCityID)
                    {
                        IsRemove = false;
                    }
                    else
                    {
                        IsRemove = true;
                    }
                }

                if (IsRemove)
                {
                    VList<Viewbookingrequest> objList = new VList<Viewbookingrequest>();
                    objList = (VList<Viewbookingrequest>)Session["AllRequest"];
                    VList<Viewbookingrequest> objListBookingTemp = objList.Copy();

                    if (objList.Count > 0)
                    {
                        for (int i = 0; i <= objList.Count - 1; i++)
                        {
                            Viewbookingrequest r = objListBookingTemp.Find(a => a.HotelId == obj.HotelId);
                            objListBookingTemp.Remove(r);
                        }
                        Session["AllRequest"] = objListBookingTemp;
                        Session["TotalRequestByToday"] = objListBookingTemp.Count;
                    }
                    Viewbookingrequest d = objRequestViewTemp.Find(a => a.Id == obj.Id);
                    objRequestViewTemp.Remove(d);
                }
            }
        }
        #region fetch hotel id

        foreach (Viewbookingrequest obj in objRequestViewTemp)
        {
            hotelid += " ," + obj.HotelId;


        }
        Session["fetchHotelId"] = hotelid;
        #endregion
        return objRequestViewTemp;
    }
    public VList<Viewbookingrequest> GetTotalTentativeRequest()
    {
        string whereclaus = string.Empty;
        if (SearchPanel1.Venus != "")
        {
            whereclaus = "Booktype in (1,2) and RequestStatus = 5 and HotelName Like '" + SearchPanel1.Venus + "%'";
        }
        else
        {
            whereclaus = "Booktype in (1,2) and RequestStatus = 5";

        }
        string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
        objRequestView = objViewHotelBooikng.Bindgrid(whereclaus, orderby);
        Session["TotalRequestByTentative"] = objRequestView.Count;
        Session["AllRequest"] = objRequestView;
        objRequestView = objRequestView.FindAllDistinct(ViewbookingrequestColumn.HotelId);
        objRequestViewTemp = objRequestView.Copy();
        foreach (Viewbookingrequest obj in objRequestView)
        {
            bool IsRemove = false;
            Hotel objHotel = objHotelInfo.GetHotelByHotelID(Convert.ToInt32(obj.HotelId));

            if (objHotel != null)
            {
                if (SearchPanel1.propCountryID != 0)
                {
                    if (objHotel.CountryId == SearchPanel1.propCountryID)
                    {
                        IsRemove = false;
                    }
                    else
                    {
                        IsRemove = true;
                    }
                }
                if (SearchPanel1.propCityID != 0)
                {
                    if (objHotel.CityId == SearchPanel1.propCityID)
                    {
                        IsRemove = false;
                    }
                    else
                    {
                        IsRemove = true;
                    }
                }

                if (IsRemove)
                {
                    VList<Viewbookingrequest> objList = new VList<Viewbookingrequest>();
                    objList = (VList<Viewbookingrequest>)Session["AllRequest"];
                    VList<Viewbookingrequest> objListBookingTemp = objList.Copy();

                    if (objList.Count > 0)
                    {
                        for (int i = 0; i <= objList.Count - 1; i++)
                        {
                            Viewbookingrequest r = objListBookingTemp.Find(a => a.HotelId == obj.HotelId);
                            objListBookingTemp.Remove(r);
                        }
                        Session["AllRequest"] = objListBookingTemp;
                        Session["TotalRequestByTentative"] = objListBookingTemp.Count;
                    }
                    Viewbookingrequest d = objRequestViewTemp.Find(a => a.Id == obj.Id);
                    objRequestViewTemp.Remove(d);
                }
            }
        }
        #region fetch hotel id

        foreach (Viewbookingrequest obj in objRequestViewTemp)
        {
            hotelid += " ," + obj.HotelId;


        }
        Session["fetchHotelId"] = hotelid;
        #endregion
        return objRequestViewTemp;
    }
    public VList<Viewbookingrequest> GetNoActionRequest()
    {
        string whereclaus = string.Empty;
        if (SearchPanel1.Venus != "")
        {
            whereclaus = "Booktype in (1,2) and RequestStatus = 1 and HotelName Like '" + SearchPanel1.Venus + "%'";
        }
        else
        {
            whereclaus = "Booktype in (1,2) and RequestStatus = 1";
        }
        string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
        objRequestView = objViewHotelBooikng.Bindgrid(whereclaus, orderby);
        Session["TotalRequestByNoAction"] = objRequestView.Count;
        Session["AllRequest"] = objRequestView;
        objRequestView = objRequestView.FindAllDistinct(ViewbookingrequestColumn.HotelId);
        objRequestViewTemp = objRequestView.Copy();
        foreach (Viewbookingrequest obj in objRequestView)
        {
            bool IsRemove = false;
            Hotel objHotel = objHotelInfo.GetHotelByHotelID(Convert.ToInt32(obj.HotelId));

            if (objHotel != null)
            {
                if (SearchPanel1.propCountryID != 0)
                {
                    if (objHotel.CountryId == SearchPanel1.propCountryID)
                    {
                        IsRemove = false;
                    }
                    else
                    {
                        IsRemove = true;
                    }
                }
                if (SearchPanel1.propCityID != 0)
                {
                    if (objHotel.CityId == SearchPanel1.propCityID)
                    {
                        IsRemove = false;
                    }
                    else
                    {
                        IsRemove = true;
                    }
                }

                if (IsRemove)
                {
                    VList<Viewbookingrequest> objList = new VList<Viewbookingrequest>();
                    objList = (VList<Viewbookingrequest>)Session["AllRequest"];
                    VList<Viewbookingrequest> objListBookingTemp = objList.Copy();

                    if (objList.Count > 0)
                    {
                        for (int i = 0; i <= objList.Count - 1; i++)
                        {
                            Viewbookingrequest r = objListBookingTemp.Find(a => a.HotelId == obj.HotelId);
                            objListBookingTemp.Remove(r);
                        }
                        Session["AllRequest"] = objListBookingTemp;
                        Session["TotalRequestByNoAction"] = objListBookingTemp.Count;
                    }
                    Viewbookingrequest d = objRequestViewTemp.Find(a => a.Id == obj.Id);
                    objRequestViewTemp.Remove(d);
                }
            }
        }
        #region fetch hotel id

        foreach (Viewbookingrequest obj in objRequestViewTemp)
        {
            hotelid += " ," + obj.HotelId;


        }
        Session["fetchHotelId"] = hotelid;
        #endregion
        return objRequestViewTemp;
    }
    public VList<Viewbookingrequest> GetOnlineRequest()
    {
        string whereclaus = string.Empty;
        if (SearchPanel1.Venus != "")
        {
            whereclaus = "Booktype in (1,2) and HotelName Like '" + SearchPanel1.Venus + "%'";
        }
        else
        {
            whereclaus = "Booktype in (1,2)";
        }
        string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
        objRequestView = objViewHotelBooikng.Bindgrid(whereclaus, orderby);
        Session["TotalRequestByOnline"] = objRequestView.Count;
        Session["AllRequest"] = objRequestView;
        objRequestView = objRequestView.FindAllDistinct(ViewbookingrequestColumn.HotelId);
        objRequestViewTemp = objRequestView.Copy();
        foreach (Viewbookingrequest obj in objRequestView)
        {
            bool IsRemove = false;
            Hotel objHotel = objHotelInfo.GetHotelByHotelID(Convert.ToInt32(obj.HotelId));

            if (objHotel != null)
            {
                if (SearchPanel1.propCountryID != 0)
                {
                    if (objHotel.CountryId == SearchPanel1.propCountryID)
                    {
                        IsRemove = false;
                    }
                    else
                    {
                        IsRemove = true;
                    }
                }
                if (SearchPanel1.propCityID != 0)
                {
                    if (objHotel.CityId == SearchPanel1.propCityID)
                    {
                        IsRemove = false;
                    }
                    else
                    {
                        IsRemove = true;
                    }
                }

                if (IsRemove)
                {
                    VList<Viewbookingrequest> objList = new VList<Viewbookingrequest>();
                    objList = (VList<Viewbookingrequest>)Session["AllRequest"];
                    VList<Viewbookingrequest> objListBookingTemp = objList.Copy();

                    if (objList.Count > 0)
                    {
                        for (int i = 0; i <= objList.Count - 1; i++)
                        {
                            Viewbookingrequest r = objListBookingTemp.Find(a => a.HotelId == obj.HotelId);
                            objListBookingTemp.Remove(r);
                        }
                        Session["AllRequest"] = objListBookingTemp;
                        Session["TotalRequestByOnline"] = objListBookingTemp.Count;
                    }
                    Viewbookingrequest d = objRequestViewTemp.Find(a => a.Id == obj.Id);
                    objRequestViewTemp.Remove(d);
                }
            }
        }

        #region fetch hotel id

        foreach (Viewbookingrequest obj in objRequestViewTemp)
        {
            hotelid += " ," + obj.HotelId;


        }
        Session["fetchHotelId"] = hotelid;
        #endregion
        return objRequestViewTemp;
    }

    #endregion
    #region PDF
    protected void lnkSavePDF_Click(object sender, EventArgs e)
    {
        try
        {
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=UserDetails.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            PrepareGridViewForExport(Divdetails);
            Divdetails.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }




    }

    #region VerifyRenderingInServerForm
    /// <summary>
    /// method to VerifyRenderingInServerForm
    /// </summary>

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    #endregion

    #region PrepareGridViewForExport
    /// <summary>
    /// method to PrepareGridViewForExport
    /// </summary>
    private void PrepareGridViewForExport(Control gv)
    {
        try
        {
            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {

                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(DropDownList))
                {

                    l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(CheckBox))
                {

                    l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(HyperLink))
                {

                    l.Text = (gv.Controls[i] as HyperLink).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.Image))
                {

                    // l.Text = (gv.Controls[i] as Image).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareGridViewForExport(gv.Controls[i]);

                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }
    #endregion
    #region PrepareGridViewForExportDIV
    /// <summary>
    /// method to PrepareGridViewForExport a div
    /// </summary>
    private void PrepareDivForExport(Control gv)
    {
        try
        {

            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {

                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].GetType() == typeof(System.Web.UI.HtmlControls.HtmlAnchor))
                {

                    l.Text = (gv.Controls[i] as System.Web.UI.HtmlControls.HtmlAnchor).InnerText;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(DropDownList))
                {

                    l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(GridView))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    // gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Repeater))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Panel))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Table))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }


                else if (gv.Controls[i].GetType() == typeof(CheckBox))
                {

                    l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(HyperLink))
                {

                    l.Text = (gv.Controls[i] as HyperLink).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                //else if (gv.Controls[i].GetType() == typeof(Label))
                //{

                //    l.Text = (gv.Controls[i] as Label).Text;

                //    gv.Controls.Remove(gv.Controls[i]);

                //    gv.Controls.AddAt(i, l);

                //}
                else if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.Image))
                {

                    // l.Text = (gv.Controls[i] as Image).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareDivForExport(gv.Controls[i]);

                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
        //PrepareGridViewForExport();
        //PrepareGridViewForExport();

    }


    #endregion

    protected void lnkSaveRequestPdf_Click(object sender, EventArgs e)
    {
        try
        {
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=UserDetails.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            PrepareGridViewForExport(DivdetailsRequest);
            DivdetailsRequest.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }
    #endregion
    #region for chart

    public void FirePrviousCommand()
    {
        var view = Session["command"];
        if (Session["command"] != null)
        {
            if (Session["command"] == "lnkViewTodayBooking_Click")
            {
                lnkViewTodayBooking_Click(lnkViewTodayBooking, null);
            }
            if (Session["command"] == "lnkGraphday_Click")
            {
                lnkGraphday_Click(lnkGraphday, null);
            }
            if (Session["command"] == "lnkGraphMonth_Click")
            {
                lnkGraphMonth_Click(lnkGraphMonth, null);
            }
            if (Session["command"] == "lnkGraphYear_Click")
            {
                lnkGraphYear_Click(lnkGraphYear, null);
            }
            if (Session["command"] == "lnkCurrentYear_Click")
            {
                lnkCurrentYear_Click(lnkCurrentYear, null);
            }
            if (Session["command"] == "lnkViewCurrentMonth_Click")
            {
                lnkViewCurrentMonth_Click(lnkViewCurrentMonth, null);
            }
            //-----------------------------
            if (Session["command"] == "lnkTodayReqGraph_Click")
            {
                lnkTodayReqGraph_Click(lnkTodayReqGraph, null);
            }
            if (Session["command"] == "lnkTotalTenReq_Click")
            {
                lnkTotalTenReq_Click(lnkTotalTenReq, null);
            }
            if (Session["command"] == "lnkReqGraphNoaction_Click")
            {
                lnkReqGraphNoaction_Click(lnkReqGraphNoaction, null);
            }
            if (Session["command"] == "lnkOnlinereq_Click")
            {
                lnkOnlinereq_Click(lnkOnlinereq, null);
            }
            if (Session["command"] == "lnkTodayRequest_Click")
            {
                lnkTodayRequest_Click(lnkTodayRequest, null);
            }
            if (Session["command"] == "lnkTotalTentative_Click")
            {
                lnkTotalTentative_Click(lnkTotalTentative, null);
            }
            if (Session["command"] == "lnkTotalNoAction_Click")
            {
                lnkTotalNoAction_Click(lnkTotalNoAction, null);
            }
            if (Session["command"] == "lnkOnlineRequest_Click")
            {
                lnkOnlineRequest_Click(lnkOnlineRequest, null);
            }




            //  Session["command"] = null;
        }
    }
    public void Bindlist(string Fromdt, string Todt, string type)
    {



        //hid+=decimal.

        objViewstatistics.HotelId = Session["fetchHotelId"].ToString();

        string quertype = type;
        DateTime fromdate = new DateTime(Convert.ToInt32(Fromdt.Split('/')[2]), Convert.ToInt32(Fromdt.Split('/')[1]), Convert.ToInt32(Fromdt.Split('/')[0]));
        DateTime todate = new DateTime(Convert.ToInt32(Todt.Split('/')[2]), Convert.ToInt32(Todt.Split('/')[1]), Convert.ToInt32(Todt.Split('/')[0]));





        if (quertype.ToLower() == "month")
        {
            fdata = objViewstatistics.Getreq(fromdate, todate, "4");
            chrtBooking.ChartAreas[0].AxisX.Title = " Date of Booking";
            chrtBooking.ChartAreas[0].AxisY.Title = " Revenue Amount ";
        }
        Fetchdata fobjfdt = new Fetchdata();


        if (quertype.ToLower() == "day")
        {
            chrtBooking.Series["SeriesBooking"].ChartType = SeriesChartType.Point;

            fobjfdt.conversion = Convert.ToDecimal(Session["totalrevenueByToday"].ToString());
            fobjfdt.currentdate = System.DateTime.Now.ToString("dd/MM/yyyy");
            fdata.Add(fobjfdt);
            chrtBooking.ChartAreas[0].AxisX.Title = " Date of Booking";
            chrtBooking.ChartAreas[0].AxisY.Title = "Revenue Amount ";
        }
        if (quertype.ToLower() == "year")
        {
            chrtBooking.Series["SeriesBooking"].ChartType = SeriesChartType.Point;

            fobjfdt.conversion = Convert.ToDecimal(Session["TotalrevenueByYear"].ToString());
            fobjfdt.currentdate = System.DateTime.Now.Year.ToString();
            //  fdata.Count = Session["TotalBookingByToday"].ToString();
            fdata.Add(fobjfdt);
            chrtBooking.ChartAreas[0].AxisX.Title = " Year of Booking";
            chrtBooking.ChartAreas[0].AxisY.Title = "Revenue Amount ";
        }


        //------------ for request

        if (quertype.ToLower() == "dayreq")
        {
            chrtBooking.Series["SeriesBooking"].ChartType = SeriesChartType.Point;
            fobjfdt.Booking = Convert.ToInt32(Session["TotalRequestByToday"].ToString());
            fobjfdt.currentdate = System.DateTime.Now.ToString("dd/MM/yyyy");
            fdata.Add(fobjfdt);
            chrtBooking.ChartAreas[0].AxisX.Title = " Date of Request";
            chrtBooking.ChartAreas[0].AxisY.Title = " No of Request ";
        }
        if (quertype.ToLower() == "tenreq")
        {
            fdata = objViewstatistics.Getreq(fromdate, todate, "1");
            chrtBooking.ChartAreas[0].AxisX.Title = " Date of Request";
            chrtBooking.ChartAreas[0].AxisY.Title = " No of Request ";
        }
        if (quertype.ToLower() == "tenreq1")
        {
            fdata = objViewstatistics.Getreq(fromdate, todate, "2");
            chrtBooking.ChartAreas[0].AxisX.Title = " Date of Request";
            chrtBooking.ChartAreas[0].AxisY.Title = " No of Request ";
        }
        if (quertype.ToLower() == "tenreq2")
        {
            fdata = objViewstatistics.Getreq(fromdate, todate, "3");
            chrtBooking.ChartAreas[0].AxisX.Title = " Date of Request";
            chrtBooking.ChartAreas[0].AxisY.Title = " No of Request ";
        }

        var datasource = fdata.ToList();



        if (quertype.ToLower() != "month")
        {
            chrtBooking.Series["SeriesBooking"].ChartType = SeriesChartType.Point;
            TextAnnotation maxText = new TextAnnotation();
            chrtBooking.Series["SeriesBooking"].Points.DataBind(datasource, "currentdate", "conversion", string.Empty);

            //	chrtBooking.ChartAreas[0].AxisX.TitleAlignment = StringAlignment.Near;
            chrtBooking.ChartAreas[0].AxisX.TextOrientation = TextOrientation.Horizontal;
            chrtBooking.ChartAreas[0].AxisY.TextOrientation = TextOrientation.Rotated90;

        }
        else
        {
            chrtBooking.Series["SeriesBooking"].Points.DataBind(datasource, "currentdate", "Booking", string.Empty);

        }

        if (quertype.ToLower() == "tenreq" || quertype.ToLower() == "tenreq1" || quertype.ToLower() == "tenreq2")
        {
            chrtBooking.Series["SeriesBooking"].ChartType = SeriesChartType.Line;
            chrtBooking.Series["SeriesBooking"].Points.DataBind(datasource, "currentdate", "Booking", string.Empty);
            chrtBooking.ChartAreas[0].AxisX.Title = " Date of Request ";
            chrtBooking.ChartAreas[0].AxisY.Title = " No of Request ";
        }

        // chrtBooking.ChartAreas[0].AxisX.LineWidth = 0; 

        //	chrtBooking.ChartAreas[0].AxisX.TitleAlignment = StringAlignment.Near;
        chrtBooking.ChartAreas[0].AxisX.TextOrientation = TextOrientation.Horizontal;
        chrtBooking.ChartAreas[0].AxisY.TextOrientation = TextOrientation.Rotated270;
        divgraph.Style.Add("display", "block");
        divlist.Style.Add("display", "none");

    }
    protected void lnkGraphday_Click(object sender, EventArgs e)
    {
        Session["command"] = "lnkGraphday_Click";
        BindLstViewHotel("TODAY");
        string frmdt = System.DateTime.Now.ToString("dd/MM/yyyy");
        string todt = System.DateTime.Now.ToString("dd/MM/yyyy");
        lblgrphfrom.Text = "Graph for Today Booking";
        Bindlist(frmdt, todt, "day");
        trTodayBooking.BgColor = "#E3F0F1";
        trCurrentMonth.BgColor = "#ffffff";
        trCurrentYearBooking.BgColor = "#ffffff";
        trTotalRequest.BgColor = "#ffffff";
        trTentative.BgColor = "#ffffff";
        trNoAction.BgColor = "#ffffff";
        trOnlineRequest.BgColor = "#ffffff";
    }
    protected void lnkGraphMonth_Click(object sender, EventArgs e)
    {
        Session["command"] = "lnkGraphMonth_Click";
        BindLstViewHotel("MONTH");
        string frmdt = frmdt = System.DateTime.Now.AddDays(-(System.DateTime.Now.Day)).ToString("dd/MM/yyyy");
        // System.DateTime.Now.AddDays(-14).ToString("dd/MM/yyyy");
        string todt = System.DateTime.Now.ToString("dd/MM/yyyy");
        lblgrphfrom.Text = "Graph for Current Month Booking";
        Bindlist(frmdt, todt, "month");
        trTodayBooking.BgColor = "#ffffff";
        trCurrentMonth.BgColor = "#E3F0F1";
        trCurrentYearBooking.BgColor = "#ffffff";
        trTotalRequest.BgColor = "#ffffff";
        trTentative.BgColor = "#ffffff";
        trNoAction.BgColor = "#ffffff";
        trOnlineRequest.BgColor = "#ffffff";
    }
    protected void lnkGraphYear_Click(object sender, EventArgs e)
    {
        Session["command"] = "lnkGraphYear_Click";
        BindLstViewHotel("YEAR");
        string frmdt = System.DateTime.Now.AddDays(-(System.DateTime.Now.DayOfYear)).ToString("dd/MM/yyyy");
        string todt = System.DateTime.Now.ToString("dd/MM/yyyy");
        lblgrphfrom.Text = "Graph for Current Year Booking";
        Bindlist(frmdt, todt, "year");
        trTodayBooking.BgColor = "#ffffff";
        trCurrentMonth.BgColor = "#ffffff";
        trCurrentYearBooking.BgColor = "#E3F0F1";
        trTotalRequest.BgColor = "#ffffff";
        trTentative.BgColor = "#ffffff";
        trNoAction.BgColor = "#ffffff";
        trOnlineRequest.BgColor = "#ffffff";
    }


    protected void lnkTodayReqGraph_Click(object sender, EventArgs e)
    {
        Session["command"] = "lnkTodayReqGraph_Click";
        string frmdt = System.DateTime.Now.AddDays(-(System.DateTime.Now.DayOfYear)).ToString("dd/MM/yyyy");
        string todt = System.DateTime.Now.ToString("dd/MM/yyyy");
        lblgrphfrom.Text = "Graph for Today Request";
        Bindlist(frmdt, todt, "dayreq");
        trTodayBooking.BgColor = "#ffffff";
        trCurrentMonth.BgColor = "#ffffff";
        trCurrentYearBooking.BgColor = "#ffffff";
        trTotalRequest.BgColor = "#E3F0F1";
        trTentative.BgColor = "#ffffff";
        trNoAction.BgColor = "#ffffff";
        trOnlineRequest.BgColor = "#ffffff";
    }
    protected void lnkTotalTenReq_Click(object sender, EventArgs e)
    {
        Session["command"] = "lnkTotalTenReq_Click";
        string frmdt = System.DateTime.Now.AddDays(-(System.DateTime.Now.DayOfYear)).ToString("dd/MM/yyyy");
        string todt = System.DateTime.Now.ToString("dd/MM/yyyy");
        lblgrphfrom.Text = "Graph for Tentative Request";
        Bindlist(frmdt, todt, "TenReq");
        trTodayBooking.BgColor = "#ffffff";
        trCurrentMonth.BgColor = "#ffffff";
        trCurrentYearBooking.BgColor = "#ffffff";
        trTotalRequest.BgColor = "#ffffff";
        trTentative.BgColor = "#E3F0F1";
        trNoAction.BgColor = "#ffffff";
        trOnlineRequest.BgColor = "#ffffff";
    }
    #endregion
    protected void lnkReqGraphNoaction_Click(object sender, EventArgs e)
    {
        Session["command"] = "lnkReqGraphNoaction_Click";
        string frmdt = System.DateTime.Now.AddDays(-(System.DateTime.Now.DayOfYear)).ToString("dd/MM/yyyy");
        string todt = System.DateTime.Now.ToString("dd/MM/yyyy");
        lblgrphfrom.Text = "Graph for No Action Request";
        Bindlist(frmdt, todt, "TenReq1");
        trTodayBooking.BgColor = "#ffffff";
        trCurrentMonth.BgColor = "#ffffff";
        trCurrentYearBooking.BgColor = "#ffffff";
        trTotalRequest.BgColor = "#ffffff";
        trTentative.BgColor = "#ffffff";
        trNoAction.BgColor = "#E3F0F1";
        trOnlineRequest.BgColor = "#ffffff";

    }
    protected void lnkOnlinereq_Click(object sender, EventArgs e)
    {
        Session["command"] = "lnkOnlinereq_Click";
        string frmdt = System.DateTime.Now.AddDays(-(System.DateTime.Now.DayOfYear)).ToString("dd/MM/yyyy");
        string todt = System.DateTime.Now.ToString("dd/MM/yyyy");
        lblgrphfrom.Text = "Graph for Online Request";
        Bindlist(frmdt, todt, "TenReq2");
        trTodayBooking.BgColor = "#ffffff";
        trCurrentMonth.BgColor = "#ffffff";
        trCurrentYearBooking.BgColor = "#ffffff";
        trTotalRequest.BgColor = "#ffffff";
        trTentative.BgColor = "#ffffff";
        trNoAction.BgColor = "#ffffff";
        trOnlineRequest.BgColor = "#E3F0F1";
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetHotel(string prefixText)
    {
        HotelInfo ObjHotelinfo = new HotelInfo();
        string whereclauseforHotel = HotelColumn.IsRemoved + "=0 AND " + HotelColumn.Name + " Like '" + prefixText + "%'";
        TList<Hotel> THotel = ObjHotelinfo.GetHotelbyCondition(whereclauseforHotel, string.Empty);

        List<string> Names = new List<string>();
        for (int i = 0; i <= THotel.Count - 1; i++)
        {
            Names.Add(THotel[i].Name.ToString());
        }
        return Names;
    }
    protected void grdViewBooking_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Session["command"] = null;
        try
        {
            divbookingdetails.Visible = false;
            pnlBookingGrid.Visible = true;
            grdViewBooking.PageIndex = e.NewPageIndex;
            ViewState["CurrentPageBookingList"] = e.NewPageIndex;
            if (ViewState["HotelIdForPagingIndex"] != null)
            {
                long hotelId = Convert.ToInt64(ViewState["HotelIdForPagingIndex"]);
                grdViewBookingBind(hotelId);
            }
            ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + pnlBookingGrid.ClientID + "');});", true);
        }
        catch (Exception ex)
        {

        }
    }

    protected void grdViewRequest_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Session["command"] = null;
        try
        {
            DivrequestDetail.Visible = false;
            pnlRequestGrid.Visible = true;
            grdViewRequest.PageIndex = e.NewPageIndex;
            ViewState["CurrentPageRequestList"] = e.NewPageIndex;
            if (ViewState["HotelIdForPagingIndexRequest"] != null)
            {
                long hotelId = Convert.ToInt64(ViewState["HotelIdForPagingIndexRequest"]);
                grdViewRequestBind(hotelId);
            }
            ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + pnlRequestGrid.ClientID + "');});", true);
        }
        catch (Exception ex)
        {

        }
    }
}