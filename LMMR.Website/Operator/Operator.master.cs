﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using LMMR.Business;
using LMMR.Entities;
using System.Web.UI.HtmlControls;
using System.Xml;
using LMMR;

public partial class Operator_Operator : System.Web.UI.MasterPage
{
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            if (string.IsNullOrEmpty(appRootUrl) || appRootUrl == "/")
            {
                return host + "/";
            }
            else
            {
                return host + appRootUrl + "/";
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentSalesPersonID"] != null)
        {
            masterbody.Attributes.Remove("class");
        }
        else
        {
            masterbody.Attributes.Add("class", "bg");
        }

        if (!IsPostBack)
        {

            if (Session["CurrentOperatorID"] == null && Session["CurrentSalesPersonID"] == null)
            {
                //Response.Redirect("~/login.aspx");
                Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                if (l != null)
                {
                    Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                }
                else
                {
                    Response.Redirect(SiteRootPath + "login/english");
                }
            }

            if (Session["CurrentSalesPersonID"] != null)
            {
                Users objUsers = (Users)Session["CurrentSalesPerson"];
                 if (objUsers.Usertype == (int)Usertype.Salesperson)
                 {
                     lblLoginUserName.Text = objUsers.FirstName + " " + objUsers.LastName;
                     objUsers.LastLogin = DateTime.Now;
                     lstLoginTime.Text = DateTime.Now.ToLongDateString();
                     Page.Title = "Sales Master";
                     masterbody.Attributes.Remove("class");
                     Divtopmenu.Style.Add("margin-bottom", "20px");
                     topmenu1.Visible = false;
                     topmenu2.Visible = false;
                     topmenu3.Visible = true;
                 }
                 else
                 {
                     Session.Remove("CurrentSalesPersonID");
                     Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                     if (l != null)
                     {
                         Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                     }
                     else
                     {
                         Response.Redirect(SiteRootPath + "login/english");
                     }
                 }
            }

            if (Session["CurrentOperator"] != null)
            {
                Session.Remove("CurrentSalesPersonID");
                Session.Remove("CurrentSalesPerson");
                Users objUsers = (Users)Session["CurrentOperator"];
                if (objUsers.Usertype == (int)Usertype.Operator)
                {
                    lblLoginUserName.Text = objUsers.FirstName + " " + objUsers.LastName;
                    objUsers.LastLogin = DateTime.Now;
                    lstLoginTime.Text = DateTime.Now.ToLongDateString();
                    Page.Title = "Operator Master";
                    topmenu1.Visible = true;
                    topmenu2.Visible = false;
                    topmenu3.Visible = false;
                    masterbody.Attributes.Add("class","bg");
                    Divtopmenu.Style.Remove("margin-bottom");
                }
                //Add for superadmin console while entering client-contract page
                else if (objUsers.Usertype == (int)Usertype.Superadmin)
                {
                    lblLoginUserName.Text = objUsers.FirstName + " " + objUsers.LastName;
                    objUsers.LastLogin = DateTime.Now;
                    lstLoginTime.Text = DateTime.Now.ToLongDateString();
                    topmenu1.Visible = false;
                    topmenu2.Visible = true;
                    topmenu3.Visible = false;
                    Page.Title = "SuperAdmin Master";
                    masterbody.Attributes.Add("class", "bg");
                    Divtopmenu.Style.Remove("margin-bottom");
                }
                else
                {
                    Session.Remove("CurrentOperator");
                    //Response.Redirect("../Login.aspx");
                    Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                    if (l != null)
                    {
                        Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                    }
                    else
                    {
                        Response.Redirect(SiteRootPath + "login/english");
                    }
                }
            }
        }
    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        Session.Remove("CurrentOperator");
        Session.Remove("CurrentOperatorID");
        Session.Remove("CurrentUserID");
        Session.Remove("CurrentUser");
        Session.Remove("CurrentHotelID");
        //Session.Abandon();
        //Response.Redirect("~/login.aspx");
        Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "login/english");
        }
    }
}
