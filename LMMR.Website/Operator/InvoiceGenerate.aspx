<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InvoiceGenerate.aspx.cs" EnableEventValidation="false" Inherits="Operator_InvoiceGenerate" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <title>Operator Invoice</title>
    <link href="../css/style.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqtransform.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" src="../js/jquery.jqtransform_drp.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script language="javascript" type="text/javascript">
        $(function () {
            jQuery('#radioKM input:radio').addClass('NoClassApply');
            jQuery('#radioKM input:radio').width(20);
            jQuery('.main_conta iner1').jqTransform({ imgPath: ' ' });
        });

    </script>
    <style type="text/css">
        .NoClassApply
        {
            width: 204px;
            padding: 2px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">   
    
       <%-- <div class="main_container1"> <div class="main_body">--%>                                
     <%--<div class="search-booking">               
                <div class="search-booking-rowmain clearfix" style="width: 950px">--%>                    
                    <%--Manas Div--%>
                    <div runat="server" id="Divdetails" visible="false">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" >                              
                            <tr bgcolor="#FFFFFF">
                                <td colspan="3" align="left">
                                <table width="100%">
                                 <tr bgcolor="#FFFFFF">
                                <td width="100%">
                                <img alt="" id="ContentPlaceHolder1_Image1" runat="server" />
                                </td>                                                        
                               </tr> 
                                </table>
                                </td>                                
                               </tr>                                                  
                             <tr bgcolor="#FFFFFF">
                                <td colspan="3" align="left">
                                <table width="100%">
                                 <tr bgcolor="#FFFFFF">
                                <td width="60%">&nbsp;</td>
                                <td width="40%" style="padding: 10px 10px; font-size: 12px; line-height: 25px"
                                    align="left" valign="top">
                                    <asp:Label ID="Label3" runat="server" Text="Hotel Name : " Font-Bold="true"></asp:Label>
                                    <asp:Label ID="lblVenueName" runat="server" Text=""></asp:Label><br />                                    
                                    <asp:Label ID="Label11" runat="server" Text="Hotel Address : " Font-Bold="true"></asp:Label><asp:Label
                                        ID="lblClientAddress" runat="server" Text=""></asp:Label><br />
                                    <asp:Label ID="Label13" runat="server" Text="Phone : " Font-Bold="true"></asp:Label><asp:Label
                                        ID="lblClientPhone" runat="server" Text=""></asp:Label><br />
                                </td>                                
                               </tr> 
                                </table>
                                </td>                                
                               </tr>                      
                             <tr bgcolor="#FFFFFF">
                                <td style="padding: 10px 10px; font-size: 25px;font-weight:bold;font-family:Times New Roman;font-style:italic;" align="left"
                                    colspan="9">
                                    <asp:Label ID="Label" runat="server" Text="Invoice number : " Font-Bold="true"></asp:Label>&nbsp;&nbsp;<asp:Label
                                        ID="lblInvoiceNumber" runat="server" Text=""></asp:Label>                                                                                
                                </td>
                            </tr>
                            <tr bgcolor="#CCD8D8">
                            <td colspan="9" style="border-top: #A3D4F7 solid 1px;">
                            <table width="100%" cellpadding="10">
                            <tr>
                                <td style="font-size: 14px;" align="left">
                                    <asp:Label ID="Label4" runat="server" Text="Client Number: " Font-Bold="true"></asp:Label>&nbsp;&nbsp;<asp:Label
                                        ID="lblClientCode" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="font-size: 14px;" align="left">
                                    <asp:Label ID="Label6" runat="server" Text="VAT Number: " Font-Bold="true"></asp:Label>&nbsp;&nbsp;<asp:Label
                                        ID="lblClientVat" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="font-size: 14px;" align="left">
                                    <asp:Label ID="Label8" runat="server" Text="Invoice Date: " Font-Bold="true"></asp:Label>&nbsp;&nbsp;
                                    <asp:Label ID="lblInvoiceDate" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            </table>
                            </td>
                            </tr>
                            <tr bgcolor="#FFFFFF">
                                <td colspan="9" style="border-top: #A3D4F7 solid 1px; padding: 10px 10px; font-weight: bold;
                                    font-size: 14px;" align="center">
                                    Period :
                                    <asp:Label ID="lblFrom" runat="server" Text=""></asp:Label>
                                    -
                                    <asp:Label ID="lblTo" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr bgcolor="#FFFFFF">
                                <td colspan="9" style="border-top: #A3D4F7 solid 1px; padding: 10px 10px; font-weight: bold">
                                    Booking
                                </td>
                            </tr>
                            <tr>
                                <td colspan="9">                                    
                                        <asp:GridView ID="grvBooking" runat="server" Width="100%" AutoGenerateColumns="False"
                                            RowStyle-HorizontalAlign="Left" CellPadding="6"
                                            OnRowDataBound="grvBooking_RowDataBound" EmptyDataText="No record Found!" EmptyDataRowStyle-Font-Bold="true"
                                            EmptyDataRowStyle-HorizontalAlign="Center" EditRowStyle-VerticalAlign="Top" GridLines="None"
                                            PageSize="10" BackColor="#ffffff">
                                            <Columns>
                                                <asp:TemplateField HeaderText="#">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Company">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCompany" runat="server" Text='<%# Eval("Usertype ") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Contact Person">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblContact" runat="server" Text='<%# Eval("Contact ") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Booking Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBookingDate" runat="server" Text='<%# Eval("BookingDate","{0:dd/MM/yy}")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Departure Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDepartureDt" runat="server" Text='<%# Eval("DepartureDate","{0:dd/MM/yy}")%>'></asp:Label>
                                                        <asp:HiddenField ID="hdnArrivalDate" runat="server" Value='<%# Eval("ArrivalDate","{0:dd/MM/yy}")%>'/>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                               
                                                <asp:TemplateField HeaderText="Net Value">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRevenueAmount" runat="server"  Text='<%# String.Format("{0:###,###,###}",Eval("ConfirmRevenueAmount")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Commission %">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCommision" runat="server" Text=""></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Value(Euro)">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFinalValue" runat="server" Text=""></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#CCD8D8" />
                                            <RowStyle BackColor="#E3F0F1" />
                                            <SelectedRowStyle BackColor="Yellow" />
                                            <AlternatingRowStyle BackColor="#ffffff" />
                                        </asp:GridView>                                    
                                </td>
                            </tr>
                            <tr bgcolor="#FFFFFF">
                                <td colspan="9" style="border-top: #A3D4F7 solid 1px; padding: 10px 10px; font-weight: bold">
                                    Requests
                                </td>
                            </tr>
                            <tr>
                                <td colspan="9">                                    
                                        <asp:GridView ID="grvRequest" runat="server" Width="100%" AutoGenerateColumns="False"
                                            RowStyle-HorizontalAlign="Left" RowStyle-VerticalAlign="Middle" CellPadding="6"
                                            OnRowDataBound="grvRequest_RowDataBound" EmptyDataText="No record Found!" EmptyDataRowStyle-Font-Bold="true"
                                            EmptyDataRowStyle-HorizontalAlign="Center" EditRowStyle-VerticalAlign="Top" GridLines="None"
                                            PageSize="10" BackColor="#ffffff">
                                            <Columns>
                                                <asp:TemplateField HeaderText="#">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Company">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCompany" runat="server" Text='<%# Eval("Usertype ") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Contact Person">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblContact" runat="server" Text='<%# Eval("Contact ") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Booking Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBookingDate" runat="server" Text='<%# Eval("BookingDate","{0:dd/MM/yy}")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Departure Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDepartureDt" runat="server" Text='<%# Eval("DepartureDate","{0:dd/MM/yy}")%>'></asp:Label>
                                                        <asp:HiddenField ID="hdnArrivalDate" runat="server" Value='<%# Eval("ArrivalDate","{0:dd/MM/yy}")%>'/>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                              
                                                <asp:TemplateField HeaderText="Net Value">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRevenueAmount" runat="server"  Text='<%# String.Format("{0:###,###,###}",Eval("ConfirmRevenueAmount")) %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Commission %">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCommision" runat="server" Text=""></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Value(Euro)">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFinalValue" runat="server" Text=""></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#CCD8D8" />
                                            <RowStyle BackColor="#E3F0F1" />
                                            <SelectedRowStyle BackColor="Yellow" />
                                            <AlternatingRowStyle BackColor="#ffffff" />
                                        </asp:GridView>                                    
                                </td>
                            </tr>
                        </table>
                        <table class="teblewidth" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF"
                            style="margin-top: 20px" id="divTotal" runat="server" visible="false">
                            <tr bgcolor="#CCD8D8">
                                <td style="border-top: #A3D4F7 solid 1px; padding: 10px 10px; font-size: 14px;" align="left" width="25%">
                                    <asp:Label ID="Label1" runat="server" Text="Total VAT Excl " Font-Bold="true"></asp:Label>&nbsp;&nbsp;
                                </td>
                                <td style="border-top: #A3D4F7 solid 1px; padding: 10px 10px; font-size: 14px;" align="left" width="25%">
                                    <asp:Label ID="Label5" runat="server" Text="VAT %" Font-Bold="true"></asp:Label>&nbsp;&nbsp;
                                </td>
                                <td style="border-top: #A3D4F7 solid 1px; padding: 10px 10px; font-size: 14px;" align="left" width="25%">
                                    <asp:Label ID="Label9" runat="server" Text="Total VAT" Font-Bold="true"></asp:Label>&nbsp;&nbsp;                                    
                                </td>
                                 <td style="border-top: #A3D4F7 solid 1px; padding: 10px 10px; font-size: 14px;" align="right" width="25%">
                                    <asp:Label ID="Label2" runat="server" Text="Total Eur " Font-Bold="true"></asp:Label>                                 
                                </td>
                            </tr>
                            <tr bgcolor="#CCD8D8">
                                <td style="border-top: #A3D4F7 solid 1px; padding: 10px 10px; font-size: 13px;" align="left">
                                    <asp:Label ID="lblTotalValue" runat="server" Text="0.00"></asp:Label>&nbsp;&nbsp;
                                </td>
                                <td style="border-top: #A3D4F7 solid 1px; padding: 10px 10px; font-size: 13px;" align="left">
                                    <asp:Label ID="Label10" runat="server" Text="21.0" Font-Bold="true"></asp:Label>&nbsp;&nbsp;
                                </td>
                                <td style="border-top: #A3D4F7 solid 1px; padding: 10px 10px; font-size: 13px;" align="left">
                                    <asp:Label ID="lblTotalVat" runat="server" Text="0.00" Font-Bold="true"></asp:Label>&nbsp;&nbsp;                                    
                                </td>
                                 <td style="border-top: #A3D4F7 solid 1px; padding: 10px 10px; font-size: 13px;" align="right">
                                   <asp:Label ID="lblFinalTotal" runat="server" Text="0.00" Font-Bold="true"></asp:Label>                                   
                                </td>
                            </tr>  
                            <tr height="40%">
                                <td colspan="4"></td>
                            </tr>                            
                            <tr bgcolor="#CCD8D8">
                                <td colspan="4" style="border-bottom: #A3D4F7 solid 1px; padding-top: 30px;">
                                    <table width="100%">
                                        <tr bgcolor="#FFFFFF">
                                            <td style="padding: 10px 10px; font-size: 16px;" align="left" width="60%">
                                                Please use the following number for communication &nbsp;&nbsp;
                                            </td>
                                            <td style="padding: 10px 10px; font-size: 16px;" align="right" width="40%">
                                                <asp:Label ID="lblUniqueNumber" runat="server" Text="003/6611/66077" Font-Bold="true"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                            <td colspan="4">
                            <asp:Label ID="lblTotalRequest" runat="server" Text="0.00" Visible="false"></asp:Label>
                            <asp:Label ID="lblTotalBooking" runat="server" Text="0.00" Visible="false"></asp:Label>
                            </td>
                            </tr>  
                            <tr height="90%">
                                <td colspan="4"></td>
                            </tr> 
                            <tr bgcolor="#FFFFFF">
                                <td colspan="4" style="border-top: #A3D4F7 solid 1px;" align="left">
                                    <table width="100%">
                                        <tr bgcolor="#FFFFFF">
                                            <td style="font-size: 16px; text-align: left;" align="left" width="100%">
                                                Please refer to the General Conditions in the Finance section for more information<br />
                                                Banking details:<br />
                                                IBAN BE79 0016 5970 2433<br />
                                                BIC GEBABEBB
                                                <br />
                                            </td>
                                        </tr>
                                    </table>
                                </td>                                 
                            </tr>                        
                        </table>
                    </div>
                    <%--Manas Div--%>
               <%-- </div>                
            </div>      --%>              
       <%-- </div></div>--%>
    </form>
               
</body>
</html>
