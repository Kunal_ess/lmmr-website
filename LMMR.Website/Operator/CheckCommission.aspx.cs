﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LMMR.Business;
using LMMR.Entities;
using System.Text.RegularExpressions;
using log4net;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Web.UI.DataVisualization.Charting;

public partial class Operator_CancelTransfer : System.Web.UI.Page
{
    #region Variable
    ILog logger = log4net.LogManager.GetLogger(typeof(Operator_CancelTransfer));  
    VList<Viewbookingrequest> Vlistreq = new VList<Viewbookingrequest>();
    VList<Viewbookingrequest> Vlistcount = new VList<Viewbookingrequest>();
    ViewBooking_Hotel objViewBooking = new ViewBooking_Hotel();
    HotelManager objHotelManager = new HotelManager();
    MeetingRoomDescManager objMeeting = new MeetingRoomDescManager();
    Viewstatistics objViewstatistics = new Viewstatistics();
    List<Fetchdata> fdata = new List<Fetchdata>();
    string hotelid = "0";


    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentOperatorID"] == null)
        {
            Response.Redirect("~/login.aspx", true);
        }
        
        SearchPanel1.SearchButtonClick += new EventHandler(Search_Button);
        SearchPanel1.CancelButtonClick += new EventHandler(Cancel_Button);
        SearchPanel1.rdbBookingClick += new EventHandler(Booking_SelectedIndex);
        if (!IsPostBack)
        {
            ViewState["SearchAlpha"] = "all";
            SearchPanel1.propBookRef = 0;
            BindBookingDetails();

        }
        else
        {
            FirePrviousCommand();
        }
       // BindBookingDetails();
        ApplyPaging();  
    }
    #endregion

    #region search
    protected void Cancel_Button(object sender, EventArgs e)
    {
        ViewState["SearchAlpha"] = "all";
        BindBookingDetails();
        ApplyPaging();
    }
    protected void Search_Button(object sender, EventArgs e)
    {
        ViewState["SearchAlpha"] = "all";
        BindBookingDetails();
        ApplyPaging();
    }
    protected void Booking_SelectedIndex(object sender, EventArgs e)
    {
        string whereclaus = string.Empty;
        string orderby = "Id DESC";
        if (SearchPanel1.propBookRef == 0)
        {
            lblType.Text = "Booking#";
            lblDate.Text = "Booking date";
            whereclaus += "booktype = 0   and ( IsComissionDone=0 or  IsComissionDone is null)";

           
        }
        else
        {
            lblType.Text = "Request#";
            lblDate.Text = "Request date";
            whereclaus += "requeststatus in (5,7,2,4) and Booktype in (1,2)  and  ( IsComissionDone=0 or  IsComissionDone is null) ";
        }
        Vlistreq = objViewBooking.Bindgrid(whereclaus, orderby);
        lblTotalBooking.Text = Convert.ToString(Vlistreq.Count);
        grvBookingDetails.DataSource = Vlistreq;
        grvBookingDetails.DataBind();
        #region fetch hotel id
        
        foreach (Viewbookingrequest obj in Vlistreq)
        {
            hotelid += " ," + obj.HotelId;


        }
        Session["fetchHotelId"] = hotelid;
        #endregion

        #region getBooking&req count
        whereclaus = string.Empty;
        orderby = "Id DESC";
        if (!string.IsNullOrEmpty(whereclaus.Trim()))
        {
            whereclaus += " and ";
        }
        whereclaus += "booktype = 0   and IsComissionDone=0";
        Vlistcount = objViewBooking.Bindgrid(whereclaus, orderby);
        lblTodayBooking.Text = Convert.ToString(Vlistcount.Count);

        whereclaus = string.Empty;
        if (!string.IsNullOrEmpty(whereclaus.Trim()))
        {
            whereclaus += " and ";
        }
        string status = Enum.GetName(typeof(BookingRequestStatus), Convert.ToInt32(5)) + "," + Enum.GetName(typeof(BookingRequestStatus), Convert.ToInt32(7))+","+ Enum.GetName(typeof(BookingRequestStatus), Convert.ToInt32(2))+","+ Enum.GetName(typeof(BookingRequestStatus), Convert.ToInt32(4));
        whereclaus += "requeststatus in ('"+status+"') and Booktype in (1,2)  and IsComissionDone=0";
        Vlistcount = objViewBooking.Bindgrid(whereclaus, orderby);
        lblTodayRequest.Text = Convert.ToString(Vlistcount.Count);
        #endregion
        Session["WhereClause"] = Vlistreq;
        Session["WhereClauseTemp"] = Vlistreq;
        ViewState["SearchAlpha"] = "all";
        ApplyPaging();
        DivrequestDetail.Visible = false;
        divbookingdetails.Visible = false;
    }

    private void ApplyPaging()
    {
        try
        {            
            GridViewRow row = grvBookingDetails.TopPagerRow;
            if (row != null)
            {
                PlaceHolder ph;
                LinkButton lnkPaging;
                LinkButton lnkPrevPage;
                LinkButton lnkNextPage;
                lnkPrevPage = new LinkButton();
                lnkPrevPage.CssClass = "pre";
                lnkPrevPage.Width = Unit.Pixel(73);
                lnkPrevPage.CommandName = "Page";
                lnkPrevPage.CommandArgument = "prev";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPrevPage);
                if (grvBookingDetails.PageIndex == 0)
                {
                    lnkPrevPage.Enabled = false;

                }
                for (int i = 1; i <= grvBookingDetails.PageCount; i++)
                {
                    lnkPaging = new LinkButton();
                    if (ViewState["CurrentPage"] != null)
                    {
                        if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    else
                    {
                        if (i == 1)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    lnkPaging.Width = Unit.Pixel(16);
                    lnkPaging.Text = i.ToString();
                    lnkPaging.CommandName = "Page";
                    lnkPaging.CommandArgument = i.ToString();
                    if (i == grvBookingDetails.PageIndex + 1)
                        ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkPaging);
                }
                lnkNextPage = new LinkButton();
                lnkNextPage.CssClass = "nex";
                lnkNextPage.Width = Unit.Pixel(42);
                lnkNextPage.CommandName = "Page";
                lnkNextPage.CommandArgument = "next";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkNextPage);
                ph = (PlaceHolder)row.FindControl("ph");
                if (grvBookingDetails.PageIndex == grvBookingDetails.PageCount - 1)
                {
                    lnkNextPage.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    protected void grvBookingDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            grvBookingDetails.PageIndex = e.NewPageIndex;
            ViewState["CurrentPage"] = e.NewPageIndex;
            grvBookingDetails.DataSource = Session["WhereClauseTemp"];
            grvBookingDetails.DataBind();
            ApplyPaging();
        }
        catch (Exception ex)
        {

        }
    }
    #region PageAlpha
    public void PageChange(object sender, EventArgs e)
    {
        try
        {
            HtmlAnchor anc = (sender as HtmlAnchor);
            anc.Style.Add("class", "select");
            var alpha = anc.InnerHtml;
            string get = alpha.Trim();
            ViewState["SearchAlpha"] = get;
            VList<Viewbookingrequest> objBooking = new VList<Viewbookingrequest>();
            objBooking = (VList<Viewbookingrequest>)Session["WhereClause"];
            ViewState["CurrentPage"] = null;
            if (get != "all")
            {
                Session["WhereClauseTemp"] = objBooking.FindAll(a => a.HotelName.ToLower().StartsWith(get.ToLower()));
                grvBookingDetails.DataSource = Session["WhereClauseTemp"];
                grvBookingDetails.DataBind();
                lblTotalBooking.Text = Convert.ToString(grvBookingDetails.Rows.Count);
                ApplyPaging();
            }
            else
            {
                Session["WhereClauseTemp"] = objBooking;
                grvBookingDetails.DataSource = objBooking;
                grvBookingDetails.DataBind();
                lblTotalBooking.Text = Convert.ToString(objBooking.Count);
                ApplyPaging();
            }            

        }
        catch (Exception ex)
        {
            //logger.Error(ex);
        }
    }
    #endregion
    #endregion

    #region BindGrid for Booking
    /// <summary>
    /// Method to bind the grid
    /// </summary>    
    protected void BindBookingDetails()
    {
        try
        {
            string whereclaus=string.Empty;
            string orderby = "Id DESC";
            if (!string.IsNullOrEmpty(SearchPanel1.propBookRefNumber))
            {
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += ViewbookingrequestColumn.Id + "=" + SearchPanel1.propBookRefNumber + " ";
            }
            if (SearchPanel1.propHotelId != 0)
            {
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += ViewbookingrequestColumn.HotelId + "=" + SearchPanel1.propHotelId + " ";
            }
            if (SearchPanel1.propMeetingRoomId != 0)
            {
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += ViewbookingrequestColumn.MainMeetingRoomId + "=" + SearchPanel1.propMeetingRoomId + " ";
            }
            if (SearchPanel1.propCountryID != 0)
            {
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += ViewbookingrequestColumn.CountryId + "=" + SearchPanel1.propCountryID + " ";
            }
            if (SearchPanel1.propCityID != 0)
            {
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += ViewbookingrequestColumn.CityId + "=" + SearchPanel1.propCityID + " ";
            }

            Session["cntwhere"] = whereclaus;
            

            if (SearchPanel1.propBookRef == 0)
            {                
                lblType.Text="Booking#";
                lblDate.Text = "Booking date";
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += "booktype = 0   and ( IsComissionDone=0 or  IsComissionDone is null) and InvoiceId= null";                
            }
            else
            {
                lblType.Text="Request#";
                lblDate.Text = "Request date";
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += "requeststatus in (7)  and ( IsComissionDone=0 or  IsComissionDone is null) and Booktype in (1,2) ";
            }
            Vlistreq = objViewBooking.Bindgrid(whereclaus, orderby);
            grvBookingDetails.DataSource = Vlistreq;//.OrderBy(a => a.Id);
            grvBookingDetails.DataBind();

            #region fetch hotel id

            foreach (Viewbookingrequest obj in Vlistreq)
            {
                hotelid += " ," + obj.HotelId;


            }
            Session["fetchHotelId"] = hotelid;
            #endregion
            #region getBooking&req count
            whereclaus = Convert.ToString(Session["cntwhere"]);
            orderby = "Id DESC";
            if (!string.IsNullOrEmpty(whereclaus.Trim()))
            {
                whereclaus += " and ";
            }
            whereclaus += "booktype = 0   and   ( IsComissionDone=0 or  IsComissionDone is null) and InvoiceId= null";
            Vlistcount = objViewBooking.Bindgrid(whereclaus, orderby);
            lblTodayBooking.Text = Convert.ToString(Vlistcount.Count);

            whereclaus = Convert.ToString(Session["cntwhere"]);
            if (!string.IsNullOrEmpty(whereclaus.Trim()))
            {
                whereclaus += " and ";
            }
            whereclaus += "requeststatus in (7) and Booktype in (1,2)  and  ( IsComissionDone=0 or  IsComissionDone is null)";
            Vlistcount = objViewBooking.Bindgrid(whereclaus, orderby);
            lblTodayRequest.Text = Convert.ToString(Vlistcount.Count);
            #endregion

            

            lblTotalBooking.Text = Convert.ToString(Vlistreq.Count);
            Session["WhereClause"] = Vlistreq;
            Session["WhereClauseTemp"] = Vlistreq;
            DivrequestDetail.Visible = false;
            divbookingdetails.Visible = false;
            divgraph.Style.Add("display", "none");
            divlist.Style.Add("display", "block");

        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

  

    #region Open popup for Cancel/Transfer
    /// <summary>
    /// when check commsion was checked
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  
    protected void btnchkcommision_Click(object sender, EventArgs e)
    {
        try
        {

            LinkButton btnchkcomm = (LinkButton)sender;
            ViewState["ChkcommBookingID"] = btnchkcomm.CommandArgument;
            divCancel.Style.Add("display", "block");
            divTransfer.Style.Add("display", "none");
            if (btnchkcomm.Text.ToLower() == "change status")
            {                
                ModalPopupCheck.Show();
                if (SearchPanel1.propBookRef == 1)
                {
                    drpStatus.Visible = false;
                    ddlstatus.Visible = true;
                    ViewState["popup"] = "1";
                }
                else
                {
                    ViewState["popup"] = "0";
                    drpStatus.Visible = true;
                    ddlstatus.Visible = false;
                }
            }            
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    //Open popup for transfer booking
    protected void btntransfer_Click(object sender, EventArgs e)
    {
        try
        {

            LinkButton btnTransfer = (LinkButton)sender;
            ViewState["ChkcommBookingID"] = btnTransfer.CommandArgument;
            divTransfer.Style.Add("display", "block");
            divCancel.Style.Add("display", "none");
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
            if (btnTransfer.Text.ToLower() == "transfer")
            {
                ModalPopupCheck.Show();
                if (SearchPanel1.propBookRef == 0)
                {
                    drpInventory.DataSource = objMeeting.GetAllMeetingRoom(Convert.ToInt32(btnTransfer.CommandName)).OrderBy(a => a.Name);
                    drpInventory.DataTextField = "Name";
                    drpInventory.DataValueField = "Id";
                    drpInventory.DataBind();                    
                    drpInventory.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--Select Inventory--", ""));
                    txtFromdate.Text = "dd/mm/yy";
                }               
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    #endregion

    #region Gridview Events
    protected void grvBookingDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
            {
                Label lblCity = (Label)e.Row.FindControl("lblCity");
                HiddenField hdnCityId = (HiddenField)e.Row.FindControl("hdnCityId");
                City objCity = objHotelManager.GetByCityID(Convert.ToInt32(hdnCityId.Value));
                lblCity.Text = objCity.City;
                Label lblArrivalDt = (Label)e.Row.FindControl("lblArrivalDt");
                LinkButton lblRefNo = (LinkButton)e.Row.FindControl("lblRefNo");
                Label lblDepartureDt = (Label)e.Row.FindControl("lblDepartureDt");
                Label lblExpiryDt = (Label)e.Row.FindControl("lblExpiryDt");
                LinkButton btnchkcommision = (LinkButton)e.Row.FindControl("btnchkcommision");
                Label lblBookingDt = (Label)e.Row.FindControl("lblBookingDt");
                Label lblstatus = (Label)e.Row.FindControl("lblstatus");
                DateTime startTime = DateTime.Now;
                string depdt = lblDepartureDt.Text;
                LinkButton btnchkcommision1 = (LinkButton)e.Row.FindControl("btnchkcommision1");
                Label lblComm = (Label)e.Row.FindControl("lblComm");
                #region Bindrequeststaus
                lblstatus.Text = Enum.GetName(typeof(BookingRequestStatus), Convert.ToInt32(lblstatus.ToolTip));
                #endregion

                DateTime endTime = new DateTime(Convert.ToInt32(depdt.Split('/')[2]), Convert.ToInt32(depdt.Split('/')[1]), Convert.ToInt32(depdt.Split('/')[0]));
                TimeSpan span = endTime.Subtract(startTime);
                int spn = span.Days;


                if (lblComm.Text.ToLower() == "false" || string.IsNullOrEmpty(lblComm.Text))
                {
                    ((System.Web.UI.WebControls.Image)e.Row.FindControl("imgCheck")).Visible = false;
                }
                else
                {
                    ((System.Web.UI.WebControls.Image)e.Row.FindControl("imgCheck")).Visible = true;
                }

                #region frozen
                if (lblstatus.Text == BookingRequestStatus.Frozen.ToString())
                {
                    //lblRefNo.Enabled = false;
                    e.Row.BackColor = System.Drawing.Color.LightGray;                
                }
                if (lblstatus.Text == BookingRequestStatus.Expired.ToString())
                {
                    //  lblRefNo.Enabled = false;
                    e.Row.BackColor = System.Drawing.Color.LightGray;
                }
                #endregion



                //if (Convert.ToDateTime(endTime.ToString("dd/MM/yyyy")) > Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy")))
                //{
                //    btnchkcommision1.Visible = false;
                //}

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    protected void grvBookingDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            Booking objbooking = objViewBooking.getparticularbookingdetails(Convert.ToInt64(e.CommandArgument));

            SearchPanel1.propBookRef = Convert.ToInt32( objbooking.BookType);
            if (Convert.ToString(e.CommandName) == "ViewBookingDetail")
            {

                if (SearchPanel1.propBookRef == 0)
                {
                    divbookingdetails.Visible = true;
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + divbookingdetails.ClientID + "');});", true);
                    DivrequestDetail.Visible = false;
                    ucViewBookingDetail.BindBooking(Convert.ToInt32(e.CommandArgument));
                }
                else
                {
                    ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
                  Booking obj=        objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(e.CommandArgument));
                  if (obj.BookType == 1)
                  {
                      requestDetails.BindHotel(Convert.ToInt32(e.CommandArgument));
                      DivrequestDetail.Visible = true;
                      divbookingdetails.Visible = false;
                      BookingDetails1.Visible = false;
                      requestDetails.Visible = true;
                      ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + DivrequestDetail.ClientID + "');});", true);
                  }
                  else if (obj.BookType == 2)
                  {
                      BookingDetails1.Visible = true;
                      BookingDetails1.BindBooking(Convert.ToInt32(e.CommandArgument));
                      requestDetails.Visible = false;
                      DivrequestDetail.Visible = true;
                      divbookingdetails.Visible = false;
                      ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + DivrequestDetail.ClientID + "');});", true);
                  }
                    //requestDetails.BindHotel(Convert.ToInt32(e.CommandArgument));
                    //DivrequestDetail.Visible = true;
                    //divbookingdetails.Visible = false;
                    //ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + DivrequestDetail.ClientID + "');});", true);
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }
    #endregion

    #region Change Status for booking/request
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            Booking objbooking = objViewBooking.getparticularbookingdetails(Convert.ToInt64(ViewState["ChkcommBookingID"].ToString()));
            if (Convert.ToString(ViewState["popup"]) == "1")
            {
                objbooking.RequestStatus = Convert.ToInt32(ddlstatus.SelectedValue);
                if (objViewBooking.updatecheckComm(objbooking))
                {
                    ModalPopupCheck.Hide();
                    BindBookingDetails();
                }
            }
            else if (Convert.ToString(ViewState["popup"]) == "0")
            {                
                objViewBooking.CancelBooking(Convert.ToInt64(ViewState["ChkcommBookingID"].ToString()));
                ModalPopupCheck.Hide();
                BindBookingDetails();             
            }
           

        }
        catch (Exception ex)
        {
            logger.Error(ex);           
        }

    }

    protected void lnkTransfer_Click(object sender, EventArgs e)
    {
        try
        {
            Booking objbooking = objViewBooking.getparticularbookingdetails(Convert.ToInt64(ViewState["ChkcommBookingID"].ToString()));
            //if (Convert.ToString(ViewState["popup"]) == "1")
            //{
            //    objbooking.RequestStatus = Convert.ToInt32(ddlstatus.SelectedValue);
            //    if (objViewBooking.updatecheckComm(objbooking))
            //    {
            //        ModalPopupCheck.Hide();
            //        BindBookingDetails();
            //    }
            //}
            //else if (Convert.ToString(ViewState["popup"]) == "0")
            //{
            //    objViewBooking.CancelBooking(Convert.ToInt64(ViewState["ChkcommBookingID"].ToString()));
            //    ModalPopupCheck.Hide();
            //    BindBookingDetails();
            //}


        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }
    #endregion

    #region Cancel Button Click
    /// <summary>
    /// Modal popup cancel event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {            
            ModalPopupCheck.Hide();
            ddlstatus.SelectedIndex = 0;           
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    #endregion
    

    #region PDF
    protected void lnkSavePDF_Click(object sender, EventArgs e)
    {
        try
        {
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=UserDetails.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            PrepareGridViewForExport(Divdetails);
            Divdetails.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }




    }

    #region VerifyRenderingInServerForm
    /// <summary>
    /// method to VerifyRenderingInServerForm
    /// </summary>

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    #endregion

    #region PrepareGridViewForExport
    /// <summary>
    /// method to PrepareGridViewForExport
    /// </summary>
    private void PrepareGridViewForExport(Control gv)
    {
        try
        {
            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {

                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(DropDownList))
                {

                    l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(CheckBox))
                {

                    l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(HyperLink))
                {

                    l.Text = (gv.Controls[i] as HyperLink).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.Image))
                {

                    // l.Text = (gv.Controls[i] as Image).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareGridViewForExport(gv.Controls[i]);

                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }
    #endregion
    #region PrepareGridViewForExportDIV
    /// <summary>
    /// method to PrepareGridViewForExport a div
    /// </summary>
    private void PrepareDivForExport(Control gv)
    {
        try
        {

            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {

                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].GetType() == typeof(System.Web.UI.HtmlControls.HtmlAnchor))
                {

                    l.Text = (gv.Controls[i] as System.Web.UI.HtmlControls.HtmlAnchor).InnerText;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(DropDownList))
                {

                    l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(GridView))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    // gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Repeater))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Panel))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Table))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }


                else if (gv.Controls[i].GetType() == typeof(CheckBox))
                {

                    l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(HyperLink))
                {

                    l.Text = (gv.Controls[i] as HyperLink).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                //else if (gv.Controls[i].GetType() == typeof(Label))
                //{

                //    l.Text = (gv.Controls[i] as Label).Text;

                //    gv.Controls.Remove(gv.Controls[i]);

                //    gv.Controls.AddAt(i, l);

                //}
                else if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.Image))
                {

                    // l.Text = (gv.Controls[i] as Image).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareDivForExport(gv.Controls[i]);

                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
        //PrepareGridViewForExport();
        //PrepareGridViewForExport();

    }


    #endregion

    protected void lnkSaveRequestPdf_Click(object sender, EventArgs e)
    {
        try
        {
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=UserDetails.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            PrepareGridViewForExport(DivdetailsRequest);
            DivdetailsRequest.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }
    #endregion
    #region Link and graph
    protected void lnkViewTodayBooking_Click(object sender, EventArgs e)
    {
        divgraph.Style.Add("display", "none");
        divlist.Style.Add("display", "block");
        ViewState["SearchAlpha"] = "all";
        SearchPanel1.propBookRef = 0;

        BindBookingDetails();
        ApplyPaging();
    }
    protected void lnkTodayRequest_Click(object sender, EventArgs e)
    {

        divgraph.Style.Add("display", "none");
        divlist.Style.Add("display", "block");
        ViewState["SearchAlpha"] = "all";
        SearchPanel1.propBookRef = 1;
        BindBookingDetails();
        SearchPanel1.FillHotelMeetingRoom();
        ApplyPaging();
    }
    protected void lnkGraphday_Click(object sender, EventArgs e)
    {
        Session["command"] = "lnkGraphday_Click";
        string frmdt = frmdt = System.DateTime.Now.AddDays(-(System.DateTime.Now.DayOfYear)).ToString("dd/MM/yyyy");
        // System.DateTime.Now.AddDays(-14).ToString("dd/MM/yyyy");
        string todt = System.DateTime.Now.ToString("dd/MM/yyyy");
        lblgrphfrom.Text = "Graph for Current Month Booking-Unchecked";
        SearchPanel1.propBookRef = 0;
        Bindlist(frmdt, todt, "1");
    }
    protected void lnkTodayReqGraph_Click(object sender, EventArgs e)
    {
        Session["command"] = "lnkTodayReqGraph_Click";
        string frmdt = frmdt = System.DateTime.Now.AddDays(-(System.DateTime.Now.DayOfYear)).ToString("dd/MM/yyyy");
        // System.DateTime.Now.AddDays(-14).ToString("dd/MM/yyyy");
        string todt = System.DateTime.Now.ToString("dd/MM/yyyy");
        lblgrphfrom.Text = "Graph for Current Month Request-Unchecked";
        SearchPanel1.propBookRef = 1;
        Bindlist(frmdt, todt, "2");
    }

    public void Bindlist(string Fromdt, string Todt, string type)
    {

        //hid+=decimal.

        objViewstatistics.HotelId = Session["fetchHotelId"].ToString();

        string quertype = type;
        DateTime fromdate = new DateTime(Convert.ToInt32(Fromdt.Split('/')[2]), Convert.ToInt32(Fromdt.Split('/')[1]), Convert.ToInt32(Fromdt.Split('/')[0]));
        DateTime todate = new DateTime(Convert.ToInt32(Todt.Split('/')[2]), Convert.ToInt32(Todt.Split('/')[1]), Convert.ToInt32(Todt.Split('/')[0]));





        if (SearchPanel1.propBookRef == 0)
        {
            fdata = objViewstatistics.GetUncheckComm(fromdate, todate, "1");
            chrtBooking.ChartAreas[0].AxisX.Title = " Date of Booking";
            chrtBooking.ChartAreas[0].AxisY.Title = " No of Booking (Unchecked Commission) ";
        }

        if (SearchPanel1.propBookRef == 1)
        {
            fdata = objViewstatistics.GetUncheckComm(fromdate, todate, "2");
            chrtBooking.ChartAreas[0].AxisX.Title = " Date of Request";
            chrtBooking.ChartAreas[0].AxisY.Title = " No of Request (Unchecked Commission) ";
        }

        
        var datasource = fdata.ToList();




        chrtBooking.Series["SeriesBooking"].ChartType = SeriesChartType.Line;
        chrtBooking.Series["SeriesBooking"].Points.DataBind(datasource, "currentdate", "Booking", string.Empty);

        divgraph.Style.Add("display", "block");
        divlist.Style.Add("display", "none");

    }
    #endregion

    public void FirePrviousCommand()
    {
        var view = Session["command"];
        if (Session["command"] != null)
        {
           
            if (Session["command"] == "lnkGraphday_Click")
            {
                lnkGraphday_Click(lnkGraphday, null);
            }
          
            //-----------------------------
            if (Session["command"] == "lnkTodayReqGraph_Click")
            {
                lnkTodayReqGraph_Click(lnkTodayReqGraph, null);
            }
            
            //  Session["command"] = null;
        }
    }
}