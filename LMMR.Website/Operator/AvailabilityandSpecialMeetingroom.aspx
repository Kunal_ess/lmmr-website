﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Operator/Operator.master" AutoEventWireup="true" CodeFile="AvailabilityandSpecialMeetingroom.aspx.cs" Inherits="Operator_AvailabilityandSpecialMeetingroom" %>
<%@ Register Src="~/UserControl/Operator/SearchPanel.ascx" TagName="Search" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="operator-mainbody">
<uc1:Search ID="search" runat="server" />
<div id="operator_mainbody" style="float:left;">
<div class="overlay-Availability"><img src="../Images/ajax-loader.gif" style="margin: 25% 0px 0px 0px; z-index: 1999" /><br /><span>Loading...</span></div>
                <div class="contract-list" style="border-bottom:none;border-right:none;" >
<div style="width: 900px;" class="contract-list-left">
                    <h2>
                        Availability and special monitoring</h2>
                </div></div>
                <div class="pager_header1-new">
                    <div class="page_text-left">
                        <a href="javascript:void(0);" id="prev" style="display:none;">
                            <img src="../images/prev_bullet.png" alt="prev"> Previous</a></div>
                    <div class="page_text-right" >
                        <a href="javascript:void(0);" id="next" style="display:none;">Next<img src="../images/next_bullet.png"></a></div>
                    
                </div>
                <!-- start meeting room table-->
                <table width="960" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" class="room">
                    <tr>
                        <td valign="top">
                            <div style="float:left;width:960px;background-color:White;">
                            <asp:Literal ID="ltrTableCalander" runat="server" ></asp:Literal>
                            </div>
                        </td>
                    </tr>
                </table>
                <!-- end meeting room table-->
            </div>
            </div>
<div id="adjust-meeting-overlay" style="z-index:53;">
    </div>
<div id="adjust-AvailabilitySpecial" style="z-index:55;">
        <div class="popup-top1-AvailabilitySpecial">
        </div>
        <div class="popup-mid1-AvailabilitySpecial">
            <table cellpadding="5" cellspacing="5" width="90%" style="padding:5px 20px 5px 20px;">
                <tr>
                    <td><h2>Manage sort order:</h2></td>
                </tr>
                <tr>
                    <td><table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td rowspan="2"><asp:ListBox ID="lstSort" runat="server" Width="240px" Rows="5" >
                        </asp:ListBox></td><td valign="top"><input type="image" src="../Images/up.png" id="btn-up" /></td>
                        </tr>
                        <tr><td valign="bottom"><input type="image" src="../Images/down.png" id="btn-down"/></td></tr>
                    </table>
                        
                    </td>
                </tr>
                <tr><asp:HiddenField ID="hdnOrder" runat="server" />
                    <td style="text-align:center;" align="center">
                        <div class="button_section">
                        <asp:LinkButton ID="lnkSort" runat="server" OnClick="lnkSort_Click" OnClientClick="return checkOrder();" CssClass="select" >Save</asp:LinkButton>
                        &nbsp;or&nbsp;
                        <a href="javascript:void(0)" onclick="return closeDiv();" class="cancelpop">Cancel</a>
                    </div></td>
                </tr>
            </table>
        </div>
        <div class="popup-bottom1-AvailabilitySpecial">
        </div>
    </div>
<script language="javascript" type="text/javascript">
    var count=0;
    jQuery(document).ready(function () {
        ManageAvailability('');
        jQuery("#prev").bind("click", function () {            
            ManageAvailability('p')
        });
        jQuery("#next").bind("click", function () {
            ManageAvailability('n')
        });
    });

    function ManageAvailability(type) {
        var OffSetMail = jQuery("#operator_mainbody").offset();
        var mainWidth = jQuery("#operator_mainbody").width();
        var mainHeight = jQuery("#operator_mainbody").height();
        jQuery(".overlay-Availability").css({ "margin-top":"41px","left": OffSetMail.left + "px", "top": OffSetMail.top + "px", "width": 961 + "px", "height": mainHeight + "px" });
        jQuery(".overlay-Availability").show();
        var tblWidth = jQuery("#myTable").width();
        var tblOffset = jQuery("#myTable").offset();
        var cssLeft = parseInt(jQuery("#myTable").css("left"), 10);
        var tblOffsetP = jQuery("#myTable").parent().offset();

        if (type == 'p') {
            count--;
//            alert(count);
//            alert(tblWidth);
//            alert(960 * count)
            var moveright = (tblWidth > 960 * count) ? 960 * count : (960 * count - tblWidth);
            jQuery(".raw1 span").animate({ left: moveright + "px" }, "slow", function () { });
            jQuery("#myTable").animate({ left: (-1 * (moveright)) + "px" }, "slow", function () {
                if (tblWidth > 960) {
                    cssLeft = parseInt(jQuery("#myTable").css("left"), 10);
                    if ((isNaN(cssLeft) ? 0 : cssLeft) < 0) {
                        jQuery("#prev").show();
                    }
                    else {
                        jQuery("#prev").hide();
                    }

                    if (tblWidth + (isNaN(cssLeft) ? 0 : cssLeft) > 960) {
                        jQuery("#next").show();
                    }
                    else {
                        jQuery("#next").hide();
                    }
                }
                else {
                    jQuery("#next").hide();
                    jQuery("#prev").hide();
                }
                jQuery(".overlay-Availability").hide();
            });
            
            //jQuery("#myTable").css("left", "" + (cssLeft + (tblWidth - 960)) + "px");
        }
        else if (type == 'n') {
            count++;
//            alert(count);
//            alert(tblWidth);
//            alert(960 * count)
            var moveleft = (tblWidth > 960 * count && (tblWidth - (960 * count)) >= 960) ? 960 * count : (tblWidth + ((960 * count) - tblWidth));
            //alert(tblWidth + " :tblWidth - " + moveleft + " :moveleft - " + count + " :count- " + (960 * count) + " :(960 * count)- " + ((960 * count) - tblWidth) + " :((960 * count) - tblWidth).");
            jQuery(".raw1 span").animate({ left: (moveleft - 30)  + "px" }, "slow", function () { });
            jQuery("#myTable").animate({ left: -1 * moveleft+30 + "px" }, "slow", function () {
                if (tblWidth > 960) {
                    cssLeft = parseInt(jQuery("#myTable").css("left"), 10);
                    if ((isNaN(cssLeft) ? 0 : cssLeft) < 0) {
                        jQuery("#prev").show();
                    }
                    else {
                        jQuery("#prev").hide();
                    }

                    if (tblWidth + (isNaN(cssLeft) ? 0 : cssLeft) > 960) {
                        jQuery("#next").show();
                    }
                    else {
                        jQuery("#next").hide();
                    }
                }
                else {
                    jQuery("#next").hide();
                    jQuery("#prev").hide();
                }
                jQuery(".overlay-Availability").hide();
            });
            
            //jQuery("#myTable").css("left", "-" + (tblWidth-960) + "px");
        }
        else {
            if (tblWidth > 960) {

                if ((isNaN(cssLeft) ? 0 : cssLeft) < 0) {
                    jQuery("#prev").show();
                }
                else {
                    jQuery("#prev").hide();
                }
                if (tblWidth + (isNaN(cssLeft) ? 0 : cssLeft) > 960) {
                    jQuery("#next").show();
                }
                else {
                    jQuery("#next").hide();
                }
            }
            else {
                jQuery("#next").hide();
                jQuery("#prev").hide();
            }
            jQuery(".overlay-Availability").hide();
        }
    }
</script>
<script language="javascript" type="text/javascript">
    function openDiv() {
        window.scroll(0, 0);
        document.getElementsByTagName('html')[0].style.overflow = 'hidden';
        document.getElementById('adjust-AvailabilitySpecial').style.display = 'block';
        document.getElementById('adjust-meeting-overlay').style.display = 'block';
        return false;
    }
    function closeDiv() {
        document.getElementsByTagName('html')[0].style.overflow = 'auto';
        document.getElementById('adjust-AvailabilitySpecial').style.display = 'none';
        document.getElementById('adjust-meeting-overlay').style.display = 'none';
        return false;
    }
    function checkOrder() {
        jQuery("#<%= hdnOrder.ClientID %>").val('');
        var order = '';
        jQuery('#<%= lstSort.ClientID %> option').each(function () {
            order += jQuery(this).val() + ",";
        });
        if (order.length > 0) {
            order = order.substring(0, order.length - 1);
        }
        jQuery("#<%= hdnOrder.ClientID %>").val(order);
    }

    jQuery(document).ready(function () {
        jQuery('#btn-up').bind('click', function () {
            jQuery('#<%= lstSort.ClientID %> option:selected').each(function () {
                var newPos = jQuery('#<%= lstSort.ClientID %> option').index(this) - 1;
                if (newPos > -1) {
                    jQuery('#<%= lstSort.ClientID %> option').eq(newPos).before("<option value='" + jQuery(this).val() + "' selected='selected'>" + jQuery(this).text() + "</option>");
                    jQuery(this).remove();
                }
            });
            return false;
        });
        jQuery('#btn-down').bind('click', function () {
            var countOptions = jQuery('#<%= lstSort.ClientID %> option').size();
            jQuery('#<%= lstSort.ClientID %> option:selected').each(function () {
                var newPos = jQuery('#<%= lstSort.ClientID %> option').index(this) + 1;
                if (newPos < countOptions) {
                    jQuery('#<%= lstSort.ClientID %> option').eq(newPos).after("<option value='" + jQuery(this).val() + "' selected='selected'>" + jQuery(this).text() + "</option>");
                    jQuery(this).remove();
                }
            });
            return false;
        });


    });
    var arrayids = <%= Arraylst %>;
    function CallMealways()
    {
        for(i=0;i<arrayids.length;i++)
        {
            jQuery.ajax({
            type: "POST",
            url: "<%=SiteRootPath %>LMMRwebservice.asmx/BindCalanderWithListWhere",
            data: "whereClause=ViewAvailabilityAndSpecialManager.id="+arrayids[i]+" AND <%=WhereClauses %>&startdate=<%=startDate %>&enddate=<%=endDate %>",
            success: function (response) { 
                var id= jQuery(response).find("FinalResultForAvailability").find("HotelID").text();

                jQuery(jQuery(response).find("FinalResultForAvailability").find("HotelString").text()).insertAfter("#tblrow" +id);
            } 
            });
        }
    }
</script>
</asp:Content>

