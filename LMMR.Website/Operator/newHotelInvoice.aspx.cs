﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Data;
using System.Configuration;
using log4net;
using log4net.Config;
using Microsoft.Practices.EnterpriseLibrary.Data;
#endregion

public partial class Operator_newHotelInvoice : System.Web.UI.Page
{
    #region variable declaration
    FinanceInvoice objFinance = new FinanceInvoice();
    VList<Viewbookingrequest> vlistreq;
    ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
    HotelInfo ObjHotelinfo = new HotelInfo();
    ILog logger = log4net.LogManager.GetLogger(typeof(Operator_newHotelInvoice));
    HotelManager objHotel = new HotelManager();
    ClientContract objClient = new ClientContract();
    EmailConfigManager em = new EmailConfigManager();
    ManageOthers objOthers = new ManageOthers();
    #endregion

    #region PageLoad
    /// <summary>
    /// On page load we manage all the containts and Apply paging if there is a post back.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        divmessage.Style.Add("display", "none");
        divmessage.Attributes.Add("class", "error");

        try
        {

            if (Session["CurrentOperatorID"] == null)
            {
                Response.Redirect("~/login.aspx", true);
            }

            if (IsPostBack)
            {
                ApplyPaging();
            }
            else if (!IsPostBack)
            {

                countryBind();
                BindFinanceInvoice();
                BindDropdownlist();

                //Check whether invoicevarify checked in others table or not                
                TList<Others> othVerify = objFinance.GetInvoiceVerify();
                if (Convert.ToByte(othVerify[0].InvoiceVerify) == 1)
                {
                    // divRedbtn.Visible = false;
                }                
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    #region Additional Methods
    /// <summary>
    //Bind finance invoice details of selected hotel into list view
    /// </summary> 
    void BindFinanceInvoice()
    {
        try
        {
            Divdetails.Visible = false;
            int countryId = Convert.ToInt32(countryDDL.SelectedValue);
            TList<Hotel> lsthCount;
            if (countryId != 0)
            {
                string wherecla = "CountryId='" + countryId + "'";
                lsthCount = ObjHotelinfo.GetHotelbyCondition(wherecla, String.Empty);
            }
            else
            {
                lsthCount = ObjHotelinfo.GetAllHotel();
            }
            if (lsthCount.Count == 0)
            {
                grvFinanceInvoice.DataSource = lsthCount;
                grvFinanceInvoice.DataBind();
                return;
            }

            string whereclause = InvoiceColumn.HotelId + " in (";
            foreach (Hotel cl in lsthCount)
            {
                whereclause += cl.Id + ",";
            }
            if (whereclause.Length > 0)
            {
                whereclause = whereclause.Substring(0, whereclause.Length - 1);
            }
            whereclause += ")";
            if (whereclause != "HotelId in )")
            {                
                TList<Invoice> lstInvoice = objFinance.GetinvoicebyClient(whereclause);
                grvFinanceInvoice.Visible = true;
                grvFinanceInvoice.DataSource = lstInvoice;
                grvFinanceInvoice.DataBind();
                if (lstInvoice.Count > 0)
                {
                    BindDropdownlist();
                    exportToExcel.Visible = true;
                    lbtSendInvoices.Visible = true;                    
                    divmessage.Style.Add("display", "none");
                    divmessage.Attributes.Add("class", "error");
                }
                else
                {
                    exportToExcel.Visible = false;
                    lbtSendInvoices.Visible = false; 
                    //lbtSendInvoices.Visible = false;
                }
            }
            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }



    /// <summary>
    /// This method binds the country DropDownList.
    /// </summary>
    public void countryBind()
    {
        //Bind country Dropdown for search 
        TList<Country> objCountry = objClient.GetByAllCountry();
        countryDDL.DataValueField = "Id";
        countryDDL.DataTextField = "CountryName";
        countryDDL.DataSource = objCountry.OrderBy(a => a.CountryName);
        countryDDL.DataBind();
        countryDDL.Items.Insert(0, new ListItem("All Country", "0"));
    }

    /// <summary>
    //Bind data in dropdown for Date and all hotelname
    /// </summary>
    void BindDropdownlist() 
    {
        try
        {
            GridViewRow header = grvFinanceInvoice.HeaderRow;

            //// Retrieve control for period
            DropDownList ddlPeriod = header.FindControl("drpPeriod") as DropDownList;
            ddlPeriod.DataValueField = "Display";
            ddlPeriod.DataTextField = "Display";
            ddlPeriod.DataSource = objFinance.GetInvoicebyCondition(String.Empty, String.Empty).Select(a => new { Display = Convert.ToDateTime(a.DateFrom).ToString("dd/MM/yyyy") + " - " + Convert.ToDateTime(a.DateTo).ToString("dd/MM/yyyy") }).Distinct();
            ddlPeriod.DataBind();
            ddlPeriod.Items.Insert(0, new ListItem("Select Period", ""));
            //// Retrieve control for period            


            DropDownList ddlHotelName = header.FindControl("drpHotelName") as DropDownList;                        

            var lstInvoice = objFinance.getAllInvoices().Select(a=> a.HotelIdSource).Distinct();
            ddlHotelName.DataValueField = "Id";
            ddlHotelName.DataTextField = "Name";
            ddlHotelName.DataSource = lstInvoice;
            ddlHotelName.DataBind();
            ddlHotelName.Items.Insert(0, new ListItem("All Hotel", ""));
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    /// <summary>
    //Start Get Bedroom details
    /// </summary>            
    protected void FillbedroomDetails()
    {
        try
        {
            TList<BookedBedRoom> objBookedBR = objViewBooking_Hotel.getbookedBedroom(Convert.ToInt64(Convert.ToString(ViewState["BookingID"])));

            foreach (BookedBedRoom br in objBookedBR)
            {
                lblBedroomType.Text = Enum.GetName(typeof(BedRoomType), br.BedRoomIdSource.Types); // Enum.GetName();
                lblChekOut.Text = String.Format("{0:dd/MM/yyyy}", br.CheckOut);
                lblCheckIn.Text = String.Format("{0:dd/MM/yyyy}", br.CheckIn);
                lblpersonName.Text = br.PersonName;
                lblNote.Text = br.Note;

                lblbedroomtotalprice.Text = String.Format("{0:#,##,##0.00}", br.Total);

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }

    /// <summary>
    //Call shorting function
    /// </summary>      
    protected void grvFinanceInvoice_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            ConvertSortDirectionToSql(e.SortExpression, e.SortDirection);
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    /// <summary>
    //Function for shorting on invoice date
    /// </summary>      
    public static SortDirection s = SortDirection.Ascending;
    private void ConvertSortDirectionToSql(string OrderBy, SortDirection sortDirection)
    {
        switch (s)
        {
            case SortDirection.Ascending:
                OrderBy += " ASC";
                s = SortDirection.Descending;
                break;

            case SortDirection.Descending:
                OrderBy += " DESC";
                s = SortDirection.Ascending;
                break;
        }
        string wherecon = "CountryId='" + countryDDL.SelectedValue + "'";
        grvFinanceInvoice.DataSource = objFinance.GetInvoicebyCondition(wherecon, OrderBy);
        grvFinanceInvoice.DataBind();
        BindDropdownlist();
        ApplyPaging();
    }


    /// <summary>
    //For Bind Plan,Images,CreditNote
    /// </summary>    
    protected void grvFinanceInvoice_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            foreach (GridViewRow gr in grvFinanceInvoice.Rows)
            {
                if (((LinkButton)gr.FindControl("lblPaid")).Text == "True")
                {
                    ((Image)gr.FindControl("imgStatus")).Visible = true;
                }
                else
                {
                    ((LinkButton)gr.FindControl("lblPaid")).Visible = true;
                    ((LinkButton)gr.FindControl("lblPaid")).Text = "Not Paid";
                }

                HiddenField hdf = ((HiddenField)gr.FindControl("hdfImage"));
                HyperLink PlanView = (HyperLink)gr.FindControl("linkviewPlan");
                HiddenField hdfcreditnote = ((HiddenField)gr.FindControl("hdfCreditnote"));
                HyperLink creditnote = ((HyperLink)gr.FindControl("linkCreditnote"));
                CheckBox chkCreditnote = (CheckBox)gr.FindControl("chkCreditnote");
                LinkButton lnbCreditnote = (LinkButton)gr.FindControl("lnbCreditnote");

                Image img = (Image)gr.FindControl("verifiedIMG");
                CheckBox chkInvoice = (CheckBox)gr.FindControl("chkInvoiceSent");
                if (img.AlternateText == "True")
                {
                    chkInvoice.Visible = false;
                    img.Visible = true;
                }
                else
                {
                    chkInvoice.Visible = true;
                    img.Visible = false;
                }


                if (hdf.Value != "")
                {
                    PlanView.NavigateUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "InvoiceFile/" + hdf.Value;
                }
                else
                {
                    PlanView.Visible = false;
                }
                if (hdfcreditnote.Value != "")
                {
                    creditnote.NavigateUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "InvoiceFile/" + hdfcreditnote.Value;
                    creditnote.Visible = true;
                    chkCreditnote.Visible = false;
                }
                else
                {
                    creditnote.Visible = false;
                }

                //For adding channel filed
                LinkButton lnbBookedId = (LinkButton)gr.FindControl("lblRefNo");
                if (lnbBookedId.ToolTip != "")
                {
                    Booking onjbooking = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(lnbBookedId.ToolTip));
                    ((Label)gr.FindControl("lblChannel")).Text = onjbooking.Channel;
                }
                //For adding channel filed
                lnbCreditnote.Attributes.Add("style", "display:none;");
                chkCreditnote.Attributes.Add("onclick", "return checkuncheck(this,'" + lnbCreditnote.ClientID + "');");

                //Bind country name on basis of hotel id
                Label lblCountry = (Label)gr.FindControl("lblCountryName");
                Label lblHotelId = (Label)gr.FindControl("lblHotelId");
                Hotel htlname = ObjHotelinfo.GetHotelByHotelID(Convert.ToInt32(lblHotelId.Text));
                lblCountry.Text = ObjHotelinfo.GetCountryByID(Convert.ToInt32(htlname.CountryId));

                //Show tooltip text for Invoice PDF file
                Label lblHotel = (Label)gr.FindControl("lblHotel");
                LinkButton lnbInvoiceNo = (LinkButton)gr.FindControl("lnbInvoiceNo");
                PlanView.ToolTip = lblHotel.Text + " _ " + lnbInvoiceNo.Text;

                //Check whether invoicevarify checked in others table or not
                CheckBox chkInvoiceSent = (CheckBox)gr.FindControl("chkInvoiceSent");
                TList<Others> othVerify = objFinance.GetInvoiceVerify();
                if (Convert.ToByte(othVerify[0].InvoiceVerify) == 1)
                {
                    chkInvoiceSent.Enabled = false;
                    lbtSendInvoices.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    /// <summary>
    //Bind Extra item grid for booking details
    /// </summary>  
    protected void rpmain_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                Label lblbmrid = e.Item.FindControl("lblbmrid") as Label;
                Label lblPackageName = e.Item.FindControl("lblPackageName") as Label;
                Label lbltotalpackage = e.Item.FindControl("lbltotalpackage") as Label;
                Label lblextratotal = e.Item.FindControl("lblextratotal") as Label;
                GridView grdDetailspackage = e.Item.FindControl("grdDetailspackage") as GridView;
                GridView grdextra = e.Item.FindControl("grdextra") as GridView;
                GridView grdequipment = e.Item.FindControl("grdequipment") as GridView;

                TList<BuildPackageConfigure> obbuildpack = objViewBooking_Hotel.getPackageDetails(Convert.ToInt64(lblbmrid.Text.Replace("'", "")));
                TList<BuildMeetingConfigure> objBuildMeetingConfigure = objViewBooking_Hotel.getextra(Convert.ToInt64(lblbmrid.Text.Replace("'", "")));

                grdextra.DataSource = objBuildMeetingConfigure.FindAll(a => a.PackageIdSource.IsExtra == true);
                grdextra.DataBind();
                grdequipment.DataSource = objBuildMeetingConfigure.FindAll(a => Convert.ToInt32(a.PackageIdSource.ItemType) == (int)ItemType.Equipment);
                grdequipment.DataBind();
                int cntextra = 0;
                foreach (BuildMeetingConfigure bmc in objBuildMeetingConfigure.FindAll(a => Convert.ToInt32(a.PackageIdSource.ItemType) == (int)ItemType.Equipment))
                {
                    cntextra += Convert.ToInt32(bmc.TotalPrice);

                }
                lblextratotal.Text = String.Format("{0:#,##,##0.00}", cntextra);
                foreach (BuildPackageConfigure bpc in obbuildpack)
                {

                    lblPackageName.Text = bpc.PackageItemIdSource.PackageName;
                    lbltotalpackage.Text = String.Format("{0:#,##,##0.00}", bpc.TotalPrice);


                    TList<BuildPackageConfigureDesc> obdesc = objViewBooking_Hotel.getpackagedetails(bpc.Id);
                    grdDetailspackage.DataSource = obdesc;
                    grdDetailspackage.DataBind();

                }


            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }

    #endregion

    #region Search Methods
    /// <summary>
    //Start Search function for client name and from-to date
    /// </summary>    
    protected void lbtSearch_Click(object sender, EventArgs e)
    {
        try
        {
            Divdetails.Visible = false;
            TList<Invoice> lstInvoice = new TList<Invoice>();
            int countryId = Convert.ToInt32(countryDDL.SelectedValue);
            string wherecla = "  1=1  ";
            if (countryId != 0)
            {
                TList<Hotel> obhotl = objFinance.GetHotelByCountryAndCity(countryId);
                wherecla += "and HotelId in (";
                foreach (Hotel h in obhotl)
                {
                    wherecla += h.Id + ",";
                }
                wherecla += "0)";
            }


            if (!string.IsNullOrEmpty(txtInvoiceNumber.Text))
            {
                wherecla += " and InvoiceNumber= '" + txtInvoiceNumber.Text + "'";
            }

            if (!string.IsNullOrEmpty(txtFromdate.Text) && !string.IsNullOrEmpty(txtTodate.Text))
            {
                if (txtFromdate.Text == txtTodate.Text)
                {
                    //DateTime fromDate = new DateTime(Convert.ToInt32("20" + txtFromdate.Text.Split('/')[2]), Convert.ToInt32(txtFromdate.Text.Split('/')[1]), Convert.ToInt32(txtFromdate.Text.Split('/')[0]));                    

                    wherecla += " and convert(varchar, DueDate,103)='" + txtFromdate.Text + "'";
                }
                else
                {
                    wherecla += " and DueDate between convert(DATETIME, '" + txtFromdate.Text + "',103) and convert(DATETIME , '" + txtTodate.Text + "',103) ";
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(txtFromdate.Text))
                    wherecla += " and  convert(DATETIME, '" + txtFromdate.Text + "',103) <= DueDate ";

                if (!string.IsNullOrEmpty(txtTodate.Text))
                    wherecla += " and  convert(DATETIME, '" + txtTodate.Text + "',103) >= DueDate ";
            }                        
            grvFinanceInvoice.DataSource = objFinance.GetinvoicebyClient(wherecla);
            grvFinanceInvoice.DataBind();
            BindDropdownlist();
            if (objFinance.GetinvoicebyClient(wherecla).Count == 0)
            {
                exportToExcel.Visible = false;
                lbtSendInvoices.Visible = false;
            }
            else
            {
                exportToExcel.Visible = true;
                lbtSendInvoices.Visible = true;
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
        //end Searching for from-to date and as per hotel also
    }

    /// <summary>
    //Generate index for paging
    /// </summary>       
    private void ApplyPaging()
    {
        GridViewRow row = grvFinanceInvoice.TopPagerRow;
        if (row != null)
        {
            PlaceHolder ph;
            LinkButton lnkPaging;
            LinkButton lnkPrevPage;
            LinkButton lnkNextPage;
            lnkPrevPage = new LinkButton();
            lnkPrevPage.CssClass = "pre";
            lnkPrevPage.Width = Unit.Pixel(73);
            lnkPrevPage.CommandName = "Page";
            lnkPrevPage.CommandArgument = "prev";
            ph = (PlaceHolder)row.FindControl("ph");
            ph.Controls.Add(lnkPrevPage);
            if (grvFinanceInvoice.PageIndex == 0)
            {
                lnkPrevPage.Enabled = false;

            }
            for (int i = 1; i <= grvFinanceInvoice.PageCount; i++)
            {
                lnkPaging = new LinkButton();
                if (ViewState["CurrentPage"] != null)
                {
                    if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                    {
                        lnkPaging.CssClass = "pag2";
                    }
                    else
                    {
                        lnkPaging.CssClass = "pag";
                    }
                }
                else
                {
                    if (i == 1)
                    {
                        lnkPaging.CssClass = "pag2";
                    }
                    else
                    {
                        lnkPaging.CssClass = "pag";
                    }
                }
                lnkPaging.Width = Unit.Pixel(16);
                lnkPaging.Text = i.ToString();
                lnkPaging.CommandName = "Page";
                lnkPaging.CommandArgument = i.ToString();
                if (i == grvFinanceInvoice.PageIndex + 1)
                    ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPaging);
            }
            lnkNextPage = new LinkButton();
            lnkNextPage.CssClass = "nex";
            lnkNextPage.Width = Unit.Pixel(42);
            lnkNextPage.CommandName = "Page";
            lnkNextPage.CommandArgument = "next";
            ph = (PlaceHolder)row.FindControl("ph");
            ph.Controls.Add(lnkNextPage);
            ph = (PlaceHolder)row.FindControl("ph");
            if (grvFinanceInvoice.PageIndex == grvFinanceInvoice.PageCount - 1)
            {
                lnkNextPage.Enabled = false;

            }
        }

    }


    /// <summary>
    //Get result when clicking on Period dropdown in Finance gridview header control
    /// </summary>           
    protected void drpPeriod_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");

            GridViewRow header = grvFinanceInvoice.HeaderRow;

            //// Retrieve control
            DropDownList ddlPeriod = header.FindControl("drpPeriod") as DropDownList;

            if (ddlPeriod.SelectedIndex == 0)
            {
                BindFinanceInvoice();
                BindDropdownlist();
                return;
            }
            else
            {
                grvFinanceInvoice.DataSource = objFinance.GetDateSearchForInvoice(ddlPeriod.SelectedItem.Text.Replace("'", ""));
                grvFinanceInvoice.DataBind();
                BindDropdownlist();
            }
            exportToExcel.Visible = true;
            lbtSendInvoices.Visible = true; 
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    /// <summary>
    //Get result when clicking on hotel name dropdown in Finance gridview header control
    /// </summary>    
    protected void drpHotelName_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            GridViewRow header = grvFinanceInvoice.HeaderRow;
            //// Retrieve control
            DropDownList drpHotelName = header.FindControl("drpHotelName") as DropDownList;
            if (drpHotelName.SelectedIndex == 0)
            {
                grvFinanceInvoice.DataSource = objFinance.GetInvoiceByHotelName();
                grvFinanceInvoice.DataBind();
            }
            else
            {
                grvFinanceInvoice.DataSource = objFinance.GetInvoiceByHotelid(Convert.ToInt32(drpHotelName.SelectedValue));
                grvFinanceInvoice.DataBind();                
            }
            BindDropdownlist();
            drpHotelName.SelectedIndex = drpHotelName.Items.IndexOf(drpHotelName.Items.FindByText(drpHotelName.SelectedItem.Text.Replace("'", "")));
            exportToExcel.Visible = true;
            lbtSendInvoices.Visible = true; 
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }



    #endregion

    #region Finance Invoice page index
    /// <summary>
    //Paging index for finance invoice grid
    /// </summary>    
    protected void grvFinanceInvoice_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grvFinanceInvoice.PageIndex = e.NewPageIndex;
        ViewState["CurrentPage"] = e.NewPageIndex;
        BindFinanceInvoice();
        BindDropdownlist();
    }
    #endregion

    #region Get Booking Information
    /// <summary>
    //Start Get all information of booking details when clicking on Invoice Number
    /// </summary>        
    protected void lblRefNo_Click(object sender, EventArgs e)
    {
        try
        {
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
            LinkButton lnk = (LinkButton)sender;

            ViewState["BookingID"] = lnk.ToolTip;
            Invoice IvnHId = objFinance.GetInvoiceByID(Convert.ToInt32(lnk.Text.Replace("'", "")));
            string whereclaus = "hotelid=" + IvnHId.HotelId + " and id=" + lnk.ToolTip; // change hotel ID make it dynamic
            string orderby = ViewBookingHotelsColumn.DepartureDate + " ASC";
            vlistreq = objViewBooking_Hotel.Bindgrid(whereclaus, orderby);

            Hotel htl = objViewBooking_Hotel.GetbyHotelID((Convert.ToInt32(lnk.ToolTip)));

            List<ViewMeetingroom> vlistmr = objViewBooking_Hotel.viewMR(Convert.ToInt64(Convert.ToString(ViewState["BookingID"])));

            rpmain.DataSource = vlistmr;
            rpmain.DataBind();

            Users obj = objViewBooking_Hotel.GetUser(Convert.ToInt64(Convert.ToString(ViewState["BookingID"])));
            TList<UserDetails> objDetails = objViewBooking_Hotel.getuserdetails(obj.UserId);
            lblConatctpersonEmail.HRef = "mailto:" + obj.EmailId;
            lblConatctpersonEmail.InnerText = obj.EmailId;
            lblContactPerson.Text = obj.FirstName + " " + obj.LastName;
            if (objDetails.Count > 0)
            {
                lblContactAddress.Text = objDetails[0].Address;
                lblContactPhone.Text = objDetails[0].Phone;
            }

            FillbedroomDetails();
            foreach (Viewbookingrequest vi in vlistreq)
            {
                #region Duration dates
                TList<BookedMeetingRoom> bookedMR = objViewBooking_Hotel.getbookedmeetingroom(Convert.ToInt64(lnk.Text.Replace("'", "")));
                #region ArrivalDate
                foreach (BookedMeetingRoom br in bookedMR)
                {
                    string day = "";
                    if (br.MeetingDay == 0)
                        day = "Morning";
                    if (br.MeetingDay == 1)
                        day = "Afternoon";
                    if (br.MeetingDay == 2)
                        day = "Full Day";

                    DateTime dtmeetingdt = (DateTime)br.MeetingDate;
                    lblBookedDays.Text = dtmeetingdt.ToString("dd/MM/yy") + " " + day;
                    lblFromDt.Text = dtmeetingdt.ToString("dd/MM/yy");
                    break;
                }
                #endregion

                #region DepartureDate
                if (bookedMR.Count == 1)
                {
                    foreach (BookedMeetingRoom br in bookedMR)
                    {
                        DateTime dtmeetingdt = (DateTime)br.MeetingDate;
                        lblToDate.Text = dtmeetingdt.ToString("dd/MM/yy");
                    }
                }
                if (bookedMR.Count > 1)
                {
                    foreach (BookedMeetingRoom br in bookedMR)
                    {
                        string day = "";
                        if (br.MeetingDay == 0)
                            day = "Morning";
                        if (br.MeetingDay == 1)
                            day = "Afternoon";
                        if (br.MeetingDay == 2)
                            day = "Full Day";
                        DateTime dtmeetingdt = (DateTime)br.MeetingDate;
                        lblBookedDays1.Text = dtmeetingdt.ToString("dd/MM/yy") + " " + day;

                        lblToDate.Text = dtmeetingdt.ToString("dd/MM/yy");
                    }
                }


                #endregion

                #endregion
                lblfinalprice.Text = String.Format("{0:#,##,##0.00}", vi.FinalTotalPrice);
                lblsplcomments.Text = Convert.ToString(vi.SpecialRequest);
            }
            Divdetails.Visible = true;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }
    #endregion

    #region Get Booking/Request Information after clicking on Invoice Number
    /// <summary>
    //Start Get all information of booking details when clicking on Invoice Number
    /// </summary> 
    protected void lnbInvoiceNo_Click(object sender, EventArgs e)
    {
        try
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + Divdetails.ClientID + "');});", true);
            lblTotalValue.Text = "0.00";
            lblTotalRequest.Text = "0.00";
            lblTotalBooking.Text = "0.00";
            lblFinalTotal.Text = "0.00";
            lblTotalVat.Text = "0.00";

            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
            LinkButton lnk = (LinkButton)sender;

            string whereclause = "HotelId='" + lnk.CommandName + "' and " + "Id='" + lnk.ToolTip + "' ";
            TList<Invoice> lstInvoice = objFinance.GetInvoicebyCondition(whereclause, String.Empty);

            DateTime fromDate = (DateTime)lstInvoice[0].DateFrom;
            DateTime toDate = (DateTime)lstInvoice[0].DateTo;
            DateTime invoiceDate = (DateTime)lstInvoice[0].DueDate;
            lblFrom.Text = fromDate.ToString("dd/MM/yyyy");
            lblTo.Text = toDate.ToString("dd/MM/yyyy");
            lblInvoiceDate.Text = invoiceDate.ToString("dd/MM/yyyy");
            lblInvoiceNumber.Text = lstInvoice[0].InvoiceNumber;
            lblUniqueNumber.Text = lstInvoice[0].InvoiceNumber;

            //Get Hotel client details
            Hotel hotel = ObjHotelinfo.GetHotelByHotelID(Convert.ToInt32(lnk.CommandName));
            lblClientAddress.Text = hotel.HotelAddress;
            lblClientPhone.Text = hotel.PhoneNumber;            
            lblClientCode.Text = hotel.ContractId;
            lblVenueName.Text = hotel.Name;
            TList<UserDetails> userdtls = ObjHotelinfo.GetUserDetails(Convert.ToInt32(hotel.ClientId));
            if (userdtls.Count > 0)
            {
                lblClientVat.Text = userdtls[0].VatNo;
            }
            //lblCTo.Text = userdtls[0].Name;

            // For Booking
            string whereclaus = "hotelid in (" + lnk.CommandName + ") and booktype = 0 and DepartureDate between '" + lstInvoice[0].DateFrom + "' and '" + lstInvoice[0].DateTo + "' and requeststatus=4";
            string orderby = ViewbookingrequestColumn.DepartureDate + " DESC";
            VList<Viewbookingrequest> vlistBooking = objViewBooking_Hotel.Bindgrid(whereclaus, orderby);
            grvBooking.DataSource = vlistBooking;
            grvBooking.DataBind();

            //For Request
            string whereclausreq = "hotelid in (" + lnk.CommandName + ") and Booktype in (1,2) and DepartureDate between '" + lstInvoice[0].DateFrom + "' and '" + lstInvoice[0].DateTo + "' and requeststatus=4 and IsComissionDone=1";
            string orderbyreq = ViewbookingrequestColumn.DepartureDate + " DESC";
            VList<Viewbookingrequest> vlistreq = objViewBooking_Hotel.Bindgrid(whereclausreq, orderbyreq);
            grvRequest.DataSource = vlistreq;
            grvRequest.DataBind();
            Divdetails.Visible = true;

            if (vlistBooking.Count == 0 && vlistreq.Count == 0)
            {
                divTotal.Visible = false;
            }
            else
            {
                divTotal.Visible = true;
            }

            lblTotalValue.Text = String.Format("{0:#,##,##0.00}", Convert.ToDecimal(lblTotalBooking.Text) + Convert.ToDecimal(lblTotalRequest.Text));
            if (ObjHotelinfo.GetCountryByID(Convert.ToInt32(hotel.CountryId)) == "Belgium")
            {
                lblTotalVat.Text = String.Format("{0:#,##,##0.00}", Convert.ToDecimal(lblTotalValue.Text) * 21 / 100);
                lblFinalTotal.Text = Convert.ToString(Convert.ToDecimal(lblTotalValue.Text) + Convert.ToDecimal(lblTotalVat.Text));
            }
            else
            {
                lblFinalTotal.Text = Convert.ToString(Convert.ToDecimal(lblTotalValue.Text) + Convert.ToDecimal(lblTotalVat.Text));
            }
            
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }

    /// <summary>
    //Start Get all information of booking details when clicking on Invoice Number and calculate net amount as per booking id
    /// </summary> 
    protected void grvBooking_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblId = (Label)e.Row.FindControl("lblId");
                // the replace check image wherver renvue is done
                Booking bookcomm = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(lblId.Text));


                #region Calculate commission as per date difference
                //Label lblBookingDate = (Label)e.Row.FindControl("lblBookingDate");
                //Label lblDepartureDt = (Label)e.Row.FindControl("lblDepartureDt");
                //HiddenField hdnArrivalDate = (HiddenField)e.Row.FindControl("hdnArrivalDate");
                //Label lblCommision = (Label)e.Row.FindControl("lblCommision");

                //DateTime fromDate = new DateTime(Convert.ToInt32(lblBookingDate.Text.Split('/')[2]), Convert.ToInt32(lblBookingDate.Text.Split('/')[1]), Convert.ToInt32(lblBookingDate.Text.Split('/')[0]));
                //DateTime todate = new DateTime(Convert.ToInt32(hdnArrivalDate.Value.Split('/')[2]), Convert.ToInt32(hdnArrivalDate.Value.Split('/')[1]), Convert.ToInt32(hdnArrivalDate.Value.Split('/')[0]), 23, 59, 59);
                //TimeSpan ts = todate - fromDate;
                //int days = ts.Days;                
                #endregion

                #region Dynamic set invoice
                //VList<ViewForSetInvoice> objView = objOthers.GetCommissionDetailsForHotelId(Convert.ToInt32(bookcomm.HotelId));
                //if (objView.Count > 0)
                //{
                //    for (int i = 0; i < objView.Count; i++)
                //    {
                //        if (days >= objView[i].FromDay && days <= objView[i].ToDay)
                //        {
                //            lblCommision.Text = objView[i].CommissionPercentage.ToString();
                //            break;
                //        }
                //    }
                //}
                //else
                //{
                //    TList<InvoiceCommission> objComm = objOthers.GetInvoiceCommission();
                //    for (int i = 0; i < objComm.Count; i++)
                //    {
                //        if (days >= objComm[i].FromDay && days <= objComm[i].ToDay)
                //        {
                //            lblCommision.Text = objComm[i].DefaultCommission.ToString();
                //            break;
                //        }
                //    }
                //}
                #endregion

                #region Calculate the final value i.e. confirmed value multiply lmmr commission
                Label lblCommision = (Label)e.Row.FindControl("lblCommision");
                Label lblFinalValue = (Label)e.Row.FindControl("lblFinalValue");
                Label lblRevenueAmount = (Label)e.Row.FindControl("lblRevenueAmount");

                if (bookcomm.Accomodation != null)
                {
                    lblCommision.Text = Convert.ToString(bookcomm.Accomodation);
                }
                
                if (lblRevenueAmount.Text != "" && lblRevenueAmount.Text != null)
                {
                    lblFinalValue.Text = String.Format("{0:#,##,##0.00}", Math.Round((Convert.ToDecimal(lblCommision.Text) * Convert.ToDecimal(lblRevenueAmount.Text) / 100), 2));
                }
                else
                {
                    lblFinalValue.Text = "0";
                }
                #endregion

                lblTotalBooking.Text = String.Format("{0:#,##,##0.00}", Math.Round((Convert.ToDecimal(lblFinalValue.Text) + Convert.ToDecimal(lblTotalBooking.Text)), 2));




            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    /// <summary>
    //Start Get all information of request details when clicking on Invoice Number and calculate net amount as per request id
    /// </summary> 
    protected void grvRequest_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblId = (Label)e.Row.FindControl("lblId");
                // the replace check image wherver renvue is done
                Booking bookcomm = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(lblId.Text));                

                #region Calculate commission as per date difference
                //Label lblBookingDate = (Label)e.Row.FindControl("lblBookingDate");
                //Label lblDepartureDt = (Label)e.Row.FindControl("lblDepartureDt");
                //HiddenField hdnArrivalDate = (HiddenField)e.Row.FindControl("hdnArrivalDate");
                //Label lblCommision = (Label)e.Row.FindControl("lblCommision");

                //DateTime fromDate = new DateTime(Convert.ToInt32(lblBookingDate.Text.Split('/')[2]), Convert.ToInt32(lblBookingDate.Text.Split('/')[1]), Convert.ToInt32(lblBookingDate.Text.Split('/')[0]));
                //DateTime todate = new DateTime(Convert.ToInt32(hdnArrivalDate.Value.Split('/')[2]), Convert.ToInt32(hdnArrivalDate.Value.Split('/')[1]), Convert.ToInt32(hdnArrivalDate.Value.Split('/')[0]), 23, 59, 59);
                //TimeSpan ts = todate - fromDate;
                //int days = ts.Days;                
                #endregion

                #region Dynamic set invoice
                //VList<ViewForSetInvoice> objView = objOthers.GetCommissionDetailsForHotelId(Convert.ToInt32(bookcomm.HotelId));
                //if (objView.Count > 0)
                //{
                //    for (int i = 0; i < objView.Count; i++)
                //    {
                //        if (days >= objView[i].FromDay && days <= objView[i].ToDay)
                //        {
                //            lblCommision.Text = objView[i].CommissionPercentage.ToString();
                //            break;
                //        }
                //    }
                //}
                //else
                //{
                //    TList<InvoiceCommission> objComm = objOthers.GetInvoiceCommission();
                //    for (int i = 0; i < objComm.Count; i++)
                //    {
                //        if (days >= objComm[i].FromDay && days <= objComm[i].ToDay)
                //        {
                //            lblCommision.Text = objComm[i].DefaultCommission.ToString();
                //            break;
                //        }
                //    }
                //}
                #endregion

                #region Calculate the final value i.e. confirmed value multiply lmmr commission
                Label lblFinalValue = (Label)e.Row.FindControl("lblFinalValue");
                Label lblCommision = (Label)e.Row.FindControl("lblCommision");
                Label lblRevenueAmount = (Label)e.Row.FindControl("lblRevenueAmount");
                if (bookcomm.Accomodation != null)
                {
                    lblCommision.Text = Convert.ToString(bookcomm.Accomodation);
                }
                
                if (lblRevenueAmount.Text != "" && lblRevenueAmount.Text != null)
                {
                    lblFinalValue.Text = String.Format("{0:#,##,##0.00}", Math.Round((Convert.ToDecimal(lblCommision.Text) * Convert.ToDecimal(lblRevenueAmount.Text) / 100), 2));
                }
                else
                {
                    lblFinalValue.Text = "0";
                }
                #endregion                
                lblTotalRequest.Text = String.Format("{0:#,##,##0.00}", Math.Round((Convert.ToDecimal(lblFinalValue.Text) + Convert.ToDecimal(lblTotalRequest.Text)), 2));
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    #region popUp

    protected void UploadBtn_Click(object sender, EventArgs e)
    {
        #region check file upload
        string strPlanName = string.Empty;
        if (fileUpload.HasFile)
        {
            string fileExtension = Path.GetExtension(fileUpload.PostedFile.FileName.ToString());

            if (fileExtension.ToLower() == ".pdf")
            {
                strPlanName = Path.GetFileName(fileUpload.FileName);
            }
            else
            {
                divPopup.InnerHtml = "Please select file in pdf formats only";
                divPopup.Attributes.Add("class", "error");
                divPopup.Style.Add("display", "block");
                modalcheckcomm.Show();
                return;
            }

            if (fileUpload.PostedFile.ContentLength > 1048576)
            {
                divPopup.InnerHtml = "Filesize of Supporting Document is too large. Maximum file size permitted is 1 MB.";
                divPopup.Attributes.Add("class", "error");
                divPopup.Style.Add("display", "block");
                modalcheckcomm.Show();
                return;
            }
            fileUpload.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "InvoiceFile/") + strPlanName);
            Invoice objInv = objFinance.GetInvoiceByID(Convert.ToInt32(ViewState["chkCreditValue"]));
            objInv.CreditNote = strPlanName;           
            objFinance.UpdateInvoice(objInv);
            BindFinanceInvoice();
            BindDropdownlist();
        }
        else
        {
            divPopup.InnerHtml = "Please select file first";
            divPopup.Attributes.Add("class", "error");
            divPopup.Style.Add("display", "block");
            modalcheckcomm.Show();
            return;
        }

        #endregion
    }

    #endregion

    protected void lnbCreditnote_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton btnchkcomm = (LinkButton)sender;
            ViewState["chkCreditValue"] = btnchkcomm.CommandName;
            divPopup.InnerHtml = "";
            divPopup.Style.Add("display", "none");
            modalcheckcomm.Show();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    /// <summary>
    /// This event is fired on the change in the selected index of the country DropDownList.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //protected void countryDDL_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        BindFinanceInvoice();
    //    }
    //    catch (Exception ex)
    //    {
    //        logger.Error(ex);
    //    }

    //}

    protected void exportToExcel_Click(object sender, EventArgs e)
    {
        GridViewExportUtil.Export("Invoice.xls", this.grvFinanceInvoice);
    }

    //Rendor Method
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    //-----------------------------------------new-----------------------------------

    public bool sendMail(string hotelname, string email, string attachment)
    {
        string previousMonth = DateTime.Now.AddMonths(-1).ToString("MMMM") + "-" + DateTime.Now.AddMonths(-1).ToString("yyyy");
        EmailConfig eConfig = em.GetByName("Operator invoice");
        if (eConfig.IsActive)
        {
            SendMails mail = new SendMails();
            mail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
            mail.ToEmail = email;
            mail.Attachment.Add(new System.Net.Mail.Attachment(Server.MapPath((ConfigurationManager.AppSettings["FilePath"].ToString() + "InvoiceFile/")) + attachment));
            //string bodymsg = File.ReadAllText(Server.MapPath("~/EmailTemplets/OperatorInvoice.htm"));
            string bodymsg = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
            EmailConfigMapping emap2 = new EmailConfigMapping();
            emap2 = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
            bodymsg = bodymsg.Replace("@CONTENTS", emap2.EmailContents);
            EmailValueCollection objEmailValues = new EmailValueCollection();
            mail.Subject = "Invoice";
            foreach (KeyValuePair<string, string> strKey in objEmailValues.InvoiceEmail(hotelname, email, ConfigurationManager.AppSettings["Sender"], previousMonth))
            {
                bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
            }
            mail.Body = bodymsg;
            string success = mail.SendMail();
            if (success == "Send Mail")
                return true;
            return false;
        }
        else
        {
            return false;
        }
    }


    //-----------------------------------------end-----------------------------------

    protected void lbtSendInvoices_Click(object sender, EventArgs e)
    {        
        foreach (GridViewRow gr in grvFinanceInvoice.Rows)
        {
            CheckBox checkBox = (CheckBox)gr.FindControl("chkInvoiceSent");
            if (checkBox.Checked == true)
            {
                Label hotelID = (Label)gr.FindControl("lblHotelId");
                LinkButton lbl = (LinkButton)gr.FindControl("lnbInvoiceNo");
                Image img = (Image)gr.FindControl("imgPlan");
                HiddenField hdfImage = (HiddenField)gr.FindControl("hdfImage");
                //string hotelEmail = ObjHotelinfo.getHotelEmail(Convert.ToInt64(hotelID.Text));
                //string secondaryEmail = ObjHotelinfo.getSecondaryEmail(Convert.ToInt64(hotelID.Text));
                Hotel htl = ObjHotelinfo.GetHotelByHotelID(Convert.ToInt32(hotelID.Text));
                if (htl.Email != null)
                {
                    bool sentStatus = sendMail(htl.Name, htl.Email, hdfImage.Value);
                    if (sentStatus == true)
                    {
                        Invoice objInv = objFinance.GetInvoiceByID(Convert.ToInt32(lbl.ToolTip));
                        objInv.InvoiceSent = true;
                        objFinance.UpdateInvoice(objInv);
                    }
                }
                if (htl.AccountingOfficer != null)
                    sendMail(htl.Name, htl.AccountingOfficer, hdfImage.Value);

            }
        }
        BindFinanceInvoice();
    }

    protected void clearBtn_Click(object sender, EventArgs e)
    {
        txtInvoiceNumber.Text = "";
        txtFromdate.Text = "";
        txtTodate.Text = "";
        countryDDL.SelectedIndex = 0;
        BindFinanceInvoice();
    }

    #region Change paid status of each invoice       
    protected void grvFinanceInvoice_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "StatusChange" && e.CommandArgument != null)
        {
            Invoice objInv = objFinance.GetInvoiceByID(Convert.ToInt32(e.CommandArgument));
            objInv.IsPaid = true;
            objFinance.UpdateInvoice(objInv);
            BindFinanceInvoice();
            BindDropdownlist();
        }
    }
    #endregion
}