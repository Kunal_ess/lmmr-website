﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;

public partial class SubscribeNewsOK : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string status = "Thank you for your subscription. Stay tuned on our monthly Meeting Advise or Newsletter";
        divmessage.Style.Add("display", "block");
        divmessage.Attributes.Add("class", "succesfuly");
        divmessage.InnerHtml = status;
    }

    /// <summary>
    /// This method return the value in Getresult function
    /// </summary>
    /// <param name="Key"></param>
    /// <returns></returns>
    public string GetKeyResult(string key)
    {
        return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    }
    protected void hypManageProfile_Click(object sender, EventArgs e)
    {
        Session["task"] = "Edit";
        Response.Redirect("Registration.aspx");
    }
    protected void hypChangepassword_Click(object sender, EventArgs e)
    {
        Response.Redirect("ChangePassword.aspx");
    }
}