﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Agency/AgencyMaster.master" AutoEventWireup="true" CodeFile="Statistics.aspx.cs" Inherits="Agency_Statistics" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ctnTop" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ctnMain" Runat="Server">
    <div class="rightside_inner_container" style="width:960px">
        <table width="920px" border="0" cellspacing="0" cellpadding="0" class="statistics">
            <tr>
                <td align="left">
                    <table border="0" cellspacing="0" cellpadding="0" align="left" class="one1" 
                        style="width: 99%">
                    <tr>
                    <td colspan="6">
                      <div  id="divmessage" runat="server" class="error">
                        </div>
                    </td>
                    </tr>
                        <tr>
                            <td>
                              &nbsp;&nbsp;  From&nbsp;
                            </td>
                            <td>
                                <asp:TextBox ID="txtFromdate" runat="server" CssClass="dateinput"></asp:TextBox>
                                &nbsp;
                                <input type="image" src="../images/date-icon.png" id="calFrom" />
                                <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtFromdate"
                                    PopupButtonID="calFrom" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                            </td>
                            <td align="center" >
                                To&nbsp;
                            </td>
                            <td style="width: 123px" >
                                <asp:TextBox ID="txtTodate" runat="server" CssClass="dateinput"></asp:TextBox>
                                &nbsp;
                                <input type="image" src="../images/date-icon.png" id="calTo" />
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtTodate"
                                    PopupButtonID="calTo" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                                
                            </td>
                            <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left" class="one1">
                        <tr>
                            <td style="padding: 10px; width: 94px;">
                                <b>Click to filter by :</b>
                            </td>
                            <td align="center" style="padding: 7px">
                      
                        <div class="n-btn">

                        <asp:LinkButton ID="lnkDays" class="btn-active" runat="server" ForeColor="White" OnClick="lnkDays_Click">
                        
                        <div class="n-btn-left"></div>                    
                    	<div  class="n-btn-mid">Days</div>                    
                    	<div class="n-btn-right"></div>                          
                        </asp:LinkButton>
                 


                        </div>   
                            </td>
                            
                            <td align="center" style="padding: 10px">
                               <div class="n-btn">
                   <asp:LinkButton ID="lnkMonths" runat="server"  class="btn" ForeColor="White" OnClick="lnkMonths_Click">
                   		<div class="n-btn-left"></div>                    
                    	<div  class="n-btn-mid">    Months</div>                    
                    	<div class="n-btn-right"></div>
                  </asp:LinkButton>
                </div>
                              
                            </td>
                                                  
                            
                            
                        </tr>
                    </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
          
            <tr id="trbookingReq"  runat="server">
                <td align="left">
                <h2>
                          <asp:Label ID="lblBooking" runat="server">Requests (Days)</asp:Label></h2>    
                          <br />

                          
<div id="bookinggraph" align="center">
<asp:Chart ID="chrtBooking" runat="server">
                        <Series>
                            <asp:Series Name="SeriesBooking" ChartArea="ChartAreaBooking" BorderWidth="4" BorderColor="#cc3300"  ChartType="Line">
                            </asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartAreaBooking">
                            <AxisX >
                            <MajorGrid Enabled="false" />
                            </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>

</div>

                    <div class="scroll"  style="width:900px">
                       
                        
&nbsp;<br />

                        <asp:DataList ID="dtlstConversion" runat="server" Width="100%" RepeatDirection="Horizontal"
                            CellPadding="0" CellSpacing="0">
                            <ItemTemplate>
                                <table align="center" width="100%" cellspacing="0" cellpadding="0" border="0" class="two">
                                    <tr bgcolor="#c3d6e2" align="center" >
                                        <th>
                                            <asp:Label ID="lbldate" runat="server" Text='<%# Eval("currentdate","{0:dd/MM/yy}") %>'></asp:Label>
                                        </th>
                                    </tr>
                                    <tr bgcolor="#ecf7fe" align="center">
                                        <th>
                                            <asp:Label ID="lblconfirm" runat="server" Text='<%# Eval("booking") %>'></asp:Label>
                                            /
                                            <asp:Label ID="lblvisits" runat="server" Text='<%# Eval("statisticvalue") %>'></asp:Label>
                                        </th>
                                    </tr>
                                    <tr bgcolor="#d9eefc" align="center">
                                        <th>
                                            <asp:Label ID="lblconversion" runat="server" Text='<%# Eval("ConversionBooking") %>'></asp:Label>%
                                        </th>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                    </div>
                </td>
            </tr>
            <tr id="trBookReq" runat="server">
                <td align="left">
                    <br />
                    <h2>
                        <asp:Label ID="lblhead" runat="server">Request</asp:Label></h2>
               <br />
               <div id="reqgraph" align="center">
               <asp:Chart ID="chrtReq" runat="server">
                        <Series>
                            <asp:Series Name="SeriesBooking" ChartArea="ChartAreaBooking" BorderWidth="4"  Color="#cc3300"  ChartType="Spline">
                            </asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartAreaBooking">
                            <AxisX>
                            <MajorGrid Enabled="false" />
                            </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                    </div>
                  <h5>  Your Request / Company total request</h5>
                     <div class="scroll"  style="width:900px">
                          <br />
                   <asp:DataList ID="DtlReq" runat="server" Width="100%" RepeatDirection="Horizontal"
                            CellPadding="0" CellSpacing="0">
                            <ItemTemplate>
                                <table align="center" width="100%" cellspacing="0" cellpadding="0" border="0" class="two">
                                    <tr bgcolor="#c3d6e2" align="center" >
                                        <th>
                                            <asp:Label ID="lbldate" runat="server" Text='<%# Eval("currentdate","{0:dd/MM/yy}") %>'></asp:Label>
                                        </th>
                                    </tr>
                                    <tr bgcolor="#ecf7fe" align="center">
                                        <th>
                                            <asp:Label ID="lblconfirm" runat="server" Text='<%# Eval("Sensitive") %>'></asp:Label>
                                            /
                                            <asp:Label ID="lblvisits" runat="server" Text='<%# Eval("FailureTimeout") %>'></asp:Label>
                                        </th>
                                    </tr>
                                    <tr bgcolor="#d9eefc" align="center">
                                        <th>
                                            <asp:Label ID="lblconversion" runat="server" Text='<%# Eval("ConversionReq") %>'></asp:Label>%
                                        </th>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                        </div>
                </td>
            </tr>
<%--             Request--%>

            <tr id="trrequest"    runat="server">
                <td align="left">
                    <br />
                  <h5>  Your Conversion / Company Conversion </h5>
                        <asp:Label ID="lblReqHead" runat="server"></asp:Label>
                    <div class="scroll" style="width:900px">
                    <br />
                    <asp:DataList ID="DtlReqcon" runat="server" Width="100%" RepeatDirection="Horizontal"
                            CellPadding="0" CellSpacing="0">
                            <ItemTemplate>
                                <table align="center" width="100%" cellspacing="0" cellpadding="0" border="0" class="two">
                                    <tr bgcolor="#c3d6e2" align="center" >
                                        <th>
                                            <asp:Label ID="lbldate" runat="server" Text='<%# Eval("currentdate","{0:dd/MM/yy}") %>'></asp:Label>
                                        </th>
                                    </tr>
                                    <tr bgcolor="#ecf7fe" align="center">
                                        <th>
                                            <asp:Label ID="lblconfirm" runat="server" Text='<%# Eval("confirmvalue") %>'></asp:Label>
                                            /
                                            <asp:Label ID="lblvisits" runat="server" Text='<%# Eval("FailureCancel") %>'></asp:Label>
                                        </th>
                                    </tr>
                                    <tr bgcolor="#d9eefc" align="center">
                                        <th>
                                            <asp:Label ID="lblconversion" runat="server" Text='<%# Eval("ConversionConfirmReq") %>'></asp:Label>%
                                        </th>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                        </div>
                </td>
            </tr>
        </table>
    </div>
    
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#<%= txtFromdate.ClientID %>").attr("disabled", true);
            jQuery("#<%= txtTodate.ClientID %>").attr("disabled", true);



            jQuery("#<%= lnkDays.ClientID %>").bind("click", function () {
                jQuery("#contentbody_txtFromdate").attr("disabled", false);
                jQuery("#contentbody_txtTodate").attr("disabled", false);

            });


            jQuery("#<%= lnkMonths.ClientID %>").bind("click", function () {
                jQuery("#contentbody_txtFromdate").attr("disabled", false);
                jQuery("#contentbody_txtTodate").attr("disabled", false);

            });

            

        });
     
    </script>
    <script language="javascript" type="text/javascript">


        jQuery("#<%= lnkDays.ClientID %>").bind("click", function () {
            var fromdate = jQuery("#<%= txtFromdate.ClientID %>").val();
            var todate = jQuery("#<%= txtTodate.ClientID %>").val();
            var todayArr = todate.split('/');
            var formdayArr = fromdate.split('/');
            var fromdatecheck = new Date();
            var todatecheck = new Date();
            fromdatecheck.setFullYear(parseInt(formdayArr[2], 10), (parseInt(formdayArr[1], 10) - 1), formdayArr[0]);
            todatecheck.setFullYear(parseInt(todayArr[2], 10), (parseInt(todayArr[1], 10) - 1), todayArr[0]);
            if (fromdatecheck > todatecheck) {
                jQuery(".error").show();
                jQuery(".error").html("From date must be less than To date.");
                jQuery("#contentbody_txtFromdate").attr("disabled", true);
                jQuery("#contentbody_txtTodate").attr("disabled", true);
                return false;
            }
            jQuery("#Loding_overlay").show();
            jQuery("#<%= txtFromdate.ClientID %>").attr("disabled", false);
            jQuery("#<%= txtTodate.ClientID %>").attr("disabled", false);
        });


        jQuery("#<%= lnkMonths.ClientID %>").bind("click", function () {
            var fromdate = jQuery("#<%= txtFromdate.ClientID %>").val();
            var todate = jQuery("#<%= txtTodate.ClientID %>").val();
            var todayArr = todate.split('/');
            var formdayArr = fromdate.split('/');
            var fromdatecheck = new Date();
            var todatecheck = new Date();
            fromdatecheck.setFullYear(parseInt(formdayArr[2], 10), (parseInt(formdayArr[1], 10) - 1), formdayArr[0]);
            todatecheck.setFullYear(parseInt(todayArr[2], 10), (parseInt(todayArr[1], 10) - 1), todayArr[0]);
            if (fromdatecheck > todatecheck) {
                jQuery(".error").show();
                jQuery(".error").html("From date must be less than To date.");
                jQuery("#contentbody_txtFromdate").attr("disabled", true);
                jQuery("#contentbody_txtTodate").attr("disabled", true);
                return false;
            }
            jQuery("#Loding_overlay").show();
            jQuery("#<%= txtFromdate.ClientID %>").attr("disabled", false);
            jQuery("#<%= txtTodate.ClientID %>").attr("disabled", false);
        });

      
       
    </script>
</asp:Content>

