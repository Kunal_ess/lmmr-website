﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Agency/Main.master" AutoEventWireup="true" CodeFile="BookingStep4.aspx.cs" Inherits="Agency_BookingStep4" EnableEventValidation="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="../UserControl/Agency/LeftSearchPanel.ascx" tagname="LeftSearchPanel" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cntMainBody" Runat="Server">
<asp:UpdateProgress ID="updProgress" runat="server" >
<ProgressTemplate>
<div id="Loding_overlay">
            <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
            <span>Saving...</span></div></ProgressTemplate>
</asp:UpdateProgress>
<asp:UpdatePanel ID="upd" runat="server">
<ContentTemplate>
<!--today-date START HERE-->
    <%-- <div class="today-date">
        <div class="today-date-left">
            <b>
                <%= GetKeyResult("TODAY")%>:</b>
            <asp:Label ID="lblLoginTime" runat="server">25 october 2011</asp:Label>
        </div>
        <div class="today-date-right">
            <%= GetKeyResult("YOUARELOGGEDINAS")%>: <b>
                <asp:Label ID="lblLoginUser" runat="server">John Dohle</asp:Label></b>
        </div>
    </div>--%>
    <!--today-date ENDS HERE-->
    <!--booking-heading START HERE-->
    <div class="booking-finalize-body">
        <div class="booking-finalize-step1">
            <span>
                <%= GetKeyResult("STEP1")%>.</span>
            <%= GetKeyResult("STEP1SELECTMEETINGROOM")%>
        </div>
        <div class="booking-finalize-step2">
            <span>
                <%= GetKeyResult("STEP2")%>.</span>
            <%= GetKeyResult("STEP2EXTRAOPTIONSBEDROOMS")%>
        </div>
        <div class="booking-finalize-step3">
            <span>
                <%= GetKeyResult("STEP3")%>.</span>
            <%= GetKeyResult("STEP3SENDIT")%>
        </div>
        <div class="booking-finalize-step4">
            <span>
                <%= GetKeyResult("STEPFINILIZED")%></span>
        </div>
    </div>
    <!--booking-heading ENDS HERE-->
    <!--booking-heading START HERE-->
    <br>
    <br>
    <!--booking-heading ENDS HERE-->
    <!--booking-finalize-mainbody START HERE-->
    <div id="divWarnning" runat="server" class="warning" style="display:none;"></div>
    <div id="divmessage" runat="server" class="error">
    </div>
    <asp:Panel runat="server" ID="pnlcreditcard" Visible="false">
        <div class="booking-finalize-mainbody clearfix" style="padding-top: 20px; text-align: left;">
            <span><%= GetKeyResult("YOURCREDITCARDISREQUIREDFORALLBOOKINGS")%></span>
            <br />
            <span><%= GetKeyResult("PLEASEREFFERTOTHETERMSANDCONDITIONFORMOREINFO")%></span>
        </div>
        <div class="booking-finalize-mainbody clearfix">
            <h2 style="display:none;">
                <%= GetKeyResult("USERINFO")%></h2>
            <div class="booking-finalize-form-body clearfix">
                <div class="booking-finaliz-field clearfix" style="display:none;">
                    <div class="field-left">
                        <asp:Label ID="lblNameText" runat="server"><%= GetKeyResult("NAME")%></asp:Label>
                    </div>
                    <div class="field-right">
                        <asp:Label ID="lblName" runat="server" Text="Label"></asp:Label>
                    </div>
                </div>
                <div class="booking-finaliz-field-dark clearfix" style="display:none;">
                    <div class="field-left">
                        <asp:Label ID="lblEmailText" runat="server"><%= GetKeyResult("EMAIL")%></asp:Label>
                    </div>
                    <div class="field-right">
                        <asp:Label ID="lblEmail" runat="server" Text="Label"></asp:Label>
                    </div>
                </div>
                <div class="booking-finaliz-field clearfix" style="display:none;">
                    <div class="field-left">
                        <asp:Label ID="lblAddress" runat="server"><%= GetKeyResult("ADDRESS")%></asp:Label>
                    </div>
                    <div class="field-right">
                        <asp:TextBox ID="txtAddress" runat="server" class="inputbox" MaxLength="250"></asp:TextBox>
                    </div>
                </div>
                <div class="booking-finaliz-field-dark clearfix" style="display:none;">
                    <div class="field-left">
                        <asp:Label ID="lblpostalCode" runat="server"><%= GetKeyResult("POSTALCODE")%></asp:Label>
                    </div>
                    <div class="field-right">
                        <asp:TextBox ID="txtPostalcode" runat="server" CssClass="inputboxsmall" Style="text-align: left;"
                            MaxLength="10"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtPostalcode"
                            ValidChars="-0123456789" runat="server">
                        </asp:FilteredTextBoxExtender>
                    </div>
                </div>
                <div class="booking-finaliz-field clearfix" style="display:none;">
                    <div class="field-left">
                        <asp:Label ID="Label1" runat="server"><%= GetKeyResult("COUNTRY")%></asp:Label>
                    </div>
                    <div class="field-right">
                        <div id="Countryf">
                            <asp:DropDownList ID="dropdownCountry" runat="server" class="bigselect">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="booking-finaliz-field-dark clearfix" style="display:none;">
                    <div class="field-left">
                        <asp:Label ID="lblPhone" runat="server"><%= GetKeyResult("PHONE")%></asp:Label>
                    </div>
                    <div class="field-right">
                        <asp:TextBox ID="txtTelphone" runat="server" class="inputboxsmall" Style="text-align: left;"
                            MaxLength="15"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" TargetControlID="txtTelphone"
                            ValidChars="-0123456789" runat="server">
                        </asp:FilteredTextBoxExtender>
                    </div>
                </div>
                <div class="booking-finaliz-field-info">
                    <%= GetKeyResult("CREDITCARDINFO")%>
                </div>
                <div class="booking-finaliz-field clearfix">
                    <div class="field-left">
                        <asp:Label ID="lblCreditCardType" runat="server"><%= GetKeyResult("CREDITCARDTYPE")%></asp:Label>
                    </div>
                    <div class="field-right">
                        <asp:DropDownList ID="dropdownCreditCardType" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="booking-finaliz-field-dark clearfix">
                    <div class="field-left">
                        <asp:Label ID="lblCreditCardName" runat="server"><%= GetKeyResult("CREDITCARDNAME")%></asp:Label>
                    </div>
                    <div class="field-right">
                        <asp:TextBox ID="txtCreditCardName" runat="server" class="inputbox"></asp:TextBox>
                    </div>
                </div>
                <div class="booking-finaliz-field clearfix">
                    <div class="field-left">
                        <asp:Label ID="lblCreditCardNumber" runat="server"><%= GetKeyResult("CREDITCARDNUMBER")%></asp:Label>
                    </div>
                    <div class="field-right">
                        <asp:TextBox ID="txtCreditcardnumber" runat="server" class="inputbox" MaxLength="16"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="txtCreditcardnumber"
                            ValidChars="0123456789" runat="server">
                        </asp:FilteredTextBoxExtender>
                    </div>
                </div>
                <div class="booking-finaliz-field-dark clearfix">
                    <div class="field-left">
                        <asp:Label ID="lblCvcNumber" runat="server"><%= GetKeyResult("CVCNUMBER")%></asp:Label>
                    </div>
                    <div class="field-right">
                        <asp:TextBox ID="txtCvcNumber" runat="server" class="inputboxsmall" MaxLength="5"
                            Style="text-align: left;"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" TargetControlID="txtCvcNumber"
                            ValidChars="0123456789" runat="server">
                        </asp:FilteredTextBoxExtender>
                    </div>
                </div>
                <div class="booking-finaliz-field clearfix">
                    <div class="field-left">
                        <asp:Label ID="lblExpiryDate" runat="server"><%= GetKeyResult("EXPIRYDATE")%></asp:Label>
                    </div>
                    <div class="field-right">
                        <div id="datef">
                            <asp:DropDownList ID="dropdownMonth" runat="server">
                                <asp:ListItem Value="01" Selected="True">01</asp:ListItem>
                                <asp:ListItem Value="02">02</asp:ListItem>
                                <asp:ListItem Value="03">03</asp:ListItem>
                                <asp:ListItem Value="04">04</asp:ListItem>
                                <asp:ListItem Value="05">05</asp:ListItem>
                                <asp:ListItem Value="06">06</asp:ListItem>
                                <asp:ListItem Value="07">07</asp:ListItem>
                                <asp:ListItem Value="08">08</asp:ListItem>
                                <asp:ListItem Value="09">09</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                                <asp:ListItem Value="11">11</asp:ListItem>
                                <asp:ListItem Value="12">12</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div id="year">
                            <asp:DropDownList ID="dropdownYear" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--booking-finalize-mainbody ENDS HERE-->
        <!--next button START HERE-->
        <div class="next-button-new">
            <asp:LinkButton ID="lnkbtnCancel" runat="server" class="new-send-btn" OnClick="lnkbtnCancel_Click">&nbsp;<%= GetKeyResult("BACK")%>&nbsp;</asp:LinkButton>
            &nbsp;&nbsp;
            <asp:LinkButton ID="lnkbtnSend" runat="server" class="new-send-btn" OnClick="lnkbtnSend_Click"><%= GetKeyResult("SEND")%></asp:LinkButton>
        </div>
        <asp:Label ID="lblNetTotal" runat="server" Visible="false" ></asp:Label>
        <!--next button ENDS HERE-->
        <div id="Loding_overlaySec">
            <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
            <span>Saving...</span></div>
        <script language="javascript" type="text/javascript">
            var ccErrorNo = 0;
            var ccErrors = new Array()

            ccErrors[0] = '<%=GetKeyResultForJavaScript("UNKNOWNCARDTYPE")%>';
            ccErrors[1] = '<%=GetKeyResultForJavaScript("NOCARDNUMBERPROVIDED")%>';
            ccErrors[2] = '<%=GetKeyResultForJavaScript("CREDITCARDNUMBERISININVALIDFORMAT")%>';
            ccErrors[3] = '<%=GetKeyResultForJavaScript("CREDITCARDNUMBERISINVALID")%>';
            ccErrors[4] = '<%=GetKeyResultForJavaScript("CREDITCARDNUMBERHASANINAPPROPRIATENUMBEROFDEGITS")%>';
            ccErrors[5] = '<%=GetKeyResultForJavaScript("CREDITCARDWARNING")%>';

            jQuery(document).ready(function () {

                jQuery("#<%= lnkbtnSend.ClientID %>").bind("click", function () {
                    if (jQuery("#<%= divmessage.ClientID %>").hasClass("succesfuly")) {
                        jQuery("#<%= divmessage.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                    }
                    var isvalid = true;
                    var errormessage = "";

                    var address = jQuery("#<%= txtAddress.ClientID %>").val();
                    if (address.length <= 0 || address == "") {
                       /* if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += '<%=GetKeyResultForJavaScript("ADDRESSERRORMESSAGE")%>';
                        isvalid = false;*/
                    }

                    var postalCode = jQuery("#<%= txtPostalcode.ClientID %>").val();
                    if (postalCode.length <= 0) {
                       /* if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += '<%=GetKeyResultForJavaScript("POSTALCODEERRORMESSAGE")%>';
                        isvalid = false;*/
                    }

                    var countryList = jQuery("#<%= dropdownCountry.ClientID %>").val();
                    if (countryList == "--Select country--" || countryList == "0") {
                       /* if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += '<%=GetKeyResultForJavaScript("COUNTRYERRORMESSAGE")%>';
                        isvalid = false;*/
                    }

                    var phone = jQuery("#<%= txtTelphone.ClientID %>").val();
                    if (phone.length <= 0) {
                        /*if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += '<%=GetKeyResultForJavaScript("PHONEERRORMESSAGE")%>';
                        isvalid = false;*/
                    }

                    var CreditCardType = jQuery("#<%= dropdownCreditCardType.ClientID %>").val();
                    if (CreditCardType == "--Select credit card type--" || countryList == "0") {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += '<%=GetKeyResultForJavaScript("CARDTYPEMSG")%>';
                        isvalid = false;
                    }

                    var CreditCardName = jQuery("#<%= txtCreditCardName.ClientID %>").val();
                    if (CreditCardName.length <= 0) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += '<%=GetKeyResultForJavaScript("CARDNAMEMSG")%>';
                        isvalid = false;
                    }

                    var Creditcardnumber = jQuery("#<%= txtCreditcardnumber.ClientID %>").val();
                    if (Creditcardnumber.length <= 0) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += '<%=GetKeyResultForJavaScript("CARDNUMBERMSG")%>';
                        isvalid = false;
                    }
                    //alert(CreditCardType);
                    if (CreditCardType.length > 0 && Creditcardnumber.length > 0) {
                        if (!checkCreditCard(Creditcardnumber, CreditCardType == "American Express" ? "AmEx" : CreditCardType.replace(' ', ''))) {
                            if (errormessage.length > 0) {
                                errormessage += "<br/>";
                            }
                            errormessage += ccErrors[ccErrorNo];
                            isvalid = false;
                        }
                    }
                    var CvcNumber = jQuery("#<%= txtCvcNumber.ClientID %>").val();
                    if (CvcNumber.length <= 0) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += '<%=GetKeyResultForJavaScript("CVCNUMBERMSG")%>';
                        isvalid = false;
                    }
                    //////////////                    
                    //                    --------------------new start---------------------------

                    if (CvcNumber.length != 0) {
                        if (CreditCardType == "Master Card" || CreditCardType == "Visa" || CreditCardType == "Diners") {
                            if (CvcNumber.length != 3) {
                                if (errormessage.length > 0) {
                                    errormessage += "<br/>";
                                }
                                errormessage += '<%=GetKeyResultForJavaScript("CVVNUMBERLENGTHMSG")%>';
                                isvalid = false;
                            }
                        }
                    }
                    if (CvcNumber.length != 0) {
                        if (CreditCardType == "American Express") {
                            if (CvcNumber.length != 4) {
                                if (errormessage.length > 0) {
                                    errormessage += "<br/>";
                                }
                                errormessage += '<%=GetKeyResultForJavaScript("CVVNUMBERLENGTHMSG")%>';
                                isvalid = false;
                            }
                        }
                    }
                    //-------------------------new end-------------------

                    var drpSelectedMonth = jQuery("#<%= dropdownMonth.ClientID %>").val();
                    var drpSelectedYear = jQuery("#<%= dropdownYear.ClientID %>").val();
                    var d = new Date();
                    var nowMonth = d.getMonth();
                    var nowYear = d.getFullYear();
                    if (drpSelectedMonth < nowMonth && drpSelectedYear == nowYear) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += '<%=GetKeyResultForJavaScript("EXPIRYDATEMSG")%>';
                        isvalid = false;
                    }


                    if (!isvalid) {
                        jQuery(".error").show();
                        jQuery(".error").html(errormessage);
                        var offseterror = jQuery(".error").offset();
                        jQuery("body").scrollTop(offseterror.top);
                        jQuery("html").scrollTop(offseterror.top);
                        return false;
                    }

                    jQuery(".error").hide();
                    jQuery(".error").html('');
                    jQuery("body").scrollTop(0);
                    jQuery("html").scrollTop(0);
                    jQuery("#Loding_overlay span").html("Saving...");
                    jQuery("#Loding_overlaySec").show();
                    document.getElementsByTagName('html')[0].style.overflow = 'hidden';

                });
            });


            function checkCreditCard(cardnumber, cardname) {

                // Array to hold the permitted card characteristics
                var cards = new Array();

                // Define the cards we support. You may add addtional card types as follows.

                //  Name:         As in the selection box of the form - must be same as user's
                //  Length:       List of possible valid lengths of the card number for the card
                //  prefixes:     List of possible prefixes for the card
                //  checkdigit:   Boolean to say whether there is a check digit

                cards[0] = { name: "Visa",
                    length: "13,16",
                    prefixes: "4",
                    checkdigit: true
                };
                cards[1] = { name: "MasterCard",
                    length: "16",
                    prefixes: "51,52,53,54,55",
                    checkdigit: true
                };
                cards[2] = { name: "DinersClub",
                    length: "14,16",
                    prefixes: "36,54,55",
                    checkdigit: true
                };
                cards[3] = { name: "CarteBlanche",
                    length: "14",
                    prefixes: "300,301,302,303,304,305",
                    checkdigit: true
                };
                cards[4] = { name: "AmEx",
                    length: "15",
                    prefixes: "34,37",
                    checkdigit: true
                };
                cards[5] = { name: "Discover",
                    length: "16",
                    prefixes: "6011,622,64,65",
                    checkdigit: true
                };
                cards[6] = { name: "JCB",
                    length: "16",
                    prefixes: "35",
                    checkdigit: true
                };
                cards[7] = { name: "enRoute",
                    length: "15",
                    prefixes: "2014,2149",
                    checkdigit: true
                };
                cards[8] = { name: "Solo",
                    length: "16,18,19",
                    prefixes: "6334,6767",
                    checkdigit: true
                };
                cards[9] = { name: "Switch",
                    length: "16,18,19",
                    prefixes: "4903,4905,4911,4936,564182,633110,6333,6759",
                    checkdigit: true
                };
                cards[10] = { name: "Maestro",
                    length: "12,13,14,15,16,18,19",
                    prefixes: "5018,5020,5038,6304,6759,6761,6762,6763",
                    checkdigit: true
                };
                cards[11] = { name: "VisaElectron",
                    length: "16",
                    prefixes: "4026,417500,4508,4844,4913,4917",
                    checkdigit: true
                };
                cards[12] = { name: "LaserCard",
                    length: "16,17,18,19",
                    prefixes: "6304,6706,6771,6709",
                    checkdigit: true
                };

                // Establish card type
                var cardType = -1;
                for (var i = 0; i < cards.length; i++) {

                    // See if it is this card (ignoring the case of the string)
                    if (cardname.toLowerCase() == cards[i].name.toLowerCase()) {
                        cardType = i;
                        break;
                    }
                }

                // If card type not found, report an error
                if (cardType == -1) {
                    ccErrorNo = 0;
                    return false;
                }

                // Ensure that the user has provided a credit card number
                if (cardnumber.length == 0) {
                    ccErrorNo = 1;
                    return false;
                }

                // Now remove any spaces from the credit card number
                cardnumber = cardnumber.replace(/\s/g, "");

                // Check that the number is numeric
                var cardNo = cardnumber
                var cardexp = /^[0-9]{13,19}$/;
                if (!cardexp.exec(cardNo)) {
                    ccErrorNo = 2;
                    return false;
                }

                // Now check the modulus 10 check digit - if required
                if (cards[cardType].checkdigit) {
                    var checksum = 0;                                  // running checksum total
                    var mychar = "";                                   // next char to process
                    var j = 1;                                         // takes value of 1 or 2

                    // Process each digit one by one starting at the right
                    var calc;
                    for (i = cardNo.length - 1; i >= 0; i--) {

                        // Extract the next digit and multiply by 1 or 2 on alternative digits.
                        calc = Number(cardNo.charAt(i)) * j;

                        // If the result is in two digits add 1 to the checksum total
                        if (calc > 9) {
                            checksum = checksum + 1;
                            calc = calc - 10;
                        }

                        // Add the units element to the checksum total
                        checksum = checksum + calc;

                        // Switch the value of j
                        if (j == 1) { j = 2 } else { j = 1 };
                    }

                    // All done - if checksum is divisible by 10, it is a valid modulus 10.
                    // If not, report an error.
                    if (checksum % 10 != 0) {
                        ccErrorNo = 3;
                        return false;
                    }
                }

                // Check it's not a spam number
                if (cardNo == '5490997771092064') {
                    ccErrorNo = 5;
                    return false;
                }

                // The following are the card-specific checks we undertake.
                var LengthValid = false;
                var PrefixValid = false;
                var undefined;

                // We use these for holding the valid lengths and prefixes of a card type
                var prefix = new Array();
                var lengths = new Array();

                // Load an array with the valid prefixes for this card
                prefix = cards[cardType].prefixes.split(",");

                // Now see if any of them match what we have in the card number
                for (i = 0; i < prefix.length; i++) {
                    var exp = new RegExp("^" + prefix[i]);
                    if (exp.test(cardNo)) PrefixValid = true;
                }

                // If it isn't a valid prefix there's no point at looking at the length
                if (!PrefixValid) {
                    ccErrorNo = 3;
                    return false;
                }

                // See if the length is valid for this card
                lengths = cards[cardType].length.split(",");
                for (j = 0; j < lengths.length; j++) {
                    if (cardNo.length == lengths[j]) LengthValid = true;
                }

                // See if all is OK by seeing if the length was valid. We only check the length if all else was 
                // hunky dory.
                if (!LengthValid) {
                    ccErrorNo = 4;
                    return false;
                };

                // The credit card is in the required format.
                return true;
            }

        </script>
    </asp:Panel>
    <style>
    .for-the-booking{ border:#9bbb59 solid 2px; width:420px; padding:15px; margin:20px auto; text-align:center}
.for-the-booking p{ padding:0px; margin:0px 0px 15px 0px;}
.for-the-booking p span{ color:#1e497c; font-weight:bold;}
    </style>
   <%-- <div class="for-the-booking">
<p>Many thanks for your online reservation: Your special CODE to get your free CANVAS DELUXE on SMARTPHOTO.COM will be send to you the day after your meeting was held.</p>

<p style="margin-top:10px;"><span>Then</span></p>
<p><span>Go to <a href="http://www.smartphoto.be/nl/producten/wanddecoratie/fotocanvas">http://www.smartphoto.be/nl/producten/wanddecoratie/fotocanvas</a></span>
<br>
<span style="font-size:12px;">You can use the code, upload your picture and get a free CANVAS DELUXE 40*60 send to your home or company</span>
</p>
<p style="font-size:10px"><span>Action valid until 31/10/12 and applicable for Belgium, Netherlands, Germany, France, UK and Luxembourg citizens / companies.</span></p>
</div>--%>
    <div class="booking-details" id="Divdetails" runat="server" style="width: 100%;display:none;">
        <table width="100%" cellpadding="8" bgcolor="#fffff" cellspacing="1" border="0">
            <tr>
                
                <td colspan="7" align="left">
                    <h2>
                        <asp:Label ID="lblHotel" runat="server" Text=""></asp:Label>
                    </h2>
                </td>
            </tr>
            <tr>
                
                <td align="left">
                    <b><%= GetKeyResult("ADDRESS")%> :</b>
                </td>
                <td align="left" colspan="3">
                    <asp:Label ID="lblhoteladd" runat="server" Text=""></asp:Label>
                </td>
                <td colspan="3">
                </td>
            </tr>
            <tr>
                <td align="left">
                    <b><%= GetKeyResult("TELEPHONE")%> :</b>
                </td>
                <td align="left" colspan="6">
                    <asp:Label ID="lblTel" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <b><%= GetKeyResult("FAXNUMBER")%> :</b>
                </td>
                <td align="left" colspan="6">
                    <asp:Label ID="lblFax" runat="server" Text=""></asp:Label>
                </td>
            </tr>
           <tr>
                <td colspan="2">
                    <b><%= GetKeyResult("CONTACTNAME")%>:</b>
                </td>
                <td colspan="1">
                    <asp:Label ID="lblContactPerson" runat="server" Text=""></asp:Label>
                </td>
                <td colspan="1">
                    <b><%= GetKeyResult("PHONENO")%>:</b>
                </td>
                <td colspan="1">
                    <asp:Label ID="lblContactPhone" runat="server" Text=""></asp:Label>
                </td>
                <td colspan="2">
                    <b><%= GetKeyResult("EMAIL")%> :</b> <asp:label  id="lblConatctpersonEmail" runat="server"></asp:label>
                </td>
            </tr>
            <tr >
                <td colspan="2">
                    <b><%= GetKeyResult("DATEFROM")%>:</b>
                </td>
                <td colspan="1">
                    <asp:Label ID="lblFromDt" runat="server" Text=""></asp:Label>
                </td>
                <td colspan="1">
                    <b><%= GetKeyResult("TO")%>:</b>
                </td>
                <td colspan="1">
                    <asp:Label ID="lblToDate" runat="server" Text=""></asp:Label>
                </td>
                <td colspan="2">
                    <b><%= GetKeyResult("DURATION")%>:</b>
                    <asp:Label ID="lblBookedDays" runat="server" Text=""></asp:Label>
                    <asp:Label ID="lblBookedDays1" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
            <td colspan="2">
                    <b><%= GetKeyResult("CONTACTADDRESS")%>:</b>
                </td>
                <td colspan="2">
                    <asp:Label ID="lblContactAddress" runat="server" Text=""></asp:Label>
                </td>
                <td><strong><asp:Label ID="lblCName" runat="server"><%= GetKeyResult("COMPANYNAME")%>:</asp:Label></strong></td>
                <td colspan="2"><asp:Label ID="lblCompanyName" runat="server" ></asp:Label></td>
                
                
            </tr>
            <asp:Repeater ID="rptMeetingRoom" runat="server" OnItemDataBound="rptMeetingRoom_ItemDataBound">
                <ItemTemplate>
                    <tr bgcolor="#ecf2e3">
                        <td colspan="7">
                            <h1><b><%= GetKeyResult("MEETINGROOM")%> 1</b></h1>
                        </td>
                    </tr>
                    <asp:Repeater ID="rptMeetingRoomConfigure" runat="server" OnItemDataBound="rptMeetingRoomConfigure_ItemDataBound">
                        <ItemTemplate>
                            <tr bgcolor="#ecf2e3">
                                <td colspan="7">
                                    <h3>
                                        <b><%= GetKeyResult("DAY")%></b>
                                        <asp:Label ID="lblSelectedDay" runat="server"></asp:Label></h3>
                                </td>
                            </tr>
                            <tr bgcolor="#d4d9cc" style="font-weight: bold">
                                <td>
                                </td>
                                <td align="left">
                                    <b><%= GetKeyResult("DESCRIPTION")%></b>
                                </td>
                                <td align="center" >
                                    <asp:Label ID="lblPriceIfNotPackage" runat="server"><b>Price</b></asp:Label>
                                </td>
                                <td align="center">
                                  <b>  <%= GetKeyResult("START")%></b>
                                </td>
                                <td align="center">
                                   <b> <%= GetKeyResult("END")%></b>
                                </td>
                                <td align="center">
                                   <b> <%= GetKeyResult("QTY")%></b>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblTotalIfNotPackage" runat="server"><b><%= GetKeyResult("TOTAL")%></b></asp:Label>
                                </td>
                            </tr>
                            <tr bgcolor="#ecf2e3">
                                <td valign="top">
                                    "<asp:Label ID="lblMeetingRoomName" runat="server"></asp:Label>"
                                </td>
                                <td align="left" valign="top">
                                    <p>
                                        <asp:Label ID="lblMeetingRoomType" runat="server"></asp:Label></p>
                                    <p>
                                        <asp:Label ID="lblMinMaxCapacity" runat="server"></asp:Label></p>
                                </td>
                                <td align="center" valign="top" >
                                    <asp:Label ID="lblMeetingRoomActualPrice" runat="server"></asp:Label>
                                </td>
                                <td align="center" valign="top">
                                    <asp:Label ID="lblStart" runat="server"></asp:Label>
                                </td>
                                <td align="center" valign="top">
                                    <asp:Label ID="lblEnd" runat="server"></asp:Label>
                                </td>
                                <td align="center" valign="top">
                                    <asp:Label ID="lblNumberOfParticepant" runat="server"></asp:Label>
                                </td>
                                <td id="Td1" align="center" valign="top" runat="server">
                                    
                                    <asp:Label ID="lblTotalMeetingRoomPrice" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td colspan="7" align="right" >
                                    <asp:Label ID="lblTotal2IfPackageIsNotAvailable" runat="server"><%= GetKeyResult("TOTAL")%>:</asp:Label> &nbsp; &nbsp; <b>
                                        <asp:Label ID="lblTotalFinalMeetingRoomPrice" runat="server"></asp:Label></b>
                                </td>
                            </tr>
                            <asp:Panel ID="pnlIsPackageSelected" runat="server">
                                <!--booking-step3-packages-mainbody START HERE-->
                                <tr bgcolor="#ecf2e3">
                                    <td valign="top" colspan="7">
                                        <h1><b>
                                            <%= GetKeyResult("PACKAGES")%></b>
                                        </h1>
                                    </td>
                                </tr>
                                <tr bgcolor="#d4d9cc">
                                    <td colspan="2">
                                    </td>
                                    <td align="left" valign="top" colspan="4">
                                        <strong><%= GetKeyResult("DESCRIPTION")%> </strong>
                                    </td>
                                    <td align="center" valign="top">
                                        <strong><%= GetKeyResult("TOTAL")%></strong>
                                    </td>
                                </tr>
                                <tr bgcolor="#ecf2e3">
                                    <td valign="top" colspan="2">
                                        <b><asp:Label ID="lblSelectedPackage" runat="server"></asp:Label></b>
                                    </td>
                                    <td valign="top" align="left" colspan="4">
                                        <p>
                                            <asp:Label ID="lblPackageDescription" runat="server"></asp:Label>
                                        </p>
                                    </td>
                                    <td align="center" valign="top">
                                        
                                        <asp:Label ID="lblPackagePrice" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <asp:Repeater ID="rptPackageItem" runat="server" OnItemDataBound="rptPackageItem_ItemDataBound">
                                   <HeaderTemplate>
                                        <tr bgcolor="#d4d9cc">
                                            <td colspan="3">
                                            </td>
                                            <td align="left" valign="top">
                                              <b>  <%= GetKeyResult("STARTINGTIME")%></b>
                                            </td>
                                            <td align="left" valign="top">
                                               <b> <%= GetKeyResult("STARTINGTIME")%></b>
                                            </td>
                                            <td align="left" valign="top">
                                               <b> <%= GetKeyResult("QTY")%></b>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr bgcolor="#ecf2e3">
                                            <td valign="top" colspan="3">
                                                
                                                <asp:Label ID="lblPackageItem" runat="server"></asp:Label>
                                            </td>
                                            <td align="left" valign="top">
                                                <asp:Label ID="lblFromTime" runat="server"></asp:Label>
                                            </td>
                                            <td align="left" valign="top">
                                                <asp:Label ID="lblToTime" runat="server"></asp:Label>
                                            </td>
                                            <td align="left" valign="top">
                                                <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Panel ID="pnlIsExtra" runat="server">
                                    <tr bgcolor="#ecf2e3">
                                        <td colspan="7" valign="top">
                                           <h1> <b><asp:Label ID="lblextratitle" runat="server" Text=""><%= GetKeyResult("EXTRAS")%></asp:Label></b></h1>
                                        </td>
                                    </tr>
                                    <asp:Repeater ID="rptExtra" runat="server" OnItemDataBound="rptExtra_ItemDataBound">
                                        <HeaderTemplate>
                                            <tr bgcolor="#d4d9cc">
                                                <td colspan="3">
                                                </td>
                                                <td width="15%">
                                                  <b>  <%= GetKeyResult("FROM")%></b>
                                                </td>
                                                <td width="15%">
                                                   <b> <%= GetKeyResult("TO")%></b>
                                                </td>
                                                <td width="15%">
                                                   <b> <%= GetKeyResult("QTY")%></b>
                                                </td>
                                                <td>
                                                  <b>  <%= GetKeyResult("TOTAL")%></b>
                                                </td>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr bgcolor="#ecf2e3">
                                                <td valign="top" colspan="3">
                                                    <asp:Label ID="lblPackageItem" runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblFrom" runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblTo" runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblQuntity" runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                   
                                                    <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <tr bgcolor="#ecf2e3">
                                        <td align="right" colspan="7">
                                            
                                            <asp:Label runat="server" ID="lblExtraPrice"></asp:Label>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <tr>
                                    <td colspan="7" align="right" bgcolor="#ffffff">
                                        <%= GetKeyResult("TOTAL")%>:&nbsp; &nbsp; <b>
                                            <asp:Label ID="lblTotalPackagePrice" runat="server"></asp:Label></b>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="pnlBuildMeeting" runat="server">
                                <!--booking-step3-BuildMeeting-mainbody START HERE-->
                                <tr bgcolor="#ecf2e3">
                                    <td colspan="7">
                                        <h3><%= GetKeyResult("CHOICE2BUILDYOURMEETING")%><br />
                                        </h3>
                                    </td>
                                </tr>
                                <asp:Repeater ID="rptBuildMeeting" runat="server" OnItemDataBound="rptBuildMeeting_ItemDataBound">
                                    <HeaderTemplate>
                                        <tr bgcolor="#d4d9cc">
                                            <td colspan="2">
                                            </td>
                                            <td align="left" colspan="3">
                                               <b> <%= GetKeyResult("DESCRIPTION")%></b>
                                            </td>
                                            <td align="center">
                                               <b> <%= GetKeyResult("QTY")%></b>
                                            </td>
                                            <td align="center">
                                               <b> <%= GetKeyResult("TOTAL")%></b>
                                            </td>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr bgcolor="#ecf2e3">
                                            <td align="left" valign="top" colspan="2">
                                                <asp:Label ID="lblItemName" runat="server"></asp:Label>
                                            </td>
                                            <td align="left" valign="top" colspan="3">
                                                <p>
                                                    <asp:Label ID="lblItemDescription" runat="server"></asp:Label>
                                                </p>
                                            </td>
                                            <td align="center" valign="top">
                                                <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                            </td>
                                            <td align="center" valign="top">
                                                
                                                <asp:Label ID="lblTotalItemPrice" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <tr bgcolor="#ffffff" align="right">
                                    <td colspan="7">
                                        <%= GetKeyResult("TOTAL")%>:&nbsp; &nbsp; <b>
                                            <asp:Label ID="lblTotalBuildPackagePrice" runat="server"></asp:Label></b>
                                    </td>
                                </tr>
                            </asp:Panel>
                            
                            <!--booking-step3-BuildMeeting ENDS HERE-->
                            <asp:Panel ID="pnlEquipment" runat="server">
                                <!--booking-step3-equipment-mainbody START HERE-->
                                <tr bgcolor="#ecf2e3">
                                    <td colspan="7">
                                        <h1><b>
                                            <%= GetKeyResult("EQUIPMENT")%></b></h1>
                                    </td>
                                </tr>
                                <asp:Repeater ID="rptEquipment" runat="server" OnItemDataBound="rptEquipment_ItemDataBound">
                                    <HeaderTemplate>
                                        <tr bgcolor="#d4d9cc">
                                            <td colspan="2">
                                            </td>
                                            <td align="left" colspan="3">
                                                <b><%= GetKeyResult("DESCRIPTION")%></b>
                                            </td>
                                            <td align="center">
                                                <b><%= GetKeyResult("QTY")%></b>
                                            </td>
                                            <td align="center">
                                                <b><%= GetKeyResult("TOTAL")%></b>
                                            </td>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr bgcolor="#ecf2e3">
                                            <td colspan="2">
                                                <asp:Label ID="lblItemName" runat="server"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <p>
                                                    <asp:Label ID="lblItemDescription" runat="server"></asp:Label>
                                                </p>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                
                                                <asp:Label ID="lblTotalPrice" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <tr bgcolor="#ffffff">
                                    <td colspan="7" align="right">
                                        <%= GetKeyResult("TOTAL")%>:&nbsp; &nbsp; <b>
                                            <asp:Label ID="lblTotalEquipmentPrice" runat="server"></asp:Label></b>
                                    </td>
                                </tr>
                                <!--booking-step3-equipment ENDS HERE-->
                            </asp:Panel>

                            <asp:Panel ID="pnlOthers" runat="server">
                                <!--booking-step3-equipment-mainbody START HERE-->
                                <tr bgcolor="#ecf2e3">
                                    <td colspan="7">
                                        <h1><b>
                                            <%= GetKeyResult("OTHERS")%></b></h1>
                                    </td>
                                </tr>
                                <asp:Repeater ID="rptOthers" runat="server" OnItemDataBound="rptOthers_ItemDataBound">
                                    <HeaderTemplate>
                                        <tr bgcolor="#d4d9cc">
                                            <td colspan="2">
                                            </td>
                                            <td align="left" colspan="3">
                                                <b><%= GetKeyResult("DESCRIPTION")%></b>
                                            </td>
                                            <td align="center">
                                                <b><%= GetKeyResult("QTY")%></b>
                                            </td>
                                            <td align="center">
                                                <b><%= GetKeyResult("TOTAL")%></b>
                                            </td>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr bgcolor="#ecf2e3">
                                            <td colspan="2">
                                                <asp:Label ID="lblItemName" runat="server"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <p>
                                                    <asp:Label ID="lblItemDescription" runat="server"></asp:Label>
                                                </p>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                
                                                <asp:Label ID="lblTotalPrice" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <tr bgcolor="#ffffff">
                                    <td colspan="7" align="right">
                                        <%= GetKeyResult("TOTAL")%>:&nbsp; &nbsp; <b>
                                            <asp:Label ID="lblTotalOthersPrice" runat="server"></asp:Label></b>
                                    </td>
                                </tr>
                                <!--booking-step3-equipment ENDS HERE-->
                            </asp:Panel>
                        </ItemTemplate>
                    </asp:Repeater>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr bgcolor="#ecf2e3">
                        <td colspan="7">
                            <h1>
                               <b><%= GetKeyResult("MEETINGROOM")%> 2</b></h1>
                        </td>
                    </tr>
                    <asp:Repeater ID="rptMeetingRoomConfigure" runat="server" OnItemDataBound="rptMeetingRoomConfigure_ItemDataBound">
                        <ItemTemplate>
                            <tr bgcolor="#ecf2e3">
                                <td colspan="7">
                                    <h3>
                                        <b><%= GetKeyResult("DAY")%></b>
                                        <asp:Label ID="lblSelectedDay" runat="server"></asp:Label></h3>
                                </td>
                            </tr>
                            <tr bgcolor="#d4d9cc" style="font-weight: bold">
                                <td>
                                </td>
                                <td align="left">
                                    <b><%= GetKeyResult("DESCRIPTION")%></b>
                                </td>
                                <td align="center" >
                                    <asp:Label ID="lblPriceIfNotPackage" runat="server"><b><%= GetKeyResult("PRICE")%></b></asp:Label>
                                </td>
                                <td align="center">
                                  <b>  <%= GetKeyResult("START")%></b>
                                </td>
                                <td align="center">
                                   <b> <%= GetKeyResult("END")%></b>
                                </td>
                                <td align="center">
                                   <b> <%= GetKeyResult("QTY")%></b>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblTotalIfNotPackage" runat="server"><b><%= GetKeyResult("TOTAL")%></b></asp:Label>
                                </td>
                            </tr>
                            <tr bgcolor="#ecf2e3">
                                <td valign="top">
                                    "<asp:Label ID="lblMeetingRoomName" runat="server"></asp:Label>"
                                </td>
                                <td align="left" valign="top">
                                    <p>
                                        <asp:Label ID="lblMeetingRoomType" runat="server"></asp:Label></p>
                                    <p>
                                        <asp:Label ID="lblMinMaxCapacity" runat="server"></asp:Label></p>
                                </td>
                                <td align="center" valign="top" >
                                    <asp:Label ID="lblMeetingRoomActualPrice" runat="server"></asp:Label>
                                </td>
                                <td align="center" valign="top">
                                    <asp:Label ID="lblStart" runat="server"></asp:Label>
                                </td>
                                <td align="center" valign="top">
                                    <asp:Label ID="lblEnd" runat="server"></asp:Label>
                                </td>
                                <td align="center" valign="top">
                                    <asp:Label ID="lblNumberOfParticepant" runat="server"></asp:Label>
                                </td>
                                <td id="Td1" align="center" valign="top" runat="server">
                                    
                                    <asp:Label ID="lblTotalMeetingRoomPrice" runat="server"> </asp:Label>
                                </td>
                            </tr>
                            <tr bgcolor="#ffffff">
                                <td colspan="7" align="right" >
                                    <asp:Label ID="lblTotal2IfPackageIsNotAvailable" runat="server"><%= GetKeyResult("TOTAL")%>:</asp:Label> &nbsp; &nbsp; <b>
                                        <asp:Label ID="lblTotalFinalMeetingRoomPrice" runat="server"> </asp:Label></b>
                                </td>
                            </tr>
                            <asp:Panel ID="pnlIsBreakdown" runat="server">
                                <tr bgcolor="#ecf2e3">
                                    <td colspan="7" align="right">
                                        <h3><%= GetKeyResult("THISMEETINGROOMISBREAKOUT")%></h3>
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel ID="pnlNotIsBreakdown" runat="server">
                                <asp:Panel ID="pnlIsPackageSelected" runat="server">
                                    <!--booking-step3-packages-mainbody START HERE-->
                                    <tr bgcolor="#ecf2e3">
                                        <td valign="top" colspan="7">
                                            <h1><b>
                                            <%= GetKeyResult("PACKAGES")%></b>
                                        </h1>
                                        </td>
                                    </tr>
                                    <tr bgcolor="#d4d9cc">
                                        <td colspan="2">
                                        </td>
                                        <td align="left" valign="top" colspan="4">
                                            <strong><%= GetKeyResult("DESCRIPTION")%> </strong>
                                        </td>
                                        <td align="center" valign="top">
                                            <strong><%= GetKeyResult("TOTAL")%></strong>
                                        </td>
                                    </tr>
                                    <tr bgcolor="#ecf2e3">
                                        <td valign="top" colspan="2">
                                            <asp:Label ID="lblSelectedPackage" runat="server"></asp:Label>
                                        </td>
                                        <td valign="top" align="left" colspan="4">
                                            <p>
                                                <asp:Label ID="lblPackageDescription" runat="server"></asp:Label>
                                            </p>
                                        </td>
                                        <td align="center" valign="top">
                                            
                                            <asp:Label ID="lblPackagePrice" runat="server"> </asp:Label>
                                        </td>
                                    </tr>
                                    <asp:Repeater ID="rptPackageItem" runat="server" OnItemDataBound="rptPackageItem_ItemDataBound">
                                        <HeaderTemplate>
                                            <tr bgcolor="#d4d9cc">
                                                <td colspan="3">
                                                </td>
                                                <td align="left" valign="top">
                                                    <b><%= GetKeyResult("STARTINGTIME")%></b>
                                                </td>
                                                <td align="left" valign="top">
                                                   <b> <%= GetKeyResult("STARTINGTIME")%></b>
                                                </td>
                                                <td align="left" valign="top">
                                                    <b><%= GetKeyResult("QTY")%></b>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr bgcolor="#ecf2e3">
                                                <td valign="top" colspan="3">
                                                    <asp:HiddenField ID="hdnItemID" runat="server" />
                                                    <asp:Label ID="lblPackageItem" runat="server"></asp:Label>
                                                </td>
                                                <td align="left" valign="top">
                                                    <asp:Label ID="lblFromTime" runat="server"></asp:Label>
                                                </td>
                                                <td align="left" valign="top">
                                                    <asp:Label ID="lblToTime" runat="server"></asp:Label>
                                                </td>
                                                <td align="left" valign="top">
                                                    <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <asp:Panel ID="pnlIsExtra" runat="server">
                                        <tr bgcolor="#ecf2e3">
                                            <td colspan="7" valign="top">
                                                <asp:Label ID="lblextratitle" runat="server" ><%= GetKeyResult("EXTRAS")%></asp:Label>
                                            </td>
                                        </tr>
                                        <asp:Repeater ID="rptExtra" runat="server" OnItemDataBound="rptExtra_ItemDataBound">
                                            <HeaderTemplate>
                                                <tr bgcolor="#d4d9cc">
                                                    <td colspan="3">
                                                    </td>
                                                    <td width="15%">
                                                       <b> <%= GetKeyResult("FROM")%></b>
                                                    </td>
                                                    <td width="15%">
                                                       <b> <%= GetKeyResult("TO")%></b>
                                                    </td>
                                                    <td width="15%">
                                                      <b>  <%= GetKeyResult("QTY")%></b>
                                                    </td>
                                                    <td>
                                                       <b> <%= GetKeyResult("TOTAL")%></b>
                                                    </td>
                                                </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr bgcolor="#ecf2e3">
                                                    <td valign="top" colspan="3">
                                                        <asp:Label ID="lblPackageItem" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblFrom" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblTo" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblQuntity" runat="server"></asp:Label>
                                                    </td>
                                                    <td>
                                                        
                                                        <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <tr bgcolor="#ecf2e3">
                                            <td align="right" colspan="7">
                                                <span class="currencyClass"></span>
                                                <asp:Label runat="server" ID="lblExtraPrice"></asp:Label>
                                            </td>
                                        </tr>
                                    </asp:Panel>
                                    <tr>
                                        <td colspan="7" align="right" bgcolor="#ffffff">
                                            <%= GetKeyResult("TOTAL")%>:&nbsp; &nbsp; <b>
                                                <asp:Label ID="lblTotalPackagePrice" runat="server"></asp:Label></b>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlBuildMeeting" runat="server">
                                    <!--booking-step3-BuildMeeting-mainbody START HERE-->
                                    <tr bgcolor="#ecf2e3">
                                        <td colspan="7">
                                            <h1><b><%= GetKeyResult("CHOICE2BUILDYOURMEETING")%></b>
                                            </h1>
                                        </td>
                                    </tr>
                                    <asp:Repeater ID="rptBuildMeeting" runat="server" OnItemDataBound="rptBuildMeeting_ItemDataBound">
                                        <HeaderTemplate>
                                            <tr bgcolor="#d4d9cc">
                                                <td colspan="2">
                                                </td>
                                                <td align="left" colspan="3">
                                                    <b><%= GetKeyResult("DESCRIPTION")%></b>
                                                </td>
                                                <td align="center">
                                                   <b> <%= GetKeyResult("QTY")%></b>
                                                </td>
                                                <td align="center">
                                                   <b> <%= GetKeyResult("TOTAL")%></b>
                                                </td>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr bgcolor="#ecf2e3">
                                                <td align="left" valign="top" colspan="2">
                                                    <asp:Label ID="lblItemName" runat="server"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" colspan="3">
                                                    <p>
                                                        <asp:Label ID="lblItemDescription" runat="server"></asp:Label>
                                                    </p>
                                                </td>
                                                <td align="center" valign="top">
                                                    <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                                </td>
                                                <td align="center" valign="top">
                                                    <span class="currencyClass"></span>
                                                    <asp:Label ID="lblTotalItemPrice" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <tr bgcolor="#ffffff" align="right">
                                        <td colspan="7">
                                            <%= GetKeyResult("TOTAL")%>:&nbsp; &nbsp; <b>
                                                <asp:Label ID="lblTotalBuildPackagePrice" runat="server"></asp:Label></b>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <!--booking-step3-BuildMeeting ENDS HERE-->
                                
                                <asp:Panel ID="pnlEquipment" runat="server">
                                    <!--booking-step3-equipment-mainbody START HERE-->
                                    <tr bgcolor="#ecf2e3">
                                        <td colspan="7">
                                            <h3>
                                                <%= GetKeyResult("EQUIPMENT")%></h3>
                                        </td>
                                    </tr>
                                    <asp:Repeater ID="rptEquipment" runat="server" OnItemDataBound="rptEquipment_ItemDataBound">
                                        <HeaderTemplate>
                                            <tr bgcolor="#d4d9cc">
                                                <td colspan="2">
                                                </td>
                                                <td align="left" colspan="3">
                                                    <b><%= GetKeyResult("DESCRIPTION")%></b>
                                                </td>
                                                <td align="center">
                                                    <b><%= GetKeyResult("QTY")%></b>
                                                </td>
                                                <td align="center">
                                                    <b><%= GetKeyResult("TOTAL")%></b>
                                                </td>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr bgcolor="#ecf2e3">
                                                <td colspan="2">
                                                    <asp:Label ID="lblItemName" runat="server"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <p>
                                                        <asp:Label ID="lblItemDescription" runat="server"></asp:Label>
                                                    </p>
                                                </td>
                                                <td align="center">
                                                    <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                                </td>
                                                <td align="center">
                                                    <span class="currencyClass"></span>
                                                    <asp:Label ID="lblTotalPrice" runat="server"> </asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <tr bgcolor="#ffffff">
                                        <td colspan="7" align="right">
                                            <%= GetKeyResult("TOTAL")%>:&nbsp; &nbsp; <b>
                                                <asp:Label ID="lblTotalEquipmentPrice" runat="server"></asp:Label></b>
                                        </td>
                                    </tr>
                                    <!--booking-step3-equipment ENDS HERE-->
                                </asp:Panel>

                                <asp:Panel ID="pnlOthers" runat="server">
                                <!--booking-step3-equipment-mainbody START HERE-->
                                <tr bgcolor="#ecf2e3">
                                    <td colspan="7">
                                        <h1><b>
                                            <%= GetKeyResult("OTHERS")%></b></h1>
                                    </td>
                                </tr>
                                <asp:Repeater ID="rptOthers" runat="server" OnItemDataBound="rptOthers_ItemDataBound">
                                    <HeaderTemplate>
                                        <tr bgcolor="#d4d9cc">
                                            <td colspan="2">
                                            </td>
                                            <td align="left" colspan="3">
                                                <b><%= GetKeyResult("DESCRIPTION")%></b>
                                            </td>
                                            <td align="center">
                                                <b><%= GetKeyResult("QTY")%></b>
                                            </td>
                                            <td align="center">
                                                <b><%= GetKeyResult("TOTAL")%></b>
                                            </td>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr bgcolor="#ecf2e3">
                                            <td colspan="2">
                                                <asp:Label ID="lblItemName" runat="server"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <p>
                                                    <asp:Label ID="lblItemDescription" runat="server"></asp:Label>
                                                </p>
                                            </td>
                                            <td align="center">
                                                <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                            </td>
                                            <td align="center">
                                                
                                                <asp:Label ID="lblTotalPrice" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <tr bgcolor="#ffffff">
                                    <td colspan="7" align="right">
                                        <%= GetKeyResult("TOTAL")%>:&nbsp; &nbsp; <b>
                                            <asp:Label ID="lblTotalOthersPrice" runat="server"></asp:Label></b>
                                    </td>
                                </tr>
                                <!--booking-step3-equipment ENDS HERE-->
                            </asp:Panel>
                            </asp:Panel>
                        </ItemTemplate>
                    </asp:Repeater>
                </AlternatingItemTemplate>
            </asp:Repeater>
            <tr bgcolor="#ecf2e3">
                <td colspan="7">
                    <asp:Panel ID="pnlAccomodation" runat="server">
            <h1>
                <%= GetKeyResult("ACCOMODATION")%>.</h1>            
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <asp:Repeater ID="rptAccomodationDaysManager" runat="server" OnItemDataBound="rptAccomodationDaysManager_ItemDataBound">
                        <HeaderTemplate>
                            <tr bgcolor="#d4d9cc">
                                <td colspan="2" style="border-top: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;
                                    padding: 5px">
                                    <%= GetKeyResult("BEDROOMDATE")%>
                                </td>
                                <asp:Repeater ID="rptDays2" runat="server" OnItemDataBound="rptDays2_ItemDataBound" >
                                    <ItemTemplate>
                                        <td valign="top" style="border-left: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;
                                            border-top: #bfd2a5 solid 1px; padding: 5px">
                                            <asp:Label ID="lblDay" runat="server"></asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendDays" runat="server" ></asp:Literal>
                                <td valign="top" style="border-left: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;
                                    border-top: #bfd2a5 solid 1px; padding: 5px; width: 70px">
                                    <%= GetKeyResult("TOTAL")%>
                                </td>
                            </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td colspan="8" style="padding: 5px">
                                    <strong>&quot;<asp:Label ID="lblBedroomType" runat="server"></asp:Label>&quot;<asp:HiddenField ID="hdnBedroomId" runat="server" /> </strong>
                                </td>
                                <td valign="top" style="padding: 5px" align="center">
                                    
                                </td>
                            </tr>
                            <tr bgcolor="#cee5ad">
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                  <%= GetKeyResult("PRICE")%>  
                                </td>
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("SINGLE")%>/<%= GetKeyResult("DOUBLE")%>
                                </td>
                                <asp:Repeater ID="rptPrice" runat="server" OnItemDataBound="rptPrice_ItemDataBound">
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                            <asp:Label ID="lblPriceDay" runat="server"></asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendPrice" runat="server"></asp:Literal>
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("ONLINEAVAILABILITY") %>
                                </td>
                                <asp:Repeater ID="rptNoOfDays" runat="server" OnItemDataBound="rptNoOfDays_ItemDataBound">
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                            <asp:Label ID="lblAvailableRoomDay" runat="server">3</asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendNoDays" runat="server"></asp:Literal>
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr bgcolor="#cee5ad">
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("YOURQTY")%>
                                </td>
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("SINGLE")%>
                                </td>
                                <asp:Repeater ID="rptSingleQuantity" runat="server" OnItemDataBound="rptSingleQuantity_ItemDataBound" >
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                            <asp:Label ID="lblQuantitySDay" runat="server"></asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendSQuantity" runat="server"></asp:Literal>
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr bgcolor="#cee5ad">
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("DOUBLE")%>
                                </td>
                                <asp:Repeater ID="rptDoubleQuantity" runat="server" OnItemDataBound="rptDoubleQuantity_ItemDataBound">
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                            <asp:Label ID="lblQuantityDDay" runat="server"></asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendDQuantity" runat="server"></asp:Literal>
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">&nbsp;</td>                                
                                <td colspan="4" style="padding: 5px;height:45px;">
                                    &nbsp;
                                </td>
                                <td valign="top" style="padding: 5px" align="center">
                                     <asp:Label ID="lblTotalAccomodation" CssClass="price" runat="server">0.00</asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr>
                                <td colspan="8" style="padding: 5px">
                                    <strong>&quot;<asp:Label ID="lblBedroomType" runat="server"></asp:Label>&quot; <asp:HiddenField ID="hdnBedroomId" runat="server" /></strong>
                                </td>
                                <td valign="top" style="padding: 5px" align="center">
                                    
                                </td>
                            </tr>
                            <tr bgcolor="#cee5ad">
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("PRICE")%>
                                </td>
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("SINGLE")%>/<%= GetKeyResult("DOUBLE")%>
                                </td>
                                <asp:Repeater ID="rptPrice" runat="server" OnItemDataBound="rptPrice_ItemDataBound">
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                            <asp:Label ID="lblPriceDay" runat="server"></asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendPrice" runat="server"></asp:Literal>
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("ONLINEAVAILABILITY") %>
                                </td>
                                <asp:Repeater ID="rptNoOfDays" runat="server" OnItemDataBound="rptNoOfDays_ItemDataBound">
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                            <asp:Label ID="lblAvailableRoomDay" runat="server">3</asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendNoDays" runat="server"></asp:Literal>
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr bgcolor="#cee5ad">
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("YOURQTY")%>
                                </td>
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("SINGLE")%>
                                </td>
                                <asp:Repeater ID="rptSingleQuantity" runat="server" OnItemDataBound="rptSingleQuantity_ItemDataBound" >
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                            <asp:Label ID="lblQuantitySDay" runat="server" ></asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendSQuantity" runat="server"></asp:Literal>
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr bgcolor="#cee5ad">
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("DOUBLE")%>
                                </td>
                                <asp:Repeater ID="rptDoubleQuantity" runat="server" OnItemDataBound="rptDoubleQuantity_ItemDataBound">
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                            <asp:Label ID="lblQuantityDDay" runat="server"></asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendDQuantity" runat="server"></asp:Literal>
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">&nbsp;</td>
                                <td colspan="4" style="padding: 5px;height:45px;">
                                    &nbsp;
                                </td>
                                <td valign="top" style="padding: 5px" align="center">
                                    <asp:Label ID="lblTotalAccomodation" CssClass="price" runat="server">0.00</asp:Label>
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                    <tr>
                        <td colspan="9" style="padding: 5px;" class="special-request-textareabox">
                            <strong><%= GetKeyResult("SPECIALREQUEST")%></strong>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="9" style="padding: 5px;" class="special-request-textareabox">
                            <asp:Label ID="lblNoteBedroom" runat="server" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="padding: 5px">
                            &nbsp;
                        </td>
                        <td valign="top" style="padding: 5px">
                            &nbsp;
                        </td>
                        <td valign="top" style="padding: 5px">
                            &nbsp;
                        </td>
                        <td valign="top" style="padding: 5px">
                            &nbsp;
                        </td>
                        <td valign="top" style="padding: 5px">
                            &nbsp;
                        </td>
                        <td colspan="3" valign="top" style="padding: 5px" align="right">
                            <%= GetKeyResult("ACCOMODATION")%> :
                        </td>
                        <td valign="top" style="padding: 5px">
                           <asp:Label ID="lblTotalAccomodation" CssClass="price" runat="server">0.00</asp:Label>
                        </td>
                    </tr>
                </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr bgcolor="#ecf2e3">
                <td colspan="7">
                    <h3><%= GetKeyResult("SPECIALREQUEST")%></h3>
                </td>
            </tr>
            <tr bgcolor="#ecf2e3">
                <td colspan="7">
                    <asp:Label ID="lblSpecialRequest" runat="server">No Special Requests</asp:Label>
                </td>
            </tr>
            <tr bgcolor="#ffffff" valign="top">
                <td colspan="7" align="right">
                    <%= GetKeyResult("FINALTOTAL")%>:
                    <asp:Label ID="lblFinalTotal" runat="server"></asp:Label>
                    <!--special-request ENDS HERE-->
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    <p>
                        <asp:LinkButton ID="tAndCPopUp" runat="server" Visible="false"><%= GetKeyResult("CANCELLATIONTEXT")%>.</asp:LinkButton></p>
                </td>
            </tr>
        </table>
    </div>
    <div id="Nettotalview" runat="server" style="display: none;">
    <table width="100%" bgcolor="#ffffff" cellspacing="1" cellpadding="8">
    <tr>
    <td align="right" >Net Total: <span class="currencyClass"></span>&#8364;
            <asp:Label ID="Label2" runat="server"> </asp:Label></td>
    </tr>
        </table>
    </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="lnkbtnCancel" />
    </Triggers>
    </asp:UpdatePanel>
</asp:Content>

<asp:Content ID="ContentLeftSearch" ContentPlaceHolderID="cntLeftSearch" runat="server" >
<uc2:LeftSearchPanel ID="LeftSearchPanel1" runat="server" />
</asp:Content>