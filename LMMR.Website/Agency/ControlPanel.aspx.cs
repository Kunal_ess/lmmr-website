﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using System.Data;
using System.Web.UI.DataVisualization.Charting;

public partial class Agency_ControlPanel : System.Web.UI.Page
{
    public ManageAgentUser objAgent = new ManageAgentUser();
    Viewstatistics objViewstatistics = new Viewstatistics();
    ViewBooking_Hotel objViewHotelBooikng = new ViewBooking_Hotel();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindControlPanelGrid();
            
        }
        Bindlist();
        ApplyPaging();
    }

    void BindControlPanelGrid()
    {
        if (Session["CurrentAgencyUserID"] != null)
        {
            TList<Users> objUsers = objAgent.GetAllAgentByAgencyIdWithOutActive(Convert.ToInt64(Session["CurrentAgencyUserID"]));
            objUsers.Add((Users)Session["CurrentAgencyUser"]);
            grdViewControlPanel.DataSource = objUsers;
            grdViewControlPanel.DataBind();
        }
    }

    private void ApplyPaging()
    {
        try
        {
            GridViewRow row = grdViewControlPanel.TopPagerRow;
            if (row != null)
            {
                PlaceHolder ph;
                LinkButton lnkPaging;
                LinkButton lnkPrevPage;
                LinkButton lnkNextPage;
                lnkPrevPage = new LinkButton();
                lnkPrevPage.CssClass = "pre";
                lnkPrevPage.Width = Unit.Pixel(73);
                lnkPrevPage.CommandName = "Page";
                lnkPrevPage.CommandArgument = "prev";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPrevPage);
                if (grdViewControlPanel.PageIndex == 0)
                {
                    lnkPrevPage.Enabled = false;

                }
                for (int i = 1; i <= grdViewControlPanel.PageCount; i++)
                {
                    lnkPaging = new LinkButton();
                    if (ViewState["CurrentPage"] != null)
                    {
                        if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    else
                    {
                        if (i == 1)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    lnkPaging.Width = Unit.Pixel(16);
                    lnkPaging.Text = i.ToString();
                    lnkPaging.CommandName = "Page";
                    lnkPaging.CommandArgument = i.ToString();
                    if (i == grdViewControlPanel.PageIndex + 1)
                        ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkPaging);
                }
                lnkNextPage = new LinkButton();
                lnkNextPage.CssClass = "nex";
                lnkNextPage.Width = Unit.Pixel(42);
                lnkNextPage.CommandName = "Page";
                lnkNextPage.CommandArgument = "next";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkNextPage);
                ph = (PlaceHolder)row.FindControl("ph");
                if (grdViewControlPanel.PageIndex == grdViewControlPanel.PageCount - 1)
                {
                    lnkNextPage.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            //logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    protected void grdViewControlPanel_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            grdViewControlPanel.PageIndex = e.NewPageIndex;
            ViewState["CurrentPage"] = e.NewPageIndex;
            BindControlPanelGrid();
            ApplyPaging();
        }
        catch (Exception ex)
        {

        }

    }
    protected void grdViewControlPanel_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            long intDataKey = Convert.ToInt32(grdViewControlPanel.DataKeys[e.Row.RowIndex].Value);
            Users objusers = e.Row.DataItem as Users;
            TList<UserDetails> objuserDetail = objAgent.GetUserDetailByID(intDataKey);
            DateTime baseDate = DateTime.Today;
            VList<Viewbookingrequest> obj = new VList<Viewbookingrequest>();
            Label lblUser = (Label)e.Row.FindControl("lblUser");
            Label lblUserId = (Label)e.Row.FindControl("lblUserId");
            if (objuserDetail != null)
            {
                if (objuserDetail[0].CompanyName != null)
                {
                    lblUserId.Text = objuserDetail[0].CompanyName;
                }

            }
            Label lblTotalBooking = (Label)e.Row.FindControl("lblTotalBooking");
            Label lblMonthBooking = (Label)e.Row.FindControl("lblMonthBooking");
            Label lblYearBooking = (Label)e.Row.FindControl("lblYearBooking");
            Label lblTotalRequest = (Label)e.Row.FindControl("lblTotalRequest");
            Label lblMonthRequest = (Label)e.Row.FindControl("lblMonthRequest");
            Label lblYearRequest = (Label)e.Row.FindControl("lblYearRequest");
            lblUser.Text = objusers.FirstName + " " + objusers.LastName;


            DateTime Todaydate = DateTime.Today;
            string fromDateToday = string.Format("{0:yyyy-MM-dd}", Todaydate);
            string toDateToday = string.Format("{0:yyyy-MM-dd}", Todaydate.AddDays(1));
            obj = GetBooingRequest("CreatorId=" + objusers.UserId + " And BookType=0 And BookingDate BETWEEN '" + fromDateToday + "' And '" + toDateToday + "'"); ;
            lblTotalBooking.Text = obj.Count.ToString();

            obj = GetBooingRequest("CreatorId=" + objusers.UserId + " And BookType in(1,2) And BookingDate BETWEEN '" + fromDateToday + "' And '" + toDateToday + "'");
            lblTotalRequest.Text = obj.Count.ToString();

            DateTime startDate = baseDate.AddDays(1 - baseDate.Day);
            DateTime endDate = DateTime.Today;
            string toDate = string.Format("{0:yyyy-MM-dd}", endDate);
            string fromDate = string.Format("{0:yyyy-MM-dd}", endDate.AddDays(-Convert.ToInt32(System.DateTime.Now.Date.ToString("dd"))));

            obj = GetBooingRequest("CreatorId=" + objusers.UserId + " And BookType=0 And BookingDate BETWEEN '" + fromDate + "' And '" + toDate + "'");
            lblMonthBooking.Text = obj.Count.ToString();

            obj = GetBooingRequest("CreatorId=" + objusers.UserId + " And BookType in(1,2) And BookingDate BETWEEN '" + fromDate + "' And '" + toDate + "'");
            lblMonthRequest.Text = obj.Count.ToString();

            DateTime startDatey = GetFirstDayOfTheYear();
            DateTime endDatey = DateTime.Today;
            string toDatey = string.Format("{0:yyyy-MM-dd}", endDatey);
            string fromDatey = string.Format("{0:yyyy-MM-dd}", endDate.AddDays(-(System.DateTime.Now.DayOfYear)));

            obj = GetBooingRequest("CreatorId=" + objusers.UserId + " And BookType=0 And BookingDate BETWEEN '" + fromDatey + "' And '" + toDatey + "'");
            lblYearBooking.Text = obj.Count.ToString();

            obj = GetBooingRequest("CreatorId=" + objusers.UserId + " And BookType in(1,2) And BookingDate BETWEEN '" + fromDatey + "' And '" + toDatey + "'");
            lblYearRequest.Text = obj.Count.ToString();
        }
    }

    static DateTime GetFirstDayOfTheYear()
    {
        return FirstDayOfYear(DateTime.Today);
    }
    static DateTime FirstDayOfYear(DateTime y)
    {
        return new DateTime(y.Year, 1, 1);
    }
    public VList<Viewbookingrequest> GetBooingRequest(string where)
    {
        VList<Viewbookingrequest> objBookingRequest = objViewHotelBooikng.Bindgrid(where, string.Empty);

        return objBookingRequest;
    }




    #region BindList
    /// <summary>
    /// BindList
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Bindlist()
    {
        // objViewstatistics.HotelId = (Session["CurrentAgencyUserID"].ToString());%



        if (Session["CurrentAgencyUserID"] != null)
        {
            int user = Convert.ToInt32(Session["CurrentAgencyUserID"].ToString());
            // string quertype = "1";// Convert.ToString(Request.QueryString["type"]);
            string Fromdt = System.DateTime.Now.AddMonths(-6).ToShortDateString(); //System.DateTime.Now.AddMonths(- ( (System.DateTime.Now.Month)-1)).ToShortDateString();
            string Todt = System.DateTime.Now.AddMonths(6).ToShortDateString();
            DateTime fromdate = new DateTime(System.DateTime.Now.AddMonths(-6).Year, System.DateTime.Now.AddMonths(-6).Month, System.DateTime.Now.AddMonths(-6).Day);
            DateTime todate = new DateTime(System.DateTime.Now.AddMonths(6).Year, System.DateTime.Now.AddMonths(6).Month, System.DateTime.Now.AddMonths(6).Day);




            chrtBooking.ChartAreas[0].AxisX.Title = " Date of Booking";
            chrtBooking.ChartAreas[0].AxisY.Title = " Revenue Amount ";
            chrtReq.ChartAreas[0].AxisX.Title = " Date of Request";
            chrtReq.ChartAreas[0].AxisY.Title = " No of Request ";

            // var datasourcedays = objViewstatistics.Getcontrolpaneluser(fromdate, todate, "2", user);

            DataTable dt = objViewstatistics.Getcontrolpaneluser(fromdate, todate, "1", user);
            int cnt = 0;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    // For each Row add a new series
                    string seriesName = row["Username"].ToString() + cnt.ToString();
                    chrtBooking.Series.Add(seriesName);
                    chrtBooking.Series[seriesName].ChartType = SeriesChartType.Spline;
                    chrtBooking.Series[seriesName].BorderWidth = 4;

                    for (int colIndex = 1; colIndex < dt.Columns.Count; colIndex++)
                    {
                        // For each column (column 1 and onward) add the value as a point
                        string columnName = dt.Columns[colIndex].ColumnName;
                        int YVal = Convert.ToInt32(row[columnName]);

                        chrtBooking.Series[seriesName].Points.AddXY(columnName, YVal);
                    }
                    cnt++;
                }
            }
            else
            {
                string seriesName = "2013 A";
                chrtBooking.Series.Add(seriesName);
                chrtBooking.Series[seriesName].ChartType = SeriesChartType.Spline;
                chrtBooking.Series[seriesName].BorderWidth = 4;
                chrtBooking.Series[seriesName].Points.AddXY(DateTime.Now.Year.ToString(), "0");
            }



            //----   for requets graph
            dt = new DataTable();
            dt = objViewstatistics.Getcontrolpaneluser(fromdate, todate, "2", user);
            cnt = 0;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    // For each Row add a new series
                    string seriesName = row["Username"].ToString() + cnt.ToString();
                    chrtReq.Series.Add(seriesName);
                    chrtReq.Series[seriesName].ChartType = SeriesChartType.Line;
                    chrtReq.Series[seriesName].BorderWidth = 2;

                    for (int colIndex = 1; colIndex < dt.Columns.Count; colIndex++)
                    {
                        // For each column (column 1 and onward) add the value as a point
                        string columnName = dt.Columns[colIndex].ColumnName;
                        int YVal = Convert.ToInt32(row[columnName]);
                        chrtReq.Series[seriesName].Points.AddXY(columnName, YVal);
                    }
                    cnt++;
                }
            }
            else
            {
                string seriesName = "2013 A";
                chrtReq.Series.Add(seriesName);
                chrtReq.Series[seriesName].ChartType = SeriesChartType.Spline;
                chrtReq.Series[seriesName].BorderWidth = 4;
                chrtReq.Series[seriesName].Points.AddXY(DateTime.Now.Year.ToString(), "0");
            }




        }
        else
        {
            return;
        }
    }

    #endregion
}
