﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Configuration;
using log4net;
using log4net.Config;
using System.Xml;
using System.Net.Mail;
using System.Net.Mime;

public partial class Agency_RequestStep4 : BasePage
{
    #region Variable and Properties

    public CreateRequest objRequest = null;
    public UserControl_Agency_LeftSearchPanel uc
    {
        get
        {
            return Page.Master.FindControl("cntLeftSearch").FindControl("LeftSearchPanel1") as UserControl_Agency_LeftSearchPanel;
        }
    }
    VList<ViewBookingHotels> vlist;
    VList<Viewbookingrequest> vlistreq;
    ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
    HotelManager objHotelManager = new HotelManager();
    string Typelink = "", AdminUser = "", Hotelid = "0";
    ILog logger = log4net.LogManager.GetLogger(typeof(Agency_RequestStep4));
    WizardLinkSettingManager ObjWizardManager = new WizardLinkSettingManager();
    Createbooking objBooking = null;
    BookingManager bm = new BookingManager();
    CurrencyManager cm = new CurrencyManager();
    HotelManager objHotel = new HotelManager();
    PackagePricingManager objPackagePricingManager = new PackagePricingManager();
    Hotel objRequestHotel = new Hotel();

    int countMr = 0;

    public List<PackageItemDetails> SelectedPackage
    {
        get;
        set;
    }
    public List<RequestExtra> SelectedExtra
    {
        get;
        set;
    }
    public TList<PackageItems> AllPackageItem
    {
        get;
        set;
    }
    public TList<PackageItems> AllFoodAndBravrages
    {
        get;
        set;
    }
    public TList<PackageItems> AllEquipments
    {
        get;
        set;
    }
    public TList<PackageItems> AllOthers
    {
        get;
        set;
    }
    public TList<PackageByHotel> AllPackageByHotel
    {
        get;
        set;
    }

    public string CurrencySign
    {
        get;
        set;
    }
    public XmlDocument XMLLanguage
    {
        get { return (XmlDocument)ViewState["Language"]; }
        set { ViewState["Language"] = value; }
    }
    
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentAgencyUserID"] == null || Session["CurrentAgencyUser"] == null)
        {
            Response.Redirect(SiteRootPath + "login/english");
        }
        uc.SearchButton += new EventHandler(u_SearchButton);
        uc.MapSearch += new EventHandler(u_MapSearch);
        if (!IsPostBack)
        {
            uc.usercontroltype = "Basic";
            Users objUsers = Session["CurrentAgencyUser"] as Users;
            //Bind Login users details
            //lblLoginTime.Text = DateTime.Now.ToString("dd MMM yyyy");
            //lblLoginUser.Text = objUsers.FirstName;
            //lblClientName.Text = objUsers.FirstName + " " + objUsers.LastName;
            SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["RequestID"]));
            if (st != null)
            {
                objRequest = (CreateRequest)TrailManager.XmlDeserialize(typeof(CreateRequest), st.SearchObject);
                //Bind Booking Details
                BindHotel();
                Divdetails.Visible = false;
                Divdetails.Visible = true;

                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);
                PrepareDivForExport(Divdetails);
                Divdetails.RenderControl(hw);
                StringReader sr = new StringReader(sw.ToString());
                Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);

                Divdetails.Visible = false;
                var dirPath = Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "Temp/");
                var filePath = dirPath + Guid.NewGuid().ToString().Replace("-", string.Empty) + ".pdf";

                try
                {
                    FileStream fileStream = new FileStream(filePath, FileMode.Create);
                    PdfWriter.GetInstance(pdfDoc, fileStream);
                    pdfDoc.Open();
                    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                    htmlparser.Parse(sr);
                    fileStream.Flush();
                    pdfDoc.Close();
                    fileStream.Close();
                    fileStream.Dispose();
                    Divdetails.Visible = false;
                }
                catch (Exception ex)
                {
                    Divdetails.Visible = false;
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
                Divdetails.Visible = false;


                if (DoneRequest(filePath))
                {
                    DirectoryInfo dir = new DirectoryInfo(dirPath);
                    DateTime dateTime = DateTime.Now.AddMinutes(-10);
                    foreach (var file in dir.GetFiles().Where(u => u.CreationTime < dateTime).OrderBy(u => u.CreationTime))
                    {
                        try
                        {
                            file.Delete();
                        }
                        catch(Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                    }
                    divMessage.Attributes.Add("class", "succesfuly");
                    divMessage.InnerHtml = GetKeyResult("REQUESTDONESUCCESSFUL") + "(" + RequestIds + ").";
                    Session.Remove("RequestID");
                    Session.Remove("Request");
                    Divdetails.Visible = false;
                    //Divdetails.Visible = true;
                    Session["BookingDone"] = "done";

                }
                else
                {
                    Divdetails.Visible = false;
                    divMessage.Attributes.Add("class", "error");
                    divMessage.InnerHtml = GetKeyResult("REQUESTNOTDONESUCCESSFUL");
                }
            }
            else
            {
                Response.Redirect("~/Agency/controlpaneluser.aspx");
            }
            
        }
    }
    BookingRequest objBookingRequest = new BookingRequest();
    void u_SearchButton(object sender, EventArgs e)
    {
        Session["CurrentUserSeleted"] = Convert.ToString(Session["CurrentAgencyUserID"]);
        ManageSession();
        Session["ComeFromBookingAndRequestPage"] = "Request";
        Response.Redirect("~/Agency/SearchBookingRequest.aspx");
    }
    void u_MapSearch(object sender, EventArgs e)
    {
        Session["CurrentUserSeleted"] = Convert.ToString(Session["CurrentAgencyUserID"]);
        ManageSession();
        Session["ComeFromBookingAndRequestPage"] = "Request";
        Response.Redirect("~/Agency/SearchBookingRequest.aspx");
    }
    void ManageSession()
    {
        string WhereClause = string.Empty;
        string WhereClauseday2 = string.Empty;
        if (uc.propCountryID != "0")
        {
            if (uc.propCountryID != "")
            {
                if (WhereClause.Length > 0)
                {
                    WhereClause += " and ";
                    WhereClauseday2 += " and ";
                }
                WhereClause += HotelColumn.CountryId + "=" + uc.propCountryID;
                WhereClauseday2 += HotelColumn.CountryId + "=" + uc.propCountryID;
            }
        }

        if (Convert.ToInt32(uc.propCity) != 0)
        {
            if (uc.propCity != "")
            {
                if (WhereClause.Length > 0)
                {
                    WhereClause += " and ";
                    WhereClauseday2 += " and ";
                }
                WhereClause += HotelColumn.CityId + "=" + uc.propCity;
                WhereClauseday2 += HotelColumn.CityId + "=" + uc.propCity;
            }
        }
        if (uc.propDuration == "1")
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                if (uc.propDay1 == "0")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (uc.propDay1 == "1")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (uc.propDay1 == "2")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
            }
        }
        if (uc.propDuration == "2")
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                WhereClauseday2 += " and ";
                if (uc.propDay1 == "0")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (uc.propDay1 == "1")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (uc.propDay1 == "2")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }


                if (uc.propDay2 == "0")
                {
                    WhereClauseday2 += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (uc.propDay2 == "1")
                {
                    WhereClauseday2 += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (uc.propDay2 == "2")
                {
                    WhereClauseday2 += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
            }
        }

        if (uc.propDate != "")
        {
            DateTime fromDate = new DateTime(Convert.ToInt32("20" + uc.propDate.Split('/')[2]), Convert.ToInt32(uc.propDate.Split('/')[1]), Convert.ToInt32(uc.propDate.Split('/')[0]));

            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                WhereClauseday2 += " and ";
            }
            WhereClause += AvailabilityColumn.AvailabilityDate + "='" + fromDate + "'" + " and " + " (DATEDIFF(day, GETDATE(),'" + fromDate + "')) >= " + AvailabilityColumn.LeadTimeForMeetingRoom + "";
            WhereClauseday2 += AvailabilityColumn.AvailabilityDate + "='" + fromDate.AddDays(1) + "'" + " and " + " (DATEDIFF(day, GETDATE(),'" + fromDate.AddDays(1) + "')) >= " + AvailabilityColumn.LeadTimeForMeetingRoom + "";
        }


        if (uc.propParticipant != "" && uc.propParticipant != "0")
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                WhereClauseday2 += " and ";
            }
            WhereClause += uc.propParticipant + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + "";
            WhereClauseday2 += uc.propParticipant + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + "";
        }

        //Send all search session value
        Session["Where"] = WhereClause;
        if (uc.propDuration == "2")
        {
            Session["Where2"] = WhereClauseday2;
        }

        //Maintain session value for all controls 
        objBookingRequest = new BookingRequest();
        objBookingRequest.propCountry = uc.propCountryID;
        objBookingRequest.propCity = uc.propCity;
        objBookingRequest.propDate = Convert.ToString(string.IsNullOrEmpty(uc.propDate) ? (uc.propDate == "dd/mm/yy" ? DateTime.Now.ToString("dd/MM/yy") : uc.propDate) : uc.propDate);
        objBookingRequest.propDuration = uc.propDuration;
        objBookingRequest.propDays = uc.propDay1;
        objBookingRequest.propDay2 = uc.propDay2;
        if (uc.propParticipant != "")
        {
            objBookingRequest.propParticipants = uc.propParticipant;
        }
        else
        {
            objBookingRequest.propParticipants = "0";
        }
        Session["masterInput"] = objBookingRequest;
        Session["CurrencyID"] = "2";
        //Dictionary<long, string> participants = new Dictionary<long, string>();
        //participants.Add(1, objBookingRequest.propParticipants);
        //Session["participants"] = participants;
    }
    private string RequestIds = "";
    public bool DoneRequest(string FilePath)
    {
        return bm.CreateRequestByAgency(objRequest, ref RequestIds, FilePath, Convert.ToInt64(Session["CurrentAgencyUserID"]), Convert.ToInt64(Session["CurrentUserSeleted"]));
    }
    /// <summary>
    /// This method returns the resultant string for the passed key
    /// </summary>
    /// <param name="key"></param>
    /// <returns>string</returns>
    //public string GetKeyResult(string key)
    //{
    //    return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    //}
    #region VerifyRenderingInServerForm
    /// <summary>
    /// method to VerifyRenderingInServerForm
    /// </summary>

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    #endregion

    #region PrepareGridViewForExport
    /// <summary>
    /// method to PrepareGridViewForExport a div
    /// </summary>
    private void PrepareDivForExport(Control gv)
    {
        try
        {

            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {

                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].GetType() == typeof(System.Web.UI.HtmlControls.HtmlAnchor))
                {

                    l.Text = (gv.Controls[i] as System.Web.UI.HtmlControls.HtmlAnchor).InnerText;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(DropDownList))
                {

                    l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(GridView))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    // gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Repeater))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Panel))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Table))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }


                else if (gv.Controls[i].GetType() == typeof(CheckBox))
                {

                    l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(HyperLink))
                {

                    l.Text = (gv.Controls[i] as HyperLink).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                //else if (gv.Controls[i].GetType() == typeof(Label))
                //{

                //    l.Text = (gv.Controls[i] as Label).Text;

                //    gv.Controls.Remove(gv.Controls[i]);

                //    gv.Controls.AddAt(i, l);

                //}
                else if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.Image))
                {

                    // l.Text = (gv.Controls[i] as Image).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareDivForExport(gv.Controls[i]);

                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        //PrepareGridViewForExport();
        //PrepareGridViewForExport();

    }
    #endregion
    #region Bookingbind


    List<VatCollection> _VatCalculation = new List<VatCollection>();
    public List<VatCollection> VatCalculation
    {
        get
        {
            if (_VatCalculation == null)
            {
                _VatCalculation = new List<VatCollection>();
            }
            return _VatCalculation;
        }
        set
        {
            _VatCalculation = value;
        }
    }



    public void BindHotel()
    {
        try
        {
            var hotelID = Convert.ToInt64(Hotelid);
            rptHotel.DataSource = objRequest.HotelList.Where(a => a.MeetingroomList.Count > 0);
            rptHotel.DataBind();

            lblFromDt.Text = objRequest.ArivalDate.ToString("dd MMM yyyy");
            Users obj = (Users)Session["CurrentAgencyUser"];
            TList<UserDetails> objDetails = objViewBooking_Hotel.getuserdetails(obj.UserId);
            lblConatctpersonEmail.Text = obj.EmailId;
            lblContactPerson.Text = obj.FirstName + " " + obj.LastName;
            if (objDetails.Count > 0)
            {
                lblContactAddress.Text = objDetails[0].Address;
                lblContactPhone.Text = objDetails[0].Phone;
            }
            lblToDate.Text = objRequest.DepartureDate.ToString("dd MMM yyyy");
            lblBookedDays.Text = objRequest.Duration == 1 ? objRequest.Duration + GetKeyResult("DAY") : objRequest.Duration + GetKeyResult("DAY");
            Int64 intHotelID = objRequest.HotelList[0].HotelID;
            if (objRequest.PackageID != 0)
            {
                pnlPackage.Visible = true;
                PackageMaster packagedetails = objPackagePricingManager.GetAllPackageName().Where(a => a.Id == objRequest.PackageID).FirstOrDefault();
                objHotel.GetDescriptionofPackageByPackage(packagedetails);
                if (packagedetails != null)
                {
                    lblPackageName.Text = packagedetails.PackageName;
                    PackageMasterDescription des = packagedetails.PackageMasterDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64(Session["LanguageID"])).FirstOrDefault();
                    if (des == null)
                    {
                        des = packagedetails.PackageMasterDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64("1")).FirstOrDefault();
                    }
                    if (des != null)
                    {
                        lblpackageDescription.Text = des.Description;
                    }
                }
                AllPackageItem = objPackagePricingManager.GetAllPackageItems();
                rptPackageItem.DataSource = objPackagePricingManager.GetPackageItemsByPackageID(objRequest.PackageID);
                rptPackageItem.DataBind();
                if (objRequest.ExtraList.Count > 0)
                {
                    //Package Selection.
                    //AllPackageItem = objPackagePricingManager.GetAllPackageItems();
                    pnlExtra.Visible = true;
                    rptExtras.DataSource = objRequest.ExtraList;
                    rptExtras.DataBind();//check AllPackageItem.Where(a => a.IsExtra == true)
                }
                else
                {
                    pnlExtra.Visible = false;
                }
                pnlFoodAndBravrages.Visible = false;
            }
            else
            {
                pnlPackage.Visible = false;

                if (objRequest.BuildYourMeetingroomList.Count > 0)
                {
                    AllFoodAndBravrages = objPackagePricingManager.GetAllFoodBeveragesItems(intHotelID);
                    pnlFoodAndBravrages.Visible = true;
                    rptFoodandBravragesDay.DataSource = objRequest.DaysList;
                    rptFoodandBravragesDay.DataBind();
                }
                else
                {
                    pnlFoodAndBravrages.Visible = false;
                }
            }


            if (objRequest.EquipmentList.Count > 0)
            {
                AllEquipments = objPackagePricingManager.GetAllEquipmentItems(intHotelID);
                pnlEquipment.Visible = true;
                rptEquipmentDay.DataSource = objRequest.DaysList;
                rptEquipmentDay.DataBind();
            }
            else
            {
                pnlEquipment.Visible = false;
            }
            if (objRequest.OthersList.Count > 0)
            {
                AllOthers = objPackagePricingManager.GetAllOtherItems(intHotelID);
                pnlOthers.Visible = true;
                rptOthersDay.DataSource = objRequest.DaysList;
                rptOthersDay.DataBind();
            }
            else
            {
                pnlOthers.Visible = false;
            }
            if (objRequest.IsAccomodation)
            {
                //pnlAccomodation.Visible = true;
                //lblaccomodationQun.Text = Convert.ToString(objRequest.RequestAccomodationList[0].QuantityDouble);
                rptDays2.DataSource = objRequest.RequestAccomodationList;
                rptDays2.DataBind();
                rptSingleQuantity.DataSource = objRequest.RequestAccomodationList;
                rptSingleQuantity.DataBind();
                rptDoubleQuantity.DataSource = objRequest.RequestAccomodationList;
                rptDoubleQuantity.DataBind();
                if (objRequest.RequestAccomodationList.Count < 6)
                {
                    ltrExtendDays.Text = "<td colspan='" + (6 - objRequest.RequestAccomodationList.Count) + "' style='border-top: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;padding: 5px;' >&nbsp;</td>";
                    ltrExtendSQuantity.Text = "<td colspan='" + (6 - objRequest.RequestAccomodationList.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
                    ltrExtendDQuantity.Text = "<td colspan='" + (6 - objRequest.RequestAccomodationList.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
                }
            }
            else
            {
                //pnlAccomodation.Visible = true;
            }
            lblSpecialRequest.Text = objRequest.SpecialRequest;
        }
        catch (Exception ex)
        {
            //    logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    #region New Accommodation
    protected void rptDays2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblDay = (Label)e.Item.FindControl("lblDay");
            RequestAccomodation a = e.Item.DataItem as RequestAccomodation;
            lblDay.Text = a.Checkin.ToString("dd/MM/yyyy");
        }
    }
    protected void rptSingleQuantity_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label txtQuantitySDay = (Label)e.Item.FindControl("txtQuantitySDay");
            HiddenField hdnDate = (HiddenField)e.Item.FindControl("hdnDate");
            RequestAccomodation a = e.Item.DataItem as RequestAccomodation;
            txtQuantitySDay.Text = a.QuantitySingle.ToString();
            hdnDate.Value = a.Checkin.ToString();
        }
    }
    protected void rptDoubleQuantity_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label txtQuantityDDay = (Label)e.Item.FindControl("txtQuantityDDay");
            HiddenField hdnDate = (HiddenField)e.Item.FindControl("hdnDate");
            RequestAccomodation a = e.Item.DataItem as RequestAccomodation;
            txtQuantityDDay.Text = a.QuantityDouble.ToString();
            hdnDate.Value = a.Checkin.ToString();
        }
    }
    #endregion

    protected void rptPackageItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label txtQuantity = (Label)e.Item.FindControl("txtQuantity");
            //HiddenField hdnItemId = (HiddenField)e.Item.FindControl("hdnItemId");
            PackageItemMapping pim = e.Item.DataItem as PackageItemMapping;
            if (pim != null)
            {
                PackageItems p = AllPackageItem.Where(a => a.Id == pim.ItemId).FirstOrDefault();
                if (p != null)
                {
                    //hdnItemId.Value = Convert.ToString(p.Id);

                    PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblItemName.Text = pdesc.ItemName;
                        lblDescription.Text = pdesc.ItemDescription;
                    }
                    else
                    {
                        lblItemName.Text = p.ItemName;
                        lblDescription.Text = "";
                    }
                    PackageItemDetails pid = objRequest.PackageItemList.Where(a => a.ItemID == p.Id).FirstOrDefault();
                    if (pid != null)
                    {
                        txtQuantity.Text = Convert.ToString(pid.Quantity);
                    }
                    else
                    {
                        txtQuantity.Text = "0";
                    }
                }
            }
        }
    }



    protected void rptHotel_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            // Label lblIndex = (Label)e.Item.FindControl("lblIndex");
            Label lblHotelName = (Label)e.Item.FindControl("lblHotelName");
            Repeater rptMeetingroom = (Repeater)e.Item.FindControl("rptMeetingroom");
            BuildHotelsRequest b = e.Item.DataItem as BuildHotelsRequest;
            if (b != null)
            {
                //  lblIndex.Text = (e.Item.ItemIndex + 1).ToString();
                Hotel objHtl = objHotel.GetHotelDetailsById(b.HotelID);
                lblHotelName.Text = objHtl.Name;
                rptMeetingroom.DataSource = b.MeetingroomList;
                rptMeetingroom.DataBind();
            }
        }
    }

    protected void rptMeetingroom_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //  Label lblIndex = (Label)e.Item.FindControl("lblIndex");
            Label lblMeetingRoomName = (Label)e.Item.FindControl("lblMeetingRoomName");
            Label lblConfigurationType = (Label)e.Item.FindControl("lblConfigurationType");
            Label lblMaxandMinCapacity = (Label)e.Item.FindControl("lblMaxandMinCapacity");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            Label lblIsMain = (Label)e.Item.FindControl("lblIsMain");
            BuildMeetingRoomRequest brm = e.Item.DataItem as BuildMeetingRoomRequest;
            if (brm != null)
            {
                // lblIndex.Text = (e.Item.ItemIndex + 1).ToString();                
                MeetingRoom objMeetingroom = objHotel.GetMeetingRoomDetailsById(brm.MeetingRoomID);
                MeetingRoomDesc objDesc = objMeetingroom.MeetingRoomDescCollection.Where(a => a.LanguageId == Convert.ToInt32(Session["LanguageID"])).FirstOrDefault();
                MeetingRoomConfig objMrConfig = objMeetingroom.MeetingRoomConfigCollection.Where(a => a.Id == brm.ConfigurationID).FirstOrDefault();
                lblMeetingRoomName.Text = objMeetingroom.Name;
                lblConfigurationType.Text = (objMrConfig.RoomShapeId == (int)RoomShape.Boardroom ? RoomShape.Boardroom.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Classroom ? RoomShape.Classroom.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Cocktail ? RoomShape.Cocktail.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.School ? RoomShape.School.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Theatre ? RoomShape.Theatre.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.UShape ? RoomShape.UShape.ToString() : RoomShape.Boardroom.ToString());
                lblMaxandMinCapacity.Text = objMrConfig.MinCapacity + " - " + objMrConfig.MaxCapicity;
                lblQuantity.Text = brm.Quantity.ToString();
                lblIsMain.Text = brm.IsMain == true ? GetKeyResult("MAIN") : GetKeyResult("BREAKOUT");
            }
        }
    }

    protected void rptExtras_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            RequestExtra r = e.Item.DataItem as RequestExtra;
            if (r != null)
            {
                PackageItems objPackage = AllPackageItem.Where(a => a.IsExtra == true && a.Id == r.ItemID).FirstOrDefault();
                if (objPackage != null)
                {

                    PackageDescription pdesc = objPackage.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblItemName.Text = pdesc.ItemName;
                        lblDescription.Text = pdesc.ItemDescription;
                    }
                    else
                    {
                        lblItemName.Text = objPackage.ItemName;
                        lblDescription.Text = "";
                    }
                    lblQuantity.Text = Convert.ToString(r.Quantity);
                }
            }
        }
    }

    protected void rptFoodandBravragesDay_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblSelectDay = (Label)e.Item.FindControl("lblSelectDay");
            Repeater rptFoodandBravragesItem = (Repeater)e.Item.FindControl("rptFoodandBravragesItem");
            NumberOfDays nod = e.Item.DataItem as NumberOfDays;
            if (nod != null)
            {
                lblSelectDay.Text = nod.SelectedDay.ToString();
                rptFoodandBravragesItem.DataSource = objRequest.BuildYourMeetingroomList.Where(a => a.SelectDay == nod.SelectedDay);
                rptFoodandBravragesItem.DataBind();
            }
        }
    }

    protected void rptFoodandBravragesItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {


        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            BuildYourMeetingroomRequest bm = e.Item.DataItem as BuildYourMeetingroomRequest;
            if (bm != null)
            {
                PackageItems p = AllFoodAndBravrages.Where(a => a.Id == bm.ItemID).FirstOrDefault();

                if (p != null)
                {

                    PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblItemName.Text = pdesc.ItemName;
                        lblDescription.Text = pdesc.ItemDescription;
                    }
                    else
                    {
                        lblItemName.Text = p.ItemName;
                        lblDescription.Text = "";
                    }
                    lblQuantity.Text = Convert.ToString(bm.Quantity);
                }
            }
        }
    }

    protected void rptEquipmentDay_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblSelectDay = (Label)e.Item.FindControl("lblSelectDay");
            Repeater rptEquipmentItem = (Repeater)e.Item.FindControl("rptEquipmentItem");
            NumberOfDays nod = e.Item.DataItem as NumberOfDays;
            if (nod != null)
            {
                lblSelectDay.Text = nod.SelectedDay.ToString();
                rptEquipmentItem.DataSource = objRequest.EquipmentList.Where(a => a.SelectedDay == nod.SelectedDay);
                rptEquipmentItem.DataBind();
            }
        }
    }

    protected void rptEquipmentItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            RequestEquipment re = e.Item.DataItem as RequestEquipment;
            if (re != null)
            {
                PackageItems p = AllEquipments.Where(a => a.Id == re.ItemID).FirstOrDefault();
                if (p != null)
                {

                    PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblItemName.Text = pdesc.ItemName;
                        lblDescription.Text = pdesc.ItemDescription;
                    }
                    else
                    {
                        lblItemName.Text = p.ItemName;
                        lblDescription.Text = "";
                    }
                    lblQuantity.Text = Convert.ToString(re.Quantity);
                }
            }
        }
    }

    protected void rptOthersDay_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblSelectDay = (Label)e.Item.FindControl("lblSelectDay");
            Repeater rptOthersItem = (Repeater)e.Item.FindControl("rptOthersItem");
            NumberOfDays nod = e.Item.DataItem as NumberOfDays;
            if (nod != null)
            {
                lblSelectDay.Text = nod.SelectedDay.ToString();
                rptOthersItem.DataSource = objRequest.OthersList.Where(a => a.SelectedDay == nod.SelectedDay);
                rptOthersItem.DataBind();
            }
        }
    }

    protected void rptOthersItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            RequestOthers re = e.Item.DataItem as RequestOthers;
            if (re != null)
            {
                PackageItems p = AllOthers.Where(a => a.Id == re.ItemID).FirstOrDefault();
                if (p != null)
                {

                    PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblItemName.Text = pdesc.ItemName;
                        lblDescription.Text = pdesc.ItemDescription;
                    }
                    else
                    {
                        lblItemName.Text = p.ItemName;
                        lblDescription.Text = "";
                    }
                    lblQuantity.Text = Convert.ToString(re.Quantity);
                }
            }
        }
    }
    #endregion


    #region Go for Calculation
    public void Calculate(Createbooking objCreateBook)
    {
        decimal TotalMeetingroomPrice = 0;
        decimal TotalPackagePrice = 0;
        decimal TotalBuildYourPackagePrice = 0;
        decimal TotalEquipmentPrice = 0;
        decimal TotalExtraPrice = 0;
        bool PackageSelected = false;
        VatCalculation = null;
        VatCalculation = new List<VatCollection>();
        if (objCreateBook != null)
        {
            foreach (BookedMR objb in objCreateBook.MeetingroomList)
            {
                foreach (BookedMrConfig objconfig in objb.MrDetails)
                {
                    TotalMeetingroomPrice = objconfig.NoOfParticepant * objconfig.MeetingroomPrice;
                    //Build mr
                    foreach (BuildYourMR bmr in objconfig.BuildManageMRLst)
                    {
                        TotalBuildYourPackagePrice += bmr.ItemPrice * bmr.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == bmr.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = bmr.vatpercent;
                            v.CalculatedPrice = bmr.ItemPrice * bmr.Quantity * bmr.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == bmr.vatpercent).FirstOrDefault().CalculatedPrice += bmr.ItemPrice * bmr.Quantity * bmr.vatpercent / 100;
                        }
                    }
                    PackageSelected = Convert.ToBoolean(objconfig.PackageID);
                    //Equipment
                    foreach (ManageEquipment eqp in objconfig.EquipmentLst)
                    {
                        TotalEquipmentPrice += eqp.ItemPrice * eqp.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = eqp.vatpercent;
                            v.CalculatedPrice = eqp.ItemPrice * eqp.Quantity * eqp.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault().CalculatedPrice += eqp.ItemPrice * eqp.Quantity * eqp.vatpercent / 100;
                        }
                    }
                    //Manage Extras
                    foreach (ManageExtras ext in objconfig.ManageExtrasLst)
                    {
                        TotalExtraPrice += ext.ItemPrice * ext.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == ext.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = ext.vatpercent;
                            v.CalculatedPrice = ext.ItemPrice * ext.Quantity * ext.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == ext.vatpercent).FirstOrDefault().CalculatedPrice += ext.ItemPrice * ext.Quantity * ext.vatpercent / 100;
                        }
                    }
                    //Manage Package Item
                    foreach (ManagePackageItem pck in objconfig.ManagePackageLst)
                    {
                        TotalPackagePrice += pck.ItemPrice * pck.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == pck.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = pck.vatpercent;
                            v.CalculatedPrice = pck.ItemPrice * pck.Quantity * pck.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == pck.vatpercent).FirstOrDefault().CalculatedPrice += pck.ItemPrice * pck.Quantity * pck.vatpercent / 100;
                        }
                    }
                }
            }

        }
    }
    #endregion
}