﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using System.Configuration;
using log4net;
using log4net.Config;
using AjaxControlToolkit;
using System.Xml;

public partial class Agency_BookingStep2 : BasePage
{
    #region Variables and Properties
    public UserControl_Agency_LeftSearchPanel u
    {
        get
        {
            return Page.Master.FindControl("cntLeftSearch").FindControl("LeftSearchPanel1") as UserControl_Agency_LeftSearchPanel;
        }
    }
    Createbooking objBooking = null;
    HotelManager objHotel = new HotelManager();
    Hotel objBookedHotel = new Hotel();
    HotelInfo ObjHotelinfo = new HotelInfo();
    BookingManager bm = new BookingManager();
    CurrencyManager cm = new CurrencyManager();
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1059:MembersShouldNotExposeCertainConcreteTypes", MessageId = "System.Xml.XmlNode")]
    public XmlDocument XMLLanguage
    {
        get { return (XmlDocument)ViewState["Language"]; }
        set { ViewState["Language"] = value; }
    }
    public string HotelCurrency
    {
        get { return Convert.ToString(ViewState["HotelCurrency"]); }
        set { ViewState["HotelCurrency"] = value; }
    }
    public int CurrentDay
    {
        get { return Convert.ToInt32(ViewState["CurrentDay"] == null ? "1" : Convert.ToString(ViewState["CurrentDay"])); }
        set { ViewState["CurrentDay"] = value; }
    }
    public string UserCurrency
    {
        get { return Convert.ToString(ViewState["UserCurrency"]); }
        set { ViewState["UserCurrency"] = value; }
    }
    public string CurrentMeetingroom
    {
        get;
        set;
    }

    public int CurrentPackageType
    {
        get;
        set;
    }
    public int CurrentNumberOfParticepent
    {
        get;
        set;
    }

    public string CurrencySign
    {
        get { return Convert.ToString(ViewState["CurrencySign"]); }
        set { ViewState["CurrencySign"] = value; }
    }
    public Int64 CurrentPackageID
    {
        get;
        set;
    }
    public List<ManagePackageItem> CurrentPackageItem
    {
        get;
        set;
    }
    public List<ManageExtras> CurrentManageExtra
    {
        get;
        set;
    }
    public List<BuildYourMR> CurrentBuilYourMR
    {
        get;
        set;
    }
    public List<ManageEquipment> CurrentManageEquipment
    {
        get;
        set;
    }
    public List<ManageOtherItems> CurrentManageOthers
    {
        get;
        set;
    }
    public decimal CurrentMeetingRoomPrice
    {
        get;
        set;
    }
    public decimal CurrentPackagePrice
    {
        get;
        set;
    }
    public decimal CurrentExtraPrice
    {
        get;
        set;
    }
    public decimal CurrentBuildYourPackagePrice
    {
        get;
        set;
    }
    public decimal CurrentEquipmentPrice
    {
        get;
        set;
    }
    public decimal CurrentOthersPrice
    {
        get;
        set;
    }
    public decimal CurrentAccomodationPrice
    {
        get;
        set;
    }
    public string MyScript
    {
        get { return Convert.ToString(Session["SCRPT"]); }
        set { Session["SCRPT"] = value; }
    }

    public decimal CurrencyConvert
    {
        get { return Convert.ToDecimal(ViewState["CurrencyConvert"]); }
        set { ViewState["CurrencyConvert"] = value; }
    }

    public bool IsSecondMeetingRoom
    {
        get { return Convert.ToBoolean(ViewState["IsSecondMeetingRoom"]); }
        set { ViewState["IsSecondMeetingRoom"] = value; }
    }
    public decimal TotalMeetingRoomPrice
    {
        get;
        set;
    }
    public bool IsBedroomAvailable
    {
        get
        {
            return Session["IsBedroomAvailable"] == null ? false : Convert.ToBoolean(Session["IsBedroomAvailable"]);
        }
        set
        {
            Session["IsBedroomAvailable"] = value;
        }
    }

    public bool IsConvertIntoRequest
    {
        get
        {
            return Session["IsConvertIntoRequest"] == null ? false : Convert.ToBoolean(Session["IsConvertIntoRequest"]);
        }
        set
        {
            Session["IsConvertIntoRequest"] = value;
        }
    }
    #endregion


    #region Page Load
    /// <summary>
    /// Initilize all the components at page load.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentAgencyUserID"] == null)
        {
            Response.Redirect("~/Agency/SearchBookingRequest.aspx");
        }
        u.SearchButton += new EventHandler(u_SearchButton);
        u.MapSearch += new EventHandler(u_MapSearch);
        if (!Page.IsPostBack)
        {
            u.usercontroltype = "Basic";
            Users objUsers = Session["CurrentAgencyUser"] as Users;
            //Bind Login users details
            //lblDateLogin.Text = DateTime.Now.ToString("dd MMM yyyy");
            //lblUserName.Text = objUsers.FirstName + " " + objUsers.LastName;
            fillStaticData();
            //Check Availability of Search.
            CheckSearchAvailable();
            if (pnlMain.Visible == true)
            {
                //Page.RegisterStartupScript("a", "<script language='javascript'>jQuery(document).ready(function(){" + MyScript + "});</script>");
                ltrScript.Text = "<script language='javascript'>jQuery(document).ready(function(){" + MyScript + "});</script>";
            }
        }
        else
        {
            if (pnlMain.Visible == true && CurrentDay==2)
            {
                //Page.RegisterStartupScript("a", "<script language='javascript'>jQuery(document).ready(function(){" + MyScript + "});</script>");
                ltrScript.Text = "<script language='javascript'>jQuery(document).ready(function(){" + MyScript + "});</script>";
            }
        }
    }
    BookingRequest objBookingRequest = new BookingRequest();
    void u_SearchButton(object sender, EventArgs e)
    {
        Session["CurrentUserSeleted"] = Convert.ToString(Session["CurrentAgencyUserID"]);
        ManageSession();
        Session["ComeFromBookingAndRequestPage"] = "Booking";
        Response.Redirect("~/Agency/SearchBookingRequest.aspx");
    }
    void u_MapSearch(object sender, EventArgs e)
    {
        Session["CurrentUserSeleted"] = Convert.ToString(Session["CurrentAgencyUserID"]);
        ManageSession();
        Session["ComeFromBookingAndRequestPage"] = "Booking";
        Response.Redirect("~/Agency/SearchBookingRequest.aspx");
    }
    void ManageSession()
    {
        string WhereClause = string.Empty;
        string WhereClauseday2 = string.Empty;
        if (u.propCountryID != "0")
        {
            if (u.propCountryID != "")
            {
                if (WhereClause.Length > 0)
                {
                    WhereClause += " and ";
                    WhereClauseday2 += " and ";
                }
                WhereClause += HotelColumn.CountryId + "=" + u.propCountryID;
                WhereClauseday2 += HotelColumn.CountryId + "=" + u.propCountryID;
            }
        }

        if (Convert.ToInt32(u.propCity) != 0)
        {
            if (u.propCity != "")
            {
                if (WhereClause.Length > 0)
                {
                    WhereClause += " and ";
                    WhereClauseday2 += " and ";
                }
                WhereClause += HotelColumn.CityId + "=" + u.propCity;
                WhereClauseday2 += HotelColumn.CityId + "=" + u.propCity;
            }
        }
        if (u.propDuration == "1")
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                if (u.propDay1 == "0")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (u.propDay1 == "1")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (u.propDay1 == "2")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
            }
        }
        if (u.propDuration == "2")
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                WhereClauseday2 += " and ";
                if (u.propDay1 == "0")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (u.propDay1 == "1")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (u.propDay1 == "2")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }


                if (u.propDay2 == "0")
                {
                    WhereClauseday2 += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (u.propDay2 == "1")
                {
                    WhereClauseday2 += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (u.propDay2 == "2")
                {
                    WhereClauseday2 += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
            }
        }

        if (u.propDate != "")
        {
            DateTime fromDate = new DateTime(Convert.ToInt32("20" + u.propDate.Split('/')[2]), Convert.ToInt32(u.propDate.Split('/')[1]), Convert.ToInt32(u.propDate.Split('/')[0]));

            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                WhereClauseday2 += " and ";
            }
            WhereClause += AvailabilityColumn.AvailabilityDate + "='" + fromDate + "'" + " and " + " (DATEDIFF(day, GETDATE(),'" + fromDate + "')) >= " + AvailabilityColumn.LeadTimeForMeetingRoom + "";
            WhereClauseday2 += AvailabilityColumn.AvailabilityDate + "='" + fromDate.AddDays(1) + "'" + " and " + " (DATEDIFF(day, GETDATE(),'" + fromDate.AddDays(1) + "')) >= " + AvailabilityColumn.LeadTimeForMeetingRoom + "";
        }


        if (u.propParticipant != "" && u.propParticipant != "0")
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                WhereClauseday2 += " and ";
            }
            WhereClause += u.propParticipant + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + "";
            WhereClauseday2 += u.propParticipant + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + "";
        }

        //Send all search session value
        Session["Where"] = WhereClause;
        if (u.propDuration == "2")
        {
            Session["Where2"] = WhereClauseday2;
        }

        //Maintain session value for all controls 
        objBookingRequest = new BookingRequest();
        objBookingRequest.propCountry = u.propCountryID;
        objBookingRequest.propCity = u.propCity;
        objBookingRequest.propDate = Convert.ToString(string.IsNullOrEmpty(u.propDate) ? (u.propDate == "dd/mm/yy" ? DateTime.Now.ToString("dd/MM/yy") : u.propDate) : u.propDate);
        objBookingRequest.propDuration = u.propDuration;
        objBookingRequest.propDays = u.propDay1;
        objBookingRequest.propDay2 = u.propDay2;
        if (u.propParticipant != "")
        {
            objBookingRequest.propParticipants = u.propParticipant;
        }
        else
        {
            objBookingRequest.propParticipants = "0";
        }
        Session["masterInput"] = objBookingRequest;
        Session["CurrencyID"] = "2";
        //Dictionary<long, string> participants = new Dictionary<long, string>();
        //participants.Add(1, objBookingRequest.propParticipants);
        //Session["participants"] = participants;
    }

    public void fillStaticData()
    {
        txtNoteBedroom.Text = GetKeyResult("NOTE");
        txtSpecialRequest.Text = GetKeyResult("PLEASEWRITE");
        txtNoteBedroom.Text = GetKeyResult("PLEASEWRITE");
    }
    /// <summary>
    /// Check Availability of Serach according To Serach ID.
    /// </summary>
    public void CheckSearchAvailable()
    {
        if (Session["SerachID"] != null)
        {
            SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
            if (st != null)
            {
                if ((st.IsSentAsRequest == null ? false : st.IsSentAsRequest) == true)
                {
                    IsConvertIntoRequest = true;
                    divWarnning.InnerHtml = GetKeyResult("YOURBOOKINGISCONVERTINTOREQUEST");//"Your booking is now converted into request.";
                    divWarnning.Attributes.Add("style", "display:block;");
                }
                else
                {
                    IsConvertIntoRequest = false;
                }
                if (st.CurrentStep == 1)
                {
                    Response.Redirect("~/Agency/BookingStep1.aspx");
                } 
                else if (st.CurrentStep == 2)
                {
                    objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
                    CurrentDay = st.CurrentDay;
                    //Bind Booking Details
                    BindBooking();
                }
                else if (st.CurrentStep == 3)
                {
                    Response.Redirect("~/Agency/BookingStep3.aspx");
                }
                else if (st.CurrentStep == 4)
                {
                    Response.Redirect("~/Agency/BookingStep4.aspx");
                }
                else 
                {
                    Response.Redirect("~/Agency/booking.aspx");
                }
            }
        }
        else
        {
            Response.Redirect("~/Agency/SearchBookingRequest.aspx");
        }
    }
    List<VatCollection> _VatCalculation = new List<VatCollection>();
    public List<VatCollection> VatCalculation
    {
        get
        {
            if (_VatCalculation == null)
            {
                _VatCalculation = new List<VatCollection>();
            }
            return _VatCalculation;
        }
        set
        {
            _VatCalculation = value;
        }
    }

    public void BindBooking()
    {
        try
        {
            MyScript = "";
            Hotel objHotelDetails = new HotelManager().GetHotelDetailsById(objBooking.HotelID);
            objBookedHotel = objHotelDetails;
            #region Currency Conversion
            Currency objUserCurrency = cm.GetCurrencyDetailsByID(Convert.ToInt64(Session["CurrencyID"]));
            UserCurrency = objUserCurrency.Currency;
            CurrencySign = objUserCurrency.CurrencySignature;
            //Currency objCurrency = cm.GetCurrencyDetailsByID(objBookedHotel.CurrencyId);
            Currency objcurrency = cm.GetCurrencyDetailsByID(objHotelDetails.CurrencyId);
            HotelCurrency = objcurrency.Currency;
            lblCurrencySymbol.Text = objcurrency.CurrencySignature;
            string currency = CurrencyManager.Currency(HotelCurrency, UserCurrency);
            //CurrencyConvert = Convert.ToDecimal(currency == "N/A" ? "1" : currency);
            CurrencyConvert = Math.Round(Convert.ToDecimal(currency == "N/A" || currency == "</HTML>" ? "1" : currency), 2);
            #endregion
            lblArrival.Text = objBooking.ArivalDate.ToString("dd MMM yyyy");
            lblDeparture.Text = objBooking.DepartureDate.ToString("dd MMM yyyy");
            lblDuration.Text = objBooking.Duration == 1 ? objBooking.Duration + GetKeyResult("DAY") : objBooking.Duration + GetKeyResult("DAY");
            lblCurrentDay.Text = CurrentDay.ToString();
            if (objBooking.MeetingroomList.Count > 1)
            {
                if (objBooking.MeetingroomList[CurrentDay - 1].MrDetails.Count > 1)
                {
                    lblNumOfParticipants.Text = objBooking.MeetingroomList[CurrentDay - 1].MrDetails[CurrentDay - 1].NoOfParticepant.ToString();
                }
                else
                {
                    lblNumOfParticipants.Text = objBooking.MeetingroomList[0].MrDetails[CurrentDay - 1].NoOfParticepant.ToString();
                }
            }
            else
            {
                lblNumOfParticipants.Text = objBooking.MeetingroomList[0].MrDetails[CurrentDay - 1].NoOfParticepant.ToString();
            }
            if (CurrentDay == 1)
            {
                rptMeetingroom.DataSource = objBooking.MeetingroomList;
                if (objBooking.MeetingroomList.Count > 1)
                {
                    IsSecondMeetingRoom = true;
                }
                else
                {
                    IsSecondMeetingRoom = false;
                }
            }
            else
            {
                rptMeetingroom.DataSource = objBooking.MeetingroomList.Where(a => a.MrDetails.Count > 1);
                if (objBooking.MeetingroomList.Where(a => a.MrDetails.Count > 1).Count() > 1)
                {
                    IsSecondMeetingRoom = true;
                }
                else
                {
                    IsSecondMeetingRoom = false;
                }
            }
            rptMeetingroom.DataBind();
            if (!string.IsNullOrEmpty(objBooking.SpecialRequest))
            {
                txtSpecialRequest.Text = objBooking.SpecialRequest;
            }
            if (objBooking.Duration == CurrentDay)
            {
                pnlAccomodation.Visible = true;
                BindAccomodation(objBooking.ManageAccomodationLst);
            }
            else
            {
                pnlAccomodation.Visible = false;
                IsBedroomAvailable = false;
            }
            #region Days Settings
            if (CurrentDay == 1)
            {
                pnlDay2.Visible = false;
                pnlMain.Visible = true;
            }
            else
            {
                pnlDay2.Visible = true;
                pnlMain.Visible = false;
                CurrentDay = 2;
                if (objBooking.MeetingroomList.FirstOrDefault().MrDetails.Where(a => a.SelectedDay == 1).FirstOrDefault().SelectedTime != objBooking.MeetingroomList.FirstOrDefault().MrDetails.Where(a => a.SelectedDay == 2).FirstOrDefault().SelectedTime)
                {
                    pnlDay2.Attributes.Add("style", "display:none");
                    pnlMain.Visible = true;
                    rbtnSameAsDay1.Checked = false;
                    rbtnDiffFromDay1.Checked = true;
                    objBooking.SecondMRNotSameAsRoom1 = false;
                }
                else
                {
                    if (!objBooking.SecondMRNotSameAsRoom1)
                    {
                        rbtnSameAsDay1.Checked = true;
                        rbtnDiffFromDay1.Checked = false;
                    }
                    else
                    {
                        rbtnSameAsDay1.Checked = false;
                        rbtnDiffFromDay1.Checked = true;
                        pnlMain.Visible = true;
                    }
                }
            }
            #endregion
            Calculate(objBooking);
            lblUserCurrencyName.Text = objUserCurrency.Currency;
            lblHotelCurrencyName.Text = objcurrency.Currency;
            lblCurrencySymbol.Text = objcurrency.CurrencySignature;
            lblFinalTotalInHotelCurrency.Text = Math.Round(objBooking.TotalBookingPrice, 2).ToString("#,##,##0.00");

            if (CurrencyConvert == Convert.ToDecimal(1))
            {
                divCurrencyNote.Visible = false;
            }
            else
            {
                divCurrencyNote.Visible = true;
            }
            lblNetTotal.Text = Math.Round((objBooking.TotalBookingPrice - VatCalculation.Sum(a => a.CalculatedPrice)) * CurrencyConvert, 2).ToString();
            //lblFinalTotalInHotelCurrency.Text = objcurrency.CurrencySignature + " " + Math.Round(objBooking.TotalBookingPrice, 2).ToString("#,##,##0.00"); 
            rptVatList.DataSource = VatCalculation.OrderBy(a => a.VATPercent);
            rptVatList.DataBind();
            lblFinalTotal.Text = Math.Round(objBooking.TotalBookingPrice * CurrencyConvert, 2).ToString("#,##,##0.00");

            SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
            st.SearchObject = TrailManager.XmlSerialize(objBooking);
            bm.SaveSearch(st);
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            Response.Redirect("~/Agency/SearchBookingRequest.aspx");
        }
    }
    public string CheckBedroomAvailability
    {
        get
        {
            return Convert.ToString(Session["CheckBedroomAvailability"] == null ? "" : Session["CheckBedroomAvailability"]);
        }
        set
        {
            Session["CheckBedroomAvailability"] = value;
        }
    }
    public void BindAccomodation(List<Accomodation> lstAccomodation)
    {
        //rbtnDiffCheckinCheckout.Checked = objBooking.AccomodationDiffCheckinChecoutTime;
        //rbtnSameCheckinCheckout.Checked = !objBooking.AccomodationDiffCheckinChecoutTime;
        //rptAccomodation.DataSource = lstAccomodation;
        //rptAccomodation.DataBind();
        //lblBedroomTotalPrice.Text = Math.Round((objBooking.AccomodationPriceTotal *Math.Round(CurrencyConvert,2) * CurrentDay),2).ToString();
        //if (lstAccomodation == null)
        //{
        //    //Hide Accomodation Section.
        //    pnlAcomo.Visible = false;
        //}
        //else
        //{
        //    pnlAcomo.Visible = true;
        //}
        if (lstAccomodation.Count > 0)
        {
            rbtnNoAccomodation.Checked = objBooking.NoAccomodation;
            rbtnBuildYourAccomodation.Checked = !objBooking.NoAccomodation;
            if (objBooking.NoAccomodation)
            {
                divAcc.Style.Add("display", "none");
                IsBedroomAvailable = false;
            }
            else
            {
                divAcc.Style.Add("display", "block");
                IsBedroomAvailable = true;
            }
            List<BedroomManage> distinctNames = (from d in objBooking.ManageAccomodationLst select new BedroomManage { BedroomID = d.BedroomId, bedroomType = d.BedroomType }).ToList();
            List<BedroomManage> disListNew = distinctNames.Distinct(new DistinctItemComparer()).ToList();
            lblTotalAccomodation.Text = string.Format("{0:#,##,##0.00}", Math.Round(objBooking.AccomodationPriceTotal * CurrencyConvert, 2));
            rptAccomodationDaysManager.DataSource = disListNew;
            rptAccomodationDaysManager.DataBind();
            if (disListNew.Count == 1)
            {
                CheckBedroomAvailability = "CheckAvailabilityRoom(1)";
            }
            else if (disListNew.Count == 2)
            {
                CheckBedroomAvailability = "CheckAvailabilityRoom(3)";
            }
            else
            {
                CheckBedroomAvailability = "";
            }
            txtNoteBedroom.Text = objBooking.BedroomNote;
            btnCheckAvail.Attributes.Add("onclick", CheckBedroomAvailability);
            pnlAcomo.Visible = true;
            IsBedroomAvailable = true;
        }
        else
        {
            IsBedroomAvailable = false;
            //pnlAcomo.Visible = false;
        }

    }
    #endregion

    #region Events
    protected void rptVatList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblVatPrice = (Label)e.Item.FindControl("lblVatPrice");
            VatCollection v = e.Item.DataItem as VatCollection;
            if (v != null)
            {
                lblVatPrice.Text = Math.Round(v.CalculatedPrice * CurrencyConvert, 2).ToString("#,##,##0.00");
            }
        }
    }

    #region New Accomodation
    protected void rptAccomodationDaysManager_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Header)
        {
            Repeater rptDays2 = (Repeater)e.Item.FindControl("rptDays2");
            Literal ltrExtendDays = (Literal)e.Item.FindControl("ltrExtendDays");
            long bedid = (from d in objBooking.ManageAccomodationLst select d.BedroomId).FirstOrDefault();
            List<Accomodation> distinctNames = (from d in objBooking.ManageAccomodationLst where d.BedroomId == bedid select d).ToList();
            rptDays2.DataSource = distinctNames;
            rptDays2.DataBind();
            if (distinctNames.Count < 5)
            {
                ltrExtendDays.Text = "<td colspan='" + (5 - distinctNames.Count) + "' style='border-top: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;padding: 5px;' >&nbsp;</td>";
            }
        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblBedroomType = (Label)e.Item.FindControl("lblBedroomType");
            HiddenField hdnBedroomId = (HiddenField)e.Item.FindControl("hdnBedroomId");
            Repeater rptPrice = (Repeater)e.Item.FindControl("rptPrice");
            Repeater rptNoOfDays = (Repeater)e.Item.FindControl("rptNoOfDays");
            Repeater rptSingleQuantity = (Repeater)e.Item.FindControl("rptSingleQuantity");
            Repeater rptDoubleQuantity = (Repeater)e.Item.FindControl("rptDoubleQuantity");
            Literal ltrExtendPrice = (Literal)e.Item.FindControl("ltrExtendPrice");
            Literal ltrExtendNoDays = (Literal)e.Item.FindControl("ltrExtendNoDays");
            Literal ltrExtendSQuantity = (Literal)e.Item.FindControl("ltrExtendSQuantity");
            Literal ltrExtendDQuantity = (Literal)e.Item.FindControl("ltrExtendDQuantity");
            Label lblTotalAccomodation = (Label)e.Item.FindControl("lblTotalAccomodation");
            Image imgBedroom = (Image)e.Item.FindControl("imgBedroom");
            BedroomManage bedm = e.Item.DataItem as BedroomManage;
            lblBedroomType.Text = Enum.GetName(typeof(BedRoomType), bedm.bedroomType);
            hdnBedroomId.Value = bedm.BedroomID.ToString();

            #region Add by manas for CR41
            HyperLink hypPhoto = (HyperLink)e.Item.FindControl("hypPhoto");
            hypPhoto.NavigateUrl = SiteRootPath + "/SlideShowPopup.aspx?Bid=" + hdnBedroomId.Value;
            hypPhoto.Attributes.Add("onclick", "return Navigate2(" + hdnBedroomId.Value + ");");

            LinkButton lnkDescription = (LinkButton)e.Item.FindControl("hypDescription");
            BedRoom objBedroom = objHotel.GetBedRoomDetailsByBedroomID(Convert.ToInt32(hdnBedroomId.Value));
            imgBedroom.ImageUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "/BedroomImage/" + objBedroom.Picture;
            BedRoomDesc objDesc = objBedroom.BedRoomDescCollection.Where(a => a.LanguageId == Convert.ToInt32(Session["LanguageID"])).FirstOrDefault();

            if (objDesc != null)
            {
                lnkDescription.Attributes.Add("alt", objDesc.Description == null ? "" : objDesc.Description);
            }
            else
            {
                int englishID = Convert.ToInt32(ObjHotelinfo.GetAllRequiredLanguage().Find(a => a.Name == "English").Id);
                BedRoomDesc objDescEng = objBedroom.BedRoomDescCollection.Where(a => a.LanguageId == englishID).FirstOrDefault();
                lnkDescription.Attributes.Add("alt", objDescEng.Description == null ? "" : objDescEng.Description);
            }
            #endregion


            List<Accomodation> lstaccomo = (from d in objBooking.ManageAccomodationLst where d.BedroomId == bedm.BedroomID select d).ToList();
            rptPrice.DataSource = lstaccomo;
            rptPrice.DataBind();
            rptNoOfDays.DataSource = lstaccomo;
            rptNoOfDays.DataBind();
            rptSingleQuantity.DataSource = lstaccomo;
            rptSingleQuantity.DataBind();
            rptDoubleQuantity.DataSource = lstaccomo;
            rptDoubleQuantity.DataBind();
            if (lstaccomo.Count < 5)
            {
                ltrExtendNoDays.Text = "<td colspan='" + (5 - lstaccomo.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
                ltrExtendPrice.Text = "<td colspan='" + (5 - lstaccomo.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
                ltrExtendSQuantity.Text = "<td colspan='" + (5 - lstaccomo.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
                ltrExtendDQuantity.Text = "<td colspan='" + (5 - lstaccomo.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
            }
            lblTotalAccomodation.Text = String.Format("{0:#,##,#0.00}", Math.Round((lstaccomo.Sum(a => a.QuantityDouble > 0 ? a.QuantityDouble * a.RoomPriceDouble * CurrencyConvert : 0) + lstaccomo.Sum(a => a.QuantitySingle > 0 ? a.QuantitySingle * a.RoomPriceSingle * CurrencyConvert : 0)), 2));
        }
    }

    protected void rptDays2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblDay = (Label)e.Item.FindControl("lblDay");
            Accomodation a = e.Item.DataItem as Accomodation;
            lblDay.Text = a.DateRequest.ToString("dd/MM/yyyy");
        }
    }

    protected void rptPrice_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPriceDay = (Label)e.Item.FindControl("lblPriceDay");
            Accomodation a = e.Item.DataItem as Accomodation;
            lblPriceDay.Text = Math.Round(a.RoomPriceSingle * CurrencyConvert, 2).ToString("#,##,##0.00") + "</BR>" + "<span class='currencyClass'></span>&nbsp;" + Math.Round(a.RoomPriceDouble * CurrencyConvert, 2).ToString("#,##,##0.00");
        }
    }

    protected void rptNoOfDays_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblAvailableRoomDay = (Label)e.Item.FindControl("lblAvailableRoomDay");
            Accomodation a = e.Item.DataItem as Accomodation;
            lblAvailableRoomDay.Text = a.RoomAvailable.ToString();
        }
    }

    protected void rptSingleQuantity_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            TextBox txtQuantitySDay = (TextBox)e.Item.FindControl("txtQuantitySDay");
            HiddenField hdnDate = (HiddenField)e.Item.FindControl("hdnDate");
            HiddenField hdnPrice = (HiddenField)e.Item.FindControl("hdnPrice");
            Accomodation a = e.Item.DataItem as Accomodation;
            if (a != null)
            {
                if (a.RoomPriceSingle == 0)
                {
                    txtQuantitySDay.Enabled = false;
                    txtQuantitySDay.Text = "NA";
                }
                else
                {
                    txtQuantitySDay.Text = a.QuantitySingle.ToString();
                }
                txtQuantitySDay.Attributes.Add("roomtype", Enum.GetName(typeof(BedRoomType), a.BedroomType));
                txtQuantitySDay.Attributes.Add("maxvalue", a.RoomAvailable.ToString());
                txtQuantitySDay.Attributes.Add("dateavailable", a.DateRequest.ToString("dd/MM/yyyy"));
                txtQuantitySDay.Attributes.Add("price", Math.Round(a.RoomPriceSingle * CurrencyConvert, 2).ToString());
                hdnDate.Value = a.DateRequest.ToString();
                hdnPrice.Value = a.RoomPriceSingle.ToString();
            }
        }
    }

    protected void rptDoubleQuantity_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            TextBox txtQuantityDDay = (TextBox)e.Item.FindControl("txtQuantityDDay");
            HiddenField hdnDate = (HiddenField)e.Item.FindControl("hdnDate");
            HiddenField hdnPrice = (HiddenField)e.Item.FindControl("hdnPrice");
            Accomodation a = e.Item.DataItem as Accomodation;
            if (a != null)
            {
                if (a.RoomPriceDouble == 0)
                {
                    txtQuantityDDay.Enabled = false;
                    txtQuantityDDay.Text = "NA";
                }
                else
                {
                    txtQuantityDDay.Text = a.QuantityDouble.ToString();
                }
                txtQuantityDDay.Attributes.Add("roomtype", Enum.GetName(typeof(BedRoomType), a.BedroomType));
                txtQuantityDDay.Attributes.Add("maxvalue", a.RoomAvailable.ToString());
                txtQuantityDDay.Attributes.Add("dateavailable", a.DateRequest.ToString("dd/MM/yyyy"));
                txtQuantityDDay.Attributes.Add("price", Math.Round(a.RoomPriceDouble * CurrencyConvert, 2).ToString());
                hdnDate.Value = a.DateRequest.ToString();
                hdnPrice.Value = a.RoomPriceDouble.ToString();
            }
        }
    }
    #endregion

    /// <summary>
    /// Bind meetingroom details 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rptMeetingroom_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //Create object of BookedMR
            BookedMR objBooked = e.Item.DataItem as BookedMR;
            if (objBooked != null)
            {
                #region Controls
                HiddenField hdnMeetingRoomID = (HiddenField)e.Item.FindControl("hdnMeetingRoomID");
                Label lblMeetingroom = (Label)e.Item.FindControl("lblMeetingroom");
                Image imgMr = (Image)e.Item.FindControl("imgMr");
                Label lblMeetingRoomType = (Label)e.Item.FindControl("lblMeetingRoomType");
                Label lblMinMaxCapacity = (Label)e.Item.FindControl("lblMinMaxCapacity");
                HyperLink hypSeePlan = (HyperLink)e.Item.FindControl("hypSeePlan");
                Repeater rptDays = (Repeater)e.Item.FindControl("rptDays");
                Repeater reppackage = (Repeater)e.Item.FindControl("reppackage");
                Repeater rptExtra = (Repeater)e.Item.FindControl("rptExtra");
                Repeater rptFoodAndBravrage = (Repeater)e.Item.FindControl("rptFoodAndBravrage");
                Repeater rptOthers = (Repeater)e.Item.FindControl("rptOthers");
                Repeater repEquipment = (Repeater)e.Item.FindControl("repEquipment");
                CheckBox chkIsExtra = (CheckBox)e.Item.FindControl("chkIsExtra");
                Label lblMeetingroomName = (Label)e.Item.FindControl("lblMeetingroomName");
                Label lblMeetingRoomPrice = (Label)e.Item.FindControl("lblMeetingRoomPrice");
                Label lblMeetingroomActualPrice = (Label)e.Item.FindControl("lblMeetingroomActualPrice");
                Label lblMeetingroomDiscountedPrice = (Label)e.Item.FindControl("lblMeetingroomDiscountedPrice");
                Label lblTotalExtra = (Label)e.Item.FindControl("lblTotalExtra");
                Label lblBuildYourPackagePrice = (Label)e.Item.FindControl("lblBuildYourPackagePrice");
                Label lblEquipmentPrice = (Label)e.Item.FindControl("lblEquipmentPrice");
                Label lblOthersPriceTotal = (Label)e.Item.FindControl("lblOthersPriceTotal");
                RadioButton ChoicePackage = (RadioButton)e.Item.FindControl("ChoicePackage");
                RadioButton BuildPackage = (RadioButton)e.Item.FindControl("BuildPackage");
                RadioButton rbtnNeedToConfigure = (RadioButton)e.Item.FindControl("rbtnNeedToConfigure");
                RadioButton rbtnSameAsAbove = (RadioButton)e.Item.FindControl("rbtnSameAsAbove");
                Label lblQuantityMR = (Label)e.Item.FindControl("lblQuantityMR");
                HtmlControl divOthers = (HtmlControl)e.Item.FindControl("divOthers");
                HtmlControl equipments = (HtmlControl)e.Item.FindControl("equipments");
                #endregion
                //Get details of Meeting room according to the meeting room Id with Deep Load
                MeetingRoom objMeetingroom = objHotel.GetMeetingRoomDetailsById(objBooked.MRId);
                MeetingRoomDesc objDesc = objMeetingroom.MeetingRoomDescCollection.Where(a => a.LanguageId == Convert.ToInt32(Session["LanguageID"])).FirstOrDefault();
                MeetingRoomConfig objMrConfig = objMeetingroom.MeetingRoomConfigCollection.Where(a => a.Id == objBooked.MrConfigId).FirstOrDefault();
                CurrentPackageType = objBooked.MrDetails[CurrentDay - 1].SelectedDay;
                CurrentMeetingroom = objBooked.MRId.ToString();
                decimal intDiscountMR = 0;
                if (objMeetingroom != null)
                {
                    //Fill All meeting room details.
                    hdnMeetingRoomID.Value = objMeetingroom.Id.ToString();
                    lblMeetingroom.Text = objMeetingroom.Name;
                    lblMeetingroomName.Text = objMeetingroom.Name;
                    if (objMrConfig != null)
                    {
                        //Bind the child repeater with the datails of days
                        lblMeetingRoomType.Text = (objMrConfig.RoomShapeId == (int)RoomShape.Boardroom ? RoomShape.Boardroom.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Classroom ? RoomShape.Classroom.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Cocktail ? RoomShape.Cocktail.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.School ? RoomShape.School.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Theatre ? RoomShape.Theatre.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.UShape ? RoomShape.UShape.ToString() : RoomShape.Boardroom.ToString());
                        lblMinMaxCapacity.Text = objMrConfig.MinCapacity + " - " + objMrConfig.MaxCapicity;
                        List<BookedMrConfig> objBookedDays = objBooked.MrDetails.Where(a => a.SelectedDay == CurrentDay).ToList();
                        rptDays.DataSource = objBookedDays;
                        rptDays.DataBind();
                    }

                    lblMeetingroomActualPrice.Text = "<span class='currencyClass'></span>" + Math.Round(Convert.ToDecimal(CurrentDayTime == 0 ? objMeetingroom.FulldayPrice : objMeetingroom.HalfdayPrice) * CurrencyConvert, 2).ToString("#,##,##0.00");
                    intDiscountMR = objHotel.GetMeetingRoomDiscountByMeetingroomIDandDate(objBooking.HotelID, objBooking.ArivalDate.AddDays(CurrentDay - 1));
                    PackagePricingManager objPackagePriceManager = new PackagePricingManager();

                    if (intDiscountMR < 0)
                    {
                        lblMeetingroomActualPrice.Visible = true;
                    }
                    else
                    {
                        lblMeetingroomActualPrice.Visible = false;
                    }
                    objBooking.MeetingroomList.Where(a => a.MRId == objMeetingroom.Id).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().MeetingroomDiscount = intDiscountMR;
                    objBooking.MeetingroomList.Where(a => a.MRId == objMeetingroom.Id).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().MeetingroomVAT = Convert.ToDecimal(objPackagePriceManager.GetRoomRentalVatByCountry(objBookedHotel.CountryId));

                    lblMeetingroomDiscountedPrice.Text = Math.Round(Convert.ToDecimal((CurrentDayTime == 0 ? objMeetingroom.FulldayPrice : objMeetingroom.HalfdayPrice) + ((CurrentDayTime == 0 ? objMeetingroom.FulldayPrice : objMeetingroom.HalfdayPrice) * intDiscountMR / 100)) * CurrencyConvert, 2).ToString("#,##,##0.00");

                    lblMeetingRoomPrice.Text = Math.Round(Convert.ToDecimal((CurrentDayTime == 0 ? objMeetingroom.FulldayPrice : objMeetingroom.HalfdayPrice) + ((CurrentDayTime == 0 ? objMeetingroom.FulldayPrice : objMeetingroom.HalfdayPrice) * intDiscountMR / 100)) * CurrencyConvert, 2).ToString("#,##,##0.00");

                    objBooking.MeetingroomList.Where(a => a.MRId == objBooked.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().MeetingroomPrice = Convert.ToDecimal(string.IsNullOrEmpty(lblMeetingroomDiscountedPrice.Text.Replace(",", "")) ? "0" : lblMeetingroomDiscountedPrice.Text.Replace(",", "")) / CurrencyConvert;

                    //imgMr.ImageUrl = (string.IsNullOrEmpty(objMeetingroom.Picture) ? ConfigurationManager.AppSettings["FilePath"] + "/HotelImage/default_hotel_logo.png" : ConfigurationManager.AppSettings["FilePath"] + "/HotelImage/" + objMeetingroom.Picture);
                    imgMr.ImageUrl = (string.IsNullOrEmpty(objMeetingroom.Picture) ? ConfigurationManager.AppSettings["FilePath"] + "HotelImage/default_hotel_logo.png" : ConfigurationManager.AppSettings["FilePath"] + "MeetingRoomImage/" + objMeetingroom.Picture);


                    if (objMeetingroom.MrPlan != "")
                    {
                        hypSeePlan.NavigateUrl = ConfigurationManager.AppSettings["FilePath"].ToString() + "Plan/" + objMeetingroom.MrPlan;
                        hypSeePlan.ToolTip = objMeetingroom.MrPlan;
                        hypSeePlan.Target = "_blank";
                        hypSeePlan.Text = GetKeyResult("SEEPLAN");
                    }
                    else
                    {
                        hypSeePlan.Visible = false;
                        hypSeePlan.Text = GetKeyResult("SEEPLAN");
                    }
                }
                //Connect To Packages Details By Hotel Id and Current package Details
                CurrentNumberOfParticepent = objBooked.MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().NoOfParticepant;
                CurrentPackageID = objBooked.MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().PackageID;

                if (rbtnSameAsAbove != null)
                {
                    Label lblBreakdownActualPrice = (Label)e.Item.FindControl("lblBreakdownActualPrice");
                    Label lblBreakdownDiscount = (Label)e.Item.FindControl("lblBreakdownDiscount");
                    Label lblBreakdownDiscountPrice = (Label)e.Item.FindControl("lblBreakdownDiscountPrice");
                    HtmlControl openclosediv = (HtmlControl)e.Item.FindControl("openclosediv");
                    HtmlControl isBreakdowndiv = (HtmlControl)e.Item.FindControl("isBreakdowndiv");
                    lblBreakdownDiscountPrice.Text = lblMeetingroomActualPrice.Text;
                    if (intDiscountMR < 0)
                    {
                        lblBreakdownDiscount.Text = Math.Abs(intDiscountMR).ToString();
                    }
                    else
                    {
                        lblBreakdownDiscount.Text = "0";
                    }
                    lblBreakdownActualPrice.Text = lblMeetingroomDiscountedPrice.Text;
                    rbtnSameAsAbove.Checked = objBooked.MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().IsBreakdown;
                    rbtnNeedToConfigure.Checked = !objBooked.MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().IsBreakdown;
                    if (rbtnSameAsAbove.Checked)
                    {
                        openclosediv.Style.Add("display", "none");
                        isBreakdowndiv.Style.Add("display", "block");
                    }
                    else
                    {
                        openclosediv.Style.Add("display", "block");
                        isBreakdowndiv.Style.Add("display", "none");
                    }
                    hdnMeetingroom2Price.Value = lblMeetingroomDiscountedPrice.Text;
                }
                else
                {
                    hdnMeetingroom1Price.Value = lblMeetingroomDiscountedPrice.Text;
                }
                Panel pnlChoice1 = (Panel)e.Item.FindControl("pnlChoice1");
                if (CurrentNumberOfParticepent >= 10)
                {
                    pnlChoice1.Visible = true;
                    BindPackages(ref reppackage, objBooking.HotelID, objBooked.MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().ManagePackageLst, objBooked.MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().PackageID);
                    if (CurrentPackageID != 0)
                    {
                        ChoicePackage.Checked = true;
                        BuildPackage.Checked = false;
                        MyScript += "var ispack" + hdnMeetingRoomID.Value + "=document.getElementById('" + ChoicePackage.ClientID + "');CreateDivlayer(ispack" + hdnMeetingRoomID.Value + ",'choice2');";
                    }
                    else
                    {
                        ChoicePackage.Checked = false;
                        BuildPackage.Checked = true;
                        MyScript += "var ispack" + hdnMeetingRoomID.Value + "=document.getElementById('" + BuildPackage.ClientID + "');CreateDivlayer(ispack" + hdnMeetingRoomID.Value + ",'choice1');";
                    }
                    //chkIsExtra.Checked = true;
                    if (objBooked.MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().ManageExtrasLst.Count != 0)
                    {
                        chkIsExtra.Checked = true;
                        MyScript += "var isext" + hdnMeetingRoomID.Value + "=document.getElementById('" + chkIsExtra.ClientID + "');ShowHideIsExtraDivs(isext" + hdnMeetingRoomID.Value + ");";
                    }
                    else
                    {
                        //chkIsExtra.Checked = true;
                        //MyScript += "var isext" + hdnMeetingRoomID.Value + "=document.getElementById('" + chkIsExtra.ClientID + "');ShowHideIsExtraDivs(isext" + hdnMeetingRoomID.Value + ");";
                    }
                    Panel pnlNoExtra = (Panel)e.Item.FindControl("pnlNoExtra");
                    BindIsExtra(ref rptExtra, objBooking.HotelID, objBooked.MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().ManageExtrasLst, pnlNoExtra);
                }
                else
                {
                    ChoicePackage.Checked = false;
                    BuildPackage.Checked = true;
                    pnlChoice1.Visible = false;
                    MyScript += "CreateDivlayer('" + BuildPackage.ClientID + "','choice1');";
                }
                lblTotalExtra.Text = Math.Round(objBooked.MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().ExtraPriceTotal * CurrencyConvert).ToString("#,##,##0.00");

                BindFoodAndBravrages(ref rptFoodAndBravrage, objBooking.HotelID, objBooked.MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().BuildManageMRLst);

                if (Math.Round(objBooked.MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().BuildPackagePriceTotal, 2) != 0 && objBooked.MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().PackageID == 0)
                {
                    lblBuildYourPackagePrice.Text = Math.Round(objBooked.MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().BuildPackagePriceTotal * CurrencyConvert, 2).ToString("#,##,##0.00");
                }
                else
                {
                    lblBuildYourPackagePrice.Text = Math.Round(Convert.ToDecimal(string.IsNullOrEmpty(lblMeetingroomDiscountedPrice.Text) ? "0" : lblMeetingroomDiscountedPrice.Text.Replace(",", "")), 2).ToString("#,##,##0.00");
                }

                BindEquipment(ref repEquipment, objBooking.HotelID, objBooked.MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().EquipmentLst);
                if (repEquipment.Items.Count <= 0)
                {
                    equipments.Attributes.Add("style", "display:none;");
                }
                lblEquipmentPrice.Text = Math.Round(objBooked.MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().EquipmentPriceTotal * CurrencyConvert, 2).ToString("#,##,##0.00");

                BindOtherItems(ref rptOthers, objBooking.HotelID, objBooked.MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().BuildOthers);
                if (rptOthers.Items.Count <= 0)
                {
                    divOthers.Attributes.Add("style", "display:none;");
                }
                lblOthersPriceTotal.Text = Math.Round(objBooked.MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().OtherPriceTotal * CurrencyConvert, 2).ToString("#,##,##0.00");
            }
        }
    }

    public string CurrentFromTime
    {
        get;
        set;
    }
    public string CurrentToTime
    {
        get;
        set;
    }
    /// <summary>
    /// Item Data bound for days on the bases of CurrentDay
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rptDays_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //Get the details of the Booking Configure object.
            BookedMrConfig objBookedMrConfig = e.Item.DataItem as BookedMrConfig;
            //Get the controls of the days repeater
            Label lblCurrentDay = (Label)e.Item.FindControl("lblCurrentDay");
            Label lblStartTime = (Label)e.Item.FindControl("lblStartTime");
            Label lblEndTime = (Label)e.Item.FindControl("lblEndTime");
            TextBox txtNumberOfParticepant = (TextBox)e.Item.FindControl("txtNumberOfParticepant");
            if (objBookedMrConfig != null)
            {
                lblCurrentDay.Text = CurrentDay.ToString();
                lblStartTime.Text = objBookedMrConfig.FromTime;
                CurrentFromTime = objBookedMrConfig.FromTime;
                lblEndTime.Text = objBookedMrConfig.ToTime;
                CurrentToTime = objBookedMrConfig.ToTime;
                txtNumberOfParticepant.Text = objBookedMrConfig.NoOfParticepant.ToString();
                CurrentDayTime = objBookedMrConfig.SelectedTime;
            }
        }
    }
    public Int64 MyCurrentPackageID
    {
        get;
        set;
    }
    public decimal MyItemPrice
    {
        get;
        set;
    }
    public int CurrentDayTime
    {
        get;
        set;
    }
    /// <summary>
    /// Rereater package items data bound
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void reppackage_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPackageName = (Label)e.Item.FindControl("lblPackageName");
            Label lblPackageDesc = (Label)e.Item.FindControl("lblPackageDesc");
            Label lblPackagePrice = (Label)e.Item.FindControl("lblPackagePrice");
            Label lblTotalPackagePrice = (Label)e.Item.FindControl("lblTotalPackagePrice");
            HiddenField hdnPackageID = (HiddenField)e.Item.FindControl("hdnPackageID");

            RadioButton rbtnPackage = (RadioButton)e.Item.FindControl("rbtnPackage");
            PackageByHotel p = e.Item.DataItem as PackageByHotel;
            Repeater rptPackageItem = (Repeater)e.Item.FindControl("rptPackageItem");
            if (p != null)
            {

                hdnPackageID.Value = p.PackageId.ToString();
                MyCurrentPackageID = Convert.ToInt64(p.PackageId);
                PackageMaster pmas = objHotel.getPackageById(Convert.ToInt64(p.PackageId.Value));
                objHotel.GetActualPriceByHotelPackageID(p.PackageIdSource);
                lblPackageName.Text = pmas.PackageName;
                decimal intDiscountPK = 0;
                decimal ActualPrice = 0;
                MyItemPrice = 0;
                rptPackageItem.DataSource = objHotel.GetPackageItemDetailsByPackageID(Convert.ToInt64(p.PackageId));
                rptPackageItem.DataBind();
                if (p.PackageIdSource.ActualPackagePriceCollection.Count > 0)
                {
                    intDiscountPK = objHotel.GetPackageDiscountByPackageIDandDate(p.HotelId, objBooking.ArivalDate.AddDays(CurrentDay - 1));
                    ActualPrice = Convert.ToDecimal(CurrentDayTime == 0 ? p.PackageIdSource.ActualPackagePriceCollection.Where(a => a.HotelId == objBooking.HotelID).FirstOrDefault().ActualFullDayPrice : p.PackageIdSource.ActualPackagePriceCollection.Where(a => a.HotelId == objBooking.HotelID).FirstOrDefault().ActualHalfDayPrice);
                    objHotel.GetDescriptionofPackageByPackage(p.PackageIdSource);
                    PackageMasterDescription des = p.PackageIdSource.PackageMasterDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64(Session["LanguageID"])).FirstOrDefault();
                    if (des == null)
                    {
                        des = p.PackageIdSource.PackageMasterDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64("1")).FirstOrDefault();
                    }
                    if (p.PackageIdSource.PackageName.ToLower() == "standard")
                    {
                        lblPackagePrice.Text = Math.Round((ActualPrice + ((ActualPrice - MyItemPrice) * intDiscountPK / 100)) * CurrencyConvert, 2).ToString("#,##,##0.00");
                        lblPackagePrice.Attributes.Add("actualprice", (ActualPrice + ((ActualPrice - MyItemPrice) * intDiscountPK / 100)).ToString("0.00"));
                        lblTotalPackagePrice.Text = Math.Round(Convert.ToDecimal((ActualPrice + ((ActualPrice - MyItemPrice) * intDiscountPK / 100))) * CurrentNumberOfParticepent * CurrencyConvert, 2).ToString("#,##,##0.00");
                        //lblPackageDesc.Text = GetKeyResult("STANDARDDESCRIPTION");
                    }
                    else
                    {
                        //if (p.PackageIdSource.PackageName.ToLower() == "favourite")
                        //{
                        //    lblPackageDesc.Text = GetKeyResult("FAVOURITEDESCRIPTION");
                        //}
                        //else if (p.PackageIdSource.PackageName.ToLower() == "elegant")
                        //{
                        //    lblPackageDesc.Text = GetKeyResult("ELEGANTDESCRIPTION");
                        //}
                        //else
                        //{

                        //}

                        lblPackagePrice.Text = Math.Round((ActualPrice) * CurrencyConvert, 2).ToString("#,##,##0.00");
                        lblPackagePrice.Attributes.Add("actualprice", (ActualPrice).ToString("0.00"));
                        lblTotalPackagePrice.Text = Math.Round(Convert.ToDecimal(ActualPrice) * CurrentNumberOfParticepent * CurrencyConvert, 2).ToString("#,##,##0.00");
                    }
                    if (des != null)
                    {
                        lblPackageDesc.Text = des.Description;
                    }
                }
                if (CurrentPackageID == p.PackageId)
                {
                    rbtnPackage.Checked = true;
                    MyScript += "var pkg" + p.Id + "=document.getElementById('" + rbtnPackage.ClientID + "');ShowHideChoice1(pkg" + p.Id + ");";
                }
                if (CurrentPackageID == 0 && p.PackageIdSource.PackageName.ToLower() == "standard")
                {
                    //rbtnPackage.Checked = true;
                    //MyScript += "var pkg" + p.Id + "=document.getElementById('" + rbtnPackage.ClientID + "');ShowHideChoice1(pkg" + p.Id + ");";
                }
                rbtnPackage.GroupName = CurrentMeetingroom;

            }
        }
    }

    /// <summary>
    /// Repeater package item data bound.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rptPackageItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPackageItem = (Label)e.Item.FindControl("lblPackageItem");
            TextBox txtQuantity = (TextBox)e.Item.FindControl("txtQuantity");
            HiddenField hdnItemID = (HiddenField)e.Item.FindControl("hdnItemID");
            HiddenField hdnVat = (HiddenField)e.Item.FindControl("hdnVat");
            HiddenField hdnPrice = (HiddenField)e.Item.FindControl("hdnPrice");
            DropDownList drpFrom = (DropDownList)e.Item.FindControl("drpFrom");
            DropDownList drpTo = (DropDownList)e.Item.FindControl("drpTo");
            BindDropdown(drpFrom, CurrentFromTime, CurrentToTime);
            BindDropdown(drpTo, CurrentFromTime, CurrentToTime);
            PackageItems p = e.Item.DataItem as PackageItems;
            PackageByHotel objp = p.PackageByHotelCollection.Where(a => a.HotelId == objBooking.HotelID && a.PackageId == MyCurrentPackageID).FirstOrDefault();
            PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
            if (p != null)
            {
                hdnItemID.Value = p.Id.ToString();
                if (pdesc != null)
                {
                    lblPackageItem.Text = pdesc.ItemName;
                }
                else
                {
                    lblPackageItem.Text = p.ItemName;
                }
                Label lblFromNA = (Label)e.Item.FindControl("lblFromNA");
                Label lblToNA = (Label)e.Item.FindControl("lblToNA");
                if (p.ItemName.ToLower().Contains("coffee break(s) morning and/or afternoon"))//|| p.ItemName.ToLower().Contains("morning / afternoon") || p.ItemName.ToLower().Contains("morning/ afternoon") || p.ItemName.ToLower().Contains("morning /afternoon") || p.ItemName.ToLower().Contains("morning / afternoon coffee break")
                {
                    if (CurrentDayTime == 0 || CurrentDayTime == 1)
                    {
                        drpFrom.Visible = true;
                        drpFrom.Enabled = true;
                    }
                    else
                    {
                        drpFrom.Visible = false;
                        drpFrom.Enabled = false;
                        lblFromNA.Text = "NA";
                    }
                    if (CurrentDayTime == 0 || CurrentDayTime == 2)
                    {
                        drpTo.Visible = true;
                        drpTo.Enabled = true;
                    }
                    else
                    {
                        drpTo.Visible = false;
                        drpTo.Enabled = false;
                        lblToNA.Text = "NA";
                    }
                }
                else
                {
                    drpTo.Visible = false;
                }
                hdnVat.Value = Convert.ToString(p.Vat);
                if (objp != null)
                {
                    hdnPrice.Value = Convert.ToString(Math.Round(Convert.ToDecimal((CurrentDayTime == 0 ? objp.FulldayPrice : objp.HalfdayPrice)), 2));
                    MyItemPrice += Convert.ToDecimal((CurrentDayTime == 0 ? objp.FulldayPrice : objp.HalfdayPrice));
                }
                else
                {
                    hdnPrice.Value = Convert.ToString(Math.Round(Convert.ToDecimal("0"), 2));
                    MyItemPrice += Convert.ToDecimal("0");
                }
                if (CurrentPackageItem != null)
                {
                    ManagePackageItem itm = CurrentPackageItem.Where(a => a.ItemId == p.Id).FirstOrDefault();
                    if (itm != null)
                    {
                        txtQuantity.Text = itm.Quantity.ToString();
                        try
                        {
                            drpTo.Items.FindByText(itm.ToTime).Selected = true;
                            drpFrom.Items.FindByText(itm.FromTime).Selected = true;
                        }
                        catch(Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                            drpTo.SelectedIndex = 0;
                            drpFrom.SelectedIndex = 0;
                        }
                    }
                    else
                    {
                        txtQuantity.Text = CurrentNumberOfParticepent.ToString();
                        drpTo.SelectedIndex = 0;
                        drpFrom.SelectedIndex = 0;
                    }

                }
                else
                {
                    txtQuantity.Text = CurrentNumberOfParticepent.ToString();
                }
            }
        }
    }

    protected void rptFoodAndBravrage_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPackageItem = (Label)e.Item.FindControl("lblPackageItem");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblPrice = (Label)e.Item.FindControl("lblPrice");
            DropDownList drpFrom = (DropDownList)e.Item.FindControl("drpFrom");
            TextBox txtQuantity = (TextBox)e.Item.FindControl("txtQuantity");
            Label lblTotal = (Label)e.Item.FindControl("lblTotal");
            BindDropdown(drpFrom, CurrentFromTime, CurrentToTime);
            HiddenField hdnVat = (HiddenField)e.Item.FindControl("hdnVat");
            HiddenField hdnItemID = (HiddenField)e.Item.FindControl("hdnItemID");

            PackageItems p = e.Item.DataItem as PackageItems;
            if (p != null)
            {
                //Package Description
                hdnItemID.Value = p.Id.ToString();
                PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                if (pdesc != null)
                {
                    lblPackageItem.Text = pdesc.ItemName;
                    lblDescription.Text = pdesc.ItemDescription;
                }
                else
                {
                    lblPackageItem.Text = p.ItemName;
                    lblDescription.Text = "";
                }
                hdnVat.Value = Convert.ToString(p.Vat);
                //Package Hotel pricing
                PackageByHotel objp = p.PackageByHotelCollection.Where(a => a.HotelId == objBooking.HotelID && a.PackageId == null).FirstOrDefault();
                if (objp != null)
                {
                    lblPrice.Text = string.Format("{0:#,##,##0.00}", Convert.ToString(Math.Round(Convert.ToDecimal((CurrentDayTime == 0 ? objp.FulldayPrice : objp.HalfdayPrice)) * CurrencyConvert, 2)));
                }
                else
                {
                    lblPrice.Text = "0";
                }
                //Bind Old value
                if (CurrentBuilYourMR != null)
                {
                    BuildYourMR objb = CurrentBuilYourMR.Where(a => a.ItemId == p.Id).FirstOrDefault();
                    if (objb != null)
                    {
                        drpFrom.Items.FindByText(objb.ServeTime).Selected = true;
                        txtQuantity.Text = objb.Quantity.ToString();
                    }
                    else
                    {
                        txtQuantity.Text = "0";
                    }
                }
                else
                {
                    txtQuantity.Text = "0";
                }
                if (objp != null)
                {
                    lblTotal.Text = Math.Round(Convert.ToDecimal(CurrentDayTime == 0 ? objp.FulldayPrice : objp.HalfdayPrice) * Convert.ToDecimal(txtQuantity.Text) * CurrencyConvert, 2).ToString("#,##,##0.00");
                }
                else
                {
                    lblTotal.Text = "0";
                }
            }
        }
    }

    protected void rptExtra_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPackageItem = (Label)e.Item.FindControl("lblPackageItem");
            HiddenField hdnPrice = (HiddenField)e.Item.FindControl("hdnPrice");
            HiddenField hdnItemID = (HiddenField)e.Item.FindControl("hdnItemID");
            HiddenField hdnOriginalPrice = (HiddenField)e.Item.FindControl("hdnOriginalPrice");
            HiddenField hdnVat = (HiddenField)e.Item.FindControl("hdnVat");
            DropDownList drpFrom = (DropDownList)e.Item.FindControl("drpFrom");
            //DropDownList drpTo = (DropDownList)e.Item.FindControl("drpTo");
            Label lblPrice = (Label)e.Item.FindControl("lblPrice");
            Label lblTotal = (Label)e.Item.FindControl("lblTotal");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            TextBox txtQuantity = (TextBox)e.Item.FindControl("txtQuantity");
            //BindDropdown(drpTo, CurrentFromTime, CurrentToTime);
            BindDropdown(drpFrom, CurrentFromTime, CurrentToTime);
            PackageItems p = e.Item.DataItem as PackageItems;
            if (p != null)
            {
                if (CurrentManageExtra != null)
                {
                    ManageExtras mngExt = CurrentManageExtra.Where(a => a.ItemId == p.Id).FirstOrDefault();
                    if (mngExt != null)
                    {
                        drpFrom.Items.FindByText(mngExt.FromTime).Selected = true;
                        //drpTo.Items.FindByText(mngExt.ToTime).Selected = true;
                        txtQuantity.Text = mngExt.Quantity.ToString();
                    }
                    else
                    {
                        txtQuantity.Text = "0";
                    }
                }
                else
                {
                    txtQuantity.Text = "0";
                }
                PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                if (pdesc != null)
                {
                    lblPackageItem.Text = pdesc.ItemName;
                    lblDescription.Text = pdesc.ItemDescription;
                }
                else
                {
                    lblPackageItem.Text = p.ItemName;
                    lblDescription.Text = "";
                }
                hdnItemID.Value = p.Id.ToString();

                hdnVat.Value = Convert.ToString(p.Vat);
                PackageByHotel objPrice = p.PackageByHotelCollection.Where(a => a.ItemId == p.Id && a.HotelId == objBooking.HotelID && a.PackageId == null).FirstOrDefault();
                if (objPrice != null)
                {
                    if (objPrice.IsComplementary)
                    {
                        hdnOriginalPrice.Value = "0";
                        hdnPrice.Value = "0";
                        lblPrice.Text = "0";
                        lblTotal.Text = (Convert.ToInt32(string.IsNullOrEmpty(txtQuantity.Text) ? "0" : txtQuantity.Text) * Convert.ToDecimal(hdnPrice.Value)).ToString();
                    }
                    else
                    {
                        hdnOriginalPrice.Value = string.IsNullOrEmpty(Convert.ToString(CurrentDayTime == 0 ? objPrice.FulldayPrice : objPrice.HalfdayPrice)) ? "0" : Convert.ToString(CurrentDayTime == 0 ? objPrice.FulldayPrice : objPrice.HalfdayPrice);
                        hdnPrice.Value = string.IsNullOrEmpty(Convert.ToString(CurrentDayTime == 0 ? objPrice.FulldayPrice : objPrice.HalfdayPrice)) ? "0" : Math.Round(Convert.ToDecimal(CurrentDayTime == 0 ? objPrice.FulldayPrice : objPrice.HalfdayPrice) * CurrencyConvert, 2).ToString();
                        lblPrice.Text = string.Format("{0:#,##,##0.00}", hdnPrice.Value);
                        lblTotal.Text = string.Format("{0:#,##,##0.00}", (Convert.ToInt32(string.IsNullOrEmpty(txtQuantity.Text) ? "0" : txtQuantity.Text) * Convert.ToDecimal(hdnPrice.Value)).ToString());
                    }
                }
                else
                {
                    hdnOriginalPrice.Value = "0";
                    hdnPrice.Value = "0";
                    lblPrice.Text = "0";
                    lblTotal.Text = string.Format("{0:#,##,##0.00}", (Convert.ToInt32(string.IsNullOrEmpty(txtQuantity.Text) ? "0" : txtQuantity.Text) * Convert.ToDecimal(hdnPrice.Value)).ToString());
                }
            }
        }
    }
    protected void rptOthers_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPackageItem = (Label)e.Item.FindControl("lblPackageItem");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblPrice = (Label)e.Item.FindControl("lblPrice");
            HiddenField hdnVat = (HiddenField)e.Item.FindControl("hdnVat");
            TextBox txtQuantity = (TextBox)e.Item.FindControl("txtQuantity");
            Label lblTotal = (Label)e.Item.FindControl("lblTotal");
            HiddenField hdnItemID = (HiddenField)e.Item.FindControl("hdnItemID");

            PackageItems p = e.Item.DataItem as PackageItems;
            if (p != null)
            {
                //Package Description
                hdnItemID.Value = p.Id.ToString();
                hdnVat.Value = Convert.ToString(p.Vat);
                PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                if (pdesc != null)
                {
                    lblPackageItem.Text = pdesc.ItemName;
                    lblDescription.Text = pdesc.ItemDescription;
                }
                else
                {
                    lblPackageItem.Text = p.ItemName;
                    lblDescription.Text = "";
                }

                //Package Hotel pricing
                PackageByHotel objp = p.PackageByHotelCollection.Where(a => a.HotelId == objBooking.HotelID && a.PackageId == null).FirstOrDefault();
                if (objp != null && objp.IsComplementary != true)
                {
                    lblPrice.Text = string.Format("{0:#,##,##0.00}", Convert.ToString(Math.Round(Convert.ToDecimal(CurrentDayTime == 0 ? objp.FulldayPrice : objp.HalfdayPrice) * CurrencyConvert, 2)));
                    txtQuantity.Text = "0";
                }
                else if (objp.IsComplementary == true)
                {
                    Image imgIsEmentary = (Image)e.Item.FindControl("imgIsComplimentary");
                    imgIsEmentary.Visible = true;
                    // lblPrice.Visible = false;
                    lblPrice.Text = "0";

                    txtQuantity.Enabled = false;
                    txtQuantity.Text = "1";
                }
                else
                {
                    txtQuantity.Text = "0";
                    lblPrice.Text = "0";
                    lblPrice.Visible = false;
                }

                //Bind Old value
                if (CurrentManageOthers != null)
                {
                    ManageOtherItems mngEqu = CurrentManageOthers.Where(a => a.ItemId == p.Id).FirstOrDefault();
                    if (mngEqu != null)
                    {
                        txtQuantity.Text = mngEqu.Quantity.ToString();
                    }
                }
                if (objp != null)
                {
                    lblTotal.Text = Math.Round(Convert.ToDecimal(CurrentDayTime == 0 ? objp.FulldayPrice : objp.HalfdayPrice) * Convert.ToDecimal(txtQuantity.Text) * CurrencyConvert, 2).ToString("#,##,##0.00");
                }
                else
                {
                    lblTotal.Text = "0";
                }
            }
        }
    }
    protected void repEquipment_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPackageItem = (Label)e.Item.FindControl("lblPackageItem");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            Label lblPrice = (Label)e.Item.FindControl("lblPrice");
            HiddenField hdnVat = (HiddenField)e.Item.FindControl("hdnVat");
            TextBox txtQuantity = (TextBox)e.Item.FindControl("txtQuantity");
            Label lblTotal = (Label)e.Item.FindControl("lblTotal");
            HiddenField hdnItemID = (HiddenField)e.Item.FindControl("hdnItemID");

            PackageItems p = e.Item.DataItem as PackageItems;
            if (p != null)
            {
                //Package Description
                hdnItemID.Value = p.Id.ToString();
                hdnVat.Value = Convert.ToString(p.Vat);
                PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                if (pdesc != null)
                {
                    lblPackageItem.Text = pdesc.ItemName;
                    lblDescription.Text = pdesc.ItemDescription;
                }
                else
                {
                    lblPackageItem.Text = p.ItemName;
                    lblDescription.Text = "";
                }
                ////Package Hotel pricing
                //PackageByHotel objp = p.PackageByHotelCollection.Where(a => a.HotelId == objBooking.HotelID && a.PackageId == null).FirstOrDefault();
                //if (objp != null)
                //{
                //    lblPrice.Text = string.Format("{0:#,##,##0.00}", Convert.ToString(Math.Round(Convert.ToDecimal(CurrentDayTime == 0 ? objp.FulldayPrice : objp.HalfdayPrice) * CurrencyConvert, 2)));

                //}
                //else
                //{
                //    lblPrice.Text = "0";
                //}

                //Package Hotel pricing
                PackageByHotel objp = p.PackageByHotelCollection.Where(a => a.HotelId == objBooking.HotelID && a.PackageId == null).FirstOrDefault();
                if (objp != null && objp.IsComplementary != true)
                {
                    lblPrice.Text = string.Format("{0:#,##,##0.00}", Convert.ToString(Math.Round(Convert.ToDecimal(CurrentDayTime == 0 ? objp.FulldayPrice : objp.HalfdayPrice) * CurrencyConvert, 2)));
                    txtQuantity.Text = "0";
                }
                else if (objp.IsComplementary == true)
                {
                    Image imgIsEmentary = (Image)e.Item.FindControl("imgIsComplimentary");
                    imgIsEmentary.Visible = true;
                    // lblPrice.Visible = false;
                    lblPrice.Text = "0";

                    txtQuantity.Enabled = false;
                    txtQuantity.Text = "1";
                }
                else
                {
                    txtQuantity.Text = "0";
                    lblPrice.Text = "0";
                    lblPrice.Visible = false;
                }

                //Bind Old value
                if (CurrentManageEquipment != null)
                {
                    ManageEquipment mngEqu = CurrentManageEquipment.Where(a => a.ItemId == p.Id).FirstOrDefault();
                    if (mngEqu != null)
                    {
                        txtQuantity.Text = mngEqu.Quantity.ToString();
                    }
                }
                if (objp != null)
                {
                    lblTotal.Text = Math.Round(Convert.ToDecimal(CurrentDayTime == 0 ? objp.FulldayPrice : objp.HalfdayPrice) * Convert.ToDecimal(txtQuantity.Text) * CurrencyConvert, 2).ToString("#,##,##0.00");
                }
                else
                {
                    lblTotal.Text = "0";
                }
            }
        }
    }


    public void BindPackages(ref Repeater rptPackage, Int64 HotelId, List<ManagePackageItem> objManagePackage, Int64 packageID)
    {
        try
        {
            CurrentPackageItem = objManagePackage;
            TList<PackageByHotel> objPbh = objHotel.GetPackageDetailsByHotel(HotelId).FindAllDistinct(PackageByHotelColumn.PackageId);
            PackageByHotel checkp = null;
            if (CurrentNumberOfParticepent <= 19)
            {
                checkp = objPbh.Where(a => a.PackageIdSource.PackageName.ToLower() == "elegant").FirstOrDefault();
                //PackageByHotel checkp2 = objPbh.Where(a => a.PackageIdSource.PackageName == "favourite").FirstOrDefault();
                if (checkp != null)
                {
                    rptPackage.DataSource = objHotel.GetPackageDetailsByHotel(HotelId).FindAllDistinct(PackageByHotelColumn.PackageId).Where(a => a.PackageId != checkp.PackageId);
                    rptPackage.DataBind();
                }
                else
                {
                    rptPackage.DataSource = objHotel.GetPackageDetailsByHotel(HotelId).FindAllDistinct(PackageByHotelColumn.PackageId);
                    rptPackage.DataBind();
                }
            }
            else
            {
                rptPackage.DataSource = objHotel.GetPackageDetailsByHotel(HotelId).FindAllDistinct(PackageByHotelColumn.PackageId);
                rptPackage.DataBind();
            }
        }
        catch (Exception ex)
        {
        }
    }
    public void BindOtherItems(ref Repeater rptOthers, Int64 HotelId, List<ManageOtherItems> objManageOtherItems)
    {
        try
        {
            CurrentManageOthers = objManageOtherItems;
            rptOthers.DataSource = objHotel.GetItemDetailsByHotelandType(HotelId, ItemType.Others).FindAllDistinct(PackageItemsColumn.Id);
            rptOthers.DataBind();
        }
        catch (Exception ex)
        {
        }
    }
    public void BindEquipment(ref Repeater rptEquip, Int64 HotelId, List<ManageEquipment> objManagePackage)
    {
        try
        {
            CurrentManageEquipment = objManagePackage;
            rptEquip.DataSource = objHotel.GetItemDetailsByHotelandType(HotelId, ItemType.Equipment).FindAllDistinct(PackageItemsColumn.Id);
            rptEquip.DataBind();
        }
        catch (Exception ex)
        {
        }
    }

    public void BindIsExtra(ref Repeater rptExtra, Int64 HotelId, List<ManageExtras> objManagePackage, Panel pnlNoExtra)
    {
        try
        {
            CurrentManageExtra = objManagePackage;
            rptExtra.DataSource = objHotel.GetIsExtraItemDetailsByHotel(HotelId).FindAllDistinct(PackageItemsColumn.Id);
            rptExtra.DataBind();
            if (objHotel.GetIsExtraItemDetailsByHotel(HotelId).FindAllDistinct(PackageItemsColumn.Id).Count <= 0)
            {
                pnlNoExtra.Visible = true;
            }
            else
            {
                pnlNoExtra.Visible = false;
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    public void BindFoodAndBravrages(ref Repeater rptFoodAndBravrage, Int64 HotelId, List<BuildYourMR> objManagePackage)
    {
        try
        {
            CurrentBuilYourMR = objManagePackage;
            //if (CurrentNumberOfParticepent < 20)
            //{
            //    rptFoodAndBravrage.DataSource = objHotel.GetItemDetailsByHotelandType(HotelId, ItemType.FoodBeverages).FindAllDistinct(PackageItemsColumn.Id).Where(a => a.ItemName.ToLower() != "hot & cold buffet (as for min. 20 people)");
            //}
            //else
            //{
            rptFoodAndBravrage.DataSource = objHotel.GetItemDetailsByHotelandType(HotelId, ItemType.FoodBeverages).FindAllDistinct(PackageItemsColumn.Id);
            //}
            rptFoodAndBravrage.DataBind();
        }
        catch(Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion
    protected void btnNext_Click(object sender, EventArgs e)
    {
        SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
        objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
        //Get The Meeting Room Details
        if (st.CurrentDay == 2)
        {
            if (rbtnSameAsDay1.Checked)
            {
                objBooking.SecondMRNotSameAsRoom1 = false;
                ManageSameAsDay1(objBooking);
            }
            else
            {
                objBooking.SecondMRNotSameAsRoom1 = true;
                foreach (RepeaterItem mr in rptMeetingroom.Items)
                {
                    RadioButton rbtnNeedToConfigure = (RadioButton)mr.FindControl("rbtnNeedToConfigure");
                    HiddenField hdnMeetingRoomID = (HiddenField)mr.FindControl("hdnMeetingRoomID");
                    //End Controls
                    if (rbtnNeedToConfigure == null)
                    {
                        ManageMeetingRoomDetails(mr, Convert.ToInt64(hdnMeetingRoomID.Value));
                    }
                    else
                    {
                        if (rbtnNeedToConfigure.Checked)
                        {
                            ManageMeetingRoomDetails(mr, Convert.ToInt64(hdnMeetingRoomID.Value));
                        }
                        else
                        {
                            BookedMrConfig objFirstMR = objBooking.MeetingroomList[0].MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault();
                            objBooking.MeetingroomList[1].MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().IsBreakdown = true;
                        }
                    }
                }
            }
        }
        else
        {
            foreach (RepeaterItem mr in rptMeetingroom.Items)
            {
                RadioButton rbtnNeedToConfigure = (RadioButton)mr.FindControl("rbtnNeedToConfigure");
                HiddenField hdnMeetingRoomID = (HiddenField)mr.FindControl("hdnMeetingRoomID");
                //End Controls
                if (rbtnNeedToConfigure == null)
                {
                    ManageMeetingRoomDetails(mr, Convert.ToInt64(hdnMeetingRoomID.Value));
                }
                else
                {
                    if (rbtnNeedToConfigure.Checked)
                    {
                        ManageMeetingRoomDetails(mr, Convert.ToInt64(hdnMeetingRoomID.Value));
                    }
                    else
                    {
                        BookedMrConfig objFirstMR = objBooking.MeetingroomList[0].MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault();
                        objBooking.MeetingroomList[1].MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().IsBreakdown = true;
                    }
                }
            }
            hdnDay1Price.Value = (Convert.ToDecimal(hdnDay1Price.Value) * CurrencyConvert).ToString();
        }
        if (objBooking.Duration == CurrentDay)
        {
            //End Meetingroom Details
            if (rbtnBuildYourAccomodation.Checked)
            {
                objBooking.NoAccomodation = false;
            }
            else
            {
                objBooking.NoAccomodation = true;
            }
            ManageAccomodation();
            objBooking.TotalBookingPrice += objBooking.AccomodationPriceTotal;
        }
        if (CurrentDay < objBooking.Duration)
        {
            //Session["Search"] = objBooking;
            st.SearchObject = TrailManager.XmlSerialize(objBooking);
            CurrentDay++;
            st.CurrentDay = CurrentDay;
            if (bm.SaveSearch(st))
            {

                CheckSearchAvailable();
                //BindBooking();
                //hdnFinalPrice.Value = (objBooking.TotalBookingPrice - Convert.ToDecimal(hdnMeetingroom1Price.Value) - (IsSecondMeetingRoom?Convert.ToDecimal(hdnMeetingroom2Price.Value):0)).ToString();
            }
            //Page.RegisterStartupScript("a", "<script language='javascript'>jQuery(document).ready(function(){" + MyScript + "});</script>");
            ltrScript.Text = "<script language='javascript'>jQuery(document).ready(function(){" + MyScript + "});</script>";
        }
        else
        {
            Calculate(objBooking);
            st.SearchObject = TrailManager.XmlSerialize(objBooking);
            //Session["Search"] = objBooking;
            st.CurrentDay = 1;
            st.CurrentStep = 3;
            if (bm.SaveSearch(st))
            {
                Response.Redirect("~/Agency/BookingStep3.aspx");
            }
        }

    }


    public void ManageSameAsDay1(Createbooking objBooked)
    {
        if (objBooked != null)
        {
            for (int meetingroomcount = 0; meetingroomcount < objBooking.MeetingroomList.Count; meetingroomcount++)
            {
                if (meetingroomcount == 0)
                {
                    if (objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 2).FirstOrDefault() != null)
                    {
                        int currentUsers = objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 1).FirstOrDefault().NoOfParticepant;
                        objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 2).FirstOrDefault().NoOfParticepant = currentUsers;
                        #region Build Package

                        List<BuildYourMR> objCheckBuildMeetingRoom = objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 1).FirstOrDefault().BuildManageMRLst;
                        List<BuildYourMR> objForBuildDay2 = new List<BuildYourMR>();
                        for (int j = 0; j < objCheckBuildMeetingRoom.Count; j++)
                        {
                            BuildYourMR obj = new BuildYourMR();
                            obj.ItemId = objCheckBuildMeetingRoom[j].ItemId;
                            obj.ItemPrice = objCheckBuildMeetingRoom[j].ItemPrice;
                            obj.ServeTime = objCheckBuildMeetingRoom[j].ServeTime;
                            obj.vatpercent = objCheckBuildMeetingRoom[j].vatpercent;
                            obj.Quantity = objCheckBuildMeetingRoom[j].Quantity;
                            objForBuildDay2.Add(obj);
                        }
                        objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 2).FirstOrDefault().BuildManageMRLst = objForBuildDay2;
                        objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 2).FirstOrDefault().PackageID = objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 1).FirstOrDefault().PackageID;
                        #endregion

                        #region Package

                        List<ManagePackageItem> objPackageItemlist = objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 1).FirstOrDefault().ManagePackageLst;
                        List<ManagePackageItem> objForPackageItemDay2 = new List<ManagePackageItem>();
                        for (int k = 0; k < objPackageItemlist.Count; k++)
                        {
                            ManagePackageItem obj = new ManagePackageItem();
                            obj.ItemId = objPackageItemlist[k].ItemId;
                            obj.ItemPrice = objPackageItemlist[k].ItemPrice;
                            obj.FromTime = objPackageItemlist[k].FromTime;
                            obj.ToTime = objPackageItemlist[k].ToTime;
                            obj.vatpercent = objPackageItemlist[k].vatpercent;
                            obj.Quantity = objPackageItemlist[k].Quantity;
                            objForPackageItemDay2.Add(obj);

                        }
                        objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 2).FirstOrDefault().PackagePricePerPerson = objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 1).FirstOrDefault().PackagePricePerPerson;
                        objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 2).FirstOrDefault().ManagePackageLst = objForPackageItemDay2;

                        #endregion

                        #region Extras
                        List<ManageExtras> objManageExtras = objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 1).FirstOrDefault().ManageExtrasLst;
                        List<ManageExtras> objForManageExtrasDay2 = new List<ManageExtras>();
                        for (int k = 0; k < objManageExtras.Count; k++)
                        {
                            ManageExtras obj = new ManageExtras();
                            obj.ItemId = objManageExtras[k].ItemId;
                            obj.ItemPrice = objManageExtras[k].ItemPrice;
                            obj.FromTime = objManageExtras[k].FromTime;
                            obj.ToTime = objManageExtras[k].ToTime;
                            obj.vatpercent = objManageExtras[k].vatpercent;
                            obj.Quantity = objManageExtras[k].Quantity; ;
                            objForManageExtrasDay2.Add(obj);
                        }
                        objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 2).FirstOrDefault().ManageExtrasLst = objForManageExtrasDay2;
                        #endregion

                        #region Equipments
                        List<ManageEquipment> objEquipments = objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 1).FirstOrDefault().EquipmentLst;
                        List<ManageEquipment> objForEquipmentsDay2 = new List<ManageEquipment>();
                        for (int k = 0; k < objEquipments.Count; k++)
                        {
                            ManageEquipment obj = new ManageEquipment();
                            obj.ItemId = objEquipments[k].ItemId;
                            obj.ItemPrice = objEquipments[k].ItemPrice;
                            obj.vatpercent = objEquipments[k].vatpercent;
                            obj.Quantity = objEquipments[k].Quantity;
                            objForEquipmentsDay2.Add(obj);
                        }
                        objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 2).FirstOrDefault().EquipmentLst = objForEquipmentsDay2;
                        #endregion

                        #region Others
                        List<ManageOtherItems> objOthers = objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 1).FirstOrDefault().BuildOthers;
                        List<ManageOtherItems> objOthersDay2 = new List<ManageOtherItems>();
                        for (int k = 0; k < objOthers.Count; k++)
                        {
                            ManageOtherItems obj = new ManageOtherItems();
                            obj.ItemId = objOthers[k].ItemId;
                            obj.ItemPrice = objOthers[k].ItemPrice;
                            obj.vatpercent = objOthers[k].vatpercent;
                            obj.Quantity = objOthers[k].Quantity;
                            objOthersDay2.Add(obj);
                        }
                        objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 2).FirstOrDefault().BuildOthers = objOthersDay2;
                        #endregion
                    }
                }
                else
                {
                    if (objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 2).FirstOrDefault() != null)
                    {
                        int currentUsers = objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 1).FirstOrDefault().NoOfParticepant;
                        objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 2).FirstOrDefault().NoOfParticepant = currentUsers;
                        objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 2).FirstOrDefault().IsBreakdown = objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 1).FirstOrDefault().IsBreakdown;
                        #region Build Package
                        List<BuildYourMR> objCheckBuildMeetingRoom = objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 1).FirstOrDefault().BuildManageMRLst;
                        List<BuildYourMR> objForBuildDay2 = new List<BuildYourMR>();
                        for (int j = 0; j < objCheckBuildMeetingRoom.Count; j++)
                        {
                            BuildYourMR obj = new BuildYourMR();
                            obj.ItemId = objCheckBuildMeetingRoom[j].ItemId;
                            obj.ItemPrice = objCheckBuildMeetingRoom[j].ItemPrice;
                            obj.ServeTime = objCheckBuildMeetingRoom[j].ServeTime;
                            obj.vatpercent = objCheckBuildMeetingRoom[j].vatpercent;
                            obj.Quantity = objCheckBuildMeetingRoom[j].Quantity;
                            objForBuildDay2.Add(obj);
                        }
                        objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 2).FirstOrDefault().BuildManageMRLst = objForBuildDay2;
                        objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 2).FirstOrDefault().PackageID = objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 1).FirstOrDefault().PackageID;
                        #endregion

                        #region Package
                        List<ManagePackageItem> objPackageItemlist = objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 1).FirstOrDefault().ManagePackageLst;
                        List<ManagePackageItem> objForPackageItemDay2 = new List<ManagePackageItem>();
                        for (int k = 0; k < objPackageItemlist.Count; k++)
                        {
                            ManagePackageItem obj = new ManagePackageItem();
                            obj.ItemId = objPackageItemlist[k].ItemId;
                            obj.ItemPrice = objPackageItemlist[k].ItemPrice;
                            obj.FromTime = objPackageItemlist[k].FromTime;
                            obj.ToTime = objPackageItemlist[k].ToTime;
                            obj.vatpercent = objPackageItemlist[k].vatpercent;
                            obj.Quantity = objPackageItemlist[k].Quantity;
                            objForPackageItemDay2.Add(obj);
                        }
                        objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 2).FirstOrDefault().PackagePricePerPerson = objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 1).FirstOrDefault().PackagePricePerPerson;
                        objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 2).FirstOrDefault().ManagePackageLst = objForPackageItemDay2;
                        #endregion

                        #region Extras
                        List<ManageExtras> objManageExtras = objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 1).FirstOrDefault().ManageExtrasLst;
                        List<ManageExtras> objForManageExtrasDay2 = new List<ManageExtras>();
                        for (int k = 0; k < objManageExtras.Count; k++)
                        {
                            ManageExtras obj = new ManageExtras();
                            obj.ItemId = objManageExtras[k].ItemId;
                            obj.ItemPrice = objManageExtras[k].ItemPrice;
                            obj.FromTime = objManageExtras[k].FromTime;
                            obj.ToTime = objManageExtras[k].ToTime;
                            obj.vatpercent = objManageExtras[k].vatpercent;
                            obj.Quantity = objManageExtras[k].Quantity;
                            objForManageExtrasDay2.Add(obj);
                        }
                        objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 2).FirstOrDefault().ManageExtrasLst = objForManageExtrasDay2;
                        #endregion

                        #region Equipments
                        List<ManageEquipment> objEquipments = objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 1).FirstOrDefault().EquipmentLst;
                        List<ManageEquipment> objForEquipmentsDay2 = new List<ManageEquipment>();
                        for (int k = 0; k < objEquipments.Count; k++)
                        {
                            ManageEquipment obj = new ManageEquipment();
                            obj.ItemId = objEquipments[k].ItemId;
                            obj.ItemPrice = objEquipments[k].ItemPrice;
                            obj.vatpercent = objEquipments[k].vatpercent;
                            obj.Quantity = objEquipments[k].Quantity;
                            objForEquipmentsDay2.Add(obj);
                        }
                        objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 2).FirstOrDefault().EquipmentLst = objForEquipmentsDay2;
                        #endregion

                        #region Equipments
                        List<ManageOtherItems> objOther = objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 1).FirstOrDefault().BuildOthers;
                        List<ManageOtherItems> objOthers2 = new List<ManageOtherItems>();
                        for (int k = 0; k < objOther.Count; k++)
                        {
                            ManageOtherItems obj = new ManageOtherItems();
                            obj.ItemId = objOther[k].ItemId;
                            obj.ItemPrice = objOther[k].ItemPrice;
                            obj.vatpercent = objOther[k].vatpercent;
                            obj.Quantity = objOther[k].Quantity;
                            objOthers2.Add(obj);
                        }
                        objBooking.MeetingroomList[meetingroomcount].MrDetails.Where(a => a.SelectedDay == 2).FirstOrDefault().BuildOthers = objOther;
                        #endregion
                    }
                }
            }

        }
    }

    public void ManageMeetingRoomDetails(RepeaterItem mr, Int64 mrid)
    {
        //Find All the controls
        Repeater rptDays = (Repeater)mr.FindControl("rptDays");
        Repeater reppackage = (Repeater)mr.FindControl("reppackage");
        Repeater rptExtra = (Repeater)mr.FindControl("rptExtra");
        Repeater rptFoodAndBravrage = (Repeater)mr.FindControl("rptFoodAndBravrage");
        Repeater rptOthers = (Repeater)mr.FindControl("rptOthers");
        Repeater repEquipment = (Repeater)mr.FindControl("repEquipment");
        RadioButton ChoicePackage = (RadioButton)mr.FindControl("ChoicePackage");
        CheckBox chkIsExtra = (CheckBox)mr.FindControl("chkIsExtra");

        #region Manage Meetingroom
        //Manage Number of days Details
        BookedMR objBookedMr = objBooking.MeetingroomList.Where(a => a.MRId == mrid).FirstOrDefault();
        foreach (RepeaterItem days in rptDays.Items)
        {
            TextBox txtNumberOfParticepant = (TextBox)days.FindControl("txtNumberOfParticepant");
            BookedMrConfig objBookeConfig = days.DataItem as BookedMrConfig;
            objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().NoOfParticepant = Convert.ToInt32(txtNumberOfParticepant.Text);
        }
        //End Number of Details

        //Manage Package Choice
        if (ChoicePackage.Checked)
        {
            CurrentPackagePrice = 0;
            #region Package Selection
            //Package Selected
            foreach (RepeaterItem package in reppackage.Items)
            {
                PackageMaster p = package.DataItem as PackageMaster;
                HiddenField hdnPackageID = (HiddenField)package.FindControl("hdnPackageID");
                RadioButton rbtnPackage = (RadioButton)package.FindControl("rbtnPackage");
                Repeater rptPackageItem = (Repeater)package.FindControl("rptPackageItem");
                Label lblTotalPackagePrice = (Label)package.FindControl("lblTotalPackagePrice");
                Label lblPackagePrice = (Label)package.FindControl("lblPackagePrice");
                //Package Selected Items
                if (rbtnPackage.Checked)
                {
                    objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().PackageID = Convert.ToInt64(hdnPackageID.Value);
                    objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().PackagePricePerPerson = Convert.ToDecimal(lblPackagePrice.Attributes["actualprice"]);
                    objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().ManagePackageLst = new List<ManagePackageItem>();
                    foreach (RepeaterItem packageitem in rptPackageItem.Items)
                    {
                        PackageItems pitem = packageitem.DataItem as PackageItems;
                        HiddenField hdnItemID = (HiddenField)packageitem.FindControl("hdnItemID");
                        DropDownList drpFrom = (DropDownList)packageitem.FindControl("drpFrom");
                        DropDownList drpTo = (DropDownList)packageitem.FindControl("drpTo");
                        TextBox txtQuantity = (TextBox)packageitem.FindControl("txtQuantity");
                        HiddenField hdnVat = (HiddenField)packageitem.FindControl("hdnVat");
                        HiddenField hdnPrice = (HiddenField)packageitem.FindControl("hdnPrice");
                        ManagePackageItem objPackageItem = new ManagePackageItem();
                        objPackageItem.ItemId = Convert.ToInt64(hdnItemID.Value);
                        objPackageItem.FromTime = drpFrom.SelectedItem.Text;
                        objPackageItem.ToTime = drpTo.SelectedItem.Text;
                        objPackageItem.vatpercent = Convert.ToDecimal(string.IsNullOrEmpty(hdnVat.Value) ? "0" : hdnVat.Value);
                        objPackageItem.ItemPrice = (Convert.ToDecimal(string.IsNullOrEmpty(hdnPrice.Value) ? "0" : hdnPrice.Value) / CurrencyConvert);
                        objPackageItem.Quantity = Convert.ToInt32(string.IsNullOrEmpty(txtQuantity.Text) ? "0" : txtQuantity.Text);
                        if (objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().ManagePackageLst == null)
                        {
                            objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().ManagePackageLst = new List<ManagePackageItem>();
                        }
                        if (objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().ManagePackageLst.Where(a => a.ItemId == objPackageItem.ItemId).FirstOrDefault() != null)
                        {
                            objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().ManagePackageLst.Remove(objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().ManagePackageLst.Where(a => a.ItemId == objPackageItem.ItemId).FirstOrDefault());
                        }
                        objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().ManagePackageLst.Add(objPackageItem);
                    }
                }
                //End Package Selected Item
            }
            #endregion
            //End Package Selected
            #region Extras
            //Manage Extra Item
            if (chkIsExtra.Checked || !chkIsExtra.Checked)
            {
                objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().ManageExtrasLst = new List<ManageExtras>();
                foreach (RepeaterItem extra in rptExtra.Items)
                {
                    PackageItems p = extra.DataItem as PackageItems;
                    HiddenField hdnItemID = (HiddenField)extra.FindControl("hdnItemID");
                    DropDownList drpFrom = (DropDownList)extra.FindControl("drpFrom");
                    //DropDownList drpTo = (DropDownList)extra.FindControl("drpTo");
                    TextBox txtQuantity = (TextBox)extra.FindControl("txtQuantity");
                    HiddenField hdnPrice = (HiddenField)extra.FindControl("hdnPrice");
                    HiddenField hdnVat = (HiddenField)extra.FindControl("hdnVat");

                    ManageExtras objExtra = new ManageExtras();
                    objExtra.ItemId = Convert.ToInt64(hdnItemID.Value);
                    objExtra.FromTime = drpFrom.SelectedItem.Text;
                    //objExtra.ToTime = drpTo.SelectedItem.Text;
                    objExtra.Quantity = Convert.ToInt32(string.IsNullOrEmpty(txtQuantity.Text) ? "0" : txtQuantity.Text);
                    objExtra.vatpercent = Convert.ToDecimal(string.IsNullOrEmpty(hdnVat.Value) ? "0" : hdnVat.Value);
                    objExtra.ItemPrice = (Convert.ToDecimal(string.IsNullOrEmpty(hdnPrice.Value) ? "0" : hdnPrice.Value) / CurrencyConvert);

                    if (objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().ManageExtrasLst == null)
                    {
                        objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().ManageExtrasLst = new List<ManageExtras>();
                    }
                    if (objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().ManageExtrasLst.Where(a => a.ItemId == objExtra.ItemId).FirstOrDefault() != null)
                    {
                        objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().ManageExtrasLst.Remove(objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().ManageExtrasLst.Where(a => a.ItemId == objExtra.ItemId).FirstOrDefault());
                    }
                    objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().ManageExtrasLst.Add(objExtra);
                }
            }
            objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().BuildManageMRLst = null;
            //End Manage Extra Item
            #endregion
        }//End Package Choice //Start Build Meeting room
        else
        {
            #region BuildYourMeetingroom
            //Manage Food and Bravrages
            objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().BuildManageMRLst = new List<BuildYourMR>();
            foreach (RepeaterItem FoodandBrev in rptFoodAndBravrage.Items)
            {
                PackageItems p = FoodandBrev.DataItem as PackageItems;
                HiddenField hdnItemID = (HiddenField)FoodandBrev.FindControl("hdnItemID");
                DropDownList drpFrom = (DropDownList)FoodandBrev.FindControl("drpFrom");
                TextBox txtQuantity = (TextBox)FoodandBrev.FindControl("txtQuantity");
                HiddenField hdnVat = (HiddenField)FoodandBrev.FindControl("hdnVat");

                Label lblPrice = (Label)FoodandBrev.FindControl("lblPrice");
                if (Convert.ToInt32(string.IsNullOrEmpty(txtQuantity.Text) ? "0" : txtQuantity.Text) != 0)
                {
                    BuildYourMR objBuild = new BuildYourMR();
                    objBuild.ItemId = Convert.ToInt64(hdnItemID.Value);
                    objBuild.ServeTime = drpFrom.SelectedItem.Text;
                    objBuild.vatpercent = Convert.ToDecimal(string.IsNullOrEmpty(hdnVat.Value) ? "0" : hdnVat.Value);
                    objBuild.Quantity = Convert.ToInt32(string.IsNullOrEmpty(txtQuantity.Text) ? "0" : txtQuantity.Text);
                    objBuild.ItemPrice = (Convert.ToDecimal(string.IsNullOrEmpty(lblPrice.Text) ? "0" : lblPrice.Text.Replace(",", "")) / CurrencyConvert);

                    if (objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().BuildManageMRLst == null)
                    {
                        objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().BuildManageMRLst = new List<BuildYourMR>();
                    }
                    if (objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().BuildManageMRLst.Where(a => a.ItemId == objBuild.ItemId).FirstOrDefault() != null)
                    {
                        objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().BuildManageMRLst.Remove(objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().BuildManageMRLst.Where(a => a.ItemId == objBuild.ItemId).FirstOrDefault());
                    }
                    objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().BuildManageMRLst.Add(objBuild);
                }
            }

            objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().PackageID = 0;
            objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().ManagePackageLst = null;
            objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().ManageExtrasLst = null;
            #endregion
            //End Food and Bravrages
        }
        //End Build Meetingroom
        #region Others
        //Manage Equipments

        objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().BuildOthers = new List<ManageOtherItems>();
        foreach (RepeaterItem roth in rptOthers.Items)
        {
            PackageItems equip = roth.DataItem as PackageItems;
            HiddenField hdnItemID = (HiddenField)roth.FindControl("hdnItemID");
            TextBox txtQuantity = (TextBox)roth.FindControl("txtQuantity");
            HiddenField hdnVat = (HiddenField)roth.FindControl("hdnVat");
            Label lblPrice = (Label)roth.FindControl("lblPrice");

            if (Convert.ToInt32(string.IsNullOrEmpty(txtQuantity.Text) ? "0" : txtQuantity.Text) != 0)
            {
                ManageOtherItems objBuildO = new ManageOtherItems();
                objBuildO.ItemId = Convert.ToInt64(hdnItemID.Value);
                objBuildO.vatpercent = Convert.ToDecimal(string.IsNullOrEmpty(hdnVat.Value) ? "0" : hdnVat.Value);
                objBuildO.Quantity = Convert.ToInt32(txtQuantity.Text);
                objBuildO.ItemPrice = (Convert.ToDecimal(string.IsNullOrEmpty(lblPrice.Text) ? "0" : lblPrice.Text.Replace(",", "")) / CurrencyConvert);
                if (objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().BuildOthers == null)
                {
                    objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().BuildOthers = new List<ManageOtherItems>();
                }
                if (objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().BuildOthers.Where(a => a.ItemId == objBuildO.ItemId).FirstOrDefault() != null)
                {
                    objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().BuildOthers.Remove(objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().BuildOthers.Where(a => a.ItemId == objBuildO.ItemId).FirstOrDefault());
                }
                objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().BuildOthers.Add(objBuildO);
            }
        }

        //End Equipments
        #endregion
        #region Equipments
        //Manage Equipments
        objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().EquipmentLst = new List<ManageEquipment>();
        foreach (RepeaterItem equipment in repEquipment.Items)
        {
            PackageItems equip = equipment.DataItem as PackageItems;
            HiddenField hdnItemID = (HiddenField)equipment.FindControl("hdnItemID");
            TextBox txtQuantity = (TextBox)equipment.FindControl("txtQuantity");
            HiddenField hdnVat = (HiddenField)equipment.FindControl("hdnVat");
            Label lblPrice = (Label)equipment.FindControl("lblPrice");

            if (Convert.ToInt32(string.IsNullOrEmpty(txtQuantity.Text) ? "0" : txtQuantity.Text) != 0)
            {
                ManageEquipment objBuild = new ManageEquipment();
                objBuild.ItemId = Convert.ToInt64(hdnItemID.Value);
                objBuild.vatpercent = Convert.ToDecimal(string.IsNullOrEmpty(hdnVat.Value) ? "0" : hdnVat.Value);
                objBuild.Quantity = Convert.ToInt32(txtQuantity.Text);
                objBuild.ItemPrice = (Convert.ToDecimal(string.IsNullOrEmpty(lblPrice.Text) ? "0" : lblPrice.Text.Replace(",", "")) / CurrencyConvert);
                if (objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().EquipmentLst == null)
                {
                    objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().EquipmentLst = new List<ManageEquipment>();
                }
                if (objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().EquipmentLst.Where(a => a.ItemId == objBuild.ItemId).FirstOrDefault() != null)
                {
                    objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().EquipmentLst.Remove(objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().EquipmentLst.Where(a => a.ItemId == objBuild.ItemId).FirstOrDefault());
                }
                objBooking.MeetingroomList.Where(a => a.MRId == objBookedMr.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().EquipmentLst.Add(objBuild);
            }
        }

        //End Equipments
        #endregion
        #endregion
    }

    public void ManageAccomodation()
    {
        decimal TotalAccomodationPrice = 0;
        if(rbtnBuildYourAccomodation.Checked)
        {
        foreach (RepeaterItem accomo in rptAccomodationDaysManager.Items)
        {
            Repeater rptSingleQuantity = (Repeater)accomo.FindControl("rptSingleQuantity");
            Repeater rptDoubleQuantity = (Repeater)accomo.FindControl("rptDoubleQuantity");
            HiddenField hdnBedroomID = (HiddenField)accomo.FindControl("hdnBedroomId");
            foreach (RepeaterItem room in rptSingleQuantity.Items)
            {
                TextBox txtQuantitySDay = (TextBox)room.FindControl("txtQuantitySDay");
                HiddenField hdnDate = (HiddenField)room.FindControl("hdnDate");
                HiddenField hdnPrice = (HiddenField)room.FindControl("hdnPrice");
                if (string.IsNullOrEmpty(txtQuantitySDay.Text.Trim()))
                {
                    objBooking.ManageAccomodationLst.Where(a => a.BedroomId == Convert.ToInt64(hdnBedroomID.Value) && a.DateRequest.ToString() == hdnDate.Value).FirstOrDefault().QuantitySingle = 0;
                    TotalAccomodationPrice += Convert.ToDecimal(hdnPrice.Value) * 0;
                }
                else
                {
                    if (txtQuantitySDay.Text.Trim() == "NA")
                    {
                        txtQuantitySDay.Text = "0";
                    }
                    objBooking.ManageAccomodationLst.Where(a => a.BedroomId == Convert.ToInt64(hdnBedroomID.Value) && a.DateRequest.ToString() == hdnDate.Value).FirstOrDefault().QuantitySingle = Convert.ToInt32(txtQuantitySDay.Text);
                    TotalAccomodationPrice += Convert.ToDecimal(hdnPrice.Value) * Convert.ToInt32(txtQuantitySDay.Text);
                }
            }
            foreach (RepeaterItem room in rptDoubleQuantity.Items)
            {
                TextBox txtQuantityDDay = (TextBox)room.FindControl("txtQuantityDDay");
                HiddenField hdnDate = (HiddenField)room.FindControl("hdnDate");
                HiddenField hdnPrice = (HiddenField)room.FindControl("hdnPrice");
                if (string.IsNullOrEmpty(txtQuantityDDay.Text.Trim()))
                {
                    objBooking.ManageAccomodationLst.Where(a => a.BedroomId == Convert.ToInt64(hdnBedroomID.Value) && a.DateRequest.ToString() == hdnDate.Value).FirstOrDefault().QuantityDouble = 0;
                    TotalAccomodationPrice += Convert.ToDecimal(hdnPrice.Value) * 0;
                }
                else
                {
                    if (txtQuantityDDay.Text.Trim() == "NA")
                    {
                        txtQuantityDDay.Text = "0";
                    }
                    objBooking.ManageAccomodationLst.Where(a => a.BedroomId == Convert.ToInt64(hdnBedroomID.Value) && a.DateRequest.ToString() == hdnDate.Value).FirstOrDefault().QuantityDouble = Convert.ToInt32(txtQuantityDDay.Text);
                    TotalAccomodationPrice += Convert.ToDecimal(hdnPrice.Value) * Convert.ToInt32(txtQuantityDDay.Text);
                }
            }
            //commented by manas objBooking.BedroomNote = txtNoteBedroom.Text;
        }
        objBooking.BedroomNote = txtNoteBedroom.Text;
        //objBooking.AccomodationPriceTotal = TotalAccomodationPrice;
        //Commented by Manas objBooking.SpecialRequest = txtSpecialRequest.Text.Trim() == "Please write here if you have a special request for this booking" ? "" : txtSpecialRequest.Text;
        }
        objBooking.SpecialRequest = txtSpecialRequest.Text.Trim() == "Please write here if you have a special request for this booking" ? "" : txtSpecialRequest.Text;
    }

    #region Radio Button for day2
    protected void rbtnSameAsDay1_CheckedChanged(object sender, EventArgs e)
    {
        if (rbtnSameAsDay1.Checked)
        {
            pnlMain.Visible = false;
        }
        if (IsConvertIntoRequest)
        {
            if (!rbtnBuildYourAccomodation.Checked)
            {
                divAcc.Style.Add("display", "none");
                IsBedroomAvailable = false;
            }
            else
            {
                divAcc.Style.Add("display", "block");
                IsBedroomAvailable = true;
            }
        }
    }
    protected void rbtnDiffFromDay1_CheckedChanged(object sender, EventArgs e)
    {
        if (rbtnDiffFromDay1.Checked)
        {
            pnlMain.Visible = true;
            //CheckSearchAvailable();
            if (IsConvertIntoRequest)
            {
                if (!rbtnBuildYourAccomodation.Checked)
                {
                    divAcc.Style.Add("display", "none");
                    IsBedroomAvailable = false;
                }
                else
                {
                    divAcc.Style.Add("display", "block");
                    IsBedroomAvailable = true;
                }
            }
            //if (Page.IsStartupScriptRegistered("a"))
            //{
            //    //Page.RegisterClientScriptBlock("a", "<script language='javascript'>jQuery(document).ready(function(){" + MyScript + "});</script>");
            //    //Page.RegisterStartupScript("abcd", "<script language='javascript'>jQuery(document).ready(function(){" + MyScript + "});</script>");
            //}
            //Page.RegisterStartupScript("abcd", "<script language='javascript'>jQuery(document).ready(function(){" + MyScript + "});</script>");
            ltrScript.Text = "<script language='javascript'>jQuery(document).ready(function(){" + MyScript + "});</script>";
            //ScriptManager.RegisterStartupScript(this, typeof(Page), "abcd", "jQuery(document).ready(function(){" + MyScript + "});", true);
        }
    }
    #endregion

    #region Radio button for accomodation
    //protected void rbtnSameCheckinCheckout_CheckedChanged(object sender, EventArgs e)
    //{
    //    if (rbtnSameCheckinCheckout.Checked)
    //    {
    //        SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
    //        objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
    //        objBooking.AccomodationDiffCheckinChecoutTime = true;
    //        objBooking.AccomodationDiffCheckinChecoutTime = false;
    //        BindAccomodation(objBooking.ManageAccomodationLst);
    //        bm.SaveSearch(st);
    //    }
    //}
    //protected void rbtnDiffCheckinCheckout_CheckedChanged(object sender, EventArgs e)
    //{
    //    if (rbtnDiffCheckinCheckout.Checked)
    //    {
    //        SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
    //        objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
    //        objBooking.AccomodationDiffCheckinChecoutTime = true;
    //        BindAccomodation(objBooking.ManageAccomodationLst);
    //        st.SearchObject = TrailManager.XmlSerialize(objBooking);
    //        bm.SaveSearch(st);
    //    }
    //}
    #endregion
    protected void lnkPrevious_Click(object sender, EventArgs e)
    {
        SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
        objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
        if (CurrentDay == 1)
        {
            st.CurrentStep = 1;
            st.CurrentDay = CurrentDay;
            st.SearchObject = TrailManager.XmlSerialize(objBooking);
            st.IsSentAsRequest = false;
            if (bm.SaveSearch(st))
            {
                Response.Redirect("~/Agency/BookingStep1.aspx");
            }
        }
        else
        {
            CurrentDay--;
            st.CurrentDay = CurrentDay;
            st.SearchObject = TrailManager.XmlSerialize(objBooking);
            if (bm.SaveSearch(st))
            {
                CheckSearchAvailable();
            }
            //Page.RegisterStartupScript("a", "<script language='javascript'>jQuery(document).ready(function(){" + MyScript + "});</script>");
            ltrScript.Text = "<script language='javascript'>jQuery(document).ready(function(){" + MyScript + "});</script>";
        }
    }

    #region Go for Calculation
    public void Calculate(Createbooking objCreateBook)
    {
        decimal TotalMeetingroomPrice = 0;
        decimal TotalPackagePrice = 0;
        decimal TotalBuildYourPackagePrice = 0;
        decimal TotalEquipmentPrice = 0;
        decimal TotalExtraPrice = 0;
        decimal TotalOthersPrice = 0;
        bool PackageSelected = false;
        VatCalculation = null;
        VatCalculation = new List<VatCollection>();
        objBooking.TotalBookingPrice = 0;
        hdnDay1Price.Value = "0";
        if (objCreateBook != null)
        {
            foreach (BookedMR objb in objCreateBook.MeetingroomList)
            {
                foreach (BookedMrConfig objconfig in objb.MrDetails)
                {
                    TotalMeetingroomPrice = objconfig.MeetingroomPrice;

                    TotalBuildYourPackagePrice = 0;
                    //Build mr
                    if (objconfig.BuildManageMRLst == null)
                    {
                        objconfig.BuildManageMRLst = new List<BuildYourMR>();
                    }
                    foreach (BuildYourMR bmr in objconfig.BuildManageMRLst)
                    {
                        TotalBuildYourPackagePrice += bmr.ItemPrice * bmr.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == bmr.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = bmr.vatpercent;
                            v.CalculatedPrice = bmr.ItemPrice * bmr.Quantity * bmr.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == bmr.vatpercent).FirstOrDefault().CalculatedPrice += bmr.ItemPrice * bmr.Quantity * bmr.vatpercent / 100;
                        }
                    }
                    PackageSelected = Convert.ToBoolean(objconfig.PackageID);
                    TotalEquipmentPrice = 0;
                    //Equipment
                    foreach (ManageEquipment eqp in objconfig.EquipmentLst)
                    {
                        TotalEquipmentPrice += eqp.ItemPrice * eqp.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = eqp.vatpercent;
                            v.CalculatedPrice = eqp.ItemPrice * eqp.Quantity * eqp.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault().CalculatedPrice += eqp.ItemPrice * eqp.Quantity * eqp.vatpercent / 100;
                        }
                    }
                    TotalOthersPrice = 0;
                    //Equipment
                    foreach (ManageOtherItems eqp in objconfig.BuildOthers)
                    {
                        TotalOthersPrice += eqp.ItemPrice * eqp.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = eqp.vatpercent;
                            v.CalculatedPrice = eqp.ItemPrice * eqp.Quantity * eqp.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == eqp.vatpercent).FirstOrDefault().CalculatedPrice += eqp.ItemPrice * eqp.Quantity * eqp.vatpercent / 100;
                        }
                    }
                    TotalExtraPrice = 0;
                    //Manage Extras
                    if (objconfig.ManageExtrasLst == null)
                    {
                        objconfig.ManageExtrasLst = new List<ManageExtras>();
                    }
                    foreach (ManageExtras ext in objconfig.ManageExtrasLst)
                    {
                        TotalExtraPrice += ext.ItemPrice * ext.Quantity;
                        if (VatCalculation.Where(a => a.VATPercent == ext.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = ext.vatpercent;
                            v.CalculatedPrice = ext.ItemPrice * ext.Quantity * ext.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == ext.vatpercent).FirstOrDefault().CalculatedPrice += ext.ItemPrice * ext.Quantity * ext.vatpercent / 100;
                        }
                    }
                    TotalPackagePrice = 0;
                    //Manage Package Item
                    if (objconfig.ManagePackageLst == null)
                    {
                        objconfig.ManagePackageLst = new List<ManagePackageItem>();
                    }
                    decimal restmeetingroomprice = 0;
                    decimal itemtotalprice = 0;
                    foreach (ManagePackageItem pck in objconfig.ManagePackageLst)
                    {
                        //TotalPackagePrice += pck.ItemPrice * pck.Quantity;
                        itemtotalprice += pck.ItemPrice;
                        if (VatCalculation.Where(a => a.VATPercent == pck.vatpercent).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = pck.vatpercent;
                            v.CalculatedPrice = pck.ItemPrice * pck.Quantity * pck.vatpercent / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == pck.vatpercent).FirstOrDefault().CalculatedPrice += pck.ItemPrice * pck.Quantity * pck.vatpercent / 100;
                        }
                    }
                    restmeetingroomprice = (objconfig.PackagePricePerPerson - itemtotalprice) * objconfig.NoOfParticepant;
                    if (PackageSelected)
                    {
                        if (VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = objconfig.MeetingroomVAT;
                            v.CalculatedPrice = restmeetingroomprice * objconfig.MeetingroomVAT / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault().CalculatedPrice += restmeetingroomprice * objconfig.MeetingroomVAT / 100;
                        }
                    }
                    else
                    {
                        if (VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault() == null)
                        {
                            VatCollection v = new VatCollection();
                            v.VATPercent = objconfig.MeetingroomVAT;
                            v.CalculatedPrice = objconfig.MeetingroomPrice * objconfig.MeetingroomVAT / 100;
                            VatCalculation.Add(v);
                        }
                        else
                        {
                            VatCalculation.Where(a => a.VATPercent == objconfig.MeetingroomVAT).FirstOrDefault().CalculatedPrice += objconfig.MeetingroomPrice * objconfig.MeetingroomVAT / 100;
                        }
                    }
                    objBooking.MeetingroomList.Where(a => a.MRId == objb.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == objconfig.SelectedDay).FirstOrDefault().BuildPackagePriceTotal = 0;
                    objBooking.MeetingroomList.Where(a => a.MRId == objb.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == objconfig.SelectedDay).FirstOrDefault().EquipmentPriceTotal = 0;
                    objBooking.MeetingroomList.Where(a => a.MRId == objb.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == objconfig.SelectedDay).FirstOrDefault().ExtraPriceTotal = 0;
                    objBooking.MeetingroomList.Where(a => a.MRId == objb.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == objconfig.SelectedDay).FirstOrDefault().PackagePriceTotal = 0;
                    objBooking.MeetingroomList.Where(a => a.MRId == objb.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == objconfig.SelectedDay).FirstOrDefault().OtherPriceTotal = 0;
                    if (PackageSelected)
                    {
                        TotalPackagePrice = objconfig.PackagePricePerPerson * objconfig.NoOfParticepant;
                        objBooking.MeetingroomList.Where(a => a.MRId == objb.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == objconfig.SelectedDay).FirstOrDefault().BuildPackagePriceTotal = TotalMeetingroomPrice;
                        objBooking.MeetingroomList.Where(a => a.MRId == objb.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == objconfig.SelectedDay).FirstOrDefault().EquipmentPriceTotal = TotalEquipmentPrice;
                        objBooking.MeetingroomList.Where(a => a.MRId == objb.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == objconfig.SelectedDay).FirstOrDefault().PackagePriceTotal = TotalPackagePrice;
                        objBooking.MeetingroomList.Where(a => a.MRId == objb.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == objconfig.SelectedDay).FirstOrDefault().ExtraPriceTotal = TotalExtraPrice;
                        objBooking.MeetingroomList.Where(a => a.MRId == objb.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == objconfig.SelectedDay).FirstOrDefault().OtherPriceTotal = TotalOthersPrice;
                        objBooking.TotalBookingPrice += TotalPackagePrice + TotalExtraPrice + TotalEquipmentPrice + TotalOthersPrice;
                        if (objconfig.SelectedDay == 1)
                        {
                            hdnDay1Price.Value = (Convert.ToDecimal(hdnDay1Price.Value) + TotalPackagePrice + TotalExtraPrice + TotalEquipmentPrice + TotalOthersPrice).ToString();
                        }
                    }
                    else if (objconfig.IsBreakdown)
                    {
                        objBooking.MeetingroomList.Where(a => a.MRId == objb.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == objconfig.SelectedDay).FirstOrDefault().BuildPackagePriceTotal = TotalMeetingroomPrice;
                        objBooking.MeetingroomList.Where(a => a.MRId == objb.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == objconfig.SelectedDay).FirstOrDefault().EquipmentPriceTotal = 0;
                        objBooking.MeetingroomList.Where(a => a.MRId == objb.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == objconfig.SelectedDay).FirstOrDefault().PackagePriceTotal = 0;
                        objBooking.MeetingroomList.Where(a => a.MRId == objb.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == objconfig.SelectedDay).FirstOrDefault().ExtraPriceTotal = 0;
                        objBooking.MeetingroomList.Where(a => a.MRId == objb.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == objconfig.SelectedDay).FirstOrDefault().OtherPriceTotal = 0;
                        objBooking.TotalBookingPrice += TotalMeetingroomPrice;
                        if (objconfig.SelectedDay == 1)
                        {
                            hdnDay1Price.Value = (Convert.ToDecimal(hdnDay1Price.Value) + TotalMeetingroomPrice).ToString();
                        }
                    }
                    else
                    {
                        objBooking.MeetingroomList.Where(a => a.MRId == objb.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == objconfig.SelectedDay).FirstOrDefault().BuildPackagePriceTotal = TotalMeetingroomPrice + TotalBuildYourPackagePrice;
                        objBooking.MeetingroomList.Where(a => a.MRId == objb.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == objconfig.SelectedDay).FirstOrDefault().EquipmentPriceTotal = TotalEquipmentPrice;
                        objBooking.MeetingroomList.Where(a => a.MRId == objb.MRId).FirstOrDefault().MrDetails.Where(a => a.SelectedDay == objconfig.SelectedDay).FirstOrDefault().OtherPriceTotal = TotalOthersPrice;
                        objBooking.TotalBookingPrice += TotalMeetingroomPrice + TotalEquipmentPrice + TotalBuildYourPackagePrice + TotalOthersPrice;
                        if (objconfig.SelectedDay == 1)
                        {
                            hdnDay1Price.Value = (Convert.ToDecimal(hdnDay1Price.Value) + TotalMeetingroomPrice + TotalEquipmentPrice + TotalBuildYourPackagePrice + TotalOthersPrice).ToString();
                        }
                    }

                }
            }
            decimal TotalAccomodationPrice = 0;
            if (!objBooking.NoAccomodation)
            {
                foreach (Accomodation acm in objBooking.ManageAccomodationLst)
                {
                    TotalAccomodationPrice += acm.QuantityDouble * acm.RoomPriceDouble + acm.QuantitySingle * acm.RoomPriceSingle;
                }
            }
            objBooking.AccomodationPriceTotal = TotalAccomodationPrice;
            objBooking.TotalBookingPrice += TotalAccomodationPrice;
        }
    }
    #endregion

    #region Step of Time
    private string[] arrTime = { "00:00 am", "00:15 am", "00:30 am", "00:45 am", "01:00 am", "01:15 am", "01:30 am", "01:45 am", "02:00 am", "02:15 am", "02:30 am", "02:45 am", "03:00 am", "03:15 am", "03:30 am", "03:45 am", "04:00 am", "04:15 am", "04:30 am", "04:45 am", "05:00 am", "05:15 am", "05:30 am", "05:45 am", "06:00 am", "06:15 am", "06:30 am", "06:45 am", "07:00 am", "07:15 am", "07:30 am", "07:45 am", "08:00 am", "08:15 am", "08:30 am", "08:45 am", "09:00 am", "09:15 am", "09:30 am", "09:45 am", "10:00 am", "10:15 am", "10:30 am", "10:45 am", "11:00 am", "11:15 am", "11:30 am", "11:45 am", "12:00 pm", "12:15 pm", "12:30 pm", "12:45 pm", "01:00 pm", "01:15 pm", "01:30 pm", "01:45 pm", "02:00 pm", "02:15 pm", "02:30 pm", "02:45 pm", "03:00 pm", "03:15 pm", "03:30 pm", "03:45 pm", "04:00 pm", "04:15 pm", "04:30 pm", "04:45 pm", "05:00 pm", "05:15 pm", "05:30 pm", "05:45 pm", "06:00 pm", "06:15 pm", "06:30 pm", "06:45 pm", "07:00 pm", "07:15 pm", "07:30 pm", "07:45 pm", "08:00 pm", "08:15 pm", "08:30 pm", "08:45 pm", "09:00 pm", "09:15 pm", "09:30 pm", "09:45 pm", "10:00 pm", "10:15 pm", "10:30 pm", "10:45 pm", "11:00 pm", "11:15 pm", "11:30 pm", "11:45 pm", "12:00 pm", "12:15 pm", "12:30 pm", "12:45 pm" };
    public void BindDropdown(DropDownList drplst, string start, string end)
    {
        drplst.Items.Clear();
        bool bstart = false;
        bool bend = false;
        for (int i = 0; i < arrTime.Length; i++)
        {
            if (arrTime[i] == start)
            {
                drplst.Items.Add(new ListItem(arrTime[i], (i + 1).ToString()));
                bstart = true;
            }
            else if (arrTime[i] == end)
            {
                drplst.Items.Add(new ListItem(arrTime[i], (i + 1).ToString()));
                bend = true;
            }
            else if (bstart && !bend)
            {
                drplst.Items.Add(new ListItem(arrTime[i], (i + 1).ToString()));
            }
            else if (bstart && bend)
            {
                break;
            }
        }
    }
    #endregion

    protected void lnkConvertAsRequest_OnClick(object sender, EventArgs e)
    {
        SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
        objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
        //Get The Meeting Room Details
        if ((st.IsSentAsRequest == null ? false : st.IsSentAsRequest) == true)
        {
            IsConvertIntoRequest = true;
            divWarnning.InnerHtml = GetKeyResult("YOURBOOKINGISCONVERTINTOREQUEST");//"Your booking is now converted into request.";
            divWarnning.Attributes.Add("style", "display:block;");
        }
        else
        {
            IsConvertIntoRequest = false;
        }
        if (st.CurrentDay == 2)
        {
            if (rbtnSameAsDay1.Checked)
            {
                objBooking.SecondMRNotSameAsRoom1 = false;
                ManageSameAsDay1(objBooking);
            }
            else
            {
                objBooking.SecondMRNotSameAsRoom1 = true;
                foreach (RepeaterItem mr in rptMeetingroom.Items)
                {
                    RadioButton rbtnNeedToConfigure = (RadioButton)mr.FindControl("rbtnNeedToConfigure");
                    HiddenField hdnMeetingRoomID = (HiddenField)mr.FindControl("hdnMeetingRoomID");
                    //End Controls
                    if (rbtnNeedToConfigure == null)
                    {
                        ManageMeetingRoomDetails(mr, Convert.ToInt64(hdnMeetingRoomID.Value));
                    }
                    else
                    {
                        if (rbtnNeedToConfigure.Checked)
                        {
                            ManageMeetingRoomDetails(mr, Convert.ToInt64(hdnMeetingRoomID.Value));
                        }
                        else
                        {
                            BookedMrConfig objFirstMR = objBooking.MeetingroomList[0].MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault();
                            objBooking.MeetingroomList[1].MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().IsBreakdown = true;
                        }
                    }
                }
            }
        }
        else
        {
            foreach (RepeaterItem mr in rptMeetingroom.Items)
            {
                RadioButton rbtnNeedToConfigure = (RadioButton)mr.FindControl("rbtnNeedToConfigure");
                HiddenField hdnMeetingRoomID = (HiddenField)mr.FindControl("hdnMeetingRoomID");
                //End Controls
                if (rbtnNeedToConfigure == null)
                {
                    ManageMeetingRoomDetails(mr, Convert.ToInt64(hdnMeetingRoomID.Value));
                }
                else
                {
                    if (rbtnNeedToConfigure.Checked)
                    {
                        ManageMeetingRoomDetails(mr, Convert.ToInt64(hdnMeetingRoomID.Value));
                    }
                    else
                    {
                        BookedMrConfig objFirstMR = objBooking.MeetingroomList[0].MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault();
                        objBooking.MeetingroomList[1].MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().IsBreakdown = true;
                    }
                }
            }
            hdnDay1Price.Value = (Convert.ToDecimal(hdnDay1Price.Value) * CurrencyConvert).ToString("0.00");
        }
        if (objBooking.Duration == CurrentDay)
        {
            //End Meetingroom Details
            if (rbtnBuildYourAccomodation.Checked)
            {
                objBooking.NoAccomodation = false;
            }
            else
            {
                objBooking.NoAccomodation = true;
            }
            ManageAccomodation();
            objBooking.TotalBookingPrice += objBooking.AccomodationPriceTotal;
        }
        if (CurrentDay < objBooking.Duration)
        {
            //Session["Search"] = objBooking;
            objBooking.NoAccomodation = false;
            st.SearchObject = TrailManager.XmlSerialize(objBooking);
            CurrentDay++;
            //st.CurrentDay = CurrentDay;
            st.IsSentAsRequest = true;
            if (Request.QueryString["wl"] != null)
            {
                st.ChannelBy = "wl";
                st.ChannelId = Convert.ToString(Request.QueryString["wl"]);
                objBooking.BookType = "wl";
                objBooking.ChannelID = Convert.ToString(Request.QueryString["wl"]);
            }
            if (bm.SaveSearch(st))
            {

                CheckSearchAvailable();
                Response.Redirect("~/Agency/BookingStep2.aspx");
                //BindBooking();
                //hdnFinalPrice.Value = (objBooking.TotalBookingPrice - Convert.ToDecimal(hdnMeetingroom1Price.Value) - (IsSecondMeetingRoom?Convert.ToDecimal(hdnMeetingroom2Price.Value):0)).ToString();
            }
            if (!objBooking.SecondMRNotSameAsRoom1)
            {
                //Page.RegisterStartupScript("a", "<script language='javascript'>jQuery(document).ready(function(){" + MyScript + "});</script>");
                ltrScript.Text = "<script language='javascript'>jQuery(document).ready(function(){" + MyScript + "});</script>";
            }
        }
        else
        {
            Calculate(objBooking);
            objBooking.NoAccomodation = false;
            st.SearchObject = TrailManager.XmlSerialize(objBooking);
            //Session["Search"] = objBooking;
            //st.CurrentDay = 1;
            st.CurrentStep = 2;
            st.IsSentAsRequest = true;
            if (Request.QueryString["wl"] != null)
            {
                st.ChannelBy = "wl";
                st.ChannelId = Convert.ToString(Request.QueryString["wl"]);
                objBooking.BookType = "wl";
                objBooking.ChannelID = Convert.ToString(Request.QueryString["wl"]);
            }
            if (bm.SaveSearch(st))
            {
                if (Request.QueryString["wl"] == null)
                {
                    //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                    //if (l != null)
                    //{
                    //    Response.Redirect(SiteRootPath + "booking-step2/" + l.Name.ToLower());
                    //}
                    //else
                    //{
                    Response.Redirect("~/Agency/BookingStep2.aspx");
                    //}
                    //Response.Redirect("Bookingstep3.aspx", false);
                }
                else
                {
                    Response.Redirect("~/Agency/BookingStep2.aspx?" + Request.RawUrl.Split('?')[1]);
                }
            }
        }
        //SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
        //objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
        //for (int i = 0; i < rptAccomodationDaysManager.Items.Count; i++)
        //{
        //    if (rptAccomodationDaysManager.Items[i].ItemType == ListItemType.AlternatingItem || rptAccomodationDaysManager.Items[i].ItemType == ListItemType.Item)
        //    {
        //        HiddenField hdnBedroomId = (HiddenField)rptAccomodationDaysManager.Items[i].FindControl("hdnBedroomId");
        //        Repeater rptSingleQuantity = (Repeater)rptAccomodationDaysManager.Items[i].FindControl("rptSingleQuantity");
        //        Repeater rptDoubleQuantity = (Repeater)rptAccomodationDaysManager.Items[i].FindControl("rptDoubleQuantity");
        //        for (int j = 0; j < rptSingleQuantity.Items.Count; j++)
        //        {
        //            if (rptSingleQuantity.Items[j].ItemType == ListItemType.AlternatingItem || rptSingleQuantity.Items[j].ItemType == ListItemType.Item)
        //            {
        //                TextBox txtQuantitySDay = (TextBox)rptSingleQuantity.Items[j].FindControl("txtQuantitySDay");
        //                HiddenField hdnDate = (HiddenField)rptSingleQuantity.Items[j].FindControl("hdnDate");
        //                if (objBooking.ManageAccomodationLst.Where(a => a.BedroomId == Convert.ToInt64(hdnBedroomId.Value) && a.DateRequest == Convert.ToDateTime(hdnDate.Value)).FirstOrDefault() != null)
        //                {
        //                    objBooking.ManageAccomodationLst.Where(a => a.BedroomId == Convert.ToInt64(hdnBedroomId.Value) && a.DateRequest == Convert.ToDateTime(hdnDate.Value)).FirstOrDefault().QuantitySingle = Convert.ToInt32(string.IsNullOrEmpty(txtQuantitySDay.Text) ? "0" : txtQuantitySDay.Text);
        //                }
        //            }
        //        }
        //        for (int j = 0; j < rptDoubleQuantity.Items.Count; j++)
        //        {
        //            if (rptDoubleQuantity.Items[j].ItemType == ListItemType.AlternatingItem || rptDoubleQuantity.Items[j].ItemType == ListItemType.Item)
        //            {
        //                TextBox txtQuantityDDay = (TextBox)rptDoubleQuantity.Items[j].FindControl("txtQuantityDDay");
        //                HiddenField hdnDate = (HiddenField)rptDoubleQuantity.Items[j].FindControl("hdnDate");
        //                if (objBooking.ManageAccomodationLst.Where(a => a.BedroomId == Convert.ToInt64(hdnBedroomId.Value) && a.DateRequest == Convert.ToDateTime(hdnDate.Value)).FirstOrDefault() != null)
        //                {
        //                    objBooking.ManageAccomodationLst.Where(a => a.BedroomId == Convert.ToInt64(hdnBedroomId.Value) && a.DateRequest == Convert.ToDateTime(hdnDate.Value)).FirstOrDefault().QuantityDouble = Convert.ToInt32(string.IsNullOrEmpty(txtQuantityDDay.Text) ? "0" : txtQuantityDDay.Text);
        //                }
        //            }
        //        }
        //    }
        //}
        //objBooking.NoAccomodation = false;
        //st.SearchObject = TrailManager.XmlSerialize(objBooking);
        //st.IsSentAsRequest = true;
        //if (bm.SaveSearch(st))
        //{
        //    Response.Redirect(Request.RawUrl);
        //}
    }

    protected void lnkConvertBooking_OnClick(object sender, EventArgs e)
    {
        SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
        objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
        //Get The Meeting Room Details
        if ((st.IsSentAsRequest == null ? false : st.IsSentAsRequest) == true)
        {
            IsConvertIntoRequest = true;
            divWarnning.InnerHtml = GetKeyResult("YOURBOOKINGISCONVERTINTOREQUEST");//"Your booking is now converted into request.";
            divWarnning.Attributes.Add("style", "display:block;");
        }
        else
        {
            IsConvertIntoRequest = false;
        }
        if (st.CurrentDay == 2)
        {
            if (rbtnSameAsDay1.Checked)
            {
                objBooking.SecondMRNotSameAsRoom1 = false;
                ManageSameAsDay1(objBooking);
            }
            else
            {
                objBooking.SecondMRNotSameAsRoom1 = true;
                foreach (RepeaterItem mr in rptMeetingroom.Items)
                {
                    RadioButton rbtnNeedToConfigure = (RadioButton)mr.FindControl("rbtnNeedToConfigure");
                    HiddenField hdnMeetingRoomID = (HiddenField)mr.FindControl("hdnMeetingRoomID");
                    //End Controls
                    if (rbtnNeedToConfigure == null)
                    {
                        ManageMeetingRoomDetails(mr, Convert.ToInt64(hdnMeetingRoomID.Value));
                    }
                    else
                    {
                        if (rbtnNeedToConfigure.Checked)
                        {
                            ManageMeetingRoomDetails(mr, Convert.ToInt64(hdnMeetingRoomID.Value));
                        }
                        else
                        {
                            BookedMrConfig objFirstMR = objBooking.MeetingroomList[0].MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault();
                            objBooking.MeetingroomList[1].MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().IsBreakdown = true;
                        }
                    }
                }
            }
        }
        else
        {
            foreach (RepeaterItem mr in rptMeetingroom.Items)
            {
                RadioButton rbtnNeedToConfigure = (RadioButton)mr.FindControl("rbtnNeedToConfigure");
                HiddenField hdnMeetingRoomID = (HiddenField)mr.FindControl("hdnMeetingRoomID");
                //End Controls
                if (rbtnNeedToConfigure == null)
                {
                    ManageMeetingRoomDetails(mr, Convert.ToInt64(hdnMeetingRoomID.Value));
                }
                else
                {
                    if (rbtnNeedToConfigure.Checked)
                    {
                        ManageMeetingRoomDetails(mr, Convert.ToInt64(hdnMeetingRoomID.Value));
                    }
                    else
                    {
                        BookedMrConfig objFirstMR = objBooking.MeetingroomList[0].MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault();
                        objBooking.MeetingroomList[1].MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().IsBreakdown = true;
                    }
                }
            }
            hdnDay1Price.Value = (Convert.ToDecimal(hdnDay1Price.Value) / CurrencyConvert).ToString("0.00");
        }
        if (objBooking.Duration == CurrentDay)
        {
            //End Meetingroom Details
            if (rbtnBuildYourAccomodation.Checked)
            {
                objBooking.NoAccomodation = false;
            }
            else
            {
                objBooking.NoAccomodation = true;
            }
            ManageAccomodation();
            objBooking.TotalBookingPrice += objBooking.AccomodationPriceTotal;
        }
        if (CurrentDay < objBooking.Duration)
        {
            //Session["Search"] = objBooking;
            objBooking.NoAccomodation = false;
            st.SearchObject = TrailManager.XmlSerialize(objBooking);
            CurrentDay++;
            //st.CurrentDay = CurrentDay;
            st.IsSentAsRequest = false;
            if (Request.QueryString["wl"] != null)
            {
                st.ChannelBy = "wl";
                st.ChannelId = Convert.ToString(Request.QueryString["wl"]);
                objBooking.BookType = "wl";
                objBooking.ChannelID = Convert.ToString(Request.QueryString["wl"]);
            }
            if (bm.SaveSearch(st))
            {

                CheckSearchAvailable();
                Response.Redirect("~/Agency/BookingStep2.aspx");
                //BindBooking();
                //hdnFinalPrice.Value = (objBooking.TotalBookingPrice - Convert.ToDecimal(hdnMeetingroom1Price.Value) - (IsSecondMeetingRoom?Convert.ToDecimal(hdnMeetingroom2Price.Value):0)).ToString();
            }
            if (!objBooking.SecondMRNotSameAsRoom1)
            {
                //Page.RegisterStartupScript("a", "<script language='javascript'>jQuery(document).ready(function(){" + MyScript + "});</script>");
                ltrScript.Text = "<script language='javascript'>jQuery(document).ready(function(){" + MyScript + "});</script>";
            }
        }
        else
        {
            Calculate(objBooking);
            objBooking.NoAccomodation = false;
            st.SearchObject = TrailManager.XmlSerialize(objBooking);
            //Session["Search"] = objBooking;
            //st.CurrentDay = 1;
            st.CurrentStep = 2;
            st.IsSentAsRequest = false;
            if (Request.QueryString["wl"] != null)
            {
                st.ChannelBy = "wl";
                st.ChannelId = Convert.ToString(Request.QueryString["wl"]);
                objBooking.BookType = "wl";
                objBooking.ChannelID = Convert.ToString(Request.QueryString["wl"]);
            }
            if (bm.SaveSearch(st))
            {
                if (Request.QueryString["wl"] == null)
                {
                    //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                    //if (l != null)
                    //{
                    //    Response.Redirect(SiteRootPath + "booking-step2/" + l.Name.ToLower());
                    //}
                    //else
                    //{
                    Response.Redirect("~/Agency/BookingStep2.aspx");
                    //}
                    //Response.Redirect("Bookingstep3.aspx", false);
                }
                else
                {
                    Response.Redirect("~/Agency/BookingStep2.aspx?" + Request.RawUrl.Split('?')[1]);
                }
            }
        }
    }

    private void Master_ButtonClick(object sender, EventArgs e)
    {
        // This Method will be Called.
        MasterNextProcess();
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        // Create an event handler for the master page's contentCallEvent event
        this.Master.contentCallEvent += new EventHandler(Master_ButtonClick);
    }

    protected void MasterNextProcess()
    {
        SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
        objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
        //Get The Meeting Room Details
        if (st.CurrentDay == 2)
        {
            if (rbtnSameAsDay1.Checked)
            {
                objBooking.SecondMRNotSameAsRoom1 = false;
                ManageSameAsDay1(objBooking);
            }
            else
            {
                objBooking.SecondMRNotSameAsRoom1 = true;
                foreach (RepeaterItem mr in rptMeetingroom.Items)
                {
                    RadioButton rbtnNeedToConfigure = (RadioButton)mr.FindControl("rbtnNeedToConfigure");
                    HiddenField hdnMeetingRoomID = (HiddenField)mr.FindControl("hdnMeetingRoomID");
                    //End Controls
                    if (rbtnNeedToConfigure == null)
                    {
                        ManageMeetingRoomDetails(mr, Convert.ToInt64(hdnMeetingRoomID.Value));
                    }
                    else
                    {
                        if (rbtnNeedToConfigure.Checked)
                        {
                            ManageMeetingRoomDetails(mr, Convert.ToInt64(hdnMeetingRoomID.Value));
                        }
                        else
                        {
                            BookedMrConfig objFirstMR = objBooking.MeetingroomList[0].MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault();
                            objBooking.MeetingroomList[1].MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().IsBreakdown = true;
                        }
                    }
                }
            }
        }
        else
        {
            foreach (RepeaterItem mr in rptMeetingroom.Items)
            {
                RadioButton rbtnNeedToConfigure = (RadioButton)mr.FindControl("rbtnNeedToConfigure");
                HiddenField hdnMeetingRoomID = (HiddenField)mr.FindControl("hdnMeetingRoomID");
                //End Controls
                if (rbtnNeedToConfigure == null)
                {
                    ManageMeetingRoomDetails(mr, Convert.ToInt64(hdnMeetingRoomID.Value));
                }
                else
                {
                    if (rbtnNeedToConfigure.Checked)
                    {
                        ManageMeetingRoomDetails(mr, Convert.ToInt64(hdnMeetingRoomID.Value));
                    }
                    else
                    {
                        BookedMrConfig objFirstMR = objBooking.MeetingroomList[0].MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault();
                        objBooking.MeetingroomList[1].MrDetails.Where(a => a.SelectedDay == CurrentDay).FirstOrDefault().IsBreakdown = true;
                    }
                }
            }
            hdnDay1Price.Value = (Convert.ToDecimal(hdnDay1Price.Value) * CurrencyConvert).ToString();
        }
        if (objBooking.Duration == CurrentDay)
        {
            //End Meetingroom Details
            if (rbtnBuildYourAccomodation.Checked)
            {
                objBooking.NoAccomodation = false;
            }
            else
            {
                objBooking.NoAccomodation = true;
            }
            ManageAccomodation();
            objBooking.TotalBookingPrice += objBooking.AccomodationPriceTotal;
        }
        if (CurrentDay < objBooking.Duration)
        {
            //Session["Search"] = objBooking;
            st.SearchObject = TrailManager.XmlSerialize(objBooking);
            CurrentDay++;
            //st.CurrentDay = CurrentDay;
            if (bm.SaveSearch(st))
            {

                CheckSearchAvailable();
                //BindBooking();
                //hdnFinalPrice.Value = (objBooking.TotalBookingPrice - Convert.ToDecimal(hdnMeetingroom1Price.Value) - (IsSecondMeetingRoom?Convert.ToDecimal(hdnMeetingroom2Price.Value):0)).ToString();
            }
            //Page.RegisterStartupScript("a", "<script language='javascript'>jQuery(document).ready(function(){" + MyScript + "});</script>");
            ltrScript.Text = "<script language='javascript'>jQuery(document).ready(function(){" + MyScript + "});</script>";
        }
        else
        {
            Calculate(objBooking);
            st.SearchObject = TrailManager.XmlSerialize(objBooking);
            //Session["Search"] = objBooking;
            //st.CurrentDay = 1;
            //st.CurrentStep = 3;
            if (bm.SaveSearch(st))
            {
                //Response.Redirect("~/Agency/BookingStep3.aspx");
            }
        }
    }
}

class DistinctItemComparer : IEqualityComparer<BedroomManage>
{

    public bool Equals(BedroomManage x, BedroomManage y)
    {
        return x.BedroomID == y.BedroomID &&
            x.bedroomType == y.bedroomType;
    }

    public int GetHashCode(BedroomManage obj)
    {
        return obj.BedroomID.GetHashCode() ^
            obj.bedroomType.GetHashCode();
    }
}