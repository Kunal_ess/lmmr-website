﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Agency/AgencyMaster.master" AutoEventWireup="true"
    CodeFile="ControlPanel.aspx.cs" Inherits="Agency_ControlPanel" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ctnTop" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ctnMain" runat="Server">
    <table width="100%" border="0" cellspacing="0">
        <tr>
            <td>
                <table width="100%;" style="background: #ffffff; border: 1px #A3D4F7 solid;">
                    <tr>
                        <td>
                            <h3 style="font-size: 21px; font-weight: bold; margin: 0px; padding: 0px 0px 0px 0px;">
                                Control Panel
                            </h3>
                        </td>
                        <td align="right">
                            <div style="float: right;" id="pageing">
                            </div>
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="grdViewControlPanel" runat="server" Width="100%" CellPadding="8"
                    border="0" HeaderStyle-CssClass="heading-row" RowStyle-CssClass="con" AlternatingRowStyle-CssClass="con-dark"
                    AutoGenerateColumns="False" GridLines="None" CellSpacing="1" BackColor="#A3D4F7"
                    DataKeyNames="UserId" AllowPaging="true" PageSize="5" OnPageIndexChanging="grdViewControlPanel_PageIndexChanging"
                    OnRowDataBound="grdViewControlPanel_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="UserID" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblUserId" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblUserId" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblUser" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblUser" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Today Booking" ItemStyle-Width="12%" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblTotalBooking" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblTotalBooking" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Current Month" ItemStyle-Width="12%" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblMonthBooking" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblMonthBooking" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Current Year" ItemStyle-Width="11%" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblYearBooking" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblYearBooking" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Today Request" ItemStyle-Width="12%" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblTotalRequest" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblTotalRequest" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Current Month" ItemStyle-Width="12%" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblMonthRequest" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblMonthRequest" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Current Year" ItemStyle-Width="11%" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblYearRequest" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblYearRequest" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <tr>
                            <td colspan="4" align="center">
                                <b>No record found</b>
                            </td>
                        </tr>
                    </EmptyDataTemplate>
                    <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                    <PagerStyle HorizontalAlign="Right" BackColor="White" CssClass="displayNone" />
                    <PagerTemplate>
                        <div id="Paging" style="width: 100%; display: none;">
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                        </div>
                    </PagerTemplate>
                </asp:GridView>
            </td>
        </tr>
        <tr>
        <td>
        <table width="100%">
        <tr>
        <td width="50%"><h4>Graph for Booking </h4></td>
        <td width="50%"><h4>Graph for Request </h4></td>
        </tr>
        <tr>

        <td>

        <div id="bookinggraph" align="center">
<asp:Chart ID="chrtBooking" runat="server">
                        <%--<Series>
                            <asp:Series Name="SeriesBooking" ChartArea="ChartAreaBooking" ChartType="Line">
                            </asp:Series>
                        </Series>--%>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartAreaBooking">
                            <AxisX>
                            <MajorGrid Enabled="false" />
                            </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>

</div>
        
        </td>
        <td>
        <div id="reqgraph" align="center">
               <asp:Chart ID="chrtReq" runat="server">
                       <%-- <Series>
                            <asp:Series Name="SeriesBooking" ChartArea="ChartAreaBooking" ChartType="Line">
                            </asp:Series>
                        </Series>--%>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartAreaBooking">
                            <AxisX>
                            <MajorGrid Enabled="false" />
                            </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>


                  
                    </div> </td>
        </tr>
        </table>
        </td>
        </tr>
    </table>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            if (jQuery("#Paging") != undefined) {
                var inner = jQuery("#Paging").html();
                jQuery("#pageing").html(inner);
                jQuery("#Paging").html("");
            }

        });
    </script>
</asp:Content>
