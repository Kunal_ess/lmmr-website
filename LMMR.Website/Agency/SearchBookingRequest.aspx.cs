﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Entities;
using LMMR.Business;
public partial class Agency_SearchBookingRequest : BasePage
{
    public UserControl_Agency_LeftSearchPanel u
    {
        get
        {
            return Page.Master.FindControl("cntLeftSearch").FindControl("LeftSearchPanel1") as UserControl_Agency_LeftSearchPanel;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentAgencyUser"] == null || Session["CurrentAgencyUserID"] == null)
        {
            Response.Redirect(SiteRootPath + "login/english");
        }        
        u.SearchButton += new EventHandler(u_SearchButton);
        u.MapSearch += new EventHandler(u_MapSearch);
        if (!Page.IsPostBack)
        {
            SearchPanel1.Visible = false;            
            AgentClient1.Visible = true;
            //u.usercontroltype = "Map";
            if (Session["ComeFromBookingAndRequestPage"] != null)
            {
                SearchPanel1.Visible = true;
                AgentClient1.Visible = false; 
            }
        }        
    }

    void u_SearchButton(object sender, EventArgs e)
    {
        Session["IsSearchStart"] = true;
        HiddenField dn = AgentClient1.FindControl("hdnUserId") as HiddenField;
        if (dn.Value == "0")
        {
            Session["CurrentUserSeleted"] = Convert.ToString(Session["CurrentAgencyUserID"]);
        }
        else
        {
            //But in this condition you need to assign the client id bcoz booking will be done as per this id.
            Session["CurrentUserSeleted"] = dn.Value;
        }
        ManageSession();
        u.CheckNexCondition();
        u.GetCityHotel();
        SearchPanel1.BookingDataSource = u.BookingDataSource;
        SearchPanel1.RequestDataSource = u.RequestDataSource;
        SearchPanel1.RequestDataSourceAll = u.RequestDataSourceAll;
        u.GetCenterPoint(Convert.ToInt32(u.propCity));
        SearchPanel1.BindSearchResult();
        SearchPanel1.fillfilter();
        SearchPanel1.Visible = true;
        AgentClient1.Visible = false;        
        //ltrResult.Text = "Country: " + u.propCountryID + " <br/>City: " + u.propCity + " <br/>Date: " + u.propDate + "<br/>Duration: " + u.propDuration + "<br/>Day1: " + u.propDay1;
    }
    void u_MapSearch(object sender, EventArgs e)
    {
        Session["IsSearchStart"] = true;
        HiddenField dn = AgentClient1.FindControl("hdnUserId") as HiddenField;
        if (dn.Value == "0")
        {
            //Gaurav in this condition no need to give the user id
            Session["CurrentUserSeleted"] = Convert.ToString(Session["CurrentAgencyUserID"]);
        }
        else
        {
            //But in this condition you need to assign the client id bcoz booking will be done as per this id.
            Session["CurrentUserSeleted"] = dn.Value;
        }

        ManageSession();
        u.CheckNexCondition();
        u.GetCityHotel();
        SearchPanel1.BookingDataSource = u.BookingDataSource;
        SearchPanel1.RequestDataSource = u.RequestDataSource;
        SearchPanel1.RequestDataSourceAll = u.RequestDataSourceAll;
        u.GetCenterPoint(Convert.ToInt32(u.propCity));
        SearchPanel1.BindSearchResult();
        SearchPanel1.fillfilter();
        SearchPanel1.Visible = true;
        AgentClient1.Visible = false;
    }
    BookingRequest objBookingRequest = null;
    void ManageSession()
    {
        string WhereClause = string.Empty;
        string WhereClauseday2 = string.Empty;
        if (u.propCountryID != "0")
        {
            if (u.propCountryID != "")
            {
                if (WhereClause.Length > 0)
                {
                    WhereClause += " and ";
                    WhereClauseday2 += " and ";
                }
                WhereClause += HotelColumn.CountryId + "=" + u.propCountryID;
                WhereClauseday2 += HotelColumn.CountryId + "=" + u.propCountryID;
            }
        }

        if (Convert.ToInt32(u.propCity) != 0)
        {
            if (u.propCity != "")
            {
                if (WhereClause.Length > 0)
                {
                    WhereClause += " and ";
                    WhereClauseday2 += " and ";
                }
                WhereClause += HotelColumn.CityId + "=" + u.propCity;
                WhereClauseday2 += HotelColumn.CityId + "=" + u.propCity;
            }
        }
        if (u.propDuration == "1")
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                if (u.propDay1 == "0")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (u.propDay1 == "1")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (u.propDay1 == "2")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
            }
        }
        if (u.propDuration == "2")
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                WhereClauseday2 += " and ";
                if (u.propDay1 == "0")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (u.propDay1 == "1")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (u.propDay1 == "2")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }


                if (u.propDay2 == "0")
                {
                    WhereClauseday2 += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (u.propDay2 == "1")
                {
                    WhereClauseday2 += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (u.propDay2 == "2")
                {
                    WhereClauseday2 += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
            }
        }

        if (u.propDate != "" && u.propDate != "dd/mm/yy")
        {
            DateTime fromDate = new DateTime(Convert.ToInt32("20" + u.propDate.Split('/')[2]), Convert.ToInt32(u.propDate.Split('/')[1]), Convert.ToInt32(u.propDate.Split('/')[0]));

            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                WhereClauseday2 += " and ";
            }
            WhereClause += AvailabilityColumn.AvailabilityDate + "='" + fromDate + "'" + " and " + " (DATEDIFF(day, GETDATE(),'" + fromDate + "')) >= " + AvailabilityColumn.LeadTimeForMeetingRoom + "";
            WhereClauseday2 += AvailabilityColumn.AvailabilityDate + "='" + fromDate.AddDays(1) + "'" + " and " + " (DATEDIFF(day, GETDATE(),'" + fromDate.AddDays(1) + "')) >= " + AvailabilityColumn.LeadTimeForMeetingRoom + "";
        }
        else
        {
            DateTime fromDate = DateTime.Now;
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                WhereClauseday2 += " and ";
            }
            WhereClause += AvailabilityColumn.AvailabilityDate + "='" + fromDate + "'" + " and " + " (DATEDIFF(day, GETDATE(),'" + fromDate + "')) >= " + AvailabilityColumn.LeadTimeForMeetingRoom + "";
            WhereClauseday2 += AvailabilityColumn.AvailabilityDate + "='" + fromDate.AddDays(1) + "'" + " and " + " (DATEDIFF(day, GETDATE(),'" + fromDate.AddDays(1) + "')) >= " + AvailabilityColumn.LeadTimeForMeetingRoom + "";
        }


        if (u.propParticipant != "" && u.propParticipant != "0")
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                WhereClauseday2 += " and ";
            }
            WhereClause += u.propParticipant + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + "";
            WhereClauseday2 += u.propParticipant + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + "";
        }

        //Send all search session value
        Session["Where"] = WhereClause;
        if (u.propDuration == "2")
        {
            Session["Where2"] = WhereClauseday2;
        }

        //Maintain session value for all controls 
        objBookingRequest = new BookingRequest();
        objBookingRequest.propCountry = u.propCountryID;
        objBookingRequest.propCity = u.propCity;
        objBookingRequest.propDate = Convert.ToString(string.IsNullOrEmpty(u.propDate)?(u.propDate=="dd/mm/yy"?DateTime.Now.ToString("dd/MM/yy"):u.propDate):u.propDate);
        objBookingRequest.propDuration = u.propDuration;
        objBookingRequest.propDays = u.propDay1;
        objBookingRequest.propDay2 = u.propDay2;
        if (u.propParticipant != "")
        {
            objBookingRequest.propParticipants = u.propParticipant;
        }
        else
        {
            objBookingRequest.propParticipants = "0";
        }
        Session["masterInput"] = objBookingRequest;
        Session["CurrencyID"] = "2";
        //Dictionary<long, string> participants = new Dictionary<long, string>();
        //participants.Add(1, objBookingRequest.propParticipants);
        //Session["participants"] = participants;
    }
}