﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Agency/AgencyMaster.master" AutoEventWireup="true"
    CodeFile="ControlPanelUser.aspx.cs" Inherits="Agency_ControlPanelUser" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControl/HotelUser/BookingDetails.ascx" TagName="BookingDetails"
    TagPrefix="uc2" %>
<%@ Register Src="~/UserControl/HotelUser/ListRequests.ascx" TagName="RequestDetails"
    TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ctnTop" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ctnMain" runat="Server">
    <style>
        .BorderBlue
        {
            border: #0377C8 solid 1px;
        }
    </style>
    <asp:Repeater ID="rptTopImages" runat="server" 
        onitemdatabound="rptTopImages_ItemDataBound" >
        <HeaderTemplate><table width="100%" cellpadding="5" cellspacing="1"><tr></HeaderTemplate>
        <ItemTemplate><td align="center"><asp:Image ID="imgMain" runat="server" /></td></ItemTemplate>
        <FooterTemplate></tr></table></FooterTemplate>
    </asp:Repeater>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <div class="cancelation-heading-body3" id="HotelAssignTab">
                    <ul>
                        <li><a id="abooking"  href="javascript:void(0);">Booking</a></li>
                        <li><a id="arequest" class="select" href="javascript:void(0);">Request</a></li>
                    </ul>
                </div>
            </td>
        </tr>
        <tr id="trbooking"  style="display:none;">
            <td valign="top" style="background-color: #FFFFFF;">
                <table style="background: #ffffff; border: 1px #A3D4F7 solid;" width="100%" >
                    <tbody>
                        <tr>
                            <td width="50px" >Filter By</td>
                            <td width="120px" ><asp:DropDownList ID="drpFilterByBooking" runat="server" >
                                <asp:ListItem Value="0">Booking Date</asp:ListItem>
                                <asp:ListItem Value="1">Arrival Date</asp:ListItem>
                            </asp:DropDownList></td>
                            <td  width="30px" >
                                From
                            </td>
                            <td  width="75px" ><div class="search-bookings-date-box">
                                <asp:TextBox ID="txtBookingFrom" runat="server" Text="dd/mm/yy" CssClass="inputbox"></asp:TextBox></div>
                            </td>
                            <td  width="30px" ><input type="image" id="calFrom1" src="../images/date-icon.png" />
                                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtBookingFrom"
                                            PopupButtonID="calFrom1" Format="dd/MM/yy">
                                        </asp:CalendarExtender></td>
                            <td  width="15px" >To</td>
                            <td  width="75px" ><div class="search-bookings-date-box"><asp:TextBox ID="txtBookingTo" runat="server" Text="dd/mm/yy" CssClass="inputbox"></asp:TextBox></div></td>
                            <td  width="30px" ><input type="image" id="calTo1" src="../images/date-icon.png" />
                                        <asp:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtBookingTo"
                                            PopupButtonID="calTo1" Format="dd/MM/yy">
                                        </asp:CalendarExtender></td>
                            <td>
                                <div class="search-bookings-btn">
                                    <div class="save-cancel-btn1" style="text-align: left; margin-top: 0px;padding-bottom:2px;float:left;width:auto;">
                                        <asp:LinkButton ID="lnkFilterBooking" runat="server" CssClass="save-btn" OnClick="lnkFilterBooking_Click"
                                            OnClientClick="return CheckBooking();">Filter</asp:LinkButton><!--<a class="save-btn" href="#">Filter</a>-->
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table width="100%" border="0" cellspacing="1" cellpadding="1" style="padding-bottom: 5px;">
                    <tr>
                        <td>
                            <h3 style="font-size: 21px; font-weight: bold; margin: 0px; padding: 0px 0px 0px 0px;">
                                Control Panel
                            </h3>
                        </td>
                        <td>
                            <div style="float: right;" id="BookingListPaging">
                            </div>
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="grdViewBooking" runat="server" border="0" CellPadding="0" OnRowDataBound="grdViewBooking_RowDataBound"
                    DataKeyNames="Id" OnRowCommand="grdViewBooking_RowCommand" AllowPaging="true"
                    OnPageIndexChanging="grdViewBooking_PageIndexChanging" PageSize="10" CellSpacing="0"
                    AutoGenerateColumns="false" Width="100%" CssClass="BorderBlue" GridLines="None">
                    <Columns>
                        <asp:TemplateField HeaderStyle-CssClass="heading-earned-row" ItemStyle-CssClass="con-earned-agency">
                            <HeaderTemplate>
                                Booking date</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblBookingDate" runat="server"></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-CssClass="heading-earned-row1" ItemStyle-CssClass="con1-earned-agency">
                            <HeaderTemplate>
                                Nr.</HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkRefno" runat="server" CommandName="ViewBookingDetail" CommandArgument='<%# Eval("Id") %>'>#56556</asp:LinkButton></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-CssClass="heading-earned-row1" ItemStyle-CssClass="con1-earned-agency">
                            <HeaderTemplate>
                                Generated by</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lnkContactName" runat="server"></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-CssClass="heading-earned-row2" ItemStyle-CssClass="con1-earned-agency">
                            <HeaderTemplate>
                                Company</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblCompany" runat="server"></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-CssClass="heading-earned-row2" ItemStyle-CssClass="con1-earned-agency">
                            <HeaderTemplate>
                                Client / Dept</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblClient" runat="server"></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-CssClass="heading-earned-row1" ItemStyle-CssClass="con1-earned-agency">
                            <HeaderTemplate>
                                Arrival date</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblStart" runat="server"></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle BackColor="#ECF7FE" />
                    <AlternatingRowStyle BackColor="#D9EEFC" />
                    <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                    <PagerStyle HorizontalAlign="Right" BackColor="White" CssClass="displayNone" />
                    <PagerTemplate>
                        <div id="BookingPaging" style="width: 100%; display: none;">
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                        </div>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        <table width="100%">
                            <tr>
                                <td colspan="4" align="center">
                                    <b>No record found</b>
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                </asp:GridView>
                <div id="divbookingdetails" align="left" runat="server" visible="false" style="margin-left: 109px;">
                    <div style="float:left;width:760px;text-align:right;padding-top:10px;"><input type="image" src="../Images/close.png" id="closeButton1" onclick="return HideParent(this);"/></div>
                    <h1 class="new" style="font: left; width: 760px">
                        Booking details</h1>
                    <div class="booking-details" id="divprint" runat="server">
                        <ul>
                            <li class="value3">
                                <div class="col9">
                                    <img src="../Images/print.png" />&nbsp; <a id="ADetails" style="cursor: pointer;"
                                        onclick="javascript:Button1_onclick('<%= Divdetails.ClientID%>');">Print</a>
                                    &bull;
                                    <img src="../Images/pdf.png" />&nbsp;
                                    <asp:LinkButton ID="lnkSavePDF" runat="server" OnClick="lnkSavePDF_Click">Save as PDF</asp:LinkButton>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div id="Divdetails" runat="server" class="meinnewbody">
                        <uc2:BookingDetails ID="ucViewBookingDetail" runat="server" />
                    </div>
                </div>
            </td>
        </tr>
        <tr id="trrequest">
            <td valign="top" style="background-color: #FFFFFF;">
                <table style="background: #ffffff; border: 1px #A3D4F7 solid;"  width="100%" >
                    <tbody>
                        <tr>
                            <td width="50px">Filter By</td>
                            <td width="120px"><asp:DropDownList ID="drpFilterByRequest" runat="server" >
                                <asp:ListItem Value="0">Request Date</asp:ListItem>
                                <asp:ListItem Value="1">Arrival Date</asp:ListItem>
                            </asp:DropDownList></td>
                            <td width="30px">From</td>
                            <td width="75px"><div class="search-bookings-date-box">
                                        <asp:TextBox ID="txtRequestFrom" runat="server" Text="dd/mm/yy" CssClass="inputbox"></asp:TextBox>
                                        <!--<input name="text" type="text" class="inputbox1" value="dd/mm/yy" />-->
                                    </div></td>
                            <td width="30px"><input type="image" src="../images/date-icon.png" id="calFrom2" />
                                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtRequestFrom"
                                            PopupButtonID="calFrom2" Format="dd/MM/yy">
                                        </asp:CalendarExtender></td>
                            <td width="15px">To</td>
                            <td width="75px"><div class="search-bookings-date-box">
                                        <asp:TextBox ID="txtRequestTo" runat="server" Text="dd/mm/yy" CssClass="inputbox"></asp:TextBox>
                                        <!--<input name="text" type="text" class="inputbox1" value="dd/mm/yy" />-->
                                    </div></td>
                            <td width="30px"><input type="image" src="../images/date-icon.png" id="calTo2" />
                                        <asp:CalendarExtender ID="calExTO" runat="server" TargetControlID="txtRequestTo"
                                            PopupButtonID="calTo2" Format="dd/MM/yy">
                                        </asp:CalendarExtender></td>
                            <td>
                                <div class="search-bookings-btn">
                                    <div class="save-cancel-btn1" style="text-align: left; margin-top: 0px;padding-bottom:2px;float:left;width:auto;">
                                        <asp:LinkButton ID="lnkRequestFilter" runat="server" CssClass="save-btn" OnClick="lnkRequestFilter_Click"
                                            OnClientClick="return CheckRequest();">Filter</asp:LinkButton><!--<a class="save-btn" href="#">Filter</a>-->
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table width="100%" border="0" cellspacing="1" cellpadding="1" style="padding-bottom: 5px;">
                    <tr>
                        <td>
                            <h3 style="font-size: 21px; font-weight: bold; margin: 0px; padding: 0px 0px 0px 0px;">
                                Control Panel
                            </h3>
                        </td>
                        <td>
                            <div style="float: right;" id="RequestListPaging">
                            </div>
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="grdViewRequest" runat="server" border="0" CellPadding="0" OnRowDataBound="grdViewRequest_RowDataBound"
                    OnRowCommand="grdViewRequest_RowCommand" AllowPaging="true"
                    OnPageIndexChanging="grdViewRequest_PageIndexChanging" PageSize="10" CellSpacing="0"
                    AutoGenerateColumns="false" Width="100%" CssClass="BorderBlue" GridLines="None">
                    <Columns>
                        <asp:TemplateField HeaderStyle-CssClass="heading-earned-row" ItemStyle-CssClass="con-earned-agency">
                            <HeaderTemplate>
                                Request date</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblBookingDate" runat="server"></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-CssClass="heading-earned-row1" ItemStyle-CssClass="con1-earned-agency">
                            <HeaderTemplate>
                                Nr.</HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkRefno" runat="server" CommandName="ViewBookingDetail" ></asp:LinkButton></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-CssClass="heading-earned-row1" ItemStyle-CssClass="con1-earned-agency">
                            <HeaderTemplate>
                                Generated by</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lnkContactName" runat="server"></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-CssClass="heading-earned-row2" ItemStyle-CssClass="con1-earned-agency">
                            <HeaderTemplate>
                                Company</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblCompany" runat="server"></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-CssClass="heading-earned-row2" ItemStyle-CssClass="con1-earned-agency">
                            <HeaderTemplate>
                                Client / Dept</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblClient" runat="server"></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-CssClass="heading-earned-row1" ItemStyle-CssClass="con1-earned-agency">
                            <HeaderTemplate>
                                Arrival date</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblStart" runat="server"></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-CssClass="heading-earned-row1" ItemStyle-CssClass="con1-earned-agency">
                            <HeaderTemplate>
                                Status</HeaderTemplate>
                            <ItemTemplate><asp:LinkButton ID="lnkStatus" runat="server" ></asp:LinkButton>
                                <asp:Label ID="lblStatus" runat="server"></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle BackColor="#ECF7FE" />
                    <AlternatingRowStyle BackColor="#D9EEFC" />
                    <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                    <PagerStyle HorizontalAlign="Right" BackColor="White" CssClass="displayNone" />
                    <PagerTemplate>
                        <div id="RequestPaging" style="width: 100%; display: none;">
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                        </div>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        <table width="100%">
                            <tr>
                                <td colspan="4" align="center">
                                    <b>No record found</b>
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                </asp:GridView>
                <div id="DivrequestDetail" align="left" runat="server" visible="false" style="margin-left: 109px;">
                    <div style="float:left;width:760px;text-align:right;padding-top:10px;"><input type="image" src="../Images/close.png"  id="closeButton2" onclick="return HideParent(this);"/></div>
                    <h1 class="new" style="font: left; width: 760px">
                        Request details</h1>
                    <div class="booking-details" id="div3" runat="server">
                        <ul>
                            <li class="value3">
                                <div class="col9">
                                    <img src="../Images/print.png" />&nbsp; <a id="A1" style="cursor: pointer;" onclick="javascript:Button2_onclick('<%= DivdetailsRequest.ClientID%>');">
                                        Print</a> &bull;
                                    <img src="../Images/pdf.png" />&nbsp;
                                    <asp:LinkButton ID="lnkSaveRequestPdf" runat="server" OnClick="lnkSaveRequestPdf_Click">Save as PDF</asp:LinkButton>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div id="DivdetailsRequest" runat="server" class="meinnewbody">
                        <uc4:RequestDetails ID="requestDetails" runat="server" />
                        <uc2:BookingDetails ID="BookingDetails1" runat="server" />
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#<%= txtBookingFrom.ClientID %>").attr("disabled", true);
            jQuery("#<%= txtBookingTo.ClientID %>").attr("disabled", true);
            jQuery("#<%= txtRequestFrom.ClientID %>").attr("disabled", true);
            jQuery("#<%= txtRequestTo.ClientID %>").attr("disabled", true);
            jQuery("#HotelAssignTab ul li").find("a").bind("click", function () {
                jQuery("#HotelAssignTab ul").find("a").removeClass("select");
                jQuery(this).addClass("select");
                if (jQuery(this).html() == "Booking") {
                    jQuery("#trbooking").show();
                    jQuery("#trrequest").hide();
                }
                else {
                    jQuery("#trbooking").hide();
                    jQuery("#trrequest").show();
                }
            });
        });

        function addRowBelow(e,tbl) {
            var row = jQuery(e).parent().parent();
            var idcreated = jQuery(row).next().attr("id");
            if (idcreated == undefined) {
                jQuery('<tr id="belowRow"><td colspan="7">' + tbl + '</td></tr>').insertAfter(row);
                jQuery(e).find("img").attr("src", "../images/open-arrow.png");
            }
            else {
                jQuery(e).find("img").attr("src", "../images/close-arrow.png");
                jQuery(row).next().remove();
            }
            return false;
        }
        function Button1_onclick(strid) {
            var prtContent = document.getElementById(strid);
            var WinPrint = window.open('', '', 'letf=0,top=0,width=800,height=800,toolbar=0,scrollbars=0,status=0');
            var mystr = '<link href="<%= SiteRootPath %>css/style.css" rel="stylesheet" type="text/css" ><\/style>' + prtContent.innerHTML;
            WinPrint.document.write(mystr);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        }
        function CheckBooking() {
            if (jQuery("#<%= txtBookingFrom.ClientID %>").val() == "dd/mm/yy" || jQuery("#<%= txtBookingFrom.ClientID %>").val() == null || jQuery("#<%= txtBookingFrom.ClientID %>").val() == "") {
                //jQuery("#adjust-meeting .error").show()
                //jQuery("#adjust-meeting .error").html("Please select From date.")
                //return false
            }
            else if (jQuery("#<%= txtBookingTo.ClientID %>").val() == "dd/mm/yy" || jQuery("#<%= txtBookingTo.ClientID %>").val() == null || jQuery("#<%= txtBookingTo.ClientID %>").val() == "") {
                //jQuery("#adjust-meeting .error").show()
                //jQuery("#adjust-meeting .error").html("Please select To date.")
                //return false
            }
            else {
                var fromdate = jQuery("#<%= txtBookingFrom.ClientID %>").val()
                var todate = jQuery("#<%= txtBookingTo.ClientID %>").val()
                var todayArr = todate.split('/')
                var formdayArr = fromdate.split('/')
                var fromdatecheck = new Date()
                var todatecheck = new Date()
                fromdatecheck.setFullYear(parseInt("20" + formdayArr[2], 10), (parseInt(formdayArr[1], 10) - 1), formdayArr[0])
                todatecheck.setFullYear(parseInt("20" + todayArr[2], 10), (parseInt(todayArr[1]) - 1, 10), todayArr[0])
                if (fromdatecheck > todatecheck) {
                    //jQuery("#adjust-meeting .error").show()
                    //jQuery("#adjust-meeting .error").html("From date must be less than To date.")
                    alert("From date must be less than To date.");
                    return false
                }
            }
            jQuery("#<%= txtBookingFrom.ClientID %>").attr("disabled", false);
            jQuery("#<%= txtBookingTo.ClientID %>").attr("disabled", false);
        }

        function CheckRequest() {
            if (jQuery("#<%= txtRequestFrom.ClientID %>").val() == "dd/mm/yy" || jQuery("#<%= txtRequestFrom.ClientID %>").val() == null || jQuery("#<%= txtRequestFrom.ClientID %>").val() == "") {
                //jQuery("#adjust-meeting .error").show()
                //jQuery("#adjust-meeting .error").html("Please select From date.")
                //return false;
            }
            else if (jQuery("#<%= txtRequestTo.ClientID %>").val() == "dd/mm/yy" || jQuery("#<%= txtRequestTo.ClientID %>").val() == null || jQuery("#<%= txtRequestTo.ClientID %>").val() == "") {
                //jQuery("#adjust-meeting .error").show()
                //jQuery("#adjust-meeting .error").html("Please select To date.")
                //return false;
            }
            else {
                var fromdate = jQuery("#<%= txtRequestFrom.ClientID %>").val()
                var todate = jQuery("#<%= txtRequestTo.ClientID %>").val()
                var todayArr = todate.split('/')
                var formdayArr = fromdate.split('/')
                var fromdatecheck = new Date()
                var todatecheck = new Date()
                fromdatecheck.setFullYear(parseInt("20" + formdayArr[2], 10), (parseInt(formdayArr[1], 10) - 1), formdayArr[0])
                todatecheck.setFullYear(parseInt("20" + todayArr[2], 10), (parseInt(todayArr[1]) - 1, 10), todayArr[0])
                if (fromdatecheck > todatecheck) {
                    //jQuery("#adjust-meeting .error").show()
                    //jQuery("#adjust-meeting .error").html("From date must be less than To date.")
                    alert("From date must be less than To date.");
                    return false;
                }
            }
            jQuery("#<%= txtRequestFrom.ClientID %>").attr("disabled", false);
            jQuery("#<%= txtRequestTo.ClientID %>").attr("disabled", false);
            return true;
        }

        function showBookingRequest(val) {
            if (val == "Booking") {
                jQuery("#trbooking").show();
                jQuery("#trrequest").hide();
                jQuery("#abooking").addClass("select");
                jQuery("#arequest").removeClass("select");
            }
            else {
                jQuery("#trbooking").hide();
                jQuery("#trrequest").show();
                jQuery("#arequest").addClass("select");
                jQuery("#abooking").removeClass("select");
            }
        }
        function ShowBelowDiv(divid, o) {
            if (jQuery("." + divid).is(":visible")) {
                //alert(jQuery(o).parent().html());
                jQuery(o).parent().find("img").attr("src", "../images/open-arrow.png");

                jQuery("." + divid).hide();
            }
            else {
                //alert(jQuery(o).parent().html());
                jQuery(o).parent().find("img").attr("src", "../images/close-arrow.png");
                jQuery("." + divid).show();
            }
        }
        function SetFocusBottom(val) {

            var ofset = jQuery("#" + val).offset();
            jQuery('body').scrollTop(ofset.top);
            jQuery('html').scrollTop(ofset.top);
        }
        jQuery(document).ready(function () {
            if (jQuery("#BookingPaging") != undefined) {
                var inner = jQuery("#BookingPaging").html();
                jQuery("#BookingListPaging").html(inner);
                jQuery("#BookingPaging").html("");
            }

        });

        jQuery(document).ready(function () {
            if (jQuery("#RequestPaging") != undefined) {
                var inner = jQuery("#RequestPaging").html();
                jQuery("#RequestListPaging").html(inner);
                jQuery("#RequestPaging").html("");
            }

        });

        function HideParent(e) {
            jQuery("#"+e.id).parent().parent().hide();
            return false;
        }
    </script>
</asp:Content>
