﻿#region NameSpaces

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using log4net;
using log4net.Config;
using System.IO;
using System.Configuration;

#endregion

public partial class Agency_SendRfp : System.Web.UI.Page
{
    #region Variable Declaration

    NewUser userObj = new NewUser();
    Rpf objrpf = new Rpf();
    EmailConfigManager em = new EmailConfigManager();
    SendMails objSendmail = new SendMails();
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl + "/";
        }
    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LanguageID"] == null)
        {
            Session["LanguageID"] = 1;  // Setting language to default English.
        }

        if (Session["CurrentAgencyUserID"] == null)
        {
            Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
            Session.Abandon();
            if (l != null)
            {
                Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
            }
            else
            {
                Response.Redirect(SiteRootPath + "login/english");
            }
        }
        if (!IsPostBack)
        {
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
            divMain.Style.Add("display", "block");
            bindCountryDDL();
            

        }
    }
    #endregion

    
    #region Methods
    public string GetKeyResult(string key)
    {
        return ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key);
    }

    public void bindCountryDDL()
    {
        CountryDDL.DataTextField = "CountryName";
        CountryDDL.DataValueField = "Id";
        CountryDDL.DataSource = userObj.GetByAllCountry();
        CountryDDL.DataBind();
        CountryDDL.Items.Insert(0, new ListItem("--Select Country--", "0"));
        txtRspduedate.Text = "dd/mm/yy";
        txtdecisiondate.Text = "dd/mm/yy";
        txteventstartdate.Text = "dd/mm/yy";
        txteventenddate.Text = "dd/mm/yy";
        txtEventRfp.Text = "";
        txtCity.Text = "";
        txtbedroomtext.Text = "";
        txtaddinfo.Text = "";
        txtnamevenues.Text = "";
        txttotalattandes.Text = "";
        txtvenuetype.Text = "";
    }
    #endregion

    protected void lbtCancel_Click(object sender, EventArgs e)
    {
        txtRspduedate.Text = "dd/mm/yy";
        txtdecisiondate.Text = "dd/mm/yy";
        txteventstartdate.Text = "dd/mm/yy";
        txteventenddate.Text = "dd/mm/yy";
        txtEventRfp.Text = "";
        bindCountryDDL();
        txtCity.Text = "";
        txtbedroomtext.Text = "";
        txtaddinfo.Text = "";
        txtnamevenues.Text = "";
        txttotalattandes.Text = "";
        txtvenuetype.Text = "";
       





    }
        
    protected void lbtSave_Click(object sender, EventArgs e)
    {
        string status;
        long id = Convert.ToInt64(Session["CurrentAgencyUserID"]);
        // Inserting user information-------Start-----------//
        objrpf.Userid = id;
        if ((txtRspduedate.Text != "") && (txtRspduedate.Text != "dd/mm/yy"))
        {
            DateTime Rspduedate = new DateTime(Convert.ToInt32("20" + txtRspduedate.Text.Split('/')[2]), Convert.ToInt32(txtRspduedate.Text.Split('/')[1]), Convert.ToInt32(txtRspduedate.Text.Split('/')[0]));
            objrpf.RespocedueDate = Rspduedate;
        }
        if ((txtdecisiondate.Text != "") && (txtdecisiondate.Text != "dd/mm/yy"))
       {
           DateTime decisiondate = new DateTime(Convert.ToInt32("20" + txtdecisiondate.Text.Split('/')[2]), Convert.ToInt32(txtdecisiondate.Text.Split('/')[1]), Convert.ToInt32(txtdecisiondate.Text.Split('/')[0]));
           objrpf.Decisiondate = decisiondate;
       }
        if ((txteventstartdate.Text != "") && (txteventstartdate.Text != "dd/mm/yy"))
       {
           DateTime eventstartdate = new DateTime(Convert.ToInt32("20" + txteventstartdate.Text.Split('/')[2]), Convert.ToInt32(txteventstartdate.Text.Split('/')[1]), Convert.ToInt32(txteventstartdate.Text.Split('/')[0]));
           objrpf.EventStartdate = eventstartdate;
       }
        if ((txteventenddate.Text != "") && (txteventenddate.Text != "dd/mm/yy"))
       {
           DateTime eventenddate = new DateTime(Convert.ToInt32("20" + txteventenddate.Text.Split('/')[2]), Convert.ToInt32(txteventenddate.Text.Split('/')[1]), Convert.ToInt32(txteventenddate.Text.Split('/')[0]));
           objrpf.EventEnddate = eventenddate;
       }
        if (CountryDDL.SelectedItem.Value == "0")
        {
            objrpf.Countryid = null;
        }
        else
        {
            objrpf.Countryid = Convert.ToInt64(CountryDDL.SelectedItem.Value);
        }
        
        objrpf.City = txtCity.Text;
        objrpf.Venuetype = txtvenuetype.Text;
        objrpf.Venuename = txtnamevenues.Text;
        objrpf.Eventtype = drpevent.SelectedItem.Text;
        objrpf.Totalattandes = txttotalattandes.Text;
        bool clientvenue;
        if (rbtcientvenue.SelectedItem.Value == "1")
        {
            clientvenue = false;
        }
        else
        {
            clientvenue = true;
        }

        bool bedroom;
        if (rbtbedroom.SelectedItem.Value == "1")
        {
            bedroom = false;
        }
        else
        {
            bedroom = true;
        }

        objrpf.Isdateflexible = clientvenue;
        objrpf.Requirebedrooms = bedroom;
        objrpf.Bedroomtext = txtbedroomtext.Text;
        objrpf.AdditionalInfomation = txtaddinfo.Text;
        status = userObj.InsertRPF(objrpf);
        if (status =="RPF successfully Saved")
        {
            divMain.Style.Add("display", "none");
            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "succesfuly");
            divmessage.InnerHtml = "Your RFP has been sent successfully. You will receive some offers within 48 hours on weekdays and 72 hours on weekends.<br/><br/> Do not hesitate to contact us, if you have any questions.<br/><br/>With best regards,<br/>Support lastminutemetingroom.com";
            EmailConfig eConfig = em.GetByName("Send RFP");
            EmailValueCollection objEmailValues = new EmailValueCollection();
            if (eConfig.IsActive)
            {
                objSendmail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"]; 

                //Mail to HotelSupport
                objSendmail.ToEmail = ConfigurationManager.AppSettings["ClientSupportEmailID"];
                //string bodymsg = File.ReadAllText(Server.MapPath("~/EmailTemplets/JoinToday.html"));
                string bodymsg = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
                EmailConfigMapping emap2 = new EmailConfigMapping();
                emap2 = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                bodymsg = bodymsg.Replace("@CONTENTS", emap2.EmailContents);
                objSendmail.Subject = "NEW RFP";
                if (txtEventRfp.Text == "")
                {
                    txtEventRfp.Text = "N/A";
                }
                if (txtRspduedate.Text == "dd/mm/yy")
                {
                    txtRspduedate.Text = "N/A";
                }
                if (txtdecisiondate.Text == "dd/mm/yy")
                {
                    txtdecisiondate.Text = "N/A";
                }
                if (txteventstartdate.Text == "dd/mm/yy")
                {
                    txteventstartdate.Text = "N/A";
                }
                if (txteventenddate.Text == "dd/mm/yy")
                {
                    txteventenddate.Text = "N/A";
                }
                if (txteventenddate.Text == "")
                {
                    txteventenddate.Text = "N/A";
                }
                if (txtCity.Text == "")
                {
                    txtCity.Text = "N/A";
                }
                if (txtvenuetype.Text == "")
                {
                    txtvenuetype.Text = "N/A";
                }
                if (txtnamevenues.Text == "")
                {
                    txtnamevenues.Text = "N/A";
                }
                if (txttotalattandes.Text == "")
                {
                    txttotalattandes.Text = "N/A";
                }
                if (txtbedroomtext.Text == "")
                {
                    txtbedroomtext.Text = "N/A";
                }
                if (txtaddinfo.Text == "")
                {
                    txtaddinfo.Text = "N/A";
                }
                if (CountryDDL.SelectedItem.Value == "0")
                {
                    foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForSendRFP("Hotel Support", txtEventRfp.Text, txtRspduedate.Text, txtdecisiondate.Text, "N/A", txtCity.Text, txtvenuetype.Text, txtnamevenues.Text, drpevent.SelectedItem.Text, txttotalattandes.Text, txteventstartdate.Text, txteventenddate.Text, rbtcientvenue.SelectedItem.Text, rbtbedroom.SelectedItem.Text, txtbedroomtext.Text, txtaddinfo.Text))
                    {
                        bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                    }
                }
                else
                {
                    foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForSendRFP("Hotel Support", txtEventRfp.Text, txtRspduedate.Text, txtdecisiondate.Text, CountryDDL.SelectedItem.Text, txtCity.Text, txtvenuetype.Text, txtnamevenues.Text, drpevent.SelectedItem.Text, txttotalattandes.Text, txteventstartdate.Text, txteventenddate.Text, rbtcientvenue.SelectedItem.Text, rbtbedroom.SelectedItem.Text, txtbedroomtext.Text, txtaddinfo.Text))
                    {
                        bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                    }
                }

                
                objSendmail.Body = bodymsg;
               objSendmail.SendMail();
                bindCountryDDL();
            }
        }
        else
        {
            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "error");
            divmessage.InnerHtml = status;
        }
    }
    protected void rbtbedroom_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbtbedroom.SelectedItem.Value == "2")
        {
            divbedroom.Visible = true;
        }
        else
        {
            divbedroom.Visible = false;
        }
    }
}