﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Entities;
using LMMR.Business;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

public partial class Agency_ControlPanelUser : BasePage
{
    ViewBooking_Hotel objViewHotelBooikng = new ViewBooking_Hotel();
    ManageCMSContent objManageCMSContent = new ManageCMSContent();
    public SuperAdminTaskManager manager = new SuperAdminTaskManager();
    public VList<Viewbookingrequest> objViewRequest
    {
        get;
        set;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentAgencyUserID"] == null)
        {
            Response.Redirect(SiteRootPath + "login/english/");
        }
        if (!Page.IsPostBack)
        {
            //calExTO.EndDate = DateTime.Now;
            //CalendarExtender1.EndDate = DateTime.Now;
            //CalendarExtender2.EndDate = DateTime.Now;
            //CalendarExtender3.EndDate = DateTime.Now;
            BindTopImages();
            BindBookingGrid();
            grdViewRequestBind();
        }
        else
        {
            ApplyPagingForBookingList();
            ApplyPagingForRequestList();
        }
    }

    public void BindTopImages()
    {
        Cms cmsObj = manager.getCmsEntityOnType("AgencyControlPanel");
        if (cmsObj != null)
        {
            rptTopImages.DataSource = objManageCMSContent.GetCMSContent(cmsObj.Id, Convert.ToInt64(Session["LanguageID"]));
            rptTopImages.DataBind();
        }
    }

    public void BindBookingGrid()
    {
        string whereclaus = "";
        string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
        if (ViewState["whereBooking"] != null)
        {
            whereclaus = Convert.ToString(ViewState["whereBooking"]);
        }
        else
        {
            whereclaus = ViewbookingrequestColumn.CreatorId + "=" + Convert.ToString(Session["CurrentAgencyUserID"]) + " AND " + ViewbookingrequestColumn.BookType + "=0 And " + ViewbookingrequestColumn.BookingDate + ">='" + DateTime.Now.AddDays(-15) + "' AND " + ViewbookingrequestColumn.BookingDate + "<='" + DateTime.Now.AddDays(1) + "'";
        }
        VList<Viewbookingrequest> objViewBooking = new VList<Viewbookingrequest>();
        objViewBooking = objViewHotelBooikng.Bindgrid(whereclaus, orderby);
        grdViewBooking.DataSource = objViewBooking;
        grdViewBooking.DataBind();
        ApplyPagingForBookingList();
        divbookingdetails.Visible = false;
        DivrequestDetail.Visible = false;
    }

    protected void grdViewBooking_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
        {
            Viewbookingrequest data = e.Row.DataItem as Viewbookingrequest;
            LinkButton lnkRefno = (LinkButton)e.Row.FindControl("lnkRefno");
            lnkRefno.Text = data.Id.ToString();
            //Label lblMeetingroom = (Label)e.Row.FindControl("lblMeetingroom");
            //lblMeetingroom.Text = data.Name;
            Label lblBookingDate = (Label)e.Row.FindControl("lblBookingDate");
            lblBookingDate.Text = string.Format("{0:dd MMM yyyy}", data.BookingDate);
            Label lblCompany = (Label)e.Row.FindControl("lblCompany");
            Label lblClient = (Label)e.Row.FindControl("lblClient");
            //lblCompany.Text = data.TypeUser;
            if (Convert.ToInt64(Session["CurrentAgencyUserID"]) == data.AgencyClientId)
            {
                UserDetails ud = new UserManager().getUserbyid(Convert.ToInt64(data.AgencyClientId)).FirstOrDefault();
                if (ud != null)
                {
                    lblCompany.Text = ud.CompanyName;
                    lblClient.Text = ud.CompanyName;
                }

            }
            else
            {
                UserDetails ud = new UserManager().getUserbyid(Convert.ToInt64(data.AgencyClientId)).FirstOrDefault();
                if (ud != null)
                {
                    lblClient.Text = ud.CompanyName;
                }
                UserDetails ud2 = new UserManager().getUserbyid(Convert.ToInt64(Session["CurrentAgencyUserID"])).FirstOrDefault();
                if (ud != null)
                {
                    lblCompany.Text = ud2.CompanyName;
                }
                //lblCompany.Text = data.Contact;
            }
            Label lnkContactName = (Label)e.Row.FindControl("lnkContactName");
            lnkContactName.Text = data.Contact;
            Label lblStart = (Label)e.Row.FindControl("lblStart");
            lblStart.Text = string.Format("{0:dd MMM yyyy}", data.ArrivalDate);
            //Label lblEnd = (Label)e.Row.FindControl("lblEnd");
            //lblEnd.Text = string.Format("{0:dd MMM yyyy}", data.DepartureDate);
            //Label lblTotal = (Label)e.Row.FindControl("lblTotal");
            //lblTotal.Text = string.Format("{0:###,###,###}", data.FinalTotalPrice);
        }
    }
    protected void grdViewBooking_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (Convert.ToString(e.CommandName) == "ViewBookingDetail")
        {
            divbookingdetails.Visible = true;
            //pnlBookingGrid.Visible = true;
            ucViewBookingDetail.BindBooking(Convert.ToInt32(e.CommandArgument));
            //ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){showBookingRequest('Booking'); SetFocusBottom('" + divbookingdetails.ClientID + "');});", true);
            Page.RegisterStartupScript("aa", "<script language='javascript' >jQuery(document).ready(function(){showBookingRequest('Booking'); SetFocusBottom('" + divbookingdetails.ClientID + "');});</script>");
        }
    }
    protected void grdViewBooking_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            divbookingdetails.Visible = false;
            //pnlBookingGrid.Visible = true;
            grdViewBooking.PageIndex = e.NewPageIndex;
            ViewState["CurrentPageBookingList"] = e.NewPageIndex;
            BindBookingGrid();
            //ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){showBookingRequest('Booking'); SetFocusBottom('" + grdViewBooking.ClientID + "');});", true);
            Page.RegisterStartupScript("aa", "<script language='javascript' >jQuery(document).ready(function(){showBookingRequest('Booking'); SetFocusBottom('" + grdViewBooking.ClientID + "');});</script>");
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    private void ApplyPagingForBookingList()
    {
        try
        {
            GridViewRow row = grdViewBooking.TopPagerRow;
            if (row != null)
            {
                PlaceHolder ph;
                LinkButton lnkPaging;
                LinkButton lnkPrevPage;
                LinkButton lnkNextPage;
                lnkPrevPage = new LinkButton();
                lnkPrevPage.CssClass = "pre";
                lnkPrevPage.Width = Unit.Pixel(73);
                lnkPrevPage.CommandName = "Page";
                lnkPrevPage.CommandArgument = "prev";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPrevPage);
                if (grdViewBooking.PageIndex == 0)
                {
                    lnkPrevPage.Enabled = false;

                }

                if (ViewState["CurrentPageBookingList"] == null)
                {
                    ViewState["CurrentPageBookingList"] = "0";
                }

                for (int i = 1; i <= grdViewBooking.PageCount; i++)
                {
                    lnkPaging = new LinkButton();
                    if (ViewState["CurrentPageBookingList"] != null)
                    {
                        if (Convert.ToInt32(ViewState["CurrentPageBookingList"]) + 1 == i)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    else
                    {
                        if (i == 1)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    lnkPaging.Width = Unit.Pixel(16);
                    lnkPaging.Text = i.ToString();
                    lnkPaging.CommandName = "Page";
                    lnkPaging.CommandArgument = i.ToString();
                    if (i == grdViewBooking.PageIndex + 1)
                        ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkPaging);
                }
                lnkNextPage = new LinkButton();
                lnkNextPage.CssClass = "nex";
                lnkNextPage.Width = Unit.Pixel(42);
                lnkNextPage.CommandName = "Page";
                lnkNextPage.CommandArgument = "next";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkNextPage);
                ph = (PlaceHolder)row.FindControl("ph");
                if (grdViewBooking.PageIndex == grdViewBooking.PageCount - 1)
                {
                    lnkNextPage.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            //logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    public void grdViewRequestBind()
    {
        string whereclaus = "";
        string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
        if (ViewState["whereRequest"] != null)
        {
            whereclaus = Convert.ToString(ViewState["whereRequest"]);
        }
        else
        {
            whereclaus = ViewbookingrequestColumn.CreatorId + "=" + Convert.ToString(Session["CurrentAgencyUserID"]) + " AND " + "BookType in (1,2)  And " + ViewbookingrequestColumn.BookingDate + ">='" + DateTime.Now.AddDays(-15) + "' AND " + ViewbookingrequestColumn.BookingDate + "<='" + DateTime.Now.AddDays(1) + "'";
        }
        objViewRequest = objViewHotelBooikng.Bindgrid(whereclaus, orderby);
        List<string> strResultIds = objViewRequest.Select(a => a.RequestCollectionId == null ? a.Id.ToString() : a.RequestCollectionId).Distinct().ToList();
        grdViewRequest.DataSource = strResultIds;//objViewRequest;
        grdViewRequest.DataBind();
        ApplyPagingForRequestList();
        DivrequestDetail.Visible = false;
        divbookingdetails.Visible = false;
    }
    private void ApplyPagingForRequestList()
    {
        try
        {
            GridViewRow row = grdViewRequest.TopPagerRow;
            if (row != null)
            {
                PlaceHolder ph;
                LinkButton lnkPaging;
                LinkButton lnkPrevPage;
                LinkButton lnkNextPage;
                lnkPrevPage = new LinkButton();
                lnkPrevPage.CssClass = "pre";
                lnkPrevPage.Width = Unit.Pixel(73);
                lnkPrevPage.CommandName = "Page";
                lnkPrevPage.CommandArgument = "prev";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPrevPage);
                if (grdViewRequest.PageIndex == 0)
                {
                    lnkPrevPage.Enabled = false;

                }

                if (ViewState["CurrentPageRequestList"] == null)
                {
                    ViewState["CurrentPageRequestList"] = "0";
                }

                for (int i = 1; i <= grdViewRequest.PageCount; i++)
                {
                    lnkPaging = new LinkButton();
                    if (ViewState["CurrentPageRequestList"] != null)
                    {
                        if (Convert.ToInt32(ViewState["CurrentPageRequestList"]) + 1 == i)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    else
                    {
                        if (i == 1)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    lnkPaging.Width = Unit.Pixel(16);
                    lnkPaging.Text = i.ToString();
                    lnkPaging.CommandName = "Page";
                    lnkPaging.CommandArgument = i.ToString();
                    if (i == grdViewRequest.PageIndex + 1)
                        ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkPaging);
                }
                lnkNextPage = new LinkButton();
                lnkNextPage.CssClass = "nex";
                lnkNextPage.Width = Unit.Pixel(42);
                lnkNextPage.CommandName = "Page";
                lnkNextPage.CommandArgument = "next";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkNextPage);
                ph = (PlaceHolder)row.FindControl("ph");
                if (grdViewRequest.PageIndex == grdViewRequest.PageCount - 1)
                {
                    lnkNextPage.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            //logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    protected void grdViewRequest_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
        {
            string s = e.Row.DataItem as string;
            List<Viewbookingrequest> varColrequest = objViewRequest.Where(a => a.RequestCollectionId == null ? a.Id.ToString() == s : a.RequestCollectionId == s).ToList();
            if (varColrequest != null)
            {
                Viewbookingrequest data = varColrequest.FirstOrDefault();
                LinkButton lnkRefno = (LinkButton)e.Row.FindControl("lnkRefno");
                string ids = "";
                foreach (Viewbookingrequest vbr in varColrequest)
                {
                    ids += vbr.Id + ",";
                }
                if (ids.Length > 0)
                {
                    ids = ids.Substring(0, ids.Length - 1);
                }
                lnkRefno.Text = ids;
                string Hoteltable = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
                foreach (Viewbookingrequest vbr in varColrequest)
                {
                    Hoteltable += "<tr><td width=\"41%\" height=\"26\">&nbsp;</td><td width=\"38%\" style=\"border-bottom:#A3D4F7 solid 1px;\">" + System.Net.WebUtility.HtmlDecode(vbr.HotelName) + " - ("+vbr.Id+")</td><td width=\"21%\" align=\"right\" style=\"border-bottom:#A3D4F7 solid 1px;padding-right:20px;\">" + Enum.GetName(typeof(BookingRequestStatus),vbr.RequestStatus) + "</td></tr>";
                }
                Hoteltable += "</table>";
                lnkRefno.CommandArgument = data.Id.ToString();
                //Label lblMeetingroom = (Label)e.Row.FindControl("lblMeetingroom");
                //lblMeetingroom.Text = data.Name;
                Label lblBookingDate = (Label)e.Row.FindControl("lblBookingDate");
                lblBookingDate.Text = string.Format("{0:dd MMM yyyy}", data.BookingDate);
                Label lblCompany = (Label)e.Row.FindControl("lblCompany");
                Label lblClient = (Label)e.Row.FindControl("lblClient");
                //lblCompany.Text = data.TypeUser;
                if (Convert.ToInt64(Session["CurrentAgencyUserID"]) == data.AgencyClientId)
                {
                    UserDetails ud = new UserManager().getUserbyid(Convert.ToInt64(data.AgencyClientId)).FirstOrDefault();
                    if (ud != null)
                    {
                        lblCompany.Text = ud.CompanyName;
                        lblClient.Text = ud.CompanyName;
                    }
                }
                else
                {
                    UserDetails ud = new UserManager().getUserbyid(Convert.ToInt64(data.AgencyClientId)).FirstOrDefault();
                    if (ud != null)
                    {
                        lblClient.Text = ud.CompanyName;
                    }
                    UserDetails ud2 = new UserManager().getUserbyid(Convert.ToInt64(Session["CurrentAgencyUserID"])).FirstOrDefault();
                    if (ud != null)
                    {
                        lblCompany.Text = ud2.CompanyName;
                    }
                    //lblCompany.Text = data.Contact;
                }
                Label lnkContactName = (Label)e.Row.FindControl("lnkContactName");
                lnkContactName.Text = data.Contact;
                Label lblStart = (Label)e.Row.FindControl("lblStart");
                lblStart.Text = string.Format("{0:dd MMM yyyy}", data.ArrivalDate);
                LinkButton lnkStatus = (LinkButton)e.Row.FindControl("lnkStatus");
                lnkStatus.Text = varColrequest.Where(p => p.RequestStatus == (int)BookingRequestStatus.Definite).Count() + " Definite &nbsp; <img src='../images/close-arrow.png'  align='absmiddle'/>";//Enum.GetName(typeof(BookingRequestStatus), data.RequestStatus);
                lnkStatus.OnClientClick = "return addRowBelow(this,'"+Hoteltable+"');";
                //Label lblTotal = (Label)e.Row.FindControl("lblTotal");
                //lblTotal.Text = string.Format("{0:###,###,###}", data.FinalTotalPrice);
            }
        }
    }
    protected void grdViewRequest_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        if (Convert.ToString(e.CommandName) == "ViewBookingDetail")
        {
            //requestDetails.BindHotel(Convert.ToInt32(e.CommandArgument));
            ////pnlRequestGrid.Visible = true;
            //DivrequestDetail.Visible = true;
            //Page.RegisterStartupScript("aa", "<script language='javascript' >jQuery(document).ready(function(){showBookingRequest('Request'); SetFocusBottom('" + DivrequestDetail.ClientID + "');});</script>");


            ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
            Booking obj = objViewBooking_Hotel.getparticularbookingdetails(Convert.ToInt64(e.CommandArgument));
            if (obj.BookType == 1)
            {
                requestDetails.BindHotel(Convert.ToInt32(e.CommandArgument));
                BookingDetails1.Visible = false;
                requestDetails.Visible = true;
                DivrequestDetail.Visible = true;
                //ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + DivrequestDetail.ClientID + "');});", true);
            }
            else if (obj.BookType == 2)
            {
                BookingDetails1.Visible = true;
                BookingDetails1.BindBooking(Convert.ToInt32(e.CommandArgument));
                requestDetails.Visible = false;
                DivrequestDetail.Visible = true;
                //ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + DivrequestDetail.ClientID + "');});", true);
            }
            Page.RegisterStartupScript("aa", "<script langauge='javascript'>jQuery(document).ready(function(){showBookingRequest('Request'); SetFocusBottom('" + grdViewRequest.ClientID + "');});</script>");

        }
    }
    protected void grdViewRequest_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            DivrequestDetail.Visible = false;
            //pnlRequestGrid.Visible = true;
            grdViewRequest.PageIndex = e.NewPageIndex;
            ViewState["CurrentPageRequestList"] = e.NewPageIndex;
            grdViewRequestBind();
            //ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){showBookingRequest('Request'); SetFocusBottom('" + grdViewRequest.ClientID + "');});", true);
            Page.RegisterStartupScript( "aa", "<script langauge='javascript'>jQuery(document).ready(function(){showBookingRequest('Request'); SetFocusBottom('" + grdViewRequest.ClientID + "');});</script>");
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    #region PDF
    protected void lnkSavePDF_Click(object sender, EventArgs e)
    {
        try
        {
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=UserDetails.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            PrepareGridViewForExport(Divdetails);
            Divdetails.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        catch (Exception ex)
        {
            //logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }




    }

    #region VerifyRenderingInServerForm
    /// <summary>
    /// method to VerifyRenderingInServerForm
    /// </summary>

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    #endregion

    #region PrepareGridViewForExport
    /// <summary>
    /// method to PrepareGridViewForExport
    /// </summary>
    private void PrepareGridViewForExport(Control gv)
    {
        try
        {
            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {

                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(DropDownList))
                {

                    l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(CheckBox))
                {

                    l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(HyperLink))
                {

                    l.Text = (gv.Controls[i] as HyperLink).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.Image))
                {

                    // l.Text = (gv.Controls[i] as Image).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareGridViewForExport(gv.Controls[i]);

                }

            }
        }
        catch (Exception ex)
        {
            //logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion
    #region PrepareGridViewForExportDIV
    /// <summary>
    /// method to PrepareGridViewForExport a div
    /// </summary>
    private void PrepareDivForExport(Control gv)
    {
        try
        {

            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {

                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].GetType() == typeof(System.Web.UI.HtmlControls.HtmlAnchor))
                {

                    l.Text = (gv.Controls[i] as System.Web.UI.HtmlControls.HtmlAnchor).InnerText;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(DropDownList))
                {

                    l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(GridView))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    // gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Repeater))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Panel))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Table))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }


                else if (gv.Controls[i].GetType() == typeof(CheckBox))
                {

                    l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(HyperLink))
                {

                    l.Text = (gv.Controls[i] as HyperLink).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                //else if (gv.Controls[i].GetType() == typeof(Label))
                //{

                //    l.Text = (gv.Controls[i] as Label).Text;

                //    gv.Controls.Remove(gv.Controls[i]);

                //    gv.Controls.AddAt(i, l);

                //}
                else if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.Image))
                {

                    // l.Text = (gv.Controls[i] as Image).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareDivForExport(gv.Controls[i]);

                }

            }
        }
        catch (Exception ex)
        {
            //logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        //PrepareGridViewForExport();
        //PrepareGridViewForExport();

    }


    #endregion

    protected void lnkSaveRequestPdf_Click(object sender, EventArgs e)
    {
        try
        {
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=UserDetails.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            PrepareGridViewForExport(DivdetailsRequest);
            DivdetailsRequest.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        catch (Exception ex)
        {
            //logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion
    protected void lnkFilterBooking_Click(object sender, EventArgs e)
    {
        DateTime dtfrom;
        DateTime dtTo;
        string whereCl = "";
        if (!string.IsNullOrEmpty(txtBookingFrom.Text) && txtBookingFrom.Text.Trim() != "dd/mm/yy")
        {
            if (whereCl.Length > 0)
            {
                whereCl += " AND ";
            }
            dtfrom = new DateTime(Convert.ToInt32("20" + txtBookingFrom.Text.Trim().Split('/')[2]), Convert.ToInt32(txtBookingFrom.Text.Trim().Split('/')[1]), Convert.ToInt32(txtBookingFrom.Text.Trim().Split('/')[0]));
            if (drpFilterByBooking.SelectedValue == "1")
            {
                whereCl += ViewbookingrequestColumn.ArrivalDate + ">='" + dtfrom + "'";
            }
            else
            {
                whereCl += ViewbookingrequestColumn.BookingDate + ">='" + dtfrom + "'";
            }
        }
        else
        {
            if (whereCl.Length > 0)
            {
                whereCl += " AND ";
            }
            if (drpFilterByBooking.SelectedValue == "1")
            {
                whereCl += ViewbookingrequestColumn.ArrivalDate + ">='" + (new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day)) + "'";
            }
            else
            {
                whereCl += ViewbookingrequestColumn.BookingDate + ">='" + (new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day)).AddDays(-15) + "'";
            }
        }
        if (!string.IsNullOrEmpty(txtBookingTo.Text) && txtBookingTo.Text.Trim() != "dd/mm/yy")
        {
            if (whereCl.Length > 0)
            {
                whereCl += " AND ";
            }
            dtTo = new DateTime(Convert.ToInt32("20" + txtBookingTo.Text.Trim().Split('/')[2]), Convert.ToInt32(txtBookingTo.Text.Trim().Split('/')[1]), Convert.ToInt32(txtBookingTo.Text.Trim().Split('/')[0]));
            if (drpFilterByBooking.SelectedValue == "1")
            {
                whereCl += ViewbookingrequestColumn.ArrivalDate + "<='" + dtTo + "'";
            }
            else
            {
                whereCl += ViewbookingrequestColumn.BookingDate + "<='" + dtTo.AddDays(1) + "'";
            }
        }
        else
        {
            if (whereCl.Length > 0)
            {
                whereCl += " AND ";
            }
            if (drpFilterByBooking.SelectedValue == "1")
            {
                whereCl += ViewbookingrequestColumn.ArrivalDate + "<='" + (new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day)).AddDays(15) + "'";
            }
            else
            {
                whereCl += ViewbookingrequestColumn.BookingDate + "<='" + (new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day)) + "'";
            }
        }
        if (whereCl.Length > 0)
        {
            whereCl += " AND ";
        }
        whereCl += ViewbookingrequestColumn.CreatorId + "=" + Convert.ToString(Session["CurrentAgencyUserID"]) + " AND " + ViewbookingrequestColumn.BookType + "=0";
        ViewState["whereBooking"] = whereCl;
        BindBookingGrid();
        //ApplyPagingForBookingList();
        Page.RegisterStartupScript("aa", "<script langauge='javascript'>jQuery(document).ready(function(){showBookingRequest('Booking'); SetFocusBottom('" + grdViewBooking.ClientID + "');});</script>");
    }

    protected void lnkRequestFilter_Click(object sender, EventArgs e)
    {
        DateTime dtfromReq;
        DateTime dtToReq;
        string whereCl = "";
        if (!string.IsNullOrEmpty(txtRequestFrom.Text) && txtRequestFrom.Text.Trim() != "dd/mm/yy")
        {
            if (whereCl.Length > 0)
            {
                whereCl += " AND ";
            }
            dtfromReq = new DateTime(Convert.ToInt32("20" + txtRequestFrom.Text.Trim().Split('/')[2]), Convert.ToInt32(txtRequestFrom.Text.Trim().Split('/')[1]), Convert.ToInt32(txtRequestFrom.Text.Trim().Split('/')[0]));
            if (drpFilterByRequest.SelectedValue == "1")
            {
                whereCl += ViewbookingrequestColumn.ArrivalDate + ">='" + dtfromReq + "'";
            }
            else
            {
                whereCl += ViewbookingrequestColumn.BookingDate + ">='" + dtfromReq + "'";
            }
        }
        if (!string.IsNullOrEmpty(txtRequestTo.Text) && txtRequestTo.Text.Trim() != "dd/mm/yy")
        {
            if (whereCl.Length > 0)
            {
                whereCl += " AND ";
            }
            dtToReq = new DateTime(Convert.ToInt32("20" + txtRequestTo.Text.Trim().Split('/')[2]), Convert.ToInt32(txtRequestTo.Text.Trim().Split('/')[1]), Convert.ToInt32(txtRequestTo.Text.Trim().Split('/')[0]));
            if (drpFilterByRequest.SelectedValue == "1")
            {
                whereCl += ViewbookingrequestColumn.ArrivalDate + "<='" + dtToReq.AddDays(1) + "'";
            }
            else
            {
                whereCl += ViewbookingrequestColumn.BookingDate + "<='" + dtToReq.AddDays(1) + "'";
            }
        }
        if (whereCl.Length > 0)
        {
            whereCl += " AND ";
        }
        whereCl += ViewbookingrequestColumn.CreatorId + "=" + Convert.ToString(Session["CurrentAgencyUserID"]) + " AND " + "BookType in (1,2)";
        ViewState["whereRequest"] = whereCl;
        grdViewRequestBind();
        Page.RegisterStartupScript("aa", "<script langauge='javascript'>jQuery(document).ready(function(){showBookingRequest('Request'); SetFocusBottom('" + grdViewRequest.ClientID + "');});</script>");
        //ApplyPagingForRequestList();
    }
    protected void rptTopImages_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            System.Web.UI.WebControls.Image imgMain = (System.Web.UI.WebControls.Image)e.Item.FindControl("imgMain");
            CmsDescription c = e.Item.DataItem as CmsDescription;
            if (c != null)
            {
                imgMain.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["FilePath"] + "/AgencyTopImages/" + c.ContentImage;
                imgMain.AlternateText = c.ContentTitle;
                imgMain.ToolTip = c.ContentTitle;
                imgMain.Width = 310;
                imgMain.Height = 200;
            }
        }
    }
}