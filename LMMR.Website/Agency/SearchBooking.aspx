﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Agency/AgencyMaster.master" AutoEventWireup="true"
    CodeFile="SearchBooking.aspx.cs" Inherits="Agency_SearchBooking" EnableEventValidation="false" %>
<%@ Register Src="../UserControl/HotelUser/BookingDetails.ascx" TagName="BookingDetails"
    TagPrefix="uc2" %>
<%@ Register Src="../UserControl/HotelUser/RequestDetails.ascx" TagName="RequestDetails"
    TagPrefix="uc4" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ctnTop" runat="Server">
    <style type="text/css">
        .modalBackground
        {
            background-color: #CCCCFF;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }
    </style>
    <style type="text/css">
        .LinkPaging
        {
            width: 20;
            background-color: White;
            border: Solid 1 Black;
            text-align: center;
            margin-left: 8;
        }
    </style>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ctnMain" runat="Server">
    <div class="operator-mainbody">
        <div class="search-operator-layout1" style="margin-bottom: 20px;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="630px">
                <%--  <asp:UpdatePanel ID="upsearch" runat="server">
               <ContentTemplate>--%>
                <div class="search-operator-layout-left1">
                    <div class="search-operator-from1">
                        <div class="commisions-top-new1 ">
                            <table width="100%" border="0" cellspacing="0" cellpadding="8">
                                <tr id="trSearch" runat="server" visible="true">
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px" runat="server"
                                        id="tdDate">
                                        <table  border="0" cellspacing="0" cellpadding="0" style="max-width:400px; width:380px;">
                                            <tr>
                                                <td>
                                                    <b>
                                                        <asp:Label ID="lbldate" runat="server" Text="Date:"></asp:Label></b>
                                                </td>
                                                <td style="padding-left: 20px; text-align: right; padding-right: 3px;">
                                                    <b>From</b>
                                                </td>
                                                <td>
                                                
                                                    <asp:TextBox ID="txtFromdate" runat="server" CssClass="inputbox1"></asp:TextBox><asp:HiddenField
                                                        ID="hdnfromdate" runat="server" />
                                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFromdate"
                                                        PopupButtonID="calFrom" Format="dd/MM/yy" OnClientDateSelectionChanged="dateChangeFrom">
                                                    </asp:CalendarExtender>
                                                    <asp:Image ID="calFrom" runat="server" ImageUrl="~/Images/date-icon.png" align="absmiddle" />
                                                </td>
                                                <td>
                                                    <b>To</b>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTodate" runat="server" CssClass="inputbox1" ></asp:TextBox>
                                                    <asp:HiddenField ID="hdntodate" runat="server" />
                                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTodate"
                                                        PopupButtonID="calTo" Format="dd/MM/yy" OnClientDateSelectionChanged="dateChangeTo">
                                                    </asp:CalendarExtender>
                                                    <asp:Image ID="calTo" runat="server" ImageUrl="~/Images/date-icon.png" align="absmiddle" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trMultiplecity" runat="server" visible="true">
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <div class="search-form1-left-new">
                                            <table width="100%">
                                                <tr id="trclientname" runat="server" visible="true">
                                                    <td>
                                                    <b>
                                                        <asp:Label ID="lblno" runat="server" Text="Date:"></asp:Label></b>
                                                    </td>
                                                    <td valign="middle">
                                                        <div class="search-form1-left-new">
                                                            <asp:TextBox ID="txtreqno" runat="server" class="inputbox" Width="100%" MaxLength="5"></asp:TextBox>
                                                            <asp:FilteredTextBoxExtender ID="filteredExtender5" TargetControlID="txtreqno" FilterType="Numbers"
                                                                runat="server">
                                                            </asp:FilteredTextBoxExtender>
                                                        </div>
                                                    </td>
                                                    <td valign="top">
                                                        <div class="search-form1-left-new">
                                                           <asp:RadioButtonList ID="rbtcientvenue" runat="server" RepeatDirection="Horizontal">
                                                           <asp:ListItem Text="Venue" Value="2" Selected="True" /> 
                                                           <asp:ListItem Text="Client" Value="1"  />
                                                           </asp:RadioButtonList>
                                                           
                                                            <%--<asp:TextBox ID="txtclientname" runat="server" class="inputbox"></asp:TextBox>--%>
                                                        </div>
                                                    </td>
                                                     <td valign="middle" colspan="2">
                                                        <div class="search-form1-left-new">
                                                            <asp:TextBox ID="txtclientname" runat="server" class="inputbox" Width="100%" MaxLength="50"></asp:TextBox>
                                                            
                                                        </div>
                                                    </td>                                                    
                                                </tr>
                                                
                                            </table>
                                        </div>
                                    </td>
                                    
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <%-- </ContentTemplate>
               </asp:UpdatePanel>--%>
            </td>
            <td align="left" valign="bottom">
                <div style="float: left; margin-left: 20px;" class="n-commisions">
                    <div class="n-btn">
                        <asp:LinkButton ID="lbtSearch" runat="server" OnClick="lbtSearch_Click" OnClientClick="return DataValidation()"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Search</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                    </div>
                </div>
                <div style="float: left; margin-left: 10px;" class="n-commisions">
                    <div class="n-btn">
                        <asp:LinkButton ID="lnkClear" runat="server" onclick="lnkClear_Click" ><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Clear</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>
        <!--contract-list start -->
        <div class="contract-list">
            <div class="contract-list" id="divAlphabeticPaging" runat="server" style="margin-top: 0px;">
                <div class="contract-list-left" style="width: 278px;">
                    <h2>
                        <asp:Label ID="lblHeader" runat="server" /></h2>
                </div>
                <div class="contract-list-right" style="width: 676px;">
                    <ul runat="server" id="AlphaList">
                        <li id="Li1" runat="server"><a href="#" class="select" runat="server" id="all" onserverclick="PageChange">all</a></li>
                        <li id="Li2" runat="server"><a href="#" runat="server" id="a" onserverclick="PageChange">a</a></li>
                        <li id="Li3" runat="server"><a href="#" runat="server" id="b" onserverclick="PageChange">b</a></li>
                        <li id="Li4" runat="server"><a href="#" runat="server" id="c" onserverclick="PageChange">c</a></li>
                        <li id="Li5" runat="server"><a href="#" runat="server" id="d" onserverclick="PageChange">d</a></li>
                        <li id="Li6" runat="server"><a href="#" runat="server" id="e" onserverclick="PageChange">e</a></li>
                        <li id="Li7" runat="server"><a href="#" runat="server" id="f" onserverclick="PageChange">f</a></li>
                        <li id="Li8" runat="server"><a href="#" runat="server" id="g" onserverclick="PageChange">g</a></li>
                        <li id="Li9" runat="server"><a href="#" runat="server" id="h" onserverclick="PageChange">h</a></li>
                        <li id="Li10" runat="server"><a href="#" runat="server" id="i" onserverclick="PageChange">i</a></li>
                        <li id="Li11" runat="server"><a href="#" runat="server" id="j" onserverclick="PageChange">j</a></li>
                        <li id="Li12" runat="server"><a href="#" runat="server" id="k" onserverclick="PageChange">k</a></li>
                        <li id="Li13" runat="server"><a href="#" runat="server" id="l" onserverclick="PageChange">l</a></li>
                        <li id="Li14" runat="server"><a href="#" runat="server" id="m" onserverclick="PageChange">m</a></li>
                        <li id="Li15" runat="server"><a href="#" runat="server" id="n" onserverclick="PageChange">n</a></li>
                        <li id="Li16" runat="server"><a href="#" runat="server" id="o" onserverclick="PageChange">o</a></li>
                        <li id="Li17" runat="server"><a href="#" runat="server" id="p" onserverclick="PageChange">p</a></li>
                        <li id="Li18" runat="server"><a href="#" runat="server" id="q" onserverclick="PageChange">q</a></li>
                        <li id="Li19" runat="server"><a href="#" runat="server" id="r" onserverclick="PageChange">r</a></li>
                        <li id="Li20" runat="server"><a href="#" runat="server" id="s" onserverclick="PageChange">s</a></li>
                        <li id="Li21" runat="server"><a href="#" runat="server" id="t" onserverclick="PageChange">t</a></li>
                        <li id="Li22" runat="server"><a href="#" runat="server" id="u" onserverclick="PageChange">u</a></li>
                        <li id="Li23" runat="server"><a href="#" runat="server" id="v" onserverclick="PageChange">v</a></li>
                        <li id="Li24" runat="server"><a href="#" runat="server" id="w" onserverclick="PageChange">w</a></li>
                        <li id="Li25" runat="server"><a href="#" runat="server" id="x" onserverclick="PageChange">x</a></li>
                        <li id="Li26" runat="server"><a href="#" runat="server" id="y" onserverclick="PageChange">y</a></li>
                        <li id="Li27" runat="server"><a href="#" runat="server" id="z" onserverclick="PageChange">z</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="pageing-operator">
            <div class="pageing-operator-new">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="left" valign="bottom">
                            <table id="tbldownloaddata" runat="server">
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkDownloadPDF" runat="server" OnClick="lnkSavePDFGRID_Click"><table><tr><td align="center"><img src="../Images/pdf.png" /></td></tr><tr><td>Download PDF</td></tr></table></asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkDownloadXLS" runat="server" OnClick="lnkDownloadXLSGRID_Click"><table><tr><td align="center"><img src="../Images/exl.jpg" /></td></tr><tr><td>Download Excel</td></tr></table></asp:LinkButton>
                                    </td>
                                    <td>
                                        <a id="A2" style="cursor: pointer;" onclick="javascript:Button1_onclick('griddetails');">
                                            <table>
                                                <tr>
                                                    <td align="center">
                                                        <img src="../Images/print.png" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Print
                                                    </td>
                                                </tr>
                                            </table>
                                        </a>
                                    </td>
                                    <%-- <td>
                                        <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick="return TestCheckBox();"><table><tr><td align="center"><img src="../Images/delete-ol-btn.png" /></td></tr><tr><td>Delete Booking</td></tr></table></asp:LinkButton>
                                    </td>--%>
                                </tr>
                            </table>
                        </td>
                        <td align="right">
                            Total Result :
                            <asp:Label ID="lblTotalBooking" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="float: right;" id="pageing">
            </div>
        </div>
        <div class="search-booking-rowmain clearfix" id="griddetails" style="width: 960px">
            <table width="100%" border="1" cellpadding="5" cellspacing="0" bgcolor="#98BCD6">
                
                <asp:GridView ID="grvBookingDetails" runat="server" Width="100%" border="0" CellPadding="5"
                    CellSpacing="1" AutoGenerateColumns="false" DataKeyNames="Id" GridLines="None"
                    ShowHeader="true" ShowFooter="false" BackColor="#98BCD6" RowStyle-CssClass="con"
                    AlternatingRowStyle-CssClass="con-dark" EmptyDataRowStyle-BackColor="White" EmptyDataRowStyle-HorizontalAlign="Center"
                    OnPageIndexChanging="grvBookingDetails_PageIndexChanging" PageSize="10" AllowPaging="true" HeaderStyle-HorizontalAlign="Left"
                    HeaderStyle-CssClass="heading-row" OnRowDataBound="grvBookingDetails_RowDataBound" OnRowCommand="grvBookingDetails_RowCommand">
                    <AlternatingRowStyle CssClass="con-dark"></AlternatingRowStyle>
                    <Columns>
                        <asp:TemplateField ItemStyle-Width="10%">
                            <HeaderTemplate>
                                <asp:Label ID="lblType" runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkRefno" runat="server" CommandName="ViewBookingDetail" Text='<%# Eval("Id") %>'
                                    CommandArgument='<%#Eval("Id") %>'></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="10%">
                            <HeaderTemplate>
                                <asp:DropDownList ID="cityDDL" runat="server" CssClass="smallselect" AutoPostBack="true"
                                                    OnSelectedIndexChanged="cityDDL_selectedIndexChanged" Width="50px"/>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblCity" Text="City"></asp:Label>
                                <asp:HiddenField ID="hdnCityId" runat="server" Value='<%# Eval("CityId") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="10%">
                            <HeaderTemplate>
                                 <asp:DropDownList ID="hotelDDL" runat="server" CssClass="midselect" AutoPostBack="true"
                                                    OnSelectedIndexChanged="hotelDDL_selectedIndexChanged" Width="70px">
                                                </asp:DropDownList>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblHotelName" runat="server" Text='<%# Eval("HotelName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="10%">
                            <HeaderTemplate>
                                <asp:DropDownList ID="meetingRoomDDL" runat="server" CssClass="midselect" AutoPostBack="true"
                                                    OnSelectedIndexChanged="meetingRoomDDL_selectedIndexChanged" Width="70px">
                                                </asp:DropDownList>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblMeetingRoomName" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="10%">
                            <HeaderTemplate>
                                Company
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblCompany" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="10%">
                            <HeaderTemplate>
                                Client/Dept
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblConctName" runat="server" Text=""></asp:Label> / 
                                <asp:Label ID="lblDept" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="10%">
                            <HeaderTemplate>
                                <asp:DropDownList ID="bookingDateDDL" runat="server" CssClass="midselect" AutoPostBack="true"
                                                    OnSelectedIndexChanged="bookingDateDDL_selectedIndexChanged" Width="70px" Visible="false">
                                                </asp:DropDownList>
                                Booking Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblBookingDt" runat="server" Text='<%# String.Format("{0:dd/MM/yyyy}",Eval("BookingDate"))%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="10%">
                            <HeaderTemplate>
                                <asp:DropDownList ID="arrivalDateDDL" runat="server" OnSelectedIndexChanged="arrivalDateDDL_selectedIndexChanged" CssClass="midselect" AutoPostBack="true" Width="70px" Visible="false"></asp:DropDownList>
                                Arrival Date
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblArrivalDt" runat="server" Text='<%# String.Format("{0:dd/MM/yyyy}",Eval("ArrivalDate"))%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="10%">
                            <HeaderTemplate>
                                Departure
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblDepartureDt" runat="server" Text='<%#  String.Format("{0:dd/MM/yyyy}", Eval("DepartureDate"))%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="10%">
                            <HeaderTemplate>
                                <asp:DropDownList ID="valueDDL" runat="server" CssClass="smallselect" OnSelectedIndexChanged="valueDDL_selectedIndexChanged" Width="60px" Visible="false"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                Value
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblFinalTotal" runat="server" Text='<%# String.Format("{0:###,###,###}",Eval("FinalTotalPrice")) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="10%">
                             <HeaderTemplate>
                                 Free cancellation until
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblCancellationLimit" runat="server" ToolTip='<%# Eval("HotelId") %>'></asp:Label>
                            </ItemTemplate>                            
                        </asp:TemplateField>
                        <asp:TemplateField>
                             <HeaderTemplate>
                                 Cancel
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtCancel" runat="server"  Visible="false" ToolTip='<%# Eval("Id") %>' CssClass="select"
                                OnClick="lbtCancel_Click" OnClientClick="return confirm('Are you sure you want to cancel.');">Cancel</asp:LinkButton>
                                <asp:Label ID="lblCancelled" runat="server" Text="" Visible="false"></asp:Label>                                
                            </ItemTemplate>                            
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <table>
                            <tr>
                                <td colspan="3" align="center">
                                    <b>No record found !</b>
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                    <PagerStyle HorizontalAlign="Right" BackColor="White" CssClass="displayNone" />
                    <PagerTemplate>
                        <div id="Paging" style="width: 100%; display: none;">
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                        </div>
                    </PagerTemplate>
                </asp:GridView>
            </table>
        </div>
        <div id="divbookingdetails" align="left" runat="server" visible="false" style="margin-left: 109px;">
        <div style="float:left;width:760px;text-align:right;padding-top:10px;"><input type="image" src="../Images/close.png" id="closeButton1" onclick="return HideParent(this);"/></div>
            <h1 class="new" style="font: left; width: 760px">
                Booking details</h1>
            <div class="booking-details" id="divprint" runat="server">
                <ul>
                    <li class="value3">
                        <div class="col9">
                            <img src="../Images/print.png" />&nbsp; <a id="ADetails" style="cursor: pointer;"
                                onclick="javascript:Button1_onclick('<%= Divdetails.ClientID%>');">Print</a>
                            &bull;
                            <img src="../Images/pdf.png" />&nbsp;
                            <asp:LinkButton ID="lnkSavePDF" runat="server" OnClick="lnkSavePDF_Click">Save as PDF</asp:LinkButton>&nbsp;
                            <img src="../Images/exl.jpg" />
                            <asp:LinkButton ID="LinksaveXLS" runat="server" OnClick="lnkDownloadXLS_Click">Download Excel</asp:LinkButton>&nbsp;
                        </div>
                    </li>
                </ul>
            </div>
            <div id="Divdetails" runat="server" class="meinnewbody">
                <uc2:BookingDetails ID="ucViewBookingDetail" runat="server" />
            </div>
        </div>
        <div id="DivrequestDetail" align="left" runat="server" visible="false" style="margin-left: 109px;">
        <div style="float:left;width:760px;text-align:right;padding-top:10px;"><input type="image" src="../Images/close.png"  id="closeButton2" onclick="return HideParent(this);"/></div>
            <h1 class="new" style="font: left; width: 760px">
                Request details</h1>
            <div class="booking-details" id="div3" runat="server">
                <ul>
                    <li class="value3">
                        <div class="col9">
                            <img src="../Images/print.png" />&nbsp; <a id="A1" style="cursor: pointer;" onclick="javascript:Button2_onclick('<%= DivdetailsRequest.ClientID%>');">
                                Print</a> &bull;
                            <img src="../Images/pdf.png" />&nbsp;
                            <asp:LinkButton ID="lnkSaveRequestPdf" runat="server" OnClick="lnkSaveRequestPdf_Click">Save as PDF</asp:LinkButton>
                        </div>
                    </li>
                </ul>
            </div>
            <div id="DivdetailsRequest" runat="server" class="meinnewbody">
                <uc4:RequestDetails ID="requestDetails" runat="server" />
                <uc2:BookingDetails ID="BookingDetails1" runat="server" />
            </div>
        </div>
        <div>
            <asp:LinkButton ID="lnkcheck" runat="server"></asp:LinkButton>
            <asp:ModalPopupExtender ID="ModalPopupCheck" TargetControlID="lnkcheck" BackgroundCssClass="modalBackground"
                PopupControlID="pnlcheck" runat="server">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlcheck" BorderColor="Black" Width="800px" BorderWidth="1" Style="display: none;"
                runat="server" BackColor="White">
                <div class="popup-mid-inner-body" align="center">
                </div>
                <div class="popup-mid-inner-body1">
                    <table width="100%" cellspacing="0" cellpadding="3" align="center">
                        <tr>
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Status
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlstatus" Width="200px" runat="server">
                                    <asp:ListItem Value="7">Definite</asp:ListItem>
                                    <asp:ListItem Value="2">Cancel</asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList ID="drpStatus" Width="200px" runat="server">
                                    <asp:ListItem Value="2">Cancel</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </div>
                <div>
                    <div class="popup-mid-inner-body2_i">
                    </div>
                    <div class="booking-details" style="width: 800px;" id="div5">
                        <ul>
                            <li class="value10">
                                <div class="col21" style="width: 795px;">
                                    <div class="button_section">
                                        <asp:LinkButton ID="lnkchecksubmit" runat="server" Text="Save" CssClass="select"
                                            OnClick="btnSubmit_Click" />
                                        <span>or</span>
                                        <asp:LinkButton ID="lnkcheckcancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
    <script language="javascript" type="text/javascript">
        function CheckOtherIsCheckedByGVID(spanChk) {
            var IsChecked = spanChk.checked;
            var CurrentRdbID = spanChk.id;
            var Chk = spanChk;
            Parent = document.getElementById('ctnMain_grvBookingDetails');
            var items = Parent.getElementsByTagName('input');

            for (i = 0; i < items.length; i++) {

                if (items[i].id != CurrentRdbID && items[i].type == "checkbox") {

                    if (items[i].checked) {

                        items[i].checked = false;

                        items[i].parentElement.parentElement.style.backgroundColor = 'white';

                        items[i].parentElement.parentElement.style.color = 'black';
                    }
                }
            }
        }
        function TestCheckBox() {
            if (jQuery("#ctnMain_grvBookingDetails").find("input:checkbox:checked").length > 0) {

            }
            else {
                alert("Please select at least one Booking");
                return false;
            }


            return confirm('Are you sure you want to delete this Booking?');



        }
                

                
    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            if (jQuery("#Paging") != undefined) {
                var inner = jQuery("#Paging").html();
                jQuery("#pageing").html(inner);
                jQuery("#Paging").html("");
            }

        });
       
        jQuery(document).ready(function () {
                            <% if (ViewState["SearchAlpha"] != null) {%>
                                jQuery('#<%= AlphaList.ClientID %> li a').removeClass('select');
                                jQuery('#ctnMain_<%= ViewState["SearchAlpha"]%>').addClass('select');
                            <% }%>
                        });
                       
    
          function Button1_onclick(strid) {

            var prtContent = document.getElementById(strid);
            var WinPrint = window.open('', '', 'letf=0,top=0,width=800,height=800,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();


        }

        function Button2_onclick(strid) {

            var prtContent = document.getElementById(strid);
            var WinPrint = window.open('', '', 'letf=0,top=0,width=800,height=800,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();


        }
          function SetFocusBottom(val) {

            var ofset = jQuery("#" + val).offset();
            jQuery('body').scrollTop(ofset.top);
            jQuery('html').scrollTop(ofset.top);
        }
    </script>
    <script type="text/javascript" language="javascript">
        //function valid(){
        //    if (document.getElementById(TxtUsername).value != "" && document.getElementById(TxtCompany).value != "") {
        //        alert("Select Only One");
        //        return false;

        //    }
        //}
        function dateChangeFrom(s, e) {
            var fromdatechange = jQuery("#<%= txtFromdate.ClientID %>").val();
            jQuery("#<%= hdnfromdate.ClientID %>").val(fromdatechange);
        }
        function dateChangeTo(s, e) {
            var todatechange = jQuery("#<%= txtTodate.ClientID %>").val();
            jQuery("#<%= hdntodate.ClientID %>").val(todatechange);
        }

        function DataValidation() {
            var fromdatechange = jQuery("#<%= txtFromdate.ClientID %>").val();
            var todatechange = jQuery("#<%= txtTodate.ClientID %>").val();
            
            var todayArr = todatechange.split('/');
            var formdayArr = fromdatechange.split('/');
            var fromdatecheck = new Date();
            var todatecheck = new Date();

            fromdatecheck.setFullYear(parseInt("20" + formdayArr[2], 10), (parseInt(formdayArr[1], 10) - 1), formdayArr[0]);
            todatecheck.setFullYear(parseInt("20" + todayArr[2], 10), (parseInt(todayArr[1], 10) - 1), todayArr[0]);
            if (fromdatecheck > todatecheck) {
                alert("From date must be less than To date.");
                return false;
            }

        }

        function HideParent(e) {
            jQuery("#" + e.id).parent().parent().hide();
            return false;
        }
</script>
<script language="javascript" type="text/javascript">
    jQuery(document).ready(function () {
        jQuery("#<%= txtFromdate.ClientID %>").attr("disabled", true);
        jQuery("#<%= txtTodate.ClientID %>").attr("disabled", true);
        jQuery("#<%= lbtSearch.ClientID %>").bind("click", function () {
            jQuery("#<%= txtFromdate.ClientID %>").attr("disabled", false);
            jQuery("#<%= txtTodate.ClientID %>").attr("disabled", false);
        });
    });
     
</script>
</asp:Content>

