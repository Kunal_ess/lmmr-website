﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Agency/Main.master" AutoEventWireup="true" CodeFile="RequestStep4.aspx.cs" Inherits="Agency_RequestStep4" EnableEventValidation="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="../UserControl/Agency/LeftSearchPanel.ascx" tagname="LeftSearchPanel" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cntMainBody" Runat="Server">
    <!--today-date START HERE-->
        <%-- <div class="today-date">
            <div class="today-date-left">
            <b><%= GetKeyResult("TODAY")%>:</b>  <asp:Label ID="lblLoginTime" runat="server" ></asp:Label>
            </div>  
            <div class="today-date-right">
            <%= GetKeyResult("YOUARELOGGEDINAS")%>: <b><asp:Label ID="lblLoginUser" runat="server" ></asp:Label></b>
            </div>  
        </div>  --%>      
        <!--today-date ENDS HERE-->
        
        <!--request-step-mainbody START HERE-->
        <div class="request-step-mainbody">
        <!--request-stepbody START HERE-->
        <div class="request-step4-stepbody">        
            <div class="request-step4-step1">
            <span><%= GetKeyResult("STEP1")%>. </span><%= GetKeyResult("BASKETREQUEST")%>
            </div>
            <div class="request-step4-step2">
            <span><%= GetKeyResult("STEP2")%>.</span> <%= GetKeyResult("EXTRAINFO")%>
            </div>
            <div class="request-step4-step3">
           <span><%= GetKeyResult("STEP3")%>.</span> <%= GetKeyResult("RECAP")%>
            </div>
           <div class="request-step4-step4">
          <span><%= GetKeyResult("STEP4")%>.</span> <%= GetKeyResult("SENDING")%>
            </div>    
        </div>
        <div style="padding-top:20px;">
        <div id="divMessage" runat="server" ></div>

            <!--request-stepbody ENDS HERE-->
         </div></div>
         <div class="booking-detailsRE" id="Divdetails" runat="server">
        <!--request-step3-body ENDS HERE-->
        <table width="100%" cellpadding="1" cellspacing="1" border="0" class="one">
            <tr>
                <td>
                    <b><%= GetKeyResult("CONTACTNAME") %>:</b>
                </td>
                <td>
                    <asp:Label ID="lblContactPerson" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    <b><%= GetKeyResult("PHONENO") %>:</b>
                </td>
                <td>
                    <asp:Label ID="lblContactPhone" runat="server" Text=""></asp:Label>
                </td>
                <td colspan="2">
                    <b><%= GetKeyResult("EMAIL") %> :</b> <asp:label id="lblConatctpersonEmail" runat="server"></asp:label>
                </td>
            </tr>
            <tr>
                <td>
                    <b><%= GetKeyResult("DATEFROM") %>:</b>
                </td>
                <td>
                    <asp:Label ID="lblFromDt" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    <b><%= GetKeyResult("TO") %>:</b>
                </td>
                <td>
                    <asp:Label ID="lblToDate" runat="server" Text=""></asp:Label>
                </td>
                <td colspan="2">
                    <b><%= GetKeyResult("DURATION") %>:</b>
                    <asp:Label ID="lblBookedDays" runat="server" Text=""></asp:Label>
                    <asp:Label ID="lblBookedDays1" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <b><%= GetKeyResult("CONTACTADDRESS") %></b>
                </td>
                <td colspan="5">
                    <asp:Label ID="lblContactAddress" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <asp:Repeater ID="rptHotel" runat="server" OnItemDataBound="rptHotel_ItemDataBound">
                <ItemTemplate>
                    <tr bgcolor="#E3F0F1">
                        <td colspan="6">
                            <h1 class="first">
                                <asp:Label ID="lblHotelName" runat="server"></asp:Label></h1>
                        </td>
                    </tr>
                    <tr bgcolor="#CCD8D8" valign="top">
                        <td align="left" valign="top" colspan="2">
                            <b><%= GetKeyResult("MEETINGROOMS") %> </b>
                        </td>
                        <td align="left" valign="top" colspan="2">
                            <b><%= GetKeyResult("DESCRIPTION") %></b>
                        </td>
                        <td align="center" valign="top">
                            <b><%= GetKeyResult("QTY") %></b>
                        </td>
                        <td align="left" valign="top">
                            <b><%= GetKeyResult("CATEGORY") %> </b>
                        </td>
                    </tr>
                    <asp:Repeater ID="rptMeetingroom" runat="server" OnItemDataBound="rptMeetingroom_ItemDataBound">
                        <ItemTemplate>
                            <tr bgcolor="#E3F0F1">
                                <td align="left" colspan="2">
                                    <asp:Label ID="lblMeetingRoomName" runat="server"></asp:Label>
                                </td>
                                <td align="left" colspan="2">
                                    <p>
                                        <asp:Label ID="lblConfigurationType" runat="server"></asp:Label></p>
                                    <p>
                                        <asp:Label ID="lblMaxandMinCapacity" runat="server">50-60</asp:Label></p>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblIsMain" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </ItemTemplate>
            </asp:Repeater>
            <asp:Panel ID="pnlPackage" runat="server">
                <tr bgcolor="#f1f8f8">
                    <td colspan="6">
                        <h1>
                            <span><%= GetKeyResult("PACKAGEREQUESTED") %></span></h1>
                    </td>
                </tr>
                <tr bgcolor="#ccd8d8" valign="top">
                    <td colspan="2">
                        &nbsp;
                    </td>
                    <td colspan="4">
                        <b><%= GetKeyResult("DESCRIPTION") %> </b>
                    </td>
                </tr>
                <tr bgcolor="#e3f0f1">
                    <td colspan="2">
                        <asp:Label ID="lblPackageName" runat="server"></asp:Label>
                    </td>
                    <td colspan="4">
                        <asp:Label ID="lblpackageDescription" runat="server"></asp:Label>
                    </td>
                </tr>
                <asp:Repeater ID="rptPackageItem" runat="server" OnItemDataBound="rptPackageItem_ItemDataBound">
                    <HeaderTemplate>
                        <tr  bgcolor="#ccd8d8" valign="top">
                            <td colspan="2">
                            </td>
                            <td colspan="3">
                                <b><%= GetKeyResult("DESCRIPTION") %> </b>
                            </td>
                            <td>
                                <b><%= GetKeyResult("QTY") %> </b>
                            </td>
                        </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr bgcolor="#e3f0f1">
                            <td colspan="2">
                                <asp:Label ID="lblItemName" runat="server"></asp:Label>
                                    
                            </td>
                            <td colspan="3">
                                <asp:Label ID="lblDescription" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtQuantity" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
               
                <asp:Panel ID="pnlExtra" runat="server" Width="100%">
                    <tr bgcolor="#f1f8f8">
                        <td colspan="6">
                            <h1><%= GetKeyResult("EXTRAS") %></h1>
                        </td>
                    </tr>
                    <asp:Repeater ID="rptExtras" runat="server" OnItemDataBound="rptExtras_ItemDataBound">
                        <HeaderTemplate>
                            <tr bgcolor="#ccd8d8" valign="top">
                                <td colspan="2">
                                </td>
                                <td colspan="3">
                                    <b><%= GetKeyResult("DESCRIPTION") %>  </b>
                                </td>
                                <td>
                                    <b><%= GetKeyResult("QTY") %>  </b>
                                </td>
                            </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr bgcolor="#e3f0f1">
                                <td colspan="2">
                                    <asp:Label ID="lblItemName" runat="server"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </asp:Panel>
                <asp:Panel ID="pnlFoodAndBravrages" runat="server">
                    <tr bgcolor="#f1f8f8">
                        <td colspan="6">
                            <h1 style="overflow: hidden">
                                <span><%= GetKeyResult("CHOICE2BUILDYOURMEETING")%></span></h1>
                        </td>
                    </tr>
                    <tr bgcolor="#ccd8d8" valign="top">
                        <td colspan="2">
                            &nbsp;
                        </td>
                        <td colspan="3">
                            <b><%= GetKeyResult("DESCRIPTION")%> </b>
                        </td>
                        <td>
                          <b>  <%= GetKeyResult("QTY")%></b>
                        </td>
                    </tr>
                    <asp:Repeater ID="rptFoodandBravragesDay" runat="server" OnItemDataBound="rptFoodandBravragesDay_ItemDataBound">
                        <ItemTemplate>
                            <tr bgcolor="#f1f8f8">
                                <td colspan="6">
                                    <h3>
                                        <b><%= GetKeyResult("DAY")%></b>
                                        <asp:Label ID="lblSelectDay" runat="server"></asp:Label></h3>
                                </td>
                            </tr>
                            <asp:Repeater ID="rptFoodandBravragesItem" runat="server" OnItemDataBound="rptFoodandBravragesItem_ItemDataBound">
                                <ItemTemplate>
                                    <tr bgcolor="#e3f0f1">
                                        <td colspan="2">
                                            <asp:Label ID="lblItemName" runat="server"></asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ItemTemplate>
                    </asp:Repeater>
                </asp:Panel>
            </asp:Panel>
            
            <asp:Panel ID="pnlEquipment" runat="server">
                <tr bgcolor="#f1f8f8">
                    <td colspan="6">
                        <h1><%= GetKeyResult("EQUIPMENT")%></h1>
                    </td>
                </tr>
                <tr bgcolor="#ccd8d8" valign="top">
                    <td colspan="2">
                        &nbsp;
                    </td>
                    <td colspan="3">
                        <b><%= GetKeyResult("DESCRIPTION")%> </b>
                    </td>
                    <td>
                        <b><%= GetKeyResult("QTY")%> </b>
                    </td>
                </tr>
                <asp:Repeater ID="rptEquipmentDay" runat="server" OnItemDataBound="rptEquipmentDay_ItemDataBound">
                    <ItemTemplate>
                            <tr bgcolor="#f1f8f8">
                                <td colspan="6">
                                    <h3>
                                        <%= GetKeyResult("DAY")%>
                                        <asp:Label ID="lblSelectDay" runat="server"></asp:Label></h3>
                                </td>
                            </tr>
                            <asp:Repeater ID="rptEquipmentItem" runat="server" OnItemDataBound="rptEquipmentItem_ItemDataBound">
                                <ItemTemplate>
                                    <tr bgcolor="#e3f0f1">
                                        <td colspan="2">
                                            <asp:Label ID="lblItemName" runat="server"></asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                    </ItemTemplate>
                </asp:Repeater>
            </asp:Panel>
            <asp:Panel ID="pnlOthers" runat="server">
                <tr bgcolor="#f1f8f8">
                    <td colspan="6">
                        <h1>
                            <%= GetKeyResult("OTHERS")%></h1>
                    </td>
                </tr>
                <tr bgcolor="#ccd8d8" valign="top">
                    <td colspan="2">
                        &nbsp;
                    </td>
                    <td colspan="3">
                        <b><%= GetKeyResult("DESCRIPTION")%> </b>
                    </td>
                    <td>
                        <b><%= GetKeyResult("QTY")%> </b>
                    </td>
                </tr>
                <asp:Repeater ID="rptOthersDay" runat="server" OnItemDataBound="rptOthersDay_ItemDataBound">
                    <ItemTemplate>
                            <tr bgcolor="#f1f8f8">
                                <td colspan="6">
                                    <h3>
                                        <%= GetKeyResult("DAY")%>
                                        <asp:Label ID="lblSelectDay" runat="server"></asp:Label></h3>
                                </td>
                            </tr>
                            <asp:Repeater ID="rptOthersItem" runat="server" OnItemDataBound="rptOthersItem_ItemDataBound">
                                <ItemTemplate>
                                    <tr bgcolor="#e3f0f1">
                                        <td colspan="2">
                                            <asp:Label ID="lblItemName" runat="server"></asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                    </ItemTemplate>
                </asp:Repeater>
            </asp:Panel>
            <tr>
            <td colspan="6">
            <%if (objRequest.IsAccomodation==true)
              { %>
                <table width="100%" bgcolor="#9cc6ca" cellspacing="1" cellpadding="0">
                        <tr bgcolor="#E3F0F1">
                            <td>
                                <h1>
                                    <%= GetKeyResult("ACCOMODATION")%>.</h1>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr bgcolor="#d4d9cc">
                                        <td colspan="2" style="border-top: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;
                                            padding: 5px">
                                            <%= GetKeyResult("BEDROOMSDATE")%>
                                        </td>
                                        <asp:Repeater ID="rptDays2" runat="server" OnItemDataBound="rptDays2_ItemDataBound">
                                            <ItemTemplate>
                                                <td valign="top"  align="center" style="border-left: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;
                                                    border-top: #bfd2a5 solid 1px; padding: 5px">
                                                    <asp:Label ID="lblDay" runat="server"></asp:Label>
                                                </td>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <asp:Literal ID="ltrExtendDays" runat="server"></asp:Literal>
                                    </tr>
                                    <tr bgcolor="#cee5ad">
                                        <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                            <%= GetKeyResult("QUANTITY")%>
                                        </td>
                                        <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                            <%= GetKeyResult("SINGLE")%>
                                        </td>
                                        <asp:Repeater ID="rptSingleQuantity" runat="server" OnItemDataBound="rptSingleQuantity_ItemDataBound">
                                            <ItemTemplate>
                                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                                    <asp:HiddenField ID="hdnDate" runat="server" />
                                                    <asp:Label ID="txtQuantitySDay" runat="server">0</asp:Label>
                                                </td>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <asp:Literal ID="ltrExtendSQuantity" runat="server"></asp:Literal>
                                    </tr>
                                    <tr bgcolor="#cee5ad">
                                        <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                            &nbsp;
                                        </td>
                                        <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                            <%= GetKeyResult("DOUBLE")%>
                                        </td>
                                        <asp:Repeater ID="rptDoubleQuantity" runat="server" OnItemDataBound="rptDoubleQuantity_ItemDataBound">
                                            <ItemTemplate>
                                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                                    <asp:HiddenField ID="hdnDate" runat="server" />
                                                    <asp:Label ID="txtQuantityDDay" runat="server">0</asp:Label>
                                                </td>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <asp:Literal ID="ltrExtendDQuantity" runat="server"></asp:Literal>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
            <%} %>
            </td>
            </tr>
             <tr>
                <td colspan="6">
                    <h1><%= GetKeyResult("SPECIALREQUEST")%></h1>
                </td>
            </tr>
            <tr bgcolor="#E3F0F1">
                <td colspan="6">
                    <asp:Label ID="lblSpecialRequest" runat="server">No special request</asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <style>
    .text-for-request{ border:#9bbb59 solid 2px; width:390px; padding:15px; margin:20px auto; text-align:center}
.text-for-request p{ padding:0px; margin:0px 0px 15px 0px;}
.text-for-request p span{ color:#1e497c; font-weight:bold;}
    </style>
    <%--<div class="text-for-request">
<p>Many thanks for your meeting request: YOUR -25 % SPECIAL DISCOUNT CODE FOR SMARTPHOTO.COM IS = <span>LMMR8562</span></p>
<p><span>Go to <a href="www.smartphoto.be">www.smartphoto.be</a></span></p>
<p style="font-size:10px"><span>Action valid until 31/10/12</span></p>
</div>--%>
</asp:Content>

<asp:Content ID="ContentLeftSearch" ContentPlaceHolderID="cntLeftSearch" runat="server" >
<uc2:LeftSearchPanel ID="LeftSearchPanel1" runat="server" />
</asp:Content>