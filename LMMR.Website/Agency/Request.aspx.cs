﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;

public partial class Agency_Request : BasePage
{
    BookingManager bm = new BookingManager();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentAgencyUserID"] != null)
        {
            if (Session["RequestID"] != null)
            {
                SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["RequestID"]));
                if (st != null)
                {
                    if (st.CurrentStep == 1)
                    {
                        Server.Transfer("~/Agency/RequestStep1.aspx");
                    }
                    else if (st.CurrentStep == 2)
                    {
                        Server.Transfer("~/Agency/RequestStep2.aspx");
                    }
                    else if (st.CurrentStep == 3)
                    {
                        Server.Transfer("~/Agency/RequestStep3.aspx");
                    }
                    else if (st.CurrentStep == 4)
                    {
                        Server.Transfer("~/Agency/RequestStep4.aspx");
                    }
                }
                else
                {
                    Response.Redirect("~/Agency/SearchBookingRequest.aspx");
                }
            }
            else
                Response.Redirect("~/Agency/SearchBookingRequest.aspx");
        }
        else 
            Response.Redirect(SiteRootPath + "login/english");
    }
}