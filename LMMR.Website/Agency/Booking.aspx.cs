﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
public partial class Agency_Booking : BasePage
{
    BookingManager bm = new BookingManager();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentAgencyUserID"] != null)
        {
            if (Session["SerachID"] != null)
            {
                SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
                if (st != null)
                {
                    if (st.CurrentStep == 1)
                    {
                        Server.Transfer("~/Agency/BookingStep1.aspx");
                    }
                    else if (st.CurrentStep == 2)
                    {
                        Server.Transfer("~/Agency/BookingStep2.aspx");
                    }
                    else if (st.CurrentStep == 3)
                    {
                        Server.Transfer("~/Agency/BookingStep3.aspx");
                    }
                    else if (st.CurrentStep == 4)
                    {
                        Server.Transfer("~/Agency/BookingStep4.aspx");
                    }
                }
                else
                {
                    Response.Redirect("~/Agency/SearchBookingRequest.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Agency/SearchBookingRequest.aspx");
            }
        }
        else
        {
            Response.Redirect(SiteRootPath + "login/english");
        }
    }
}