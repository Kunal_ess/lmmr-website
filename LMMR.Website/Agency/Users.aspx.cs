﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using System.Configuration;
using System.IO;

public partial class Agency_Users : System.Web.UI.Page
{
    public ManageAgentUser objAgent = new ManageAgentUser();
    EmailConfigManager em = new EmailConfigManager();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindAgentUserGrid();

        }

        ApplyPaging();
    }
    #region Function
    /// <summary>
    /// This function used to clear form.
    /// </summary>
    void clearForm()
    {
        txtFirstName.Text = "";
        txtLastName.Text = "";
        txtEmail.Text = "";
        txtReEmail.Text = "";
        txtPassword.Attributes.Add("value", "");
        txtrePassword.Attributes.Add("value", "");
        chkIsActive.Checked = true;
        txtEmail.ReadOnly = false;
        txtReEmail.ReadOnly = false;
        foreach (ListItem chk in checkboxlistAgent.Items)
        {
            chk.Selected = false;
        }
        hdnAgentId.Value = "0";
    }
    /// <summary>
    /// This function used to bind user agent with grid.
    /// </summary>
    void BindAgentUserGrid()
    {
        if (Session["CurrentAgencyUserID"] != null)
        {
            TList<Users> obj = objAgent.GetAllAgentByAgencyIdWithOutActive(Convert.ToInt64(Session["CurrentAgencyUserID"]));
            grdViewAgentUser.DataSource = obj;
            grdViewAgentUser.DataBind();
            if (obj.Count < 1)
            {
                DeleteButton.Visible = false;
            }
            else
            {
                DeleteButton.Visible = true;
            }

        }
    }
    /// <summary>
    /// This function used to bind to agent user.
    /// </summary>
    void BindAgentUserCheckBoxList()
    {
        if (Session["CurrentAgencyUserID"] != null)
        {
            List<mySelect> mList = objAgent.GetAllAgentByAgencyId(Convert.ToInt64(Session["CurrentAgencyUserID"])).Select(a => new mySelect() { Name = a.FirstName + " " + a.LastName, ID = a.UserId }).ToList();
            checkboxlistAgent.DataSource = mList;
            checkboxlistAgent.DataTextField = "Name";
            checkboxlistAgent.DataValueField = "ID";
            checkboxlistAgent.DataBind();
        }
    }

    /// <summary>
    /// This function used to bind agents.
    /// </summary>
    void BindAgentLinkDropdown()
    {
        if (Session["CurrentAgencyUserID"] != null)
        {
            List<mySelect> mList = objAgent.GetAllAgentByAgencyId(Convert.ToInt64(Session["CurrentAgencyUserID"])).Select(a => new mySelect() { Name = a.FirstName + " " + a.LastName, ID = a.UserId }).ToList();
            CheckBoxListLinkWith.DataSource = mList;
            CheckBoxListLinkWith.DataTextField = "Name";
            CheckBoxListLinkWith.DataValueField = "ID";
            CheckBoxListLinkWith.DataBind();
        }
    }


    private void ApplyPaging()
    {
        try
        {
            GridViewRow row = grdViewAgentUser.TopPagerRow;
            if (row != null)
            {
                PlaceHolder ph;
                LinkButton lnkPaging;
                LinkButton lnkPrevPage;
                LinkButton lnkNextPage;
                lnkPrevPage = new LinkButton();
                lnkPrevPage.CssClass = "pre";
                lnkPrevPage.Width = Unit.Pixel(73);
                lnkPrevPage.CommandName = "Page";
                lnkPrevPage.CommandArgument = "prev";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPrevPage);
                if (grdViewAgentUser.PageIndex == 0)
                {
                    lnkPrevPage.Enabled = false;

                }
                for (int i = 1; i <= grdViewAgentUser.PageCount; i++)
                {
                    lnkPaging = new LinkButton();
                    if (ViewState["CurrentPage"] != null)
                    {
                        if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    else
                    {
                        if (i == 1)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    lnkPaging.Width = Unit.Pixel(16);
                    lnkPaging.Text = i.ToString();
                    lnkPaging.CommandName = "Page";
                    lnkPaging.CommandArgument = i.ToString();
                    if (i == grdViewAgentUser.PageIndex + 1)
                        ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkPaging);
                }
                lnkNextPage = new LinkButton();
                lnkNextPage.CssClass = "nex";
                lnkNextPage.Width = Unit.Pixel(42);
                lnkNextPage.CommandName = "Page";
                lnkNextPage.CommandArgument = "next";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkNextPage);
                ph = (PlaceHolder)row.FindControl("ph");
                if (grdViewAgentUser.PageIndex == grdViewAgentUser.PageCount - 1)
                {
                    lnkNextPage.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            //logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Event
    protected void lnkButtonAddnew_Click(object sender, EventArgs e)
    {

        divmsg.Style.Add("display", "none");
        divmsg.Attributes.Add("class", "error");

        BindAgentUserCheckBoxList();
        if (checkboxlistAgent.Items.Count <= 0)
        {
            lblNoUser.Visible = true;
        }
        else
        {
            lblNoUser.Visible = false;
        }
        clearForm();
        DivAddUpdateAgentUser.Visible = true;
        lblHeader.Text = "create user account";
        UserDetails objdetai = objAgent.GetUserDetailByID(Convert.ToInt64(Session["CurrentAgencyUserID"])).FirstOrDefault();
        if (objdetai != null)
        {
            if (objdetai.CompanyName != null)
            {
                if (objdetai.CompanyName.Length > 3)
                {
                    lblUserID.Text = objdetai.CompanyName.Substring(0, 3) + objAgent.GetLastUserID();
                }
                else
                {
                    lblUserID.Text = objdetai.CompanyName + objAgent.GetLastUserID();
                }
            }
        }

        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + DivAddUpdateAgentUser.ClientID + "');});", true);
    }

    protected void lnkButtonModify_Click(object sender, EventArgs e)
    {
        divmsg.Style.Add("display", "none");
        divmsg.Attributes.Add("class", "error");

        DivAddUpdateAgentUser.Visible = true;
        BindAgentUserCheckBoxList();
        foreach (GridViewRow l in grdViewAgentUser.Rows)
        {
            CheckBox r = (CheckBox)l.FindControl("rbselect");
            if (r.Checked)
            {
                DataKey currentDataKey = this.grdViewAgentUser.DataKeys[l.RowIndex];
                hdnAgentId.Value = currentDataKey.Value.ToString();
            }
        }

        lblHeader.Text = "Modify user account";

        if (hdnAgentId.Value != "0")
        {
            ListItem itemToRemove = checkboxlistAgent.Items.FindByValue(hdnAgentId.Value);
            if (itemToRemove != null)
            {
                checkboxlistAgent.Items.Remove(itemToRemove);
            }

            if (checkboxlistAgent.Items.Count <= 0)
            {
                lblNoUser.Visible = true;
            }
            else
            {
                lblNoUser.Visible = false;
            }

            Users objuser = objAgent.GetUserID(Convert.ToInt64(hdnAgentId.Value));
            UserDetails objuserDetail = objAgent.GetUserDetailByID(Convert.ToInt64(hdnAgentId.Value)).FirstOrDefault();
            if (objuser != null)
            {
                lblUserID.Text = objuserDetail.CompanyName;
                txtFirstName.Text = objuser.FirstName;
                txtLastName.Text = objuser.LastName;
                txtEmail.Text = objuser.EmailId;
                txtEmail.ReadOnly = true;
                txtReEmail.Text = objuser.EmailId;
                txtReEmail.ReadOnly = true;
                txtPassword.Attributes.Add("value", PasswordManager.Decrypt(objuser.Password, true));
                txtrePassword.Attributes.Add("value", PasswordManager.Decrypt(objuser.Password, true));
                if (objuser.IsActive)
                {
                    chkIsActive.Checked = true;
                }
                else
                {
                    chkIsActive.Checked = false;
                }
                if (objuserDetail != null)
                {
                    if (objuserDetail.LinkWith != null && objuserDetail.LinkWith != "0")
                    {

                        foreach (ListItem obchebox in checkboxlistAgent.Items)
                        {
                            string[] agentId = objuserDetail.LinkWith.Split(',');
                            if (agentId.Length > 0)
                            {
                                int i = 0;
                                for (i = 0; i <= agentId.Length - 1; i++)
                                {
                                    if (obchebox.Value == agentId[i])
                                    {
                                        obchebox.Selected = true;
                                    }
                                }
                            }
                        }

                    }
                }

            }

        }
        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + DivAddUpdateAgentUser.ClientID + "');});", true);

    }

    protected void lnkButtonDelete_Click(object sender, EventArgs e)
    {
        divmsg.Style.Add("display", "none");
        divmsg.Attributes.Add("class", "error");

        foreach (GridViewRow l in grdViewAgentUser.Rows)
        {
            CheckBox r = (CheckBox)l.FindControl("rbselect");
            if (r.Checked)
            {
                DataKey currentDataKey = this.grdViewAgentUser.DataKeys[l.RowIndex];
                hdnAgentId.Value = currentDataKey.Value.ToString();
            }
        }

        if (hdnAgentId.Value != "0")
        {
            BindAgentLinkDropdown();
            ListItem itemToRemove = CheckBoxListLinkWith.Items.FindByValue(hdnAgentId.Value);
            if (itemToRemove != null)
            {
                CheckBoxListLinkWith.Items.Remove(itemToRemove);
            }
            UserDetails obj = objAgent.GetUserDetailByID(Convert.ToInt64(hdnAgentId.Value)).FirstOrDefault();
            if (obj != null)
            {
                if (obj.LinkWith == "0" || obj.LinkWith == null || obj.LinkWith == "")
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenDelete", "OpenDeletePopUp();", true);
                }
                else
                {
                    if (objAgent.DeleteAgent(Convert.ToInt64(hdnAgentId.Value)))
                    {
                        divmsg.Style.Add("display", "block");
                        divmsg.Attributes.Add("class", "succesfuly");
                        divmsg.InnerHtml = "User deleted successfully";
                        BindAgentUserGrid();
                        ApplyPaging();
                    }
                    else
                    {
                        divmsg.Style.Add("display", "none");
                        divmsg.Attributes.Add("class", "error");
                        divmsg.InnerHtml = "Please contact lmmt support.";
                    }
                }
            }
            DivAddUpdateAgentUser.Visible = false;
        }
    }

    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        string agentsID = "0";
        foreach (ListItem agt in CheckBoxListLinkWith.Items)
        {
            if (agt.Selected == true)
            {
                if (agentsID == "0")
                {
                    agentsID = agt.Value;
                }
                else
                {
                    agentsID = agentsID + "," + agt.Value;
                }
            }
        }
        if (objAgent.updateLinkwith(Convert.ToInt64(hdnAgentId.Value), agentsID))
        {
            if (objAgent.DeleteAgent(Convert.ToInt64(hdnAgentId.Value)))
            {
                divmsg.Style.Add("display", "block");
                divmsg.Attributes.Add("class", "succesfuly");
                divmsg.InnerHtml = "User deleted successfully";
            }
            else
            {
                divmsg.Style.Add("display", "none");
                divmsg.Attributes.Add("class", "error");
                divmsg.InnerHtml = "Please contact lmmt support.";
            }
        }
        else
        {
            divmsg.Style.Add("display", "none");
            divmsg.Attributes.Add("class", "error");
            divmsg.InnerHtml = "Please contact lmmt support.";
        }
        BindAgentUserGrid();
        ApplyPaging();
    }

    protected void lnkSave_Click(object sender, EventArgs e)
    {
        if (hdnAgentId.Value != "0")
        {
            Users objuser = objAgent.GetUserID(Convert.ToInt64(hdnAgentId.Value));
            UserDetails objuserDetail = objAgent.GetUserDetailByID(Convert.ToInt64(hdnAgentId.Value)).FirstOrDefault(); ;
            objuser.FirstName = txtFirstName.Text;
            objuser.LastName = txtLastName.Text;
            objuser.EmailId = txtEmail.Text;
            objuser.Password = PasswordManager.Encrypt(txtPassword.Text, true);
            string agentsID = string.Empty;
            foreach (ListItem agt in checkboxlistAgent.Items)
            {
                if (agt.Selected == true)
                {
                    if (agentsID == string.Empty)
                    {
                        agentsID = agt.Value;
                    }
                    else
                    {
                        agentsID = agentsID + "," + agt.Value;
                    }
                }
            }

            objuserDetail.LinkWith = agentsID;
            objuser.IsActive = chkIsActive.Checked;

            if (objAgent.UpdateUser(objuser, objuserDetail))
            {
                foreach (ListItem objChek in checkboxlistAgent.Items)
                {
                    long agentID = Convert.ToInt64(objChek.Value);
                    if (objChek.Selected == false)
                    {
                        UserDetails objUsers = objAgent.GetUserDetailByID(agentID).FirstOrDefault();
                        if (objUsers.LinkWith.Contains(hdnAgentId.Value))
                        {
                            string UpdateAgenId = "0";
                            string[] linkSplitID = objUsers.LinkWith.Split(',');
                            if (linkSplitID.Length > 0)
                            {
                                int i = 0;
                                for (i = 0; i <= linkSplitID.Length - 1; i++)
                                {
                                    if (linkSplitID[i] != hdnAgentId.Value)
                                    {
                                        if (linkSplitID[i] != "0")
                                        {
                                            if (UpdateAgenId == "0")
                                            {
                                                UpdateAgenId = linkSplitID[i];
                                            }
                                            else
                                            {
                                                UpdateAgenId = UpdateAgenId + "," + linkSplitID[i];
                                            }
                                        }
                                    }
                                }
                            }
                            objAgent.AddUsersLink(Convert.ToInt64(objChek.Value), UpdateAgenId);
                        }
                    }
                    else
                    {
                        UserDetails objUsers = objAgent.GetUserDetailByID(agentID).FirstOrDefault();
                        if (!objUsers.LinkWith.Contains(hdnAgentId.Value))
                        {
                            string UpdateAgenId = objuser.UserId.ToString();
                            string[] linkSplitID = objUsers.LinkWith.Split(',');
                            if (linkSplitID.Length > 0)
                            {
                                int i = 0;
                                for (i = 0; i <= linkSplitID.Length - 1; i++)
                                {
                                    if (linkSplitID[i] != hdnAgentId.Value)
                                    {
                                        if (linkSplitID[i] != "0")
                                        {
                                            if (UpdateAgenId == "0")
                                            {
                                                UpdateAgenId = linkSplitID[i];
                                            }
                                            else
                                            {
                                                UpdateAgenId = UpdateAgenId + "," + linkSplitID[i];
                                            }
                                        }
                                    }
                                }
                            }
                            objAgent.AddUsersLink(Convert.ToInt64(objChek.Value), UpdateAgenId);
                        }

                    }
                }
                divmsg.Style.Add("display", "block");
                divmsg.Attributes.Add("class", "succesfuly");
                divmsg.InnerHtml = "User info updated successfully";
            }
            else
            {
                divmsg.Style.Add("display", "none");
                divmsg.Attributes.Add("class", "error");
                divmsg.InnerHtml = "Please contact lmmt support.";
            }

            DivAddUpdateAgentUser.Visible = false;
            clearForm();
            BindAgentUserGrid();
            ApplyPaging();
        }
        else
        {
            Users objuser = new Users();
            UserDetails objuserDetail = new UserDetails();

            objuser.FirstName = txtFirstName.Text;
            objuser.LastName = txtLastName.Text;
            objuser.EmailId = txtEmail.Text;
            objuser.IsRemoved = false;
            objuser.Password = PasswordManager.Encrypt(txtPassword.Text, true);
            objuser.ParentId = Convert.ToInt64(Session["CurrentAgencyUserID"]);
            string agentsID = "0";
            foreach (ListItem agt in checkboxlistAgent.Items)
            {
                if (agt.Selected == true)
                {
                    if (agentsID == "0")
                    {
                        agentsID = agt.Value;
                    }
                    else
                    {
                        agentsID = agentsID + "," + agt.Value;
                    }
                }
            }
            objuserDetail.LinkWith = agentsID;
            objuserDetail.CompanyName = lblUserID.Text;
            objuser.IsActive = chkIsActive.Checked;
            objuser.Usertype = (int)Usertype.agencyuser;
            if (!objAgent.EmailExist(txtEmail.Text.Trim()))
            {
                if (objAgent.CreateUser(objuser, objuserDetail))
                {

                    foreach (ListItem objChek in checkboxlistAgent.Items)
                    {
                        long agentID = Convert.ToInt64(objChek.Value);
                        if (objChek.Selected == true)
                        {
                            UserDetails objUsers = objAgent.GetUserDetailByID(agentID).FirstOrDefault();
                            string UpdateAgenId = objuser.UserId.ToString();
                            string[] linkSplitID = objUsers.LinkWith.Split(',');
                            if (linkSplitID.Length > 0)
                            {
                                int i = 0;
                                for (i = 0; i <= linkSplitID.Length - 1; i++)
                                {
                                    if (linkSplitID[i] != "0")
                                    {
                                        if (UpdateAgenId == "0")
                                        {
                                            UpdateAgenId = linkSplitID[i];
                                        }
                                        else
                                        {
                                            UpdateAgenId = UpdateAgenId + "," + linkSplitID[i];
                                        }
                                    }

                                }
                            }
                            objAgent.AddUsersLink(Convert.ToInt64(objChek.Value), UpdateAgenId);
                        }
                    }

                    divmsg.Style.Add("display", "block");
                    divmsg.Attributes.Add("class", "succesfuly");
                    divmsg.InnerHtml = "User account created successfully";
                    sendMailTouser();
                }
                else
                {
                    divmsg.Style.Add("display", "none");
                    divmsg.Attributes.Add("class", "error");
                    divmsg.InnerHtml = "Please contact lmmt support.";
                }


                DivAddUpdateAgentUser.Visible = false;
                clearForm();
                BindAgentUserGrid();
                ApplyPaging();
            }
            else
            {
                divmessage.Style.Add("display", "block");
                divmessage.Attributes.Add("class", "error");
                divmessage.InnerHtml = "Email already exist";
            }
        }
    }

    void sendMailTouser()
    {
        try
        {
            EmailConfig eConfig = em.GetByName("Login details for agency users");
            SendMails mail = new SendMails();
            mail.FromEmail = ConfigurationManager.AppSettings["ClientSupportEmailID"];
            mail.ToEmail = txtEmail.Text.Trim();
            mail.Bcc = ConfigurationManager.AppSettings["WatchEMailID"];
            //string bodymsg = File.ReadAllText(Server.MapPath("~/EmailTemplets/registrationEmailTemplate.htm"));
            string bodymsg = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
            EmailConfigMapping emap = new EmailConfigMapping();
            emap = eConfig.EmailConfigMappingCollection.FirstOrDefault();
            if (emap != null)
            {
                bodymsg = bodymsg.Replace("@CONTENTS", emap.EmailContents);
            }
            else
            {
                emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                bodymsg = bodymsg.Replace("@CONTENTS", emap.EmailContents);
            }
            EmailValueCollection objEmailValues = new EmailValueCollection();
            mail.Subject = "LMMR login detail";
            foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForLoginDetailsOfAgencyUser(txtEmail.Text, txtPassword.Text, ConfigurationManager.AppSettings["Sender"]))
            {
                bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
            }
            mail.Body = bodymsg;
            mail.SendMail();
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }

    protected void lnkCancel_Click(object sender, EventArgs e)
    {
        clearForm();
        BindAgentUserGrid();
        ApplyPaging();
        DivAddUpdateAgentUser.Visible = false;
    }
    #endregion
    protected void grdViewAgentUser_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            long intDataKey = Convert.ToInt32(grdViewAgentUser.DataKeys[e.Row.RowIndex].Value);
            TList<UserDetails> objuserDetail = objAgent.GetUserDetailByID(intDataKey);
            Users objusers = e.Row.DataItem as Users;
            Label lblName = (Label)e.Row.FindControl("lblName");
            Label lblEmail = (Label)e.Row.FindControl("lblEmail");
            Label lblLinkWith = (Label)e.Row.FindControl("lblLinkWith");
            Label lblUserID = (Label)e.Row.FindControl("lblUserID");
            Label lblIsActive = (Label)e.Row.FindControl("lblIsActive");
            if (objusers.IsActive)
            {
                lblIsActive.Text = "Yes";
            }
            else
            {
                lblIsActive.Text = "No";
            }
            lblName.Text = objusers.FirstName + " " + objusers.LastName;
            lblEmail.Text = objusers.EmailId;
            if (objuserDetail != null)
            {
                if (objuserDetail[0].LinkWith != null && objuserDetail[0].LinkWith != "0" && objuserDetail[0].LinkWith != "")
                {
                    string[] agentid = objuserDetail[0].LinkWith.Split(',');
                    if (agentid.Length > 0)
                    {
                        int i = 0;
                        for (i = 0; i <= agentid.Length - 1; i++)
                        {
                            Users obj = new Users();
                            if (agentid[i] != "")
                            {
                                obj = objAgent.GetUserID(Convert.ToInt64(agentid[i]));
                                if (obj != null)
                                {
                                    if (lblLinkWith.Text == "")
                                    {
                                        lblLinkWith.Text = obj.FirstName + " " + obj.LastName;
                                    }
                                    else
                                    {
                                        lblLinkWith.Text = lblLinkWith.Text + " , " + obj.FirstName + " " + obj.LastName;
                                    }
                                }
                            }
                        }
                    }
                    lblUserID.Text = objuserDetail[0].CompanyName;
                }
                else
                {
                    lblLinkWith.Text = "Not Linked";
                    lblUserID.Text = objuserDetail[0].CompanyName;
                }
            }
        }
    }
    protected void grdViewAgentUser_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            grdViewAgentUser.PageIndex = e.NewPageIndex;
            ViewState["CurrentPage"] = e.NewPageIndex;
            BindAgentUserGrid();
            ApplyPaging();
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
}

class mySelect
{
    public string Name
    {
        get;
        set;
    }
    public long ID
    {
        get;
        set;
    }
}