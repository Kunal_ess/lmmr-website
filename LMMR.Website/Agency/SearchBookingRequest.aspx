﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Agency/Main.master" AutoEventWireup="true" CodeFile="SearchBookingRequest.aspx.cs" Inherits="Agency_SearchBookingRequest" %>
<%@ Register src="~/UserControl/Agency/SearchBookingPage.ascx" tagname="SearchPanel" tagprefix="uc2" %>
<%@ Register src="~/UserControl/Agency/AgenctClient.ascx" tagname="AgentClient" tagprefix="uc2" %>
<%@ Register Src="../UserControl/Agency/LeftSearchPanel.ascx" TagName="LeftSearchPanel"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMainBody" Runat="Server">
<uc2:AgentClient ID="AgentClient1" runat="server" />
<div id="map_canvas" style="width:100%;height:290px;" ></div>
<div id="mapMessgage" style="display:none;color:Red;">Please pin point on the map to change your location.</div>
<uc2:SearchPanel ID="SearchPanel1" runat="server" />



</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
<div id="leftSection">
    </div>
</asp:Content>
<asp:Content ID="ContentLeftSearch" ContentPlaceHolderID="cntLeftSearch" runat="server" >
<uc2:LeftSearchPanel ID="LeftSearchPanel1" runat="server" />
</asp:Content>

