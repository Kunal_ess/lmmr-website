﻿#region Namespaces Included

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Entities;
using LMMR.Business;

#endregion

public partial class Agency_Setting : System.Web.UI.Page
{
    #region Variable Declaration

    NewUser userObj = new NewUser();
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl + "/";
        }
    }
    #endregion

    #region PageLoad

    /// <summary>
    /// The Page load method.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LanguageID"] == null)
        {
            Session["LanguageID"] = 1;  // Setting language to default English.
        }
        if (Session["CurrentRestUserID"] == null)
        {
            Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
            Session.Abandon();
            if (l != null)
            {
                Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
            }
            else
            {
                Response.Redirect(SiteRootPath + "login/english");
            }
        }
        if (!IsPostBack)
        {
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
            fillStaticContent();
            fillForm();
        }
    }

    #endregion

    #region Methods

    /// <summary>
    /// This method binds the organisation country DropDownList.
    /// </summary>
    public void bindOrganisationCountryDDL()
    {
        organisationCountryDDL.DataTextField = "CountryName";
        organisationCountryDDL.DataValueField = "Id";
        organisationCountryDDL.DataSource = userObj.GetByAllCountry();
        organisationCountryDDL.DataBind();
        organisationCountryDDL.Items.Insert(0, new ListItem("--Select Country--", "0"));
    }

    /// <summary>
    /// This method binds the financial country DropDownList.
    /// </summary>
    public void bindFinancialCountryDDL()
    {
        financialCountryDDL.DataTextField = "CountryName";
        financialCountryDDL.DataValueField = "Id";
        financialCountryDDL.DataSource = userObj.GetByAllCountry();
        financialCountryDDL.DataBind();
        financialCountryDDL.Items.Insert(0,new ListItem("--Select Country--", "0"));
    }

    /// <summary>
    /// This method fills the form with information retrieved from database.
    /// </summary>
    public void fillForm()
    {
        Users user = userObj.GetUserByID(Convert.ToInt64(Session["CurrentRestUserID"]));
        UserDetails userDetails = userObj.getDetailsByID(Convert.ToInt64(user.UserId));
        FinancialInfo userFinancialInfo = userObj.getFinanceInfoByUserID(Convert.ToInt64(user.UserId));

        // Fill user details in the form

        if (userDetails != null)
        {
            lblAgencyNameText.Text = userDetails.CompanyName == null ? "" : userDetails.CompanyName;
            lblIataNoText.Text = userDetails.IataNo == null ? "" : userDetails.IataNo;
            lblLoginText.Text = user.EmailId == null ? "" : user.EmailId;
            ViewState["password"] = PasswordManager.Decrypt(user.Password == null ? "" : user.Password, true);
            txtPassword.Text = ViewState["password"].ToString();
            txtReTypePassword.Text = ViewState["password"].ToString();

            // Reading organisation section data-------Start--------//

            cPCountryCodeDDL.SelectedValue = userDetails.CountryCode == null ? "0" : userDetails.CountryCode;
            txtCPPhone.Text = userDetails.Phone == null ? "" : userDetails.Phone;
            txtAddress.Text = userDetails.Address == null ? "" : userDetails.Address;
            bindOrganisationCountryDDL();
            organisationCountryDDL.SelectedValue = userDetails.CountryId == null ? "0" : userDetails.CountryId.ToString();
            txtPostalCode.Text = userDetails.PostalCode == null ? "" : userDetails.PostalCode;
            txtCity.Text = userDetails.CityName == null ? "0" : userDetails.CityName;

            // Reading organisation section data-------End--------//


            // Reading contact person data-------Start--------//

            txtFirstName.Text = user.FirstName == null ? "" : user.FirstName;
            txtLastName.Text = user.LastName == null ? "" : user.LastName;
            lblEmailTxt.Text = user.EmailId == null ? "" : user.EmailId;
            //cPCountryCodeDDL.SelectedValue=userDetails.CountryCode
            //txtCPPhone.Text=;

            // Reading contact person data-------End--------//


            // Reading financial data-------Start--------//
            if (userFinancialInfo != null)
            {
                txtVatNo.Text = userFinancialInfo.VatNo == null ? "" : userFinancialInfo.VatNo;
                txtVatValue.Text = userFinancialInfo.VatValue == null ? "" : userFinancialInfo.VatValue.ToString();
                txtName.Text = userFinancialInfo.Name == null ? "" : userFinancialInfo.Name;
                txtDepartment.Text = userFinancialInfo.Department == null ? "" : userFinancialInfo.Department;
                txtCommunicationEmail.Text = userFinancialInfo.CommunicationEmail == null ? "" : userFinancialInfo.CommunicationEmail;
                CountryCodeFinancialDDL.SelectedValue = userFinancialInfo.CountryCode == null ? "0" : userFinancialInfo.CountryCode;
                txtFinancialPhone.Text = userFinancialInfo.Phone == null ? "" : userFinancialInfo.Phone;
                txtFinancialAddress.Text = userFinancialInfo.Address == null ? "" : userFinancialInfo.Address;
                bindFinancialCountryDDL();
                financialCountryDDL.SelectedValue = userFinancialInfo.CountryId == null ? "0" : userFinancialInfo.CountryId.ToString();
                txtFinancialPostalCode.Text = userFinancialInfo.PostalCode == null ? "" : userFinancialInfo.PostalCode;
                txtFinancialCity.Text = userFinancialInfo.CityName == null ? "0" : userFinancialInfo.CityName;
            }

            // Reading financial data-------End--------//

            // Reading data for internal purpose.

            ViewState["hearFromUs"] = userDetails.HearFromUs;
        }
    }

    /// <summary>
    /// This method returns the resultant string for the passed key
    /// </summary>
    /// <param name="key"></param>
    /// <returns>string</returns>
    public string GetKeyResult(string key)
    {
        return ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key);
    }

    /// <summary>
    /// This method fills static data considering the language.
    /// </summary>
    public void fillStaticContent()
    {
        cPCountryCodeDDL.Items.Insert(0, new ListItem(GetKeyResult("EXTENSION"), "0"));
        CountryCodeFinancialDDL.Items.Insert(0, new ListItem(GetKeyResult("EXTENSION"), "0"));
    }

    #endregion

    #region Events

    /// <summary>
    /// This is the event handler for save LinkButton.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtSave_Click(object sender, EventArgs e)
    {
        Users user = new Users();
        UserDetails details = new UserDetails();
        FinancialInfo financeInfo = new FinancialInfo();
        string status;
        long id = Convert.ToInt64(Session["CurrentRestUserID"]);

        // Reading and setting general user information-------Start-----------//

        user.FirstName = txtFirstName.Text;
        user.LastName = txtLastName.Text;
        user.EmailId = lblLoginText.Text;
        user.Usertype = 7;
        user.UserId = id;
        user.IsActive = true;
        user.Password = PasswordManager.Encrypt(txtPassword.Text, true);

        // Reading and setting general user information-------End-----------//


        // Reading and setting general user details-------Start-----------//

        details.CountryCode = cPCountryCodeDDL.SelectedItem.Value;
        details.Phone = txtCPPhone.Text;
        details.Address = txtAddress.Text;
        details.CountryId = Convert.ToInt64(organisationCountryDDL.SelectedItem.Value);
        details.PostalCode = txtPostalCode.Text;
        details.CityName = txtCity.Text.Trim();
        details.UserId = id;
        details.IataNo = lblIataNoText.Text;
        details.LanguageId = Convert.ToInt32(Session["LanguageID"]);
        details.HearFromUs = ViewState["hearFromUs"].ToString();
        details.VisitCount = 1;
        details.CompanyName = lblAgencyNameText.Text;

        // Reading and setting general user details-------End-----------//


        // Reading and setting user financial details-------Start-----------//

        financeInfo.VatNo = txtVatNo.Text;
        financeInfo.VatValue = Convert.ToInt64(txtVatValue.Text);
        financeInfo.Name = txtName.Text;
        financeInfo.Department = txtDepartment.Text;
        financeInfo.CommunicationEmail = txtCommunicationEmail.Text;
        financeInfo.CountryCode = CountryCodeFinancialDDL.SelectedItem.Text;
        financeInfo.Phone = txtFinancialPhone.Text;
        financeInfo.Address = txtFinancialAddress.Text;
        financeInfo.CountryId = Convert.ToInt32(financialCountryDDL.SelectedItem.Value);
        financeInfo.PostalCode = txtFinancialPostalCode.Text;
        financeInfo.CityName = txtFinancialCity.Text;
        status = userObj.update(user, details);
        financeInfo.UserId = user.UserId;

        // Reading and setting user financial details-------End-----------//

        // Updating information-------------Start--------//

        if (status == "Information updated successfully.")
        {
            status = userObj.update(financeInfo, user.UserId);
            if (status == "Information updated successfully.")
            {
                Session["status"] = status;
                Session["CurrentAgency"] = userObj.GetUserByID(id); // Updating the agency user session object.
                Session["CurrentRestUser"] = userObj.GetUserByID(user.UserId);
                Response.Redirect("ControlPanel.aspx");
            }
        }
        Response.Redirect("ControlPanel.aspx");

        // Updating information-------------End--------//

    }

    /// <summary>
    /// This is the event handler for Cancel button click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("ControlPanel.aspx");
    }

    /// <summary>
    /// The page pre render event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreRender(object sender, System.EventArgs e)
    {
        txtPassword.Attributes.Add("Value", ViewState["password"].ToString());
        txtReTypePassword.Attributes.Add("Value", ViewState["password"].ToString());
    }

    #endregion
    
}