﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using LMMR.Entities;
using LMMR.Data;
using LMMR.Business;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Configuration;
using log4net;
using log4net.Config;
using System.Xml;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;

public partial class Agency_SearchBooking : System.Web.UI.Page
{

    #region Variable
    ILog logger = log4net.LogManager.GetLogger(typeof(Agency_SearchBooking));
    VList<Viewbookingrequest> Vlistreq = new VList<Viewbookingrequest>();
    ViewBooking_Hotel objViewBooking = new ViewBooking_Hotel();
    HotelManager objHotelManager = new HotelManager();
    HotelInfo ObjHotelinfo = new HotelInfo();
    VList<Viewbookingrequest> vlistreq;
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl + "/";
        }
    }
    int userId;
    public int propBookRef
    {
        get;
        set;
    }
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        lbldate.Text = "Arrival Date:";

        if (string.IsNullOrEmpty(Convert.ToString(Request.QueryString["type"])))
        {
            Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
            Session.Abandon();
            if (l != null)
            {
                Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
            }
            else
            {
                Response.Redirect(SiteRootPath + "login/english");
            }
        }
        else
        {

            propBookRef = Convert.ToInt32(Request.QueryString["type"].ToString());

        }
        if (propBookRef == 0)
        {
            lblno.Text = "Book No:";
            
            CalendarExtender1.EndDate = DateTime.Now.AddDays(89);
            CalendarExtender2.EndDate = DateTime.Now.AddDays(89);
            CalendarExtender1.StartDate = DateTime.Now.AddDays(-59);
            CalendarExtender2.StartDate = DateTime.Now.AddDays(-59);
        }
        else
        {
            lblno.Text = "Req No:";
            
            CalendarExtender1.EndDate = DateTime.Now.AddDays(59);
            CalendarExtender2.EndDate = DateTime.Now.AddDays(59);
            CalendarExtender1.StartDate = DateTime.Now.AddDays(-59);
            CalendarExtender2.StartDate = DateTime.Now.AddDays(-59);
        }
        if (Session["CurrentAgencyUserID"] == null)
        {

            Response.Redirect("~/Default.aspx", false);
            return;

        }
        else
        {

            Users objUsers = (Users)Session["CurrentAgencyUser"];

            objUsers.LastLogin = DateTime.Now;
        }
        
        

        //userId = "493";//tempporarily static to be commented
        userId = Convert.ToInt32(Session["CurrentAgencyUserID"]);

        if (!IsPostBack)
        {
            ViewState["SearchAlpha"] = "all";
            txtFromdate.Text = "dd/mm/yy";
            txtTodate.Text = "dd/mm/yy";
            tbldownloaddata.Visible = true;
            BindBookingDetails();
            bindAllDDLs();
        }
        else
        {
            if ((txtFromdate.Text == "") || (txtTodate.Text == "dd/mm/yy"))
            {
                if ((ViewState["fromdate"] != "") && (ViewState["fromdate"] != null) && (ViewState["fromdate"] != "dd/mm/yy"))
                {
                    txtFromdate.Text = ViewState["fromdate"].ToString();
                }
            }
            if ((txtTodate.Text == "") || (txtTodate.Text == "dd/mm/yy"))
            {
                if ((ViewState["todate"] != "") && (ViewState["todate"] != null) && (ViewState["todate"] != "dd/mm/yy"))
                {
                    txtTodate.Text = ViewState["todate"].ToString();
                }
            }

            //BindBookingDetails();
            //bindAllDDLs();
        }

        //BindBookingDetails();

        ApplyPaging();
    }
    #endregion

    #region search
    protected void lbtSearch_Click(object sender, EventArgs e)
    {
        ViewState["SearchAlpha"] = "all";
        BindBookingDetails();
        ApplyPaging();
    }

    protected void lnkClear_Click(object sender, EventArgs e)
    {
        txtFromdate.Text = "dd/mm/yy";
        txtTodate.Text = "dd/mm/yy";
        txtclientname.Text = "";
        txtreqno.Text = "";
        ViewState["SearchAlpha"] = "all";
        BindBookingDetails();
        ApplyPaging();

    }
    private void ApplyPaging()
    {
        try
        {
            GridViewRow row = grvBookingDetails.TopPagerRow;
            if (row != null)
            {
                PlaceHolder ph;
                LinkButton lnkPaging;
                LinkButton lnkPrevPage;
                LinkButton lnkNextPage;
                lnkPrevPage = new LinkButton();
                lnkPrevPage.CssClass = "pre";
                lnkPrevPage.Width = Unit.Pixel(73);
                lnkPrevPage.CommandName = "Page";
                lnkPrevPage.CommandArgument = "prev";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPrevPage);
                if (grvBookingDetails.PageIndex == 0)
                {
                    lnkPrevPage.Enabled = false;

                }
                for (int i = 1; i <= grvBookingDetails.PageCount; i++)
                {
                    lnkPaging = new LinkButton();
                    if (ViewState["CurrentPage"] != null)
                    {
                        if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    else
                    {
                        if (i == 1)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    lnkPaging.Width = Unit.Pixel(16);
                    lnkPaging.Text = i.ToString();
                    lnkPaging.CommandName = "Page";
                    lnkPaging.CommandArgument = i.ToString();
                    if (i == grvBookingDetails.PageIndex + 1)
                        ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkPaging);
                }
                lnkNextPage = new LinkButton();
                lnkNextPage.CssClass = "nex";
                lnkNextPage.Width = Unit.Pixel(42);
                lnkNextPage.CommandName = "Page";
                lnkNextPage.CommandArgument = "next";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkNextPage);
                ph = (PlaceHolder)row.FindControl("ph");
                if (grvBookingDetails.PageIndex == grvBookingDetails.PageCount - 1)
                {
                    lnkNextPage.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    protected void grvBookingDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            grvBookingDetails.PageIndex = e.NewPageIndex;
            ViewState["CurrentPage"] = e.NewPageIndex;
            grvBookingDetails.DataSource = Session["WhereClauseTemp"];
            grvBookingDetails.DataBind();
            bindAllDDLs();
            ApplyPaging();
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #region PageAlpha
    public void PageChange(object sender, EventArgs e)
    {
        try
        {
            HtmlAnchor anc = (sender as HtmlAnchor);
            anc.Style.Add("class", "select");
            var alpha = anc.InnerHtml;
            string get = alpha.Trim();
            ViewState["SearchAlpha"] = get;
            VList<Viewbookingrequest> objBooking = new VList<Viewbookingrequest>();
            objBooking = (VList<Viewbookingrequest>)Session["WhereClause"];
            ViewState["CurrentPage"] = null;
            if (get != "all")
            {
                Session["WhereClauseTemp"] = objBooking.FindAll(a => a.HotelName.ToLower().StartsWith(get.ToLower()));
                grvBookingDetails.DataSource = Session["WhereClauseTemp"];
                grvBookingDetails.DataBind();
                if (grvBookingDetails.Rows.Count > 0)
                {
                    tbldownloaddata.Visible = true;
                }
                else
                {
                    tbldownloaddata.Visible = false;
                }
                lblTotalBooking.Text = Convert.ToString(grvBookingDetails.Rows.Count);
                ApplyPaging();
            }
            else
            {
                Session["WhereClauseTemp"] = objBooking;
                grvBookingDetails.DataSource = objBooking;
                grvBookingDetails.DataBind();
                if (grvBookingDetails.Rows.Count > 0)
                {
                    tbldownloaddata.Visible = true;
                }
                else
                {
                    tbldownloaddata.Visible = false;
                }
                lblTotalBooking.Text = Convert.ToString(objBooking.Count);
                ApplyPaging();
            }
            bindAllDDLs();
            DivrequestDetail.Visible = false;
            divbookingdetails.Visible = false;
        }
        catch (Exception ex)
        {
            //logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion
    #endregion

    #region BindGrid for Booking
    /// <summary>
    /// Method to bind the grid
    /// </summary>    
    protected void BindBookingDetails()
    {
        try
        {
            string strUsers = string.Empty;
            ManageAgentUser objAgent = new ManageAgentUser();
            Viewstatistics objview = new Viewstatistics();
            UserDetails objUsers = objAgent.GetUserDetailByID(Convert.ToInt64(userId)).FirstOrDefault();
            strUsers = userId.ToString();
            if (strUsers == string.Empty)
            {
                strUsers = objUsers.LinkWith;
            }
            else
            {
                if (objUsers != null)
                {
                    strUsers = strUsers + "," + objUsers.LinkWith;
                }                
            }
            if (strUsers.EndsWith(","))
            {
                strUsers = strUsers.Substring(0, strUsers.Length - 1);
            }
            #region user
            Users objuser = DataRepository.UsersProvider.GetByUserId(Convert.ToInt64(userId));
            string whereclaus = "";
            #endregion
            if (objuser.Usertype == (int)(Usertype.Agency))
            {

                whereclaus = "CreatorId IN (" + objview.GetAllAgencyUsers(Convert.ToInt32(userId)) + ")";
            }
            else
            {

                 whereclaus = "CreatorId IN (" + strUsers + ")";
            }
            string orderby = "Id DESC";

            ViewState["fromdate"] = txtFromdate.Text.ToString();
            ViewState["todate"] = txtTodate.Text.ToString();
            if ((txtFromdate.Text != "") && (txtTodate.Text != "dd/mm/yy"))
            {
                ViewState["fromdate"] = txtFromdate.Text.ToString();
                if (whereclaus.Length > 0)
                {
                    whereclaus += " and ";
                }
                DateTime startdate = new DateTime(Convert.ToInt32("20" + txtFromdate.Text.Split('/')[2]), Convert.ToInt32(txtFromdate.Text.Split('/')[1]), Convert.ToInt32(txtFromdate.Text.Split('/')[0]));
                whereclaus += ViewbookingrequestColumn.ArrivalDate + ">=  '" + startdate + "'";
                //  whereclaus += ViewbookingrequestColumn.ArrivalDate + " >='" + SearchPanel1.propFromDate + "'  and   ViewbookingrequestColumn.ArrivalDate >='" + SearchPanel1.propToDate + "',103) ";
            }
            if ((txtTodate.Text != "") && (txtTodate.Text != "dd/mm/yy"))
            {
                ViewState["todate"] = txtTodate.Text.ToString();
                if (whereclaus.Length > 0)
                {
                    whereclaus += " and ";
                }
                DateTime enddate = new DateTime(Convert.ToInt32("20" + txtTodate.Text.Split('/')[2]), Convert.ToInt32(txtTodate.Text.Split('/')[1]), Convert.ToInt32(txtTodate.Text.Split('/')[0]));
                whereclaus += ViewbookingrequestColumn.ArrivalDate + "<= '" + enddate + "'";
                //  whereclaus += ViewbookingrequestColumn.ArrivalDate + " >='" + SearchPanel1.propFromDate + "'  and   ViewbookingrequestColumn.ArrivalDate >='" + SearchPanel1.propToDate + "',103) ";
            }
            //--Booking/request Number----------------//
            if (!string.IsNullOrEmpty(txtreqno.Text))
            {
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += ViewbookingrequestColumn.Id + " = '" + txtreqno.Text + "'";
            }
            //--------End-----------------------------//
            //--- client
            if (!string.IsNullOrEmpty(txtclientname.Text))
            {
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += "(";
                if (rbtcientvenue.SelectedItem.Value == "1")
                {
                    whereclaus += ViewbookingrequestColumn.Contact + " like '" + txtclientname.Text + "%'";
                }
                else if (rbtcientvenue.SelectedItem.Value == "2")
                {
                    whereclaus += ViewbookingrequestColumn.HotelName + " like '" + txtclientname.Text + "%'  ";
                }


                whereclaus += ")";
            }
            if (propBookRef == 0)
            {
                //lblType.Text = "Booking#";
                //lblDate.Text = "Booking date";
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                lblHeader.Text = "Booking Result";
                whereclaus += "booktype = 0";
            }
            else
            {
                //lblType.Text = "Request#";
                //lblDate.Text = "Request date";
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                lblHeader.Text = "Request Result";
                whereclaus += "booktype in (1,2) ";
            }
            Vlistreq = objViewBooking.Bindgrid(whereclaus, orderby);
            if (Vlistreq.Count > 0)
            {
                tbldownloaddata.Visible = true;
            }
            else
            {
                tbldownloaddata.Visible = false;
            }
            grvBookingDetails.DataSource = Vlistreq;//.OrderBy(a => a.Id);
            grvBookingDetails.DataBind();
            //if (grvBookingDetails.Columns.Count > 0)
            //{
            //    grvBookingDetails.Columns[4].Visible = false;
            //}
	    

            bindAllDDLs();
            lblTotalBooking.Text = Convert.ToString(Vlistreq.Count);
            Session["WhereClause"] = Vlistreq;
            Session["WhereClauseTemp"] = Vlistreq;
            //  ApplyPaging();
            DivrequestDetail.Visible = false;
            divbookingdetails.Visible = false;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion



    #region Change booking/request status
    /// <summary>
    /// when check commsion was checked
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  
    protected void btnchkcommision_Click(object sender, EventArgs e)
    {
        try
        {

            LinkButton btnchkcomm = (LinkButton)sender;
            ViewState["ChkcommBookingID"] = btnchkcomm.CommandArgument;

            if (btnchkcomm.Text.ToLower() == "change status")
            {
                ModalPopupCheck.Show();
                if (propBookRef == 1)
                {
                    drpStatus.Visible = false;
                    ddlstatus.Visible = true;
                    ViewState["popup"] = "1";
                }
                else
                {
                    ViewState["popup"] = "0";
                    drpStatus.Visible = true;
                    ddlstatus.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Bind some related data of booking/request in rowdatabound
    protected void grvBookingDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if ((e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate) && (e.Row.RowType == DataControlRowType.DataRow || e.Row.RowType == DataControlRowType.Header))
            {
                //Now set the visibility of cell we want to hide to false
                if (propBookRef == 0)
                {
                    e.Row.Cells[9].Visible = true;
                    e.Row.Cells[10].Visible = true;
                    e.Row.Cells[11].Visible = true; 
                }
                else
                {
                    e.Row.Cells[9].Visible = false;
                    e.Row.Cells[10].Visible = false;
                    e.Row.Cells[11].Visible = false; 
                }
                
            }
        
            if (e.Row.RowType == DataControlRowType.Header)
            {
                Label lblDate = (Label)e.Row.FindControl("lblDate");
                Label lblType = (Label)e.Row.FindControl("lblType");
                if (propBookRef == 0)
                {
                    
                    lblType.Text = "Booking#";
                }
                else
                {
                    
                    lblType.Text = "Request#";
                }
            }
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
            {
                Viewbookingrequest data = e.Row.DataItem as Viewbookingrequest;
                Label lblCity = (Label)e.Row.FindControl("lblCity");
                Label lblCompany = (Label)e.Row.FindControl("lblCompany");
                Label lblConctName = (Label)e.Row.FindControl("lblConctName");
                Label lblDept = (Label)e.Row.FindControl("lblDept");
                HiddenField hdnCityId = (HiddenField)e.Row.FindControl("hdnCityId");
                City objCity = objHotelManager.GetByCityID(Convert.ToInt32(hdnCityId.Value));
                lblCity.Text = objCity.City;
                UserDetails ud = new UserManager().getUserbyid(Convert.ToInt64(data.CreatorId)).FirstOrDefault();
                if (ud != null)
                {
                    lblCompany.Text = ud.CompanyName;
                }
                UserDetails ud2 = new UserManager().getUserbyid(Convert.ToInt64(data.AgencyClientId)).FirstOrDefault();
                if (ud2 != null)
                {
                    lblDept.Text = ud2.CompanyName;
                }
                Users objusers = objHotelManager.GetByUserID(Convert.ToInt64(data.AgencyClientId));
                lblConctName.Text = (objusers.FirstName == null ? "" : objusers.FirstName);

                Label lblArrivalDt = (Label)e.Row.FindControl("lblArrivalDt");
                LinkButton lnkRefno = (LinkButton)e.Row.FindControl("lnkRefno");
                Label lblDepartureDt = (Label)e.Row.FindControl("lblDepartureDt");
                Label lblExpiryDt = (Label)e.Row.FindControl("lblExpiryDt");

                Label lblBookingDt = (Label)e.Row.FindControl("lblBookingDt");
                Label lblstatus = (Label)e.Row.FindControl("lblstatus");
                DateTime startTime = DateTime.Now;
                HyperLink PlanView = (HyperLink)e.Row.FindControl("linkviewPlan");
                HiddenField hdf = ((HiddenField)e.Row.FindControl("hdfImage"));
                string depdt = lblDepartureDt.Text;

                #region Bindrequeststaus
                //lblstatus.Text = Enum.GetName(typeof(BookingRequestStatus), Convert.ToInt32(lblstatus.ToolTip));
                #endregion



                #region frozen
                //if (lblstatus.Text == BookingRequestStatus.Frozen.ToString())
                //{                    
                //    e.Row.BackColor = System.Drawing.Color.LightGray;
                //}
                //if (lblstatus.Text == BookingRequestStatus.Expired.ToString())
                //{                    
                //    e.Row.BackColor = System.Drawing.Color.LightGray;
                //}
                #endregion


                Label lblCancellationLimit = (Label)e.Row.FindControl("lblCancellationLimit");
                Label lblCancelled = (Label)e.Row.FindControl("lblCancelled");
                LinkButton lbtCancel = (LinkButton)e.Row.FindControl("lbtCancel");
                double x = (Convert.ToDouble(getCancellationLimit(Convert.ToInt32(lblCancellationLimit.ToolTip))));
                lblCancellationLimit.Text = "N/A";

                string[] fragments = Regex.Split(lblArrivalDt.Text, "/");
                string newStr = "";
                newStr += fragments[1] + "/";
                newStr += fragments[0] + "/";
                newStr += fragments[2];
                DateTime arrivalDate = Convert.ToDateTime(newStr);
                if (arrivalDate < DateTime.Now.Date)
                {
                    //Do Nothing and let the checkbox be not visible.
                }
                else
                {
                    double days = Math.Abs(((DateTime.Now.Date - arrivalDate).TotalDays));
                    if (days >= Convert.ToInt32(getCancellationLimit(Convert.ToInt32(lblCancellationLimit.ToolTip))))
                    {
                        lbtCancel.Visible = true;
                        lblCancellationLimit.Text = (DateTime.ParseExact(lblArrivalDt.Text, "dd/MM/yyyy", null).AddDays(-(Convert.ToDouble(getCancellationLimit(Convert.ToInt32(lblCancellationLimit.ToolTip)))))).ToString("dd/MM/yyyy");
                    }
                }
                int requestStatus = objViewBooking.getRequestStatus(Convert.ToInt64(lnkRefno.Text));
                if (requestStatus == 2)
                {
                    lblCancelled.Text = "Cancelled";
                    lblCancelled.Visible = true;
                    lbtCancel.Visible = false;
                    lblCancellationLimit.Text = "N/A";

                }
                if (lblCancelled.Text != "Cancelled" && lbtCancel.Visible == false)
                {
                    lblCancelled.Text = "N/A";
                    lblCancelled.Visible = true;
                }



            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    protected void grvBookingDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (Convert.ToString(e.CommandName) == "ViewBookingDetail")
            {
                if (propBookRef == 0)
                {
                    divbookingdetails.Visible = true;
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + divbookingdetails.ClientID + "');});", true);
                    DivrequestDetail.Visible = false;
                    ucViewBookingDetail.BindBooking(Convert.ToInt32(e.CommandArgument));
                }
                else
                {


                    ViewBooking_Hotel objViewBooking = new ViewBooking_Hotel();
                    Booking obj = objViewBooking.getparticularbookingdetails(Convert.ToInt64(e.CommandArgument));
                    if (obj.BookType == 1)
                    {
                        requestDetails.BindHotel(Convert.ToInt32(e.CommandArgument));
                        BookingDetails1.Visible = false;
                        requestDetails.Visible = true;
                        DivrequestDetail.Visible = true;
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + DivrequestDetail.ClientID + "');});", true);
                    }
                    else if (obj.BookType == 2)
                    {
                        BookingDetails1.Visible = true;
                        BookingDetails1.BindBooking(Convert.ToInt32(e.CommandArgument));
                        requestDetails.Visible = false;

                        DivrequestDetail.Visible = true;
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + DivrequestDetail.ClientID + "');});", true);
                    }


                    //requestDetails.BindHotel(Convert.ToInt32(e.CommandArgument));
                    //DivrequestDetail.Visible = true;
                    //divbookingdetails.Visible = false;
                    //ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + DivrequestDetail.ClientID + "');});", true);
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region Change status of booking/request
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            Booking objbooking = objViewBooking.getparticularbookingdetails(Convert.ToInt64(ViewState["ChkcommBookingID"].ToString()));
            if (Convert.ToString(ViewState["popup"]) == "1")
            {
                objbooking.RequestStatus = Convert.ToInt32(ddlstatus.SelectedValue);
                if (objViewBooking.updatecheckComm(objbooking))
                {
                    ModalPopupCheck.Hide();
                    BindBookingDetails();
                }
            }
            else if (Convert.ToString(ViewState["popup"]) == "0")
            {
                objViewBooking.CancelBooking(Convert.ToInt64(ViewState["ChkcommBookingID"].ToString()));
                ModalPopupCheck.Hide();
                BindBookingDetails();
            }


        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }
    #endregion

    #region Cancel Button Click
    /// <summary>
    /// Modal popup cancel event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>  

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            ModalPopupCheck.Hide();
            ddlstatus.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    #endregion
    
    #region XLS
    #region For Booking/Request Details
    protected void lnkDownloadXLS_Click(object sender, EventArgs e)
    {
        //GridViewExportUtil.Export("Invoice.xls", this.result);
        try
        {
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=UserDetails.xls");
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.xls";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            Divdetails.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.End();
        }
        catch (Exception ex)
        {
            Page.RegisterStartupScript("aa", "<script language='javascript' type='text/javascript'>document.getElementsByTagName('html')[0].style.overflow = 'auto';jQuery('#Loding_overlay').hide();</script>");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    #region For Gridview
    protected void lnkDownloadXLSGRID_Click(object sender, EventArgs e)
    {
        //GridViewExportUtil.Export("Invoice.xls", this.result);
        try
        {
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=UserDetails.xls");
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.xls";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            grvBookingDetails.AllowPaging = false;
            grvBookingDetails.BorderWidth = 1;
            grvBookingDetails.BorderStyle = BorderStyle.Solid;
            grvBookingDetails.DataSource = Session["WhereClauseTemp"];
            grvBookingDetails.DataBind();
            Control phhide = grvBookingDetails.TopPagerRow.FindControl("ph");
            phhide.Visible = false;
            grvBookingDetails.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.End();
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            //Page.RegisterStartupScript("aa", "<script language='javascript' type='text/javascript'>document.getElementsByTagName('html')[0].style.overflow = 'auto';jQuery('#Loding_overlay').hide();</script>");
        }
    }
    #endregion
    #endregion

    #region PDF

    #region For Gridview
    protected void lnkSavePDFGRID_Click(object sender, EventArgs e)
    {
        try
        {
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=UserDetails.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            grvBookingDetails.AllowPaging = false;
            grvBookingDetails.BorderWidth = 1;
            grvBookingDetails.BorderStyle = BorderStyle.Solid;
            grvBookingDetails.DataSource = Session["WhereClauseTemp"];
            grvBookingDetails.DataBind();
            bindAllDDLs();
            Control phhide = grvBookingDetails.TopPagerRow.FindControl("ph");
            phhide.Visible = false;
            PrepareGridViewForExport(grvBookingDetails);
            grvBookingDetails.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            Page.RegisterStartupScript("aa", "<script language='javascript' type='text/javascript'>document.getElementsByTagName('html')[0].style.overflow = 'auto';jQuery('#Loding_overlay').hide();</script>");
        }




    }
    #endregion

    #region For Booking/Request Details
    protected void lnkSavePDF_Click(object sender, EventArgs e)
    {
        try
        {

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=UserDetails.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            PrepareGridViewForExport(Divdetails);
            Divdetails.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            Page.RegisterStartupScript("aa", "<script language='javascript' type='text/javascript'>document.getElementsByTagName('html')[0].style.overflow = 'auto';jQuery('#Loding_overlay').hide();</script>");
        }




    }
    #endregion

    #region VerifyRenderingInServerForm
    /// <summary>
    /// method to VerifyRenderingInServerForm
    /// </summary>

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    #endregion

    #region PrepareGridViewForExport
    /// <summary>
    /// method to PrepareGridViewForExport
    /// </summary>
    private void PrepareGridViewForExport(Control gv)
    {
        try
        {
            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {

                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(DropDownList))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    if ((gv.Controls[i] as DropDownList).SelectedItem.Text == "City")
                    {
                        l.Text = "City";
                    }
                    if ((gv.Controls[i] as DropDownList).SelectedItem.Text == "Venue")
                    {
                        l.Text = "Venue";
                    }
                    if ((gv.Controls[i] as DropDownList).SelectedItem.Text == "Meeting Room")
                    {
                        l.Text = "Meeting Room";
                    }
                    if ((gv.Controls[i] as DropDownList).SelectedItem.Text == "Booking Date")
                    {
                        l.Text = "Booking Date";
                    }
                    if ((gv.Controls[i] as DropDownList).SelectedItem.Text == "Arrival Date")
                    {
                        l.Text = "Arrival Date";
                    }
                    if ((gv.Controls[i] as DropDownList).SelectedItem.Text == "Value")
                    {
                        l.Text = "Value";
                    }

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(CheckBox))
                {

                    l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(HyperLink))
                {

                    l.Text = (gv.Controls[i] as HyperLink).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.Image))
                {

                    // l.Text = (gv.Controls[i] as Image).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareGridViewForExport(gv.Controls[i]);

                }

            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            logger.Error(ex);
        }

    }
    #endregion

    #region PrepareGridViewForExportDIV
    /// <summary>
    /// method to PrepareGridViewForExport a div
    /// </summary>
    private void PrepareDivForExport(Control gv)
    {
        try
        {

            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {

                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].GetType() == typeof(System.Web.UI.HtmlControls.HtmlAnchor))
                {

                    l.Text = (gv.Controls[i] as System.Web.UI.HtmlControls.HtmlAnchor).InnerText;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(DropDownList))
                {

                    l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(GridView))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    // gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Repeater))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Panel))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(Table))
                {

                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                    PrepareDivForExport(gv.Controls[i]);
                    //gv.Controls.Remove(gv.Controls[i]);

                    //gv.Controls.AddAt(i, l);

                }


                else if (gv.Controls[i].GetType() == typeof(CheckBox))
                {

                    l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(HyperLink))
                {

                    l.Text = (gv.Controls[i] as HyperLink).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                //else if (gv.Controls[i].GetType() == typeof(Label))
                //{

                //    l.Text = (gv.Controls[i] as Label).Text;

                //    gv.Controls.Remove(gv.Controls[i]);

                //    gv.Controls.AddAt(i, l);

                //}
                else if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.Image))
                {

                    // l.Text = (gv.Controls[i] as Image).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareDivForExport(gv.Controls[i]);

                }

            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
        //PrepareGridViewForExport();
        //PrepareGridViewForExport();

    }


    #endregion

    protected void lnkSaveRequestPdf_Click(object sender, EventArgs e)
    {
        try
        {
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=UserDetails.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);            
            PrepareGridViewForExport(DivdetailsRequest);
            DivdetailsRequest.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        catch (Exception ex)
        {
            Page.RegisterStartupScript("aa", "<script language='javascript' type='text/javascript'>document.getElementsByTagName('html')[0].style.overflow = 'auto';jQuery('#Loding_overlay').hide();</script>");
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

    }

    #endregion

    /// <summary>
    /// This is the event handler for the lbtCancel checkbox CheckedChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtCancel_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        objViewBooking.CancelBooking(Convert.ToInt64(lb.ToolTip));
        BindBookingDetails();

    }


    /// <summary>
    /// This method gets the cancellation limit for a particular hotel on the basis of its ID.
    /// </summary>
    /// <param name="hotelId"></param>
    /// <returns></returns>
    public string getCancellationLimit(int hotelId)
    {
        long policyId = objViewBooking.getSpecialPolicyId(hotelId);
        int cancellationLimit;
        if (policyId != 0)
        {
            cancellationLimit = Convert.ToInt32(objViewBooking.getPolicy(policyId).CancelationLimit);
        }
        else
        {
            long countryId = objViewBooking.getHotelCountryId(hotelId);
            cancellationLimit = Convert.ToInt32(objViewBooking.getDefaultPolicy(countryId).CancelationLimit);
        }
        return cancellationLimit.ToString();
    }

    /// <summary>
    /// This method calls the binding methods of all the DDL's in the header.
    /// </summary>
    public void bindAllDDLs()
    {
        bindcityDDL();
        bindHotelDDL();
        bindMeetingRoomDDL();
        bindbookingDateDDL();
        bindArrivalDateDDL();
        bindvalueDDL();
    }

    /// <summary>
    /// This method binds the city dropdownlist.
    /// </summary>
    public void bindcityDDL()
    {
        if (grvBookingDetails.Rows.Count > 0)
        {
            string whereclause = string.Empty;
            if (propBookRef == 0)
            {
                whereclause = "booktype in (0) ";
            }
            else
            {
                whereclause = "booktype in (1,2) ";
            }
            if (!string.IsNullOrEmpty(whereclause.Trim()))
            {
                whereclause += " and ";
            }
            whereclause += "CreatorId in (" + userId + ")"; // book type for request and booking
            vlistreq = objViewBooking.Bindgrid(whereclause, String.Empty).FindAllDistinct(ViewbookingrequestColumn.HotelId);

            GridViewRow header = grvBookingDetails.HeaderRow;
            DropDownList cityDDL = header.FindControl("cityDDL") as DropDownList;
            cityDDL.DataTextField = "City";
            cityDDL.DataValueField = "Id";
            cityDDL.DataSource = objViewBooking.getAllCities(vlistreq);
            cityDDL.DataBind();
            cityDDL.Items.Insert(0, new System.Web.UI.WebControls.ListItem("City", "0"));
        }
        
    }

    /// <summary>
    /// This method binds the hotel DDL.
    /// </summary>
    public void bindHotelDDL()
    {
        if (grvBookingDetails.Rows.Count > 0)
        {
            string whereclause = string.Empty;
            if (propBookRef == 0)
            {
                whereclause = "booktype in (0) ";
            }
            else
            {
                whereclause = "booktype in (1,2) ";
            }
            if (!string.IsNullOrEmpty(whereclause.Trim()))
            {
                whereclause += " and ";
            }
            whereclause += "CreatorId in (" + userId + ")"; // book type for request and booking
            vlistreq = objViewBooking.Bindgrid(whereclause, String.Empty).FindAllDistinct(ViewbookingrequestColumn.HotelId);
            GridViewRow header = grvBookingDetails.HeaderRow;
            DropDownList hotelDDL = header.FindControl("hotelDDL") as DropDownList;
            hotelDDL.DataTextField = "Name";
            hotelDDL.DataValueField = "Id";
            hotelDDL.DataSource = objViewBooking.getAllHotels(vlistreq);
            hotelDDL.DataBind();
            hotelDDL.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Venue", "0"));
        }
        
    }

    /// <summary>
    /// This method binds the meetingRoom DDL.
    /// </summary>
    public void bindMeetingRoomDDL()
    {
        if (grvBookingDetails.Rows.Count > 0)
        {
            string whereclause = string.Empty;
            if (propBookRef == 0)
            {
                whereclause = "booktype in (0) ";
            }
            else
            {
                whereclause = "booktype in (1,2) ";
            }
            if (!string.IsNullOrEmpty(whereclause.Trim()))
            {
                whereclause += " and ";
            }
            whereclause += "CreatorId in (" + userId + ")"; // book type for request and booking
            vlistreq = objViewBooking.Bindgrid(whereclause, String.Empty).FindAllDistinct(ViewbookingrequestColumn.Name);
            GridViewRow header = grvBookingDetails.HeaderRow;
            DropDownList meetingRoomDDL = header.FindControl("meetingRoomDDL") as DropDownList;
            meetingRoomDDL.DataTextField = "Name";
            meetingRoomDDL.DataValueField = "MainMeetingRoomId";
            meetingRoomDDL.DataSource = vlistreq;
            meetingRoomDDL.DataBind();
            meetingRoomDDL.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Meeting Room", "0"));
        }
        
    }

    /// <summary>
    /// This method binds bookingDate DDL.
    /// </summary>
    public void bindbookingDateDDL()
    {
        if (grvBookingDetails.Rows.Count > 0)
        {
            string whereclause = string.Empty;
            if (propBookRef == 0)
            {
                whereclause = "booktype in (0) ";
            }
            else
            {
                whereclause = "booktype in (1,2) ";
            }
            GridViewRow header = grvBookingDetails.HeaderRow;
            DropDownList bookingDateDDL = header.FindControl("bookingDateDDL") as DropDownList;
            if (!string.IsNullOrEmpty(whereclause.Trim()))
            {
                whereclause += " and ";
            }
            whereclause += "CreatorId in (" + userId + ")"; // book type for request and booking
            vlistreq = objViewBooking.Bindgrid(whereclause, String.Empty).FindAllDistinct(ViewbookingrequestColumn.BookingDate);
            List<string> dateList = new List<string>();
            foreach (Viewbookingrequest temp in vlistreq)
            {
                string dt = temp.BookingDate.ToString();
                string[] str = dt.Split(new char[] { ' ' });
                string[] formatted = Regex.Split(str[0], "/");
                string newStr = "";
                newStr += formatted[1] + "/";
                newStr += formatted[0] + "/";
                newStr += formatted[2];
                if (!dateList.Contains(newStr))
                    dateList.Add(newStr);
            }
            bookingDateDDL.DataSource = dateList;
            //bookingDateDDL.DataTextField = "Display";
            //bookingDateDDL.DataValueField = "Display";
            //bookingDateDDL.DataSource = vlistreq.Select(a => new { Display = Convert.ToDateTime(a.BookingDate).ToString("dd/M/yyyy") });
            bookingDateDDL.DataBind();
            if (propBookRef == 0)
            {
                bookingDateDDL.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Booking Date", "0"));
            }
            else
            {
                bookingDateDDL.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Request Date", "0"));
            }
           
        }
        
    }

    /// <summary>
    /// This method binds the arrival date DropDownList.
    /// </summary>
    public void bindArrivalDateDDL()
    {
        if (grvBookingDetails.Rows.Count > 0)
        {
            string whereclause = string.Empty;
            if (propBookRef == 0)
            {
                whereclause = "booktype in (0) ";
            }
            else
            {
                whereclause = "booktype in (1,2) ";
            }
            GridViewRow header = grvBookingDetails.HeaderRow;
            DropDownList arrivalDateDDL = header.FindControl("arrivalDateDDL") as DropDownList;
            if (!string.IsNullOrEmpty(whereclause.Trim()))
            {
                whereclause += " and ";
            }
            whereclause += "CreatorId in (" + userId + ")"; // book type for request and booking
            vlistreq = objViewBooking.Bindgrid(whereclause, String.Empty).FindAllDistinct(ViewbookingrequestColumn.ArrivalDate);
            List<string> dateList = new List<string>();
            foreach (Viewbookingrequest temp in vlistreq)
            {
                string dt = temp.ArrivalDate.ToString();
                string[] str = dt.Split(new char[] { ' ' });
                string[] formatted = Regex.Split(str[0], "/");
                string newStr = "";
                newStr += formatted[1] + "/";
                newStr += formatted[0] + "/";
                newStr += formatted[2];
                if (!dateList.Contains(newStr))
                    dateList.Add(newStr);
            }
            arrivalDateDDL.DataSource = dateList;
            //bookingDateDDL.DataTextField = "Display";
            //bookingDateDDL.DataValueField = "Display";
            //bookingDateDDL.DataSource = vlistreq.Select(a => new { Display = Convert.ToDateTime(a.BookingDate).ToString("dd/M/yyyy") });
            arrivalDateDDL.DataBind();
            arrivalDateDDL.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Arrival Date", "0"));
        }
    }

    /// <summary>
    /// This method binds value DDl.
    /// </summary>
    public void bindvalueDDL()
    {
        if (propBookRef == 0)
                {
                    if (grvBookingDetails.Rows.Count > 0)
                    {
                        string whereclause = string.Empty;
                        if (propBookRef == 0)
                        {
                            whereclause = "booktype in (0) ";
                        }
                        else
                        {
                            whereclause = "booktype in (1,2) ";
                        }
                        if (!string.IsNullOrEmpty(whereclause.Trim()))
                        {
                            whereclause += " and ";
                        }
                        whereclause += "CreatorId in (" + userId + ")"; // book type for request and booking
                        string orderby = ViewbookingrequestColumn.FinalTotalPrice + " DESC";
                        vlistreq = objViewBooking.Bindgrid(whereclause, orderby).FindAllDistinct(ViewbookingrequestColumn.FinalTotalPrice);
                        DataTable dt = new DataTable();
                        dt.Columns.Add("PriceForDDLText", typeof(string));
                        dt.Columns.Add("PriceForDDLValue", typeof(double));
                        foreach (Viewbookingrequest temp in vlistreq)
                        {
                            DataRow dr = dt.NewRow();
                            dr["PriceForDDLText"] = String.Format("{0:#,###}", temp.FinalTotalPrice);
                            dr["PriceForDDLValue"] = temp.FinalTotalPrice;
                            dt.Rows.Add(dr);
                        }

                        GridViewRow header = grvBookingDetails.HeaderRow;
                        DropDownList valueDDL = header.FindControl("valueDDL") as DropDownList;
                        valueDDL.DataTextField = "PriceForDDLText";
                        valueDDL.DataValueField = "PriceForDDLValue";
                        valueDDL.DataSource = dt; //.Select(a => new { FinalTotalPrice = String.Format("{0:#,###}",a.FinalTotalPrice) });
                        valueDDL.DataBind();
                        valueDDL.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Value", "0"));
                    }
        }
    }

    /// <summary>
    /// This is the event handler for city DDL SelectedIndexChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void cityDDL_selectedIndexChanged(object sender, EventArgs e)
    {
        string whereclause = string.Empty;
        if (propBookRef == 0)
        {
            whereclause = "booktype in (0) ";
        }
        else
        {
            whereclause = "booktype in (1,2) ";
        }
        try
        {
            DropDownList cityDDL = (DropDownList)grvBookingDetails.HeaderRow.FindControl("cityDDL");
            if (!string.IsNullOrEmpty(whereclause.Trim()))
            {
                whereclause += " and ";
            }
            whereclause += "CreatorId in (" + userId + ")"; // book type for request and booking
            string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
            VList<Viewbookingrequest> filteredSet = new VList<Viewbookingrequest>();
            vlistreq = objViewBooking.Bindgrid(whereclause, orderby);
            foreach (Viewbookingrequest temp in vlistreq)
            {
                long cityId = objViewBooking.getCityId(temp.HotelId);
                if (cityId == Convert.ToInt64(cityDDL.SelectedItem.Value))
                {
                    filteredSet.Add(temp);
                }
            }
            grvBookingDetails.DataSource = filteredSet;
            Session["WhereClauseTemp"] = filteredSet;
            grvBookingDetails.DataBind();
            bindAllDDLs();
            if (grvBookingDetails.Rows.Count > 0)
            {
                //divprint.Visible = true;
            }
            lblTotalBooking.Text = Convert.ToString(grvBookingDetails.Rows.Count);
            divbookingdetails.Visible = false;
            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This is the event handler for hotel DDL SelectedIndexChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void hotelDDL_selectedIndexChanged(object sender, EventArgs e)
    {
        string whereclause = string.Empty;
        if (propBookRef == 0)
        {
            whereclause = "booktype in (0) ";
        }
        else
        {
            whereclause = "booktype in (1,2) ";
        }
        try
        {
            DropDownList hotelDDL = (DropDownList)grvBookingDetails.HeaderRow.FindControl("hotelDDL");
            if (!string.IsNullOrEmpty(whereclause.Trim()))
            {
                whereclause += " and ";
            }
            
             whereclause += "CreatorId in (" + userId + ")" + " and HotelId = " + hotelDDL.SelectedItem.Value; // book type for request and booking
            string orderby = ViewbookingrequestColumn.FinalTotalPrice + " DESC";
            vlistreq = objViewBooking.Bindgrid(whereclause, orderby);
            grvBookingDetails.DataSource = vlistreq;
            Session["WhereClauseTemp"] = vlistreq;
            grvBookingDetails.DataBind();
            bindAllDDLs();

            if (grvBookingDetails.Rows.Count > 0)
            {
                //divprint.Visible = true;
            }
            lblTotalBooking.Text = Convert.ToString(grvBookingDetails.Rows.Count);
            divbookingdetails.Visible = false;
            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }


    }


    /// <summary>
    /// This is the event handler for meetingRoom DDL SelectedIndexChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void meetingRoomDDL_selectedIndexChanged(object sender, EventArgs e)
    {
        string whereclause = string.Empty;
        if (propBookRef == 0)
        {
            whereclause = "booktype in (0) ";
        }
        else
        {
            whereclause = "booktype in (1,2) ";
        }
        try
        {
            DropDownList meetingRoomDDL = (DropDownList)grvBookingDetails.HeaderRow.FindControl("meetingRoomDDL");
            if (!string.IsNullOrEmpty(whereclause.Trim()))
            {
                whereclause += " and ";
            }
            
             whereclause += "CreatorId in (" + userId + ")" + " and Name = '" + meetingRoomDDL.SelectedItem.Text + "'"; // book type for request and booking
            string orderby = ViewbookingrequestColumn.FinalTotalPrice + " DESC";
            vlistreq = objViewBooking.Bindgrid(whereclause, orderby);
            grvBookingDetails.DataSource = vlistreq;
            Session["WhereClauseTemp"] = vlistreq;
            grvBookingDetails.DataBind();
            bindAllDDLs();

            if (grvBookingDetails.Rows.Count > 0)
            {
                //divprint.Visible = true;
            }
            lblTotalBooking.Text = Convert.ToString(grvBookingDetails.Rows.Count);
            divbookingdetails.Visible = false;
            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This is the event handler for bookingDate DDL SelectedIndexChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void bookingDateDDL_selectedIndexChanged(object sender, EventArgs e)
    {
        string whereclause = string.Empty;
        if (propBookRef == 0)
        {
            whereclause = "booktype in (0) ";
        }
        else
        {
            whereclause = "booktype in (1,2) ";
        }
        try
        {
            DropDownList bookingDateDDL = (DropDownList)grvBookingDetails.HeaderRow.FindControl("bookingDateDDL");
            if (!string.IsNullOrEmpty(whereclause.Trim()))
        {
            whereclause += " and ";
        }
         whereclause += "CreatorId in (" + userId + ")"; // book type for request and booking
            string orderby = ViewbookingrequestColumn.FinalTotalPrice + " DESC";
            vlistreq = objViewBooking.Bindgrid(whereclause, orderby);
            VList<Viewbookingrequest> filtered = new VList<Viewbookingrequest>();
            foreach (Viewbookingrequest temp in vlistreq)
            {
                string dt = temp.BookingDate.ToString();
                string[] str = dt.Split(new char[] { ' ' });
                string[] formatted = Regex.Split(str[0], "/");
                string newStr = "";
                newStr += formatted[1] + "/";
                newStr += formatted[0] + "/";
                newStr += formatted[2];
                if (bookingDateDDL.SelectedValue == newStr)
                {
                    filtered.Add(temp);
                }
            }
            grvBookingDetails.DataSource = filtered;
            Session["WhereClauseTemp"] = filtered;
            grvBookingDetails.DataBind();
            bindAllDDLs();


            if (grvBookingDetails.Rows.Count > 0)
            {
                //divprint.Visible = true;
            }
            lblTotalBooking.Text = Convert.ToString(grvBookingDetails.Rows.Count);
            divbookingdetails.Visible = false;
            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This is the event handler for arrivalDate DDL SelectedIndexChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void arrivalDateDDL_selectedIndexChanged(object sender, EventArgs e)
    {
        string whereclause = string.Empty;
        if (propBookRef == 0)
        {
            whereclause = "booktype in (0) ";
        }
        else
        {
            whereclause = "booktype in (1,2) ";
        }
        try
        {
            DropDownList arrivalDateDDL = (DropDownList)grvBookingDetails.HeaderRow.FindControl("arrivalDateDDL");
            if (!string.IsNullOrEmpty(whereclause.Trim()))
            {
                whereclause += " and ";
            }
            whereclause += "CreatorId in (" + userId + ")"; // book type for request and booking
            string orderby = ViewbookingrequestColumn.FinalTotalPrice + " DESC";
            vlistreq = objViewBooking.Bindgrid(whereclause, orderby);
            VList<Viewbookingrequest> filtered = new VList<Viewbookingrequest>();
            foreach (Viewbookingrequest temp in vlistreq)
            {
                string dt = temp.ArrivalDate.ToString();
                string[] str = dt.Split(new char[] { ' ' });
                string[] formatted = Regex.Split(str[0], "/");
                string newStr = "";
                newStr += formatted[1] + "/";
                newStr += formatted[0] + "/";
                newStr += formatted[2];
                if (arrivalDateDDL.SelectedValue == newStr)
                {
                    filtered.Add(temp);
                }
            }
            grvBookingDetails.DataSource = filtered;
            Session["WhereClauseTemp"] = filtered;
            grvBookingDetails.DataBind();
            bindAllDDLs();


            if (grvBookingDetails.Rows.Count > 0)
            {
                //divprint.Visible = true;
            }
            lblTotalBooking.Text = Convert.ToString(grvBookingDetails.Rows.Count);
            divbookingdetails.Visible = false;
            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }

    /// <summary>
    /// This is the event handler for value DDL SelectedIndexChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void valueDDL_selectedIndexChanged(object sender, EventArgs e)
    {
        string whereclause = string.Empty;
        if (propBookRef == 0)
        {
            whereclause = "booktype in (0) ";
        }
        else
        {
            whereclause = "booktype in (1,2) ";
        }
        try
        {
            DropDownList valueDDL = (DropDownList)grvBookingDetails.HeaderRow.FindControl("valueDDL");
            if (!string.IsNullOrEmpty(whereclause.Trim()))
            {
                whereclause += " and ";
            }
            
             whereclause = "CreatorId in (" + userId + ")" + " and FinalTotalPrice = " + valueDDL.SelectedItem.Value; // book type for request and booking
            string orderby = ViewbookingrequestColumn.FinalTotalPrice + " DESC";
            vlistreq = objViewBooking.Bindgrid(whereclause, orderby);
            grvBookingDetails.DataSource = vlistreq;
            Session["WhereClauseTemp"] = vlistreq;
            grvBookingDetails.DataBind();
            bindAllDDLs();


            if (grvBookingDetails.Rows.Count > 0)
            {
                //divprint.Visible = true;
            }
            lblTotalBooking.Text = Convert.ToString(grvBookingDetails.Rows.Count);
            divbookingdetails.Visible = false;
            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
}