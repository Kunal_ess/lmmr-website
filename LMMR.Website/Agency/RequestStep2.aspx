﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Agency/Main.master" AutoEventWireup="true" CodeFile="RequestStep2.aspx.cs" Inherits="Agency_RequestStep2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="../UserControl/Agency/LeftSearchPanel.ascx" tagname="LeftSearchPanel" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cntMainBody" Runat="Server">
<!--today-date START HERE-->
    <%-- <div class="today-date">
        <div class="today-date-left">
            <b><%= GetKeyResult("TODAY")%>:</b> <asp:Label ID="lblDateLogin" runat="server"></asp:Label>
        </div>
        <div class="today-date-right">
            <%= GetKeyResult("YOUARELOGGEDINAS")%>: <b><asp:Label ID="lblUserName" runat="server"></asp:Label></b>
        </div>
    </div>--%>
    <!--today-date ENDS HERE-->
    <!--request-step-mainbody START HERE-->
    <div class="request-step2-step-mainbody">
        <!--request-stepbody START HERE-->
        <div class="request-step2-stepbody">
            <div class="request-step2-step1">
               <span><%= GetKeyResult("STEP1")%>. </span><%= GetKeyResult("BASKETREQUEST")%>
            </div>
            <div class="request-step2-step2">
                <span><%= GetKeyResult("STEP2")%>.</span> <%= GetKeyResult("EXTRAINFO")%>
            </div>
            <div class="request-step2-step3">
                <span><%= GetKeyResult("STEP3")%>.</span> <%= GetKeyResult("RECAP")%>
            </div>
            <div class="request-step2-step4">
                <span><%= GetKeyResult("STEP4")%>.</span> <%= GetKeyResult("SENDING")%>
            </div>
        </div>
        <!--request-stepbody ENDS HERE-->
        <!--request-heading1 START HERE-->
        <div class="request-heading1">
            <h1>
                <span><%= GetKeyResult("YOURBASKETREQUEST")%> </span><%= GetKeyResult("EXTRAINFO")%></h1>
        </div>
        <!--request-heading1 ENDS HERE-->
        <!--request-date-body START HERE-->
        <div class="request-date-body">
            <div class="request-arrival-date">
                <span><%= GetKeyResult("ARRIVALDATE")%>:</span>
                <asp:Label ID="lblArrival" runat="server"></asp:Label>
            </div>
            <div class="request-departure-date">
                <span><%= GetKeyResult("DEPARTUREDATE")%>:</span>
                <asp:Label ID="lblDeparture" runat="server"></asp:Label>
            </div>
            <div class="request-duration-day">
                <span><%= GetKeyResult("DURATION")%>:</span>
                <asp:Label ID="lblDuration" runat="server"></asp:Label>
            </div>
            <div class="request-participants1">
                <%--<span>Participants:</span> 45--%>
            </div>
        </div>
        <div class="extracontent-blue">
       <p><%= GetKeyResult("INTHISSECTIONYOUMESSAGE")%></p>
 
<p><%= GetKeyResult("IFYOUHAVESELECTEDMESSAGE")%></p>
        </div>
        <div id="errorMessage" class="error" style="display:none;" ></div>
        <!--request-date-body ENDS HERE-->
        <!--Hotels / Facility body START HERE-->
        <div class="hotels-facility-body">
            <h1>
                <%= GetKeyResult("VENUES")%></h1>
            <div class="hotels-facility-body-haeding">
                <div class="hotels-facility-body-haeding1">
                    <%= GetKeyResult("MEETINGROOMS")%>
                </div>
                <div class="hotels-facility-body-haeding2">
                    <%= GetKeyResult("DESCRIPTION")%>
                </div>
                <div class="hotels-facility-body-haeding3">
                   <%= GetKeyResult("QTY")%>
                </div>
                <div class="hotels-facility-body-haeding4">
                    <%= GetKeyResult("CHOOSECATEGORY")%>
                </div>
                <div class="hotels-facility-body-haeding5">
                    &nbsp;
                </div>
            </div>
            <ul>
                <asp:Repeater ID="rptHotels" runat="server" OnItemDataBound="rptHotels_ItemDataBound">
                    <ItemTemplate><asp:HiddenField ID="hdnHotelID" runat="server" />
                        <li>
                            <h3 class="first">
                                <span>
                                    <asp:Label ID="lblIndex" runat="server"></asp:Label>.</span>
                                <asp:Label ID="lblHotelName" runat="server"></asp:Label></h3>
                            <div class="hotels-facility-inner">
                                <ul>
                                    <asp:Repeater ID="rptMeetingRoom" runat="server" OnItemDataBound="rptMeetingRoom_ItemDataBound" OnItemCommand="dtLstMeetingroom_ItemCommand">
                                        <ItemTemplate><asp:HiddenField ID="hdnMeetingroomID" runat="server" />
                                            <li>
                                                <div class="hotels-facility-inner-box">
                                                    <div class="hotels-facility-inner-box1">
                                                        <span>
                                                            <asp:Label ID="lblIndex" runat="server"></asp:Label>.</span>
                                                            <asp:Label ID="lblMeetingRoomName" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="hotels-facility-inner-box2">
                                                        <p>
                                                            <asp:Label ID="lblConfigurationType" runat="server"></asp:Label></p>
                                                        <p>
                                                            <asp:Label ID="lblMaxandMinCapacity" runat="server" CssClass="minmax"></asp:Label></p>
                                                    </div>
                                                    <div class="hotels-facility-inner-box3">
                                                        <asp:TextBox ID="txtQuantity" runat="server" CssClass="inputbox" MaxLength="3"></asp:TextBox>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtQuantity"
                            ValidChars="0123456789" runat="server">
                        </asp:FilteredTextBoxExtender>
                                                    </div>
                                                    <div class="hotels-facility-inner-box4">
                                                        <asp:Label ID="lblIsMain" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="hotels-facility-inner-box5">
                                                        <asp:ImageButton ID="btnDelete" runat="server" OnClientClick="return confirm('Do you want to delete this meeting room?');"
                                                            ImageUrl="~/images/delete-ol-btn.png" CssClass="ImageFloat" CommandName="DeleteMeetingroom" />
                                                    </div>
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
        <!--Hotels / Facility body ENDS HERE-->
        <!--request-step2-body START HERE-->
        <div id="divOverlay" class="overlay" ></div>
        <div class="request-step2-body">
            <ul>
                <li>
                    <div class="request-step2-body-inner1" id="choice1" >
                        <h1>
                            <asp:RadioButton ID="rbtnChoice1" runat="server" GroupName="Package" onclick="return Choice1Selected();" Checked="true" />
                            &nbsp;<%= GetKeyResult("CHOICE1")%>. <span><%= GetKeyResult("BOOKYOURPACKAGE")%></span></h1>
                        <div class="request-step2-body-inner1-haeding">
                            <div class="request-step2-body-inner1-haeding1">
                                &nbsp;
                            </div>
                            <div class="request-step2-body-inner1-haeding2">
                                <%= GetKeyResult("DESCRIPTION")%>
                            </div>
                        </div>
                        <div class="request-step2-ul-body1">
                            <div class="request-step2-ul-body1-box">
                                <ul>
                                    <asp:Repeater ID="rptPackage" runat="server" OnItemDataBound="rptPackage_ItemDataBound">
                                        <ItemTemplate>
                                            <li>
                                                <div class="request-step2-ul-body1-box-inner">
                                                    <div class="request-step2-ul-body1-box1"><asp:HiddenField ID="hdnPackageID" runat="server" />
                                                        <asp:RadioButton ID="rbtnPackage" runat="server" GroupName="pp" onclick="return ShowHideChoice1(this);" />
                                                        <asp:Label ID="lblPackageName" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="request-step2-ul-body1-box2">
                                                        <asp:Label ID="lblpackageDescription" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                                <div id="OpenDiv" runat="server" class="OpenDiv">
                                                    <div class="request-step2-body-inner-haeding">
                                                        <div class="request-step2-body-inner-haeding1">
                                                            &nbsp;
                                                        </div>
                                                        <div class="request-step2-body-inner-haeding2">
                                                            <%= GetKeyResult("DESCRIPTION")%>
                                                        </div>
                                                        <div class="request-step2-body-inner-haeding3">
                                                            <%= GetKeyResult("QTY")%>
                                                        </div>
                                                    </div>
                                                    <div class="request-step2-ul-body">
                                                        <div class="request-step2-ul-body-box">
                                                            <ul>
                                                                <asp:Repeater ID="rptPackageItem" runat="server" OnItemDataBound="rptPackageItem_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <li><asp:HiddenField ID="hdnItemId" runat="server" />
                                                                            <div class="request-step2-ul-body-box-inner">
                                                                                <div class="request-step2-ul-body-box1">
                                                                                    &nbsp;<asp:Label ID="lblItemName" runat="server"></asp:Label>
                                                                                </div>
                                                                                <div class="request-step2-ul-body-box2">
                                                                                    <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                                                                </div>
                                                                                <div class="request-step2-ul-body-box3">
                                                                                    <asp:TextBox ID="txtQuantity" runat="server" CssClass="inputbox" MaxLength="3" ></asp:TextBox>
                                                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtQuantity"
                            ValidChars="0123456789" runat="server">
                        </asp:FilteredTextBoxExtender>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <li>
                                        <div class="request-step2-ul-body1-box-inner">
                                            <div class="request-step2-ul-body1-box1">
                                                <span style="display:none;"><asp:CheckBox ID="chkIsExtra" runat="server" onclick="return ShowExtra(this);" /></span>
                                                 <%= GetKeyResult("EXTRAS")%>
                                            </div>
                                            <div class="request-step2-ul-body1-box2">
                                                <%--<input type="text" class="inputbox">--%>
                                            </div>
                                        </div>
                                        <div id="IsExtraDiv" runat="server" class="IsExtraDiv">
                                            <div class="request-step2-body-inner-haeding">
                                                <div class="request-step2-body-inner-haeding1">
                                                    &nbsp;
                                                </div>
                                                <div class="request-step2-body-inner-haeding2">
                                                    <%= GetKeyResult("DESCRIPTION")%>
                                                </div>
                                                <div class="request-step2-body-inner-haeding3">
                                                    <%= GetKeyResult("QTY")%>
                                                </div>
                                            </div>
                                            <div class="request-step2-ul-body">
                                                <div class="request-step2-ul-body-box">
                                                    <ul>
                                                        <asp:Repeater ID="rptExtraItem" runat="server" OnItemDataBound="rptExtraItem_ItemDataBound" >
                                                            <ItemTemplate>
                                                                <li><asp:HiddenField ID="hdnItemId" runat="server" />
                                                                    <div class="request-step2-ul-body-box-inner">
                                                                        <div class="request-step2-ul-body-box1">
                                                                            &nbsp;<asp:Label ID="lblItemName" runat="server"></asp:Label>
                                                                        </div>
                                                                        <div class="request-step2-ul-body-box2">
                                                                            <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                                                        </div>
                                                                        <div class="request-step2-ul-body-box3">
                                                                            <asp:TextBox ID="txtQuantity" runat="server" CssClass="inputbox" MaxLength="2"></asp:TextBox>
                                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtQuantity"
                            ValidChars="0123456789" runat="server">
                        </asp:FilteredTextBoxExtender>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        <asp:Panel ID="pnlNoExtra" runat="server" >
                                                            <li>
                                                                <div class="request-step2-ul-body"><h3><%= GetKeyResult("NOEXTRAS")%></h3></div>
                                                            </li>
                                                        </asp:Panel>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="request-step2-body-inner" id="choice2" >
                        <h1>
                            <asp:RadioButton ID="rbtnBuildYourMeeting" runat="server" GroupName="Package" onclick="return Choice2Selected();"/>
                            &nbsp;<%= GetKeyResult("CHOICE2")%>. <span><%= GetKeyResult("CHOICE2BUILDYOURMEETING")%></span></h1>
                        <div class="request-step2-body-inner-haeding">
                            <div class="request-step2-body-inner-haeding1">
                                &nbsp;
                            </div>
                            <div class="request-step2-body-inner-haeding2">
                               <%= GetKeyResult("DESCRIPTION")%>
                            </div>
                            <div class="request-step2-body-inner-haeding3">
                                <%= GetKeyResult("QTY")%>
                            </div>
                        </div>
                        <asp:Repeater ID="rptDaysFoodAndBragrages" runat="server" OnItemDataBound="rptDaysFoodAndBragrages_ItemDataBound" >
                            <ItemTemplate>
                                <div class="request-step2-ul-body">
                                    <h3>
                                        <%= GetKeyResult("DAY")%> <asp:Label ID="lblSelectDay" runat="server" ></asp:Label></h3>
                                    <div class="request-step2-ul-body-box">
                                        <ul>
                                            <asp:Repeater ID="rptFoodAndBravrage" runat="server" OnItemDataBound="rptFoodAndBravrage_ItemDataBound">
                                                <ItemTemplate>
                                                    <li><asp:HiddenField ID="hdnItemId" runat="server" />
                                                        <div class="request-step2-ul-body-box-inner">
                                                            <div class="request-step2-ul-body-box1">
                                                                &nbsp;<asp:Label ID="lblItemName" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="request-step2-ul-body-box2">
                                                                <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="request-step2-ul-body-box3">
                                                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="inputbox" MaxLength="3"></asp:TextBox>
                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtQuantity"
                            ValidChars="0123456789" runat="server">
                        </asp:FilteredTextBoxExtender>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </div>
                                </div>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <div class="request-step2-ul-body">
                                    <h3>
                                        <%= GetKeyResult("DAY")%> <asp:Label ID="lblSelectDay" runat="server" ></asp:Label></h3>
                                    <div class="request-step2-ul-body-box">
                                        <ul>
                                            <asp:Repeater ID="rptFoodAndBravrage" runat="server" OnItemDataBound="rptFoodAndBravrage_ItemDataBound" >
                                                <ItemTemplate>
                                                    <li><asp:HiddenField ID="hdnItemId" runat="server" />
                                                        <div class="request-step2-ul-body-box-inner">
                                                            <div class="request-step2-ul-body-box1">
                                                                &nbsp;<asp:Label ID="lblItemName" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="request-step2-ul-body-box2">
                                                                <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="request-step2-ul-body-box3">                                                                
                                                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="inputbox" MaxLength="3"></asp:TextBox>
                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtQuantity"
                            ValidChars="0123456789" runat="server">
                        </asp:FilteredTextBoxExtender>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </div>
                                </div>
                            </AlternatingItemTemplate>
                        </asp:Repeater>
                    </div>
                </li>
                
                <li>
                    <div class="request-step2-body-inner">
                        <h1>
                            <%= GetKeyResult("EQUIPMENT")%></h1>
                        <div class="request-step2-body-inner-haeding">
                            <div class="request-step2-body-inner-haeding1">
                                &nbsp;
                            </div>
                            <div class="request-step2-body-inner-haeding2">
                                <%= GetKeyResult("DESCRIPTION")%>
                            </div>
                            <div class="request-step2-body-inner-haeding3">
                               <%= GetKeyResult("QTY")%>
                            </div>
                        </div>
                        <asp:Repeater ID="rptEquipmentsDay" runat="server" OnItemDataBound="rptEquipmentsDay_ItemDataBound" >
                            <ItemTemplate>
                                <div class="request-step2-ul-body">
                                    <h3>
                                        <%= GetKeyResult("DAY")%>  <asp:Label ID="lblSelectDay" runat="server" ></asp:Label></h3>
                                    <div class="request-step2-ul-body-box">
                                        <ul>
                                            <asp:Repeater ID="rptEquipment" runat="server" OnItemDataBound="rptEquipment_ItemDataBound" >
                                                <ItemTemplate>
                                                    <li><asp:HiddenField ID="hdnItemId" runat="server" />
                                                        <div class="request-step2-ul-body-box-inner">
                                                            <div class="request-step2-ul-body-box1">
                                                                &nbsp;<asp:Label ID="lblItemName" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="request-step2-ul-body-box2">
                                                                <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="request-step2-ul-body-box4">
                                                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="inputbox" MaxLength="3"></asp:TextBox>
                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtQuantity"
                            ValidChars="0123456789" runat="server">
                        </asp:FilteredTextBoxExtender>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </div>
                                </div>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <div class="request-step2-ul-body">
                                    <h3>
                                       <%= GetKeyResult("DAY")%>  <asp:Label ID="lblSelectDay" runat="server" ></asp:Label></h3>
                                    <div class="request-step2-ul-body-box">
                                        <ul>
                                            <asp:Repeater ID="rptEquipment" runat="server" OnItemDataBound="rptEquipment_ItemDataBound">
                                                <ItemTemplate>
                                                    <li><asp:HiddenField ID="hdnItemId" runat="server" />
                                                        <div class="request-step2-ul-body-box-inner">
                                                            <div class="request-step2-ul-body-box1">                                                                
                                                                &nbsp;<asp:Label ID="lblItemName" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="request-step2-ul-body-box2">
                                                                <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="request-step2-ul-body-box4">
                                                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="inputbox" MaxLength="3"></asp:TextBox>
                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtQuantity"
                            ValidChars="0123456789" runat="server">
                        </asp:FilteredTextBoxExtender>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </div>
                                </div>
                            </AlternatingItemTemplate>
                        </asp:Repeater>
                    </div>
                </li>
                <% if (AllOthersItems.Count>0)
                   { %>
                <li>
                    <div class="request-step2-body-inner" id="divrptOthersDay" runat="server">
                        <h1>
                            <%= GetKeyResult("OTHERS")%></h1>
                        <div class="request-step2-body-inner-haeding">
                            <div class="request-step2-body-inner-haeding1">
                                &nbsp;
                            </div>
                            <div class="request-step2-body-inner-haeding2">
                                <%= GetKeyResult("DESCRIPTION")%>
                            </div>
                            <div class="request-step2-body-inner-haeding3">
                               <%= GetKeyResult("QTY")%>
                            </div>
                        </div>
                        <asp:Repeater ID="rptOthersDay" runat="server" OnItemDataBound="rptOthersDay_ItemDataBound" >
                            <ItemTemplate>
                                <div class="request-step2-ul-body">
                                    <h3>
                                        <%= GetKeyResult("DAY")%>  <asp:Label ID="lblSelectDay" runat="server" ></asp:Label></h3>
                                    <div class="request-step2-ul-body-box">
                                        <ul>
                                            <asp:Repeater ID="rptOthers" runat="server" OnItemDataBound="rptOthers_ItemDataBound" >
                                                <ItemTemplate>
                                                    <li><asp:HiddenField ID="hdnItemId" runat="server" />
                                                        <div class="request-step2-ul-body-box-inner">
                                                            <div class="request-step2-ul-body-box1">
                                                                &nbsp;<asp:Label ID="lblItemName" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="request-step2-ul-body-box2">
                                                                <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="request-step2-ul-body-box4">
                                                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="inputbox" MaxLength="3"></asp:TextBox>
                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtQuantity"
                            ValidChars="0123456789" runat="server">
                        </asp:FilteredTextBoxExtender>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </div>
                                </div>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <div class="request-step2-ul-body">
                                    <h3>
                                       <%= GetKeyResult("DAY")%>  <asp:Label ID="lblSelectDay" runat="server" ></asp:Label></h3>
                                    <div class="request-step2-ul-body-box">
                                        <ul>
                                            <asp:Repeater ID="rptOthers" runat="server" OnItemDataBound="rptOthers_ItemDataBound" >
                                                <ItemTemplate>
                                                    <li><asp:HiddenField ID="hdnItemId" runat="server" />
                                                        <div class="request-step2-ul-body-box-inner">
                                                            <div class="request-step2-ul-body-box1">
                                                                &nbsp;<asp:Label ID="lblItemName" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="request-step2-ul-body-box2">
                                                                <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                                            </div>
                                                            <div class="request-step2-ul-body-box4">
                                                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="inputbox" MaxLength="3"></asp:TextBox>
                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtQuantity"
                            ValidChars="0123456789" runat="server">
                        </asp:FilteredTextBoxExtender>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </div>
                                </div>
                            </AlternatingItemTemplate>
                        </asp:Repeater>
                    </div>
                </li>
                <%} %>
                <li>
                    <div class="request-step2-body-inner" >
                        <h1>
                            <%= GetKeyResult("ACCOMODATION")%></h1>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                            <tr bgcolor="#d4d9cc">
                                <td colspan="2" style="border-top: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;
                                    padding: 5px">
                                    <%= GetKeyResult("BEDROOMSDATE")%>
                                </td>
                                <asp:Repeater ID="rptDays2" runat="server" OnItemDataBound="rptDays2_ItemDataBound">
                                    <ItemTemplate>
                                        <td valign="top" style="border-left: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;
                                            border-top: #bfd2a5 solid 1px; padding: 5px">
                                            <asp:Label ID="lblDay" runat="server"></asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendDays" runat="server"></asp:Literal>                                
                            </tr>
                            <tr bgcolor="#cee5ad">
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("QUANTITY")%>
                                </td>
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("SINGLE")%>
                                </td>
                                <asp:Repeater ID="rptSingleQuantity" runat="server" OnItemDataBound="rptSingleQuantity_ItemDataBound">
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                            <asp:HiddenField ID="hdnDate" runat="server" />
                                            <asp:TextBox ID="txtQuantitySDay" CssClass="inputboxsmall1" runat="server" TabIndex="1" MaxLength="3">0</asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtQuantitySDay"
                                                ValidChars="0123456789" runat="server">
                                            </asp:FilteredTextBoxExtender>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendSQuantity" runat="server"></asp:Literal>
                            </tr>
                            <tr bgcolor="#cee5ad">
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("DOUBLE")%>
                                </td>
                                <asp:Repeater ID="rptDoubleQuantity" runat="server" OnItemDataBound="rptDoubleQuantity_ItemDataBound">
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                            <asp:HiddenField ID="hdnDate" runat="server" />
                                            <asp:TextBox ID="txtQuantityDDay" CssClass="inputboxsmall1" runat="server" TabIndex="1" MaxLength="3">0</asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" TargetControlID="txtQuantityDDay"
                                                ValidChars="0123456789" runat="server">
                                            </asp:FilteredTextBoxExtender>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendDQuantity" runat="server"></asp:Literal>
                            </tr>
                        </table>
                    </div>
                </li>
            </ul>
        </div>
        <div class="request-step2-special-request">
            <h1>
                <%= GetKeyResult("SPECIALREQUEST")%></h1>
            <div class="request-step2-special-request-textareabox">
                <asp:TextBox ID="txtSpecialRequest" runat="server" TextMode="MultiLine" MaxLength="200"
                    CssClass="textareabox"></asp:TextBox>
            </div>
        </div>
    </div>
    <!--request-step2-body ENDS HERE-->
    <!--next button START HERE-->
        <div class="next-button">
         <asp:LinkButton ID="lnkPrevious" runat="server" CssClass="step-pre-blue-btn" 
                onclick="lnkPrevious_Click"><%= GetKeyResult("PREVIOUSSTEP")%></asp:LinkButton> &nbsp;<asp:LinkButton 
                ID="lnkNext" runat="server" CssClass="step-next-blue-btn" 
                onclick="lnkNext_Click"><%= GetKeyResult("NEXTSTEP")%></asp:LinkButton>
        </div>        
        <!--next button ENDS HERE-->
        <script language="javascript" type="text/javascript">
            jQuery(document).ready(function () {
                jQuery("#<%= txtSpecialRequest.ClientID %>").bind("focus", function () {
                    if ('<%= GetKeyResultForJavaScript("PLEASEWRITEHEREIFYOUHAVEASPECIALREQUESTFORTHISBOOKING") %>' == jQuery(this).val()) {
                        jQuery(this).val('');
                    }
                });
            });
        </script>
        <script language="javascript" type="text/javascript">
            jQuery(document).ready(function () {
                jQuery("input:text").bind("focus", function () {
                    var hasval = jQuery(this).val();
                    if (parseInt(hasval, 10) == 0) {
                        jQuery(this).val('');
                    }
                });
            });

            //Show Hide the Choice 1 Div Selection of package.
            function ShowHideChoice1(sender) {
                var idsender = sender.id;
                var str = idsender.substring(0, 51);
                var parents = jQuery("#" + sender.id).parent("div").parent("div").parent("li").parent("ul");
                //alert(parents.children("li input:checked").length);
                parents.children("li").each(function () {
                    var radios = jQuery(this).find("input:radio");
                    //alert(radios.html());
                    radios.attr("checked", false);
                    jQuery(this).children(".OpenDiv").hide();
                });

                jQuery("#" + sender.id).attr("checked", true);
                //jQuery("#" + str.replace("reppackage", "divOverlay")).hide();
                //jQuery("#" + str.replace("reppackage", "ChoicePackage")).attr("checked", true);
                jQuery("#" + idsender.replace("rbtnPackage", "OpenDiv")).slideDown("fast", function () {
                    //var obj = document.getElementById(str.replace("reppackage", "OpenDiv"));
                    //CreateOverlay(obj, 'choice2');
                    Choice1Selected();
                });
            }

            function ShowExtra(val) {
                if (val.checked) {
                    jQuery(".IsExtraDiv").slideDown("fast", function () { Choice1Selected(); });
                }
                else {
                    jQuery(".IsExtraDiv").slideUp("fast", function () { Choice1Selected(); });
                }
            }

            function Choice1Selected() {
                //alert("choice1");
                //        if (val.checked) {
                var choice2offset = jQuery("#choice2").offset();
                //alert(jQuery("#choice2"));
                jQuery("#divOverlay").height(jQuery("#choice2").height() - 45);
                jQuery("#divOverlay").width(jQuery("#choice2").width());
                jQuery("#divOverlay").css({ "left": choice2offset.left + "px", "top": (choice2offset.top + 45) + "px" });
                jQuery("#divOverlay").show();
                //        }
                //        else {
                //            var choice1offset = jQuery("#choice1").offset();
                //            jQuery("#divOverlay").height(jQuery("#choice1").height() - 10);
                //            jQuery("#divOverlay").width(jQuery("#choice1").width());
                //            jQuery("#divOverlay").css({ "left": choice2offset.left + "px", "top": (choice2offset.top + 10) + "px" });
                //        }
            }

            function Choice2Selected() {
                // alert("choice2");
                //        if (val.checked) {
                var choice1offset = jQuery("#choice1").offset();
                jQuery("#divOverlay").height(jQuery("#choice1").height() - 45);
                jQuery("#divOverlay").width(jQuery("#choice1").width());
                jQuery("#divOverlay").css({ "left": choice1offset.left + "px", "top": (choice1offset.top + 45) + "px" });
                jQuery("#divOverlay").show();
                //        }
                //        else {
                //            var choice2offset = jQuery("#choice2").offset();
                //            jQuery("#divOverlay").height(jQuery("#choice2").height() - 10);
                //            jQuery("#divOverlay").width(jQuery("#choice2").width());
                //            jQuery("#divOverlay").css({ "left": choice2offset.left + "px", "top": (choice2offset.top + 10) + "px" });
                //        }
            }
            jQuery(document).ready(function () {
                jQuery("#<%= lnkNext.ClientID %>").bind("click", function () {
                    var IsValid = true;
                    var errorMessage = "";
                    jQuery(".hotels-facility-body").find("input:text").each(function () {
                        var checkminmax = jQuery(this).parent().parent().find(".hotels-facility-inner-box2").find(".minmax").html();
                        var p = jQuery(this).val();
                        if (p == '' || p.length <= 0 || parseInt(p, 2) == 0 || isNaN(p)) {
                            if (errorMessage.length > 0) {
                                errorMessage += "<br/>";
                            }
                            errorMessage += '<%= GetKeyResultForJavaScript("PLEASEENTERVALIDVALUES")%>';
                            IsValid = false;
                        }
                        //                    else if (parseInt(p, 10) < parseInt(checkminmax.split('-')[0], 10) || parseInt(p, 10) > parseInt(checkminmax.split('-')[1], 10)) {
                        //                        if (errorMessage.length > 0) {
                        //                            errorMessage += "<br/>";
                        //                        }
                        //                        errorMessage += '<%= GetKeyResult("PLEASEENTERVALUEBETWEEN")%>' + checkminmax.split('-')[0] + ' <%= GetKeyResult("AND")%> ' + checkminmax.split('-')[1] + '.';
                        //                        IsValid = false;
                        //                    }
                    });
                    if (jQuery("#cntMainBody_cntMainbody_rbtnChoice1").is(":checked")) {
                        if (jQuery("#choice1").find("input:radio").length <= 1) {
                            if (errorMessage.length > 0) {
                                errorMessage += "<br/>";
                            }
                            errorMessage += '<%= GetKeyResultForJavaScript("PLEASESELECTATLEASTONEPACKAGE")%>';
                            IsValid = false;
                        }
                    }
                    if (!IsValid) {
                        //alert(errorMessage);
                        jQuery(".error").show();
                        jQuery(".error").html(errorMessage);
                        var offseterror = jQuery(".error").offset();
                        jQuery("body").scrollTop(offseterror.top);
                        jQuery("html").scrollTop(offseterror.top);
                        if (jQuery("#cntMainBody_cntMainbody_rbtnChoice1").is(":checked")) {
                            Choice1Selected();
                        }
                        else {
                            Choice2Selected();
                        }
                        return false;
                    }
                    jQuery(".error").hide();
                    jQuery(".error").html('');
                    jQuery("body").scrollTop(0);
                    jQuery("html").scrollTop(0);
                    Choice1Selected();
                    return true;
                });
            });
</script>
</asp:Content>

<asp:Content ID="ContentLeftSearch" ContentPlaceHolderID="cntLeftSearch" runat="server" >
<uc2:LeftSearchPanel ID="LeftSearchPanel1" runat="server" />
</asp:Content>