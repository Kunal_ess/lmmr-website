﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Agency/AgencyMaster.master" AutoEventWireup="true" CodeFile="AgencyClients.aspx.cs" Inherits="Agency_AgencyClients" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ctnTop" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ctnMain" Runat="Server">
    <asp:HiddenField ID="hdnType" runat="server" Value="0" />
    <div class="search-operator-layout1">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="630px">         
                <div class="search-operator-layout-left1">
                    <div class="search-operator-from1">
                        <div class="commisions-top-new1 ">
                            <table width="100%" border="0" cellspacing="0" cellpadding="8">                                                            
                                <tr>                                 
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <div class="search-form1-left-new">
                                            Last Name : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </div>                                        
                                        <asp:TextBox ID="txtClientName" runat="server" Text="" class="inputbox"></asp:TextBox> 
                                    </td>                                   
                                </tr>                               
                                <tr>                                 
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <div class="search-form1-left-new">
                                            Company/Dept Name : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </div>                                        
                                        <asp:TextBox ID="txtCompanyName" runat="server" Text="" class="inputbox"></asp:TextBox>
                                    </td>                                   
                                </tr>                                                                
                            </table>                            
                        </div>
                    </div>
                </div>           
            </td>
            <td align="left" valign="bottom">
                <div style="float: left; margin-left: 20px;" class="n-commisions">
                    <div class="n-btn">
                        <asp:LinkButton ID="lbtSearch" runat="server" OnClick="lbtSearch_Click"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Search</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                    </div>
                </div>
                <div style="float: left; margin-left: 10px;" class="n-commisions">
                    <div class="n-btn">
                        <asp:LinkButton ID="lbtReset" runat="server" OnClick="lbtReset_Click"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Reset</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>
<div id="divAlphabeticPaging" runat="server" style="margin-top: 20px;">
                <div class="contract-list-left" style="width: 250px;">
                    <h2>
                        Client Details</h2>
                </div>                
            </div>
 <div class="operator-mainbody">
 <div id="divMessageOnGridHeader" runat="server">
 </div>
 <div class="pageing-operator-new">
  <table width="100%;" style="background: #ffffff;">
                     <tr>
            <td>
                <div id="divmsg" runat="server" class="error" style="display: none">
                </div>
            </td>
        </tr>
                    <tr>
                        <td>
                            <div style="margin-right: 10px; float: left">
                                <div class="n-btn1">
                                    <asp:LinkButton ID="lnbNewClient" runat="server" CssClass="register-hote2-btn"
                                        OnClick="lnbNewClient_Click">
                        <div class="n-btn-left">
                        </div>
                        <div class="n-btn-mid">
                            Add new</div>
                        <div class="n-btn-right">
                        </div>
                                    </asp:LinkButton>
                                </div>
                            </div>
                            <div style="margin-right: 10px; float: left;" runat="server" id="ModifyButton">
                                <div class="n-btn1">
                                    <asp:LinkButton ID="lnbModify" runat="server" CssClass="register-hote2-btn"
                                        OnClick="lnbModify_Click">
                        <div class="n-btn-left">
                        </div>
                        <div class="n-btn-mid">
                            Modify</div>
                        <div class="n-btn-right">
                        </div>
                                    </asp:LinkButton>
                                </div>
                            </div>
                            <div style="margin-right: 10px; float: left;" runat="server" id="DeleteButton">
                                <div class="n-btn1" style="margin-right: 10px;">
                                    <asp:LinkButton ID="lnbDelete" runat="server" CssClass="register-hote2-btn"
                                        OnClick="lnbDelete_Click">
                        <div class="n-btn-left">
                        </div>
                        <div class="n-btn-mid">
                            Delete</div>
                        <div class="n-btn-right">
                        </div>
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </td>
                        <td align="right">
                            <div style="float: right;" id="pageing">
                            </div>
                        </td>
                    </tr>
                </table>           
        </div>        
<table width="100%" cellspacing="1" cellpadding="5" border="0" bgcolor="#98BCD6">
  <tbody>
        <%--<tr bgcolor="#CCD8D8">
<td valign="top" width="3%"></td>
<td valign="top" width="9%">Last Name</td>
<td valign="top" width="9%">First Name</td>
<td valign="top" width="12%">Email</td>
<td valign="top" width="12%">Company Name</td>    
<td valign="top" width="13%">Phone</td>    
<td valign="top" width="10%">Last Booking</td>    
<td valign="top" width="10%">Date</td>    
<td valign="top" width="10%">Last Request</td>    
<td valign="top" width="10%">Date</td>    
<td valign="top" width="7%"></td>    
</tr>--%> 
        <asp:GridView ID="grvClients" runat="server" Width="100%" border="0" CellPadding="5"
            CellSpacing="1" AutoGenerateColumns="false" 
            DataKeyNames="UserId" GridLines="None"
            ShowHeader="true" ShowFooter="false" BackColor="#98BCD6" 
            RowStyle-CssClass="con" AlternatingRowStyle-CssClass="con-dark"                    
            EmptyDataRowStyle-BackColor="White" EmptyDataRowStyle-HorizontalAlign="Center"
            OnPageIndexChanging="grvClients_PageIndexChanging" PageSize="10" AllowPaging="true"
             onrowdatabound="grvClients_RowDataBound" HeaderStyle-HorizontalAlign="Left" HeaderStyle-CssClass="heading-row">
            <AlternatingRowStyle CssClass="con-dark"></AlternatingRowStyle>
            <Columns>
                <asp:TemplateField ItemStyle-Width="3%">                            
                    <ItemTemplate>
                        <asp:CheckBox ID="rbselect" runat="server" onclick="javascript:CheckOtherIsCheckedByGVID(this);" />
                            <asp:HiddenField ID="hdnId" runat="server" Value='<%# Eval("UserId") %>'></asp:HiddenField>   
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <asp:CheckBox ID="rbselect" runat="server" onclick="javascript:CheckOtherIsCheckedByGVID(this);" />
                            <asp:HiddenField ID="hdnId" runat="server" Value='<%# Eval("UserId") %>'></asp:HiddenField>   
                    </AlternatingItemTemplate>
                    </asp:TemplateField> 
                <asp:TemplateField ItemStyle-Width="9%" HeaderText="Last Name">
                    <ItemTemplate>                                                                
                        <asp:Label ID="lblLastName" runat="server" Text='<%# Eval("LastName") %>' CssClass="chk"></asp:Label>
                    </ItemTemplate>     
                        <AlternatingItemTemplate>                                                                
                        <asp:Label ID="lblLastName" runat="server" Text='<%# Eval("LastName") %>' CssClass="chk"></asp:Label>
                    </AlternatingItemTemplate>   
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-Width="9%" HeaderText="First Name">
                    <ItemTemplate>
                        <asp:Label ID="lblFirstName" runat="server" Text='<%# Eval("FirstName") %>' CssClass="chk1"></asp:Label>
                    </ItemTemplate>    
                        <AlternatingItemTemplate>
                        <asp:Label ID="lblFirstName" runat="server" Text='<%# Eval("FirstName") %>' CssClass="chk1"></asp:Label>
                    </AlternatingItemTemplate>                        
                </asp:TemplateField> 
                <asp:TemplateField ItemStyle-Width="12%" HeaderText="Email">
                    <ItemTemplate>
                        <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("EmailId") %>' CssClass="chk1"></asp:Label>
                    </ItemTemplate>    
                        <AlternatingItemTemplate>
                        <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("EmailId") %>' CssClass="chk1"></asp:Label>
                    </AlternatingItemTemplate>                        
                </asp:TemplateField> 
                <asp:TemplateField ItemStyle-Width="12%" HeaderText="Company/Dept Name">
                    <ItemTemplate>
                        <asp:Label ID="lblCompanyName" runat="server" Text=""></asp:Label>
                    </ItemTemplate>
                        <AlternatingItemTemplate>
                        <asp:Label ID="lblCompanyName" runat="server" Text=""></asp:Label>
                    </AlternatingItemTemplate>
                </asp:TemplateField>                                           
                <asp:TemplateField ItemStyle-Width="13%" HeaderText="Phone">
                    <ItemTemplate>
                        <asp:Label ID="lblPhoneNumber" runat="server" Text=""></asp:Label>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <asp:Label ID="lblPhoneNumber" runat="server" Text=""></asp:Label>
                    </AlternatingItemTemplate>
                </asp:TemplateField> 
                <asp:TemplateField ItemStyle-Width="10%" HeaderText="Last Booking">
                    <ItemTemplate>
                        <asp:Label ID="lblLastBooking" runat="server" Text=""></asp:Label>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <asp:Label ID="lblLastBooking" runat="server" Text=""></asp:Label>
                    </AlternatingItemTemplate>
                </asp:TemplateField> 
                <asp:TemplateField ItemStyle-Width="10%" HeaderText="Date">
                    <ItemTemplate>
                        <asp:Label ID="lblLastBookingDate" runat="server" Text=""></asp:Label>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <asp:Label ID="lblLastBookingDate" runat="server" Text=""></asp:Label>
                    </AlternatingItemTemplate>
                </asp:TemplateField> 
                <asp:TemplateField ItemStyle-Width="10%" HeaderText="Last Request">
                    <ItemTemplate>
                        <asp:Label ID="lblLastRequest" runat="server" Text=""></asp:Label>
                    </ItemTemplate>
                        <AlternatingItemTemplate>
                        <asp:Label ID="lblLastRequest" runat="server" Text=""></asp:Label>
                    </AlternatingItemTemplate>
                </asp:TemplateField> 
                <asp:TemplateField ItemStyle-Width="10%" HeaderText="Date">
                    <ItemTemplate>
                        <asp:Label ID="lblLastRequestDate" runat="server" Text=""></asp:Label>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <asp:Label ID="lblLastRequestDate" runat="server" Text=""></asp:Label>
                    </AlternatingItemTemplate>
                </asp:TemplateField> 
                <asp:TemplateField ItemStyle-Width="7%">
                    <ItemTemplate>
                        <asp:LinkButton ID="lbnFinancial" runat="server" CssClass="register-hote2-btn" Text="Statistic" CommandArgument='<%# Eval("UserId") %>' OnClick="lbnFinancial_Click"></asp:LinkButton>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <asp:LinkButton ID="lbnFinancial" runat="server" CssClass="register-hote2-btn" Text="Statistic" CommandArgument='<%# Eval("UserId") %>' OnClick="lbnFinancial_Click"></asp:LinkButton>
                    </AlternatingItemTemplate>
                </asp:TemplateField> 
            </Columns>       

<EmptyDataRowStyle HorizontalAlign="Center" BackColor="White"></EmptyDataRowStyle>
            <EmptyDataTemplate>
                <table>
                    <tr>
                        <td colspan="3" align="center">
                            <b>No record found !</b>
                        </td>
                    </tr>
                </table>
            </EmptyDataTemplate>             
            <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
            <PagerStyle HorizontalAlign="Right" BackColor="White" CssClass="displayNone" />
            <PagerTemplate>
                <div id="Paging" style="width: 100%; display: none;">
                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                </div>
            </PagerTemplate>
        </asp:GridView>
</tbody></table>
</div>
 <br />
    <br />
    <div class="superadmin-example-mainbody" id="DivAddUpdateAgentUser" runat="server"
        visible="false">
        <div id="divmessage" runat="server" class="error" style="display: none">
        </div>
        <div class="superadmin-mainbody-sub1" style="padding-left: 0px;">
            <div class="superadmin-mainbody-sub1-left" style="width: 100%;">
                <h2>
                    <asp:Label ID="lblHeader" runat="server" Text=""></asp:Label></h2>
            </div>
        </div>
        <table style="margin-left: -1px;" width="100%" bgcolor="#92bddd" cellspacing="1"
            cellpadding="0">            
            <tr bgcolor="#E3F0F1">
                <td width="15%" style="line-height: 31px; padding-left: 5px;">
                    <strong>First Name:</strong>
                </td>
                <td>
                    <asp:TextBox ID="txtFirstName" runat="server" MaxLength="25" TabIndex="1" Width="200px"
                        CssClass="txtpadding" />
                </td>
            </tr>
            <tr bgcolor="#F1F8F8">
                <td width="15%" style="line-height: 31px; padding-left: 5px;">
                    <strong>Last Name:</strong>
                </td>
                <td>
                    <asp:TextBox ID="txtLastName" runat="server" MaxLength="25" TabIndex="1" Width="200px"
                        CssClass="txtpadding" />
                </td>
            </tr>
            <tr bgcolor="#F1F8F8">
                <td width="15%" style="line-height: 31px; padding-left: 5px;">
                    <strong>Email:</strong>
                </td>
                <td>
                    <asp:TextBox ID="txtEmailAccount" runat="server" MaxLength="50" TabIndex="1" Width="200px"
                        CssClass="txtpadding" />
                </td>
            </tr>
            <tr bgcolor="#E3F0F1">
                <td width="15%" style="line-height: 31px; padding-left: 5px;">
                    <strong>Company/Dept:</strong>
                </td>
                <td>
                    <asp:TextBox ID="txtCompany" runat="server" MaxLength="100" TabIndex="1" Width="200px"
                        CssClass="txtpadding" />
                </td>
            </tr>
            <tr bgcolor="#F1F8F8">
                <td width="15%" style="line-height: 31px; padding-left: 5px;">
                    <strong>Phone:</strong>
                </td>
                <td>
                    <asp:TextBox ID="txtPhone" runat="server" MaxLength="15" TabIndex="1" Width="200px"
                        CssClass="txtpadding" />
                         <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtPhone"
                            FilterType="Numbers">
                        </asp:FilteredTextBoxExtender>
                </td>
            </tr>            
        </table>
        <div style="border-bottom: 1px solid #92BDDD; width: 100%; float: left; padding-top: 10px;
            padding-bottom: 10px;">
            <div class="button_section">
                <asp:LinkButton ID="lnkSave" runat="server" CssClass="select" OnClick="lbnSave_Click">Save</asp:LinkButton>
                &nbsp;or&nbsp;
                <asp:LinkButton ID="lbnClear" runat="server" CssClass="cancelpop" OnClick="lbnClear_Click">Cancel</asp:LinkButton>
            </div>
        </div>
    </div>




    <div class="superadmin-example-mainbody" id="DivFinancialData" runat="server"
        visible="false">        
        <div class="superadmin-mainbody-sub1" style="padding-left: 0px;">
            <div class="superadmin-mainbody-sub1-left" style="width: 100%;">
                <h2>
                    <asp:Label ID="Label1" runat="server" Text="Financial Data"></asp:Label></h2>
            </div>
        </div>
        <table style="margin-left: -1px;" width="100%" bgcolor="#92bddd" cellspacing="1"
            cellpadding="0">  
            <tr>
                <td width="50%" style="line-height: 31px; padding-left: 5px;text-align:center;">
                    <strong>Booking</strong>
                </td>
                <td  style="line-height: 31px; padding-left: 5px;text-align:center;">
                    <strong>Request</strong>
                </td>
            </tr>          
            <tr bgcolor="#E3F0F1">
                <td width="50%" style="line-height: 31px; padding-left: 5px;">
                    <strong>Total Booking in 
                        <asp:Label ID="lblBookingYear" runat="server" Text="" Width="11%"></asp:Label>  :  &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblTotalBooking" runat="server" Text=""></asp:Label></strong>
                </td>
                <td>
                    <strong>&nbsp;&nbsp;&nbsp;&nbsp;Total Request in 
                        <asp:Label ID="lblRequestYear" runat="server" Text="" Width="10%"></asp:Label>  :  &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblTotalRequest" runat="server" Text=""></asp:Label></strong>
                </td>
            </tr>
            <tr bgcolor="#F1F8F8">
                <td colspan="2" style="line-height: 31px; padding-left: 5px;">
                    <strong>Annual booking revenue &nbsp;&nbsp; :  &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblRevenue" runat="server" Text=""></asp:Label></strong>
                </td>               
            </tr>                    
        </table>        
    </div>
    <script type="text/javascript" language="javascript">
        jQuery(document).ready(function () {
            if (jQuery("#Paging") != undefined) {
                var inner = jQuery("#Paging").html();
                jQuery("#pageing").html(inner);
                jQuery("#Paging").html("");
            }

        });

        jQuery(document).ready(function () {
            jQuery("#<%= lnbModify.ClientID %>").bind("click", function () {
                if (jQuery("#ctnMain_grvClients").find("input:checkbox:checked").length <= 0) {
                    alert('please select client to modify.');
                    return false;
                }
            });

            jQuery("#<%= lnbDelete.ClientID %>").bind("click", function () {
                if (jQuery("#ctnMain_grvClients").find("input:checkbox:checked").length <= 0) {
                    alert('please select client to delete.');
                    return false;
                }
                else {
                    return confirm("Are you sure you want to delete it?");
                }

            });
        });


        jQuery("#<%= lnkSave.ClientID %>").bind("click", function () {
            if (jQuery("#<%= divmessage.ClientID %>").hasClass("succesfuly")) {
                jQuery("#<%= divmessage.ClientID %>").removeClass("succesfuly").addClass("error").hide();
            }
            var isvalid = true;
            var errormessage = "";
            var firstName = jQuery("#<%= txtFirstName.ClientID %>").val();
            if (firstName.length <= 0 || firstName == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }
                errormessage += 'Enter first name';
                isvalid = false;
            }

            var lastName = jQuery("#<%= txtLastName.ClientID %>").val();
            if (lastName.length <= 0 || lastName == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }
                errormessage += 'Enter last name';
                isvalid = false;
            }
            var txtEmailAccount = jQuery("#<%= txtEmailAccount.ClientID %>").val();
            if (txtEmailAccount.length <= 0) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Emailid is required.";
                isvalid = false;
            }
            else {
                if (!validateEmail(txtEmailAccount)) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "Enter valid email id.";
                    isvalid = false;
                }
            }
            var Company = jQuery("#<%= txtCompany.ClientID %>").val();
            if (Company.length <= 0 || Company == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }
                errormessage += 'Enter company or department name';
                isvalid = false;
            }
            var phone = jQuery("#<%= txtPhone.ClientID %>").val();
            if (phone.length <= 0 || phone == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }
                errormessage += 'Enter phone number';
                isvalid = false;
            }        

            if (!isvalid) {
                jQuery("#<%= divmessage.ClientID %>").show();
                jQuery("#<%= divmessage.ClientID %>").html(errormessage);
                return false;
            }
            jQuery("#<%= divmessage.ClientID %>").html("");
            jQuery("#<%= divmessage.ClientID %>").hide();
            jQuery("#Loding_overlay").show();
            jQuery("body").scrollTop(0);
            jQuery("html").scrollTop(0);
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            jQuery("#divJoinToday").show();
            jQuery("#divJoinToday-overlay").show();
        });

        function CheckOtherIsCheckedByGVID(spanChk) {
            var IsChecked = spanChk.checked;

            var CurrentRdbID = spanChk.id;
            var Chk = spanChk;
            Parent = document.getElementById('ctnMain_grvClients');
            var items = Parent.getElementsByTagName('input');

            for (i = 0; i < items.length; i++) {

                if (items[i].id != CurrentRdbID && items[i].type == "checkbox") {

                    if (items[i].checked) {

                        items[i].checked = false;

                        items[i].parentElement.parentElement.style.backgroundColor = '';

                        //items[i].parentElement.parentElement.style.color = 'black';
                    }
                    else {

                        items[i].parentElement.parentElement.style.backgroundColor = '';
                    }
                }
                else {

                    items[i].parentElement.parentElement.style.backgroundColor = '';
                }
            }
            if (IsChecked != true) {
                spanChk.parentElement.parentElement.style.backgroundColor = '';
            }
        }

        function validateEmail(txtEmail) {
            var a = txtEmail;
            var filter = /^[a-zA-Z0-9_.-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{1,4}$/;
            if (filter.test(a)) {
                return true;
            }
            else {
                return false;
            }
        }    
    </script>    

    <script language="javascript" type="text/javascript">        
        function SetFocusBottom(val) {

            var ofset = jQuery("#" + val).offset();
            jQuery('body').scrollTop(ofset.top);
            jQuery('html').scrollTop(ofset.top);
        }
    </script>
</asp:Content>

