﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using System.Configuration;
using log4net;
using log4net.Config;
#endregion

    public partial class Agency_AgencyClients : System.Web.UI.Page
    {
        #region Variable
        ILog logger = log4net.LogManager.GetLogger(typeof(Agency_AgencyClients));
        ClientContract objClient = new ClientContract();
        HotelInfo ObjHotelinfo = new HotelInfo();
        HotelManager objHotelManager = new HotelManager();
        EmailConfigManager em = new EmailConfigManager();
        ViewBooking_Hotel objViewBooking_Hotel = new ViewBooking_Hotel();
        ManageAgentUser objAgent = new ManageAgentUser();
        VList<Viewbookingrequest> vlistBooking;
        VList<Viewbookingrequest> vlistreq;
        string strUsers = string.Empty;
        public string SiteRootPath
        {
            get
            {
                string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                string appRootUrl = HttpContext.Current.Request.ApplicationPath;
                return host + appRootUrl + "/";
            }
        }
        #endregion

        #region Pageload
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Check session exist or not
                if (Session["CurrentAgencyUserID"] == null)
                {
                    Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
                    Session.Abandon();
                    if (l != null)
                    {
                        Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
                    }
                    else
                    {
                        Response.Redirect(SiteRootPath + "login/english");
                    }
                }


                if (!IsPostBack)
                {
                    BindAgencyClients();
                }
                ApplyPaging();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        #endregion

        #region Bind all clients as per agency user
        public void BindAgencyClients()
        {
            GetUserLinkId();
            TList<Users> user = objClient.GetAllClientsOfAgencyUsers(strUsers);
            grvClients.DataSource = user;
            grvClients.DataBind();
            Session["WhereClauseTemp"] = user;
        }

        public void GetUserLinkId()
        {
            UserDetails objUsers = objAgent.GetUserDetailByID(Convert.ToInt64(Session["CurrentAgencyUserID"])).FirstOrDefault();
            strUsers = Session["CurrentAgencyUserID"].ToString();
            if (strUsers == string.Empty)
            {
                strUsers = objUsers.LinkWith;
            }
            else
            {
                if (objUsers != null)
                {
                    strUsers = strUsers + "," + objUsers.LinkWith;
                }
            }
            if (strUsers.EndsWith(","))
            {
                strUsers = strUsers.Substring(0, strUsers.Length - 1);
            }
        }
        #endregion

        #region Get CompanyName/Phone number and other details from userdetails
        protected void grvClients_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
            {
                Users data = e.Row.DataItem as Users;
                Label lblPhoneNumber = (Label)e.Row.FindControl("lblPhoneNumber");
                Label lblCompanyName = (Label)e.Row.FindControl("lblCompanyName");
                Label lblLastBooking = (Label)e.Row.FindControl("lblLastBooking");
                Label lblLastRequest = (Label)e.Row.FindControl("lblLastRequest");
                Label lblLastBookingDate = (Label)e.Row.FindControl("lblLastBookingDate");
                Label lblLastRequestDate = (Label)e.Row.FindControl("lblLastRequestDate");
                HiddenField hdnId = (HiddenField)e.Row.FindControl("hdnId");
                TList<UserDetails> objDtls = ObjHotelinfo.GetUserDetails(Convert.ToInt32(data.UserId));
                if (objDtls.Count > 0)
                {
                    lblPhoneNumber.Text = objDtls[0].Phone;
                    lblCompanyName.Text = objDtls[0].CompanyName;
                }


                string whereclause = "AgencyClientId='" + hdnId.Value + "' and booktype = 0";
                string orderby = ViewbookingrequestColumn.BookingDate + " DESC";
                vlistBooking = objViewBooking_Hotel.Bindgrid(whereclause, orderby).FindAllDistinct(ViewbookingrequestColumn.HotelId);
                if (vlistBooking.Count > 0)
                {
                    lblLastBooking.Text = Convert.ToString(vlistBooking[0].Id);
                    DateTime BookDate = (DateTime)vlistBooking[0].BookingDate;
                    lblLastBookingDate.Text = BookDate.ToString("dd/MM/yyyy");
                }
                else
                {
                    lblLastBooking.Text = "N/A";
                    lblLastBookingDate.Text = "N/A";
                }

                string whereclauseReq = "AgencyClientId='" + hdnId.Value + "' and booktype in (1,2) ";
                vlistreq = objViewBooking_Hotel.Bindgrid(whereclauseReq, orderby).FindAllDistinct(ViewbookingrequestColumn.HotelId);
                if (vlistreq.Count > 0)
                {
                    lblLastRequest.Text = Convert.ToString(vlistreq[0].Id);
                    DateTime RequestDate = (DateTime)vlistreq[0].BookingDate;
                    lblLastRequestDate.Text = RequestDate.ToString("dd/MM/yyyy");
                }
                else
                {
                    lblLastRequest.Text = "N/A";
                    lblLastRequestDate.Text = "N/A";
                }
            }
        }
        #endregion

        #region Search function as per client name
        protected void lbtSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtClientName.Text != "" || txtCompanyName.Text != "")
                {
                    GetUserLinkId();
                    string whereclause = UsersColumn.Usertype + "=" + ((int)Usertype.AgencyClient) + " and " + UsersColumn.IsActive + "=1" + " and " + "isnull(IsRemoved, 0)" + "!=1" + " and " + "ParentId IN (" + strUsers + ")";
                    if (txtClientName.Text != "")
                    {
                        if (whereclause.Length > 0)
                        {
                            whereclause += " AND ";
                        }
                        //whereclause += "(" + UsersColumn.FirstName + " LIKE '" + txtClientName.Text + "%'  or " + UsersColumn.LastName + " LIKE '" + txtClientName.Text + "%')";
                        whereclause += "(" + UsersColumn.LastName + " LIKE '" + txtClientName.Text + "%')";
                    }
                    if (txtCompanyName.Text != "")
                    {
                        grvClients.DataSource = objClient.GetClientsCompanyOfAgencySearch(whereclause).Where(i => (i.UserDetailsCollection.Count > 0 ? i.UserDetailsCollection[0].CompanyName.ToLower() : "").StartsWith(txtCompanyName.Text.ToLower())).ToList();
                        grvClients.DataBind();
                        //Session["WhereClauseTemp"] = objClient.GetClientsCompanyOfAgencySearch(whereclause).Where(i => (i.UserDetailsCollection.Count > 0 ? i.UserDetailsCollection[0].CompanyName.ToLower() : "").StartsWith(txtCompanyName.Text.ToLower()));
                    }
                    else
                    {
                        grvClients.DataSource = objClient.GetClientsCompanyOfAgencySearch(whereclause);
                        grvClients.DataBind();
                        Session["WhereClauseTemp"] = objClient.GetClientsCompanyOfAgencySearch(whereclause);
                    }
                }
                else
                {
                    GetUserLinkId();
                    grvClients.DataSource = objClient.GetAllClientsOfAgencyUsers(strUsers);
                    grvClients.DataBind();
                    Session["WhereClauseTemp"] = objClient.GetAllClientsOfAgencyUsers(strUsers);
                }
                ApplyPaging();
                DivAddUpdateAgentUser.Visible = false;
                DivFinancialData.Visible = false;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        protected void lbtReset_Click(object sender, EventArgs e)
        {
            txtClientName.Text = "";
            txtCompanyName.Text = "";
            BindAgencyClients();
            ApplyPaging();
            divmsg.Style.Add("display", "none");
            divmsg.Attributes.Add("class", "error");
            DivAddUpdateAgentUser.Visible = false;
            DivFinancialData.Visible = false;
            ViewState["CurrentPage"] = 0;
        }
        #endregion

        #region Add,modify,delete clients
        protected void lnbNewClient_Click(object sender, EventArgs e)
        {
            lblHeader.Text = "Add new client";
            divmsg.Style.Add("display", "none");
            divmsg.Attributes.Add("class", "error");
            txtEmailAccount.ReadOnly = false;
            BindAgencyClients();
            ApplyPaging();
            clearForm();
            DivAddUpdateAgentUser.Visible = true;
            DivFinancialData.Visible = false;
            ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + DivAddUpdateAgentUser.ClientID + "');});", true);
        }

        protected void lnbModify_Click(object sender, EventArgs e)
        {
            divmsg.Style.Add("display", "none");
            divmsg.Attributes.Add("class", "error");

            DivAddUpdateAgentUser.Visible = true;
            DivFinancialData.Visible = false;
            txtEmailAccount.ReadOnly = true;

            foreach (GridViewRow l in grvClients.Rows)
            {
                CheckBox r = (CheckBox)l.FindControl("rbselect");
                if (r.Checked)
                {
                    DataKey currentDataKey = this.grvClients.DataKeys[l.RowIndex];
                    hdnType.Value = currentDataKey.Value.ToString();
                }
            }

            lblHeader.Text = "Modify client details";

            if (hdnType.Value != "0")
            {
                Users objuser = objAgent.GetUserID(Convert.ToInt64(hdnType.Value));
                UserDetails objuserDetail = objAgent.GetUserDetailByID(Convert.ToInt64(hdnType.Value)).FirstOrDefault();
                if (objuser != null)
                {
                    txtCompany.Text = objuserDetail.CompanyName;
                    txtPhone.Text = objuserDetail.Phone;
                    txtEmailAccount.Text = objuser.EmailId;
                    txtFirstName.Text = objuser.FirstName;
                    txtLastName.Text = objuser.LastName;
                }

            }
            ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + DivAddUpdateAgentUser.ClientID + "');});", true);

        }

        protected void lnbDelete_Click(object sender, EventArgs e)
        {
            divmsg.Style.Add("display", "none");
            divmsg.Attributes.Add("class", "error");

            foreach (GridViewRow l in grvClients.Rows)
            {
                CheckBox r = (CheckBox)l.FindControl("rbselect");
                if (r.Checked)
                {
                    DataKey currentDataKey = this.grvClients.DataKeys[l.RowIndex];
                    hdnType.Value = currentDataKey.Value.ToString();
                }
            }

            if (hdnType.Value != "0")
            {
                UserDetails obj = objAgent.GetUserDetailByID(Convert.ToInt64(hdnType.Value)).FirstOrDefault();
                if (obj != null)
                {
                    if (objAgent.DeleteAgent(Convert.ToInt64(hdnType.Value)))
                    {
                        divmsg.Style.Add("display", "block");
                        divmsg.Attributes.Add("class", "succesfuly");
                        divmsg.InnerHtml = "User deleted successfully";
                        BindAgencyClients();
                        ApplyPaging();
                    }
                }
                DivAddUpdateAgentUser.Visible = false;
                DivFinancialData.Visible = false;
            }
        }

        protected void lbnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdnType.Value != "0")
                {
                    Users objuser = objAgent.GetUserID(Convert.ToInt64(hdnType.Value));
                    UserDetails objuserDetail = objAgent.GetUserDetailByID(Convert.ToInt64(hdnType.Value)).FirstOrDefault(); ;
                    objuser.FirstName = txtFirstName.Text;
                    objuser.LastName = txtLastName.Text;
                    objuserDetail.CompanyName = txtCompany.Text;
                    objuserDetail.Phone = txtPhone.Text;
                    objuser.EmailId = txtEmailAccount.Text;

                    if (objAgent.UpdateUser(objuser, objuserDetail))
                    {
                        divmsg.Style.Add("display", "block");
                        divmsg.Attributes.Add("class", "succesfuly");
                        divmsg.InnerHtml = "Client details updated successfully";
                    }
                    else
                    {
                        divmsg.Style.Add("display", "none");
                        divmsg.Attributes.Add("class", "error");
                        divmsg.InnerHtml = "Please contact lmmt support.";
                    }

                    DivAddUpdateAgentUser.Visible = false;
                    DivFinancialData.Visible = false;
                    clearForm();
                    BindAgencyClients();
                }
                else
                {
                    if (objClient.CheckEmailExistAdminLink(txtEmailAccount.Text.Trim().Replace("'", "")))
                    {
                        divmsg.InnerHtml = "Email Id already exist in records.";
                        divmsg.Attributes.Add("class", "error");
                        divmsg.Style.Add("display", "block");
                        return;
                    }
                    Users objuser = new Users();
                    UserDetails objuserDetail = new UserDetails();

                    objuser.FirstName = txtFirstName.Text;
                    objuser.LastName = txtLastName.Text;
                    objuserDetail.CompanyName = txtCompany.Text;
                    objuserDetail.Phone = txtPhone.Text;
                    objuser.EmailId = txtEmailAccount.Text;
                    objuser.Usertype = (int)Usertype.AgencyClient;
                    objuser.ParentId = Convert.ToInt32(Session["CurrentAgencyUserID"]);
                    objuser.IsActive = true;
                    if (objAgent.CreateUser(objuser, objuserDetail))
                    {
                        divmsg.Style.Add("display", "block");
                        divmsg.Attributes.Add("class", "succesfuly");
                        divmsg.InnerHtml = "New client created successfully";
                        DivAddUpdateAgentUser.Visible = false;
                        DivFinancialData.Visible = false;
                        clearForm();
                        BindAgencyClients();
                    }
                }
                ApplyPaging();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        #endregion

        #region Clear controls
        protected void lbnClear_Click(object sender, EventArgs e)
        {
            try
            {
                clearForm();
                BindAgencyClients();
                ApplyPaging();
                DivAddUpdateAgentUser.Visible = false;
                DivFinancialData.Visible = false;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        void clearForm()
        {
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtCompany.Text = "";
            txtPhone.Text = "";
            txtEmailAccount.Text = "";
            txtEmailAccount.ReadOnly = false;
            hdnType.Value = "0";
            BindAgencyClients();
            ApplyPaging();
        }
        #endregion

        #region Get financial data of booking/request as per client
        protected void lbnFinancial_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){SetFocusBottom('" + DivFinancialData.ClientID + "');});", true);

                divmsg.Style.Add("display", "none");
                divmsg.Attributes.Add("class", "error");
                LinkButton lbnFinancial = (LinkButton)sender;
                ViewState["UserId"] = lbnFinancial.CommandArgument;

                DivFinancialData.Visible = true;
                DivAddUpdateAgentUser.Visible = false;

                DateTime dt = DateTime.Now;
                lblBookingYear.Text = dt.Year.ToString();
                lblRequestYear.Text = dt.Year.ToString();


                string whereclause = "AgencyClientId='" + ViewState["UserId"] + "' and booktype = 0 and datepart(year,BookingDate)='" + dt.Year.ToString() + "'";
                VList<Viewbookingrequest> vBooking = objViewBooking_Hotel.Bindgrid(whereclause, string.Empty);
                lblTotalBooking.Text = vBooking.Count.ToString();

                string whereclauseReq = "AgencyClientId='" + ViewState["UserId"] + "' and booktype in (1,2) and datepart(year,BookingDate)='" + dt.Year.ToString() + "'";
                VList<Viewbookingrequest> vRequest = objViewBooking_Hotel.Bindgrid(whereclauseReq, string.Empty);
                lblTotalRequest.Text = vRequest.Count.ToString();

                Decimal amt = Convert.ToDecimal(vBooking.Sum(P => P.ConfirmRevenueAmount));
                if (amt.ToString() == "0")
                {
                    lblRevenue.Text = amt.ToString();
                }
                else
                {
                    lblRevenue.Text = amt.ToString() + " Euro";
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }
        #endregion

        private void ApplyPaging()
        {
            try
            {
                GridViewRow row = grvClients.TopPagerRow;
                if (row != null)
                {
                    PlaceHolder ph;
                    LinkButton lnkPaging;
                    LinkButton lnkPrevPage;
                    LinkButton lnkNextPage;
                    lnkPrevPage = new LinkButton();
                    lnkPrevPage.CssClass = "pre";
                    lnkPrevPage.Width = Unit.Pixel(73);
                    lnkPrevPage.CommandName = "Page";
                    lnkPrevPage.CommandArgument = "prev";
                    ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkPrevPage);
                    if (grvClients.PageIndex == 0)
                    {
                        lnkPrevPage.Enabled = false;

                    }
                    for (int i = 1; i <= grvClients.PageCount; i++)
                    {
                        lnkPaging = new LinkButton();
                        if (ViewState["CurrentPage"] != null)
                        {
                            if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                            {
                                lnkPaging.CssClass = "pag2";
                            }
                            else
                            {
                                lnkPaging.CssClass = "pag";
                            }
                        }
                        else
                        {
                            if (i == 1)
                            {
                                lnkPaging.CssClass = "pag2";
                            }
                            else
                            {
                                lnkPaging.CssClass = "pag";
                            }
                        }
                        lnkPaging.Width = Unit.Pixel(16);
                        lnkPaging.Text = i.ToString();
                        lnkPaging.CommandName = "Page";
                        lnkPaging.CommandArgument = i.ToString();
                        if (i == grvClients.PageIndex + 1)
                            ph = (PlaceHolder)row.FindControl("ph");
                        ph.Controls.Add(lnkPaging);
                    }
                    lnkNextPage = new LinkButton();
                    lnkNextPage.CssClass = "nex";
                    lnkNextPage.Width = Unit.Pixel(42);
                    lnkNextPage.CommandName = "Page";
                    lnkNextPage.CommandArgument = "next";
                    ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkNextPage);
                    ph = (PlaceHolder)row.FindControl("ph");
                    if (grvClients.PageIndex == grvClients.PageCount - 1)
                    {
                        lnkNextPage.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        protected void grvClients_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grvClients.PageIndex = e.NewPageIndex;
                ViewState["CurrentPage"] = e.NewPageIndex;
                grvClients.DataSource = Session["WhereClauseTemp"];
                grvClients.DataBind();
                ApplyPaging();
                DivAddUpdateAgentUser.Visible = false;
                DivFinancialData.Visible = false;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
    }