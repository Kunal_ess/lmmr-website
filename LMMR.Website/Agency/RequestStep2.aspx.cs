﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LMMR.Business;
using LMMR.Entities;
using log4net;
using System.Configuration;

public partial class Agency_RequestStep2 : BasePage
{
    #region Variables and Properties
    HotelManager objHotel = new HotelManager();
    PackagePricingManager objPackagePricingManager = new PackagePricingManager();
    Hotel objRequestHotel = new Hotel();
    BookingManager bm = new BookingManager();
    int countMr = 0;
    CreateRequest objRequest = null;
    public List<PackageItemDetails> SelectedPackage
    {
        get;
        set;
    }
    public List<RequestExtra> SelectedExtra
    {
        get;
        set;
    }
    public TList<PackageItems> AllPackageItem
    {
        get;
        set;
    }
    public TList<PackageItems> AllFoodAndBravrages
    {
        get;
        set;
    }
    public TList<PackageItems> AllEquipments
    {
        get;
        set;
    }
    public TList<PackageItems> AllOthersItems
    {
        get;
        set;
    }
    public UserControl_Agency_LeftSearchPanel u
    {
        get
        {
            return Page.Master.FindControl("cntLeftSearch").FindControl("LeftSearchPanel1") as UserControl_Agency_LeftSearchPanel;
        }
    }
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentAgencyUserID"] == null)
        {
                Response.Redirect(SiteRootPath + "login/english");
        }
        try
        {
            u.SearchButton += new EventHandler(u_SearchButton);
            u.MapSearch += new EventHandler(u_MapSearch);
            if (!Page.IsPostBack)
            {
                u.usercontroltype = "Basic";
                Users objUsers = Session["CurrentAgencyUser"] as Users;
                //Bind Login users details
                //lblDateLogin.Text = DateTime.Now.ToString("dd MMM yyyy");
                //lblUserName.Text = objUsers.FirstName + " " + objUsers.LastName;
                fillStaticData();
                CheckSearchAvailable();
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    BookingRequest objBookingRequest = new BookingRequest();
    void u_SearchButton(object sender, EventArgs e)
    {
        Session["CurrentUserSeleted"] = Convert.ToString(Session["CurrentAgencyUserID"]);
        ManageSession();
        Session["ComeFromBookingAndRequestPage"] = "Request";
        Response.Redirect("~/Agency/SearchBookingRequest.aspx");
    }
    void u_MapSearch(object sender, EventArgs e)
    {
        Session["CurrentUserSeleted"] = Convert.ToString(Session["CurrentAgencyUserID"]);
        ManageSession();
        Session["ComeFromBookingAndRequestPage"] = "Request";
        Response.Redirect("~/Agency/SearchBookingRequest.aspx");
    }
    void ManageSession()
    {
        string WhereClause = string.Empty;
        string WhereClauseday2 = string.Empty;
        if (u.propCountryID != "0")
        {
            if (u.propCountryID != "")
            {
                if (WhereClause.Length > 0)
                {
                    WhereClause += " and ";
                    WhereClauseday2 += " and ";
                }
                WhereClause += HotelColumn.CountryId + "=" + u.propCountryID;
                WhereClauseday2 += HotelColumn.CountryId + "=" + u.propCountryID;
            }
        }

        if (Convert.ToInt32(u.propCity) != 0)
        {
            if (u.propCity != "")
            {
                if (WhereClause.Length > 0)
                {
                    WhereClause += " and ";
                    WhereClauseday2 += " and ";
                }
                WhereClause += HotelColumn.CityId + "=" + u.propCity;
                WhereClauseday2 += HotelColumn.CityId + "=" + u.propCity;
            }
        }
        if (u.propDuration == "1")
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                if (u.propDay1 == "0")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (u.propDay1 == "1")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (u.propDay1 == "2")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
            }
        }
        if (u.propDuration == "2")
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                WhereClauseday2 += " and ";
                if (u.propDay1 == "0")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (u.propDay1 == "1")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (u.propDay1 == "2")
                {
                    WhereClause += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }


                if (u.propDay2 == "0")
                {
                    WhereClauseday2 += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0" + " and " + ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
                else if (u.propDay2 == "1")
                {
                    WhereClauseday2 += ViewAvailabilityOfRoomsColumn.MorningStatus + "=0";
                }
                else if (u.propDay2 == "2")
                {
                    WhereClauseday2 += ViewAvailabilityOfRoomsColumn.AfternoonStatus + "=0";
                }
            }
        }

        if (u.propDate != "")
        {
            DateTime fromDate = new DateTime(Convert.ToInt32("20" + u.propDate.Split('/')[2]), Convert.ToInt32(u.propDate.Split('/')[1]), Convert.ToInt32(u.propDate.Split('/')[0]));

            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                WhereClauseday2 += " and ";
            }
            WhereClause += AvailabilityColumn.AvailabilityDate + "='" + fromDate + "'" + " and " + " (DATEDIFF(day, GETDATE(),'" + fromDate + "')) >= " + AvailabilityColumn.LeadTimeForMeetingRoom + "";
            WhereClauseday2 += AvailabilityColumn.AvailabilityDate + "='" + fromDate.AddDays(1) + "'" + " and " + " (DATEDIFF(day, GETDATE(),'" + fromDate.AddDays(1) + "')) >= " + AvailabilityColumn.LeadTimeForMeetingRoom + "";
        }


        if (u.propParticipant != "" && u.propParticipant != "0")
        {
            if (WhereClause.Length > 0)
            {
                WhereClause += " and ";
                WhereClauseday2 += " and ";
            }
            WhereClause += u.propParticipant + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + "";
            WhereClauseday2 += u.propParticipant + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + "";
        }

        //Send all search session value
        Session["Where"] = WhereClause;
        if (u.propDuration == "2")
        {
            Session["Where2"] = WhereClauseday2;
        }

        //Maintain session value for all controls 
        objBookingRequest = new BookingRequest();
        objBookingRequest.propCountry = u.propCountryID;
        objBookingRequest.propCity = u.propCity;
        objBookingRequest.propDate = Convert.ToString(string.IsNullOrEmpty(u.propDate) ? (u.propDate == "dd/mm/yy" ? DateTime.Now.ToString("dd/MM/yy") : u.propDate) : u.propDate);
        objBookingRequest.propDuration = u.propDuration;
        objBookingRequest.propDays = u.propDay1;
        objBookingRequest.propDay2 = u.propDay2;
        if (u.propParticipant != "")
        {
            objBookingRequest.propParticipants = u.propParticipant;
        }
        else
        {
            objBookingRequest.propParticipants = "0";
        }
        Session["masterInput"] = objBookingRequest;
        Session["CurrencyID"] = "2";
        //Dictionary<long, string> participants = new Dictionary<long, string>();
        //participants.Add(1, objBookingRequest.propParticipants);
        //Session["participants"] = participants;
    }
    #endregion

    #region Additional methods

    public void fillStaticData()
    {
        txtSpecialRequest.Text = GetKeyResult("PLEASEWRITEHEREIFYOUHAVEASPECIALREQUESTFORTHISBOOKING");
    }

    /// <summary>
    /// Check Availability of Serach according To Serach ID.
    /// </summary>
    public void CheckSearchAvailable()
    {
        if (Session["RequestID"] != null)
        {
            SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["RequestID"]));
            if (st != null)
            {
                if (st.CurrentStep == 2)
                {
                    objRequest = (CreateRequest)TrailManager.XmlDeserialize(typeof(CreateRequest), st.SearchObject);
                    BindHotel();
                }
                else
                {
                    Response.Redirect("~/Agency/request.aspx");
                }
            }
        }
        else
        {
            Response.Redirect("~/Agency/request.aspx");
        }
    }
    private long _CountryID;
    public long countryID
    {
        get { return _CountryID; }
        set { _CountryID = value; }
    }
    public void BindHotel()
    {
        try
        {
            SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["RequestID"]));
            if (st != null)
            {
                objRequest = (CreateRequest)TrailManager.XmlDeserialize(typeof(CreateRequest), st.SearchObject);
            }
            else
            {
                st = new SearchTracer();
                objRequest = (CreateRequest)Session["Request"];
                if (Session["RequestID"] == null)
                {
                    Session["RequestID"] = Guid.NewGuid().ToString();
                    st.SearchId = Convert.ToString(Session["RequestID"]);
                }
                else
                {
                    st.SearchId = Convert.ToString(Session["RequestID"]);
                }
            }
            List<BuildHotelsRequest> objHotelReq = objRequest.HotelList.Where(a => a.MeetingroomList.Count > 0).ToList();
            if (objHotelReq.Count <= 0)
            {
                Response.Redirect("~/Agency/controlpaneluser.aspx");
            }
            rptHotels.DataSource = objHotelReq;
            rptHotels.DataBind();
            lblArrival.Text = objRequest.ArivalDate.ToString("dd MMM yyyy");
            lblDeparture.Text = objRequest.DepartureDate.ToString("dd MMM yyyy");
            //Package Selection.
            AllPackageItem = objPackagePricingManager.GetAllPackageItems();
            rptPackage.DataSource = objPackagePricingManager.GetAllPackageName(countryID);
            rptPackage.DataBind();
            //Extra Items
            SelectedExtra = objRequest.ExtraList;

            rptExtraItem.DataSource = AllPackageItem.Where(a => a.IsExtra == true && a.CountryId == countryID);
            rptExtraItem.DataBind();
            if (AllPackageItem.Where(a => a.IsExtra == true).Count() <= 0)
            {
                pnlNoExtra.Visible = true;
            }
            else
            {
                pnlNoExtra.Visible = false;
            }
            chkIsExtra.Checked = objRequest.ExtraList.Count > 0 ? true : true;
            if (SelectedExtra.Count > 0)
            {
                IsExtraDiv.Style.Add("display", "block");
            }
            else
            {
                IsExtraDiv.Style.Add("display", "block");
            }
            Int64 intHotelID = objRequest.HotelList[0].HotelID;
            AllFoodAndBravrages = objPackagePricingManager.GetAllFoodBeveragesItems(intHotelID);
            rptDaysFoodAndBragrages.DataSource = objRequest.DaysList;
            rptDaysFoodAndBragrages.DataBind();
            if (AllFoodAndBravrages.Count > 0)
            {
                rptDaysFoodAndBragrages.Visible = true;
            }
            else
            {
                rptDaysFoodAndBragrages.Visible = false;
            }
            AllEquipments = objPackagePricingManager.GetAllEquipmentItems(intHotelID);
            rptEquipmentsDay.DataSource = objRequest.DaysList;
            rptEquipmentsDay.DataBind();
            if (AllEquipments.Count > 0)
            {
                rptEquipmentsDay.Visible = true;
            }
            else
            {
                rptEquipmentsDay.Visible = false;
            }
            AllOthersItems = objPackagePricingManager.GetAllOtherItems(intHotelID);
            rptOthersDay.DataSource = objRequest.DaysList;
            rptOthersDay.DataBind();
            if (AllOthersItems.Count > 0)
            {
                rptOthersDay.Visible = true;
            }
            else
            {
                rptOthersDay.Visible = false;
            }
            lblDuration.Text = Convert.ToString(objRequest.Duration == 1 ? objRequest.Duration + GetKeyResult("DAY") : objRequest.Duration + GetKeyResult("DAY"));
            if (objRequest.RequestAccomodationList == null || objRequest.RequestAccomodationList.Count <= 0)
            {
                objRequest.RequestAccomodationList = new List<RequestAccomodation>();
                for (int date = 0; date < (objRequest.Duration == 1 ? 5 : 6); date++)
                {
                    RequestAccomodation ra = new RequestAccomodation();
                    ra.Checkin = objRequest.ArivalDate.AddDays(date - 2);
                    ra.QuantityDouble = 0;
                    ra.QuantitySingle = 0;
                    objRequest.RequestAccomodationList.Add(ra);
                }
                //txtBedroomQun.Text = Convert.ToString(objRequest.RequestAccomodationList[0].QuantityDouble);
            }
            rptDays2.DataSource = objRequest.RequestAccomodationList;
            rptDays2.DataBind();
            rptSingleQuantity.DataSource = objRequest.RequestAccomodationList;
            rptSingleQuantity.DataBind();
            rptDoubleQuantity.DataSource = objRequest.RequestAccomodationList;
            rptDoubleQuantity.DataBind();
            if (objRequest.RequestAccomodationList.Count < 6)
            {
                ltrExtendDays.Text = "<td colspan='" + (6 - objRequest.RequestAccomodationList.Count) + "' style='border-top: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;padding: 5px;' >&nbsp;</td>";
                ltrExtendSQuantity.Text = "<td colspan='" + (6 - objRequest.RequestAccomodationList.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
                ltrExtendDQuantity.Text = "<td colspan='" + (6 - objRequest.RequestAccomodationList.Count) + "' style='padding: 5px; border-top: #bfd2a5 solid 1px' >&nbsp;</td>";
            }
            //txtBedroomQun.Text = Convert.ToString(objRequest.RequestAccomodationList.QuantityDouble);
            txtSpecialRequest.Text = objRequest.SpecialRequest;
            if (objRequest.BuildYourMeetingroomList.Count > 0)
            {
                rbtnBuildYourMeeting.Checked = true;
                rbtnChoice1.Checked = false;
                Page.RegisterStartupScript("aa", "<script language='javascript' type='text/javascript'>jQuery(document).ready(function(){Choice2Selected();});</script>");
            }
            else
            {
                rbtnChoice1.Checked = true;
                rbtnBuildYourMeeting.Checked = false;
                Page.RegisterStartupScript("aa", "<script language='javascript' type='text/javascript'>jQuery(document).ready(function(){Choice1Selected();});</script>");
            }
            objRequest.SpecialRequest = txtSpecialRequest.Text;

            st.SearchObject = TrailManager.XmlSerialize(objRequest);
            //st.CurrentDay = 1;
            //st.CurrentStep = 3;
            bm.SaveSearch(st);
        }
        catch (Exception ex)
        {
            logger.Error(ex);
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }
    }
    #endregion

    protected void rptHotels_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblIndex = (Label)e.Item.FindControl("lblIndex");
            Label lblHotelName = (Label)e.Item.FindControl("lblHotelName");
            Repeater rptMeetingRoom = (Repeater)e.Item.FindControl("rptMeetingRoom");
            HiddenField hdnHotelID = (HiddenField)e.Item.FindControl("hdnHotelID");
            BuildHotelsRequest b = e.Item.DataItem as BuildHotelsRequest;
            if (b != null)
            {
                lblIndex.Text = (e.Item.ItemIndex + 1).ToString();
                Hotel objHtl = objHotel.GetHotelDetailsById(b.HotelID);
                lblHotelName.Text = objHtl.Name;
                hdnHotelID.Value = Convert.ToString(objHtl.Id);
                countryID = objHtl.CountryId;
                rptMeetingRoom.DataSource = b.MeetingroomList;
                rptMeetingRoom.DataBind();
            }
        }
    }

    protected void rptMeetingRoom_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblIndex = (Label)e.Item.FindControl("lblIndex");
            Label lblMeetingRoomName = (Label)e.Item.FindControl("lblMeetingRoomName");
            Label lblConfigurationType = (Label)e.Item.FindControl("lblConfigurationType");
            Label lblMaxandMinCapacity = (Label)e.Item.FindControl("lblMaxandMinCapacity");
            TextBox txtQuantity = (TextBox)e.Item.FindControl("txtQuantity");
            Label lblIsMain = (Label)e.Item.FindControl("lblIsMain");
            ImageButton btnDelete = (ImageButton)e.Item.FindControl("btnDelete");
            HiddenField hdnMeetingroomID = (HiddenField)e.Item.FindControl("hdnMeetingroomID");
            BuildMeetingRoomRequest brm = e.Item.DataItem as BuildMeetingRoomRequest;
            if (brm != null)
            {
                lblIndex.Text = (e.Item.ItemIndex + 1).ToString();
                hdnMeetingroomID.Value = Convert.ToString(brm.MeetingRoomID);
                MeetingRoom objMeetingroom = objHotel.GetMeetingRoomDetailsById(brm.MeetingRoomID);
                MeetingRoomDesc objDesc = objMeetingroom.MeetingRoomDescCollection.Where(a => a.LanguageId == Convert.ToInt32(Session["LanguageID"])).FirstOrDefault();
                MeetingRoomConfig objMrConfig = objMeetingroom.MeetingRoomConfigCollection.Where(a => a.Id == brm.ConfigurationID).FirstOrDefault();
                lblMeetingRoomName.Text = objMeetingroom.Name;
                lblConfigurationType.Text = (objMrConfig.RoomShapeId == (int)RoomShape.Boardroom ? RoomShape.Boardroom.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Classroom ? RoomShape.Classroom.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Cocktail ? RoomShape.Cocktail.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.School ? RoomShape.School.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.Theatre ? RoomShape.Theatre.ToString() : objMrConfig.RoomShapeId == (int)RoomShape.UShape ? RoomShape.UShape.ToString() : RoomShape.Boardroom.ToString());
                lblMaxandMinCapacity.Text = objMrConfig.MinCapacity + " - " + objMrConfig.MaxCapicity;
                txtQuantity.Text = brm.Quantity.ToString();
                lblIsMain.Text = brm.IsMain == true ? GetKeyResult("MAIN") : GetKeyResult("BREAKOUT");
                btnDelete.CommandArgument = objMeetingroom.Id + "," + objMeetingroom.HotelId;
            }
        }
    }

    protected void dtLstMeetingroom_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "DeleteMeetingroom")
        {
            SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["RequestID"]));
            if (st != null)
            {
                objRequest = (CreateRequest)TrailManager.XmlDeserialize(typeof(CreateRequest), st.SearchObject);
            }
            else
            {
                st = new SearchTracer();
                objRequest = (CreateRequest)Session["Request"];
                if (Session["RequestID"] == null)
                {
                    Session["RequestID"] = Guid.NewGuid().ToString();
                    st.SearchId = Convert.ToString(Session["RequestID"]);
                }
                else
                {
                    st.SearchId = Convert.ToString(Session["RequestID"]);
                }
            }
            string allval = e.CommandArgument.ToString();
            BuildMeetingRoomRequest objMeetingRoom = objRequest.HotelList.Where(a => a.HotelID == Convert.ToInt64(allval.Split(',')[1])).FirstOrDefault().MeetingroomList.Where(a => a.MeetingRoomID == Convert.ToInt64(allval.Split(',')[0])).FirstOrDefault();
            objRequest.HotelList.Where(a => a.HotelID == Convert.ToInt64(allval.Split(',')[1])).FirstOrDefault().MeetingroomList.Remove(objMeetingRoom);
            if (objRequest.HotelList.Where(a => a.HotelID == Convert.ToInt64(allval.Split(',')[1])).FirstOrDefault().MeetingroomList.Count > 0)
            {
                objRequest.HotelList.Where(a => a.HotelID == Convert.ToInt64(allval.Split(',')[1])).FirstOrDefault().MeetingroomList[0].IsMain = true;
            }
            Session["Request"] = objRequest;
            st.SearchObject = TrailManager.XmlSerialize(objRequest);
            if (bm.SaveSearch(st))
            {
                BindHotel();
            }
        }
    }

    protected void rptPackage_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            RadioButton rbtnPackage = (RadioButton)e.Item.FindControl("rbtnPackage");
            Label lblPackageName = (Label)e.Item.FindControl("lblPackageName");
            Label lblpackageDescription = (Label)e.Item.FindControl("lblpackageDescription");
            HtmlControl OpenDiv = (HtmlControl)e.Item.FindControl("OpenDiv");
            Repeater rptPackageItem = (Repeater)e.Item.FindControl("rptPackageItem");
            HiddenField hdnPackageID = (HiddenField)e.Item.FindControl("hdnPackageID");
            PackageMaster p = e.Item.DataItem as PackageMaster;
            if (p != null)
            {
                lblPackageName.Text = p.PackageName;
                hdnPackageID.Value = Convert.ToString(p.Id);
                objHotel.GetDescriptionofPackageByPackage(p);
                PackageMasterDescription des = p.PackageMasterDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64(Session["LanguageID"])).FirstOrDefault();
                if (des == null)
                {
                    des = p.PackageMasterDescriptionCollection.Where(a => a.LanguageId == Convert.ToInt64("1")).FirstOrDefault();
                }
                if (des != null)
                {
                    lblpackageDescription.Text = des.Description;
                }
                if (p.Id == objRequest.PackageID)
                {
                    rbtnPackage.Checked = true;
                    OpenDiv.Style.Add("display", "block");
                    if (SelectedPackage == null)
                    {
                        SelectedPackage = new List<PackageItemDetails>();
                    }
                    SelectedPackage = objRequest.PackageItemList;
                }
                else
                {
                    rbtnPackage.Checked = false;
                    OpenDiv.Style.Add("display", "none");
                    SelectedPackage = null;
                }

                rptPackageItem.DataSource = objPackagePricingManager.GetPackageItemsByPackageID(p.Id);
                rptPackageItem.DataBind();
            }
        }
    }

    protected void rptPackageItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            TextBox txtQuantity = (TextBox)e.Item.FindControl("txtQuantity");
            HiddenField hdnItemId = (HiddenField)e.Item.FindControl("hdnItemId");
            //AllPackageItem
            PackageItemMapping pim = e.Item.DataItem as PackageItemMapping;
            if (pim != null)
            {
                PackageItems p = AllPackageItem.Where(a => a.Id == pim.ItemId).FirstOrDefault();
                if (p != null)
                {
                    hdnItemId.Value = Convert.ToString(p.Id);
                    //lblItemName.Text = p.ItemName;
                    PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                    if (pdesc != null)
                    {
                        lblDescription.Text = pdesc.ItemDescription;
                        lblItemName.Text = pdesc.ItemName;
                    }
                    else
                    {
                        lblDescription.Text = "";
                        lblItemName.Text = p.ItemName;
                    }
                    if (SelectedPackage != null)
                    {
                        PackageItemDetails pid = SelectedPackage.Where(a => a.ItemID == p.Id).FirstOrDefault();
                        if (pid != null)
                        {
                            txtQuantity.Text = Convert.ToString(pid.Quantity);
                        }
                        else
                        {
                            txtQuantity.Text = "0";
                        }
                    }
                    else
                    {
                        txtQuantity.Text = "0";
                    }
                }
            }
        }
    }

    protected void rptExtraItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            HiddenField hdnItemId = (HiddenField)e.Item.FindControl("hdnItemId");
            TextBox txtQuantity = (TextBox)e.Item.FindControl("txtQuantity");
            PackageItems p = e.Item.DataItem as PackageItems;
            if (p != null)
            {
                //lblItemName.Text = p.ItemName;
                hdnItemId.Value = Convert.ToString(p.Id);
                PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                if (pdesc != null)
                {
                    lblDescription.Text = pdesc.ItemDescription;
                    lblItemName.Text = pdesc.ItemName;
                }
                else
                {
                    lblDescription.Text = "";
                    lblItemName.Text = p.ItemName;
                }
                if (SelectedExtra != null)
                {
                    RequestExtra pid = SelectedExtra.Where(a => a.ItemID == p.Id).FirstOrDefault();
                    if (pid != null)
                    {
                        txtQuantity.Text = Convert.ToString(pid.Quantity);
                    }
                    else
                    {
                        txtQuantity.Text = "0";
                    }
                }
                else
                {
                    txtQuantity.Text = "0";
                }
            }
        }
    }

    public List<BuildYourMeetingroomRequest> CurrentFoodAndBravrages
    {
        get;
        set;
    }
    protected void rptDaysFoodAndBragrages_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblSelectDay = (Label)e.Item.FindControl("lblSelectDay");
            Repeater rptFoodAndBravrage = (Repeater)e.Item.FindControl("rptFoodAndBravrage");
            NumberOfDays objNoofDay = e.Item.DataItem as NumberOfDays;
            if (objNoofDay != null)
            {
                lblSelectDay.Text = Convert.ToString(objNoofDay.SelectedDay);
                if (objRequest.BuildYourMeetingroomList == null)
                {
                    objRequest.BuildYourMeetingroomList = new List<BuildYourMeetingroomRequest>();
                }
                CurrentFoodAndBravrages = objRequest.BuildYourMeetingroomList.Where(a => a.SelectDay == objNoofDay.SelectedDay).ToList();
                rptFoodAndBravrage.DataSource = AllFoodAndBravrages;
                rptFoodAndBravrage.DataBind();
            }
        }
    }

    protected void rptFoodAndBravrage_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            TextBox txtQuantity = (TextBox)e.Item.FindControl("txtQuantity");
            HiddenField hdnItemId = (HiddenField)e.Item.FindControl("hdnItemId");
            PackageItems p = e.Item.DataItem as PackageItems;
            if (p != null)
            {
                //lblItemName.Text = p.ItemName;
                hdnItemId.Value = Convert.ToString(p.Id);
                PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                if (pdesc != null)
                {
                    lblDescription.Text = pdesc.ItemDescription;
                    lblItemName.Text = pdesc.ItemName;
                }
                else
                {
                    lblDescription.Text = "";
                    lblItemName.Text = p.ItemName;
                }
                if (CurrentFoodAndBravrages != null)
                {
                    BuildYourMeetingroomRequest pid = CurrentFoodAndBravrages.Where(a => a.ItemID == p.Id).FirstOrDefault();
                    if (pid != null)
                    {
                        txtQuantity.Text = Convert.ToString(pid.Quantity);
                    }
                    else
                    {
                        txtQuantity.Text = "0";
                    }
                }
                else
                {
                    txtQuantity.Text = "0";
                }
            }
        }
    }

    public List<RequestEquipment> CurrentEquipment
    {
        get;
        set;
    }
    protected void rptEquipmentsDay_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblSelectDay = (Label)e.Item.FindControl("lblSelectDay");
            Repeater rptEquipment = (Repeater)e.Item.FindControl("rptEquipment");
            NumberOfDays objNoofDay = e.Item.DataItem as NumberOfDays;
            if (objNoofDay != null)
            {
                lblSelectDay.Text = Convert.ToString(objNoofDay.SelectedDay);
                if (objRequest.EquipmentList == null)
                {
                    objRequest.EquipmentList = new List<RequestEquipment>();
                }
                CurrentEquipment = objRequest.EquipmentList.Where(a => a.SelectedDay == objNoofDay.SelectedDay).ToList();
                rptEquipment.DataSource = AllEquipments;
                rptEquipment.DataBind();
            }
        }
    }
    public List<RequestOthers> CurrentOthers
    {
        get;
        set;
    }
    protected void rptOthersDay_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblSelectDay = (Label)e.Item.FindControl("lblSelectDay");
            Repeater rptOthers = (Repeater)e.Item.FindControl("rptOthers");
            NumberOfDays objNoofDay = e.Item.DataItem as NumberOfDays;
            if (objNoofDay != null)
            {
                lblSelectDay.Text = Convert.ToString(objNoofDay.SelectedDay);
                if (objRequest.EquipmentList == null)
                {
                    objRequest.OthersList = new List<RequestOthers>();
                }
                CurrentOthers = objRequest.OthersList.Where(a => a.SelectedDay == objNoofDay.SelectedDay).ToList();                
                if (AllOthersItems.Count > 0)
                {
                    rptOthers.DataSource = AllOthersItems;
                    rptOthers.DataBind();
                    divrptOthersDay.Visible = true;
                }
                else
                {
                    divrptOthersDay.Visible = false;
                }
            }
        }
    }
    protected void rptEquipment_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            TextBox txtQuantity = (TextBox)e.Item.FindControl("txtQuantity");
            HiddenField hdnItemId = (HiddenField)e.Item.FindControl("hdnItemId");
            PackageItems p = e.Item.DataItem as PackageItems;
            if (p != null)
            {
                //lblItemName.Text = p.ItemName;
                hdnItemId.Value = Convert.ToString(p.Id);
                PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                if (pdesc != null)
                {
                    lblDescription.Text = pdesc.ItemDescription;
                    lblItemName.Text = pdesc.ItemName;
                }
                else
                {
                    lblDescription.Text = "";
                    lblItemName.Text = p.ItemName;
                }
                //if (SelectedPackage != null)
                //{
                RequestEquipment pid = CurrentEquipment.Where(a => a.ItemID == p.Id).FirstOrDefault();
                if (pid != null)
                {
                    txtQuantity.Text = Convert.ToString(pid.Quantity);
                }
                else
                {
                    txtQuantity.Text = "0";
                }
                //}
                //else
                //{
                //    txtQuantity.Text = "0";
                //}
            }
        }
    }
    protected void rptOthers_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblItemName = (Label)e.Item.FindControl("lblItemName");
            Label lblDescription = (Label)e.Item.FindControl("lblDescription");
            TextBox txtQuantity = (TextBox)e.Item.FindControl("txtQuantity");
            HiddenField hdnItemId = (HiddenField)e.Item.FindControl("hdnItemId");
            PackageItems p = e.Item.DataItem as PackageItems;
            if (p != null)
            {
                //lblItemName.Text = p.ItemName;
                hdnItemId.Value = Convert.ToString(p.Id);
                PackageDescription pdesc = p.PackageDescriptionCollection.Where(a => a.LanguageId == (Convert.ToInt64(Session["LanguageID"]) == 0 ? 1 : Convert.ToInt64(Session["LanguageID"]))).FirstOrDefault();
                if (pdesc != null)
                {
                    lblDescription.Text = pdesc.ItemDescription;
                    lblItemName.Text = pdesc.ItemName;
                }
                else
                {
                    lblDescription.Text = "";
                    lblItemName.Text = p.ItemName;
                }
                //if (SelectedPackage != null)
                //{
                RequestOthers pid = CurrentOthers.Where(a => a.ItemID == p.Id).FirstOrDefault();
                if (pid != null)
                {
                    txtQuantity.Text = Convert.ToString(pid.Quantity);
                }
                else
                {
                    txtQuantity.Text = "0";
                }
                //}
                //else
                //{
                //    txtQuantity.Text = "0";
                //}
            }
        }
    }
    protected void lnkPrevious_Click(object sender, EventArgs e)
    {
        SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["RequestID"]));
        objRequest = (CreateRequest)TrailManager.XmlDeserialize(typeof(CreateRequest), st.SearchObject);
        st.CurrentStep = 1;
        st.CurrentDay = 1;
        st.SearchObject = TrailManager.XmlSerialize(objRequest);
        if (bm.SaveSearch(st))
        {
            //Response.Redirect("RequestStep1.aspx");
            //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
            if (l != null)
            {
                Response.Redirect(SiteRootPath + "request-step1/" + l.Name.ToLower());
            }
            else
            {
                Response.Redirect(SiteRootPath + "request-step1/english");
            }
        }
    }

    protected void lnkNext_Click(object sender, EventArgs e)
    {
        SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["RequestID"]));
        objRequest = (CreateRequest)TrailManager.XmlDeserialize(typeof(CreateRequest), st.SearchObject);
        for (int i = 0; i < rptHotels.Items.Count; i++)
        {
            HiddenField hdnHotelID = (HiddenField)rptHotels.Items[i].FindControl("hdnHotelID");
            Repeater rptMeetingRoom = (Repeater)rptHotels.Items[i].FindControl("rptMeetingRoom");
            for (int j = 0; j < rptMeetingRoom.Items.Count; j++)
            {
                HiddenField hdnMeetingroomID = (HiddenField)rptMeetingRoom.Items[j].FindControl("hdnMeetingroomID");
                TextBox txtQuantity = (TextBox)rptMeetingRoom.Items[j].FindControl("txtQuantity");
                objRequest.HotelList.Where(a => a.HotelID == Convert.ToInt64(hdnHotelID.Value)).FirstOrDefault().MeetingroomList.Where(a => a.MeetingRoomID == Convert.ToInt64(hdnMeetingroomID.Value)).FirstOrDefault().Quantity = Convert.ToInt32(txtQuantity.Text);
            }
        }
        if (rbtnChoice1.Checked)
        {
            for (int k = 0; k < rptPackage.Items.Count; k++)
            {
                RadioButton rbtnPackage = (RadioButton)rptPackage.Items[k].FindControl("rbtnPackage");
                HiddenField hdnPackageID = (HiddenField)rptPackage.Items[k].FindControl("hdnPackageID");
                Repeater rptPackageItem = (Repeater)rptPackage.Items[k].FindControl("rptPackageItem");
                objRequest.PackageItemList = new List<PackageItemDetails>();
                if (rbtnPackage.Checked)
                {
                    objRequest.PackageID = Convert.ToInt64(hdnPackageID.Value);

                    for (int l = 0; l < rptPackageItem.Items.Count; l++)
                    {
                        HiddenField hdnItemId = (HiddenField)rptPackageItem.Items[l].FindControl("hdnItemId");
                        TextBox txtQuantity = (TextBox)rptPackageItem.Items[l].FindControl("txtQuantity");
                        if (Convert.ToInt32(string.IsNullOrEmpty(txtQuantity.Text) ? "0" : txtQuantity.Text) > 0)
                        {
                            if (objRequest.PackageItemList == null)
                            {
                                objRequest.PackageItemList = new List<PackageItemDetails>();
                            }
                            PackageItemDetails p = new PackageItemDetails();
                            p.ItemID = Convert.ToInt64(hdnItemId.Value);
                            p.Quantity = Convert.ToInt32(string.IsNullOrEmpty(txtQuantity.Text) ? "0" : txtQuantity.Text);
                            objRequest.PackageItemList.Add(p);
                        }
                    }
                    break;
                }
            }
            if (chkIsExtra.Checked)
            {
                objRequest.ExtraList = new List<RequestExtra>();
                for (int ext = 0; ext < rptExtraItem.Items.Count; ext++)
                {
                    HiddenField hdnItemId = (HiddenField)rptExtraItem.Items[ext].FindControl("hdnItemId");
                    TextBox txtQuantity = (TextBox)rptExtraItem.Items[ext].FindControl("txtQuantity");
                    if (Convert.ToInt32(string.IsNullOrEmpty(txtQuantity.Text) ? "0" : txtQuantity.Text) > 0)
                    {
                        if (objRequest.ExtraList == null)
                        {
                            objRequest.ExtraList = new List<RequestExtra>();
                        }
                        RequestExtra p = new RequestExtra();
                        p.ItemID = Convert.ToInt64(hdnItemId.Value);
                        p.Quantity = Convert.ToInt32(string.IsNullOrEmpty(txtQuantity.Text) ? "0" : txtQuantity.Text);
                        objRequest.ExtraList.Add(p);
                    }
                }
            }
            objRequest.BuildYourMeetingroomList = new List<BuildYourMeetingroomRequest>();
        }
        else
        {
            objRequest.PackageID = 0;
            objRequest.PackageItemList = new List<PackageItemDetails>();
            objRequest.ExtraList = new List<RequestExtra>();
            objRequest.BuildYourMeetingroomList = new List<BuildYourMeetingroomRequest>();
            if (rbtnBuildYourMeeting.Checked)
            {
                for (int fb = 0; fb < rptDaysFoodAndBragrages.Items.Count; fb++)
                {
                    Label lblSelectDay = (Label)rptDaysFoodAndBragrages.Items[fb].FindControl("lblSelectDay");
                    Repeater rptFoodAndBravrage = (Repeater)rptDaysFoodAndBragrages.Items[fb].FindControl("rptFoodAndBravrage");

                    for (int fbi = 0; fbi < rptFoodAndBravrage.Items.Count; fbi++)
                    {
                        HiddenField hdnItemId = (HiddenField)rptFoodAndBravrage.Items[fbi].FindControl("hdnItemId");
                        TextBox txtQuantity = (TextBox)rptFoodAndBravrage.Items[fbi].FindControl("txtQuantity");
                        if (Convert.ToInt32(string.IsNullOrEmpty(txtQuantity.Text) ? "0" : txtQuantity.Text) > 0)
                        {
                            if (objRequest.BuildYourMeetingroomList == null)
                            {
                                objRequest.BuildYourMeetingroomList = new List<BuildYourMeetingroomRequest>();
                            }
                            BuildYourMeetingroomRequest p = new BuildYourMeetingroomRequest();
                            p.SelectDay = Convert.ToInt32(lblSelectDay.Text);
                            p.ItemID = Convert.ToInt64(hdnItemId.Value);
                            p.Quantity = Convert.ToInt32(string.IsNullOrEmpty(txtQuantity.Text) ? "0" : txtQuantity.Text);
                            objRequest.BuildYourMeetingroomList.Add(p);
                        }
                    }
                }
            }
        }
        objRequest.EquipmentList = new List<RequestEquipment>();
        for (int eq = 0; eq < rptEquipmentsDay.Items.Count; eq++)
        {
            Label lblSelectDay = (Label)rptEquipmentsDay.Items[eq].FindControl("lblSelectDay");
            Repeater rptEquipment = (Repeater)rptEquipmentsDay.Items[eq].FindControl("rptEquipment");

            for (int eqi = 0; eqi < rptEquipment.Items.Count; eqi++)
            {
                HiddenField hdnItemId = (HiddenField)rptEquipment.Items[eqi].FindControl("hdnItemId");
                TextBox txtQuantity = (TextBox)rptEquipment.Items[eqi].FindControl("txtQuantity");
                if (Convert.ToInt32(string.IsNullOrEmpty(txtQuantity.Text) ? "0" : txtQuantity.Text) > 0)
                {
                    if (objRequest.EquipmentList == null)
                    {
                        objRequest.EquipmentList = new List<RequestEquipment>();
                    }
                    RequestEquipment p = new RequestEquipment();
                    p.SelectedDay = Convert.ToInt32(lblSelectDay.Text);
                    p.ItemID = Convert.ToInt64(hdnItemId.Value);
                    p.Quantity = Convert.ToInt32(string.IsNullOrEmpty(txtQuantity.Text) ? "0" : txtQuantity.Text);
                    objRequest.EquipmentList.Add(p);
                }
            }
        }
        objRequest.OthersList = new List<RequestOthers>();
        for (int eq = 0; eq < rptOthersDay.Items.Count; eq++)
        {
            Label lblSelectDay = (Label)rptOthersDay.Items[eq].FindControl("lblSelectDay");
            Repeater rptOthers = (Repeater)rptOthersDay.Items[eq].FindControl("rptOthers");

            for (int eqi = 0; eqi < rptOthers.Items.Count; eqi++)
            {
                HiddenField hdnItemId = (HiddenField)rptOthers.Items[eqi].FindControl("hdnItemId");
                TextBox txtQuantity = (TextBox)rptOthers.Items[eqi].FindControl("txtQuantity");
                if (Convert.ToInt32(string.IsNullOrEmpty(txtQuantity.Text) ? "0" : txtQuantity.Text) > 0)
                {
                    if (objRequest.OthersList == null)
                    {
                        objRequest.OthersList = new List<RequestOthers>();
                    }
                    RequestOthers p = new RequestOthers();
                    p.SelectedDay = Convert.ToInt32(lblSelectDay.Text);
                    p.ItemID = Convert.ToInt64(hdnItemId.Value);
                    p.Quantity = Convert.ToInt32(string.IsNullOrEmpty(txtQuantity.Text) ? "0" : txtQuantity.Text);
                    objRequest.OthersList.Add(p);
                }
            }
        }
        //if (Convert.ToInt32(string.IsNullOrEmpty(txtBedroomQun.Text) ? "0" : txtBedroomQun.Text) > 0)
        //{
        objRequest.IsAccomodation = true;
        if (objRequest.RequestAccomodationList == null)
        {
            objRequest.RequestAccomodationList = new List<RequestAccomodation>();
        }
        else
        {
            foreach (RepeaterItem r in rptSingleQuantity.Items)
            {
                TextBox txtQuantitySDay = (TextBox)r.FindControl("txtQuantitySDay");
                HiddenField hdnDate = (HiddenField)r.FindControl("hdnDate");
                if (string.IsNullOrEmpty(txtQuantitySDay.Text.Trim()))
                {
                    objRequest.RequestAccomodationList.Where(a => a.Checkin.ToString() == hdnDate.Value).FirstOrDefault().QuantitySingle = 0;
                }
                else
                {
                    objRequest.RequestAccomodationList.Where(a => a.Checkin.ToString() == hdnDate.Value).FirstOrDefault().QuantitySingle = Convert.ToInt32(txtQuantitySDay.Text);
                }
            }
            foreach (RepeaterItem r in rptDoubleQuantity.Items)
            {
                TextBox txtQuantityDDay = (TextBox)r.FindControl("txtQuantityDDay");
                HiddenField hdnDate = (HiddenField)r.FindControl("hdnDate");
                if (string.IsNullOrEmpty(txtQuantityDDay.Text.Trim()))
                {
                    objRequest.RequestAccomodationList.Where(a => a.Checkin.ToString() == hdnDate.Value).FirstOrDefault().QuantityDouble = 0;
                }
                else
                {
                    objRequest.RequestAccomodationList.Where(a => a.Checkin.ToString() == hdnDate.Value).FirstOrDefault().QuantityDouble = Convert.ToInt32(txtQuantityDDay.Text);
                }
            }
        }
        //objRequest.RequestAccomodationList[0].QuantityDouble = Convert.ToInt32(string.IsNullOrEmpty(txtBedroomQun.Text) ? "0" : txtBedroomQun.Text);
        //}
        objRequest.SpecialRequest = txtSpecialRequest.Text;
        st.SearchObject = TrailManager.XmlSerialize(objRequest);
        st.CurrentDay = 1;
        st.CurrentStep = 3;
        if (bm.SaveSearch(st))
        {
            Response.Redirect("~/Agency/request.aspx");
        }
    }



    #region New Accommodation
    protected void rptDays2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblDay = (Label)e.Item.FindControl("lblDay");
            RequestAccomodation a = e.Item.DataItem as RequestAccomodation;
            lblDay.Text = a.Checkin.ToString("dd/MM/yyyy");
        }
    }
    protected void rptSingleQuantity_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            TextBox txtQuantitySDay = (TextBox)e.Item.FindControl("txtQuantitySDay");
            HiddenField hdnDate = (HiddenField)e.Item.FindControl("hdnDate");
            RequestAccomodation a = e.Item.DataItem as RequestAccomodation;
            txtQuantitySDay.Text = a.QuantitySingle.ToString();
            hdnDate.Value = a.Checkin.ToString();
        }
    }
    protected void rptDoubleQuantity_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            TextBox txtQuantityDDay = (TextBox)e.Item.FindControl("txtQuantityDDay");
            HiddenField hdnDate = (HiddenField)e.Item.FindControl("hdnDate");
            RequestAccomodation a = e.Item.DataItem as RequestAccomodation;
            txtQuantityDDay.Text = a.QuantityDouble.ToString();
            hdnDate.Value = a.Checkin.ToString();
        }
    }
    #endregion
}