﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using System.Xml;
using System.Web.Services;
using System.Xml.Serialization;

public partial class Agency_Main : System.Web.UI.MasterPage
{
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl + "/";
        }
    }
    public event EventHandler contentCallEvent;

    public Language l
    {
        get;
        set;
    }
    public XmlDocument xmlLanguage
    {
        get
        {
            if (Session["xmlLanguage"] != null)
            {
                return (Session["xmlLanguage"] as XmlDocument);
            }
            else
            {
                return null;
            }
        }
        set
        {
            Session["xmlLanguage"] = value;
        }
    }
    public string TimeoutTime
    {
        get;
        set;
    }
    public string GetKeyResult(string key)
    {
        if (Session["LanguageChange"] != null)
        {
            xmlLanguage = null;
            Session["LanguageChange"] = null;
        }
        if (Session["Language"] != null)
        {
            Language currentL = Session["Language"] as Language;
            if (Request.RawUrl.ToLower().Contains(currentL.Name.ToLower()))
            {
                if (xmlLanguage != null)
                {
                    XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                    return System.Net.WebUtility.HtmlDecode(nodes.InnerText);
                }
                else
                {
                    xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
                    XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                    return System.Net.WebUtility.HtmlDecode(nodes.InnerText);
                }
            }
            else
            {
                xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
                XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                return System.Net.WebUtility.HtmlDecode(nodes.InnerText);
            }
        }
        else
        {
            xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
            XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
            return System.Net.WebUtility.HtmlDecode(nodes.InnerText);
        }
        //return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    }
    public string GetKeyResultForJavaScript(string key)
    {
        if (Session["LanguageChange"] != null)
        {
            xmlLanguage = null;
            Session["LanguageChange"] = null;
        }
        if (Session["Language"] != null)
        {
            Language currentL = Session["Language"] as Language;
            if (Request.RawUrl.ToLower().Contains(currentL.Name.ToLower()))
            {
                if (xmlLanguage != null)
                {
                    XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                    return System.Net.WebUtility.HtmlDecode(nodes.InnerText).Replace("'", "\\'").Replace("\"", "\\\"");
                }
                else
                {
                    xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
                    XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                    return System.Net.WebUtility.HtmlDecode(nodes.InnerText).Replace("'", "\\'").Replace("\"", "\\\"");
                }
            }
            else
            {
                xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
                XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
                return System.Net.WebUtility.HtmlDecode(nodes.InnerText).Replace("'", "\\'").Replace("\"", "\\\"");
            }
        }
        else
        {
            xmlLanguage = ResultManager.GetLanguageXMLFile(Convert.ToInt64(Session["LanguageID"]));
            XmlNode nodes = xmlLanguage.SelectSingleNode("items/item[@key='" + key + "']");
            return System.Net.WebUtility.HtmlDecode(nodes.InnerText).Replace("'", "\\'").Replace("\"", "\\\"");
        }
        //return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key)).Replace("'", "\\'").Replace("\"", "\\\"");
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.RawUrl.ToLower().Contains("default") || Request.RawUrl.ToLower().Contains("searchbookingrequest") || Request.RawUrl.ToLower().Contains("booking-step") || Request.RawUrl.ToLower().Contains("bootingstep") || Request.RawUrl.ToLower().Contains("requeststep") || Request.RawUrl.ToLower().Contains("request-step") || Request.RawUrl.ToLower().Contains("request") || Request.RawUrl.ToLower().Contains("booking"))
        {
            hdrltr.Text = "<script src=\"http://maps.google.com/maps/api/js?sensor=false\" type=\"text/javascript\"></script>";
        }
        if (!Page.IsPostBack)
        {
            Users objUsers = Session["CurrentAgencyUser"] as Users;
            if (objUsers != null)
            {
                lblLoginUserName.Text = objUsers.FirstName + " " + objUsers.LastName;
                lblDateLogin.Text = DateTime.Now.ToLongDateString();
                if (objUsers.Usertype == (int)Usertype.Agency)
                {
                    lnkChangeUser.Visible = true;
                    switchUser();
                }
                else
                {
                    lnkChangeUser.Visible = false;
                }
            }
        }
        TimeoutTime = GetExectTimeOut(new HotelManager().GetSessionTimeoutValue());
    }

    public string GetExectTimeOut(string timeout)
    {
        SearchTracer st = null;
        Createbooking objBooking = null;
        BookingManager bm = new BookingManager();
        if (objBooking == null)
        {
            if (Session["SerachID"] != null)
            {
                objBooking = Session["Search"] as Createbooking;
                st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
            }
            else
            {
                st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
                if (st != null)
                {
                    objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
                }
            }
        }
        if (objBooking != null && st != null)
        {
            DateTime ExpireTime = st.CreationDate.AddMinutes(Convert.ToDouble(timeout));
            TimeSpan t = DateTime.Now.Subtract(st.CreationDate);
            if (Convert.ToDouble(timeout) > t.Minutes)
            {
                if (t.Minutes == Convert.ToDouble(timeout) - 1 && t.Seconds > 0)
                {
                    return Convert.ToString(((Convert.ToDouble(timeout) - (t.Minutes + (t.Seconds * .01))) * 60000) + 30000);
                }
                else
                {
                    return Convert.ToString((Convert.ToDouble(timeout) - (t.Minutes + (t.Seconds * .01))) * 60000);
                }
            }
            else
            {
                return "0";
            }
        }
        else
        {
            return (Convert.ToDouble(timeout) * 60000).ToString();
        }
    }
    void switchUser()
    {
        Users u = Session["CurrentAgencyUser"] as Users;
        if (u != null)
        {
            if (!u.WorkAsAgentUser)
            {
                lnkChangeUser.Text = "Work as user";
            }
            else
            {
                lnkChangeUser.Text = "Work as admin";
            }
        }
    }
    protected void lnkChangeUser_OnClick(object sender, EventArgs e)
    {
        Users u = Session["CurrentAgencyUser"] as Users;
        if (u != null)
        {
            if (!u.WorkAsAgentUser)
            {
                u.WorkAsAgentUser = true;
                lnkChangeUser.Text = "Work as agency";
                Server.Transfer("ControlPanelUser.aspx");
            }
            else
            {
                u.WorkAsAgentUser = false;
                lnkChangeUser.Text = "Work as user";
                Server.Transfer("ControlPanel.aspx");
            }
            Session["CurrentAgentUser"] = u;
        }
    }

    protected void lnklogout_Click(object sender, EventArgs e)
    {
        Session.Remove("CurrentAgencyUserID");
        Session.Remove("CurrentAgencyUser");
        Session.Remove("SerachID");
        Session.Remove("CurrencyID");
        Session.Remove("RequestID");
        Session.Remove("masterInput");
        Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        Session.Abandon();
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "login/english");
        }
    }

    protected void btnResume_OnClick(object sender, EventArgs e)
    {
        BookingManager bm = new BookingManager();
        SearchTracer st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
        st.CreationDate = DateTime.Now;
        bm.SaveSearch(st);
        if (contentCallEvent != null)
            contentCallEvent(this, EventArgs.Empty);
        Response.Redirect(Request.RawUrl);
    }

    protected void btnNo_OnClick(object sender, EventArgs e)
    {
        SearchTracer st = null;
        Createbooking objBooking = null;
        BookingManager bm = new BookingManager();
        if (objBooking == null)
        {
            if (Session["SerachID"] == null)
            {
                objBooking = Session["Search"] as Createbooking;
            }
            else
            {
                st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
                objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
            }
        }
        bm.ResetIsReserverFalse(objBooking);
        Session.Remove("Search");
        Session.Remove("SearchID");
        Session.Remove("Request");
        Session.Remove("RequestID");
        Session.Remove("Request");
        Response.Redirect("~/Agency/ControlPanelUser.aspx");
        ////Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        //if (Request.QueryString["wl"] == null)
        //{
        //    if (l != null)
        //    {
        //        Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
        //    }
        //    else
        //    {
        //        Response.Redirect(SiteRootPath + "default/english");
        //    }
        //}
        //else
        //{
        //    Response.Redirect("~/WL/Expired.aspx");
        //}
    }

    protected void btnLoginAgain_OnClick(object sender, EventArgs e)
    {

        SearchTracer st = null;
        Createbooking objBooking = null;
        BookingManager bm = new BookingManager();
        if (objBooking == null)
        {
            if (Session["SerachID"] == null)
            {
                objBooking = Session["Search"] as Createbooking;
            }
            else
            {
                st = bm.GetSearchDetailsBySearchID(Convert.ToString(Session["SerachID"]));
                objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st.SearchObject);
            }
        }
        bm.ResetIsReserverFalse(objBooking);
        Session.Remove("Search");
        Session.Remove("SearchID");
        Session.Remove("Request");
        Session.Remove("RequestID");
        Session.Remove("Request");
        Response.Redirect("~/Agency/ControlPanelUser.aspx");
        ////Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        //if (Request.QueryString["wl"] == null)
        //{
        //    if (l != null)
        //    {
        //        Response.Redirect(SiteRootPath + "default/" + l.Name.ToLower());
        //    }
        //    else
        //    {
        //        Response.Redirect(SiteRootPath + "default/english");
        //    }
        //}
        //else
        //{
        //    Response.Redirect("~/WL/Expired.aspx");
        //}
    }
}
