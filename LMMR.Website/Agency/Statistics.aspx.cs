﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Entities;
using LMMR.Data;
using LMMR.Business;
using System.Web.UI.DataVisualization.Charting;

public partial class Agency_Statistics : System.Web.UI.Page
{
    #region Variables and properties
   
    Viewstatistics objViewstatistics = new Viewstatistics();
   
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    { if (Session["CurrentAgencyUserID"] == null)
        {
            Response.Redirect("~/login.aspx", false);
            return;
        }
    if (!Page.IsPostBack)
    {
        //Session check               
        if (Session["CurrentAgencyUserID"] == null)
        {
            Response.Redirect("~/login.aspx", false);
            return;
        }
        //objViewstatistics.HotelId = (Session["CurrentHotelID"].ToString());
        //Session check
        Users objUsers = (Users)Session["CurrentAgencyUser"];
        TList< UserDetails> userdetail = DataRepository.UserDetailsProvider.GetByUserId( Convert.ToInt64(objUsers.UserId) );
        txtFromdate.Text = System.DateTime.Now.AddDays(-30).ToString("dd/MM/yyyy");
        txtTodate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
        lblBooking.Text = "Statistics Booking made by :" + objUsers.FirstName + " " + objUsers.LastName+" :"+userdetail[0].CompanyName;
        lblhead.Text = "Statistics Request made by :" + objUsers.FirstName + " " + objUsers.LastName + " :" + userdetail[0].CompanyName;
        Bindlist(txtFromdate.Text, txtTodate.Text, "days");
        divmessage.Style.Add("display", "none");


    }
    else
    {
        if (string.IsNullOrEmpty(txtFromdate.Text))
        {
            Users objUsers = (Users)Session["CurrentAgencyUser"];
            TList<UserDetails> userdetail = DataRepository.UserDetailsProvider.GetByUserId(Convert.ToInt64(objUsers.UserId));
            txtFromdate.Text = System.DateTime.Now.AddDays(-30).ToString("dd/MM/yyyy");
            txtTodate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            lblBooking.Text = "Statistics Booking made by :" + objUsers.FirstName + " " + objUsers.LastName + " :" + userdetail[0].CompanyName; 
            lblhead.Text = "Statistics Request made by :" + objUsers.FirstName + " " + objUsers.LastName + " :" + userdetail[0].CompanyName;
            Bindlist(txtFromdate.Text, txtTodate.Text, "days");
            divmessage.Style.Add("display", "none");
        }
    
    }
       
    }
    #region fliter by Days
    /// <summary>
    /// method to fliter by Days     
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkDays_Click(object sender, EventArgs e)
    {
        Users objUsers = (Users)Session["CurrentAgencyUser"];
        TList<UserDetails> userdetail = DataRepository.UserDetailsProvider.GetByUserId(Convert.ToInt64(objUsers.UserId));
        Bindlist(txtFromdate.Text, txtTodate.Text, "days");
        lblBooking.Text = "Statistics Booking made by :" + objUsers.FirstName + " " + objUsers.LastName + " :" + userdetail[0].CompanyName;
        lblhead.Text = "Statistics Request made by :" + objUsers.FirstName + " " + objUsers.LastName + " :" + userdetail[0].CompanyName;
        lblReqHead.Text = "";
        lnkMonths.Attributes.Add("class", "btn");
        lnkDays.Attributes.Add("class", "btn-active");
    }
    #endregion
    #region fliter by Month
    /// <summary>
    /// method to fliter by Month     
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkMonths_Click(object sender, EventArgs e)
    {
        Users objUsers = (Users)Session["CurrentAgencyUser"];
        Bindlist(txtFromdate.Text, txtTodate.Text, "months");
        TList<UserDetails> userdetail = DataRepository.UserDetailsProvider.GetByUserId(Convert.ToInt64(objUsers.UserId));
        lblBooking.Text = "Statistics Booking made by :" + objUsers.FirstName + " " + objUsers.LastName + " :" + userdetail[0].CompanyName;
        lblhead.Text = "Statistics Request made by :" + objUsers.FirstName + " " + objUsers.LastName + " :" + userdetail[0].CompanyName;
        lblReqHead.Text = "";
        lnkMonths.Attributes.Add("class", "btn-active");
        lnkDays.Attributes.Add("class", "btn");

    }
    #endregion



    #region BindList
    /// <summary>
    /// BindList
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Bindlist(string Fromdt, string Todt, string type)
    {
       // objViewstatistics.HotelId = (Session["CurrentAgencyUserID"].ToString());
        int  user = Convert.ToInt32(Session["CurrentAgencyUserID"].ToString());
       // string quertype = "1";// Convert.ToString(Request.QueryString["type"]);
        DateTime fromdate = new DateTime(Convert.ToInt32(Fromdt.Split('/')[2]), Convert.ToInt32(Fromdt.Split('/')[1]), Convert.ToInt32(Fromdt.Split('/')[0]));
        DateTime todate = new DateTime(Convert.ToInt32(Todt.Split('/')[2]), Convert.ToInt32(Todt.Split('/')[1]), Convert.ToInt32(Todt.Split('/')[0]));
         
        if (type.ToLower() == "days")
        {
            chrtBooking.ChartAreas[0].AxisX.Title = " Date of Booking";
            chrtBooking.ChartAreas[0].AxisY.Title = " Revenue Amount ";
            chrtReq.ChartAreas[0].AxisX.Title = " Date of Request";
            chrtReq.ChartAreas[0].AxisY.Title = " No of Request ";

                var datasourcedays = objViewstatistics.Getagencyuser(fromdate, todate, "1", user);
                dtlstConversion.DataSource = datasourcedays;
                dtlstConversion.DataBind();
                DtlReq.DataSource = datasourcedays;
                DtlReq.DataBind();
                DtlReqcon.DataSource = datasourcedays;
                DtlReqcon.DataBind();

                chrtBooking.Series["SeriesBooking"].Points.DataBind(datasourcedays, "currentdate", "conversion", string.Empty);
                chrtBooking.ChartAreas[0].AxisX.TextOrientation = TextOrientation.Horizontal;
                chrtBooking.ChartAreas[0].AxisY.TextOrientation = TextOrientation.Rotated90;
                chrtReq.Series["SeriesBooking"].Points.DataBind(datasourcedays, "currentdate", "Sensitive", string.Empty);
                chrtReq.ChartAreas[0].AxisX.TextOrientation = TextOrientation.Horizontal;
                chrtReq.ChartAreas[0].AxisY.TextOrientation = TextOrientation.Rotated90;
            
         }

        if (type.ToLower() == "months")
        {
            chrtBooking.ChartAreas[0].AxisX.Title = " Month of Booking";
            chrtBooking.ChartAreas[0].AxisY.Title = " Revenue Amount ";
           
            chrtReq.ChartAreas[0].AxisX.Title = " Month of Request";
            chrtReq.ChartAreas[0].AxisY.Title = " No of Request ";
            
            var datasourcedays = objViewstatistics.Getagencyuser(fromdate, todate, "2", user);
            dtlstConversion.DataSource = datasourcedays;
            dtlstConversion.DataBind();
            DtlReq.DataSource = datasourcedays;
            DtlReq.DataBind();
            DtlReqcon.DataSource = datasourcedays;
            DtlReqcon.DataBind();

            if (datasourcedays.Count == 1)
            {
                chrtBooking.Series["SeriesBooking"].ChartType = SeriesChartType.Point;
                chrtReq.Series["SeriesBooking"].ChartType = SeriesChartType.Point;
                chrtBooking.Series["SeriesBooking"].Points.DataBind(datasourcedays, "currentdate", "conversion", string.Empty);
                chrtBooking.ChartAreas[0].AxisX.TextOrientation = TextOrientation.Horizontal;
                chrtBooking.ChartAreas[0].AxisY.TextOrientation = TextOrientation.Rotated90;
                chrtReq.Series["SeriesBooking"].Points.DataBind(datasourcedays, "currentdate", "Sensitive", string.Empty);
                chrtReq.ChartAreas[0].AxisX.TextOrientation = TextOrientation.Horizontal;
                chrtReq.ChartAreas[0].AxisY.TextOrientation = TextOrientation.Rotated90;
            }
            else
            {
                chrtBooking.Series["SeriesBooking"].Points.DataBind(datasourcedays, "currentdate", "conversion", string.Empty);
                chrtBooking.ChartAreas[0].AxisX.TextOrientation = TextOrientation.Horizontal;
                chrtBooking.ChartAreas[0].AxisY.TextOrientation = TextOrientation.Rotated90;
                chrtReq.Series["SeriesBooking"].Points.DataBind(datasourcedays, "currentdate", "Sensitive", string.Empty);
                chrtReq.ChartAreas[0].AxisX.TextOrientation = TextOrientation.Horizontal;
                chrtReq.ChartAreas[0].AxisY.TextOrientation = TextOrientation.Rotated90;
            
            }
        }

       
    }
    #endregion
}