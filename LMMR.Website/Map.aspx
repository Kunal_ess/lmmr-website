﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeFile="Map.aspx.cs" Inherits="Map" %>

<%@ Register Src="UserControl/Frontend/BottomLink.ascx" TagName="BottomLink" TagPrefix="uc1" %>
<%@ Register Src="UserControl/Frontend/LinkMedia.ascx" TagName="LinkMedia" TagPrefix="uc2" %>
<%@ Register Src="~/UserControl/Frontend/RecentlyJoinedHotel.ascx" TagName="RecentlyJoinedHotel"
    TagPrefix="uc2" %>
<%@ Register Src="~/UserControl/Frontend/NewsSubscriber.ascx" TagName="NewsSubscriber"
    TagPrefix="uc3" %>
<%@ Register Src="UserControl/Frontend/HotelOfTheWeek.ascx" TagName="HotelOfTheWeek"
    TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntLeftSearch" runat="Server">
    <div class="left-inner-top-map">
        <div class="map">
            <a href="#">
                <img src="images/map.png" /></a>
        </div>
        <div class="logo">
            <a href="#">
                <img src="images/logo-front.png" /></a>
        </div>
        <!--left-form START HERE-->
        <div class="left-form">
            <div id="Country">
                <asp:DropDownList ID="drpCountry" runat="server" CssClass="bigselect" OnSelectedIndexChanged="drpCountry_SelectedIndexChanged"
                    AutoPostBack="True">
                </asp:DropDownList>
            </div>
            <div id="City">
                <asp:DropDownList ID="drpCity" CssClass="bigselect" runat="server" AutoPostBack="True"
                    OnSelectedIndexChanged="drpCity_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div id="location">
                <div id="locationtext">
                    <asp:HiddenField ID="hdnMapLatitude" runat="server" />
                    <asp:HiddenField ID="hdnMapLogitude" runat="server" />
                    <%= GetKeyResult("YOURLOCATIONCHANGE")%><br>
                    <%= GetKeyResult("LATITUDE")%> = <b><span id="selectLatitude"></span></b>
                    <br>
                    <%= GetKeyResult("LONGITUDE")%> = <b><span id="selectLogitude"></span></b>
                </div>
                <div class="locationsearch">
                    <img src="images/map-search.png" />
                </div>
            </div>
            <div id="radiusbody">
                <div class="radiustext">
                    <%= GetKeyResult("SEARCHINRADIUSOF")%>:</div>
                <div id="radius">
                    <asp:DropDownList ID="drpRadius" runat="server" OnSelectedIndexChanged="drpRadius_SelectedIndexChanged"
                        AutoPostBack="true">
                        <asp:ListItem Selected="True" Text="Select" Value="0"></asp:ListItem>
                        <asp:ListItem Text="0.5 km" Value="0.5"></asp:ListItem>
                        <asp:ListItem Text="1.0 km" Value="1.0"></asp:ListItem>
                        <asp:ListItem Text="1.5 km" Value="1.5"></asp:ListItem>
                        <asp:ListItem Text="2.0 km" Value="2.0"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div id="Date">
                <input name="AnotherDate" class="dateinput">
                <input type="button" value="select" class="datebutton" onclick="displayDatePicker('AnotherDate', this);">
            </div>
            <div id="Quantity-day">
                <div id="Quantity">
                    <label>
                        <%= GetKeyResult("DURATION")%> &nbsp;</label><input type="text" value="1" name="quantity[]" id="quantity74"
                            class="inputbox">
                    <input type="button" onclick="var qty_el = document.getElementById('quantity74'); var qty = qty_el.value; if( !isNaN( qty )) qty_el.value++;return false;"
                        class="button_up">
                    <input type="button" onclick="var qty_el = document.getElementById('quantity74'); var qty = qty_el.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) qty_el.value--;return false;"
                        class="button_down">
                </div>
                <div id="daysname">
                    <%= GetKeyResult("DAYSUSERCONTROL")%></div>
                <div id="Day">
                    <select name="test">
                        <option>Full Day</option>
                        <option value="Day1">Day1</option>
                        <option value="Day2">Day2</option>
                        <option value="Day3">Day3</option>
                    </select>
                </div>
            </div>
            <div id="Participants">
                <label>
                    <%= GetKeyResult("MEETINGDELEGATES")%> &nbsp;</label><input type="text" value="1" name="Participants[]" id="Participants74"
                        class="inputbox">
                <input type="button" onclick="var qty_el = document.getElementById('Participants74'); var qty = qty_el.value; if( !isNaN( qty )) qty_el.value++;return false;"
                    class="button_up">
                <input type="button" onclick="var qty_el = document.getElementById('Participants74'); var qty = qty_el.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) qty_el.value--;return false;"
                    class="button_down">
            </div>
            <input type="button" value="Find your meeting room !" class="find-button" />
        </div>
        <!--left-form ENDS HERE-->
    </div>
    </span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cntLeftBottom" runat="Server">
    <uc3:HotelOfTheWeek ID="HotelOfTheWeek1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cntTopNavigation" runat="Server">
    <div class="nav">
        <!--Main navigation start -->
        <div id="subnavbar">
            <ul id="subnav">
                <li class="cat-item"><a href="Default.aspx">
                    <%= GetKeyResult("HOME")%></a></li>
                <li class="cat-item"><a href="AboutUs.aspx">
                    <%= GetKeyResult("ABOUTUS")%></a></li>
                <li class="cat-item"><a href="ContactUs.aspx">
                    <%= GetKeyResult("CONTACTUS")%></a></li>
                <li class="cat-item"><a href="JoinToday.aspx">
                    <%= GetKeyResult("JOINTODAY")%></a></li>
                <li class="cat-item"><a href="Login.aspx">
                    <%= GetKeyResult("LOGIN")%></a></li>
            </ul>
        </div>
        <!--main navigation end -->
    </div>
    <div class="nav-form">
        <div id="Euro">
            <select name="test" class="smallselect">
                <option>Euro</option>
                <option value="Euro1">Euro1</option>
                <option value="Euro2">Euro2</option>
                <option value="Euro3">Euro3</option>
            </select>
        </div>
        <div id="English">
            <asp:DropDownList ID="DDLlanguage" CssClass="midselect" runat="server" AutoPostBack="True"
                OnSelectedIndexChanged="DDLlanguage_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cntMainBody" runat="Server">
    <div class="mainbody">
        <div class="mainbody-left">
            <!--banner START HERE-->
            <div class="banner">
                <div id="map_canvas" style="width: 460px; height: 300px; float: left;">
                </div>
            </div>
            <div class="video">
                <div class="videoleftbody">
                    <div class="videobody">
                        <img src="images/con-img1.png" />
                    </div>
                   <div class="video-button">
                    <a href="#" onclick="return Navigate();"><%= GetKeyResult("FORGUESTS")%></a>
                </div>
                </div>
                <div class="videorightbody">
                    <div class="videobody">
                        <img src="images/con-img2.png" />
                    </div>
                    <div class="video-button">
                    <a href="#" onclick="return Navigate();"><%= GetKeyResult("FORHOTELS")%></a>
                </div>
                </div>
            </div>
            <!--banner ENDS HERE-->
            <div class="mid">
                <div class="left">
                    <h2>
                        <%= GetKeyResult("HOWTOBOOKONLINE")%>?</h2>
                    <p>
                        Aenean porta, tellus vel accumsan fermentum, diam arcu condimentum purus, hendrerit
                        posuere purus mauris nec urna. Nam id elit dolor.
                    </p>
                    <p>
                        <a href="#"><%= GetKeyResult("READ")%></a></p>
                </div>
                <div class="right">
                    <h2>
                        <%= GetKeyResult("HOWTOSENDREQUEST")%>?</h2>
                    <p>
                        Vivamus et felis felis, nec mattis diam. Sed luctus mollis nisi, sed blandit urna
                        pulvinar ut. Integer male suada vesti bulum erat, at posuere mauris.
                    </p>
                    <p>
                        <a href="#"><%= GetKeyResult("READ")%></a></p>
                </div>
            </div>
            <!--bottom ENDS HERE-->
        </div>
        <div class="mainbody-right">
            <!--mainbody-right-call START HERE-->
            <div class="mainbody-right-call">
                <div class="need">
                    <span><%= GetKeyResult("NEED")%>?</span>
                    <br />
                    <%= GetKeyResult("CALLUS")%> 5/7
                </div>
                <div class="phone">
                    +32 2 344 25 50 </div>
             <p style="font-size:10px;color:White;text-align:center">  CALL US and WE DO IT FOR YOU</p>
                <div class="mail">
                    <a href="http://needameetingroom.com/requestform/" onClick="javascript: _gaq.push(['_trackPageview', '/email']); window.open(this.href, 'popupwindow1','width=900,height=600,scrollbars,resizable,toolbar=NO'); &#13;&#10;return false;" Target="_blank"">
                        Send us your RFP</a>
                </div>
            </div>
            <!--mainbody-right-call ENDS HERE-->
            <!--mainbody-right-join START HERE-->
            <div class="mainbody-right-join">
                <div class="join">
                    <a href="#"><%= GetKeyResult("JOINUSTODAY")%> !</a></div>
                (<%= GetKeyResult("FORHOTELSMEETINGFACILITIESEVENT")%>)
            </div>
            <!--mainbody-right-join ENDS HERE-->
            <!--mainbody-right-you START HERE-->
            <div class="mainbody-right-you">
                <img src="images/youtube.png" />
            </div>
            <!--mainbody-right-you ENDS HERE-->
            <!--mainbody-right-news START HERE-->
            <div class="mainbody-right-news">
                <uc3:NewsSubscriber ID="NewsSubscriber1" runat="server" />
                <!--mainbody-right-news ENDS HERE-->
                <!--mainbody-right-recently-joined START HERE-->
            </div>
            <uc2:RecentlyJoinedHotel ID="RecentlyJoinedHotel1" runat="server" />
            <!--mainbody-right-recently-joined ENDS HERE-->
        </div>
    </div>
    <script type="text/javascript">
        var geocoder;
        var map;
        var markersArray = [];
        var marker;
        var address;
        var addressByUser;
        var image;
        var locations;
        var infowindow = new google.maps.InfoWindow();
        var locations = <%= HotelData %>;

        
        function OnDemand(CenterPoint, PlaceName) {
            var cent = CenterPoint.split(',');
            map = new google.maps.Map(document.getElementById('map_canvas'), {
            zoom: 6,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
            map.setCenter(new google.maps.LatLng(cent[0], cent[1]));
            image = new google.maps.MarkerImage('http://maps.google.com/mapfiles/ms/icons/blue-dot.png');
            marker = new google.maps.Marker({
                map: map,
                title: PlaceName,
                position: new google.maps.LatLng(cent[0], cent[1]),
                icon: image
            });
            infowindow.setContent(PlaceName);
            infowindow.open(map, marker);

            google.maps.event.addListener(marker, 'mouseover', function () {
                infowindow.setContent(PlaceName);
                infowindow.open(map, this);
            });
            jQuery('#selectLatitude').html(cent[0]);
            jQuery('#selectLogitude').html(cent[1]);
            ShowHotelOfCity();
            var radiusSelected= jQuery('#<%=drpRadius.ClientID %>').val();
                       if(radiusSelected!=0 && radiusSelected!= null)
                       {
                            circleArray = new google.maps.Circle({
                            map: map,
                            radius: 2*1609,    // 1 miles in metres
                            fillColor: '#AA0000'
                        });
                        circleArray.bindTo('center', marker, 'position');
                    }
        }

        //This function used for show all hotel of the city.
        function ShowHotelOfCity() {
            clearOverlays();
            for (i = 0; i < locations.length; i++) {
                if (locations[i][4] == 'Special') {
                    image = new google.maps.MarkerImage('http://maps.google.com/mapfiles/ms/icons/green-dot.png');
                }
                else {
                    image = new google.maps.MarkerImage('http://maps.google.com/mapfiles/ms/icons/red-dot.png');
                }
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: image
                });
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {
                        //infowindow.setContent(locations[i][0]);
                        infowindow.setContent('<div style="text-align:right; font-size:8pt; color:black;" id="some_id_here">'+ locations[i][0] + '</br>click here<a href="http://www.google.com/"></a> </div>');
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
            google.maps.event.addListener(map, 'click', function (event) {
                AddPoint(event.latLng);
            });
        }

        function DefaultMap() {
        map = new google.maps.Map(document.getElementById('map_canvas'), {
            zoom: 3,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        address="europe";
        geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    clearOverlays();
                    map.setCenter(results[0].geometry.location);
                    marker = new google.maps.Marker({
                        map: map,
                        title: results[0]['formatted_address'],
                        position: results[0].geometry.location
                    });
                     
                     var latlog = results[0].geometry.location;
                     if(latlog!=null)
                     {
                     var splitLogLat = latlog.toString().split(',');
                     jQuery('#selectLatitude').html(splitLogLat[0].replace("(",""));
                     jQuery('#selectLogitude').html(splitLogLat[1].replace(")",""));
                     }
                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);
                    markersArray.push(marker);
                    var radiusSelected= jQuery('#<%=drpRadius.ClientID %>').val();
                       if(radiusSelected!=0 && radiusSelected!= null)
                       {
                            circleArray = new google.maps.Circle({
                            map: map,
                            radius: 2*1609,    // 1 miles in metres
                            fillColor: '#AA0000'
                        });
                        circleArray.bindTo('center', marker, 'position');
                    }

                } else {
                    alert('<%= GetKeyResult("GEOCODENTSUCCESS")%>:' + status);
                }
            });
        }

        //Add referace point on click on map.
        function AddPoint(location) {
            geocoder = new google.maps.Geocoder();
            image = new google.maps.MarkerImage('http://maps.google.com/mapfiles/ms/icons/blue-dot.png');
            geocoder.geocode({ 'latLng': location }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        clearOverlays();
                        
                     var latlog = location;
                     if(latlog!=null)
                     {
                     var splitLogLat = latlog.toString().split(',');
                     jQuery('#selectLatitude').html(splitLogLat[0].replace("(",""));
                     jQuery('#selectLogitude').html(splitLogLat[1].replace(")",""));
                     jQuery('#<%=hdnMapLatitude.ClientID %>').val(splitLogLat[0].replace("(",""));
                     jQuery('#<%=hdnMapLogitude.ClientID %>').val(splitLogLat[1].replace(")",""));
                     }
                        marker = new google.maps.Marker({
                            position: location,
                            title: results[1].formatted_address,
                            map: map,
                            icon: image
                        });
                        infowindow.setContent(results[1].formatted_address);
                        infowindow.open(map, marker);
                        markersArray.push(marker);
                        map.setCenter(location);
                        //click
                        google.maps.event.addListener(marker, 'mouseover', function () {
                            infowindow.setContent(results[1].formatted_address);
                            infowindow.open(map, this);
                        });

                   var radiusSelected= jQuery('#<%=drpRadius.ClientID %>').val();
                   alert(jQuery('#<%=drpRadius.ClientID %>').val());
                      if(radiusSelected!=0 && radiusSelected!= null)
                       {
                            circleArray = new google.maps.Circle({
                            map: map,
                            radius: 2*1609,    // 1 miles in metres
                            fillColor: '#AA0000'
                        });
                        circleArray.bindTo('center', marker, 'position');
                    }

                    }
                } else {
                    alert('<%= GetKeyResult("GEOCODEFAIL")%>: ' + status);
                }
            });

        }

        function clearOverlays() {
            if (markersArray) {
                for (i = 0; i < markersArray.length; i++) {
                    markersArray[i].setMap(null);
                }
                markersArray.length = 0;
            }

        }

        function toggleBounce() {

            if (marker.getAnimation() != null) {
                marker.setAnimation(null);
            } else {
                marker.setAnimation(google.maps.Animation.BOUNCE);
            }
        }
        
    </script>
             
   
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cntMediaLink" runat="Server">
    <uc2:LinkMedia ID="LinkMedia1" runat="server" />
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cntFotter" runat="Server">
    <uc1:BottomLink ID="BottomLink1" runat="server" />

   
</asp:Content>
