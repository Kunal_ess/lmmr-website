﻿
#region Included Namespaces

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Data;
using LMMR.Entities;
using System.Text;
using System.IO;
using System.Configuration;
using System.Web.Services;
using log4net;
using log4net.Config;
using System.Text.RegularExpressions;
using log4net;
using log4net.Config;

#endregion

public partial class SuperAdmin_StaticPagesContents : System.Web.UI.Page
{

    #region Variable Declaration

    SuperAdminTaskManager manager = new SuperAdminTaskManager();
    public string heading;
    public string task;
    public string ContentTile = string.Empty;
    ManageCMSContent objManageCMSContent = new ManageCMSContent();
    NewUser obj = new NewUser();
    ILog logger = log4net.LogManager.GetLogger(typeof(SuperAdmin_StaticPagesContents));
    public CmsDescription cmd
    {
        set
        {
            ViewState["Cmd"] = value;
        }
        get
        {
            return (ViewState["Cmd"] == null ? null : (CmsDescription)ViewState["Cmd"]);
        }
    }

    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            if (string.IsNullOrEmpty(appRootUrl) || appRootUrl == "/")
            {
                return host + "/";
            }
            else
            {
                return host + appRootUrl + "/";
            }
        }
    }
    #endregion

    #region PageLoad

    /// <summary>
    /// The PageLoad Method.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void Page_Load(object sender, EventArgs e)
    {
        Session["CMS"] = null;
        Session["CMSDESCRIPTION"] = null;
        if (Session["CurrentSuperAdmin"] == null)
        {
            Response.Redirect("~/Default.aspx", false);
            return;
        }
        divmessage.Style.Add("display", "none");
        divmessage.Attributes.Add("class", "error");
        heading = hdnHeading.Value;
        calExTO.StartDate = DateTime.Now.Date;
        CalendarExtender2.StartDate = DateTime.Now.Date;
        CalendarExtender1.EndDate = DateTime.Now.Date;
        if (!IsPostBack)
        {
            hdnLanguageID.Value = "1"; // initializing language to English.
        }
        else
        {
            pagingDIV.Visible = true;
            if (ViewState["TypeGrd"] != null)
            {
                ApplyPaging(Convert.ToString(ViewState["TypeGrd"]));
            }
        }
    }

    #endregion

    #region Methods

    /// <summary>
    /// This method binds the socialSitesDDL DropDownList and txtPageUrl TextBox.
    /// </summary>

    public void bindSocialSitesDDL(string selectedValue)
    {
        if (selectedValue == "0")
        {
            SocialSitesDDL.DataSource = manager.getCmsDescriptionOnCmsID(Convert.ToInt64(hdnCmsId.Value));
            SocialSitesDDL.DataTextField = "ContentShortDesc";
            SocialSitesDDL.DataValueField = "Id";
            SocialSitesDDL.DataBind();
            CmsDescription entity = manager.getCmsDescriptionOnID(Convert.ToInt64(SocialSitesDDL.SelectedItem.Value));
            if (entity != null)
            {
                txtPageUrl.Text = entity.PageUrl;
                hdnContentTitle.Value = entity.ContentTitle;
                hdnRecordID.Value = entity.CmsId.ToString();
                string path = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Replace("~/", "") + "SocialImage/" + entity.ContentImage;
                imgIcon.ImageUrl = path;
            }
        }
        else
        {
            SocialSitesDDL.DataSource = manager.getCmsDescriptionOnCmsID(Convert.ToInt64(hdnCmsId.Value));
            SocialSitesDDL.DataTextField = "ContentShortDesc";
            SocialSitesDDL.DataValueField = "Id";
            SocialSitesDDL.DataBind();
            SocialSitesDDL.SelectedValue = selectedValue;
            CmsDescription entity = manager.getCmsDescriptionOnID(Convert.ToInt64(selectedValue));
            if (entity != null)
            {
                txtPageUrl.Text = entity.PageUrl;
                hdnContentTitle.Value = entity.ContentTitle;
                hdnRecordID.Value = entity.CmsId.ToString();
                string path = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Replace("~/", "") + "SocialImage/" + entity.ContentImage;
                imgIcon.ImageUrl = path;
            }
        }
    }

    /// <summary>
    /// This method returns a ListItem from the DropDownList on the basis of case insensitive text matching.
    /// </summary>
    /// <param name="ddl"></param>
    /// <param name="value"></param>
    /// <returns></returns>

    private ListItem FindByTextCaseInsensitive(DropDownList ddl, string value)
    {
        foreach (ListItem item in ddl.Items)
        {

            if (string.Compare(value, item.Text, true) == 0)
            {
                return item;
            }

        }
        return null;
    }

    #region Apply Paging

    /// <summary>
    /// Method to apply Paging in grid
    /// </summary>

    private void ApplyPaging(string TypeGrd)
    {
        try
        {
            GridViewRow row = null;
            switch (TypeGrd)
            {
                case "HotelOfTheWeek":
                    pagingDIV.Visible = true;
                    row = grvHotelsOfTheWeek.TopPagerRow;
                    #region Paging
                    if (row != null)
                    {
                        PlaceHolder ph;
                        LinkButton lnkPaging;
                        LinkButton lnkPrevPage;
                        LinkButton lnkNextPage;
                        lnkPrevPage = new LinkButton();
                        lnkPrevPage.CssClass = "pre";
                        lnkPrevPage.Width = Unit.Pixel(73);
                        lnkPrevPage.CommandName = "Page";
                        lnkPrevPage.CommandArgument = "prev";
                        ph = (PlaceHolder)row.FindControl("ph");
                        ph.Controls.Add(lnkPrevPage);
                        if (grvHotelsOfTheWeek.PageIndex == 0)
                        {
                            lnkPrevPage.Enabled = false;

                        }
                        for (int i = 1; i <= grvHotelsOfTheWeek.PageCount; i++)
                        {
                            lnkPaging = new LinkButton();
                            if (ViewState["CurrentPage"] != null)
                            {
                                if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                                {
                                    lnkPaging.CssClass = "pag2";
                                }
                                else
                                {
                                    lnkPaging.CssClass = "pag";
                                }
                            }
                            else
                            {
                                if (i == 1)
                                {
                                    lnkPaging.CssClass = "pag2";
                                }
                                else
                                {
                                    lnkPaging.CssClass = "pag";
                                }
                            }
                            lnkPaging.Width = Unit.Pixel(16);
                            lnkPaging.Text = i.ToString();
                            lnkPaging.CommandName = "Page";
                            lnkPaging.CommandArgument = i.ToString();
                            if (i == grvHotelsOfTheWeek.PageIndex + 1)
                                ph = (PlaceHolder)row.FindControl("ph");
                            ph.Controls.Add(lnkPaging);
                        }
                        lnkNextPage = new LinkButton();
                        lnkNextPage.CssClass = "nex";
                        lnkNextPage.Width = Unit.Pixel(42);
                        lnkNextPage.CommandName = "Page";
                        lnkNextPage.CommandArgument = "next";
                        ph = (PlaceHolder)row.FindControl("ph");
                        ph.Controls.Add(lnkNextPage);
                        ph = (PlaceHolder)row.FindControl("ph");
                        if (grvHotelsOfTheWeek.PageIndex == grvHotelsOfTheWeek.PageCount - 1)
                        {
                            lnkNextPage.Enabled = false;
                        }
                    }
                    #endregion
                    break;
                case "Banner":
                    pagingDIV.Visible = true;
                    row = grvBanner.TopPagerRow;
                    #region Paging
                    if (row != null)
                    {
                        PlaceHolder ph;
                        LinkButton lnkPaging;
                        LinkButton lnkPrevPage;
                        LinkButton lnkNextPage;
                        lnkPrevPage = new LinkButton();
                        lnkPrevPage.CssClass = "pre";
                        lnkPrevPage.Width = Unit.Pixel(73);
                        lnkPrevPage.CommandName = "Page";
                        lnkPrevPage.CommandArgument = "prev";
                        ph = (PlaceHolder)row.FindControl("ph");
                        ph.Controls.Add(lnkPrevPage);
                        if (grvBanner.PageIndex == 0)
                        {
                            lnkPrevPage.Enabled = false;

                        }
                        for (int i = 1; i <= grvBanner.PageCount; i++)
                        {
                            lnkPaging = new LinkButton();
                            if (ViewState["CurrentPage"] != null)
                            {
                                if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                                {
                                    lnkPaging.CssClass = "pag2";
                                }
                                else
                                {
                                    lnkPaging.CssClass = "pag";
                                }
                            }
                            else
                            {
                                if (i == 1)
                                {
                                    lnkPaging.CssClass = "pag2";
                                }
                                else
                                {
                                    lnkPaging.CssClass = "pag";
                                }
                            }
                            lnkPaging.Width = Unit.Pixel(16);
                            lnkPaging.Text = i.ToString();
                            lnkPaging.CommandName = "Page";
                            lnkPaging.CommandArgument = i.ToString();
                            if (i == grvBanner.PageIndex + 1)
                                ph = (PlaceHolder)row.FindControl("ph");
                            ph.Controls.Add(lnkPaging);
                        }
                        lnkNextPage = new LinkButton();
                        lnkNextPage.CssClass = "nex";
                        lnkNextPage.Width = Unit.Pixel(42);
                        lnkNextPage.CommandName = "Page";
                        lnkNextPage.CommandArgument = "next";
                        ph = (PlaceHolder)row.FindControl("ph");
                        ph.Controls.Add(lnkNextPage);
                        ph = (PlaceHolder)row.FindControl("ph");
                        if (grvBanner.PageIndex == grvBanner.PageCount - 1)
                        {
                            lnkNextPage.Enabled = false;
                        }
                    }
                    #endregion
                    break;
                case "BottomLink":
                    pagingDIV.Visible = true;
                    row = grvBottomLinks.TopPagerRow;
                    #region Paging
                    if (row != null)
                    {
                        PlaceHolder ph;
                        LinkButton lnkPaging;
                        LinkButton lnkPrevPage;
                        LinkButton lnkNextPage;
                        lnkPrevPage = new LinkButton();
                        lnkPrevPage.CssClass = "pre";
                        lnkPrevPage.Width = Unit.Pixel(73);
                        lnkPrevPage.CommandName = "Page";
                        lnkPrevPage.CommandArgument = "prev";
                        ph = (PlaceHolder)row.FindControl("ph");
                        ph.Controls.Add(lnkPrevPage);
                        if (grvBottomLinks.PageIndex == 0)
                        {
                            lnkPrevPage.Enabled = false;

                        }
                        for (int i = 1; i <= grvBottomLinks.PageCount; i++)
                        {
                            lnkPaging = new LinkButton();
                            if (ViewState["CurrentPage"] != null)
                            {
                                if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                                {
                                    lnkPaging.CssClass = "pag2";
                                }
                                else
                                {
                                    lnkPaging.CssClass = "pag";
                                }
                            }
                            else
                            {
                                if (i == 1)
                                {
                                    lnkPaging.CssClass = "pag2";
                                }
                                else
                                {
                                    lnkPaging.CssClass = "pag";
                                }
                            }
                            lnkPaging.Width = Unit.Pixel(16);
                            lnkPaging.Text = i.ToString();
                            lnkPaging.CommandName = "Page";
                            lnkPaging.CommandArgument = i.ToString();
                            if (i == grvBottomLinks.PageIndex + 1)
                                ph = (PlaceHolder)row.FindControl("ph");
                            ph.Controls.Add(lnkPaging);
                        }
                        lnkNextPage = new LinkButton();
                        lnkNextPage.CssClass = "nex";
                        lnkNextPage.Width = Unit.Pixel(42);
                        lnkNextPage.CommandName = "Page";
                        lnkNextPage.CommandArgument = "next";
                        ph = (PlaceHolder)row.FindControl("ph");
                        ph.Controls.Add(lnkNextPage);
                        ph = (PlaceHolder)row.FindControl("ph");
                        if (grvBottomLinks.PageIndex == grvBottomLinks.PageCount - 1)
                        {
                            lnkNextPage.Enabled = false;
                        }
                    }
                    #endregion
                    break;
                case "ImageGallery":
                    pagingDIV.Visible = true;
                    row = grvImageGallery.TopPagerRow;
                    #region Paging
                    if (row != null)
                    {
                        PlaceHolder ph;
                        LinkButton lnkPaging;
                        LinkButton lnkPrevPage;
                        LinkButton lnkNextPage;
                        lnkPrevPage = new LinkButton();
                        lnkPrevPage.CssClass = "pre";
                        lnkPrevPage.Width = Unit.Pixel(73);
                        lnkPrevPage.CommandName = "Page";
                        lnkPrevPage.CommandArgument = "prev";
                        ph = (PlaceHolder)row.FindControl("ph");
                        ph.Controls.Add(lnkPrevPage);
                        if (grvImageGallery.PageIndex == 0)
                        {
                            lnkPrevPage.Enabled = false;

                        }
                        for (int i = 1; i <= grvImageGallery.PageCount; i++)
                        {
                            lnkPaging = new LinkButton();
                            if (ViewState["CurrentPage"] != null)
                            {
                                if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                                {
                                    lnkPaging.CssClass = "pag2";
                                }
                                else
                                {
                                    lnkPaging.CssClass = "pag";
                                }
                            }
                            else
                            {
                                if (i == 1)
                                {
                                    lnkPaging.CssClass = "pag2";
                                }
                                else
                                {
                                    lnkPaging.CssClass = "pag";
                                }
                            }
                            lnkPaging.Width = Unit.Pixel(16);
                            lnkPaging.Text = i.ToString();
                            lnkPaging.CommandName = "Page";
                            lnkPaging.CommandArgument = i.ToString();
                            if (i == grvImageGallery.PageIndex + 1)
                                ph = (PlaceHolder)row.FindControl("ph");
                            ph.Controls.Add(lnkPaging);
                        }
                        lnkNextPage = new LinkButton();
                        lnkNextPage.CssClass = "nex";
                        lnkNextPage.Width = Unit.Pixel(42);
                        lnkNextPage.CommandName = "Page";
                        lnkNextPage.CommandArgument = "next";
                        ph = (PlaceHolder)row.FindControl("ph");
                        ph.Controls.Add(lnkNextPage);
                        ph = (PlaceHolder)row.FindControl("ph");
                        if (grvImageGallery.PageIndex == grvImageGallery.PageCount - 1)
                        {
                            lnkNextPage.Enabled = false;
                        }
                    }
                    #endregion
                    break;
                case "News":
                    pagingDIV.Visible = true;
                    row = grvNews.TopPagerRow;
                    #region Paging
                    if (row != null)
                    {
                        PlaceHolder ph;
                        LinkButton lnkPaging;
                        LinkButton lnkPrevPage;
                        LinkButton lnkNextPage;
                        lnkPrevPage = new LinkButton();
                        lnkPrevPage.CssClass = "pre";
                        lnkPrevPage.Width = Unit.Pixel(73);
                        lnkPrevPage.CommandName = "Page";
                        lnkPrevPage.CommandArgument = "prev";
                        ph = (PlaceHolder)row.FindControl("ph");
                        ph.Controls.Add(lnkPrevPage);
                        if (grvNews.PageIndex == 0)
                        {
                            lnkPrevPage.Enabled = false;

                        }
                        for (int i = 1; i <= grvNews.PageCount; i++)
                        {
                            lnkPaging = new LinkButton();
                            if (ViewState["CurrentPage"] != null)
                            {
                                if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                                {
                                    lnkPaging.CssClass = "pag2";
                                }
                                else
                                {
                                    lnkPaging.CssClass = "pag";
                                }
                            }
                            else
                            {
                                if (i == 1)
                                {
                                    lnkPaging.CssClass = "pag2";
                                }
                                else
                                {
                                    lnkPaging.CssClass = "pag";
                                }
                            }
                            lnkPaging.Width = Unit.Pixel(16);
                            lnkPaging.Text = i.ToString();
                            lnkPaging.CommandName = "Page";
                            lnkPaging.CommandArgument = i.ToString();
                            if (i == grvNews.PageIndex + 1)
                                ph = (PlaceHolder)row.FindControl("ph");
                            ph.Controls.Add(lnkPaging);
                        }
                        lnkNextPage = new LinkButton();
                        lnkNextPage.CssClass = "nex";
                        lnkNextPage.Width = Unit.Pixel(42);
                        lnkNextPage.CommandName = "Page";
                        lnkNextPage.CommandArgument = "next";
                        ph = (PlaceHolder)row.FindControl("ph");
                        ph.Controls.Add(lnkNextPage);
                        ph = (PlaceHolder)row.FindControl("ph");
                        if (grvNews.PageIndex == grvNews.PageCount - 1)
                        {
                            lnkNextPage.Enabled = false;
                        }
                    }
                    #endregion
                    break;
                case "Benifites":
                    pagingDIV.Visible = true;
                    row = grvBenefits.TopPagerRow;
                    #region Paging
                    if (row != null)
                    {
                        PlaceHolder ph;
                        LinkButton lnkPaging;
                        LinkButton lnkPrevPage;
                        LinkButton lnkNextPage;
                        lnkPrevPage = new LinkButton();
                        lnkPrevPage.CssClass = "pre";
                        lnkPrevPage.Width = Unit.Pixel(73);
                        lnkPrevPage.CommandName = "Page";
                        lnkPrevPage.CommandArgument = "prev";
                        ph = (PlaceHolder)row.FindControl("ph");
                        ph.Controls.Add(lnkPrevPage);
                        if (grvBenefits.PageIndex == 0)
                        {
                            lnkPrevPage.Enabled = false;

                        }
                        for (int i = 1; i <= grvBenefits.PageCount; i++)
                        {
                            lnkPaging = new LinkButton();
                            if (ViewState["CurrentPage"] != null)
                            {
                                if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                                {
                                    lnkPaging.CssClass = "pag2";
                                }
                                else
                                {
                                    lnkPaging.CssClass = "pag";
                                }
                            }
                            else
                            {
                                if (i == 1)
                                {
                                    lnkPaging.CssClass = "pag2";
                                }
                                else
                                {
                                    lnkPaging.CssClass = "pag";
                                }
                            }
                            lnkPaging.Width = Unit.Pixel(16);
                            lnkPaging.Text = i.ToString();
                            lnkPaging.CommandName = "Page";
                            lnkPaging.CommandArgument = i.ToString();
                            if (i == grvBenefits.PageIndex + 1)
                                ph = (PlaceHolder)row.FindControl("ph");
                            ph.Controls.Add(lnkPaging);
                        }
                        lnkNextPage = new LinkButton();
                        lnkNextPage.CssClass = "nex";
                        lnkNextPage.Width = Unit.Pixel(42);
                        lnkNextPage.CommandName = "Page";
                        lnkNextPage.CommandArgument = "next";
                        ph = (PlaceHolder)row.FindControl("ph");
                        ph.Controls.Add(lnkNextPage);
                        ph = (PlaceHolder)row.FindControl("ph");
                        if (grvBenefits.PageIndex == grvBenefits.PageCount - 1)
                        {
                            lnkNextPage.Enabled = false;
                        }
                    }
                    #endregion
                    break;
                default:
                    //row = grvHotelsOfTheWeek.TopPagerRow;
                    break;
            }

            //GridViewRow row = grv.TopPagerRow;
            
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    #endregion

    #region PageIndexChanging

    /// <summary>
    /// Page Index change to bind the grvHotelsOfTheWeek grid.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void grvHotelsOfTheWeek_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            grvHotelsOfTheWeek.PageIndex = e.NewPageIndex;
            ViewState["CurrentPage"] = e.NewPageIndex;
            ViewState["TypeGrd"] = "HotelOfTheWeek";
            FilterGridWithHeader("HotelOfTheWeek", Convert.ToString(ViewState["Where"]), Convert.ToString(ViewState["Order"]), Convert.ToString(ViewState["ClientName"]));

        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    /// <summary>
    /// Page Index change to bind the grvBenefits grid.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void grvBenefits_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            grvBenefits.PageIndex = e.NewPageIndex;
            ViewState["CurrentPage"] = e.NewPageIndex;
            ViewState["TypeGrd"] = "Benifites";
            FilterGridWithHeader("Benifites", Convert.ToString(ViewState["Where"]), Convert.ToString(ViewState["Order"]), Convert.ToString(ViewState["ClientName"]));

        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    /// <summary>
    /// Page Index change to bind the grvNews grid.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void grvNews_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            grvNews.PageIndex = e.NewPageIndex;
            ViewState["CurrentPage"] = e.NewPageIndex;
            ViewState["TypeGrd"] = "News";
            FilterGridWithHeader("News", Convert.ToString(ViewState["Where"]), Convert.ToString(ViewState["Order"]), Convert.ToString(ViewState["ClientName"]));

        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    /// <summary>
    /// Page Index change to bind the grvImageGallery grid.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void grvImageGallery_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            grvImageGallery.PageIndex = e.NewPageIndex;
            ViewState["CurrentPage"] = e.NewPageIndex;
            ViewState["TypeGrd"] = "ImageGallery";
            FilterGridWithHeader("ImageGallery", Convert.ToString(ViewState["Where"]), Convert.ToString(ViewState["Order"]), Convert.ToString(ViewState["ClientName"]));

        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    /// <summary>
    /// Page Index change to bind the gridBottomLinks grid.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void grvBottomLinks_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            grvBottomLinks.PageIndex = e.NewPageIndex;
            ViewState["CurrentPage"] = e.NewPageIndex;
            ViewState["TypeGrd"] = "BottomLink";
            FilterGridWithHeader("BottomLink", Convert.ToString(ViewState["Where"]), Convert.ToString(ViewState["Order"]), Convert.ToString(ViewState["ClientName"]));

        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    /// <summary>
    /// Page Index change to bind the grvBanner grid.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void grvBanner_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            grvBanner.PageIndex = e.NewPageIndex;
            ViewState["CurrentPage"] = e.NewPageIndex;
            ViewState["TypeGrd"] = "Banner";
            FilterGridWithHeader("Banner", Convert.ToString(ViewState["Where"]), Convert.ToString(ViewState["Order"]), Convert.ToString(ViewState["ClientName"]));

        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    #endregion

    /// <summary>
    /// Bind grid with filter condition
    /// </summary>
    /// <param name="wherec">This is the string value which contains value of Viewstate Where condition</param>
    /// <param name="order">This is the string value which contains value of Viewstate Order condition</param>

    public void FilterGridWithHeader(string TypeGrd, string wherec, string order, string clientNameStarts)
    {
        try
        {
            TList<CmsDescription> list = manager.getCmsDescriptionAll(Convert.ToInt64(hdnCmsId.Value),1);
            switch (TypeGrd)
            {
                case "HotelOfTheWeek":
                    grvHotelsOfTheWeek.PageIndex = Convert.ToInt32(ViewState["CurrentPage"] == null ? "0" : ViewState["CurrentPage"]);
                    grvHotelsOfTheWeek.DataSource = list;
                    grvHotelsOfTheWeek.DataBind();
                    ApplyPaging(TypeGrd);
                    break;
                case "Banner":
                    grvBanner.PageIndex = Convert.ToInt32(ViewState["CurrentPage"] == null ? "0" : ViewState["CurrentPage"]);
                    grvBanner.DataSource = list;
                    grvBanner.DataBind();
                    ApplyPaging(TypeGrd);
                    break;
                case "BottomLink":
                    grvBottomLinks.PageIndex = Convert.ToInt32(ViewState["CurrentPage"] == null ? "0" : ViewState["CurrentPage"]);
                    grvBottomLinks.DataSource = list;
                    grvBottomLinks.DataBind();
                    ApplyPaging(TypeGrd);
                    break;
                case "ImageGallery":
                    grvImageGallery.PageIndex = Convert.ToInt32(ViewState["CurrentPage"] == null ? "0" : ViewState["CurrentPage"]);
                    grvImageGallery.DataSource = list;
                    grvImageGallery.DataBind();
                    ApplyPaging(TypeGrd);
                    break;
                case "News":
                    grvNews.PageIndex = Convert.ToInt32(ViewState["CurrentPage"] == null ? "0" : ViewState["CurrentPage"]);
                    grvNews.DataSource = list;
                    grvNews.DataBind();
                    ApplyPaging(TypeGrd);
                    break;
                case "Benifites":
                    grvBenefits.PageIndex = Convert.ToInt32(ViewState["CurrentPage"] == null ? "0" : ViewState["CurrentPage"]);
                    grvBenefits.DataSource = list;
                    grvBenefits.DataBind();
                    ApplyPaging(TypeGrd);
                    break;
                default :
                    break;
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    #endregion

    #region Events

    /// <summary>
    /// This is the event handler for lbtSave LinkButton click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    
    protected void lbtSave_Click(object sender, EventArgs e)
    {
        CmsDescription entity = new CmsDescription();
        bool status = false;
        bool IsFile = false;
        bool IsInfo = false;
        if (HttpUtility.HtmlDecode(hdnContentTitle.Value) == hdnContentTitle.Value)
        {
            hdnContentTitle.Value = HttpUtility.HtmlEncode(hdnContentTitle.Value);
        }
        entity = manager.getCmsDescriptionEntity(Convert.ToInt64(hdnCmsId.Value), Convert.ToInt64(hdnLanguageID.Value), hdnContentTitle.Value);

        if (hdnLanguageID.Value != "1")
        {
            CmsDescription englishEntity = manager.getCmsDescriptionEntity(Convert.ToInt64(hdnCmsId.Value), 1, hdnContentTitle.Value);
            if (englishEntity == null)
            {
                divmessage.Attributes.Add("class", "error");
                divmessage.Style.Add("display", "block");
                divmessage.InnerHtml = "Please provide data for english language first.";
                return;
            }
        }

        #region When Updating (if part)

        if (entity != null)
        {
            entity.CmsId = Convert.ToInt64(hdnCmsId.Value);
            entity.LanguageId = Convert.ToInt64(hdnLanguageID.Value);
            if (editor.Visible == true)
            {
                IsInfo = true;
                entity.ContentsDesc = ckeEditorEN.Text;
                entity.ContentTitle = hdnContentTitle.Value;
                if (txtPublishedDate.Text != "")
                {
                    entity.UpdatedDate = DateTime.ParseExact(txtPublishedDate.Text, "dd/MM/yyyy", null);
                }
                else
                {
                    entity.UpdatedDate = DateTime.Now;
                }
                //entity.UpdatedDate = DateTime.Now;
            }
            if (socialNetworkingDiv.Visible == true)
            {
                IsInfo = true;
                entity.PageUrl = txtPageUrl.Text;
                entity.ContentTitle = hdnContentTitle.Value;
                entity.UpdatedDate = DateTime.Now;
            }

            #region Uploading PDf files
            if (pdfUpload.Visible == true)
            {

                // pdf upload logic here

                if (pdfUpload.HasFile)
                {
                    string strPlanName = "";
                    IsFile = true;
                    //if (pdfUpload.PostedFile.ContentLength > 1048576)
                    //{
                    //    divmessage.InnerHtml = "Filesize of plan is too large. Maximum file size permitted is 1 MB.";
                    //    divmessage.Attributes.Add("class", "error");
                    //    divmessage.Style.Add("display", "block");
                    //    return;
                    //}
                    //else
                    //{
                        Guid objId = Guid.NewGuid();
                        strPlanName = Path.GetFileName(pdfUpload.FileName);
                        strPlanName = objId + strPlanName;
                        if (gridNews.Visible == true)
                        {
                            pdfUpload.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"] + "NewsFile/") + strPlanName);
                            if (txtPublishedDate.Text != "")
                            {
                                entity.UpdatedDate = DateTime.ParseExact(txtPublishedDate.Text, "dd/MM/yyyy", null);
                            }
                            else
                            {
                                entity.UpdatedDate = DateTime.ParseExact(txtPublishedDate.Text, "dd/MM/yyyy", null);
                            }
                        }
                        else
                        {
                            pdfUpload.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"] + "MediaFile/") + strPlanName);
                            entity.UpdatedDate = DateTime.Now;
                        }
                        entity.PageUrl = strPlanName;

                    //}
                }
                entity.ContentTitle = hdnContentTitle.Value;
            }
            #endregion

            #region #region Uploading images
            if (imageUpload.Visible == true)
            {
                // image upload logic here
                string strLogo = "";
                IsFile = true;
                if (imageUpload.HasFile)
                {
                    if ((gridHotelsOfTheWeek.Visible == true || gridBenefits.Visible == true) && imageUpload.PostedFile.ContentLength > (Convert.ToInt32(ConfigurationManager.AppSettings["LimitKB2"])))
                    {
                        divmessage.InnerHtml = "Filesize of image is too large. Maximum file size permitted is " + ConfigurationManager.AppSettings["MessageKB2"] + ".";
                        divmessage.Attributes.Add("class", "error");
                        divmessage.Style.Add("display", "block");
                        return;
                    }
                    else if (gridImageGallery.Visible == true && imageUpload.PostedFile.ContentLength > (Convert.ToInt32(ConfigurationManager.AppSettings["LimitKB3"])))
                    {
                        divmessage.InnerHtml = "Filesize of image is too large. Maximum file size permitted is " + ConfigurationManager.AppSettings["MessageKB3"] + ".";
                        divmessage.Attributes.Add("class", "error");
                        divmessage.Style.Add("display", "block");
                        return;
                    }
                    else if (hotelBanner.Visible == true && imageUpload.PostedFile.ContentLength > (Convert.ToInt32(ConfigurationManager.AppSettings["LimitKB4"])))
                    {
                        divmessage.InnerHtml = "Filesize of image is too large. Maximum file size permitted is " + ConfigurationManager.AppSettings["MessageKB4"] + ".";
                        divmessage.Attributes.Add("class", "error");
                        divmessage.Style.Add("display", "block");
                        return;
                    }
                    else if (socialNetworkingDiv.Visible == true && imageUpload.PostedFile.ContentLength > (Convert.ToInt32(ConfigurationManager.AppSettings["LimitKB1"])))
                    {
                        divmessage.InnerHtml = "Filesize of image is too large. Maximum file size permitted is " + ConfigurationManager.AppSettings["MessageKB1"] + ".";
                        divmessage.Attributes.Add("class", "error");
                        divmessage.Style.Add("display", "block");
                        return;
                    }
                    else
                    {
                        Guid objID = Guid.NewGuid();
                        strLogo = Path.GetFileName(imageUpload.FileName).Replace(" ", "").Replace("%20", "").Trim();
                        strLogo = System.Text.RegularExpressions.Regex.Replace(strLogo, @"[^a-zA-Z 0-9'.@]", string.Empty).Trim();
                        strLogo = objID + strLogo;
                        if (descriptionTextboxDIV.Visible == true)
                        {
                            imageUpload.SaveAs(Server.MapPath("~/Images/") + strLogo);

                        }
                        else if (gridImageGallery.Visible == true)
                        {
                            imageUpload.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"] + "FrontImages/") + strLogo);

                        }
                        else if (socialNetworkingDiv.Visible == true)
                        {
                            imageUpload.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"] + "SocialImage/") + strLogo);

                        }
                        else if (hotelBanner.Visible == true)
                        {
                            imageUpload.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"] + "FrontImages/") + strLogo);
                        }
                        else
                        {
                            imageUpload.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"] + "HotelOfTheWeek/") + strLogo);

                        }
                        entity.ContentImage = strLogo;
                    }
                }
                if (!socialNetworkingDiv.Visible)
                {
                    entity.PageUrl = txturl.Text;
                    entity.UpdatedDate = DateTime.Now;
                    entity.ContentTitle = hdnContentTitle.Value;
                }
            }
            #endregion

            if (descriptionTextboxDIV.Visible == true)
            {
                IsInfo = true;
                entity.ContentShortDesc = txtDescription.Text;
                entity.ContentTitle = hdnContentTitle.Value;
                entity.UpdatedDate = DateTime.Now;
            }
            if (titleDiv.Visible == true)
            {
                IsInfo = true;
                entity.ContentTitle = System.Net.WebUtility.HtmlEncode(txtTitle.Text);
                if (Regex.Split(hdnContentTitle.Value, ",").Count() == 2)       // condition introduced to avoid exception in case of old entities without GUID,
                {                                                               // in case of new entities this will always evalute to true.
                    entity.ContentTitle += "," + Regex.Split(hdnContentTitle.Value, ",")[1];
                }
                entity.UpdatedDate = DateTime.Now;
                if (txtFrom.Text != "" && txtTo.Text != "")
                {
                    entity.FromDate = DateTime.ParseExact(txtFrom.Text, "dd/MM/yyyy", null);
                    entity.ToDate = DateTime.ParseExact(txtTo.Text, "dd/MM/yyyy", null);
                }
            }
            if (gridBottomLinks.Visible == true)
            {
                entity.ContentsDesc = txtContentDesc.Text;
                entity.ContentShortDesc = txtContentDesc.Text;
                string pageUrl = "search-results/" + ddlCountry.SelectedItem.Text.Trim() + "/" + ddlCity.SelectedItem.Text.Trim();
                if (ddlZone.SelectedItem.Text != "Select Zone")
                {
                    pageUrl += "/" + ddlZone.SelectedItem.Text.Trim();
                }
                entity.PageUrl = pageUrl.Trim();
                entity.MetaKeyword = txtKeywords.Text;
                entity.MetaDescription = txtMetaDesc.Text;
            }
            bool benefitsAndNotEnglish = (hdnCmsId.Value == "21" && hdnLanguageID.Value != "1");
            bool hotelOfWeekAndNotEnglish = (hdnCmsId.Value == "3" && hdnLanguageID.Value != "1");
            bool benefitsAndEnglish = (hdnCmsId.Value == "21" && hdnLanguageID.Value == "1");
            bool hotelOfWeekAndEnglish = (hdnCmsId.Value == "3" && hdnLanguageID.Value == "1");

            if (benefitsAndNotEnglish || hotelOfWeekAndNotEnglish)
            {
                CmsDescription obj = new CmsDescription();
                obj = manager.getCmsDescriptionEntity(Convert.ToInt64(hdnCmsId.Value), 1, hdnContentTitle.Value);
                entity.ContentImage = obj.ContentImage;
            }
            else if (benefitsAndEnglish || hotelOfWeekAndEnglish)
            {
                string newValueState = entity.ContentImage;
                string contentShortDesc = entity.ContentShortDesc;
                string contentsDesc = entity.ContentsDesc;
                DateTime? toDate = entity.ToDate;
                DateTime? fromDate = entity.FromDate;
                string title = entity.ContentTitle;
                TList<CmsDescription> allOtherLanguageRecords = new TList<CmsDescription>();
                allOtherLanguageRecords = manager.getAllCmsDescriptionsOnTitle(hdnContentTitle.Value);
                foreach (CmsDescription desc in allOtherLanguageRecords)
                {
                    if (hotelOfWeekAndEnglish)
                    {
                        desc.ToDate = toDate;
                        desc.FromDate = fromDate;
                        desc.ContentTitle = title;
                        desc.ContentsDesc = contentsDesc;
                        desc.ContentShortDesc = contentShortDesc;
                    }
                    if(benefitsAndEnglish)
                    {
                        entity.ContentShortDesc = contentShortDesc;
                    }
                    desc.ContentImage = newValueState;
                    status = manager.updateCMSDescriptionEntity(desc);
                }
            }
            if (!(benefitsAndEnglish) && !(hotelOfWeekAndEnglish))
            {
                status = manager.updateCMSDescriptionEntity(entity);
            }
        }

        #endregion

        #region When Inserting new (else part)

        else
        {
            CmsDescription newEntity = new CmsDescription();
            Cms cmsEntity = manager.getCmsEntity(Convert.ToInt64(hdnCmsId.Value));
            newEntity.CmsId = Convert.ToInt64(hdnCmsId.Value);
            newEntity.IsActive = true;

            newEntity.LanguageId = Convert.ToInt64(hdnLanguageID.Value);
            if (editor.Visible == true)
            {
                IsInfo = true;
                newEntity.ContentsDesc = ckeEditorEN.Text;
                newEntity.UpdatedDate = DateTime.Now;
            }
            if (socialNetworkingDiv.Visible == true)
            {
                IsInfo = true;
                newEntity.PageUrl = txtPageUrl.Text;
                newEntity.UpdatedDate = DateTime.Now;
            }

            #region Uploading PDf files
            if (pdfUpload.Visible == true)
            {

                // pdf upload logic here

                if (pdfUpload.HasFile)
                {
                    string strPlanName = "";
                    IsFile = true;
                    //if (pdfUpload.PostedFile.ContentLength > 1048576)
                    //{
                    //    divmessage.InnerHtml = "Filesize of plan is too large. Maximum file size permitted is 1 MB.";
                    //    divmessage.Attributes.Add("class", "error");
                    //    divmessage.Style.Add("display", "block");
                    //    return;
                    //}
                    //else
                    //{
                        Guid objId = Guid.NewGuid();
                        strPlanName = Path.GetFileName(pdfUpload.FileName);
                        strPlanName = objId + strPlanName;
                        if (gridNews.Visible == true)
                        {
                            pdfUpload.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"] + "NewsFile/") + strPlanName);
                            if (txtPublishedDate.Text != "")
                            {
                                newEntity.UpdatedDate = DateTime.ParseExact(txtPublishedDate.Text, "dd/MM/yyyy", null);
                            }
                            else
                            {
                                newEntity.UpdatedDate = DateTime.Now;
                            }
                        }
                        else
                        {
                            pdfUpload.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"] + "MediaFile/") + strPlanName);
                            newEntity.UpdatedDate = DateTime.Now;
                        }
                        newEntity.PageUrl = strPlanName;

                    //}
                }
            }
            #endregion

            #region Uploading images
            if (imageUpload.Visible == true)
            {
                // image upload logic here
                string strLogo = "";
                IsFile = true;
                if (imageUpload.HasFile)
                {
                    if ((gridHotelsOfTheWeek.Visible == true || gridBenefits.Visible == true) && imageUpload.PostedFile.ContentLength > (Convert.ToInt32(ConfigurationManager.AppSettings["LimitKB2"])))
                    {
                        divmessage.InnerHtml = "Filesize of image is too large. Maximum file size permitted is " + ConfigurationManager.AppSettings["MessageKB2"] + ".";
                        divmessage.Attributes.Add("class", "error");
                        divmessage.Style.Add("display", "block");
                        return;
                    }
                    else if (gridImageGallery.Visible == true && imageUpload.PostedFile.ContentLength > (Convert.ToInt32(ConfigurationManager.AppSettings["LimitKB3"])))
                    {
                        divmessage.InnerHtml = "Filesize of image is too large. Maximum file size permitted is " + ConfigurationManager.AppSettings["MessageKB3"] + ".";
                        divmessage.Attributes.Add("class", "error");
                        divmessage.Style.Add("display", "block");
                        return;
                    }
                    else if (hotelBanner.Visible == true && imageUpload.PostedFile.ContentLength > (Convert.ToInt32(ConfigurationManager.AppSettings["LimitKB4"])))
                    {
                        divmessage.InnerHtml = "Filesize of image is too large. Maximum file size permitted is " + ConfigurationManager.AppSettings["MessageKB4"] + ".";
                        divmessage.Attributes.Add("class", "error");
                        divmessage.Style.Add("display", "block");
                        return;
                    }
                    else if (socialNetworkingDiv.Visible == true && imageUpload.PostedFile.ContentLength > (Convert.ToInt32(ConfigurationManager.AppSettings["LimitKB1"])))
                    {
                        divmessage.InnerHtml = "Filesize of image is too large. Maximum file size permitted is " + ConfigurationManager.AppSettings["MessageKB1"] + ".";
                        divmessage.Attributes.Add("class", "error");
                        divmessage.Style.Add("display", "block");
                        return;
                    }                    
                    else
                    {
                        Guid objID = Guid.NewGuid();
                        strLogo = Path.GetFileName(imageUpload.FileName).Replace(" ", "").Replace("%20", "").Trim();
                        strLogo = System.Text.RegularExpressions.Regex.Replace(strLogo, @"[^a-zA-Z 0-9'.@]", string.Empty).Trim();
                        strLogo = objID + strLogo;
                        if (descriptionTextboxDIV.Visible == true)
                        {
                            imageUpload.SaveAs(Server.MapPath("~/Images/") + strLogo);
                        }
                        else if (gridImageGallery.Visible == true)
                        {
                            imageUpload.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"] + "FrontImages/") + strLogo);
                        }
                        else if (socialNetworkingDiv.Visible == true)
                        {
                            imageUpload.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"] + "SocialImage/") + strLogo);
                        }
                        else if (hotelBanner.Visible == true)
                        {
                            imageUpload.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"] + "FrontImages/") + strLogo);
                        }
                        else
                        {
                            imageUpload.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"] + "HotelOfTheWeek/") + strLogo);
                        }
                        newEntity.ContentImage = strLogo;
                    }
                }
                newEntity.UpdatedDate = DateTime.Now;
                newEntity.PageUrl = txturl.Text;
            }
            #endregion


            if (descriptionTextboxDIV.Visible == true)
            {
                IsInfo = true;
                newEntity.ContentShortDesc = txtDescription.Text;
                newEntity.UpdatedDate = DateTime.Now;
            }
            if (titleDiv.Visible == true)
            {
                IsInfo = true;
                newEntity.ContentTitle = System.Net.WebUtility.HtmlEncode(txtTitle.Text);
                newEntity.UpdatedDate = DateTime.Now;
                if (txtFrom.Text != "" && txtTo.Text != "")
                {
                    newEntity.FromDate = DateTime.ParseExact(txtFrom.Text, "dd/MM/yyyy", null);
                    newEntity.ToDate = DateTime.ParseExact(txtTo.Text, "dd/MM/yyyy", null);
                }

            }
            if (gridBottomLinks.Visible == true)
            {
                newEntity.ContentsDesc = txtContentDesc.Text;
                newEntity.ContentShortDesc = txtContentDesc.Text;
                string pageUrl = "search-results/" + ddlCountry.SelectedItem.Text.Trim() + "/" + ddlCity.SelectedItem.Text.Trim();
                if (ddlZone.SelectedItem.Text != "Select Zone")
                {
                    pageUrl += "/" + ddlZone.SelectedItem.Text.Trim();
                }
                newEntity.PageUrl = pageUrl.Trim();
                newEntity.MetaKeyword = txtKeywords.Text;
                newEntity.MetaDescription = txtMetaDesc.Text;
            }
            bool benefitsAndNotEnglish = (hdnCmsId.Value == "21" && hdnLanguageID.Value != "1");
            bool hotelOfWeekAndNotEnglish = (hdnCmsId.Value == "3" && hdnLanguageID.Value != "1");

            if (benefitsAndNotEnglish || hotelOfWeekAndNotEnglish)
            {
                CmsDescription obj = new CmsDescription();
                obj = manager.getCmsDescriptionEntity(Convert.ToInt64(hdnCmsId.Value), 1, hdnContentTitle.Value); // getting english entity.
                if (obj != null)
                {
                    newEntity.ContentImage = obj.ContentImage;
                    newEntity.FromDate = obj.FromDate;
                    newEntity.ToDate = obj.ToDate;
                }
            }
            
            if (hdnContentTitle.Value == "0")
            {
                Cms obj = new Cms();
                Guid objUniqId = Guid.NewGuid();
                obj = manager.getCmsEntity(Convert.ToInt64(hdnCmsId.Value == null ? "0" : hdnCmsId.Value));
                if (obj != null)
                {
                    if (txtTitle.Text == "")
                    {
                        ContentTile = obj.CmsType == null ? "" : obj.CmsType + "," + objUniqId;
                    }
                    else
                    {
                        ContentTile = System.Net.WebUtility.HtmlEncode(obj.CmsType == null ? "" : txtTitle.Text + "," + objUniqId);
                    }
                }
            }
            else
            {
                ContentTile = hdnContentTitle.Value;
            }
            newEntity.ContentTitle = ContentTile;
            status = manager.saveCMSDescriptionEntity(newEntity);
        }
        #endregion

        if (status)
        {
            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "succesfuly");
            divmessage.InnerHtml = "The information saved successfully.";
            OnClickDIV.Visible = false;
            saveCancelBtnDIV.Visible = false;
            titleHeadingDiv.Visible = false;
        }
        else
        {
            divmessage.Attributes.Add("class", "error");
            divmessage.Style.Add("display", "block");
            divmessage.InnerHtml = "The information could not be saved.";
        }
        if (gridHotelsOfTheWeek.Visible == true)
        {
            #region Bind The Grid
            titleHeadingDiv.Visible = true;
            ViewState["TypeGrd"] = "HotelOfTheWeek";
            TList<CmsDescription> list = manager.getCmsDescriptionAll(Convert.ToInt64(hdnCmsId.Value), 1);
            grvHotelsOfTheWeek.DataSource = list;
            grvHotelsOfTheWeek.DataBind();
            ApplyPaging("HotelOfTheWeek");
            #endregion
        }
        if (gridBenefits.Visible == true)
        {
            ViewState["TypeGrd"] = "Benifites";
            #region Bind The Grid
            titleHeadingDiv.Visible = true;
            TList<CmsDescription> list = manager.getCmsDescriptionAll(Convert.ToInt64(hdnCmsId.Value), 1);
            grvBenefits.DataSource = list;
            grvBenefits.DataBind();
            ApplyPaging("Benifites");
            #endregion
        }
        if (gridNews.Visible == true)
        {
            ViewState["TypeGrd"] = "News";
            #region Bind The Grid
            titleHeadingDiv.Visible = true;
            TList<CmsDescription> list = manager.getCmsDescriptionAll(Convert.ToInt64(hdnCmsId.Value), 1);
            grvNews.DataSource = list;
            grvNews.DataBind();
            ApplyPaging("News");
            #endregion
        }
        if (gridImageGallery.Visible == true)
        {
            ViewState["TypeGrd"] = "ImageGallery";
            #region Bind The Grid
            titleHeadingDiv.Visible = true;
            TList<CmsDescription> list = manager.getCmsDescriptionAll(Convert.ToInt64(hdnCmsId.Value), 1);
            grvImageGallery.DataSource = list;
            grvImageGallery.DataBind();
            ApplyPaging("ImageGallery");
            #endregion
        }
        if (hotelBanner.Visible == true)
        {
            ViewState["TypeGrd"] = "Banner";
            #region Bind The Grid
            titleHeadingDiv.Visible = true;
            TList<CmsDescription> list = manager.getCmsDescriptionAll(Convert.ToInt64(hdnCmsId.Value),1);
            grvBanner.DataSource = list;
            grvBanner.DataBind();
            ApplyPaging("Banner");
            #endregion
        }
        if (gridBottomLinks.Visible == true)
        {
            ViewState["TypeGrd"] = "BottomLink";
            #region Bind The Grid
            titleHeadingDiv.Visible = true;
            TList<CmsDescription> list = manager.getCmsDescriptionAll(Convert.ToInt64(hdnCmsId.Value),1);
            grvBottomLinks.DataSource = list;
            grvBottomLinks.DataBind();
            ApplyPaging("BottomLink");
            #endregion
        }
        if (socialNetworkingDiv.Visible == true)
        {
            bindSocialSitesDDL(SocialSitesDDL.SelectedItem.Value);
        }
    }

    /// <summary>
    /// This is the event handler for the lbtCancel LinkButton click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtCancel_Click(object sender, EventArgs e)
    {
        //Response.Redirect(Request.RawUrl);
        if (heading == "Your advantages" || heading == "Contact us" || heading == "For investers" || heading == "How to book online" || heading == "Send now A REQUEST" || heading == "Company information" || heading == "Request advantage"
            || heading == "Privacy statement" || heading == "Legal aspects" || heading == "Terms & conditions" || heading == "Social networking" || heading == "Personalised message" || heading == "Booking advantage")
        {
            topTitleDIV.Visible = false;
        }
        OnClickDIV.Visible = false;
        saveCancelBtnDIV.Visible = false;
    }

    /// <summary>
    /// This is the event handler for lbtAboutUs LinkButton click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtAboutUs_Click(object sender, EventArgs e)
    {
        #region managing control visibility
        divAgencyControlPanelImage.Visible = false;
        divMetaDescription.Visible = false;
        editor.Visible = true;
        fileUploadPDF.Visible = false;
        fileUploadImage.Visible = false;
        lbtPartners.Visible = false;
        lbtClients.Visible = false;
        gridHotelsOfTheWeek.Visible = false;
        TabbedPanels2.Visible = true;
        titleDiv.Visible = false;
        descriptionTextboxDIV.Visible = false;
        gridBenefits.Visible = false;
        gridImageGallery.Visible = false;
        gridNews.Visible = false;
        saveCancelBtnDIV.Visible = true;
        OnClickDIV.Visible = true;
        socialNetworkingDiv.Visible = false;
        divpageurl.Visible = false;
        hotelBanner.Visible = false;
        PublishedDateDIV.Visible = false;
        ViewPreviousFIleDIV.Visible = false;
        topTitleDIV.Visible = true;
        bottomLinkDropDownListsDIV.Visible = false;
        gridBottomLinks.Visible = false;
        pagingDIV.Visible = false;
        titleHeadingDiv.Visible = true;
        lbtAddNewHotel.Visible = false;
        lbtAddNewImage.Visible = false;
        lbtAddNewNews.Visible = false;
        lbtAddBenefit.Visible = false;
        lbtAddNewBottomLink.Visible = false;
        SaveStaticPages.Visible = false;
        frontStaticPages.Visible = false;
        #endregion

        heading = hdnHeading.Value = lbtAboutUs.Text;
        Cms cmsObj = manager.getCmsEntityOnType("Aboutus");
        if (cmsObj != null)
        {
            hdnContentTitle.Value = cmsObj.CmsType;
            hdnCmsId.Value = cmsObj.Id.ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        else
        {
            hdnContentTitle.Value = "Aboutus";
            hdnCmsId.Value = manager.createCmsEntity("Aboutus").ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        hdnLanguageID.Value = "1";
        LanaguageTabWithoutCountry1.CreateLanguageTab(true);
        string staticText = manager.getCmsDescription(Convert.ToInt64(hdnCmsId.Value), 1);// by default english.
        ckeEditorEN.Text = staticText;
    }

    /// <summary>
    /// This is the event handler for lbtContactUs LinkButton click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtContactUs_Click(object sender, EventArgs e)
    {
        #region managing control visibility
        divAgencyControlPanelImage.Visible = false;
        divMetaDescription.Visible = false;
        editor.Visible = true;
        fileUploadPDF.Visible = false;
        fileUploadImage.Visible = false;
        divpageurl.Visible = false;
        lbtPartners.Visible = false;
        lbtClients.Visible = false;
        gridHotelsOfTheWeek.Visible = false;
        TabbedPanels2.Visible = true;
        titleDiv.Visible = false;
        descriptionTextboxDIV.Visible = false;
        gridBenefits.Visible = false;
        saveCancelBtnDIV.Visible = true;
        OnClickDIV.Visible = true;
        gridImageGallery.Visible = false;
        gridNews.Visible = false;
        socialNetworkingDiv.Visible = false;
        hotelBanner.Visible = false;
        PublishedDateDIV.Visible = false;
        ViewPreviousFIleDIV.Visible = false;
        topTitleDIV.Visible = true;
        bottomLinkDropDownListsDIV.Visible = false;
        gridBottomLinks.Visible = false;
        pagingDIV.Visible = false;
        titleHeadingDiv.Visible = true;
        lbtAddNewHotel.Visible = false;
        lbtAddNewImage.Visible = false;
        lbtAddNewNews.Visible = false;
        lbtAddBenefit.Visible = false;
        lbtAddNewBottomLink.Visible = false;
        SaveStaticPages.Visible = false;
        frontStaticPages.Visible = false;
        #endregion

        heading = hdnHeading.Value = lbtContactUs.Text;
        Cms cmsObj = manager.getCmsEntityOnType("ContactUs");
        if (cmsObj != null)
        {
            hdnContentTitle.Value = cmsObj.CmsType;
            hdnCmsId.Value = cmsObj.Id.ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        else
        {
            hdnContentTitle.Value = "ContactUs";
            hdnCmsId.Value = manager.createCmsEntity("ContactUs").ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        hdnLanguageID.Value = "1";
        hdnContentTitle.Value = "ContactUs";
        LanaguageTabWithoutCountry1.CreateLanguageTab(true);
        string staticText = manager.getCmsDescription(Convert.ToInt64(hdnCmsId.Value), 1);
        ckeEditorEN.Text = staticText;
    }

    /// <summary>
    /// This is the event handler for lbtJoinToday LinkButton click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtJoinToday_Click(object sender, EventArgs e)
    {
        #region managing control visibility
        divAgencyControlPanelImage.Visible = false;
        divMetaDescription.Visible = false;
        editor.Visible = false;
        lbtPartners.Visible = true;
        lbtClients.Visible = true;
        fileUploadPDF.Visible = false;
        fileUploadImage.Visible = false;
        divpageurl.Visible = false;
        gridHotelsOfTheWeek.Visible = false;
        TabbedPanels2.Visible = false;
        titleDiv.Visible = false;
        descriptionTextboxDIV.Visible = false;
        gridBenefits.Visible = false;
        saveCancelBtnDIV.Visible = false;
        gridImageGallery.Visible = false;
        gridNews.Visible = false;
        socialNetworkingDiv.Visible = false;
        hotelBanner.Visible = false;
        PublishedDateDIV.Visible = false;
        ViewPreviousFIleDIV.Visible = false;
        topTitleDIV.Visible = true;
        bottomLinkDropDownListsDIV.Visible = false;
        gridBottomLinks.Visible = false;
        pagingDIV.Visible = false;
        titleHeadingDiv.Visible = true;
        lbtAddNewHotel.Visible = false;
        lbtAddNewImage.Visible = false;
        lbtAddNewNews.Visible = false;
        lbtAddBenefit.Visible = false;
        lbtAddNewBottomLink.Visible = false;
        SaveStaticPages.Visible = false;
        frontStaticPages.Visible = false;
        #endregion

        heading = hdnHeading.Value = lbtJoinToday.Text;
        hdnCmsId.Value = "0";
        hdnLanguageID.Value = "1";
        ckeEditorEN.Text = "";
        lbtPartners.BackColor = System.Drawing.Color.White;
        lbtClients.BackColor = System.Drawing.Color.White;
    }

    /// <summary>
    /// This is the event handler for lbtPartners LinkButton click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtPartners_Click(object sender, EventArgs e)
    {
        #region managing control visibility
        divAgencyControlPanelImage.Visible = false;
        divMetaDescription.Visible = false;
        editor.Visible = true;
        fileUploadPDF.Visible = false;
        fileUploadImage.Visible = false;
        divpageurl.Visible = false;
        gridHotelsOfTheWeek.Visible = false;
        TabbedPanels2.Visible = true;
        titleDiv.Visible = false;
        descriptionTextboxDIV.Visible = false;
        gridBenefits.Visible = false;
        saveCancelBtnDIV.Visible = true;
        OnClickDIV.Visible = true;
        gridImageGallery.Visible = false;
        gridNews.Visible = false;
        socialNetworkingDiv.Visible = false;
        hotelBanner.Visible = false;
        PublishedDateDIV.Visible = false;
        ViewPreviousFIleDIV.Visible = false;
        topTitleDIV.Visible = true;
        bottomLinkDropDownListsDIV.Visible = false;
        gridBottomLinks.Visible = false;
        pagingDIV.Visible = false;
        titleHeadingDiv.Visible = true;
        lbtAddNewHotel.Visible = false;
        lbtAddNewImage.Visible = false;
        lbtAddNewNews.Visible = false;
        lbtAddBenefit.Visible = false;
        lbtAddNewBottomLink.Visible = false;

        #endregion

        Cms cmsObj = manager.getCmsEntityOnType("JoinTodayPartener");
        if (cmsObj != null)
        {
            hdnContentTitle.Value = cmsObj.CmsType;
            hdnCmsId.Value = cmsObj.Id.ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        else
        {
            hdnContentTitle.Value = "JoinTodayPartener";
            hdnCmsId.Value = manager.createCmsEntity("JoinTodayPartener").ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        lbtPartners.BackColor = System.Drawing.Color.Red;
        hdnLanguageID.Value = "1";
        LanaguageTabWithoutCountry1.CreateLanguageTab(true);
        string staticText = manager.getCmsDescription(Convert.ToInt64(hdnCmsId.Value), 1);
        ckeEditorEN.Text = staticText;
        lbtPartners.BackColor = System.Drawing.Color.LightBlue;
        lbtClients.BackColor = System.Drawing.Color.White;
    }

    /// <summary>
    /// This is the event handler for lbtClients LinkButton click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtClients_Click(object sender, EventArgs e)
    {
        #region managing control visibility
        divAgencyControlPanelImage.Visible = false;
        divMetaDescription.Visible = false;
        editor.Visible = true;
        fileUploadPDF.Visible = false;
        fileUploadImage.Visible = false;
        divpageurl.Visible = false;
        gridHotelsOfTheWeek.Visible = false;
        TabbedPanels2.Visible = true;
        titleDiv.Visible = false;
        descriptionTextboxDIV.Visible = false;
        gridBenefits.Visible = false;
        saveCancelBtnDIV.Visible = true;
        OnClickDIV.Visible = true;
        gridImageGallery.Visible = false;
        gridNews.Visible = false;
        socialNetworkingDiv.Visible = false;
        hotelBanner.Visible = false;
        PublishedDateDIV.Visible = false;
        ViewPreviousFIleDIV.Visible = false;
        topTitleDIV.Visible = true;
        bottomLinkDropDownListsDIV.Visible = false;
        gridBottomLinks.Visible = false;
        pagingDIV.Visible = false;
        titleHeadingDiv.Visible = true;
        lbtAddNewHotel.Visible = false;
        lbtAddNewImage.Visible = false;
        lbtAddNewNews.Visible = false;
        lbtAddBenefit.Visible = false;
        lbtAddNewBottomLink.Visible = false;

        #endregion

        Cms cmsObj = manager.getCmsEntityOnType("JoinTodayUsers");
        if (cmsObj != null)
        {
            hdnContentTitle.Value = cmsObj.CmsType;
            hdnCmsId.Value = cmsObj.Id.ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        else
        {
            hdnContentTitle.Value = "JoinTodayUsers";
            hdnCmsId.Value = manager.createCmsEntity("JoinTodayUsers").ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        hdnLanguageID.Value = "1";
        LanaguageTabWithoutCountry1.CreateLanguageTab(true);
        string staticText = manager.getCmsDescription(Convert.ToInt64(hdnCmsId.Value), 1);
        ckeEditorEN.Text = staticText;
        lbtClients.BackColor = System.Drawing.Color.LightBlue;
        lbtPartners.BackColor = System.Drawing.Color.White;
    }

    /// <summary>
    /// This is the event handler for lbtWhyBookOnline LinkButton click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtWhyBookOnline_Click(object sender, EventArgs e)
    {
        #region managing control visibility
        divAgencyControlPanelImage.Visible = false;
        divMetaDescription.Visible = false;
        editor.Visible = true;
        fileUploadPDF.Visible = false;
        fileUploadImage.Visible = false;
        divpageurl.Visible = false;
        lbtPartners.Visible = false;
        lbtClients.Visible = false;
        gridHotelsOfTheWeek.Visible = false;
        TabbedPanels2.Visible = true;
        titleDiv.Visible = false;
        descriptionTextboxDIV.Visible = false;
        gridBenefits.Visible = false;
        saveCancelBtnDIV.Visible = true;
        gridImageGallery.Visible = false;
        gridNews.Visible = false;
        socialNetworkingDiv.Visible = false;
        hotelBanner.Visible = false;
        PublishedDateDIV.Visible = false;
        ViewPreviousFIleDIV.Visible = false;
        OnClickDIV.Visible = true;
        topTitleDIV.Visible = true;
        bottomLinkDropDownListsDIV.Visible = false;
        gridBottomLinks.Visible = false;
        pagingDIV.Visible = false;
        titleHeadingDiv.Visible = true;
        lbtAddNewHotel.Visible = false;
        lbtAddNewImage.Visible = false;
        lbtAddNewNews.Visible = false;
        lbtAddBenefit.Visible = false;
        lbtAddNewBottomLink.Visible = false;
        SaveStaticPages.Visible = false;
        frontStaticPages.Visible = false;
        #endregion

        heading = hdnHeading.Value = lbtWhyBookOnline.Text;
        Cms cmsObj = manager.getCmsEntityOnType("ReadMoreLeft");
        if (cmsObj != null)
        {
            hdnContentTitle.Value = cmsObj.CmsType;
            hdnCmsId.Value = cmsObj.Id.ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        else
        {
            hdnContentTitle.Value = "ReadMoreLeft";
            hdnCmsId.Value = manager.createCmsEntity("ReadMoreLeft").ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        hdnLanguageID.Value = "1";
        LanaguageTabWithoutCountry1.CreateLanguageTab(true);
        string staticText = manager.getCmsDescription(Convert.ToInt64(hdnCmsId.Value), 1);
        ckeEditorEN.Text = staticText;
    }

    /// <summary>
    /// This is the event handler for lbtLegalAspects LinkButton click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtLegalAspects_Click(object sender, EventArgs e)
    {
        #region managing control visibility
        divAgencyControlPanelImage.Visible = false;
        divMetaDescription.Visible = false;
        editor.Visible = false;
        fileUploadPDF.Visible = true;
        fileUploadImage.Visible = false;
        divpageurl.Visible = false;
        lbtPartners.Visible = false;
        lbtClients.Visible = false;
        gridHotelsOfTheWeek.Visible = false;
        TabbedPanels2.Visible = true;
        titleDiv.Visible = false;
        descriptionTextboxDIV.Visible = false;
        gridBenefits.Visible = false;
        saveCancelBtnDIV.Visible = true;
        gridImageGallery.Visible = false;
        gridNews.Visible = false;
        socialNetworkingDiv.Visible = false;
        hotelBanner.Visible = false;
        PublishedDateDIV.Visible = false;
        ViewPreviousFIleDIV.Visible = true;
        OnClickDIV.Visible = true;
        topTitleDIV.Visible = true;
        bottomLinkDropDownListsDIV.Visible = false;
        gridBottomLinks.Visible = false;
        pagingDIV.Visible = false;
        titleHeadingDiv.Visible = true;
        lbtAddNewHotel.Visible = false;
        lbtAddNewImage.Visible = false;
        lbtAddNewNews.Visible = false;
        lbtAddBenefit.Visible = false;
        lbtAddNewBottomLink.Visible = false;
        SaveStaticPages.Visible = false;
        frontStaticPages.Visible = false;
        #endregion

        heading = hdnHeading.Value = lbtLegalAspects.Text;
        Cms cmsObj = manager.getCmsEntityOnType("LegalAspects");
        if (cmsObj != null)
        {
            hdnContentTitle.Value = cmsObj.CmsType;
            hdnCmsId.Value = cmsObj.Id.ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        else
        {
            hdnContentTitle.Value = "LegalAspects";
            hdnCmsId.Value = manager.createCmsEntity("LegalAspects").ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        hdnLanguageID.Value = "1";
        hdnLinkMedia.Value = "Legal aspects";
        hdnTask.Value = "New";
        LanaguageTabWithoutCountry1.CreateLanguageTab(true);
        CmsDescription obj1 = null;
        TList<CmsDescription> list = null;
        string filepath;
        list = objManageCMSContent.GetCMSContent((Int32)cmsObj.Id, Convert.ToInt64(hdnLanguageID.Value));
        if (list.Count != 0)
        {
            obj1 = list[0];
        }

        if (obj1 != null)
        {
            filepath = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Replace("~/", "") + "MediaFile/" + obj1.PageUrl;
            lbtViewCurrentFile.OnClientClick = "return open_win('" + filepath + "')";
        }
        else
        {
            ViewPreviousFIleDIV.Visible = false;
        }
    }

    /// <summary>
    /// This is the event handler for lbtWhySendRequest LinkButton click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtWhySendRequest_Click(object sender, EventArgs e)
    {
        #region managing control visibility
        divAgencyControlPanelImage.Visible = false;
        divMetaDescription.Visible = false;
        editor.Visible = true;
        fileUploadPDF.Visible = false;
        fileUploadImage.Visible = false;
        divpageurl.Visible = false;
        lbtPartners.Visible = false;
        lbtClients.Visible = false;
        gridHotelsOfTheWeek.Visible = false;
        TabbedPanels2.Visible = true;
        titleDiv.Visible = false;
        descriptionTextboxDIV.Visible = false;
        gridBenefits.Visible = false;
        saveCancelBtnDIV.Visible = true;
        gridImageGallery.Visible = false;
        gridNews.Visible = false;
        socialNetworkingDiv.Visible = false;
        hotelBanner.Visible = false;
        PublishedDateDIV.Visible = false;
        ViewPreviousFIleDIV.Visible = false;
        OnClickDIV.Visible = true;
        topTitleDIV.Visible = true;
        bottomLinkDropDownListsDIV.Visible = false;
        gridBottomLinks.Visible = false;
        pagingDIV.Visible = false;
        titleHeadingDiv.Visible = true;
        lbtAddNewHotel.Visible = false;
        lbtAddNewImage.Visible = false;
        lbtAddNewNews.Visible = false;
        lbtAddBenefit.Visible = false;
        lbtAddNewBottomLink.Visible = false;
        SaveStaticPages.Visible = false;
        frontStaticPages.Visible = false;
        #endregion

        heading = hdnHeading.Value = lbtWhySendRequest.Text;
        Cms cmsObj = manager.getCmsEntityOnType("ReadMoreRight");
        if (cmsObj != null)
        {
            hdnContentTitle.Value = cmsObj.CmsType;
            hdnCmsId.Value = cmsObj.Id.ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        else
        {
            hdnContentTitle.Value = "ReadMoreRight";
            hdnCmsId.Value = manager.createCmsEntity("ReadMoreRight").ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        hdnLanguageID.Value = "1";
        LanaguageTabWithoutCountry1.CreateLanguageTab(true);
        string staticText = manager.getCmsDescription(Convert.ToInt64(hdnCmsId.Value), 1);
        ckeEditorEN.Text = staticText;
    }

    /// <summary>
    /// This is the event handler for lbtTermsNConditions LinkButton click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtTermsNConditions_Click(object sender, EventArgs e)
    {
        #region managing control visibility
        divAgencyControlPanelImage.Visible = false;
        divMetaDescription.Visible = false;
        fileUploadPDF.Visible = true;
        editor.Visible = false;
        fileUploadImage.Visible = false;
        divpageurl.Visible = false;
        lbtPartners.Visible = false;
        lbtClients.Visible = false;
        gridHotelsOfTheWeek.Visible = false;
        TabbedPanels2.Visible = true;
        titleDiv.Visible = false;
        descriptionTextboxDIV.Visible = false;
        gridBenefits.Visible = false;
        saveCancelBtnDIV.Visible = true;
        gridImageGallery.Visible = false;
        gridNews.Visible = false;
        socialNetworkingDiv.Visible = false;
        hotelBanner.Visible = false;
        PublishedDateDIV.Visible = false;
        ViewPreviousFIleDIV.Visible = true;
        OnClickDIV.Visible = true;
        topTitleDIV.Visible = true;
        bottomLinkDropDownListsDIV.Visible = false;
        gridBottomLinks.Visible = false;
        pagingDIV.Visible = false;
        titleHeadingDiv.Visible = true;
        lbtAddNewHotel.Visible = false;
        lbtAddNewImage.Visible = false;
        lbtAddNewNews.Visible = false;
        lbtAddBenefit.Visible = false;
        lbtAddNewBottomLink.Visible = false;
        SaveStaticPages.Visible = false;
        frontStaticPages.Visible = false;
        #endregion

        heading = hdnHeading.Value = lbtTermsNConditions.Text;
        Cms cmsObj = manager.getCmsEntityOnType("TermsAndConditions");
        if (cmsObj != null)
        {
            hdnContentTitle.Value = cmsObj.CmsType;
            hdnCmsId.Value = cmsObj.Id.ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        else
        {
            hdnContentTitle.Value = "TermsAndConditions";
            hdnCmsId.Value = manager.createCmsEntity("TermsAndConditions").ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        hdnLanguageID.Value = "1";
        hdnLinkMedia.Value = "Terms and conditions";
        hdnTask.Value = "New";
        LanaguageTabWithoutCountry1.CreateLanguageTab(true);
        CmsDescription obj1 = null;
        TList<CmsDescription> list = null;
        string filepath;
        list = objManageCMSContent.GetCMSContent((Int32)cmsObj.Id, Convert.ToInt64(hdnLanguageID.Value));
        if (list.Count != 0)
        {
            obj1 = list[0];
        }

        if (obj1 != null)
        {
            filepath = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Replace("~/", "") + "MediaFile/" + obj1.PageUrl.ToString();
            lbtViewCurrentFile.OnClientClick = "return open_win('" + filepath + "')";
        }
        else
        {
            ViewPreviousFIleDIV.Visible = false;
        }
    }

    /// <summary>
    /// This is the event handler for lbtCompanyInfo LinkButton click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtCompanyInfo_Click(object sender, EventArgs e)
    {
        #region managing control visibility
        divAgencyControlPanelImage.Visible = false;
        divMetaDescription.Visible = false;
        fileUploadPDF.Visible = true;
        editor.Visible = false;
        fileUploadImage.Visible = false;
        divpageurl.Visible = false;
        lbtPartners.Visible = false;
        lbtClients.Visible = false;
        gridHotelsOfTheWeek.Visible = false;
        TabbedPanels2.Visible = true;
        titleDiv.Visible = false;
        descriptionTextboxDIV.Visible = false;
        gridBenefits.Visible = false;
        saveCancelBtnDIV.Visible = true;
        gridImageGallery.Visible = false;
        gridNews.Visible = false;
        socialNetworkingDiv.Visible = false;
        hotelBanner.Visible = false;
        PublishedDateDIV.Visible = false;
        ViewPreviousFIleDIV.Visible = true;
        OnClickDIV.Visible = true;
        topTitleDIV.Visible = true;
        bottomLinkDropDownListsDIV.Visible = false;
        gridBottomLinks.Visible = false;
        pagingDIV.Visible = false;
        titleHeadingDiv.Visible = true;
        lbtAddNewHotel.Visible = false;
        lbtAddNewImage.Visible = false;
        lbtAddNewNews.Visible = false;
        lbtAddBenefit.Visible = false;
        lbtAddNewBottomLink.Visible = false;
        SaveStaticPages.Visible = false;
        frontStaticPages.Visible = false;
        #endregion

        heading = hdnHeading.Value = lbtCompanyInfo.Text;
        Cms cmsObj = manager.getCmsEntityOnType("CompanyInfo");
        if (cmsObj != null)
        {
            hdnContentTitle.Value = cmsObj.CmsType;
            hdnCmsId.Value = cmsObj.Id.ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        else
        {
            hdnContentTitle.Value = "CompanyInfo";
            hdnCmsId.Value = manager.createCmsEntity("CompanyInfo").ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        hdnLanguageID.Value = "1";
        hdnLinkMedia.Value = "Company information";
        hdnTask.Value = "New";
        LanaguageTabWithoutCountry1.CreateLanguageTab(true);
        CmsDescription obj1 = null;
        TList<CmsDescription> list = null;
        string filepath;
        list = objManageCMSContent.GetCMSContent((Int32)cmsObj.Id, Convert.ToInt64(hdnLanguageID.Value));
        if (list.Count != 0)
        {
            obj1 = list[0];
        }

        if (obj1 != null)
        {
            filepath = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Replace("~/", "") + "MediaFile/" + obj1.PageUrl.ToString();
            lbtViewCurrentFile.OnClientClick = "return open_win('" + filepath + "')";
        }
        else
        {
            ViewPreviousFIleDIV.Visible = false;
        }
    }

    /// <summary>
    /// This is the event handler for lbtHotelsOfWeek LinkButton click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtHotelsOfWeek_Click(object sender, EventArgs e)
    {
        ViewState["TypeGrd"] = "HotelOfTheWeek";
        #region managing control visibility
        divAgencyControlPanelImage.Visible = false;
        gridHotelsOfTheWeek.Visible = true;
        editor.Visible = false;
        fileUploadPDF.Visible = false;
        fileUploadImage.Visible = false;
        divpageurl.Visible = false;
        lbtClients.Visible = false;
        lbtPartners.Visible = false;
        TabbedPanels2.Visible = false;
        titleDiv.Visible = false;
        descriptionTextboxDIV.Visible = false;
        gridBenefits.Visible = false;
        gridImageGallery.Visible = false;
        gridNews.Visible = false;
        saveCancelBtnDIV.Visible = false;
        socialNetworkingDiv.Visible = false;
        hotelBanner.Visible = false;
        PublishedDateDIV.Visible = false;
        ViewPreviousFIleDIV.Visible = false;
        topTitleDIV.Visible = true;
        bottomLinkDropDownListsDIV.Visible = false;
        gridBottomLinks.Visible = false;
        pagingDIV.Visible = true;
        titleHeadingDiv.Visible = true;
        SaveStaticPages.Visible = false;
        frontStaticPages.Visible = false;
        divMetaDescription.Visible = false;
        #endregion

        hdnLanguageID.Value = "1";
        Cms cmsObj = manager.getCmsEntityOnType("HoteloftheWeek");
        if (cmsObj != null)
        {
            hdnContentTitle.Value = cmsObj.CmsType;
            hdnCmsId.Value = cmsObj.Id.ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        else
        {
            hdnContentTitle.Value = "HoteloftheWeek";
            hdnCmsId.Value = manager.createCmsEntity("HoteloftheWeek").ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        heading = hdnHeading.Value = lbtHotelsOfWeek.Text;
        lbtAddNewHotel.Visible = true;   
        lbtAddNewImage.Visible = false;
        lbtAddNewNews.Visible = false;
        lbtAddBenefit.Visible = false;
        lbtAddNewBottomLink.Visible = false;

        #region Bind The Grid

        TList<CmsDescription> list = manager.getCmsDescriptionAll(Convert.ToInt64(hdnCmsId.Value),1);
        grvHotelsOfTheWeek.DataSource = list;
        grvHotelsOfTheWeek.DataBind();
        ApplyPaging("HotelOfTheWeek");
        //ApplyPaging(grvHotelsOfTheWeek);
        Session["CurrentGrid"] = grvHotelsOfTheWeek;

        #endregion

        Cms obj = manager.getCmsEntity(Convert.ToInt64(hdnCmsId.Value));
        if (obj != null)
        {
            if (obj.IsActive == true)
            {
                chkActivate.Checked = true;
            }
            else
            {
                chkActivate.Checked = false;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lnkStaticPages_Click(object sender, EventArgs e)
    {
        
        Cms cmsObj = manager.getCmsEntityOnType("StaticPages");
        if(cmsObj != null)
        {
            
            rptFrontStaticPage.DataSource = objManageCMSContent.GetCMSContent(cmsObj.Id, Convert.ToInt64(hdnLanguageID.Value)).Select(a => new newString() { name = a.ContentTitle }).Distinct();
            rptFrontStaticPage.DataBind();
            #region managing control visibility
            divAgencyControlPanelImage.Visible = false;
            fileUploadPDF.Visible = false;
            editor.Visible = false;
            fileUploadImage.Visible = false;
            divpageurl.Visible = false;
            lbtPartners.Visible = false;
            lbtClients.Visible = false;
            gridHotelsOfTheWeek.Visible = false;
            TabbedPanels2.Visible = false;
            titleDiv.Visible = false;
            descriptionTextboxDIV.Visible = false;
            gridBenefits.Visible = false;
            saveCancelBtnDIV.Visible = false;
            gridImageGallery.Visible = false;
            gridNews.Visible = false;
            socialNetworkingDiv.Visible = false;
            TabbedPanels2.Visible = false;
            hotelBanner.Visible = false;
            PublishedDateDIV.Visible = false;
            ViewPreviousFIleDIV.Visible = false;
            OnClickDIV.Visible = false;
            topTitleDIV.Visible = true;
            gridBottomLinks.Visible = false;
            calender.Visible = false;
            bottomLinkDropDownListsDIV.Visible = false;
            pagingDIV.Visible = false;
            titleHeadingDiv.Visible = true;
            lbtAddNewHotel.Visible = false;
            lbtAddNewImage.Visible = false;
            lbtAddNewNews.Visible = false;
            lbtAddBenefit.Visible = false;
            SaveStaticPages.Visible = false;
            frontStaticPages.Visible = true;
            
            
            #endregion
            topTitleDIV.Visible = true;
            titleHeadingDiv.Visible = true;
            heading = "Front Static Pages";
            
        }
    }

    /// <summary>
    /// This is the event handler for lbtForInvestors LinkButton click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtForInvestors_Click(object sender, EventArgs e)
    {
        #region managing control visibility
        divAgencyControlPanelImage.Visible = false;
        fileUploadPDF.Visible = true;
        editor.Visible = false;
        fileUploadImage.Visible = false;
        divpageurl.Visible = false;
        lbtPartners.Visible = false;
        lbtClients.Visible = false;
        gridHotelsOfTheWeek.Visible = false;
        TabbedPanels2.Visible = true;
        titleDiv.Visible = false;
        descriptionTextboxDIV.Visible = false;
        gridBenefits.Visible = false;
        saveCancelBtnDIV.Visible = true;
        gridImageGallery.Visible = false;
        gridNews.Visible = false;
        socialNetworkingDiv.Visible = false;
        hotelBanner.Visible = false;
        PublishedDateDIV.Visible = false;
        ViewPreviousFIleDIV.Visible = true;
        OnClickDIV.Visible = true;
        topTitleDIV.Visible = true;
        bottomLinkDropDownListsDIV.Visible = false;
        gridBottomLinks.Visible = false;
        pagingDIV.Visible = false;
        titleHeadingDiv.Visible = true;
        lbtAddNewHotel.Visible = false;
        lbtAddNewImage.Visible = false;
        lbtAddNewNews.Visible = false;
        lbtAddBenefit.Visible = false;
        lbtAddNewBottomLink.Visible = false;
        SaveStaticPages.Visible = false;
        frontStaticPages.Visible = false;
        #endregion
        string filepath;
        CmsDescription obj1 = null;
        TList<CmsDescription> list = null;
        heading = hdnHeading.Value = lbtForInvestors.Text;
        Cms cmsObj = manager.getCmsEntityOnType("ForInvestors");
        if (cmsObj != null)
        {
            hdnContentTitle.Value = cmsObj.CmsType;
            hdnCmsId.Value = cmsObj.Id.ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        else
        {
            hdnContentTitle.Value = "ForInvestors";
            hdnCmsId.Value = manager.createCmsEntity("ForInvestors").ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        hdnLanguageID.Value = "1";
        hdnLinkMedia.Value = "For investors";
        hdnTask.Value = "New";
        LanaguageTabWithoutCountry1.CreateLanguageTab(true);
        list = objManageCMSContent.GetCMSContent((Int32)cmsObj.Id, Convert.ToInt64(hdnLanguageID.Value));
        if (list.Count != 0)
        {
            obj1 = list[0];
        }

        if (obj1 != null)
        {
            filepath = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Replace("~/", "") + "MediaFile/" + obj1.PageUrl.ToString();
            lbtViewCurrentFile.OnClientClick = "return open_win('" + filepath + "')";
        }
        else
        {
            ViewPreviousFIleDIV.Visible = false;
        }
                
    }

    /// <summary>
    /// This is the event handler for lbtImageGalleryFrontPage LinkButton click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtImageGalleryFrontPage_Click(object sender, EventArgs e)
    {
        ViewState["TypeGrd"] = "ImageGallery";
        #region managing control visibility
        divAgencyControlPanelImage.Visible = false;
        gridImageGallery.Visible = true;
        fileUploadPDF.Visible = false;
        divpageurl.Visible = true;
        editor.Visible = false;
        fileUploadImage.Visible = false;
        lbtPartners.Visible = false;
        lbtClients.Visible = false;
        gridHotelsOfTheWeek.Visible = false;
        gridNews.Visible = false;
        TabbedPanels2.Visible = false;
        titleDiv.Visible = false;
        descriptionTextboxDIV.Visible = false;
        gridBenefits.Visible = false;
        saveCancelBtnDIV.Visible = false;
        socialNetworkingDiv.Visible = false;
        hotelBanner.Visible = false;
        PublishedDateDIV.Visible = false;
        ViewPreviousFIleDIV.Visible = false;
        topTitleDIV.Visible = true;
        bottomLinkDropDownListsDIV.Visible = false;
        gridBottomLinks.Visible = false;
        pagingDIV.Visible = true;
        titleHeadingDiv.Visible = true;
        SaveStaticPages.Visible = false;
        frontStaticPages.Visible = false;
        divMetaDescription.Visible = false;
        #endregion

        Cms cmsObj = manager.getCmsEntityOnType("ImageGalleryFrontEnd");
        if (cmsObj != null)
        {
            hdnContentTitle.Value = cmsObj.CmsType;
            hdnCmsId.Value = cmsObj.Id.ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        else
        {
            hdnContentTitle.Value = "ImageGalleryFrontEnd";
            hdnCmsId.Value = manager.createCmsEntity("ImageGalleryFrontEnd").ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        hdnLanguageID.Value = "1";
        heading = hdnHeading.Value = lbtImageGalleryFrontPage.Text;        
        lbtAddNewHotel.Visible = false;   
        lbtAddNewImage.Visible = true;
        lbtAddNewNews.Visible = false;
        lbtAddBenefit.Visible = false;
        lbtAddNewBottomLink.Visible = false;

        #region Bind The Grid

        TList<CmsDescription> list = manager.getCmsDescriptionAll(Convert.ToInt64(hdnCmsId.Value),1);
        grvImageGallery.DataSource = list;
        grvImageGallery.DataBind();
        ApplyPaging("ImageGallery");

        #endregion
    }

    /// <summary>
    /// This is the event handler for lbtNews LinkButton click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtNews_Click(object sender, EventArgs e)
    {
        ViewState["TypeGrd"] = "News";
        #region managing control visibility
        divAgencyControlPanelImage.Visible = false;
        editor.Visible = false;
        gridNews.Visible = true;
        lbtPartners.Visible = false;
        lbtClients.Visible = false;
        gridHotelsOfTheWeek.Visible = false;
        TabbedPanels2.Visible = false;
        titleDiv.Visible = false;
        descriptionTextboxDIV.Visible = false;
        gridBenefits.Visible = false;
        saveCancelBtnDIV.Visible = false;
        fileUploadImage.Visible = false;
        divpageurl.Visible = false;
        fileUploadPDF.Visible = false;
        gridImageGallery.Visible = false;
        socialNetworkingDiv.Visible = false;
        hotelBanner.Visible = false;
        PublishedDateDIV.Visible = false;
        ViewPreviousFIleDIV.Visible = false;
        topTitleDIV.Visible = true;
        bottomLinkDropDownListsDIV.Visible = false;
        gridBottomLinks.Visible = false;
        pagingDIV.Visible = true;
        titleHeadingDiv.Visible = true;
        SaveStaticPages.Visible = false;
        frontStaticPages.Visible = false;
        #endregion

        heading = hdnHeading.Value = lbtNews.Text;
        Cms cmsObj = manager.getCmsEntityOnType("news");
        if (cmsObj != null)
        {
            hdnContentTitle.Value = cmsObj.CmsType;
            hdnCmsId.Value = cmsObj.Id.ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        else
        {
            hdnContentTitle.Value = "news";
            hdnCmsId.Value = manager.createCmsEntity("news").ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        hdnLanguageID.Value = "1";
        CalendarExtender1.SelectedDate = DateTime.Now.Date;
        LanaguageTabWithoutCountry1.CreateLanguageTab(true);        
        lbtAddNewHotel.Visible = false;
        lbtAddNewImage.Visible = false;
        lbtAddNewNews.Visible = true;
        lbtAddBenefit.Visible = false;
        lbtAddNewBottomLink.Visible = false;

        #region Bind The Grid

        TList<CmsDescription> list = manager.getCmsDescriptionAll(Convert.ToInt64(hdnCmsId.Value),1);
        //ViewState["CurrentGrid"] = grvNews;
        grvNews.DataSource = list;
        grvNews.DataBind();
        ApplyPaging("News");

        #endregion
    }

    /// <summary>
    /// This is the event handler for lbtBenefit LinkButton click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtBenefits_Click(object sender, EventArgs e)
    {
        ViewState["TypeGrd"] = "Benifites";
        #region managing control visibility
        divAgencyControlPanelImage.Visible = false;
        fileUploadImage.Visible = false;
        divpageurl.Visible = false;
        TabbedPanels2.Visible = false;
        titleDiv.Visible = false;
        descriptionTextboxDIV.Visible = false;
        fileUploadPDF.Visible = false;
        editor.Visible = false;
        lbtPartners.Visible = false;
        lbtClients.Visible = false;
        gridHotelsOfTheWeek.Visible = false;
        gridBenefits.Visible = true;
        saveCancelBtnDIV.Visible = false;
        gridImageGallery.Visible = false;
        gridNews.Visible = false;
        socialNetworkingDiv.Visible = false;
        hotelBanner.Visible = false;
        PublishedDateDIV.Visible = false;
        ViewPreviousFIleDIV.Visible = false;
        topTitleDIV.Visible = true;
        bottomLinkDropDownListsDIV.Visible = false;
        gridBottomLinks.Visible = false;
        pagingDIV.Visible = true;
        titleHeadingDiv.Visible = true;
        SaveStaticPages.Visible = false;
        frontStaticPages.Visible = false;
        #endregion

        heading = hdnHeading.Value = lbtBenefits.Text;
        Cms cmsObj = manager.getCmsEntityOnType("Benifits");
        if (cmsObj != null)
        {
            hdnContentTitle.Value = cmsObj.CmsType;
            hdnCmsId.Value = cmsObj.Id.ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        else
        {
            hdnContentTitle.Value = "Benifits";
            hdnCmsId.Value = manager.createCmsEntity("Benifits").ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        hdnLanguageID.Value = "1";
        lbtAddNewHotel.Visible = false;
        lbtAddNewImage.Visible = false;
        lbtAddNewNews.Visible = false;
        lbtAddBenefit.Visible = true;
        lbtAddNewBottomLink.Visible = false;


        #region Bind The Grid

        TList<CmsDescription> list = manager.getCmsDescriptionAll(Convert.ToInt64(hdnCmsId.Value),1);
        //ViewState["CurrentGrid"] = grvBenefits;
        grvBenefits.DataSource = list;
        grvBenefits.DataBind();
        ApplyPaging("Benifites");

        #endregion
    }

    /// <summary>
    /// This is the event handler for lbtPrivacyStatement LinkButton click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtPrivacyStatement_Click(object sender, EventArgs e)
    {
        #region managing control visibility
        divAgencyControlPanelImage.Visible = false;
        fileUploadPDF.Visible = true;
        editor.Visible = false;
        fileUploadImage.Visible = false;
        divpageurl.Visible = false;
        lbtPartners.Visible = false;
        lbtClients.Visible = false;
        gridHotelsOfTheWeek.Visible = false;
        TabbedPanels2.Visible = true;
        titleDiv.Visible = false;
        descriptionTextboxDIV.Visible = false;
        gridBenefits.Visible = false;
        saveCancelBtnDIV.Visible = true;
        gridImageGallery.Visible = false;
        gridNews.Visible = false;
        socialNetworkingDiv.Visible = false;
        hotelBanner.Visible = false;
        PublishedDateDIV.Visible = false;
        ViewPreviousFIleDIV.Visible = true;
        OnClickDIV.Visible = true;
        topTitleDIV.Visible = true;
        bottomLinkDropDownListsDIV.Visible = false;
        gridBottomLinks.Visible = false;
        pagingDIV.Visible = false;
        titleHeadingDiv.Visible = true;
        lbtAddNewHotel.Visible = false;
        lbtAddNewImage.Visible = false;
        lbtAddNewNews.Visible = false;
        lbtAddBenefit.Visible = false;
        lbtAddNewBottomLink.Visible = false;
        SaveStaticPages.Visible = false;
        frontStaticPages.Visible = false;
        #endregion

        heading = hdnHeading.Value = lbtPrivacyStatement.Text;
        Cms cmsObj = manager.getCmsEntityOnType("PrivacyStatement");
        if (cmsObj != null)
        {
            hdnContentTitle.Value = cmsObj.CmsType;
            hdnCmsId.Value = cmsObj.Id.ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        else
        {
            hdnContentTitle.Value = "PrivacyStatement";
            hdnCmsId.Value = manager.createCmsEntity("PrivacyStatement").ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        hdnLanguageID.Value = "1";
        hdnLinkMedia.Value = "Privacy statement";
        hdnTask.Value = "New";
        LanaguageTabWithoutCountry1.CreateLanguageTab(true);
        CmsDescription obj1 = null;
        TList<CmsDescription> list = null;
        string filepath;
        list = objManageCMSContent.GetCMSContent((Int32)cmsObj.Id, Convert.ToInt64(hdnLanguageID.Value));
        if (list.Count != 0)
        {
            obj1 = list[0];
        }

        if (obj1 != null)
        {
            filepath = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Replace("~/", "") + "MediaFile/" + obj1.PageUrl.ToString();
            lbtViewCurrentFile.OnClientClick = "return open_win('" + filepath + "')";
        }
        else
        {
            ViewPreviousFIleDIV.Visible = false;
        }
    }

    /// <summary>
    /// This is the event handler for lbtSocialLinks LinkButton click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtSocialLinks_Click(object sender, EventArgs e)
    {
        #region managing control visibility
        divAgencyControlPanelImage.Visible = false;
        fileUploadPDF.Visible = false;
        editor.Visible = false;
        fileUploadImage.Visible = true;
        divpageurl.Visible = false;
        lbtPartners.Visible = false;
        divpageurl.Visible = false;
        lbtClients.Visible = false;
        gridHotelsOfTheWeek.Visible = false;
        TabbedPanels2.Visible = false;
        titleDiv.Visible = false;
        descriptionTextboxDIV.Visible = false;
        gridBenefits.Visible = false;
        saveCancelBtnDIV.Visible = true;
        gridImageGallery.Visible = false;
        gridNews.Visible = false;
        socialNetworkingDiv.Visible = true;
        
        hotelBanner.Visible = false;
        PublishedDateDIV.Visible = false;
        ViewPreviousFIleDIV.Visible = false;
        OnClickDIV.Visible = true;
        topTitleDIV.Visible = true;
        bottomLinkDropDownListsDIV.Visible = false;
        gridBottomLinks.Visible = false;
        pagingDIV.Visible = false;
        titleHeadingDiv.Visible = true;
        lbtAddNewHotel.Visible = false;
        lbtAddNewImage.Visible = false;
        lbtAddNewNews.Visible = false;
        lbtAddBenefit.Visible = false;
        lbtAddNewBottomLink.Visible = false;
        SaveStaticPages.Visible = false;
        frontStaticPages.Visible = false;
        #endregion

        Cms cmsObj = manager.getCmsEntityOnType("Links");
        if (cmsObj != null)
        {
            hdnContentTitle.Value = cmsObj.CmsType;
            hdnCmsId.Value = cmsObj.Id.ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        else
        {
            hdnContentTitle.Value = "Links";
            hdnCmsId.Value = manager.createCmsEntity("Links").ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        hdnLanguageID.Value = "1";
        hdnTask.Value = "New";
        heading = hdnHeading.Value = lbtSocialLinks.Text;
        bindSocialSitesDDL("0");
    }

    /// <summary>
    /// This is the event handler for lbtNewsBanner LinkButton click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtNewsBanner_Click(object sender, EventArgs e)
    {
        ViewState["TypeGrd"] = "Banner";
        #region managing control visibility
        divAgencyControlPanelImage.Visible = false;
        fileUploadPDF.Visible = false;
        editor.Visible = false;
        fileUploadImage.Visible = false;
        divpageurl.Visible = false;
        lbtPartners.Visible = false;
        lbtClients.Visible = false;
        gridHotelsOfTheWeek.Visible = false;
        TabbedPanels2.Visible = false;
        titleDiv.Visible = false;
        descriptionTextboxDIV.Visible = false;
        gridBenefits.Visible = false;
        saveCancelBtnDIV.Visible = false;
        gridImageGallery.Visible = false;
        gridNews.Visible = false;
        socialNetworkingDiv.Visible = false;
        hotelBanner.Visible = true;
        PublishedDateDIV.Visible = false;
        ViewPreviousFIleDIV.Visible = false;
        topTitleDIV.Visible = true;
        bottomLinkDropDownListsDIV.Visible = false;
        gridBottomLinks.Visible = false;
        pagingDIV.Visible = false;
        titleHeadingDiv.Visible = true;
        lbtAddNewHotel.Visible = false;
        lbtAddNewImage.Visible = false;
        lbtAddNewNews.Visible = false;
        lbtAddBenefit.Visible = false;
        lbtAddNewBottomLink.Visible = false;
        SaveStaticPages.Visible = false;
        frontStaticPages.Visible = false;
        #endregion

        Cms cmsObj = manager.getCmsEntityOnType("HotelBackendBanner");
        if (cmsObj != null)
        {
            hdnContentTitle.Value = cmsObj.CmsType;
            hdnCmsId.Value = cmsObj.Id.ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        else
        {
            hdnContentTitle.Value = "HotelBackendBanner";
            hdnCmsId.Value = manager.createCmsEntity("HotelBackendBanner").ToString();
        }
        hdnLanguageID.Value = "1";
        hdnRecordID.Value = hdnCmsId.Value;
        heading = hdnHeading.Value = lbtNewsBanner.Text;
        

        #region Bind The Grid

        TList<CmsDescription> list = manager.getCmsDescriptionAll(Convert.ToInt64(hdnCmsId.Value),1);
        //ViewState["CurrentGrid"] = grvBanner;
        grvBanner.DataSource = list;
        grvBanner.DataBind();
        ApplyPaging("Banner");
       
        #endregion
        
    }


    protected void lbtAgencyImage_Click(object sender, EventArgs e)
    {
        ViewState["TypeGrd"] = "AgencyControlPanel";
        modifyAgencyControlPanel.Visible = false;
        #region managing control visibility
        fileUploadPDF.Visible = false;
        editor.Visible = false;
        fileUploadImage.Visible = false;
        divpageurl.Visible = false;
        lbtPartners.Visible = false;
        lbtClients.Visible = false;
        gridHotelsOfTheWeek.Visible = false;
        TabbedPanels2.Visible = false;
        titleDiv.Visible = false;
        descriptionTextboxDIV.Visible = false;
        gridBenefits.Visible = false;
        saveCancelBtnDIV.Visible = false;
        gridImageGallery.Visible = false;
        gridNews.Visible = false;
        socialNetworkingDiv.Visible = false;
        hotelBanner.Visible = false;
        PublishedDateDIV.Visible = false;
        ViewPreviousFIleDIV.Visible = false;
        topTitleDIV.Visible = true;
        bottomLinkDropDownListsDIV.Visible = false;
        gridBottomLinks.Visible = false;
        pagingDIV.Visible = false;
        titleHeadingDiv.Visible = true;
        lbtAddNewHotel.Visible = false;
        lbtAddNewImage.Visible = false;
        lbtAddNewNews.Visible = false;
        lbtAddBenefit.Visible = false;
        lbtAddNewBottomLink.Visible = false;
        SaveStaticPages.Visible = false;
        frontStaticPages.Visible = false;
        divAgencyControlPanelImage.Visible = true;
        #endregion

        Cms cmsObj = manager.getCmsEntityOnType("AgencyControlPanel");
        if (cmsObj != null)
        {
            //hdnContentTitle.Value = cmsObj.CmsType;
            hdnCmsId.Value = cmsObj.Id.ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        else
        {
            //hdnContentTitle.Value = "AgencyControlPanel";
            hdnCmsId.Value = manager.createCmsEntity("AgencyControlPanel").ToString();
        }
        hdnLanguageID.Value = "1";
        hdnRecordID.Value = hdnCmsId.Value;
        heading = hdnHeading.Value = lnkAgencyImage.Text;


        #region Bind The Grid

        TList<CmsDescription> list = manager.getCmsDescriptionAll(Convert.ToInt64(hdnCmsId.Value), Convert.ToInt32(hdnLanguageID.Value));
        //ViewState["CurrentGrid"] = grvBanner;
        rptAgencyControlPanel.DataSource = list;
        rptAgencyControlPanel.DataBind();
        if (list.Count <= 0)
        {
            modifyAgencyControlPanel.Visible = true ;
        }
        #endregion
    }
    protected void lnkModify_OnClick(object sender, EventArgs e)
    {
        modifyAgencyControlPanel.Visible = true;
        TList<CmsDescription> cmd2 = objManageCMSContent.GetCMSContent(Convert.ToInt64(hdnCmsId.Value), Convert.ToInt64(hdnLanguageID.Value));
        if (cmd2 != null)
        {
            if (cmd2.Count > 0)
            {
                txtTitleImage1.Text = HttpUtility.HtmlEncode(cmd2[0].ContentTitle);
            }
            if (cmd2.Count > 1)
            {
                txtTitleImage2.Text = HttpUtility.HtmlEncode(cmd2[1].ContentTitle);
            }
            if (cmd2.Count > 2)
            {
                txtTitleImage3.Text = HttpUtility.HtmlEncode(cmd2[2].ContentTitle);
            }
        }
    }
    protected void lnkSaveAgencyImage_Click(object sender, EventArgs e)
    {
        bool status = false;
        if (HttpUtility.HtmlDecode(hdnContentTitle.Value) == hdnContentTitle.Value)
        {
            hdnContentTitle.Value = HttpUtility.HtmlEncode(hdnContentTitle.Value);
        }
        TList <CmsDescription> cmd2 = objManageCMSContent.GetCMSContent(Convert.ToInt64(hdnCmsId.Value), Convert.ToInt64(hdnLanguageID.Value));
        CmsDescription cmdImg1 = new CmsDescription();
        CmsDescription cmdImg2 = new CmsDescription();
        CmsDescription cmdImg3 = new CmsDescription();
        TList<CmsDescription> cmd2New = new TList<CmsDescription>();
        if (cmd2 != null)
        {
            if (cmd2.Count > 0)
            {
                cmdImg1 = cmd2[0];
                cmdImg1.EntityState = EntityState.Changed;
            }
            if (cmd2.Count > 1)
            {
                cmdImg2 = cmd2[1];
                cmdImg2.EntityState = EntityState.Changed;
            }
            if (cmd2.Count > 2)
            {
                cmdImg3 = cmd2[2];
                cmdImg3.EntityState = EntityState.Changed;
            }
            cmdImg1.CmsId = Convert.ToInt64(hdnCmsId.Value);
            cmdImg1.LanguageId = Convert.ToInt64(hdnLanguageID.Value);
            if (HttpUtility.HtmlDecode(txtTitleImage1.Text) == txtTitleImage1.Text)
            {
                cmdImg1.ContentTitle = txtTitleImage1.Text;
            }
            else
            {
                cmdImg1.ContentTitle = txtTitleImage1.Text;
            }
            if (upfImage1.HasFile)
            {
                cmdImg1.ContentImage = upfImage1.PostedFile.FileName.Replace(" ", "").Replace("&", "_");
                upfImage1.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"] + "/AgencyTopImages/" + upfImage1.PostedFile.FileName.Replace(" ", "").Replace("&", "_")));
            }
            cmdImg1.IsActive = true;
            cmd2New.Add(cmdImg1);
            cmdImg2.CmsId = Convert.ToInt64(hdnCmsId.Value);
            cmdImg2.LanguageId = Convert.ToInt64(hdnLanguageID.Value);
            if (HttpUtility.HtmlDecode(txtTitleImage2.Text) == txtTitleImage2.Text)
            {
                cmdImg2.ContentTitle = txtTitleImage2.Text;
            }
            else
            {
                cmdImg2.ContentTitle = txtTitleImage2.Text;
            }
            if (upfImage2.HasFile)
            {
                cmdImg2.ContentImage = upfImage2.PostedFile.FileName.Replace(" ", "").Replace("&", "_");
                upfImage2.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"] + "/AgencyTopImages/" + upfImage2.PostedFile.FileName.Replace(" ", "").Replace("&", "_")));
            }
            cmdImg2.IsActive = true;
            
            cmd2New.Add(cmdImg2);
            cmdImg3.CmsId = Convert.ToInt64(hdnCmsId.Value);
            cmdImg3.LanguageId = Convert.ToInt64(hdnLanguageID.Value);
            if (HttpUtility.HtmlDecode(txtTitleImage3.Text) == txtTitleImage3.Text)
            {
                cmdImg3.ContentTitle = txtTitleImage3.Text;
            }
            else
            {
                cmdImg3.ContentTitle = txtTitleImage3.Text;
            }
            if (upfImage3.HasFile)
            {
                cmdImg3.ContentImage = upfImage3.PostedFile.FileName.Replace(" ", "").Replace("&", "_");
                upfImage3.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["FilePath"] + "/AgencyTopImages/" + upfImage3.PostedFile.FileName.Replace(" ", "").Replace("&", "_")));
            }
            cmdImg3.IsActive = true;
            cmd2New.Add(cmdImg3);
        }
        
        if (cmd2.Count > 0)
        {
            status = manager.updateCMSDescriptionEntity(cmd2New);
        }
        else
        {
            status = manager.saveCMSDescriptionEntity(cmd2New);
        }
        if (status)
        {
            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "succesfuly");
            divmessage.InnerHtml = "The information saved successfully.";
            OnClickDIV.Visible = false;
            SaveStaticPages.Visible = false;
            modifyAgencyControlPanel.Visible = false;
        }
        else
        {
            divmessage.Attributes.Add("class", "error");
            divmessage.Style.Add("display", "block");
            divmessage.InnerHtml = "The information could not be saved.";
        }
        heading = lnkAgencyImage.Text;
        TList<CmsDescription> list = manager.getCmsDescriptionAll(Convert.ToInt64(hdnCmsId.Value), Convert.ToInt32(hdnLanguageID.Value));
        //ViewState["CurrentGrid"] = grvBanner;
        rptAgencyControlPanel.DataSource = list;
        rptAgencyControlPanel.DataBind();
    }
    protected void lnkCancelAgencyAdd_Click(object sender, EventArgs e)
    {
        modifyAgencyControlPanel.Visible = false;
    }
    protected void rptAgencyControlPanel_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            System.Web.UI.WebControls.Image imgControlPanelImage = (System.Web.UI.WebControls.Image)e.Item.FindControl("imgControlPanelImage");
            Label lblControlPanelTitel = (Label)e.Item.FindControl("lblControlPanelTitel");
            CmsDescription c = e.Item.DataItem as CmsDescription;
            if (c != null)
            {
                imgControlPanelImage.ImageUrl = ConfigurationManager.AppSettings["FilePath"] + "/AgencyTopImages/" + c.ContentImage;
                imgControlPanelImage.AlternateText = c.ContentTitle;
                imgControlPanelImage.Width = 200;
                imgControlPanelImage.Height = 200;
                lblControlPanelTitel.Text = HttpUtility.HtmlEncode(c.ContentTitle);
            }
        }
    }
    /// <summary>
    /// This is the event handler for lbtPersonalizedMessage LinkButton click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtPersonalizedMessage_Click(object sender, EventArgs e)
    {
        #region managing control visibility
        divAgencyControlPanelImage.Visible = false;
        fileUploadPDF.Visible = false;
        editor.Visible = true;
        fileUploadImage.Visible = false;
        divpageurl.Visible = false;
        lbtPartners.Visible = false;
        lbtClients.Visible = false;
        gridHotelsOfTheWeek.Visible = false;
        TabbedPanels2.Visible = false;
        titleDiv.Visible = false;
        descriptionTextboxDIV.Visible = false;
        gridBenefits.Visible = false;
        saveCancelBtnDIV.Visible = true;
        gridImageGallery.Visible = false;
        gridNews.Visible = false;
        socialNetworkingDiv.Visible = false;
        TabbedPanels2.Visible = true;
        hotelBanner.Visible = false;
        PublishedDateDIV.Visible = false;
        ViewPreviousFIleDIV.Visible = false;
        OnClickDIV.Visible = true;
        topTitleDIV.Visible = true;
        bottomLinkDropDownListsDIV.Visible = false;
        gridBottomLinks.Visible = false;
        pagingDIV.Visible = false;
        titleHeadingDiv.Visible = true;
        lbtAddNewHotel.Visible = false;
        lbtAddNewImage.Visible = false;
        lbtAddNewNews.Visible = false;
        lbtAddBenefit.Visible = false;
        lbtAddNewBottomLink.Visible = false;
        SaveStaticPages.Visible = false;
        frontStaticPages.Visible = false;
        #endregion

        Cms cmsObj=manager.getCmsEntityOnType("FrontEndWelcomeMessage");
        if (cmsObj != null)
        {
            hdnContentTitle.Value = cmsObj.CmsType;
            hdnCmsId.Value = cmsObj.Id.ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        else
        {
            hdnContentTitle.Value = "FrontEndWelcomeMessage";
            long cmsId = manager.createCmsEntity("FrontEndWelcomeMessage");
            hdnCmsId.Value = cmsId.ToString();
        }
        hdnLanguageID.Value = "1";
        CmsDescription obj = new CmsDescription();
        obj = manager.getCmsDescriptionEntity(Convert.ToInt64(hdnCmsId.Value), Convert.ToInt64(hdnLanguageID.Value), "FrontEndWelcomeMessage");
        if (obj != null)
        {
            ckeEditorEN.Text = obj.ContentsDesc;
        }
        else
        {
            ckeEditorEN.Text = "";
        }
        heading = hdnHeading.Value = lbtPersonalizedMessage.Text;
        LanaguageTabWithoutCountry1.CreateLanguageTab(true);
    }

    /// <summary>
    /// This is the event handler for lbtFrontEndBottomLink LinkButton click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtFrontEndBottomLink_Click(object sender, EventArgs e)
    {
        ViewState["TypeGrd"] = "BottomLink";
        #region managing control visibility
        divAgencyControlPanelImage.Visible = false;
        fileUploadPDF.Visible = false;
        editor.Visible = false;
        fileUploadImage.Visible = false;
        divpageurl.Visible = false;
        lbtPartners.Visible = false;
        lbtClients.Visible = false;
        gridHotelsOfTheWeek.Visible = false;
        TabbedPanels2.Visible = false;
        titleDiv.Visible = false;
        descriptionTextboxDIV.Visible = false;
        gridBenefits.Visible = false;
        saveCancelBtnDIV.Visible = false;
        gridImageGallery.Visible = false;
        gridNews.Visible = false;
        socialNetworkingDiv.Visible = false;
        TabbedPanels2.Visible = false;
        hotelBanner.Visible = false;
        PublishedDateDIV.Visible = false;
        ViewPreviousFIleDIV.Visible = false;
        OnClickDIV.Visible = false;
        topTitleDIV.Visible = true;
        gridBottomLinks.Visible = true;
        calender.Visible = false;
        bottomLinkDropDownListsDIV.Visible = false;
        pagingDIV.Visible = false;
        titleHeadingDiv.Visible = true;
        lbtAddNewHotel.Visible = false;
        lbtAddNewImage.Visible = false;
        lbtAddNewNews.Visible = false;
        lbtAddBenefit.Visible = false;
        
        SaveStaticPages.Visible = false;
        frontStaticPages.Visible = false;
        lbtAddNewBottomLink.Visible = true;
        #endregion

        Cms cmsObj = manager.getCmsEntityOnType("BottomPart");
        if (cmsObj != null)
        {
            hdnContentTitle.Value = cmsObj.CmsType;
            hdnCmsId.Value = cmsObj.Id.ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        else
        {
            hdnContentTitle.Value = "BottomPart";
            hdnCmsId.Value = manager.createCmsEntity("BottomPart").ToString();
        }
        hdnLanguageID.Value = "1";
        hdnRecordID.Value = hdnCmsId.Value;
        heading = hdnHeading.Value = lbtFrontEndBottomLink.Text;

        #region Bind The Grid

        TList<CmsDescription> list = manager.getCmsDescriptionAll(Convert.ToInt64(hdnCmsId.Value),1);
        //ViewState["CurrentGrid"] = grvBottomLinks;
        grvBottomLinks.DataSource = list;
        grvBottomLinks.DataBind();
        ApplyPaging("BottomLink");

        #endregion

    }

    /// <summary>
    /// This is the event handler for lbtBookingAdvantage LinkButton click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtBookingAdvantage_Click(object sender, EventArgs e)
    {
        #region managing control visibility
        divAgencyControlPanelImage.Visible = false;
        editor.Visible = true;
        fileUploadPDF.Visible = false;
        fileUploadImage.Visible = false;
        divpageurl.Visible = false;
        lbtPartners.Visible = false;
        lbtClients.Visible = false;
        gridHotelsOfTheWeek.Visible = false;
        TabbedPanels2.Visible = true;
        titleDiv.Visible = false;
        descriptionTextboxDIV.Visible = false;
        gridBenefits.Visible = false;
        saveCancelBtnDIV.Visible = true;
        gridImageGallery.Visible = false;
        gridNews.Visible = false;
        socialNetworkingDiv.Visible = false;
        hotelBanner.Visible = false;
        PublishedDateDIV.Visible = false;
        ViewPreviousFIleDIV.Visible = false;
        OnClickDIV.Visible = true;
        topTitleDIV.Visible = true;
        bottomLinkDropDownListsDIV.Visible = false;
        gridBottomLinks.Visible = false;
        pagingDIV.Visible = false;
        titleHeadingDiv.Visible = true;
        lbtAddNewHotel.Visible = false;
        lbtAddNewImage.Visible = false;
        lbtAddNewNews.Visible = false;
        lbtAddBenefit.Visible = false;
        lbtAddNewBottomLink.Visible = false;
        SaveStaticPages.Visible = false;
        frontStaticPages.Visible = false;
        #endregion

        heading = hdnHeading.Value = lbtBookingAdvantage.Text;
        Cms cmsObj = manager.getCmsEntityOnType("BookingAdvantage");
        if (cmsObj != null)
        {
            hdnContentTitle.Value = cmsObj.CmsType;
            hdnCmsId.Value = cmsObj.Id.ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        else
        {
            hdnContentTitle.Value = "BookingAdvantage";
            hdnCmsId.Value = manager.createCmsEntity("BookingAdvantage").ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        hdnLanguageID.Value = "1";
        LanaguageTabWithoutCountry1.CreateLanguageTab(true);
        string staticText = manager.getCmsDescription(Convert.ToInt64(hdnCmsId.Value), 1);
        ckeEditorEN.Text = staticText;
    }

    /// <summary>
    /// This is the event handler for lbtRequestAdvantage LinkButton click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtRequestAdvantage_Click(object sender, EventArgs e)
    {
        #region managing control visibility
        divAgencyControlPanelImage.Visible = false;
        editor.Visible = true;
        fileUploadPDF.Visible = false;
        fileUploadImage.Visible = false;
        divpageurl.Visible = false;
        lbtPartners.Visible = false;
        lbtClients.Visible = false;
        gridHotelsOfTheWeek.Visible = false;
        TabbedPanels2.Visible = true;
        titleDiv.Visible = false;
        descriptionTextboxDIV.Visible = false;
        gridBenefits.Visible = false;
        saveCancelBtnDIV.Visible = true;
        gridImageGallery.Visible = false;
        gridNews.Visible = false;
        socialNetworkingDiv.Visible = false;
        hotelBanner.Visible = false;
        PublishedDateDIV.Visible = false;
        ViewPreviousFIleDIV.Visible = false;
        OnClickDIV.Visible = true;
        topTitleDIV.Visible = true;
        bottomLinkDropDownListsDIV.Visible = false;
        gridBottomLinks.Visible = false;
        pagingDIV.Visible = false;
        titleHeadingDiv.Visible = true;
        lbtAddNewHotel.Visible = false;
        lbtAddNewImage.Visible = false;
        lbtAddNewNews.Visible = false;
        lbtAddBenefit.Visible = false;
        lbtAddNewBottomLink.Visible = false;
        SaveStaticPages.Visible = false;
        frontStaticPages.Visible = false;
        #endregion

        heading = hdnHeading.Value = lbtRequestAdvantage.Text;
        Cms cmsObj = manager.getCmsEntityOnType("RequestAdvantage");
        if (cmsObj != null)
        {
            hdnContentTitle.Value = cmsObj.CmsType;
            hdnCmsId.Value = cmsObj.Id.ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        else
        {
            hdnContentTitle.Value = "RequestAdvantage";
            hdnCmsId.Value = manager.createCmsEntity("RequestAdvantage").ToString();
            hdnRecordID.Value = hdnCmsId.Value;
        }
        hdnLanguageID.Value = "1";
        LanaguageTabWithoutCountry1.CreateLanguageTab(true);
        string staticText = manager.getCmsDescription(Convert.ToInt64(hdnCmsId.Value), 1);
        ckeEditorEN.Text = staticText;
    }

    #region GridEvents

    /// <summary>
    /// The rowDataBound of grvHotelsOfTheWeek GridView.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void grvHotelsOfTheWeek_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
        {
            GridViewRow gr = e.Row;
            LinkButton lbtID = (LinkButton)gr.FindControl("lbtID");
            Image imgHotel = (Image)gr.FindControl("imgHotel");
            Label lblDate = (Label)gr.FindControl("lblDate");
            ImageButton imgBtn = (ImageButton)gr.FindControl("imgBtnActivateHotel");
            imgHotel.ImageUrl = "~/Uploads/HotelOfTheWeek/" + DataBinder.Eval(e.Row.DataItem, "ContentImage");
            Label lblTitleBody = (Label)gr.FindControl("lblTitleBody");
            string lbl = DataBinder.Eval(e.Row.DataItem,"ContentTitle").ToString();
            string [] strArray = Regex.Split(lbl, ",");
            lblTitleBody.Text = System.Net.WebUtility.HtmlDecode(strArray[0]);
            CmsDescription obj = manager.getCmsDescriptionOnID(Convert.ToInt64(imgBtn.ToolTip));
            if (obj != null)
            {
                if (obj.IsActive == true)
                {
                    imgBtn.ImageUrl = "~/images/check.png";
                }
                else
                {
                    imgBtn.ImageUrl = "~/images/Close2.png";
                }
            }
        }
    }

    /// <summary>
    /// The rowDataBound of grvBenefits GridView.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void grvBenefits_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
        {
            GridViewRow gr = e.Row;
            Image imgHotelBenefits = (Image)gr.FindControl("imgHotelBenefits");
            imgHotelBenefits.ImageUrl = "~/Images/" + DataBinder.Eval(e.Row.DataItem, "ContentImage");

            ImageButton imgBtn = (ImageButton)gr.FindControl("imgBtnActivateBenefit");


            CmsDescription obj = manager.getCmsDescriptionOnID(Convert.ToInt64(imgBtn.ToolTip));
            if (obj != null)
            {
                if (obj.IsActive == true)
                {
                    imgBtn.ImageUrl = "~/images/check.png";
                }
                else
                {
                    imgBtn.ImageUrl = "~/images/Close2.png";
                }
            }
        }
    }

    /// <summary>
    /// The rowDataBound of grvImageGallery GridView.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void grvImageGallery_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
        {
            GridViewRow gr = e.Row;
            Image imgGallery = (Image)gr.FindControl("imgGallery");
            imgGallery.ImageUrl = "~/Uploads/FrontImages/" + DataBinder.Eval(e.Row.DataItem, "ContentImage");
            ImageButton imgBtn = (ImageButton)gr.FindControl("imgBtnActivateImage");
            Label lblImageTitleText = (Label)gr.FindControl("lblImageTitleText");
            string lbl = DataBinder.Eval(e.Row.DataItem,"ContentTitle").ToString();
            string [] strArray = Regex.Split(lbl, ",");
            lblImageTitleText.Text = strArray[0];
            CmsDescription obj = manager.getCmsDescriptionOnID(Convert.ToInt64(imgBtn.CommandArgument));
            if (obj != null)
            {
                if (obj.IsActive == true)
                {
                    imgBtn.ImageUrl = "~/images/check.png";
                }
                else
                {
                    imgBtn.ImageUrl = "~/images/Close2.png";
                }
            }
        }
    }

    /// <summary>
    /// The rowDataBound of grvBanner GridView.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void grvBanner_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
        {
            GridViewRow gr = e.Row;
            Image imgBanner = (Image)gr.FindControl("imgBanner");
            CmsDescription obj = new CmsDescription();
            imgBanner.ImageUrl = "~/Uploads/FrontImages/" + DataBinder.Eval(e.Row.DataItem, "ContentImage");
        }
    }

    /// <summary>
    /// The rowDataBound of grvNews GridView.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void grvNews_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
        {
            GridViewRow gr = e.Row;

            ImageButton imgBtn = (ImageButton)gr.FindControl("imgBtnActivateNews");


            CmsDescription obj = manager.getCmsDescriptionOnID(Convert.ToInt64(imgBtn.ToolTip));
            if (obj != null)
            {
                if (obj.IsActive == true)
                {
                    imgBtn.ImageUrl = "~/images/check.png";
                }
                else
                {
                    imgBtn.ImageUrl = "~/images/Close2.png";
                }
            }
        }
    }

    /// <summary>
    /// The rowCommand of grvHotelsOfTheWeek.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void grvHotelsOfTheWeek_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        bool status = false;
        if (e.CommandName == "Modify")
        {
            TList<CmsDescription> subjectList = manager.getAllCmsDescriptionsOnTitle(e.CommandArgument.ToString());
            if (subjectList.Count != 0)
            {
                if (subjectList[0].IsActive)
                {
                    foreach (CmsDescription desc in subjectList)
                    {
                        desc.IsActive = false;
                    }
                }
                else
                {
                    foreach (CmsDescription desc in subjectList)
                    {
                        desc.IsActive = true;
                    }
                }
                foreach (CmsDescription desc in subjectList)
                {
                    status = manager.updateCMSDescriptionEntity(desc);
                }
                if (status)
                {
                    divmessage.Style.Add("display", "block");
                    divmessage.Attributes.Add("class", "succesfuly");
                    string message = subjectList[0].IsActive ? "Activated" : "Deactivated";
                    divmessage.InnerHtml = "Hotel has been " + message;
                }

                #region Bind The Grid

                TList<CmsDescription> list = manager.getCmsDescriptionAll(Convert.ToInt64(hdnCmsId.Value),1);
                grvHotelsOfTheWeek.DataSource = list;
                grvHotelsOfTheWeek.DataBind();
                ApplyPaging("HotelOfTheWeek");

                #endregion
            }
        }
    }

    /// <summary>
    /// The rowCommand of grvBenefits.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void grvBenefits_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        bool status = false;
        if (e.CommandName == "Modify")
        {
            TList<CmsDescription> subjectList = manager.getAllCmsDescriptionsOnTitle(e.CommandArgument.ToString());
            if (subjectList.Count != 0)
            {
                if (subjectList[0].IsActive)
                {
                    foreach (CmsDescription desc in subjectList)
                    {
                        desc.IsActive = false;
                    }
                }
                else
                {
                    foreach (CmsDescription desc in subjectList)
                    {
                        desc.IsActive = true;
                    }
                }
                foreach (CmsDescription desc in subjectList)
                {
                    status = manager.updateCMSDescriptionEntity(desc);
                }
                if (status)
                {
                    divmessage.Style.Add("display", "block");
                    divmessage.Attributes.Add("class", "succesfuly");
                    string message = subjectList[0].IsActive ? "Activated" : "Deactivated";
                    divmessage.InnerHtml = "Benefit has been " + message;
                }

                #region Bind The Grid

                TList<CmsDescription> list = manager.getCmsDescriptionAll(Convert.ToInt64(hdnCmsId.Value),1);
                grvBenefits.DataSource = list;
                grvBenefits.DataBind();
                ApplyPaging("Benifites");

                #endregion
            }
        }
    }

    /// <summary>
    /// The rowCommand of grvNews.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void grvNews_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        bool status = false;
        if (e.CommandName == "Modify")
        {
            TList<CmsDescription> subjectList = manager.getAllCmsDescriptionsOnTitle(e.CommandArgument.ToString());
            if (subjectList.Count != 0)
            {
                if (subjectList[0].IsActive)
                {
                    foreach (CmsDescription desc in subjectList)
                    {
                        desc.IsActive = false;
                    }
                }
                else
                {
                    foreach (CmsDescription desc in subjectList)
                    {
                        desc.IsActive = true;
                    }
                }
                foreach (CmsDescription desc in subjectList)
                {
                    status = manager.updateCMSDescriptionEntity(desc);
                }
                if (status)
                {
                    divmessage.Style.Add("display", "block");
                    divmessage.Attributes.Add("class", "succesfuly");
                    string message = subjectList[0].IsActive ? "Activated" : "Deactivated";
                    divmessage.InnerHtml = "News has been " + message;
                }

                #region Bind The Grid

                TList<CmsDescription> list = manager.getCmsDescriptionAll(Convert.ToInt64(hdnCmsId.Value),1);
                grvNews.DataSource = list;
                grvNews.DataBind();
                ApplyPaging("News");

                #endregion
            }
        }
    }

    /// <summary>
    /// The rowCommand of grvImageGallery.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void grvImageGallery_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Modify")
        {
            CmsDescription obj = manager.getCmsDescriptionOnID(Convert.ToInt64(e.CommandArgument));
            if (obj != null)
            {
                if (obj.IsActive)
                {
                    obj.IsActive = false;
                }
                else
                {
                    obj.IsActive = true;
                }
                bool status = manager.updateCMSDescriptionEntity(obj);
                if (status)
                {
                    divmessage.Style.Add("display", "block");
                    divmessage.Attributes.Add("class", "succesfuly");
                    string message = obj.IsActive ? "Activated" : "Deactivated";
                    divmessage.InnerHtml = "Image has been " + message;
                }
                #region Bind The Grid

                TList<CmsDescription> list = manager.getCmsDescriptionAll(Convert.ToInt64(hdnCmsId.Value),1);
                grvImageGallery.DataSource = list;
                grvImageGallery.DataBind();
                ApplyPaging("ImageGallery");

                #endregion
            }
        }
    }

    /// <summary>
    /// The rowCommand of grvBottomLinks.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void grvBottomLinks_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        bool status=false;
        if (e.CommandName == "Modify")
        {
            TList<CmsDescription> subjectList = manager.getAllCmsDescriptionsOnTitle(e.CommandArgument.ToString());
            if (subjectList.Count != 0)
            {
                if (subjectList[0].IsActive)
                {
                    foreach (CmsDescription desc in subjectList)
                    {
                        desc.IsActive = false;
                    }
                }
                else
                {
                    foreach (CmsDescription desc in subjectList)
                    {
                        desc.IsActive = true;
                    }
                }
                foreach (CmsDescription desc in subjectList)
                {
                    status = manager.updateCMSDescriptionEntity(desc);
                }
                if (status)
                {
                    divmessage.Style.Add("display", "block");
                    divmessage.Attributes.Add("class", "succesfuly");
                    string message = subjectList[0].IsActive ? "Activated" : "Deactivated";
                    divmessage.InnerHtml = "BottomLink has been " + message;
                }

                #region Bind The Grid

                TList<CmsDescription> list = manager.getCmsDescriptionAll(Convert.ToInt64(hdnCmsId.Value),1);
                grvBottomLinks.DataSource = list;
                grvBottomLinks.DataBind();
                ApplyPaging("BottomLink");

                #endregion
            }
        }
    }

    /// <summary>
    /// The rowDataBound of grvBottomLinks GridView.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void grvBottomLinks_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
        {
            GridViewRow gr = e.Row;
            ImageButton imgBtn = (ImageButton)gr.FindControl("imgBtnActivateLink");
            CmsDescription obj = manager.getCmsDescriptionOnID(Convert.ToInt64(imgBtn.ToolTip));
            if (obj != null)
            {
                if (obj.IsActive == true)
                {
                    imgBtn.ImageUrl = "~/images/check.png";
                }
                else
                {
                    imgBtn.ImageUrl = "~/images/Close2.png";
                }
            }
        }
    }

    #endregion

    /// <summary>
    /// This the event handler for lbtEdit LinkButton Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtEdit_Click(object sender, EventArgs e)
    {
        LinkButton lbtEdit = (LinkButton)sender;
        editor.Visible = true;
        fileUploadImage.Visible = true;
        divpageurl.Visible = false;
        titleDiv.Visible = true;
        TabbedPanels2.Visible = true;
        saveCancelBtnDIV.Visible = true;
        OnClickDIV.Visible = true;
        titleHeadingDiv.Visible = true;
        hdnLanguageID.Value = "1";
        hdnUniqueId.Value = lbtEdit.ToolTip;
        hdnTask.Value = "Edit";
        CmsDescription obj = manager.getCmsDescriptionOnID(Convert.ToInt64(hdnUniqueId.Value));
        if (obj != null)
        {
            hdnContentTitle.Value = obj.ContentTitle;
            hdnRecordID.Value = obj.CmsId.ToString();
            ckeEditorEN.Text = obj.ContentsDesc;
            txtTitle.Text = System.Net.WebUtility.HtmlDecode(Regex.Split(obj.ContentTitle, ",")[0]);
            calExTO.SelectedDate = obj.FromDate;
            CalendarExtender2.SelectedDate = obj.ToDate;
        }
        txtTitle.ReadOnly = false;
        LanaguageTabWithoutCountry1.CreateLanguageTab(true);
        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "SetFocus();", true);
    }

    /// <summary>
    /// This the event handler for lbtEditDesc LinkButton Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtEditDesc_Click(object sender, EventArgs e)
    {
        LinkButton lbtEditDesc = (LinkButton)sender;
        fileUploadImage.Visible = true;
        divpageurl.Visible = false;
        TabbedPanels2.Visible = true;
        descriptionTextboxDIV.Visible = true;
        saveCancelBtnDIV.Visible = true;
        OnClickDIV.Visible = true;
        titleHeadingDiv.Visible = true;
        hdnLanguageID.Value = "1";
        hdnUniqueId.Value = lbtEditDesc.ToolTip;
        hdnTask.Value = "Edit";
        CmsDescription obj = manager.getCmsDescriptionOnID(Convert.ToInt64(hdnUniqueId.Value));
        if (obj != null)
        {
            hdnContentTitle.Value = obj.ContentTitle;
            hdnRecordID.Value = obj.CmsId.ToString();
            LanaguageTabWithoutCountry1.CreateLanguageTab(true);
            txtDescription.Text = obj.ContentShortDesc;
        }
        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "SetFocus();", true);
    }

    /// <summary>
    /// This the event handler for lbtEditNews LinkButton Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtEditNews_Click(object sender, EventArgs e)
    {
        LinkButton lbtEditNews = (LinkButton)sender;
        editor.Visible = true;
        TabbedPanels2.Visible = true;
        saveCancelBtnDIV.Visible = true;
        hdnLanguageID.Value = "1";
        fileUploadPDF.Visible = true;
        divpageurl.Visible = false;
        PublishedDateDIV.Visible = true;
        titleHeadingDiv.Visible = true;
        OnClickDIV.Visible = true;
        hdnUniqueId.Value = lbtEditNews.ToolTip;
        hdnTask.Value = "Edit";
        CmsDescription obj = manager.getCmsDescriptionOnID(Convert.ToInt64(hdnUniqueId.Value));
        if (obj != null)
        {
            txtPublishedDate.Text = "";
            hdnContentTitle.Value = obj.ContentTitle;
            hdnRecordID.Value = obj.CmsId.ToString();
            ckeEditorEN.Text = obj.ContentsDesc;
            CalendarExtender1.SelectedDate = obj.UpdatedDate;
            txtPublishedDate.Text = String.Format("{0:dd/MM/yyyy}", obj.UpdatedDate);
        }
        LanaguageTabWithoutCountry1.CreateLanguageTab(true);
        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "SetFocus();", true);
    }

    /// <summary>
    /// This the event handler for lbtEditImage LinkButton Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtEditImage_Click(object sender, EventArgs e)
    {
        LinkButton lbtEditImage = (LinkButton)sender;
        saveCancelBtnDIV.Visible = true;
        editor.Visible = false;
        titleDiv.Visible = true;
        calender.Visible = false;
        fileUploadImage.Visible = true;
        divpageurl.Visible = true;
        txtTitle.ReadOnly = false;
        OnClickDIV.Visible = true;
        titleHeadingDiv.Visible = true;
        hdnLanguageID.Value = "1";
        hdnUniqueId.Value = lbtEditImage.ToolTip;
        hdnTask.Value = "Edit";
        CmsDescription obj = manager.getCmsDescriptionOnID(Convert.ToInt64(hdnUniqueId.Value));
        if (obj != null)
        {
            hdnContentTitle.Value = obj.ContentTitle;
            hdnRecordID.Value = obj.CmsId.ToString();
            txturl.Text = obj.PageUrl;
            ckeEditorEN.Text = obj.ContentsDesc;
            txtTitle.Text = System.Net.WebUtility.HtmlDecode(Regex.Split(obj.ContentTitle, ",")[0]);
        }
        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "SetFocus();", true);
    }

    /// <summary>
    /// This the event handler for lbtEditLink LinkButton Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtEditLink_Click(object sender, EventArgs e)
    {
        LinkButton lbtEditLink = (LinkButton)sender;
        TabbedPanels2.Visible = true;
        saveCancelBtnDIV.Visible = true;
        OnClickDIV.Visible = true;
        bottomLinkDropDownListsDIV.Visible = true;
        divMetaDescription.Visible = false;
        titleHeadingDiv.Visible = true;
        hdnLanguageID.Value = "1";
        hdnUniqueId.Value = lbtEditLink.ToolTip;
        CmsDescription descObj = manager.getCmsDescriptionOnID(Convert.ToInt64(hdnUniqueId.Value));
        if (descObj != null)
        {
            hdnContentTitle.Value = descObj.ContentTitle;
            hdnRecordID.Value = descObj.CmsId.ToString();
            txtContentDesc.Text = descObj.ContentsDesc;
            txtKeywords.Text = descObj.MetaKeyword;
            txtMetaDesc.Text = descObj.MetaDescription;
            string pageUrl = descObj.PageUrl;
            string[] urlFragments = Regex.Split(pageUrl, "/");
            TList<City> cityList = null;

            TList<Country> countryList = obj.GetByAllCountrycms();
            TList<Zone> zoneList = null;
            ddlCountry.DataSource = countryList;
            ddlCountry.DataTextField = "CountryName";
            ddlCountry.DataValueField = "Id";
            ddlCountry.DataBind();
            if (urlFragments.Count() != 0)
            {
                ListItem toBeSelected = FindByTextCaseInsensitive(ddlCountry, urlFragments[1]);
                toBeSelected.Selected = true;
                cityList = obj.GetCityByCountry(Convert.ToInt32(toBeSelected.Value)).FindAll(a => a.IsActive == true);
                ddlCity.DataSource = cityList;
                ddlCity.DataTextField = "City";
                ddlCity.DataValueField = "id";
                ddlCity.DataBind();
                if (cityList.Count == 0)
                {
                    ddlCity.Items.Insert(0, new ListItem("Select City", "0"));
                }
                ListItem toBeSelectedCity = FindByTextCaseInsensitive(ddlCity, urlFragments[2]);
                if (toBeSelectedCity != null)
                {
                    toBeSelectedCity.Selected = true;
                }
                if (toBeSelectedCity != null)
                {
                    zoneList = manager.getZonesByCityId(Convert.ToInt64(toBeSelectedCity.Value)).FindAll(a => a.IsActive == true);
                    ddlZone.DataSource = zoneList;
                    ddlZone.DataTextField = "Zone";
                    ddlZone.DataValueField = "Id";
                    ddlZone.DataBind();
                }
                if (urlFragments.Count() > 3)
                {
                    ListItem toBeSelectedZone = FindByTextCaseInsensitive(ddlZone, urlFragments[3]);
                    if (toBeSelectedZone != null)
                    {
                        ddlZone.Items.Insert(0, new ListItem("Select Zone", "0"));
                        toBeSelectedZone.Selected = true;
                    }
                    else
                    {
                        ddlZone.Items.Insert(0,new ListItem("Select Zone", "0"));
                        ddlZone.SelectedIndex = 0;
                    }
                }
                else
                {
                    ddlZone.Items.Insert(0,new ListItem("Select Zone", "0"));
                    ddlZone.SelectedIndex = 0;
                }
            }
        }
        LanaguageTabWithoutCountry1.CreateLanguageTab(true);
        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "SetFocus();", true);
    }

    /// <summary>
    /// This the event handler for lbtAddNew LinkButton Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtAddNew_Click(object sender, EventArgs e)
    {
        fileUploadImage.Visible = true;
        divpageurl.Visible = false;
        OnClickDIV.Visible = true;
        saveCancelBtnDIV.Visible = true;
        titleHeadingDiv.Visible = true;
    }

    /// <summary>
    /// This the event handler for lbtAddNewImage LinkButton Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtAddNewImage_Click(object sender, EventArgs e)
    {
        saveCancelBtnDIV.Visible = true;
        editor.Visible = true;
        titleDiv.Visible = true;
        calender.Visible = false;
        fileUploadImage.Visible = true;
        divpageurl.Visible = true;
        txtTitle.ReadOnly = false;
        OnClickDIV.Visible = true;
        titleHeadingDiv.Visible = true;
        hdnContentTitle.Value = "0";
        txtTitle.Text = "";
        txturl.Text = "";
        ckeEditorEN.Text = "";
        hdnTask.Value = "New";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "SetFocus();", true);
    }

    /// <summary>
    /// This the event handler for lbtAddNewHotel LinkButton Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtAddNewHotel_Click(object sender, EventArgs e)
    {
        editor.Visible = true;
        fileUploadImage.Visible = true;
        divpageurl.Visible = false;
        titleDiv.Visible = true;
        TabbedPanels2.Visible = true;
        saveCancelBtnDIV.Visible = true;
        txtTitle.ReadOnly = false;
        OnClickDIV.Visible = true;
        titleHeadingDiv.Visible = true;
        ckeEditorEN.Text = "";
        txtTitle.Text = "";
        calExTO.SelectedDate = DateTime.Now.Date;
        CalendarExtender2.SelectedDate = DateTime.Now.Date;
        hdnContentTitle.Value = "0";
        hdnTask.Value = "New";
        LanaguageTabWithoutCountry1.CreateLanguageTab(true);
        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "SetFocus();", true);
    }

    /// <summary>
    /// This the event handler for lbtAddBenefit LinkButton Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtAddBenefit_Click(object sender, EventArgs e)
    {
        fileUploadImage.Visible = true;
        divpageurl.Visible = false;
        TabbedPanels2.Visible = true;
        descriptionTextboxDIV.Visible = true;
        saveCancelBtnDIV.Visible = true;
        OnClickDIV.Visible = true;
        titleHeadingDiv.Visible = true;
        hdnContentTitle.Value = "0";
        txtDescription.Text = "";
        hdnLanguageID.Value = "1";
        hdnTask.Value = "New";
        LanaguageTabWithoutCountry1.CreateLanguageTab(true);
        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "SetFocus();", true);
    }

    /// <summary>
    /// This the event handler for lbtAddNewNews LinkButton Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtAddNewNews_Click(object sender, EventArgs e)
    {
        editor.Visible = true;
        TabbedPanels2.Visible = true;
        saveCancelBtnDIV.Visible = true;
        fileUploadPDF.Visible = true;
        PublishedDateDIV.Visible = true;
        OnClickDIV.Visible = true;
        titleHeadingDiv.Visible = true;
        hdnLanguageID.Value = "1";
        hdnContentTitle.Value = "0";
        ckeEditorEN.Text = "";
        hdnTask.Value = "New";
        CalendarExtender1.SelectedDate = DateTime.Now.Date;
        LanaguageTabWithoutCountry1.CreateLanguageTab(true);
        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "SetFocus();", true);
    }

    /// <summary>
    /// This the event handler for lbtAddNewBottomLink LinkButton Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtAddNewBottomLink_Click(object sender, EventArgs e)
    {
        TabbedPanels2.Visible = true;
        bottomLinkDropDownListsDIV.Visible = true;
        divMetaDescription.Visible = false;
        saveCancelBtnDIV.Visible = true;
        OnClickDIV.Visible = true;
        titleHeadingDiv.Visible = true;
        hdnContentTitle.Value = "0";
        TList<City> cityList = null;
        TList<Country> countryList = obj.GetByAllCountrycms();
        TList<Zone> zoneList = null;
        //bind country DropDownList
        ddlCountry.DataSource = countryList;
        ddlCountry.DataTextField = "CountryName";
        ddlCountry.DataValueField = "Id";
        ddlCountry.DataBind();
        //bind city DropDownList
        if (ddlCountry.SelectedItem != null)
        {
            cityList = obj.GetCityByCountry(Convert.ToInt32(ddlCountry.SelectedItem.Value)).FindAll(a => a.IsActive == true);
            ddlCity.DataSource = cityList;
            ddlCity.DataTextField = "City";
            ddlCity.DataValueField = "Id";
            ddlCity.DataBind();
            if (cityList.Count == 0)
            {
                ddlCity.Items.Insert(0, new ListItem("Select City", "0"));
            }
        }
        //bind zone DropDownList
        if (ddlCity.SelectedItem != null)
        {
            zoneList = obj.GetZonesByCityID(Convert.ToInt64(ddlCity.SelectedItem.Value)).FindAll(a => a.IsActive == true);
            ddlZone.DataSource = zoneList;
            ddlZone.DataTextField = "Zone";
            ddlZone.DataValueField = "Id";
            ddlZone.DataBind();
            ddlZone.Items.Insert(0, new ListItem("Select Zone", "0"));
        }
        //reset all controls
        txtContentDesc.Text = "";
        txtKeywords.Text = "";
        txtMetaDesc.Text = "";
        LanaguageTabWithoutCountry1.CreateLanguageTab(true);
        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "SetFocus();", true);
    }

    /// <summary>
    /// This the event handler for lbtBannerImage LinkButton Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtBannerImage_Click(object sender, EventArgs e)
    {
        OnClickDIV.Visible = true;
        fileUploadImage.Visible = true;
        divpageurl.Visible = false;
        saveCancelBtnDIV.Visible = true;
        titleHeadingDiv.Visible = true;
        hdnTask.Value = "New";
        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg2", "SetFocus();", true);
    }

    /// <summary>
    /// This the event handler for btnLanguage Button Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void btnLanguage_Click(object sender, EventArgs e)
    {
        string filepath;
        CmsDescription entity = manager.getCmsDescriptionEntity(Convert.ToInt64(hdnCmsId.Value == "" || hdnCmsId.Value == null ? "0" : hdnCmsId.Value), Convert.ToInt32(hdnLanguageID.Value == "" || hdnLanguageID.Value == null ? "0" : hdnLanguageID.Value), hdnContentTitle.Value);
        if (ViewState["TypeGrd"] != null)
        {
            if (ViewState["TypeGrd"].ToString() == "BottomLink")
            {
                txtContentDesc.Text = entity == null ? "" : entity.ContentShortDesc;
            }
        }
        ckeEditorEN.Text = entity == null ? "" : entity.ContentsDesc;
        txtDescription.Text = entity == null ? "" : entity.ContentShortDesc;
        txtTitle.Text = System.Net.WebUtility.HtmlDecode(entity == null ? "" : Regex.Split(entity.ContentTitle, ",")[0]);
        bool benefitsAndNotEnglish = (hdnCmsId.Value == "21" && hdnLanguageID.Value != "1");
        bool benefitsAndEnglish = (hdnCmsId.Value == "21" && hdnLanguageID.Value == "1");
        bool hotelOfWeekAndNotEnglish = (hdnCmsId.Value == "3" && hdnLanguageID.Value != "1");
        bool hotelOfWeekAndEnglish = (hdnCmsId.Value == "3" && hdnLanguageID.Value == "1");
        
        if (benefitsAndNotEnglish || hotelOfWeekAndNotEnglish)
        {
            // Hiding image upload control  in case of benefits.hotel of the week and language other than english.
            fileUploadImage.Visible = false;
            titleDiv.Visible = false;
        }
        else if (benefitsAndEnglish || hotelOfWeekAndEnglish)
        {
            fileUploadImage.Visible = true;
            if (hotelOfWeekAndEnglish)
            {
                titleDiv.Visible = true;
            }
        }
        if (TabbedPanels2.Visible == true)
        {
            if (ViewState["TypeGrd"] != null)
            {
                if (ViewState["TypeGrd"].ToString().Trim().ToLower() == "Front Static Pages".Trim().ToLower())
                {
                    LanaguageTabWithoutCountry1.CreateLanguageThree(true);
                }
                else
                {
                    LanaguageTabWithoutCountry1.CreateLanguageTab(true);
                }
            }
            else
            {
                LanaguageTabWithoutCountry1.CreateLanguageTab(true);
            }
        }
        if (entity == null)
        {
            ViewPreviousFIleDIV.Visible = false;
            CalendarExtender1.SelectedDate = DateTime.Now.Date;
        }
        else
        {
            ViewPreviousFIleDIV.Visible = true;
            CalendarExtender1.SelectedDate = entity.UpdatedDate;
            if (entity.PageUrl != null)
            {
                filepath = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Replace("~/", "") + "MediaFile/" + entity.PageUrl.ToString();
                lbtViewCurrentFile.OnClientClick = "return open_win('" + filepath + "')";
            }
        }
        if (gridHotelsOfTheWeek.Visible == true || gridBenefits.Visible == true || gridImageGallery.Visible == true || gridNews.Visible == true || gridBottomLinks.Visible == true)
        {
            pagingDIV.Visible = true;
        }
        else
        {
            pagingDIV.Visible = false;
        }
        if (ViewState["TypeGrd"] != null)
        {
            if (ViewState["TypeGrd"].ToString().Trim().ToLower() == "Front Static Pages".Trim().ToLower())
            {
                if (entity != null)
                {
                    txtTitle.Text = entity.ContentShortDesc == null ? "" : entity.ContentShortDesc;
                    txtMetaDesc.Text = entity.MetaDescription == null ? "" : entity.MetaDescription;
                    txtKeywords.Text = entity.MetaKeyword == null ? "" : entity.MetaKeyword;
                }
                else
                {
                    txtTitle.Text = hdnContentTitle.Value;
                    txtMetaDesc.Text = "";
                    txtKeywords.Text = "";
                }
            }
        }
        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg2", "SetFocus();", true);
    }

    /// <summary>
    /// This the event handler for SocialSitesDDL DropDownList SelectedIndexChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void SocialSitesDDL_SelectedIndexChanged(object sender, EventArgs e)
    {
        CmsDescription obj = manager.getCmsDescriptionOnID(Convert.ToInt64(SocialSitesDDL.SelectedItem.Value));
        pagingDIV.Visible = false;
        if (obj != null)
        {
            hdnContentTitle.Value = obj.ContentTitle;
            hdnRecordID.Value = obj.CmsId.ToString();
        }
        string path = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Replace("~/", "") + "SocialImage/" + obj.ContentImage;
        txtPageUrl.Text = obj.PageUrl;
        imgIcon.ImageUrl = path;
    }

    /// <summary>
    /// The event handler for the SelectedIndexChanged event of ddlCountry DDL.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        TList<City> cityList = null;
        if (ddlCountry.SelectedItem != null)
        {
            cityList = obj.GetCityByCountry(Convert.ToInt32(ddlCountry.SelectedItem.Value)).FindAll(a => a.IsActive == true);
            ddlCity.DataSource = cityList;
            ddlCity.DataTextField = "City";
            ddlCity.DataValueField = "id";
            ddlCity.DataBind();
            if (cityList.Count == 0)
            {
                ddlCity.Items.Insert(0, new ListItem("Select City", "0"));
            }
        }
        if (ddlCity.SelectedItem != null)
        {
            TList<Zone> zoneList = manager.getZonesByCityId(Convert.ToInt64(ddlCity.SelectedItem.Value)).FindAll(a => a.IsActive == true);
            ddlZone.DataSource = zoneList;
            ddlZone.DataTextField = "Zone";
            ddlZone.DataValueField = "Id";
            ddlZone.DataBind();
            ddlZone.Items.Insert(0, new ListItem("Select Zone", "0"));
        }
        else
        {
            ddlZone.Items.Insert(0, new ListItem("Select Zone", "0"));
        }
        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "SetFocus();", true);
    }

    /// <summary>
    /// The event handler for the SelectedIndexChanged event of ddlCountry DDL.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCity.SelectedItem != null)
        {
            TList<Zone> zoneList = manager.getZonesByCityId(Convert.ToInt64(ddlCity.SelectedItem.Value)).FindAll(a => a.IsActive == true);
            ddlZone.DataSource = zoneList;
            ddlZone.DataTextField = "Zone";
            ddlZone.DataValueField = "Id";
            ddlZone.DataBind();
            ddlZone.Items.Insert(0, new ListItem("Select Zone", "0"));
        }
        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "SetFocus();", true);
    }

    /// <summary>
    /// This is the event handler for lbtDelete CheckBox CheckedChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtDelete_Click(object sender, EventArgs e)
    {
        LinkButton chk = (LinkButton)sender;
        bool status = manager.deleteCmsDescriptionEntity(Convert.ToInt64(chk.ToolTip));
        if (status)
        {
            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "succesfuly");
            divmessage.InnerHtml = "The information deleted successfully.";
            editor.Visible = false;
            fileUploadImage.Visible = false;
            titleDiv.Visible = false;
            TabbedPanels2.Visible = false;
            saveCancelBtnDIV.Visible = false;
            OnClickDIV.Visible = false;
            titleHeadingDiv.Visible = false;
        }
        else
        {
            divmessage.Attributes.Add("class", "error");
            divmessage.Style.Add("display", "block");
            divmessage.InnerHtml = "The information could not be deleted.";
        }
        #region Bind The Grid
        titleHeadingDiv.Visible = true;
        TList<CmsDescription> list = manager.getCmsDescriptionAll(Convert.ToInt64(hdnCmsId.Value),1);
        grvHotelsOfTheWeek.DataSource = list;
        grvHotelsOfTheWeek.DataBind();
        ApplyPaging("HotelOfTheWeek");
        #endregion
    }

    /// <summary>
    /// This is the event handler for lbtDeleteBenefit CheckBox CheckedChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtDeleteBenefit_Click(object sender, EventArgs e)
    {
        LinkButton chk = (LinkButton)sender;
        bool status = manager.deleteCmsDescriptionEntity(Convert.ToInt64(chk.ToolTip));
        if (status)
        {
            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "succesfuly");
            divmessage.InnerHtml = "The information deleted successfully.";
            fileUploadImage.Visible = false;
            TabbedPanels2.Visible = false;
            descriptionTextboxDIV.Visible = false;
            saveCancelBtnDIV.Visible = false;
            OnClickDIV.Visible = false;
            titleHeadingDiv.Visible = false;
        }
        else
        {
            divmessage.Attributes.Add("class", "error");
            divmessage.Style.Add("display", "block");
            divmessage.InnerHtml = "The information could not be deleted.";
        }
        #region Bind The Grid
        titleHeadingDiv.Visible = true;
        TList<CmsDescription> list = manager.getCmsDescriptionAll(Convert.ToInt64(hdnCmsId.Value), 1);
        grvBenefits.DataSource = list;
        grvBenefits.DataBind();
        ApplyPaging("Benifites");
        #endregion
    }

    /// <summary>
    /// This is the event handler for lbtDeleteImage CheckBox CheckedChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtDeleteImage_Click(object sender, EventArgs e)
    {
        LinkButton chk = (LinkButton)sender;
        bool status = manager.deleteCmsDescriptionEntity(Convert.ToInt64(chk.ToolTip));
        if (status)
        {
            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "succesfuly");
            divmessage.InnerHtml = "The information deleted successfully.";
            saveCancelBtnDIV.Visible = false;
            titleDiv.Visible = false;
            fileUploadImage.Visible = false;
            OnClickDIV.Visible = false;
            titleHeadingDiv.Visible = false;
        }
        else
        {
            divmessage.Attributes.Add("class", "error");
            divmessage.Style.Add("display", "block");
            divmessage.InnerHtml = "The information could not be deleted.";
        }
        #region Bind The Grid
        titleHeadingDiv.Visible = true;
        TList<CmsDescription> list = manager.getCmsDescriptionAll(Convert.ToInt64(hdnCmsId.Value), 1);
        grvImageGallery.DataSource = list;
        grvImageGallery.DataBind();
        ApplyPaging("ImageGallery");
        #endregion
    }

    /// <summary>
    /// This is the event handler for lbtDeleteNews LinkButton Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtDeleteNews_Click(object sender, EventArgs e)
    {
        LinkButton chk = (LinkButton)sender;
        bool status = manager.deleteCmsDescriptionEntity(Convert.ToInt64(chk.ToolTip));
        if (status)
        {
            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "succesfuly");
            divmessage.InnerHtml = "The information deleted successfully.";
            editor.Visible = false;
            TabbedPanels2.Visible = false;
            saveCancelBtnDIV.Visible = false;
            fileUploadPDF.Visible = false;
            PublishedDateDIV.Visible = false;
            titleHeadingDiv.Visible = false;
            OnClickDIV.Visible = false;
        }
        else
        {
            divmessage.Attributes.Add("class", "error");
            divmessage.Style.Add("display", "block");
            divmessage.InnerHtml = "The information could not be deleted.";
        }
        #region Bind The Grid
        titleHeadingDiv.Visible = true;
        TList<CmsDescription> list = manager.getCmsDescriptionAll(Convert.ToInt64(hdnCmsId.Value), 1);
        grvNews.DataSource = list;
        grvNews.DataBind();
        ApplyPaging("News");
        #endregion
    }

    /// <summary>
    /// This is the event handler for lbtDeleteLink LinkButton Click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void lbtDeleteLink_Click(object sender, EventArgs e)
    {
        LinkButton chk = (LinkButton)sender;
        bool status = manager.deleteCmsDescriptionEntity(Convert.ToInt64(chk.ToolTip));
        if (status)
        {
            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "succesfuly");
            divmessage.InnerHtml = "The information deleted successfully.";
            TabbedPanels2.Visible = false;
            saveCancelBtnDIV.Visible = false;
            OnClickDIV.Visible = false;
            bottomLinkDropDownListsDIV.Visible = false;
            titleHeadingDiv.Visible = false;
        }
        else
        {
            divmessage.Attributes.Add("class", "error");
            divmessage.Style.Add("display", "block");
            divmessage.InnerHtml = "The information could not be deleted.";
        }
        #region Bind The Grid

        TList<CmsDescription> list = manager.getCmsDescriptionAll(Convert.ToInt64(hdnCmsId.Value), 1);
        grvBottomLinks.DataSource = list;
        grvBottomLinks.DataBind();
        ApplyPaging("BottomLink");
        #endregion
    }

    #region Activate/Deactivate


    /// <summary>
    /// This is the event handler for chkActivate CheckBox CheckedChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void chkActivate_CheckedChanged(object sender, EventArgs e)
    {
        Cms obj = manager.getCmsEntity(Convert.ToInt64(hdnCmsId.Value));
        if (obj != null)
        {
            if (chkActivate.Checked == true)
            {
                obj.IsActive = true;
            }
            else
            {
                obj.IsActive = false;
            }
            bool status = manager.updateCmsEntity(obj);
            if (status)
            {
                divmessage.Style.Add("display", "block");
                divmessage.Attributes.Add("class", "succesfuly");
                string message = obj.IsActive ? "Activated" : "Deactivated";
                divmessage.InnerHtml = "List has been " + message;
            }
        }
    }

    /// <summary>
    /// This is the event handler for chkActivateHotel CheckBox CheckedChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void chkActivateHotel_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chk = (CheckBox)sender;
        CmsDescription obj = manager.getCmsDescriptionOnID(Convert.ToInt64(chk.ToolTip));
        if (obj != null)
        {
            if (chk.Checked == true)
            {
                obj.IsActive = true;
            }
            else
            {
                obj.IsActive = false;
            }
            bool status = manager.updateCMSDescriptionEntity(obj);
            if (status)
            {
                divmessage.Style.Add("display", "block");
                divmessage.Attributes.Add("class", "succesfuly");
                string message = obj.IsActive ? "Activated" : "Deactivated";
                divmessage.InnerHtml = "Hotel has been " + message;
            }
        }
    }



    #endregion

    protected void rptFrontStaticPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            newString n = e.Item.DataItem as newString;
            if (n != null)
            {
                Label lblPageName = (Label)e.Item.FindControl("lblPageName");
                LinkButton lnkEdit = (LinkButton)e.Item.FindControl("lnkEdit");
                //ImageButton imgBtnActivateHotel = (ImageButton)e.Item.FindControl("imgBtnActivateHotel");
                lblPageName.Text = n.name;
                lnkEdit.CommandArgument = n.name;
                //imgBtnActivateHotel.CommandArgument = n.name;
            }
        }
    }


    protected void rptFrontStaticPage_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        heading = "Front Static Pages";
        if (e.CommandName == "Modify")
        {
            ViewState["TypeGrd"] = "Front Static Pages";
            Cms cmsObj = manager.getCmsEntityOnType("StaticPages");
            if (cmsObj != null)
            {
                hdnLanguageID.Value = "1";
                cmd = objManageCMSContent.GetCMSContent(cmsObj.Id, Convert.ToInt64(hdnLanguageID.Value)).Find(a => a.ContentTitle.ToLower() == e.CommandArgument.ToString().ToLower());
                #region managing control visibility
                divAgencyControlPanelImage.Visible = false;
                fileUploadPDF.Visible = false;
                editor.Visible = false;
                fileUploadImage.Visible = false;
                lbtPartners.Visible = false;
                lbtClients.Visible = false;
                lbtAddNewBottomLink.Visible = false;
                gridHotelsOfTheWeek.Visible = false;
                TabbedPanels2.Visible = false;
                titleDiv.Visible = false;
                descriptionTextboxDIV.Visible = false;
                gridBenefits.Visible = false;
                saveCancelBtnDIV.Visible = false;
                gridImageGallery.Visible = false;
                gridNews.Visible = false;
                socialNetworkingDiv.Visible = false;
                TabbedPanels2.Visible = false;
                hotelBanner.Visible = false;
                PublishedDateDIV.Visible = false;
                ViewPreviousFIleDIV.Visible = false;
                OnClickDIV.Visible = false;
                topTitleDIV.Visible = true;
                gridBottomLinks.Visible = false;
                calender.Visible = false;
                bottomLinkDropDownListsDIV.Visible = false;
                pagingDIV.Visible = false;
                titleHeadingDiv.Visible = true;
                lbtAddNewHotel.Visible = false;
                lbtAddNewImage.Visible = false;
                lbtAddNewNews.Visible = false;
                lbtAddBenefit.Visible = false;
                SaveStaticPages.Visible = false;
                frontStaticPages.Visible = true;


                #endregion
                editor.Visible = true;
                SaveStaticPages.Visible = true;
                saveCancelBtnDIV.Visible = false;
                titleDiv.Visible = true;
                txtTitle.ReadOnly = false;
                calender.Visible = false;
                divMetaDescription.Visible = false;
                TabbedPanels2.Visible = true;
                hdnCmsId.Value = Convert.ToString(cmsObj.Id);
                hdnRecordID.Value = hdnCmsId.Value;
                hdnContentTitle.Value = Convert.ToString(e.CommandArgument);
                LanaguageTabWithoutCountry1.CreateLanguageThree(true);
                OnClickDIV.Visible = true;

                if (cmd == null)
                {
                    cmd = new CmsDescription();
                    cmd.CmsId = cmsObj.Id;
                    cmd.LanguageId = Convert.ToInt64(hdnLanguageID.Value);
                    cmd.ContentTitle = e.CommandArgument.ToString();
                    cmd.IsActive = true;
                }
                else
                {
                    txtTitle.Text = System.Net.WebUtility.HtmlDecode(cmd.ContentShortDesc == null ? "" : cmd.ContentShortDesc);
                    ckeEditorEN.Text = cmd.ContentsDesc == null ? "" : cmd.ContentsDesc;
                    txtKeywords.Text = cmd.MetaKeyword == null ? "" : cmd.MetaKeyword;
                    txtMetaDesc.Text = cmd.MetaDescription == null ? "" : cmd.MetaDescription;
                }
                ScriptManager.RegisterStartupScript(this, typeof(Page), "msg2", "SetFocus();", true);
            }
        }
    }


    protected void lbtCancel2_Click(object sender, EventArgs e)
    {
        editor.Visible = false;
        SaveStaticPages.Visible = false;
        titleDiv.Visible = false;
        txtTitle.ReadOnly = true;
        calender.Visible = true;
        TabbedPanels2.Visible = false;
        cmd = null;
        OnClickDIV.Visible = false;
        #region managing control visibility
        divAgencyControlPanelImage.Visible = false;
        fileUploadPDF.Visible = false;
        editor.Visible = false;
        fileUploadImage.Visible = false;
        lbtPartners.Visible = false;
        lbtClients.Visible = false;
        lbtAddNewBottomLink.Visible = false;
        gridHotelsOfTheWeek.Visible = false;
        TabbedPanels2.Visible = false;
        titleDiv.Visible = false;
        descriptionTextboxDIV.Visible = false;
        gridBenefits.Visible = false;
        saveCancelBtnDIV.Visible = false;
        gridImageGallery.Visible = false;
        gridNews.Visible = false;
        socialNetworkingDiv.Visible = false;
        TabbedPanels2.Visible = false;
        hotelBanner.Visible = false;
        PublishedDateDIV.Visible = false;
        ViewPreviousFIleDIV.Visible = false;
        OnClickDIV.Visible = false;
        topTitleDIV.Visible = true;
        gridBottomLinks.Visible = false;
        calender.Visible = false;
        bottomLinkDropDownListsDIV.Visible = false;
        pagingDIV.Visible = false;
        titleHeadingDiv.Visible = true;
        lbtAddNewHotel.Visible = false;
        lbtAddNewImage.Visible = false;
        lbtAddNewNews.Visible = false;
        lbtAddBenefit.Visible = false;
        SaveStaticPages.Visible = false;
        frontStaticPages.Visible = true;


        #endregion
        heading = "Front Static Pages";
    }


    protected void lnkSaveStatic_Click(object sender, EventArgs e)
    {
        bool status = false;
        if (HttpUtility.HtmlDecode(hdnContentTitle.Value) == hdnContentTitle.Value)
        {
            hdnContentTitle.Value = HttpUtility.HtmlEncode(hdnContentTitle.Value);
        }
        cmd = objManageCMSContent.GetCMSContent(Convert.ToInt64(hdnCmsId.Value), Convert.ToInt64(hdnLanguageID.Value)).Find(a => a.ContentTitle.ToLower() == hdnContentTitle.Value.ToString().ToLower() && a.LanguageId == Convert.ToInt64(hdnLanguageID.Value));
        if (cmd == null)
        {
            cmd = new CmsDescription();
            cmd.CmsId = Convert.ToInt64(hdnCmsId.Value);
            cmd.LanguageId = Convert.ToInt64(hdnLanguageID.Value);
            cmd.ContentTitle = hdnContentTitle.Value;
            cmd.IsActive = true;
        }
        cmd.ContentShortDesc = System.Net.WebUtility.HtmlEncode(txtTitle.Text);
        cmd.ContentsDesc = ckeEditorEN.Text;
        cmd.MetaDescription = txtMetaDesc.Text;
        cmd.MetaKeyword = txtKeywords.Text;
        if (cmd.Id != 0)
        {
            status = manager.updateCMSDescriptionEntity(cmd);
        }
        else
        {
            status = manager.saveCMSDescriptionEntity(cmd);
        }
        if (status)
        {
            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "succesfuly");
            divmessage.InnerHtml = "The information saved successfully.";
            OnClickDIV.Visible = false;
            SaveStaticPages.Visible = false;
        }
        else
        {
            divmessage.Attributes.Add("class", "error");
            divmessage.Style.Add("display", "block");
            divmessage.InnerHtml = "The information could not be saved.";
        }
        heading = "Front Static Pages";
    }

    #endregion

}

public class newString
{
    public string name
    {
        get;
        set;
    }
}