﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuperAdmin/Main.master" AutoEventWireup="true"
    CodeFile="ApplicationOthers.aspx.cs" Inherits="SuperAdmin_ApplicationOthers" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="cancelation-heading-body3" id="HotelAssignTab" runat="server">
        <ul>
            <li><a id="liTimeFrame" href="ManageCountryOthers.aspx">Country</a></li>
            <li><a id="liApplication" class="select" href="ApplicationOthers.aspx">Application</a></li>
            <li><a id="liManageLanaguage" href="ManageLanguage.aspx">Manage Language</a></li>
            <li><a id="liSetinvoice" href="SetInvoice.aspx">Set Invoice</a></li>
        </ul>
    </div>
    <table style="border-top: 1px red solid; border-top-color: #A3D4F7; background: #ffffff;
        width: 100%">
        <tr>
            <td width="100%" bgcolor="#FFFFFF">
                <h3 style="font-size: 21px; border-bottom: #A3D4F7 solid 1px; font-weight: bold;
                    margin: 0px; padding: 0px 0px 0px 0px;">
                    Application Others
                </h3>
            </td>
        </tr>
        <tr>
            <td>
                <div id="divmessage" runat="server">
                </div>
            </td>
        </tr>
    </table>
    <table cellspacing="1" style="margin-left: -1px;" width="963px" bgcolor="#92bddd">
        <tr bgcolor="#E3F0F1">
            <td width="20%" style="line-height: 31px; padding-left: 5px;">
                <strong>Session timeout:</strong>
            </td>
            <td>
                <asp:TextBox ID="txtSessionTimeOut" Width="50px" MaxLength="5" runat="server"></asp:TextBox>
                &nbsp;&nbsp;Minute
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" TargetControlID="txtSessionTimeOut"
                    ValidChars="0123456789" runat="server">
                </asp:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr bgcolor="#F1F8F8">
            <td width="20%" style="line-height: 31px; padding-left: 5px;">
                <strong>Hotel of the week scroll amount:</strong>
            </td>
            <td>
                <asp:TextBox ID="txtScrollAmount" runat="server" Width="50px" MaxLength="5"></asp:TextBox>
                &nbsp;&nbsp;Second
                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtScrollAmount"
                    ValidChars="0123456789" runat="server">
                </asp:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr bgcolor="#FFFFFF">
            <td colspan="2" align="left" style="line-height: 41px; padding-top: 12px; padding-bottom: 10px;">
                <div style="float: left; width: 500px; padding-left: 140px">
                    <div class="n-commisions" style="float: left; margin-left: 20px;">
                        <div class="n-btn">
                            <asp:LinkButton ID="lnkSaveOthers" runat="server" OnClick="lnkSaveOthers_Click">
                                    <div class="n-btn-left">
                                    </div>
                                    <div class="n-btn-mid">
                                        Save</div>
                                    <div class="n-btn-right">
                                    </div>
                            </asp:LinkButton>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <script language="javascript" type="text/javascript">
        jQuery("#<%= lnkSaveOthers.ClientID %>").bind("click", function () {
            if (jQuery('#<%= divmessage.ClientID %>').hasClass("succesfuly")) {
                jQuery('#<%= divmessage.ClientID %>').removeClass("succesfuly").addClass("error").hide();
            }

            var isvalid = true;
            var errormessage = "";

            var sessiontimeout = jQuery("#<%=txtSessionTimeOut.ClientID %>").val();
            if (sessiontimeout.length <= 0 || sessiontimeout == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }
                errormessage += 'Enter session timeout value';
                isvalid = false;
            }

            var scrollAmount = jQuery("#<%= txtScrollAmount.ClientID %>").val();
            if (scrollAmount.length <= 0 || scrollAmount == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }
                errormessage += 'Enter hotel of the week scroll amount';
                isvalid = false;
            }

            if (!isvalid) {
                jQuery("#<%= divmessage.ClientID %>").show();
                jQuery("#<%= divmessage.ClientID %>").html(errormessage);

                return false;
            }
            jQuery("#<%= divmessage.ClientID %>").html("");
            jQuery("#<%= divmessage.ClientID %>").hide();
            //jQuery("#Loding_overlay").height(jQuery("body").height());
            jQuery("#Loding_overlay").show();
            jQuery("body").scrollTop(0);
            jQuery("html").scrollTop(0);
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';
        });
    </script>
</asp:Content>
