﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuperAdmin/Main.master" AutoEventWireup="true"
    CodeFile="ContentManagementSystem.aspx.cs" Inherits="SuperAdmin_ContentManagementSystem" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Src="../UserControl/SuperAdmin/StaticContent.ascx" TagName="StaticContent"
    TagPrefix="ucStaticContent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:HiddenField ID="hdnLanguageID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnCmsId" runat="server" />
    <asp:HiddenField ID="hdnHeading" runat="server" />
    <asp:HiddenField ID="hdnRecordID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnContentTitle" runat="server" Value="0" />
    <asp:HiddenField ID="hdnUniqueId" runat="server" Value="0" />
    <asp:HiddenField ID="hdnLinkMedia" runat="server" Value="0" />

    <div class="main_container1">
        <div class="superadmin-example-layout">
            <div class="superadmin-cms">
                <div class="superadmin-cms-box1" style="min-height: 210px;">
                    <h3>
                        Front end</h3>
                    <ul>
                        <li>
                            <asp:LinkButton ID="lbtAboutUs" runat="server" OnClick="lbtAboutUs_Click">Your advantages</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtWhyBookOnline" runat="server" OnClick="lbtWhyBookOnline_Click">Why book online</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtLegalAspects" runat="server" OnClick="lbtLegalAspects_Click">Legal aspects</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtContactUs" runat="server" OnClick="lbtContactUs_Click">Contact us</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtWhySendRequest" runat="server" OnClick="lbtWhySendRequest_Click">Why send a request</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtTermsNConditions" runat="server" OnClick="lbtTermsNConditions_Click">Terms & conditions</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtJoinToday" runat="server" OnClick="lbtJoinToday_Click">Join today</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtCompanyInfo" runat="server" OnClick="lbtCompanyInfo_Click">Company information</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtHotelsOfWeek" runat="server" OnClick="lbtHotelsOfWeek_Click">Hotels of the week</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtForInvestors" runat="server" OnClick="lbtForInvestors_Click">For investors</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtPrivacyStatement" runat="server" OnClick="lbtPrivacyStatement_Click">Privacy statement</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtImageGalleryFrontPage" runat="server" OnClick="lbtImageGalleryFrontPage_Click">Image gallery front page</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtNews" runat="server" OnClick="lbtNews_Click">News</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtBenefits" runat="server" OnClick="lbtBenefits_Click">Benefits</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtSocialLinks" runat="server" OnClick="lbtSocialLinks_Click">Social networking</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbtFrontEndBottomLink" runat="server" OnClick="lbtFrontEndBottomLink_Click">Front end bottom part</asp:LinkButton></li>
                        <li style="display: none;">
                            <asp:LinkButton ID="lnkStaticPages" runat="server" OnClick="lnkStaticPages_Click">Front Static Pages</asp:LinkButton></li>
                    </ul>
                </div>
                <div class="superadmin-cms-box2">
                    <h3>
                        Backend for hotel</h3>
                    <ul>
                        <li>
                            <asp:LinkButton ID="lbtNewsBanner" runat="server" OnClick="lbtNewsBanner_Click">News banner</asp:LinkButton></li>
                    </ul>
                </div>
                <div class="superadmin-cms-box3">
                    <h3>
                        Backend for agencies
                    </h3>
                </div>
                <div class="superadmin-cms-box4">
                    <h3>
                        Backend for user</h3>
                    <ul>
                        <li>
                            <asp:LinkButton ID="lbtPersonalizedMessage" runat="server" OnClick="lbtPersonalizedMessage_Click">Personalised message</asp:LinkButton></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <ucStaticContent:StaticContent ID="StaticContent1" runat="server" />
</asp:Content>
