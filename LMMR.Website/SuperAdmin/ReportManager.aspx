﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuperAdmin/Main.master" AutoEventWireup="true"
    CodeFile="ReportManager.aspx.cs" Inherits="SuperAdmin_Report_Manager" %>

<%@ Register Src="../UserControl/SuperAdmin/ReportSearchPanel.ascx" TagName="ReportSearchPanel"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="superadmin-example-layout">
        <div class="superadmin-cms">
            <div id="divmessage" runat="server" style="margin-bottom: 10px; width: 913px;">
            </div>
            <div class="superadmin-report-box1">
                <h3>
                    Reports</h3>
                <ul>
                    <li>
                        <asp:LinkButton ID="lblBookingRequest" runat="server" OnClick="lblBookingRequest_Click">Booking / Request Report</asp:LinkButton></li>
                    <li><asp:LinkButton ID="lnkcompanyAgencyReport" runat="server" OnClick="lnkcompanyAgencyReport_Click">Profile Company / Agency Report</asp:LinkButton></li>
                    <li>
                        <asp:LinkButton ID="lnkProductionCompanyAgency" runat="server" OnClick="lnkProductionCompanyAgency_Click">Production Company / Meet2 Report</asp:LinkButton></li>
                    <li>
                        <asp:LinkButton ID="lnkConversitionRequest" runat="server" 
                            onclick="lnkConversitionRequest_Click">Conversion Booking Report</asp:LinkButton></li>
                    <li>
                        <asp:LinkButton ID="lnkLeadTime" runat="server" OnClick="lnkLeadTime_Click">Lead Time Report</asp:LinkButton></li>
                    <li>
                        <asp:LinkButton ID="lnkRanking" runat="server" onclick="lnkRanking_Click">Ranking Report</asp:LinkButton></li>
                    <li>
                        <asp:LinkButton ID="lnkAvailability" runat="server" OnClick="lnkAvailability_Click">Availability Report</asp:LinkButton></li>
                    <li>
                        <asp:LinkButton ID="lnkCancelation" runat="server" OnClick="lnkCancelation_Click">Cancelation Report</asp:LinkButton></li>
                    <li>
                        <asp:LinkButton ID="lnkInventoryExistingHotel" runat="server" OnClick="lnkInventoryExistingHotel_Click">Inventory Report of Existing Hotel</asp:LinkButton></li>
                    <li>
                        <asp:LinkButton ID="lnkAddedHotel" runat="server" OnClick="lnkAddedHotel_Click">Added Hotel In Period Inventory Report</asp:LinkButton></li>
                    <li>
                        <asp:LinkButton ID="lnkOnlineInventoryCharge" runat="server" OnClick="lnkOnlineInventoryCharge_Click">Online Inventory Changes Report</asp:LinkButton></li>
                    <li>
                        <asp:LinkButton ID="lnkProductionHotelAndMeeting" runat="server" OnClick="lnkProductionHotelAndMeeting_Click">Production Report Hotel and Meeting facilities</asp:LinkButton></li>
                        <li>
                        <asp:LinkButton ID="lnkCommissionControl" runat="server" OnClick="lnkCommissionControl_Click">Commission Control Report</asp:LinkButton></li>
                    <li>
                        <asp:LinkButton ID="lnkRevenue" runat="server" OnClick="lnkRevenue_Click">Revenue Report</asp:LinkButton></li>
                    <li>
                        <asp:LinkButton ID="lnkNewsLetter" runat="server" OnClick="lnkNewsLetter_Click">Newsletter Report</asp:LinkButton></li>
                </ul>
            </div>
        </div>
        <div style="padding-top: 30px;float:left;" id="DivReportFilter" runat="server">
            <h3 style="padding-left:10px;" ><asp:Label ID="lblReportName" runat="server" Text="Label"></asp:Label></h3>
            <uc1:ReportSearchPanel ID="ucReportSearch" runat="server" />
            <asp:Panel ID="pnlExportReport" runat="server" Visible="false">
                <table>
                    <tr>
                        <td>
                            <asp:LinkButton ID="lnkDownloadPDF" runat="server" OnClick="lnkDownloadPDF_Click"><table><tr><td align="center"><img src="../Images/pdf.png" /></td></tr><tr><td>Download PDF</td></tr></table></asp:LinkButton>
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkDownloadXLS" runat="server" OnClick="lnkDownloadXLS_Click"><table><tr><td align="center"><img src="../Images/exl.jpg" /></td></tr><tr><td>Download Excel</td></tr></table></asp:LinkButton>
                        </td>
                        
                        <td  align="center" style="color:Red;" runat="server" id="trEmptydata" visible="false">
                            <b>No record found on above mentioned criteria.</b>
                        </td>
                    
                    </tr>
                    
                </table>
                <script language="javascript" type="text/javascript">
                    function movePage() {
                        document.getElementsByTagName('html')[0].style.overflow = 'auto';
                        jQuery('#Loding_overlay').hide();
                        var ofset = jQuery("#<%= DivReportFilter.ClientID %>").offset();

                        jQuery('body').scrollTop(ofset.top);
                        jQuery('html').scrollTop(ofset.top);


                    }
            </script>
            </asp:Panel>
            <div id="result" runat="server"  style="display:none" >
                <!-- Lead Time Report start-->
                <table border="1" id="LeadTimeandAvailabilityTable" runat="server">
                    <tr>
                        <td>
                            <asp:Label ID="LblReportFinalName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table id="forAllCountryText" runat="server" border="1">
                                <tr>
                                    <td>Report for All country from <asp:Label ID="lblFromDate" runat="server" ></asp:Label> to <asp:Label ID="lblTodate" runat="server" ></asp:Label></td>
                                </tr>
                            </table>
                            <table id="forSelectedCountryCity" runat="server" border="1">
                                <tr>
                                    <td>
                                        <strong>Country</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCountryName" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <strong>City</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCityName" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>From</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblFrom" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <strong>To</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblTo" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table border="1">
                                <asp:Repeater ID="rptMaster" runat="server" 
                                    onitemdatabound="rptMaster_ItemDataBound">
                                    <HeaderTemplate>
                                        <tr>
                                            <td>
                                                <strong>Country</strong>
                                            </td>
                                            <td>
                                                <strong>City</strong>
                                            </td>
                                            <td>
                                                <strong>Hotel Name</strong>
                                            </td>
                                            <td id="Meetingroomtd" runat="server" ><strong>Meeting room</strong></td>
                                            <asp:Repeater ID="rptDaysName" runat="server" onitemdatabound="rptDaysName_ItemDataBound">
                                                <ItemTemplate>
                                                    <td>
                                                        <strong><asp:Label ID="lblDays" runat="server"></asp:Label></strong>
                                                    </td>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <td id="tdCommissionTotalHeader" runat="server">Total</td>
                                            <td id="tdCommissionAvgHeader" runat="server" >Avg. per day/period</td>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblCountry" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCity" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblHotel" runat="server"></asp:Label>
                                            </td>
                                            <td id="Meetingroomtdbody" runat="server" ><asp:Label ID="lblMeetingroom" runat="server"></asp:Label></td>
                                            <asp:Repeater ID="rptDaysLead" runat="server" onitemdatabound="rptDaysLead_ItemDataBound">
                                                <ItemTemplate>
                                                    <td>
                                                        <asp:Label ID="lblLead" runat="server"></asp:Label>
                                                    </td>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <td id="tdCommissionTotalBody" runat="server"><asp:Label ID="lblCommissionTotal" runat="server"></asp:Label></td>
                                            <td id="tdCommissionAvgBody" runat="server" ><asp:Label ID="lblAvgPerDay" runat="server"></asp:Label></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </td>
                    </tr>
                </table>
                <!-- Lead Time Report end-->

                <!-- Production Report Start-->
                <table border="1" id="ProductionReport" runat="server" >
                    <tr>
                        <td>
                            <asp:Label ID="lblPReportFinalName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="PForAllCountryText" runat="server" border="1">
                                <tr>
                                    <td>Report for All country of year <asp:Label ID="lblPYearTop" runat="server" ></asp:Label>.</td>
                                </tr>
                            </table>
                            <table id="PForSelectedCountryCityText" runat="server" border="1">
                                <tr>
                                    <td>
                                        <strong>Country</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPCountry" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <strong>City</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPCity" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Year</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPYear" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table border="1">
                                <asp:Repeater ID="rptPMaster" runat="server" 
                                    onitemdatabound="rptPMaster_ItemDataBound">
                                    <HeaderTemplate>
                                        <tr>
                                            <td id="tdPCountry" runat="server" width="10%">
                                                <strong>Country</strong>
                                            </td>
                                            <td id="tdPCity"  runat="server"  width="10%">
                                                <strong>City</strong>
                                            </td>
                                            <td  width="10%">
                                                <strong><asp:Label ID="lblheaderType" runat="server"></asp:Label></strong>
                                            </td>
                                            <td  width="20%">
                                                <strong>Statistics</strong>
                                            </td>
                                            <asp:Repeater ID="rptPMonthName" runat="server" onitemdatabound="rptPMonthName_ItemDataBound">
                                                <ItemTemplate>
                                                    <td  width="6%">
                                                        <strong><asp:Label ID="lblPDays" runat="server"></asp:Label></strong>
                                                    </td>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td id="tdPCountryBody" runat="server" width="10%"  valign="top">
                                                <asp:Label ID="lblPCountry" runat="server"></asp:Label>
                                            </td>
                                            <td id="tdPCityBody" runat="server" width="10%"  valign="top">
                                                <asp:Label ID="lblPCity" runat="server"></asp:Label>
                                            </td>
                                            <td width="10%"  valign="top">
                                                <asp:Label ID="lblHotel" runat="server"></asp:Label>
                                            </td>
                                            <td width="20%"  valign="top">
                                                <table border="1" width="100%" height="100%">
                                                    <tr>
                                                        <td>No. of Booking</td>
                                                    </tr>
                                                    <tr>
                                                        <td>No. of Booking canceled</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Total amount from booking (in Euro)</td>
                                                    </tr>
                                                    <tr id="trTotalCommBookingStat" runat="server">
                                                        <td>Total Commission Booking</td>
                                                    </tr>
                                                    <tr id="trStatBookingTimeoutStat" runat="server">
                                                        <td>Statistics Booking Timeout</td>
                                                    </tr>
                                                    <tr>
                                                        <td>No. of Request</td>
                                                    </tr>
                                                    <tr>
                                                        <td>No. of Request canceled</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Modify revenue from request (in Euro)</td>
                                                    </tr>
                                                    <tr id="trTotalCommRequestStat" runat="server">
                                                        <td>Total Commission Request</td>
                                                    </tr>
                                                    <tr id="trStatRequestTimeoutStat" runat="server">
                                                        <td>Statistics Request Timeout</td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <asp:Repeater ID="rptPMonthDetails" runat="server" onitemdatabound="rptPMonthDetails_ItemDataBound">
                                                <ItemTemplate>
                                                    <td width="6%" valign="top">
                                                        <table border="1" width="100%" height="100%">
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblPNoOfBookings" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblPNoOfBookingCanceled" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblPTotalAmountForBooking" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr id="trTotalCommBookingStatBody" runat="server">
                                                                <td>
                                                                    <asp:Label ID="lblTotalCommissionBooking" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr id="trStatBookingTimeoutStatBody" runat="server">
                                                                <td>
                                                                    <asp:Label ID="lblStatisticsBookingTimeout" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblPNoOfRequest" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblPNoOfRequestCanceled" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblPModifyRevenue" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr id="trTotalCommRequestStatBody" runat="server">
                                                                <td>
                                                                    <asp:Label ID="lblTotalCommissionRequest" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr id="trStatRequestTimeoutStatBody" runat="server">
                                                                <td>
                                                                    <asp:Label ID="lblStatRequestTimeout" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <tr>
                                            <td id="tdPCountryFooter" runat="server" width="10%"  valign="top">
                                                &nbsp;
                                            </td>
                                            <td id="tdPCityFooter" runat="server" width="10%"  valign="top">
                                                &nbsp;
                                            </td>
                                            <td width="10%"  valign="top">
                                                &nbsp;                                            </td>
                                            <td width="20%"  valign="top">
                                                <table border="1" width="100%" height="100%">
                                                    <tr>
                                                        <td><b>Total No. of Booking</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Total No. of Booking canceled</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Total amount from booking (in Euro)</b></td>
                                                    </tr>
                                                    <tr id="trTotalCommBookingStatFooter" runat="server">
                                                        <td><b>Total Commission Booking</b></td>
                                                    </tr>
                                                    <tr id="trStatBookingTimeoutStatFooter" runat="server">
                                                        <td><b>Total Statistics Booking Timeout</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Total No. of Request</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Total No. of Request canceled</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Total Modify revenue from request (in Euro)</b></td>
                                                    </tr>
                                                    <tr id="trTotalCommRequestStatFooter" runat="server">
                                                        <td><b>Total Commission Request</b></td>
                                                    </tr>
                                                    <tr id="trStatRequestTimeoutStatFooter" runat="server">
                                                        <td><b>Total Statistics Request Timeout</b></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <asp:Repeater ID="rptPMonthDetailsFooter" runat="server" onitemdatabound="rptPMonthDetailsFooter_ItemDataBound">
                                                <ItemTemplate>
                                                    <td width="6%" valign="top">
                                                        <table border="1" width="100%" height="100%">
                                                            <tr>
                                                                <td>
                                                                    <b><asp:Label ID="lblPNoOfBookingsFooter" runat="server"></asp:Label></b>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b><asp:Label ID="lblPNoOfBookingCanceledFooter" runat="server"></asp:Label></b>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b><asp:Label ID="lblPTotalAmountForBookingFooter" runat="server"></asp:Label></b>
                                                                </td>
                                                            </tr>
                                                            <tr id="trTotalCommBookingStatBodyFooter" runat="server">
                                                                <td>
                                                                    <b><asp:Label ID="lblTotalCommissionBookingFooter" runat="server"></asp:Label></b>
                                                                </td>
                                                            </tr>
                                                            <tr id="trStatBookingTimeoutStatBody" runat="server">
                                                                <td>
                                                                    <b><asp:Label ID="lblStatisticsBookingTimeoutFooter" runat="server"></asp:Label></b>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b><asp:Label ID="lblPNoOfRequestFooter" runat="server"></asp:Label></b>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b><asp:Label ID="lblPNoOfRequestCanceledFooter" runat="server"></asp:Label></b>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b><asp:Label ID="lblPModifyRevenueFooter" runat="server"></asp:Label></b>
                                                                </td>
                                                            </tr>
                                                            <tr id="trTotalCommRequestStatBodyFooter" runat="server">
                                                                <td>
                                                                    <b><asp:Label ID="lblTotalCommissionRequestFooter" runat="server"></asp:Label></b>
                                                                </td>
                                                            </tr>
                                                            <tr id="trStatRequestTimeoutStatBodyFooter" runat="server">
                                                                <td>
                                                                    <b><asp:Label ID="lblStatRequestTimeoutFooter" runat="server"></asp:Label></b>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tr>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </table>
                        </td>
                    </tr>
                </table>
                <!-- Production Report End-->

                   <!-- Profile Company Agency Report start-->
                <table border="1" id="tblProfileCompanyReport" runat="server">
                    <tr>
                        <td>
                            <table id="tblForAllProfile" runat="server" border="1">
                                <tr>
                                    <td>Report for All country from <asp:Label ID="lblAllProfileFrom" runat="server" ></asp:Label> to <asp:Label ID="lblAllProfileTo" runat="server" ></asp:Label></td>
                                </tr>
                            </table>
                            <table id="tblForAllProfileByCountry" runat="server" border="1">
                                <tr>
                                    <td>
                                        <strong>Country</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblAllProfileCountry" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <strong>City</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblAllProfileCity" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>From</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblAllProfileFrom2" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <strong>To</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblAllProfileTo2" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table border="1">
                                <asp:Repeater ID="rptProfileReport" runat="server" OnItemDataBound="rptProfileReport_ItemDataBound">
                                    <HeaderTemplate>
                                        <tr>
                                            <td id="tdCompany" runat="server">
                                                Company name
                                            </td>
                                            <td id="tdContact" runat="server">
                                                Primary contact person
                                            </td>
                                            <td>
                                                City
                                            </td>
                                            <td>
                                                Postal code
                                            </td>
                                            <td>
                                                Address
                                            </td>
                                            <td>
                                                Email
                                            </td>
                                            <td>
                                                Phone
                                            </td>
                                            <td>
                                                Activation Date
                                            </td>
                                            <td>
                                                No of bookings till today
                                            </td>
                                            <td>
                                                No of request till today
                                            </td>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td id="tdCompany" runat="server">
                                                <asp:Label ID="lblCompanyName" runat="server" Text='<%# Eval("CompanyName") %>'></asp:Label>
                                                <asp:Label ID="lblUserId" runat="server" Text='<%# Eval("UserId") %>' Visible="false"></asp:Label>
                                            </td>
                                            <td id="tdContact" runat="server">
                                                <asp:Label ID="lblFirstName" runat="server" Text='<%# Eval("FirstName") %>'></asp:Label>
                                                <asp:Label ID="lblLastName" runat="server" Text='<%# Eval("LastName") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCityId" runat="server" Text='<%# Eval("CityName") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPostalCode" runat="server" Text='<%# Eval("PostalCode") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblAddress" runat="server" Text='<%# Eval("Address") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblEmailId" runat="server" Text='<%# Eval("EmailId") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPhone" runat="server" Text='<%# Eval("Phone") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblLastLogin" runat="server" Text='<%# Eval("CreatedDate") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblNoofBooking" runat="server" Text="10"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblNoofRequest" runat="server" Text="15"></asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </td>
                    </tr>
                </table>
                <!-- Profile Company Agency Report end-->

                <!-- Pratiks Work -->
                <!-- Cancellation Report Start -->
                <table border="1" id="tblCancellationReport" runat="server">
                    <tr>
                        <td>
                            <asp:Label ID="lblCancelfinalReport" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="forcancelallcountrytext" runat="server" border="1">
                                <tr>
                                    <td>
                                        Report for All country from
                                        <asp:Label ID="cancellblfromdate" runat="server"></asp:Label>
                                        to
                                        <asp:Label ID="cancellbltodate" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <table id="forcancelallcountrycity" runat="server" border="1">
                                <tr>
                                    <td>
                                        <strong>Country</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblcancelcountry" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <strong>City</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblcancelcity" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>From</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblcancelfrom" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <strong>To</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblcancelto" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table border="1">
                                <asp:Repeater ID="rptCancelreport" runat="server" OnItemDataBound="rptCancelreport_ItemDataBound">
                                    <HeaderTemplate>
                                        <tr>
                                            <td>
                                                Country
                                            </td>
                                            <td>
                                                City
                                            </td>
                                            <td>
                                                Hotel
                                            </td>
                                            <td>
                                                Canceled Booking By Client
                                            </td>
                                            <td>
                                                Canceled Booking By Operator
                                            </td>
                                            <td>
                                                Cancel Request by Operator
                                            </td>
                                            <td>
                                                Cancel Request by Hotel
                                            </td>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblcancelcountryname" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblcancelcityname" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblcancelhotelname" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdf1" runat="server" Visible="false" />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblcancelbookbyclient" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblcancelbookbyoperator" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblcancelrequestbyoperator" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblcancelrequestbyhotel" runat="server"></asp:Label>
                                            </td>
                                            
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </td>
                    </tr>
                </table>
                <!-- cancellation Report End -->

                <!-- BookingrequestHistory Report Start -->
                <table border="1" id="tblbookingrequesthistory" runat="server">

                    <tr>
                        <td>
                            <asp:Label ID="lblbrhfinalReport" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="forbrhallcountrytext" runat="server" border="1">
                                <tr>
                                    <td>
                                        Report for All country from
                                        <asp:Label ID="lblbrhfromdate" runat="server"></asp:Label>
                                        to
                                        <asp:Label ID="lblbrhtodate" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <table id="forbrhallcountrycity" runat="server" border="1">
                                <tr>
                                    <td>
                                        <strong>Country</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblbrhcountryname" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <strong>City</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblbrhcityname" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>From</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblbrhfrom" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <strong>To</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblbrhto" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table border="1">
                                <asp:Repeater ID="rptbrhmaster" runat="server" onitemdatabound="rptbrhmaster_ItemDataBound">
                                    <HeaderTemplate>
                                        <tr>
                                            <td>
                                                <strong>Country</strong>
                                            </td>
                                            <td>
                                                <strong>City</strong>
                                            </td>
                                            <td>
                                                <strong>Hotel Name</strong>
                                            </td>
                                            <%--<td id="Meetingroomtd" runat="server">
                                                <strong>Meeting room</strong>
                                            </td>--%>
                                            <asp:Repeater ID="rptbrhDaysName" runat="server" onitemdatabound="rptbrhDaysName_ItemDataBound">
                                                <ItemTemplate>
                                                    <td>
                                                        <strong>
                                                            <asp:Label ID="lblDays" runat="server"></asp:Label></strong>
                                                    </td>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblCountry" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCity" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblHotel" runat="server"></asp:Label>
                                            </td>
                                            <%--<td id="Meetingroomtdbody" runat="server">
                                                <asp:Label ID="lblMeetingroom" runat="server"></asp:Label>
                                            </td>--%>
                                            <asp:Repeater ID="rptbrhDaysLead" runat="server" onitemdatabound="rptbrhDaysLead_ItemDataBound">
                                                <ItemTemplate>
                                                    <td>
                                                        <asp:Label ID="lblLead" runat="server"></asp:Label>
                                                    </td>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </td>
                    </tr>
                </table>
                <!-- BookingrequestHistory Report End -->

                <!-- Inventory Report of Existing Hotel Start -->
                <table border="1" id="tblinventoryExistingHotel" runat="server">
                    <tr>
                        <td>
                            <asp:Label ID="lblinvExisthotelfinalReport" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="forinvExisthotelallcountrytext" runat="server" border="1">
                                <tr>
                                    <td>
                                        Report for All country from
                                        <asp:Label ID="lblforinvExisthotelfromdate" runat="server"></asp:Label>
                                        to
                                        <asp:Label ID="lblforinvExisthoteltodate" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <table id="forinvExisthotelallcountrycity" runat="server" border="1">
                                <tr>
                                    <td>
                                        <strong>Country</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblinvExisthotelcountry" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <strong>City</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblinvExisthotelcity" runat="server"></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>From</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblinvExisthotelfrom" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <strong>To</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblinvExisthotelto" runat="server"></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table border="1">
                                <asp:Repeater ID="rptgrdinvExisthotel" runat="server" OnItemDataBound="rptgrdinvExisthotel_ItemDataBound">
                                    <HeaderTemplate>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblheadercountry" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblheadercity" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblheaderhotel" runat="server"></asp:Label>
                                            </td>
                                            <td id="header1" runat="server">
                                                <asp:Label ID="lblheadernoofmeeting" runat="server"></asp:Label>
                                            </td>
                                            <td id="header2" runat="server">
                                                <asp:Label ID="lblheadermeetingonline" runat="server"></asp:Label>
                                            </td>
                                            
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblinvExisthotelcountryname" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblinvExisthotelcityname" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblinvExisthotelhotelname" runat="server"></asp:Label>
                                            </td>
                                            <td id="item1" runat="server" >
                                                <asp:Label ID="lblinvExisthotelmeetingrooms" runat="server"></asp:Label>
                                            </td>
                                            <td id="item2" runat="server">
                                                <asp:Label ID="lblinvExisthotelmeetingroomsonline" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </td>
                    </tr>
                </table>
                <!-- Inventory Report of Existing Hotel End -->
                <!-- End Pratiks Work -->

                
                <!-- Ranking-->
                <table border="1" id="tblRank" runat="server" >
                    <tr>
                        <td>
                            <asp:Label ID="lblheading" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                               <table id="forRankallcountrytext" runat="server" border="1">
                                <tr>
                                    <td>
                                        Report for All country
                                    </td>
                                </tr>
                            </table>
                      
                            <table id="forRankallcountrycity" runat="server" border="1">
                                <tr>
                                    <td>
                                        <strong>Country</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblrankCountry" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <strong>City</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblrankCity" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table border="1">
                                <asp:Repeater ID="rptRank" runat="server"                           >
                                    <HeaderTemplate>
                                        <tr>
                                            <td>
                                                <strong>Country</strong>
                                            </td>
                                            <td>
                                                <strong>City</strong>
                                            </td>
                                            <td>
                                                <strong>Hotel Name</strong>
                                            </td>
                                            <td ><strong>Today Booking Rank</strong></td>
                                            <td><strong>Today Request Rank</strong></td>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblCountry" runat="server" Text='<%# Eval("Countryname") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCity" runat="server" Text='<%# Eval("City") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblHotel" runat="server" Text='<%# Eval("HotelName") %>'></asp:Label>
                                            </td>
                                            <td><asp:Label ID="lblBookingrank" runat="server" Text='<%# Eval("BookingAlgo") %>'></asp:Label></td>
                                            <td><asp:Label ID="LblRequestRank" runat="server" Text='<%# Eval("RequestAlgo") %>'></asp:Label></td>
                                         
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </td>
                    </tr>
                </table>
                <!-- Ranking Report end-->
                  
                <!-- Conversion-->
                <table border="1" id="tblconv" runat="server" >
                    <tr>
                        <td>
                            <asp:Label ID="lblconverHead" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                               <table id="forConvallcountrytext" runat="server" border="1">
                                <tr>
                                    <td>
                                     
                                    Report for All country of year <asp:Label ID="lblYearConvTop" runat="server" ></asp:Label>.
                                    </td>
                                </tr>
                            </table>
                                    
                      
                            <table id="forConvallcountrycity" runat="server" border="1">
                                <tr>
                                    <td>
                                        <strong>Country</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblConvCountry" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <strong>City</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblConvCity" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Year</strong>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblYearConv" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table border="1">
                                <asp:Repeater ID="rptConver" runat="server" 
                                    onitemdatabound="rptConver_ItemDataBound"                           >
                                    <HeaderTemplate>
                                        <tr>
                                            <td>
                                                <strong>Country</strong>
                                            </td>
                                            <td>
                                                <strong>City</strong>
                                            </td>
                                            <td>
                                                <strong>Hotel Name</strong>
                                            </td>
                                          <asp:Repeater ID="rptPMonthName" runat="server" onitemdatabound="rptPMonthName_ItemDataBound">
                                                <ItemTemplate>
                                                    <td  width="6%">
                                                        <strong><asp:Label ID="lblPDays" runat="server"></asp:Label></strong>
                                                    </td>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblCountry" runat="server" Text='<%# Eval("Countryname") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCity" runat="server" Text='<%# Eval("CityName") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblHotel" runat="server" Text='<%# Eval("HotelName") %>'></asp:Label>
                                            </td>
                                            
                                             <asp:Repeater ID="rptDaysLead" runat="server" onitemdatabound="rptDaysLead_ItemDataBound">
                                                <ItemTemplate>
                                                    <td>
                                                        <asp:Label ID="lblLead" runat="server"></asp:Label>
                                                    </td>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            
                                         
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </td>
                    </tr>
                </table>
                <!-- Conversion Report end-->

                <!-- Newsletter-->
                <table border="1" id="tblNewsletter" runat="server">
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>                    
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table border="1">
                                <asp:Repeater ID="rptNewsLetter" runat="server"                           >
                                    <HeaderTemplate>
                                        <tr>
                                            <td>
                                                <strong>FirstName</strong>
                                            </td>
                                            <td>
                                                <strong>LastName</strong>
                                            </td>
                                            <td>
                                                <strong>Subscriber EmailId</strong>
                                            </td>                                          
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblFirstName" runat="server" Text='<%# Eval("FirstName") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblLastName" runat="server" Text='<%# Eval("LastName") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblSubscriberEmailid" runat="server" Text='<%# Eval("SubscriberEmailid") %>'></asp:Label>
                                            </td>                                           
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </td>
                    </tr>
                </table>
                <!-- Newsletter Report end-->

           
            </div>
        </div>
    </div>
</asp:Content>
