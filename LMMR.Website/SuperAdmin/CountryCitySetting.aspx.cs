﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Data;
using LMMR.Entities;
using System.Data;
using System.Xml;
using System.Drawing;
using System.Net;



public partial class SuperAdmin_CountryCitySetting : System.Web.UI.Page
{
    ClientContract objClient = new ClientContract();
    Staffaccount objstaff = new Staffaccount();
    NewUser newUserObject = new NewUser();
    TList<City> objCity = new TList<City>();
    TList<Zone> objzone = new TList<Zone>();
    TList<MainPoint> objmainpoint = new TList<MainPoint>();
    TList<Currency> objcurrency = new TList<Currency>();

    public string property
    {
        get;
        set;
        
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindAllCountry();
            BindallCity();
            bindcityasperid(0);
            bindcityforZone();
            bindgridview(0);
            bindcityasperpointCountry(0);
            bindgridviewZone(0);
            bindgridviewcurrency(0);
            divaddcurrency1.Visible = false;
            DivAddCity.Visible = false;
            divAddCountry.Visible = false;
            DivAddPoints.Visible = false;
            DivAddZone.Visible = false;

        }

    }
    public void allcountrybind()
    {

    }

    public void binddata()
    {
        BindAllCountry();
        BindallCity();
        bindcityasperid(0);

        bindcityforZone();
        bindgridview(0);
        bindcityasperpointCountry(0);
        bindgridviewZone(0);
        bindgridviewcurrency(0);
        divaddcurrency1.Visible = false;
        DivAddCity.Visible = false;
        divAddCountry.Visible = false;
        DivAddPoints.Visible = false;
        DivAddZone.Visible = false;


    }

    /// <summary>
    /// This function used for bind all county for repeater and dropdown.
    /// </summary>
    public void BindAllCountry()
    {
        TList<Country> objCountry = objClient.GetByAllCountry();
        rptcountrylist.DataSource = objCountry;
        rptcountrylist.DataBind();
        if (objCountry.Count <= 0)
        {
            lblemptycountry.Visible = true;
            rptcountrylist.Visible = false;
        }
        else
        {
            lblemptycountry.Visible = false;
            rptcountrylist.Visible = true;
        }

        //drppointcountry.DataSource = objCountry;
        //drppointcountry.DataTextField = "CountryName";
        //drppointcountry.DataValueField = "Id";
        //drppointcountry.DataBind();
        //drppointcountry.Items.Insert(0, new ListItem("Select Country", "0"));

    }

    public void BindallCity()
    {
        //TList<City> objcity = objClient.Getallcity();
        drpcity.DataSource = objstaff.GetByAllCountry();
        drpcity.DataTextField = "CountryName";
        drpcity.DataValueField = "Id";
        drpcity.DataBind();
        drpcity.Items.Insert(0, new ListItem("--Select country--", "0"));

        dropZoneSectionCountry.DataSource = objstaff.GetByAllCountry();
        dropZoneSectionCountry.DataTextField = "CountryName";
        dropZoneSectionCountry.DataValueField = "Id";
        dropZoneSectionCountry.DataBind();
        dropZoneSectionCountry.Items.Insert(0, new ListItem("--Select country--", "0"));

        dropCountryPoint.DataSource = objstaff.GetByAllCountry();
        dropCountryPoint.DataTextField = "CountryName";
        dropCountryPoint.DataValueField = "Id";
        dropCountryPoint.DataBind();
        dropCountryPoint.Items.Insert(0, new ListItem("--Select country--", "0"));

        //DlstZone1.DataSource = objClient.Getallcity();
        //DlstZone1.DataTextField = "City";
        //DlstZone1.DataValueField = "Id";
        //DlstZone1.DataBind();
        //DlstZone1.Items.Insert(0, new ListItem("--Select city--", "0"));


        TList<City> objGetCityByCoutnryID = newUserObject.GetCityByCountry(Convert.ToInt32(dropZoneSectionCountry.SelectedValue));
        if (objGetCityByCoutnryID.Count > 0)
        {
            DlstZone1.Items.Clear();
            DlstZone1.DataSource = objGetCityByCoutnryID;
            DlstZone1.DataTextField = "City";
            DlstZone1.DataValueField = "Id";
            DlstZone1.DataBind();
            DlstZone1.Items.Insert(0, new ListItem("--Select city--", "0"));
        }
        else
        {
            DlstZone1.Items.Clear();
            DlstZone1.Items.Insert(0, new ListItem("--Select city--", "0"));
        }




        //drpaddcity.DataSource = objClient.GetByAllCountry();
        //drpaddcity.DataTextField = "CountryName";
        //drpaddcity.DataValueField = "Id";
        //drpaddcity.DataBind();
        //drpaddcity.Items.Insert(0, new ListItem("--Select Country--", "0"));
    }

    public void bindcityforZone()
    {
        //TList<City> ObjCity = objClient.Getallcity().FindAll(a => a.IsActive == true);
        //drpcityZone.DataSource = ObjCity;
        //drpcityZone.DataTextField = "City";
        //drpcityZone.DataValueField = "Id";
        //drpcityZone.DataBind();
        //drpcityZone.Items.Insert(0, new ListItem("--Select city--", "0"));

        //drpcityforzoneadd.DataSource = ObjCity;
        //drpcityforzoneadd.DataTextField = "City";
        //drpcityforzoneadd.DataValueField = "Id";
        //drpcityforzoneadd.DataBind();
        //drpcityforzoneadd.Items.Insert(0, new ListItem("--Select city--", "0"));

        
        TList<City> objGetCityByCoutnryID = newUserObject.GetCityByCountry(Convert.ToInt32(dropCountryPoint.SelectedValue));
        if (objGetCityByCoutnryID.Count > 0)
        {
            drpcityZone.Items.Clear();
            drpcityZone.DataSource = objGetCityByCoutnryID;
            drpcityZone.DataTextField = "City";
            drpcityZone.DataValueField = "Id";
            drpcityZone.DataBind();
            drpcityZone.Items.Insert(0, new ListItem("--Select city--", "0"));
        }
        else
        {
            drpcityZone.Items.Clear();
            drpcityZone.Items.Insert(0, new ListItem("--Select city--", "0"));
        }

    }




    protected void rptcountrylist_ItemDataBound1(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Country Country = e.Item.DataItem as Country;
            Label lblcountryname = (Label)e.Item.FindControl("lblcountryname");
            LinkButton lnkDelete = (LinkButton)e.Item.FindControl("lnkDelete");
            LinkButton lnkModify = (LinkButton)e.Item.FindControl("lnkModify");
            //Label lblShortName = (Label)e.Item.FindControl("lblShortName");
            if (Country != null)
            {
                lblcountryname.Text = Country.CountryName.Trim().Replace("'", "");
                lnkDelete.CommandArgument = Convert.ToString(Country.Id);
                lnkModify.CommandArgument = Convert.ToString(Country.Id);
            }
        }
    }
    protected void dlstcity_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        City cities = e.Item.DataItem as City;
        Label lblcityname = (Label)e.Item.FindControl("lblcityname");
        LinkButton lnkDelete = (LinkButton)e.Item.FindControl("lnkDelete");
        LinkButton lnkModify = (LinkButton)e.Item.FindControl("lnkModify");
        //Label lblShortName = (Label)e.Item.FindControl("lblShortName");

        if (cities != null)
        {
            lblcityname.Text = cities.City.Trim().Replace("'", "");
            lnkDelete.CommandArgument = Convert.ToString(cities.Id);
            lnkModify.CommandArgument = Convert.ToString(cities.Id);
        }

    }


    protected void dlstzone_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        Zone zones = e.Item.DataItem as Zone;
        Label lblzonename = (Label)e.Item.FindControl("lblzonename");
        LinkButton lnkDelete = (LinkButton)e.Item.FindControl("lnkDelete");
        LinkButton lnkModify = (LinkButton)e.Item.FindControl("lnkModify");
        Label lblshow = (Label)FindControl("lblEmpty");
        City Citytest = objstaff.GetcityByCityID(zones.CityId.ToString());


        if (Citytest != null)
        {
            lblzonename.Text = zones.Zone.Trim().Replace("'", "") + "(" + Citytest.City + ")";
            lnkDelete.CommandArgument = Convert.ToString(zones.Id);
            lnkModify.CommandArgument = Convert.ToString(zones.Id);
        }

    }

    protected void dlstzone_ItemCommand(object source, DataListCommandEventArgs e)
    {
        string status;
        if (e.CommandName == "modifyme")
        {

            property = "zone";

            City Cityupdate = objstaff.GetCityById(Convert.ToInt64(e.CommandArgument));
            Zone zoneupdate = objstaff.GetzoneByID(Convert.ToInt64(e.CommandArgument));
            if (zoneupdate != null)
            {
                hdnzoneid.Value = Convert.ToString(zoneupdate.Id);
                txtzone.Text = zoneupdate.Zone.Trim().Replace("'", "");
                DlstZone1.SelectedValue = zoneupdate.CityId.ToString();
                lblheaderzone.Text = "Edit Zone";
                DivAddZone.Visible = true;
                divMainMessage.InnerHtml = "";
                divMainMessage.Attributes.Add("class", "");
                Divzonemesssage.InnerHtml = "";
                Divzonemesssage.Attributes.Add("class", "");
                ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + DivAddZone.ClientID + "');});", true);


            }
            else
            {
                divMainMessage.InnerHtml = "Some error occured";
                divMainMessage.Attributes.Add("class", "error");
            }
        }
        if (e.CommandName == "deleteme")
        {
            property = "zone";
            bool Deletesuccess;
            City Citydelete = objstaff.GetCityById(Convert.ToInt64(e.CommandArgument));
            Zone zonedelete = objstaff.GetzoneByID(Convert.ToInt64(e.CommandArgument));
            long cityid = zonedelete.Id;
            //zonedelete.IsActive = false;
            Deletesuccess = objstaff.Zonedelete(zonedelete);
            if (Deletesuccess == true)
            {
                status = "Zone deleted successfully";
                divMainMessage.InnerHtml = status;
                divMainMessage.Attributes.Add("class", "succesfuly");
                Divzonemesssage.InnerHtml = "";
                Divzonemesssage.Attributes.Add("class", "");
                if (ViewState["SelectCountryForZone"] != null)
                {
                    dropZoneSectionCountry.SelectedValue = ViewState["SelectCountryForZone"].ToString();
                    BindallCity();
                    DlstZone1.SelectedValue = ViewState["SelectCityForZone"].ToString();
                    bindgridviewZone(Convert.ToInt32(ViewState["SelectCityForZone"]));
                }
                DivAddZone.Visible = false;
            }
            else
            {
                //bindgridviewZone(0);
                DivAddZone.Visible = false;
                //BindallCity();
                divMainMessage.InnerHtml = "Error occured while deleting.";
                divMainMessage.Attributes.Add("class", "error");
            }
        }
    }

    protected void dlstCurrency_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        Currency currency = e.Item.DataItem as Currency;
        Label lblcurrency = (Label)e.Item.FindControl("lblcurrency");
        LinkButton lnkDelete = (LinkButton)e.Item.FindControl("lnkDelete");
        LinkButton lnkModify = (LinkButton)e.Item.FindControl("lnkModify");
        Label lblshow = (Label)FindControl("lblEmpty");

        if (currency != null)
        {
            lblcurrency.Text = currency.Currency.Trim().Replace("'", "") + " (" + WebUtility.HtmlDecode(currency.CurrencySignature) + ")";
            lnkDelete.CommandArgument = Convert.ToString(currency.Id);
            lnkModify.CommandArgument = Convert.ToString(currency.Id);
        }





    }

    protected void dlstCurrency_ItemCommand(object source, DataListCommandEventArgs e)
    {
        string status;
        if (e.CommandName == "modifyme")
        {

            property = "currency";

            City Cityupdate = objstaff.GetCityById(Convert.ToInt64(e.CommandArgument));
            Zone zoneupdate = objstaff.GetzoneByID(Convert.ToInt64(e.CommandArgument));
            Currency Currencyupdate = objstaff.GetcurrencyByID(Convert.ToInt64(e.CommandArgument));

            if (Currencyupdate != null)
            {
                hdfcurrency.Value = Convert.ToString(Currencyupdate.Id);
                txtcurrency.Text = Currencyupdate.Currency.Trim().Replace("'", "");
                txtsignature.Text = Currencyupdate.CurrencySignature;

                lblcurrency.Text = "Edit Currency";
                divaddcurrency1.Visible = true;
                divMainMessage.InnerHtml = "";
                divMainMessage.Attributes.Add("class", "");
                divmessagecurrency.InnerHtml = "";
                divmessagecurrency.Attributes.Add("class", "");
                ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + divaddcurrency1.ClientID + "');});", true);


            }
            else
            {
                divMainMessage.InnerHtml = "Some error occured";
                divMainMessage.Attributes.Add("class", "error");
            }
        }
        if (e.CommandName == "deleteme")
        {
            property = "currency";
            bool Deletesuccess;
            City Citydelete = objstaff.GetCityById(Convert.ToInt64(e.CommandArgument));
            Zone zonedelete = objstaff.GetzoneByID(Convert.ToInt64(e.CommandArgument));
            Currency Currencyupdate = objstaff.GetcurrencyByID(Convert.ToInt64(e.CommandArgument));
            Deletesuccess = objstaff.CurrencyDelete(Currencyupdate);
            if (Deletesuccess == true)
            {
                status = "Currency Deleted successfully";
                divMainMessage.InnerHtml = status;
                divMainMessage.Attributes.Add("class", "succesfuly");
                divmessagecurrency.InnerHtml = "";
                divmessagecurrency.Attributes.Add("class", "");
                bindgridviewcurrency(0);
                divaddcurrency1.Visible = false;
                //binddata();
            }
            else
            {
                //binddata();
                bindgridviewcurrency(0);
                divaddcurrency1.Visible = false;
                divMainMessage.InnerHtml = "Error occured while deleting.";
                divMainMessage.Attributes.Add("class", "error");
            }
        }
    }



    protected void drpcity_SelectedIndexChanged(object sender, EventArgs e)
    {
        property = "city";
        DivAddCity.Visible = false;
        if (drpcity.SelectedValue == "0")
        {
            bindcityasperid(0);

        }
        else
        {
            bindcityasperid(Convert.ToInt32(drpcity.SelectedValue));
        }
    }


    protected void DlstZone1_SelectedIndexChanged(object sender, EventArgs e)
    {
        property = "zone";
        DivAddZone.Visible = false;
        if (DlstZone1.SelectedValue == "0")
        {
            ViewState["SelectCityForZone"] = "0";
            bindgridviewZone(0);

        }
        else
        {
            ViewState["SelectCityForZone"] = DlstZone1.SelectedValue;
            bindgridviewZone(Convert.ToInt32(DlstZone1.SelectedValue));
        }
    }

    protected void dropZoneSectionCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        property = "zone";
        if (dropZoneSectionCountry.SelectedValue != "0")
        {
            ViewState["SelectCountryForZone"] = dropZoneSectionCountry.SelectedValue;
            DlstZone1.Items.Clear();
            TList<City> objGetCityByCoutnryID = newUserObject.GetCityByCountry(Convert.ToInt32(dropZoneSectionCountry.SelectedValue));
            DlstZone1.DataSource = objGetCityByCoutnryID;
            DlstZone1.DataTextField = "City";
            DlstZone1.DataValueField = "Id";
            DlstZone1.DataBind();
            DlstZone1.Items.Insert(0, new ListItem("--Select city--", "0"));
        }
        else
        {
            ViewState["SelectCountryForZone"] = "0";
            DlstZone1.Items.Clear();
            DlstZone1.Items.Insert(0, new ListItem("--Select city--", "0"));
        }
    }

    protected void dropCountryPoint_SelectedIndexChanged(object sender, EventArgs e)
    {
        property = "point";
        if (dropCountryPoint.SelectedValue != "0")
        {
            ViewState["SelectedCountryForPoint"] = dropCountryPoint.SelectedValue;
            drpcityZone.Items.Clear();
            TList<City> objGetCityByCoutnryID = newUserObject.GetCityByCountry(Convert.ToInt32(dropCountryPoint.SelectedValue));
            drpcityZone.DataSource = objGetCityByCoutnryID;
            drpcityZone.DataTextField = "City";
            drpcityZone.DataValueField = "Id";
            drpcityZone.DataBind();
            drpcityZone.Items.Insert(0, new ListItem("--Select city--", "0"));
        }
        else
        {
            ViewState["SelectedCountryForPoint"] = "0";
            drpcityZone.Items.Clear();
            drpcityZone.Items.Insert(0, new ListItem("--Select city--", "0"));
        }
    }
    protected void drpcityforaddmainpoints_SelectedIndexChanged(object sender, EventArgs e)
    {
        property = "point";

        //height = "1";

        //ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "setAddressValue('" + drpcityforaddmainpoints.SelectedItem.Text + "');", true);
        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){setAddressValue('" + drpcityZone.SelectedItem.Text + "'); movePage('" + DivAddPoints.ClientID + "');});", true);
        //divMainMessage.InnerHtml = "";
    }


    public void bindcityasperid(int id)
    {
        TList<City> cityall = new TList<City>();
        if (id == 0)
        {
            //cityall = objstaff.Getcity();
            ////cityall = cityall.FindAll(a => a.IsActive == true);
            //if (cityall.Count > 0)
            //{
            //    dlstcity.DataSource = cityall;
            //    dlstcity.DataBind();
            //    Session["allcity"] = cityall;
            //    lblemptyCity.Visible = false;
            //    dlstcity.Visible = true;
            //}
            //else
            //{
            lblemptyCity.Style.Add("display", "block");
            dlstcity.Visible = false;
            DivAddNewCityButton.Style.Add("display", "none");
            //}
            //drpcityforaddmainpoints.DataTextField = "City";
            //drpcityforaddmainpoints.DataValueField = "Id";
            //drpcityforaddmainpoints.DataSource = newUserObject.GetCityByCountry(id).Where(a => a.IsActive == true);
            //if (newUserObject.GetCityByCountry(Convert.ToInt32(id)).Count > 0)
            //{
            //    drpcityforaddmainpoints.DataBind();
            //    drpcityforaddmainpoints.Items.Insert(0, new ListItem("Select  city", "0"));
            //}
            //else
            //{
            //drpcityforaddmainpoints.Items.Clear();
            //drpcityforaddmainpoints.Items.Insert(0, new ListItem("Select  city", "0"));
            //}
        }
        else
        {

            DivAddNewCityButton.Style.Add("display", "block");
            cityall = newUserObject.GetCityByCountry(id);
            //cityall = cityall.FindAll(a => a.IsActive == true);
            if (cityall.Count > 0)
            {
                dlstcity.DataSource = cityall; //cityall.Where(a => a.IsActive == true);
                dlstcity.DataBind();
                lblemptyCity.Style.Add("display", "none");
                dlstcity.Visible = true;
            }
            else
            {
                lblemptyCity.Style.Add("display", "block");
                dlstcity.Visible = false;
            }
            //drpcityforaddmainpoints.DataTextField = "City";
            //drpcityforaddmainpoints.DataValueField = "Id";
            //drpcityforaddmainpoints.DataSource = newUserObject.GetCityByCountry(id).Where(a => a.IsActive == true);
            //if (newUserObject.GetCityByCountry(Convert.ToInt32(id)).Count > 0)
            //{
            //    drpcityforaddmainpoints.DataBind();
            //    drpcityforaddmainpoints.Items.Insert(0, new ListItem("Select  city", "0"));
            //}
            //else
            //{
            //    drpcityforaddmainpoints.Items.Clear();
            //    drpcityforaddmainpoints.Items.Insert(0, new ListItem("Select  city", "0"));
            //}
        }
    }

    public void bindcityasperpointCountry(int id)
    {
        //if (id == 0)
        //{
        //    drpcityforaddmainpoints.Items.Insert(0, new ListItem("Select  city", "0"));
        //}


        //if (drppointcountry.SelectedValue != "0")
        //{

        //    drpcityforaddmainpoints.DataTextField = "City";
        //    drpcityforaddmainpoints.DataValueField = "Id";
        //    drpcityforaddmainpoints.DataSource = newUserObject.GetCityByCountry(Convert.ToInt32(drppointcountry.SelectedItem.Value)).Where(a => a.IsActive == true);
        //    if (newUserObject.GetCityByCountry(Convert.ToInt32(drppointcountry.SelectedItem.Value)).Count > 0)
        //    {
        //        drpcityforaddmainpoints.DataBind();
        //        drpcityforaddmainpoints.Items.Insert(0, new ListItem("Select  city", "0"));
        //    }
        //    else
        //    {
        //        drpcityforaddmainpoints.Items.Clear();
        //        drpcityforaddmainpoints.Items.Insert(0, new ListItem("Select  city", "0"));
        //    }

        //}
        //else
        //{
        //    drpcityforaddmainpoints.Items.Clear();
        //    drpcityforaddmainpoints.Items.Insert(0, new ListItem("Select  city", "0"));

        //}


    }


    public string GetKeyResult(string key)
    {
        return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    }
    protected void lnkAddNew_Click(object sender, EventArgs e)
    {

        property = "country";
        divAddCountry.Visible = true;

        txtcountry.Text = "";
        txtcountry.Enabled = true;
        lblHeader.Text = "Add Country";
        hdnId.Value = "0";
        drpIsforall.SelectedItem.Value = "0";
        divMainMessage.InnerHtml = "";
        divMainMessage.Attributes.Add("class", "");
        divMessage.InnerHtml = "";
        divMessage.Attributes.Add("class", "");
        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + divAddCountry.ClientID + "');});", true);
        //ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "jQuery(document).ready(function () { SetFocusBottom();});", true);

    }
    protected void lnkSave_Click(object sender, EventArgs e)
    {
        bool success;
        property = "country";
        Country newCountry = null;
        if (hdnId.Value == "0")
        {
            TList<Country> allcountry = objstaff.Getcountry();
            foreach (Country country in allcountry)
            {
                string test = txtcountry.Text;
                if ((txtcountry.Text.Trim().Replace("'", "") == country.CountryName.Trim().Replace("'", "")) && (country.IsActive == true))
                {

                    ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + divAddCountry.ClientID + "');});", true);
                    divMessage.InnerHtml = "Country already exists.";
                    divMessage.Attributes.Add("class", "error");
                    return;
                }
            }
            newCountry = new Country();
            newCountry.CountryName = txtcountry.Text.Trim().Replace("'", "");
            newCountry.CurrencyId = 2;
            newCountry.IsActive = true;

            if (drpIsforall.SelectedItem.Value == "1")
            {
                newCountry.IsForAll = true;
            }
            else
            {
                newCountry.IsForAll = false;
            }

            success = objstaff.countryinsert(newCountry);
            if (success == true)
            {
                objstaff.AddLanguageByCountry(newCountry.Id);
                divMainMessage.InnerHtml = "Country added successfully.";
                divMainMessage.Attributes.Add("class", "succesfuly");
                divMessage.InnerHtml = "";
                divMessage.Attributes.Add("class", "");
                //binddata();
                BindAllCountry();
                BindallCity();
                divAddCountry.Visible = false;

            }
            else
            {
                BindAllCountry();
                //divAddCountry.Visible = false;
                divMessage.InnerHtml = "Some error occured";
                divMessage.Attributes.Add("class", "error");
                ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + divAddCountry.ClientID + "');});", true);
            }

        }
        else
        {

            newCountry = objstaff.GetCountrybyid(Convert.ToInt64(hdnId.Value));
            string oldname = newCountry.CountryName;

            TList<Country> allcountry = objstaff.Getcountry();
            foreach (Country country in allcountry)
            {

                if ((txtcountry.Text.Trim().Replace("'", "") == country.CountryName.Trim().Replace("'", "")) && (country.IsActive == true))
                {
                    if (txtcountry.Text.Trim().Replace("'", "") == newCountry.CountryName.Trim().Replace("'", ""))
                    {

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + divAddCountry.ClientID + "');});", true);
                        divMessage.InnerHtml = "Country already exists.";
                        divMessage.Attributes.Add("class", "error");
                        return;
                    }

                }
            }
            newCountry.CountryName = txtcountry.Text.Trim().Replace("'", "");
            if (drpIsforall.SelectedItem.Value == "1")
            {
                newCountry.IsForAll = true;
            }
            else
            {
                newCountry.IsForAll = false;
            }

            if (objstaff.Updatecountry(newCountry, oldname))
            {
                objstaff.AddLanguageByCountry(newCountry.Id);
                divMainMessage.InnerHtml = "Country updated successfully.";
                divMainMessage.Attributes.Add("class", "succesfuly");
                divMessage.InnerHtml = "";
                divMessage.Attributes.Add("class", "");
                BindAllCountry();
                BindallCity();
                divAddCountry.Visible = false;

            }
            else
            {

                divMessage.InnerHtml = "Error occur while updating.";
                divMessage.Attributes.Add("class", "error");
            }

        }

    }
    protected void rptcountrylist_ItemCommand(object source, DataListCommandEventArgs e)
    {


        if (e.CommandName == "modifyme")
        {
            property = "country";


            Country countryUpdate = objstaff.GetCountrybyid(Convert.ToInt64(e.CommandArgument));
            if (countryUpdate != null)
            {
                hdnId.Value = Convert.ToString(countryUpdate.Id);
                txtcountry.Text = countryUpdate.CountryName.Trim().Replace("'", "");
                if (countryUpdate.IsForAll == true)
                {
                    drpIsforall.SelectedValue = "1";
                }
                else
                {
                    drpIsforall.SelectedValue = "0";
                }
                lblHeader.Text = "Edit Country";
                divAddCountry.Visible = true;
                divMainMessage.InnerHtml = "";
                divMainMessage.Attributes.Add("class", "");
                divMessage.InnerHtml = "";
                divMessage.Attributes.Add("class", "");
                ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + divAddCountry.ClientID + "');});", true);


            }
            else
            {
                divMainMessage.InnerHtml = "Some Error Occured";
                divMainMessage.Attributes.Add("class", "error");
            }
        }

        if (e.CommandName == "deleteme")
        {
            property = "country";
            bool Deletesuccess;
            Country Countrydelete = objstaff.GetCountrybyid(Convert.ToInt64(e.CommandArgument));
            long countryid = Countrydelete.Id;
            TList<City> allcityperid = objstaff.GetCitybyCountryid(Countrydelete.Id);
            foreach (City City in allcityperid)
            {
                if (countryid == City.CountryId)
                {
                    TList<Zone> zonedeleteForCountry = objstaff.GetZonebycityid(City.Id);
                    foreach (Zone Zone in zonedeleteForCountry)
                    {
                        if (City.Id == Zone.CityId)
                        {
                            Deletesuccess = objstaff.Deletecountryall(Zone, City, Countrydelete);
                            if (Deletesuccess == true)
                            {
                                divMainMessage.InnerHtml = "Country deleted sucessfully.";
                                divMainMessage.Attributes.Add("class", "succesfuly");
                                divMessage.InnerHtml = "";
                                divMessage.Attributes.Add("class", "");
                                BindAllCountry();
                                //divAddCountry.Visible = false;
                                divAddCountry.Visible = false;
                            }
                            else
                            {
                                divMainMessage.InnerHtml = "Error occured while deleting.";
                                divMainMessage.Attributes.Add("class", "error");
                            }

                        }


                    }
                    TList<MainPoint> mainpointdelete = objstaff.getmainpointdel(City.Id);
                    foreach (MainPoint point in mainpointdelete)
                    {
                        if (City.Id == point.CityId)
                        {
                            Deletesuccess = objstaff.pointdelete(point);
                            if (Deletesuccess == true)
                            {
                                divMainMessage.InnerHtml = "Country deleted sucessfully.";
                                divMainMessage.Attributes.Add("class", "succesfuly");
                                divMessage.InnerHtml = "";
                                divMessage.Attributes.Add("class", "");
                                BindAllCountry();
                                divAddCountry.Visible = false;
                                divAddCountry.Visible = false;
                            }
                            else
                            {
                                divMainMessage.InnerHtml = "Error occured while deleting.";
                                divMainMessage.Attributes.Add("class", "error");
                            }

                        }


                    }


                }
                if (City.IsActive == true)
                {
                    Deletesuccess = objstaff.Deletecountrycity(City, Countrydelete);
                    if (Deletesuccess == true)
                    {
                        divMainMessage.InnerHtml = "Country deleted sucessfully.";
                        divMainMessage.Attributes.Add("class", "succesfuly");
                        divMessage.InnerHtml = "";
                        divMessage.Attributes.Add("class", "");
                        //binddata();
                        BindAllCountry();
                        divAddCountry.Visible = false;

                    }
                    else
                    {
                        divMainMessage.InnerHtml = "Error occured while deleting.";
                        divMainMessage.Attributes.Add("class", "error");
                    }
                }


            }
            if (Countrydelete.IsActive == true)
            {
                Deletesuccess = objstaff.Deletecountry(Countrydelete);
                if (Deletesuccess == true)
                {
                    divMainMessage.InnerHtml = "Country deleted sucessfully.";
                    divMainMessage.Attributes.Add("class", "succesfuly");
                    divMessage.InnerHtml = "";
                    divMessage.Attributes.Add("class", "");
                    BindAllCountry();
                    divAddCountry.Visible = false;
                    divAddCountry.Visible = false;
                }
                else
                {
                    divMainMessage.InnerHtml = "Error occured while deleting.";
                    divMainMessage.Attributes.Add("class", "error");
                }
            }



        }

    }


    protected void lnkaddnewzone_Click(object sender, EventArgs e)
    {
        property = "zone";

        DivAddZone.Visible = true;
        txtzone.Text = "";
        txtzone.Enabled = true;
        lblheaderzone.Text = "Add Zone";
        //drpcityforzoneadd.SelectedValue = "0";
        hdnzoneid.Value = "0";
        divMainMessage.InnerHtml = "";
        divMainMessage.Attributes.Add("class", "");
        Divzonemesssage.InnerHtml = "";
        Divzonemesssage.Attributes.Add("class", "");
        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + DivAddZone.ClientID + "');});", true);
    }
    protected void lnkcurrencyadd_Click(object sender, EventArgs e)
    {
        property = "currency";

        divaddcurrency1.Visible = true;

        txtsignature.Text = "";
        txtsignature.Enabled = true;
        txtcurrency.Text = "";
        txtcurrency.Enabled = true;
        lblcurrency.Text = "Add Currency";
        hdfcurrency.Value = "0";
        divMainMessage.InnerHtml = "";
        divMainMessage.Attributes.Add("class", "");
        divmessagecurrency.InnerHtml = "";
        divmessagecurrency.Attributes.Add("class", "");
        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + divaddcurrency1.ClientID + "');});", true);
    }
    protected void LnkAddNewCity_Click(object sender, EventArgs e)
    {
        property = "city";
        DivAddCity.Visible = true;
        txtcity.Text = "";
        txtcity.Enabled = true;
        lblheadercity.Text = "Add City";
        //drpaddcity.SelectedValue = "0";
        hdnId1.Value = "0";
        divMainMessage.InnerHtml = "";
        divMainMessage.Attributes.Add("class", "");
        divmessagecity.InnerHtml = "";
        divmessagecity.Attributes.Add("class", "");
        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + DivAddCity.ClientID + "');});", true);
    }

    protected void lnkaddnewmainpoint_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){ setAddressValue('" + drpcityZone.SelectedItem.Text + "'); movePage('" + DivAddPoints.ClientID + "');});", true);
        //ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "setAddressValue('europe');", true);
        property = "point";
        DivAddPoints.Visible = true;
        txtpoint.Text = "";
        txtpoint.Enabled = true;
        lblLatitude.Text = "";
        lblLongitude.Text = "";
        BindAllCountry();
        bindcityasperid(0);
        //divvisible.Visible = true;
        lblheaderpointname.Text = "Add Points";
        hdnId2.Value = "0";




        //BindAllCountry();
        divMainMessage.Attributes.Add("class", "");
        divMainMessage.InnerHtml = "";
        divmessagepoint.InnerHtml = "";
        divmessagepoint.Attributes.Add("class", "");
    }
    protected void lnkcity_Click(object sender, EventArgs e)
    {

        bool success;
        City newcity = null;
        property = "city";
        string status;
        if (hdnId1.Value == "0")
        {


            //long countryId = Convert.ToInt64(countryList.SelectedItem.Value);
            TList<City> allcity = objstaff.Getcity();
            foreach (City city in allcity)
            {
                if ((txtcity.Text.Trim().Replace("'", "") == city.City.Trim().Replace("'", "")) && (city.IsActive == true) && (city.CountryId==Convert.ToInt64(drpcity.SelectedValue)))
                {

                    ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + DivAddCity.ClientID + "');});", true);
                    divmessagecity.InnerHtml = "City already exists ";
                    divmessagecity.Attributes.Add("class", "error");
                    return;
                }
            }

            newcity = new City();
            newcity.City = txtcity.Text.Trim().Replace("'", "");
            newcity.CountryId = Convert.ToInt64(drpcity.SelectedValue);
            newcity.IsActive = true;
            success = objstaff.Cityinsert(newcity);
            if (success == true)
            {
                divMainMessage.InnerHtml = "City added successfully.";
                divMainMessage.Attributes.Add("class", "succesfuly");

                divmessagecity.InnerHtml = "";
                divmessagecity.Attributes.Add("class", "");
                //binddata();
                bindcityasperid(Convert.ToInt32(newcity.CountryId));
                DivAddCity.Visible = false;
                BindallCity();
                drpcity.SelectedValue = newcity.CountryId.ToString();
            }
            else
            {
                divmessagecity.InnerHtml = "Some Error Occured";
                divmessagecity.Attributes.Add("class", "error");
            }

        }
        else
        {
            newcity = objstaff.GetCityById(Convert.ToInt64(hdnId1.Value));
            TList<City> allcity = objstaff.Getcity();
            foreach (City city in allcity)
            {
                if ((txtcity.Text.Trim().Replace("'", "") == city.City.Trim().Replace("'", "")) && (city.IsActive == true) && (city.CountryId == Convert.ToInt64(drpcity.SelectedValue)))
                {
                    if (txtcity.Text.Trim().Replace("'", "") == newcity.City.Trim().Replace("'", ""))
                    {

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + DivAddCity.ClientID + "');});", true);
                        divmessagecity.InnerHtml = "City already exists .";
                        divmessagecity.Attributes.Add("class", "error");
                        return;
                    }

                }
            }
            newcity.City = txtcity.Text.Trim().Replace("'", "");
            newcity.CountryId = Convert.ToInt64(drpcity.SelectedValue);
            status = objstaff.Updatecity(newcity);
            if (status == "Information Updated successfully.")
            {
                divMainMessage.InnerHtml = "City updated successfully.";
                divMainMessage.Attributes.Add("class", "succesfuly");
                divmessagecity.InnerHtml = "";
                divmessagecity.Attributes.Add("class", "");
                bindcityasperid(0);
                DivAddCity.Visible = false;
                BindallCity();


            }
            else
            {
                divmessagecity.InnerHtml = "Error occur while updating.";
                divmessagecity.Attributes.Add("class", "error");
            }


        }

    }
    protected void dlstcity_ItemCommand(object source, DataListCommandEventArgs e)
    {
        string status;
        if (e.CommandName == "modifyme")
        {
            property = "city";

            City Cityupdate = objstaff.GetCityById(Convert.ToInt64(e.CommandArgument));
            if (Cityupdate != null)
            {
                hdnId1.Value = Convert.ToString(Cityupdate.Id);
                txtcity.Text = Cityupdate.City.Trim().Replace("'", "");
                drpcity.SelectedValue = Cityupdate.CountryId.ToString();

                lblheadercity.Text = "Edit City";
                DivAddCity.Visible = true;
                divMainMessage.InnerHtml = "";
                divMainMessage.Attributes.Add("class", "");
                divmessagecity.InnerHtml = "";
                divmessagecity.Attributes.Add("class", "");
                ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + DivAddCity.ClientID + "');});", true);


            }
            else
            {
                divMainMessage.InnerHtml = "Some Error Occured";
                divMainMessage.Attributes.Add("class", "error");
            }
        }

        if (e.CommandName == "deleteme")
        {
            property = "city";
            bool Deletesuccess;
            City Citydelete = objstaff.GetCityById(Convert.ToInt64(e.CommandArgument));
            long cityid = Citydelete.Id;
            TList<Zone> zonedeleteForCity = objstaff.GetZonebycityid(cityid);
            foreach (Zone Zone in zonedeleteForCity)
            {
                if (cityid == Zone.CityId)
                {

                    //Deletesuccess = objstaff.Deletecountryall(Zone,Countrydelete);
                    status = objstaff.deletezone(Zone, Citydelete);
                    if (status == "Information Deleted successfully.")
                    {
                        divMainMessage.InnerHtml = "City deleted Successfully";
                        divMainMessage.Attributes.Add("class", "succesfuly");
                        divMessage.InnerHtml = "City deleted Successfully";
                        divMessage.Attributes.Add("class", "");
                        bindcityasperid(0);
                        DivAddCity.Visible = false;
                        BindallCity();
                    }
                    else
                    {
                        divMainMessage.InnerHtml = "Error occured while deleting.";
                        divMainMessage.Attributes.Add("class", "error");
                    }

                }



            }

            if (Citydelete.IsActive == true)
            {
                status = objstaff.deletezone(Citydelete);
                if (status == "Information Deleted successfully.")
                {
                    divMainMessage.InnerHtml = "City deleted Successfully";
                    divMainMessage.Attributes.Add("class", "succesfuly");
                    divMessage.InnerHtml = "City deleted Successfully";
                    divMessage.Attributes.Add("class", "");
                    bindcityasperid(0);
                    DivAddCity.Visible = false;
                    BindallCity();
                }
                else
                {
                    divMainMessage.InnerHtml = "Error occured while deleting.";
                    divMainMessage.Attributes.Add("class", "error");
                }
            }
            TList<MainPoint> mainpointdelete = objstaff.getmainpointdel(Citydelete.Id);
            foreach (MainPoint point in mainpointdelete)
            {
                if (Citydelete.Id == point.CityId)
                {
                    Deletesuccess = objstaff.pointdelete(point);
                    if (Deletesuccess == true)
                    {
                        divMainMessage.InnerHtml = "City deleted successfully.";
                        divMainMessage.Attributes.Add("class", "succesfuly");
                        divMessage.InnerHtml = "";
                        divMessage.Attributes.Add("class", "");
                        bindcityasperid(0);
                        DivAddCity.Visible = false;
                        BindallCity();
                    }
                    else
                    {
                        divMainMessage.InnerHtml = "Error occured while deleting.";
                        divMainMessage.Attributes.Add("class", "error");
                    }

                }


            }

        }

    }

    protected void dlstMainpoints_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            MainPoint point = e.Item.DataItem as MainPoint;
            Label lblmainpoints = (Label)e.Item.FindControl("lblmainpoints");
            LinkButton lnkDelete = (LinkButton)e.Item.FindControl("lnkDelete");
            LinkButton lnkModify = (LinkButton)e.Item.FindControl("lnkModify");
            City Citypoint = objstaff.GetcityByCityID(point.CityId.ToString());

            if (Citypoint != null)
            {
                if (point.IsCenter == true)
                {
                    lblmainpoints.ForeColor = System.Drawing.Color.Red;
                    //e.Item.ForeColor = System.Drawing.Color.Red;
                }
                lblmainpoints.Text = point.MainPointName + "(" + Citypoint.City + ")";
                lnkDelete.CommandArgument = Convert.ToString(point.MainPointId);
                lnkModify.CommandArgument = Convert.ToString(point.MainPointId);
            }
            else
            {
                lblmainpoints.Visible = false;
                lblmainpoints.Enabled = false;
                lnkDelete.CommandArgument = "";
                lnkModify.CommandArgument = "";
            }

        }
    }

    protected void dlstMainpoints_ItemCommand(object source, DataListCommandEventArgs e)
    {
        string status;

        if (e.CommandName == "modifyme")
        {
            property = "point";
            //City Cityupdate = objstaff.GetCityById(Convert.ToInt64(e.CommandArgument));
            //Zone zoneupdate = objstaff.GetzoneByID(Convert.ToInt64(e.CommandArgument));


            MainPoint pointupdate = objstaff.Getmainpointbyid(Convert.ToInt64(e.CommandArgument));
            if (pointupdate != null)
            {
                hdnId2.Value = Convert.ToString(pointupdate.MainPointId);

                //drpcityforaddmainpoints.SelectedValue = pointupdate.CityId.ToString();

                DivAddPoints.Visible = true;
                //divvisible.Visible = false;
                txtpoint.Text = pointupdate.MainPointName.Trim().Replace("'", "");
                lblLatitude.Text = pointupdate.Latitude.ToString();
                lblLongitude.Text = pointupdate.Longitude.ToString();
                hf_latitude.Value = pointupdate.Latitude.ToString();
                hf_longtitude.Value = pointupdate.Longitude.ToString();

                if (Convert.ToBoolean(pointupdate.IsCenter))
                {
                    chkboxIsCenter.Checked = true;
                }
                else
                {
                    chkboxIsCenter.Checked = false;
                }

                //City Citypoint = objstaff.GetcityByCityID(pointupdate.CityId.ToString());
                //BindAllCountry();
                //drpcityZone.SelectedValue = Citypoint.CountryId.ToString();
                //if (drppointcountry.SelectedValue != "0")
                //{
                //    //drpcityforaddmainpoints.DataSource = objstaff.bindcityasperid(Convert.ToInt64(Citypoint.CountryId));
                //    bindcityasperid(Convert.ToInt32(Citypoint.CountryId));
                //    //drpcityforaddmainpoints.SelectedValue = pointupdate.CityId.ToString();
                //    drpcityZone.SelectedValue = pointupdate.CityId.ToString();

                //}

                //ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "setAddressValue('" + hf_latitude.Value + "," + hf_longtitude.Value + "');", true);
                ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){ setAddressValue('" + hf_latitude.Value + "," + hf_longtitude.Value + "'); movePage('" + DivAddPoints.ClientID + "');});", true);
                txtpoint.Enabled = true;
                lblheaderpointname.Text = "Edit Point";
                divMainMessage.Attributes.Add("class", "");
                divmessagepoint.InnerHtml = "";
                divmessagepoint.Attributes.Add("class", "");


            }
            else
            {
                divMainMessage.InnerHtml = "Some Error Occured";
                divMainMessage.Attributes.Add("class", "error");
            }
        }
        if (e.CommandName == "deleteme")
        {
            property = "point";
            bool Deletesuccess;
            //City Citydelete = objstaff.GetCityById(Convert.ToInt64(e.CommandArgument));
            //Zone zonedelete = objstaff.GetzoneByID(Convert.ToInt64(e.CommandArgument));
            MainPoint pointupdate = objstaff.Getmainpointbyid(Convert.ToInt64(e.CommandArgument));
            long cityid = pointupdate.MainPointId;
            Deletesuccess = objstaff.pointdelete(pointupdate);
            if (Deletesuccess == true)
            {
                status = "Main Point deleted successfully";
                divMainMessage.InnerHtml = status;
                divMainMessage.Attributes.Add("class", "succesfuly");
                divmessagepoint.InnerHtml = "";
                divmessagepoint.Attributes.Add("class", "");
                if (ViewState["SelectedCountryForPoint"] != null)
                {
                    dropCountryPoint.SelectedValue = ViewState["SelectedCountryForPoint"].ToString();
                    bindcityforZone();
                    bindgridview(Convert.ToInt32(ViewState["SelectedCityForPoint"]));
                }
            }
            else
            {
                divMainMessage.InnerHtml = "Error occured while deleting.";
                divMainMessage.Attributes.Add("class", "error");
            }
        }
    }

    public void bindgridview(int id)
    {
        if (id == 0)
        {
            //string whereclauseforHotel = string.Empty;
            //string orderby = "MainPointName ASC";
            //objmainpoint = objstaff.Getmainpoint(whereclauseforHotel, orderby);


            //if (objmainpoint.Count > 0)
            //{
            //    dlstMainpoints.DataSource = objmainpoint;
            //    dlstMainpoints.DataBind();
            //    lblEmptyCityPoint.Visible = false;
            //    dlstMainpoints.Visible = true;
            //}
            //else
            //{
            lblEmptyCityPoint.Style.Add("display", "block");
            dlstMainpoints.Visible = false;
            DivAddPointButton.Style.Add("display", "none");
            // }
        }
        else
        {
            DivAddPointButton.Style.Add("display", "block");
            string whereclauseforHotel = "CityId=" + id;
            string orderby = "MainPointName ASC";
            objmainpoint = objstaff.Getmainpoint(whereclauseforHotel, orderby);
            if (objmainpoint.Count > 0)
            {
                dlstMainpoints.DataSource = objmainpoint;
                dlstMainpoints.DataBind();
                lblEmptyCityPoint.Style.Add("display", "none");
                dlstMainpoints.Visible = true;
            }
            else
            {
                lblEmptyCityPoint.Style.Add("display", "block");
                dlstMainpoints.Visible = false;
            }
        }
    }

    public void bindgridviewcurrency(int id)
    {
        if (id == 0)
        {
            objcurrency = objstaff.Getcurrency();
            if (objcurrency.Count > 0)
            {
                dlstCurrency.DataSource = objcurrency;
                dlstCurrency.DataBind();
                lblEmptyCurrency.Visible = false;
                dlstCurrency.Visible = true;
            }
            else
            {

                lblEmptyCurrency.Visible = true;
                dlstCurrency.Visible = false;
            }

        }
    }
    protected void drpcityZone_SelectedIndexChanged(object sender, EventArgs e)
    {
        property = "point";
        DivAddPoints.Visible = false;
        if (drpcityZone.SelectedValue == "0")
        {
            ViewState["SelectedCityForPoint"] = "0";
            bindgridview(0);
        }
        else
        {
            ViewState["SelectedCityForPoint"] = drpcityZone.SelectedValue;
            bindgridview(Convert.ToInt32(drpcityZone.SelectedItem.Value));
            ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){ setAddressValue('" + drpcityZone.SelectedItem.Text + "'); movePage('" + DivAddPoints.ClientID + "');});", true);

        }

    }

    public void bindgridviewZone(int id)
    {
        if (id == 0)
        {
            //string whereclauseforHotel = string.Empty;
            //string orderby = "Zone ASC";
            //objzone = objstaff.GetZonelist(whereclauseforHotel, orderby);
            ////objzone = objzone.FindAll(a => a.IsActive == true);
            //if (objzone.Count > 0)
            //{
            //    dlstzone.DataSource = objzone;
            //    dlstzone.DataBind();
            //    lblEmptyZone.Visible = false;
            //    dlstzone.Visible = true;
            //}
            //else
            //{
            lblEmptyZone.Style.Add("display", "block");
            DivAddZoneButton.Style.Add("display", "none");
            dlstzone.Visible = false;
            //}

        }
        else
        {
            DivAddZoneButton.Style.Add("display", "block");
            string whereclauseforHotel = "CityId=" + id;
            string orderby = "Zone ASC";
            objzone = objstaff.GetZonelist(whereclauseforHotel, orderby);
            if (objzone.Count > 0)
            {
                dlstzone.DataSource = objzone;
                dlstzone.DataBind();
                lblEmptyZone.Style.Add("display", "none");
                dlstzone.Visible = true;
            }
            else
            {
                lblEmptyZone.Style.Add("display", "block");
                dlstzone.Visible = false;
            }
        }


    }
    protected void drpcityforzones_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void lnkzone_Click(object sender, EventArgs e)
    {
        bool success;
        string status;
        property = "zone";
        Zone newZone = null;
        if (hdnzoneid.Value == "0")
        {


            //long countryId = Convert.ToInt64(countryList.SelectedItem.Value);
            TList<Zone> AllZone = objstaff.GetZone();
            foreach (Zone zone in AllZone)
            {

                if ((txtzone.Text.Trim().Replace("'", "") == zone.Zone.Trim().Replace("'", "")) && (zone.IsActive == true) && (zone.CityId == Convert.ToInt64(DlstZone1.SelectedValue)))
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + DivAddZone.ClientID + "');});", true);
                    Divzonemesssage.InnerHtml = "Zone already exists.";
                    Divzonemesssage.Attributes.Add("class", "error");
                    return;
                }
            }
            newZone = new Zone();
            newZone.Zone = txtzone.Text.Trim().Replace("'", "");
            newZone.IsActive = true;
            newZone.CityId = Convert.ToInt64(DlstZone1.SelectedValue);
            success = objstaff.Zoneinsert(newZone);
            if (success == true)
            {
                divMainMessage.InnerHtml = "Zone added successfully.";
                divMainMessage.Attributes.Add("class", "succesfuly");
                Divzonemesssage.InnerHtml = "";
                Divzonemesssage.Attributes.Add("class", "");
                bindgridviewZone(Convert.ToInt32(DlstZone1.SelectedValue));
                DivAddZone.Visible = false;
                if (ViewState["SelectCountryForZone"] != null)
                {
                    dropZoneSectionCountry.SelectedValue = ViewState["SelectCountryForZone"].ToString();
                    BindallCity();
                    DlstZone1.SelectedValue = newZone.CityId.ToString();
                }

            }
            else
            {
                Divzonemesssage.InnerHtml = "Some error occured";
                Divzonemesssage.Attributes.Add("class", "error");
            }

        }
        else
        {
            newZone = objstaff.GetzoneByID(Convert.ToInt64(hdnzoneid.Value));
            TList<Zone> AllZone = objstaff.GetZone();
            foreach (Zone zone in AllZone)
            {

                if ((txtzone.Text.Trim().Replace("'", "") == zone.Zone.Trim().Replace("'", "")) && (zone.IsActive == true) && (zone.CityId == Convert.ToInt64(DlstZone1.SelectedValue)))
                {
                    if (txtzone.Text.Trim().Replace("'", "") == newZone.Zone.Trim().Replace("'", ""))
                    {

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + DivAddZone.ClientID + "');});", true);
                        Divzonemesssage.InnerHtml = "Zone already exists.";
                        Divzonemesssage.Attributes.Add("class", "error");
                        return;
                    }

                }
            }

            newZone.Zone = txtzone.Text.Trim().Replace("'", "");
            newZone.CityId = Convert.ToInt64(DlstZone1.SelectedValue);
            status = objstaff.Updatezone(newZone);
            if (status == "Information Updated successfully.")
            {
                divMainMessage.InnerHtml = "Zone updated successfully.";
                divMainMessage.Attributes.Add("class", "succesfuly");
                Divzonemesssage.InnerHtml = "";
                Divzonemesssage.Attributes.Add("class", "");
                bindgridviewZone(Convert.ToInt32(DlstZone1.SelectedValue));
                DivAddZone.Visible = false;
                if (ViewState["SelectCountryForZone"] != null)
                {
                    dropZoneSectionCountry.SelectedValue = ViewState["SelectCountryForZone"].ToString();
                    BindallCity();
                    DlstZone1.SelectedValue = newZone.CityId.ToString();
                }
            }
            else
            {
                Divzonemesssage.InnerHtml = "Error occur while updating.";
                Divzonemesssage.Attributes.Add("class", "error");
            }

        }


    }
    protected void lnksavecurrency_Click(object sender, EventArgs e)
    {
        bool success;
        string status;
        property = "currency";
        Currency newcurrency = null;
        if (hdfcurrency.Value == "0")
        {


            //long countryId = Convert.ToInt64(countryList.SelectedItem.Value);
            TList<Currency> Allcurrency = objstaff.Getcurrency();
            foreach (Currency zone in Allcurrency)
            {

                if (txtcurrency.Text.Trim().Replace("'", "") == zone.Currency.Trim().Replace("'", ""))
                {

                    ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + divaddcurrency1.ClientID + "');});", true);
                    divmessagecurrency.InnerHtml = "Currency already exists.";
                    divmessagecurrency.Attributes.Add("class", "error");
                    return;
                }
            }
            newcurrency = new Currency();
            newcurrency.Currency = txtcurrency.Text.Trim().Replace("'", "");
            newcurrency.CurrencySignature = WebUtility.HtmlEncode(txtsignature.Text).Replace("amp;", "");

            success = objstaff.Currencyinsert(newcurrency);
            if (success == true)
            {
                divMainMessage.InnerHtml = "Currency added successfully.";
                divMainMessage.Attributes.Add("class", "succesfuly");
                divmessagecurrency.InnerHtml = "";
                divmessagecurrency.Attributes.Add("class", "");
                bindgridviewcurrency(0);
                divaddcurrency1.Visible = false;
                //binddata();

            }
            else
            {
                divmessagecurrency.InnerHtml = "Some error occured";
                divmessagecurrency.Attributes.Add("class", "error");
            }

        }
        else
        {
            //newZone = objstaff.GetzoneByID(Convert.ToInt64(hdnzoneid.Value));
            newcurrency = objstaff.GetcurrencyByID(Convert.ToInt64(hdfcurrency.Value));
            TList<Currency> Allcurrency = objstaff.Getcurrency();
            foreach (Currency currency in Allcurrency)
            {

                if (txtcurrency.Text.Trim().Replace("'", "") == currency.Currency.Trim().Replace("'", ""))
                {
                    if (txtcurrency.Text.Trim().Replace("'", "") == newcurrency.Currency.Trim().Replace("'", ""))
                    {

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + divaddcurrency1.ClientID + "');});", true);
                        divmessagecurrency.InnerHtml = "Currency already exists.";
                        divmessagecurrency.Attributes.Add("class", "error");
                        return;
                    }

                }
            }
            newcurrency.CurrencySignature = WebUtility.HtmlEncode(txtsignature.Text.Trim()).Replace("amp;", "");
            newcurrency.Currency = txtcurrency.Text.Trim().Replace("'", "");
            success = objstaff.UpdateCurrency(newcurrency);
            if (success == true)
            {
                divMainMessage.InnerHtml = "Currency updated successfully.";
                divMainMessage.Attributes.Add("class", "succesfuly");
                divmessagecurrency.InnerHtml = "";
                divmessagecurrency.Attributes.Add("class", "");
                bindgridviewcurrency(0);
                divaddcurrency1.Visible = false;

            }
            else
            {
                divmessagecurrency.InnerHtml = "Error occur while updating.";
                divmessagecurrency.Attributes.Add("class", "error");
            }

        }


    }

    protected void lblmainpoint_Click(object sender, EventArgs e)
    {
        property = "point";
        bool success;
        MainPoint newpoint = null;
        if (hdnId2.Value == "0")
        {
            //long countryId = Convert.ToInt64(countryList.SelectedItem.Value);
            string where = string.Empty;
            string order = string.Empty;
            TList<MainPoint> AllPoint = objstaff.Getmainpoint(where, order);
            foreach (MainPoint point in AllPoint)
            {

                if ((txtpoint.Text.Trim().Replace("'", "") == point.MainPointName.Trim().Replace("'", "")) && (point.CityId == Convert.ToInt64(drpcityZone.SelectedValue)))
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + DivAddPoints.ClientID + "');});", true);
                    divmessagepoint.InnerHtml = "Point already exists .";
                    divmessagepoint.Attributes.Add("class", "error");
                    return;
                }
            }
            newpoint = new MainPoint();
            newpoint.MainPointName = txtpoint.Text.Trim().Replace("'", "");
            newpoint.Longitude = Convert.ToDecimal(lblLongitude.Text);
            newpoint.Latitude = Convert.ToDecimal(lblLatitude.Text);
            newpoint.CityId = Convert.ToInt64(drpcityZone.SelectedValue);
            string whereclauseforHotel = "CityId=" + Convert.ToInt64(drpcityZone.SelectedValue);
            string orderby = "MainPointName ASC";
            objmainpoint = objstaff.Getmainpoint(whereclauseforHotel, orderby);
            if (objmainpoint.Count > 0)
            {
                if (chkboxIsCenter.Checked)
                {
                    newpoint.IsCenter = true;
                }
                else
                {
                    newpoint.IsCenter = false;
                }
            }
            else
            {
                newpoint.IsCenter = true;
            }
            success = objstaff.Pointinsert(newpoint);
            if (success == true)
            {
                bindgridview(Convert.ToInt32(newpoint.CityId));
                if (ViewState["SelectedCountryForPoint"] != null)
                {
                    dropCountryPoint.SelectedValue = ViewState["SelectedCountryForPoint"].ToString();
                    bindcityforZone();
                    drpcityZone.SelectedValue = newpoint.CityId.ToString();
                }
                DivAddPoints.Visible = false;

                divMainMessage.InnerHtml = "MainPoint added successfully.";
                divMainMessage.Attributes.Add("class", "succesfuly");
                divmessagepoint.InnerHtml = "";
                divmessagepoint.Attributes.Add("class", "");
                ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + DivAddPoints.ClientID + "');});", true);
            }
            else
            {
                divmessagepoint.InnerHtml = "some error occured";
                divmessagepoint.Attributes.Add("class", "error");
                ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + DivAddPoints.ClientID + "');});", true);
            }

        }
        else
        {
            newpoint = objstaff.Getmainpointbyid(Convert.ToInt64(hdnId2.Value));
            string where = string.Empty;
            string order = string.Empty;
            TList<MainPoint> AllPoint = objstaff.Getmainpoint(where, order);
            foreach (MainPoint point in AllPoint)
            {

                if ((txtpoint.Text.Trim().Replace("'", "") == point.MainPointName.Trim().Replace("'", "")) && (point.CityId == Convert.ToInt64(drpcityZone.SelectedValue)))
                {
                    if (txtpoint.Text.Trim().Replace("'", "") == newpoint.MainPointName.Trim().Replace("'", ""))
                    {

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + DivAddPoints.ClientID + "');});", true);
                        divmessagepoint.InnerHtml = "Point already exists ";
                        divmessagepoint.Attributes.Add("class", "error");
                        return;
                    }

                }
            }

            newpoint = objstaff.Getmainpointbyid(Convert.ToInt64(hdnId2.Value));
            newpoint.MainPointName = txtpoint.Text.Trim().Replace("'", "");
            newpoint.Longitude = Convert.ToDecimal(lblLongitude.Text);
            newpoint.Latitude = Convert.ToDecimal(lblLatitude.Text);
            //newpoint.CityId = Convert.ToInt64(drpcityforaddmainpoints.SelectedItem.Value);
            if (chkboxIsCenter.Checked)
            {
                newpoint.IsCenter = true;
            }
            else
            {
                newpoint.IsCenter = false;
            }
            success = objstaff.Pointupdate(newpoint);
            if (success == true)
            {
                divMainMessage.InnerHtml = "Point updated successfully.";
                divMainMessage.Attributes.Add("class", "succesfuly");
                divmessagepoint.InnerHtml = "";
                divmessagepoint.Attributes.Add("class", "");
                bindgridview(Convert.ToInt32(newpoint.CityId));

                if (ViewState["SelectedCountryForPoint"] != null)
                {
                    dropCountryPoint.SelectedValue = ViewState["SelectedCountryForPoint"].ToString();
                    bindcityforZone();
                    drpcityZone.SelectedValue = newpoint.CityId.ToString();
                }

                DivAddPoints.Visible = false;

            }
            else
            {
                divmessagepoint.InnerHtml = "Error occur while updating.";
                divmessagepoint.Attributes.Add("class", "error");
            }

        }

    }
    protected void lnkCancelcountry_Click(object sender, EventArgs e)
    {
        //binddata();
        divAddCountry.Visible = false;
        DivAddCity.Visible = false;
        DivAddZone.Visible = false;
        DivAddPoints.Visible = false;
        property = "country";
    }
    protected void lnkCancelcity_Click(object sender, EventArgs e)
    {
        //binddata();
        divAddCountry.Visible = false;
        DivAddCity.Visible = false;
        DivAddZone.Visible = false;
        DivAddPoints.Visible = false;
        property = "city";
    }
    protected void lnkCancelzone_Click(object sender, EventArgs e)
    {
        //binddata();
        divAddCountry.Visible = false;
        DivAddCity.Visible = false;
        DivAddZone.Visible = false;
        DivAddPoints.Visible = false;
        property = "zone";
    }
    protected void lnkCancelpoint_Click(object sender, EventArgs e)
    {
        //binddata();
        divAddCountry.Visible = false;
        DivAddCity.Visible = false;
        DivAddZone.Visible = false;
        DivAddPoints.Visible = false;
        property = "point";
    }
    protected void lnkcancelcurrency_Click(object sender, EventArgs e)
    {
        //binddata();
        divAddCountry.Visible = false;
        DivAddCity.Visible = false;
        DivAddZone.Visible = false;
        DivAddPoints.Visible = false;
        divaddcurrency1.Visible = false;
        property = "currency";
    }
}