﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;

public partial class SuperAdmin_ManageCountry : System.Web.UI.Page
{
    #region Variable
    public CancellationPolicy objCancellationManager = new CancellationPolicy();
    public ManageOthers objOthers = new ManageOthers();
    public HotelManager objHotel = new HotelManager();
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCountryListView();
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
        }

        ApplyPaging();
    }
    #endregion

    #region Function
    /// <summary>
    /// This function used for get 
    /// </summary>
    public void BindCountryListView()
    {
        grdViewCountryOthers.DataSource = objCancellationManager.GetCountry();
        grdViewCountryOthers.DataBind();
    }

    /// <summary>
    /// This function used for bind cancellation policy with dropdown by countryID.
    /// </summary>
    public void BindCancellationPolicy()
    {

        TList<CancelationPolicy> obj = objCancellationManager.GetPolicyByCountry(Convert.ToInt32(hdnCountryID.Value));
        if (obj.Count > 0)
        {
            drpCancellationPolicy.Items.Clear();
            drpCancellationPolicy.DataSource = obj;
            drpCancellationPolicy.DataTextField = "PolicyName";
            drpCancellationPolicy.DataValueField = "Id";
            drpCancellationPolicy.DataBind();
        }
        else
        {
            drpCancellationPolicy.Items.Clear();
            drpCancellationPolicy.Items.Add(new ListItem("--Select policy--", "0"));
        }
    }

    /// <summary>
    /// This function used for bind all language with checkbox list.
    /// </summary>
    void BindChekBoxListLanguage()
    {
        chkListLanguage.DataTextField = "Name";
        chkListLanguage.DataValueField = "Id";
        chkListLanguage.DataSource = objOthers.GetLanguages();
        chkListLanguage.DataBind();
    }

    /// <summary>
    /// This function used for bind currency with dropdown.
    /// </summary>
    void BindDropDownCurrency()
    {
        drpSelectCurrency.DataTextField = "Currency";
        drpSelectCurrency.DataValueField = "Id";
        drpSelectCurrency.DataSource = objOthers.GetAllCurrency();
        drpSelectCurrency.DataBind();
    }

    /// <summary>
    /// This function used for get all old information get by countryID
    /// </summary>
    /// <param name="countryID"></param>
    void GetOldInfo(int countryID)
    {
        TList<Others> obj = objOthers.GetInfoByCountryID(countryID);
        if (obj.Count > 0)
        {
            Others objRow = obj.FirstOrDefault();

            if (objRow.CurrencyId != null)
            {
                drpSelectCurrency.SelectedValue = objRow.CurrencyId.ToString();
            }

            txtIntervalVariationMin.Text = objRow.MinIntervalVariation.ToString();
            txtIntervalVariationMax.Text = objRow.MaxIntervalVariation.ToString();
            txtMinimumSpecial.Text = objRow.MinConsiderSpl.ToString();
            //txtInvoiceCommissionHotel.Text = objRow.InvoiceComm.ToString();
            txtModificationTrigger.Text = objRow.RevenueModificationTrigger.ToString();
            txtRequiredMaximumHotelBasket.Text = objRow.MaxRequestBasket.ToString();
            //txtSetAgencyComm.Text = objRow.AgencyComm.ToString();
            txtMinimumStock.Text = objRow.MinStockforHotel.ToString();
            drpCancellationPolicy.SelectedValue = objRow.CancellationPolicyId.ToString();
            txtCheckCommitionButton.Text = objRow.CheckCommWait.ToString();
            TList<CountryLanguage> objLanguage = objOthers.GetLanguageByCountryID(countryID);
            if (objLanguage.Count > 0)
            {
                int intIndex = 0;
                for (intIndex = 0; intIndex <= objLanguage.Count - 1; intIndex++)
                {
                    foreach (ListItem chkObj in chkListLanguage.Items)
                    {
                        if (chkObj.Value == objLanguage[intIndex].LanguageId.ToString())
                        {
                            chkObj.Selected = true;
                        }
                    }
                }
            }
        }

        foreach (ListItem chkObj in chkListLanguage.Items)
        {

            if (chkObj.Text == "English")
            {
                chkObj.Selected = true;
                chkObj.Enabled = false;
            }

        }

        TList<HearUsQuestion> objQuestion = objOthers.GetAllQuestion(countryID);
        if (objQuestion.Count > 0)
        {
            int i = 0;
            for (i = 0; i <= objQuestion.Count - 1; i++)
            {
                string strQuestion = objQuestion[i].Question;
                //strQuestion = strQuestion.Replace("'", "P~L");

                ScriptManager.RegisterStartupScript(this, typeof(Page), "AddTextBox" + i + "", "addElementWithText('" + strQuestion + "');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "AddTextBox" + 1 + "", "addElementWithText('');", true);
        }


    }

    void ClearForm()
    {
        drpCancellationPolicy.SelectedIndex = 0;
        drpSelectCurrency.SelectedIndex = 0;
        txtCheckCommitionButton.Text = "";
        txtIntervalVariationMax.Text = "";
        txtIntervalVariationMin.Text = "";
        txtInvoiceCommissionHotel.Text = "";
        txtMinimumSpecial.Text = "";
        txtMinimumStock.Text = "";
        txtModificationTrigger.Text = "";
        txtRequiredMaximumHotelBasket.Text = "";
        txtSetAgencyComm.Text = "";
    }

    private void ApplyPaging()
    {
        try
        {
            GridViewRow row = grdViewCountryOthers.TopPagerRow;
            if (row != null)
            {
                PlaceHolder ph;
                LinkButton lnkPaging;
                LinkButton lnkPrevPage;
                LinkButton lnkNextPage;
                lnkPrevPage = new LinkButton();
                lnkPrevPage.CssClass = "pre";
                lnkPrevPage.Width = Unit.Pixel(73);
                lnkPrevPage.CommandName = "Page";
                lnkPrevPage.CommandArgument = "prev";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPrevPage);
                if (grdViewCountryOthers.PageIndex == 0)
                {
                    lnkPrevPage.Enabled = false;

                }
                for (int i = 1; i <= grdViewCountryOthers.PageCount; i++)
                {
                    lnkPaging = new LinkButton();
                    if (ViewState["CurrentPage"] != null)
                    {
                        if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    else
                    {
                        if (i == 1)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    lnkPaging.Width = Unit.Pixel(16);
                    lnkPaging.Text = i.ToString();
                    lnkPaging.CommandName = "Page";
                    lnkPaging.CommandArgument = i.ToString();
                    if (i == grdViewCountryOthers.PageIndex + 1)
                        ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkPaging);
                }
                lnkNextPage = new LinkButton();
                lnkNextPage.CssClass = "nex";
                lnkNextPage.Width = Unit.Pixel(42);
                lnkNextPage.CommandName = "Page";
                lnkNextPage.CommandArgument = "next";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkNextPage);
                ph = (PlaceHolder)row.FindControl("ph");
                if (grdViewCountryOthers.PageIndex == grdViewCountryOthers.PageCount - 1)
                {
                    lnkNextPage.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            //logger.Error(ex);
        }
    }
    #endregion

    #region Events
    protected void grdViewCountryOthers_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int intDataKey = Convert.ToInt32(grdViewCountryOthers.DataKeys[e.Row.RowIndex].Value);

            TList<Others> obj = objOthers.GetInfoByCountryID(intDataKey);
            Label lblCurrency = (Label)e.Row.FindControl("lblCurrency");
            Label lblIntervalMin = (Label)e.Row.FindControl("lblIntervalMin");
            Label lblIntervalMax = (Label)e.Row.FindControl("lblIntervalMax");
            Label lblSpecial = (Label)e.Row.FindControl("lblSpecial");

            if (obj.Count > 0)
            {
                Others objRow = obj.FirstOrDefault();
                if (objRow.CurrencyId != null)
                {
                    lblCurrency.Text = objOthers.GetCurrencyByID(Convert.ToInt32(objRow.CurrencyId));
                }
                else
                {
                    lblCurrency.Text = "N/A";
                }

                if (objRow.MinIntervalVariation != null)
                {
                    lblIntervalMin.Text = objRow.MinIntervalVariation.ToString();
                }
                else
                {
                    lblIntervalMin.Text = "N/A";
                }

                if (objRow.MaxIntervalVariation != null)
                {
                    lblIntervalMax.Text = objRow.MaxIntervalVariation.ToString();
                }
                else
                {
                    lblIntervalMax.Text = "N/A";
                }

                if (objRow.MinConsiderSpl != null)
                {
                    lblSpecial.Text = objRow.MinConsiderSpl.ToString();
                }
                else
                {
                    lblSpecial.Text = "N/A";
                }
            }
            else
            {
                lblCurrency.Text = "N/A";
                lblIntervalMin.Text = "N/A";
                lblIntervalMax.Text = "N/A";
                lblSpecial.Text = "N/A";

            }

        }
    }
    protected void lnkCancelOthers_Click(object sender, EventArgs e)
    {
        divmessage.Attributes.Add("class", "error");
        divmessage.Style.Add("display", "none");
        divAddUpdateCountryOthers.Style.Add("display", "none");
        ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "CloseCountryOthersPopUp();", true);
    }
    protected void lnkSaveOthers_Click(object sender, EventArgs e)
    {
        TList<Others> obj = objOthers.GetInfoByCountryID(Convert.ToInt32(hdnCountryID.Value));
        if (obj.Count > 0)
        {
            Others objRow = obj.FirstOrDefault();
            objRow.CurrencyId = Convert.ToInt32(drpSelectCurrency.SelectedValue);
            //objRow.InvoiceComm = Convert.ToInt32(txtInvoiceCommissionHotel.Text);
            objRow.MaxIntervalVariation = Convert.ToInt32(txtIntervalVariationMax.Text);
            objRow.MinIntervalVariation = Convert.ToInt32(txtIntervalVariationMin.Text);
            objRow.MinConsiderSpl = Convert.ToInt32(txtMinimumSpecial.Text);
            objRow.MaxRequestBasket = Convert.ToInt32(txtRequiredMaximumHotelBasket.Text);
            objRow.RevenueModificationTrigger = Convert.ToInt32(txtModificationTrigger.Text);
            //objRow.AgencyComm = Convert.ToInt32(txtSetAgencyComm.Text);
            objRow.MinStockforHotel = Convert.ToInt32(txtMinimumStock.Text);
            objRow.CancellationPolicyId = Convert.ToInt32(drpCancellationPolicy.SelectedValue);
            if (!string.IsNullOrEmpty(txtCheckCommitionButton.Text))
            {
                objRow.CheckCommWait = Convert.ToInt32(txtCheckCommitionButton.Text);
            }
            objOthers.ClearOldLanguageMapping(Convert.ToInt32(hdnCountryID.Value));
            foreach (ListItem chkedObj in chkListLanguage.Items)
            {
                if (chkedObj.Selected == true)
                {
                    CountryLanguage objcountryLanguage = new CountryLanguage();
                    objcountryLanguage.CountryId = Convert.ToInt32(hdnCountryID.Value);
                    objcountryLanguage.LanguageId = Convert.ToInt32(chkedObj.Value);
                    objOthers.InsertLanguageMapping(objcountryLanguage);
                }
            }
            if (objOthers.UpdateOthersInfo(objRow))
            {
                objOthers.ClearQuestion(Convert.ToInt32(hdnCountryID.Value));
                for (int i = 1; i < Request.Form.Count - 1; i++)
                {
                    string strquestion = "txtQuestion" + i.ToString();
                    if (Request.Form[strquestion] != null)
                    {
                        string strQuestionValue = Page.Request.Form[strquestion].ToString();
                        HearUsQuestion objFromUs = new HearUsQuestion();
                        objFromUs.CountryId = Convert.ToInt32(hdnCountryID.Value);
                        objFromUs.Question = strQuestionValue;
                        objOthers.InsertQuestion(objFromUs);
                    }
                }

                TList<Hotel> objHotels = new TList<Hotel>();
                objHotels = objHotel.GetHotelByWhereAndOrderby("CountryID=" + Convert.ToInt32(hdnCountryID.Value) + "", string.Empty, string.Empty);
                if (objHotels.Count > 0)
                {
                    int intindex = 0;
                    for (intindex = 0; intindex <= objHotels.Count - 1; intindex++)
                    {
                        Hotel objHotelRow = new Hotel();
                        objHotelRow = objHotels[intindex];
                        objHotelRow.CurrencyId = Convert.ToInt32(drpSelectCurrency.SelectedValue);
                        objHotel.UpdateHotel(objHotelRow);
                    }
                }
                objCancellationManager.SetDefaultPolicyOfCountry(Convert.ToInt32(drpCancellationPolicy.SelectedValue));
                divmessage.InnerHtml = "data saved successfully.";
                divmessage.Attributes.Add("class", "succesfuly");
                divmessage.Style.Add("display", "block");
            }
            else
            {

                divmessage.InnerHtml = "data not saved successfully.";
                divmessage.Attributes.Add("class", "error");
                divmessage.Style.Add("display", "block");
            }

        }
        else
        {
            Others objaddrow = new Others();
            objaddrow.CurrencyId = Convert.ToInt32(drpSelectCurrency.SelectedValue);
            //objaddrow.InvoiceComm = Convert.ToInt32(txtInvoiceCommissionHotel.Text);
            objaddrow.MaxIntervalVariation = Convert.ToInt32(txtIntervalVariationMax.Text);
            objaddrow.MinIntervalVariation = Convert.ToInt32(txtIntervalVariationMin.Text);
            objaddrow.MinConsiderSpl = Convert.ToInt32(txtMinimumSpecial.Text);
            objaddrow.RevenueModificationTrigger = Convert.ToInt32(txtModificationTrigger.Text);
            objaddrow.MaxRequestBasket = Convert.ToInt32(txtRequiredMaximumHotelBasket.Text);
            //objaddrow.AgencyComm = Convert.ToInt32(txtSetAgencyComm.Text);
            objaddrow.MinStockforHotel = Convert.ToInt32(txtMinimumStock.Text);
            objaddrow.CancellationPolicyId = Convert.ToInt32(drpCancellationPolicy.SelectedValue);
            objaddrow.CheckCommWait = Convert.ToInt32(txtCheckCommitionButton.Text);
            objOthers.ClearOldLanguageMapping(Convert.ToInt32(hdnCountryID.Value));
            foreach (ListItem chkedObj in chkListLanguage.Items)
            {
                if (chkedObj.Selected == true)
                {
                    CountryLanguage objcountryLanguage = new CountryLanguage();
                    objcountryLanguage.CountryId = Convert.ToInt32(hdnCountryID.Value);
                    objcountryLanguage.LanguageId = Convert.ToInt32(chkedObj.Value);
                    objOthers.InsertLanguageMapping(objcountryLanguage);
                }
            }
            if (objOthers.UpdateOthersInfo(objaddrow))
            {
                objOthers.ClearQuestion(Convert.ToInt32(hdnCountryID.Value));
                for (int i = 1; i < Request.Form.Count - 1; i++)
                {
                    string strquestion = "txtQuestion" + i.ToString();
                    if (Request.Form[strquestion] != null)
                    {
                        string strQuestionValue = Page.Request.Form[strquestion].ToString();
                        HearUsQuestion objFromUs = new HearUsQuestion();
                        objFromUs.CountryId = Convert.ToInt32(hdnCountryID.Value);
                        objFromUs.Question = strQuestionValue;
                        objOthers.InsertQuestion(objFromUs);
                    }
                }

                TList<Hotel> objHotels = new TList<Hotel>();
                objHotels = objHotel.GetHotelByWhereAndOrderby("CountryID=" + Convert.ToInt32(hdnCountryID.Value) + "", string.Empty, string.Empty);
                if (objHotels.Count > 0)
                {
                    int intindex = 0;
                    for (intindex = 0; intindex <= objHotels.Count - 1; intindex++)
                    {
                        Hotel objHotelRow = new Hotel();
                        objHotelRow = objHotels[intindex];
                        objHotelRow.CurrencyId = Convert.ToInt32(drpSelectCurrency.SelectedValue);
                        objHotel.UpdateHotel(objHotelRow);
                    }
                }

                objCancellationManager.SetDefaultPolicyOfCountry(Convert.ToInt32(drpCancellationPolicy.SelectedValue));
                divmessage.InnerHtml = "data saved successfully.";
                divmessage.Attributes.Add("class", "succesfuly");
                divmessage.Style.Add("display", "block");
            }
            else
            {
                divmessage.InnerHtml = "data not saved successfully.";
                divmessage.Attributes.Add("class", "error");
                divmessage.Style.Add("display", "block");
            }

        }
        BindCountryListView();
        ApplyPaging();
        ClearForm();
    }
    protected void grdViewCountryOthers_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ManageOthers")
        {

            hdnCountryID.Value = Convert.ToString(e.CommandArgument);
            BindChekBoxListLanguage();
            BindDropDownCurrency();
            BindCancellationPolicy();
            GetOldInfo(Convert.ToInt32(e.CommandArgument));
            divmessage.Attributes.Add("class", "error");
            divmessage.Style.Add("display", "none");
            divAddUpdateCountryOthers.Style.Add("display", "block");
            ScriptManager.RegisterStartupScript(this, typeof(Page), "OpenPopUp", "OpenCountryOthersPopUp();", true);

        }
    }
    #endregion

    protected void grdViewCountryOthers_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            grdViewCountryOthers.PageIndex = e.NewPageIndex;
            ViewState["CurrentPage"] = e.NewPageIndex;
            BindCountryListView();
            ApplyPaging();
        }
        catch (Exception ex)
        {

        }

    }
}