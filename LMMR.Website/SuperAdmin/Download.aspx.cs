﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;

public partial class SuperAdmin_Download : System.Web.UI.Page
{
    String strAttachmentsPath = String.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        strAttachmentsPath = ConfigurationManager.AppSettings["FilePath"].ToString();
        string filename = Request["file"].ToString(); 
        string path =  Server.MapPath(ConfigurationManager.AppSettings["FilePath"].ToString() + "WLCodeFile/") + filename;
        DownloadFile(path);

    }

    private void DownloadFile(string serverFilePath)
    {
        FileInfo file = new FileInfo(serverFilePath);
        if (file.Exists == true)
        {
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            Response.AddHeader("Content-Length", file.Length.ToString());
            Response.ContentType = "application/octet-stream";
            Response.WriteFile(file.FullName);
            Response.End();

        }

    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        string strDisAbleBackButton;
        strDisAbleBackButton = "<script language='javascript'>\n";
        strDisAbleBackButton += "window.history.forward(1);\n"; strDisAbleBackButton += "\n</script>";
        ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "clientScript", strDisAbleBackButton);
    }
}