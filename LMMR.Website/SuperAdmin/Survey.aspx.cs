﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Data;
using LMMR.Entities;
using System.Data;
using System.Xml;

public partial class SuperAdmin_Survey : System.Web.UI.Page
{

    surveybookingrequest objsurveybookingrequest = new surveybookingrequest();
    ServeyAnswer insertsurvey = new ServeyAnswer();
    TList<ServeyAnswer> objsurvey = new TList<ServeyAnswer>();
    String status;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
            // By default the bookings rankings are displayed below.
            radioBookinngs.Attributes.Add("class", "btn-active");
            radioRequests.Attributes.Add("class", "btn");
            

            bookingDIV.Visible = true;
            bookingSaveCancelDIV.Visible = true;
            
            bindallquestions();
        }

    }
    

    public void bindallquestions()
    {
        objsurvey = objsurveybookingrequest.Getsurveybooking();
        rptsurveyquestions.DataSource = objsurvey;
        rptsurveyquestions.DataBind();
        objsurvey = objsurveybookingrequest.Getsurveyrequest();
        rptSurveyrequest.DataSource = objsurvey;
        rptSurveyrequest.DataBind();
    }
    protected void rptsurveyquestions_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblString = (Label)e.Item.FindControl("lblString");
            TextBox txtString = (TextBox)e.Item.FindControl("txtString");
            HiddenField hdnKey = (HiddenField)e.Item.FindControl("hdnKey");
            hdnKey.Value = DataBinder.Eval(e.Item.DataItem, "AnswerID").ToString();
            //lblString.Text = DataBinder.Eval(e.Item.DataItem, "AnswerID").ToString();
            txtString.Text = DataBinder.Eval(e.Item.DataItem, "Answer").ToString();
            ServeyAnswer surveyanswer = e.Item.DataItem as ServeyAnswer;
            //lblString.Text = surveyanswer.AnswerId.ToString();
            int answerid = surveyanswer.AnswerId;
            switch (answerid)
            {
                case 1: lblString.Text = "Question 1";
                    break;
                case 2: lblString.Text = "Question 2";
                    break;
                case 3: lblString.Text = "Question 3";
                    break;
                case 4: lblString.Text = "Question 4";
                    break;
                case 5: lblString.Text = "Question 5";
                    break;
                case 6: lblString.Text = "Question 6";
                    break;
                case 7: lblString.Text = "Question 7";
                    break;
                case 8: lblString.Text = "Question 8";
                    break;
                case 9: lblString.Text = "Question 9";
                    break;
                case 10: lblString.Text = "Question 10";
                    break;
                case 11: lblString.Text = "Question 11";
                    break;
            }
        }
    }
    protected void rptSurveyrequest_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblString = (Label)e.Item.FindControl("lblString");
            TextBox txtString = (TextBox)e.Item.FindControl("txtString");
            HiddenField hdnKey = (HiddenField)e.Item.FindControl("hdnKey");
            hdnKey.Value = DataBinder.Eval(e.Item.DataItem, "AnswerID").ToString();
            //lblString.Text = DataBinder.Eval(e.Item.DataItem, "AnswerID").ToString();
            txtString.Text = DataBinder.Eval(e.Item.DataItem, "Answer").ToString();
            ServeyAnswer surveyanswer = e.Item.DataItem as ServeyAnswer;
            //lblString.Text = surveyanswer.AnswerId.ToString();
            int answerid = surveyanswer.AnswerId;
            switch (answerid)
            {
                
                case 12: lblString.Text = "Question 1";
                    break;
                case 13: lblString.Text = "Question 2";
                    break;
                case 14: lblString.Text = "Question 3";
                    break;
                case 15: lblString.Text = "Question 4";
                    break;
                case 16: lblString.Text = "Question 5";
                    break;
                case 17: lblString.Text = "Question 6";
                    break;
                case 18: lblString.Text = "Question 7";
                    break;
                case 19: lblString.Text = "Question 8";
                    break;
                case 20: lblString.Text = "Question 9";
                    break;
                case 21: lblString.Text = "Question 10";
                    break;
                case 22: lblString.Text = "Question 11";
                    break;
            }
        }
    }


    protected void radioBookinngs_CheckedChanged(object sender, EventArgs e)
    {
        divmessage.Style.Add("display", "none");
        divmessage.Attributes.Add("class", "error");
        bookingDIV.Visible = true;
        bookingSaveCancelDIV.Visible = true;
        requestDIV.Visible = false;
        requestSaveCancelDIV.Visible = false;

        radioBookinngs.Attributes.Add("class", "btn-active");
        radioRequests.Attributes.Add("class", "btn");
        
    }

    /// <summary>
    /// This the event handler for radioRequests RadioButton checkedChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void radioRequests_CheckedChanged(object sender, EventArgs e)
    {
        divmessage.Style.Add("display", "none");
        divmessage.Attributes.Add("class", "error");
        bookingDIV.Visible = false;
        bookingSaveCancelDIV.Visible = false;
        requestDIV.Visible = true;
        requestSaveCancelDIV.Visible = true;
        radioBookinngs.Attributes.Add("class", "btn");
        radioRequests.Attributes.Add("class", "btn-active");
        
    }

    protected void lbtSaveBooking_Click(object sender, EventArgs e)
    {
        foreach (RepeaterItem r in rptsurveyquestions.Items)
        {
            HiddenField hdnKey = (HiddenField)r.FindControl("hdnKey");
            TextBox txtString = (TextBox)r.FindControl("txtString");
            Label lblString = (Label)r.FindControl("lblString");
            if (hdnKey.Value != null)
            {
                insertsurvey = objsurveybookingrequest.GetbySurveyid(Convert.ToInt32(hdnKey.Value));
                insertsurvey.Answer = txtString.Text;
                insertsurvey.AnswerType = true;
                status = objsurveybookingrequest.UpdateSurvey(insertsurvey);
            }

        }
        
        if (status == "Information Updated successfully.")
        {
            bindallquestions();
            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "succesfuly");
            divmessage.InnerHtml = "Survey Booking Question updated successfully";
            radioBookinngs.Attributes.Add("class", "btn-active");
            radioRequests.Attributes.Add("class", "btn");
            bookingDIV.Visible = true;
            bookingSaveCancelDIV.Visible = true;
        }
        else
        {
            divmessage.Attributes.Add("class", "error");
            divmessage.Style.Add("display", "block");
            divmessage.InnerHtml = status;
            radioBookinngs.Attributes.Add("class", "btn-active");
            radioRequests.Attributes.Add("class", "btn");
            bookingDIV.Visible = true;
            bookingSaveCancelDIV.Visible = true;
        }
    }

    protected void lbtSaveRequest_Click(object sender, EventArgs e)
    {
        foreach (RepeaterItem r in rptSurveyrequest.Items)
        {
            HiddenField hdnKey = (HiddenField)r.FindControl("hdnKey");
            TextBox txtString = (TextBox)r.FindControl("txtString");
            Label lblString = (Label)r.FindControl("lblString");
            if (hdnKey.Value != null)
            {
                insertsurvey = objsurveybookingrequest.GetbySurveyid(Convert.ToInt32(hdnKey.Value));
                insertsurvey.Answer = txtString.Text;
                insertsurvey.AnswerType = false;
                status = objsurveybookingrequest.UpdateSurvey(insertsurvey);
            }

        }

        if (status == "Information Updated successfully.")
        {
            bindallquestions();
            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "succesfuly");
            divmessage.InnerHtml = "Survey request Question updated successfully";
            bookingDIV.Visible = false;
            bookingSaveCancelDIV.Visible = false;
            requestDIV.Visible = true;
            requestSaveCancelDIV.Visible = true;
            radioBookinngs.Attributes.Add("class", "btn");
            radioRequests.Attributes.Add("class", "btn-active");
        }
        else
        {
            divmessage.Attributes.Add("class", "error");
            divmessage.Style.Add("display", "block");
            divmessage.InnerHtml = status;
            bookingDIV.Visible = false;
        bookingSaveCancelDIV.Visible = false;
        requestDIV.Visible = true;
        requestSaveCancelDIV.Visible = true;
        radioBookinngs.Attributes.Add("class", "btn");
        radioRequests.Attributes.Add("class", "btn-active");
        }
 
        
        
    }
}