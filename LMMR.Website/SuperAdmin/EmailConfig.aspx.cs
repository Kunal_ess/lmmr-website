﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Data;
using LMMR.Entities;
using System.IO;
#endregion


public partial class SuperAdmin_EmailConfig : System.Web.UI.Page
{
    #region Variables and Properties
    EmailConfigManager em = new EmailConfigManager();
    LanguageManager lm = new LanguageManager();

    public EmailConfig eCONFIG
    {
        get { return ViewState["_eCONFIG"] as EmailConfig; }
        set { ViewState["_eCONFIG"] = value; }
    }
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl + "/";
        }
    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentSuperAdminID"] == null)
        {
            //Response.Redirect("~/login.aspx");
            Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
            Session.Abandon();
            if (l != null)
            {
                Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
            }
            else
            {
                Response.Redirect(SiteRootPath + "login/english");
            }
        }
        if (!Page.IsPostBack)
        {
            pnlEmailDetails.Visible = false;
            BindEmailsList();
        }
    }
    #endregion

    #region Methods
    /// <summary>
    /// Bind List of Emails From the Database.
    /// </summary>
    protected void BindEmailsList()
    {
        TList<EmailConfig> econfig = em.GetAll();
        rptEventDriven.DataSource = econfig.Where(a => a.EmailConfigType == (int)EmailConfigType.EVENTDRIVEN);
        rptEventDriven.DataBind();
        rptPeriodic.DataSource = econfig.Where(a => a.EmailConfigType == (int)EmailConfigType.PERIODIC);
        rptPeriodic.DataBind();
    }
    /// <summary>
    /// Language Tab binding
    /// </summary>
    /// <param name="Id"></param>
    public void BindForm(long Id)
    {
        pnlEmailDetails.Visible = true;
        if (eCONFIG == null)
        {
            eCONFIG = em.GetById(Id);
        }
        if (eCONFIG != null)
        {
            ltrLanguage.Text = "";
            TList<Language> AllLanguage = lm.GetData();
            if (string.IsNullOrEmpty(hdnLanguageID.Value))
            {
                hdnLanguageID.Value = AllLanguage.FirstOrDefault().Id.ToString();
            }
            foreach (Language l in AllLanguage)
            {
                EmailConfigMapping emp = eCONFIG.EmailConfigMappingCollection.Where(a => a.LanguageId == l.Id).FirstOrDefault();
                if (emp != null)
                {
                    if (hdnLanguageID.Value == Convert.ToString(l.Id))
                    {
                        ltrLanguage.Text += "<li><a href='javascript:void(0)' class='select' onclick='SetLanguage(" + l.Id + ");'>" + l.Name + "</a></li>";
                    }
                    else if (string.IsNullOrEmpty(emp.EmailContents))
                    {
                        ltrLanguage.Text += "<li class='redtab'><a href='javascript:void(0)' onclick='SetLanguage(" + l.Id + ");'>" + l.Name + "</a></li>";
                    }
                    else
                    {
                        ltrLanguage.Text += "<li ><a href='javascript:void(0)'  onclick='SetLanguage(" + l.Id + ");'>" + l.Name + "</a></li>";
                    }
                }
                else
                {
                    if (hdnLanguageID.Value == Convert.ToString(l.Id))
                    {
                        ltrLanguage.Text += "<li class='redtab'><a href='javascript:void(0)' class='select' onclick='SetLanguage(" + l.Id + ");'>" + l.Name + "</a></li>";
                    }
                    else
                    {
                        ltrLanguage.Text += "<li class='redtab'><a href='javascript:void(0)' onclick='SetLanguage(" + l.Id + ");'>" + l.Name + "</a></li>";
                    }
                }
            }
            lblEmailName.Text = eCONFIG.EmailName;
            if (eCONFIG.EmailConfigType == 1)
            {
                lblEmailType.Text = "Periodic";
                pnlPeriodic.Visible = true;
                txtPeriodOfCall.Text = Convert.ToString(eCONFIG.PeriodOfCall);
                if (eCONFIG.TypeOfPeriod == 0)
                {
                    drpPeriodType.Style.Add("display", "none");
                    lblPeriodType.Style.Add("display", "block");
                    drpPeriodType.SelectedValue = "0";
                    lblPeriodType.Text = "Duration Based";
                }
                else
                {
                    drpPeriodType.Style.Add("display", "none");
                    lblPeriodType.Style.Add("display", "block");
                    drpPeriodType.SelectedValue = "1";
                    lblPeriodType.Text = "Date Based";
                }
            }
            else
            {
                lblEmailType.Text = "Event Driven";
                pnlPeriodic.Visible = false;
            }
            if (eCONFIG.IsActive)
            {
                drpIsActive.SelectedValue = "1";
            }
            else
            {
                drpIsActive.SelectedValue = "0";
            }
            //drpIsActive.Focus();
            EmailConfigMapping ecmnew = eCONFIG.EmailConfigMappingCollection.Where(a => a.LanguageId == Convert.ToInt64(hdnLanguageID.Value)).FirstOrDefault();
            if (ecmnew != null)
            {
                CKEditorContents.Text = ecmnew.EmailContents;
            }
            else
            {
                CKEditorContents.Text = "";
            }
            ltrParameters.Text = eCONFIG.PerameterSupported;
        }
        else
        {
            pnlEmailDetails.Visible = false;
        }
    }
    /// <summary>
    /// Clear for details
    /// </summary>
    public void ClearForm()
    {
        lblEmailType.Text = "";
        lblEmailName.Text = "";
        CKEditorContents.Text = "";
        ltrParameters.Text = "";
        hdnLanguageID.Value = string.Empty;
    }
    #endregion

    #region Events
    /// <summary>
    /// Item Databound for Event driven mails
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rptEventDriven_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton lnkEmail = (LinkButton)e.Item.FindControl("lnkEmail");
            EmailConfig ec = e.Item.DataItem as EmailConfig;
            if (ec != null)
            {
                lnkEmail.Text = ec.EmailName;
                lnkEmail.CommandName = "Edit_Emails";
                if (!ec.IsActive)
                {
                    lnkEmail.ForeColor = System.Drawing.Color.Red;
                }
                lnkEmail.CommandArgument = Convert.ToString(ec.Id);
                divMessage.InnerHtml = "";
                divMessage.Attributes.Add("class", "");
            }
        }
    }
    /// <summary>
    /// Item Command for Event driven mails
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void rptEventDriven_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Edit_Emails")
        {
            hdnItemId.Value = e.CommandArgument.ToString();
            eCONFIG = null;
            hdnLanguageID.Value = "";
            hdnOldLanguageID.Value = "";
            BindForm(Convert.ToInt64(e.CommandArgument.ToString()));
            divMessage.InnerHtml = "";
            divMessage.Attributes.Add("class", "");
            divLowerMessage.InnerHtml = "";
            divLowerMessage.Attributes.Add("class", "");

            ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + pnlEmailDetails.ClientID + "');});", true);
        }
    }
    /// <summary>
    /// Item Datadound for Periodic Mails
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rptPeriodic_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton lnkEmail = (LinkButton)e.Item.FindControl("lnkEmail");
            EmailConfig ec = e.Item.DataItem as EmailConfig;
            if (ec != null)
            {
                lnkEmail.Text = ec.EmailName;
                lnkEmail.CommandName = "Edit_Emails";
                if (!ec.IsActive)
                {
                    lnkEmail.ForeColor = System.Drawing.Color.Red;
                }
                lnkEmail.CommandArgument = Convert.ToString(ec.Id);
                divMessage.InnerHtml = "";
                divMessage.Attributes.Add("class", "");
            }
        }
    }
    /// <summary>
    /// Item Command for Periodic Mails
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void rptPeriodic_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Edit_Emails")
        {
            hdnItemId.Value = e.CommandArgument.ToString();
            eCONFIG = null;
            hdnLanguageID.Value = "";
            hdnOldLanguageID.Value = "";
            BindForm(Convert.ToInt64(e.CommandArgument.ToString()));
            divMessage.InnerHtml = "";
            divMessage.Attributes.Add("class", "");
            divLowerMessage.InnerHtml = "";
            divLowerMessage.Attributes.Add("class", "");

            ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + pnlEmailDetails.ClientID + "');});", true);
        }
    }
    /// <summary>
    /// Cancel linkbutton
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkCancel_Click(object sender, EventArgs e)
    {
        ClearForm();
        eCONFIG = null;
        pnlEmailDetails.Visible = false;
        BindEmailsList();
        divMessage.InnerHtml = "";
        divMessage.Attributes.Add("class", "");
        divLowerMessage.InnerHtml = "";
        divLowerMessage.Attributes.Add("class", "");
        hdnLanguageID.Value = string.Empty;

    }
    /// <summary>
    /// Save linkbutton
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkSave_Click(object sender, EventArgs e)
    {
        eCONFIG.IsActive = Convert.ToBoolean(Convert.ToInt32(drpIsActive.SelectedValue));
        eCONFIG.PeriodOfCall = Convert.ToInt32(string.IsNullOrEmpty(txtPeriodOfCall.Text) ? "0" : txtPeriodOfCall.Text);
        eCONFIG.TypeOfPeriod = Convert.ToInt32(drpPeriodType.SelectedValue);
        EmailConfigMapping ecm = eCONFIG.EmailConfigMappingCollection.Where(a => a.LanguageId == Convert.ToInt64(hdnLanguageID.Value)).FirstOrDefault();
        if (ecm != null)
        {
            ecm.EmailContents = CKEditorContents.Text;
        }
        else
        {
            ecm = new EmailConfigMapping();
            ecm.LanguageId = Convert.ToInt64(hdnLanguageID.Value);
            ecm.EmailConfigureId = Convert.ToInt64(hdnItemId.Value);
            ecm.EmailContents = CKEditorContents.Text;
            ecm.IsNew = true;
            eCONFIG.EmailConfigMappingCollection.Add(ecm);
        }
        EmailConfigMapping ecmCheckEnglish = eCONFIG.EmailConfigMappingCollection.Where(a => a.LanguageId == Convert.ToInt64(1)).FirstOrDefault();
        if (ecmCheckEnglish == null)
        {
            divLowerMessage.InnerHtml = "Please enter content of English language.";
            divLowerMessage.Attributes.Add("class", "error");
            return;
        }
        else
        {
            if (string.IsNullOrEmpty(ecmCheckEnglish.EmailContents) || ecmCheckEnglish.EmailContents == "<p></p>")
            {
                divLowerMessage.InnerHtml = "Please enter content of English language.";
                divLowerMessage.Attributes.Add("class", "error");
                return;
            }
        }

        if (em.SaveRecord(eCONFIG, true))
        {
            pnlEmailDetails.Visible = false;
            ClearForm();
            BindEmailsList();
            divMessage.InnerHtml = "Email details save successfully.";
            divMessage.Attributes.Add("class", "succesfuly");
            divLowerMessage.InnerHtml = "";
            divLowerMessage.Attributes.Add("class", "");

        }
        else
        {
            divLowerMessage.InnerHtml = "Error occured on record saving.";
            divLowerMessage.Attributes.Add("class", "error");
        }
    }
    /// <summary>
    /// Set language button to save tem entry in Database
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSetLanguage_Click(object sender, EventArgs e)
    {
        EmailConfigMapping ecm = eCONFIG.EmailConfigMappingCollection.Where(a => a.LanguageId == Convert.ToInt64(string.IsNullOrEmpty(hdnOldLanguageID.Value) ? "1" : hdnOldLanguageID.Value)).FirstOrDefault();
        if (ecm != null)
        {
            ecm.EmailContents = CKEditorContents.Text;
        }
        else
        {
            ecm = new EmailConfigMapping();
            ecm.LanguageId = Convert.ToInt64(hdnOldLanguageID.Value);
            ecm.EmailConfigureId = Convert.ToInt64(hdnItemId.Value);
            ecm.EmailContents = CKEditorContents.Text;
            ecm.IsNew = true;
            eCONFIG.EmailConfigMappingCollection.Add(ecm);
        }
        BindForm(Convert.ToInt64(hdnItemId.Value));
        divLowerMessage.InnerHtml = "";
        divLowerMessage.Attributes.Add("class", "");
        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + pnlEmailDetails.ClientID + "');});", true);
    }
    #endregion
}