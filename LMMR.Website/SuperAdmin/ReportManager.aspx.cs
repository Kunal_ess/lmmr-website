﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using System.IO;
using System.Text;
using System.Net;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Configuration;
using System.Xml;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;

public partial class SuperAdmin_Report_Manager : System.Web.UI.Page
{
    #region Variables and Properties
    HotelInfo objHotelInfo = new HotelInfo();
    ReportCreation rc = new ReportCreation();
    Viewstatistics objviewstat = new Viewstatistics();

    HotelManager objHotelManager = new HotelManager();
    public static  List<string> objHotelList = new List<string>();
    DateTime dt;
    string dtformat;
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            if (string.IsNullOrEmpty(appRootUrl) || appRootUrl == "/")
            {
                return host + "/";
            }
            else
            {
                return host + appRootUrl + "/";
            }
        }
    }

    public string ReportName
    {
        get
        {
            switch (lblReportName.Text.ToLower())
            {
                case "lead time report":
                    return "lead time report";                    
                case "availability report":
                    return "availability report";                  
                case "profile company / agency report":
                    return "profile company / agency report";
                case "cancelation report":
                    return "cancelation report";
                case "booking / request report":
                    return "booking / request report";
                case "production company / meet2 report":
                    return "production company / meet2 report";
                case "inventory report of existing hotel":
                    return "inventory report of existing hotel";
                case "added hotel in period inventory report":
                    return "added hotel in period inventory report";
                case "conversition request report":
                    return "conversition request report";
                case "revenue report":
                    return "revenue report";
                case "newsletter report":
                    return "newsletter report";
                case "ranking report":
                    return "ranking report";
                case "online inventory changes report":
                    return "online inventory changes report";
                case "production report hotel and meeting facilities":
                    return "Production Report Hotel and Meeting facilities";
                case "commission control report":
                    return "commission control report";
                case "conversion booking report":
                    return "conversion booking report";
                default:
                    return "no report";
            }
        }
    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
    
        ucReportSearch.SearchButtonClick += new EventHandler(ucReportSearch_SearchButtonClick);
        ucReportSearch.ClearButtonClick += new EventHandler(ucReportSearch_ClearButtonClick);
        if (!Page.IsPostBack)
        {
            DivReportFilter.Visible = false;
            pnlExportReport.Visible = false;
            result.Visible = false;
            //tdEmptydata.Visible = false;
            
        }
    }
    #endregion

    #region Clear Button Event
    void ucReportSearch_ClearButtonClick(object sender, EventArgs e)
    {
        pnlExportReport.Visible = false;
        result.Visible = false;
    }
    #endregion

    #region Report Search button click
    void ucReportSearch_SearchButtonClick(object sender, EventArgs e)
    {
        string whereclause = "";
        if (ucReportSearch.propCountryID != 0)
        {
            if (whereclause.Length > 0)
            {
                whereclause += " AND ";
            }
            switch (lblReportName.Text.ToLower())
            {
                case "inventory report of existing hotel":
                    whereclause += "Countryname like '" + ucReportSearch.propCountryName + "%'";
                    break;
                default:
                    whereclause += "CountryId=" + ucReportSearch.propCountryID;
                    break;
            }
            
        }
        if (ucReportSearch.propCityID != 0)
        {
            if (whereclause.Length > 0)
            {
                whereclause += " AND ";
            }
            switch (lblReportName.Text.ToLower())
            {
                case "production company / meet2 report":
                    whereclause += "CityName like '" + ucReportSearch.propCityName + "%'";
                    break;
                case "inventory report of existing hotel":
                    whereclause += "city like '" + ucReportSearch.propCityName + "%'";
                    break;
                case "profile company / agency report":
                    whereclause += "CityName like '" + ucReportSearch.propCityName + "%'";
                    break;
                default:
                    whereclause += "CityId=" + ucReportSearch.propCityID;
                    break;
            }


        }
        else
        {
            //ucReportSearch.propCityID = 0;
        }
       
        if (ucReportSearch.propProfileName !=null)
        {
            
            switch (lblReportName.Text.ToLower())
            {                                    
                case "booking / request report":
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }

                    if (ucReportSearch.propProfileName == 0)
                    {
                        whereclause += "Booktype='" + ucReportSearch.propProfileName + "'";
                    }
                    else
                    {
                        whereclause += "Booktype in ('" + ucReportSearch.propProfileName + "',2)";
                    }
                    break;
                case "production company / meet2 report":
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }
                    if (ucReportSearch.propProfileName == 9)
                    {
                        whereclause += "Usertype='" + ucReportSearch.propProfileName + "'";
                    }
                    else
                    {
                        whereclause += "Usertype in ('" + ucReportSearch.propProfileName + "',8)";
                    }
                   
                    break;
                case "commission control report":
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }
                    //if (ucReportSearch.propProfileName == 0)
                    //{
                        whereclause += "BookType='0' and RequestStatus=6 ";
                    //}
                    //else
                    //{
                    //    whereclause += "BookType in ('" + ucReportSearch.propProfileName + "',2) and RequestStatus=7 ";
                    //}
                    break;
                case "revenue report":
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }
                    if (ucReportSearch.propProfileName == 0)
                    {
                        whereclause += "BookType='" + ucReportSearch.propProfileName + "' and RequestStatus=6 ";
                    }
                    else
                    {
                        whereclause += "BookType in ('" + ucReportSearch.propProfileName + "',2) and RequestStatus=7 ";
                    }
                    break;
                case "profile company / agency report":
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }

                    whereclause += "Usertype='" + ucReportSearch.propProfileName + "' ";
                    break;
            }
        }
        if (ucReportSearch.propFromDate != DateTime.MinValue)
        {
            switch (lblReportName.Text.ToLower())
            {
                case "lead time report":
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }
                    whereclause += "AvailabilityDate>='" + ucReportSearch.propFromDate + "' ";
                    break;
                case "availability report":
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }
                    whereclause += "AvailabilityDate>='" + ucReportSearch.propFromDate + "' ";
                    break;
                case "commission control report":
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }
                    whereclause += "BookingDate>='" + ucReportSearch.propFromDate + "' ";
                    break;
                case "revenue report":
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }
                    whereclause += "BookingDate>='" + ucReportSearch.propFromDate + "' ";
                    break;
                case "profile company / agency report":
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }
                    whereclause += "BookingDate>='" + ucReportSearch.propFromDate + "' ";
                    break;
                case "cancelation report":
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }
                    whereclause += "BookingDate>='" + ucReportSearch.propFromDate + "' ";
                    break;
                case "booking / request report":
                    if (ucReportSearch.propFromDate == null)
                    {

                    }
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }
                    whereclause += "BookingDate>='" + ucReportSearch.propFromDate + "' ";
                    break;
                case "added hotel in period inventory report":
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }
                    whereclause += "Creationdate>='" + ucReportSearch.propFromDate + "' ";
                    break;
                case "online inventory changes report":
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }
                    whereclause += "AvailabilityDate>='" + ucReportSearch.propFromDate + "' ";
                    break;
            }
        }
        else
        {
            DateTime dtm = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            TimeSpan t = dtm.Subtract(DateTime.Now);
            switch (lblReportName.Text.ToLower())
            {
                case "lead time report":
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }
                    whereclause += "AvailabilityDate>='" + DateTime.Now.AddDays(t.Days) + "' ";
                    break;
                case "availability report":
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }
                    whereclause += "AvailabilityDate>='" + DateTime.Now.AddDays(t.Days) + "' ";
                    break;
                case "commission control report":
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }
                    whereclause += "BookingDate>='" + DateTime.Now.AddDays(t.Days) + "' ";
                    break;
                case "revenue report":
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }
                    whereclause += "BookingDate>='" + DateTime.Now.AddDays(t.Days) + "' ";
                    break;
                case "profile company / agency report":
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }
                    whereclause += "BookingDate>='" + DateTime.Now.AddDays(t.Days) + "' ";
                    break;
                case "cancelation report":
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }
                    whereclause += "BookingDate>='" + DateTime.Now.AddDays(t.Days) + "' ";
                    break;
                case "booking / request report":
                    
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }
                    whereclause += "BookingDate>='" + DateTime.Now.AddDays(t.Days) + "' ";
                    break;
                
                case "added hotel in period inventory report":
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }
                    whereclause += "Creationdate>='" + DateTime.Now.AddDays(t.Days) + "' ";
                    break;
                case "online inventory changes report":
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }
                    whereclause += "AvailabilityDate>='" + DateTime.Now.AddDays(t.Days) + "' ";
                    break;
            } 
        }
        if (lblReportName.Text.ToLower() != "production company / meet2 report" && lblReportName.Text.ToLower()!="production report hotel and meeting facilities" && lblReportName.Text.ToLower() != "conversion booking report")
        {
            if (ucReportSearch.propToDate != DateTime.MinValue)
            {

                switch (lblReportName.Text.ToLower())
                {
                    case "lead time report":
                        if (whereclause.Length > 0)
                        {
                            whereclause += " AND ";
                        }
                        whereclause += "AvailabilityDate<='" + ucReportSearch.propToDate + "' ";
                        break;
                    case "availability report":
                        if (whereclause.Length > 0)
                        {
                            whereclause += " AND ";
                        }
                        whereclause += "AvailabilityDate<='" + ucReportSearch.propToDate + "' ";
                        break;
                    case "commission control report":
                        if (whereclause.Length > 0)
                        {
                            whereclause += " AND ";
                        }
                        whereclause += "BookingDate<='" + ucReportSearch.propToDate.AddDays(1) + "' ";
                        break;
                    case "revenue report":
                        if (whereclause.Length > 0)
                        {
                            whereclause += " AND ";
                        }
                        whereclause += "BookingDate<='" + ucReportSearch.propToDate.AddDays(1) + "' ";
                        break;
                    case "profile company / agency report":
                        if (whereclause.Length > 0)
                        {
                            whereclause += " AND ";
                        }
                        whereclause += "BookingDate<='" + ucReportSearch.propToDate.AddDays(1) + "' ";
                        break;
                    case "cancelation report":
                        if (whereclause.Length > 0)
                        {
                            whereclause += " AND ";
                        }
                        whereclause += "BookingDate<='" + ucReportSearch.propToDate + "' ";
                        break;
                    case "booking / request report":
                        if (whereclause.Length > 0)
                        {
                            whereclause += " AND ";
                        }
                        whereclause += " BookingDate<='" + ucReportSearch.propToDate.AddDays(1) + "' ";
                        break;
                    
                    case "added hotel in period inventory report":
                        if (whereclause.Length > 0)
                        {
                            whereclause += " AND ";
                        }
                        whereclause += "Creationdate<='" + ucReportSearch.propToDate.AddDays(1) + "' ";
                        break;
                    case "online inventory changes report":
                        if (whereclause.Length > 0)
                        {
                            whereclause += " AND ";
                        }
                        whereclause += "AvailabilityDate<='" + ucReportSearch.propToDate + "' ";
                        break;
                }
            }
            else
            {
                
                switch (lblReportName.Text.ToLower())
                {
                    case "lead time report":
                        if (whereclause.Length > 0)
                        {
                            whereclause += " AND ";
                        }
                        whereclause += "AvailabilityDate<='" + DateTime.Now + "' ";
                        break;
                    case "availability report":
                        if (whereclause.Length > 0)
                        {
                            whereclause += " AND ";
                        }
                        whereclause += "AvailabilityDate<='" + DateTime.Now + "' ";
                        break;
                    case "commission control report":
                        if (whereclause.Length > 0)
                        {
                            whereclause += " AND ";
                        }
                        whereclause += "BookingDate<='" + DateTime.Now + "' ";
                        break;
                    case "revenue report":
                        if (whereclause.Length > 0)
                        {
                            whereclause += " AND ";
                        }
                        whereclause += "BookingDate<='" + DateTime.Now + "' ";
                        break;
                    case "profile company / agency report":
                        if (whereclause.Length > 0)
                        {
                            whereclause += " AND ";
                        }
                        whereclause += "BookingDate<='" + DateTime.Now + "' ";
                        break;
                    case "cancelation report":
                        if (whereclause.Length > 0)
                        {
                            whereclause += " AND ";
                        }
                        whereclause += "BookingDate<='" + DateTime.Now + "' ";
                        break;
                    case "booking / request report":
                        if (whereclause.Length > 0)
                        {
                            whereclause += " AND ";
                        }
                        whereclause += "BookingDate<='" + DateTime.Now + "' ";
                        break;
                    
                    case "added hotel in period inventory report":
                        if (whereclause.Length > 0)
                        {
                            whereclause += " AND ";
                        }
                        whereclause += "Creationdate<='" + DateTime.Now + "' ";
                        break;
                    case "online inventory changes report":
                        if (whereclause.Length > 0)
                        {
                            whereclause += " AND ";
                        }
                        whereclause += "AvailabilityDate<='" + DateTime.Now + "' ";
                        break;
                }
            }
        }
        else
        {
            switch (lblReportName.Text.ToLower())
            {

                case "production company / meet2 report":
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }
                    whereclause += "Years ='" + ucReportSearch.Years + "' ";
                    break;
                case "production report hotel and meeting facilities":
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }
                    whereclause += "Years ='" + ucReportSearch.Years + "' ";
                    break;
                case "conversion booking report":
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }
                    whereclause += "Years ='" + ucReportSearch.Years + "' ";
                    break;
            }
            
        }
        if (ucReportSearch.PropHotelIds != "")
        {
            if (whereclause.Length > 0)
            {
                whereclause += " AND ";
            }
            switch (lblReportName.Text.ToLower())
            {
                case "lead time report":
                    whereclause += "HotelId in (" + ucReportSearch.PropHotelIds + ")";
                    break;
                case "availability report":
                    whereclause += "HotelId in (" + ucReportSearch.PropHotelIds + ")";
                    break;
                case "commission control report":
                    whereclause += "HotelID in (" + ucReportSearch.PropHotelIds + ")";
                    break;
                case "revenue report":
                    whereclause += "HotelID in (" + ucReportSearch.PropHotelIds + ")";
                    break;
                case "profile company / agency report":
                    whereclause += "UserId in (" + ucReportSearch.PropHotelIds + ")";
                    break;
                case "production company / meet2 report":
                    whereclause += "UserId in (" + ucReportSearch.PropHotelIds + ")";
                    break;
                case "Cancelation Report":
                    whereclause += "HotelId in (" + ucReportSearch.PropHotelIds + ")";
                    break;
                case "booking / request report":
                    whereclause += "HotelId in (" + ucReportSearch.PropHotelIds + ")";
                    break;
                case "ranking report":
                    whereclause += "HotelId in (" + ucReportSearch.PropHotelIds + ")";
                    break;
                case "inventory report of existing hotel":
                    whereclause += "HotelId in (" + ucReportSearch.PropHotelIds + ")";
                    break;

                case "added hotel in period inventory report":
                    whereclause += "Id in (" + ucReportSearch.PropHotelIds + ")";
                    break;

                case "online inventory changes report":
                    whereclause += "Id in (" + ucReportSearch.PropHotelIds + ")";
                    break;
                case "production report hotel and meeting facilities":
                    whereclause += "Id in (" + ucReportSearch.PropHotelIds + ")";
                    break;
                case "conversion booking report":
                    whereclause += "Id in (" + ucReportSearch.PropHotelIds + ")";
                    break;
            }            
        }
        if (ucReportSearch.propFlName != "")
        {
            switch (lblReportName.Text.ToLower())
            {
                case "newsletter report":
                    if (whereclause.Length > 0)
                    {
                        whereclause += " AND ";
                    }

                    whereclause += "(FirstName" + " Like '" + ucReportSearch.propFlName + "%' or + LastName" + " Like '" + ucReportSearch.propFlName + "%')";                    
                    break;               
            }
        }

        
            
            
         

        
        switch (lblReportName.Text.ToLower())
        {
            case "lead time report":
                LblReportFinalName.Text = lblReportName.Text;
                LeadTimeReport(whereclause);
                DivReportFilter.Visible = true;
                //pnlExportReport.Visible = true;
                //result.Visible = true;
                //lnkDownloadPDF.Visible = false;
                tblNewsletter.Visible = false;
                tblProfileCompanyReport.Visible = false;
                tblbookingrequesthistory.Visible = false;
                tblCancellationReport.Visible = false;
                LeadTimeandAvailabilityTable.Visible = true;
                tblinventoryExistingHotel.Visible = false;
                ProductionReport.Visible = false;
                tblRank.Visible = false;
				tblconv.Visible = false;
                break;
            case "availability report":
                LblReportFinalName.Text = lblReportName.Text;
                AvailabilityReport(whereclause);
                tblNewsletter.Visible = false;
                tblProfileCompanyReport.Visible = false;
                LeadTimeandAvailabilityTable.Visible = true;
                tblbookingrequesthistory.Visible = false;
                tblCancellationReport.Visible = false;
                tblinventoryExistingHotel.Visible = false;
                ProductionReport.Visible = false;                
                tblRank.Visible = false;
				tblconv.Visible = false;
                break;
            case "commission control report":
                LblReportFinalName.Text = lblReportName.Text;
                CommissionControlReport(whereclause);
                //pnlExportReport.Visible = true;
                //result.Visible = true;
                tblNewsletter.Visible = false;
                tblProfileCompanyReport.Visible = false;
                LeadTimeandAvailabilityTable.Visible = true;
                tblbookingrequesthistory.Visible = false;
                tblCancellationReport.Visible = false;
                tblinventoryExistingHotel.Visible = false;
                ProductionReport.Visible = false;                
                //lnkDownloadPDF.Visible = false;
                tblRank.Visible = false;
				tblconv.Visible = false;
                break;
            case "revenue report":
                LblReportFinalName.Text = lblReportName.Text;
                CommissionControlReport(whereclause);
                //pnlExportReport.Visible = true;
                //result.Visible = true;
                tblNewsletter.Visible = false;
                tblProfileCompanyReport.Visible = false;
                LeadTimeandAvailabilityTable.Visible = true;
                tblbookingrequesthistory.Visible = false;
                tblCancellationReport.Visible = false;
                tblinventoryExistingHotel.Visible = false;
                ProductionReport.Visible = false;
                //lnkDownloadPDF.Visible = false;
                tblRank.Visible = false;
                tblconv.Visible = false;
                break;
            case "production company / meet2 report":
                lblPReportFinalName.Text = lblReportName.Text;
                ProductionReportCompany(whereclause);
                //pnlExportReport.Visible = true;
                //result.Visible = true;
                tblNewsletter.Visible = false;
                tblProfileCompanyReport.Visible = false;
                LeadTimeandAvailabilityTable.Visible = false;
                tblbookingrequesthistory.Visible = false;
                tblCancellationReport.Visible = false;
                tblinventoryExistingHotel.Visible = false;
                ProductionReport.Visible = true;
                //lnkDownloadPDF.Visible = false;
                tblRank.Visible = false;
				tblconv.Visible = false;
                break;

            case "profile company / agency report":
                ViewReportProfileCompanyAgency(whereclause);
                tblNewsletter.Visible = false;
                tblProfileCompanyReport.Visible = true;
                tblbookingrequesthistory.Visible = false;
                tblCancellationReport.Visible = false;
                LeadTimeandAvailabilityTable.Visible = false;
                tblinventoryExistingHotel.Visible = false;
                ProductionReport.Visible = false;
                tblRank.Visible = false;
				tblconv.Visible = false;
                break;
            case "cancelation report":
                lblCancelfinalReport.Text = lblReportName.Text;
                CancelationReport(whereclause);
                tblNewsletter.Visible = false;
                tblCancellationReport.Visible = true;
                tblProfileCompanyReport.Visible = false;
                tblbookingrequesthistory.Visible = false;
                LeadTimeandAvailabilityTable.Visible = false;
                tblinventoryExistingHotel.Visible = false;
                ProductionReport.Visible = false;
                tblRank.Visible = false;
				tblconv.Visible = false;
                break;
            case "booking / request report":

                lblbrhfinalReport.Text = lblReportName.Text;
                BookingRequestreport(whereclause);
                tblNewsletter.Visible = false;
                tblProfileCompanyReport.Visible = false;
                tblbookingrequesthistory.Visible = true;
                tblCancellationReport.Visible = false;
                LeadTimeandAvailabilityTable.Visible = false;
                tblinventoryExistingHotel.Visible = false;
                ProductionReport.Visible = false;
                tblRank.Visible = false;
				tblconv.Visible = false;
                break;
            case "ranking report":
                Rankingreport(whereclause);
                //pnlExportReport.Visible = true;
                lblheading.Text = lblReportName.Text;
                tblNewsletter.Visible = false;
                //result.Visible = true;
                tblProfileCompanyReport.Visible = false;
                tblbookingrequesthistory.Visible = false;
                tblCancellationReport.Visible = false;
                LeadTimeandAvailabilityTable.Visible = false;
                tblinventoryExistingHotel.Visible = false;
                ProductionReport.Visible = false;
                tblRank.Visible = true;
                //lnkDownloadPDF.Visible = true;
                tblRank.Visible = true;
				tblconv.Visible = false;
                break;
            case "newsletter report":
                Newsletterreport(whereclause);                
                lblheading.Text = lblReportName.Text;
                
                tblProfileCompanyReport.Visible = false;
                tblbookingrequesthistory.Visible = false;
                tblCancellationReport.Visible = false;
                LeadTimeandAvailabilityTable.Visible = false;
                tblinventoryExistingHotel.Visible = false;
                ProductionReport.Visible = false;
                tblNewsletter.Visible = true;
                tblRank.Visible = false;
				tblconv.Visible = false;
                break;
            case "inventory report of existing hotel":
                invexistingreport(whereclause);
                tblNewsletter.Visible = false;
                lblinvExisthotelfinalReport.Text = lblReportName.Text;
                tblProfileCompanyReport.Visible = false;
                tblbookingrequesthistory.Visible = false;
                tblCancellationReport.Visible = false;
                LeadTimeandAvailabilityTable.Visible = false;
                tblinventoryExistingHotel.Visible = true;
                ProductionReport.Visible = false;
                tblRank.Visible = false;
                //lnkDownloadPDF.Visible = true;
				tblconv.Visible = false;
                break;
            case "added hotel in period inventory report":
                invexistingreport(whereclause);
                tblNewsletter.Visible = false;
                lblinvExisthotelfinalReport.Text = lblReportName.Text;
                tblProfileCompanyReport.Visible = false;
                tblbookingrequesthistory.Visible = false;
                tblCancellationReport.Visible = false;
                LeadTimeandAvailabilityTable.Visible = false;
                tblinventoryExistingHotel.Visible = true;
                tblRank.Visible = false;
                ProductionReport.Visible = false;
				tblconv.Visible = false;
                break;
            case "online inventory changes report":
                OnLineInventoryReport(whereclause);
                tblNewsletter.Visible = false;
                LblReportFinalName.Text = lblReportName.Text;
                tblProfileCompanyReport.Visible = false;
                tblbookingrequesthistory.Visible = false;
                tblCancellationReport.Visible = false;
                LeadTimeandAvailabilityTable.Visible = true;
                tblinventoryExistingHotel.Visible = false;
                ProductionReport.Visible = false;
                //lnkDownloadPDF.Visible = false;
                tblRank.Visible = false;
                tblconv.Visible = false;
                break;
            case "production report hotel and meeting facilities":
                ProductionReportHotel(whereclause);
                lblPReportFinalName.Text = lblReportName.Text;
                tblNewsletter.Visible = false;
                //pnlExportReport.Visible = true;
                //result.Visible = true;
                tblProfileCompanyReport.Visible = false;
                LeadTimeandAvailabilityTable.Visible = false;
                tblbookingrequesthistory.Visible = false;
                tblCancellationReport.Visible = false;
                tblinventoryExistingHotel.Visible = false;
                ProductionReport.Visible = true;
                //lnkDownloadPDF.Visible = false;
                tblRank.Visible = false;
                tblconv.Visible = false;
                break;
	        case "conversion booking report":
                ConversionReport(whereclause);
                tblNewsletter.Visible = false;
                //pnlExportReport.Visible = true;
                tblProfileCompanyReport.Visible = false;
                tblbookingrequesthistory.Visible = false;
                tblCancellationReport.Visible = false;
                LeadTimeandAvailabilityTable.Visible = false;
                tblinventoryExistingHotel.Visible = false;
                ProductionReport.Visible = false;
                tblRank.Visible = false;
				tblconv.Visible = true;
                break;
        }
        Page.RegisterStartupScript("aa", "<script language='javascript' type='text/javascript' >jQuery(document).ready(function(){movePage();});</script>");
    }

    
    #endregion 

    #region Cancellation report
    public VList<ViewReportCancelation> vcancelation
    {
        get;
        set;
    }
    public void CancelationReport(string where)
    {
        vcancelation = rc.GetCancelationReport(where);
        DateTime dtm = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        TimeSpan t = dtm.Subtract(DateTime.Now);
        if (ucReportSearch.propCountryID == 0)
        {
            
            cancellblfromdate.Text = (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).ToString("dd/MM/yy");
            cancellbltodate.Text = (ucReportSearch.propToDate == DateTime.MinValue ? DateTime.Now : ucReportSearch.propToDate).ToString("dd/MM/yy");
            forcancelallcountrytext.Visible = true;
            forcancelallcountrycity.Visible = false;
        }
        else
        {
            forcancelallcountrytext.Visible = false;
            forcancelallcountrycity.Visible = true;
            lblcancelfrom.Text = (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).ToString("dd/MM/yy");
            lblcancelto.Text = (ucReportSearch.propToDate == DateTime.MinValue ? DateTime.Now : ucReportSearch.propToDate).ToString("dd/MM/yy");
            cancellblfromdate.Text = (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).ToString("dd/MM/yy");
            cancellbltodate.Text = (ucReportSearch.propToDate == DateTime.MinValue ? DateTime.Now : ucReportSearch.propToDate).ToString("dd/MM/yy");
            lblcancelcountry.Text = ucReportSearch.propCountryName;
            lblcancelcity.Text = ucReportSearch.propCityName;
        }

        //List<ReportMasterDetails> lstReportMasterDetails = vcancelation.Select(a => new ReportMasterDetails() { CountryID = a.CountryId, CountryName = a.CountryName, CityID = a.CityId, CityName = a.City, HotelID = a.HotelId, HotelName = a.HotelName }).Distinct(new DistinctItemComparerReportMaster()).ToList();
        if (vcancelation.Count >= 1)
        {
            rptCancelreport.DataSource = vcancelation.FindAllDistinct(ViewReportCancelationColumn.HotelId);
            rptCancelreport.DataBind();
            pnlExportReport.Enabled = true;
            pnlExportReport.Visible = true;
            result.Visible = true;
            lnkDownloadPDF.Visible = true;
            lnkDownloadXLS.Visible = true;
            trEmptydata.Visible = false;
        }
        else
        {
            rptCancelreport.DataSource = null;
            rptCancelreport.DataBind();
            pnlExportReport.Visible = true;
            pnlExportReport.Enabled = false;
            lnkDownloadPDF.Visible = false;
            lnkDownloadXLS.Visible = false;
            trEmptydata.Visible = true;
           
        }
    }

    #endregion

    #region Online Inventory Report
    public VList<ViewReportOnLineInventoryReport> vOnlineInventory
    {
        get;
        set;
    }
    public void OnLineInventoryReport(string where)
    {
        vOnlineInventory = rc.GetOnLineInventoryReport(where);
        if (ucReportSearch.propCountryID == 0)
        {
            lblFromDate.Text = ucReportSearch.propFromDate.ToString("dd/MM/yy");
            lblTodate.Text = ucReportSearch.propToDate.ToString("dd/MM/yy");
            forAllCountryText.Visible = true;
            forSelectedCountryCity.Visible = false;
        }
        else
        {
            forAllCountryText.Visible = false;
            forSelectedCountryCity.Visible = true;
            lblFrom.Text = ucReportSearch.propFromDate.ToString("dd/MM/yy");
            lblTo.Text = ucReportSearch.propToDate.ToString("dd/MM/yy");
            lblCountryName.Text = ucReportSearch.propCountryName;
            lblCityName.Text = ucReportSearch.propCityName;
        }

        List<ReportMasterDetails> lstReportMasterDetails = vOnlineInventory.Select(a => new ReportMasterDetails() { CountryID = a.CountryId, CountryName = a.CountryName, CityID = a.CityId, CityName = a.City, HotelID = a.Id, HotelName = a.Name }).Distinct(new DistinctItemComparerReportMaster()).ToList();
        if (vOnlineInventory.Count >= 1)
        {
            rptMaster.DataSource = lstReportMasterDetails;
            rptMaster.DataBind();
            pnlExportReport.Enabled = true;
            pnlExportReport.Visible = true;
            result.Visible = true;
            lnkDownloadPDF.Visible = false;
            trEmptydata.Visible = false;
            lnkDownloadXLS.Visible = true;
        }
        else
        {
            rptMaster.DataSource = null;
            rptMaster.DataBind();
            lnkDownloadPDF.Visible = false;
            pnlExportReport.Enabled = false;
            pnlExportReport.Visible = true;
            lnkDownloadPDF.Visible = false;
            lnkDownloadXLS.Visible = false;
            trEmptydata.Visible = true;
        }
        
    }
    #endregion End Lead time report

    #region Lead Time Report
    public VList<ViewReportLeadTime> vLeadTime
    {
        get;
        set;
    }
    public void LeadTimeReport(string where)
    {
        vLeadTime = rc.GetLeadTimeReport(where);
        if (ucReportSearch.propCountryID == 0)
        {
            DateTime dtm = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            TimeSpan t = dtm.Subtract(DateTime.Now);
            lblFromDate.Text = (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).ToString("dd/MM/yy");
            lblTodate.Text = (ucReportSearch.propToDate == DateTime.MinValue ? DateTime.Now : ucReportSearch.propToDate).ToString("dd/MM/yy");
            forAllCountryText.Visible = true;
            forSelectedCountryCity.Visible = false;
        }
        else
        {
            DateTime dtm = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            TimeSpan t = dtm.Subtract(DateTime.Now);
            forAllCountryText.Visible = false;
            forSelectedCountryCity.Visible = true;
            lblFrom.Text = (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).ToString("dd/MM/yy");
            lblTo.Text = (ucReportSearch.propToDate == DateTime.MinValue ? DateTime.Now : ucReportSearch.propToDate).ToString("dd/MM/yy");
            lblCountryName.Text = ucReportSearch.propCountryName;
            lblCityName.Text = ucReportSearch.propCityName;
        }

        List<ReportMasterDetails> lstReportMasterDetails = vLeadTime.Select(a => new ReportMasterDetails() { CountryID = a.CountryId, CountryName = a.CountryName, CityID = a.CityId, CityName = a.City, HotelID = a.HotelId, HotelName = a.Name }).Distinct(new DistinctItemComparerReportMaster()).ToList();
        if (vLeadTime.Count >= 1)
        {
            rptMaster.DataSource = lstReportMasterDetails;
            rptMaster.DataBind();
            pnlExportReport.Visible = true;
            pnlExportReport.Enabled = true;
            result.Visible = true;
            lnkDownloadPDF.Visible = false;
            trEmptydata.Visible = false;
            lnkDownloadXLS.Visible = true;
        }
        else
        {
            rptMaster.DataSource = null;
            rptMaster.DataBind();
            lnkDownloadPDF.Visible = false;
            pnlExportReport.Enabled = false;
            pnlExportReport.Visible = true;
            lnkDownloadPDF.Visible = false;
            lnkDownloadXLS.Visible = false;
            trEmptydata.Visible = true;
        }
    }
    #endregion

    #region Availability Report
    public VList<ViewReportAvailability> vAvailability
    {
        get;
        set;
    }
    public void AvailabilityReport(string where)
    {
        vAvailability = rc.GetAvailabilityReport(where);
        DateTime dtm = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        TimeSpan t = dtm.Subtract(DateTime.Now);
        if (ucReportSearch.propCountryID == 0)
        {
            lblFromDate.Text = (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).ToString("dd/MM/yy");
            lblTodate.Text = (ucReportSearch.propToDate == DateTime.MinValue ? DateTime.Now : ucReportSearch.propToDate).ToString("dd/MM/yy");
            forAllCountryText.Visible = true;
            forSelectedCountryCity.Visible = false;
        }
        else
        {
            forAllCountryText.Visible = false;
            forSelectedCountryCity.Visible = true;
            lblFrom.Text = (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).ToString("dd/MM/yy");
            lblTo.Text = (ucReportSearch.propToDate == DateTime.MinValue ? DateTime.Now : ucReportSearch.propToDate).ToString("dd/MM/yy");
            lblCountryName.Text = ucReportSearch.propCountryName;
            lblCityName.Text = ucReportSearch.propCityName;
        }

        List<ReportMasterDetails> lstReportMasterDetails = vAvailability.Select(a => new ReportMasterDetails() { CountryID = a.CountryId, CountryName = a.CountryName, CityID = a.CityId, CityName = a.City, HotelID = a.HotelId, HotelName = a.HotelName, MeetingroomID=a.MeetingRoomId,MeetingroomName=a.MeetingRoomName }).Distinct(new DistinctItemComparerReportMaster()).ToList();
        if (vAvailability.Count >= 1)
        {
            rptMaster.DataSource = lstReportMasterDetails;
            rptMaster.DataBind();
            pnlExportReport.Visible = true;
            pnlExportReport.Enabled = true;
            result.Visible = true;
            lnkDownloadPDF.Visible = false;
            lnkDownloadXLS.Visible = true;
            trEmptydata.Visible = false;
        }
        else
        {
            rptMaster.DataSource = null;
            rptMaster.DataBind();
            lnkDownloadPDF.Visible = false;
            pnlExportReport.Enabled = false;
            pnlExportReport.Visible = true;
            lnkDownloadPDF.Visible = false;
            lnkDownloadXLS.Visible = false;
            trEmptydata.Visible = true;
        }
    }
    #endregion End Lead time report

    #region Commission Control Report
    public VList<ViewReportCommissionControl> vCommissionControl
    {
        get;
        set;
    }
    public void CommissionControlReport(string where)
    {
        vCommissionControl = rc.GetCommissionControlReport(where);
        DateTime dtm = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        TimeSpan t = dtm.Subtract(DateTime.Now);
        if (ucReportSearch.propCountryID == 0)
        {
            lblFromDate.Text = (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).ToString("dd/MM/yy");
            lblTodate.Text = (ucReportSearch.propToDate == DateTime.MinValue ? DateTime.Now : ucReportSearch.propToDate).ToString("dd/MM/yy");
            forAllCountryText.Visible = true;
            forSelectedCountryCity.Visible = false;
        }
        else
        {
            forAllCountryText.Visible = false;
            forSelectedCountryCity.Visible = true;
            lblFrom.Text = (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).ToString("dd/MM/yy");
            lblTo.Text = (ucReportSearch.propToDate == DateTime.MinValue ? DateTime.Now : ucReportSearch.propToDate).ToString("dd/MM/yy");
            lblCountryName.Text = ucReportSearch.propCountryName;
            lblCityName.Text = ucReportSearch.propCityName;
        }

        List<ReportMasterDetails> lstReportMasterDetails = vCommissionControl.Select(a => new ReportMasterDetails() { CountryID = a.CountryId, CountryName = a.CountryName, CityID = a.CityId, CityName = a.City, HotelID = a.HotelId, HotelName = a.HotelName }).Distinct(new DistinctItemComparerReportMaster()).ToList();
        if (vCommissionControl.Count >= 1)
        {
            rptMaster.DataSource = lstReportMasterDetails;
            rptMaster.DataBind();
            pnlExportReport.Visible = true;
            pnlExportReport.Enabled = true;
            result.Visible = true;
            lnkDownloadPDF.Visible = false;
            trEmptydata.Visible = false;
            lnkDownloadXLS.Visible = true;
        }
        else
        {
            rptMaster.DataSource = null;
            rptMaster.DataBind();
            rptMaster.DataSource = null;
            rptMaster.DataBind();
            lnkDownloadPDF.Visible = false;
            pnlExportReport.Enabled = false;
            pnlExportReport.Visible = true;
            lnkDownloadPDF.Visible = false;
            lnkDownloadXLS.Visible = false;
            trEmptydata.Visible = true;
        }
        
    }
    #endregion

    #region Commission Control Report
    public VList<ViewReportCommissionControl> vRevenue
    {
        get;
        set;
    }
    public void RevenueReport(string where)
    {
        vRevenue = rc.GetCommissionControlReport(where);
        DateTime dtm = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        TimeSpan t = dtm.Subtract(DateTime.Now);
        if (ucReportSearch.propCountryID == 0)
        {
            lblFromDate.Text = (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).ToString("dd/MM/yy");
            lblTodate.Text = (ucReportSearch.propToDate == DateTime.MinValue ? DateTime.Now : ucReportSearch.propToDate).ToString("dd/MM/yy");
            forAllCountryText.Visible = true;
            forSelectedCountryCity.Visible = false;
        }
        else
        {
            forAllCountryText.Visible = false;
            forSelectedCountryCity.Visible = true;
            lblFrom.Text = (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).ToString("dd/MM/yy");
            lblTo.Text = (ucReportSearch.propToDate == DateTime.MinValue ? DateTime.Now : ucReportSearch.propToDate).ToString("dd/MM/yy");
            lblCountryName.Text = ucReportSearch.propCountryName;
            lblCityName.Text = ucReportSearch.propCityName;
        }

        List<ReportMasterDetails> lstReportMasterDetails = vRevenue.Select(a => new ReportMasterDetails() { CountryID = a.CountryId, CountryName = a.CountryName, CityID = a.CityId, CityName = a.City, HotelID = a.HotelId, HotelName = a.HotelName }).Distinct(new DistinctItemComparerReportMaster()).ToList();
        if (vCommissionControl.Count >= 1)
        {
            rptMaster.DataSource = lstReportMasterDetails;
            rptMaster.DataBind();
            pnlExportReport.Visible = true;
            pnlExportReport.Enabled = true;
            result.Visible = true;
            lnkDownloadPDF.Visible = false;
            trEmptydata.Visible = false;
            lnkDownloadXLS.Visible = true;
        }
        else
        {
            rptMaster.DataSource = null;
            rptMaster.DataBind();
            rptMaster.DataSource = null;
            rptMaster.DataBind();
            lnkDownloadPDF.Visible = false;
            pnlExportReport.Enabled = false;
            pnlExportReport.Visible = true;
            lnkDownloadPDF.Visible = false;
            lnkDownloadXLS.Visible = false;
            trEmptydata.Visible = true;
        }

    }
    #endregion

    #region Production Report Company
    public VList<ViewReportProductionCompanyAgency> vProduction
    {
        get;
        set;
    }
    public void ProductionReportCompany(string whereclause)
    {
        vProduction = rc.GetProductionReport(whereclause);
        
        if (ucReportSearch.propCountryID == 0)
        {
            lblPYearTop.Text = ucReportSearch.Years;
            PForAllCountryText.Visible = true;
            PForSelectedCountryCityText.Visible = false;
        }
        else
        {
            PForAllCountryText.Visible = false;
            PForSelectedCountryCityText.Visible = true;
            lblPYear.Text = ucReportSearch.Years;
            lblPCountry.Text = ucReportSearch.propCountryName;
            lblPCity.Text = ucReportSearch.propCityName;
        }


        List<ReportMasterDetails> lstReportMasterDetails = vProduction.Select(a => new ReportMasterDetails() { CountryID = a.CountryId, CountryName = a.CountryName, CityName = a.CityName, HotelID = a.Userid, HotelName = a.CompanyName }).Distinct(new DistinctItemComparerReportMaster()).ToList();
        
        if (vProduction.Count >= 1)
        {
            rptPMaster.DataSource = lstReportMasterDetails;
            rptPMaster.DataBind();
            pnlExportReport.Enabled = true;
            pnlExportReport.Visible = true;
            result.Visible = true;
            lnkDownloadPDF.Visible = true;
            trEmptydata.Visible = false;
            lnkDownloadXLS.Visible = true;
        }
        else
        {
            rptPMaster.DataSource = null;
            rptPMaster.DataBind();
            pnlExportReport.Visible = true;
            pnlExportReport.Enabled = false;
            lnkDownloadPDF.Visible = false;
            lnkDownloadXLS.Visible = false;
            trEmptydata.Visible = true;
        }
    }
    #endregion

    #region Conversion Report
    public void ConversionReport(string whereclause)
    {
        vProductionHotel = rc.GetProductionReportHotel(whereclause);
        if (ucReportSearch.propCountryID == 0)
        {
            lblYearConvTop.Text = ucReportSearch.Years;
            forConvallcountrytext.Visible = true;
            forConvallcountrycity.Visible = false;
        }
        else
        {
            forConvallcountrytext.Visible = false;
            forConvallcountrycity.Visible = true;
            lblYearConv.Text = ucReportSearch.Years;
            lblConvCountry.Text = ucReportSearch.propCountryName;
            lblConvCity.Text = ucReportSearch.propCityName;
        }


        List<ReportMasterDetails> lstReportMasterDetails = vProductionHotel.Select(a => new ReportMasterDetails() { CountryID = a.CountryId, CountryName = a.CountryName, CityName = a.City, HotelID = a.Id, HotelName = a.HotelName }).Distinct(new DistinctItemComparerReportMaster()).ToList();
        //rptPMaster.DataSource = lstReportMasterDetails;
        //rptPMaster.DataBind();
        if (vProductionHotel.Count >= 1)
        {
            rptConver.DataSource = lstReportMasterDetails;
            rptConver.DataBind();
            pnlExportReport.Visible = true;
            pnlExportReport.Enabled = true;
            lnkDownloadPDF.Visible = false;
            lnkDownloadXLS.Visible = true;
            result.Visible = true;
            trEmptydata.Visible = false;
            lnkDownloadXLS.Visible = true;
            
        }
        else
        {
            rptConver.DataSource = null;
            rptConver.DataBind();
            lnkDownloadPDF.Visible = false;
            pnlExportReport.Enabled = false;
            pnlExportReport.Visible = true;
            lnkDownloadPDF.Visible = false;
            lnkDownloadXLS.Visible = false;
            trEmptydata.Visible = true;
        }
       
    }
    #endregion

    #region Production Report Hotel
    public VList<ViewReportProductionReportHotel> vProductionHotel
    {
        get;
        set;
    }
    public void ProductionReportHotel(string whereclause)
    {
        vProductionHotel = rc.GetProductionReportHotel(whereclause);
        if (ucReportSearch.propCountryID == 0)
        {
            lblPYearTop.Text = ucReportSearch.Years;
            PForAllCountryText.Visible = true;
            PForSelectedCountryCityText.Visible = false;
        }
        else
        {
            PForAllCountryText.Visible = false;
            PForSelectedCountryCityText.Visible = true;
            lblPYear.Text = ucReportSearch.Years;
            lblPCountry.Text = ucReportSearch.propCountryName;
            lblPCity.Text = ucReportSearch.propCityName;
        }


        List<ReportMasterDetails> lstReportMasterDetails = vProductionHotel.Select(a => new ReportMasterDetails() { CountryID = a.CountryId, CountryName = a.CountryName, CityName = a.City, HotelID = a.Id, HotelName = a.HotelName }).Distinct(new DistinctItemComparerReportMaster()).ToList();
        if (vProductionHotel.Count >= 1)
        {
            rptPMaster.DataSource = lstReportMasterDetails;
            rptPMaster.DataBind();
             //rptConver.DataSource = lstReportMasterDetails;
             //        rptConver.DataBind();
            pnlExportReport.Enabled = true;
            pnlExportReport.Visible = true;
            result.Visible = true;
            lnkDownloadPDF.Visible = false;
            trEmptydata.Visible = false;
            lnkDownloadXLS.Visible = true;
        }
        else
        {
            rptPMaster.DataSource = null;
            rptPMaster.DataBind();
            lnkDownloadPDF.Visible = false;
            pnlExportReport.Visible = true;
            pnlExportReport.Enabled = false;
            lnkDownloadPDF.Visible = false;
            lnkDownloadXLS.Visible = false;
            trEmptydata.Visible = true;
        }
        
    }
    #endregion

    #region Profile-company-agency Report
    public VList<ViewReportforProfileCompanyAgency> vProfileCompanyAgency
    {
        get;
        set;
    }
    public void ViewReportProfileCompanyAgency(string where)
    {
        vProfileCompanyAgency = rc.GetProfileCompanyAgencyReport(where);
        DateTime dtm = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        TimeSpan t = dtm.Subtract(DateTime.Now);
        if (ucReportSearch.propCountryID == 0)
        {
            lblAllProfileFrom.Text = (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).ToString("dd/MM/yy");
            lblAllProfileTo.Text = (ucReportSearch.propToDate == DateTime.MinValue ? DateTime.Now : ucReportSearch.propToDate).ToString("dd/MM/yy");
            tblForAllProfile.Visible = true;
            tblForAllProfileByCountry.Visible = false;
        }
        else
        {
            tblForAllProfile.Visible = false;
            tblForAllProfileByCountry.Visible = true;
            lblAllProfileFrom2.Text = (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).ToString("dd/MM/yy");
            lblAllProfileTo2.Text = (ucReportSearch.propToDate == DateTime.MinValue ? DateTime.Now : ucReportSearch.propToDate).ToString("dd/MM/yy");
            lblAllProfileCountry.Text = ucReportSearch.propCountryName;
            lblAllProfileCity.Text = ucReportSearch.propCityName;
        }

       
        if (vProfileCompanyAgency.Count >= 1)
        {
            rptProfileReport.DataSource = vProfileCompanyAgency.FindAllDistinct(ViewReportforProfileCompanyAgencyColumn.UserId);
            rptProfileReport.DataBind();
            pnlExportReport.Enabled = true;
            pnlExportReport.Visible = true;
            result.Visible = true;
            lnkDownloadPDF.Visible = true;
            trEmptydata.Visible = false;
            lnkDownloadXLS.Visible = true;

        }
        else
        {
            rptProfileReport.DataSource = null;
            rptProfileReport.DataBind();
            lnkDownloadPDF.Visible = true;
            pnlExportReport.Enabled = false;
            pnlExportReport.Visible = true;
            lnkDownloadPDF.Visible = false;
            lnkDownloadXLS.Visible = false;
            trEmptydata.Visible = true;
        }
    }

    protected void rptProfileReport_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                System.Web.UI.HtmlControls.HtmlTableCell tdCompany = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdCompany");
                System.Web.UI.HtmlControls.HtmlTableCell tdContact = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdContact");
                switch (lblReportName.Text.ToLower())
                {
                    case "profile company / agency report":
                        if (ucReportSearch.propProfileName == 9)
                        {
                            tdCompany.Visible = true;
                            tdContact.Visible = false;
                        }
                        else
                        {
                            tdCompany.Visible = false;
                            tdContact.Visible = true;
                        }
                    break;
                }
            }
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                System.Web.UI.HtmlControls.HtmlTableCell tdCompany = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdCompany");
                System.Web.UI.HtmlControls.HtmlTableCell tdContact = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdContact");
                switch (lblReportName.Text.ToLower())
                {
                    case "profile company / agency report":
                        if (ucReportSearch.propProfileName == 9)
                        {
                            tdCompany.Visible = true;
                            tdContact.Visible = false;
                        }
                        else
                        {
                            tdCompany.Visible = false;
                            tdContact.Visible = true;
                        }
                        break;
                }
                Label lblUserID = (Label)e.Item.FindControl("lblUserID");
                Label lblNoofBooking = (Label)e.Item.FindControl("lblNoofBooking");
                Label lblNoofRequest = (Label)e.Item.FindControl("lblNoofRequest");
                string where = ViewReportforProfileCompanyAgencyColumn.BookType + "=0 AND " + ViewReportforProfileCompanyAgencyColumn.UserId + "=" + lblUserID.Text;
                List<ViewReportforProfileCompanyAgency> vProfile = vProfileCompanyAgency.Where(a => a.BookType == 0 && a.UserId == Convert.ToInt64(lblUserID.Text)).ToList();
                if (vProfile.Count > 0)
                {
                    lblNoofBooking.Text = vProfile.Count.ToString();
                }
                else
                {
                    lblNoofBooking.Text = "0";
                }

                string whereReq = ViewReportforProfileCompanyAgencyColumn.BookType + " in (1,2) AND " + ViewReportforProfileCompanyAgencyColumn.UserId + "=" + lblUserID.Text;
                List<ViewReportforProfileCompanyAgency> vProfileReq = vProfileCompanyAgency.Where(a => (a.BookType == 1 || a.BookType == 2) && a.UserId == Convert.ToInt64(lblUserID.Text)).ToList();
                if (vProfileReq.Count > 0)
                {
                    lblNoofRequest.Text = vProfileReq.Count.ToString();
                }
                else
                {
                    lblNoofRequest.Text = "0";
                }
            }
        }
        catch (Exception ex)
        {           
        }
    }
    #endregion End profile-company-agency

    #region Booking/Request History
    public VList<ViewReportBookingRequestHistory> vbookreqhistory
    {
        get;
        set;
    }
    public void BookingRequestreport(string where)
    {
        vbookreqhistory = rc.GetBookingRequestHistoryReport(where);
        DateTime dtm = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        TimeSpan t = dtm.Subtract(DateTime.Now);
        if (ucReportSearch.propCountryID == 0)
        {
            lblbrhfromdate.Text = (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).ToString("dd/MM/yy");
            lblbrhtodate.Text = (ucReportSearch.propToDate == DateTime.MinValue ? DateTime.Now : ucReportSearch.propToDate).ToString("dd/MM/yy");
            forbrhallcountrytext.Visible = true;
            forbrhallcountrycity.Visible = false;
        }
        else
        {
            forbrhallcountrytext.Visible = false;
            forbrhallcountrycity.Visible = true;
            lblbrhfrom.Text = (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).ToString("dd/MM/yy");
            lblbrhto.Text = (ucReportSearch.propToDate == DateTime.MinValue ? DateTime.Now : ucReportSearch.propToDate).ToString("dd/MM/yy");
            lblbrhcountryname.Text = ucReportSearch.propCountryName;
            lblbrhcityname.Text = ucReportSearch.propCityName;
        }

        List<ReportMasterDetails> lstBookingRequestHistorystatus = vbookreqhistory.Select(a => new ReportMasterDetails() { CountryID = a.CountryId, CountryName = a.CountryName, CityID = a.CityId, CityName = a.City, HotelID = a.HotelId, HotelName = a.HotelName }).Distinct(new DistinctItemComparerReportMaster()).ToList();

        if (vbookreqhistory.Count >= 1)
        {
            rptbrhmaster.DataSource = lstBookingRequestHistorystatus;
            rptbrhmaster.DataBind();
            pnlExportReport.Enabled = true;
            pnlExportReport.Visible = true;
            result.Visible = true;
            lnkDownloadPDF.Visible = false;
            trEmptydata.Visible = false;
            lnkDownloadXLS.Visible = true;

        }
        else
        {
            rptbrhmaster.DataSource = null;
            rptbrhmaster.DataBind();
            lnkDownloadPDF.Visible = false;
            lnkDownloadXLS.Visible = false;
            //pnlExportReport.Enabled = false;
            pnlExportReport.Visible = true;
            trEmptydata.Visible = true;
        }

    }
    #endregion

    #region inventory report
    public VList<ViewreportInventoryReportofExistingHotel> vinvreportexistinghotel
    {
        get;
        set;
    }
    public TList<Hotel> vinvreportaddedhotelinperiod
    {
        get;
        set;
    }
    public void invexistingreport(string where)
    {
        switch (lblReportName.Text.ToLower())
        {
            case "inventory report of existing hotel":
                vinvreportexistinghotel = rc.GetInvExistinghotelReportmeeting(where);
                if (vinvreportexistinghotel.Count > 0)
                {
                    rptgrdinvExisthotel.DataSource = vinvreportexistinghotel.FindAllDistinct(ViewreportInventoryReportofExistingHotelColumn.HotelName);
                    rptgrdinvExisthotel.DataBind();
                    pnlExportReport.Enabled = true;
                    pnlExportReport.Visible = true;
                    result.Visible = true;
                    lnkDownloadPDF.Visible = true;
                    trEmptydata.Visible = false;
                    lnkDownloadXLS.Visible = true;

                    if (ucReportSearch.propCountryID == 0)
                    {
                        //lblforinvExisthotelfromdate.Text = ucReportSearch.propFromDate.ToString("dd/MM/yy");
                        //lblforinvExisthoteltodate.Text = ucReportSearch.propToDate.ToString("dd/MM/yy");
                        forinvExisthotelallcountrytext.Visible = true;
                        forinvExisthotelallcountrycity.Visible = false;
                    }
                    else
                    {
                        forinvExisthotelallcountrytext.Visible = false;
                        forinvExisthotelallcountrycity.Visible = true;
                        //lblinvExisthotelfrom.Text = ucReportSearch.propFromDate.ToString("dd/MM/yy");
                        //lblinvExisthotelto.Text = ucReportSearch.propToDate.ToString("dd/MM/yy");
                        lblinvExisthotelcountry.Text = ucReportSearch.propCountryName;
                        lblinvExisthotelcity.Text = ucReportSearch.propCityName;
                    }

                }
                else
                {
                    //lnkDownloadPDF.Visible = false;
                    pnlExportReport.Enabled = false;
                    pnlExportReport.Visible = true;
                    lnkDownloadPDF.Visible = false;
                    lnkDownloadXLS.Visible = false;
                    trEmptydata.Visible = true;
                }
                break;
            case "added hotel in period inventory report":
                vinvreportaddedhotelinperiod = rc.GetInvAddedhotelReport(where);
                DateTime dtm = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                TimeSpan t = dtm.Subtract(DateTime.Now);
                if (ucReportSearch.propCountryID == 0)
                {
                    lblforinvExisthotelfromdate.Text = (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).ToString("dd/MM/yy");
                    lblforinvExisthoteltodate.Text = (ucReportSearch.propToDate == DateTime.MinValue ? DateTime.Now : ucReportSearch.propToDate).ToString("dd/MM/yy");
                    forinvExisthotelallcountrytext.Visible = true;
                    forinvExisthotelallcountrycity.Visible = false;
                }
                else
                {
                    forinvExisthotelallcountrytext.Visible = false;
                    forinvExisthotelallcountrycity.Visible = true;
                    lblinvExisthotelfrom.Text = (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).ToString("dd/MM/yy");
                    lblinvExisthotelto.Text = (ucReportSearch.propToDate == DateTime.MinValue ? DateTime.Now : ucReportSearch.propToDate).ToString("dd/MM/yy");
                    lblinvExisthotelcountry.Text = ucReportSearch.propCountryName;
                    lblinvExisthotelcity.Text = ucReportSearch.propCityName;
                }

                 
                if (vinvreportaddedhotelinperiod.Count >= 1)
                {
                    rptgrdinvExisthotel.DataSource = vinvreportaddedhotelinperiod.FindAllDistinct(HotelColumn.Name);
                    rptgrdinvExisthotel.DataBind();
                    pnlExportReport.Enabled = true;
                    pnlExportReport.Visible = true;
                    result.Visible = true;
                    lnkDownloadPDF.Visible = true;
                    trEmptydata.Visible = false;
                    lnkDownloadXLS.Visible = true;

                }
                else
                {
                    rptgrdinvExisthotel.DataSource = null;
                    rptgrdinvExisthotel.DataBind();
                    //lnkDownloadPDF.Visible = true;
                    pnlExportReport.Enabled = false;
                    pnlExportReport.Visible = true;
                    lnkDownloadPDF.Visible = false;
                    lnkDownloadXLS.Visible = false;
                    trEmptydata.Visible = true;
                }
                break;
        }
       

        
    }

    #endregion

    #region Ranking
    public VList<ViewReportRanking> vReportRanking
    {
        get;
        set;
    }
    public void Rankingreport(string where)
    {
        vReportRanking = rc.GetRanking(where);
        if (ucReportSearch.propCountryID == 0)
        {
           
            forRankallcountrytext.Visible = true;
            forRankallcountrycity.Visible = false;
        }
        else
        {
            forRankallcountrytext.Visible = false;
            forRankallcountrycity.Visible = true;            
            lblrankCountry.Text = ucReportSearch.propCountryName;
            lblrankCity.Text = ucReportSearch.propCityName;
        }

        if (vReportRanking.Count >= 1)
        {
            rptRank.DataSource = vReportRanking;
            rptRank.DataBind();
            pnlExportReport.Enabled = true;
            pnlExportReport.Visible = true;
            result.Visible = true;
            lnkDownloadPDF.Visible = true;
            trEmptydata.Visible = false;
            lnkDownloadXLS.Visible = true;

        }
        else
        {
            rptRank.DataSource = null;
            rptRank.DataBind();
            lnkDownloadPDF.Visible = true;
            pnlExportReport.Enabled = false;
            pnlExportReport.Visible = true;
            lnkDownloadPDF.Visible = false;
            lnkDownloadXLS.Visible = false;
            trEmptydata.Visible = true;
        }


    }
    #endregion

    #region NewsLetter
    public TList<NewsLetterSubscriber> vNewsLetterSubscriber
    {
        get;
        set;
    }
    public void Newsletterreport(string where)
    {
        vNewsLetterSubscriber = rc.GetNewsletter(where);
        if (vNewsLetterSubscriber.Count >= 1)
        {
            rptNewsLetter.DataSource = vNewsLetterSubscriber;
            rptNewsLetter.DataBind();
            pnlExportReport.Enabled = true;
            pnlExportReport.Visible = true;
            result.Visible = true;
            lnkDownloadPDF.Visible = true;
            trEmptydata.Visible = false;
            lnkDownloadXLS.Visible = true;

        }
        else
        {
            rptRank.DataSource = null;
            rptRank.DataBind();
            lnkDownloadPDF.Visible = true;
            pnlExportReport.Enabled = false;
            pnlExportReport.Visible = true;
            lnkDownloadPDF.Visible = false;
            lnkDownloadXLS.Visible = false;
            trEmptydata.Visible = true;
        }


    }
    #endregion

    #region Download PDF and Excel
    protected void lnkDownloadPDF_Click(object sender, EventArgs e)
    {
        try
        {

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename="+lblReportName.Text.Replace(" ","")+".pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            PrepareGridViewForExport(result);
            result.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();


        }
        catch (Exception ex)
        {
            //logger.Error(ex);
            Page.RegisterStartupScript("aa", "<script language='javascript' type='text/javascript'>document.getElementsByTagName('html')[0].style.overflow = 'auto';jQuery('#Loding_overlay').hide();</script>");
        }
    }
    protected void lnkDownloadXLS_Click(object sender, EventArgs e)
    {
        //GridViewExportUtil.Export("Invoice.xls", this.result);
        try
        {
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=" + lblReportName.Text.Replace(" ", "") + ".xls");
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.xls";
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            result.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.End(); 
        }
        catch (Exception ex)
        {
            Page.RegisterStartupScript("aa", "<script language='javascript' type='text/javascript'>document.getElementsByTagName('html')[0].style.overflow = 'auto';jQuery('#Loding_overlay').hide();</script>");
        }
    }
    #region PrepareGridViewForExport
    /// <summary>
    /// method to PrepareGridViewForExport
    /// </summary>
    private void PrepareGridViewForExport(Control gv)
    {
        try
        {
            LinkButton lb = new LinkButton();

            Literal l = new Literal();

            string name = String.Empty;

            for (int i = 0; i < gv.Controls.Count; i++)
            {

                if (gv.Controls[i].GetType() == typeof(LinkButton))
                {

                    l.Text = (gv.Controls[i] as LinkButton).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(DropDownList))
                {
                    //l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;

                    if ((gv.Controls[i] as DropDownList).SelectedItem.Text == "--Select City--")
                    {
                        l.Text = "City";
                    }
                    if ((gv.Controls[i] as DropDownList).SelectedItem.Text == "--Select Hotel--")
                    {
                        l.Text = "Hotel/Facility";
                    }
                    if ((gv.Controls[i] as DropDownList).SelectedItem.Text == "--Select Meeting Room--")
                    {
                        l.Text = "Meeting Room";
                    }
                    if ((gv.Controls[i] as DropDownList).SelectedItem.Text == "--Select Booking Date--")
                    {
                        l.Text = "Booking Date";
                    }
                    if ((gv.Controls[i] as DropDownList).SelectedItem.Text == "--Select Value--")
                    {
                        l.Text = "Value";
                    }
                    if ((gv.Controls[i] as DropDownList).SelectedItem.Text == "--Select Arrival Date--")
                    {
                        l.Text = "Arrival Date";
                    }

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(CheckBox))
                {

                    l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }
                else if (gv.Controls[i].GetType() == typeof(HyperLink))
                {

                    l.Text = (gv.Controls[i] as HyperLink).Text;

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                else if (gv.Controls[i].GetType() == typeof(System.Web.UI.WebControls.Image))
                {

                    // l.Text = (gv.Controls[i] as Image).Checked ? "True" : "False";

                    gv.Controls.Remove(gv.Controls[i]);

                    gv.Controls.AddAt(i, l);

                }

                if (gv.Controls[i].HasControls())
                {

                    PrepareGridViewForExport(gv.Controls[i]);

                }

            }
        }
        catch (Exception ex)
        {
            //logger.Error(ex);
        }

    }
    #endregion
    #endregion

    #region Service method
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static Array GetHotel(string strHotelName)
    {

        HotelInfo ObjHotelinfo = new HotelInfo();
        string whereclauseforHotel = HotelColumn.IsRemoved + "=0 AND " + HotelColumn.Name + " Like '" + strHotelName + "%'";
        TList<Hotel> THotel = ObjHotelinfo.GetHotelbyCondition(whereclauseforHotel, string.Empty);

        List<string> Names = new List<string>();
        for (int i = 0; i <= THotel.Count - 1; i++)
        {
            Names.Add(THotel[i].Name.ToString());
        }

         objHotelList = Names;
        //UserControl Obj = (UserControl)t
        //CheckBoxList ObjCheckBoxList = (CheckBoxList)ucReportSearch.FindControl("chkchkListHotel"); 
        return Names.ToArray();
    }
    #endregion

    #region Events
    void BindHotelCheckboxList()
    {
       CheckBoxList ObjCheckBoxList = (CheckBoxList)ucReportSearch.FindControl("chkchkListHotel"); 
    }
    protected void lblBookingRequest_Click(object sender, EventArgs e)
    {
        lblReportName.Text = (sender as LinkButton).Text;
        DivReportFilter.Visible = true;
        pnlExportReport.Visible = false;
        result.Visible = false;
        ucReportSearch.ClearRecords();
        ucReportSearch.GetPropertiesforControl();
    }
    protected void lnkConversitionRequest_Click(object sender, EventArgs e)
    {
      //  ConversionReport(whereclause);
        lblReportName.Text = (sender as LinkButton).Text;
        DivReportFilter.Visible = true;
        pnlExportReport.Visible = false;
        result.Visible = false;
        ucReportSearch.ClearRecords();
        ucReportSearch.GetPropertiesforControl();
    
    }
    protected void lnkCancelation_Click(object sender, EventArgs e)
    {
        lblReportName.Text = (sender as LinkButton).Text;
        DivReportFilter.Visible = true;
        pnlExportReport.Visible = false;
        result.Visible = false;
        ucReportSearch.ClearRecords();
        ucReportSearch.GetPropertiesforControl();
    }
    protected void lnkInventoryExistingHotel_Click(object sender, EventArgs e)
    {
        lblReportName.Text = (sender as LinkButton).Text;
        DivReportFilter.Visible = true;
        pnlExportReport.Visible = false;
        result.Visible = false;
        ucReportSearch.ClearRecords();
        ucReportSearch.GetPropertiesforControl();
    }
    protected void lnkAddedHotel_Click(object sender, EventArgs e)
    {
        lblReportName.Text = (sender as LinkButton).Text;
        DivReportFilter.Visible = true;
        pnlExportReport.Visible = false;
        result.Visible = false;
        ucReportSearch.ClearRecords();
        ucReportSearch.GetPropertiesforControl();
    }
    protected void lnkLeadTime_Click(object sender, EventArgs e)
    {
        lblReportName.Text = (sender as LinkButton).Text;
        DivReportFilter.Visible = true;
        pnlExportReport.Visible = false;
        result.Visible = false;
        ucReportSearch.ClearRecords();
        ucReportSearch.GetPropertiesforControl();
    }
    protected void lnkOnlineInventoryCharge_Click(object sender, EventArgs e)
    {
        lblReportName.Text = (sender as LinkButton).Text;
        DivReportFilter.Visible = true;
        pnlExportReport.Visible = false;
        result.Visible = false;
        ucReportSearch.ClearRecords();
        ucReportSearch.GetPropertiesforControl();
    }
    protected void lnkAvailability_Click(object sender, EventArgs e)
    {
        lblReportName.Text = (sender as LinkButton).Text;
        DivReportFilter.Visible = true;
        pnlExportReport.Visible = false;
        result.Visible = false;
        ucReportSearch.ClearRecords();
        ucReportSearch.GetPropertiesforControl();
    }
    protected void lnkCommissionControl_Click(object sender, EventArgs e)
    {
        lblReportName.Text = (sender as LinkButton).Text;
        DivReportFilter.Visible = true;
        pnlExportReport.Visible = false;
        result.Visible = false;
        ucReportSearch.ClearRecords();
        ucReportSearch.GetPropertiesforControl();
    }
    protected void lnkcompanyAgencyReport_Click(object sender, EventArgs e)
    {
        lblReportName.Text = (sender as LinkButton).Text;
        DivReportFilter.Visible = true;
        pnlExportReport.Visible = false;
        result.Visible = false;
        ucReportSearch.ClearRecords();
        ucReportSearch.GetPropertiesforControl();
    }
    protected void lnkProductionCompanyAgency_Click(object sender, EventArgs e)
    {
        lblReportName.Text = (sender as LinkButton).Text;
        DivReportFilter.Visible = true;
        pnlExportReport.Visible = false;
        result.Visible = false;
        ucReportSearch.ClearRecords();
        ucReportSearch.GetPropertiesforControl();
    }
    protected void lnkProductionHotelAndMeeting_Click(object sender, EventArgs e)
    {
        lblReportName.Text = (sender as LinkButton).Text;
        DivReportFilter.Visible = true;
        pnlExportReport.Visible = false;
        result.Visible = false;
        ucReportSearch.ClearRecords();
        ucReportSearch.GetPropertiesforControl();
    }
    protected void lnkRanking_Click(object sender, EventArgs e)
    {
        lblReportName.Text = (sender as LinkButton).Text;
        DivReportFilter.Visible = true;
        pnlExportReport.Visible = false;
        result.Visible = false;
        ucReportSearch.ClearRecords();
        ucReportSearch.GetPropertiesforControl();
    }
    protected void lnkRevenue_Click(object sender, EventArgs e)
    {
        lblReportName.Text = (sender as LinkButton).Text;
        DivReportFilter.Visible = true;
        pnlExportReport.Visible = false;
        result.Visible = false;
        ucReportSearch.ClearRecords();
        ucReportSearch.GetPropertiesforControl();
    }
    protected void lnkNewsLetter_Click(object sender, EventArgs e)
    {
        lblReportName.Text = (sender as LinkButton).Text;
        DivReportFilter.Visible = true;
        pnlExportReport.Visible = false;
        result.Visible = false;
        ucReportSearch.ClearRecords();
        ucReportSearch.GetPropertiesforControl();
    }
    #endregion

    #region Grid For Lead time, Availability, Commission Control, Revenue and Online Inventory Handlers
    protected void rptMaster_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Header)
        {
            System.Web.UI.HtmlControls.HtmlTableCell Meetingroomtd = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("Meetingroomtd");
            System.Web.UI.HtmlControls.HtmlTableCell tdCommissionTotalHeader = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdCommissionTotalHeader");
            System.Web.UI.HtmlControls.HtmlTableCell tdCommissionAvgHeader = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdCommissionAvgHeader");
            
            switch (lblReportName.Text.ToLower())
            {
                case "lead time report":
                    Meetingroomtd.Visible = false;
                    tdCommissionTotalHeader.Visible = false;
                    tdCommissionAvgHeader.Visible = false;
                    break;
                case "availability report":
                    Meetingroomtd.Visible = true;
                    tdCommissionTotalHeader.Visible = false;
                    tdCommissionAvgHeader.Visible = false;
                    break;
                case "online inventory changes report":
                    Meetingroomtd.Visible = false;
                    tdCommissionTotalHeader.Visible = false;
                    tdCommissionAvgHeader.Visible = true;
                    break;
                case "commission control report":
                    Meetingroomtd.Visible = false;
                    tdCommissionTotalHeader.Visible = true;
                    tdCommissionAvgHeader.Visible = true;
                    break;
                case "revenue report":
                    Meetingroomtd.Visible = false;
                    tdCommissionTotalHeader.Visible = true;
                    tdCommissionAvgHeader.Visible = true;
                    break;
                default:
                    Meetingroomtd.Visible = false;
                    tdCommissionTotalHeader.Visible = false;
                    tdCommissionAvgHeader.Visible = false;
                    break;
            }
            Repeater rptDaysName = (Repeater)e.Item.FindControl("rptDaysName");
            List<ReportDayList> DayList = new List<ReportDayList>();
            DateTime dtm = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            TimeSpan t = dtm.Subtract(DateTime.Now);
            for (int i = 0; (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).AddDays(i) <= (ucReportSearch.propToDate == DateTime.MinValue ? DateTime.Now : ucReportSearch.propToDate); i++)
            {
                ReportDayList d = new ReportDayList();
                d.DATE = (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).AddDays(i);
                DayList.Add(d);
            }
            rptDaysName.DataSource = DayList;
            rptDaysName.DataBind();
        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblCountry = (Label)e.Item.FindControl("lblCountry");
            Label lblCity = (Label)e.Item.FindControl("lblCity");
            Label lblHotel = (Label)e.Item.FindControl("lblHotel");
            Label lblMeetingroom = (Label)e.Item.FindControl("lblMeetingroom");
            System.Web.UI.HtmlControls.HtmlTableCell Meetingroomtdbody = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("Meetingroomtdbody");
            System.Web.UI.HtmlControls.HtmlTableCell tdCommissionTotalBody = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdCommissionTotalBody");
            System.Web.UI.HtmlControls.HtmlTableCell tdCommissionAvgBody = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdCommissionAvgBody");
            Label lblCommissionTotal = (Label)e.Item.FindControl("lblCommissionTotal");
            Label lblAvgPerDay = (Label)e.Item.FindControl("lblAvgPerDay");

            Repeater rptDaysLead = (Repeater)e.Item.FindControl("rptDaysLead");
            ReportMasterDetails r = e.Item.DataItem as ReportMasterDetails;
            if (r != null)
            {
                lblCountry.Text = r.CountryName == null ? "" : r.CountryName;
                lblCity.Text = r.CityName == null ? "" : r.CityName;
                lblHotel.Text = r.HotelName==null?"":r.HotelName;
                lblMeetingroom.Text = r.MeetingroomName == null ? "" : r.MeetingroomName;
                switch (lblReportName.Text.ToLower())
                {
                    case "lead time report":
                        Meetingroomtdbody.Visible = false;
                        tdCommissionTotalBody.Visible = false;
                        tdCommissionAvgBody.Visible = false;
                        rptDaysLead.DataSource = vLeadTime.Where(a=>a.HotelId==r.HotelID).Select(a=> new ReportResultDateAndValue(){DATE=Convert.ToDateTime(a.AvailabilityDate==null?DateTime.Now:a.AvailabilityDate),VALUE=Convert.ToString(a.LeadTimeForMeetingRoom)}).ToList();
                        rptDaysLead.DataBind();
                        break;
                    case "availability report":
                        Meetingroomtdbody.Visible = true;
                        tdCommissionTotalBody.Visible = false;
                        tdCommissionAvgBody.Visible = false;
                        DateTime dtm = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        TimeSpan t2 = dtm.Subtract(DateTime.Now);
                        DateTime dtStart = (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t2.Days) : ucReportSearch.propFromDate);
                        DateTime dtEnd = (ucReportSearch.propToDate == DateTime.MinValue ? DateTime.Now : ucReportSearch.propToDate);
                        List<ReportAvailability> rptAvailability = new List<ReportAvailability>();
                        while (dtStart <= dtEnd)
                        {
                            ReportAvailability newAvail = new ReportAvailability();
                            var newResult = vAvailability.FirstOrDefault(a => a.HotelId == r.HotelID && a.MeetingRoomId == r.MeetingroomID && a.AvailabilityDate==dtStart);
                            if (newResult != null)
                            {
                                newAvail.DATE = dtStart;
                                newAvail.MorningStatus = Convert.ToString(newResult.MorningStatus == (int)AvailabilityStatus.AVAILABLE ? "M" : "");
                                newAvail.AfternoonStatus = Convert.ToString(newResult.AfternoonStatus == (int)AvailabilityStatus.AVAILABLE ? "A" : "");
                            }
                            else
                            {
                                newAvail.DATE = dtStart;
                                newAvail.MorningStatus = "";
                                newAvail.AfternoonStatus = "";
                            }
                            rptAvailability.Add(newAvail);
                            dtStart = dtStart.AddDays(1);
                        }
                        //GetLastDate = (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t2.Days) : ucReportSearch.propFromDate);
                        rptDaysLead.DataSource = rptAvailability;//vAvailability.Where(a => a.HotelId == r.HotelID && a.MeetingRoomId == r.MeetingroomID).Select(a => new ReportAvailability() { DATE = Convert.ToDateTime(a.AvailabilityDate == null ? DateTime.Now : a.AvailabilityDate), MorningStatus = Convert.ToString(a.MorningStatus == (int)AvailabilityStatus.AVAILABLE ? "M" : ""), AfternoonStatus = Convert.ToString(a.AfternoonStatus == (int)AvailabilityStatus.AVAILABLE ? "A" : "") }).ToList();
                        rptDaysLead.DataBind();
                        break;
                    case "online inventory changes report":
                        Meetingroomtdbody.Visible = false;
                        tdCommissionTotalBody.Visible = false;
                        tdCommissionAvgBody.Visible = true;
                        rptDaysLead.DataSource = vOnlineInventory.Where(a => a.Id == r.HotelID).Select(a => new ReportOnlineInventories() { DATE = Convert.ToDateTime(a.AvailabilityDate == null ? DateTime.Now : a.AvailabilityDate), MeetingroomCount = Convert.ToString(a.MeetingroomCount), BookedMeetingRoom = Convert.ToString(a.BookedMeetingRoom) }).ToList();
                        rptDaysLead.DataBind();
                        double p = Convert.ToDouble(vOnlineInventory.Where(a => a.Id == r.HotelID).Sum(a => a.BookedMeetingRoom));
                        TimeSpan t = (ucReportSearch.propToDate == DateTime.MinValue ? DateTime.Now : ucReportSearch.propToDate).Subtract((ucReportSearch.propFromDate == DateTime.MinValue ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) : ucReportSearch.propFromDate));
                        if (Math.Round(t.TotalDays,0) == 0)
                        {
                            lblAvgPerDay.Text = Convert.ToString(p / 1);
                        }
                        else
                        {
                            lblAvgPerDay.Text = Convert.ToString(p / Math.Round(t.TotalDays, 0));
                        }
                        break;
                    case "revenue report":
                    case "commission control report":
                        Meetingroomtdbody.Visible = false;
                        tdCommissionTotalBody.Visible = true;
                        tdCommissionAvgBody.Visible = true;
                        double MyVal = 0;
                        long loopcount = 0;
                        List<ReportCommissionControl> lst = vCommissionControl.Where(a => a.HotelId == r.HotelID).Select(a => new ReportCommissionControl() { DATE = Convert.ToDateTime(a.BookingDate == null ? DateTime.Now : a.BookingDate), InitialRevenue = Convert.ToString(a.InitialValue), InitialCommission = Convert.ToString(a.InitialComm), ActualRevenue = Convert.ToString(a.ActualValue), ActualCommission = Convert.ToString(a.ActualComm) }).ToList();
                        List<ReportCommissionControl> newLSt = new List<ReportCommissionControl>();
                        for (int i = 0; (ucReportSearch.propFromDate == DateTime.MinValue ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) : ucReportSearch.propFromDate).AddDays(i) <= (ucReportSearch.propToDate == DateTime.MinValue ? DateTime.Now : ucReportSearch.propToDate); i++)
                        {
                            ReportCommissionControl old = lst.Where(a => a.DATE.Month == (ucReportSearch.propFromDate == DateTime.MinValue ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) : ucReportSearch.propFromDate).AddDays(i).Month && a.DATE.Year == (ucReportSearch.propFromDate == DateTime.MinValue ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) : ucReportSearch.propFromDate).AddDays(i).Year && a.DATE.Day == (ucReportSearch.propFromDate == DateTime.MinValue ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) : ucReportSearch.propFromDate).AddDays(i).Day).FirstOrDefault();
                            if (old != null)
                            {
                                ReportCommissionControl n = new ReportCommissionControl();
                                n.DATE = old.DATE;
                                n.InitialRevenue = old.InitialRevenue;
                                n.InitialCommission = old.InitialCommission;
                                n.ActualRevenue = old.ActualRevenue;
                                n.ActualCommission = old.ActualCommission;
                                newLSt.Add(n);
                                MyVal += Convert.ToDouble(string.IsNullOrEmpty(old.InitialRevenue) ? "0" : old.InitialRevenue);
                            }
                            else
                            {
                                ReportCommissionControl n = new ReportCommissionControl();
                                n.DATE = (ucReportSearch.propFromDate == DateTime.MinValue ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) : ucReportSearch.propFromDate).AddDays(i);
                                n.InitialRevenue = "0";
                                n.InitialCommission = "0";
                                n.ActualRevenue = "0";
                                n.ActualCommission = "0";
                                newLSt.Add(n);
                                MyVal += Convert.ToInt32("0");
                            }
                            loopcount++;
                        }
                        rptDaysLead.DataSource = newLSt;
                        rptDaysLead.DataBind();
                        lblCommissionTotal.Text =Convert.ToString(MyVal);
                        if (loopcount == 0)
                        {
                            lblAvgPerDay.Text = Convert.ToString(MyVal / 1);
                        }
                        else
                        {
                            lblAvgPerDay.Text = Convert.ToString(MyVal / loopcount);
                        }
                        break;
                }
            }
        }
    }
    protected void rptDaysName_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblDays = (Label)e.Item.FindControl("lblDays");
            ReportDayList d = (ReportDayList)e.Item.DataItem;
            if (d != null)
            {
                switch (lblReportName.Text.ToLower())
                {
                    case "commission control report":
                        lblDays.Text = "<table border=\"1\"><tr><td colspan=\"4\" align=\"center\">"+d.DATE.ToString("dd/MM/yy")+"</td></tr><tr><td>Initial Value</td><td>Initial Commission</td><td>Actual Value</td><td>Actual Commission</td></tr></table>";//d.DATE.ToString("dd/MM/yy");
                        break;
                    case "revenue report":
                        lblDays.Text = "<table border=\"1\"><tr><td colspan=\"2\" align=\"center\">" + d.DATE.ToString("dd/MM/yy") + "</td></tr><tr><td>Total Revenue</td><td>Commission Revenue</td></tr></table>";//d.DATE.ToString("dd/MM/yy");
                        break;
                    default:
                        lblDays.Text = d.DATE.ToString("dd/MM/yy");
                        break;
                }
                
            }
        }
    }

    
    protected void rptDaysLead_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblLead = (Label)e.Item.FindControl("lblLead");
            switch (lblReportName.Text.ToLower())
            {
                case "lead time report":
                    ReportResultDateAndValue valLead = (ReportResultDateAndValue)e.Item.DataItem;
                    if (valLead != null)
                    {
                        lblLead.Text = valLead.VALUE == null ? "" : valLead.VALUE;
                    }
                    break;
                case "availability report":
                    ReportAvailability valAvail = (ReportAvailability)e.Item.DataItem;
                    if (valAvail != null)
                    {
                        lblLead.Text = "";
                        lblLead.Text += "<table border=\"1\"><tr><td>" + (valAvail.MorningStatus == null ? "" : valAvail.MorningStatus) + "</td></tr><tr><td>" + (valAvail.AfternoonStatus == null ? "" : valAvail.AfternoonStatus) + "</td></tr></table>";
                    }
                    break;
                case "online inventory changes report":
                    ReportOnlineInventories valOnline = (ReportOnlineInventories)e.Item.DataItem;
                    if (valOnline != null)
                    {
                        lblLead.Text = "<table border=\"1\" width=\"100%\"><tr><td width=\"50%\">" + (valOnline.BookedMeetingRoom == null ? "0" : valOnline.BookedMeetingRoom) + "/" + (valOnline.MeetingroomCount == null ? "0" : valOnline.MeetingroomCount) + "</td><td width=\"50%\">" + Math.Round((Convert.ToDecimal(valOnline.BookedMeetingRoom == null ? "0" : valOnline.BookedMeetingRoom) / Convert.ToDecimal(valOnline.MeetingroomCount == null ? "0" : valOnline.MeetingroomCount)) * 100, 2) + "%</td></tr></table>";
                    }
                    break;
                case "commission control report":
                    ReportCommissionControl valComControl = (ReportCommissionControl)e.Item.DataItem;
                    if (valComControl != null)
                    {
                        lblLead.Text = "<table border=\"1\" width=\"100%\"><tr><td>" + (valComControl.InitialRevenue == null ? "0" : valComControl.InitialRevenue) + "</td><td>" + (valComControl.InitialCommission == null ? "0" : valComControl.InitialCommission) + "</td><td>" + (valComControl.ActualRevenue == null ? "0" : valComControl.ActualRevenue) + "</td><td>" + (valComControl.ActualCommission == null ? "0" : valComControl.ActualCommission) + "</td></tr></table>";
                    }
                    break;
                case "revenue report":
                    ReportCommissionControl valrevenue = (ReportCommissionControl)e.Item.DataItem;
                    if (valrevenue != null)
                    {
                        lblLead.Text = "<table border=\"1\" width=\"100%\"><tr><td>" + (valrevenue.InitialRevenue == null ? "0" : valrevenue.InitialRevenue) + "</td><td>" + (valrevenue.ActualRevenue == null ? "0" : valrevenue.ActualRevenue) + "</td></tr></table>";
                    }
                    break;
                case "conversion booking report":
                    ReportProductionStat Valconv = (ReportProductionStat)e.Item.DataItem;
                    if (Valconv != null)
                    {
                        lblLead.Text = "<table border=\"1\" width=\"100%\"><tr><td>" + (Valconv.conv == null ? "0" : Valconv.conv) + "</td><td>" + (Valconv.Convper == null ? "0" : Valconv.Convper) + "</td></tr></table>";
                    }
                    break;
            
            
            }
        }
    }
    #endregion    

    #region Grid For Cancelation report Handlers
    protected void rptCancelreport_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdf=(HiddenField)e.Item.FindControl("hdf1");
            Label lblcancelcountryname = (Label)e.Item.FindControl("lblcancelcountryname");
            Label lblcancelcityname = (Label)e.Item.FindControl("lblcancelcityname");
            Label lblcancelhotelname = (Label)e.Item.FindControl("lblcancelhotelname");
            Label lblcancelbookbyclient = (Label)e.Item.FindControl("lblcancelbookbyclient");
            Label lblcancelbookbyoperator = (Label)e.Item.FindControl("lblcancelbookbyoperator");
            Label lblcancelrequestbyoperator = (Label)e.Item.FindControl("lblcancelrequestbyoperator");
            Label lblcancelrequestbyhotel = (Label)e.Item.FindControl("lblcancelrequestbyhotel");

            ViewReportCancelation cancel = e.Item.DataItem as ViewReportCancelation;
            if (cancel != null)
            {

                lblcancelcountryname.Text = cancel.CountryName;
                lblcancelcityname.Text = cancel.City;
                lblcancelhotelname.Text = cancel.HotelName;
                hdf.Value=cancel.HotelId.ToString();
                List<ViewReportCancelation> vcancel = vcancelation.Where(a => a.HotelId == Convert.ToInt64(hdf.Value)).ToList();
                lblcancelbookbyclient.Text = vcancel.Sum(a => a.CancelBookByClient).ToString();
                lblcancelbookbyoperator.Text = vcancel.Sum(a => a.CancelBookByOperator).ToString();
                lblcancelrequestbyoperator.Text = vcancel.Sum(a => a.CancelReqByOperator).ToString();
                lblcancelrequestbyhotel.Text = vcancel.Sum(a => a.CancelReqByHotel).ToString();

                

            }
        }
        
    }

    #endregion

    #region Grid For Production report Handlers
    protected void rptPMaster_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        #region Header
        if (e.Item.ItemType == ListItemType.Header)
        {
            System.Web.UI.HtmlControls.HtmlTableCell tdPCountry = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdPCountry");
            System.Web.UI.HtmlControls.HtmlTableCell tdPCity = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdPCity");
            
            if (ucReportSearch.propCountryID == 0)
            {
                tdPCountry.Visible = true;
                tdPCity.Visible = true;
            }
            else if (ucReportSearch.propCityID == 0)
            {
                tdPCountry.Visible = false;
                tdPCity.Visible = true;
            }
            else
            {
                tdPCountry.Visible = false;
                tdPCity.Visible = false;
            }
            Label lblheaderType = (Label)e.Item.FindControl("lblheaderType");
            switch (lblReportName.Text.ToLower())
            {
                case "production report hotel and meeting facilities":
                    lblheaderType.Text = "Hotel Name";
                    
                    break;
                default:
                    lblheaderType.Text = "Company Name";
                   
                    break;
            }
            Repeater rptPMonthName = (Repeater)e.Item.FindControl("rptPMonthName");
            List<ReportMonthList> MonthList = new List<ReportMonthList>();
            for (int i = 0; i <= 11; i++)
            {
                ReportMonthList d = new ReportMonthList();
                d.MonthID = i + 1;
                d.MonthName = (i == 0 ? "Jan" : (i == 1 ? "Feb" : (i == 2 ? "Mar" : (i == 3 ? "Apr" : (i == 4 ? "May" : (i == 5 ? "Jun" : (i == 6 ? "Jul" : (i == 7 ? "Aug" : (i == 8 ? "Sep" : (i == 9 ? "Oct" : (i == 10 ? "Nov" : (i == 11 ? "Dec" : ""))))))))))));
                MonthList.Add(d);
            }
            rptPMonthName.DataSource = MonthList;
            rptPMonthName.DataBind();
        }
        #endregion
        #region Items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPCountry = (Label)e.Item.FindControl("lblPCountry");
            Label lblPCity = (Label)e.Item.FindControl("lblPCity");
            Label lblHotel = (Label)e.Item.FindControl("lblHotel");
            Label lblMeetingroom = (Label)e.Item.FindControl("lblMeetingroom");
            System.Web.UI.HtmlControls.HtmlTableCell tdPCountryBody = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdPCountryBody");
            System.Web.UI.HtmlControls.HtmlTableCell tdPCityBody = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdPCityBody");
            System.Web.UI.HtmlControls.HtmlTableRow trTotalCommBookingStat = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("trTotalCommBookingStat");
            System.Web.UI.HtmlControls.HtmlTableRow trStatBookingTimeoutStat = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("trStatBookingTimeoutStat");
            System.Web.UI.HtmlControls.HtmlTableRow trTotalCommRequestStat = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("trTotalCommRequestStat");
            System.Web.UI.HtmlControls.HtmlTableRow trStatRequestTimeoutStat = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("trStatRequestTimeoutStat");
            

            Repeater rptPMonthDetails = (Repeater)e.Item.FindControl("rptPMonthDetails");
            ReportMasterDetails r = e.Item.DataItem as ReportMasterDetails;
            if (r != null)
            {
                lblPCountry.Text = r.CountryName == null ? "" : r.CountryName;
                lblPCity.Text = r.CityName == null ? "" : r.CityName;
                lblHotel.Text = r.HotelName == null ? "" : r.HotelName;
                if (r.MeetingroomName != null)
                {
                    lblMeetingroom.Text = r.MeetingroomName == null ? "" : r.MeetingroomName;
                }
                if (ucReportSearch.propCountryID == 0)
                {
                    tdPCountryBody.Visible = true;
                    tdPCityBody.Visible = true;
                }
                else if (ucReportSearch.propCityID == 0)
                {
                    tdPCountryBody.Visible = false;
                    tdPCityBody.Visible = true;
                }
                else
                {
                    tdPCountryBody.Visible = false;
                    tdPCityBody.Visible = false;
                }
                List<ReportProductionStat> lstRepProduction = new List<ReportProductionStat>();
                
                switch (lblReportName.Text.ToLower())
                {
                    case "production report hotel and meeting facilities":
                         trTotalCommBookingStat.Visible = true;
                    trStatBookingTimeoutStat.Visible = true;
                    trTotalCommRequestStat.Visible = true;
                    trStatRequestTimeoutStat.Visible = true;
                        for (int i = 1; i <= 12; i++)
                        {

                            ViewReportProductionReportHotel vp = vProductionHotel.Where(a => a.Id == r.HotelID && a.Months == i).FirstOrDefault();
                            if (vp != null)
                            {
                                ReportProductionStat rps = new ReportProductionStat();
                                rps.MonthID = i;
                                rps.NoOfBookings = Convert.ToString(vp.NoOfBooking);
                                rps.NoOfBookingCanceled = Convert.ToString(vp.NoOfBookingCancled);
                                rps.PTotalAmountForBooking = Convert.ToString(vp.TotalRevenueAmountFromBooking);
                                rps.NoOfReques = Convert.ToString(vp.NoOfRequest);
                                rps.NoOfRequestCanceled = Convert.ToString(vp.NoOfRequestCancled);
                                //rps.TotalRequestRevenue = Convert.ToString(vp.Modifyrevenuefromrequest);
                                rps.ModifyRevenue = Convert.ToString(vp.Modifyrevenuefromrequest);
                                rps.TotalCommission = Convert.ToString(vp.TotalCommission);
                                rps.TotalCommissionRequest = Convert.ToString(vp.TotalCommissionRequest);
                                rps.StatisticsBookingTimeout = Convert.ToString(vp.StatisticsBookingTimeout);
                                rps.StatisticsRequestTimeout = Convert.ToString(vp.StatisticsRequestTimeout);
                                lstRepProduction.Add(rps);
                            }
                            else
                            {
                                ReportProductionStat rps = new ReportProductionStat();
                                rps.MonthID = i;
                                lstRepProduction.Add(rps);
                            }
                        }
                        break;
                    default:
                          trTotalCommBookingStat.Visible = false;
                    trStatBookingTimeoutStat.Visible = false;
                    trTotalCommRequestStat.Visible = false;
                    trStatRequestTimeoutStat.Visible = false;
                        for (int i = 1; i <= 12; i++)
                        {

                            ViewReportProductionCompanyAgency vp = vProduction.Where(a => a.Userid == r.HotelID && a.Months == i).FirstOrDefault();
                            if (vp != null)
                            {
                                ReportProductionStat rps = new ReportProductionStat();
                                rps.MonthID = i;
                                rps.NoOfBookings = Convert.ToString(vp.NoOfBooking);
                                rps.NoOfBookingCanceled = Convert.ToString(vp.NoOfBookingCancled);
                                rps.PTotalAmountForBooking = Convert.ToString(vp.TotalRevenueAmountFromBooking);
                                rps.NoOfReques = Convert.ToString(vp.NoOfRequest);
                                rps.NoOfRequestCanceled = Convert.ToString(vp.NoOfRequestCancled);
                                //rps.TotalRequestRevenue = Convert.ToString(vp.Modifyrevenuefromrequest);
                                rps.ModifyRevenue = Convert.ToString(vp.Modifyrevenuefromrequest);
                                lstRepProduction.Add(rps);
                            }
                            else
                            {
                                ReportProductionStat rps = new ReportProductionStat();
                                rps.MonthID = i;
                                lstRepProduction.Add(rps);
                            }
                        }
                        break;
                }
                rptPMonthDetails.DataSource = lstRepProduction;//vProduction.Where(a => a.Id == r.HotelID).Select(a => new ReportResultDateAndValue() { DATE = Convert.ToDateTime(a.AvailabilityDate == null ? DateTime.Now : a.AvailabilityDate), VALUE = Convert.ToString(a.LeadTimeForMeetingRoom) }).ToList();
                rptPMonthDetails.DataBind();
            }
        }
        #endregion
        #region Footer
        if (e.Item.ItemType == ListItemType.Footer)
        {
            //Label lblHotelFooter = (Label)e.Item.FindControl("lblHotelFooter");
            System.Web.UI.HtmlControls.HtmlTableCell tdPCountryFooter = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdPCountryFooter");
            System.Web.UI.HtmlControls.HtmlTableCell tdPCityFooter = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("tdPCityFooter");
            System.Web.UI.HtmlControls.HtmlTableRow trTotalCommBookingStatFooter = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("trTotalCommBookingStatFooter");
            System.Web.UI.HtmlControls.HtmlTableRow trStatBookingTimeoutStatFooter = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("trStatBookingTimeoutStatFooter");
            System.Web.UI.HtmlControls.HtmlTableRow trTotalCommRequestStatFooter = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("trTotalCommRequestStatFooter");
            System.Web.UI.HtmlControls.HtmlTableRow trStatRequestTimeoutStatFooter = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("trStatRequestTimeoutStatFooter");
            Repeater rptPMonthDetailsFooter = (Repeater)e.Item.FindControl("rptPMonthDetailsFooter");
            //ReportMasterDetails r = e.Item.DataItem as ReportMasterDetails;
            //if (r != null)
            //{
                //lblHotelFooter.Text = r.HotelName == null ? "" : r.HotelName;
                if (ucReportSearch.propCountryID == 0)
                {
                    tdPCountryFooter.Visible = true;
                    tdPCityFooter.Visible = true;
                }
                else if (ucReportSearch.propCityID == 0)
                {
                    tdPCountryFooter.Visible = false;
                    tdPCityFooter.Visible = true;
                }
                else
                {
                    tdPCountryFooter.Visible = false;
                    tdPCityFooter.Visible = false;
                }
                List<ReportProductionStat> lstRepProductionFooter = new List<ReportProductionStat>();

                switch (lblReportName.Text.ToLower())
                {
                    case "production report hotel and meeting facilities":
                        trTotalCommBookingStatFooter.Visible = true;
                        trStatBookingTimeoutStatFooter.Visible = true;
                        trTotalCommRequestStatFooter.Visible = true;
                        trStatRequestTimeoutStatFooter.Visible = true;
                        for (int i = 1; i <= 12; i++)
                        {

                            List<ViewReportProductionReportHotel> vp = vProductionHotel.Where(a =>a.Months == i).ToList();
                            if (vp != null)
                            {
                                ReportProductionStat rps = new ReportProductionStat();
                                rps.MonthID = i;
                                for (int j = 0; j < vp.Count; j++)
                                {
                                    rps.NoOfBookings = (Convert.ToInt32(rps.NoOfBookings == null ? "0" : rps.NoOfBookings) + Convert.ToInt32(vp[j].NoOfBooking)).ToString();
                                    rps.NoOfBookingCanceled = (Convert.ToInt32(rps.NoOfBookingCanceled == null ? "0" : rps.NoOfBookingCanceled) + Convert.ToInt32(vp[j].NoOfBookingCancled)).ToString();
                                    rps.PTotalAmountForBooking = (Convert.ToInt32(rps.PTotalAmountForBooking == null ? "0" : rps.PTotalAmountForBooking) + Convert.ToInt32(vp[j].TotalRevenueAmountFromBooking)).ToString();
                                    rps.NoOfReques = (Convert.ToInt32(rps.NoOfReques == null ? "0" : rps.NoOfReques) + Convert.ToInt32(vp[j].NoOfRequest)).ToString();
                                    rps.NoOfRequestCanceled = (Convert.ToInt32(rps.NoOfRequestCanceled == null ? "0" : rps.NoOfRequestCanceled) + Convert.ToInt32(vp[j].NoOfRequestCancled)).ToString();
                                    //rps.TotalRequestRevenue = Convert.ToString(vp.Modifyrevenuefromrequest);
                                    rps.ModifyRevenue = (Convert.ToInt32(rps.ModifyRevenue == null ? "0" : rps.ModifyRevenue) + Convert.ToInt32(vp[j].Modifyrevenuefromrequest)).ToString();
                                    rps.TotalCommission = (Convert.ToInt32(rps.TotalCommission == null ? "0" : rps.TotalCommission) + Convert.ToInt32(vp[j].TotalCommission)).ToString();
                                    rps.TotalCommissionRequest = (Convert.ToInt32(rps.TotalCommissionRequest == null ? "0" : rps.TotalCommissionRequest) + Convert.ToInt32(vp[j].TotalCommissionRequest)).ToString();
                                    rps.StatisticsBookingTimeout = (Convert.ToInt32(rps.StatisticsBookingTimeout == null ? "0" : rps.StatisticsBookingTimeout) + Convert.ToInt32(vp[j].StatisticsBookingTimeout)).ToString();
                                    rps.StatisticsRequestTimeout = (Convert.ToInt32(rps.StatisticsRequestTimeout == null ? "0" : rps.StatisticsRequestTimeout) + Convert.ToInt32(vp[j].StatisticsRequestTimeout)).ToString();
                                }
                                lstRepProductionFooter.Add(rps);
                            }
                            else
                            {
                                ReportProductionStat rps = new ReportProductionStat();
                                rps.MonthID = i;
                                lstRepProductionFooter.Add(rps);
                            }
                        }
                        break;
                    default:
                        trTotalCommBookingStatFooter.Visible = false;
                        trStatBookingTimeoutStatFooter.Visible = false;
                        trTotalCommRequestStatFooter.Visible = false;
                        trStatRequestTimeoutStatFooter.Visible = false;
                        for (int i = 1; i <= 12; i++)
                        {

                            List<ViewReportProductionCompanyAgency> vp = vProduction.Where(a => a.Months == i).ToList();
                            if (vp != null)
                            {
                                ReportProductionStat rps = new ReportProductionStat();
                                rps.MonthID = i;
                                for (int j = 0; j < vp.Count; j++)
                                {
                                    rps.NoOfBookings = (Convert.ToInt32(rps.NoOfBookings == null ? "0" : rps.NoOfBookings) + Convert.ToInt32(vp[j].NoOfBooking)).ToString();
                                    rps.NoOfBookingCanceled = (Convert.ToInt32(rps.NoOfBookingCanceled == null ? "0" : rps.NoOfBookingCanceled) + Convert.ToInt32(vp[j].NoOfBookingCancled)).ToString();//Convert.ToString(vp.NoOfBookingCancled);
                                    rps.PTotalAmountForBooking = (Convert.ToInt32(rps.PTotalAmountForBooking == null ? "0" : rps.PTotalAmountForBooking) + Convert.ToInt32(vp[j].TotalRevenueAmountFromBooking)).ToString();//Convert.ToString(vp.TotalRevenueAmountFromBooking);
                                    rps.NoOfReques = (Convert.ToInt32(rps.NoOfReques == null ? "0" : rps.NoOfReques) + Convert.ToInt32(vp[j].NoOfRequest)).ToString();//Convert.ToString(vp.NoOfRequest);
                                    rps.NoOfRequestCanceled = (Convert.ToInt32(rps.NoOfRequestCanceled == null ? "0" : rps.NoOfRequestCanceled) + Convert.ToInt32(vp[j].NoOfRequestCancled)).ToString();//Convert.ToString(vp.NoOfRequestCancled);
                                    //rps.TotalRequestRevenue = Convert.ToString(vp.Modifyrevenuefromrequest);
                                    rps.ModifyRevenue = (Convert.ToInt32(rps.ModifyRevenue == null ? "0" : rps.ModifyRevenue) + Convert.ToInt32(vp[j].Modifyrevenuefromrequest)).ToString();//Convert.ToString(vp.Modifyrevenuefromrequest);
                                }
                                lstRepProductionFooter.Add(rps);
                            }
                            else
                            {
                                ReportProductionStat rps = new ReportProductionStat();
                                rps.MonthID = i;
                                lstRepProductionFooter.Add(rps);
                            }
                        }
                        break;
                }
                rptPMonthDetailsFooter.DataSource = lstRepProductionFooter;//vProduction.Where(a => a.Id == r.HotelID).Select(a => new ReportResultDateAndValue() { DATE = Convert.ToDateTime(a.AvailabilityDate == null ? DateTime.Now : a.AvailabilityDate), VALUE = Convert.ToString(a.LeadTimeForMeetingRoom) }).ToList();
                rptPMonthDetailsFooter.DataBind();
            //}
        }
        #endregion
    }
    protected void rptPMonthName_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPDays = (Label)e.Item.FindControl("lblPDays");
            
            ReportMonthList d = (ReportMonthList)e.Item.DataItem;
            if (d != null)
            {
                lblPDays.Text = d.MonthName;
            }
        }
    }
    protected void rptPMonthDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPNoOfBookings = (Label)e.Item.FindControl("lblPNoOfBookings");
            Label lblPNoOfBookingCanceled = (Label)e.Item.FindControl("lblPNoOfBookingCanceled");
            Label lblPTotalAmountForBooking = (Label)e.Item.FindControl("lblPTotalAmountForBooking");
            Label lblPNoOfRequest = (Label)e.Item.FindControl("lblPNoOfRequest");
            Label lblPNoOfRequestCanceled = (Label)e.Item.FindControl("lblPNoOfRequestCanceled");
            Label lblTotalCommissionBooking = (Label)e.Item.FindControl("lblTotalCommissionBooking");
            Label lblStatisticsBookingTimeout = (Label)e.Item.FindControl("lblStatisticsBookingTimeout");
            Label lblTotalCommissionRequest = (Label)e.Item.FindControl("lblTotalCommissionRequest");
            Label lblStatRequestTimeout = (Label)e.Item.FindControl("lblStatRequestTimeout");
            //Label lblPTotalRequestRevenue = (Label)e.Item.FindControl("lblPTotalRequestRevenue");
            Label lblPModifyRevenue = (Label)e.Item.FindControl("lblPModifyRevenue");
            System.Web.UI.HtmlControls.HtmlTableRow trStatRequestTimeoutStatBody = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("trStatRequestTimeoutStatBody");
            System.Web.UI.HtmlControls.HtmlTableRow trTotalCommRequestStatBody = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("trTotalCommRequestStatBody");
            System.Web.UI.HtmlControls.HtmlTableRow trStatBookingTimeoutStatBody = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("trStatBookingTimeoutStatBody");
            System.Web.UI.HtmlControls.HtmlTableRow trTotalCommBookingStatBody = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("trTotalCommBookingStatBody");
            
            ReportProductionStat r = (ReportProductionStat)e.Item.DataItem;
            if (r != null)
            {
                lblPNoOfBookings.Text = r.NoOfBookings == null ? "&nbsp;" : r.NoOfBookings;
                lblPNoOfBookingCanceled.Text = r.NoOfBookingCanceled == null ? "&nbsp;" : r.NoOfBookingCanceled;
                lblPTotalAmountForBooking.Text = r.PTotalAmountForBooking == null ? "&nbsp;" : r.PTotalAmountForBooking;
                lblPNoOfRequest.Text = r.NoOfReques == null ? "&nbsp;" : r.NoOfReques;
                lblPNoOfRequestCanceled.Text = r.NoOfRequestCanceled == null ? "&nbsp;" : r.NoOfRequestCanceled;
                //lblPTotalRequestRevenue.Text = r.TotalRequestRevenue == null ? "" : r.TotalRequestRevenue;
                lblPModifyRevenue.Text = r.ModifyRevenue == null ? "&nbsp;" : r.ModifyRevenue;
                
                switch (lblReportName.Text.ToLower())
                {
                    case "production report hotel and meeting facilities":
                       
                        trStatRequestTimeoutStatBody.Visible=true;
                            trTotalCommRequestStatBody.Visible=true;
                            trStatBookingTimeoutStatBody.Visible = true;
                                trTotalCommBookingStatBody.Visible=true;
                        lblTotalCommissionBooking.Text = r.TotalCommission == null ? "&nbsp;" : r.TotalCommission;
                        lblStatisticsBookingTimeout.Text = r.StatisticsBookingTimeout == null ? "&nbsp;" : r.StatisticsBookingTimeout;
                        lblTotalCommissionRequest.Text = r.TotalCommissionRequest == null ? "&nbsp;" : r.TotalCommissionRequest;
                        lblStatRequestTimeout.Text = r.StatisticsRequestTimeout == null ? "&nbsp;" : r.StatisticsRequestTimeout;
                        break;
                    default:
                        trStatRequestTimeoutStatBody.Visible=false;
                            trTotalCommRequestStatBody.Visible=false;
                            trStatBookingTimeoutStatBody.Visible = false;
                                trTotalCommBookingStatBody.Visible=false;
                        break;
                }
            }
        }
    }
    protected void rptPMonthDetailsFooter_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblPNoOfBookingsFooter = (Label)e.Item.FindControl("lblPNoOfBookingsFooter");
            Label lblPNoOfBookingCanceledFooter = (Label)e.Item.FindControl("lblPNoOfBookingCanceledFooter");
            Label lblPTotalAmountForBookingFooter = (Label)e.Item.FindControl("lblPTotalAmountForBookingFooter");
            Label lblPNoOfRequestFooter = (Label)e.Item.FindControl("lblPNoOfRequestFooter");
            Label lblPNoOfRequestCanceledFooter = (Label)e.Item.FindControl("lblPNoOfRequestCanceledFooter");
            Label lblTotalCommissionBookingFooter = (Label)e.Item.FindControl("lblTotalCommissionBookingFooter");
            Label lblStatisticsBookingTimeoutFooter = (Label)e.Item.FindControl("lblStatisticsBookingTimeoutFooter");
            Label lblTotalCommissionRequestFooter = (Label)e.Item.FindControl("lblTotalCommissionRequestFooter");
            Label lblStatRequestTimeoutFooter = (Label)e.Item.FindControl("lblStatRequestTimeoutFooter");
            //Label lblPTotalRequestRevenue = (Label)e.Item.FindControl("lblPTotalRequestRevenue");
            Label lblPModifyRevenueFooter = (Label)e.Item.FindControl("lblPModifyRevenueFooter");
            System.Web.UI.HtmlControls.HtmlTableRow trStatRequestTimeoutStatBodyFooter = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("trStatRequestTimeoutStatBodyFooter");
            System.Web.UI.HtmlControls.HtmlTableRow trTotalCommRequestStatBodyFooter = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("trTotalCommRequestStatBodyFooter");
            System.Web.UI.HtmlControls.HtmlTableRow trStatBookingTimeoutStatBody = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("trStatBookingTimeoutStatBody");
            System.Web.UI.HtmlControls.HtmlTableRow trTotalCommBookingStatBodyFooter = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("trTotalCommBookingStatBodyFooter");

            ReportProductionStat r = (ReportProductionStat)e.Item.DataItem;
            if (r != null)
            {
                lblPNoOfBookingsFooter.Text = r.NoOfBookings == null ? "&nbsp;" : r.NoOfBookings;
                lblPNoOfBookingCanceledFooter.Text = r.NoOfBookingCanceled == null ? "&nbsp;" : r.NoOfBookingCanceled;
                lblPTotalAmountForBookingFooter.Text = r.PTotalAmountForBooking == null ? "&nbsp;" : r.PTotalAmountForBooking;
                lblPNoOfRequestFooter.Text = r.NoOfReques == null ? "&nbsp;" : r.NoOfReques;
                lblPNoOfRequestCanceledFooter.Text = r.NoOfRequestCanceled == null ? "&nbsp;" : r.NoOfRequestCanceled;
                //lblPTotalRequestRevenue.Text = r.TotalRequestRevenue == null ? "" : r.TotalRequestRevenue;
                lblPModifyRevenueFooter.Text = r.ModifyRevenue == null ? "&nbsp;" : r.ModifyRevenue;

                switch (lblReportName.Text.ToLower())
                {
                    case "production report hotel and meeting facilities":

                        trStatRequestTimeoutStatBodyFooter.Visible = true;
                        trTotalCommRequestStatBodyFooter.Visible = true;
                        trStatBookingTimeoutStatBody.Visible = true;
                        trTotalCommBookingStatBodyFooter.Visible = true;
                        lblTotalCommissionBookingFooter.Text = r.TotalCommission == null ? "&nbsp;" : r.TotalCommission;
                        lblStatisticsBookingTimeoutFooter.Text = r.StatisticsBookingTimeout == null ? "&nbsp;" : r.StatisticsBookingTimeout;
                        lblTotalCommissionRequestFooter.Text = r.TotalCommissionRequest == null ? "&nbsp;" : r.TotalCommissionRequest;
                        lblStatRequestTimeoutFooter.Text = r.StatisticsRequestTimeout == null ? "&nbsp;" : r.StatisticsRequestTimeout;
                        break;
                    default:
                        trStatRequestTimeoutStatBodyFooter.Visible = false;
                        trTotalCommRequestStatBodyFooter.Visible = false;
                        trStatBookingTimeoutStatBody.Visible = false;
                        trTotalCommBookingStatBodyFooter.Visible = false;
                        break;
                }
            }
        }
    }
    #endregion    

    #region Grid For BookingRequestHistory report Handlers
    protected void rptbrhmaster_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Header)
        {
            
            Repeater rptbrhDaysName = (Repeater)e.Item.FindControl("rptbrhDaysName");
            List<ReportDayList> DayList = new List<ReportDayList>();
            DateTime dtm = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            TimeSpan t = dtm.Subtract(DateTime.Now);
            for (int i = 0; (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).AddDays(i) <= (ucReportSearch.propToDate == DateTime.MinValue ? DateTime.Now : ucReportSearch.propToDate); i++)
            {
                ReportDayList d = new ReportDayList();
                d.DATE = (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).AddDays(i);
                DayList.Add(d);
            }
            rptbrhDaysName.DataSource = DayList;
            rptbrhDaysName.DataBind();
        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblCountry = (Label)e.Item.FindControl("lblCountry");
            Label lblCity = (Label)e.Item.FindControl("lblCity");
            Label lblHotel = (Label)e.Item.FindControl("lblHotel");
            Repeater rptbrhDaysLead = (Repeater)e.Item.FindControl("rptbrhDaysLead");
            ReportMasterDetails r = e.Item.DataItem as ReportMasterDetails;
            if (r != null)
            {
                lblCountry.Text = r.CountryName == null ? "" : r.CountryName;
                lblCity.Text = r.CityName == null ? "" : r.CityName;
                lblHotel.Text = r.HotelName == null ? "" : r.HotelName;
                List<ReportResultDateAndValue> lst = vbookreqhistory.Where(a => a.HotelId == r.HotelID).Select(a => new ReportResultDateAndValue() { d = Convert.ToDateTime(a.BookingDate).ToString("dd/MM/yy"), VALUE = Convert.ToString(a.CountBooking) }).ToList();
                List<ReportResultDateAndValue> newLSt = new List<ReportResultDateAndValue>();
                DateTime dtm = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                TimeSpan t = dtm.Subtract(DateTime.Now);
                //for (int i = 0; (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).AddDays(i) <= (ucReportSearch.propToDate == DateTime.MinValue ? DateTime.Now : ucReportSearch.propToDate); i++)
                for (int i = 0; (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).AddDays(i) <= (ucReportSearch.propToDate == DateTime.MinValue ? DateTime.Now : ucReportSearch.propToDate); i++)
                 {
                    
                     //ReportResultDateAndValue old = lst.Where(a => a.d == (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).AddDays(i).ToString("dd/MM/yy")).FirstOrDefault();
                     ReportResultDateAndValue old = lst.Where(a => a.d == (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).AddDays(i).ToString("dd/MM/yy")).FirstOrDefault();

                     ReportResultDateAndValue n1 = new ReportResultDateAndValue();
                     int total = 0;
                     if (old != null)
                     {
                         foreach (ReportResultDateAndValue test in lst.Where(a => a.d == (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).AddDays(i).ToString("dd/MM/yy")))
                     {

                         n1.d = old.d;
                         n1.VALUE = test.VALUE;
                         total += Convert.ToInt32(n1.VALUE);

                     }

                    
                        ReportResultDateAndValue n = new ReportResultDateAndValue();
                        n.d = n1.d;
                        n.VALUE = total.ToString();
                        newLSt.Add(n);
                    }
                    else
                    {
                        ReportResultDateAndValue n = new ReportResultDateAndValue();
                        n.d = (ucReportSearch.propFromDate == DateTime.MinValue ? DateTime.Now.AddDays(t.Days) : ucReportSearch.propFromDate).AddDays(i).ToString("dd/MM/yy");
                        n.VALUE = "";
                        newLSt.Add(n);
                    }
                }
                rptbrhDaysLead.DataSource = newLSt;
                rptbrhDaysLead.DataBind();
            }
        }
    }
    protected void rptbrhDaysName_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblDays = (Label)e.Item.FindControl("lblDays");
            ReportDayList d = (ReportDayList)e.Item.DataItem;
            if (d != null)
            {
                lblDays.Text = d.DATE.ToString("dd/MM/yy");
            }
        }
    }
    protected void rptbrhDaysLead_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblLead = (Label)e.Item.FindControl("lblLead");
            ReportResultDateAndValue valLead = (ReportResultDateAndValue)e.Item.DataItem;
            if (valLead != null)
            {
                lblLead.Text = valLead.VALUE == "" ? "0" : valLead.VALUE;
            }
        }
    }
    #endregion

    #region Grid For inventory report Handlers

    protected void rptgrdinvExisthotel_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Header)
        {
            System.Web.UI.HtmlControls.HtmlTableCell header1 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("header1");
            System.Web.UI.HtmlControls.HtmlTableCell header2 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("header2");

            Label lblheadercountry = (Label)e.Item.FindControl("lblheadercountry");
            Label lblheadercity = (Label)e.Item.FindControl("lblheadercity");
            Label lblheaderhotel = (Label)e.Item.FindControl("lblheaderhotel");
            Label lblheadernoofmeeting = (Label)e.Item.FindControl("lblheadernoofmeeting");
            Label lblheadermeetingonline = (Label)e.Item.FindControl("lblheadermeetingonline");
            switch (lblReportName.Text.ToLower())
            {
                case "inventory report of existing hotel":
                    lblheadercountry.Text = "Country";
                    lblheadercity.Text = "City";
                    lblheaderhotel.Text = "Hotel";
                    header1.Visible = true;
                    header2.Visible = true;
                    lblheadernoofmeeting.Text = "No of MeetingRoom";
                    lblheadermeetingonline.Text = "No of MeetingRoom Online";
                    break;
                case "added hotel in period inventory report":
                    lblheadercountry.Text = "Country";
                    lblheadercity.Text = "City";
                    lblheaderhotel.Text = "Hotel";
                    header1.Visible = false;
                    header2.Visible = false;
                    break;
            }
        }
        
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            System.Web.UI.HtmlControls.HtmlTableCell item1 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("item1");
            System.Web.UI.HtmlControls.HtmlTableCell item2 = (System.Web.UI.HtmlControls.HtmlTableCell)e.Item.FindControl("item2");

            Label lblinvExisthotelcountryname = (Label)e.Item.FindControl("lblinvExisthotelcountryname");
            Label lblinvExisthotelcityname = (Label)e.Item.FindControl("lblinvExisthotelcityname");
            Label lblinvExisthotelhotelname = (Label)e.Item.FindControl("lblinvExisthotelhotelname");
            Label lblinvExisthotelmeetingrooms = (Label)e.Item.FindControl("lblinvExisthotelmeetingrooms");
            Label lblinvExisthotelmeetingroomsonline = (Label)e.Item.FindControl("lblinvExisthotelmeetingroomsonline");

            
                switch (lblReportName.Text.ToLower())
                {
                    case "inventory report of existing hotel":
                        ViewreportInventoryReportofExistingHotel invreport = e.Item.DataItem as ViewreportInventoryReportofExistingHotel;

                        if (invreport != null)
                        {
                            item1.Visible = true;
                            item2.Visible = true;
                            lblinvExisthotelcountryname.Text = invreport.Countryname;
                            lblinvExisthotelcityname.Text = invreport.City;
                            lblinvExisthotelhotelname.Text = invreport.HotelName;
                            lblinvExisthotelmeetingrooms.Text = invreport.NoOfMeetingRoom.ToString();
                            lblinvExisthotelmeetingroomsonline.Text = invreport.NoofMeetingRoomOnline.ToString();
                        }
                        break;
                    case "added hotel in period inventory report":
                        item1.Visible = false;
                        item2.Visible = false;
                        Hotel data = e.Item.DataItem as Hotel;
                        Country objCountry = objHotelManager.GetByCountryID(data.CountryId);
                        lblinvExisthotelcountryname.Text = (objCountry.CountryName == null ? "" : objCountry.CountryName);
                        City objCity = objHotelManager.GetByCityID(data.CityId);
                        lblinvExisthotelcityname.Text = (objCity.City == null ? "" : objCity.City);
                        lblinvExisthotelhotelname.Text = data.Name;
                        break;
                }

            
        }
    }
    #endregion

    
    protected void rptConver_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Header)
        {
            
               Repeater rptPMonthName = (Repeater)e.Item.FindControl("rptPMonthName");
            List<ReportMonthList> MonthList = new List<ReportMonthList>();
            for (int i = 0; i <= 11; i++)
            {
                ReportMonthList d = new ReportMonthList();
                d.MonthID = i + 1;
                d.MonthName = (i == 0 ? "Jan" : (i == 1 ? "Feb" : (i == 2 ? "Mar" : (i == 3 ? "Apr" : (i == 4 ? "May" : (i == 5 ? "Jun" : (i == 6 ? "Jul" : (i == 7 ? "Aug" : (i == 8 ? "Sep" : (i == 9 ? "Oct" : (i == 10 ? "Nov" : (i == 11 ? "Dec" : ""))))))))))));
                MonthList.Add(d);
            }
            rptPMonthName.DataSource = MonthList;
            rptPMonthName.DataBind();
        }
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblCountry = (Label)e.Item.FindControl("lblCountry");
            Label lblCity = (Label)e.Item.FindControl("lblCity");
            Label lblHotel = (Label)e.Item.FindControl("lblHotel");
            List<ReportProductionStat> lstRepProduction = new List<ReportProductionStat>();
                
          
            Repeater rptDaysLead = (Repeater)e.Item.FindControl("rptDaysLead");
            ReportMasterDetails r = e.Item.DataItem as ReportMasterDetails;
            if (r != null)
            {
                lblCountry.Text = r.CountryName == null ? "" : r.CountryName;
                lblCity.Text = r.CityName == null ? "" : r.CityName;
                lblHotel.Text = r.HotelName == null ? "" : r.HotelName;

                for (int i = 1; i <= 12; i++)
                {

                    decimal[] obj = objviewstat.getreport(i, Convert.ToString( r.HotelID));
                   
                    //ViewReportProductionReportHotel vp = vProductionHotel.Where(a => a.Id == r.HotelID && a.Months == i).FirstOrDefault();
                    if (obj != null)
                    {
                        ReportProductionStat rps = new ReportProductionStat();
                        rps.conv = Convert.ToString(Math.Round(obj[2], 0)) + "/" + Convert.ToString(Math.Round(obj[0], 0));
                        //rps.Convper = Convert.ToString( Math.Round(Convert.ToDecimal( vp.Convper),2));
                        rps.Convper = Convert.ToString( Math.Round( obj[1],2))+"%";
                        lstRepProduction.Add(rps);
                    }
                    else
                    {
                        ReportProductionStat rps = new ReportProductionStat();
                      //rps.MonthID = i;
                        lstRepProduction.Add(rps);
                    }





                }
                rptDaysLead.DataSource = lstRepProduction;
                rptDaysLead.DataBind();
       
                
            }
        }
    }
}

#region Filter Class
class ReportMasterDetails
{
    public long CountryID
    {
        get;
        set;
    }
    public string CountryName
    {
        get;
        set;
    }
    public long CityID
    {
        get;
        set;
    }
    public string CityName
    {
        get;
        set;
    }
    public long HotelID
    {
        get;
        set;
    }
    public string HotelName
    {
        get;
        set;
    }
    public long MeetingroomID
    {
        get;
        set;
    }
    public string MeetingroomName
    {
        get;
        set;
    }
}
class ReportDayList
{
    public DateTime DATE
    {
        get;
        set;
    }
}
class ReportResultDateAndValue
{
    public DateTime DATE
    {
        get;
        set;
    }
    public string d
    {
        get;
        set;
    }
    public string VALUE
    {
        get;
        set;
    }
}
class ReportAvailability
{
    public DateTime DATE
    {
        get;
        set;
    }
    public string MorningStatus
    {
        get;
        set;
    }
    public string AfternoonStatus
    {
        get;
        set;
    }
}
class ReportOnlineInventories
{
    public DateTime DATE
    {
        get;
        set;
    }
    public string MeetingroomCount
    {
        get;
        set;
    }
    public string BookedMeetingRoom
    {
        get;
        set;
    }
}
class ReportCommissionControl
{
    public DateTime DATE
    {
        get;
        set;
    }
    public string InitialRevenue
    {
        get;
        set;
    }
    public string InitialCommission
    {
        get;
        set;
    }
    public string ActualRevenue
    {
        get;
        set;
    }
    public string ActualCommission
    {
        get;
        set;
    }
}

class DistinctItemComparerReportMaster : IEqualityComparer<ReportMasterDetails>
{

    public bool Equals(ReportMasterDetails x, ReportMasterDetails y)
    {
        return x.CountryID == y.CountryID &&
            x.CountryName == y.CountryName && x.CityID == y.CityID && x.CityName == y.CityName && x.HotelID == y.HotelID && x.HotelName == y.HotelName && x.MeetingroomID == y.MeetingroomID && x.MeetingroomName == y.MeetingroomName;
    }

    public int GetHashCode(ReportMasterDetails obj)
    {
        return obj.CountryID.GetHashCode() ^
            obj.CountryName.GetHashCode() ^ (obj.CityID == null ? 1 : obj.CityID.GetHashCode()) ^ (obj.CityName == null ? 1 : obj.CityName.GetHashCode()) ^ (obj.HotelID == null ? 1 : obj.HotelID.GetHashCode()) ^ (obj.HotelName == null ? 1 : obj.HotelName.GetHashCode()) ^ (obj.MeetingroomID == null ? 1 : obj.MeetingroomID.GetHashCode()) ^ (obj.MeetingroomName == null ? 1 : obj.MeetingroomName.GetHashCode());
    }
}

class ReportMonthList
{
    public int MonthID
    {
        get;
        set;
    }
    public string MonthName
    {
        get;
        set;
    }
}
class ReportProductionStat
{
    public int MonthID
    {
        get;
        set;
    }
    public string NoOfBookings
    {
        get;
        set;
    }
    public string NoOfBookingCanceled
    {
        get;
        set;
    }
    public string PTotalAmountForBooking
    {
        get;
        set;
    }
    public string NoOfReques
    {
        get;
        set;
    }
    public string NoOfRequestCanceled
    {
        get;
        set;
    }
    //public string TotalRequestRevenue
    //{
    //    get;
    //    set;
    //}
    public string ModifyRevenue
    {
        get;
        set;
    }
    public string TotalCommission
    {
        get;
        set;
    }
    public string TotalCommissionRequest
    {
        get;
        set;
    }
    public string StatisticsBookingTimeout
    {
        get;
        set;
    }
    public string StatisticsRequestTimeout
    {
        get;
        set;
    }
    public string Convper
    {
        get;
        set;
    }
    public string conv
    {
        get;
        set;
    }
}

class BookingRequestHistorystatus
{
    public long CountryID
    {
        get;
        set;
    }
    public string CountryName
    {
        get;
        set;
    }
    public long CityID
    {
        get;
        set;
    }
    public string CityName
    {
        get;
        set;
    }
    public long HotelID
    {
        get;
        set;
    }
    public string HotelName
    {
        get;
        set;
    }
    
}


#endregion