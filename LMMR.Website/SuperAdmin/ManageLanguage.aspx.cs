﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Data;
using LMMR.Entities;
using System.Data;
using System.Xml;
#endregion

public partial class SuperAdmin_ManageLanguage : System.Web.UI.Page
{
    #region Variables and Properties
    LanguageManager lm = new LanguageManager();
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetAllLanguage();
            divAddLanguage.Visible = false;
            divXML.Visible = false;
        }
    }
    #endregion

    #region Methods
    /// <summary>
    /// Get All Language details
    /// </summary>
    public void GetAllLanguage()
    {
        TList<Language> lstLanguage = lm.GetData();
        rptLanguage.DataSource = lstLanguage;
        rptLanguage.DataBind();
    }
    /// <summary>
    /// Bind XML for static contents
    /// </summary>
    /// <param name="language"></param>
    public void BindXML(string language)
    {
        DataSet ds = new DataSet();
        ds.ReadXml(Server.MapPath("~/resource/english/resultmessage.xml"));
        ds.Tables[0].PrimaryKey = new DataColumn[]{ds.Tables[0].Columns[0]};

        DataSet ds2 = new DataSet();
        ds2.ReadXml(Server.MapPath("~/resource/" + language.Replace(" ","_") + "/resultmessage.xml"));
        ds2.Tables[0].PrimaryKey = new DataColumn[] { ds2.Tables[0].Columns[0] };

        DataTable dt = new DataTable("resource");
        dt.Columns.Add("Key", typeof(string));
        dt.Columns.Add("Source", typeof(string));
        dt.Columns.Add("Target", typeof(string));
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            DataRow dtar = ds2.Tables[0].Rows.Find(dr["Key"]);
            if (dtar != null)
            {
                DataRow drow = dt.NewRow();
                drow["Key"] = dr["key"];
                drow["Source"] = dr["item_Text"];
                drow["Target"] = System.Net.WebUtility.HtmlDecode(Convert.ToString(dtar["item_Text"]));
                dt.Rows.Add(drow);
            }
            else
            {
                DataRow drow = dt.NewRow();
                drow["Key"] = dr["key"];
                drow["Source"] = dr["item_Text"];
                drow["Target"] = "";
                dt.Rows.Add(drow);
            }
        }
        rptXmlResult.DataSource = dt;
        rptXmlResult.DataBind();
        lblHaderXml.Text = "Manage static contents of Language (" + language + ")";
        divXML.Visible = true;
        ds = null;
        ds2 = null;
    }
    /// <summary>
    /// Save language file again
    /// </summary>
    /// <param name="language"></param>
    protected void SaveXML(string language)
    {
        XmlDocument doc = new XmlDocument();
        doc.Load(Server.MapPath("~/resource/" + language.ToLower().Replace(" ", "_") + "/resultmessage.xml"));
        foreach (RepeaterItem r in rptXmlResult.Items)
        {
            HiddenField hdnKey = (HiddenField)r.FindControl("hdnKey");
            TextBox txtString = (TextBox)r.FindControl("txtString");
            Label lblString = (Label)r.FindControl("lblString");
            XmlNode nod = doc.SelectSingleNode("/items/item[@key='" + hdnKey.Value + "']");
            if (rbtnTranslate.Checked)
            {
                if (nod != null)
                {
                    nod.InnerText = System.Net.WebUtility.HtmlEncode(txtString.Text.TrimStart());
                }
                else
                {
                    XmlNode newChild = doc.CreateNode(XmlNodeType.Element, "item", null);
                    XmlAttribute newAttr = doc.CreateAttribute("key");
                    newAttr.Value = hdnKey.Value.TrimStart().TrimEnd();
                    newChild.Attributes.Append(newAttr);
                    newChild.InnerText = System.Net.WebUtility.HtmlEncode(lblString.Text.TrimStart().TrimEnd());
                    doc.DocumentElement.AppendChild(newChild);
                    //doc.Save(Server.MapPath("~/resource/" + language.ToLower().Replace(" ", "_") + "/resultmessage.xml"));
                }
            }
            else if (rbtnUseEnglish.Checked)
            {
                if (nod != null)
                {
                    nod.InnerText = System.Net.WebUtility.HtmlEncode(lblString.Text.TrimStart().TrimEnd());
                }
                else
                {
                    XmlNode newChild = doc.CreateNode(XmlNodeType.Element, "item", null);
                    XmlAttribute newAttr = doc.CreateAttribute("key");
                    newAttr.Value = hdnKey.Value.TrimStart().TrimEnd();
                    newChild.Attributes.Append(newAttr);
                    newChild.InnerText = System.Net.WebUtility.HtmlEncode(lblString.Text.TrimStart().TrimEnd());
                    doc.DocumentElement.AppendChild(newChild);
                    //doc.Save(Server.MapPath("~/resource/" + language.ToLower().Replace(" ", "_") + "/resultmessage.xml"));
                }
            }
            else if (rbtnUseBlankEnglishCopy.Checked)
            {
                if (nod != null)
                {
                    if (txtString.Text.Trim().Length <= 0)
                    {
                        nod.InnerText = System.Net.WebUtility.HtmlEncode(lblString.Text.TrimStart());
                    }
                    else
                    {
                        nod.InnerText = System.Net.WebUtility.HtmlEncode(txtString.Text.TrimStart());
                    }
                }
                else
                {
                    XmlNode newChild = doc.CreateNode(XmlNodeType.Element, "item", null);
                    XmlAttribute newAttr = doc.CreateAttribute("key");
                    newAttr.Value = hdnKey.Value.TrimStart().TrimEnd();
                    newChild.Attributes.Append(newAttr);
                    newChild.InnerText = System.Net.WebUtility.HtmlEncode(lblString.Text.TrimStart().TrimEnd());
                    doc.DocumentElement.AppendChild(newChild);
                    //doc.Save(Server.MapPath("~/resource/" + language.ToLower().Replace(" ", "_") + "/resultmessage.xml"));
                }
            }
            else
            {
                if (nod != null)
                {
                    nod.InnerText = System.Net.WebUtility.HtmlEncode(txtString.Text.TrimStart());
                }
            }
        }
        doc.Save(Server.MapPath("~/resource/" + language.ToLower().Replace(" ", "_") + "/resultmessage.xml"));
        doc = null;
    }
    #endregion

    #region Event
    /// <summary>
    /// XML result Item Databound
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rptXmlResult_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblString = (Label)e.Item.FindControl("lblString");
            TextBox txtString = (TextBox)e.Item.FindControl("txtString");
            HiddenField hdnKey = (HiddenField)e.Item.FindControl("hdnKey");
            hdnKey.Value = DataBinder.Eval(e.Item.DataItem, "Key").ToString();
            lblString.Text = DataBinder.Eval(e.Item.DataItem, "Source").ToString();
            txtString.Text = System.Net.WebUtility.HtmlDecode(DataBinder.Eval(e.Item.DataItem, "Target").ToString());
        }
    }

    /// <summary>
    /// Edit static contents
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkEditStaticContents_Click(object sender, EventArgs e)
    {
        Language l = lm.GetLanguageByID(Convert.ToInt64(hdnId.Value));
        BindXML(l.Name);
        rbtnTranslate.Checked = true;
        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + divXML .ClientID+ "');});", true);
    }

    /// <summary>
    /// Add New Language
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkAddNew_Click(object sender, EventArgs e)
    {
        divAddLanguage.Visible = true;
        txtLanguage.Text = "";
        txtShortName.Text = "";
        txtLanguage.Enabled = true;
        txtShortName.Enabled = true;
        drpIsActive.SelectedValue = "0";
        lblHeader.Text = "Add Language";
        hdnId.Value = "0";
        divMainMessage.InnerHtml = "";
        divMainMessage.Attributes.Add("class", "");
        divMessage.InnerHtml = "";
        divMessage.Attributes.Add("class", "");
        lnkEDitSC.Visible = false;
        divXML.Visible = false;
        trIsActive.Visible = false;
        rbtnTranslate.Checked = true;
        ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + divAddLanguage.ClientID + "')});", true);
    }

    /// <summary>
    /// Cancel Details
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkCancel_Click(object sender, EventArgs e)
    {
        divAddLanguage.Visible = false;
        divXML.Visible = false;
    }

    /// <summary>
    /// Save Language
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkSave_Click(object sender, EventArgs e)
    {
        Language l = null;
        if (hdnId.Value == "0")
        {
            l = new Language();
            l.Name = txtLanguage.Text.Trim().Replace("'", "");
            l.ShortName = txtShortName.Text.Trim().Replace("'", "");
            if (drpIsActive.SelectedValue == "1")
            {
                l.IsActive = true;
            }
            else
            {
                l.IsActive = false;
            }
            if (lm.CheckLanguageExist(txtLanguage.Text.Trim().ToLower().Replace("'", ""),l.Id))
            {
                divMessage.InnerHtml = "Duplicate language already exist.";
                divMessage.Attributes.Add("class", "error");
            }
            else
            {
                l.Name = txtLanguage.Text.Trim().Replace("'", "");
                l.ShortName = txtShortName.Text.Trim().Replace("'", "");
                if (drpIsActive.SelectedValue == "1")
                {
                    l.IsActive = true;
                }
                else
                {
                    l.IsActive = false;
                }
                if (lm.AddLanguage(l))
                {
                    divMainMessage.InnerHtml = "Language added successfully.";
                    divMainMessage.Attributes.Add("class", "succesfuly");
                    divMessage.InnerHtml = "";
                    divMessage.Attributes.Add("class", "");
                    GetAllLanguage();
                    //BindXML(l.Name);
                    divAddLanguage.Visible = false;
                }
                else
                {
                    divMessage.InnerHtml = "Error occur while inserting.";
                    divMessage.Attributes.Add("class", "error");
                }
            }
        }
        else
        {
            l = lm.GetLanguageByID(Convert.ToInt64(hdnId.Value));
            string oldname = l.Name;
            l.Name = txtLanguage.Text.Trim().Replace("'", "");
            l.ShortName = txtShortName.Text.Trim().Replace("'", "");
            
            if (lm.CheckLanguageExist(txtLanguage.Text.Trim().ToLower().Replace("'", ""),l.Id))
            {
                divMessage.InnerHtml = "Duplicate language already exist.";
                divMessage.Attributes.Add("class", "error");
            }
            else
            {
                l.Name = txtLanguage.Text.Trim().Replace("'", "");
                l.ShortName = txtShortName.Text.Trim().Replace("'", "");
                if (drpIsActive.SelectedValue == "1")
                {
                    l.IsActive = true;
                }
                else
                {
                    l.IsActive = false;
                }
                if (lm.UpdateLanguage(l, oldname))
                {
                    divMainMessage.InnerHtml = "Language updated successfully.";
                    divMainMessage.Attributes.Add("class", "succesfuly");
                    divMessage.InnerHtml = "";
                    divMessage.Attributes.Add("class", "");
                    GetAllLanguage();
                    divAddLanguage.Visible = false;
                    divXML.Visible = false;
                }
                else
                {
                    divMessage.InnerHtml = "Error occur while updating.";
                    divMessage.Attributes.Add("class", "error");
                }
            }
        }
    }

    /// <summary>
    /// Language Repeater Item Databound
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rptLanguage_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Language lng = e.Item.DataItem as Language;
            Label lblLanguageName = (Label)e.Item.FindControl("lblLanguageName");
            LinkButton lnkDelete = (LinkButton)e.Item.FindControl("lnkDelete");
            LinkButton lnkModify = (LinkButton)e.Item.FindControl("lnkModify");
            Label lblShortName = (Label)e.Item.FindControl("lblShortName");
            if (lng != null)
            {
                lblLanguageName.Text = lng.Name;
                lblShortName.Text = lng.ShortName;  
                if (!lng.IsActive)
                {
                    lblLanguageName.ForeColor = System.Drawing.Color.Red;
                }
                if (lng.Name.ToLower()=="english")
                {
                    lnkDelete.Visible = false;
                }
                lnkDelete.CommandArgument = Convert.ToString(lng.Id);
                lnkModify.CommandArgument = Convert.ToString(lng.Id);
            }
        }
    }

    /// <summary>
    /// Language Repeter Item Command
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void rptLanguage_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "deleteme")
        {
            Language l = lm.GetLanguageByID(Convert.ToInt64(e.CommandArgument));
            if (lm.DeleteLanguage(Convert.ToInt64(e.CommandArgument),l.Name))
            {
                divMainMessage.InnerHtml = "Language deleted sucessfully.";
                divMainMessage.Attributes.Add("class", "succesfuly");
                GetAllLanguage();
                divXML.Visible = false;
                divAddLanguage.Visible = false;
            }
            else
            {
                divMainMessage.InnerHtml = "Error occured while deleting.";
                divMainMessage.Attributes.Add("class", "error");
            }
        }
        if (e.CommandName == "modifyme")
        {
            Language l = lm.GetLanguageByID(Convert.ToInt64(e.CommandArgument));
            if (l != null)
            {
                hdnId.Value = Convert.ToString(l.Id);
                txtLanguage.Text = l.Name;
                txtShortName.Text = l.ShortName;
                if (l.IsActive)
                {
                    drpIsActive.SelectedValue = "1";
                }
                else
                {
                    drpIsActive.SelectedValue = "0";
                }
                if (l.Name.ToLower() == "english")
                {
                    txtLanguage.Enabled = false;
                    txtShortName.Enabled = false;
                }
                else
                {
                    txtLanguage.Enabled = true;
                    txtShortName.Enabled = true;
                }
                lblHeader.Text = "Edit Language";
                divAddLanguage.Visible = true;
                divMainMessage.InnerHtml = "";
                divMainMessage.Attributes.Add("class", "");
                divMessage.InnerHtml = "";
                divMessage.Attributes.Add("class", "");
                lnkEDitSC.Visible = true;
                divXML.Visible = false;
                trIsActive.Visible = true;
                rbtnTranslate.Checked = true;
                ScriptManager.RegisterStartupScript(this, typeof(Page), "aa", "jQuery(document).ready(function(){movePage('" + divAddLanguage.ClientID + "');});", true);
            }
        }
    }

    /// <summary>
    /// Linkbutton Save xml
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void lnkSaveXML_Click(object source, EventArgs e)
    {
        Language l = lm.GetLanguageByID(Convert.ToInt64(hdnId.Value));
        SaveXML(l.Name);
        divMessage.InnerHtml = "Static containts save successfully.";
        divMessage.Attributes.Add("class", "succesfuly");
        divXML.Visible = false;
    }
    #endregion
    
}