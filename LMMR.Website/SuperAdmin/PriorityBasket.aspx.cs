﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LMMR.Business;
using LMMR.Entities;
using System.Text.RegularExpressions;
using log4net;
using System.IO;

public partial class SuperAdmin_PriorityBasket : System.Web.UI.Page
{
    #region Variable
    ILog logger = log4net.LogManager.GetLogger(typeof(SuperAdmin_PriorityBasket));
    BookingRequest objBook = new BookingRequest();
    HotelInfo ObjHotelinfo = new HotelInfo();
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl + "/";
        }
    }
    #endregion

    #region Pageload
    protected void Page_Load(object sender, EventArgs e)
    {
        SearchPanel1.SearchButtonClick += new EventHandler(Search_Button);
        SearchPanel1.CancelButtonClick += new EventHandler(Cancel_Button);
        SearchPanel1.rdbBookingClick += new EventHandler(Booking_SelectedIndex);
        if (Session["CurrentSuperAdminID"] == null)
        {
            //Response.Redirect("~/login.aspx");
            Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
            Session.Abandon();
            if (l != null)
            {
                Response.Redirect(SiteRootPath + "login/" + l.Name.ToLower());
            }
            else
            {
                Response.Redirect(SiteRootPath + "login/english");
            }
        }
        if (!IsPostBack)
        {
            ViewState["SearchAlpha"] = "all";
            BindAllHotel();
            BindPriorityBasket();
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
            divPriority.Style.Add("display", "none");
            divPriority.Attributes.Add("class", "succesfuly");
            CalendarExtender1.StartDate = DateTime.Now.Date;
            CalendarExtender2.StartDate = DateTime.Now.Date;
        }

    }
    #endregion


    #region Bind All Hotel
    /// <summary>
    /// Bind the Hotel details to the drop down drpHotelList
    /// </summary>
    public void BindAllHotel()
    {
        try
        {
            drpHotelName.DataSource = ObjHotelinfo.GetAllActiveHotel().OrderBy(a => a.Name);
            drpHotelName.DataTextField = "Name";
            drpHotelName.DataValueField = "Id";
            drpHotelName.DataBind();
            drpHotelName.Items.Insert(0, new ListItem("Select Hotel", ""));
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    #region Event handler 
    protected void Cancel_Button(object sender, EventArgs e)
    {
        ViewState["SearchAlpha"] = "all";
        BindPriorityBasket();

        divPriority.Attributes.Add("class", "succesfuly");
        divPriority.Style.Add("display", "none");
    }
    protected void Search_Button(object sender, EventArgs e)
    {
        ViewState["SearchAlpha"] = "all";
        BindPriorityBasket();

        divPriority.Attributes.Add("class", "succesfuly");
        divPriority.Style.Add("display", "none");
    }
    protected void Booking_SelectedIndex(object sender, EventArgs e)
    {
        string whereclaus = string.Empty;
        string orderby = "Id DESC";
        if (SearchPanel1.propBookRefType == 0)
        {            
            whereclaus += "BRType = 1";
        }
        else
        {
            whereclaus += "BRType = 0";
        }
        TList<PriorityBasket> objPriority = objBook.GetPriorityBasket(whereclaus, orderby);
        lblTotalCount.Text = Convert.ToString(objPriority.Count);
        grvPriorityBasket.DataSource = objPriority;
        grvPriorityBasket.DataBind();
        Session["WhereClause"] = objPriority;
        Session["WhereClauseTemp"] = objPriority;
        ViewState["SearchAlpha"] = "all";

        if (objPriority.Count > 0)
        {
            pnlActivate.Visible = true;
        }
        else
        {
            pnlActivate.Visible = false;
        }

        #region Check no of basket not more then 10
        //if (SearchPanel1.propBookRefType == 0)
        //{
        //    string whereCheck = "BRType = 1";
        //    TList<PriorityBasket> objPriorityBasket = objBook.GetPriorityBasket(whereCheck, string.Empty);
        //    if (objPriorityBasket.Count == 10)
        //    {
        //        lnbNewBasket.Visible = false;
        //    }
        //    else
        //    {
        //        lnbNewBasket.Visible = true;
        //    }
        //}
        //else
        //{
        //    string whereCheck = "BRType = 0";
        //    TList<PriorityBasket> objPriorityBasket = objBook.GetPriorityBasket(whereCheck, string.Empty);
        //    if (objPriorityBasket.Count == 10)
        //    {
        //        lnbNewBasket.Visible = false;
        //    }
        //    else
        //    {
        //        lnbNewBasket.Visible = true;
        //    }
        //}
        #endregion

        divPriority.Attributes.Add("class", "succesfuly");
        divPriority.Style.Add("display", "none");
    }
    #endregion

    #region PageAlpha
    public void PageChange(object sender, EventArgs e)
    {
        try
        {
            HtmlAnchor anc = (sender as HtmlAnchor);
            anc.Style.Add("class", "select");
            var alpha = anc.InnerHtml;
            string get = alpha.Trim();
            ViewState["SearchAlpha"] = get;
            TList<PriorityBasket> objPriority = new TList<PriorityBasket>();
            objPriority = (TList<PriorityBasket>)Session["WhereClause"];
            ViewState["CurrentPage"] = null;
            if (get != "all")
            {
                Session["WhereClauseTemp"] = objPriority.FindAll(a => a.HotelIdSource.Name.ToLower().StartsWith(get.ToLower()));
                grvPriorityBasket.DataSource = Session["WhereClauseTemp"];
                grvPriorityBasket.DataBind();
                lblTotalCount.Text = Convert.ToString(grvPriorityBasket.Rows.Count);
            }
            else
            {
                BindPriorityBasket();            
            }
            divPriority.Style.Add("display", "none");
            divPriority.Attributes.Add("class", "succesfuly"); 

        }
        catch (Exception ex)
        {
            //logger.Error(ex);
        }
    }
    #endregion

    #region BindGrid for Booking
    /// <summary>
    /// Method to bind the grid
    /// </summary>    
    protected void BindPriorityBasket()
    {
        try
        {
            string whereclaus = string.Empty;
            string orderby = "Id DESC";            
            if (SearchPanel1.propCountryID != 0)
            {
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += PriorityBasketColumn.CountryId + "=" + SearchPanel1.propCountryID + " ";
            }
            if (SearchPanel1.propCityID != 0)
            {
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += PriorityBasketColumn.CityId + "=" + SearchPanel1.propCityID + " ";
            }

            if (SearchPanel1.propDateFrom != "No" && SearchPanel1.propDateTo != "No")
            {
                if (whereclaus.Length > 0)
                {
                    whereclaus += " and ";
                }
               // whereclaus += PriorityBasketColumn.FromDate + ">=  '" + SearchPanel1.propFromDate + "' " + " and " + PriorityBasketColumn.ToDate + "<= '" + SearchPanel1.propToDate + "' ";               
                whereclaus = "(('" + SearchPanel1.propFromDate + "' between FromDate and Todate)" + " or " + "('" + SearchPanel1.propToDate + "' between FromDate and Todate))";
            }

            if (SearchPanel1.propBookRefType == 0)
            {               
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += "BRType = 1";
            }
            else
            {
                if (!string.IsNullOrEmpty(whereclaus.Trim()))
                {
                    whereclaus += " and ";
                }
                whereclaus += "BRType = 0";
            }
            TList<PriorityBasket> objPriority = objBook.GetPriorityBasket(whereclaus, orderby);
            lblTotalCount.Text = Convert.ToString(objPriority.Count);
            grvPriorityBasket.DataSource = objPriority;
            grvPriorityBasket.DataBind();
            if (objPriority.Count > 0)
            {
                pnlActivate.Visible = true;
            }
            else
            {
                pnlActivate.Visible = false;
            }
            Session["WhereClause"] = objPriority;
            Session["WhereClauseTemp"] = objPriority;
            drpHotelName.Enabled=true;            

            #region Check no of basket not more then 10
            //if (SearchPanel1.propBookRefType == 0)
            //{
            //    string whereCheck = "BRType = 1";
            //    TList<PriorityBasket> objPriorityBasket = objBook.GetPriorityBasket(whereCheck, string.Empty);
            //    if (objPriorityBasket.Count == 10)
            //    {
            //        lnbNewBasket.Visible = false;
            //    }
            //    else
            //    {
            //        lnbNewBasket.Visible = true;
            //    }
            //}
            //else
            //{
            //    string whereCheck = "BRType = 0";
            //    TList<PriorityBasket> objPriorityBasket = objBook.GetPriorityBasket(whereCheck, string.Empty);
            //    if (objPriorityBasket.Count == 10)
            //    {
            //        lnbNewBasket.Visible = false;
            //    }
            //    else
            //    {
            //        lnbNewBasket.Visible = true;
            //    }
            //}
            #endregion
                       
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    #region Add new hotel for booking/request in priority basket
    protected void lnbNewBasket_Click(object sender, EventArgs e)
    {
        try
        {           
            ModalPopupCheck.Show();
            divPriority.Style.Add("display", "none");
            divPriority.Attributes.Add("class", "succesfuly");
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
            //drpHotelName.Enabled = true;
            //CalendarExtender1.StartDate = DateTime.Now.Date;
            //CalendarExtender2.StartDate = DateTime.Now.Date;
            
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    protected void lbtSearch_Click(object sender, EventArgs e)
    {
        try
        {
            DateTime fromDate = new DateTime(Convert.ToInt32(txtFromdate.Text.Split('/')[2]), Convert.ToInt32(txtFromdate.Text.Split('/')[1]), Convert.ToInt32(txtFromdate.Text.Split('/')[0]));
            DateTime toDate = new DateTime(Convert.ToInt32(txtTodate.Text.Split('/')[2]), Convert.ToInt32(txtTodate.Text.Split('/')[1]), Convert.ToInt32(txtTodate.Text.Split('/')[0]));
            if (toDate < fromDate)
            {
                divmessage.InnerHtml = "Fromdate should be greater than todate";
                divmessage.Attributes.Add("class", "error");
                divmessage.Style.Add("display", "block");
                ModalPopupCheck.Show();
                return;
            }
            PriorityBasket objPriority = new PriorityBasket();
            Hotel htl = ObjHotelinfo.GetHotelByHotelID(Convert.ToInt32(drpHotelName.SelectedValue));
            bool result = false;
            if (SearchPanel1.propBookRefType == 0)
            {
                VList<ViewForAvailabilityCheck> vcheck = new VList<ViewForAvailabilityCheck>();
                string where= ViewForAvailabilityCheckColumn.AvailabilityDate + " between '" + fromDate + "' and '" + toDate + "'" + " and " + ViewForAvailabilityCheckColumn.HotelId + "='" + drpHotelName.SelectedValue + "'";
                vcheck = objBook.AvailabilityCheck(where).FindAllDistinct(ViewForAvailabilityCheckColumn.AvailabilityDate);
                TimeSpan span = toDate.AddDays(1).Subtract(fromDate);
                int difference = span.Days;
                if (vcheck.Count == difference)
                {
                    if (ViewState["PriorityId"] != null)
                    {
                        objPriority = objBook.GetBasketByID(Convert.ToInt32(ViewState["PriorityId"]));
                        objPriority.HotelId = Convert.ToInt32(drpHotelName.SelectedValue);
                        objPriority.CountryId = htl.CountryId;
                        objPriority.CityId = htl.CityId;
                        objPriority.FromDate = fromDate;
                        objPriority.ToDate = toDate;                       
                        objPriority.BrType = true;
                        result = objBook.UpdatePriorityBaket(objPriority);                       
                    }
                    else
                    {
                        string whereclaus = PriorityBasketColumn.HotelId + "=" + drpHotelName.SelectedValue + " and " + "BRType = 1";                        
                        TList<PriorityBasket> objPriorityBasket = objBook.GetPriorityBasket(whereclaus, string.Empty);
                        if (objPriorityBasket.Count == 0)
                        {
                            objPriority.HotelId = Convert.ToInt32(drpHotelName.SelectedValue);
                            objPriority.CountryId = htl.CountryId;
                            objPriority.CityId = htl.CityId;
                            objPriority.FromDate = fromDate;
                            objPriority.ToDate = toDate;
                            objPriority.IsActive = true;
                            objPriority.BrType = true;
                            objBook.InsertPriorityBaket(objPriority);
                        }
                        else
                        {
                            divmessage.InnerHtml = "This hotel has been already exist in priority basket";
                            divmessage.Attributes.Add("class", "error");
                            divmessage.Style.Add("display", "block");
                            ModalPopupCheck.Show();
                            return; 
                        }
                    }
                }
                else
                {
                    divmessage.InnerHtml = "Availability is not available for this durations";
                    divmessage.Attributes.Add("class", "error");
                    divmessage.Style.Add("display", "block");
                    ModalPopupCheck.Show();
                    return;              
                }
            }
            else
            {
                if (ViewState["PriorityId"] != null)
                {
                    objPriority = objBook.GetBasketByID(Convert.ToInt32(ViewState["PriorityId"]));
                    objPriority.HotelId = Convert.ToInt32(drpHotelName.SelectedValue);
                    objPriority.CountryId = htl.CountryId;
                    objPriority.CityId = htl.CityId;
                    objPriority.FromDate = fromDate;
                    objPriority.ToDate = toDate;                    
                    objPriority.BrType = false;
                    result = objBook.UpdatePriorityBaket(objPriority);
                }
                else
                {
                    string whereclaus = PriorityBasketColumn.HotelId + "=" + drpHotelName.SelectedValue + " and " + "BRType = 0";                        
                    TList<PriorityBasket> objPriorityBasket = objBook.GetPriorityBasket(whereclaus, string.Empty);
                    if (objPriorityBasket.Count == 0)
                    {
                        objPriority.HotelId = Convert.ToInt32(drpHotelName.SelectedValue);
                        objPriority.CountryId = htl.CountryId;
                        objPriority.CityId = htl.CityId;
                        objPriority.FromDate = fromDate;
                        objPriority.ToDate = toDate;
                        objPriority.IsActive = true;
                        objPriority.BrType = false;
                        objBook.InsertPriorityBaket(objPriority);
                    }
                    else
                    {
                        divmessage.InnerHtml = "This hotel has been already exist in priority basket";
                        divmessage.Attributes.Add("class", "error");
                        divmessage.Style.Add("display", "block");
                        ModalPopupCheck.Show();
                        return; 
                    }
                }                
            }
            ModalPopupCheck.Hide();
            BindPriorityBasket();
            ViewState["SearchAlpha"] = "all";
            ViewState["PriorityId"] = null;
            divPriority.Attributes.Add("class", "succesfuly");
            divPriority.Style.Add("display", "none");
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    #region Hide popup control for cancel button
    protected void lnkClear_Click(object sender, EventArgs e)
    {
        try
        {
            ModalPopupCheck.Hide();
            BindPriorityBasket();
            ViewState["PriorityId"] = null;
            drpHotelName.SelectedIndex = 0;
            txtFromdate.Text = "dd/MM/yyyy";
            txtTodate.Text = "dd/MM/yyyy";
            divPriority.Style.Add("display", "none");
            divPriority.Attributes.Add("class", "succesfuly");
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    #region Bind status and other things in Rowdatabound
    protected void grvPriorityBasket_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
            {
                PriorityBasket data = e.Row.DataItem as PriorityBasket;
                Label lblFromDate = (Label)e.Row.FindControl("lblFromDate");
                Label lblToDate = (Label)e.Row.FindControl("lblToDate");
                //CheckBox uiCheckBoxTodayActive = (CheckBox)e.Row.FindControl("uiCheckBoxTodayActive");
                CheckBox uiCheckBoxIsActive = (CheckBox)e.Row.FindControl("uiCheckBoxIsActive");
                
                DateTime fromdate = new DateTime(Convert.ToInt32(lblFromDate.Text.Split('/')[2]), Convert.ToInt32(lblFromDate.Text.Split('/')[1]), Convert.ToInt32(lblFromDate.Text.Split('/')[0]));
                DateTime todate = new DateTime(Convert.ToInt32(lblToDate.Text.Split('/')[2]), Convert.ToInt32(lblToDate.Text.Split('/')[1]), Convert.ToInt32(lblToDate.Text.Split('/')[0]));
                string Day = DateTime.Now.ToString("dd/MM/yy");
                //DateTime Today = new DateTime(Convert.ToInt32(Day.Split('/')[2]), Convert.ToInt32(Day.Split('/')[1]), Convert.ToInt32(Day.Split('/')[0]));
                //if (Today >= fromdate && Today <= todate)
                //{
                //    uiCheckBoxTodayActive.Checked = true;
                //}
                uiCheckBoxIsActive.Attributes.Add("AccessKey", data.Id.ToString());
                if (data.IsActive == true)
                {
                    uiCheckBoxIsActive.Checked = true;
                }
                else
                {
                    uiCheckBoxIsActive.Checked = false;
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    #region Change status for priority basket
    protected void lnbDelete_Click(object sender, EventArgs e)
    {
        try
        {
            int basketId = 0;
            foreach (GridViewRow grv in grvPriorityBasket.Rows)
            {
                CheckBox rbselect = (CheckBox)grv.FindControl("rbselect");                               
                if (rbselect.Checked)
                {
                    DataKey currentDataKey = this.grvPriorityBasket.DataKeys[grv.DataItemIndex];
                    basketId = Convert.ToInt32(currentDataKey.Value);
                }        
            }
            PriorityBasket obj = objBook.GetBasketByID(basketId);
            objBook.DeletePriorityBaket(obj);
            BindPriorityBasket();
            divPriority.InnerHtml = "Hotel name has been successfully deleted";
            divPriority.Attributes.Add("class", "succesfuly");
            divPriority.Style.Add("display", "block");
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    #region Modify record of priority basket
    protected void lnbModify_Click(object sender, EventArgs e)
    {
        divmessage.Attributes.Add("class", "error");
        divmessage.Style.Add("display", "none");
        int basketId = 0;
        foreach (GridViewRow grv in grvPriorityBasket.Rows)
        {
            CheckBox rbselect = (CheckBox)grv.FindControl("rbselect");
            if (rbselect.Checked)
            {
                DataKey currentDataKey = this.grvPriorityBasket.DataKeys[grv.DataItemIndex];
                basketId = Convert.ToInt32(currentDataKey.Value);
                ModalPopupCheck.Show();
                PriorityBasket obj = objBook.GetBasketByID(basketId);
                string Hotel = Convert.ToString(obj.HotelId);
                drpHotelName.SelectedValue = Hotel;
                drpHotelName.Enabled = false;                
                txtFromdate.Text = String.Format("{0:dd/MM/yyyy}", obj.FromDate);
                txtTodate.Text = String.Format("{0:dd/MM/yyyy}", obj.ToDate);
                ViewState["PriorityId"] = basketId;
            }
        }
    }
    #endregion

    #region Deactivate status for priority basket
    protected void lnbDeactivate_Click(object sender, EventArgs e)
    {
        try
        {
            int basketId = 0;
            foreach (GridViewRow grv in grvPriorityBasket.Rows)
            {
                CheckBox rbselect = (CheckBox)grv.FindControl("rbselect");
                CheckBox uiCheckBoxIsActive = (CheckBox)grv.FindControl("uiCheckBoxIsActive");
                DataKey currentDataKey = this.grvPriorityBasket.DataKeys[grv.DataItemIndex];
                basketId = Convert.ToInt32(currentDataKey.Value);
                if (rbselect.Checked)
                {
                    objBook.ChangePriorityActiveStatus(basketId, false);
                    divPriority.InnerHtml = "Hotel name has been deactivated successfully.";
                    divPriority.Attributes.Add("class", "succesfuly");
                    divPriority.Style.Add("display", "block");
                } 

                ////This code is for activate/deactive status
                //if (rbselect.Checked && uiCheckBoxIsActive.Checked==false)
                //{
                //    objBook.ChangePriorityActiveStatus(basketId, true);
                //    divPriority.InnerHtml = "Hotel name has been activated successfully.";
                //    divPriority.Attributes.Add("class", "succesfuly");
                //    divPriority.Style.Add("display", "block");
                //}
                //if (rbselect.Checked && uiCheckBoxIsActive.Checked == true)
                //{
                //    objBook.ChangePriorityActiveStatus(basketId, false);
                //    divPriority.InnerHtml = "Hotel name has been deactivated successfully.";
                //    divPriority.Attributes.Add("class", "succesfuly");
                //    divPriority.Style.Add("display", "block");
                //}               
            }
            BindPriorityBasket();         
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    #region Sending mail function for login id and password
    protected void uiCheckBoxIsActive_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox ch = sender as CheckBox; 
            var data = ch.Attributes["AccessKey"];
            if (ch.Checked == true)
            {
                objBook.ChangePriorityActiveStatus(Convert.ToInt32(data), true);
                divPriority.InnerHtml = "Hotel name has been activated successfully.";
                divPriority.Attributes.Add("class", "succesfuly");
                divPriority.Style.Add("display", "block");
            }

            if (ch.Checked == false)
            {
                objBook.ChangePriorityActiveStatus(Convert.ToInt32(data), false);
                divPriority.InnerHtml = "Hotel name has been deactivated successfully.";
                divPriority.Attributes.Add("class", "succesfuly");
                divPriority.Style.Add("display", "block");
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }

    }
    #endregion
}