﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuperAdmin/Main.master" AutoEventWireup="true" CodeFile="PriorityBasket.aspx.cs" Inherits="SuperAdmin_PriorityBasket" %>
<%@ Register Src="~/UserControl/Operator/SearchPanel.ascx" TagName="SearchPanel" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc1:SearchPanel ID="SearchPanel1" runat="server" />
<div class="contract-list" id="divAlphabeticPaging" runat="server" style="margin-top: 20px;">
                <div class="contract-list-left" style="width: 278px;">
                    <h2>
                        Priority Basket</h2>
                </div>
                <div class="contract-list-right" style="width: 676px;">
                    <ul runat="server" id="AlphaList">
                    <li id="Li1" runat="server"><a href="#" class="select" runat="server" id="all" onserverclick="PageChange">
                        all</a></li>
                    <li id="Li2" runat="server"><a href="#" runat="server" id="a" onserverclick="PageChange">
                        a</a></li>
                    <li id="Li3" runat="server"><a href="#" runat="server" id="b" onserverclick="PageChange">
                        b</a></li>
                    <li id="Li4" runat="server"><a href="#" runat="server" id="c" onserverclick="PageChange">
                        c</a></li>
                    <li id="Li5" runat="server"><a href="#" runat="server" id="d" onserverclick="PageChange">
                        d</a></li>
                    <li id="Li6" runat="server"><a href="#" runat="server" id="e" onserverclick="PageChange">
                        e</a></li>
                    <li id="Li7" runat="server"><a href="#" runat="server" id="f" onserverclick="PageChange">
                        f</a></li>
                    <li id="Li8" runat="server"><a href="#" runat="server" id="g" onserverclick="PageChange">
                        g</a></li>
                    <li id="Li9" runat="server"><a href="#" runat="server" id="h" onserverclick="PageChange">
                        h</a></li>
                    <li id="Li10" runat="server"><a href="#" runat="server" id="i" onserverclick="PageChange">
                        i</a></li>
                    <li id="Li11" runat="server"><a href="#" runat="server" id="j" onserverclick="PageChange">
                        j</a></li>
                    <li id="Li12" runat="server"><a href="#" runat="server" id="k" onserverclick="PageChange">
                        k</a></li>
                    <li id="Li13" runat="server"><a href="#" runat="server" id="l" onserverclick="PageChange">
                        l</a></li>
                    <li id="Li14" runat="server"><a href="#" runat="server" id="m" onserverclick="PageChange">
                        m</a></li>
                    <li id="Li15" runat="server"><a href="#" runat="server" id="n" onserverclick="PageChange">
                        n</a></li>
                    <li id="Li16" runat="server"><a href="#" runat="server" id="o" onserverclick="PageChange">
                        o</a></li>
                    <li id="Li17" runat="server"><a href="#" runat="server" id="p" onserverclick="PageChange">
                        p</a></li>
                    <li id="Li18" runat="server"><a href="#" runat="server" id="q" onserverclick="PageChange">
                        q</a></li>
                    <li id="Li19" runat="server"><a href="#" runat="server" id="r" onserverclick="PageChange">
                        r</a></li>
                    <li id="Li20" runat="server"><a href="#" runat="server" id="s" onserverclick="PageChange">
                        s</a></li>
                    <li id="Li21" runat="server"><a href="#" runat="server" id="t" onserverclick="PageChange">
                        t</a></li>
                    <li id="Li22" runat="server"><a href="#" runat="server" id="u" onserverclick="PageChange">
                        u</a></li>
                    <li id="Li23" runat="server"><a href="#" runat="server" id="v" onserverclick="PageChange">
                        v</a></li>
                    <li id="Li24" runat="server"><a href="#" runat="server" id="w" onserverclick="PageChange">
                        w</a></li>
                    <li id="Li25" runat="server"><a href="#" runat="server" id="x" onserverclick="PageChange">
                        x</a></li>
                    <li id="Li26" runat="server"><a href="#" runat="server" id="y" onserverclick="PageChange">
                        y</a></li>
                    <li id="Li27" runat="server"><a href="#" runat="server" id="z" onserverclick="PageChange">
                        z</a></li>
                </ul>
                </div>
            </div>    
        <div id="divPriority" runat="server" class="error">
            </div>     
<div class="pageing-operator-new">
  
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>   
                    <td align="left" valign="bottom">
                <div style="float: left; margin-left: 20px;" class="n-commisions">
                    <div class="n-btn" id="divNewHotel" runat="server">
                        <asp:LinkButton ID="lnbNewBasket" runat="server" OnClick="lnbNewBasket_Click"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Add new hotel</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                    </div>
                </div>
                <asp:Panel ID="pnlActivate" runat="server">       
                <div style="float: left;display:none;margin-left: 10px;" class="n-commisions">
                    <div class="n-btn">
                        <asp:LinkButton ID="lnbDeactivate" runat="server" OnClick="lnbDeactivate_Click"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Deactive</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                    </div>
                </div>
                <div style="float: left; margin-left: 10px;" class="n-commisions">
                    <div class="n-btn">
                        <asp:LinkButton ID="lnbModify" runat="server" OnClick="lnbModify_Click"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Modify</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                    </div>
                </div>
                <div style="float: left; display:none;margin-left: 10px;" class="n-commisions">
                    <div class="n-btn">
                        <asp:LinkButton ID="lnbDelete" runat="server" OnClick="lnbDelete_Click"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Delete</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                    </div>
                </div>
                </asp:Panel>
            </td>
             <td align="right">
                        Total Result :
                        <asp:Label ID="lblTotalCount" runat="server" Text=""></asp:Label>
                    </td>                                                                  
                </tr>
            </table>
        </div>
<div class="search-booking-rowmain clearfix" style="width: 960px">
             <asp:Panel ID="pnlPriority" runat="server" Height="460px" ScrollBars="Horizontal">
            <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#98BCD6">
                <tr bgcolor="#CCD8D8"> 
                    <td valign="top" width="10%">
                    Edit
                    </td>  
                    <td valign="top" width="20%">
                        Country
                    </td>                
                    <td valign="top" width="15%">
                        City
                    </td>
                    <td valign="top" width="20%">
                        Hotel Name
                    </td>
                    <%--<td valign="top" width="15%">
                        Active Today
                    </td>--%>
                     <td valign="top" width="15%">
                        Active
                    </td>  
                    <td valign="top" width="20%">
                        Priority Basket Period
                    </td>                                      
                </tr>                  
                <asp:GridView ID="grvPriorityBasket" runat="server" Width="100%" border="0" CellPadding="5"
                    CellSpacing="1" AutoGenerateColumns="false" DataKeyNames="Id" GridLines="None"
                    ShowHeader="false" ShowFooter="false" BackColor="#98BCD6" 
                    RowStyle-CssClass="con" AlternatingRowStyle-CssClass="con-dark"             
                    EmptyDataRowStyle-BackColor="White" EmptyDataRowStyle-HorizontalAlign="Center"
                    HeaderStyle-BackColor="#98BCD6" 
                    onrowdatabound="grvPriorityBasket_RowDataBound">
                    <Columns>
                        <asp:TemplateField ItemStyle-Width="10%">
                            <ItemTemplate>                                
                                <asp:CheckBox ID="rbselect" runat="server" onclick="javascript:CheckOtherIsCheckedByGVID(this);" />
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:CheckBox ID="rbselect" runat="server" onclick="javascript:CheckOtherIsCheckedByGVID(this);" />
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="20%">
                            <ItemTemplate>                                
                                <asp:Label ID="lblCountry" runat="server" Text='<%# Eval("CountryIdSource.CountryName") %>'></asp:Label>
                            </ItemTemplate>
                             <AlternatingItemTemplate>
                                <asp:Label ID="lblCountry" runat="server" Text='<%# Eval("CountryIdSource.CountryName") %>'></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="15%">
                            <ItemTemplate>                                
                                <asp:Label ID="lblCity" runat="server" Text='<%# Eval("CityIdSource.City") %>'></asp:Label>
                            </ItemTemplate>
                             <AlternatingItemTemplate>
                                <asp:Label ID="lblCity" runat="server" Text='<%# Eval("CityIdSource.City") %>'></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="20%">
                            <ItemTemplate>
                               <asp:Label ID="lblHotel" runat="server" Text='<%# Eval("HotelIdSource.Name") %>'></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblHotel" runat="server" Text='<%# Eval("HotelIdSource.Name") %>'></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                       <%-- <asp:TemplateField ItemStyle-Width="15%">
                            <ItemTemplate>
                                <asp:CheckBox ID="uiCheckBoxTodayActive" runat="server" Enabled="false" />
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:CheckBox ID="uiCheckBoxTodayActive" runat="server" Enabled="false" />
                            </AlternatingItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField ItemStyle-Width="15%">
                            <ItemTemplate>
                                <asp:CheckBox ID="uiCheckBoxIsActive" runat="server" AutoPostBack="true" OnCheckedChanged="uiCheckBoxIsActive_CheckedChanged"/>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:CheckBox ID="uiCheckBoxIsActive" runat="server" AutoPostBack="true" OnCheckedChanged="uiCheckBoxIsActive_CheckedChanged"/>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>            
                        <asp:TemplateField ItemStyle-Width="20%">
                            <ItemTemplate>
                                <asp:Label ID="lblFromDate" runat="server" Text='<%# Eval("FromDate","{0:dd/MM/yy}")%>'></asp:Label> - <asp:Label ID="lblToDate" runat="server" Text='<%# Eval("ToDate","{0:dd/MM/yy}")%>'></asp:Label>                                
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblFromDate" runat="server" Text='<%# Eval("FromDate","{0:dd/MM/yy}")%>'></asp:Label> - <asp:Label ID="lblToDate" runat="server" Text='<%# Eval("ToDate","{0:dd/MM/yy}")%>'></asp:Label>                                
                            </AlternatingItemTemplate>
                        </asp:TemplateField>                                
                    </Columns>
                    <EmptyDataTemplate>
                        <table>
                            <tr>
                                <td colspan="5" align="center">
                                    <b>No record found !</b>
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>                  
                </asp:GridView>                                        
            </table>
             </asp:Panel>    
        </div>
     <asp:ModalPopupExtender ID="ModalPopupCheck" TargetControlID="lnbNewBasket" BackgroundCssClass="modalBackground"
                PopupControlID="pnlcheck" runat="server">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlcheck" BorderColor="Black" Width="700px" BorderWidth="1" Style="display: none;"
                runat="server" BackColor="White">
                <div class="popup-mid-inner-body" align="center">
                </div>
                <div class="popup-mid-inner-body1">
                    <table width="100%" border="0" cellspacing="5" cellpadding="2">
        <tr>
            <td>    
                <div id="divmessage" runat="server">
            </div>      
               <div class="commisions-top-new1 ">
                            <table width="100%" border="0" cellspacing="0" cellpadding="5">   
                                <tr>                                  
                                    <td style="border-bottom: #92bede solid 1px" align="left" colspan="5">
                                    <b>
                                        <asp:Label ID="Label1" runat="server" Text="Hotel Name : "></asp:Label></b>&nbsp;&nbsp;&nbsp;
                                        <asp:DropDownList ID="drpHotelName" runat="server" AutoPostBack="false" CssClass="NoClassApply"
                                            Style="width: 215px">                                            
                                        </asp:DropDownList>
                                    </td>                                                                
                                </tr>                         
                                <tr>                                   
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <b>
                                                        <asp:Label ID="lbldate" runat="server" Text="Date:"></asp:Label></b>
                                                </td>
                                                <td style="padding-left: 20px; text-align: right; padding-right: 3px;">
                                                    <b>From</b>
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtFromdate" runat="server" value="dd/mm/yyyy" CssClass="inputbox1"></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFromdate"
                                                        PopupButtonID="calFrom" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:Image ID="calFrom" runat="server" ImageUrl="~/Images/date-icon.png" align="absmiddle" />
                                                </td>
                                                <td>
                                                    <b>To</b>
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtTodate" runat="server" value="dd/mm/yyyy" CssClass="inputbox1"></asp:TextBox>                                               
                                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTodate"
                                                        PopupButtonID="calTo" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:Image ID="calTo" runat="server" ImageUrl="~/Images/date-icon.png" align="absmiddle" />
                                                </td>
                                            </tr>                                           
                                        </table>
                                    </td>
                                </tr>                                                                                             
                            </table>                            
                        </div>                                  
            </td>                       
        </tr>
        <tr>
        <td align="left" valign="bottom">
                 <div class="button_section">
                        <asp:LinkButton ID="lbtSearch" runat="server" CssClass="select" OnClick="lbtSearch_Click">Save</asp:LinkButton>
                        &nbsp;or&nbsp;
                        <asp:LinkButton ID="lnkClear" runat="server" CssClass="cancelpop" OnClick="lnkClear_Click">Cancel</asp:LinkButton>
                    </div>
            </td>
        </tr>
    </table>
                </div>    
                      
            </asp:Panel>
<script language="javascript" type="text/javascript">
    function CheckOtherIsCheckedByGVID(spanChk) {
        if (jQuery("#" + spanChk.id).is(":checked")) {
            jQuery("#ContentPlaceHolder1_grvPriorityBasket tr").find("td:first input").each(function () { jQuery(this).attr("checked", false); });
            jQuery("#ContentPlaceHolder1_grvPriorityBasket tr").removeAttr("style");
            jQuery("#" + spanChk.id).attr("checked", true);
            //jQuery("#" + spanChk.id).parent().parent().attr("style", "background-color:yellow;");
        }
    }


    jQuery(document).ready(function () {
        jQuery("#<%= lbtSearch.ClientID %>").bind("click", function () {
            var isvalid = true;
            var errormessage = "";
            if (jQuery("#<%= divmessage.ClientID %>").hasClass("succesfuly")) {
                jQuery("#<%= divmessage.ClientID %>").removeClass("succesfuly").addClass("error").hide();
            }
            var drpHotelName = jQuery("#<%= drpHotelName.ClientID %>").val();
            if (drpHotelName == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Please select hotel name.";
                isvalid = false;
            }
            var txtFromdate = jQuery("#<%= txtFromdate.ClientID %>").val();
            if (txtFromdate == "dd/MM/yyyy") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Fromdate is required.";
                isvalid = false;
            }
            var txtTodate = jQuery("#<%= txtTodate.ClientID %>").val();
            if (txtTodate == "dd/MM/yyyy") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Todate is required.";
                isvalid = false;
            }
            if (!isvalid) {
                jQuery("#<%= divmessage.ClientID %>").show();
                jQuery("#<%= divmessage.ClientID %>").html(errormessage);
                var offseterror = jQuery("#<%= divmessage.ClientID %>").offset();
                jQuery("body").scrollTop(offseterror.top);
                jQuery("html").scrollTop(offseterror.top);
                return false;
            }
            jQuery("#<%= divmessage.ClientID %>").html("");
            jQuery("#<%= divmessage.ClientID %>").hide();
            jQuery("#<%= divmessage.ClientID %>").removeClass('error');
            jQuery("body").scrollTop(0);
            jQuery("html").scrollTop(0);
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';
        });
        jQuery("#<%=txtFromdate.ClientID %>").prop("readonly", "readonly");
        jQuery("#<%=txtTodate.ClientID %>").prop("readonly", "readonly");
        jQuery("#<%= lnbNewBasket.ClientID %>").bind("click", function () {        
            jQuery("#<%= divmessage.ClientID %>").html("");
            jQuery("#<%= divmessage.ClientID %>").hide();           
            jQuery("#<%= drpHotelName.ClientID %>").val(0);
            jQuery("#<%= txtFromdate.ClientID %>").val("dd/MM/yyyy");
            jQuery("#<%= txtTodate.ClientID %>").val("dd/MM/yyyy");
        });
    });
    </script>
    <script language="javascript" type="text/javascript">       
        jQuery(document).ready(function () {
            jQuery("#<%= lnbDeactivate.ClientID %>").bind("click", function () {                
                if (jQuery("#ContentPlaceHolder1_grvPriorityBasket tr").find("td:first").find("input:checkbox:checked").length > 0) {
                    return true;
                }   
                else
                {
                    alert('Please select hotel to deactivate');
                    return false;
                }                                                                          
            });
            jQuery("#<%= lnbModify.ClientID %>").bind("click", function () {
                if (jQuery("#ContentPlaceHolder1_grvPriorityBasket tr").find("td:first").find("input:checkbox:checked").length > 0) {
                   return true;
                }
                else{
                 alert('Please select hotel to modify');
                    return false;
                }
            });

            jQuery("#<%= lnbDelete.ClientID %>").bind("click", function () {
                if (jQuery("#ContentPlaceHolder1_grvPriorityBasket tr").find("td:first").find("input:checkbox:checked").length > 0) {
                  return confirm("Are you sure you want to delete it?");
                }
                else {                    
                      alert('Please select hotel to delete.');
                    return false;
                }
            });
        });
               jQuery(document).ready(function () {
                            <% if (ViewState["SearchAlpha"] != null) {%>
                                jQuery('#<%= AlphaList.ClientID %> li a').removeClass('select');
                                jQuery('#ContentPlaceHolder1_<%= ViewState["SearchAlpha"]%>').addClass('select');
                            <% }%>
                        });             
    </script>
</asp:Content>

