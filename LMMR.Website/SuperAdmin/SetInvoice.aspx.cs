﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using System.Configuration;
using log4net;
using log4net.Config;
#endregion

public partial class SuperAdmin_SetInvoice : System.Web.UI.Page
{
    #region Variable
    ILog logger = log4net.LogManager.GetLogger(typeof(SuperAdmin_SetInvoice));
    ClientContract objClient = new ClientContract();    
    ManageOthers objOthers = new ManageOthers();
    HotelInfo ObjHotelinfo = new HotelInfo();
    #endregion

    #region PageLoad
    /// <summary>
    /// On page load we manage all the containts and Apply paging if there is a post back.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Check session exist or not
            if (Session["CurrentOperatorID"] == null)
            {
                Response.Redirect("~/login.aspx", true);
            }

            //Check condition for Page postback.
            if (!IsPostBack)
            {                
                divmessage.Style.Add("display", "none");
                divmessage.Attributes.Add("class", "error");
                divGrid.Style.Add("display", "none");
                divGrid.Attributes.Add("class", "error");
                FillCountryForSearch();
                BindInvoiceCommission();
            }
            ApplyPaging();
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    #region Bind All Hotel
    /// <summary>
    /// Bind the Hotel details to the drop down drpHotelList
    /// </summary>
    public void BindAllHotel()
    {
        try
        {
            if (objOthers.GetAllActiveHotel(Convert.ToInt32(drpCountry.SelectedValue)).Count > 0)
            {
                chkHotelName.DataSource = objOthers.GetAllActiveHotel(Convert.ToInt32(drpCountry.SelectedValue)).OrderBy(a => a.Name);
                chkHotelName.DataTextField = "Name";
                chkHotelName.DataValueField = "Id";
                chkHotelName.DataBind();
                trMessage.Visible = false;
                trHotel.Visible = true;
            }
            else
            {
                trMessage.Visible = true;
                trHotel.Visible = false;
                lblMessage.Text = "Hotel doesn't exist for this country";
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    /// <summary>
    /// Bind all the country Table in dropdown.
    /// </summary>
    protected void FillCountryForSearch()
    {
        try
        {
            //Bind country Dropdown for search 
            TList<Country> objCountry = objClient.GetByAllCountry();           
            drpCountry.DataValueField = "Id";
            drpCountry.DataTextField = "CountryName";
            drpCountry.DataSource = objCountry.OrderBy(a => a.CountryName);
            drpCountry.DataBind();
            drpCountry.Items.Insert(0, new ListItem("Select Country", ""));
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

     /// <summary>
    /// Bind all invoice commission from master table
    /// </summary>
    protected void BindInvoiceCommission()
    {
        try
        {
            TList<InvoiceCommission> objComm = objOthers.GetInvoiceCommission();
            grvCommission.DataSource = objComm;
            grvCommission.DataBind();

            VList<ViewForSetInvoice> objView = objOthers.GetCommissionDetails();
            grvInvoiceCommPercentage.DataSource = objView;
            grvInvoiceCommPercentage.DataBind();            
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    #region Events
    /// <summary>
    /// Add panel country dropdown bind city according to country
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void drpCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (drpCountry.SelectedIndex > 0)
            {
                BindAllHotel();
            }
            else
            {
                
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        for (int j = 0; j < chkHotelName.Items.Count; j++)
        {
            if (chkHotelName.Items[j].Selected)
            {
                TList<InvoiceCommPercentage> objComm = objOthers.CheckInvoiceCommissionByHotelId(Convert.ToInt32(chkHotelName.Items[j].Value));
                Hotel objHotelAds = ObjHotelinfo.GetHotelByHotelID(Convert.ToInt32(chkHotelName.Items[j].Value));
                if (objComm.Count > 0)
                {
                    divmessage.Style.Add("display", "block");
                    divmessage.Attributes.Add("class", "error");
                    divmessage.InnerHtml =  objHotelAds.Name + " has been already created";
                    return;
                }
            }
        }

        for (int i = 0; i < chkHotelName.Items.Count; i++)
        {
            if (chkHotelName.Items[i].Selected)
            {
                foreach (GridViewRow row in grvCommission.Rows)
                {
                    HiddenField hdnId = (HiddenField)row.FindControl("hdnId");
                    Label lblFromDay = (Label)row.FindControl("FromDay");
                    Label lblToDay = (Label)row.FindControl("ToDay");
                    TextBox txtCommission = (TextBox)row.FindControl("txtCommission");

                    if (txtCommission.Text != "")
                    {
                        InvoiceCommPercentage Invoice = new InvoiceCommPercentage();
                        Invoice.ProfileName = txtProfileName.Text.ToString();
                        Invoice.CommissionDayId = Convert.ToInt32(hdnId.Value);
                        Invoice.CommissionPercentage = Convert.ToInt32(txtCommission.Text);
                        Invoice.HotelId = Convert.ToInt32(chkHotelName.Items[i].Value);
                        objOthers.RegisterNewCommission(Invoice);
                    }
                }
            }
        }

        BindInvoiceCommission();
        ApplyPaging();
        drpCountry.SelectedIndex = -1;
        txtProfileName.Text = "";
        chkHotelName.SelectedIndex = -1;
        trHotel.Visible = false;
        divmessage.Style.Add("display", "block");
        divmessage.Attributes.Add("class", "succesfuly");
        divmessage.InnerHtml = "New profile have been created for the specified hotel";
        divGrid.Attributes.Add("class", "error");
        divGrid.Attributes.Add("class", "succesfuly");
        divGrid.Style.Add("display", "none");            
        
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        divmessage.Style.Add("display", "none");
        divmessage.Attributes.Add("class", "error");
        divGrid.Style.Add("display", "none");
        divGrid.Attributes.Add("class", "error");
        Response.Redirect("~/Superadmin/SetInvoice.aspx", true);
    }

    protected void grvInvoiceCommPercentage_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
        {
            Label lblHotelId = (Label)e.Row.FindControl("lblHotelId");
            Hotel htlname = ObjHotelinfo.GetHotelByHotelID(Convert.ToInt32(lblHotelId.Text));
            lblHotelId.Text = htlname.Name;
        }
    }
    #endregion

    #region Apply paging
    private void ApplyPaging()
    {
        try
        {
            GridViewRow row = grvInvoiceCommPercentage.TopPagerRow;
            if (row != null)
            {
                PlaceHolder ph;
                LinkButton lnkPaging;
                LinkButton lnkPrevPage;
                LinkButton lnkNextPage;
                lnkPrevPage = new LinkButton();
                lnkPrevPage.CssClass = "pre";
                lnkPrevPage.Width = Unit.Pixel(73);
                lnkPrevPage.CommandName = "Page";
                lnkPrevPage.CommandArgument = "prev";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkPrevPage);
                if (grvInvoiceCommPercentage.PageIndex == 0)
                {
                    lnkPrevPage.Enabled = false;

                }
                for (int i = 1; i <= grvInvoiceCommPercentage.PageCount; i++)
                {
                    lnkPaging = new LinkButton();
                    if (ViewState["CurrentPage"] != null)
                    {
                        if (Convert.ToInt32(ViewState["CurrentPage"]) + 1 == i)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    else
                    {
                        if (i == 1)
                        {
                            lnkPaging.CssClass = "pag2";
                        }
                        else
                        {
                            lnkPaging.CssClass = "pag";
                        }
                    }
                    lnkPaging.Width = Unit.Pixel(16);
                    lnkPaging.Text = i.ToString();
                    lnkPaging.CommandName = "Page";
                    lnkPaging.CommandArgument = i.ToString();
                    if (i == grvInvoiceCommPercentage.PageIndex + 1)
                        ph = (PlaceHolder)row.FindControl("ph");
                    ph.Controls.Add(lnkPaging);
                }
                lnkNextPage = new LinkButton();
                lnkNextPage.CssClass = "nex";
                lnkNextPage.Width = Unit.Pixel(42);
                lnkNextPage.CommandName = "Page";
                lnkNextPage.CommandArgument = "next";
                ph = (PlaceHolder)row.FindControl("ph");
                ph.Controls.Add(lnkNextPage);
                ph = (PlaceHolder)row.FindControl("ph");
                if (grvInvoiceCommPercentage.PageIndex == grvInvoiceCommPercentage.PageCount - 1)
                {
                    lnkNextPage.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }

    protected void grvInvoiceCommPercentage_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            grvInvoiceCommPercentage.PageIndex = e.NewPageIndex;
            ViewState["CurrentPage"] = e.NewPageIndex;
            BindInvoiceCommission();
            ApplyPaging();
        }
        catch (Exception ex)
        {

        }
    }
    #endregion


    protected void imgDeleteBtn_Click(object sender, EventArgs e)
    {
        ImageButton btnchkcomm = (ImageButton)sender;       
        bool status = objOthers.DeleteCommission(Convert.ToInt32(btnchkcomm.CommandArgument));
        if (status)
        {
            divGrid.Style.Add("display", "block");
            divGrid.Attributes.Add("class", "succesfuly");
            divmessage.Attributes.Add("class", "error");
            divmessage.Attributes.Add("class", "succesfuly");
            divmessage.Style.Add("display", "none");            
            divGrid.InnerHtml = "Commission has been deleted.";
            BindInvoiceCommission();
        }
    }
}