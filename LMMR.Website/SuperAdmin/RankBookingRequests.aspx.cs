﻿
#region Included Namespaces

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;

#endregion


public partial class SuperAdmin_RankBookings : System.Web.UI.Page
{
    #region Variable Declaration

    SuperAdminTaskManager manager = new SuperAdminTaskManager();

    #endregion

    #region PageLoad

    /// <summary>
    /// The page load event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
            // By default the bookings rankings are displayed below.
            radioBookinngs.Attributes.Add("class", "btn-active");
            radioRequests.Attributes.Add("class", "btn");
            bookingDIV.Visible = true;
            bookingSaveCancelDIV.Visible = true;
            requestDIV.Visible = false;
            requestSaveCancelDIV.Visible = false;

            fillInfo();            
        }
        //fillInfo();
        //if (radioBookinngs.Checked == true)
        //{
        //    bookingDIV.Visible = true;
        //    bookingSaveCancelDIV.Visible = true;
        //    requestDIV.Visible = false;
        //    requestSaveCancelDIV.Visible = false;
        //}
        //else
        //{
        //    requestDIV.Visible = true;
        //    requestSaveCancelDIV.Visible = true;
        //    bookingDIV.Visible = false;
        //    bookingSaveCancelDIV.Visible = false;
        //}
        
    }

    #endregion

    #region Methods

    /// <summary>
    /// This method fills the page with information retrieved from database.
    /// </summary>
    
    public void fillInfo()
    {
        TList<RankingAlgoMaster> entities = new TList<RankingAlgoMaster>();
        entities = manager.getInfo();
        if (bookingDIV.Visible == true)
        {
            txtWeightageR1.Text = entities[0].RulePercentage.ToString();
            txtWeightageR2.Text = entities[1].RulePercentage.ToString();
            txtWeightageR3.Text = entities[2].RulePercentage.ToString();
            txtWeightageR4.Text = entities[3].RulePercentage.ToString();
            txtWeightageR5.Text = entities[4].RulePercentage.ToString();
        }
        else
        {
            txtWeightageRR1.Text = entities[5].RulePercentage.ToString();
            txtWeightageRR2.Text = entities[6].RulePercentage.ToString();
            txtWeightageRR3.Text = entities[7].RulePercentage.ToString();
            txtWeightageRR4.Text = entities[8].RulePercentage.ToString();
            txtWeightageRR5.Text = entities[9].RulePercentage.ToString();
            txtWeightageRR6.Text = entities[10].RulePercentage.ToString();
        }
    }

    #endregion

    #region Events

    /// <summary>
    /// This is the event handler for the save Button click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    
    protected void lbtSave_Click(object sender, EventArgs e)
    {
        RankingAlgoMaster entity1 = new RankingAlgoMaster();
        RankingAlgoMaster entity2 = new RankingAlgoMaster();
        RankingAlgoMaster entity3 = new RankingAlgoMaster();
        RankingAlgoMaster entity4 = new RankingAlgoMaster();
        RankingAlgoMaster entity5 = new RankingAlgoMaster();

        entity1.Id = 1;
        entity1.RulePercentage = Convert.ToInt32(txtWeightageR1.Text);
        string status1 = manager.updateInfo(entity1);
        entity2.Id = 2;
        entity2.RulePercentage = Convert.ToInt32(txtWeightageR2.Text);
        string status2 = manager.updateInfo(entity2);
        entity3.Id = 3;
        entity3.RulePercentage = Convert.ToInt32(txtWeightageR3.Text);
        string status3 = manager.updateInfo(entity3);
        entity4.Id = 4;
        entity4.RulePercentage = Convert.ToInt32(txtWeightageR4.Text);
        string status4 = manager.updateInfo(entity4);
        entity5.Id = 5;
        entity5.RulePercentage = Convert.ToInt32(txtWeightageR5.Text);
        string status5 = manager.updateInfo(entity5);
        if (status1 == "Information updated successfully." && status2 == "Information updated successfully." && status3 == "Information updated successfully." && status4 == "Information updated successfully." && status5 == "Information updated successfully.")
        {
            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "succesfuly");
            divmessage.InnerHtml = status1;
        }
        else
        {
            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "error");
            divmessage.InnerHtml = "Information could not be updated.";
        }
        fillInfo();
    }

    /// <summary>
    /// This is the event handler for the cancel Button click event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    
    protected void lbtCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.RawUrl);
    }

    /// <summary>
    /// This is the event handler for the cancel Button click event in case of request.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    
    protected void lbtCancelRequest_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.RawUrl);
    }

    /// <summary>
    /// This is the event handler for the save Button click event in case of request.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    
    protected void lbtSaveRequest_Click(object sender, EventArgs e)
    {
        RankingAlgoMaster entity1 = new RankingAlgoMaster();
        RankingAlgoMaster entity2 = new RankingAlgoMaster();
        RankingAlgoMaster entity3 = new RankingAlgoMaster();
        RankingAlgoMaster entity4 = new RankingAlgoMaster();
        RankingAlgoMaster entity5 = new RankingAlgoMaster();
        RankingAlgoMaster entity6 = new RankingAlgoMaster();

        entity1.Id = 6;
        entity1.RulePercentage = Convert.ToInt32(txtWeightageRR1.Text);
        string status1 = manager.updateInfo(entity1);
        entity2.Id = 7;
        entity2.RulePercentage = Convert.ToInt32(txtWeightageRR2.Text);
        string status2 = manager.updateInfo(entity2);
        entity3.Id = 8;
        entity3.RulePercentage = Convert.ToInt32(txtWeightageRR3.Text);
        string status3 = manager.updateInfo(entity3);
        entity4.Id = 9;
        entity4.RulePercentage = Convert.ToInt32(txtWeightageRR4.Text);
        string status4 = manager.updateInfo(entity4);
        entity5.Id = 10;
        entity5.RulePercentage = Convert.ToInt32(txtWeightageRR5.Text);
        string status5 = manager.updateInfo(entity5);
        entity6.Id = 11;
        entity6.RulePercentage = Convert.ToInt32(txtWeightageRR6.Text);
        string status6 = manager.updateInfo(entity6);
        if (status1 == "Information updated successfully." && status2 == "Information updated successfully." && status3 == "Information updated successfully." && status4 == "Information updated successfully." && status5 == "Information updated successfully." && status6 == "Information updated successfully.")
        {
            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "succesfuly");
            divmessage.InnerHtml = status1;
        }
        else
        {
            divmessage.Style.Add("display", "block");
            divmessage.Attributes.Add("class", "error");
            divmessage.InnerHtml = "Information could not be updated.";
        }
        fillInfo();
    }

    /// <summary>
    /// This the event handler for radioBookinngs RadioButton checkedChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void radioBookinngs_CheckedChanged(object sender, EventArgs e)
    {
        divmessage.Style.Add("display", "none");
        divmessage.Attributes.Add("class", "error");
        bookingDIV.Visible = true;
        bookingSaveCancelDIV.Visible = true;
        requestDIV.Visible = false;
        requestSaveCancelDIV.Visible = false;

        radioBookinngs.Attributes.Add("class", "btn-active");
        radioRequests.Attributes.Add("class", "btn");
        fillInfo();
    }

    /// <summary>
    /// This the event handler for radioRequests RadioButton checkedChanged event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void radioRequests_CheckedChanged(object sender, EventArgs e)
    {
        divmessage.Style.Add("display", "none");
        divmessage.Attributes.Add("class", "error");
        bookingDIV.Visible = false ;
        bookingSaveCancelDIV.Visible = false;
        requestDIV.Visible = true;
        requestSaveCancelDIV.Visible = true;
        radioBookinngs.Attributes.Add("class", "btn");
        radioRequests.Attributes.Add("class", "btn-active");
        fillInfo();
    }

    #endregion
}