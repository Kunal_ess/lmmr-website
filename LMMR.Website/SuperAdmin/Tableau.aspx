﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuperAdmin/Main.master" AutoEventWireup="true"
    CodeFile="Tableau.aspx.cs" Inherits="SuperAdmin_Tableau" %>

<%@ Register Src="../UserControl/SuperAdmin/LanguageTab.ascx" TagName="LanguageTab"
    TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:HiddenField ID="hdnRecordID" Value="" runat="server" />
    <asp:HiddenField ID="hdnLanguageID" Value="" runat="server" />
    <asp:HiddenField ID="hdnItmeDescriptionID" Value="" runat="server" />
    <asp:HiddenField ID="hdnItemType" Value="" runat="server" />
    <asp:HiddenField ID="hdnPackageDescriptionID" Value="" runat="server" />
    <asp:HiddenField ID="hdnIsProcessDone" Value="0" runat="server" />
    <asp:HiddenField ID="IsItemChecked" Value="0" runat="server" />
    <div class="search-operator-layout1" style="margin-bottom: 20px;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <div class="search-operator-layout-left1">
                        <div class="search-operator-from1">
                            <div class="commisions-top-new1 ">
                                <table>
                                    <tr>
                                        <td style="height: 30px;">
                                            <asp:DropDownList ID="ddlCountry" runat="server" Width="180px" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <table width="100%" border="0" cellspacing="0">
        <%--<tr>
            <td bgcolor="#ffffff" style="border-top: 1px solid #A3D4F7; padding-left: 15px; width: 100px;">
                <b>Select Country</b>
            </td>
            <td bgcolor="#ffffff" style="border-top: 1px solid #A3D4F7;">
            </td>
        </tr>--%>
        <%--<tr>
            <td bgcolor="#ffffff" style="border-top: 1px solid #A3D4F7; padding-left: 15px;" colspan="2">
                <uc1:LanguageTab ID="LanguageTab1" runat="server" />
            </td>
        </tr>--%>
        <tr>
            <td bgcolor="#ffffff" style="padding: 10px;" colspan="2">
                <table width="100%;">
                    <tr>
                        <td colspan="2" width="100%">
                            <h3 style="font-size: 21px; border-bottom: #A3D4F7 solid 1px; font-weight: bold;
                                margin: 0px; padding: 0px 0px 0px 0px;">
                                Meetingroom Hire Items</h3>
                        </td>
                    </tr>
                    <tr align="right">
                        <td width="100%">
                            <asp:LinkButton ID="lnkAddNewMeetingRoom" runat="server" OnClick="lnkAddNewMeetingRoom_Click">Add New</asp:LinkButton>
                        </td>
                        <td width="0%">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="grdViewMeeringRoom" runat="server" Width="100%" CellPadding="8"
                    border="0" HeaderStyle-CssClass="heading-row" RowStyle-CssClass="con" AlternatingRowStyle-CssClass="con-dark"
                    AutoGenerateColumns="False" OnRowDataBound="grdViewMeeringRoom_RowDataBound"
                    GridLines="None" CellSpacing="1" BackColor="#A3D4F7" OnRowCommand="grdViewMeeringRoom_RowCommand">
                    <Columns>
                        <asp:TemplateField ItemStyle-Width="2%">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgDeleteBtn" CommandName="DeleteItem" CommandArgument='<%#Eval("Id") %>'
                                    ImageUrl="../images/delete-ol-btn.png" runat="server" OnClientClick="return ConfirmOnDelete();"
                                    ToolTip="Delete" />
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:ImageButton ID="imgDeleteBtn" CommandName="DeleteItem" CommandArgument='<%#Eval("Id") %>'
                                    ImageUrl="../images/delete-ol-btn.png" runat="server" OnClientClick="return ConfirmOnDelete();"
                                    ToolTip="Delete" />
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Item" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblItme" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr align="right">
                                        <td>
                                            <asp:LinkButton ID="lnkModifypackage" runat="server" CommandName="Modify" CommandArgument='<%# Eval("Id") %>'>Modify</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblItme" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr align="right">
                                        <td>
                                            <asp:LinkButton ID="lnkModifypackage" runat="server" CommandName="Modify" CommandArgument='<%# Eval("Id") %>'>Modify</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="VAT%" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="use on system ?" ItemStyle-Width="20%">
                            <ItemTemplate>
                                <asp:Label ID="lblUseOnSystem" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblUseOnSystem" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description" ItemStyle-Width="43%">
                            <ItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <center>
                            <b>No record found</b></center>
                    </EmptyDataTemplate>
                </asp:GridView>
                <br />
                <br />
                <table style="border-top: 1px red solid; border-top-color: #A3D4F7; width: 100%">
                    <tr>
                        <td colspan="2" width="100%">
                            <h3 style="font-size: 21px; border-bottom: #A3D4F7 solid 1px; font-weight: bold;
                                margin: 0px; padding: 0px 0px 0px 0px;">
                                Food & Beverages Items</h3>
                        </td>
                    </tr>
                    <tr align="right">
                        <td width="100%">
                            <asp:LinkButton ID="lnkFoodBeverageItem" runat="server" OnClick="lnkFoodBeverageItem_Click">Add New</asp:LinkButton>
                        </td>
                        <td width="0%">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="grdFoodBeverage" runat="server" Width="100%" CellPadding="8" border="0"
                    HeaderStyle-CssClass="heading-row" RowStyle-CssClass="con" AlternatingRowStyle-CssClass="con-dark"
                    AutoGenerateColumns="False" OnRowDataBound="grdFoodBeverage_RowDataBound" GridLines="None"
                    CellSpacing="1" DataKeyNames="Id" BackColor="#A3D4F7" OnRowCommand="grdFoodBeverage_RowCommand">
                    <Columns>
                        <asp:TemplateField ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="imgDeleteBtn" CommandName="DeleteItem" CommandArgument='<%#Eval("Id") %>'
                                                ImageUrl="../images/delete-ol-btn.png" runat="server" OnClientClick="return ConfirmOnDelete();"
                                                ToolTip="Delete" />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="imgDeleteBtn" CommandName="DeleteItem" CommandArgument='<%#Eval("Id") %>'
                                                ImageUrl="../images/delete-ol-btn.png" runat="server" OnClientClick="return ConfirmOnDelete();"
                                                ToolTip="Delete" />
                                        </td>
                                    </tr>
                                </table>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Item" ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblItemName" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr align="right">
                                        <td>
                                            <asp:LinkButton ID="lnkModifypackage" runat="server" CommandName="Modify" CommandArgument='<%# Eval("Id") %>'>Modify</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblItemName" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr align="right">
                                        <td>
                                            <asp:LinkButton ID="lnkModifypackage" runat="server" CommandName="Modify" CommandArgument='<%# Eval("Id") %>'>Modify</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="VAT%" ItemStyle-Width="8%" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="use on system ?" ItemStyle-Width="22%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblUseOnSystem" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblUseOnSystem" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Is extra ?" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblIsExtra" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblIsExtra" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description" ItemStyle-Width="43%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td align="center">
                                        <b>No record found </b>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </EmptyDataTemplate>
                </asp:GridView>
                <br />
                <br />
                <table style="border-top: 1px red solid; border-top-color: #A3D4F7; width: 100%">
                    <tr>
                        <td colspan="2" width="100%">
                            <h3 style="font-size: 21px; border-bottom: #A3D4F7 solid 1px; font-weight: bold;
                                margin: 0px; padding: 0px 0px 0px 0px;">
                                Equipment Items</h3>
                        </td>
                    </tr>
                    <tr align="right">
                        <td width="100%">
                            <asp:LinkButton ID="lnkAddNewEquipmentItem" runat="server" OnClick="lnkAddNewEquipmentItem_Click">Add New</asp:LinkButton>
                        </td>
                        <td width="0%">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="grdEquipment" runat="server" Width="100%" CellPadding="8" border="0"
                    HeaderStyle-CssClass="heading-row" RowStyle-CssClass="con" AlternatingRowStyle-CssClass="con-dark"
                    AutoGenerateColumns="False" OnRowDataBound="grdEquipment_RowDataBound" OnRowCommand="grdEquipment_RowCommand"
                    GridLines="None" CellSpacing="1" DataKeyNames="Id" BackColor="#A3D4F7">
                    <Columns>
                        <asp:TemplateField ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="imgDeleteBtn" CommandName="DeleteItem" CommandArgument='<%#Eval("Id") %>'
                                                ImageUrl="../images/delete-ol-btn.png" runat="server" OnClientClick="return ConfirmOnDelete();"
                                                ToolTip="Delete" />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="imgDeleteBtn" CommandName="DeleteItem" CommandArgument='<%#Eval("Id") %>'
                                                ImageUrl="../images/delete-ol-btn.png" runat="server" OnClientClick="return ConfirmOnDelete();"
                                                ToolTip="Delete" />
                                        </td>
                                    </tr>
                                </table>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Item" ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblItemName" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr align="right">
                                        <td>
                                            <asp:LinkButton ID="lnkModifypackage" runat="server" CommandName="Modify" CommandArgument='<%# Eval("Id") %>'>Modify</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblItemName" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr align="right">
                                        <td>
                                            <asp:LinkButton ID="lnkModifypackage" runat="server" CommandName="Modify" CommandArgument='<%# Eval("Id") %>'>Modify</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="VAT%" ItemStyle-Width="8%" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="use on system ?" ItemStyle-Width="17%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblUseOnSystem" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblUseOnSystem" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Is extra ?" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblIsExtra" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblIsExtra" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description" ItemStyle-Width="38%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td align="center">
                                        <b>No record found </b>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </EmptyDataTemplate>
                </asp:GridView>
                <br />
                <br />
                <table style="border-top: 1px red solid; border-top-color: #A3D4F7; width: 100%">
                    <tr>
                        <td colspan="2" width="100%">
                            <h3 style="font-size: 21px; border-bottom: #A3D4F7 solid 1px; font-weight: bold;
                                margin: 0px; padding: 0px 0px 0px 0px;">
                                Other Items</h3>
                        </td>
                    </tr>
                    <tr align="right">
                        <td width="100%">
                            <asp:LinkButton ID="lnkAddNewOtherItem" runat="server" OnClick="lnkAddNewOtherItem_Click">Add New</asp:LinkButton>
                        </td>
                        <td width="0%">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="grdOthers" runat="server" Width="100%" CellPadding="8" border="0"
                    HeaderStyle-CssClass="heading-row" RowStyle-CssClass="con" AlternatingRowStyle-CssClass="con-dark"
                    AutoGenerateColumns="False" OnRowDataBound="grdOthers_RowDataBound" OnRowCommand="grdOthers_RowCommand"
                    GridLines="None" CellSpacing="1" DataKeyNames="Id" EmptyDataRowStyle-BackColor="#A3D4F7"
                    BackColor="#A3D4F7">
                    <Columns>
                        <asp:TemplateField ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="imgDeleteBtn" CommandName="DeleteItem" CommandArgument='<%#Eval("Id") %>'
                                                ImageUrl="../images/delete-ol-btn.png" runat="server" OnClientClick="return ConfirmOnDelete();"
                                                ToolTip="Delete" />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="imgDeleteBtn" CommandName="DeleteItem" CommandArgument='<%#Eval("Id") %>'
                                                ImageUrl="../images/delete-ol-btn.png" runat="server" OnClientClick="return ConfirmOnDelete();"
                                                ToolTip="Delete" />
                                        </td>
                                    </tr>
                                </table>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Item" ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblItemName" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr align="right">
                                        <td>
                                            <asp:LinkButton ID="lnkModifypackage" runat="server" CommandName="Modify" CommandArgument='<%# Eval("Id") %>'>Modify</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblItemName" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr align="right">
                                        <td>
                                            <asp:LinkButton ID="lnkModifypackage" runat="server" CommandName="Modify" CommandArgument='<%# Eval("Id") %>'>Modify</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="VAT%" ItemStyle-Width="8%" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="use on system ?" ItemStyle-Width="17%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblUseOnSystem" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblUseOnSystem" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Is extra ?" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblIsExtra" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblIsExtra" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description" ItemStyle-Width="38%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td align="center">
                                        <b>No record found </b>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </EmptyDataTemplate>
                </asp:GridView>
                <br />
                <br />
                <table style="border-top: 1px red solid; border-top-color: #A3D4F7; width: 100%">
                    <tr>
                        <td colspan="2" width="100%">
                            <h3 style="font-size: 21px; border-bottom: #A3D4F7 solid 1px; font-weight: bold;
                                margin: 0px; padding: 0px 0px 0px 0px;">
                                Packages</h3>
                        </td>
                    </tr>
                    <tr align="right">
                        <td width="100%">
                            <asp:LinkButton ID="lnkAddpackage" runat="server" OnClick="lnkAddpackage_Click">Add New</asp:LinkButton>
                        </td>
                        <td width="0%">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="grdPackage" runat="server" Width="100%" CellPadding="8" border="0"
                    HeaderStyle-CssClass="heading-row" RowStyle-CssClass="con" AlternatingRowStyle-CssClass="con-dark"
                    AutoGenerateColumns="False" OnRowDataBound="grdPackage_RowDataBound" GridLines="None"
                    CellSpacing="1" DataKeyNames="Id" BackColor="#A3D4F7" OnRowCommand="grdPackage_RowCommand">
                    <Columns>
                        <asp:TemplateField ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="imgDeleteBtn" CommandName="DeletePackage" CommandArgument='<%#Eval("Id") %>'
                                                ImageUrl="../images/delete-ol-btn.png" runat="server" OnClientClick="return ConfirmOnDelete();"
                                                ToolTip="Delete" />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="imgDeleteBtn" CommandName="DeletePackage" CommandArgument='<%#Eval("Id") %>'
                                                ImageUrl="../images/delete-ol-btn.png" runat="server" OnClientClick="return ConfirmOnDelete();"
                                                ToolTip="Delete" />
                                        </td>
                                    </tr>
                                </table>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Item" ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblPackageName" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr align="right">
                                        <td>
                                            <asp:LinkButton ID="lnkModifypackage" CommandName="Modify" CommandArgument='<%# Eval("Id") %>'
                                                runat="server">Modify</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblPackageName" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr align="right">
                                        <td>
                                            <asp:LinkButton ID="lnkModifypackage" CommandName="Modify" CommandArgument='<%# Eval("Id") %>'
                                                runat="server">Modify</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="VAT%" ItemStyle-Width="8%" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblVat" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="use on system ?" ItemStyle-Width="22%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblUseOnSystem" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblUseOnSystem" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description" ItemStyle-Width="43%" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" Text=""></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td align="center">
                                        <b>No record found </b>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </EmptyDataTemplate>
                </asp:GridView>
                
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlPackge" runat="server" DefaultButton="lnkSavePackage">
        <div id="divAddUpdatePackagePopUp" runat="server" style="display: none">
            <div id="AddUpdatePackagePopUp-overlay">
            </div>
            <div id="AddUpdatePackagePopUp">
                <div class="popup-top">
                </div>
                <div class="popup-mid">
                    <div class="popup-mid-inner">
                        <table cellspacing="10" width="100%">
                            <tr>
                                <td colspan="2" align="center">
                                    <div class="error" id="errorPopupPackage" runat="server">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                    <asp:Label ID="lblPackageLanguageTab" Font-Bold="true" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Package Name :</b>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPackageName" runat="server" Text="" MaxLength="13" Width="250px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Package Description :</b>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPackageDescription" runat="server" Text="" TextMode="MultiLine"
                                        Width="350px" MaxLength="250"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Use on system? </b>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkPackageIsActive" runat="server" Checked="true" />
                                </td>
                            </tr>
                            <tr style="display: none">
                                <td>
                                    <b>item type :</b>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkfoodbeveragetype" runat="server" Text="food beverage" onclick="showlist(this);" />&nbsp;&nbsp;
                                    <asp:CheckBox ID="chkEquipmenttype" runat="server" Text="equipment" onclick="showlist(this);" />&nbsp;&nbsp;
                                    <asp:CheckBox ID="chkMeetingroomhiretype" runat="server" Text="meetingroom hire"
                                        onclick="showlist(this);" />
                                </td>
                            </tr>
                            <tr id="trItemCheckBoxList" runat="server">
                                <td>
                                </td>
                                <td>
                                    <div style="overflow-x: hidden; overflow-y: auto; height: 150px;">
                                        <table width="100%">
                                            <tr>
                                                <asp:Panel ID="MeetingroomDiv" runat="server" Visible="false">
                                                    <td align="left" valign="top" id="tdMeetingroomHire" style="width: 25%;>
                                                        <b>Meetingroom Hire</b>
                                                        <asp:CheckBoxList ID="chkListMeetingroom" runat="server">
                                                        </asp:CheckBoxList>
                                                    </td>
                                                </asp:Panel>
                                                <asp:Panel ID="FoodBeverageDiv" runat="server">
                                                    <td align="left" valign="top" id="tdFoodbeverage" style="width: 25%;">
                                                        <b>Food & beverages</b>
                                                        <asp:CheckBoxList ID="chkListFoodBeverage" runat="server" onclick="IsItemCheckedByCheckBox();">
                                                        </asp:CheckBoxList>
                                                    </td>
                                                </asp:Panel>
                                                <asp:Panel ID="EquipmentDiv" runat="server">
                                                    <td align="left" valign="top" id="tdEquipment" style="width: 25%;">
                                                        <b>Equipment</b>
                                                        <asp:CheckBoxList ID="chkListEquipment" runat="server" onclick="return IsItemCheckedByCheckBox();">
                                                        </asp:CheckBoxList>
                                                    </td>
                                                </asp:Panel>
                                                <asp:Panel ID="OthersDiv" runat="server">
                                                    <td align="left" valign="top" id="tdOthers" style="width: 25%;">
                                                        <b>Others</b>
                                                        <asp:CheckBoxList ID="chkListOthers" runat="server" onclick="return IsItemCheckedByCheckBox();">
                                                        </asp:CheckBoxList>
                                                    </td>
                                                </asp:Panel>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div class="button_section">
                            <asp:LinkButton ID="lnkSavePackage" runat="server" CssClass="select" OnClientClick="return ValidationAndSavePackage();">Save</asp:LinkButton>
                            &nbsp;or&nbsp;
                            <asp:LinkButton ID="lnkCancelPackage" runat="server" CssClass="cancelpop" OnClick="lnkCancelPackage_Click">Cancel</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="popup-bottom">
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlItem" runat="server" DefaultButton="btnSaveItme">
        <div id="DivItemPopUp" runat="server" style="display: none">
            <div id="AddUpdateItemPopUp-overlay">
            </div>
            <div id="AddUpdateItemPopUp">
                <div class="popup-top">
                </div>
                <div class="popup-mid">
                    <div class="popup-mid-inner">
                        <table cellspacing="8">
                            <tr>
                                <td colspan="2" align="center">
                                    <div class="error" id="errorPopupItem" runat="server">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="ucLanguageTab" runat="server" Visible="true"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Item :</b>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtItem" runat="server" Text="" MaxLength="100" Width="230px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Description:</b>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDescription" runat="server" Text="" MaxLength="250" TextMode="MultiLine"
                                        Width="230px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>VAT % :</b>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtVat" runat="server" Text="" MaxLength="5" Width="50px"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" TargetControlID="txtVat"
                                        ValidChars="0123456789." runat="server">
                                    </asp:FilteredTextBoxExtender>
                                </td>
                            </tr>
                            <tr id="trIsExtra" runat="server">
                                <td>
                                    <b>Use on extra? </b>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkIsExtra" runat="server" Checked="false" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Use on system? </b>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkIsActive" runat="server" Checked="true" />
                                </td>
                            </tr>
                        </table>
                        <div class="button_section">
                            <asp:LinkButton ID="btnSaveItme" runat="server" CssClass="select" OnClientClick="return ValidateAndSaveItem();">Save</asp:LinkButton>
                            &nbsp;or&nbsp;
                            <asp:LinkButton ID="lnkFoodBeverageCnacel" runat="server" CssClass="cancelpop" OnClick="lnkFoodBeverageCnacel_Click">Cancel</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="popup-bottom">
                </div>
            </div>
        </div>
    </asp:Panel>
    <script type="text/javascript" language="javascript">

        function ConfirmOnDelete() {
            return confirm("Are you sure you want to delete it?");
        }
        function OpenPackagePopUp() {
            jQuery("body").scrollTop(0);
            jQuery("html").scrollTop(0);
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            jQuery("#AddUpdatePackagePopUp").show();
            jQuery("#AddUpdatePackagePopUp-overlay").show();
            return false;
        }

        function ClosePackagePopUp() {
            document.getElementsByTagName('html')[0].style.overflow = 'auto';
            document.getElementById('AddUpdatePackagePopUp').style.display = 'none';
            document.getElementById('AddUpdatePackagePopUp-overlay').style.display = 'none';
            return false;
        }

        function OpenItemPopUp() {
            jQuery("body").scrollTop(0);
            jQuery("html").scrollTop(0);
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            jQuery("#AddUpdateItemPopUp").show();
            jQuery("#AddUpdateItemPopUp-overlay").show();
            return false;
        }

        function CloseItemPopUp() {
            jQuery('#<%=txtItem.ClientID %>').val('');
            jQuery('#<%=txtDescription.ClientID %>').val('');
            jQuery('#<%=txtVat.ClientID %>').val('');
            jQuery('#<%=chkIsActive.ClientID %>').attr('checked', false);
            document.getElementsByTagName('html')[0].style.overflow = 'auto';
            document.getElementById('AddUpdateItemPopUp').style.display = 'none';
            document.getElementById('AddUpdateItemPopUp-overlay').style.display = 'none';
            return false;
        }



        function SaveItem() {
            jQuery("#Loding_overlay").show();
            jQuery("#Loding_overlay").find("span").html('Saving...');

            var getname = jQuery('#<%=txtItem.ClientID %>').val();
            var replaceName = getname.replace("'", "R~D").replace("'", "R~L");

            var getdescription = jQuery('#<%=txtDescription.ClientID %>').val();
            var replaceDesc = getdescription.replace("'", "R~D").replace("'", "R~L");

            jQuery.ajax({
                type: "POST",
                url: "Tableau.aspx/SaveItem",
                data: "{'intLanguageID':'" + parseInt(jQuery('#<%=hdnLanguageID.ClientID %>').val()) + "','intItemID':'" + parseInt(jQuery('#<%=hdnRecordID.ClientID %>').val()) + "','intItemDescriptionID':'" + parseInt(jQuery('#<%=hdnItmeDescriptionID.ClientID %>').val()) + "','strItemName':'" + replaceName + "','strItemDescription':'" + replaceDesc + "','vat':'" + jQuery('#<%=txtVat.ClientID %>').val() + "','boolIsActive':'" + jQuery('#<%=chkIsActive.ClientID %>').is(":checked") + "','intcountryID':'" + jQuery('#<%=ddlCountry.ClientID %>').val() + "','intItemType':'" + jQuery('#<%=hdnItemType.ClientID %>').val() + "','boolIsExtra':'" + jQuery('#<%=chkIsExtra.ClientID %>').is(":checked") + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != null) {
                        var GetString = msg.d;
                        var strSplit = GetString.split(',');

                        if (strSplit[0] == 'Saved Successfully') {
                            jQuery('#<%=hdnRecordID.ClientID %>').val(strSplit[1]);
                            jQuery('#<%=hdnItmeDescriptionID.ClientID %>').val(strSplit[2]);
                            if (jQuery("#<%= errorPopupItem.ClientID %>").hasClass("error")) {
                                jQuery("#<%= errorPopupItem.ClientID %>").removeClass("error").addClass("succesfuly").show();
                                jQuery("#<%= errorPopupItem.ClientID %>").html('Data saved successfully');
                            }

                            if (jQuery("#liLanguageTab" + parseInt(jQuery('#<%=hdnLanguageID.ClientID %>').val()) + "").hasClass('select')) {
                                jQuery("#liLanguageTab" + parseInt(jQuery('#<%=hdnLanguageID.ClientID %>').val()) + "").parent("li").removeClass('redtab');
                            }
                            jQuery("#Loding_overlay").hide();
                        }
                        else if (strSplit[0] == 'Sorry ! Please contact to technical support') {

                            if (jQuery("#<%= errorPopupItem.ClientID %>").hasClass("succesfuly")) {
                                jQuery("#<%= errorPopupItem.ClientID %>").removeClass("succesfuly").addClass("error").show();
                                jQuery('#<%=errorPopupItem.ClientID %>').html('Sorry ! Please contact to technical support.');

                            }
                            else {
                                jQuery("#<%= errorPopupItem.ClientID %>").show();
                                jQuery('#<%=errorPopupItem.ClientID %>').html('Sorry ! Please contact to technical support.');

                            }
                            jQuery("#Loding_overlay").hide();
                        }
                        else if (strSplit[0] == 'Please insert in english first') {

                            if (jQuery("#<%= errorPopupItem.ClientID %>").hasClass("succesfuly")) {
                                jQuery("#<%= errorPopupItem.ClientID %>").removeClass("succesfuly").addClass("error").show();
                                jQuery('#<%=errorPopupItem.ClientID %>').html('Please insert in english first.');
                            }
                            else {
                                jQuery("#<%= errorPopupItem.ClientID %>").show();
                                jQuery('#<%=errorPopupItem.ClientID %>').html('Please insert in english first.');
                            }
                            jQuery("#Loding_overlay").hide();
                        }
                        else {
                            if (jQuery("#<%= errorPopupItem.ClientID %>").hasClass("succesfuly")) {
                                jQuery("#<%= errorPopupItem.ClientID %>").removeClass("succesfuly").addClass("error").show();
                                jQuery('#<%=errorPopupItem.ClientID %>').html('data not saved successfully.');
                            }
                            else {
                                jQuery("#<%= errorPopupItem.ClientID %>").show();
                                jQuery('#<%=errorPopupItem.ClientID %>').html('data not saved successfully.');
                            }

                            jQuery("#Loding_overlay").hide();

                        }
                    } else {

                        if (jQuery("#<%= errorPopupItem.ClientID %>").hasClass("succesfuly")) {
                            jQuery("#<%= errorPopupItem.ClientID %>").removeClass("succesfuly").addClass("error").show();
                            jQuery('#<%=errorPopupItem.ClientID %>').html('data not saved successfully.');
                        }

                        jQuery("#Loding_overlay").hide();
                    }
                }

            });

            return false;
        }

        function GetLanguageID(languageId) {
            jQuery("#<%=hdnLanguageID.ClientID %>").val(languageId);
            jQuery(".smalltab ul li a").removeClass("select");
            jQuery("#liLanguageTab" + languageId + "").addClass("select");
            if (jQuery("#<%= errorPopupItem.ClientID %>").hasClass("succesfuly")) {
                jQuery("#<%= errorPopupItem.ClientID %>").removeClass("succesfuly").addClass("error").hide();
            }
            jQuery("#<%= errorPopupItem.ClientID %>").hide();
            jQuery("#<%= errorPopupItem.ClientID %>").html('');

            jQuery.ajax({
                type: "POST",
                url: "Tableau.aspx/GetDescription",
                data: "{'intLanguageID':'" + parseInt(jQuery('#<%=hdnLanguageID.ClientID %>').val()) + "','intItemID':'" + parseInt(jQuery('#<%=hdnRecordID.ClientID %>').val()) + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var result = jQuery.parseJSON(msg.d)
                    if (result != null) {
                        jQuery('#<%=txtItem.ClientID %>').val(result.ItemName);
                        jQuery('#<%=txtDescription.ClientID %>').val(result.ItemDescription);
                        jQuery('#<%=hdnItmeDescriptionID.ClientID %>').val(result.Id);

                    } else {
                        jQuery('#<%=txtItem.ClientID %>').val("");
                        jQuery('#<%=txtDescription.ClientID %>').val("");
                        jQuery('#<%=hdnItmeDescriptionID.ClientID %>').val("0");
                    }
                }

            });
        }

        function GetPackageLanguageID(languageId) {

            jQuery("#<%=hdnLanguageID.ClientID %>").val(languageId);
            jQuery(".smalltab ul li a").removeClass("select");
            jQuery("#liLanguageTab" + languageId + "").addClass("select");
            if (jQuery("#<%= errorPopupPackage.ClientID %>").hasClass("succesfuly")) {
                jQuery("#<%= errorPopupPackage.ClientID %>").removeClass("succesfuly").addClass("error").hide();
            }
            jQuery("#<%= errorPopupPackage.ClientID %>").hide();
            jQuery("#<%= errorPopupPackage.ClientID %>").html('');
            jQuery.ajax({
                type: "POST",
                url: "Tableau.aspx/GetPackageDescription",
                data: "{'intLanguageID':'" + parseInt(jQuery('#<%=hdnLanguageID.ClientID %>').val()) + "','intPackageID':'" + parseInt(jQuery('#<%=hdnRecordID.ClientID %>').val()) + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var result = jQuery.parseJSON(msg.d)

                    if (result != null) {

                        jQuery('#<%=txtPackageDescription.ClientID %>').val(result.Description);
                        jQuery('#<%=hdnPackageDescriptionID.ClientID %>').val(result.Id);

                    } else {
                        jQuery('#<%=txtPackageDescription.ClientID %>').val("");
                        jQuery('#<%=hdnPackageDescriptionID.ClientID %>').val("0");
                    }
                }

            });
        }


        function SendPackegeData() {
            jQuery("#Loding_overlay").show();
            jQuery("#Loding_overlay").find("span").html('Saving...');

            var getname = jQuery('#<%=txtPackageName.ClientID %>').val();
            var replaceName = getname.replace("'", "R~D").replace("'", "R~L");

            var getdescription = jQuery('#<%=txtPackageDescription.ClientID %>').val();
            var replaceDesc = getdescription.replace("'", "R~D").replace("'", "R~L");

            jQuery.ajax({
                type: "POST",
                url: "Tableau.aspx/SavePackage",
                data: "{'intLanguageID':'" + parseInt(jQuery('#<%=hdnLanguageID.ClientID %>').val()) + "','intPackageID':'" + parseInt(jQuery('#<%=hdnRecordID.ClientID %>').val()) + "','intPackageDescriptionID':'" + parseInt(jQuery('#<%=hdnPackageDescriptionID.ClientID %>').val()) + "','strPackageName':'" + replaceName + "','strPackageDescription':'" + replaceDesc + "','boolIsActive':'" + jQuery('#<%=chkPackageIsActive.ClientID %>').is(":checked") + "','intcountryID':'" + jQuery('#<%=ddlCountry.ClientID %>').val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != null) {

                        var GetString = msg.d;
                        var strSplit = GetString.split(',');

                        if (strSplit[0] == 'Saved Successfully') {

                            jQuery('#<%=hdnRecordID.ClientID %>').val(strSplit[1]);
                            jQuery('#<%=hdnPackageDescriptionID.ClientID %>').val(strSplit[2]);

                            var isItemChk = jQuery("#<%=IsItemChecked.ClientID %>").val();
                            if (isItemChk == "1") {
                                jQuery('#<%=IsItemChecked.ClientID %>').val('0');
                                jQuery.ajax({
                                    type: "POST",
                                    url: "Tableau.aspx/CleanOldMapping",
                                    data: "{'intPackageID':'" + parseInt(jQuery('#<%=hdnRecordID.ClientID %>').val()) + "'}",
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    success: function (msg) {
                                        if (msg.d != null) {
                                            if (msg.d == "Deleted") {

                                                jQuery("#tdMeetingroomHire").find("input:checkbox:checked").each(function () {

                                                    var itemValue = jQuery(this).val();
                                                    PackageItemMapping(itemValue);

                                                }
);
                                                jQuery("#tdFoodbeverage").find("input:checkbox:checked").each(function () {
                                                    
                                                    var itemValue = jQuery(this).val();
                                                    PackageItemMapping(itemValue);
                                                }
);

                                                jQuery("#tdEquipment").find("input:checkbox:checked").each(function () {

                                                    var itemValue = jQuery(this).val();
                                                    PackageItemMapping(itemValue);

                                                }
);

                                                jQuery("#tdOthers").find("input:checkbox:checked").each(function () {

                                                    var itemValue = jQuery(this).val();
                                                    PackageItemMapping(itemValue);
                                                }
);
                                                IsMappingDone();
                                            }
                                            else {

                                                if (jQuery("#<%= errorPopupPackage.ClientID %>").hasClass("succesfuly")) {
                                                    jQuery("#<%= errorPopupPackage.ClientID %>").removeClass("succesfuly").addClass("error").show();
                                                    jQuery('#<%=errorPopupPackage.ClientID %>').html('data not saved successfully.');
                                                }

                                            }
                                        }


                                    }

                                });
                            }
                            else {

                                IsMappingDone();
                            }



                        }
                        else if (strSplit[0] == 'Sorry ! Please contact to technical support') {

                            if (jQuery("#<%= errorPopupPackage.ClientID %>").hasClass("succesfuly")) {
                                jQuery("#<%= errorPopupPackage.ClientID %>").removeClass("succesfuly").addClass("error").show();
                                jQuery('#<%=errorPopupPackage.ClientID %>').html('Sorry ! Please contact to technical support.');

                            }
                            else {
                                jQuery("#<%= errorPopupPackage.ClientID %>").show();
                                jQuery('#<%=errorPopupPackage.ClientID %>').html('Sorry ! Please contact to technical support.');

                            }
                            jQuery("#Loding_overlay").hide();
                        }
                        else if (strSplit[0] == 'Please insert in english first') {

                            if (jQuery("#<%= errorPopupPackage.ClientID %>").hasClass("succesfuly")) {
                                jQuery("#<%= errorPopupPackage.ClientID %>").removeClass("succesfuly").addClass("error").show();
                                jQuery('#<%=errorPopupPackage.ClientID %>').html('Please insert in english first.');
                            }
                            else {
                                jQuery("#<%= errorPopupPackage.ClientID %>").show();
                                jQuery('#<%=errorPopupPackage.ClientID %>').html('Please insert in english first.');
                            }
                            jQuery("#Loding_overlay").hide();
                        }


                    }
                }
            });

            return false;
        }

        function PackageItemMapping(itemValue) {
            jQuery.ajax({
                type: "POST",
                url: "Tableau.aspx/SavePackageItemMapping",
                data: "{'intPackageID':'" + parseInt(jQuery('#<%=hdnRecordID.ClientID %>').val()) + "','intItemID':'" + parseInt(itemValue) + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != null) {
                        if (msg.d == "Saved") {

                        }
                        else {

                        }
                    }
                }
            });
            return false;
        }


        function IsMappingDone() {
            jQuery.ajax({
                type: "POST",
                url: "Tableau.aspx/IsMappingDone",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != null) {
                        if (msg.d == "Saved") {

                            if (jQuery("#<%= errorPopupPackage.ClientID %>").hasClass("error")) {
                                jQuery("#<%= errorPopupPackage.ClientID %>").removeClass("error").addClass("succesfuly").show();
                                jQuery("#<%= errorPopupPackage.ClientID %>").html('Data saved successfully');
                            }

                            if (jQuery("#liLanguageTab" + parseInt(jQuery('#<%=hdnLanguageID.ClientID %>').val()) + "").hasClass('select')) {
                                jQuery("#liLanguageTab" + parseInt(jQuery('#<%=hdnLanguageID.ClientID %>').val()) + "").parent("li").removeClass('redtab');
                            }
                            jQuery("#Loding_overlay").hide();
                        }
                        else {

                        }
                    }
                }
            });
            return false;
        }
        jQuery(document).ready(function () {
            if (jQuery("#<%= errorPopupItem.ClientID %>").hasClass("succesfuly")) {
                jQuery("#<%= errorPopupItem.ClientID %>").removeClass("succesfuly").addClass("error").hide();
            }
            jQuery("#<%= errorPopupItem.ClientID %>").html("");
            jQuery("#<%= errorPopupItem.ClientID %>").hide();

            if (jQuery("#<%= errorPopupPackage.ClientID %>").hasClass("succesfuly")) {
                jQuery("#<%= errorPopupPackage.ClientID %>").removeClass("succesfuly").addClass("error").hide();
            }
            jQuery("#<%= errorPopupPackage.ClientID %>").html("");
            jQuery("#<%= errorPopupPackage.ClientID %>").hide();
        });

        function ValidateAndSaveItem() {
            if (jQuery("#<%= errorPopupItem.ClientID %>").hasClass("succesfuly")) {
                jQuery("#<%= errorPopupItem.ClientID %>").removeClass("succesfuly").addClass("error").hide();
            }
            var isvalid = true;
            var errormessage = "";
            var ItemName = jQuery("#<%= txtItem.ClientID %>").val();

            if (ItemName.length <= 0 || ItemName == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }

                errormessage += 'Please enter item name';
                isvalid = false;
            }

            var ItemDescription = jQuery("#<%= txtDescription.ClientID %>").val();
            if (ItemDescription.length <= 0 || ItemDescription == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }
                errormessage += 'Please enter item description';
                isvalid = false;
            }

            var ItemVat = jQuery("#<%= txtVat.ClientID %>").val();
            if (ItemVat.length <= 0 || ItemVat == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }
                errormessage += 'Please enter item vat';
                isvalid = false;
            }
            else if (isNaN(ItemVat)) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Enter valid vat";
                isvalid = false;
            }
            if (!isvalid) {
                jQuery("#<%= errorPopupItem.ClientID %>").show();
                jQuery("#<%= errorPopupItem.ClientID %>").html(errormessage);
                return false;
            }
            jQuery("#<%= errorPopupItem.ClientID %>").html("");
            jQuery("#<%= errorPopupItem.ClientID %>").hide();
            jQuery("body").scrollTop(0);
            jQuery("html").scrollTop(0);
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            jQuery("#AddUpdateItemPopUp").show();
            jQuery("#AddUpdateItemPopUp-overlay").show();

            //This method call for save item.
            SaveItem();

            return false;
        }

        function ValidationAndSavePackage() {
            if (jQuery("#<%= errorPopupPackage.ClientID %>").hasClass("succesfuly")) {
                jQuery("#<%= errorPopupPackage.ClientID %>").removeClass("succesfuly").addClass("error").hide();
            }
            var isvalid = true;
            var errormessage = "";
            var PackageName = jQuery("#<%= txtPackageName.ClientID %>").val();

            if (PackageName.length <= 0 || PackageName == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }

                errormessage += 'Please enter package name';
                isvalid = false;
            }

            var PackageItmeDescription = jQuery("#<%= txtPackageDescription.ClientID %>").val();
            if (PackageItmeDescription.length <= 0 || PackageItmeDescription == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>"
                }
                errormessage += 'Please enter package description';
                isvalid = false;
            }

            var IsNew = jQuery("#<%=hdnRecordID.ClientID %>").val();
            if (IsNew == "0") {
                if (jQuery("#tdFoodbeverage").find("input:checkbox:checked").length <= 0 && jQuery("#tdEquipment").find("input:checkbox:checked").length <= 0 && jQuery("#tdOthers").find("input:checkbox:checked").length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>"
                    }
                    errormessage += 'Please select atleast one item.';
                    isvalid = false;
                }
            }

            if (!isvalid) {
                jQuery("#<%= errorPopupPackage.ClientID %>").show();
                jQuery("#<%= errorPopupPackage.ClientID %>").html(errormessage);
                return false;
            }
            jQuery("#<%= errorPopupPackage.ClientID %>").html("");
            jQuery("#<%= errorPopupPackage.ClientID %>").hide();
            jQuery("body").scrollTop(0);
            jQuery("html").scrollTop(0);
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';
            jQuery("#AddUpdatePackagePopUp").show();
            jQuery("#AddUpdatePackagePopUp-overlay").show();

            //This method call for save package.
            SendPackegeData();

            return false;
        }


        function IsItemCheckedByCheckBox() {
            jQuery("#<%=IsItemChecked.ClientID %>").val('1');
        }
    </script>
</asp:Content>
