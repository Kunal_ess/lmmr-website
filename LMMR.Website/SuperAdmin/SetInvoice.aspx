﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuperAdmin/Main.master" AutoEventWireup="true" CodeFile="SetInvoice.aspx.cs" Inherits="SuperAdmin_SetInvoice" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="cancelation-heading-body3" id="HotelAssignTab" runat="server">
        <ul>
            <li><a id="liTimeFrame" href="ManageCountryOthers.aspx">Country</a></li>
            <li><a id="liApplication" href="ApplicationOthers.aspx">Application</a></li>
            <li><a id="liManageLanaguage" href="ManageLanguage.aspx">Manage Language</a></li>
            <li><a id="liSetinvoice" class="select" href="SetInvoice.aspx">Set Invoice</a></li>
        </ul>
    </div>
<div class="operator-mainbody-1"  >
<div id="divmessage" runat="server" class="error">
            </div>     
<table width="100%" border="0" cellspacing="0"  style="border: 1px solid #A3D4F7; background:#ECF7FE; padding:10px" cellpadding="5">
<tr>
    <td width="30%">Country Name</td>
    <td width="70%">
        <asp:DropDownList ID="drpCountry" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpCountry_SelectedIndexChanged"
                                            CssClass="NoClassApply">
                                        </asp:DropDownList></td>
  </tr>
  <tr>
    <td width="30%">Profile Name</td>
    <td width="70%">
        <asp:TextBox ID="txtProfileName" runat="server" class="inputbox13" MaxLength="50"></asp:TextBox></td>
  </tr> 
  <tr id="trMessage" runat="server">    
    <td width="30%"></td>
    <td width="70%">
        <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label></td>
  </tr> 
    <tr id="trHotel" runat="server" visible="false">
    <td valign="top" width="20%">List of all hotels</td>
    <td width="80%"><table border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td width="100%" style="text-align:left">
    <asp:Panel ID="Panel1" runat="server" Height="200px" ScrollBars="Vertical">    
    <asp:CheckBoxList ID="chkHotelName" runat="server" Width="100%">                              
    </asp:CheckBoxList>    
    </asp:Panel>
    </td>
  </tr>  
</table>
</td>
  </tr> 
      <tr>
    <td valign="top" width="30%">Set Commission Percentage</td>
    <td width="70%">
    <table width="100%" cellspacing="1" cellpadding="5" border="0" bgcolor="#98BCD6">  
<tr bgcolor="#CCD8D8"> 
                    <td valign="top" width="20%">
                    From
                    </td>  
                    <td valign="top" width="20%">
                        To
                    </td>                
                    <td valign="top" width="30%">
                        Commission
                    </td>                                                               
                </tr> 
                </table>
<asp:GridView ID="grvCommission" runat="server" Width="100%" border="0" CellPadding="5"
                    CellSpacing="1" AutoGenerateColumns="false" DataKeyNames="Id" GridLines="None"
                    ShowHeader="false" ShowFooter="false" BackColor="#98BCD6" 
                    RowStyle-CssClass="con" AlternatingRowStyle-CssClass="con-dark"             
                    EmptyDataRowStyle-BackColor="White" EmptyDataRowStyle-HorizontalAlign="Center"
                    HeaderStyle-BackColor="#98BCD6">
                    <Columns>
                        <asp:TemplateField ItemStyle-Width="20%">
                            <ItemTemplate>                                
                                <asp:Label ID="lblFromDay" runat="server" Text='<%# Eval("FromDay") %>'></asp:Label>
                                <asp:HiddenField ID="hdnId" runat="server" Value='<%# Eval("Id") %>'></asp:HiddenField>
                            </ItemTemplate>                           
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="20%">
                            <ItemTemplate>                                
                                <asp:Label ID="lblToDay" runat="server" Text='<%# Eval("ToDay") %>'></asp:Label>
                            </ItemTemplate>                           
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="30%">
                            <ItemTemplate>                                
                                <asp:TextBox ID="txtCommission" runat="server" class="inputbox13" MaxLength="2"></asp:TextBox>
                                  <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" TargetControlID="txtCommission"
                                ValidChars="0123456789" runat="server">                                
                            </asp:FilteredTextBoxExtender>
                            </ItemTemplate>                           
                        </asp:TemplateField>                      

                   </Columns>                                        
                </asp:GridView>
</td>
  </tr>  
 <tr>    
    <td width="100%" colspan="2" align="left"> <div class="button_section">
                            <asp:LinkButton ID="btnSubmit" CssClass="select" runat="server" Text="Save"
                                ValidationGroup="LOGIN" onclick="btnSubmit_Click" />
                            <span>or</span>
                            <asp:LinkButton ID="btnCancel" runat="server" Text="Cancel" 
                                onclick="btnCancel_Click"/>
                        </div></td>
    <td>
    </td>
  </tr>
</table>
          </div>
          <div class="pageing-operator">
            <div style="float: right;" id="pageing">
            </div>
        </div>
        <div id="divGrid" runat="server" class="error">
            </div>  
          <div class="operator-mainbody-1">
          <table width="100%" cellspacing="1" cellpadding="5" border="0" bgcolor="#98BCD6">
  <tbody>
  <tr bgcolor="#CCD8D8">
  <td valign="top" width="25%">Profile Name</td>
    <td valign="top" width="25%">Hotel Name</td>
    <td valign="top" width="10%">From</td>    
    <td valign="top" width="10%">To</td>
    <td valign="top" width="15%">Commission %</td>
    <td valign="top" width="15%"></td>
  </tr> 
  <asp:GridView ID="grvInvoiceCommPercentage" runat="server" Width="100%" border="0" CellPadding="5"
                    CellSpacing="1" AutoGenerateColumns="false" 
          DataKeyNames="CommId" GridLines="None"
                    ShowHeader="false" ShowFooter="false" BackColor="#98BCD6" 
          RowStyle-CssClass="con" AlternatingRowStyle-CssClass="con-dark"                    
                    EmptyDataRowStyle-BackColor="White" EmptyDataRowStyle-HorizontalAlign="Center"
                    HeaderStyle-BackColor="#98BCD6" 
          onrowdatabound="grvInvoiceCommPercentage_RowDataBound" OnPageIndexChanging="grvInvoiceCommPercentage_PageIndexChanging" PageSize="10" AllowPaging="true"> 
                    <Columns>
                        <asp:TemplateField ItemStyle-Width="25%">
                            <ItemTemplate>                                
                                <asp:Label ID="lblProfileName" runat="server" Text='<%# Eval("ProfileName") %>'></asp:Label>
                            </ItemTemplate>     
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="25%">
                            <ItemTemplate>
                                <asp:Label ID="lblHotelId" runat="server" Text='<%# Eval("HotelId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField> 
                        <asp:TemplateField ItemStyle-Width="10%">
                            <ItemTemplate>
                                <asp:Label ID="lblFromDay" runat="server" Text='<%# Eval("FromDay") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField> 
                        <asp:TemplateField ItemStyle-Width="10%">
                            <ItemTemplate>
                                <asp:Label ID="lblToDay" runat="server" Text='<%# Eval("ToDay") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField> 
                        <asp:TemplateField ItemStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="lblCommissionPercentage" runat="server" Text='<%# Eval("CommissionPercentage") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>  
                        <asp:TemplateField ItemStyle-Width="15%">
                            <ItemTemplate>
                                 <asp:ImageButton ID="imgDeleteBtn" CommandName="deleteItem" CommandArgument='<%# Eval("CommPernId") %>'
                                ImageUrl="../images/delete-ol-btn.png" runat="server" OnClientClick="return ConfirmOnDelete();"  OnClick="imgDeleteBtn_Click"
                                ToolTip="Delete" />                              
                            </ItemTemplate>
                        </asp:TemplateField>  
                                 
                    </Columns>       
                    <EmptyDataTemplate>
                        <table>
                            <tr>
                                <td colspan="3" align="center">
                                    <b>No record found !</b>
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>   
                    <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                    <PagerStyle HorizontalAlign="Right" BackColor="White" CssClass="displayNone" />
                    <PagerTemplate>
                        <div id="Paging" style="width: 100%; display: none;">
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                        </div>
                    </PagerTemplate>          
                </asp:GridView>
</tbody></table>
          </div>
          <script language="javascript" type="text/javascript">
              function ConfirmOnDelete() {
                  return confirm("Are you sure you want to delete it?");
              }
              jQuery(document).ready(function () {
                  if (jQuery("#Paging") != undefined) {
                      var inner = jQuery("#Paging").html();
                      jQuery("#pageing").html(inner);
                      jQuery("#Paging").html("");
                  }

              });

              jQuery(document).ready(function () {

                  jQuery("#<%= btnSubmit.ClientID %>").bind("click", function () {
                      var isvalid = true;
                      var errormessage = "";
                      if (jQuery("#<%= divmessage.ClientID %>").hasClass("succesfuly")) {
                          jQuery("#<%= divmessage.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                      }

                      var drpCountry = jQuery("#<%= drpCountry.ClientID %>").val();
                      if (drpCountry == "") {
                          if (errormessage.length > 0) {
                              errormessage += "<br/>";
                          }
                          errormessage += "Please select country name.";
                          isvalid = false;
                      }

                      var txtProfileName = jQuery("#<%= txtProfileName.ClientID %>").val();
                      if (txtProfileName.length <= 0) {
                          if (errormessage.length > 0) {
                              errormessage += "<br/>";
                          }
                          errormessage += "Profile name is required.";
                          isvalid = false;
                      }
                      var chkHotelName = jQuery("#<%= chkHotelName.ClientID %>").find("input:checkbox:checked").length;
                      if (chkHotelName == 0) {
                          if (errormessage.length > 0) {
                              errormessage += "<br/>";
                          }
                          errormessage += "Please select atleast one hotel. ";
                          isvalid = false;
                      }
                      var chkcomm = "";
                      jQuery("#<%= grvCommission.ClientID %>").find("tr").each(function () {
                          jQuery(this).find("td:last").each(function () {
                              var myLength = jQuery(this).find("input:text").val().length;
                              if (myLength <= 0) {                                  
                                  chkcomm = "1";
                              }
                          });
                      });
                      if (chkcomm == "1") {
                          if (errormessage.length > 0) {
                              errormessage += "<br/>";
                          }
                          errormessage += "Please enter commission percentage ";
                          isvalid = false;
                      }
                      //                      alert(myLength);


                      if (!isvalid) {
                          jQuery("#<%= divmessage.ClientID %>").show();
                          jQuery("#<%= divmessage.ClientID %>").html(errormessage);
                          var offseterror = jQuery("#<%= divmessage.ClientID %>").offset();
                          jQuery("body").scrollTop(offseterror.top);
                          jQuery("html").scrollTop(offseterror.top);
                          return false;
                      }
                      jQuery("#<%= divmessage.ClientID %>").html("");
                      jQuery("#<%= divmessage.ClientID %>").hide();
                      jQuery("body").scrollTop(0);
                      jQuery("html").scrollTop(0);
                      document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                  });
              });             
    </script>
</asp:Content>

