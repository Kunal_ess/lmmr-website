﻿<%@ Page Title="Staff Accounts" Language="C#" MasterPageFile="~/SuperAdmin/Main.master" AutoEventWireup="true" CodeFile="Staffadmin.aspx.cs" Inherits="SuperAdmin_Staffadmin" %>
<%@ Register Src="~/UserControl/Operator/SearchPanel.ascx" TagName="SearchPanel" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc1:SearchPanel ID="SearchPanel1" runat="server" />
        <div id="divmessage" runat="server">
           </div>
        <div class="operator-mainbody">
                <div class="contract-list" id="divAlphabeticPaging" runat="server" style="margin-top: 20px;">
                <div class="contract-list-left" style="width: 278px;">
                    <h2>
                        Staff Account</h2>
                </div>
                <div class="contract-list-right" style="width: 676px;">
                    <ul runat="server" id="AlphaList">
                        <li id="Li1" runat="server"><a href="#" class="select" runat="server" id="all" onserverclick="PageChange">all</a></li>
                        <li id="Li2" runat="server"><a href="#" runat="server" id="a" onserverclick="PageChange">a</a></li>
                        <li id="Li3" runat="server"><a href="#" runat="server" id="b" onserverclick="PageChange">b</a></li>
                        <li id="Li4" runat="server"><a href="#" runat="server" id="c" onserverclick="PageChange">c</a></li>
                        <li id="Li5" runat="server"><a href="#" runat="server" id="d" onserverclick="PageChange">d</a></li>
                        <li id="Li6" runat="server"><a href="#" runat="server" id="e" onserverclick="PageChange">e</a></li>
                        <li id="Li7" runat="server"><a href="#" runat="server" id="f" onserverclick="PageChange">f</a></li>
                        <li id="Li8" runat="server"><a href="#" runat="server" id="g" onserverclick="PageChange">g</a></li>
                        <li id="Li9" runat="server"><a href="#" runat="server" id="h" onserverclick="PageChange">h</a></li>
                        <li id="Li10" runat="server"><a href="#" runat="server" id="i" onserverclick="PageChange">i</a></li>
                        <li id="Li11" runat="server"><a href="#" runat="server" id="j" onserverclick="PageChange">j</a></li>
                        <li id="Li12" runat="server"><a href="#" runat="server" id="k" onserverclick="PageChange">k</a></li>
                        <li id="Li13" runat="server"><a href="#" runat="server" id="l" onserverclick="PageChange">l</a></li>
                        <li id="Li14" runat="server"><a href="#" runat="server" id="m" onserverclick="PageChange">m</a></li>
                        <li id="Li15" runat="server"><a href="#" runat="server" id="n" onserverclick="PageChange">n</a></li>
                        <li id="Li16" runat="server"><a href="#" runat="server" id="o" onserverclick="PageChange">o</a></li>
                        <li id="Li17" runat="server"><a href="#" runat="server" id="p" onserverclick="PageChange">p</a></li>
                        <li id="Li18" runat="server"><a href="#" runat="server" id="q" onserverclick="PageChange">q</a></li>
                        <li id="Li19" runat="server"><a href="#" runat="server" id="r" onserverclick="PageChange">r</a></li>
                        <li id="Li20" runat="server"><a href="#" runat="server" id="s" onserverclick="PageChange">s</a></li>
                        <li id="Li21" runat="server"><a href="#" runat="server" id="t" onserverclick="PageChange">t</a></li>
                        <li id="Li22" runat="server"><a href="#" runat="server" id="u" onserverclick="PageChange">u</a></li>
                        <li id="Li23" runat="server"><a href="#" runat="server" id="v" onserverclick="PageChange">v</a></li>
                        <li id="Li24" runat="server"><a href="#" runat="server" id="w" onserverclick="PageChange">w</a></li>
                        <li id="Li25" runat="server"><a href="#" runat="server" id="x" onserverclick="PageChange">x</a></li>
                        <li id="Li26" runat="server"><a href="#" runat="server" id="y" onserverclick="PageChange">y</a></li>
                        <li id="Li27" runat="server"><a href="#" runat="server" id="z" onserverclick="PageChange">z</a></li>
                    </ul>
                </div>
            </div>
                
                <div class="pageing-operator">
                    <div class="pageing-operator-new">
  
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>   
                    <td align="left" valign="bottom">
                <div style="float: left; margin-left: 20px;" class="n-commisions">
                    <div class="n-btn">
                        <asp:LinkButton ID="lnkAddNew" runat="server" onclick="lnkAddNew_Click"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Add New Staff </div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                    </div>
                </div>
                <asp:Panel ID="pnlActivate" runat="server">       
                <div style="float: left; margin-left: 10px;" class="n-commisions">
                    <div class="n-btn">
                        <asp:LinkButton ID="lnkmodify" runat="server"  
                        OnClientClick="javascript:return TestCheckBox1();" onclick="lnkmodify_Click"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Modify</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                    </div>
                </div>
                
                <div style="float: left; margin-left: 10px;" class="n-commisions">
                    <div class="n-btn">
                        <asp:LinkButton ID="lnkdelete" runat="server" 
                        onclick="lnkdelete_Click" OnClientClick="return TestCheckBox();"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Delete</div>
                            <div class="n-btn-right">
                            </div></asp:LinkButton>
                    </div>
                </div>
                </asp:Panel>
            </td>
             <td align="right">
                        Total Results :
                        <asp:Label ID="lblTotalBooking"  runat="server" Text=""></asp:Label>
                    </td>                                                                  
                </tr>
            </table>
        </div>
                    <div style="float: right;" id="pageing">
                    </div>
                </div>
                <!-- end contract-list-->
                <div id="divGrid">
                <asp:GridView ID="grdstaff" runat="server" Width="100%" AutoGenerateColumns="False"
                    RowStyle-HorizontalAlign="Center" RowStyle-VerticalAlign="Middle" CellPadding="0"
                    CellSpacing="1" EmptyDataText="No record Found!" EmptyDataRowStyle-Font-Bold="true"
                    EmptyDataRowStyle-HorizontalAlign="Center" 
                    EditRowStyle-VerticalAlign="Top" AllowPaging="true" RowStyle-CssClass="con" AlternatingRowStyle-CssClass="con-dark"             
                    GridLines="None" PageSize="10" CssClass="cofig" BackColor="#ffffff" ShowHeader="true"
                    ShowHeaderWhenEmpty="true" 
                    onpageindexchanging="grdstaff_PageIndexChanging" 
                        onrowdatabound="grdstaff_RowDataBound">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderStyle CssClass="heading-earned-row" HorizontalAlign="Center" />
                            
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="chkField" onclick="javascript:CheckOtherIsCheckedByGVID(this);"  />
                                <asp:HiddenField ID="hdfImage" runat="server" Value='<%# Eval("UserId") %>' />
                                    

                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                 <asp:CheckBox runat="server" ID="chkField" onclick="javascript:CheckOtherIsCheckedByGVID(this);"  />
                                <asp:HiddenField ID="hdfImage" runat="server" Value='<%# Eval("UserId") %>' />
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderStyle CssClass="heading-earned-row" HorizontalAlign="Center" />
                            
                            <HeaderTemplate>
                                Staff Name
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblRefNo" runat="server"  Text='<%# Eval("FirstName") %>'></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblRefNo" runat="server"  Text='<%# Eval("Firstname") %>'></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderStyle CssClass="heading-earned-row" HorizontalAlign="Center" />
                            
                            <HeaderTemplate>
                                Email
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblEmailId" runat="server" Text='<%# Eval("EmailId") %>'></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblEmailId" runat="server" Text='<%# Eval("EmailId") %>'></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderStyle CssClass="heading-earned-row" HorizontalAlign="Center" />
                            
                            <HeaderTemplate>
                                Role
                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblrole" runat="server" Text='<%# Eval("Usertype") %>'></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblrole" runat="server" Text='<%# Eval("Usertype") %>'></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderStyle CssClass="heading-earned-row" HorizontalAlign="Center" />
                            
                            <HeaderTemplate>
                                Creation Date
                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblcreationdate" runat="server" Text='<%# String.Format("{0:dd/MM/yyyy}",Eval("CreatedDate"))%>'></asp:Label>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:Label ID="lblcreationdate" runat="server" Text='<%# String.Format("{0:dd/MM/yyyy}",Eval("CreatedDate"))%>'></asp:Label>
                            </AlternatingItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle VerticalAlign="Top"></EditRowStyle>
                    <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="True"></EmptyDataRowStyle>
                    <EmptyDataTemplate>
                        <table>
                            <tr>
                                <td colspan="3" align="center">
                                    <b>No record found !</b>
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                    <PagerStyle HorizontalAlign="Right" BackColor="White" CssClass="displayNone" />
                    <PagerTemplate>
                        <div id="Paging" style="width: 100%; display: none;">
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                        </div>
                    </PagerTemplate>
                    <RowStyle HorizontalAlign="Center" VerticalAlign="Middle"></RowStyle>
                </asp:GridView>
            </div>
            
            <div class="superadmin-cms-box1" style="width: 100%; min-height: 100px; margin-top: 20px;
                            border-right: 1px solid #92BEDE;" id="DivAddStaff" runat="server" visible="false">
            
            <div id="divvalidstf" runat="server">
                    </div>
                    <div class="superadmin-mainbody-sub1" style="padding-left:0px;">
                        <div class="superadmin-mainbody-sub1-left" style="width: 100%;">
                            <h2>
                                <asp:Label ID="lblHeader" runat="server"></asp:Label></h2>
                        </div>
                    </div>
                    <table cellspacing="1" style="margin-left:-1px;" width="963px" bgcolor="#92bddd">
                        <tr bgcolor="#E3F0F1">
                            <td width="15%" style="line-height:31px;padding-left:5px;">
                                <strong>Staff Name:</strong>
                            </td>
                            <td>
                                <asp:TextBox ID="txtstaffname" runat="server" MaxLength="100" TabIndex="1" Width="200px" />
                            </td>
                        </tr>
                        <tr bgcolor="#F1F8F8" >
                            <td width="15%" style="line-height:31px;padding-left:5px;">
                                <strong>Email Id:</strong>
                            </td>
                            <td>
                                <asp:TextBox ID="txtEmail" runat="server" MaxLength="100" TabIndex="1" Width="200px" />
                            </td>
                        </tr>
                        <div id="passDIV" runat="server">
                        <tr bgcolor="#E3F0F1" >
                            <td width="15%" style="line-height:31px;padding-left:5px;">
                                <strong>Password</strong>
                            </td>
                            <td>
                                <asp:TextBox ID="txtpassword" runat="server" MaxLength="100" TextMode="Password" TabIndex="1" Width="200px" />
                            </td>
                        </tr>
                        <tr bgcolor="#F1F8F8" >
                            <td width="15%" style="line-height:31px;padding-left:5px;">
                                <strong>Confirm Password</strong>
                            </td>
                            <td>
                                <asp:TextBox ID="txtpasswordconfirm" runat="server" MaxLength="100" TextMode="Password" TabIndex="1" Width="200px" />
                            </td>
                        </tr>
                        </div>
                        <tr bgcolor="#E3F0F1" >
                            <td width="15%" style="line-height:31px;padding-left:5px;">
                                <strong>Role</strong>
                            </td>
                            <td>
                                <asp:DropDownList ID="drprole" runat="server" TabIndex="1" Width="200px">
                                           
                                            </asp:DropDownList>
                            </td>
                        </tr>
                        
                        <tr bgcolor="#FFFFFF">
                            <td colspan="2" style="line-height: 41px; padding-top: 12px;">
                                <div class="booking-details" style="width: 760px;">
                                    <ul>
                                        <li class="value10">
                                            <div class="col21" style="width: 955px;">
                                                <div class="button_section">
                                                    <asp:LinkButton ID="registerBtn" runat="server" Text="Save" CssClass="select" OnClick="registerBtn_Click" />
                                                    <span>or</span>
                                                    <asp:LinkButton ID="btnRegisterCancel" runat="server" Text="Cancel" OnClick="cancelBtn_Click" />
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                </div>
    <script language="javascript" type="text/javascript">
     function SetFocusBottom(val) {
              //alert("1");
            var ofset = jQuery("#" + val).offset();
            jQuery('body').scrollTop(ofset.top);
            jQuery('html').scrollTop(ofset.top);
        }
                        jQuery(document).ready(function () {
                            <% if (ViewState["SearchAlpha"] != null) {%>
                            //alert("2");
                                jQuery('#<%= AlphaList.ClientID %> li a').removeClass('select');

                                jQuery('#ContentPlaceHolder1_<%= ViewState["SearchAlpha"]%>').addClass('select');
                            <% }%>
                           
                        });



    </script>
    <script language="javascript" type="text/javascript">
        function CheckOtherIsCheckedByGVID(spanChk) {
            var IsChecked = spanChk.checked;
            var CurrentRdbID = spanChk.id;
            var Chk = spanChk;
            Parent = document.getElementById('ContentPlaceHolder1_grdstaff');
            var items = Parent.getElementsByTagName('input');

            for (i = 0; i < items.length; i++) {

                if (items[i].id != CurrentRdbID && items[i].type == "checkbox") {

                    if (items[i].checked) {

                        items[i].checked = false;
                    }
                }
            }
        }
                

                
    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            if ('<%= Session["task"] %>' == "register") {
                jQuery("#<%= registerBtn.ClientID %>").bind("click", function () {
                    if (jQuery("#<%= divvalidstf.ClientID %>").hasClass("succesfuly")) {
                        jQuery("#<%= divvalidstf.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                    }
                    var isvalid = true;
                    var errormessage = "";
                    var Staffname = jQuery("#<%= txtstaffname.ClientID %>").val();
                    if (Staffname.length <= 0) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>"
                        }
                        errormessage += 'Enter The Staff Name.';
                        isvalid = false;
                    }
                    var email = jQuery("#<%= txtEmail.ClientID %>").val();
                    if (email.length <= 0) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>"
                        }
                        errormessage += 'Enter The Email.';
                        isvalid = false;
                    }
                    else {
                        if (!validateEmail(email)) {
                            if (errormessage.length > 0) {
                                errormessage += "<br/>";
                            }
                            errormessage += 'Enter valid email';
                            isvalid = false;
                        }
                    }
                    var password = jQuery("#<%= txtpassword.ClientID %>").val();
                    if (password.length <= 0) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>"
                        }
                        errormessage += 'Enter your password.';
                        isvalid = false;
                    }
                    else {
                        if (password.length < 6) {
                            if (errormessage.length > 0) {
                                errormessage += "<br/>";
                            }
                            errormessage += 'Password should have minimum six characters.';
                            isvalid = false;
                        }

                    }

                    var Confirmpass = jQuery("#<%= txtpasswordconfirm.ClientID %>").val();
                    if (Confirmpass.length <= 0) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>"
                        }
                        errormessage += 'Enter confirm password.';
                        isvalid = false;
                    }
                    if ((password != "") && (Confirmpass != "")) {
                        if (password != Confirmpass) {
                            if (errormessage.length > 0) {
                                errormessage += "<br/>";
                            }
                            errormessage += 'Password do not match.';
                            isvalid = false;
                        }
                    }

                    var dropRole = jQuery("#<%= drprole.ClientID %>").val();
                    if (dropRole == '--Select a Role--' || dropRole == "0") {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += 'Select a Role.';
                        isvalid = false;
                    }

                    if (!isvalid) {
                        jQuery("#<%= divvalidstf.ClientID %>").show();
                        jQuery("#<%= divvalidstf.ClientID %>").html(errormessage);
                        var offseterror = jQuery("#<%= divvalidstf.ClientID %>").offset();
                        jQuery("body").scrollTop(offseterror.top);
                        jQuery("html").scrollTop(offseterror.top);
                        return false;
                    }
                    jQuery("#<%= divvalidstf.ClientID %>").html("");
                    jQuery("#<%= divvalidstf.ClientID %>").hide();
                    jQuery("#<%= divvalidstf.ClientID %>").removeClass('error');
                    jQuery("body").scrollTop(0);
                    jQuery("html").scrollTop(0);
                    document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                });
            }
            else if ('<%= Session["task"] %>' == "update") {
                jQuery("#<%= registerBtn.ClientID %>").bind("click", function () {
                    if (jQuery("#<%= divvalidstf.ClientID %>").hasClass("succesfuly")) {
                        jQuery("#<%= divvalidstf.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                    }
                    var isvalid = true;
                    var errormessage = "";
                    var Staffname = jQuery("#<%= txtstaffname.ClientID %>").val();
                    if (Staffname.length <= 0) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>"
                        }
                        errormessage += 'Enter The Staff Name.';
                        isvalid = false;
                    }
                    var email = jQuery("#<%= txtEmail.ClientID %>").val();
                    if (email.length <= 0) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>"
                        }
                        errormessage += 'Enter The Email.';
                        isvalid = false;
                    }
                    else {
                        if (!validateEmail(email)) {
                            if (errormessage.length > 0) {
                                errormessage += "<br/>";
                            }
                            errormessage += 'Enter valid email.';
                            isvalid = false;
                        }
                    }
                    var dropRole = jQuery("#<%= drprole.ClientID %>").val();
                    if (dropRole == '--Select a Role--' || dropRole == "0") {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += 'Select a Role.';
                        isvalid = false;
                    }

                    if (!isvalid) {
                        jQuery("#<%= divvalidstf.ClientID %>").show();
                        jQuery("#<%= divvalidstf.ClientID %>").html(errormessage);
                        var offseterror = jQuery("#<%= divvalidstf.ClientID %>").offset();
                        jQuery("body").scrollTop(offseterror.top);
                        jQuery("html").scrollTop(offseterror.top);
                        return false;
                    }
                    jQuery("#<%= divvalidstf.ClientID %>").html("");
                    jQuery("#<%= divvalidstf.ClientID %>").hide();
                    jQuery("#<%= divvalidstf.ClientID %>").removeClass('error');
                    jQuery("body").scrollTop(0);
                    jQuery("html").scrollTop(0);
                    document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                });
            }
        });
        function validateEmail(email) {
            var a = email;
            var filter = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
            if (filter.test(a)) {
                return true;
            }
            else {
                return false;
            }

        }
    </script>
    <script language="javascript" type="text/javascript">

        jQuery(document).ready(function () {
            if (jQuery("#Paging") != undefined) {
                var inner = jQuery("#Paging").html();
                jQuery("#pageing").html(inner);
                jQuery("#Paging").html("");
            }

        });

        jQuery(document).ready(function () {
            if (jQuery("#PagingRequest") != undefined) {
                var inner = jQuery("#PagingRequest").html();
                jQuery("#PageingRequest").html(inner);
                jQuery("#PagingRequest").html("");
            }

        });
        function ConfirmOnDelete() {
            return confirm("Are you sure you want to delete it?");
        }

       
        function TestCheckBox() {
            if (jQuery("#ContentPlaceHolder1_grdstaff").find("input:checkbox:checked").length > 0) {
                
            }
            else {
                alert("Please select at least one staff name");
                return false;
            }


            return confirm('Are you sure you want to delete this staff?');
                        
           
            
        }
        function TestCheckBox1() {
            if (jQuery("#ContentPlaceHolder1_grdstaff").find("input:checkbox:checked").length > 0) {

            }
            else {
                alert("Please select at least one staff name");
                return false;
            }

        }

        
                       
    </script>
</asp:Content>

