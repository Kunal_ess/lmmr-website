﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Entities;
using LMMR.Data;
using System.Data;
using System.IO;
using System.Configuration;
using LMMR.Business;
using System.Web.UI.HtmlControls;

public partial class SuperAdmin_Log : System.Web.UI.Page
{
    LogBase log = new LogBase();
    ClientContract objClient = new ClientContract();
    HotelManager objhm = new HotelManager();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillCountryForSearch();
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
        }
    }


    /// <summary>
    /// Bind all the country Table in dropdown.
    /// </summary>
    protected void FillCountryForSearch()
    {
        //get list of countries.
        TList<Country> objCountry = objClient.GetByAllCountry();
        if (objCountry.Count > 0)
        {
            drpSearchCountry.DataValueField = "Id";
            drpSearchCountry.DataTextField = "CountryName";
            drpSearchCountry.DataSource = objCountry.OrderBy(a => a.CountryName);
            drpSearchCountry.DataBind();
            drpSearchCountry.Items.Insert(0, new ListItem("--Select country--", ""));
            drpHotelBycountry.Items.Insert(0, new ListItem("--Select Facility Name--", "0"));

        }
        else
        {
            drpSearchCountry.Items.Insert(0, new ListItem("--Select country--", ""));
            drpHotelBycountry.Items.Insert(0, new ListItem("--Select Facility Name--", "0"));
        }

    }

    #region PageAlpha
    public void PageChange(object sender, EventArgs e)
    {
        try
        {
            HtmlAnchor anc = (sender as HtmlAnchor);
            anc.Style.Add("class", "select");
            var alpha = anc.ID;
            string get = alpha.Trim();
            //ViewState["SearchAlpha"] = get;
            string HotelId = drpHotelBycountry.SelectedValue.ToString();

            DateTime fromdate = new DateTime(Convert.ToInt32("20" + txtFromdate.Text.Split('/')[2]), Convert.ToInt32(txtFromdate.Text.Split('/')[1]), Convert.ToInt32(txtFromdate.Text.Split('/')[0]));
            DateTime todate = new DateTime( Convert.ToInt32("20" + txtTodate.Text.Split('/')[2]), Convert.ToInt32(txtTodate.Text.Split('/')[1]), Convert.ToInt32(txtTodate.Text.Split('/')[0]));

            DataTable dt = new DataTable();
                if (get == "Booking")
                {
                    dt = log.Booking(HotelId, "0", fromdate, todate);
                }
                else if (get == "Booking1")
                {
                    dt = log.Booking(HotelId, "1", fromdate, todate);
                }
                else
                {
                     dt = log.GetTable(get, HotelId,fromdate,todate);
                }
                //if (dt.Rows.Count > 0)
                //{
                grvDetails.DataSource = dt;
                grvDetails.DataBind();
                lblHeading.Text = "Log For :" + anc.InnerText;
                trfilldetails.Visible = true;
                //}
                //else
                //{
                //   grvDetails.DataSource = "";
                    
                //   // grvDetails.DataBind();
                // grvDetails.EmptyDataText = "No Records";
                //    lblHeading.Text = "Log For :" + anc.InnerText;
                //    trfilldetails.Visible = true;
                //}
        }
        catch (Exception ex)
        {
            //logger.Error(ex);
        }
    }
    #endregion
    protected void lbtSearch_Click(object sender, EventArgs e)
    {
        trfilldetails.Visible = false;
        trfillMenu.Visible = true;
        lblHeading.Text = "Kindly select a menu item to view the log for the same";
        if (Request.QueryString.Get("Querystring") != null)
        {
            if (Request.QueryString.Get("Querystring") == "ClientContract")
            {
                Client.Visible = true;
            }
        }        

    }
    protected void lnkClear_Click(object sender, EventArgs e)
    {
        trfilldetails.Visible = false;
        trfillMenu.Visible = false;
        lblHeading.Text = "Note : Kindly select the country and Hotel first";
        drpSearchCountry.SelectedIndex = 0;
        drpHotelBycountry.SelectedIndex = 0;
        drpHotelBycountry.Items.Clear();
        drpHotelBycountry.Items.Insert(0, new ListItem("--Select Facility Name--", "0"));
        txtFromdate.Text = "dd/MM/yy";
        txtTodate.Text = "dd/MM/yy";
    }
    protected void drpSearchCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillHotelFroSearch(Convert.ToInt32(drpSearchCountry.SelectedValue));
    }
    protected void FillHotelFroSearch(int intcountryID)
    {
        if (intcountryID > 0)
        {
            drpHotelBycountry.DataValueField = "Id";
            drpHotelBycountry.DataTextField = "Name";
          
             TList<Hotel> tlisthotel   = objhm.GetHotelByCountryAndCity(Convert.ToInt64(drpSearchCountry.SelectedValue),0);
             drpHotelBycountry.DataSource= tlisthotel.OrderBy(a => a.Name);
            drpHotelBycountry.DataBind();
            drpHotelBycountry.Items.Insert(0, new ListItem("--Select Facility Name--", ""));
            drpHotelBycountry.Enabled = true;


        }
        else
        {
            drpHotelBycountry.Items.Clear();
            drpHotelBycountry.Items.Insert(0, new ListItem("--Select Facility Name--", ""));
            drpHotelBycountry.Enabled = false;
           
        }
    }
}