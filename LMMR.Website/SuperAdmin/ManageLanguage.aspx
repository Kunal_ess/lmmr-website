﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuperAdmin/Main.master" AutoEventWireup="true"
    CodeFile="ManageLanguage.aspx.cs" Inherits="SuperAdmin_ManageLanguage" ValidateRequest="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .command
        {
            display: none;
        }
    </style>
    <div class="cancelation-heading-body3" id="HotelAssignTab" runat="server">
        <ul>
            <li><a id="liTimeFrame" href="ManageCountryOthers.aspx">Country</a></li>
            <li><a id="liApplication" href="ApplicationOthers.aspx">Application</a></li>
            <li><a id="liManageLanaguage" class="select" href="ManageLanguage.aspx">Manage Language</a></li>
            <li><a id="liSetinvoice" href="SetInvoice.aspx">Set Invoice</a></li>
        </ul>
    </div>
    <%--<div id="TabbedPanels2" class="TabbedPanels" style="border-bottom:1px solid #92BEDE;" >
        <ul class="TabbedPanelsTabGroup">
            <li class="TabbedPanelsTab TabbedPanelsTabSelected" tabindex="0">Language</li>
            <li class="TabbedPanelsTab" tabindex="0">Country</li>
        </ul>
    </div>--%>
    <div style="float: left;">
        <div class="superadmin-example-layout">
            <div id="divMainMessage" runat="server">
            </div>
            <div class="superadmin-cms">
                <div class="superadmin-cms-box1" style="width: 100%; border-right: 1px solid #92BEDE;margin-bottom:10px;background-color:#FFFFFF;padding-bottom:0px;">
                <div style="border-bottom: #A3D4F7 solid 1px;border-top: #A3D4F7 solid 1px;padding: 0px 0px 0px 0px;"><h3 style="background-color:#FFFFFF;height:31px;border:none;padding:0px 0px 0px 0px; font-size: 21px;font-weight: bold;margin-left: 10px;" >
                        Manage Language</h3></div>
                    <br /><br />
                    <div style="float: left; margin-right: 10px; margin-top: -22px;">
                        <div class="n-commisions" style="float: left; margin-left: 10px;">
                                        <div class="n-btn">
                                            <asp:LinkButton ID="lnkAddNew" runat="server" OnClick="lnkAddNew_Click">
                                    <div class="n-btn-left">
                                    </div>
                                    <div class="n-btn-mid">
                                        Add New</div>
                                    <div class="n-btn-right">
                                    </div>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                        </div><br />
                        <div style="background-color:#ECF7FE;float:left;width:100%;padding-bottom:10px;border-top: #A3D4F7 solid 1px;" >
                    <ul>
                        <asp:Repeater ID="rptLanguage" runat="server" OnItemCommand="rptLanguage_ItemCommand"
                            OnItemDataBound="rptLanguage_ItemDataBound">
                            <ItemTemplate>
                                <li>
                                    <div style="float: left; margin-left: 5px;width:70%;">
                                        <asp:Label ID="lblLanguageName" runat="server">English</asp:Label>
                                        (<asp:Label ID="lblShortName" runat="server">En</asp:Label>)</div>
                                    <div style="float: right; margin-right: 5px;" class="command">
                                        <asp:LinkButton ID="lnkModify" runat="server" CommandName="modifyme"><img src="../Images/modify.png" alt="modify" height="15px" width="15px" /></asp:LinkButton>
                                        <asp:LinkButton
                                            ID="lnkDelete" Enabled="false" Visible="false" runat="server" CommandName="deleteme" OnClientClick="return confirm('Do you really want to delete this language?');"><img height="15px" width="15px" src="../Images/delete-ol-btn.png" alt="delete" /></asp:LinkButton>
                                            </div>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul></div>
                </div>
                <br />
                <br />
                <div class="superadmin-example-mainbody" id="divAddLanguage" runat="server" style="">
                    <div id="divMessage" runat="server">
                    </div>
                    <div class="superadmin-mainbody-sub1" style="padding-left:0px;">
                        <div class="superadmin-mainbody-sub1-left" style="width: 100%;">
                            <h2>
                                <asp:Label ID="lblHeader" runat="server">Add Language</asp:Label></h2>
                        </div>
                    </div>
                    <asp:HiddenField ID="hdnId" runat="server" />
                    <table cellspacing="1" style="margin-left:-1px;" width="963px" bgcolor="#92bddd">
                        <tr bgcolor="#E3F0F1">
                            <td width="15%" style="line-height:31px;padding-left:5px;">
                                <strong>Language:</strong>
                            </td>
                            <td>
                                <asp:TextBox ID="txtLanguage" runat="server" MaxLength="50" TabIndex="1" Width="200px" CssClass="txtpadding" />
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtLanguage"
                            FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" ValidChars="-_" runat="server">
                        </asp:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr bgcolor="#F1F8F8" >
                            <td width="15%" style="line-height:31px;padding-left:5px;">
                                <strong>Short Name:</strong>
                            </td>
                            <td>
                                <asp:TextBox ID="txtShortName" runat="server" MaxLength="10" TabIndex="1" Width="200px" CssClass="txtpadding" />
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="txtShortName"
                            FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" ValidChars="-_" runat="server">
                        </asp:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr id="trIsActive" runat="server" bgcolor="#E3F0F1">
                            <td width="15%" style="line-height:31px;padding-left:5px;">
                                <strong>Is Active:</strong>
                            </td>
                            <td>
                                <asp:DropDownList ID="drpIsActive" runat="server" CssClass="txtpadding">
                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td colspan="2" style="line-height:41px;padding-top:12px;" >
                                <div style="float: left;width:100%;margin-left:33%">
                                    <div class="n-commisions" style="float: left; margin-left: 20px;">
                                        <div class="n-btn">
                                            <asp:LinkButton ID="lnkSave" runat="server" OnClick="lnkSave_Click" OnClientClick="return SaveLanguage();">
                                    <div class="n-btn-left">
                                    </div>
                                    <div class="n-btn-mid">
                                        Save</div>
                                    <div class="n-btn-right">
                                    </div>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                    &nbsp;<div style="margin: -5px 0px 0px 10px;float:left;" ><b>or</b></div>&nbsp;
                                    <div class="n-commisions" style="float: left; margin-left: 10px;">
                                        <div class="n-btn">
                                            <asp:LinkButton ID="lnkCancel" runat="server" OnClick="lnkCancel_Click">
                                    <div class="n-btn-left">
                                    </div>
                                    <div class="n-btn-mid">
                                        Cancel</div>
                                    <div class="n-btn-right">
                                    </div>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="n-commisions" style="float: left; margin-left: 10px;" id="lnkEDitSC"
                                        runat="server">
                                        <div class="n-btn">
                                            <asp:LinkButton ID="lnkEditStaticContents" runat="server" OnClick="lnkEditStaticContents_Click">
                                    <div class="n-btn-left">
                                    </div>
                                    <div class="n-btn-mid">
                                        Edit Static Contents</div>
                                    <div class="n-btn-right">
                                    </div>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                </div>
                <div class="superadmin-example-mainbody" style="width: 100%; min-height: 100px;padding-bottom:10px; margin-top: 20px;border-bottom:1px solid #A3D4F7;border-right: 1px solid #92BEDE;" id="divXML" runat="server">
                    <div class="superadmin-mainbody-sub1" style="padding-left:0px;">
                        <div class="superadmin-mainbody-sub1-left" style="width: 100%;border-bottom:1px solid #A3D4F7;">
                            <h2>
                                <asp:Label ID="lblHaderXml" runat="server"></asp:Label></h2>
                        </div>
                    </div>
                    <div style="float: left;border-bottom:1px solid #A3D4F7;width:100%;">
                        <div id="erroronadd" style="width: 100%; float: left;">
                        </div>
                        <table width="100%" cellpadding="5" cellspacing="5">
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rbtnTranslate" runat="server" Text="Use translated copy." GroupName="Copy"
                                        Checked="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rbtnUseEnglish" runat="server" Text="Use english copy." GroupName="Copy" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rbtnUseBlankEnglishCopy" runat="server" Text="Use english copy on blank spaces only."
                                        GroupName="Copy" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="height: 500px; overflow-x: hidden; overflow-y: scroll; float: left;border-bottom:1px solid #A3D4F7;width: 960px;">
                        <table cellpadding="1" cellspacing="1" width="100%" id="tblTesult" bgcolor="#ECF7FE">
                            <asp:Repeater ID="rptXmlResult" runat="server" OnItemDataBound="rptXmlResult_ItemDataBound">
                                <HeaderTemplate>
                                    <tr bgcolor="#C3D6E2" style="line-height:21px;" >
                                        <th>
                                            English
                                        </th>
                                        <th>
                                            Translate
                                        </th>
                                    </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr style="border:1px solid #92bddd;">
                                        <td width="35%">
                                            <asp:Label ID="lblString" runat="server"></asp:Label><asp:HiddenField ID="hdnKey"
                                                runat="server" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtString" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <tr bgcolor="#D9EEFC" style="border:1px solid #92bddd;" >
                                        <td width="35%">
                                            <asp:Label ID="lblString" runat="server"></asp:Label><asp:HiddenField ID="hdnKey"
                                                runat="server" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtString" runat="server" TextMode="MultiLine" Width="100%" Height="100%"></asp:TextBox>
                                        </td>
                                    </tr>
                                </AlternatingItemTemplate>
                            </asp:Repeater>
                        </table>
                    </div>
                    <div >
                        <div class="button_section" style="padding-top:10px;" >
                        <asp:LinkButton ID="lnkSaveXML" runat="server" OnClick="lnkSaveXML_Click" OnClientClick="return CheckFieldEmpty();" CssClass="select">Save</asp:LinkButton>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script language="javascript" type="text/javascript">
        var langid = 0;
        jQuery(document).ready(function () {
            jQuery("li").mouseover(function () { jQuery(this).css({ "background-color": "#FFFFFF" }); jQuery(this).find(".command").show() });
            jQuery("li").mouseout(function () { jQuery(this).css({ "background-color": "" }); jQuery(this).find(".command").hide() });
        });

        function SaveLanguage() {
            var errorMessage = "";
            var isValid = true;
            var languageName = jQuery("#<%= txtLanguage.ClientID %>").val();
            if (languageName.length <= 0) {
                if (errorMessage.length > 0) {
                    errorMessage += "<br/>";
                }
                errorMessage += "Language is required.";
                isValid = false;
            }

            var languageshortName = jQuery("#<%= txtShortName.ClientID %>").val();
            if (languageshortName.length <= 0) {
                if (errorMessage.length > 0) {
                    errorMessage += "<br/>";
                }
                errorMessage += "Short name is required.";
                isValid = false;
            }
            if (!isValid) {
                jQuery("#<%= divMessage.ClientID %>").html(errorMessage);
                jQuery("#<%= divMessage.ClientID %>").addClass('error');
                return false;
            }
            else {
                jQuery("#<%= divMessage.ClientID %>").html('');
                jQuery("#<%= divMessage.ClientID %>").removeClass('error');
                return true;
            }
        }

        function CheckFieldEmpty() {
            var emptyTextBox = 0;
            jQuery("#tblTesult textarea").each(function () {
                var strResult = jQuery(this).val().trim();
                if (strResult.length <= 0) {
                    emptyTextBox++;
                }
            });
            if (emptyTextBox > 0 && jQuery("#<%= rbtnTranslate.ClientID %>").is(":checked")) {
                jQuery("#erroronadd").addClass("error");
                jQuery("#erroronadd").show();
                jQuery("#erroronadd").html("Fields are empty. Please fill all the blank spaces.");
                return false;
            }
            else {
                jQuery("#erroronadd").removeClass("error");
                jQuery("#erroronadd").hide();
                jQuery("#erroronadd").html("");
                return true;
            }
        }
    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#TabbedPanels2 li").bind({ mouseenter: function () { jQuery(this).addClass("TabbedPanelsTabHover"); }, mouseleave: function () { jQuery(this).removeClass("TabbedPanelsTabHover"); } });
        });
        function movePage(val) {
            document.getElementsByTagName('html')[0].style.overflow = 'auto';
            jQuery('#Loding_overlay').hide();
            var ofset = jQuery("#" + val).offset();
            jQuery('body').scrollTop(ofset.top);
            jQuery('html').scrollTop(ofset.top);
            
        }
    </script>
</asp:Content>
