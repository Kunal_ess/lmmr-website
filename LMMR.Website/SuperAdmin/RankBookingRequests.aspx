﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuperAdmin/Main.master" AutoEventWireup="true"
    CodeFile="RankBookingRequests.aspx.cs" Inherits="SuperAdmin_RankBookings" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <asp:UpdateProgress ID="uprog" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
          <div id="Loding_overlay" style="display: block;">
                <img src="../Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
                <span>Loading...</span></div
       </ProgressTemplate>
    </asp:UpdateProgress>
   <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
    <div>
        <table width="100%">
            <tr>
                <td align="left" width="10%">
                    <div class="n-btn">
                        <asp:LinkButton ID="radioBookinngs" runat="server" class="btn" ForeColor="White"
                            OnClick="radioBookinngs_CheckedChanged">
                   		<div class="n-btn-left"></div>                    
                    	<div  class="n-btn-mid">  Bookings</div>                    
                    	<div class="n-btn-right"></div>
                        </asp:LinkButton>
                    </div>
                    </td><td>
                        <div class="n-btn">
                            <asp:LinkButton ID="radioRequests" runat="server" class="btn" ForeColor="White" OnClick="radioRequests_CheckedChanged">
                   		<div class="n-btn-left"></div>                    
                    	<div  class="n-btn-mid">  Requests</div>                    
                    	<div class="n-btn-right"></div>
                            </asp:LinkButton>
                        </div>
                    </td>
            </tr>
        </table>
        <%-- <asp:RadioButton ID="radioBookinngs1" runat="server" AutoPostBack="true" 
                        GroupName="bookingRequestGroup" Text="Bookings" 
                        oncheckedchanged="radioBookinngs_CheckedChanged" />
                    <asp:RadioButton ID="radioRequests1" runat="server" AutoPostBack="true"
                        GroupName="bookingRequestGroup" Text="Requests" 
                        oncheckedchanged="radioRequests_CheckedChanged" />--%>
    </div>
    <div class="superadmin-mainbody-inner" style="width: 971px;">
        <div id="divmessage" runat="server" class="error">
        </div>
        <div id="bookingDIV" runat="server" visible="false">
            <div class="heading-body">
                <div class="heading-left" style="width: 971px;">
                    <h2 style="width: 952px;">
                        For Booking</h2>
                </div>
            </div>
            <div class="operator-mainbody">
                <br>
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                    <tr>
                        <td bgcolor="#C3D6E2" width="40%" style="border-bottom: 1px solid #92BEDE; border-top: 1px solid #92BEDE"
                            align="center">
                            Rules
                        </td>
                        <td bgcolor="#C3D6E2" width="40%" style="border-bottom: 1px solid #92BEDE; border-top: 1px solid #92BEDE;
                            border-left: 1px solid #92BEDE" align="center">
                            Points
                        </td>
                        <td bgcolor="#C3D6E2" width="10%" style="border-bottom: 1px solid #92BEDE; border-top: 1px solid #92BEDE;
                            border-left: 1px solid #92BEDE; font-size: 11px" align="center">
                            Max score
                        </td>
                        <td bgcolor="#C3D6E2" width="10%" style="border-bottom: 1px solid #92BEDE; border-top: 1px solid #92BEDE;
                            border-left: 1px solid #92BEDE" align="center">
                            %
                        </td>
                    </tr>
                    <tr bgcolor="#ECF7FE">
                        <td style="border-bottom: 1px solid #92BEDE;">
                            <b>1.</b> Total online availability for 60 days in the Future
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE">
                            <table width="100%">
                                <tr align="left">
                                    <td align="left">
                                        0,5 points for each day
                                    </td>
                                    <td align="right">
                                        30
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE" align="center">
                            20
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE" align="center">
                            <asp:TextBox ID="txtWeightageR1" runat="server" CssClass="inputboxsmall" MaxLength="3"
                                Text="0"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txtWeightageR1"
                                FilterType="Numbers">
                            </asp:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr bgcolor="#D9EEFC">
                        <td style="border-bottom: 1px solid #92BEDE;">
                            <b>2.</b> Total discount value for 60 days ahead
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE">
                            <table width="100%">
                                <tr align="left">
                                    <td align="left">
                                        If discount value is between 5-10
                                    </td>
                                    <td align="right">
                                        5
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        If discount value is between 11-15
                                    </td>
                                    <td align="right">
                                        10
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        If discount value is between 16-25
                                    </td>
                                    <td align="right">
                                        15
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        If discount value is more than 26
                                    </td>
                                    <td align="right">
                                        20
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE" align="center">
                            40
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE" align="center">
                            <asp:TextBox ID="txtWeightageR2" runat="server" CssClass="inputboxsmall" MaxLength="3"
                                Text="0"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtWeightageR2"
                                FilterType="Numbers">
                            </asp:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr bgcolor="#ECF7FE">
                        <td style="border-bottom: 1px solid #92BEDE;">
                            <b>3.</b> Number of bookings made 60 days in the past
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE">
                            <table width="100%">
                                <tr align="left">
                                    <td align="left">
                                        If the company is in the TOP 5
                                    </td>
                                    <td align="right">
                                        10
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        Top 6-10 points
                                    </td>
                                    <td align="right">
                                        7
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        Top 11-30
                                    </td>
                                    <td align="right">
                                        4
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        Below 31
                                    </td>
                                    <td align="right">
                                        0
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE" align="center">
                            0
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE" align="center">
                            <asp:TextBox ID="txtWeightageR3" runat="server" CssClass="inputboxsmall" MaxLength="3"
                                Text="0"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="txtWeightageR3"
                                FilterType="Numbers">
                            </asp:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr bgcolor="#D9EEFC">
                        <td style="border-bottom: 1px solid #92BEDE;">
                            <b>4.</b> Operator choice
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE">
                            <table width="100%">
                                <tr align="left">
                                    <td align="left">
                                        Excellent
                                    </td>
                                    <td align="right">
                                        20
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        Good
                                    </td>
                                    <td align="right">
                                        15
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        Average
                                    </td>
                                    <td align="right">
                                        10
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        Bad
                                    </td>
                                    <td align="right">
                                        5
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE" align="center">
                            40
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE" align="center">
                            <asp:TextBox ID="txtWeightageR4" runat="server" CssClass="inputboxsmall" MaxLength="3"
                                Text="0"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="txtWeightageR4"
                                FilterType="Numbers">
                            </asp:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr bgcolor="#ECF7FE">
                        <td style="border-bottom: 1px solid #92BEDE;">
                            <b>5.</b> Guest score
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE">
                            <table width="100%">
                                <tr align="left">
                                    <td align="left">
                                        If the score is between 9 and 10
                                    </td>
                                    <td align="right">
                                        20
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        If the score is between 8 and 9
                                    </td>
                                    <td align="right">
                                        15
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        If the score is between 7 and 8
                                    </td>
                                    <td align="right">
                                        10
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        If the score is between 5 and 7
                                    </td>
                                    <td align="right">
                                        5
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        If the score is between 0 and 6
                                    </td>
                                    <td align="right">
                                        0
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE" align="center">
                            0
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE" align="center">
                            <asp:TextBox ID="txtWeightageR5" runat="server" CssClass="inputboxsmall" MaxLength="3"
                                Text="0"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" TargetControlID="txtWeightageR5"
                                FilterType="Numbers">
                            </asp:FilteredTextBoxExtender>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="requestDIV" runat="server" visible="false">
            <div class="heading-body">
                <div class="heading-left" style="width: 971px;">
                    <h2 style="width: 952px;">
                        For Request</h2>
                </div>
            </div>
            <div class="operator-mainbody">
                <br>
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                    <tr>
                        <td bgcolor="#C3D6E2" width="40%" style="border-bottom: 1px solid #92BEDE; border-top: 1px solid #92BEDE"
                            align="center">
                            Rules
                        </td>
                        <td bgcolor="#C3D6E2" width="40%" style="border-bottom: 1px solid #92BEDE; border-top: 1px solid #92BEDE;
                            border-left: 1px solid #92BEDE" align="center">
                            Points
                        </td>
                        <td bgcolor="#C3D6E2" width="10%" style="border-bottom: 1px solid #92BEDE; border-top: 1px solid #92BEDE;
                            border-left: 1px solid #92BEDE; font-size: 11px" align="center">
                            Max score
                        </td>
                        <td bgcolor="#C3D6E2" width="10%" style="border-bottom: 1px solid #92BEDE; border-top: 1px solid #92BEDE;
                            border-left: 1px solid #92BEDE" align="center">
                            %
                        </td>
                    </tr>
                    <tr bgcolor="#ECF7FE">
                        <td style="border-bottom: 1px solid #92BEDE;">
                            <b>1.</b> Inventory Volume (total number of meeting rooms)
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE">
                            <table width="100%">
                                <tr align="left">
                                    <td align="left">
                                        If the company is in the TOP 5
                                    </td>
                                    <td align="center">
                                        20
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        Top 6-10 points
                                    </td>
                                    <td align="center">
                                        15
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        Top 11-30
                                    </td>
                                    <td align="center">
                                        5
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        Below 31
                                    </td>
                                    <td align="center">
                                        0
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE" align="center">
                            35
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE" align="center">
                            <asp:TextBox ID="txtWeightageRR1" runat="server" CssClass="inputboxsmall" MaxLength="3"
                                Text="0"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" TargetControlID="txtWeightageRR1"
                                FilterType="Numbers">
                            </asp:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr bgcolor="#D9EEFC">
                        <td style="border-bottom: 1px solid #92BEDE;">
                            <b>2.</b> Guest Score
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE">
                            <table width="100%">
                                <tr align="left">
                                    <td align="left">
                                        If the score is between 9 and 10
                                    </td>
                                    <td align="center">
                                        20
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        If the score is between 8 and 9
                                    </td>
                                    <td align="center">
                                        15
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        If the score is between 7 and 8
                                    </td>
                                    <td align="center">
                                        10
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        If the score is between 5 and 7
                                    </td>
                                    <td align="center">
                                        5
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        If the score is between 0 and 6
                                    </td>
                                    <td align="center">
                                        0
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE" align="center">
                            0
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE" align="center">
                            <asp:TextBox ID="txtWeightageRR2" runat="server" CssClass="inputboxsmall" MaxLength="3"
                                Text="0"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" TargetControlID="txtWeightageRR2"
                                FilterType="Numbers">
                            </asp:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr bgcolor="#ECF7FE">
                        <td style="border-bottom: 1px solid #92BEDE;">
                            <b>3.</b> Number of Requests past 60 days
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE">
                            <table width="100%">
                                <tr align="left">
                                    <td align="left">
                                        If the company is in the TOP 5
                                    </td>
                                    <td align="center">
                                        20
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        Top 6-10 points
                                    </td>
                                    <td align="center">
                                        15
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        Top 11-30
                                    </td>
                                    <td align="center">
                                        10
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        Below 31
                                    </td>
                                    <td align="center">
                                        0
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE" align="center">
                            15
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE" align="center">
                            <asp:TextBox ID="txtWeightageRR3" runat="server" CssClass="inputboxsmall" MaxLength="3"
                                Text="0"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" TargetControlID="txtWeightageRR3"
                                FilterType="Numbers">
                            </asp:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr bgcolor="#D9EEFC">
                        <td style="border-bottom: 1px solid #92BEDE;">
                            <b>4.</b> Total number of conversion from Request to TEN for 60 in past
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE">
                            <table width="100%">
                                <tr align="left">
                                    <td align="left">
                                        If conversion is between 80% and 100%
                                    </td>
                                    <td align="center">
                                        20
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        If conversion is between 70% and 79%
                                    </td>
                                    <td align="center">
                                        15
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        If conversion is between 50% and 69%
                                    </td>
                                    <td align="center">
                                        10
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        If conversion is between 0% and 49%
                                    </td>
                                    <td align="center">
                                        0
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE" align="center">
                            10
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE" align="center">
                            <asp:TextBox ID="txtWeightageRR4" runat="server" CssClass="inputboxsmall" MaxLength="3"
                                Text="0"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" TargetControlID="txtWeightageRR4"
                                FilterType="Numbers">
                            </asp:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr bgcolor="#ECF7FE">
                        <td style="border-bottom: 1px solid #92BEDE;">
                            <b>5.</b> Total number of conversion from TEN to DEF for 60 in past
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE">
                            <table width="100%">
                                <tr align="left">
                                    <td align="left">
                                        If conversion is between 70% and 100%
                                    </td>
                                    <td align="center">
                                        20
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        If conversion is between 50% and 69%
                                    </td>
                                    <td align="center">
                                        15
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        If conversion is between 30% and 49%
                                    </td>
                                    <td align="center">
                                        10
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        If conversion is between 0% and 29%
                                    </td>
                                    <td align="center">
                                        5
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE" align="center">
                            10
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE" align="center">
                            <asp:TextBox ID="txtWeightageRR5" runat="server" CssClass="inputboxsmall" MaxLength="3"
                                Text="0"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" TargetControlID="txtWeightageRR5"
                                FilterType="Numbers">
                            </asp:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr bgcolor="#D9EEFC">
                        <td style="border-bottom: 1px solid #92BEDE;">
                            <b>6.</b> Operator choice
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE">
                            <table width="100%">
                                <tr align="left">
                                    <td align="left">
                                        Excellent
                                    </td>
                                    <td align="right">
                                        20
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        Good
                                    </td>
                                    <td align="right">
                                        15
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        Average
                                    </td>
                                    <td align="right">
                                        10
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td align="left">
                                        Bad
                                    </td>
                                    <td align="right">
                                        5
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE" align="center">
                            30
                        </td>
                        <td style="border-bottom: 1px solid #92BEDE; border-left: 1px solid #92BEDE" align="center">
                            <asp:TextBox ID="txtWeightageRR6" runat="server" CssClass="inputboxsmall" MaxLength="3"
                                Text="0"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" TargetControlID="txtWeightageRR6"
                                FilterType="Numbers">
                            </asp:FilteredTextBoxExtender>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div id="bookingSaveCancelDIV" runat="server" visible="false" style="float: left;">
        <%--<div class="bottom-prices-container-btn" style="width: 945px; margin: 0 14px;">
            <div class="save-cancel-btn1" style="width: 545px;">
                <asp:LinkButton ID="lbtCancel" runat="server" CssClass="cancel-btn" OnClick="lbtCancel_Click">Cancel</asp:LinkButton>
                &nbsp; &nbsp;&nbsp;<asp:LinkButton ID="lbtSave1" runat="server" CssClass="save-btn"
                   >Save</asp:LinkButton>
            </div>
        </div>--%>
        <div class="booking-details" style="width: 760px;">
            <ul>
                <li class="value10">
                    <div class="col21" style="width: 955px;">
                        <div class="button_section">
                            <asp:LinkButton ID="lbtSave" runat="server" Text="Save" CssClass="select" OnClientClick="return Check();"
                                OnClick="lbtSave_Click" />
                            <span>or</span>
                            <asp:LinkButton ID="lbtCancel" runat="server" Text="Cancel" OnClick="lbtCancel_Click" />
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div id="requestSaveCancelDIV" runat="server" visible="false" style="float: left;">
        <%--<div class="bottom-prices-container-btn" style="width: 945px; margin: 0 14px;">
            <div class="save-cancel-btn1" style="width: 545px;">
                <asp:LinkButton ID="lbtCancelRequest1" runat="server" CssClass="cancel-btn" OnClick="lbtCancelRequest_Click">Cancel</asp:LinkButton>
                &nbsp; &nbsp;&nbsp;<asp:LinkButton ID="lbtSaveRequest1" runat="server" CssClass="save-btn"
                    OnClientClick="return CheckRequest();" OnClick="lbtSaveRequest_Click">Save</asp:LinkButton>
            </div>
        </div>--%>
        <div class="booking-details" style="width: 760px;">
            <ul>
                <li class="value10">
                    <div class="col21" style="width: 955px;">
                        <div class="button_section">
                            <asp:LinkButton ID="lbtSaveRequest" runat="server" Text="Save" CssClass="select"
                                OnClientClick="return CheckRequest();" OnClick="lbtSaveRequest_Click" />
                            <span>or</span>
                            <asp:LinkButton ID="lbtCancelRequest" runat="server" Text="Cancel" OnClick="lbtCancelRequest_Click" />
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <%--    </ContentTemplate>
    </asp:UpdatePanel>--%>
    <script language="javascript" type="text/javascript">

        function Check() {
            if (jQuery("#<%= divmessage.ClientID%>").hasClass("succesfuly")) {
                jQuery("#<%= divmessage.ClientID%>").removeClass("succesfuly").addClass("error").hide();
            }
            var isvalid = true;
            var errormessage = "";
            var v1 = parseInt(jQuery("#<%= txtWeightageR1.ClientID%>").val(), 10);
            var v2 = parseInt(jQuery("#<%= txtWeightageR2.ClientID%>").val(), 10);
            var v3 = parseInt(jQuery("#<%= txtWeightageR3.ClientID%>").val(), 10);
            var v4 = parseInt(jQuery("#<%= txtWeightageR4.ClientID%>").val(), 10);
            var v5 = parseInt(jQuery("#<%= txtWeightageR5.ClientID%>").val(), 10);
            var total = v1 + v2 + v3 + v4 + v5;
            if (total != 100) {
                if (isNaN(total)) {
                    errormessage = "Please provide all the weights.";
                    isvalid = false;
                }
                else {
                    errormessage = "The total assigned weight is " + total + ", but total assigned weight must be equal to 100.";
                    isvalid = false;
                }
            }


            if (!isvalid) {
                jQuery(".error").show();
                jQuery(".error").html(errormessage);
                var offseterror = jQuery(".error").offset();
                jQuery("body").scrollTop(offseterror.top);
                jQuery("html").scrollTop(offseterror.top);
                return false;
            }


            jQuery(".error").hide();
            jQuery(".error").html('');
            jQuery("body").scrollTop(0);
            jQuery("html").scrollTop(0);
            jQuery("#Loding_overlaySec span").html("Saving...");
            jQuery("#Loding_overlaySec").show();
        }


        function CheckRequest() {
            if (jQuery("#<%= divmessage.ClientID%>").hasClass("succesfuly")) {
                jQuery("#<%= divmessage.ClientID%>").removeClass("succesfuly").addClass("error").hide();
            }
            var isvalid = true;
            var errormessage = "";

            var v1 = parseInt(jQuery("#<%= txtWeightageRR1.ClientID%>").val(), 10);
            var v2 = parseInt(jQuery("#<%= txtWeightageRR2.ClientID%>").val(), 10);
            var v3 = parseInt(jQuery("#<%= txtWeightageRR3.ClientID%>").val(), 10);
            var v4 = parseInt(jQuery("#<%= txtWeightageRR4.ClientID%>").val(), 10);
            var v5 = parseInt(jQuery("#<%= txtWeightageRR5.ClientID%>").val(), 10);
            var v6 = parseInt(jQuery("#<%= txtWeightageRR6.ClientID%>").val(), 10);
            var total = v1 + v2 + v3 + v4 + v5 + v6;

            if (total != 100) {
                if (isNaN(total)) {
                    errormessage = "Please provide all the weights.";
                    isvalid = false;
                }
                else {
                    errormessage = "The weights assigned total to " + total + " but total weight must be equal to 100";
                    isvalid = false;
                }
            }


            if (!isvalid) {
                jQuery(".error").show();
                jQuery(".error").html(errormessage);
                var offseterror = jQuery(".error").offset();
                jQuery("body").scrollTop(offseterror.top);
                jQuery("html").scrollTop(offseterror.top);
                return false;
            }


            jQuery(".error").hide();
            jQuery(".error").html('');
            jQuery("body").scrollTop(0);
            jQuery("html").scrollTop(0);

        }

    </script>
</asp:Content>
