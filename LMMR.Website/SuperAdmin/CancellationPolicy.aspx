﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuperAdmin/Main.master" AutoEventWireup="true"
    CodeFile="CancellationPolicy.aspx.cs" Inherits="SuperAdmin_CancellationPolicy"
    MaintainScrollPositionOnPostback="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControl/SuperAdmin/LanguageTab.ascx" TagName="LanguageTab"
    TagPrefix="uc1" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:HiddenField ID="hdnLanguageID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnPolicyID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnTimeFrameTab" runat="server" Value="0" />
    <asp:Button ID="btnLanguage" runat="server" Text="Button" OnClick="btnLanguage_Click"
        Style="display: none" />
    <div class="search-operator-layout1" style="margin-bottom: 20px;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <div class="search-operator-layout-left1">
                        <div class="search-operator-from1">
                            <div class="commisions-top-new1 ">
                                <table>
                                    <tr>
                                        <td style="height: 30px;">
                                            <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="true" Width="180px"
                                                OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <table width="100%" border="0" cellspacing="0">
        <tr>
            <td>
                <div id="divmsg" runat="server" class="error" style="display: none">
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%;" style="background: #ffffff; border: 1px #A3D4F7 solid;">
                    <tr>
                        <td>
                            <div style="margin-right: 10px; float: left">
                                <div class="n-btn1">
                                    <asp:LinkButton ID="lnkButtonAddnew" runat="server" CssClass="register-hote2-btn"
                                        OnClick="lnkButtonAddnew_Click">
                        <div class="n-btn-left">
                        </div>
                        <div class="n-btn-mid">
                            Add new</div>
                        <div class="n-btn-right">
                        </div>
                                    </asp:LinkButton>
                                </div>
                            </div>
                            <div style="margin-right: 10px; float: left;" runat="server" id="ModifyButton">
                                <div class="n-btn1">
                                    <asp:LinkButton ID="lnkButtonModify" runat="server" CssClass="register-hote2-btn"
                                        OnClick="lnkButtonModify_Click">
                        <div class="n-btn-left">
                        </div>
                        <div class="n-btn-mid">
                            Modify</div>
                        <div class="n-btn-right">
                        </div>
                                    </asp:LinkButton>
                                </div>
                            </div>
                            <div style="margin-right: 10px; float: left;" runat="server" id="DeleteButton">
                                <div class="n-btn1" style="margin-right: 10px;">
                                    <asp:LinkButton ID="lnkButtonDelete" runat="server" CssClass="register-hote2-btn"
                                        OnClick="lnkButtonDelete_Click">
                        <div class="n-btn-left">
                        </div>
                        <div class="n-btn-mid">
                            Delete</div>
                        <div class="n-btn-right">
                        </div>
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </td>
                        <td align="right">
                            <div style="float: right;" id="pageing">
                            </div>
                        </td>
                    </tr>
                </table>
                <div id="lstViewPolicy">
                    <%--<asp:ListView ID="lstViewPolicy" runat="server" ItemPlaceholderID="placeHolderPolicy"
                        AutomaticGenerateColumns="false" OnItemDataBound="lstViewPolicy_ItemDataBound"
                        DataKeyNames="Id">
                        <LayoutTemplate>
                            <table width="959px" border="0" cellspacing="0" bgcolor="#FFFFFF" style="border-bottom: #A3D4F7 solid 1px;
                                border-left: #A3D4F7 solid 1px; border-right: #A3D4F7 solid 1px;" cellpadding="10">
                                <tr>
                                    <td style="border-bottom: #A3D4F7 solid 1px; font-weight: bold" width="1%">
                                    </td>
                                    <td style="border-bottom: #A3D4F7 solid 1px; font-weight: bold" width="4%">
                                        Country
                                    </td>
                                    <td style="border-bottom: #A3D4F7 solid 1px; font-weight: bold" width="10%">
                                        Name
                                    </td>
                                    <td style="border-bottom: #A3D4F7 solid 1px; font-weight: bold" width="15%">
                                        Creation date
                                    </td>
                                    <td style="border-bottom: #A3D4F7 solid 1px; font-weight: bold" width="8%">
                                        Timeframe
                                    </td>
                                    <td style="border-bottom: #A3D4F7 solid 1px; font-weight: bold" width="15%">
                                        Applied to
                                    </td>
                                    <td style="border-bottom: #A3D4F7 solid 1px; font-weight: bold" width="15%">
                                        User can cancel
                                    </td>
                                    <td style="border-bottom: #A3D4F7 solid 1px; font-weight: bold" width="15%">
                                        Country Default
                                    </td>
                                    <td style="border-bottom: #A3D4F7 solid 1px; font-weight: bold" width="2%">
                                        &nbsp;
                                    </td>
                                </tr>
                                <asp:PlaceHolder ID="placeHolderPolicy" runat="server"></asp:PlaceHolder>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td valign="top">
                                    <asp:RadioButton ID="rbselectpolicy" runat="server" onclick="javascript:CheckOtherIsCheckedByGVID(this);" />
                                </td>
                                <td valign="top">
                                    <asp:Label ID="lblCountryName" runat="server" Text=""></asp:Label>
                                </td>
                                <td valign="top">
                                    <asp:Label ID="lblPolicyName" runat="server" Text=""></asp:Label>
                                </td>
                                <td valign="top">
                                    <asp:Label ID="lblCreationDate" runat="server" Text=""></asp:Label>
                                </td>
                                <td valign="top">
                                    <asp:Label ID="lblTimeFrame" runat="server" Text=""></asp:Label>
                                </td>
                                <td valign="top">
                                    <asp:Label ID="lblAppliedTo" runat="server" Text=""></asp:Label>
                                </td>
                                <td align="center" valign="top">
                                    <asp:Label ID="lblUserCanCancel" runat="server" Text="test"></asp:Label>
                                </td>
                                <td align="center" valign="top">
                                    <asp:Image ID="imgIsDefault" runat="server" />
                                    <asp:CheckBox ID="chkSetDefault" runat="server" key='<%# Eval("Id") %>' OnCheckedChanged="SetDefaultPolicy"
                                        AutoPostBack="true" />
                                </td>
                                <td align="center" valign="top">
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>--%>
                    <asp:GridView ID="lstViewPolicy" runat="server" Width="100%" CellPadding="8" border="0"
                        HeaderStyle-CssClass="heading-row" RowStyle-CssClass="con" AlternatingRowStyle-CssClass="con-dark"
                        AutoGenerateColumns="False" OnRowDataBound="lstViewPolicy_ItemDataBound" GridLines="None"
                        CellSpacing="1" BackColor="#A3D4F7" DataKeyNames="Id" OnPageIndexChanging="lstViewPolicy_PageIndexChanging"
                        PageSize="10" AllowPaging="true">
                        <Columns>
                            <asp:TemplateField HeaderText="" ItemStyle-Width="1%">
                                <ItemTemplate>
                                    <asp:CheckBox ID="rbselectpolicy" runat="server" onclick="javascript:CheckOtherIsCheckedByGVID(this);" />
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:CheckBox ID="rbselectpolicy" runat="server" onclick="javascript:CheckOtherIsCheckedByGVID(this);" />
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Country" ItemStyle-Width="4%">
                                <ItemTemplate>
                                    <asp:Label ID="lblCountryName" runat="server" Text=""></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblCountryName" runat="server" Text=""></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name" ItemStyle-Width="13%">
                                <ItemTemplate>
                                    <asp:Label ID="lblPolicyName" runat="server" Text=""></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblPolicyName" runat="server" Text=""></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Creation date" ItemStyle-Width="12%" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreationDate" runat="server" Text=""></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblCreationDate" runat="server" Text=""></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Timeframe" ItemStyle-Width="14%" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblTimeFrame" runat="server" Text=""></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblTimeFrame" runat="server" Text=""></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Applied to" ItemStyle-Width="30%">
                                <ItemTemplate>
                                    <asp:Label ID="lblAppliedTo" runat="server" Text=""></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblAppliedTo" runat="server" Text=""></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="User can cancel" ItemStyle-Width="13%" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblUserCanCancel" runat="server" Text="test"></asp:Label>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Label ID="lblUserCanCancel" runat="server" Text="test"></asp:Label>
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Country Default" ItemStyle-Width="13%" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Image ID="imgIsDefault" runat="server" />
                                    <asp:CheckBox ID="chkSetDefault" runat="server" key='<%# Eval("Id") %>' OnCheckedChanged="SetDefaultPolicy"
                                        AutoPostBack="true" />
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <asp:Image ID="imgIsDefault" runat="server" />
                                    <asp:CheckBox ID="chkSetDefault" runat="server" key='<%# Eval("Id") %>' OnCheckedChanged="SetDefaultPolicy"
                                        AutoPostBack="true" />
                                </AlternatingItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <center>
                                <b>No record found</b></span></center>
                        </EmptyDataTemplate>
                        <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                        <PagerStyle HorizontalAlign="Right" BackColor="White" CssClass="displayNone" />
                        <PagerTemplate>
                            <div id="Paging" style="width: 100%; display: none;">
                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                            </div>
                        </PagerTemplate>
                    </asp:GridView>
                </div>
                <div class="cancelation-heading-body3" id="HotelAssignTab" runat="server">
                    <ul>
                        <li><a href="#" id="liTimeFrame" class="select" onclick="return SelectTab('TimeFrame');">
                            Time frame-conditions</a></li>
                        <li><a href="#" id="liHotelAssign" onclick="return SelectTab('HotelAssign');">Assign
                            hotel</a></li>
                    </ul>
                </div>
                <div id="divmessage" runat="server" class="error" style="display: none; width: 99%;">
                </div>
                <asp:Panel ID="pnlAddModify" runat="server">
                    <table width="100%" style="background: #ffffff; border: #A3D4F7 solid 1px;">
                        <tr>
                            <td width="20%">
                                Name :
                            </td>
                            <td>
                                <asp:TextBox ID="txtName" runat="server" class="inputbox"></asp:TextBox>
                            </td>
                        </tr>
                        <%--<div class="cancelation-heading-body">
                    </div>--%>
                        <tr>
                            <td>
                                User can cancel a booking in day
                            </td>
                            <td>
                                <asp:TextBox ID="txtCancellationLimit" class="inputboxsmall" Style="text-align: left"
                                    runat="server" MaxLength="2"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" TargetControlID="txtCancellationLimit"
                                    FilterType="Numbers" runat="server">
                                </asp:FilteredTextBoxExtender>
                                before arrived
                            </td>
                        </tr>
                        <%--<div class="superadmin-mainbody-sub4-left">
                        </div>--%>
                        <uc1:LanguageTab ID="LanguageTab1" runat="server" />
                        <tr>
                            <td>
                                <b>Section 1</b> Between day
                            </td>
                            <td>
                                <asp:TextBox ID="txtTimeFrameMax1" class="inputboxsmall" runat="server" Text="60"
                                    ReadOnly="true" MaxLength="2"></asp:TextBox>
                                and day
                                <asp:TextBox ID="txtTimeFrameMin1" class="inputboxsmall" runat="server" Style="text-align: left"
                                    onkeyup="setTimeFame1();" MaxLength="2"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtTimeFrameMin1"
                                    FilterType="Numbers" runat="server">
                                </asp:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <%--<div class="superadmin-mainbody-sub5">
                        </div>--%>
                        <tr>
                            <td colspan="2">
                                <div class="tab-textarea1">
                                    <CKEditor:CKEditorControl ID="CKEditorSection1" runat="server" Height="200" Toolbar="Basic"></CKEditor:CKEditorControl>
                                </div>
                            </td>
                        </tr>
                        <%--<div class="cancelation-tabbody1">
                            <div class="superadmin-mainbody-sub8">
                            </div>
                        </div>--%>
                        <tr>
                            <td>
                                <b>Section 2</b> Between day
                            </td>
                            <td>
                                <asp:TextBox ID="txtTimeFrameMax2" class="inputboxsmall" runat="server" Style="text-align: left"
                                    MaxLength="2"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="txtTimeFrameMax2"
                                    FilterType="Numbers" runat="server">
                                </asp:FilteredTextBoxExtender>
                                and day
                                <asp:TextBox ID="txtTimeFrameMin2" class="inputboxsmall" runat="server" Style="text-align: left"
                                    onkeyup="setTimeFame2();" MaxLength="2">
                                </asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" TargetControlID="txtTimeFrameMin2"
                                    FilterType="Numbers" runat="server">
                                </asp:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <%--<div class="superadmin-mainbody-sub4">
                            <div class="superadmin-mainbody-sub4-left">
                            </div>
                        </div>--%>
                        <tr>
                            <td colspan="2">
                                <div class="tab-textarea1">
                                    <CKEditor:CKEditorControl ID="CKEditorSection2" runat="server" Height="200" Toolbar="Basic"></CKEditor:CKEditorControl>
                                </div>
                            </td>
                        </tr>
                        <%--<div class="cancelation-tabbody1">
                            <div class="superadmin-mainbody-sub8">
                            </div>
                        </div>--%>
                        <tr>
                            <td>
                                <b>Section 3</b> Between day
                            </td>
                            <td>
                                <asp:TextBox ID="txtTimeFrameMax3" class="inputboxsmall" runat="server" Style="text-align: left"
                                    MaxLength="2"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" TargetControlID="txtTimeFrameMax3"
                                    FilterType="Numbers" runat="server">
                                </asp:FilteredTextBoxExtender>
                                and day
                                <asp:TextBox ID="txtTimeFrameMin3" class="inputboxsmall" runat="server" Style="text-align: left"
                                    onkeyup="setTimeFame3();" MaxLength="2">
                                </asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" TargetControlID="txtTimeFrameMin3"
                                    FilterType="Numbers" runat="server">
                                </asp:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <%--<div class="superadmin-mainbody-sub4">
                            <div class="superadmin-mainbody-sub4-left">
                            </div>
                        </div>--%>
                        <tr>
                            <td colspan="2">
                                <div class="tab-textarea1">
                                    <CKEditor:CKEditorControl ID="CKEditorSection3" runat="server" Height="200" Toolbar="Basic"></CKEditor:CKEditorControl>
                                </div>
                            </td>
                        </tr>
                        <%--<div class="cancelation-tabbody1">
                            <div class="superadmin-mainbody-sub8">
                            </div>
                        </div>--%>
                        <tr>
                            <td>
                                <b>Section 4</b> Between day
                            </td>
                            <td>
                                <asp:TextBox ID="txtTimeFrameMax4" class="inputboxsmall" runat="server" Style="text-align: left"
                                    MaxLength="2"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" TargetControlID="txtTimeFrameMax4"
                                    FilterType="Numbers" runat="server">
                                </asp:FilteredTextBoxExtender>
                                and day
                                <asp:TextBox ID="txtTimeFrameMin4" class="inputboxsmall" runat="server" Style="text-align: center"
                                    Text="0" ReadOnly="true" MaxLength="2">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <%--<br>
                        <br>
                        <br>
                        <div class="superadmin-mainbody-sub4">
                            <div class="superadmin-mainbody-sub4-left">
                            </div>
                        </div>--%>
                        <tr>
                            <td colspan="2">
                                <div class="tab-textarea1">
                                    <CKEditor:CKEditorControl ID="CKEditorSection4" runat="server" Height="200" Toolbar="Basic"></CKEditor:CKEditorControl>
                                </div>
                            </td>
                        </tr>
                        <%--  <div class="cancelation-tabbody1">
                            <div class="superadmin-mainbody-sub8">
                            </div>
                        </div>--%>
                        <tr>
                            <td colspan="2">
                                <div class="button_section">
                                    <asp:LinkButton ID="btnSave" CssClass="select" runat="server" Text="Save" OnClick="btnSave_Click" />
                                    <asp:LinkButton ID="btnUpdate" CssClass="select" runat="server" Text="Save" OnClick="btnUpdate_Click" />
                                    <span>or</span>
                                    <asp:LinkButton ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlAssignHotel" runat="server">
                    <table style="background: #ffffff; border: #A3D4F7 solid 1px;" width="100%">
                        <tr>
                            <td>
                                Policy Name :
                                <asp:Label ID="lblPolicyName" runat="server" Style="font-size: medium" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="tab-textarea1">
                                    <asp:CheckBoxList ID="chklstHotel" runat="server" RepeatColumns="4" RepeatDirection="Horizontal">
                                    </asp:CheckBoxList>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <%-- <div class="cancelation-heading-body" style="display:none">
                        </asp:Label><asp:DropDownList ID="ddlPolicy" runat="server" Style="display: none">
                        </asp:DropDownList>
                    </div>
                    --%>
                    <%--<div class="cancelation-tabbody1">
                        <div class="superadmin-mainbody-sub8">
                        </div>
                    </div>--%>
                    <div style="padding-top: 50px; width: 100%;">
                        <asp:CheckBox ID="CheckBox1" runat="server" Text="Hotel assigned to this policy"
                            Enabled="false" Style="background-color: Green;" />
                        <asp:CheckBox ID="CheckBox2" runat="server" Text="Hotel assigned to other policy"
                            Enabled="false" Style="background-color: Yellow;" />
                        <asp:CheckBox ID="CheckBox3" runat="server" Text="Not assigned" Enabled="false" Style="background-color: White;" />
                    </div>
                    <div style="padding-top: 50px;">
                        <div class="button_section">
                            <asp:LinkButton ID="lnkSaveAssingHotel" CssClass="select" runat="server" Text="Save"
                                OnClick="lnkSaveAssingHotel_Click" />
                            <span>or</span>
                            <asp:LinkButton ID="lnkCancelAssingHotel" runat="server" Text="Cancel" OnClick="lnkCancelAssingHotel_Click" />
                        </div>
                    </div>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <div class="cancelation-body">
    </div>
    <script language="javascript" type="text/javascript">

        jQuery(document).ready(function () {

        });

        jQuery(document).ready(function () {
            jQuery("#<%= lnkButtonModify.ClientID %>").bind("click", function () {
                if (jQuery("#lstViewPolicy").find("input:checkbox:checked").length <= 0) {
                    alert('please select policy to modify.');
                    return false;
                }
            });

            jQuery("#<%= lnkButtonDelete.ClientID %>").bind("click", function () {
                if (jQuery("#lstViewPolicy").find("input:checkbox:checked").length <= 0) {
                    alert('please select policy to delete.');
                    return false;
                }
                else {
                    return confirm("Are you sure you want to delete it?");
                }
            });
        });
                            
    </script>
    <script type="text/javascript" language="javascript">

        jQuery(document).ready(function () {
            var languageId = jQuery("#<%=hdnLanguageID.ClientID %>").val();
            if (languageId != "0") {
                jQuery(".superadmin-mainbody-sub6 ul li a").removeClass("select");
                jQuery("#liLanguageTab" + languageId + "").addClass("select");
            }

            //            var selectedTab = jQuery("#<%=hdnTimeFrameTab.ClientID %>").val();
            //            if (selectedTab == 'HotelAssign') {
            //                SelectTab('HotelAssign');
            //            } else {
            //                SelectTab('0');
            //            }

        });

        jQuery(document).ready(function () {
            if (jQuery("#Paging") != undefined) {
                var inner = jQuery("#Paging").html();
                jQuery("#pageing").html(inner);
                jQuery("#Paging").html("");
            }

        });

        function SelectTab(tab) {
            if (tab == 'HotelAssign') {
                jQuery("#<%=hdnTimeFrameTab.ClientID %>").val('HotelAssign')
                jQuery("#liTimeFrame").removeClass("select");
                jQuery("#liHotelAssign").addClass("select");
                jQuery("#<%=pnlAssignHotel.ClientID %>").show();
                jQuery("#<%=pnlAddModify.ClientID %>").hide();
            }
            else {
                jQuery("#<%=hdnTimeFrameTab.ClientID %>").val('0')
                jQuery("#liTimeFrame").addClass("select");
                jQuery("#liHotelAssign").removeClass("select");
                jQuery("#<%=pnlAddModify.ClientID %>").show();
                jQuery("#<%=pnlAssignHotel.ClientID %>").hide();
            }

            var tab = "<%=HotelAssignTab.ClientID %>";
            SetFocusBottom(tab);

            return false;
        }

        function setTimeFame1() {

            var min1 = jQuery("#<%=txtTimeFrameMin1.ClientID %>").val();
            jQuery("#<%=txtTimeFrameMax2.ClientID %>").val(parseInt(min1) - 1);
        }

        function setTimeFame2() {

            var min2 = jQuery("#<%=txtTimeFrameMin2.ClientID %>").val();
            jQuery("#<%=txtTimeFrameMax3.ClientID %>").val(parseInt(min2) - 1);
        }

        function setTimeFame3() {

            var min3 = jQuery("#<%=txtTimeFrameMin3.ClientID %>").val();
            jQuery("#<%=txtTimeFrameMax4.ClientID %>").val(parseInt(min3) - 1);
        }

        function GetLanguageID(languageId) {
            jQuery("#<%=hdnLanguageID.ClientID %>").val(languageId);
            jQuery(".superadmin-mainbody-sub6 ul li a").removeClass("select");
            jQuery("#liLanguageTab" + languageId + "").addClass("select");
            jQuery("#<%=btnLanguage.ClientID %>").click();
        }





        function CheckOtherIsCheckedByGVID(spanChk) {
            var IsChecked = spanChk.checked;

            var CurrentRdbID = spanChk.id;
            var Chk = spanChk;
            Parent = document.getElementById('ContentPlaceHolder1_lstViewPolicy');
            var items = Parent.getElementsByTagName('input');

            for (i = 0; i < items.length; i++) {

                if (items[i].id != CurrentRdbID && items[i].type == "checkbox") {

                    if (items[i].checked) {

                        items[i].checked = false;

                        items[i].parentElement.parentElement.style.backgroundColor = '';

                        //items[i].parentElement.parentElement.style.color = 'black';
                    }
                    else {

                        items[i].parentElement.parentElement.style.backgroundColor = '';
                    }
                }
                else {

                    items[i].parentElement.parentElement.style.backgroundColor = '#feff99';
                }
            }
            if (IsChecked != true) {
                spanChk.parentElement.parentElement.style.backgroundColor = '';
            }
        }


        jQuery("#<%= btnSave.ClientID %>").bind("click", function () {
            if (jQuery("#<%= divmessage.ClientID %>").hasClass("succesfuly")) {
                jQuery("#<%= divmessage.ClientID %>").removeClass("succesfuly").addClass("error").hide();
            }
            var isvalid = true;
            var errormessage = "";


            var policyName = jQuery("#<%= txtName.ClientID %>").val();
            if (policyName.length <= 0 || policyName == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter policy name';
                isvalid = false;
            }

            var cancellationlimit = jQuery("#<%= txtCancellationLimit.ClientID %>").val();
            if (cancellationlimit.length <= 0 || cancellationlimit == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter cancellation limit';
                isvalid = false;
            }

            if (parseInt(cancellationlimit, 10) > "60") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Cancellation limit must be 60';
                isvalid = false;
            }

            var min1 = jQuery("#<%= txtTimeFrameMin1.ClientID %>").val();
            if (min1.length <= 0 || min1 == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter minimum TimeFrame of section 1';
                isvalid = false;
            }

            var frame = jQuery("#<%=CKEditorSection1.ClientID %>").siblings().find('span').find('iframe').get(0);
            var body = jQuery(frame).contents().find('body');
            var text = jQuery(body).text();
            if (text == '') {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter policy in section 1';
                isvalid = false;

            }

            var max2 = jQuery("#<%= txtTimeFrameMax2.ClientID %>").val();
            if (max2.length <= 0 || max2 == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter maximum TimeFrame of section 2';
                isvalid = false;
            }

            var min2 = jQuery("#<%= txtTimeFrameMin2.ClientID %>").val();
            if (min2.length <= 0 || min2 == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter minimum TimeFrame of section 2';
                isvalid = false;
            }

            frame = jQuery("#<%=CKEditorSection2.ClientID %>").siblings().find('span').find('iframe').get(0);
            body = jQuery(frame).contents().find('body');
            text = jQuery(body).text();
            if (text == '') {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter policy in section 2';
                isvalid = false;

            }

            var max3 = jQuery("#<%= txtTimeFrameMax3.ClientID %>").val();
            if (max3.length <= 0 || max3 == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter maximum TimeFrame of section 3';
                isvalid = false;
            }

            var min3 = jQuery("#<%= txtTimeFrameMin3.ClientID %>").val();
            if (min3.length <= 0 || min3 == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter minimum TimeFrame of section 3';
                isvalid = false;
            }

            frame = jQuery("#<%=CKEditorSection3.ClientID %>").siblings().find('span').find('iframe').get(0);
            body = jQuery(frame).contents().find('body');
            text = jQuery(body).text();
            if (text == '') {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter policy in section 3';
                isvalid = false;

            }

            var max4 = jQuery("#<%= txtTimeFrameMax4.ClientID %>").val();
            if (max4.length <= 0 || max4 == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter maximum TimeFrame of section 4';
                isvalid = false;
            }

            frame = jQuery("#<%=CKEditorSection4.ClientID %>").siblings().find('span').find('iframe').get(0);
            body = jQuery(frame).contents().find('body');
            text = jQuery(body).text();
            if (text == '') {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter policy in section 4';
                isvalid = false;

            }

            var Timemax2 = jQuery("#<%= txtTimeFrameMax2.ClientID %>").val();
            var Timemin1 = jQuery("#<%= txtTimeFrameMin1.ClientID %>").val();
            if (parseInt(Timemax2, 10) > parseInt(Timemin1, 10)) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Maximum timefame section 2 should be smaller to minimum timeframe of section 1';
                isvalid = false;
            }

            var Timemax3 = jQuery("#<%= txtTimeFrameMax3.ClientID %>").val();
            var Timemin2 = jQuery("#<%= txtTimeFrameMin2.ClientID %>").val();
            if (parseInt(Timemax3, 10) > parseInt(Timemin2, 10)) {

                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Maximum timefame section 3 should be smaller to minimum timeframe of section 2';
                isvalid = false;
            }

            var Timemax4 = jQuery("#<%= txtTimeFrameMax4.ClientID %>").val();
            var Timemin3 = jQuery("#<%= txtTimeFrameMin3.ClientID %>").val();
            if (parseInt(Timemax4, 10) > parseInt(Timemin3, 10)) {

                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Maximum timefame section 4 should be smaller to minimum timeframe of section 3';
                isvalid = false;
            }

            //////////////////

            var Timemax1 = jQuery("#<%= txtTimeFrameMax1.ClientID %>").val();
            var Timemin1 = jQuery("#<%= txtTimeFrameMin1.ClientID %>").val();
            if (parseInt(Timemax1, 10) <= parseInt(Timemin1, 10)) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Maximum timeframe should be greater then minimum timefram section 1';
                isvalid = false;
            }

            var Timemax2 = jQuery("#<%= txtTimeFrameMax2.ClientID %>").val();
            var Timemin2 = jQuery("#<%= txtTimeFrameMin2.ClientID %>").val();
            if (parseInt(Timemax2, 10) <= parseInt(Timemin2, 10)) {

                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Maximum timeframe should be greater then minimum timefram section 2';
                isvalid = false;
            }

            var Timemax3 = jQuery("#<%= txtTimeFrameMax3.ClientID %>").val();
            var Timemin3 = jQuery("#<%= txtTimeFrameMin3.ClientID %>").val();
            if (parseInt(Timemax3, 10) <= parseInt(Timemin3, 10)) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Maximum timeframe should be greater then minimum timefram section 3';
                isvalid = false;
            }

            var Timemax4 = jQuery("#<%= txtTimeFrameMax4.ClientID %>").val();
            var Timemin4 = jQuery("#<%= txtTimeFrameMin4.ClientID %>").val();
            if (parseInt(Timemax4, 10) <= parseInt(Timemin4, 10)) {

                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Maximum timeframe should be greater then minimum timefram section 4';
                isvalid = false;
            }


            if (!isvalid) {
                jQuery("#<%=divmessage.ClientID %>").show();
                jQuery("#<%=divmessage.ClientID %>").html(errormessage);
                var offseterror = jQuery("#<%=divmessage.ClientID %>").offset();
                jQuery("body").scrollTop(offseterror.top);
                jQuery("html").scrollTop(offseterror.top);
                return false;
            }

            jQuery("#<%=divmessage.ClientID %>").hide();
            jQuery("#<%=divmessage.ClientID %>").html('');
            jQuery("body").scrollTop(0);
            jQuery("html").scrollTop(0);
            jQuery("#Loding_overlaySec span").html("Saving...");
            jQuery("#Loding_overlaySec").show();
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';

        });


        jQuery("#<%= btnUpdate.ClientID %>").bind("click", function () {
            if (jQuery("#<%= divmessage.ClientID %>").hasClass("succesfuly")) {
                jQuery("#<%= divmessage.ClientID %>").removeClass("succesfuly").addClass("error").hide();
            }
            var isvalid = true;
            var errormessage = "";


            var policyName = jQuery("#<%= txtName.ClientID %>").val();
            if (policyName.length <= 0 || policyName == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter policy name';
                isvalid = false;
            }

            var cancellationlimit = jQuery("#<%= txtCancellationLimit.ClientID %>").val();
            if (cancellationlimit.length <= 0 || cancellationlimit == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter cancellation limit';
                isvalid = false;
            }


            if (parseInt(cancellationlimit, 10) > "60") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Cancellation limit must be 60';
                isvalid = false;
            }

            var min1 = jQuery("#<%= txtTimeFrameMin1.ClientID %>").val();
            if (min1.length <= 0 || min1 == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter minimum TimeFrame of section 1';
                isvalid = false;
            }

            var frame = jQuery("#<%=CKEditorSection1.ClientID %>").siblings().find('span').find('iframe').get(0);
            var body = jQuery(frame).contents().find('body');
            var text = jQuery(body).text();
            if (text == '') {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter policy in section 1';
                isvalid = false;

            }

            var max2 = jQuery("#<%= txtTimeFrameMax2.ClientID %>").val();
            if (max2.length <= 0 || max2 == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter maximum TimeFrame of section 2';
                isvalid = false;
            }

            var min2 = jQuery("#<%= txtTimeFrameMin2.ClientID %>").val();
            if (min2.length <= 0 || min2 == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter minimum TimeFrame of section 2';
                isvalid = false;
            }

            frame = jQuery("#<%=CKEditorSection2.ClientID %>").siblings().find('span').find('iframe').get(0);
            body = jQuery(frame).contents().find('body');
            text = jQuery(body).text();
            if (text == '') {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter policy in section 2';
                isvalid = false;

            }

            var max3 = jQuery("#<%= txtTimeFrameMax3.ClientID %>").val();
            if (max3.length <= 0 || max3 == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter maximum TimeFrame of section 3';
                isvalid = false;
            }

            var min3 = jQuery("#<%= txtTimeFrameMin3.ClientID %>").val();
            if (min3.length <= 0 || min3 == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter minimum TimeFrame of section 3';
                isvalid = false;
            }

            frame = jQuery("#<%=CKEditorSection3.ClientID %>").siblings().find('span').find('iframe').get(0);
            body = jQuery(frame).contents().find('body');
            text = jQuery(body).text();
            if (text == '') {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter policy in section 3';
                isvalid = false;

            }

            var max4 = jQuery("#<%= txtTimeFrameMax4.ClientID %>").val();
            if (max4.length <= 0 || max4 == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter maximum TimeFrame of section 4';
                isvalid = false;
            }

            frame = jQuery("#<%=CKEditorSection4.ClientID %>").siblings().find('span').find('iframe').get(0);
            body = jQuery(frame).contents().find('body');
            text = jQuery(body).text();
            if (text == '') {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Enter policy in section 4';
                isvalid = false;

            }
            var Timemax2 = jQuery("#<%= txtTimeFrameMax2.ClientID %>").val();
            var Timemin1 = jQuery("#<%= txtTimeFrameMin1.ClientID %>").val();
            if (parseInt(Timemax2, 10) > parseInt(Timemin1, 10)) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Maximum timefame section 2 should be smaller to minimum timeframe of section 1';
                isvalid = false;
            }

            var Timemax3 = jQuery("#<%= txtTimeFrameMax3.ClientID %>").val();
            var Timemin2 = jQuery("#<%= txtTimeFrameMin2.ClientID %>").val();
            if (parseInt(Timemax3, 10) > parseInt(Timemin2, 10)) {

                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Maximum timefame section 3 should be smaller to minimum timeframe of section 2';
                isvalid = false;
            }

            var Timemax4 = jQuery("#<%= txtTimeFrameMax4.ClientID %>").val();
            var Timemin3 = jQuery("#<%= txtTimeFrameMin3.ClientID %>").val();
            if (parseInt(Timemax4, 10) > parseInt(Timemin3, 10)) {

                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Maximum timefame section 4 should be smaller to minimum timeframe of section 3';
                isvalid = false;
            }            //////////////////

            var Timemax1 = jQuery("#<%= txtTimeFrameMax1.ClientID %>").val();
            var Timemin1 = jQuery("#<%= txtTimeFrameMin1.ClientID %>").val();
            if (parseInt(Timemax1, 10) <= parseInt(Timemin1, 10)) {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Maximum timeframe should be greater then minimum timefram section 1';
                isvalid = false;
            }

            var Timemax2 = jQuery("#<%= txtTimeFrameMax2.ClientID %>").val();
            var Timemin2 = jQuery("#<%= txtTimeFrameMin2.ClientID %>").val();
            if (parseInt(Timemax2, 10) <= parseInt(Timemin2, 10)) {

                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Maximum timeframe should be greater then minimum timefram section 2';
                isvalid = false;
            }

            var Timemax3 = jQuery("#<%= txtTimeFrameMax3.ClientID %>").val();
            var Timemin3 = jQuery("#<%= txtTimeFrameMin3.ClientID %>").val();
            if (parseInt(Timemax3, 10) <= parseInt(Timemin3, 10)) {
               
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Maximum timeframe should be greater then minimum timefram section 3';
                isvalid = false;
            }

            var Timemax4 = jQuery("#<%= txtTimeFrameMax4.ClientID %>").val();
            var Timemin4 = jQuery("#<%= txtTimeFrameMin4.ClientID %>").val();
            if (parseInt(Timemax4, 10) <= parseInt(Timemin4, 10)) {

                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += 'Maximum timeframe should be greater then minimum timefram section 4';
                isvalid = false;
            }

            if (!isvalid) {
                jQuery("#<%=divmessage.ClientID %>").show();
                jQuery("#<%=divmessage.ClientID %>").html(errormessage);
                var offseterror = jQuery("#<%=divmessage.ClientID %>").offset();
                jQuery("body").scrollTop(offseterror.top);
                jQuery("html").scrollTop(offseterror.top);
                return false;
            }

            jQuery("#<%=divmessage.ClientID %>").hide();
            jQuery("#<%=divmessage.ClientID %>").html('');
            jQuery("body").scrollTop(0);
            jQuery("html").scrollTop(0);
            jQuery("#Loding_overlaySec span").html("Saving...");
            jQuery("#Loding_overlaySec").show();
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';

        });

        function SetFocusBottom(val) {

            var ofset = jQuery("#" + val).offset();
            jQuery('body').scrollTop(ofset.top);
            jQuery('html').scrollTop(ofset.top);
        }
        
    </script>
</asp:Content>
