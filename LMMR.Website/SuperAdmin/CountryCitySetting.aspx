﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuperAdmin/Main.master" AutoEventWireup="true"
    CodeFile="CountryCitySetting.aspx.cs" Inherits="SuperAdmin_CountryCitySetting" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <style>
        .command
        {
            display: none;
        }
    </style>
    <style type="text/css">
        .NoClassApply
        {
            width: 204px;
            padding: 2px;
        }
    </style>
    <style type="text/css">
        .modalBackground
        {
            background-color: #CCCCFF;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }
        .water
        {
            color: Gray;
        }
        .style1
        {
            height: 25px;
        }
        .n-btn
        {
            height: 15px;
        }
        .style2
        {
            width: 100%;
        }
        .inputbox
        {
        }
    </style>
    <style type="text/css">
        .floatleft
        {
            float: left;
        }
    </style>
    <script type="text/javascript">
        function LoadMapLatLong(hf_latitude, hf_longtitude) {

            var map = new google.maps.Map(document.getElementById('map_canvas'), {
                zoom: 14,
                center: new google.maps.LatLng(document.getElementById("hf_latitude").value, document.getElementById("hf_longtitude").value),
                mapTypeId: google.maps.MapTypeId.ROADMAP

            });

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(document.getElementById("hf_latitude").value, document.getElementById("hf_longtitude").value),
                map: map
            });

        }

        function SelectTabShowHide(tab) {
            if (tab == "country") {
                document.getElementById('divcities').style.display = 'none';
                document.getElementById('divcountries').style.display = 'block';
                document.getElementById('DivZones').style.display = 'none';
                document.getElementById('DivMainpoints').style.display = 'none';
                document.getElementById('DivCurrency').style.display = 'none';
                jQuery("#<%=divAddCountry.ClientID %>").hide();
                return false;
            }
            else if (tab == "city") {
                document.getElementById('divcities').style.display = 'block';
                document.getElementById('divcountries').style.display = 'none';
                document.getElementById('DivZones').style.display = 'none';
                document.getElementById('DivCurrency').style.display = 'none';
                document.getElementById('DivMainpoints').style.display = 'none';
                jQuery("#<%=lblemptyCity.ClientID %>").show();
                jQuery("#<%=drpcity.ClientID %>").val('0');
                jQuery("#<%=dlstcity.ClientID %>").hide();
                jQuery("#<%=DivAddNewCityButton.ClientID %>").hide();
                jQuery("#<%=DivAddCity.ClientID %>").hide();
                return false;
            }
            else if (tab == "zone") {
                document.getElementById('DivZones').style.display = 'block';
                document.getElementById('divcountries').style.display = 'none';
                document.getElementById('divcities').style.display = 'none';
                document.getElementById('DivMainpoints').style.display = 'none';
                document.getElementById('DivCurrency').style.display = 'none';
                jQuery("#<%=lblEmptyZone.ClientID %>").show();
                jQuery("#<%=DlstZone1.ClientID %>").val('0');
                jQuery("#<%=dlstzone.ClientID %>").hide();
                jQuery("#<%=DivAddZoneButton.ClientID %>").hide();
                jQuery("#<%=DivAddZone.ClientID %>").hide();
                jQuery("#<%=dropZoneSectionCountry.ClientID %>").val('0');
                return false;
            }
            else if (tab == "point") {
                document.getElementById('DivMainpoints').style.display = 'block';
                document.getElementById('divcountries').style.display = 'none';
                document.getElementById('divcities').style.display = 'none';
                document.getElementById('DivZones').style.display = 'none';
                document.getElementById('DivCurrency').style.display = 'none';
                jQuery("#<%=lblEmptyCityPoint.ClientID %>").show();
                jQuery("#<%=drpcityZone.ClientID %>").val('0');
                jQuery("#<%=dlstMainpoints.ClientID %>").hide();
                jQuery("#<%=DivAddPointButton.ClientID %>").hide();
                jQuery("#<%=DivAddPoints.ClientID %>").hide();
                jQuery("#<%=dropCountryPoint.ClientID %>").val('0');
                return false;
            }
            else if (tab == "currency") {
                document.getElementById('DivMainpoints').style.display = 'none';
                document.getElementById('divcountries').style.display = 'none';
                document.getElementById('divcities').style.display = 'none';
                document.getElementById('DivZones').style.display = 'none';
                document.getElementById('DivCurrency').style.display = 'block';
                jQuery("#<%=divaddcurrency1.ClientID %>").hide();
                return false;

            }
        }
    </script>
    <div id="divMainMessage" runat="server" style="display: block;">
    </div>
    <div class="cancelation-heading-body3" id="HotelAssignTab" runat="server">
        <asp:HiddenField ID="hf_address" runat="server" />
        <asp:HiddenField ID="hf_latitude" runat="server" />
        <asp:HiddenField ID="hf_longtitude" runat="server" />
        <ul id="tabgroup">
            <li tabindex="0" id="liCountry" runat="server" class="select" style="cursor: pointer;"
                onclick="return SelectTabShowHide('country')"><a class="select">Country</a></li>
            <li tabindex="0" id="liCities" runat="server" style="cursor: pointer;" onclick="return SelectTabShowHide('city')">
                <a>City</a></li>
            <li tabindex="0" id="li1" runat="server" style="cursor: pointer;" onclick="return SelectTabShowHide('zone')">
                <a>Zone</a></li>
            <li tabindex="0" id="li2" runat="server" style="cursor: pointer;" onclick="return SelectTabShowHide('point')">
                <a>City point</a></li>
            <li tabindex="0" id="li3" runat="server" style="cursor: pointer;" onclick="return SelectTabShowHide('currency')">
                <a>Currency</a></li>
        </ul>
    </div>
    <asp:UpdatePanel ID="updpnl" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hdnItemId" runat="server" />
            <asp:HiddenField ID="hdnId" runat="server" />
            <asp:HiddenField ID="hdfcityid" runat="server" />
            <asp:HiddenField ID="hdnId1" runat="server" />
            <asp:HiddenField ID="hdnzoneadd" runat="server" />
            <asp:HiddenField ID="hdnzoneid" runat="server" />
            <asp:HiddenField ID="hdfpointname" runat="server" />
            <asp:HiddenField ID="hdnId2" runat="server" />
            <asp:HiddenField ID="HiddenField1" runat="server" />
            <asp:HiddenField ID="hdfcurrency" runat="server" />
            <div style="float: left;">
                <div class="superadmin-example-layout">
                    <div class="superadmin-cms" id="divcountries" style="display: block;">
                        <div class="superadmin-cms-box1" style="width: 100%; border-right: 1px solid #92BEDE;
                            margin-bottom: 10px; background-color: #FFFFFF; padding-bottom: 0px;">
                            <div style="border-bottom: #A3D4F7 solid 1px; border-top: #A3D4F7 solid 1px; padding: 0px 0px 0px 0px;">
                                <h3 style="background-color: #FFFFFF; height: 31px; border: none; padding: 0px 0px 0px 0px;
                                    font-size: 21px; font-weight: bold; margin-left: 10px;">
                                    Country List</h3>
                            </div>
                            <br />
                            <br />
                            <div style="float: left; margin-right: 10px; margin-top: -22px;">
                                <div class="n-commisions" style="float: left; margin-left: 10px;">
                                    <div class="n-btn">
                                        <asp:LinkButton ID="lnkAddNew" runat="server" OnClick="lnkAddNew_Click">
                                    <div class="n-btn-left">
                                    </div>
                                    <div class="n-btn-mid">
                                        Add New Country</div>
                                    <div class="n-btn-right">
                                    </div>
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div style="background-color: #ECF7FE; float: left; width: 100%; padding-bottom: 10px;
                                border-top: #A3D4F7 solid 1px;">
                                <div id="lblemptycountry" runat="server" visible="false" style="padding-top: 5px;">
                                    <center>
                                        <span><b>No record found</b></span></center>
                                </div>
                                <ul>
                                    <asp:DataList runat="server" ID="rptcountrylist" RepeatDirection="Horizontal" RepeatColumns="5"
                                        OnItemDataBound="rptcountrylist_ItemDataBound1" OnItemCommand="rptcountrylist_ItemCommand">
                                        <ItemTemplate>
                                            <li style="width: 175px;">
                                                <div style="float: left; margin-left: 5px; width: 70%;">
                                                    <asp:Label ID="lblcountryname" runat="server"></asp:Label>
                                                </div>
                                                <div style="float: right; margin-right: 5px;" class="command">
                                                    <asp:LinkButton ID="lnkModify" runat="server" CommandName="modifyme"><img src="../Images/modify.png" alt="modify" height="15px" width="15px" /></asp:LinkButton><asp:LinkButton
                                                        ID="lnkDelete" runat="server" CommandName="deleteme" OnClientClick="return confirm('Are you sure you want to delete this country?');"><img height="15px" width="15px" src="../Images/delete-ol-btn.png" alt="delete" /></asp:LinkButton></div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </ul>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="superadmin-example-mainbody" id="divAddCountry" runat="server" visible="false">
                            <div id="divMessage" runat="server">
                            </div>
                            <div class="superadmin-mainbody-sub1" style="padding-left: 0px;">
                                <div class="superadmin-mainbody-sub1-left" style="width: 100%;">
                                    <h2>
                                        <asp:Label ID="lblHeader" runat="server">Add Country</asp:Label></h2>
                                </div>
                            </div>
                            <table style="margin-left: -1px;" width="100%" bgcolor="#92bddd" cellspacing="1"
                                cellpadding="0">
                                <tr bgcolor="#E3F0F1">
                                    <td width="15%" style="line-height: 31px; padding-left: 5px;">
                                        <strong>Country name:</strong>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtcountry" runat="server" MaxLength="20" TabIndex="1" Width="200px"
                                            CssClass="txtpadding" />
                                    </td>
                                </tr>
                                <tr bgcolor="#F1F8F8">
                                    <td width="15%" style="line-height: 31px; padding-left: 5px;">
                                        <strong>Active(Yes/No) :</strong>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="drpIsforall" runat="server" CssClass="txtpadding">
                                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                            <div style="border-bottom: 1px solid #92BDDD; width: 100%; float: left; padding-top: 10px;
                                padding-bottom: 10px;">
                                <div class="button_section">
                                    <asp:LinkButton ID="lnkSave" runat="server" CssClass="select" ValidationGroup="LOGIN"
                                        OnClientClick="return Savecountry();" OnClick="lnkSave_Click">Save</asp:LinkButton>
                                    &nbsp;or&nbsp;
                                    <asp:LinkButton ID="lnkCancelcountry" CssClass="cancelpop" runat="server" OnClick="lnkCancelcountry_Click">Cancel</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="superadmin-cms" id="divcities" style="display: none;">
                        <div class="superadmin-cms-box1" style="width: 100%; border-right: 1px solid #92BEDE;
                            margin-bottom: 10px; background-color: #FFFFFF; padding-bottom: 0px;">
                            <div style="border-bottom: #A3D4F7 solid 1px; border-top: #A3D4F7 solid 1px; padding: 0px 0px 0px 0px;">
                                <h3 style="background-color: #FFFFFF; height: 31px; border: none; padding: 0px 0px 0px 0px;
                                    font-size: 21px; font-weight: bold; margin-left: 10px;">
                                    City List</h3>
                            </div>
                            <div style="float: left; margin-right: 10px; width: 100%; height: 30px; border-bottom: #A3D4F7 solid 1px;
                                padding-bottom: 10px; padding-top: 10px; margin-bottom: 10px;" runat="server"
                                id="DivAddNewCityButton">
                                <div class="n-commisions" style="float: left; margin-left: 10px;">
                                    <div class="n-btn">
                                        <asp:LinkButton ID="LnkAddNewCity" runat="server" OnClick="LnkAddNewCity_Click">
                                    <div class="n-btn-left">
                                    </div>
                                    <div class="n-btn-mid">
                                        Add New City</div>
                                    <div class="n-btn-right">
                                    </div>
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top: 10px; padding-bottom: 10px; padding-left: 10px;">
                                <b>Select country :</b>
                                <asp:DropDownList ID="drpcity" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpcity_SelectedIndexChanged" />
                            </div>
                            <div style="background-color: #ECF7FE; float: left; width: 100%; padding-bottom: 10px;
                                border-top: #A3D4F7 solid 1px;">
                                <div id="lblemptyCity" runat="server" style="padding-top: 5px; display: none;">
                                    <center>
                                        <span><b>No record found</b></span></center>
                                </div>
                                <ul>
                                    <asp:DataList runat="server" ID="dlstcity" RepeatDirection="Horizontal" RepeatColumns="5"
                                        OnItemDataBound="dlstcity_ItemDataBound" OnItemCommand="dlstcity_ItemCommand">
                                        <ItemTemplate>
                                            <li style="width: 175px;">
                                                <div style="float: left; margin-left: 5px; width: 70%;">
                                                    <asp:Label ID="lblcityname" runat="server"></asp:Label>
                                                </div>
                                                <div style="float: right; margin-right: 5px;" class="command">
                                                    <b>
                                                        <asp:LinkButton ID="lnkModify" runat="server" CommandName="modifyme"><img src="../Images/modify.png" alt="modify" height="15px" width="15px" /></asp:LinkButton><asp:LinkButton
                                                            ID="lnkDelete" runat="server" CommandName="deleteme" OnClientClick="return confirm('Are you sure you want to delete this City?');"><img height="15px" width="15px" src="../Images/delete-ol-btn.png" alt="delete" /></asp:LinkButton></b></div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </ul>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="superadmin-example-mainbody" id="DivAddCity" runat="server" visible="false">
                            <div id="divmessagecity" runat="server">
                            </div>
                            <div class="superadmin-mainbody-sub1" style="padding-left: 0px;">
                                <div class="superadmin-mainbody-sub1-left" style="width: 100%;">
                                    <h2>
                                        <asp:Label ID="lblheadercity" runat="server">Add City</asp:Label></h2>
                                </div>
                            </div>
                            <table style="margin-left: -1px;" width="100%" bgcolor="#92bddd" cellspacing="1"
                                cellpadding="0">
                                <%--<tr bgcolor="#E3F0F1" style="display:none">
                                    <td width="15%" style="line-height: 31px; padding-left: 5px;">
                                        <strong>Select country:</strong>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="drpaddcity" runat="server" CssClass="txtpadding" />
                                    </td>
                                </tr>--%>
                                <tr bgcolor="#E3F0F1">
                                    <td width="15%" style="line-height: 31px; padding-left: 5px;">
                                        <strong>City:</strong>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtcity" runat="server" MaxLength="25" TabIndex="1" Width="200px"
                                            CssClass="txtpadding" />
                                    </td>
                                </tr>
                            </table>
                            <div style="border-bottom: 1px solid #92BDDD; width: 100%; float: left; padding-top: 10px;
                                padding-bottom: 10px;">
                                <div class="button_section">
                                    <asp:LinkButton ID="lnkcity" runat="server" CssClass="select" OnClientClick="return Savecity();"
                                        OnClick="lnkcity_Click"> Save</asp:LinkButton>
                                    &nbsp;or&nbsp;
                                    <asp:LinkButton ID="lnkCancelcity" runat="server" CssClass="cancelpop" OnClick="lnkCancelcity_Click">Cancel</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="superadmin-cms" id="DivZones" style="display: none;">
                        <div class="superadmin-cms-box1" style="width: 100%; border-right: 1px solid #92BEDE;
                            margin-bottom: 10px; background-color: #FFFFFF; padding-bottom: 0px;">
                            <div style="border-bottom: #A3D4F7 solid 1px; border-top: #A3D4F7 solid 1px; padding: 0px 0px 0px 0px;">
                                <h3 style="background-color: #FFFFFF; height: 31px; border: none; padding: 0px 0px 0px 0px;
                                    font-size: 21px; font-weight: bold; margin-left: 10px;">
                                    Zone List</h3>
                            </div>
                            <div style="float: left; margin-right: 10px; width: 100%; height: 30px; border-bottom: #A3D4F7 solid 1px;
                                padding-bottom: 10px; padding-top: 10px; margin-bottom: 10px;" runat="server"
                                id="DivAddZoneButton">
                                <div class="n-commisions" style="float: left; margin-left: 10px;">
                                    <div class="n-btn">
                                        <asp:LinkButton ID="lnkaddnewzone" runat="server" OnClick="lnkaddnewzone_Click">
                                    <div class="n-btn-left">
                                    </div>
                                    <div class="n-btn-mid">
                                        Add New Zone</div>
                                    <div class="n-btn-right">
                                    </div>
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top: 10px; padding-bottom: 5px; padding-left: 10px;">
                                <b>Select country :</b>
                                <asp:DropDownList ID="dropZoneSectionCountry" runat="server" AutoPostBack="true"
                                    OnSelectedIndexChanged="dropZoneSectionCountry_SelectedIndexChanged" />
                            </div>
                            <div style="padding-top: 5px; padding-bottom: 10px; padding-left: 10px;">
                                <b>Select city : </b>
                                <asp:DropDownList ID="DlstZone1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DlstZone1_SelectedIndexChanged" style="margin-left:23px"  />
                            </div>
                            <div style="background-color: #ECF7FE; float: left; width: 100%; padding-bottom: 10px;
                                border-top: #A3D4F7 solid 1px;">
                                <div id="lblEmptyZone" runat="server" style="padding-top: 5px; display: none;">
                                    <center>
                                        <span><b>No record found</b></span></center>
                                </div>
                                <ul>
                                    <asp:DataList runat="server" ID="dlstzone" RepeatDirection="Horizontal" RepeatColumns="3"
                                        OnItemDataBound="dlstzone_ItemDataBound" OnItemCommand="dlstzone_ItemCommand">
                                        <ItemTemplate>
                                            <li style="width: 290px;">
                                                <div style="float: left; margin-left: 5px; width: 70%;">
                                                    <asp:Label ID="lblzonename" runat="server"></asp:Label>
                                                </div>
                                                <div style="float: right; margin-right: 5px;" class="command">
                                                    <b>
                                                        <asp:LinkButton ID="lnkModify" runat="server" CommandName="modifyme"><img src="../Images/modify.png" alt="modify" height="15px" width="15px" /></asp:LinkButton><asp:LinkButton
                                                            ID="lnkDelete" runat="server" CommandName="deleteme" OnClientClick="return confirm('Are you sure you want to delete this Zone?');"><img height="15px" width="15px" src="../Images/delete-ol-btn.png" alt="delete" /></asp:LinkButton></b></div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </ul>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="superadmin-example-mainbody" id="DivAddZone" runat="server" visible="false">
                            <div class="superadmin-mainbody-sub1" style="padding-left: 0px;">
                                <div class="superadmin-mainbody-sub1-left" style="width: 100%;">
                                    <h2>
                                        <asp:Label ID="lblheaderzone" runat="server">Add Zone</asp:Label></h2>
                                </div>
                            </div>
                            <div id="Divzonemesssage" runat="server" style="float: left; width: 94%;">
                            </div>
                            <table style="margin-left: -1px;" width="100%" bgcolor="#92bddd" cellspacing="1"
                                cellpadding="0">
                                <%--<tr bgcolor="#E3F0F1">
                                    <td width="15%" style="line-height: 31px; padding-left: 5px;">
                                        <strong>Select city:</strong>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="drpcityforzoneadd" runat="server" CssClass="txtpadding" />
                                    </td>
                                </tr>--%>
                                <tr bgcolor="#E3F0F1">
                                    <td width="15%" style="line-height: 31px; padding-left: 5px;">
                                        <strong>Zone:</strong>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtzone" runat="server" MaxLength="25" TabIndex="1" Width="200px"
                                            CssClass="txtpadding" />
                                    </td>
                                </tr>
                            </table>
                            <div style="border-bottom: 1px solid #92BDDD; width: 100%; float: left; padding-top: 10px;
                                padding-bottom: 10px;">
                                <div class="button_section">
                                    <asp:LinkButton ID="lnkzone" runat="server" CssClass="select" OnClick="lnkzone_Click"
                                        OnClientClick="return SaveZone();"> Save</asp:LinkButton>
                                    &nbsp;or&nbsp;
                                    <asp:LinkButton ID="lnkCancelzone" runat="server" CssClass="cancelpop" OnClick="lnkCancelzone_Click">Cancel</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="superadmin-cms" id="DivMainpoints" style="display: block;">
                        <div class="superadmin-cms-box1" style="width: 100%; border-right: 1px solid #92BEDE;
                            margin-bottom: 10px; background-color: #FFFFFF; padding-bottom: 0px;">
                            <div style="border-bottom: #A3D4F7 solid 1px; border-top: #A3D4F7 solid 1px; padding: 0px 0px 0px 0px;">
                                <h3 style="background-color: #FFFFFF; height: 31px; border: none; padding: 0px 0px 0px 0px;
                                    font-size: 21px; font-weight: bold; margin-left: 10px;">
                                    City Point List</h3>
                            </div>
                            <div style="float: left; margin-right: 10px; width: 100%; height: 30px; border-bottom: #A3D4F7 solid 1px;
                                padding-bottom: 10px; padding-top: 10px; margin-bottom: 10px;" runat="server"
                                id="DivAddPointButton">
                                <div class="n-commisions" style="float: left; margin-left: 10px;">
                                    <div class="n-btn">
                                        <asp:LinkButton ID="lnkaddnewmainpoint" runat="server" OnClick="lnkaddnewmainpoint_Click">
                                    <div class="n-btn-left">
                                    </div>
                                    <div class="n-btn-mid">
                                        Add New Point</div>
                                    <div class="n-btn-right">
                                    </div>
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div style="padding-top: 10px; padding-bottom: 5px; padding-left: 10px;">
                                <b>Select country :</b>
                                <asp:DropDownList ID="dropCountryPoint" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dropCountryPoint_SelectedIndexChanged" />
                            </div>
                            <div style="padding-top: 5px; padding-bottom: 10px; padding-left: 10px;">
                                <b>Select city :</b>
                                <asp:DropDownList ID="drpcityZone" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpcityZone_SelectedIndexChanged" style="margin-left:23px" />
                            </div>
                            <div style="background-color: #ECF7FE; float: left; width: 100%; padding-bottom: 10px;
                                border-top: #A3D4F7 solid 1px;">
                                <div id="lblEmptyCityPoint" runat="server" style="padding-top: 5px;">
                                    <center>
                                        <span><b>No record found</b></span></center>
                                </div>
                                <ul>
                                    <asp:DataList runat="server" ID="dlstMainpoints" RepeatDirection="Horizontal" RepeatColumns="3"
                                        OnItemDataBound="dlstMainpoints_ItemDataBound" OnItemCommand="dlstMainpoints_ItemCommand">
                                        <ItemTemplate>
                                            <li style="width: 290px;">
                                                <div style="float: left; margin-left: 5px; width: 70%;">
                                                    <asp:Label ID="lblmainpoints" runat="server"></asp:Label>
                                                </div>
                                                <div style="float: right; margin-right: 5px;" class="command">
                                                    <b>
                                                        <asp:LinkButton ID="lnkModify" runat="server" CommandName="modifyme"><img src="../Images/modify.png" alt="modify" height="15px" width="15px" /></asp:LinkButton><asp:LinkButton
                                                            ID="lnkDelete" runat="server" CommandName="deleteme" OnClientClick="return confirm('Are you sure you want to delete this Mainpoint?');"><img height="15px" width="15px" src="../Images/delete-ol-btn.png" alt="delete" /></asp:LinkButton></b></div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </ul>
                            </div>
                        </div>
                        <br />
                        <br />
                        <%--<asp:UpdatePanel ID="up1" runat="server">
                            <ContentTemplate>--%>
                        <div class="superadmin-example-mainbody" id="DivAddPoints" runat="server" visible="false"
                            style="margin-left: 0px;">
                            <div id="divmessagepoint" runat="server">
                            </div>
                            <div class="superadmin-mainbody-sub1" style="padding-left: 0px;">
                                <div class="superadmin-mainbody-sub1-left" style="width: 100%;">
                                    <h2>
                                        <asp:Label ID="lblheaderpointname" runat="server">Add City Point</asp:Label></h2>
                                </div>
                            </div>
                            <table style="border: 1px #92bddd solid;" width="width=100%">
                                <tr>
                                    <td valign="top">
                                        <table width="100%" style="margin-left: -1px;" bgcolor="#92bddd" cellspacing="1"
                                            cellpadding="0">
                                            <%--<div id="divvisible" runat="server" visible="false">--%>
                                            <%-- <tr bgcolor="#E3F0F1">
                                                <td width="40%" style="line-height: 31px; padding-left: 5px;" valign="top">
                                                    <strong>Select country :</strong>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="drppointcountry" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drppointcountry_SelectedIndexChanged"
                                                        CssClass="txtpadding" />
                                                </td>
                                            </tr>
                                            <tr bgcolor="#F1F8F8">
                                                <td width="40%" style="line-height: 31px; padding-left: 5px;">
                                                    <strong>Select city :</strong>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="drpcityforaddmainpoints" runat="server" AutoPostBack="true"
                                                        OnSelectedIndexChanged="drpcityforaddmainpoints_SelectedIndexChanged" CssClass="txtpadding" />
                                                </td>
                                            </tr>
                                            --%>
                                            <%--</div>--%>
                                            <tr bgcolor="#E3F0F1">
                                                <td width="40%" style="line-height: 31px; padding-left: 5px;">
                                                    <strong>Point name :</strong>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtpoint" runat="server" MaxLength="25" CssClass="txtpadding"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr bgcolor="#F1F8F8">
                                                <td width="40%" style="line-height: 31px; padding-left: 5px;">
                                                    <strong>Latitude / Longitude :</strong>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="lblLatitude" runat="server" MaxLength="10" CssClass="txtpadding"></asp:TextBox><asp:FilteredTextBoxExtender
                                                        ID="FilteredTextBoxExtender2" TargetControlID="lblLatitude" ValidChars="-0123456789."
                                                        runat="server">
                                                    </asp:FilteredTextBoxExtender>
                                                    /<asp:TextBox ID="lblLongitude" runat="server" MaxLength="10" CssClass="txtpadding"></asp:TextBox><asp:FilteredTextBoxExtender
                                                        ID="FilteredTextBoxExtender3" TargetControlID="lblLongitude" ValidChars="-0123456789."
                                                        runat="server">
                                                    </asp:FilteredTextBoxExtender>
                                                </td>
                                            </tr>

                                            <tr bgcolor="#E3F0F1">
                                                <td width="40%" style="line-height: 31px; padding-left: 5px;">
                                                    <strong>Is center point :</strong>
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkboxIsCenter" runat="server" Checked="false" CssClass="txtpadding" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <div style="width: 614px; height: 260px; margin: 5px;" id="map_canvas">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <div style="border-bottom: 1px solid #92BDDD; width: 100%; float: left; padding-top: 10px;
                                padding-bottom: 10px;">
                                <div class="button_section">
                                    <asp:LinkButton ID="lblmainpoint" runat="server" OnClick="lblmainpoint_Click" CssClass="select"
                                        OnClientClick="return savemainpoints();"> Save</asp:LinkButton>
                                    &nbsp;or&nbsp;
                                    <asp:LinkButton ID="lnkCancelpoint" runat="server" CssClass="cancelpop" OnClick="lnkCancelpoint_Click">Cancel</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <%--                            </ContentTemplate>
                        </asp:UpdatePanel>--%>
                    </div>
                    <div class="superadmin-cms" id="DivCurrency" style="display: none;">
                        <div class="superadmin-cms-box1" style="width: 100%; border-right: 1px solid #92BEDE;
                            margin-bottom: 10px; background-color: #FFFFFF; padding-bottom: 0px;">
                            <div style="border-bottom: #A3D4F7 solid 1px; border-top: #A3D4F7 solid 1px; padding: 0px 0px 0px 0px;">
                                <h3 style="background-color: #FFFFFF; height: 31px; border: none; padding: 0px 0px 0px 0px;
                                    font-size: 21px; font-weight: bold; margin-left: 10px;">
                                    Currency List</h3>
                            </div>
                            <br />
                            <br />
                            <div style="float: left; margin-right: 10px; margin-top: -22px;">
                                <div class="n-commisions" style="float: left; margin-left: 10px;">
                                    <div class="n-btn">
                                        <asp:LinkButton ID="lnkcurrencyadd" runat="server" OnClick="lnkcurrencyadd_Click">
                                    <div class="n-btn-left">
                                    </div>
                                    <div class="n-btn-mid">
                                        Add New Currency</div>
                                    <div class="n-btn-right">
                                    </div>
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div style="background-color: #ECF7FE; float: left; width: 100%; padding-bottom: 10px;
                                border-top: #A3D4F7 solid 1px;">
                                <div id="lblEmptyCurrency" runat="server" visible="false" style="padding-top: 5px;">
                                    <center>
                                        <span><b>No record found</b></span></center>
                                </div>
                                <ul>
                                    <asp:DataList runat="server" ID="dlstCurrency" RepeatDirection="Horizontal" RepeatColumns="3"
                                        OnItemDataBound="dlstCurrency_ItemDataBound" OnItemCommand="dlstCurrency_ItemCommand">
                                        <ItemTemplate>
                                            <li style="width: 290px;">
                                                <div style="float: left; margin-left: 5px; width: 70%;">
                                                    <asp:Label ID="lblcurrency" runat="server" ></asp:Label>
                                                </div>
                                                <div style="float: right; margin-right: 5px;" class="command">
                                                    <b>
                                                        <asp:LinkButton ID="lnkModify" runat="server" CommandName="modifyme"><img src="../Images/modify.png" alt="modify" height="15px" width="15px" /></asp:LinkButton><asp:LinkButton
                                                            ID="lnkDelete" runat="server" CommandName="deleteme" OnClientClick="return confirm('Are you sure you want to delete this Currency?');"><img height="15px" width="15px" src="../Images/delete-ol-btn.png" alt="delete" /></asp:LinkButton></b></div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </ul>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="superadmin-example-mainbody" id="divaddcurrency1" runat="server" visible="false">
                            <div id="divmessagecurrency" runat="server">
                            </div>
                            <div class="superadmin-mainbody-sub1" style="padding-left: 0px;">
                                <div class="superadmin-mainbody-sub1-left" style="width: 100%;">
                                    <h2>
                                        <asp:Label ID="lblcurrency" runat="server">Add Currency</asp:Label></h2>
                                </div>
                            </div>
                            <table style="margin-left: -1px;" width="100%" bgcolor="#92bddd" cellspacing="1"
                                cellpadding="0">
                                <tr bgcolor="#E3F0F1">
                                    <td width="15%" style="line-height: 31px; padding-left: 5px;">
                                        <strong>Currency:</strong>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtcurrency" runat="server" MaxLength="20" TabIndex="1" Width="200px"
                                            CssClass="txtpadding" />
                                    </td>
                                </tr>
                                <tr bgcolor="#F1F8F8">
                                    <td width="15%" style="line-height: 31px; padding-left: 5px;">
                                        <strong>Signature:</strong>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtsignature" runat="server" MaxLength="20" TabIndex="1" Width="200px"
                                            CssClass="txtpadding" />
                                    </td>
                                </tr>
                            </table>
                            <div style="border-bottom: 1px solid #92BDDD; width: 100%; float: left; padding-top: 10px;
                                padding-bottom: 10px;">
                                <div class="button_section">
                                    <asp:LinkButton ID="lnksavecurrency" runat="server" CssClass="select" OnClick="lnksavecurrency_Click"
                                        OnClientClick="return Savecurrency();"> Save</asp:LinkButton>
                                    &nbsp;or&nbsp;
                                    <asp:LinkButton ID="lnkcancelcurrency" runat="server" CssClass="cancelpop" OnClick="lnkcancelcurrency_Click">Cancel</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("li").mouseover(function () { jQuery(this).css({ "background-color": "#FFFFFF" }); jQuery(this).find(".command").show() });
            //            jQuery("li").click(function () { jQuery(this).css({ "background-color": "#FFFFFF" }); jQuery(this).find(".command").show() });
            jQuery("li").mouseout(function () { jQuery(this).css({ "background-color": "" }); jQuery(this).find(".command").hide() });
        });
        function movePage(val) {
            var ofset = jQuery("#" + val).offset();
            jQuery('body').scrollTop(ofset.top);
            jQuery('html').scrollTop(ofset.top);
            jQuery('#Loding_overlay').hide();
        }
    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("li").mouseover(function () { jQuery(this).css({ "background-color": "#fff" }); jQuery(this).find(".command").show() });
            jQuery("li").mouseout(function () { jQuery(this).css({ "background-color": "" }); jQuery(this).find(".command").hide() });

        });

        function Savecountry() {
            var errorMessage = "";
            var isValid = true;
            var languageName = jQuery("#<%= txtcountry.ClientID %>").val();
            if (languageName.length <= 0) {
                if (errorMessage.length > 0) {
                    errorMessage += "<br/>";
                }
                errorMessage += "Country is required.";
                isValid = false;
            }


            if (!isValid) {
                jQuery("#<%= divMessage.ClientID %>").html(errorMessage);
                jQuery("#<%= divMessage.ClientID %>").addClass('error');
                return false;
            }
            else {
                jQuery("#<%= divMessage.ClientID %>").html('');
                jQuery("#<%= divMessage.ClientID %>").removeClass('error');
                return true;
            }
        }




        function Savecity() {
            var errorMessage = "";
            var isValid = true;
            var dropcountrycity = jQuery("#<%= drpcity.ClientID %>").val();
            if (dropcountrycity == "0") {
                if (errorMessage.length > 0) {
                    errorMessage += "<br/>";
                }
                errorMessage += 'Country is required.';
                isValid = false;
            }
            var cityName = jQuery("#<%= txtcity.ClientID %>").val();
            if (cityName.length <= 0) {
                if (errorMessage.length > 0) {
                    errorMessage += "<br/>";
                }
                errorMessage += "City is required.";

                isValid = false;
            }

            if (!isValid) {
                jQuery("#<%= divmessagecity.ClientID %>").html(errorMessage);
                jQuery("#<%= divmessagecity.ClientID %>").addClass('error');
                return false;
            }
            else {
                jQuery("#<%= divmessagecity.ClientID %>").html('');
                jQuery("#<%= divmessagecity.ClientID %>").removeClass('error');
                return true;
            }
        }

        function SaveZone() {
            var errorMessage = "";
            var isValid = true;
            var zonename = jQuery("#<%= txtzone.ClientID %>").val();
            var dropcountrycity = jQuery("#<%= DlstZone1.ClientID %>").val();
            if (dropcountrycity == "0") {
                if (errorMessage.length > 0) {
                    errorMessage += "<br/>";
                }
                errorMessage += 'City is required.';
                isValid = false;
            }

            if (zonename.length <= 0) {
                if (errorMessage.length > 0) {
                    errorMessage += "<br/>";
                }
                errorMessage += "Zone  is required.";
                isValid = false;
            }


            if (!isValid) {
                jQuery("#<%= Divzonemesssage.ClientID %>").html(errorMessage);
                jQuery("#<%= Divzonemesssage.ClientID %>").addClass('error');
                return false;
            }
            else {
                jQuery("#<%= Divzonemesssage.ClientID %>").html('');
                jQuery("#<%= Divzonemesssage.ClientID %>").removeClass('error');
                return true;
            }
        }

        function savemainpoints() {
            var errorMessage = "";
            var isValid = true;

            var drpcity = jQuery("#<%= drpcityZone.ClientID %>").val();
            if (drpcity == "0") {
                if (errorMessage.length > 0) {
                    errorMessage += "<br/>";
                }
                errorMessage += 'City is required.';
                isValid = false;
            }
            var pointname = jQuery("#<%= txtpoint.ClientID %>").val();
            if (pointname.length <= 0) {
                if (errorMessage.length > 0) {
                    errorMessage += "<br/>";
                }
                errorMessage += "Point Name  is required.";
                isValid = false;
            }
            var lblLongitude = jQuery("#<%= lblLongitude.ClientID %>").val();
            if (lblLongitude.length <= 0) {
                if (errorMessage.length > 0) {
                    errorMessage += "<br/>";
                }
                errorMessage += "Longitude  is required.";
                isValid = false;
            }
            var lblLatitude = jQuery("#<%= lblLatitude.ClientID %>").val();
            if (lblLatitude.length <= 0) {
                if (errorMessage.length > 0) {
                    errorMessage += "<br/>";
                }
                errorMessage += "Latitude  is required.";
                isValid = false;
            }


            if (!isValid) {
                jQuery("#<%= divmessagepoint.ClientID %>").html(errorMessage);
                jQuery("#<%= divmessagepoint.ClientID %>").addClass('error');
                return false;
            }
            else {
                jQuery("#<%= divmessagepoint.ClientID %>").html('');
                jQuery("#<%= divmessagepoint.ClientID %>").removeClass('error');
                return true;
            }

        }

        function Savecurrency() {
            var errorMessage = "";
            var isValid = true;
            var currency = jQuery("#<%= txtcurrency.ClientID %>").val();
            if (currency.length <= 0) {
                if (errorMessage.length > 0) {
                    errorMessage += "<br/>";
                }
                errorMessage += "Currency is required.";

                isValid = false;
            }
            var signature = jQuery("#<%= txtsignature.ClientID %>").val();
            if (signature.length <= 0) {
                if (errorMessage.length > 0) {
                    errorMessage += "<br/>";
                }
                errorMessage += "Currency signature is required.";

                isValid = false;
            }

            if (!isValid) {
                jQuery("#<%= divmessagecurrency.ClientID %>").html(errorMessage);
                jQuery("#<%= divmessagecurrency.ClientID %>").addClass('error');
                return false;
            }
            else {
                jQuery("#<%= divmessagecurrency.ClientID %>").html('');
                jQuery("#<%= divmessagecurrency.ClientID %>").removeClass('error');
                return true;
            }
        }
        
    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#tabgroup li a").bind({ click: function () { jQuery("#tabgroup li a").removeClass("select"); jQuery(this).addClass("select"); jQuery("#<%=divMainMessage.ClientID %>").hide(); } });
            //alert("1");
        });
    </script>
    <script language="javascript" type="text/javascript">
        var prop = '<%= property %>';

        jQuery(document).ready(function () {
            CallmeProperty(prop);
        })
        function CallmeProperty(prop) {

            if (prop == "country") {
                jQuery("#tabgroup li a").removeClass("select"); jQuery("#tabgroup li a:first").addClass("select")
                //                jQuery(".TabbedPanelsTabGroup li").removeClass("TabbedPanelsTabSelected");
                //                jQuery(".TabbedPanelsTabGroup li:first").addClass("TabbedPanelsTabSelected");
                document.getElementById('divcities').style.display = 'none'; document.getElementById('divcountries').style.display = 'block'; document.getElementById('DivZones').style.display = 'none'; document.getElementById('DivMainpoints').style.display = 'none'; document.getElementById('DivCurrency').style.display = 'none';

            }
            else if (prop == "city") {
                jQuery("#tabgroup li a").removeClass("select"); jQuery("#tabgroup li a:eq(1)").addClass("select")
                //                jQuery(".TabbedPanelsTabGroup li").removeClass("TabbedPanelsTabSelected");
                //                jQuery(".TabbedPanelsTabGroup li:eq(1)").addClass("TabbedPanelsTabSelected");
                document.getElementById('divcities').style.display = 'block'; document.getElementById('divcountries').style.display = 'none'; document.getElementById('DivZones').style.display = 'none'; document.getElementById('DivMainpoints').style.display = 'none'; document.getElementById('DivCurrency').style.display = 'none';

            }
            else if (prop == "zone") {
                jQuery("#tabgroup li a").removeClass("select"); jQuery("#tabgroup li a:eq(2)").addClass("select")
                //                jQuery(".TabbedPanelsTabGroup li").removeClass("TabbedPanelsTabSelected");
                //                jQuery(".TabbedPanelsTabGroup li:eq(2)").addClass("TabbedPanelsTabSelected");
                document.getElementById('divcities').style.display = 'none'; document.getElementById('divcountries').style.display = 'none'; document.getElementById('DivZones').style.display = 'block'; document.getElementById('DivMainpoints').style.display = 'none'; document.getElementById('DivCurrency').style.display = 'none';

            }

            else if (prop == "point") {
                jQuery("#tabgroup li a").removeClass("select"); jQuery("#tabgroup li a:eq(3)").addClass("select")
                //                jQuery(".TabbedPanelsTabGroup li").removeClass("TabbedPanelsTabSelected");
                //                jQuery(".TabbedPanelsTabGroup li:eq(3)").addClass("TabbedPanelsTabSelected");
                document.getElementById('divcities').style.display = 'none'; document.getElementById('divcountries').style.display = 'none'; document.getElementById('DivZones').style.display = 'none'; document.getElementById('DivMainpoints').style.display = 'block'; document.getElementById('DivCurrency').style.display = 'none';

            }
            else if (prop == "currency") {
                jQuery("#tabgroup li a").removeClass("select"); jQuery("#tabgroup li a:eq(4)").addClass("select")
                //                jQuery(".TabbedPanelsTabGroup li").removeClass("TabbedPanelsTabSelected");
                //                jQuery(".TabbedPanelsTabGroup li:eq(3)").addClass("TabbedPanelsTabSelected");
                document.getElementById('divcities').style.display = 'none'; document.getElementById('divcountries').style.display = 'none'; document.getElementById('DivZones').style.display = 'none'; document.getElementById('DivMainpoints').style.display = 'none'; document.getElementById('DivCurrency').style.display = 'block';

            }
            else {
                jQuery("#tabgroup li a").removeClass("select"); jQuery("#tabgroup li a:first").addClass("select")
                //                jQuery(".TabbedPanelsTabGroup li").removeClass("TabbedPanelsTabSelected");
                //                jQuery(".TabbedPanelsTabGroup li:first").addClass("TabbedPanelsTabSelected");
                document.getElementById('divcities').style.display = 'none'; document.getElementById('divcountries').style.display = 'block'; document.getElementById('DivZones').style.display = 'none'; document.getElementById('DivMainpoints').style.display = 'none'; document.getElementById('DivCurrency').style.display = 'none';

            }
        }
    </script>
    <script type="text/javascript">
        var geocoder;
        var map;
        var markersArray = [];
        var marker;
        var address;
        var addressByUser;
        var Mylocation = "";
        var infowindow = new google.maps.InfoWindow();

        function setAddressValue(elementID) {

            if (elementID != null) {

                if (elementID == "europe") {
                    address = elementID;

                    initialize(4);
                }
                else {

                    address = elementID;
                    initialize(10);
                }




            }


        }
        function initialize(zoomlevel) {
            geocoder = new google.maps.Geocoder();
            var myOptions = {
                zoom: zoomlevel,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
            //            map = new google.maps.Map(document.getElementById("map_canvasupdate"), myOptions);


            codeAddress();

            google.maps.event.addListener(map, 'click', function (event) {
                placeMarker(event.latLng);

            });

        }


        function codeAddress() {
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    clearOverlays();


                    var cordinate = results[0].geometry.location;
                    map.setCenter(results[0].geometry.location);
                    marker = new google.maps.Marker({
                        map: map,
                        title: results[0]['formatted_address'],
                        position: results[0].geometry.location
                    });
                    if (cordinate != null) {
                        var splitCordinate = cordinate.toString().split(',');
                        if (mycordinate != 'U') {
                            jQuery("#<%=lblLatitude.ClientID %>").val(splitCordinate[0]);
                            jQuery("#<%=lblLongitude.ClientID %>").val(splitCordinate[1]);
                            jQuery("#<%=txtpoint.ClientID %>").val(results[0]['formatted_address']);


                        }
                    }


                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);

                    markersArray.push(marker);

                } else {

                    alert("Geocode was not successful for the following reason: " + status);

                }
            });
        }


        function placeMarker(location) {
            geocoder.geocode({ 'latLng': location }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        clearOverlays();

                        var clickplaceAddress = results[1].formatted_address;
                        if (clickplaceAddress != null) {
                            var splitAddress = clickplaceAddress.toString().split(',');

                            var cordinate = results[0].geometry.location;
                            marker = new google.maps.Marker({
                                position: location,
                                title: results[1].formatted_address,
                                map: map

                            });
                            if (cordinate != null) {
                                var splitCordinate = cordinate.toString().split(',');
                                jQuery("#<%=lblLatitude.ClientID %>").val(splitCordinate[0]);
                                jQuery("#<%=lblLongitude.ClientID %>").val(splitCordinate[1]);
                                jQuery("#<%=txtpoint.ClientID %>").val(results[1].formatted_address);

                            }


                            infowindow.setContent(results[1].formatted_address);
                            infowindow.open(map, marker);

                            markersArray.push(marker);
                            map.setCenter(location);

                            //mouseover
                            google.maps.event.addListener(marker, 'click', function () {
                                infowindow.setContent(results[1].formatted_address);
                                infowindow.open(map, this);
                                //document.getElementById("latlong").innerText = results[0].geometry.location;
                                //document.getElementById("address").value = results[0]['formatted_address'];
                            });

                        } else {

                        }
                    }
                } else {
                    initialize(10);
                    alert("Geocoder failed due to: " + status);
                }
            });

        }


        function clearOverlays() {
            if (markersArray) {
                for (i in markersArray) {
                    markersArray[i].setMap(null);
                }
            }
        }

        function toggleBounce() {

            if (marker.getAnimation() != null) {
                marker.setAnimation(null);
            } else {
                marker.setAnimation(google.maps.Animation.BOUNCE);
            }
        }

        
    </script>
    <script language="javascript" type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#<%= lnkAddNew.ClientID %>").bind("click", function () {

                $('#txtcountry').focus();
                //$('#ContentPlaceHolder1_txtcountry').focus();
            })
        });                     
    </script>
</asp:Content>
