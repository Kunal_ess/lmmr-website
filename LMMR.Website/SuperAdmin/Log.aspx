﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SuperAdmin/Main.master" AutoEventWireup="true" CodeFile="Log.aspx.cs" Inherits="SuperAdmin_Log" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/style.css" />
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="630px">
             <div id="divmessage" runat="server" class="error">
                             </div>
    <div class="search-operator-layout-left1">
                    <div class="search-operator-from1">
                        <div class="commisions-top-new1 ">
                        
                            <table width="100%" border="0" cellspacing="0" cellpadding="8">
                              <tr id="trSearch" runat="server">
                                <td  style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px" >  
                                        <div class="search-form1-left-new">
                                             Select Country :
                                        </div>
                                </td>

                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px;" runat="server" id="tdcountry">
                                   
                                     <asp:DropDownList ID="drpSearchCountry" runat="server" AutoPostBack="true" Style="width: 215px" 
                                            onselectedindexchanged="drpSearchCountry_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    
                                       
                                    </td>
                                    
                                </tr>                               
                                  <tr id="trhotelbycountry" runat="server" >
                                   <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px"  >
                                        <div class="search-form1-left-new">
                                             Select Facility Name :
                                        </div>
                                    </td>
                                    <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px">
                                        <div class="search-form1-left-new">
                                        <asp:DropDownList ID="drpHotelBycountry" runat="server" AutoPostBack="false" CssClass="NoClassApply"
                                            Style="width: 215px">                                            
                                        </asp:DropDownList>
                                        </div>                                        
                                    </td>

                                </tr>
                                <tr>
                                <td>
                                  <div class="search-form1-left-new">
                                             Select Dates :
                                        </div>
                                        </td>
                                <td style="border-bottom: #92bede solid 1px; border-left: #92bede solid 1px" runat="server" id="tdDate">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                
                                                <td style="padding-left: 20px; text-align: right; padding-right: 3px;">
                                                    <b>From</b>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFromdate" runat="server" value="dd/mm/yy"  CssClass="inputbox1"></asp:TextBox><asp:HiddenField
                                                        ID="hdnfromdate" runat="server" />

                                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFromdate"
                                                        PopupButtonID="calFrom" Format="dd/MM/yy">
                                                    </asp:CalendarExtender>                                                    
                                                    <asp:Image ID="calFrom" runat="server" ImageUrl="~/Images/date-icon.png" align="absmiddle" />
                                                </td>
                                                <td>
                                                    <b>To</b>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTodate" runat="server" value="dd/mm/yy"  CssClass="inputbox1"></asp:TextBox>
                                                    <asp:HiddenField ID="hdntodate" runat="server" />
                                                    <asp:CalendarExtender ID="CalendarExtender2"  runat="server" TargetControlID="txtTodate"
                                                        PopupButtonID="calTo" Format="dd/MM/yy">
                                                    </asp:CalendarExtender>
                                                    <asp:Image ID="calTo" runat="server" ImageUrl="~/Images/date-icon.png" align="absmiddle" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                
                            </table>
                            
                        </div>
                    </div>
                </div>
                       </td>
            <td align="left" valign="bottom">
                <div style="float: left; margin-left: 20px;" class="n-commisions">
                    <div class="n-btn">
                        <asp:LinkButton ID="lbtSearch" runat="server" onclick="lbtSearch_Click"><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Search</div><div class="n-btn-right">
                            </div></asp:LinkButton>
                    </div>
                </div>
                <div style="float: left; margin-left: 10px; height: 15px;" class="n-commisions">
                    <div class="n-btn" style="height: 15px">
                        <asp:LinkButton ID="lnkClear" runat="server" onclick="lnkClear_Click" ><div class="n-btn-left">
                            </div>
                            <div class="n-btn-mid">
                                Clear</div><div class="n-btn-right">
                            </div></asp:LinkButton>
                    </div>
                </div>
            </td>
        </tr>
    </table>
<table  width="100%">

<tr id="trfillMenu" runat="server" visible="false">
<td width="100%" valign="top"  align="center">
   <!-- start har menu blue-->
  <div id="harmenu-blue">
 
 <div id="subnavbar-new-blue">
					  <ul id="subnav-new-blue">
        <li class="cat-item"><a href="#nogo">Property Level</a>
            <ul class="children">
                <li class="cat-item"><a href="#nogo">General info</a>
                    <ul class="children">
                        <li><a href="#nogo" runat="server" id="Hotel" onserverclick="PageChange">Hotel Info</a></li>
						<li><a href="#nogo"  runat="server" id="HotelContact" onserverclick="PageChange">Contact Details</a></li>
						<li><a href="#nogo"  runat="server" id="HotelFacilities" onserverclick="PageChange">Facilities</a></li>
                    </ul>
                </li>
                <li class="cat-item"><a href="#nogo">Meeting rooms</a>
                     <ul class="children">
                       <li><a href="#nogo"  runat="server" id="MeetingRoom" onserverclick="PageChange" >Meeting room Description</a></li>
						<li><a href="#nogo"  runat="server" id="MeetingRoomConfig" onserverclick="PageChange">Configuration View</a></li>
                    </ul>
                
                </li>
                <li class="cat-item"><a href="#nogo">Bed Rooms</a>
                    <ul class="children">
                       <li><a href="#nogo"  runat="server" id="BedRoom" onserverclick="PageChange">Bedroom Description</a></li>
                    </ul>
                
                </li>
                <li class="cat-item"><a href="#nogo">Pricing </a>
                    <ul class="children">
                       <li><a href="#nogo"  runat="server" id="PackageByHotel" onserverclick="PageChange">Packages /Category</a></li>
                        
                    </ul>
                
                </li>
            </ul>
        </li>
        <li class="cat-item"><a href="#nogo">Availability</a>        
            <ul class="children">
               <li><a href="#nogo"  runat="server" id="AvailibilityOfRooms" onserverclick="PageChange">Log Report</a></li>
            </ul>
            
        </li>
        <li class="cat-item"><a href="#nogo" >Online Booking Status</a>
        
        <ul class="children">
            	<li><a href="#nogo"  runat="server" id="Booking" onserverclick="PageChange">All Bookings</a></li>
            </ul>
            
        </li>
        <li class="cat-item"><a href="#nogo">Specials & Promotions</a>
        
            <ul class="children">
                	<li><a href="#nogo" runat="server" id="SpecialPriceAndPromo" onserverclick="PageChange">Packages / Meeting room Discount</a></li>
                
            </ul>
        
        </li>
        <li class="cat-item"><a href="#nogo">Request Status</a>
            <ul class="children">
                <li><a href="#nogo" runat="server" id="Booking1" onserverclick="PageChange">All Requests</a></li>
            </ul>
        
        </li>
        <li class="cat-item" id="Client" runat="server" visible="false"><a href="#nogo" runat="server" id="ClientContract" onserverclick="PageChange">Contract</a>            
        </li>
        
        
        
    </ul>

					</div>
 
 </div>
  <!-- end har menu blue-->

</td>
</tr>
<tr>
<td  align="center">
    <asp:Label ID="lblHeading" runat="server" Font-Bold="true" Text="Note : Kindly select the country and Hotel first" Width="400px" ></asp:Label>
</td>
</tr>
<tr id="trfilldetails" runat="server">
<td width="100%" valign="top" align="center">
<asp:Panel ID="Panel1" runat="server" Height="700px" ScrollBars="Vertical">    
<div class="scroll" style="width:885px">
  <asp:GridView ID="grvDetails" runat="server" Width="100%" border="0" CellPadding="5"
                    CellSpacing="1" AutoGenerateColumns="true" GridLines="None" EmptyDataText="No records available"
                     ShowFooter="false"  BackColor="#98BCD6" RowStyle-BackColor="#FFFFFF"  AlternatingRowStyle-BackColor="#E3F0F1"
                    
                  AllowPaging="false"  HeaderStyle-BackColor="#98BCD6"
                    EmptyDataRowStyle-BackColor="White" EmptyDataRowStyle-HorizontalAlign="Center" 
                     >
                    
                    <EmptyDataTemplate>
                        <table>
                            <tr>
                                <td colspan="3" align="center">
                                    <b>No record found !</b>
                                </td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                     <HeaderStyle BackColor="#CCD8D8" />
                    <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                    <PagerStyle HorizontalAlign="Right" BackColor="White" CssClass="displayNone" />
                    <PagerTemplate>
                        <div id="Paging" style="width: 100%; display: none;">
                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                        </div>
                    </PagerTemplate>
                </asp:GridView>
</div>
</asp:Panel>
</td>
</tr>
</table>

<script language="javascript" type="text/javascript">

//    jQuery(document).ready(function () {
//        jQuery("#<%= txtFromdate.ClientID %>").attr("disabled", true);
//        jQuery("#<%= txtTodate.ClientID %>").attr("disabled", true);
//        
//        jQuery("#<%= lbtSearch.ClientID %>").bind("click", function () {
//            jQuery("#contentbody_txtFromdate").attr("disabled", false);
//            jQuery("#contentbody_txtTodate").attr("disabled", false);
//            
//        });
//    });

    jQuery(document).ready(function () {
        jQuery("#<%= lbtSearch.ClientID %>").bind("click", function () {
            if (jQuery("#<%= divmessage.ClientID %>").hasClass("succesfuly")) {
                jQuery("#<%= divmessage.ClientID %>").removeClass("succesfuly").addClass("error").hide();
            }

            var isvalid = true;
            var errormessage = "";
            var country = jQuery("#<%= drpSearchCountry.ClientID %>").val();
            if (country == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Please select country.";
                isvalid = false;
            }

            var Currency = jQuery("#<%= drpHotelBycountry.ClientID %>").val();
            if (Currency == "") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "Please select Facility Name.";
                isvalid = false;
            }

            var txtFromdate = jQuery("#<%= txtFromdate.ClientID %>").val();
            var txtTodate = jQuery("#<%= txtTodate.ClientID %>").val();
            if (txtFromdate == "dd/mm/yy" || txtFromdate == "dd/MM/yy") {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "From date is required.";
                isvalid = false;
            }

            else if (txtTodate == "dd/mm/yy" || txtTodate == "dd/MM/yy")
             {
                if (errormessage.length > 0) {
                    errormessage += "<br/>";
                }
                errormessage += "To date is required.";
                isvalid = false;
            }
            else {

                var todayArr = txtTodate.split('/');
                var formdayArr = txtFromdate.split('/');
                var fromdatecheck = new Date();
                var todatecheck = new Date();
                fromdatecheck.setFullYear(parseInt("20" + formdayArr[2]), (parseInt(formdayArr[1]) - 1), formdayArr[0]);
                todatecheck.setFullYear(parseInt("20" + todayArr[2]), (parseInt(todayArr[1]) - 1), todayArr[0]);
                if (fromdatecheck > todatecheck) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += "From date must be less than To date.";
                    isvalid = false;
                }
            }

            if (!isvalid) {
                jQuery("#<%= divmessage.ClientID %>").show();
                jQuery("#<%= divmessage.ClientID %>").html(errormessage);
                var offseterror = jQuery("#<%= divmessage.ClientID %>").offset();
                jQuery("body").scrollTop(offseterror.top);
                jQuery("html").scrollTop(offseterror.top);
                return false;
            }
            jQuery("#<%= divmessage.ClientID %>").html("");
            jQuery("#<%= divmessage.ClientID %>").hide();
            jQuery("#Loding_overlay").show();
            jQuery("body").scrollTop(0);
            jQuery("html").scrollTop(0);
            document.getElementsByTagName('html')[0].style.overflow = 'hidden';
        });
    });
    </script>
</asp:Content>

