﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;

public partial class SuperAdmin_ApplicationOthers : System.Web.UI.Page
{
    public ManageOthers objOthers = new ManageOthers();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetData();
            divmessage.Style.Add("display", "none");
            divmessage.Attributes.Add("class", "error");
        }
    }

    public void GetData()
    {
        Others objGet = objOthers.GetApplicationOthers();
        if (objGet != null)
        {
            txtSessionTimeOut.Text = objGet.SessionTimeout.ToString();
            txtScrollAmount.Text = objGet.ScrollHotelOfWeek.ToString();
        }
    }
    protected void lnkSaveOthers_Click(object sender, EventArgs e)
    {
        Others objGetOthers = objOthers.GetApplicationOthers();
        if (objGetOthers != null)
        {
            objGetOthers.SessionTimeout = Convert.ToInt32(txtSessionTimeOut.Text);
            objGetOthers.ScrollHotelOfWeek = Convert.ToInt32(txtScrollAmount.Text);
            if (objOthers.UpdateOthersInfo(objGetOthers))
            {
                divmessage.InnerHtml = "data saved successfully.";
                divmessage.Attributes.Add("class", "succesfuly");
                divmessage.Style.Add("display", "block");
            }
            else
            {
                divmessage.InnerHtml = "data not saved successfully.";
                divmessage.Attributes.Add("class", "error");
                divmessage.Style.Add("display", "block");
            }
        }
        else
        {
            divmessage.InnerHtml = "data not saved successfully.";
            divmessage.Attributes.Add("class", "error");
            divmessage.Style.Add("display", "block");
        }
    }
}