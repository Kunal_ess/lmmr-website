﻿<%@ WebHandler Language="C#" Class="Ping" %>

using System;
using System.Web;

public class Ping : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";

        context.Response.Write("Session Renewed");
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}