﻿var WLTokenKey;
var clientaddres;
var ServiceUrl;

function UrlBuilder()
{
   var href = jQuery(location).attr('href');
    if (href != null) {
        pageUrl = href;
        var fullUrl = pageUrl.split("?");
        ServiceUrl = fullUrl[0].replace("Communication.aspx", "WLCommunication.asmx");

    }
}


function LoadWL(key,url) {

    clientaddres = url;
    WLTokenKey = key;
    CheckPermission();
}

function open_win_wl(pageurl) {
    var url = pageurl;
    var winName = 'myWindow_wl';
    var w = '1200';
    var h = '800';
    var scroll = 'no';
    var popupWindow = null;
    LeftPosition = (screen.width) ? (screen.width - w) / 2 : 0;
    TopPosition = (screen.height) ? (screen.height - h) / 2 : 0;
    settings = 'height=' + h + ',width=' + w + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=yes,directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no';
    popupWindow = window.open(url, winName, settings)
    popupWindow.focus();
    return false;
}

function onlyNumeric(event) {

if(event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 9)				
				            {
												
		       				}
							else
						    if((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 ))
					        {
								
					         return false;	
							 
					         }
}

function WLSearchCanvasHTML() {
   UrlBuilder();
   var service = ServiceUrl +"/SendSearchHTML";
    jQuery.ajax({
        type: "POST",
        url: ""+service+"",
        data: "{}",
        //contentType: "application/json; charset=utf-8",
        dataType: "xml",
        success: function (msg) {
            //var response = msg.d;
            var response = jQuery(msg).find('string').text();
            jQuery('#WL_Search').html(response);

            jQuery('#DropDownWLCountry').append($("<option></option>").val('0').html('--Select country--'));
            jQuery('#DropDownWLCity').append($("<option></option>").val('0').html('--Select city--'));

            BindWLDateDropdown();
            BindWLMonthDropDown();
            BindWLYearDropDown();
            WLSetTodayDate();
            getCountry();
        }
    });
}


function WLValidation() {
    var dropWLcountry = jQuery('#DropDownWLCountry').val();
    if (dropWLcountry == "0" || dropWLcountry == "") {
        //alert('Please select country');
        jQuery('#validCountry').show();
        jQuery('#validcity').hide();
        jQuery('#validDelegate').hide();
        jQuery('#validdateBack').hide();
        return false;
    }
    else {
        jQuery('#validCountry').hide();
        
    }
    var dropWLcity = jQuery('#DropDownWLCity').val();
    if (dropWLcity == "0" || dropWLcity == "") {
        //alert('Please select city');
        jQuery('#validcity').show();
        jQuery('#validDelegate').hide();
        jQuery('#validCountry').hide();
        jQuery('#validdateBack').hide();
        return false;
    }
    else {
        jQuery('#validcity').hide();
      
    }
    
     var dropDate = parseInt(parseInt(jQuery('#DropDownWLMonth').val())+1)+"/"+jQuery('#DropDownWLDate').val()+"/"+jQuery('#DropDownWLYear').val();

if (isDate(dropDate)==false){

        jQuery('#validdate').show();
        jQuery('#validDelegate').hide();
        jQuery('#validCountry').hide();
        jQuery('#validdateBack').hide();
        jQuery('#validcity').hide();
        return false
	}
else
{
    jQuery('#validdate').hide();
}

   var dropDateBack = parseInt(parseInt(jQuery('#DropDownWLMonth').val())+1)+"/"+jQuery('#DropDownWLDate').val()+"/"+jQuery('#DropDownWLYear').val();

    var selectedDateWL = new Date(dropDateBack);
     var todayWL = new Date();
      if (IsDateLess(selectedDateWL,todayWL)==true){


        jQuery('#validdateBack').show();
        jQuery('#validdate').hide();
        jQuery('#validDelegate').hide();
        jQuery('#validCountry').hide();
        jQuery('#validcity').hide();
	return false;
	}
else
{
jQuery('#validdateBack').hide();
}
   
   
    var txtDelegate = jQuery('#TextBoxDelegate').val();
    if (txtDelegate == "0" || txtDelegate == "" || parseInt(txtDelegate) == 0) {
        jQuery('#validDelegate').show();
        jQuery('#validCountry').hide();
        jQuery('#validdateBack').hide();
        jQuery('#validcity').hide();
        //alert('Please enter valid number of delegate');
        return false;
    }
    else {
        jQuery('#validDelegate').hide();
        
    }

    var m_sum = Number(jQuery('#DropDownWLMonth').val()) + Number(1);
    var month = (parseInt(jQuery('#DropDownWLMonth').val()) < 10 ? "0" + m_sum : m_sum);
    var searchUrl = "../SearchResult.aspx?wl=" + WLTokenKey + "&country=" + jQuery('#DropDownWLCountry').val() + "&city=" + jQuery('#DropDownWLCity').val() + "&d=" + jQuery('#DropDownWLDate').val() + "&m=" + month + "&y=" + jQuery('#DropDownWLYear').val() + "&delegate=" + jQuery('#TextBoxDelegate').val() + "&duration=" + jQuery('#DropDownWLDuration').val() + "&day1=" + jQuery('#DropDownWLDay1').val() + "&day2=" + jQuery('#DropDownWLDay2').val() + "";
    open_win_wl(searchUrl);
    return false;
}
function WLSetTodayDate() {
    var d = new Date();
    jQuery('#DropDownWLDate').val(d.getDate());
    jQuery('#DropDownWLMonth').val(d.getMonth());
    jQuery('#DropDownWLYear').val(d.getFullYear());
}

function BindWLDateDropdown() {
    var firstDate = 1;
    var lastDate = 31;
    for (firstDate = 1; firstDate <= 31; firstDate++) {
        jQuery('#DropDownWLDate').append($("<option></option>").val(firstDate).html(firstDate));
    }

}

function BindWLYearDropDown() {
    var d = new Date();
    var firstYear = d.getFullYear();
    var ToYear = parseInt(firstYear) + 20;
    for (firstYear = parseInt(firstYear); firstYear <= ToYear; firstYear++) {
        jQuery('#DropDownWLYear').append($("<option></option>").val(firstYear).html(firstYear));
    }
}

function BindWLMonthDropDown() {
    var m_names = new Array("January", "February", "March",
"April", "May", "June", "July", "August", "September",
"October", "November", "December");
    var firstMonth = 0;
    for (firstMonth = 0; firstMonth <= m_names.length - 1; firstMonth++) {
        jQuery('#DropDownWLMonth').append($("<option></option>").val(firstMonth).html(m_names[firstMonth]));
    }
}

function getCountry() {
    UrlBuilder();
    var key = WLTokenKey;
    var service = ServiceUrl +"/GetCountryData";
    jQuery.ajax({
        type: "POST",
        url: "" + service +"",
        data: "{'WLChannelID':'" + parseInt(key) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "JSON",
        success: function (msg) {
            var result = JSON.parse(msg.d);
            if (result != null) {

                //jQuery(msg).find('ArrayOfCountry').find("Country").each(function () {
                    //jQuery('#DropDownWLCountry').append(jQuery("<option></option>").val(jQuery(this).find('Id').text()).html(jQuery(this).find('CountryName').text()));

                //});
                                jQuery.each(result, function () {
                                    jQuery('#DropDownWLCountry').append(jQuery("<option></option>").val(this['Id']).html(this['CountryName']));
                               });
            }

        }
    });
}

function getCiy() {
    UrlBuilder();
    var key = WLTokenKey;
    var countryID = jQuery('#DropDownWLCountry').val();
    var service = ServiceUrl +"/GetCityData";

    jQuery.ajax({
        type: "POST",
        url: "" + service + "",
        data: "{'WLChannelID':'" + parseInt(key) + "','WLCountryID':'" + parseInt(countryID) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "JSON",
        success: function (msg) {

             jQuery('#DropDownWLCity').empty();
             jQuery('#DropDownWLCity').append($("<option></option>").val('0').html('--Select city--'));
            //var result = jQuery.parseJSON(msg.d)
            var result = JSON.parse(msg.d);
            if (result != null) {
                

jQuery.each(result, function () {
                                    jQuery('#DropDownWLCity').append(jQuery("<option></option>").val(this['Id']).html(this['City']));
                               });


            }
            //else {
               // jQuery('#DropDownWLCity').append($("<option></option>").val('0').html('--Select city--'));
            //}
        }
    });
}



function CheckPermission() {
    UrlBuilder();
    var key = WLTokenKey;
    var openURL = clientaddres;
    var service = ServiceUrl +"/VerifiedClient";
    jQuery.ajax({
        type: "POST",
        url: "" + service +"",
        data: "WLChannelID=" + parseInt(key) + "&clientURL=" + openURL + "",
        //contentType: "application/json; charset=utf-8",
        dataType: "xml",
        success: function (msg) {
            //var response = msg.d;
            var response = jQuery(msg).find("string").text();
            if (response == 'verified') {
                WLSearchCanvasHTML();
            }
            else if (response == 'deactivate') {
                var errorHtml = '<table border="0" id="mainbody" style="width: 260px; margin: 0px auto; padding: 10px;"><tr><td><b>Sorry ! your service is deactivated, please contact to LMMR support.</b></td></tr>';
                jQuery("#WL_Search").html(errorHtml);
            }
            else {
                var errorHtml = '<table border="0" id="mainbody" style="width: 260px; margin: 0px auto; padding: 10px;"><tr><td><b>Sorry ! you are not authorised to use this service, please contact to LMMR support.</b></td></tr>';
                jQuery("#WL_Search").html(errorHtml);
            }
        }
    });
}

function ShowHideWLDayDropDown() {

    var selectedDay = jQuery('#DropDownWLDuration').val();
    if (selectedDay == '2') {
        jQuery('#DropDownWLDay2').val('0');
        jQuery('#trWLDay2').show();

    }
    else {
        jQuery('#trWLDay2').hide();
    }

}


//Date validation.

var dtCh= "/";
var minYear=1900;
var maxYear=2100;

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}



function isDate(dtStr){
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strMonth=dtStr.substring(0,pos1)
	var strDay=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){

		//alert("The date format should be : mm/dd/yyyy")
		return false;
	}
	if (strMonth.length<1 || month<1 || month>12){
		//alert("Please enter a valid month")
		return false;

	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		//alert("Please enter a valid day")
		return false;
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		//alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear)
		return false;

	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		//alert("Please enter a valid date")
		return false;

	}

return true;
}


function IsDateLess(DateValue1, DateValue2)
{
var DaysDiff;
Date1 = new Date(DateValue1);
Date2 = new Date(DateValue2);
DaysDiff = Math.floor((Date1.getTime() - Date2.getTime())/(1000*60*60*24));
//alert("DaysDiff ="+DaysDiff);
if(DaysDiff < -1)
return true;
else
return false;
}

function CrossDomainTesting() {

    $.ajax({
        url: "http://tonofweb.com/MyService.asmx/Sum",
        contentType: "application/json; charset=utf-8",
        data: { x: JSON.stringify("1"), y: JSON.stringify("2") },
        dataType: "jsonp",
        success: function (json) {
            alert(json.d);
        },
        error: function () {
            alert("Hit error fn!");
        }
    });
}
