﻿//Created by Vaibhav 07, Nov,2011
var dialogSeed = 1000;
function jQueryDialogWrapper(title, message, showModel, width, height, showOK, showCancel, executeThisOnOK, executeThisBeforeClose, executeThisOnClose) {
    //
    // Get an Id for the "<div>" to diaply the error messages.
    // The Id is made sure to be unique in the web page.
    var dialogId = "D_dia_log" + dialogSeed++;
    while (jQuery("#" + dialogId).length != 0) {

        dialogId = "D_dia_log" + dialogSeed++;
    }
    // create the error message "div" and add it to the dom.
    // it will be use to display the validation error messages.
    var dialogText = "<div id='" + dialogId
            + "' title='" + title + "'>"
             + "</div>";
    jQuery("body").append(dialogText);
    var $dialog = jQuery("#" + dialogId);
    jQuery($dialog).append(message); //Vaibhav- added: it will insert the data( whether string, html, jquery object) as last child of target element
    $dialog.dialog('destroy');

    width = (typeof (width) != 'undefined' || width != null) ? width : 'auto';
    height = (typeof (height) != 'undefined' || height != null) ? height : 'auto';

    showModel = (typeof (showModel) != 'undefined' || showModel != null) ? showModel : false;

    var buttons = [];
    if (typeof (showOK) != 'undefined' && showOK != null && showOK == true && typeof (showCancel) != 'undefined' && showCancel != null && showCancel == true) {
        buttons = [{
            text: "Ok",
            click: function () {
                jQuery(this).dialog("close");
                if (executeThisOnOK == undefined || executeThisOnOK == null) {
                }
                else {
                    executeThisOnOK();
                }
                return true;
            }
        },
        {
            text: "Cancel",
            click: function () { jQuery(this).dialog("close"); return false; }
        }];
    }
    else if (typeof (showOK) != 'undefined' && showOK != null && showOK == true) {
        buttons = [{
            text: "Ok",
            click: function () {
                jQuery(this).dialog("close");
                if (executeThisOnOK == undefined || executeThisOnOK == null) {
                }
                else {
                    executeThisOnOK();
                }
                return true;
            }
        }];
    }
    else if (typeof (showCancel) != 'undefined' && showCancel != null && showCancel == true) {
        buttons = [
    {
        text: "Cancel",
        click: function () { jQuery(this).dialog("close"); return false; }
    }];
    }

    //create dialog
    $dialog.attr('title', title);
    $dialog.dialog({
        autoOpen: false,
        draggable: true,
        resizable: true,
        modal: showModel,
        width: width,
        height: height,
        closeOnEscape: true,
        stack: true,
        buttons: buttons,
        beforeClose: function (event, ui) {
            if (executeThisBeforeClose == undefined || executeThisBeforeClose == null) {
            }
            else {
                executeThisBeforeClose();
            }
            return true;
        },
        close: function (event, ui) {
            if (executeThisOnClose == undefined || executeThisOnClose == null) {
            }
            else {
                executeThisOnClose();
            }
            return true;
        },
        open: function (type, data) {
            jQuery(this).parent().appendTo("form");
            //$(this).parent().appendTo("form[0]");
            //or $(this).parent().appendTo("form:first");
        }
    });

    // This is the function to call when to show dialog
    this.show = function () {
        //
       $dialog.dialog('open');
//        $(this).parent().maxZIndex({ inc: 500 });
    };

    // This is the function to call when to destroy dialog
    this.destroy = function () {
        //
        $dialog.dialog('destroy');
    };

    // This is the function to call to move dialog on top
    this.moveToTop = function () {
        //
        $dialog.dialog("moveToTop");
    };

    this.close = function () {
        //
        $dialog.dialog("close");
    };
}