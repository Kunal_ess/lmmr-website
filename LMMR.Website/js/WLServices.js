﻿var pageUrl;
var WLTokenKey;
var IsStyle;
var communicationUrl;
// get Current page url.
var href = jQuery(location).attr('href');
if (href != null) {
    pageUrl = href;
}
var scripts = document.getElementsByTagName("script");
var script = scripts[scripts.length - 1];

jQuery(document).ready(function () {
    for (var i = 0, len = scripts.length; i < len; i++) {
        if (scripts[i].getAttribute("src") != null) {
            var src = scripts[i].getAttribute("src").split("?");
            var url = src[0];
            var args = src[1];
            if (args == undefined) {
                continue;
            }
            else {
                if (url != undefined) {
                    var getUrl = url.split("js");
                    communicationUrl = getUrl[0];
                }
                var argv = args.split("&");
                for (var j = 0, argc = argv.length; j < argc; j++) {

                    var pair = argv[j].split("=");
                    var argName = pair[0];
                    var argValue = pair[1];
                    if (argName == "w") {
                        WLTokenKey = argValue;
                    }
                    if (argName == "style") {
                        IsStyle = argValue;
                    }

                }
                ShowWL();
                break;
            }
        }
    }

});

function ShowWL() {

    var wlhtml = '<iframe id="MyIframe" name="MyIframe" src="' + communicationUrl + 'WL/Communication.aspx?WLID=' + WLTokenKey + '&src=' + pageUrl + '&style=' + IsStyle + '" width="275" border="0" frameborder=0 height="275" style="border:none;float:left;" ></iframe>';

    jQuery(wlhtml).appendTo('#search_canvas');
}
