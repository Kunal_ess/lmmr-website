﻿jQuery(document).ready(function () {
    jQuery("#adjust-meeting #contentbody_txtFrom").attr("disabled", true)
    jQuery("#adjust-meeting #contentbody_txtTo").attr("disabled", true)
    jQuery('#adjust-meeting .adjust-meeting-form-body-box3 input:checkbox').attr("disabled", true)
    jQuery("#adjust-meeting .error").hide()
    jQuery("#adjust-meeting .adjust-meeting-form-body-box #chkAllMeetingRoom").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery('#adjust-meeting .adjust-meeting-form-body-box input:checkbox').attr("disabled", true)
            jQuery('#adjust-meeting .adjust-meeting-form-body-box input:checkbox').attr("checked", true)
            jQuery(this).attr("disabled", false)
            jQuery(this).attr("checked", true)
        }
        else {
            jQuery('#adjust-meeting .adjust-meeting-form-body-box input:checkbox').attr("disabled", false)
            jQuery('#adjust-meeting .adjust-meeting-form-body-box input:checkbox').attr("checked", false)
        }
    })
    jQuery("#adjust-meeting .adjust-meeting-form-body-box2 #chkAllDays").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery('#adjust-meeting .adjust-meeting-form-body-box2 input:checkbox').attr("disabled", true)
            jQuery('#adjust-meeting .adjust-meeting-form-body-box3 input:checkbox').attr("disabled", true)
            jQuery('#adjust-meeting .adjust-meeting-form-body-box2 input:checkbox').attr("checked", true)
            jQuery('#adjust-meeting .adjust-meeting-form-body-box3 input:checkbox').attr("checked", false)
            jQuery(this).attr("disabled", false)
            jQuery(this).attr("checked", true)
            jQuery("#adjust-meeting .adjust-meeting-form-body-box3 #chkAllClose").attr("disabled", false)
            jQuery("#adjust-meeting .adjust-meeting-form-body-box3 #chkAllOpen").attr("disabled", false)
        }
        else {
            jQuery('#adjust-meeting .adjust-meeting-form-body-box2 input:checkbox').attr("disabled", false)
            jQuery('#adjust-meeting .adjust-meeting-form-body-box3 input:checkbox').attr("disabled", true)
            jQuery('#adjust-meeting .adjust-meeting-form-body-box2 input:checkbox').attr("checked", false)
            jQuery('#adjust-meeting .adjust-meeting-form-body-box3 input:checkbox').attr("checked", false)
        }
    })
    jQuery("#adjust-meeting .adjust-meeting-form-body-box2 #chkAllDays").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery('#adjust-meeting .adjust-meeting-form-body-box2 input:checkbox').attr("disabled", true)
            jQuery(this).attr("disabled", false)
        }
        else {
            jQuery('#adjust-meeting .adjust-meeting-form-body-box2 input:checkbox').attr("disabled", false)
        }
    })
    jQuery("#adjust-meeting #chkSun").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #chkOpenM").attr("disabled", false)
            jQuery("#adjust-meeting #chkCloseM").attr("disabled", false)
        }
        else {
            jQuery("#adjust-meeting #chkOpenM").attr("disabled", true)
            jQuery("#adjust-meeting #chkCloseM").attr("disabled", true)
            jQuery("#adjust-meeting #chkOpenM").attr("checked", false)
            jQuery("#adjust-meeting #chkCloseM").attr("checked", false)
        }
    })
    jQuery("#adjust-meeting #chkMon").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #chkOpenTu").attr("disabled", false)
            jQuery("#adjust-meeting #chkCloseTu").attr("disabled", false)
        }
        else {
            jQuery("#adjust-meeting #chkOpenTu").attr("disabled", true)
            jQuery("#adjust-meeting #chkCloseTu").attr("disabled", true)
            jQuery("#adjust-meeting #chkOpenTu").attr("checked", false)
            jQuery("#adjust-meeting #chkCloseTu").attr("checked", false)
        }
    })
    jQuery("#adjust-meeting #chkTue").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #chkOpenW").attr("disabled", false)
            jQuery("#adjust-meeting #chkCloseW").attr("disabled", false)
        }
        else {
            jQuery("#adjust-meeting #chkOpenW").attr("disabled", true)
            jQuery("#adjust-meeting #chkCloseW").attr("disabled", true)
            jQuery("#adjust-meeting #chkOpenW").attr("checked", false)
            jQuery("#adjust-meeting #chkCloseW").attr("checked", false)
        }
    })
    jQuery("#adjust-meeting #chkWed").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #chkOpenTh").attr("disabled", false)
            jQuery("#adjust-meeting #chkCloseTh").attr("disabled", false)
        }
        else {
            jQuery("#adjust-meeting #chkOpenTh").attr("disabled", true)
            jQuery("#adjust-meeting #chkCloseTh").attr("disabled", true)
            jQuery("#adjust-meeting #chkOpenTh").attr("checked", false)
            jQuery("#adjust-meeting #chkCloseTh").attr("checked", false)
        }
    })
    jQuery("#adjust-meeting #chkThu").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #chkOpenF").attr("disabled", false)
            jQuery("#adjust-meeting #chkCloseF").attr("disabled", false)
        }
        else {
            jQuery("#adjust-meeting #chkOpenF").attr("disabled", true)
            jQuery("#adjust-meeting #chkCloseF").attr("disabled", true)
            jQuery("#adjust-meeting #chkOpenF").attr("checked", false)
            jQuery("#adjust-meeting #chkCloseF").attr("checked", false)
        }
    })
    jQuery("#adjust-meeting #chkFri").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #chkOpenSa").attr("disabled", false)
            jQuery("#adjust-meeting #chkCloseSa").attr("disabled", false)
        }
        else {
            jQuery("#adjust-meeting #chkOpenSa").attr("disabled", true)
            jQuery("#adjust-meeting #chkCloseSa").attr("disabled", true)
            jQuery("#adjust-meeting #chkOpenSa").attr("checked", false)
            jQuery("#adjust-meeting #chkCloseSa").attr("checked", false)
        }
    })
    jQuery("#adjust-meeting #chkSat").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #chkOpenSu").attr("disabled", false)
            jQuery("#adjust-meeting #chkCloseSu").attr("disabled", false)
        }
        else {
            jQuery("#adjust-meeting #chkOpenSu").attr("disabled", true)
            jQuery("#adjust-meeting #chkCloseSu").attr("disabled", true)
            jQuery("#adjust-meeting #chkOpenSu").attr("checked", false)
            jQuery("#adjust-meeting #chkCloseSu").attr("checked", false)
        }
    })
    jQuery("#adjust-meeting #chkCloseM").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #chkOpenM").attr("checked", false)
        }
    })
    jQuery("#adjust-meeting #chkOpenM").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #chkCloseM").attr("checked", false)
        }
    })
    jQuery("#adjust-meeting #chkCloseTu").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #chkOpenTu").attr("checked", false)
        }
    })
    jQuery("#adjust-meeting #chkOpenTu").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #chkCloseTu").attr("checked", false)
        }
    })
    jQuery("#adjust-meeting #chkCloseW").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #chkOpenW").attr("checked", false)
        }
    })
    jQuery("#adjust-meeting #chkOpenW").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #chkCloseW").attr("checked", false)
        }
    })
    jQuery("#adjust-meeting #chkCloseTh").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #chkOpenTh").attr("checked", false)
        }
    })
    jQuery("#adjust-meeting #chkOpenTh").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #chkCloseTh").attr("checked", false)
        }
    })
    jQuery("#adjust-meeting #chkCloseF").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #chkOpenF").attr("checked", false)
        }
    })
    jQuery("#adjust-meeting #chkOpenF").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #chkCloseF").attr("checked", false)
        }
    })
    jQuery("#adjust-meeting #chkCloseSa").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #chkOpenSa").attr("checked", false)
        }
    })
    jQuery("#adjust-meeting #chkOpenSa").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #chkCloseSa").attr("checked", false)
        }
    })
    jQuery("#adjust-meeting #chkCloseSu").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #chkOpenSu").attr("checked", false)
        }
    })
    jQuery("#adjust-meeting #chkOpenSu").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting #chkCloseSu").attr("checked", false)
        }
    })
    jQuery("#adjust-meeting .adjust-meeting-form-body-box3left #chkAllClose").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting .adjust-meeting-form-body-box3left input:checkbox").attr("checked", true)
            jQuery("#adjust-meeting .adjust-meeting-form-body-box3right input:checkbox").attr("checked", false)
        }
        else {
            jQuery("#adjust-meeting .adjust-meeting-form-body-box3left input:checkbox").attr("checked", false)
        }
    })
    jQuery("#adjust-meeting .adjust-meeting-form-body-box3right #chkAllOpen").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-meeting .adjust-meeting-form-body-box3right input:checkbox").attr("checked", true)
            jQuery("#adjust-meeting .adjust-meeting-form-body-box3left input:checkbox").attr("checked", false)
        }
        else {
            jQuery("#adjust-meeting .adjust-meeting-form-body-box3right input:checkbox").attr("checked", false)
        }
    })
    jQuery("#adjust-meeting .cancelbtn").click(function () {
        jQuery("#adjust-meeting .error").hide()
        jQuery("#adjust-meeting #contentbody_txtFrom").val("dd/mm/yy")
        jQuery("#adjust-meeting #contentbody_txtTo").val("dd/mm/yy")
        jQuery("#adjust-meeting .adjust-meeting-form-body-box input:checkbox").attr("checked", false)
        jQuery("#adjust-meeting .adjust-meeting-form-body-box input:checkbox").attr("disabled", false)
        jQuery("#adjust-meeting .adjust-meeting-form-body-box2 input:checkbox").attr("checked", false)
        jQuery("#adjust-meeting .adjust-meeting-form-body-box2 input:checkbox").attr("disabled", false)
        jQuery("#adjust-meeting .adjust-meeting-form-body-box1right input:checkbox").attr("checked", false)
        jQuery("#adjust-meeting .adjust-meeting-form-body-box1right input:checkbox").attr("disabled", false)
        jQuery("#adjust-meeting .adjust-meeting-form-body-box2right input:checkbox").attr("checked", false)
        jQuery("#adjust-meeting .adjust-meeting-form-body-box2right input:checkbox").attr("disabled", false)
        jQuery("#adjust-meeting .adjust-meeting-form-body-box3left input:checkbox").attr("checked", false)
        jQuery("#adjust-meeting .adjust-meeting-form-body-box3left input:checkbox").attr("disabled", true)
        jQuery("#adjust-meeting .adjust-meeting-form-body-box3right input:checkbox").attr("checked", false)
        jQuery("#adjust-meeting .adjust-meeting-form-body-box3right input:checkbox").attr("disabled", true)
    })
    jQuery("#adjust-meeting #contentbody_ancSubmit").click(function () {
        if (jQuery("#adjust-meeting #contentbody_txtFrom").val() == "dd/mm/yy" || jQuery("#adjust-meeting #contentbody_txtFrom").val() == null || jQuery("#adjust-meeting #contentbody_txtFrom").val() == "") {
            jQuery("#adjust-meeting .error").show()
            jQuery("#adjust-meeting .error").html("Please select From date.")
            return false
        }
        if (jQuery("#adjust-meeting #contentbody_txtTo").val() == "dd/mm/yy" || jQuery("#adjust-meeting #contentbody_txtTo").val() == null || jQuery("#adjust-meeting #contentbody_txtTo").val() == "") {
            jQuery("#adjust-meeting .error").show()
            jQuery("#adjust-meeting .error").html("Please select To date.")
            return false
        }
        var fromdate = jQuery("#adjust-meeting #contentbody_txtFrom").val()
        var todate = jQuery("#adjust-meeting #contentbody_txtTo").val()
        var todayArr = todate.split('/')
        var formdayArr = fromdate.split('/')
        var fromdatecheck = new Date()
        var todatecheck = new Date()
        fromdatecheck.setFullYear(parseInt("20" + formdayArr[2], 10), (parseInt(formdayArr[1], 10) - 1), formdayArr[0])
        todatecheck.setFullYear(parseInt("20" + todayArr[2], 10), (parseInt(todayArr[1], 10) - 1), todayArr[0])
        if (fromdatecheck > todatecheck) {
            jQuery("#adjust-meeting .error").show()
            jQuery("#adjust-meeting .error").html("From date must be less than To date.")
            return false
        }
        if (jQuery("#adjust-meeting .adjust-meeting-form-body-box input:checkbox:checked").length == 0) {
            jQuery("#adjust-meeting .error").show()
            jQuery("#adjust-meeting .error").html("Please select atleast one meeting room.")
            return false
        }
        if (jQuery("#adjust-meeting .adjust-meeting-form-body-box2 input:checkbox:checked").length == 0) {
            jQuery("#adjust-meeting .error").show()
            jQuery("#adjust-meeting .error").html("Please select atleast one day.")
            return false
        }
        var MeetingRoomIds = ""
        var MeetingDays = ""
        var ClosingStatus = ""
        var OpeningStatus = ""
        var totalMR = jQuery("#adjust-meeting .adjust-meeting-form-body-box1right input:checkbox").length
        for (var mr = 1; mr < totalMR; mr++) {
            if (jQuery("#adjust-meeting .adjust-meeting-form-body-box1right input:checkbox:eq(" + mr + ")").is(':checked')) {
                MeetingRoomIds += jQuery("#adjust-meeting .adjust-meeting-form-body-box1right input:checkbox:eq(" + mr + ")").attr("alt") + "|"
                if (jQuery("#adjust-meeting .adjust-meeting-form-body-box1right input:checkbox:eq(" + mr + ")").attr("alt") == "0") {
                    break
                } 
            } 
        }
        if (MeetingRoomIds.length > 0) {
            MeetingRoomIds = MeetingRoomIds.substring(0, MeetingRoomIds.length - 1)
        }
        var totalDays = jQuery("#adjust-meeting .adjust-meeting-form-body-box2right input:checkbox").length
        for (var td = 1; td < totalDays; td++) {
            if (jQuery("#adjust-meeting .adjust-meeting-form-body-box2right input:checkbox:eq(" + td + ")").is(':checked')) {
                MeetingDays += "1|"
            }
            else {
                MeetingDays += "0|"
            }
            if (jQuery("#adjust-meeting .adjust-meeting-form-body-box3left input:checkbox:eq(" + td + ")").is(':checked')) {
                ClosingStatus += "1|"
            }
            else {
                ClosingStatus += "0|"
            }
            if (jQuery("#adjust-meeting .adjust-meeting-form-body-box3right input:checkbox:eq(" + td + ")").is(':checked')) {
                OpeningStatus += "1|"
            }
            else {
                OpeningStatus += "0|"
            } 
        }
        if (MeetingDays.length > 0) {
            MeetingDays = MeetingDays.substring(0, MeetingDays.length - 1)
        }
        if (ClosingStatus.length > 0) {
            ClosingStatus = ClosingStatus.substring(0, ClosingStatus.length - 1)
        }
        if (OpeningStatus.length > 0) {
            OpeningStatus = OpeningStatus.substring(0, OpeningStatus.length - 1)
        }
        jQuery("#adjust-meeting .error").html("")
        jQuery("#adjust-meeting .error").hide()
        jQuery("#adjust-meeting #contentbody_txtFrom").attr("disabled", false)
        jQuery("#adjust-meeting #contentbody_txtTo").attr("disabled", false)
        jQuery("#adjust-meeting #contentbody_hdnMeetingRoomIds").val(MeetingRoomIds)
        jQuery("#adjust-meeting #contentbody_hdnMeetingDays").val(MeetingDays)
        jQuery("#adjust-meeting #contentbody_hdnClosingStatus").val(ClosingStatus)
        jQuery("#adjust-meeting #contentbody_hdnOpenStatus").val(OpeningStatus)
    })
    jQuery("#adjust-bedroom #contentbody_txtFromBr").attr("disabled", true)
    jQuery("#adjust-bedroom #contentbody_txtToBr").attr("disabled", true)
    jQuery('#adjust-bedroom .adjust-meeting-form-body-box3 input:checkbox').attr("disabled", true)
    jQuery("#adjust-bedroom .error").hide()
    jQuery("#adjust-bedroom .adjust-meeting-form-body-box #chkAllMeetingRoom").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery('#adjust-bedroom .adjust-meeting-form-body-box input:checkbox').attr("disabled", true)
            jQuery('#adjust-bedroom .adjust-meeting-form-body-box input:checkbox').attr("checked", true)
            jQuery(this).attr("disabled", false)
            jQuery(this).attr("checked", true)
        }
        else {
            jQuery('#adjust-bedroom .adjust-meeting-form-body-box input:checkbox').attr("disabled", false)
            jQuery('#adjust-bedroom .adjust-meeting-form-body-box input:checkbox').attr("checked", false)
        }
    })
    jQuery("#adjust-bedroom .adjust-meeting-form-body-box2 #chkAllDays").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery('#adjust-bedroom .adjust-meeting-form-body-box2 input:checkbox').attr("disabled", true)
            jQuery('#adjust-bedroom .adjust-meeting-form-body-box3 input:checkbox').attr("disabled", true)
            jQuery('#adjust-bedroom .adjust-meeting-form-body-box2 input:checkbox').attr("checked", true)
            jQuery('#adjust-bedroom .adjust-meeting-form-body-box3 input:checkbox').attr("checked", false)
            jQuery(this).attr("disabled", false)
            jQuery(this).attr("checked", true)
            jQuery("#adjust-bedroom .adjust-meeting-form-body-box3 #chkAllClose").attr("disabled", false)
            jQuery("#adjust-bedroom .adjust-meeting-form-body-box3 #chkAllOpen").attr("disabled", false)
        }
        else {
            jQuery('#adjust-bedroom .adjust-meeting-form-body-box2 input:checkbox').attr("disabled", false)
            jQuery('#adjust-bedroom .adjust-meeting-form-body-box3 input:checkbox').attr("disabled", true)
            jQuery('#adjust-bedroom .adjust-meeting-form-body-box2 input:checkbox').attr("checked", false)
            jQuery('#adjust-bedroom .adjust-meeting-form-body-box3 input:checkbox').attr("checked", false)
        }
    })
    jQuery("#adjust-bedroom .adjust-meeting-form-body-box2 #chkAllDays").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery('#adjust-bedroom .adjust-meeting-form-body-box2 input:checkbox').attr("disabled", true)
            jQuery(this).attr("disabled", false)
        }
        else {
            jQuery('#adjust-bedroom .adjust-meeting-form-body-box2 input:checkbox').attr("disabled", false)
        }
    })
    jQuery("#adjust-bedroom #chkSun").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #chkOpenM").attr("disabled", false)
            jQuery("#adjust-bedroom #chkCloseM").attr("disabled", false)
        }
        else {
            jQuery("#adjust-bedroom #chkOpenM").attr("disabled", true)
            jQuery("#adjust-bedroom #chkCloseM").attr("disabled", true)
            jQuery("#adjust-bedroom #chkOpenM").attr("checked", false)
            jQuery("#adjust-bedroom #chkCloseM").attr("checked", false)
        }
    })
    jQuery("#adjust-bedroom #chkMon").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #chkOpenTu").attr("disabled", false)
            jQuery("#adjust-bedroom #chkCloseTu").attr("disabled", false)
        }
        else {
            jQuery("#adjust-bedroom #chkOpenTu").attr("disabled", true)
            jQuery("#adjust-bedroom #chkCloseTu").attr("disabled", true)
            jQuery("#adjust-bedroom #chkOpenTu").attr("checked", false)
            jQuery("#adjust-bedroom #chkCloseTu").attr("checked", false)
        }
    })
    jQuery("#adjust-bedroom #chkTue").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #chkOpenW").attr("disabled", false)
            jQuery("#adjust-bedroom #chkCloseW").attr("disabled", false)
        }
        else {
            jQuery("#adjust-bedroom #chkOpenW").attr("disabled", true)
            jQuery("#adjust-bedroom #chkCloseW").attr("disabled", true)
            jQuery("#adjust-bedroom #chkOpenW").attr("checked", false)
            jQuery("#adjust-bedroom #chkCloseW").attr("checked", false)
        }
    })
    jQuery("#adjust-bedroom #chkWed").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #chkOpenTh").attr("disabled", false)
            jQuery("#adjust-bedroom #chkCloseTh").attr("disabled", false)
        }
        else {
            jQuery("#adjust-bedroom #chkOpenTh").attr("disabled", true)
            jQuery("#adjust-bedroom #chkCloseTh").attr("disabled", true)
            jQuery("#adjust-bedroom #chkOpenTh").attr("checked", false)
            jQuery("#adjust-bedroom #chkCloseTh").attr("checked", false)
        }
    })
    jQuery("#adjust-bedroom #chkThu").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #chkOpenF").attr("disabled", false)
            jQuery("#adjust-bedroom #chkCloseF").attr("disabled", false)
        }
        else {
            jQuery("#adjust-bedroom #chkOpenF").attr("disabled", true)
            jQuery("#adjust-bedroom #chkCloseF").attr("disabled", true)
            jQuery("#adjust-bedroom #chkOpenF").attr("checked", false)
            jQuery("#adjust-bedroom #chkCloseF").attr("checked", false)
        }
    })
    jQuery("#adjust-bedroom #chkFri").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #chkOpenSa").attr("disabled", false)
            jQuery("#adjust-bedroom #chkCloseSa").attr("disabled", false)
        }
        else {
            jQuery("#adjust-bedroom #chkOpenSa").attr("disabled", true)
            jQuery("#adjust-bedroom #chkCloseSa").attr("disabled", true)
            jQuery("#adjust-bedroom #chkOpenSa").attr("checked", false)
            jQuery("#adjust-bedroom #chkCloseSa").attr("checked", false)
        }
    })
    jQuery("#adjust-bedroom #chkSat").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #chkOpenSu").attr("disabled", false)
            jQuery("#adjust-bedroom #chkCloseSu").attr("disabled", false)
        }
        else {
            jQuery("#adjust-bedroom #chkOpenSu").attr("disabled", true)
            jQuery("#adjust-bedroom #chkCloseSu").attr("disabled", true)
            jQuery("#adjust-bedroom #chkOpenSu").attr("checked", false)
            jQuery("#adjust-bedroom #chkCloseSu").attr("checked", false)
        }
    })
    jQuery("#adjust-bedroom #chkCloseM").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #chkOpenM").attr("checked", false)
        }
    })
    jQuery("#adjust-bedroom #chkOpenM").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #chkCloseM").attr("checked", false)
        }
    })
    jQuery("#adjust-bedroom #chkCloseTu").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #chkOpenTu").attr("checked", false)
        }
    })
    jQuery("#adjust-bedroom #chkOpenTu").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #chkCloseTu").attr("checked", false)
        }
    })
    jQuery("#adjust-bedroom #chkCloseW").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #chkOpenW").attr("checked", false)
        }
    })
    jQuery("#adjust-bedroom #chkOpenW").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #chkCloseW").attr("checked", false)
        }
    })
    jQuery("#adjust-bedroom #chkCloseTh").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #chkOpenTh").attr("checked", false)
        }
    })
    jQuery("#adjust-bedroom #chkOpenTh").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #chkCloseTh").attr("checked", false)
        }
    })
    jQuery("#adjust-bedroom #chkCloseF").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #chkOpenF").attr("checked", false)
        }
    })
    jQuery("#adjust-bedroom #chkOpenF").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #chkCloseF").attr("checked", false)
        }
    })
    jQuery("#adjust-bedroom #chkCloseSa").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #chkOpenSa").attr("checked", false)
        }
    })
    jQuery("#adjust-bedroom #chkOpenSa").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #chkCloseSa").attr("checked", false)
        }
    })
    jQuery("#adjust-bedroom #chkCloseSu").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #chkOpenSu").attr("checked", false)
        }
    })
    jQuery("#adjust-bedroom #chkOpenSu").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom #chkCloseSu").attr("checked", false)
        }
    })
    jQuery("#adjust-bedroom .adjust-meeting-form-body-box3left #chkAllClose").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom .adjust-meeting-form-body-box3left input:checkbox").attr("checked", true)
            jQuery("#adjust-bedroom .adjust-meeting-form-body-box3right input:checkbox").attr("checked", false)
        }
        else {
            jQuery("#adjust-bedroom .adjust-meeting-form-body-box3left input:checkbox").attr("checked", false)
        }
    })
    jQuery("#adjust-bedroom .adjust-meeting-form-body-box3right #chkAllOpen").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery("#adjust-bedroom .adjust-meeting-form-body-box3right input:checkbox").attr("checked", true)
            jQuery("#adjust-bedroom .adjust-meeting-form-body-box3left input:checkbox").attr("checked", false)
        }
        else {
            jQuery("#adjust-bedroom .adjust-meeting-form-body-box3right input:checkbox").attr("checked", false)
        }
    })
    jQuery("#adjust-bedroom .cancelbtn").click(function () {
        jQuery("#adjust-bedroom .error").hide()
        jQuery("#adjust-bedroom #contentbody_txtFromBr").val("dd/mm/yy")
        jQuery("#adjust-bedroom #contentbody_txtToBr").val("dd/mm/yy")
        jQuery("#adjust-bedroom .adjust-meeting-form-body-box input:checkbox").attr("checked", false)
        jQuery("#adjust-bedroom .adjust-meeting-form-body-box input:checkbox").attr("disabled", false)
        jQuery("#adjust-bedroom .adjust-meeting-form-body-box2 input:checkbox").attr("checked", false)
        jQuery("#adjust-bedroom .adjust-meeting-form-body-box2 input:checkbox").attr("disabled", false)
        jQuery("#adjust-bedroom .adjust-meeting-form-body-box1right input:checkbox").attr("checked", false)
        jQuery("#adjust-bedroom .adjust-meeting-form-body-box1right input:checkbox").attr("disabled", false)
        jQuery("#adjust-bedroom .adjust-meeting-form-body-box2right input:checkbox").attr("checked", false)
        jQuery("#adjust-bedroom .adjust-meeting-form-body-box2right input:checkbox").attr("disabled", false)
        jQuery("#adjust-bedroom .adjust-meeting-form-body-box3left input:checkbox").attr("checked", false)
        jQuery("#adjust-bedroom .adjust-meeting-form-body-box3left input:checkbox").attr("disabled", true)
        jQuery("#adjust-bedroom .adjust-meeting-form-body-box3right input:checkbox").attr("checked", false)
        jQuery("#adjust-bedroom .adjust-meeting-form-body-box3right input:checkbox").attr("disabled", true)
    })
    jQuery("#adjust-bedroom #contentbody_ancSubmitBr").click(function () {
        if (jQuery("#adjust-bedroom #contentbody_txtFromBr").val() == "dd/mm/yy" || jQuery("#adjust-bedroom #contentbody_txtFromBr").val() == null || jQuery("#adjust-bedroom #contentbody_txtFromBr").val() == "") {
            jQuery("#adjust-bedroom .error").show()
            jQuery("#adjust-bedroom .error").html("Please select From date.")
            return false
        }
        if (jQuery("#adjust-bedroom #contentbody_txtToBr").val() == "dd/mm/yy" || jQuery("#adjust-bedroom #contentbody_txtToBr").val() == null || jQuery("#adjust-bedroom #contentbody_txtToBr").val() == "") {
            jQuery("#adjust-bedroom .error").show()
            jQuery("#adjust-bedroom .error").html("Please select To date.")
            return false
        }
        var fromdate = jQuery("#adjust-bedroom #contentbody_txtFromBr").val()
        var todate = jQuery("#adjust-bedroom #contentbody_txtToBr").val()
        var todayArr = todate.split('/')
        var formdayArr = fromdate.split('/')
        var fromdatecheck = new Date()
        var todatecheck = new Date()
        fromdatecheck.setFullYear(parseInt("20" + formdayArr[2], 10), (parseInt(formdayArr[1], 10) - 1), formdayArr[0])
        todatecheck.setFullYear(parseInt("20" + todayArr[2], 10), (parseInt(todayArr[1], 10) - 1), todayArr[0])
        if (fromdatecheck > todatecheck) {
            jQuery("#adjust-bedroom .error").show()
            jQuery("#adjust-bedroom .error").html("From date must be less than To date.")
            return false
        }
        if (jQuery("#adjust-bedroom .adjust-meeting-form-body-box input:checkbox:checked").length == 0) {
            jQuery("#adjust-bedroom .error").show()
            jQuery("#adjust-bedroom .error").html("Please select atleast one bedroom.")
            return false
        }
        if (jQuery("#adjust-bedroom .adjust-meeting-form-body-box2 input:checkbox:checked").length == 0) {
            jQuery("#adjust-bedroom .error").show()
            jQuery("#adjust-bedroom .error").html("Please select atleast one day.")
            return false
        }
        var MeetingRoomIds = ""
        var MeetingDays = ""
        var ClosingStatus = ""
        var OpeningStatus = ""
        var totalMR = jQuery("#adjust-bedroom .adjust-meeting-form-body-box1right input:checkbox").length
        for (var mr = 1; mr < totalMR; mr++) {
            if (jQuery("#adjust-bedroom .adjust-meeting-form-body-box1right input:checkbox:eq(" + mr + ")").is(':checked')) {
                MeetingRoomIds += jQuery("#adjust-bedroom .adjust-meeting-form-body-box1right input:checkbox:eq(" + mr + ")").attr("alt") + "|"
                if (jQuery("#adjust-bedroom .adjust-meeting-form-body-box1right input:checkbox:eq(" + mr + ")").attr("alt") == "0") {
                    break
                } 
            } 
        }
        if (MeetingRoomIds.length > 0) {
            MeetingRoomIds = MeetingRoomIds.substring(0, MeetingRoomIds.length - 1)
        }
        var totalDays = jQuery("#adjust-bedroom .adjust-meeting-form-body-box2right input:checkbox").length
        for (var td = 1; td < totalDays; td++) {
            if (jQuery("#adjust-bedroom .adjust-meeting-form-body-box2right input:checkbox:eq(" + td + ")").is(':checked')) {
                MeetingDays += "1|"
            }
            else {
                MeetingDays += "0|"
            }
            if (jQuery("#adjust-bedroom .adjust-meeting-form-body-box3left input:checkbox:eq(" + td + ")").is(':checked')) {
                ClosingStatus += "1|"
            }
            else {
                ClosingStatus += "0|"
            }
            if (jQuery("#adjust-bedroom .adjust-meeting-form-body-box3right input:checkbox:eq(" + td + ")").is(':checked')) {
                OpeningStatus += "1|"
            }
            else {
                OpeningStatus += "0|"
            } 
        }
        if (MeetingDays.length > 0) {
            MeetingDays = MeetingDays.substring(0, MeetingDays.length - 1)
        }
        if (ClosingStatus.length > 0) {
            ClosingStatus = ClosingStatus.substring(0, ClosingStatus.length - 1)
        }
        if (OpeningStatus.length > 0) {
            OpeningStatus = OpeningStatus.substring(0, OpeningStatus.length - 1)
        }
        jQuery("#adjust-bedroom .error").html("")
        jQuery("#adjust-bedroom .error").hide()
        jQuery("#adjust-bedroom #contentbody_txtFromBr").attr("disabled", false)
        jQuery("#adjust-bedroom #contentbody_txtToBr").attr("disabled", false)
        jQuery("#adjust-bedroom #contentbody_hdnBedroomIds").val(MeetingRoomIds)
        jQuery("#adjust-bedroom #contentbody_hdnBedroomDays").val(MeetingDays)
        jQuery("#adjust-bedroom #contentbody_hdnClosingStatusBr").val(ClosingStatus)
        jQuery("#adjust-bedroom #contentbody_hdnOpeningstatusBr").val(OpeningStatus)
    })
})
function isValidDate(varFrom, varTo) {
    var fromdate, todate, dt1, dt2, mon1, mon2, yr1, yr2, date1, date2
    var chkFrom = document.getElementById(varFrom)
    var chkTo = document.getElementById(varTo)
    if (trim(document.getElementById(varFrom).value, '') == '') {
        alert('From date should not be empty')
        document.getElementById(varFrom).value = ''
        document.getElementById(varFrom).focus()
        return false
    }
    else if (trim(document.getElementById(varTo).value, '') == '') {
        alert('To date should not be empty')
        document.getElementById(varTo).value = ''
        document.getElementById(varTo).focus()
        return false
    }
    if (varFrom != null && trim(document.getElementById(varFrom).value, '') != '' && varTo != null && trim(document.getElementById(varTo).value, '') != '') {
        if (checkdate(chkFrom) != true) {
            document.getElementById(varFrom).value = ''
            document.getElementById(varFrom).focus()
            return false
        }
        else if (checkdate(chkTo) != true) {
            document.getElementById(varTo).value = ''
            document.getElementById(varTo).focus()
            return false
        }
        else {
            fromdate = trim(document.getElementById(varFrom).value, '')
            todate = trim(document.getElementById(varTo).value, '')
            dt1 = parseInt(fromdate.substring(0, 2), 10)
            mon1 = parseInt(fromdate.substring(3, 5), 10)
            yr1 = parseInt(fromdate.substring(6, 10), 10)
            dt2 = parseInt(todate.substring(0, 2), 10)
            mon2 = parseInt(todate.substring(3, 5), 10)
            yr2 = parseInt(todate.substring(6, 10), 10)
            date1 = new Date(yr1, mon1, dt1)
            date2 = new Date(yr2, mon2, dt2)
            if (date2 <= date1) {
                alert("To date Should be greater than From date")
                document.getElementById(varTo).value = ''
                document.getElementById(varTo).focus()
                return false
            } 
        } 
    }
    return true
}
