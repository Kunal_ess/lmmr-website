﻿
/*
* gatsPopup - http:/www.ganeshatechnosystems.com
*
* Uses the built in easing capabilities added In jQuery 1.1
* to offer multiple easing options
*
* TERMS OF USE - GTS Popup
* 
* Open source under the BSD License. 
* 
* Copyright å© 2011 Ganesh Techno Systems
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without modification, 
* are permitted provided that the following conditions are met:
* 
* Redistributions of source code must retain the above copyright notice, this list of 
* conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list 
* of conditions and the following disclaimer in the documentation and/or other materials 
* provided with the distribution.
* 
* Neither the name of the author nor the names of contributors may be used to endorse 
* or promote products derived from this software without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
*  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
*  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
* AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
*  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
* OF THE POSSIBILITY OF SUCH DAMAGE. 
*
*/
String.prototype.contains = function (it) { return this.indexOf(it) != -1; };

// set viewport
window.viewport =
        {
            height: function () {
                return jQuery(window).height();
            },

            width: function () {
                return jQuery(window).width();
            },

            scrollTop: function () {
                return jQuery(window).scrollTop();
            },

            scrollLeft: function () {
                return jQuery(window).scrollLeft();
            }
        };

// display the lightbox
function lightbox(insertContent, elementId, ajaxContentUrl) {

    // add lightbox/shadow <div/>'s if not previously added
    if (jQuery('#lightbox').size() == 0) {
        var theLightbox = jQuery('<div id="lightbox" style="border-width: 5px;border-color: #148AAD;border-style: solid;z-index: 10015;position: absolute;	top: 0;left: 50%;width: 570px;margin-left: -250px;background: #fff;display: none;padding:15px 18px;"/>');
        var theShadow = jQuery('<div id="lightbox-shadow" style="position: absolute;top: 0;left: 0;width: 100%;background: #000;filter: alpha(opacity=75);-moz-opacity: 0.75;-khtml-opacity: 0.75;opacity: 0.75;z-index: 10010;"/>');
        jQuery(theShadow).click(function (e) {
            hideLightBox();
        });
        jQuery('body').append(theShadow);
        jQuery('body').append(theLightbox);
    }

    // remove any previously added content
    jQuery('#lightbox').empty();

    // insert HTML content
    if (insertContent != null) {
        jQuery('#lightbox').append(insertContent);
    }

    // insert HTML content from an element
    if (elementId != null) {
        jQuery('#lightbox').append(jQuery('#' + elementId + '').html());
    }
        
    // insert AJAX content
    if (ajaxContentUrl != null) {
        // temporarily add a "Loading..." message in the lightbox
        jQuery('#lightbox').append('<p class="loading">Loading...</p>');

        if (ajaxContentUrl.contains("?")) {
            ajaxContentUrl = ajaxContentUrl + "&timestamp=" + new Date().getTime().toString();
        }
        else {
            ajaxContentUrl = ajaxContentUrl + "?timestamp=" + new Date().getTime().toString();
        }
        
        // request AJAX content
        jQuery.ajax({
            type: 'GET',
            url: ajaxContentUrl,
            success: function (data) {
                // remove "Loading..." message and append AJAX content
                jQuery('#lightbox').empty();
                jQuery('#lightbox').append(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert('AJAX Failure! : ' + xhr.status);
                alert(thrownError);
                hideLightBox();
            }
        });
    }

    // move the lightbox to the current window top + 100px
    jQuery('#lightbox').css('top', jQuery(window).scrollTop() + 50 + 'px');
    
    // change height of shadow to height of documnet
    jQuery('#lightbox-shadow').height(jQuery(document).height() + 300);

    // display the lightbox
    jQuery('#lightbox').show();
    jQuery('#lightbox-shadow').show();
};

// close the lightbox
function hideLightBox() {
    // hide lightbox/shadow <div/>'s
    jQuery('#lightbox').hide('fast');
    jQuery('#lightbox-shadow').hide('fast');

    // remove contents of lightbox in case a video or other content is actively playing
    jQuery('#lightbox').empty();
};


function getPageSize() {
    var xScroll, yScroll;

    if (window.innerHeight && window.scrollMaxY) {
        xScroll = window.innerWidth + window.scrollMaxX;
        yScroll = window.innerHeight + window.scrollMaxY;
    } else if (document.body.scrollHeight > document.body.offsetHeight) { // all but Explorer Mac
        xScroll = document.body.scrollWidth;
        yScroll = document.body.scrollHeight;
    } else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
        xScroll = document.body.offsetWidth;
        yScroll = document.body.offsetHeight;
    }

    var windowWidth, windowHeight;

    if (self.innerHeight) {	// all except Explorer
        if (document.documentElement.clientWidth) {
            windowWidth = document.documentElement.clientWidth;
        } else {
            windowWidth = self.innerWidth;
        }
        windowHeight = self.innerHeight;
    } else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
        windowWidth = document.documentElement.clientWidth;
        windowHeight = document.documentElement.clientHeight;
    } else if (document.body) { // other Explorers
        windowWidth = document.body.clientWidth;
        windowHeight = document.body.clientHeight;
    }

    // for small pages with total height less then height of the viewport
    if (yScroll < windowHeight) {
        pageHeight = windowHeight;
    } else {
        pageHeight = yScroll;
    }

    // for small pages with total width less then width of the viewport
    if (xScroll < windowWidth) {
        pageWidth = xScroll;
    } else {
        pageWidth = windowWidth;
    }

    return [pageWidth, pageHeight];
}
