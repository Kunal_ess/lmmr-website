﻿
//Common.js created by vaibhav : 9,Nov,2011

function CheckNullOrUndefined(item) {
    if (typeof (item) == "undefined" || item == null) {
        return true;
    }
    else {
        return false;
    }
};
function checkReadpermission() {
    var CanReadChecked = false;

}
function gridValidation(gridClientID, postbackButtonUniqueID) {
    var flagLeastChecked = false;
    jQuery.each($("#" + gridClientID + " input[type='checkbox']"), function () {
        if (this.checked === true) {
            flagLeastChecked = true;
        }
    });

    if (flagLeastChecked == false) {

        var validatorWrapper = new jQueryDialogWrapper("Information", "Please select at least one row.", true, 'auto', 'auto', true, true);
        validatorWrapper.show();
        dialogWrapper.moveToTop();

        return false;
    }
    else {
        var validatorWrapper = new jQueryDialogWrapper("Please conform!", "Do you want to delete the selected items? Any entity on which some other data is dependent cannot be deleted.", true, 'auto', 'auto', true, true, function () { __doPostBack(postbackButtonUniqueID, ''); });
        validatorWrapper.show();
        dialogWrapper.moveToTop();
        //        if (validatorWrapper.show()) {
        //            
        //           
        //        }
        //        else {
        //            return false;
        //        }
    }
};

function ToggleAllGridCheckboxes(gridID, checkboxID) {
    var temp = jQuery("#" + checkboxID).prop('checked');
    //    $('#' + gridID +' >tbody >tr >td >input:checkbox').attr('checked', temp); // prop is new keyword
    //    $('#' + gridID +' >tbody >tr >td >input:checkbox').attr('checked', function(){
    //    this.checked = temp;
    //    });
    jQuery('#' + gridID + ' :checkbox').attr('checked', temp);
};

function ResetScrollPosition() {
    setTimeout("window.scrollTo(0,0)", 0);
};

function ResetScrollPositionSetTrueFromConfig() {
    var scrollX = document.getElementById('__SCROLLPOSITIONX');
    var scrollY = document.getElementById('__SCROLLPOSITIONY');

    if (scrollX && scrollY) {
        scrollX.value = 0;
        scrollY.value = 0;
    }
};

function ShowMessageOnPostback(title, error) {
    var dialogWrapper = new jQueryDialogWrapper(title, error, false, 'auto', 'auto', true);
    dialogWrapper.show();
    dialogWrapper.moveToTop();
};

function ShowHtmlAsPopup(title, elementID, showModel, width, height, showOK, showCancel, executeThisOnOK, executeThisBeforeClose, executeThisOnClose) {
    var html = jQuery("#" + elementID).detach();
    var dialogWrapper = new jQueryDialogWrapper(title, html, showModel, width, height, showOK, showCancel, executeThisOnOK, executeThisBeforeClose, executeThisOnClose);
    dialogWrapper.show();
    dialogWrapper.moveToTop();
    return dialogWrapper;
    ///$("#" + elementID).html(""); == $("#" + elementID).empty()
    //$("#" + elementID).remove();// removes the element and all attached events and jquery object
    //$("#" + elementID).detach();// removes the element and preserve all attached events and jquery object so that it can be inserted somewhere
    //$("#" + elementID).unwrap();// removes the elemnt but leaves all childrens
};

function MicrosoftAjaxPopup(title, elementID, showModel, width, height, showOK, showCancel, executeThisOnOK, executeThisBeforeClose, executeThisOnClose) {
    var html = jQuery("#" + elementID).detach();
    var dialogWrapper = new jQueryDialogWrapper(title, elementID, showModel, width, height, showOK, showCancel, executeThisOnOK, executeThisBeforeClose, executeThisOnClose);
    dialogWrapper.show();
    return dialogWrapper;
    ///$("#" + elementID).html(""); == $("#" + elementID).empty()
    //$("#" + elementID).remove();// removes the element and all attached events and jquery object
    //$("#" + elementID).detach();// removes the element and preserve all attached events and jquery object so that it can be inserted somewhere
    //$("#" + elementID).unwrap();// removes the elemnt but leaves all childrens
};

function jQueryAjaxPopup(title, elementID, showModel, width, height, showOK, showCancel, executeThisOnOK, executeThisBeforeClose, executeThisOnClose) {
    var html = jQuery("#" + elementID).detach();
    var dialogWrapper = new jQueryDialogWrapper(title, elementID, showModel, width, height, showOK, showCancel, executeThisOnOK, executeThisBeforeClose, executeThisOnClose);
    dialogWrapper.show();
    dialogWrapper.moveToTop();
    return dialogWrapper;
    ///$("#" + elementID).html(""); == $("#" + elementID).empty()
    //$("#" + elementID).remove();// removes the element and all attached events and jquery object
    //$("#" + elementID).detach();// removes the element and preserve all attached events and jquery object so that it can be inserted somewhere
    //$("#" + elementID).unwrap();// removes the elemnt but leaves all childrens
};

Date.prototype.DaysBetween = function () {
    var intMilDay = 24 * 60 * 60 * 1000;
    var intMilDif = arguments[0] - this;
    var intDays = Math.floor(intMilDif / intMilDay);
    return intDays;
};
Date.prototype.MonthsBetween = function () {
    var date1, date2, negPos;
    if (arguments[0] > this) {
        date1 = this;
        date2 = arguments[0];
        negPos = 1;
    }
    else {
        date2 = this;
        date1 = arguments[0];
        negPos = -1;
    }

    if (date1.getFullYear() == date2.getFullYear()) {
        return negPos * (date2.getMonth() - date1.getMonth());
    }
    else {
        var mT = 11 - date1.getMonth();
        mT += date2.getMonth() + 1;
        mT += (date2.getFullYear() - date1.getFullYear() - 1) * 12;
        return negPos * mT;
    }
};


function validateUsingWrapper(validatorWrapper,validationGroupClass) {
    if (!validatorWrapper.validate(validationGroupClass)) {
        return false;
    }
    else {
        return true;
    }
};



function ConvertToNumber(val) {
    var str = val;
    if (val != null) {
        str = str.replace(/,/gi, "");
    }
    else {
        str = 0;
    }
    return str;
}

function moneyConvert(value) {
    var buf = "";
    var sBuf = "";
    var j = 0;
    value = String(value);

    if (value.indexOf(".") > 0) {
        buf = value.substring(0, value.indexOf("."));
    } else {
        buf = value;
    }
    if (buf.length % 3 != 0 && (buf.length / 3 - 1) > 0) {
        sBuf = buf.substring(0, buf.length % 3) + ",";
        buf = buf.substring(buf.length % 3);
    }
    j = buf.length;
    for (var i = 0; i < (j / 3 - 1); i++) {
        sBuf = sBuf + buf.substring(0, 3) + ",";
        buf = buf.substring(3);
    }
    sBuf = sBuf + buf;
    if (value.indexOf(".") > 0) {
        value = sBuf + value.substring(value.indexOf("."));
    }
    else {
        value = sBuf;
    }
    return value;
}