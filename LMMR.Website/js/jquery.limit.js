(function (jQuery) {
    jQuery.fn.limita = function (options) {

        var defaults = {
            limit: 200,
            id_result: false,
            alertClass: false
        }

        var options = jQuery.extend(defaults, options);

        return this.each(function () {

            var caratteri = options.limit;

            if (options.id_result != false) {
                jQuery("#" + options.id_result).append("Ti restano <strong>" + caratteri + "</strong> caratteri");
            }

            jQuery(this).keyup(function () {
                if (jQuery(this).val().length > caratteri) {
                    jQuery(this).val(jQuery(this).val().substr(0, caratteri));
                }

                if (options.id_result != false) {

                    var restanti = caratteri - jQuery(this).val().length;
                    jQuery("#" + options.id_result).html("Ti restano <strong>" + restanti + "</strong> caratteri");

                    if (restanti <= 10) {
                        jQuery("#" + options.id_result).addClass(options.alertClass);
                    }
                    else {
                        jQuery("#" + options.id_result).removeClass(options.alertClass);
                    }
                }
            });

        });
    };
})(jQuery);