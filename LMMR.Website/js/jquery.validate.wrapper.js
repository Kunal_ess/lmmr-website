﻿var dialogIdSeed = 1000000000;
function jQueryValidatorWrapper(formId, rules, messages, controlGroupClass) {
    // Get an Id for the "<div>" to diaply the error messages.
    // The Id is made sure to be unique in the web page.
    var dialogId = "V_dia_log" + dialogIdSeed++;
   // 
    while (jQuery("#" + dialogId).length != 0) {

        dialogId = "V_dia_log" + dialogIdSeed++;
    }

    // create the error message "div" and add it to the dom.
    // it will be use to display the validation error messages.
    var dialogText = "<div id='" + dialogId
            + "' title='Please correct the errors ...'>"
            + "<label></label><br/>"
            + "<ul></ul></div>";
    //console.log(dialogText);
    jQuery("body").append(dialogText);
    var $dialog = jQuery("#" + dialogId);
    var $label = jQuery("#" + dialogId + ">label"); // vaibhav: to display specific message
    var $ul = jQuery("#" + dialogId + ">ul");

    $dialog.dialog({
        autoOpen: false,
        draggable: true,
        resizable: true,
        modal: true,
        closeOnEscape: true,
        stack: true,
        close: function (event, ui) {
            $ul.html("");
            $ul.text("");
            jQuery(".ui-widget-overlay").css("display", "none");
        }
    });

    var validationGroupClass = controlGroupClass;
    // hook up the form, the validation rules, and messages with jQuery validate.
    var showErrorMessage = true;
    var validator = jQuery("#" + formId).validate({
        onchange: true,
        onsubmit: false, //vaibhav: to stop on every submit
        ignore: ':hidden',
        rules: rules,
        focusInvalid: true,
        messages: messages,
        errorPlacement: function (error, element) {
            if (showErrorMessage) {
                //
                var li = document.createElement("li")
                li.appendChild(document
                    .createTextNode(error.html()));
                $ul.append(li);
            }
        },
        showErrors: function (errorMap, errorList) {
            if ((errorList.length != 0) && showErrorMessage) {
                $ul.html("");
                $ul.text("");
                $label.html("Your form contains "
                                   + this.numberOfInvalids()
                              + " errors, see details below.");
                //console.log($label.html());
                this.defaultShowErrors();
                $dialog.dialog('open');
                $dialog.focus();
            }
        }
    });

    // This is the function to call whem make the validation
    this.validate = function (controlGroupClass) {
        validationGroupClass = controlGroupClass;
        //
        $ul.html("");
        $ul.text("");
        showErrorMessage = true;

        var result = validator.form(validationGroupClass);
        //var result = validator.valid();
        //                $dialog.dialog('open');
        //                $dialog.focus(); 
        showErrorMessage = false;

        //
        return result;
    };

}