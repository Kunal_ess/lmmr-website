﻿

//Functions

function initialize_map(id) {
    //alert(address);
    map = new GMap2(document.getElementById(id));
     map.setCenter(default_point, default_zoom);
    map.addControl(new GLargeMapControl);
    GEvent.addListener(map, "click", getAddress);
    geocoder = new GClientGeocoder();
    get_latlong(address);
}

function getAddress(overlay, latlng) {
    if (latlng != null) {
        address = latlng;
        geocoder.getLocations(latlng, showAddress);
    }
}

function get_latlong(address) {
   
    if (geocoder) {
      
        geocoder.getLatLng(
                  address,
                  function (point) {
                      if (!point) {
                          //alert(address + " not found");
                      } else {
                          map.setCenter(point, 13);
                          var greenIcon = new GIcon(G_DEFAULT_ICON);
                          greenIcon.image = "http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
                          var markerOptions = { icon: greenIcon };
                          var marker = new GMarker(point, markerOptions);
                          map.addOverlay(marker);
                          marker.openInfoWindowHtml(address);

                          txt_lat.value = point.y;
                          txt_long.value = point.x;
                      }
                  }
                );
    }
}


function showAddress(response) {
    map.clearOverlays();
    if (!response || response.Status.code != 200) {
        alert("Status Code:" + response.Status.code);
    } else {
        place = response.Placemark[0];
        point = new GLatLng(place.Point.coordinates[1], place.Point.coordinates[0]);
        var greenIcon = new GIcon(G_DEFAULT_ICON);
        greenIcon.image = "http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
        var markerOptions = { icon: greenIcon };

        //var marker = new GMarker(point, markerOptions);
        marker = new GMarker(point, markerOptions);
        map.addOverlay(marker);
        map.setCenter(point);

        try {

            jQuery("#" + txt_lat).val(point.y);
            jQuery("#" + txt_long).val(point.x);
           marker.openInfoWindowHtml(
                    '<b>orig latlng:</b>' + response.name + '<br/>' +
'<b>latlng:</b>' + place.Point.coordinates[0] + "," + place.Point.coordinates[1] + '<br>' +
           //'<b>Status Code:</b>' + response.Status.code + '<br>' +
           //'<b>Status Request:</b>' + response.Status.request + '<br>' +
'<b>Address:</b>' + place.address + '<br>' +
'<b>Accuracy:</b>' + place.AddressDetails.Accuracy + '<br>' +
'<b>Country:</b> ' + place.AddressDetails.Country.CountryName);

        }

        catch (err) {

        }
    }
}


