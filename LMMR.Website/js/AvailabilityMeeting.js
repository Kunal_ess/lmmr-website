﻿(function (jQuery) {
    jQuery.fn.MyPlugin = function (options) {
        var settings = jQuery.extend({}, jQuery.fn.MyPlugin.defaults, options)
        var weeks = ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA']
        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
        var weekdays = parseInt(settings.StartWeek)
        var AvailabilityStatus = ['opened', 'closed', 'booked', 'sold']
        var countall = settings.MonthYearDayDate.length
        var current = 1
        function ManageCalander(indexvalue, counttotal) {
            jQuery("#divAvailability .pager_header .page_text #left").show()
            jQuery("#divAvailability .pager_header .page_text #right").show()
            jQuery("#divAvailability .pager_header #nextlink").show()
            jQuery("#divAvailability .pager_header #prevlink").show()
            if (settings.IsBedroomAvailable) {
                jQuery("#divAvailabilityBedroom .pager_header .page_text #left").show()
                jQuery("#divAvailabilityBedroom .pager_header .page_text #right").show()
                jQuery("#divAvailabilityBedroom .pager_header #nextlink").show()
                jQuery("#divAvailabilityBedroom .pager_header #prevlink").show()
            }
            if (indexvalue == 1) {
                jQuery("#divAvailability .pager_header .page_text #left").hide()
                jQuery("#divAvailability .pager_header #prevlink").hide()
                if (settings.IsBedroomAvailable) {
                    jQuery("#divAvailabilityBedroom .pager_header .page_text #left").hide()
                    jQuery("#divAvailabilityBedroom .pager_header #prevlink").hide()
                }
            }
            if (indexvalue == counttotal) {
                jQuery("#divAvailability .pager_header .page_text #right").hide()
                jQuery("#divAvailability .pager_header #nextlink").hide()
                if (settings.IsBedroomAvailable) {
                    jQuery("#divAvailabilityBedroom .pager_header .page_text #right").hide()
                    jQuery("#divAvailabilityBedroom .pager_header #nextlink").hide()
                }
            }
            if (parseInt(settings.MonthYearDayDate[indexvalue - 1][0]) > 0) {
                jQuery("#divAvailability .pager_header .page_text span").html("" + months[parseInt(settings.MonthYearDayDate[indexvalue - 1][0]) - 1] + " " + parseInt(settings.MonthYearDayDate[indexvalue - 1][1]) + "")
                if (settings.IsBedroomAvailable) {
                    jQuery("#divAvailabilityBedroom .pager_header .page_text span").html("" + months[parseInt(settings.MonthYearDayDate[indexvalue - 1][0]) - 1] + " " + parseInt(settings.MonthYearDayDate[indexvalue - 1][1]) + "")
                }
            }
            jQuery("#contentbody_hdnStartingDate").val(parseInt(settings.MonthYearDayDate[indexvalue - 1][0]) + "-" + parseInt(settings.MonthYearDayDate[indexvalue - 1][1]))
            jQuery("#Calander").html("")
            if (settings.IsBedroomAvailable) {
                jQuery("#calanderBR").html("")
            }
            jQuery("#discount").html("")
            jQuery("#variation").html("")
            jQuery("#leadtime").html("")
            jQuery("#leadtimeEdit").html("")
            jQuery("#fullproperty").html("")
            var j = 0
            var fullCDVLP
            weekdays = parseInt(settings.MonthYearDayDate[indexvalue - 1][3])
            jQuery("#Loding_overlay").show()

            jQuery.ajax({
                type: "POST",
                url: settings.Webservicepath + "/GetFullPropertDetails",
                data: "HotelId=" + settings.HotelId + "&AvailabilityDate=" + settings.MonthYearDayDate[indexvalue - 1][1] + (parseInt(settings.MonthYearDayDate[indexvalue - 1][0], 10) < 10 ? "-0" + settings.MonthYearDayDate[indexvalue - 1][0] : "-" + settings.MonthYearDayDate[indexvalue - 1][0]) + "&StartDate=" + settings.MonthYearDayDate[0][1] + "-" + (parseInt(settings.StartAvailableDates[1], 10) < 10 ? "0" + parseInt(settings.StartAvailableDates[1], 10) : parseInt(settings.StartAvailableDates[1], 10)) + "-" + (parseInt(settings.StartAvailableDates[0], 10) < 10 ? "0" + settings.StartAvailableDates[0] : settings.StartAvailableDates[0]) + "&EndDate=" + settings.MonthYearDayDate[settings.MonthYearDayDate.length - 1][1] + "-" + (parseInt(settings.EndAvailableDate[1], 10) < 10 ? "0" + settings.EndAvailableDate[1] : settings.EndAvailableDate[1]) + "-" + (parseInt(settings.EndAvailableDate[0]) < 10 ? "0" + settings.EndAvailableDate[0] : settings.EndAvailableDate[0]),
                success: function (response, status) {
                    fullCDVLP = response
                    var fullprop
                    var calander
                    var variation
                    var discount
                    var leedtime
                    var leadtimeEdit
                    var calanderBR
                    for (j = 0; j <= parseInt(settings.MonthYearDayDate[indexvalue - 1][2]); j++) {
                        calander = jQuery("<td></td>").appendTo("#Calander")
                        if (settings.IsBedroomAvailable) {
                            calanderBR = jQuery("<td></td>").appendTo("#calanderBR")
                        }
                        discount = jQuery("<td></td>").appendTo("#discount")
                        variation = jQuery("<td></td>").appendTo("#variation")
                        leedtime = jQuery("<td></td>").appendTo("#leadtime")
                        leadtimeEdit = jQuery('<td></td>').appendTo("#leadtimeEdit")
                        fullprop = jQuery("<td></td>").appendTo("#fullproperty")
                        if (j > 0) {
                            if (parseInt(settings.StartAvailableDates[0]) > j && parseInt(settings.StartAvailableDates[1]) == parseInt(settings.MonthYearDayDate[indexvalue - 1][0])) {
                                calander.addClass("disable").append("" + weeks[weekdays] + "<br/><span>" + (j) + "</span>")
                                if (settings.IsBedroomAvailable) {
                                    calanderBR.addClass("disable").append("" + weeks[weekdays] + "<br/><span>" + (j) + "</span>")
                                }
                                discount.addClass("disable SetHeight")
                                variation.addClass("disable SetHeight")
                                leedtime.addClass("disable SetHeight")
                                leadtimeEdit.addClass("disable SetHeight")
                                fullprop.addClass("disable SetHeight")
                            }
                            else if (parseInt(settings.EndAvailableDate[0]) < j && parseInt(settings.EndAvailableDate[1]) == parseInt(settings.MonthYearDayDate[indexvalue - 1][0])) {
                                calander.addClass("disable").append("" + weeks[weekdays] + "<br/><span>" + (j) + "</span>")
                                if (settings.IsBedroomAvailable) {
                                    calanderBR.addClass("disable").append("" + weeks[weekdays] + "<br/><span>" + (j) + "</span>")
                                }
                                discount.addClass("disable SetHeight")
                                variation.addClass("disable SetHeight")
                                leedtime.addClass("disable SetHeight")
                                leadtimeEdit.addClass("disable SetHeight")
                                fullprop.addClass("disable SetHeight")
                            }
                            else {
                                calander.append("" + weeks[weekdays] + "<br/><span>" + (j) + "</span>")
                                if (settings.IsBedroomAvailable) {
                                    calanderBR.append("" + weeks[weekdays] + "<br/><span>" + (j) + "</span>")
                                }
                                discount.addClass("SetHeight")
                                variation.addClass("SetHeight")
                                leedtime.addClass("SetHeight")
                                leadtimeEdit.addClass("SetHeight")
                                fullprop.addClass("SetHeight")
                            }
                            jQuery(fullCDVLP).find("ArrayOfAvailabilityWithSpandp").each(function () {
                                jQuery(this).find("AvailabilityWithSpandp").each(function () {
                                    if ((settings.MonthYearDayDate[indexvalue - 1][1] + "-" + (parseInt(settings.MonthYearDayDate[indexvalue - 1][0], 10) < 10 ? "0" + settings.MonthYearDayDate[indexvalue - 1][0] : settings.MonthYearDayDate[indexvalue - 1][0]) + (j < 10 ? "-0" + (j) + "T00:00:00" : "-" + (j) + "T00:00:00")) == jQuery(this).find("AvailabilityDate").text()) {
                                        fullprop.attr("title", (j) + "/" + parseInt(settings.MonthYearDayDate[indexvalue - 1][0]) + "/" + parseInt(settings.MonthYearDayDate[indexvalue - 1][1]) + "|full property|" + AvailabilityStatus[parseInt(jQuery(this).find("FullPropertyStatus").text())] + "|" + jQuery(this).find("Id").text())
                                        fullprop.addClass(AvailabilityStatus[parseInt(jQuery(this).find("FullPropertyStatus").text())])
                                        calander.addClass(AvailabilityStatus[parseInt(jQuery(this).find("FullPropertyStatus").text())])
                                        if (settings.IsBedroomAvailable) {
                                            calanderBR.addClass(AvailabilityStatus[parseInt(jQuery(this).find("FullPropertyStatus").text())])
                                        }
                                        discount.addClass(AvailabilityStatus[parseInt(jQuery(this).find("FullPropertyStatus").text())]).html(jQuery(this).find("DdrPercent").text().split('.')[0])
                                        variation.addClass(AvailabilityStatus[parseInt(jQuery(this).find("FullPropertyStatus").text())]).html(jQuery(this).find("MeetingRoomPercent").text().split('.')[0])
                                        leedtime.addClass(AvailabilityStatus[parseInt(jQuery(this).find("FullPropertyStatus").text())]).html(jQuery(this).find("LeadTimeForMeetingRoom").text())
                                        leadtimeEdit.html("<input type='text' maxlength='2' class='myinput' value='" + jQuery(this).find("LeadTimeForMeetingRoom").text() + "' />").addClass(AvailabilityStatus[parseInt(jQuery(this).find("FullPropertyStatus").text())])
                                        if (parseInt(jQuery(this).find("FullPropertyStatus").text()) == 0 || parseInt(jQuery(this).find("FullPropertyStatus").text()) == 1) {
                                            fullprop.bind("click", function () {
                                                var d = jQuery(this)
                                                if (jQuery(this).hasClass("closed")) {
                                                    var titoblinebooked = jQuery(this).attr("title").replace('closed', 'opened')
                                                    jQuery.ajax({
                                                        type: "POST",
                                                        url: settings.Webservicepath + "/UpdateFullProperty",
                                                        data: "FullPropDetails=" + titoblinebooked + "",
                                                        success: function (msg) {
                                                            d.removeClass("closed").attr("title", titoblinebooked)
                                                            jQuery("#tblMR tr td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                                            if (settings.IsBedroomAvailable) {
                                                                jQuery("#contentbody_tblBR tr td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                                            }
                                                        }
                                                    })
                                                }
                                                else {
                                                    var titclosed = jQuery(this).attr("title").replace('opened', 'closed')
                                                    jQuery.ajax({
                                                        type: "POST",
                                                        url: settings.Webservicepath + "/UpdateFullProperty",
                                                        data: "FullPropDetails=" + titclosed + "",
                                                        success: function (msg) {
                                                            d.removeClass("opened").attr("title", titclosed)
                                                            jQuery("#tblMR tr td:nth-child(" + (parseInt(titclosed.split('/')[0]) + 1) + ")").removeClass("opened").addClass("closed")
                                                            if (settings.IsBedroomAvailable) {
                                                                jQuery("#contentbody_tblBR tr td:nth-child(" + (parseInt(titclosed.split('/')[0]) + 1) + ")").removeClass("opened").addClass("closed")
                                                            }
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    }
                                })
                            })
                            weekdays++
                            if (weekdays >= 7) {
                                weekdays = 0
                            }
                        }
                    }
                    jQuery("#Loding_overlay").hide()
                }
            })
        }
        function MoveNextBedRoom(indexvalue, counttotal) {
            for (var p = 0; p < settings.BedRoomDivs.length && settings.BedRoomDivs[p] != null; p++) {
                jQuery("#" + settings.BedRoomDivs[p] + "_bedroom").html("")
                jQuery("#" + settings.BedRoomDivs[p] + "_AvailBedroom").html("")
                jQuery("#" + settings.BedRoomDivs[p] + "_EditBedroom").html("")
            }
            var fullCDVLP
            weekdays = parseInt(settings.MonthYearDayDate[indexvalue - 1][3])
            jQuery("#Loding_overlay").show()
            jQuery.ajax({
                type: "POST",
                url: settings.Webservicepath + "/GetBedRoomDetails",
                data: "HotelId=" + settings.HotelId + "&AvailabilityDate=" + settings.MonthYearDayDate[indexvalue - 1][1] + (parseInt(settings.MonthYearDayDate[indexvalue - 1][0], 10) < 10 ? "-0" + settings.MonthYearDayDate[indexvalue - 1][0] : "-" + settings.MonthYearDayDate[indexvalue - 1][0]) + "&StartDate=" + settings.MonthYearDayDate[0][1] + "-" + (parseInt(settings.StartAvailableDates[1], 10) < 10 ? "0" + settings.StartAvailableDates[1] : settings.StartAvailableDates[1]) + "-" + (parseInt(settings.StartAvailableDates[0], 10) < 10 ? "0" + settings.StartAvailableDates[0] : settings.StartAvailableDates[0]) + "&EndDate=" + settings.MonthYearDayDate[settings.MonthYearDayDate.length - 1][1] + "-" + (parseInt(settings.EndAvailableDate[1], 10) < 10 ? "0" + settings.EndAvailableDate[1] : settings.EndAvailableDate[1]) + "-" + (parseInt(settings.EndAvailableDate[0], 10) < 10 ? "0" + settings.EndAvailableDate[0] : settings.EndAvailableDate[0]),
                success: function (response, status) {
                    fullCDVLP = response
                    var BedRoom = new Array()
                    var BedroomAvailability = new Array()
                    var BadRoomAvailabilityEdit = new Array()
                    for (j = 0; j <= parseInt(settings.MonthYearDayDate[indexvalue - 1][2]); j++) {
                        for (var divcount = 0; divcount < settings.BedRoomDivs.length && settings.BedRoomDivs[divcount] != null; divcount++) {
                            BedRoom[divcount] = jQuery("<td></td>").appendTo("#" + settings.BedRoomDivs[divcount] + "_bedroom")
                            BedroomAvailability[divcount] = jQuery("<td></td>").appendTo("#" + settings.BedRoomDivs[divcount] + "_AvailBedroom")
                            BadRoomAvailabilityEdit[divcount] = jQuery("<td></td>").appendTo("#" + settings.BedRoomDivs[divcount] + "_EditBedroom")
                            if (j > 0) {
                                if (parseInt(settings.StartAvailableDates[0]) > j && parseInt(settings.StartAvailableDates[1]) == parseInt(settings.MonthYearDayDate[indexvalue - 1][0])) {
                                    BedRoom[divcount].addClass("disable SetHeight")
                                    BedroomAvailability[divcount].addClass("disable SetHeight")
                                    BadRoomAvailabilityEdit[divcount].addClass("disable SetHeight")
                                }
                                else if (parseInt(settings.EndAvailableDate[0]) < j && parseInt(settings.EndAvailableDate[1]) == parseInt(settings.MonthYearDayDate[indexvalue - 1][0])) {
                                    BedRoom[divcount].addClass("disable SetHeight")
                                    BedroomAvailability[divcount].addClass("disable SetHeight")
                                    BadRoomAvailabilityEdit[divcount].addClass("disable SetHeight")
                                }
                                else {
                                    BedRoom[divcount].addClass("SetHeight")
                                    BedroomAvailability[divcount].addClass("SetHeight")
                                    BadRoomAvailabilityEdit[divcount].addClass("SetHeight")
                                }
                                jQuery(fullCDVLP).find("ArrayOfViewAvailabilityOfBedRoom").each(function () {
                                    jQuery(this).find("ViewAvailabilityOfBedRoom").each(function () {
                                        if ((settings.MonthYearDayDate[indexvalue - 1][1] + "-" + (parseInt(settings.MonthYearDayDate[indexvalue - 1][0]) < 10 ? "0" + settings.MonthYearDayDate[indexvalue - 1][0] : settings.MonthYearDayDate[indexvalue - 1][0]) + (j < 10 ? "-0" + (j) + "T00:00:00" : "-" + (j) + "T00:00:00")) == jQuery(this).find("AvailabilityDate").text()) {
                                            if (parseInt(jQuery(this).find("RoomId").text()) == parseInt(settings.BedRoomDivs[divcount])) {
                                                BedRoom[divcount].attr("title", (j) + "/" + parseInt(settings.MonthYearDayDate[indexvalue - 1][0]) + "/" + parseInt(settings.MonthYearDayDate[indexvalue - 1][1]) + "|Bedroom|" + AvailabilityStatus[parseInt(jQuery(this).find("MorningStatus").text())] + "|" + jQuery(this).find("Id").text()).removeClass("closed").removeClass("opened").addClass(AvailabilityStatus[parseInt(jQuery(this).find("MorningStatus").text())]).html(parseInt(jQuery(this).find("NumberOfRoomBooked").text(), 10));
                                                BedroomAvailability[divcount].removeClass("closed").removeClass("opened").addClass(AvailabilityStatus[parseInt(jQuery(this).find("MorningStatus").text())]).html(parseInt(jQuery(this).find("NumberOfRoomsAvailable").text(), 10));
                                                BadRoomAvailabilityEdit[divcount].html("<input type='text' maxlength='2' class='myinput' value='" + (parseInt(jQuery(this).find("NumberOfRoomsAvailable").text(), 10)) + "' />").removeClass("closed").removeClass("opened").addClass(AvailabilityStatus[parseInt(jQuery(this).find("MorningStatus").text())]);
                                            }
                                        }
                                    })
                                })
                            }
                        }
                    }
                    jQuery("#Loding_overlay").hide()
                    if (settings.BedRoomDivs.length >= 1) {
                        jQuery("#" + settings.BedRoomDivs[0] + "_bedroom td").bind("click", function () {
                            var parentid = jQuery(this).closest("tr").attr("id")
                            parentid = parentid.split('_')[0]
                            var mytitl = jQuery(this).attr("title")
                            if (mytitl != undefined) {
                                var counter = jQuery("#tblMR td[class*='closed']:nth-child(" + (parseInt(mytitl.split('/')[0]) + 1) + ")").length
                                var mrtotaldics = settings.DivNames.length * 2
                                var d = jQuery(this)
                                if (jQuery(this).hasClass("closed")) {
                                    var titoblinebooked = jQuery(this).attr("title").replace('closed', 'opened')
                                    jQuery.ajax({
                                        type: "POST",
                                        url: settings.Webservicepath + "/UpdateAvailabilityBR",
                                        data: "BedRoomDetails=" + titoblinebooked + "",
                                        success: function (msg) {
                                            d.removeClass("closed").addClass("opened").attr("title", titoblinebooked)
                                            jQuery("#" + parentid + "_AvailBedroom td:nth-child(" + (parseInt(mytitl.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened").attr("title", titoblinebooked)
                                            jQuery("#" + parentid + "_EditBedroom td:nth-child(" + (parseInt(mytitl.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened").attr("title", titoblinebooked)
                                            var fp = jQuery("#fullproperty td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + " )")
                                            var newTitel = fp.attr("title").replace('closed', 'opened')
                                            jQuery.ajax({
                                                type: "POST",
                                                url: settings.Webservicepath + "/UpdateonlyFullPropertyDetails",
                                                data: "FullPropDetails=" + newTitel + "",
                                                success: function (msg) {
                                                    fp.removeClass("closed").addClass("opened").attr("title", newTitel)
                                                }
                                            })
                                            jQuery("#Calander td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                            jQuery("#discount td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                            jQuery("#variation td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                            jQuery("#leadtime td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                            jQuery("#leadtimeEdit td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                            jQuery("#fullproperty td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                            if (settings.IsBedroomAvailable) {
                                                jQuery("#calanderBR td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                            }
                                        }
                                    })
                                }
                                else if (jQuery(this).hasClass("opened")) {
                                    var titclosed = jQuery(this).attr("title").replace('opened', 'closed')
                                    jQuery.ajax({
                                        type: "POST",
                                        url: settings.Webservicepath + "/UpdateAvailabilityBR",
                                        data: "BedRoomDetails=" + titclosed + "",
                                        success: function (msg) {
                                            d.removeClass('opened').addClass("closed").attr("title", titclosed)
                                            jQuery("#" + parentid + "_AvailBedroom td:nth-child(" + (parseInt(mytitl.split('/')[0]) + 1) + ")").removeClass('opened').addClass("closed").attr("title", titclosed)
                                            jQuery("#" + parentid + "_EditBedroom td:nth-child(" + (parseInt(mytitl.split('/')[0]) + 1) + ")").removeClass('opened').addClass("closed").attr("title", titclosed)
                                            var counttotalMrInIE = 0
                                            for (var p = 0; p < settings.DivNames.length; p++) {
                                                if (settings.DivNames[p] != '[' && settings.DivNames[p] != ']' && settings.DivNames[p] != ',' && settings.DivNames[p] != undefined) {
                                                    counttotalMrInIE++
                                                }
                                            }
                                            var mrtotaldics = counttotalMrInIE * 2
                                            var counttotalBrInIE = 0
                                            var counter = jQuery("#tblMR td[class*='closed']:nth-child(" + (parseInt(titclosed.split('/')[0]) + 1) + ")").length
                                            if (settings.IsBedroomAvailable) {
                                                for (var p = 0; p < settings.BedRoomDivs.length; p++) {
                                                    if (settings.BedRoomDivs[p] != '[' && settings.BedRoomDivs[p] != ']' && settings.BedRoomDivs[p] != ',' && settings.BedRoomDivs[p] != undefined) {
                                                        counttotalBrInIE++
                                                    }
                                                }
                                                var counterBr = jQuery("#contentbody_tblBR td[class*='closed']:nth-child(" + (parseInt(titclosed.split('/')[0]) + 1) + ")").length
                                                counter = counter + counterBr
                                            }
                                            mrtotaldics = mrtotaldics + (counttotalBrInIE * 3)
                                            if (mrtotaldics == counter) {
                                                var fp2 = jQuery("#fullproperty td:nth-child(" + (parseInt(titclosed.split('/')[0]) + 1) + " )")
                                                var newTitel2 = fp2.attr("title").replace('opened', 'closed')
                                                jQuery.ajax({
                                                    type: "POST",
                                                    url: settings.Webservicepath + "/UpdateFullProperty",
                                                    data: "FullPropDetails=" + newTitel2 + "",
                                                    success: function (msg) {
                                                        fp2.removeClass("opened").attr("title", newTitel2)
                                                        jQuery("#tblMR tr td:nth-child(" + (parseInt(newTitel2.split('/')[0]) + 1) + ")").removeClass("opened").addClass("closed")
                                                        if (settings.IsBedroomAvailable) {
                                                            jQuery("#contentbody_tblBR tr td:nth-child(" + (parseInt(newTitel2.split('/')[0]) + 1) + ")").removeClass("opened").addClass("closed")
                                                        }
                                                    }
                                                })
                                            }
                                        }
                                    })
                                }
                            }
                        })
                    }
                    if (settings.BedRoomDivs.length >= 2) {
                        jQuery("#" + settings.BedRoomDivs[1] + "_bedroom td").bind("click", function () {
                            var parentid = jQuery(this).closest("tr").attr("id")
                            parentid = parentid.split('_')[0]
                            var mytitl = jQuery(this).attr("title")
                            if (mytitl != undefined) {
                                var counter = jQuery("#tblMR td[class*='closed']:nth-child(" + (parseInt(mytitl.split('/')[0]) + 1) + ")").length
                                var mrtotaldics = settings.DivNames.length * 2
                                var d = jQuery(this)
                                if (jQuery(this).hasClass("closed")) {
                                    var titoblinebooked = jQuery(this).attr("title").replace('closed', 'opened')
                                    jQuery.ajax({
                                        type: "POST",
                                        url: settings.Webservicepath + "/UpdateAvailabilityBR",
                                        data: "BedRoomDetails=" + titoblinebooked + "",
                                        success: function (msg) {
                                            d.removeClass("closed").addClass("opened").attr("title", titoblinebooked)
                                            jQuery("#" + parentid + "_AvailBedroom td:nth-child(" + (parseInt(mytitl.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened").attr("title", titoblinebooked)
                                            jQuery("#" + parentid + "_EditBedroom td:nth-child(" + (parseInt(mytitl.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened").attr("title", titoblinebooked)
                                            var fp = jQuery("#fullproperty td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + " )")
                                            var newTitel = fp.attr("title").replace('closed', 'opened')
                                            jQuery.ajax({
                                                type: "POST",
                                                url: settings.Webservicepath + "/UpdateonlyFullPropertyDetails",
                                                data: "FullPropDetails=" + newTitel + "",
                                                success: function (msg) {
                                                    fp.removeClass("closed").addClass("opened").attr("title", newTitel)
                                                }
                                            })
                                            jQuery("#Calander td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                            jQuery("#discount td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                            jQuery("#variation td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                            jQuery("#leadtime td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                            jQuery("#leadtimeEdit td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                            jQuery("#fullproperty td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                            if (settings.IsBedroomAvailable) {
                                                jQuery("#calanderBR td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                            }
                                        }
                                    })
                                }
                                else if (jQuery(this).hasClass("opened")) {
                                    var titclosed = jQuery(this).attr("title").replace('opened', 'closed')
                                    jQuery.ajax({
                                        type: "POST",
                                        url: settings.Webservicepath + "/UpdateAvailabilityBR",
                                        data: "BedRoomDetails=" + titclosed + "",
                                        success: function (msg) {
                                            d.removeClass('opened').addClass("closed").attr("title", titclosed)
                                            jQuery("#" + parentid + "_AvailBedroom td:nth-child(" + (parseInt(mytitl.split('/')[0]) + 1) + ")").removeClass('opened').addClass("closed").attr("title", titclosed)
                                            jQuery("#" + parentid + "_EditBedroom td:nth-child(" + (parseInt(mytitl.split('/')[0]) + 1) + ")").removeClass('opened').addClass("closed").attr("title", titclosed)
                                            var counttotalMrInIE = 0
                                            for (var p = 0; p < settings.DivNames.length; p++) {
                                                if (settings.DivNames[p] != '[' && settings.DivNames[p] != ']' && settings.DivNames[p] != ',' && settings.DivNames[p] != undefined) {
                                                    counttotalMrInIE++
                                                }
                                            }
                                            var mrtotaldics = counttotalMrInIE * 2
                                            var counttotalBrInIE = 0
                                            var counter = jQuery("#tblMR td[class*='closed']:nth-child(" + (parseInt(titclosed.split('/')[0]) + 1) + ")").length
                                            if (settings.IsBedroomAvailable) {
                                                for (var p = 0; p < settings.BedRoomDivs.length; p++) {
                                                    if (settings.BedRoomDivs[p] != '[' && settings.BedRoomDivs[p] != ']' && settings.BedRoomDivs[p] != ',' && settings.BedRoomDivs[p] != undefined) {
                                                        counttotalBrInIE++
                                                    }
                                                }
                                                var counterBr = jQuery("#contentbody_tblBR td[class*='closed']:nth-child(" + (parseInt(titclosed.split('/')[0]) + 1) + ")").length
                                                counter = counter + counterBr
                                            }
                                            mrtotaldics = mrtotaldics + (counttotalBrInIE * 3)
                                            if (mrtotaldics == counter) {
                                                var fp2 = jQuery("#fullproperty td:nth-child(" + (parseInt(titclosed.split('/')[0]) + 1) + " )")
                                                var newTitel2 = fp2.attr("title").replace('opened', 'closed')
                                                jQuery.ajax({
                                                    type: "POST",
                                                    url: settings.Webservicepath + "/UpdateFullProperty",
                                                    data: "FullPropDetails=" + newTitel2 + "",
                                                    success: function (msg) {
                                                        fp2.removeClass("opened").attr("title", newTitel2)
                                                        jQuery("#tblMR tr td:nth-child(" + (parseInt(newTitel2.split('/')[0]) + 1) + ")").removeClass("opened").addClass("closed")
                                                        if (settings.IsBedroomAvailable) {
                                                            jQuery("#contentbody_tblBR tr td:nth-child(" + (parseInt(newTitel2.split('/')[0]) + 1) + ")").removeClass("opened").addClass("closed")
                                                        }
                                                    }
                                                })
                                            }
                                        }
                                    })
                                }
                            }
                        })
                    }
                }
            })
        }
        function MoveNext(indexvalue, counttotal) {
            jQuery("#contentbody_hdnCurrentPanel").val(indexvalue)
            ManageCalander(indexvalue, counttotal)
            if (settings.IsBedroomAvailable == true) {
                MoveNextBedRoom(indexvalue, counttotal)
            }
            for (var p = 0; p < settings.DivNames.length; p++) {
                nextMeetingRoom(indexvalue, counttotal, settings.DivNames[p])
            }
        }
        function nextMeetingRoom(indexvalue, counttotal, meetingroomID) {
            var MeetingRoomDetails
            jQuery("#Loding_overlay").show()
            jQuery("#" + meetingroomID + "_MrMorning").html("")
            jQuery("#" + meetingroomID + "_MrAfternoon").html("")
            jQuery.ajax({
                type: "POST",
                url: settings.Webservicepath + "/GetMeetingRoomDetails",
                data: "HotelId=" + settings.HotelId + "&AvailabilityDate=" + settings.MonthYearDayDate[indexvalue - 1][1] + (parseInt(settings.MonthYearDayDate[indexvalue - 1][0], 10) < 10 ? "-0" + settings.MonthYearDayDate[indexvalue - 1][0] : "-" + settings.MonthYearDayDate[indexvalue - 1][0]) + "&StartDate=" + settings.MonthYearDayDate[0][1] + "-" + (parseInt(settings.StartAvailableDates[1], 10) < 10 ? "0" + settings.StartAvailableDates[1] : settings.StartAvailableDates[1]) + "-" + (parseInt(settings.StartAvailableDates[0], 10) < 10 ? "0" + settings.StartAvailableDates[0] : settings.StartAvailableDates[0]) + "&EndDate=" + settings.MonthYearDayDate[settings.MonthYearDayDate.length - 1][1] + "-" + (parseInt(settings.EndAvailableDate[1], 10) < 10 ? "0" + settings.EndAvailableDate[1] : settings.EndAvailableDate[1]) + "-" + (parseInt(settings.EndAvailableDate[0], 10) < 10 ? "0" + settings.EndAvailableDate[0] : settings.EndAvailableDate[0]) + "&meetingroomid=" + meetingroomID,
                success: function (response, status) {
                    MeetingRoomDetails = response
                    for (j = 0; j <= parseInt(settings.MonthYearDayDate[indexvalue - 1][2]); j++) {
                        if (j == 0) {
                            jQuery("<td></td>").appendTo("#" + meetingroomID + "_MrMorning")
                            jQuery("<td></td>").appendTo("#" + meetingroomID + "_MrAfternoon")
                        }
                        else {
                            if (parseInt(settings.StartAvailableDates[0]) > j && parseInt(settings.StartAvailableDates[1]) == parseInt(settings.MonthYearDayDate[indexvalue - 1][0])) {
                                jQuery("<td class='disable'></td>").addClass("SetHeight").appendTo("#" + meetingroomID + "_MrMorning")
                                jQuery("<td class='disable'></td>").addClass("SetHeight").appendTo("#" + meetingroomID + "_MrAfternoon")
                            }
                            else if (parseInt(settings.EndAvailableDate[0]) < j && parseInt(settings.EndAvailableDate[1]) == parseInt(settings.MonthYearDayDate[indexvalue - 1][0])) {
                                jQuery("<td class='disable'></td>").addClass("SetHeight").appendTo("#" + meetingroomID + "_MrMorning")
                                jQuery("<td class='disable'></td>").addClass("SetHeight").appendTo("#" + meetingroomID + "_MrAfternoon")
                            }
                            else {
                                jQuery(MeetingRoomDetails).find("ArrayOfViewAvailabilityOfRooms").each(function () {
                                    jQuery(this).find("ViewAvailabilityOfRooms").each(function () {
                                        if ((settings.MonthYearDayDate[indexvalue - 1][1] + "-" + (parseInt(settings.MonthYearDayDate[indexvalue - 1][0]) < 10 ? "0" + settings.MonthYearDayDate[indexvalue - 1][0] : settings.MonthYearDayDate[indexvalue - 1][0]) + (j < 10 ? "-0" + (j) + "T00:00:00" : "-" + (j) + "T00:00:00")) == jQuery(this).find("AvailabilityDate").text()) {
                                            var meetingDivM
                                            var meetingDivA
                                            meetingDivM = jQuery("<td title='" + (j) + "/" + parseInt(settings.MonthYearDayDate[indexvalue - 1][0]) + "/" + parseInt(settings.MonthYearDayDate[indexvalue - 1][1]) + "|Morning|" + AvailabilityStatus[parseInt(jQuery(this).find("MorningStatus").text())] + "|" + jQuery(this).find("Id").text() + "' ></td>").addClass("SetHeight")
                                            meetingDivM.addClass(AvailabilityStatus[parseInt(jQuery(this).find("MorningStatus").text())])
                                            meetingDivA = jQuery("<td title='" + (j) + "/" + parseInt(settings.MonthYearDayDate[indexvalue - 1][0]) + "/" + parseInt(settings.MonthYearDayDate[indexvalue - 1][1]) + "|Afternoon|" + AvailabilityStatus[parseInt(jQuery(this).find("AfternoonStatus").text())] + "|" + jQuery(this).find("Id").text() + "' ></td>").addClass("SetHeight")
                                            meetingDivA.addClass(AvailabilityStatus[parseInt(jQuery(this).find("AfternoonStatus").text())])
                                            meetingDivM.appendTo("#" + meetingroomID + "_MrMorning")
                                            meetingDivA.appendTo("#" + meetingroomID + "_MrAfternoon")
                                            if (parseInt(jQuery(this).find("MorningStatus").text()) == 0 || parseInt(jQuery(this).find("MorningStatus").text()) == 1) {
                                                meetingDivM.bind("click", function () {
                                                    var d = jQuery(this)
                                                    if (jQuery(this).hasClass("closed")) {
                                                        var titoblinebooked = jQuery(this).attr("title").replace('closed', 'opened')
                                                        jQuery.ajax({
                                                            type: "POST",
                                                            url: settings.Webservicepath + "/UpdateAvailability",
                                                            data: "MeetingRoomDetails=" + titoblinebooked + "",
                                                            success: function (msg) {
                                                                d.removeClass("closed").addClass("opened").attr("title", titoblinebooked)
                                                                var fp = jQuery("#fullproperty td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + " )")
                                                                var newTitel = fp.attr("title").replace('closed', 'opened')
                                                                jQuery.ajax({
                                                                    type: "POST",
                                                                    url: settings.Webservicepath + "/UpdateonlyFullPropertyDetails",
                                                                    data: "FullPropDetails=" + newTitel + "",
                                                                    success: function (msg) {
                                                                        fp.removeClass("closed").addClass("opened").attr("title", newTitel)
                                                                    }
                                                                })
                                                                jQuery("#Calander td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                                                jQuery("#discount td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                                                jQuery("#variation td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                                                jQuery("#leadtime td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                                                jQuery("#leadtimeEdit td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                                                jQuery("#fullproperty td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                                                if (settings.IsBedroomAvailable) {
                                                                    jQuery("#calanderBR td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                                                }
                                                            }
                                                        })
                                                    }
                                                    else {
                                                        var titclosed = jQuery(this).attr("title").replace('opened', 'closed')
                                                        jQuery.ajax({
                                                            type: "POST",
                                                            url: settings.Webservicepath + "/UpdateAvailability",
                                                            data: "MeetingRoomDetails=" + titclosed + "",
                                                            success: function (msg) {
                                                                d.removeClass('opened').addClass("closed").attr("title", titclosed)
                                                                var counttotalMrInIE = 0
                                                                for (var p = 0; p < settings.DivNames.length; p++) {
                                                                    if (settings.DivNames[p] != '[' && settings.DivNames[p] != ']' && settings.DivNames[p] != ',' && settings.DivNames[p] != undefined) {
                                                                        counttotalMrInIE++
                                                                    }
                                                                }
                                                                var mrtotaldics = counttotalMrInIE * 2
                                                                var counttotalBrInIE = 0
                                                                var counter = jQuery("#tblMR td[class*='closed']:nth-child(" + (parseInt(titclosed.split('/')[0]) + 1) + ")").length
                                                                if (settings.IsBedroomAvailable) {
                                                                    for (var p = 0; p < settings.BedRoomDivs.length; p++) {
                                                                        if (settings.BedRoomDivs[p] != '[' && settings.BedRoomDivs[p] != ']' && settings.BedRoomDivs[p] != ',' && settings.BedRoomDivs[p] != undefined) {
                                                                            counttotalBrInIE++
                                                                        }
                                                                    }
                                                                    var counterBr = jQuery("#contentbody_tblBR td[class*='closed']:nth-child(" + (parseInt(titclosed.split('/')[0]) + 1) + ")").length
                                                                    counter = counter + counterBr
                                                                }
                                                                mrtotaldics = mrtotaldics + (counttotalBrInIE * 3)
                                                                if (mrtotaldics == counter) {
                                                                    var fp2 = jQuery("#fullproperty td:nth-child(" + (parseInt(titclosed.split('/')[0]) + 1) + " )")
                                                                    var newTitel2 = fp2.attr("title").replace('opened', 'closed')
                                                                    jQuery.ajax({
                                                                        type: "POST",
                                                                        url: settings.Webservicepath + "/UpdateFullProperty",
                                                                        data: "FullPropDetails=" + newTitel2 + "",
                                                                        success: function (msg) {
                                                                            fp2.removeClass("opened").attr("title", newTitel2)
                                                                            jQuery("#tblMR tr td:nth-child(" + (parseInt(newTitel2.split('/')[0]) + 1) + ")").removeClass("opened").addClass("closed")
                                                                            if (settings.IsBedroomAvailable) {
                                                                                jQuery("#contentbody_tblBR tr td:nth-child(" + (parseInt(newTitel2.split('/')[0]) + 1) + ")").removeClass("opened").addClass("closed")
                                                                            }
                                                                        }
                                                                    })
                                                                }
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                            if (parseInt(jQuery(this).find("AfternoonStatus").text()) == 0 || parseInt(jQuery(this).find("AfternoonStatus").text()) == 1) {
                                                meetingDivA.bind("click", function () {
                                                    var d = jQuery(this)
                                                    if (jQuery(this).hasClass("closed")) {
                                                        var titoblinebooked = jQuery(this).attr("title").replace('closed', 'opened')
                                                        jQuery.ajax({
                                                            type: "POST",
                                                            url: settings.Webservicepath + "/UpdateAvailability",
                                                            data: "MeetingRoomDetails=" + titoblinebooked + "",
                                                            success: function (msg) {
                                                                d.removeClass("closed").addClass("opened").attr("title", titoblinebooked)
                                                                var fp = jQuery("#fullproperty td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + " )")
                                                                var newTitel = fp.attr("title").replace('closed', 'opened')
                                                                jQuery.ajax({
                                                                    type: "POST",
                                                                    url: settings.Webservicepath + "/UpdateonlyFullPropertyDetails",
                                                                    data: "FullPropDetails=" + newTitel + "",
                                                                    success: function (msg) {
                                                                        fp.removeClass("closed").addClass("opened").attr("title", newTitel)
                                                                    }
                                                                })
                                                                jQuery("#Calander td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                                                jQuery("#discount td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                                                jQuery("#variation td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                                                jQuery("#leadtime td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                                                jQuery("#leadtimeEdit td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                                                jQuery("#fullproperty td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                                                if (settings.IsBedroomAvailable) {
                                                                    jQuery("#calanderBR td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened")
                                                                }
                                                            }
                                                        })
                                                    }
                                                    else if (jQuery(this).hasClass("opened")) {
                                                        var titclosed = jQuery(this).attr("title").replace('opened', 'closed')
                                                        jQuery.ajax({
                                                            type: "POST",
                                                            url: settings.Webservicepath + "/UpdateAvailability",
                                                            data: "MeetingRoomDetails=" + titclosed + "",
                                                            success: function (msg) {
                                                                d.removeClass('opened').addClass("closed").attr("title", titclosed)
                                                                var counttotalMrInIE = 0
                                                                for (var p = 0; p < settings.DivNames.length; p++) {
                                                                    if (settings.DivNames[p] != '[' && settings.DivNames[p] != ']' && settings.DivNames[p] != ',' && settings.DivNames[p] != undefined) {
                                                                        counttotalMrInIE++
                                                                    }
                                                                }
                                                                var mrtotaldics = counttotalMrInIE * 2
                                                                var counttotalBrInIE = 0
                                                                var counter = jQuery("#tblMR td[class*='closed']:nth-child(" + (parseInt(titclosed.split('/')[0]) + 1) + ")").length
                                                                if (settings.IsBedroomAvailable) {
                                                                    for (var p = 0; p < settings.BedRoomDivs.length; p++) {
                                                                        if (settings.BedRoomDivs[p] != '[' && settings.BedRoomDivs[p] != ']' && settings.BedRoomDivs[p] != ',' && settings.BedRoomDivs[p] != undefined) {
                                                                            counttotalBrInIE++
                                                                        }
                                                                    }
                                                                    var counterBr = jQuery("#contentbody_tblBR td[class*='closed']:nth-child(" + (parseInt(titclosed.split('/')[0]) + 1) + ")").length
                                                                    counter = counter + counterBr
                                                                }
                                                                mrtotaldics = mrtotaldics + (counttotalBrInIE * 3)
                                                                if (mrtotaldics == counter) {
                                                                    var fp2 = jQuery("#fullproperty td:nth-child(" + (parseInt(titclosed.split('/')[0]) + 1) + " )")
                                                                    var newTitel2 = fp2.attr("title").replace('opened', 'closed')
                                                                    jQuery.ajax({
                                                                        type: "POST",
                                                                        url: settings.Webservicepath + "/UpdateFullProperty",
                                                                        data: "FullPropDetails=" + newTitel2 + "",
                                                                        success: function (msg) {
                                                                            fp2.removeClass("opened").attr("title", newTitel2)
                                                                            jQuery("#tblMR tr td:nth-child(" + (parseInt(newTitel2.split('/')[0]) + 1) + ")").removeClass("opened").addClass("closed")
                                                                            if (settings.IsBedroomAvailable) {
                                                                                jQuery("#contentbody_tblBR tr td:nth-child(" + (parseInt(newTitel2.split('/')[0]) + 1) + ")").removeClass("opened").addClass("closed")
                                                                            }
                                                                        }
                                                                    })
                                                                }
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        }
                                    })
                                })
                            }
                        }
                    }
                    jQuery("#Loding_overlay").hide()
                }
            })
        }
        return this.each(function () {

            current = settings.CurrentPanel
            if (settings.MonthYearDayDate.length > 0) {
                MoveNext(current, countall)
            }
            else {
                jQuery(this).append("No Record Found")
            }
            jQuery("#divAvailability .pager_header .page_text #left").bind("click", function (e) {
                current--
                MoveNext(current, countall)
            })
            jQuery("#divAvailability .pager_header .page_text #right").bind("click", function (e) {
                current++
                MoveNext(current, countall)
            })
            jQuery("#divAvailability .pager_header #nextlink").bind("click", function (e) {
                current++
                MoveNext(current, countall)
            })
            jQuery("#divAvailability .pager_header #prevlink").bind("click", function (e) {
                current--
                MoveNext(current, countall)
            })
            if (settings.IsBedroomAvailable == true) {
                jQuery("#divAvailabilityBedroom .pager_header .page_text #left").bind("click", function (e) {
                    current--
                    MoveNext(current, countall)
                })
                jQuery("#divAvailabilityBedroom .pager_header .page_text #right").bind("click", function (e) {
                    current++
                    MoveNext(current, countall)
                })
                jQuery("#divAvailabilityBedroom .pager_header #nextlink").bind("click", function (e) {
                    current++
                    MoveNext(current, countall)
                })
                jQuery("#divAvailabilityBedroom .pager_header #prevlink").bind("click", function (e) {
                    current--
                    MoveNext(current, countall)
                })
                if (settings.BedRoomDivs.length >= 1) {
                    jQuery("#" + settings.BedRoomDivs[0] + "_EditBedroom").hide()
                    jQuery("#" + settings.BedRoomDivs[0] + "_SaveBR").hide()
                    jQuery("#" + settings.BedRoomDivs[0] + "_CancelBR").hide()
                    jQuery("#" + settings.BedRoomDivs[0] + "_modifyBR").bind("click", function () {
                        jQuery("#" + settings.BedRoomDivs[0] + "_AvailBedroom").hide()
                        jQuery("#" + settings.BedRoomDivs[0] + "_modifyBR").hide()
                        jQuery("#" + settings.BedRoomDivs[0] + "_EditBedroom").show()
                        jQuery("#" + settings.BedRoomDivs[0] + "_SaveBR").show()
                        jQuery("#" + settings.BedRoomDivs[0] + "_CancelBR").show()
                    })
                    jQuery("#" + settings.BedRoomDivs[0] + "_CancelBR").bind("click", function () {
                        jQuery("#" + settings.BedRoomDivs[0] + "_AvailBedroom").show()
                        jQuery("#" + settings.BedRoomDivs[0] + "_modifyBR").show()
                        jQuery("#" + settings.BedRoomDivs[0] + "_EditBedroom").hide()
                        jQuery("#" + settings.BedRoomDivs[0] + "_SaveBR").hide()
                        jQuery("#" + settings.BedRoomDivs[0] + "_CancelBR").hide()
                    })
                    jQuery("#" + settings.BedRoomDivs[0] + "_SaveBR").bind("click", function () {
                        var AllValues = ""
                        var ischaracter = false
                        jQuery("#" + settings.BedRoomDivs[0] + "_EditBedroom td input").each(function () {
                            if (!isNaN(jQuery(this).val())) {
                                if (parseInt(jQuery(this).val()) >= 0) {
                                    AllValues += parseInt(jQuery(this).val()) + "|"
                                }
                                else {
                                    jQuery(this).focus()
                                    AllValues = ""
                                    ischaracter = true
                                    alert("Please enter value greater than equal to 0.")
                                    return false
                                }
                            }
                            else {
                                jQuery(this).focus()
                                AllValues = ""
                                ischaracter = true
                                alert("Please enter numaric value only.")
                                return false
                            }
                        })
                        if (AllValues.length != 0) {
                            AllValues = AllValues.substring(0, AllValues.length - 1)
                        }
                        jQuery("#contentbody_hdn_AvailabileRooms").val(AllValues)
                        jQuery("#contentbody_hdnBedroomID").val(settings.BedRoomDivs[0])
                        jQuery("#contentbody_hdn_Date").val(settings.MonthYearDayDate[current - 1][0] + "-" + settings.MonthYearDayDate[current - 1][1])
                        if (!ischaracter) {
                            jQuery("#contentbody_btnSubmit1").click()
                        }
                    })
                }
                if (settings.BedRoomDivs.length >= 2) {
                    jQuery("#" + settings.BedRoomDivs[1] + "_EditBedroom").hide()
                    jQuery("#" + settings.BedRoomDivs[1] + "_SaveBR").hide()
                    jQuery("#" + settings.BedRoomDivs[1] + "_CancelBR").hide()
                    jQuery("#" + settings.BedRoomDivs[1] + "_modifyBR").bind("click", function () {
                        jQuery("#" + settings.BedRoomDivs[1] + "_AvailBedroom").hide()
                        jQuery("#" + settings.BedRoomDivs[1] + "_modifyBR").hide()
                        jQuery("#" + settings.BedRoomDivs[1] + "_EditBedroom").show()
                        jQuery("#" + settings.BedRoomDivs[1] + "_SaveBR").show()
                        jQuery("#" + settings.BedRoomDivs[1] + "_CancelBR").show()
                    })
                    jQuery("#" + settings.BedRoomDivs[1] + "_CancelBR").bind("click", function () {
                        jQuery("#" + settings.BedRoomDivs[1] + "_AvailBedroom").show()
                        jQuery("#" + settings.BedRoomDivs[1] + "_modifyBR").show()
                        jQuery("#" + settings.BedRoomDivs[1] + "_EditBedroom").hide()
                        jQuery("#" + settings.BedRoomDivs[1] + "_SaveBR").hide()
                        jQuery("#" + settings.BedRoomDivs[1] + "_CancelBR").hide()
                    })
                    jQuery("#" + settings.BedRoomDivs[1] + "_SaveBR").bind("click", function () {
                        var AllValues = ""
                        var ischaracter = false
                        jQuery("#" + settings.BedRoomDivs[1] + "_EditBedroom td input").each(function () {
                            if (!isNaN(jQuery(this).val())) {
                                if (parseInt(jQuery(this).val()) >= 0) {
                                    AllValues += parseInt(jQuery(this).val()) + "|"
                                }
                                else {
                                    jQuery(this).focus()
                                    AllValues = ""
                                    ischaracter = true
                                    alert("Please enter value greater than equal to 0.")
                                    return false
                                }
                            }
                            else {
                                jQuery(this).focus()
                                AllValues = ""
                                ischaracter = true
                                alert("Please enter numaric value only.")
                                return false
                            }
                        })
                        if (AllValues.length != 0) {
                            AllValues = AllValues.substring(0, AllValues.length - 1)
                        }
                        jQuery("#contentbody_hdn_AvailabileRooms").val(AllValues)
                        jQuery("#contentbody_hdnBedroomID").val(settings.BedRoomDivs[1])
                        jQuery("#contentbody_hdn_Date").val(settings.MonthYearDayDate[current - 1][0] + "-" + settings.MonthYearDayDate[current - 1][1])
                        if (!ischaracter) {
                            jQuery("#contentbody_btnSubmit1").click()
                        }
                    })
                }
            }
            jQuery("#leadtimeEdit").hide()
            jQuery("#leadTimeSave").hide()
            jQuery("#leadTimeCancel").hide()
            jQuery("#modifyLeadTime").bind("click", function () {
                jQuery("#leadtime").hide()
                jQuery("#modifyLeadTime").hide()
                jQuery("#leadtimeEdit").show()
                jQuery("#leadTimeSave").show()
                jQuery("#leadTimeCancel").show()
            })
            jQuery("#leadTimeCancel").bind("click", function () {
                jQuery("#leadtime").show()
                jQuery("#modifyLeadTime").show()
                jQuery("#leadtimeEdit").hide()
                jQuery("#leadTimeSave").hide()
                jQuery("#leadTimeCancel").hide()
            })
            jQuery("#leadTimeSave").bind("click", function () {
                var AllValues = ""
                var ischaracter = false
                jQuery("#leadtimeEdit td input").each(function () {
                    if (!isNaN(jQuery(this).val())) {
                        if (parseInt(jQuery(this).val()) >= 0) {
                            AllValues += parseInt(jQuery(this).val()) + "|"
                        }
                        else {
                            jQuery(this).focus()
                            AllValues = ""
                            ischaracter = true
                            alert("Please enter value greater than equal to 0.")
                            return false
                        }
                    }
                    else {
                        jQuery(this).focus()
                        AllValues = ""
                        ischaracter = true
                        alert("Please enter numaric value only.")
                        return false
                    }
                })
                if (AllValues.length != 0) {
                    AllValues = AllValues.substring(0, AllValues.length - 1)
                }
                if (!ischaracter) {
                    jQuery("#contentbody_hdnLeadTime").val(AllValues)
                    jQuery("#contentbody_btnSaveLeed").click()
                }
            })
        })
    }
    jQuery.fn.MyPlugin.defaults = {
        HotelId: 0,
        MonthYearDayDate: [[0, 0, 0, 0]],
        StartWeek: 0,
        isEditable: true,
        DivNames: [],
        IsBedroomAvailable: true,
        BedRoomDivs: [],
        StartAvailableDates: [1, 1],
        EndAvailableDate: [31, 12],
        Webservicepath: "http://localhost/LMMR.Website/LMMRwebservice.asmx",
        CurrentPanel: 1
    }
})(jQuery)
