﻿(function (jQuery) {
    jQuery.fn.AvailabilityBedRoom = function (options) {
        var settings = jQuery.extend({}, jQuery.fn.AvailabilityBedRoom.defaults, options);

        var weeks = ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'];
        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var weekdays = parseInt(settings.StartWeek);
        var AvailabilityStatus = ['opened', 'closed', 'booked', 'sold'];
        var countall = settings.MonthYearDayDate.length;
        var current = 1;

        function MoveNextBedRoom(indexvalue, counttotal) {
            //Set the functionality for the left and right moving element in calander
            jQuery("#divAvailabilityBedroom .pager_header .page_text #left").show();
            jQuery("#divAvailabilityBedroom .pager_header .page_text #right").show();
            jQuery("#divAvailabilityBedroom .pager_header #nextlink").show();

            //if calander month is started then hide left move arrow
            if (indexvalue == 1) {
                jQuery("#divAvailabilityBedroom .pager_header .page_text #left").hide();
            }

            //if calander month is ended then hide right arrow
            if (indexvalue == counttotal) {
                jQuery("#divAvailabilityBedroom .pager_header .page_text #right").hide();
                jQuery("#divAvailabilityBedroom .pager_header #nextlink").hide();
            }

            //Change the name of the month
            if (parseInt(settings.MonthYearDayDate[indexvalue - 1][0]) > 0) {
                jQuery("#divAvailabilityBedroom .pager_header .page_text span").html("" + months[parseInt(settings.MonthYearDayDate[indexvalue - 1][0]) - 1] + " " + parseInt(settings.MonthYearDayDate[indexvalue - 1][1]) + "");
            }

            //Empty all the divs inner html.
            for (var p = 0; p < settings.DivNames.length; p++) {
                jQuery("#" + settings.DivNames[p]).html("");
                jQuery("#" + settings.DivNames[p] + "_Avail").html("");
                jQuery("#" + settings.DivNames[p] + "_Edit").html("");
            }
            jQuery("#calanderBR").html("");
            jQuery("#fullpropertyBR").html("");

            var j = 0;

            var fullCDVLP;
            //Get result from webservice according to the property and hotelid.
            weekdays = parseInt(settings.MonthYearDayDate[indexvalue - 1][3]);
            jQuery("#Loding_overlay").show();
            jQuery.ajax({
                type: "POST",
                url: settings.Webservicepath + "/GetBedRoomDetails",
                data: "HotelId=" + settings.HotelId + "&AvailabilityDate=" + settings.MonthYearDayDate[indexvalue - 1][1] + "-0" + settings.MonthYearDayDate[indexvalue - 1][0] + "&StartDate=" + settings.MonthYearDayDate[indexvalue - 1][1] + "-" + settings.StartAvailableDates[1] + "-" + settings.StartAvailableDates[0] + "&EndDate=" + settings.MonthYearDayDate[indexvalue - 1][1] + "-" + settings.EndAvailableDate[1] + "-" + settings.EndAvailableDate[0],
                success: function (response, status) {
                    //Store result recived from webserver
                    fullCDVLP = response;
                    var fullprop;
                    var calander;
                    var BedRoom = new Array();
                    var BedroomAvailability = new Array();
                    var BadRoomAvailabilityEdit = new Array();
                    //Run for Number of days

                    for (j = 0; j <= parseInt(settings.MonthYearDayDate[indexvalue - 1][2]); j++) {
                        calander = jQuery("<td></td>").appendTo("#calanderBR");
                        for (var divcount = 0; divcount < settings.DivNames.length && settings.DivNames[divcount] != null; divcount++) {
                            BedRoom[divcount] = jQuery("<td></td>").appendTo("#" + settings.DivNames[divcount]);
                            BedroomAvailability[divcount] = jQuery("<td></td>").appendTo("#" + settings.DivNames[divcount] + "_Avail");
                            BadRoomAvailabilityEdit[divcount] = jQuery("<td></td>").appendTo("#" + settings.DivNames[divcount] + "_Edit");
                        }
                        fullprop = jQuery("<td></td>").appendTo("#fullpropertyBR");

                        if (j > 0) {
                            if (parseInt(settings.StartAvailableDates[0]) > j && parseInt(settings.StartAvailableDates[1]) == parseInt(settings.MonthYearDayDate[indexvalue - 1][0])) {
                                calander.addClass("disable").append("" + weeks[weekdays] + "<br/><span>" + (j) + "</span>");
                                for (var divcount = 0; divcount < settings.DivNames.length && settings.DivNames[divcount] != null; divcount++) {
                                    BedRoom[divcount].addClass("disable SetHeight");
                                    BedroomAvailability[divcount].addClass("disable SetHeight");
                                    BadRoomAvailabilityEdit[divcount].addClass("disable SetHeight");
                                }
                                fullprop.addClass("disable SetHeight");
                            }
                            else if (parseInt(settings.EndAvailableDate[0]) < j && parseInt(settings.EndAvailableDate[1]) == parseInt(settings.MonthYearDayDate[indexvalue - 1][0])) {
                                calander.addClass("disable").append("" + weeks[weekdays] + "<br/><span>" + (j) + "</span>");
                                for (var divcount = 0; divcount < settings.DivNames.length && settings.DivNames[divcount] != null; divcount++) {
                                    BedRoom[divcount].addClass("disable SetHeight");
                                    BedroomAvailability[divcount].addClass("disable SetHeight");
                                    BadRoomAvailabilityEdit[divcount].addClass("disable SetHeight");
                                }
                                fullprop.addClass("disable SetHeight");
                            }
                            else {
                                calander.append("" + weeks[weekdays] + "<br/><span>" + (j) + "</span>");
                                for (var divcount = 0; divcount < settings.DivNames.length && settings.DivNames[divcount] != null; divcount++) {
                                    BedRoom[divcount].addClass("SetHeight");
                                    BedroomAvailability[divcount].addClass("SetHeight");
                                    BadRoomAvailabilityEdit[divcount].addClass("SetHeight");
                                }
                                fullprop.addClass("SetHeight");
                            }
                            jQuery(fullCDVLP).find("ArrayOfViewAvailabilityOfBedRoom").each(function () {
                                jQuery(this).find("ViewAvailabilityOfBedRoom").each(function () {
                                    if ((settings.MonthYearDayDate[indexvalue - 1][1] + "-" + (parseInt(settings.MonthYearDayDate[indexvalue - 1][0]) < 10 ? "0" + settings.MonthYearDayDate[indexvalue - 1][0] : settings.MonthYearDayDate[indexvalue - 1][0]) + (j < 10 ? "-0" + (j) + "T00:00:00" : "-" + (j) + "T00:00:00")) == jQuery(this).find("AvailabilityDate").text()) {
                                        fullprop.attr("title", (j) + "/" + parseInt(settings.MonthYearDayDate[indexvalue - 1][0]) + "/" + parseInt(settings.MonthYearDayDate[indexvalue - 1][1]) + "|full property|" + AvailabilityStatus[parseInt(jQuery(this).find("FullProrertyStatusBr").text())] + "|" + jQuery(this).find("AvailibilityIdForBedRoom").text()).addClass(AvailabilityStatus[parseInt(jQuery(this).find("FullProrertyStatusBr").text())]);
                                        fullprop.addClass(AvailabilityStatus[parseInt(jQuery(this).find("FullPropertyStatus").text())]);
                                        calander.addClass(AvailabilityStatus[parseInt(jQuery(this).find("FullPropertyStatus").text())]);
                                        for (var divcount = 0; divcount < settings.DivNames.length && settings.DivNames[divcount] != null; divcount++) {
                                            BedRoom[divcount].attr("title", (j) + "/" + parseInt(settings.MonthYearDayDate[indexvalue - 1][0]) + "/" + parseInt(settings.MonthYearDayDate[indexvalue - 1][1]) + "|" + jQuery(this).find("Name").text() + "|Morning|" + AvailabilityStatus[parseInt(jQuery(this).find("MorningStatus").text())] + "|" + jQuery(this).find("Id").text()).addClass("SetHeight");
                                            BedRoom[divcount].addClass(AvailabilityStatus[parseInt(jQuery(this).find("MorningStatus").text())]).bind("click", function () {
                                                var d = jQuery(this);
                                                if (jQuery(this).hasClass("closed")) {
                                                    var titoblinebooked = jQuery(this).attr("title").replace('closed', 'opened');
                                                    jQuery.ajax({
                                                        type: "POST",
                                                        url: settings.Webservicepath + "/UpdateAvailabilityBR",
                                                        data: "BedRoomDetails=" + titoblinebooked + "",
                                                        success: function (msg) {
                                                            d.removeClass("closed").addClass("opened").attr("title", titoblinebooked);
                                                            var fp = jQuery("#fullpropertyBR td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + " )");
                                                            var newTitel = fp.attr("title").replace('closed', 'opened');
                                                            jQuery.ajax({
                                                                type: "POST",
                                                                url: settings.Webservicepath + "/UpdateFullBedRoomProperty",
                                                                data: "FullBRPropDetails=" + newTitel + "",
                                                                success: function (msg) {
                                                                    fp.removeClass("closed").attr("title", newTitel);
                                                                    //jQuery("#contentbody_tblBR tr td:nth-child(" + (parseInt(newTitel.split('/')[0]) + 1) + ")").addClass("opened");
                                                                }
                                                            });

                                                            jQuery("#calanderBR td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened");
                                                            jQuery("#fullpropertyBR td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + " )").removeClass("closed").addClass("opened");
                                                        }
                                                    });
                                                }
                                                else {
                                                    var titclosed = jQuery(this).attr("title").replace('opened', 'closed');
                                                    jQuery.ajax({
                                                        type: "POST",
                                                        url: settings.Webservicepath + "/UpdateAvailabilityBR",
                                                        data: "BedRoomDetails=" + titclosed + "",
                                                        success: function (msg) {
                                                            d.removeClass('opened').addClass("closed").attr("title", titclosed);
                                                            var mrtotaldics = settings.DivNames.length;
                                                            //alert(mrtotaldics);
                                                            var counter = jQuery("#contentbody_tblBR td[class*='closed']:nth-child(" + (parseInt(titclosed.split('/')[0]) + 1) + ")").length;
                                                            alert((2 * counter - mrtotaldics));
                                                            alert(counter);
                                                            if ((mrtotaldics - 2 * counter) == counter) {
                                                                var fp2 = jQuery("#fullpropertyBR td:nth-child(" + (parseInt(titclosed.split('/')[0]) + 1) + " )");
                                                                var newTitel2 = fp2.attr("title").replace('opened', 'closed');
                                                                jQuery.ajax({
                                                                    type: "POST",
                                                                    url: settings.Webservicepath + "/UpdateFullBedRoomProperty",
                                                                    data: "FullBRPropDetails=" + newTitel2 + "",
                                                                    success: function (msg) {
                                                                        fp2.removeClass("opened").attr("title", newTitel2);
                                                                        jQuery("#contentbody_tblBR tr td:nth-child(" + (parseInt(newTitel2.split('/')[0]) + 1) + ")").addClass("closed");
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    });
                                                }
                                            });
                                            if (divcount <= 0) {
                                                if (parseInt(jQuery(this).find("RoomId").text()) == parseInt(jQuery("#contentbody_hdnBedroomID1").val())) {
                                                    BedroomAvailability[divcount].addClass(AvailabilityStatus[parseInt(jQuery(this).find("MorningStatus").text())]).html(jQuery(this).find("NumberOfRoomsAvailable").text());
                                                    BadRoomAvailabilityEdit[divcount].html("<input type='text' maxlength='2' class='myinput' value='" + jQuery(this).find("NumberOfRoomsAvailable").text() + "' />").addClass(AvailabilityStatus[parseInt(jQuery(this).find("MorningStatus").text())]);   
                                                }
                                            }
                                            else {
                                                //if (parseInt(jQuery(this).find("RoomId").text()) == parseInt(jQuery("#contentbody_hdnBedroomID2").val())) {
                                                    BedroomAvailability[divcount].addClass(AvailabilityStatus[parseInt(jQuery(this).find("MorningStatus").text())]).html(jQuery(this).find("NumberOfRoomsAvailable").text());
                                                    BadRoomAvailabilityEdit[divcount].html("<input type='text' maxlength='2' class='myinput' value='" + jQuery(this).find("NumberOfRoomsAvailable").text() + "' />").addClass(AvailabilityStatus[parseInt(jQuery(this).find("MorningStatus").text())]);
                                                //}
                                            }

                                        }
                                        fullprop.bind("click", function () {
                                            var d = jQuery(this);
                                            if (jQuery(this).hasClass("closed")) {
                                                var titoblinebooked = jQuery(this).attr("title").replace('closed', 'opened');
                                                jQuery.ajax({
                                                    type: "POST",
                                                    url: settings.Webservicepath + "/UpdateFullBedRoomProperty",
                                                    data: "FullBRPropDetails=" + titoblinebooked + "",
                                                    success: function (msg) {
                                                        d.removeClass("closed").attr("title", titoblinebooked);
                                                        jQuery("#contentbody_tblBR tr td:nth-child(" + (parseInt(titoblinebooked.split('/')[0]) + 1) + ")").removeClass("closed").addClass("opened");
                                                    }
                                                });
                                            }
                                            else {
                                                var titclosed = jQuery(this).attr("title").replace('opened', 'closed');
                                                jQuery.ajax({
                                                    type: "POST",
                                                    url: settings.Webservicepath + "/UpdateFullBedRoomProperty",
                                                    data: "FullBRPropDetails=" + titclosed + "",
                                                    success: function (msg) {
                                                        d.removeClass("opened").attr("title", titclosed);
                                                        jQuery("#contentbody_tblBR tr td:nth-child(" + (parseInt(titclosed.split('/')[0]) + 1) + ")").addClass("closed");
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            });
                            weekdays++;
                            if (weekdays >= 7) {
                                weekdays = 0;
                            }
                        }
                    }
                    jQuery("#Loding_overlay").hide();
                }
            });
        }


        return this.each(function () {

            if (settings.MonthYearDayDate.length > 0) {
                MoveNextBedRoom(current, countall);
            }
            else {
                jQuery(this).append("No Record Found");
            }
            jQuery("#divAvailabilityBedroom .pager_header .page_text #left").bind("click", function (e) {
                current--;
                MoveNextBedRoom(current, countall);
            });
            jQuery("#divAvailabilityBedroom .pager_header .page_text #right").bind("click", function (e) {
                current++;
                MoveNextBedRoom(current, countall);
            });
            jQuery("#divAvailabilityBedroom .pager_header #nextlink").bind("click", function (e) {
                current++;
                MoveNextBedRoom(current, countall);
            });

            var MyDivEdit = new Array();
            var MyDivSave = new Array();
            var MyDivCancel = new Array();
            var MyDivmodify = new Array();
            var maindivs = new Array();
            if (settings.DivNames.length >= 1) {
                jQuery("#bedroom1_Edit").hide();
                jQuery("#bedroom1_Save").hide();
                jQuery("#bedroom1_Cancel").hide();

                jQuery("#bedroom1_modify").bind("click", function () {
                    jQuery("#bedroom1_Avail").hide();
                    jQuery("#bedroom1_modify").hide();
                    jQuery("#bedroom1_Edit").show();
                    jQuery("#bedroom1_Save").show();
                    jQuery("#bedroom1_Cancel").show();
                });
                jQuery("#bedroom1_Cancel").bind("click", function () {
                    jQuery("#bedroom1_Avail").show();
                    jQuery("#bedroom1_modify").show();
                    jQuery("#bedroom1_Edit").hide();
                    jQuery("#bedroom1_Save").hide();
                    jQuery("#bedroom1_Cancel").hide();
                });
                jQuery("#bedroom1_Save").bind("click", function () {
                    var AllValues = "";
                    var ischaracter = false;
                    jQuery("#bedroom1_Edit td input").each(function () {
                        if (!isNaN(jQuery(this).val())) {
                            //AllValues += jQuery(this).val() + "|";
                            if (parseInt(jQuery(this).val()) >= 2) {
                                AllValues += jQuery(this).val() + "|";
                            }
                            else {
                                jQuery(this).focus();
                                AllValues = "";
                                ischaracter = true;
                                alert("Please enter value greater then 2.");
                                return false;
                            }
                        }
                        else {
                            jQuery(this).focus();
                            AllValues = "";
                            ischaracter = true;
                            alert("Please enter numaric value only.");
                            return false;
                        }
                    });
                    if (AllValues.length != 0) {
                        AllValues = AllValues.substring(0, AllValues.length - 1);
                    }
                    jQuery("#contentbody_hdn_Time1").val(AllValues);

                    jQuery("#contentbody_hdn_Date1").val(settings.MonthYearDayDate[current - 1][0] + "-" + settings.MonthYearDayDate[current - 1][1]);

                    if (!ischaracter) {
                        jQuery("#contentbody_btnSubmit1").click();
                    }
                });
            }
            if (settings.DivNames.length >= 2) {
                jQuery("#bedroom2_Edit").hide();
                jQuery("#bedroom2_Save").hide();
                jQuery("#bedroom2_Cancel").hide();

                jQuery("#bedroom2_modify").bind("click", function () {
                    jQuery("#bedroom2_Avail").hide();
                    jQuery("#bedroom2_modify").hide();
                    jQuery("#bedroom2_Edit").show();
                    jQuery("#bedroom2_Save").show();
                    jQuery("#bedroom2_Cancel").show();
                });
                jQuery("#bedroom2_Cancel").bind("click", function () {
                    jQuery("#bedroom2_Avail").show();
                    jQuery("#bedroom2_modify").show();
                    jQuery("#bedroom2_Edit").hide();
                    jQuery("#bedroom2_Save").hide();
                    jQuery("#bedroom2_Cancel").hide();
                });
                jQuery("#bedroom2_Save").bind("click", function () {
                    var AllValues = "";
                    var ischaracter = false;
                    jQuery("#bedroom2_Edit td input").each(function () {
                        if (!isNaN(jQuery(this).val())) {
                            //AllValues += jQuery(this).val() + "|";
                            if (parseInt(jQuery(this).val()) >= 2) {
                                AllValues += jQuery(this).val() + "|";
                            }
                            else {
                                jQuery(this).focus();
                                AllValues = "";
                                ischaracter = true;
                                alert("Please enter value greater then 2.");
                                return false;
                            }
                        }
                        else {
                            jQuery(this).focus();
                            AllValues = "";
                            ischaracter = true;
                            alert("Please enter numaric value only.");
                            return false;
                        }
                    });
                    if (AllValues.length != 0) {
                        AllValues = AllValues.substring(0, AllValues.length - 1);
                    }
                    //alert(AllValues);
                    jQuery("#contentbody_hdn_Time2").val(AllValues);

                    jQuery("#contentbody_hdn_Date2").val(settings.MonthYearDayDate[current - 1][0] + "-" + settings.MonthYearDayDate[current - 1][1]);

                    if (!ischaracter) {
                        jQuery("#contentbody_Button2").click();
                    }
                });
            }
        });
    };
    jQuery.fn.AvailabilityBedRoom.defaults = {
        HotelId: 0,
        MonthYearDayDate: [[0, 0, 0, 0]],
        StartWeek: 0,
        isEditable: true,
        DivNames: 0,
        StartAvailableDates: [1, 1],
        EndAvailableDate: [31, 12],
        Webservicepath: "http://localhost/LMMR.Website/LMMRwebservice.asmx"
    }
})(jQuery);