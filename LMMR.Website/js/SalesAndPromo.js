﻿(function (s) {
    jQuery.fn.SalesPromo = function (options) {
        var opt = jQuery.fn.extend({}, jQuery.fn.SalesPromo.defaults, options)
        var weeks = ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA']
        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
        var weekdays = 0
        var AvailabilityStatus = ['opened', 'closed', 'booked', 'sold']
        var countall = opt.MonthYearDayDate.length
        var current = 1
        function moveNextEditSection(indexvalue, counttotal) {
            jQuery("#contentbody_hdnCurrentPanel").val(indexvalue)
            jQuery("#left").show()
            jQuery("#right").show()
            jQuery("#prevlink").show()
            jQuery("#nextlink").show()
            if (indexvalue == 1) {
                jQuery("#left").hide()
                jQuery("#prevlink").hide()
            }
            if (indexvalue == counttotal) {
                jQuery("#right").hide()
                jQuery("#nextlink").hide()
            }
            if (parseInt(opt.MonthYearDayDate[indexvalue - 1][0], 10) > 0) {
                jQuery("#spnDateYear").html("" + months[parseInt(opt.MonthYearDayDate[indexvalue - 1][0], 10) - 1] + " " + parseInt(opt.MonthYearDayDate[indexvalue - 1][1], 10) + "")
            }
            jQuery("#calander").html("")
            jQuery("#packages").html("")
            jQuery("#meetingRoomPrice").html("")
            var j = 0
            weekdays = parseInt(opt.MonthYearDayDate[indexvalue - 1][3], 10)
            var calender
            var package
            var meetingroomprice
            var SPandPbyDateResponse
            for (j = 1; j <= parseInt(opt.MonthYearDayDate[indexvalue - 1][2], 10); j++) {
                calender = jQuery("<th height='45'></th>").appendTo("#calander").addClass("AddBorder")
                calender.append("<b>" + weeks[weekdays] + "</b> <br/>" + (j) + "")
                if (parseInt(opt.StartSalesandPromo[0], 10) > j && parseInt(opt.StartSalesandPromo[1], 10) == parseInt(opt.MonthYearDayDate[indexvalue - 1][0], 10)) {
                    calender.addClass("disable")
                }
                else if (parseInt(opt.EndSalesandPromo[0], 10) < j && parseInt(opt.EndSalesandPromo[1], 10) == parseInt(opt.MonthYearDayDate[indexvalue - 1][0], 10)) {
                    calender.addClass("disable")
                }
                weekdays++
                if (weekdays >= 7) {
                    weekdays = 0
                } 
            }
            jQuery("#Loding_overlay").show()
            jQuery.ajax({
                type: "POST",
                url: opt.Webservicepath + "/GetSpecialPriceandPromoByDate",
                data: "HotelId=" + opt.HotelId + "&AvailabilityDate=" + opt.MonthYearDayDate[indexvalue - 1][1] + (parseInt(opt.MonthYearDayDate[indexvalue - 1][0], 10) < 10 ? "-0" + opt.MonthYearDayDate[indexvalue - 1][0] : "-" + opt.MonthYearDayDate[indexvalue - 1][0]) + "&StartDate=" + opt.MonthYearDayDate[0][1] + "-" + opt.StartSalesandPromo[1] + "-" + opt.StartSalesandPromo[0] + "&EndDate=" + opt.MonthYearDayDate[opt.MonthYearDayDate.length - 1][1] + "-" + opt.EndSalesandPromo[1] + "-" + opt.EndSalesandPromo[0],
                success: function (response, status) {
                    SPandPbyDateResponse = response
                    for (j = 1; j <= parseInt(opt.MonthYearDayDate[indexvalue - 1][2], 10); j++) {
                        package = jQuery("<td align='center' height='30'></td>").appendTo("#packages")
                        meetingroomprice = jQuery("<td align='center' height='30'></td>").appendTo("#meetingRoomPrice")
                        if (parseInt(opt.StartSalesandPromo[0], 10) > j && parseInt(opt.StartSalesandPromo[1], 10) == parseInt(opt.MonthYearDayDate[indexvalue - 1][0], 10)) {
                            package.addClass("disable")
                            meetingroomprice.addClass("disable")
                        }
                        else if (parseInt(opt.EndSalesandPromo[0], 10) < j && parseInt(opt.EndSalesandPromo[1], 10) == parseInt(opt.MonthYearDayDate[indexvalue - 1][0], 10)) {
                            package.addClass("disable")
                            meetingroomprice.addClass("disable")
                        }
                        else {
                            jQuery(SPandPbyDateResponse).find("ArrayOfSpecialPriceAndPromo").each(function () {
                                jQuery(this).find("SpecialPriceAndPromo").each(function () {
                                    if ((opt.MonthYearDayDate[indexvalue - 1][1] + "-" + (parseInt(opt.MonthYearDayDate[indexvalue - 1][0], 10) < 10 ? "0" + opt.MonthYearDayDate[indexvalue - 1][0] : opt.MonthYearDayDate[indexvalue - 1][0]) + (j < 10 ? "-0" + (j) + "T00:00:00" : "-" + (j) + "T00:00:00")) == jQuery(this).find("SpecialPriceDate").text()) {
                                        package.html("<input type='text'  style='text-align: center;width:20px;'  value='" + parseInt(jQuery(this).find("DdrPercent").text(), 10) + "' />")
                                        meetingroomprice.html("<input type='text' style='text-align: center;width:20px;' value='" + parseInt(jQuery(this).find("MeetingRoomPercent").text(), 10) + "' />")
                                        if (parseFloat(jQuery(this).find("DdrPercent").text()) < 0.00) {
                                            package.addClass("closed")
                                        }
                                        if (parseFloat(jQuery(this).find("MeetingRoomPercent").text()) < 0.00) {
                                            meetingroomprice.addClass("closed")
                                        } 
                                    }
                                })
                            })
                        } 
                    }
                    jQuery("#packages td input:text").change(function () { changeTextVal(jQuery(this).index("#packages td input:text")); })
                    jQuery("#Loding_overlay").hide()
                }
            })
            var p = 0
            for (p = 0; p < opt.PackageDivNames.length; p++) {
                changePackages(indexvalue, counttotal, opt.PackageDivNames[p])
            }
            var m = 0
            for (m = 0; m < opt.MeetingRoomDivNames.length; m++) {
                changeMeetingRoom(indexvalue, counttotal, opt.MeetingRoomDivNames[m])
            }
            var i = 0
            for (i = 0; i < opt.BedroomDivNames.length; i++) {
                ChangeBedroom(indexvalue, counttotal, opt.BedroomDivNames[i])
            }
            changeAvailability(indexvalue, counttotal)
        }
        function changeAvailability(indexvalue, counttotal) {
            jQuery("#divAvailability").html("")
            jQuery("#Loding_overlay").show()
            var availabilityResponse
            jQuery.ajax({
                type: "POST",
                url: opt.Webservicepath + "/GetSpecialPriceandPromoAvailability",
                data: "HotelId=" + opt.HotelId + "&AvailabilityDate=" + opt.MonthYearDayDate[indexvalue - 1][1] + (parseInt(opt.MonthYearDayDate[indexvalue - 1][0], 10) < 10 ? "-0" + opt.MonthYearDayDate[indexvalue - 1][0] : "-" + opt.MonthYearDayDate[indexvalue - 1][0]) + "&StartDate=" + opt.MonthYearDayDate[0][1] + "-" + opt.StartSalesandPromo[1] + "-" + opt.StartSalesandPromo[0] + "&EndDate=" + opt.MonthYearDayDate[opt.MonthYearDayDate.length - 1][1] + "-" + opt.EndSalesandPromo[1] + "-" + opt.EndSalesandPromo[0],
                success: function (response, status) {
                    availabilityResponse = response
                    for (j = 1; j <= parseInt(opt.MonthYearDayDate[indexvalue - 1][2], 10); j++) {
                        if (parseInt(opt.StartSalesandPromo[0], 10) > j && parseInt(opt.StartSalesandPromo[1], 10) == parseInt(opt.MonthYearDayDate[indexvalue - 1][0], 10)) {
                            jQuery("<td align='center' height='30'></td>").appendTo("#divAvailability").addClass("disable")
                        }
                        else if (parseInt(opt.EndSalesandPromo[0], 10) < j && parseInt(opt.EndSalesandPromo[1], 10) == parseInt(opt.MonthYearDayDate[indexvalue - 1][0], 10)) {
                            jQuery("<td  align='center' height='30'></td>").appendTo("#divAvailability").addClass("disable")
                        }
                        else {
                            jQuery(availabilityResponse).find("ArrayOfAvailabilitySPandP").each(function () {
                                jQuery(this).find("AvailabilitySPandP").each(function () {
                                    if ((opt.MonthYearDayDate[indexvalue - 1][1] + "-" + (parseInt(opt.MonthYearDayDate[indexvalue - 1][0], 10) < 10 ? "0" + opt.MonthYearDayDate[indexvalue - 1][0] : opt.MonthYearDayDate[indexvalue - 1][0]) + (j < 10 ? "-0" + (j) + "T00:00:00" : "-" + (j) + "T00:00:00")) == jQuery(this).find("AvailDate").text()) {
                                        if (parseFloat(jQuery(this).find("AvailMR").text()) <= 0) {
                                            jQuery("<td  align='center' height='30'></td>").appendTo("#divAvailability").html(jQuery(this).find("AvailMR").text()).addClass("closed")
                                        }
                                        else {
                                            jQuery("<td align='center' height='30'></td>").appendTo("#divAvailability").html(jQuery(this).find("AvailMR").text())
                                        } 
                                    }
                                })
                            })
                        } 
                    }
                    jQuery("#Loding_overlay").hide()
                }
            })
        }
        function changePackages(indexvalue, counttotal, packageID) {
            jQuery("#" + packageID + "_PHalfday").html("")
            jQuery("#" + packageID + "_PFullday").html("")
            jQuery("#Loding_overlay").show()
            jQuery.ajax({
                type: "POST",
                url: opt.Webservicepath + "/GetSpecialPriceandPromoByPackage",
                data: "HotelId=" + opt.HotelId + "&AvailabilityDate=" + opt.MonthYearDayDate[indexvalue - 1][1] + (parseInt(opt.MonthYearDayDate[indexvalue - 1][0], 10) < 10 ? "-0" + opt.MonthYearDayDate[indexvalue - 1][0] : "-" + opt.MonthYearDayDate[indexvalue - 1][0]) + "&StartDate=" + opt.MonthYearDayDate[0][1] + "-" + opt.StartSalesandPromo[1] + "-" + opt.StartSalesandPromo[0] + "&EndDate=" + opt.MonthYearDayDate[opt.MonthYearDayDate.length - 1][1] + "-" + opt.EndSalesandPromo[1] + "-" + opt.EndSalesandPromo[0] + "&PId=" + packageID,
                success: function (response, status) {
                    SPandPbyDateResponse = response
                    jQuery("#" + packageID + "_PHalfday").html("")
                    jQuery("#" + packageID + "_PFullday").html("")
                    for (j = 1; j <= parseInt(opt.MonthYearDayDate[indexvalue - 1][2], 10); j++) {
                        if (parseInt(opt.StartSalesandPromo[0], 10) > j && parseInt(opt.StartSalesandPromo[1], 10) == parseInt(opt.MonthYearDayDate[indexvalue - 1][0], 10)) {
                            jQuery("<td align='center' width='20' height='30'></td>").appendTo("#" + packageID + "_PHalfday").addClass("disable")
                            jQuery("<td align='center' width='20' height='30'></td>").appendTo("#" + packageID + "_PFullday").addClass("disable")
                        }
                        else if (parseInt(opt.EndSalesandPromo[0], 10) < j && parseInt(opt.EndSalesandPromo[1], 10) == parseInt(opt.MonthYearDayDate[indexvalue - 1][0], 10)) {
                            jQuery("<td align='center' width='20' height='30'></td>").appendTo("#" + packageID + "_PHalfday").addClass("disable")
                            jQuery("<td align='center' width='20' height='30'></td>").appendTo("#" + packageID + "_PFullday").addClass("disable")
                        }
                        else {
                            jQuery(SPandPbyDateResponse).find("ArrayOfViewSpandPpercentageOfPackage").each(function () {
                                jQuery(this).find("ViewSpandPpercentageOfPackage").each(function () {
                                    if ((opt.MonthYearDayDate[indexvalue - 1][1] + "-" + (parseInt(opt.MonthYearDayDate[indexvalue - 1][0], 10) < 10 ? "0" + opt.MonthYearDayDate[indexvalue - 1][0] : opt.MonthYearDayDate[indexvalue - 1][0]) + (j < 10 ? "-0" + (j) + "T00:00:00" : "-" + (j) + "T00:00:00")) == jQuery(this).find("SpecialPriceDate").text()) {
                                        var actualprice = jQuery(this).find("ActualPrice").text()
                                        var actualpricedescount = (parseFloat(jQuery(this).find("OtherItemTotal").text()) + parseFloat(jQuery(this).find("FullDayPrice").text()) + parseFloat(jQuery(this).find("FullDayPrice").text()) * (parseFloat(jQuery(this).find("DdrPercent").text()) / 100))
                                        if (parseFloat(jQuery(this).find("DdrPercent").text()) < 0.00) {
                                            jQuery("<td align='center' height='30' width='20' title='" + roundNumber(parseFloat(actualprice), 2) + "'></td>").appendTo("#" + packageID + "_PHalfday").html(roundNumber(parseFloat(actualprice), 2).toString().substr(0, 3)).addClass("closed")
                                            jQuery("<td align='center' height='30' width='20'  title='" + roundNumber(parseFloat(actualpricedescount), 2) + "'></td>").appendTo("#" + packageID + "_PFullday").html(roundNumber(actualpricedescount, 2).toString().substr(0, 3)).addClass("closed")
                                        }
                                        else {
                                            jQuery("<td align='center' height='30' width='20'  title='" + parseInt(actualprice, 10) + "'></td>").appendTo("#" + packageID + "_PHalfday").html(parseInt(actualprice, 10).toString().substr(0, 3))
                                            jQuery("<td align='center' height='30' width='20'  title='" + actualpricedescount + "'></td>").appendTo("#" + packageID + "_PFullday").html(actualpricedescount.toString().substr(0, 3))
                                        } 
                                    }
                                })
                            })
                        } 
                    }
                    jQuery("#Loding_overlay").hide()
                }
            })
        }
        function ChangeBedroom(indexvalue, counttotal, bedRoomID) {
            jQuery("#" + bedRoomID + "_Single").html("")
            jQuery("#" + bedRoomID + "_Double").html("")
            jQuery("#Loding_overlay").show()
            jQuery.ajax({
                type: "POST",
                url: opt.Webservicepath + "/GetSpecialPriceandPromoByBedRoom",
                data: "HotelId=" + opt.HotelId + "&AvailabilityDate=" + opt.MonthYearDayDate[indexvalue - 1][1] + (parseInt(opt.MonthYearDayDate[indexvalue - 1][0], 10) < 10 ? "-0" + opt.MonthYearDayDate[indexvalue - 1][0] : "-" + opt.MonthYearDayDate[indexvalue - 1][0]) + "&StartDate=" + opt.MonthYearDayDate[0][1] + "-" + opt.StartSalesandPromo[1] + "-" + opt.StartSalesandPromo[0] + "&EndDate=" + opt.MonthYearDayDate[opt.MonthYearDayDate.length - 1][1] + "-" + opt.EndSalesandPromo[1] + "-" + opt.EndSalesandPromo[0] + "&BRId=" + bedRoomID,
                success: function (response, status) {
                    for (j = 1; j <= parseInt(opt.MonthYearDayDate[indexvalue - 1][2], 10); j++) {
                        if (parseInt(opt.StartSalesandPromo[0], 10) > j && parseInt(opt.StartSalesandPromo[1], 10) == parseInt(opt.MonthYearDayDate[indexvalue - 1][0], 10)) {
                            jQuery("<td align='center' height='30'></td>").appendTo("#" + bedRoomID + "_Single").addClass("disable")
                            jQuery("<td align='center' height='30'></td>").appendTo("#" + bedRoomID + "_Double").addClass("disable")
                        }
                        else if (parseInt(opt.EndSalesandPromo[0], 10) < j && parseInt(opt.EndSalesandPromo[1], 10) == parseInt(opt.MonthYearDayDate[indexvalue - 1][0], 10)) {
                            jQuery("<td align='center' height='30'></td>").appendTo("#" + bedRoomID + "_Single").addClass("disable")
                            jQuery("<td align='center' height='30'></td>").appendTo("#" + bedRoomID + "_Double").addClass("disable")
                        }
                        else {
                            jQuery(response).find("ArrayOfSpecialPriceForBedroom").each(function () {
                                jQuery(this).find("SpecialPriceForBedroom").each(function () {
                                    if ((opt.MonthYearDayDate[indexvalue - 1][1] + "-" + (parseInt(opt.MonthYearDayDate[indexvalue - 1][0], 10) < 10 ? "0" + opt.MonthYearDayDate[indexvalue - 1][0] : opt.MonthYearDayDate[indexvalue - 1][0]) + (j < 10 ? "-0" + (j) + "T00:00:00" : "-" + (j) + "T00:00:00")) == jQuery(this).find("PriceDate").text()) {
                                        jQuery("<td align='center' height='30'></td>").appendTo("#" + bedRoomID + "_Single").html("<input type='text' style='text-align: center;width:20px;'  value='" + parseFloat(Math.abs(jQuery(this).find("PriceOfTheDaySingle").text())) + "'  title='" + parseFloat(Math.abs(jQuery(this).find("PriceOfTheDaySingle").text())) + "' />")
                                        jQuery("<td align='center' height='30'></td>").appendTo("#" + bedRoomID + "_Double").html("<input type='text' style='text-align: center;width:20px;'  value='" + parseFloat(Math.abs(jQuery(this).find("PriceOfTheDayDouble").text())) + "'  title='" + parseFloat(Math.abs(jQuery(this).find("PriceOfTheDayDouble").text())) + "'/>")
                                    }
                                })
                            })
                        } 
                    }
                    jQuery("#Loding_overlay").hide()
                }
            })
        }
        function changeMeetingRoom(indexvalue, counttotal, meetingRoomID) {
            jQuery("#" + meetingRoomID + "_MRHalfday").html("")
            jQuery("#" + meetingRoomID + "_MRFullday").html("")
            jQuery("#Loding_overlay").show()
            jQuery.ajax({
                type: "POST",
                url: opt.Webservicepath + "/GetSpecialPriceandPromoByMeetingRoom",
                data: "HotelId=" + opt.HotelId + "&AvailabilityDate=" + opt.MonthYearDayDate[indexvalue - 1][1] + (parseInt(opt.MonthYearDayDate[indexvalue - 1][0], 10) < 10 ? "-0" + opt.MonthYearDayDate[indexvalue - 1][0] : "-" + opt.MonthYearDayDate[indexvalue - 1][0]) + "&StartDate=" + opt.MonthYearDayDate[0][1] + "-" + opt.StartSalesandPromo[1] + "-" + opt.StartSalesandPromo[0] + "&EndDate=" + opt.MonthYearDayDate[opt.MonthYearDayDate.length - 1][1] + "-" + opt.EndSalesandPromo[1] + "-" + opt.EndSalesandPromo[0] + "&MRId=" + meetingRoomID,
                success: function (response, status) {
                    for (j = 1; j <= parseInt(opt.MonthYearDayDate[indexvalue - 1][2], 10); j++) {
                        if (parseInt(opt.StartSalesandPromo[0], 10) > j && parseInt(opt.StartSalesandPromo[1], 10) == parseInt(opt.MonthYearDayDate[indexvalue - 1][0], 10)) {
                            jQuery("<td align='center' height='30'></td>").appendTo("#" + meetingRoomID + "_MRHalfday").addClass("disable")
                            jQuery("<td align='center' height='30'></td>").appendTo("#" + meetingRoomID + "_MRFullday").addClass("disable")
                        }
                        else if (parseInt(opt.EndSalesandPromo[0], 10) < j && parseInt(opt.EndSalesandPromo[1], 10) == parseInt(opt.MonthYearDayDate[indexvalue - 1][0], 10)) {
                            jQuery("<td align='center' height='30'></td>").appendTo("#" + meetingRoomID + "_MRHalfday").addClass("disable")
                            jQuery("<td align='center' height='30'></td>").appendTo("#" + meetingRoomID + "_MRFullday").addClass("disable")
                        }
                        else {
                            jQuery(response).find("ArrayOfViewSpandPpercentageOfMeetingRoom").each(function () {
                                jQuery(this).find("ViewSpandPpercentageOfMeetingRoom").each(function () {
                                    if ((opt.MonthYearDayDate[indexvalue - 1][1] + "-" + (parseInt(opt.MonthYearDayDate[indexvalue - 1][0], 10) < 10 ? "0" + opt.MonthYearDayDate[indexvalue - 1][0] : opt.MonthYearDayDate[indexvalue - 1][0]) + (j < 10 ? "-0" + (j) + "T00:00:00" : "-" + (j) + "T00:00:00")) == jQuery(this).find("SpecialPriceDate").text()) {
                                        if (parseFloat(jQuery(this).find("MeetingRoomPercent").text()) < 0.00) {
                                            jQuery("<td align='center' height='30'></td>").appendTo("#" + meetingRoomID + "_MRHalfday").html(parseInt(Math.abs(jQuery(this).find("MrHalfDayPricePercent").text()), 10)).addClass("closed")
                                            jQuery("<td align='center'  height='30'></td>").appendTo("#" + meetingRoomID + "_MRFullday").html(parseInt(Math.abs(jQuery(this).find("MrFullDayPricePercent").text()), 10)).addClass("closed")
                                        }
                                        else {
                                            jQuery("<td align='center'  height='30'></td>").appendTo("#" + meetingRoomID + "_MRHalfday").html(parseInt(Math.abs(jQuery(this).find("MrHalfDayPricePercent").text()), 10))
                                            jQuery("<td align='center'  height='30'></td>").appendTo("#" + meetingRoomID + "_MRFullday").html(parseInt(Math.abs(jQuery(this).find("MrFullDayPricePercent").text()), 10))
                                        } 
                                    }
                                })
                            })
                        } 
                    }
                    jQuery("#Loding_overlay").hide()
                }
            })
        }
        function changeTextVal(myval) {
            var ret = jQuery("#packages td input:text:eq(" + myval + ")").val()
            jQuery("#meetingRoomPrice td input:text:eq(" + myval + ")").val(ret)
        }
        return this.each(function () {
            current = opt.CurrentPanel
            if (opt.MonthYearDayDate.length > 0) {
                moveNextEditSection(current, countall)
            }
            else {
                jQuery(this).append("No Record Found")
            }
            jQuery("#left").bind("click", function (e) {
                current--
                moveNextEditSection(current, countall)
            })
            jQuery("#right").bind("click", function (e) {
                current++
                moveNextEditSection(current, countall)
            })
            jQuery("#prevlink").bind("click", function () {
                current--
                moveNextEditSection(current, countall)
            })
            jQuery("#nextlink").bind("click", function () {
                current++
                moveNextEditSection(current, countall)
            })
            jQuery("#contentbody_lnkSave").bind("click", function () {
                var AllDiscountMR = ""
                var AllDiscountPK = ""
                var ischaracter = false
                jQuery("#packages td input").each(function () {
                    if (!isNaN(jQuery(this).val())) {
                        if (parseInt(jQuery(this).val(), 10) <= parseInt(jQuery("#contentbody_hdnMaxPercentage").val(), 10) && parseInt(jQuery(this).val(), 10) >= parseInt(jQuery("#contentbody_hdnMinPercentage").val(), 10)) {
                            AllDiscountPK += parseInt(jQuery(this).val(), 10) + "|"
                        }
                        else {
                            jQuery(this).focus()
                            AllDiscountPK = ""
                            ischaracter = true
                            alert("Please enter value between " + parseInt(jQuery("#contentbody_hdnMinPercentage").val(), 10) + " and " + parseInt(jQuery("#contentbody_hdnMaxPercentage").val(), 10) + ".")
                            return false
                        } 
                    }
                    else {
                        jQuery(this).focus()
                        AllDiscountPK = ""
                        ischaracter = true
                        alert("Please enter numeric value only.")
                        return false
                    }
                })
                if (AllDiscountPK.length != 0) {
                    AllDiscountPK = AllDiscountPK.substring(0, AllDiscountPK.length - 1)
                }
                else {
                    return false
                }
                jQuery("#meetingRoomPrice td input").each(function () {
                    if (!isNaN(jQuery(this).val())) {
                        if (parseFloat(parseInt(jQuery(this).val(), 10)) < parseFloat(jQuery(this).val())) {
                            jQuery(this).focus()
                            AllDiscountMR = ""
                            ischaracter = true
                            alert("Please enter integer numaric value only.")
                            return false
                        }
                        if (parseInt(jQuery(this).val(), 10) <= parseInt(jQuery("#contentbody_hdnMaxPercentage").val(), 10) && parseInt(jQuery(this).val(), 10) >= parseInt(jQuery("#contentbody_hdnMinPercentage").val(), 10)) {
                            AllDiscountMR += parseInt(jQuery(this).val(), 10) + "|"
                        }
                        else {
                            jQuery(this).focus()
                            AllDiscountMR = ""
                            ischaracter = true
                            alert("Please enter value between " + parseInt(jQuery("#contentbody_hdnMinPercentage").val(), 10) + " and " + parseInt(jQuery("#contentbody_hdnMaxPercentage").val(), 10) + ".")
                            return false
                        } 
                    }
                    else {
                        jQuery(this).focus()
                        AllDiscountMR = ""
                        ischaracter = true
                        alert("Please enter integer numaric value only.")
                        return false
                    }
                })
                if (AllDiscountMR.length != 0) {
                    AllDiscountMR = AllDiscountMR.substring(0, AllDiscountMR.length - 1)
                }
                else {
                    return false
                }
                if (!ischaracter) {
                    jQuery("#contentbody_hdnPackageDiscount").val(AllDiscountPK)
                    jQuery("#contentbody_hdnMeetingRoomDiscount").val(AllDiscountMR)
                    jQuery("#contentbody_hdnStartingDate").val(opt.MonthYearDayDate[current - 1][0] + "-" + opt.MonthYearDayDate[current - 1][1])
                }
                else {
                    return false
                }
                jQuery("#Loding_overlaySec").show()
                return true
            })
            jQuery("#contentbody_lnkSaveBedroom").bind("click", function () {
                var ischaracter = false
                var bedroom1id = jQuery("#contentbody_hdnBedroom1ID").val()
                if (bedroom1id.length > 0 && bedroom1id != "" && bedroom1id != "0") {
                    var bedroom1single = ""
                    var bedroom1double = ""
                    jQuery("#" + bedroom1id + "_Single td input").each(function () {
                        if (!isNaN(jQuery(this).val())) {
                            if (parseFloat(jQuery(this).val()) >= 0 ) {
                                bedroom1single += parseFloat(jQuery(this).val()) + "|"
                            }
                            else {
                                jQuery(this).focus()
                                bedroom1single = ""
                                ischaracter = true
                                alert("Please enter value greater than equal to 0.")
                                return false
                            } 
                        }
                        else {
                            jQuery(this).focus()
                            bedroom1single = ""
                            ischaracter = true
                            alert("Please enter numaric value only.")
                            return false
                        }
                    })
                    if (bedroom1single.length != 0) {
                        bedroom1single = bedroom1single.substring(0, bedroom1single.length - 1)
                    }
                    else {
                        return false
                    }
                    jQuery("#" + bedroom1id + "_Double td input").each(function () {
                        if (!isNaN(jQuery(this).val())) {
                            if (parseFloat(jQuery(this).val()) >= 0) {
                                bedroom1double += parseFloat(jQuery(this).val()) + "|"
                            }
                            else {
                                jQuery(this).focus()
                                bedroom1double = ""
                                ischaracter = true
                                alert("Please enter value greater than equal to 0.")
                                return false
                            } 
                        }
                        else {
                            jQuery(this).focus()
                            bedroom1double = ""
                            ischaracter = true
                            alert("Please enter numaric value only.")
                            return false
                        }
                    })
                    if (bedroom1double.length != 0) {
                        bedroom1double = bedroom1double.substring(0, bedroom1double.length - 1)
                    }
                    else {
                        return false
                    }
                    if (!ischaracter) {
                        jQuery("#contentbody_hdnBedroom1single").val(bedroom1single)
                        jQuery("#contentbody_hdnBedroom1double").val(bedroom1double)
                        jQuery("#contentbody_hdnStartingDate").val(opt.MonthYearDayDate[current - 1][0] + "-" + opt.MonthYearDayDate[current - 1][1])
                    }
                    else {
                        return false
                    } 
                }
                var bedroom2id = jQuery("#contentbody_hdnBedroom2ID").val()
                if (bedroom2id.length > 0 && bedroom1id != "" && bedroom1id != "0") {
                    var bedroom2single = ""
                    var bedroom2double = ""
                    jQuery("#" + bedroom2id + "_Single td input").each(function () {
                        if (!isNaN(jQuery(this).val())) {
                            if (parseFloat(jQuery(this).val()) >= 0 ) {
                                bedroom2single += parseFloat(jQuery(this).val()) + "|"
                            }
                            else {
                                jQuery(this).focus()
                                bedroom2single = ""
                                ischaracter = true
                                alert("Please enter value greater than equal to 0.")
                                return false
                            } 
                        }
                        else {
                            jQuery(this).focus()
                            bedroom2single = ""
                            ischaracter = true
                            alert("Please enter numaric value only.")
                            return false
                        }
                    })
                    if (bedroom2single.length != 0) {
                        bedroom2single = bedroom2single.substring(0, bedroom2single.length - 1)
                    }
                    else {
                        return false
                    }
                    jQuery("#" + bedroom2id + "_Double td input").each(function () {
                        if (!isNaN(jQuery(this).val())) {
                            if (parseFloat(jQuery(this).val()) >= 0 ) {
                                bedroom2double += parseFloat(jQuery(this).val()) + "|"
                            }
                            else {
                                jQuery(this).focus()
                                bedroom2double = ""
                                ischaracter = true
                                alert("Please enter value greater than equal to 0.")
                                return false
                            } 
                        }
                        else {
                            jQuery(this).focus()
                            bedroom2double = ""
                            ischaracter = true
                            alert("Please enter numaric value only.")
                            return false
                        }
                    })
                    if (bedroom2double.length != 0) {
                        bedroom2double = bedroom2double.substring(0, bedroom2double.length - 1)
                    }
                    else {
                        return false
                    }
                    if (!ischaracter) {
                        jQuery("#contentbody_hdnBedroom2single").val(bedroom2single)
                        jQuery("#contentbody_hdnBedroom2double").val(bedroom2double)
                        jQuery("#contentbody_hdnStartingDate").val(opt.MonthYearDayDate[current - 1][0] + "-" + opt.MonthYearDayDate[current - 1][1])
                    }
                    else {
                        return false
                    } 
                }
                jQuery("#Loding_overlaySec").show()
                return true
            })
        })
    }
    jQuery.fn.SalesPromo.defaults = {
        HotelId: 0,
        MonthYearDayDate: [[0, 0, 0, 0]],
        PackageDivNames: [],
        MeetingRoomDivNames: [],
        BedroomDivNames: [],
        StartSalesandPromo: [1, 1],
        EndSalesandPromo: [31, 12],
        CurrentPanel: 1
    }
})(jQuery)
function roundNumber(num, dec) {
    var result = Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec)
    return result
}
