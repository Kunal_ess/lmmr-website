﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeMaster.master" AutoEventWireup="true"
    CodeFile="RequestStep3.aspx.cs" Inherits="RequestStep3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntLeft" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMainbody" runat="Server">
    <!--today-date START HERE-->
    <div class="today-date">
        <div class="today-date-left">
            <b><%= GetKeyResult("TODAY")%>:</b>
            <asp:Label ID="lblDateLogin" runat="server"></asp:Label>
        </div>
        <div class="today-date-right">
            <%= GetKeyResult("YOUARELOGGEDINAS")%>: <b>
                <asp:Label ID="lblUserName" runat="server"></asp:Label></b>
        </div>
    </div>
    <!--today-date ENDS HERE-->
    <!--request-step-mainbody START HERE-->
    <div class="request-step-mainbody">
        <!--request-stepbody START HERE-->
        <div class="request-step3-stepbody">
            <div class="request-step3-step1">
                <span><%= GetKeyResult("STEP1")%>. </span><%= GetKeyResult("BASKETREQUEST")%>
            </div>
            <div class="request-step3-step2">
                <span><%= GetKeyResult("STEP2")%>.</span> <%= GetKeyResult("RECAP")%>
            </div>
            <div class="request-step3-step3">
                <span><%= GetKeyResult("STEP3")%>.</span> <%= GetKeyResult("RECAP")%>
            </div>
            <div class="request-step3-step4">
                <span><%= GetKeyResult("STEP4")%>.</span> <%= GetKeyResult("SENDING")%>
            </div>
        </div>
        <!--request-stepbody ENDS HERE-->
        <!--request-heading1 START HERE-->
        <div class="request-heading1">
            <h1>
                <span><%= GetKeyResult("YOURBASKETREQUEST")%> </span><%= GetKeyResult("EXTRAINFO")%></h1>
        </div>
        <!--request-heading1 ENDS HERE-->
        <!--request-date-body START HERE-->
        <div class="request-date-body">
            <div class="request-arrival-date">
                <span><%= GetKeyResult("ARRIVALDATE")%>:</span>
                <asp:Label ID="lblArrival" runat="server"></asp:Label>
            </div>
            <div class="request-departure-date">
                <span><%= GetKeyResult("DEPARTUREDATE")%>:</span>
                <asp:Label ID="lblDeparture" runat="server"></asp:Label>
            </div>
            <div class="request-duration-day">
                <span><%= GetKeyResult("DURATION")%>:</span>
                <asp:Label ID="lblDuration" runat="server"></asp:Label>
            </div>
            <div class="request-participants1">
                <%--<span>Participants:</span> 45--%>
            </div>
        </div>
        <!--request-date-body ENDS HERE-->
        <!--Hotels / Facility body START HERE-->
        <div class="hotels-facility-step3-body">
            <h1>
                <%= GetKeyResult("VENUES")%></h1>
            <div class="hotels-facility-step3-body-haeding">
                <div class="hotels-facility-step3-body-haeding1">
                    <%= GetKeyResult("MEETINGROOMS")%>
                </div>
                <div class="hotels-facility-step3-body-haeding2">
                    <%= GetKeyResult("DESCRIPTION")%>
                </div>
                <div class="hotels-facility-step3-body-haeding3">
                   <%= GetKeyResult("QTY")%>
                </div>
                <div class="hotels-facility-step3-body-haeding4">
                    <%= GetKeyResult("CHOOSECATEGORY")%>
                </div>
            </div>
            <ul>
                <asp:Repeater ID="rptHotel" runat="server" OnItemDataBound="rptHotel_ItemDataBound">
                    <ItemTemplate>
                        <li>
                            <h3 class="first">
                                <span>
                                    <asp:Label ID="lblIndex" runat="server"></asp:Label>.</span>
                                <asp:Label ID="lblHotelName" runat="server"></asp:Label></h3>
                            <div class="hotels-facility-step3-inner">
                                <ul>
                                    <asp:Repeater ID="rptMeetingroom" runat="server" OnItemDataBound="rptMeetingroom_ItemDataBound">
                                        <ItemTemplate>
                                            <li>
                                                <div class="hotels-facility-step3-inner-box">
                                                    <div class="hotels-facility-step3-inner-box1">
                                                        <span>
                                                            <asp:Label ID="lblIndex" runat="server"></asp:Label>.</span>
                                                        <asp:Label ID="lblMeetingRoomName" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="hotels-facility-step3-inner-box2">
                                                        <p>
                                                            <asp:Label ID="lblConfigurationType" runat="server"></asp:Label></p>
                                                        <p>
                                                            <asp:Label ID="lblMaxandMinCapacity" runat="server"></asp:Label></p>
                                                    </div>
                                                    <div class="hotels-facility-step3-inner-box3">
                                                        <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="hotels-facility-step3-inner-box4">
                                                        <asp:Label ID="lblIsMain" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
        <!--Hotels / Facility body ENDS HERE-->
        <!--request-step3-body START HERE-->
        <div class="request-step3-body">
            <ul>
                <asp:Panel ID="pnlPackage" runat="server">
                    <li>
                        <div class="request-step3-body-inner" style="margin-top:30px;">
                            <h1>
                                 <span><%= GetKeyResult("PACKAGEREQUESTED")%></span></h1>
                            <div class="request-step2-body-inner1-haeding">
                                <div class="request-step2-body-inner1-haeding1">
                                    &nbsp;
                                </div>
                                <div class="request-step2-body-inner1-haeding2">
                                     <%= GetKeyResult("DESCRIPTION")%>
                                </div>
                            </div>
                            <div class="request-step2-ul-body1">
                                <div class="request-step2-ul-body1-box">
                                    <ul>
                                        <li>
                                            <div class="request-step2-ul-body1-box-inner">
                                                <div class="request-step2-ul-body1-box1">
                                                    <asp:Label ID="lblPackageName" runat="server"></asp:Label>
                                                </div>
                                                <div class="request-step2-ul-body1-box2">
                                                    <asp:Label ID="lblpackageDescription" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="request-step2-body-inner-haeding">
                                <div class="request-step2-body-inner-haeding1">
                                    &nbsp;
                                </div>
                                <div class="request-step2-body-inner-haeding2">
                                    <%= GetKeyResult("DESCRIPTION")%>
                                </div>
                                <div class="request-step2-body-inner-haeding3">
                                    <%= GetKeyResult("QTY")%>
                                </div>
                            </div>
                            <div class="request-step2-ul-body">
                                <div class="request-step2-ul-body-box">
                                    <ul>
                                        <asp:Repeater ID="rptPackageItem" runat="server" OnItemDataBound="rptPackageItem_ItemDataBound">
                                            <ItemTemplate>
                                                <li>
                                                    <asp:HiddenField ID="hdnItemId" runat="server" />
                                                    <div class="request-step2-ul-body-box-inner">
                                                        <div class="request-step2-ul-body-box1">
                                                            &nbsp;<asp:Label ID="lblItemName" runat="server"></asp:Label>
                                                        </div>
                                                        <div class="request-step2-ul-body-box2">
                                                            <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                                        </div>
                                                        <div class="request-step2-ul-body-box3">
                                                            <asp:Label ID="txtQuantity" runat="server" ></asp:Label>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </div>
                            </div>
                                </div>
                            </div>
                            
                        </div>
                    </li>
                    <asp:Panel ID="pnlExtra" runat="server" style="margin-top:30px;" >
                    <li>
                        <div class="request-step3-body-inner">
                            <h1>
                                <%= GetKeyResult("EXTRAS")%></h1>
                            <div class="request-step3-body-inner-haeding">
                                <div class="request-step3-body-inner-haeding1">
                                    &nbsp;
                                </div>
                                <div class="request-step3-body-inner-haeding2">
                                    <%= GetKeyResult("DESCRIPTION")%>
                                </div>
                                <div class="request-step3-body-inner-haeding3">
                                    <%= GetKeyResult("QTY")%>
                                </div>
                            </div>
                            <div class="request-step3-ul-body">
                                <div class="request-step3-ul-body-box">
                                    <ul>
                                        <asp:Repeater ID="rptExtras" runat="server" OnItemDataBound="rptExtras_ItemDataBound" ><ItemTemplate>
                                        <li>
                                            <div class="request-step3-ul-body-box-inner">
                                                <div class="request-step3-ul-body-box1">
                                                    <asp:Label ID="lblItemName" runat="server" ></asp:Label>
                                                </div>
                                                <div class="request-step3-ul-body-box2">
                                                    <asp:Label ID="lblDescription" runat="server" ></asp:Label>
                                                </div>
                                                <div class="request-step3-ul-body-box3">
                                                    <asp:Label ID="lblQuantity" runat="server" ></asp:Label>
                                                </div>
                                            </div>
                                        </li>
                                        </ItemTemplate></asp:Repeater>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="pnlFoodAndBravrages" runat="server" style="margin-top:30px;" >
                <li>
                    <div class="request-step3-body-inner">
                        <h1>
                            <span><%= GetKeyResult("CHOICE2BUILDYOURMEETING")%></span></h1>
                        <div class="request-step3-body-inner-haeding">
                            <div class="request-step3-body-inner-haeding1">
                                &nbsp;
                            </div>
                            <div class="request-step3-body-inner-haeding2">
                                <%= GetKeyResult("DESCRIPTION")%>
                            </div>
                            <div class="request-step3-body-inner-haeding3">
                                 <%= GetKeyResult("QTY")%>
                            </div>
                        </div>
                        <asp:Repeater ID="rptFoodandBravragesDay" runat="server" OnItemDataBound="rptFoodandBravragesDay_ItemDataBound">
                            <ItemTemplate>
                                <div class="request-step3-ul-body">
                            <h3>
                               <%= GetKeyResult("DAY")%> <asp:Label ID="lblSelectDay" runat="server" ></asp:Label></h3>
                            <div class="request-step3-ul-body-box">
                                <ul>
                                    <asp:Repeater ID="rptFoodandBravragesItem" runat="server" OnItemDataBound="rptFoodandBravragesItem_ItemDataBound" ><ItemTemplate>
                                    <li>
                                        <div class="request-step3-ul-body-box-inner">
                                            <div class="request-step3-ul-body-box1">
                                                <asp:Label ID="lblItemName" runat="server" ></asp:Label>
                                            </div>
                                            <div class="request-step3-ul-body-box2">
                                                <asp:Label ID="lblDescription" runat="server" ></asp:Label>
                                            </div>
                                            <div class="request-step3-ul-body-box3">
                                                <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </li>
                                    </ItemTemplate></asp:Repeater>
                                </ul>
                            </div>
                        </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        
                    </div>
                </li>
                </asp:Panel>
                
                <asp:Panel ID="pnlEquipment" runat="server" style="margin-top:30px;" >
                <li>
                    <div class="request-step3-body-inner">
                        <h1>
                            <%= GetKeyResult("EQUIPMENT")%></h1>
                        <div class="request-step3-body-inner-haeding">
                            <div class="request-step3-body-inner-haeding1">
                                &nbsp;
                            </div>
                            <div class="request-step3-body-inner-haeding2">
                                 <%= GetKeyResult("DESCRIPTION")%>
                            </div>
                            <div class="request-step3-body-inner-haeding3">
                                 <%= GetKeyResult("QTY")%>
                            </div>
                        </div>
                        <asp:Repeater ID="rptEquipmentDay" runat="server" OnItemDataBound="rptEquipmentDay_ItemDataBound" ><ItemTemplate>
                        <div class="request-step3-ul-body">
                            <h3>
                                <asp:Label ID="lblSelectDay" runat="server" ></asp:Label></h3>
                            <div class="request-step3-ul-body-box">
                                <ul>
                                    <asp:Repeater ID="rptEquipmentItem" runat="server" OnItemDataBound="rptEquipmentItem_ItemDataBound" ><ItemTemplate >
                                    <li>
                                        <div class="request-step3-ul-body-box-inner">
                                            <div class="request-step3-ul-body-box1">
                                                <asp:Label ID="lblItemName" runat="server" ></asp:Label>
                                            </div>
                                            <div class="request-step3-ul-body-box2">
                                                <asp:Label ID="lblDescription" runat="server" ></asp:Label>
                                            </div>
                                            <div class="request-step3-ul-body-box4">
                                                <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </li>
                                    </ItemTemplate></asp:Repeater>
                                </ul>
                            </div>
                        </div> 
                        </ItemTemplate></asp:Repeater>                       
                    </div>
                </li>
                </asp:Panel>
                <asp:Panel ID="pnlOthers" runat="server" style="margin-top:30px;" >
                <li>
                    <div class="request-step3-body-inner">
                        <h1>
                            <%= GetKeyResult("OTHERS")%></h1>
                        <div class="request-step3-body-inner-haeding">
                            <div class="request-step3-body-inner-haeding1">
                                &nbsp;
                            </div>
                            <div class="request-step3-body-inner-haeding2">
                                 <%= GetKeyResult("DESCRIPTION")%>
                            </div>
                            <div class="request-step3-body-inner-haeding3">
                                 <%= GetKeyResult("QTY")%>
                            </div>
                        </div>
                        <asp:Repeater ID="rptOthersDay" runat="server" OnItemDataBound="rptOthersDay_ItemDataBound" ><ItemTemplate>
                        <div class="request-step3-ul-body">
                            <h3>
                                <asp:Label ID="lblSelectDay" runat="server" ></asp:Label></h3>
                            <div class="request-step3-ul-body-box">
                                <ul>
                                    <asp:Repeater ID="rptOthersItem" runat="server" OnItemDataBound="rptOthersItem_ItemDataBound" ><ItemTemplate >
                                    <li>
                                        <div class="request-step3-ul-body-box-inner">
                                            <div class="request-step3-ul-body-box1">
                                                <asp:Label ID="lblItemName" runat="server" ></asp:Label>
                                            </div>
                                            <div class="request-step3-ul-body-box2">
                                                <asp:Label ID="lblDescription" runat="server" ></asp:Label>
                                            </div>
                                            <div class="request-step3-ul-body-box4">
                                                <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </li>
                                    </ItemTemplate></asp:Repeater>
                                </ul>
                            </div>
                        </div> 
                        </ItemTemplate></asp:Repeater>                       
                    </div>
                </li>
                </asp:Panel>
                <asp:Panel ID="pnlAccomodation" runat="server" style="margin-top:30px;" >
                <li>
                    <div class="request-step3-body-inner">
                        <h1>
                             <%= GetKeyResult("ACCOMODATION")%></h1>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                            <tr bgcolor="#d4d9cc">
                                <td colspan="2" style="border-top: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;
                                    padding: 5px">
                                    <%= GetKeyResult("BEDROOMSDATE")%>
                                </td>
                                <asp:Repeater ID="rptDays2" runat="server" OnItemDataBound="rptDays2_ItemDataBound">
                                    <ItemTemplate>
                                        <td valign="top" style="border-left: #bfd2a5 solid 1px; border-bottom: #bfd2a5 solid 1px;
                                            border-top: #bfd2a5 solid 1px; padding: 5px">
                                            <asp:Label ID="lblDay" runat="server"></asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendDays" runat="server"></asp:Literal>                                
                            </tr>
                            <tr bgcolor="#cee5ad">
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("QUANTITY")%>
                                </td>
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("SINGLE")%>
                                </td>
                                <asp:Repeater ID="rptSingleQuantity" runat="server" OnItemDataBound="rptSingleQuantity_ItemDataBound">
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                            <asp:HiddenField ID="hdnDate" runat="server" />
                                            <asp:Label ID="txtQuantitySDay" runat="server" >0</asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendSQuantity" runat="server"></asp:Literal>
                            </tr>
                            <tr bgcolor="#cee5ad">
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    &nbsp;
                                </td>
                                <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                    <%= GetKeyResult("DOUBLE")%>
                                </td>
                                <asp:Repeater ID="rptDoubleQuantity" runat="server" OnItemDataBound="rptDoubleQuantity_ItemDataBound">
                                    <ItemTemplate>
                                        <td valign="top" align="center" style="padding: 5px; border-top: #bfd2a5 solid 1px">
                                            <asp:HiddenField ID="hdnDate" runat="server" />
                                            <asp:Label ID="txtQuantityDDay" runat="server" >0</asp:Label>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="ltrExtendDQuantity" runat="server"></asp:Literal>
                            </tr>
                        </table>
                    </div>
                </li>
                </asp:Panel>
            </ul>
        </div>
        <!--request-step3-body ENDS HERE-->
        <div class="request-step3-special-request">
            <h1>
                 <%= GetKeyResult("SPECIALREQUEST")%></h1>
            <div class="request-step3-special-request-textareabox">
                <asp:Label ID="lblSpecialRequest" runat="server"></asp:Label>
            </div>
        </div>
        <div class="request-step3-content">
            <h3>
                <%= GetKeyResult("TERMSANDCONDITIONS")%></h3>
            <p>
                <asp:HyperLink ID="hypTermsandCondition" runat="server"><%= GetKeyResult("TERMSANDCONDITIONSTEXT")%>.</asp:HyperLink>
            </p>
            <input type="checkbox" id="chkIAgree" />&nbsp;&nbsp;<b><%= GetKeyResult("IREADANDAGREE")%></b>
        </div>
    </div>
    <!--request-step3-body ENDS HERE-->
    <div class="next-button">
        <asp:LinkButton ID="lnkPrevious" runat="server" CssClass="step-pre-blue-btn" 
                onclick="lnkPrevious_Click"><%= GetKeyResult("PREVIOUSSTEP")%></asp:LinkButton> &nbsp;<asp:LinkButton ID="lnkConfirm" runat="server" CssClass="confirm-blue-btn" onclick="lnkConfirm_Click"><%= GetKeyResult("STEP3SENDIT")%></asp:LinkButton>
    </div>
    <script language="javascript"  type="text/javascript">
        function open_win(pageurl) {

            var url = pageurl;
            var winName = 'myWindow';
            var w = '700';
            var h = '500';
            var scroll = 'no';
            var popupWindow = null;
            LeftPosition = (screen.width) ? (screen.width - w) / 2 : 0;
            TopPosition = (screen.height) ? (screen.height - h) / 2 : 0;
            settings = 'height=' + h + ',width=' + w + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=no,directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no';
            popupWindow = window.open(url, winName, settings)
            return false;

        }


    jQuery(document).ready(function () {  jQuery("#<%= lnkConfirm.ClientID %>").bind("click",function(){
        if (document.getElementById('chkIAgree').checked) { return true; } else {
            alert('<%= GetKeyResultForJavaScript("AGREETERMSANDCONDITION") %>.');
    return false; } }); });</script>
</asp:Content>
