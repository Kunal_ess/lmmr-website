﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using LMMR.Business;
using LMMR.Entities;

public partial class WL_Communication : System.Web.UI.Page
{
    ManageWhiteLabel objManager = new ManageWhiteLabel();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.QueryString["src"] != null && Request.QueryString["WLID"] != null && Request.QueryString["style"] != null)
        {
            long wlID = Convert.ToInt64(Request.QueryString["WLID"]);
            WhiteLabel obj = objManager.GetWhiteLabelByID(wlID);
            if (obj != null)
            {
                string Link = Convert.ToString(Request.QueryString["src"]);
                if (Convert.ToBoolean(Request.QueryString["style"]))
                {
                    ltrCSS.Text = "<link href=\"" + (obj.TargetUrl.EndsWith("/") ? obj.TargetUrl.Substring(0, obj.TargetUrl.Length - 1) : obj.TargetUrl) + "/css/lmmr.css\" rel=\"stylesheet\" type=\"text/css\" />";
                }
                ScriptManager.RegisterStartupScript(this, typeof(Page), "WL", "LoadWL('" + Convert.ToString(Request.QueryString["WLID"]) + "','" + Link + "');", true);
            }
        }
        if (Request.QueryString["WLID"] != null)
        {

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "WL", "LoadWL('4');", true);
        }
    }
}