﻿<%@ Page Title="Booking Step 1" Language="C#" MasterPageFile="~/HomeMaster.master"
    AutoEventWireup="true" CodeFile="BookingStep1.aspx.cs" Inherits="BookingStep1" %>
 <%@ MasterType VirtualPath="~/HomeMaster.master" %> 
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntLeft" runat="Server">
    <asp:Literal ID="ltrHeader" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntMainbody" runat="Server">
    <!-- ClickTale Top part -->
    <script type="text/javascript">
        var WRInitTime = (new Date()).getTime();
    </script>
    <!-- ClickTale end of Top part -->
    <!--today-date START HERE-->
    <div class="today-date">
        <asp:Panel ID="pnlLogin" runat="server">
            <div class="today-date-left">
                <b>
                    <%= GetKeyResult("TODAY")%>:</b><asp:Label ID="lblDateLogin" runat="server"></asp:Label>
            </div>
            <div class="today-date-right">
                <%= GetKeyResult("YOUARELOGGEDINAS")%>: <b>
                    <asp:Label ID="lblUserName" runat="server"></asp:Label></b>
            </div>
        </asp:Panel>
    </div>
    <!--today-date ENDS HERE-->
    <!--booking-heading START HERE-->
    <div class="booking-stepbody">
        <div class="booking-step1">
            <span>
                <%= GetKeyResult("STEP1")%>.</span>
            <%= GetKeyResult("STEP1SELECTMEETINGROOM")%>
        </div>
        <div class="booking-step2">
            <span>
                <%= GetKeyResult("STEP2")%>.</span>
            <%= GetKeyResult("STEP2EXTRAOPTIONSBEDROOMS")%>
        </div>
        <div class="booking-step3">
            <span>
                <%= GetKeyResult("STEP3")%>.</span>
            <%= GetKeyResult("STEP3SENDIT")%>
        </div>
        <div class="booking-step4">
            <span>
                <%= GetKeyResult("STEPFINILIZED")%></span>
        </div>
    </div>
    <!--booking-heading ENDS HERE-->
    <!--booking-heading START HERE-->
    <div class="booking-heading">
        <span>
            <%= GetKeyResult("YOURBOOKING")%>:</span>
        <%= GetKeyResult("SELECTEDMEETINGROOMS")%>
    </div>
    <!--booking-heading ENDS HERE-->
    <!--booking date body START HERE-->
    <div class="booking-date-body">
        <div class="arrival-date">
            <span>
                <%= GetKeyResult("ARRIVALDATE")%>:</span><asp:Label ID="lblArrival" runat="server"></asp:Label>
        </div>
        <div class="departure-date">
            <span>
                <%= GetKeyResult("DEPARTUREDATE")%>:</span><asp:Label ID="lblDeparture" runat="server"></asp:Label>
        </div>
        <div class="duration-day">
            <span>
                <%= GetKeyResult("DURATION")%>:</span> <span class="red">
                    <asp:Label ID="lblDuration" runat="server"></asp:Label></span>
        </div>
    </div>
    <!--booking date body ENDS HERE-->
    <!--profile-body START HERE-->
    <div class="profile-body">
        <div class="profile-body-left">
            <div class="profile-body-left-left">
                <asp:Image ID="imgHotel" runat="server" align="absmiddle" Width="100px" Height="100px" />
            </div>
            <div class="profile-body-left-right">
                <div class="star3">
                    <asp:Image ID="imgstars" runat="server" />
                </div>
                <h2>
                    <asp:Label ID="lblHotelname" runat="server"></asp:Label></h2>
                <p class="heading">
                    <asp:Label ID="lblAddress" runat="server"></asp:Label></p>
                <p>
                    <asp:Label runat="server" ID="lblDescription"></asp:Label></p>
                <table cellpadding="0" cellspacing="0" style="margin: 10px 0px; border: #D7D1D1 solid 1px;">
                    <tr>
                        <td style="padding: 5px; border-bottom: #D7D1D1 solid 1px;">
                            <b>
                                <%= GetKeyResult("TIMING")%></b>
                        </td>
                        <td style="padding: 5px; border-bottom: #D7D1D1 solid 1px;">
                            <b>
                                <%= GetKeyResult("FROM")%></b>
                        </td>
                        <td style="padding: 5px; border-bottom: #D7D1D1 solid 1px;">
                            <b>
                                <%= GetKeyResult("TO")%></b>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px; border-bottom: #D7D1D1 solid 1px;">
                            <%= GetKeyResult("FULLDAY")%>
                        </td>
                        <td style="padding: 5px; border-bottom: #D7D1D1 solid 1px;">
                            <asp:Label ID="lblFullFrom" runat="server"></asp:Label>
                        </td>
                        <td style="padding: 5px; border-bottom: #D7D1D1 solid 1px;">
                            <asp:Label ID="lblFullTo" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px; border-bottom: #D7D1D1 solid 1px;">
                            <%= GetKeyResult("MORNING")%>
                        </td>
                        <td style="padding: 5px; border-bottom: #D7D1D1 solid 1px;">
                            <asp:Label ID="lblMorningFrom" runat="server"></asp:Label>
                        </td>
                        <td style="padding: 5px; border-bottom: #D7D1D1 solid 1px;">
                            <asp:Label ID="lblMorningTo" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px;">
                            <%= GetKeyResult("AFTERNOON")%>
                        </td>
                        <td style="padding: 5px;">
                            <asp:Label ID="lblAfternoonFrom" runat="server"></asp:Label>
                        </td>
                        <td style="padding: 5px;">
                            <asp:Label ID="lblAfternoonTo" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="profile-body-right">
            <a href="javascript:void(0);" id="showMap">
                <img src="<%= SiteRootPath %>images/map-icon.png" align="absmiddle" />
                <%= GetKeyResult("SHOWMAP")%></a>
        </div>
        <div id="maploc" style="background-color: #E5E3DF; border: 2px solid green; float: right;
            height: 176px; left: 923px; overflow: hidden; position: absolute; top: 242px;
            width: 249px; z-index: 50; display: none;">
        </div>
        <input type="image" src="<%= SiteRootPath %>Images/close.png" style="float: right; position: absolute;
            z-index: 55; display: none;" class="close" />
        <script language="javascript" type="text/javascript">
            jQuery(document).ready(function () {
                jQuery("#showMap").bind("click", function () {var offsetshowmap = jQuery("#showMap").offset();
                    jQuery("#maploc").css({"top":(offsetshowmap.top+20) + "px","left":(offsetshowmap.left-10)+"px"});
                    jQuery(".close").css({"top":(offsetshowmap.top+10) + "px","left":(offsetshowmap.left + 230)+"px"})
                    jQuery("#maploc").show();jQuery(".close").show();LoadMapLatLong();});
                jQuery(".close").bind("click",function(){ jQuery("#maploc").hide();jQuery(".close").hide();return false;});
            });
            function LoadMapLatLong() {

                var map = new google.maps.Map(document.getElementById('maploc'), {
                    zoom: 14,
                    center: new google.maps.LatLng(<%= CurrentLat %>, <%= CurrentLong %>),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(<%= CurrentLat %>, <%= CurrentLong %>),
                    map: map
                });

            }
        </script>
    </div>
    <!--profile-body ENDS HERE-->
    <div id="divWarnning" runat="server" class="warning" style="display:none;"></div>
    <div class="error" id="divError" runat="server" style="display: none;">
    </div>
    <!--meeting room body START HERE-->
    <div class="meeting-room-body">
        <h1>
            <%= GetKeyResult("MEETINGROOMS")%></h1>
        <span style="margin-left: 10px;"><i>
            <%= GetKeyResult("FORALLMEETINGROOMSYOUHAVETOSPECIFIED")%>.</i></span>
        <div class="meeting-room-body-heading">
            <div class="meeting-room-body-heading1">
                <%= GetKeyResult("DESCRIPTION")%>
            </div>
            <div class="meeting-room-body-heading2">
                <%= GetKeyResult("ARRIVALTIME")%>
                &nbsp;&nbsp;
                <%= GetKeyResult("DEPARTURETIME")%>
            </div>
            <div class="meeting-room-body-heading3">
                <%= GetKeyResult("QTY")%>
            </div>
        </div>
        <ul>
            <asp:Repeater ID="dtLstMeetingroom" runat="server" OnItemDataBound="dtLstMeetingroom_ItemDataBound"
                OnItemCommand="dtLstMeetingroom_ItemCommand">
                <ItemTemplate>
                    <li>
                        <div class="meeting-room-body-ulli-heading">
                            <h2>
                                1.
                                <asp:Label ID="lblMeetingRoomName" runat="server"></asp:Label></h2>
                            <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/images/delete-ol-btn.png"
                                CssClass="ImageFloat" CommandName="DeleteMeetingroom" CommandArgument='<%# Eval("MRId") %>' />
                        </div>
                        <div class="meeting-room-body-ulli-body" id="mydiv" runat="server">
                            <div class="meeting-room-body-ulli-body-image">
                                <asp:Image ID="imgMrImage" runat="server" Width="69px" Height="69px" />
                            </div>
                            <div class="meeting-room-body-ulli-body-description">
                                <p>
                                    <asp:Label ID="lblConfigurationType" runat="server"></asp:Label></p>
                                <p class="last">
                                    <asp:Label ID="lblMaxandMinCapacity" runat="server"></asp:Label></p>
                                <div>
                                    <asp:HyperLink ID="hypPlan" runat="server"></asp:HyperLink></div>
                            </div>
                            <div class="meeting-room-body-ulli-right">
                                <asp:Repeater ID="rptDays" runat="server" OnItemDataBound="rptDays_ItemDataBound">
                                    <ItemTemplate>
                                        <div class="meeting-room-body-ulli-right-inner">
                                            <div class="right-inner1">
                                                <%= GetKeyResult("DAY")%>
                                                1
                                            </div>
                                            <div class="right-inner2">
                                                <asp:Label ID="lblDayDuration" runat="server"></asp:Label>
                                            </div>
                                            <div class="right-inner3">
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td align="left">
                                                            <asp:HiddenField ID="hdnHotelArivalTime" runat="server" />
                                                            <asp:DropDownList ID="drpFrom" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td align="right">
                                                            <asp:HiddenField ID="hdnHotelDepartureTime" runat="server" />
                                                            <asp:DropDownList ID="drpTo" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="right-inner4">
                                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="inputbox" MaxLength="3"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtQuantity"
                                                    ValidChars="0123456789" runat="server">
                                                </asp:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <div class="meeting-room-body-ulli-right-inner-bottom">
                                            <div class="right-inner1">
                                                <%= GetKeyResult("DAY")%>
                                                2
                                            </div>
                                            <div class="right-inner2">
                                                <asp:Label ID="lblDayDuration" runat="server"></asp:Label>
                                            </div>
                                            <div class="right-inner3">
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td align="left">
                                                            <asp:HiddenField ID="hdnHotelArivalTime" runat="server" />
                                                            <asp:DropDownList ID="drpFrom" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td align="right">
                                                            <asp:HiddenField ID="hdnHotelDepartureTime" runat="server" />
                                                            <asp:DropDownList ID="drpTo" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="right-inner4">
                                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="inputbox" MaxLength="3"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtQuantity"
                                                    ValidChars="0123456789" runat="server">
                                                </asp:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                    </AlternatingItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </li>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <li>
                        <div class="meeting-room-body-ulli-heading-last">
                            <h2>
                                2.
                                <asp:Label ID="lblMeetingRoomName" runat="server"></asp:Label></h2>
                            <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/images/delete-ol-btn.png"
                                CommandName="DeleteMeetingroom" CommandArgument='<%# Eval("MRId") %>' CssClass="ImageFloat" />
                        </div>
                        <div class="meeting-room-body-ulli-body" id="mydiv" runat="server">
                            <div class="meeting-room-body-ulli-body-image">
                                <asp:Image ID="imgMrImage" runat="server" Width="69px" Height="69px" />
                            </div>
                            <div class="meeting-room-body-ulli-body-description">
                                <p>
                                    <asp:Label ID="lblConfigurationType" runat="server"></asp:Label></p>
                                <p class="last">
                                    <asp:Label ID="lblMaxandMinCapacity" runat="server"></asp:Label></p>
                                <div>
                                    <asp:HyperLink ID="hypPlan" runat="server"></asp:HyperLink></div>
                            </div>
                            <div class="meeting-room-body-ulli-right">
                                <asp:Repeater ID="rptDays" runat="server" OnItemDataBound="rptDays_ItemDataBound">
                                    <ItemTemplate>
                                        <div class="meeting-room-body-ulli-right-inner">
                                            <div class="right-inner1">
                                                <%= GetKeyResult("DAY")%>
                                                1
                                            </div>
                                            <div class="right-inner2">
                                                <asp:Label ID="lblDayDuration" runat="server"></asp:Label>
                                            </div>
                                            <div class="right-inner3">
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td align="left">
                                                            <asp:HiddenField ID="hdnHotelArivalTime" runat="server" />
                                                            <asp:DropDownList ID="drpFrom" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td align="right">
                                                            <asp:HiddenField ID="hdnHotelDepartureTime" runat="server" />
                                                            <asp:DropDownList ID="drpTo" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="right-inner4">
                                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="inputbox" MaxLength="3"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtQuantity"
                                                    ValidChars="0123456789" runat="server">
                                                </asp:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <div class="meeting-room-body-ulli-right-inner-bottom">
                                            <div class="right-inner1">
                                                <%= GetKeyResult("DAY")%>
                                                2
                                            </div>
                                            <div class="right-inner2">
                                                <asp:Label ID="lblDayDuration" runat="server"></asp:Label>
                                            </div>
                                            <div class="right-inner3">
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td align="left">
                                                            <asp:HiddenField ID="hdnHotelArivalTime" runat="server" />
                                                            <asp:DropDownList ID="drpFrom" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td align="right">
                                                            <asp:HiddenField ID="hdnHotelDepartureTime" runat="server" />
                                                            <asp:DropDownList ID="drpTo" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="right-inner4">
                                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="inputbox" MaxLength="3"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtQuantity"
                                                    ValidChars="0123456789" runat="server">
                                                </asp:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                    </AlternatingItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </li>
                </AlternatingItemTemplate>
            </asp:Repeater>
        </ul>
    </div>
    <!-- Code added For WL -->
    <div id="divJoinToday-overlay">
    </div>
    <div id="divJoinToday">
        <div class="popup-top">
        </div>
        <div class="popup-mid">
            <div class="popup-mid-inner">
                <div class="error" id="errorPopup" runat="server">
                </div>
                <div>
                    <asp:RadioButton ID="rbregister" runat="server" GroupName="register" Text="Quick Register"
                        onclick="return registerORLogin();" Checked="true" /><br />
                    <asp:RadioButton ID="rblogin" runat="server" GroupName="register" Text="Using Lastminutemeetingroom.com Login credentials"
                        onclick="return registerORLogin();" />
                </div>
                <table cellspacing="5" id="tableRegister" style="margin-left: 20px;">
                    <tr>
                        <td>
                            <b>
                                <%= GetKeyResult("FIRSTNAME")%>:</b>
                        </td>
                        <td>
                            <asp:TextBox ID="txtFirstName" runat="server" value="" class="inputregister"  MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>
                                <%= GetKeyResult("LASTNAME")%>:</b>
                        </td>
                        <td>
                            <asp:TextBox ID="txtLastName" runat="server" value="" class="inputregister" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>
                                <%= GetKeyResult("COMPANY")%>:</b>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCompanyName" runat="server" value="" class="inputregister" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>
                                <%= GetKeyResult("EMAIL")%>:</b>
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmail" runat="server" value="" class="inputregister" MaxLength="250"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>
                                <%= GetKeyResult("PHONE")%>:</b>
                        </td>
                        <td>
                            <asp:TextBox ID="txtphoneNo" runat="server" value="" class="inputregister" MaxLength="15"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtphoneNo"
                                ValidChars="-0123456789">
                            </asp:FilteredTextBoxExtender>
                        </td>
                    </tr>
                </table>
                <div class="subscribe-btn1" id="divRegisterButton" style="padding-top: 0;">
                    <div class="save-cancel-btn1 button_section">
                        <asp:LinkButton ID="btnRegister" runat="server" CssClass="send-request-btn" OnClick="btnRegister_Click"><%= GetKeyResult("CONTINUE")%></asp:LinkButton>
                        &nbsp;or&nbsp;
                        <asp:LinkButton ID="lnkCnacel" runat="server" CssClass="cancelpop" OnClientClick="return closeRegisterORLoginPopUp();"><%=GetKeyResult("CANCEL") %></asp:LinkButton>
                    </div>
                </div>
                <table id="tableLogin" cellspacing="5" style="display: none; margin-left: 20px;">
                    <tr>
                        <td>
                            <b>
                                <%= GetKeyResult("LOGIN")%>:</b>
                        </td>
                        <td>
                            <asp:TextBox ID="txtLoginID" runat="server" value="" class="inputregister"  MaxLength="250"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>
                                <%= GetKeyResult("PASSWORD")%>:</b>
                        </td>
                        <td>
                            <asp:TextBox ID="txtPassword" runat="server" value="" class="inputregister" MaxLength="250" TextMode="Password"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <div class="subscribe-btn" id="divLoginButton" style="display: none">
                    <div class="save-cancel-btn1 button_section">
                        <asp:LinkButton ID="btnLogin" runat="server" CssClass="select" OnClick="btnLogin_Click"
                            OnClientClick="javascript: _gaq.push(['_trackPageview', 'RegisterOK-Venue.aspx']);"><%=GetKeyResult("LOGIN")%></asp:LinkButton>
                        &nbsp;or&nbsp;
                        <asp:LinkButton ID="btnLoginCancel" runat="server" CssClass="cancelpop" OnClientClick="return closeRegisterORLoginPopUp();"><%=GetKeyResult("CANCEL") %></asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <div class="popup-bottom">
        </div>
        <script language="javascript" type="text/javascript">

            function showlogipopup(msg) {
                jQuery('#tableRegister').hide();
                jQuery('#tableLogin').show();
                jQuery('#divRegisterButton').hide();
                jQuery('#divLoginButton').show();
                if (jQuery("#<%= errorPopup.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= errorPopup.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                }
                jQuery("#<%= errorPopup.ClientID %>").html(msg);
                jQuery("#<%= errorPopup.ClientID %>").show();
            }

            function showregistrationpopup(msg) {
                jQuery('#tableLogin').hide();
                jQuery('#tableRegister').show();
                jQuery('#divRegisterButton').show();
                jQuery('#divLoginButton').hide();
                if (jQuery("#<%= errorPopup.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= errorPopup.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                }
                jQuery("#<%= errorPopup.ClientID %>").html(msg);
                jQuery("#<%= errorPopup.ClientID %>").show();

            }
            function registerORLogin() {
                if (jQuery('#<%=rbregister.ClientID %>:checked').val() == "rbregister") {
                    jQuery('#tableLogin').hide();
                    jQuery('#tableRegister').show();
                    jQuery('#divRegisterButton').show();
                    jQuery('#divLoginButton').hide();
                } else {
                    jQuery('#tableRegister').hide();
                    jQuery('#tableLogin').show();
                    jQuery('#divRegisterButton').hide();
                    jQuery('#divLoginButton').show();
                }

                if (jQuery("#<%= errorPopup.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= errorPopup.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                }
                jQuery("#<%= errorPopup.ClientID %>").html("");
                jQuery("#<%= errorPopup.ClientID %>").hide();
            }

            function openRegisterORLoginPopUp() {

                if (jQuery("#<%= errorPopup.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= errorPopup.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                }
                jQuery("#<%= errorPopup.ClientID %>").html("");
                jQuery("#<%= errorPopup.ClientID %>").hide();
                //jQuery("#Loding_overlay").show();
                //jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                jQuery("#divJoinToday").show();
                jQuery("#divJoinToday-overlay").show();
                return false;
            }

            function closeRegisterORLoginPopUp() {

                document.getElementsByTagName('html')[0].style.overflow = 'auto';
                document.getElementById('divJoinToday').style.display = 'none';
                document.getElementById('divJoinToday-overlay').style.display = 'none';
                jQuery('#<%=txtFirstName.ClientID %>').val('');
                jQuery('#<%=txtLastName.ClientID %>').val('');
                jQuery('#<%=txtCompanyName.ClientID %>').val('');
                jQuery('#<%=txtEmail.ClientID %>').val('');
                jQuery('#<%=txtphoneNo.ClientID %>').val('');
                jQuery('#<%=txtLoginID.ClientID %>').val('');
                jQuery('#<%=txtPassword.ClientID %>').val('');
                jQuery('#<%=rbregister.ClientID %>').attr('checked', true);
                registerORLogin();
                return false;
            }

            jQuery("#<%= btnRegister.ClientID %>").bind("click", function () {
                if (jQuery("#<%= errorPopup.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= errorPopup.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                }
                var isvalid = true;
                var errormessage = "";
                var FirstName = jQuery("#<%= txtFirstName.ClientID %>").val();
                if (FirstName.length <= 0 || FirstName == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>"
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("FIRSTNAMEERRORMESSAGE")%>';
                    isvalid = false;
                }

                var LastName = jQuery("#<%= txtLastName.ClientID %>").val();
                if (LastName.length <= 0 || LastName == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>"
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("LASTNAMEERRORMESSAGE")%>';
                    isvalid = false;
                }

                var emailp1 = jQuery("#<%= txtEmail.ClientID %>").val();
                if (emailp1.length <= 0 || emailp1 == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("EMAILIDERRORMESSAGE")%>';
                    isvalid = false;
                }

                else {
                    if (!validateEmail(emailp1)) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += '<%= GetKeyResultForJavaScript("EMAILVALADITYERRORMESSAGE")%>';
                        isvalid = false;
                    }
                }

                var Phone = jQuery("#<%=txtphoneNo.ClientID %>").val();
                if (Phone.length <= 0 || Phone == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>"
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("ERRORPHONE")%>';
                    isvalid = false;
                }

                if (!isvalid) {
                    jQuery("#<%= errorPopup.ClientID %>").show();
                    jQuery("#<%= errorPopup.ClientID %>").html(errormessage);
                    return false;
                }
                jQuery("#Loding_overlay").show();
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                jQuery("#divJoinToday").show();
                jQuery("#divJoinToday-overlay").show();
            });

            jQuery("#<%= btnLogin.ClientID %>").bind("click", function () {
                if (jQuery("#<%= errorPopup.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= errorPopup.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                }
                var isvalid = true;
                var errormessage = "";
                var Email = jQuery("#<%=txtLoginID.ClientID %>").val();
                if (Email.length == 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("EMAILISREQUIRED")%>';
                    isvalid = false;
                }
                else {
                    if (!validateEmail(Email)) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += '<%= GetKeyResultForJavaScript("ENTERVALIDEMAIL")%>';
                        isvalid = false;
                    }
                }

                var password = jQuery("#<%=txtPassword.ClientID %>").val();
                if (password.length == 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("PASSWORDISREQUIRED")%>';
                    isvalid = false;
                }
                if (!isvalid) {
                    jQuery("#<%= errorPopup.ClientID %>").show();
                    jQuery("#<%= errorPopup.ClientID %>").html(errormessage);
                    return false;
                }

                jQuery("#Loding_overlay").show();
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                jQuery("#divJoinToday").show();
                jQuery("#divJoinToday-overlay").show();
            });
            function validateEmail(txtEmail) {
                var a = txtEmail;
                var filter = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
                if (filter.test(a)) {
                    return true;
                }
                else {
                    return false;
                }
            }
        </script>
    </div>
    <!-- End WL Code -->
    <!--meeting room body ENDS HERE-->
    <!--next button START HERE-->
    <div class="next-button">
        &nbsp;&nbsp;<asp:LinkButton ID="lnkNext" runat="server" CssClass="step-next-btn"
            OnClick="lnkNext_Click"><%= GetKeyResult("NEXTSTEP")%></asp:LinkButton>
    </div>
    <!--next button ENDS HERE-->
    <script language="javascript" type="text/javascript">
        var loginUser = '<%= Session["CurrentRestUserID"] %>';
        jQuery(document).ready(function () {
            jQuery("#<%= divError.ClientID %>").hide();
            jQuery("#<%= lnkNext.ClientID %>").bind("click", function () {
                var strQu = getQuerystring('wl', 'no');
                if (strQu == 'no') {
                    var errorMessage = "";
                    var isValid = true;
                    var countdivs = jQuery(".meeting-room-body-ulli-body").length;
                    for (i = 0; i < parseInt(countdivs, 10); i++) {
                        var maxandmin = jQuery("#cntMainBody_cntMainbody_dtLstMeetingroom_mydiv_" + i + " .meeting-room-body-ulli-body-description .last span").html();
                        jQuery("#cntMainBody_cntMainbody_dtLstMeetingroom_mydiv_" + i + " .meeting-room-body-ulli-right .right-inner4 input:text").each(function (e) {
                            var val = jQuery(this).val();
                            if (parseInt(val) < parseInt(maxandmin.split('-')[0]) || parseInt(val) > parseInt(maxandmin.split('-')[1])) {
                                jQuery(this).focus();
                                if (errorMessage.length > 0) {
                                    errorMessage += "<br>";
                                }
                                errorMessage += '<%= GetKeyResultForJavaScript("PLEASEENTERVALUEBETWEEN")%>' + maxandmin.split('-')[0] + ' <%= GetKeyResult("AND")%> ' + maxandmin.split('-')[1] + '.';
                                isValid = false;
                            }
                            else if (isNaN(val) || val.length <= 0) {
                                //jQuery(this).addClass("errorQuantity");
                                if (errorMessage.length > 0) {
                                    errorMessage += "<br>";
                                }
                                errorMessage += '<%= GetKeyResultForJavaScript("PLEASEENTERQUANTITY")%>.';
                                isValid = false;
                            }
                        });
                    }
                    if (!isValid) {
                        jQuery("#<%= divError.ClientID %>").html(errorMessage);
                        jQuery("#<%= divError.ClientID %>").show();
                        var offseterror = jQuery("#<%= divError.ClientID %>").offset();
                        jQuery("body").scrollTop(offseterror.top);
                        jQuery("html").scrollTop(offseterror.top);
                        return false;
                    }
                    else {
                        if (loginUser == '') {
                            alert('<%= GetKeyResultForJavaScript("LOGINFORBOOKING")%>.');
                            jQuery("#<%= divError.ClientID %>").hide();
                        }
                        else {
                            jQuery("#<%= divError.ClientID %>").hide();
                        }
                    }

                } else {
                    //Code for white label.
                    var errorMessage = "";
                    var isValid = true;
                    var countdivs = jQuery(".meeting-room-body-ulli-body").length;
                    for (i = 0; i < parseInt(countdivs, 10); i++) {
                        var maxandmin = jQuery("#cntMainBody_cntMainbody_dtLstMeetingroom_mydiv_" + i + " .meeting-room-body-ulli-body-description .last span").html();
                        jQuery("#cntMainBody_cntMainbody_dtLstMeetingroom_mydiv_" + i + " .meeting-room-body-ulli-right .right-inner4 input:text").each(function (e) {
                            var val = jQuery(this).val();
                            if (parseInt(val) < parseInt(maxandmin.split('-')[0]) || parseInt(val) > parseInt(maxandmin.split('-')[1])) {
                                jQuery(this).focus();
                                if (errorMessage.length > 0) {
                                    errorMessage += "<br>";
                                }
                                errorMessage += '<%= GetKeyResultForJavaScript("PLEASEENTERVALUEBETWEEN")%>' + maxandmin.split('-')[0] + ' <%= GetKeyResult("AND")%> ' + maxandmin.split('-')[1] + '.';
                                isValid = false;
                            }
                            else if (isNaN(val) || val.length <= 0) {
                                //jQuery(this).addClass("errorQuantity");
                                if (errorMessage.length > 0) {
                                    errorMessage += "<br>";
                                }
                                errorMessage += '<%= GetKeyResultForJavaScript("PLEASEENTERQUANTITY")%>.';
                                isValid = false;
                            }
                        });
                    }
                    if (!isValid) {
                        jQuery("#<%= divError.ClientID %>").html(errorMessage);
                        jQuery("#<%= divError.ClientID %>").show();
                        var offseterror = jQuery("#<%= divError.ClientID %>").offset();
                        jQuery("body").scrollTop(offseterror.top);
                        jQuery("html").scrollTop(offseterror.top);
                        return false;
                    }
                    else {
                        if (loginUser == '') {
                            openRegisterORLoginPopUp();
                            jQuery("#<%= divError.ClientID %>").hide();
                            return false;
                        }
                        else {
                            jQuery("#<%= divError.ClientID %>").hide();
                        }
                    }
                }
            });

        });

        function getQuerystring(key, default_) {
            if (default_ == null) default_ = "";
            key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
            var qs = regex.exec(window.location.href);
            if (qs == null)
                return default_;
            else
                return qs[1];
        }
    </script>
    <!-- ClickTale Bottom part -->
    <div id="ClickTaleDiv" style="display: none;">
    </div>
    <script type="text/javascript">
        if (document.location.protocol != 'https:')
            document.write(unescape("%3Cscript%20src='http://s.clicktale.net/WRd.js'%20type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
        if (typeof ClickTale == 'function') ClickTale(24322, 0.0586, "www02");
    </script>
    <!-- ClickTale end of Bottom part -->
</asp:Content>
