﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeFile="SearchResult.aspx.cs" Inherits="SearchResult" %>
   
<%@ Register Src="~/UserControl/Frontend/TopMenu.ascx" TagName="TopMenu" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="UserControl/Frontend/BottomLink.ascx" TagName="BottomLink" TagPrefix="uc1" %>
<%@ Register Src="UserControl/Frontend/LinkMedia.ascx" TagName="LinkMedia" TagPrefix="uc2" %>
<%@ Register Src="UserControl/Frontend/Mapsearch.ascx" TagName="Mapsearch" TagPrefix="uc3" %>
<%@ Register Src="UserControl/Frontend/Benefits.ascx" TagName="Benefits" TagPrefix="uc2" %>
<%@ Register Src="UserControl/Frontend/RecentlyJoinedHotel.ascx" TagName="RecentlyJoinedHotel"
    TagPrefix="uc3" %>
<%@ Register Src="UserControl/Frontend/NewsSubscriber.ascx" TagName="NewsSubscriber"
    TagPrefix="uc4" %>
<%@ Register Src="UserControl/Frontend/HotelOfTheWeek.ascx" TagName="HotelOfTheWeek"
    TagPrefix="uc5" %>
<%@ Register Src="UserControl/Frontend/SearchResult.ascx" TagName="SearchResult"
    TagPrefix="uc6" %>
<%@ Register Src="UserControl/Frontend/WithoutNews.ascx" TagName="WithoutNews" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cntLeftSearch" runat="Server">

    <asp:HiddenField ID="hdnSearchButtonClick" Value="0" runat="server" />
    <div class="left-inner-top clearfix">
        <div class="left-inner-top-top">
            <div class="map" style="cursor: pointer" id="Divmapbutton" runat="server">
                <asp:ImageButton ID="imgMapSearch" ImageUrl="~/images/map.png" runat="server" OnClick="imgMapSearch_Click"
                    OnClientClick="return IsCountryCitySelected();" />
            </div>
            <div id="divlogo" runat="server">
                <a id="logoHotel" runat="server">
                    <img src="<%=SiteRootPath %>images/logo-front.png" width="155" height="109" runat="server" id="imgSiteLogo" /></a>
            </div>
            <!--left-form START HERE-->
        </div>
        <script language="javascript" type="text/javascript">
            function onDropdownCountrySelected() {
                //alert(document.getElementById("<%=drpCountry.ClientID%>").value);
                var me = document.getElementById("<%=drpCountry.ClientID%>").value;
                MiscWebService.set_timeout = 60000;
                jQuery("#<%=drpCity.ClientID%>").find("option").remove()
                jQuery("#<%=drpCity.ClientID%>").append(jQuery("<option></option>").val("0").html('--<%= GetKeyResultForJavaScript("LOADING") %>--'));
                MiscWebService.GetCityByCountryIDForFrontend(me, OnComplete, OnTimeOut, OnError);
            }
            function OnComplete(args) {
                jQuery("#<%=drpCity.ClientID%>").find("option").remove()
                jQuery("#<%=drpCity.ClientID%>").append(jQuery("<option></option>").val("0").html('--<%= GetKeyResultForJavaScript("SELECTCITY") %>--'));
                for (i = 0; i < args.length; i++) {
                    //alert(args[i].id);
                    jQuery("#<%=drpCity.ClientID%>").append(jQuery("<option></option>").val(args[i].id).html(args[i].Name));
                }
            }
            function OnTimeOut(args) {
                jQuery("#<%=drpCity.ClientID%>").find("option").remove()
                jQuery("#<%=drpCity.ClientID%>").append(jQuery("<option></option>").val("0").html('--<%= GetKeyResultForJavaScript("SELECTCITY") %>--'));
                alert("Service call timed out." + jQuery(args).text());
            }

            function OnError(args) {
                jQuery("#<%=drpCity.ClientID%>").find("option").remove()
                jQuery("#<%=drpCity.ClientID%>").append(jQuery("<option></option>").val("0").html('--<%= GetKeyResultForJavaScript("SELECTCITY") %>--'));
                alert("Error calling service method." + jQuery(args).text());
            }
            function onDropdownCitySelected() {
                var ids = jQuery("#<%= drpCity.ClientID %>").find("option:selected").val();
                jQuery("#<%= hdnCityDetail.ClientID %>").val(ids);
                var ids = jQuery("#<%= drpCountry.ClientID %>").find("option:selected").val();
                jQuery("#<%= hdnCountry.ClientID %>").val(ids);
                return false;
            }
        </script>
         <div class="left-inner-top-mid clearfix">
            <div class="left-form">
                <div id="Country"><asp:HiddenField ID="hdnCountry" runat="server" />
                    <asp:DropDownList ID="drpCountry" runat="server" CssClass="bigselect" onchange="return onDropdownCountrySelected();" 
                        AutoPostBack="false">
                    </asp:DropDownList><!-- OnSelectedIndexChanged="drpCountry_SelectedIndexChanged" -->
                </div>
                <div id="City"><asp:HiddenField ID="hdnCityDetail" runat="server" />
                    <asp:DropDownList ID="drpCity" CssClass="bigselect" runat="server" onchange="return onDropdownCitySelected();"
                        AutoPostBack="false">
                    </asp:DropDownList><!--OnSelectedIndexChanged="drpCity_SelectedIndexChanged"-->
                </div>
                <%--<div>
                <sliderControl:slider ID="slider" runat="server" /></div>--%>
                <div id="Date">
                    <div style="float: left; width: 145px;">
                        <asp:TextBox ID="txtBookingDate" runat="server" value="dd/mm/yy" class="input-white-mid"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtBookingDate"
                            PopupButtonID="calFrom" Format="dd/MM/yy">
                        </asp:CalendarExtender>
                        <img id="calFrom" src="<%=SiteRootPath %>Images/date-icon.png" class="datebutton" />
                    </div>
                    <div style="float: left; width: 20px; padding-top: 5px;">
                        <asp:Image ID="imgIcontext" runat="server" ImageUrl="~/Images/infoicon.png" CssClass="information" />
                    </div>
                </div>
                <div id="Quantity-day">
                    <div id="Quantity">
                        <label>
                            <%= GetKeyResult("DURATION")%>
                            &nbsp;</label><asp:DropDownList ID="drpDuration" runat="server" AutoPostBack="false"
                                OnSelectedIndexChanged="drpDuration_SelectedIndexChanged" onchange="ShowHideDay();">
                                <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                    </div>
                    <div id="daysname">
                        <%= GetKeyResult("DAY1")%></div>
                    <div id="Day">
                        <asp:DropDownList ID="drpDays" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="dayone" id="divDay2" runat="server" style="display: none;">
                        <div id="daysonename">
                            <%= GetKeyResult("DAY2")%></div>
                        <div id="Dayone">
                            <asp:DropDownList ID="drpDay2" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div id="Participants">
                    <label>
                        <%= GetKeyResult("MEETINGDELEGATES")%>
                        &nbsp;</label>
                    <asp:TextBox ID="txtParticipant" runat="server" CssClass="input-white-small" MaxLength="3"></asp:TextBox>
                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtParticipant"
                        FilterType="Numbers">
                    </asp:FilteredTextBoxExtender>
                </div>
            </div>
        </div>
        <div class="left-inner-top-bottom clearfix">
            <asp:Button ID="btnSearch" runat="server" class="find-button" OnClick="btnSerchOnMap_Click"
                OnClientClick="return searchclick();" />
        </div>
        <script language="javascript" type="text/javascript">
            function ClickOnSearch() {
                jQuery("#<%=btnSerchOnMap.ClientID %>").click();
                return false;
            }
        </script>
        <!--left-form ENDS HERE-->
    </div>
    <script language="javascript" type="text/javascript">
        function validate() {
            if (document.getElementById("<%=drpCountry.ClientID%>").value == 0) {
                alert(' <%= GetKeyResultForJavaScript("PLEASESELECTCOUNTRY")%>.');
                document.getElementById("<%=drpCountry.ClientID%>").focus();
                return false;
            }
            if (document.getElementById("<%=drpCity.ClientID%>").value == 0) {
                alert('<%= GetKeyResultForJavaScript("PLEASESELECTCITY")%>.');
                document.getElementById("<%=drpCity.ClientID%>").focus();
                return false;
            }
            if (document.getElementById("<%=txtBookingDate.ClientID%>").value == "dd/mm/yy") {
                alert('<%= GetKeyResultForJavaScript("PLEASEENTERDATE")%>.');
                document.getElementById("<%=txtBookingDate.ClientID%>").focus();
                return false;
            }
            var no = document.getElementById("<%=txtParticipant.ClientID%>").value;
            if (no == "" || no <= 0) {

                alert('<%= GetKeyResultForJavaScript("PLEASEENTERNUMBERPARTICIPANTS")%>.');
                document.getElementById("<%=txtParticipant.ClientID%>").focus();
                return false;
            }
            else if (no > 500) {
                alert('<%= GetKeyResultForJavaScript("PARTICIPANTSSHOULDNOTMORETHAN")%>.');
                document.getElementById("<%=txtParticipant.ClientID%>").focus();
                return false;

            }
        }
        function searchclick() {
            jQuery('#<%=hdnSearchButtonClick.ClientID %>').val('1');
            return true;
        }
        function IsCountryCitySelected() {
            if (jQuery("#<%=drpCountry.ClientID%>").val() == "0" || jQuery("#<%=drpCity.ClientID%>").val() == "0") {
                alert('<%= GetKeyResultForJavaScript("COUNTRYANDCITYBOTHAREREQUIRED")%>.');
                return false;
            }

            //            var no = document.getElementById("<%=txtParticipant.ClientID%>").value;
            //            if (no == "") {
            //                alert("Please enter number of participants.");
            //                document.getElementById("<%=txtParticipant.ClientID%>").focus();
            //                return false;
            //            }
        }    


    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cntLeftBottom" runat="Server">
    <div id="leftSection">
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cntTopNavigation" runat="Server">
    <uc1:TopMenu ID="tmFrontEnd" runat="server" ></uc1:TopMenu>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cntMainBody" runat="Server">
    <div class="mainbody">
        <div class="mainbody-left">
            <div class="banner">
                <div id="map_canvas" style="width: 460px; height: 300px; float: left; display: none;">
                </div>
                <asp:Image ID="imgMap" runat="server" Height="300px" Width="460px" ImageUrl="~/Images/faded-map.jpg"
                    Visible="false" />
            </div>
        </div>
        <div class="mainbody-right">
            <div id="DivRightPart" runat="server">
                <div id="beforeLogin" runat="server">
                    <div class="mainbody-right-call">
                        <div class="need">
                            <span>
                                <%= GetKeyResult("NEEDHELP")%></span>
                            <br />
                            <%= GetKeyResult("CALLUS")%>
                            5/7
                        </div>
                        <div class="phone">
                            +32 2 344 25 50
                        </div>
                        <p style="font-size: 10px; color: White; text-align: center">
                            <%= GetKeyResult("CALLUSANDWEDOITFORYOU")%></p>
                        <div class="mail">
                        <a href="http://needameetingroom.com/requestform/" onClick="javascript: _gaq.push(['_trackPageview', '/email']); window.open(this.href, 'popupwindow1','width=900,height=600,scrollbars,resizable,toolbar=NO'); &#13;&#10;return false;" Target="_blank"">
                        Send us your RFP</a>
                    </div>
                    </div>
                    <div class="mainbody-right-join" id="divjoinus" runat="server">
                        <div class="join">
                            <a runat="server" id="joinToday">
                                <%= GetKeyResult("JOINUSTODAY")%></a></div>
                        <%= GetKeyResult("FORHOTELSMEETINGFACILITIESEVENT")%>
                    </div>
                </div>
                <div style="width: 216px; height: auto; overflow: hidden; margin: 0px auto; display: none"
                    id="afterLogin" runat="server">
                    <div class="mainbody-right-call">
                        <div class="need">
                            <span>
                                <%= GetKeyResult("NEEDHELP")%></span>
                            <br />
                            <%= GetKeyResult("CALLUS")%>
                            5/7
                        </div>
                        <div class="phone">
                            +32 2 344 25 50
                        </div>
                        <p style="font-size: 10px; color: White; text-align: center">
                            CALL US and WE DO IT FOR YOU</p>
                        <div class="mail">
                        <a href="http://needameetingroom.com/requestform/" onClick="javascript: _gaq.push(['_trackPageview', '/email']); window.open(this.href, 'popupwindow1','width=900,height=600,scrollbars,resizable,toolbar=NO'); &#13;&#10;return false;" Target="_blank"">
                        Send us your RFP</a>
                    </div>
                    </div>
                    <!--start mainbody-right-afterlogin-body -->
                    <div class="mainbody-right-afterlogin-body">
                        <div class="mainbody-right-afterlogin-top">
                            <div class="mainbody-right-afterlogin-inner">
                                <b>
                                    <%= GetKeyResult("TODAY")%>
                                    :</b>
                                <asp:Label ID="lstLoginTime" runat="server"></asp:Label>
                                <div class="afterlogin-welcome">
                                    <span>
                                        <%= GetKeyResult("WELCOME")%></span>
                                    <br>
                                    <asp:Label ID="lblLoginUserName" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="mainbody-right-afterlogin-bottom">
                            <div class="mainbody-right-afterlogin-bottom-inner">
                                <asp:LinkButton ID="hypManageProfile" runat="server" OnClick="hypManageProfile_Click"><%= GetKeyResult("MANAGEPROFILE")%></asp:LinkButton>
                                <br />
                                <asp:LinkButton ID="hypChangepassword" runat="server" OnClick="hypChangepassword_Click"><%= GetKeyResult("CHANGEPASSWORD")%></asp:LinkButton><br />
                                <asp:LinkButton ID="hypListBookings" runat="server" OnClick="hypListBookings_Click"
                                    Visible="true">My bookings</asp:LinkButton><br />
                                <asp:LinkButton ID="hypListRequests" runat="server" OnClick="hypListRequests_Click"
                                    Visible="true">My requests</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <!--end mainbody-right-afterlogin-body -->
                </div>
                <uc7:WithoutNews ID="NewsSubscriber1" runat="server" />
            </div>
        </div>
        <asp:Button ID="btnSerchOnMap" runat="server" Text="Button" OnClick="btnSerchOnMap_Click"
            Style="display: none;" />
        <uc6:SearchResult ID="SearchResult1" runat="server" />
        <div id="divJoinToday-overlay">
        </div>
        <div id="divJoinToday">
            <div class="popup-top">
            </div>
            <div class="popup-mid">
                <div class="popup-mid-inner">
                    <table cellspacing="10" style="margin-left: 20px">
                        <tr>
                            <td>
                                Do you want Secondary Meetingroom ?
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RadioButton ID="rbYes" GroupName="checkout" runat="server" />
                                &nbsp;&nbsp; Yes i need for&nbsp;&nbsp;
                                <asp:TextBox ID="txtSecparticipants" runat="server" CssClass="input-white-small"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtSecparticipants"
                                    FilterType="Numbers">
                                </asp:FilteredTextBoxExtender>
                                &nbsp;&nbsp;People
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RadioButton ID="rbNo" GroupName="checkout" runat="server" Checked="true" />
                                &nbsp;&nbsp; No
                            </td>
                        </tr>
                    </table>
                    <div class="subscribe-btn1">
                        <div class="save-cancel-btn1 button_section">
                            <asp:LinkButton ID="btnContinue" runat="server" CssClass="send-request-btn">Continue</asp:LinkButton>
                            &nbsp;&nbsp; &nbsp;&nbsp; <a title="Close" class="Close-btn" href="#" onclick="document.getElementsByTagName('html')[0].style.overflow='auto';document.getElementById('divJoinToday').style.display='none';document.getElementById('divJoinToday-overlay').style.display='none';">
                                Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="popup-bottom">
            </div>
        </div>
    </div>
    <script type="text/javascript" language="javascript">

        //        function Navigate() {

        //            var url = 'Video.aspx';
        //            var winName = 'myWindow';
        //            var w = '500';
        //            var h = '300';
        //            var scroll = 'no';
        //            var popupWindow = null;
        //            LeftPosition = (screen.width) ? (screen.width - w) / 2 : 0;
        //            TopPosition = (screen.height) ? (screen.height - h) / 2 : 0;
        //            settings = 'height=' + h + ',width=' + w + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=no,directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no';
        //            popupWindow = window.open(url, winName, settings)
        //            return false;
        //        }


        function ShowHideDay() {

            var selectedDay = jQuery('#<%=drpDuration.ClientID %>').val();
            if (selectedDay == '2') {
                jQuery('#<%=drpDay2.ClientID %>').val('0');
                jQuery('#<%=divDay2.ClientID %>').show();

            }
            else {
                jQuery('#<%=divDay2.ClientID %>').hide();
            }

        }

        
    </script>
    <script type="text/javascript">
        var geocoder;
        var map;
        var markersArray = [];
        var marker;
        var address;
        var addressByUser;
        var image;
        var circleArray;
        var infowindow = new google.maps.InfoWindow();
        var locations = <%= HotelData %>;
        
        function OnDemand(CenterPoint, PlaceName,zoomLevel) {        
         jQuery('#map_canvas').show();
         jQuery('#mapMessgage').show();
            var cent = CenterPoint.split(',');
            map = new google.maps.Map(document.getElementById('map_canvas'), {
            zoom: parseFloat(zoomLevel),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scaleControl:true,
            scrollwheel:false
        });
            
            map.setCenter(new google.maps.LatLng(cent[0], cent[1]));
            
             image = new google.maps.MarkerImage('http://maps.google.com/mapfiles/ms/icons/yellow-dot.png');
//            marker = new google.maps.Marker({
//                map: map,
//                title: PlaceName,
//                position: new google.maps.LatLng(cent[0], cent[1]),
//                icon: image
//            });

            jQuery('#selectLatitude').html(cent[0]);
            jQuery('#selectLongitude').html(cent[1]);
            ShowHotelOfCity();
             
        }

        //This function used for show all hotel of the city.
        function ShowHotelOfCity() {        
            clearOverlays();
            
            markersArray.push(marker);
            for (i = 0; i < locations.length; i++) {
                
            //alert(locations);
                if (locations[i][4] == 'YES') {
                    image = new google.maps.MarkerImage('http://maps.google.com/mapfiles/ms/icons/red-dot.png');
                }
                else if(locations[i][4] == 'RE'){
                    image = new google.maps.MarkerImage('http://maps.google.com/mapfiles/ms/icons/blue-dot.png');
                }
                else if(locations[i][4] == 'NO'){
                    image = new google.maps.MarkerImage('http://maps.google.com/mapfiles/ms/icons/green-dot.png');
                }
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: image
                });
                google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                    return function () {
                       
                      var stars = null;
                      if(locations[i][6]!=null)
                      {

                      if(locations[i][6]=='0')
                      {
                      stars ='  N/A';
                      }
                     else if(locations[i][6]=='1')
                      {
                      stars ='<img src="<%=SiteRootPath %>Images/1.png" />';
                      }
                     else if(locations[i][6]=='2')
                      {
                      stars ='<img src="<%=SiteRootPath %>Images/2.png" />';
                      //stars ="Images/2.png";
                      }
                      else if(locations[i][6]=='3')
                      {
                      stars ='<img src="<%=SiteRootPath %>Images/3.png" />';
                      //stars ="Images/3.png";
                      }
                      else if(locations[i][6]=='4')
                      {
                      stars ='<img src="<%=SiteRootPath %>Images/4.png" />';
                      //stars ="Images/4.png";
                      }
                      else if(locations[i][6]=='5')
                      {
                      stars ='<img src="<%=SiteRootPath %>Images/5.png" />';
                      //stars ="Images/5.png";
                      }
                      else if(locations[i][6]=='6')
                      {
                      stars ='<img src="<%=SiteRootPath %>Images/6.png" />';
                      //stars ="Images/6.png";
                      }
                      else if(locations[i][6]=='7'){
                      stars ='<img src="<%=SiteRootPath %>Images/7.png" />';
                      //stars ="Images/7.png";
                      }
                      }
                      var htmlContent
                      if(locations[i][4] == 'YES')
                      {

                      var hotelName = locations[i][0];
                      hotelName = hotelName.replace(/~/g,"'");
                      var hotelAddress = locations[i][5];
                      hotelAddress = hotelAddress.replace(/~/g,"'");
                       htmlContent ='<table width="100%"><tr><td><b><%=GetKeyResultForJavaScript("VENUE")%> :</b> '+ hotelName + '</td></tr><tr><td><b><%=GetKeyResultForJavaScript("STARS")%> :</b>'+stars+'</td></tr><tr><td><b><%=GetKeyResultForJavaScript("ADDRESS")%> :</b> '+hotelAddress+'</td></tr><tr font><td><b><%=GetKeyResultForJavaScript("SPECIALDEALS")%> :</b> <span style="color:Red">€'+locations[i][8]+' pp/day</span> From €'+ locations[i][7]+'</td></tr></table>';

                      }
                      else
                      {
                      var hotelName = locations[i][0];
                      hotelName = hotelName.replace(/~/g,"'");
                      var hotelAddress = locations[i][5];
                      hotelAddress = hotelAddress.replace(/~/g,"'");
                      htmlContent ='<table width="100%"><tr><td><b><%=GetKeyResultForJavaScript("VENUE")%> :</b> '+ hotelName + '</td></tr><tr><td><b><%=GetKeyResultForJavaScript("STARS")%> :</b>'+stars+'</td></tr><tr><td><b><%=GetKeyResultForJavaScript("ADDRESS")%> :</b> '+ hotelAddress + '</td></tr></table>';
                     
                      }
                       infowindow.setContent(htmlContent);
                       infowindow.open(map, marker);

         }
                })(marker, i));
                
            }
            google.maps.event.addListener(map, 'click', function (event) {
                AddPoint(event.latLng);
            });
        }

        function clearOverlays() {
            if (markersArray) {
                for (i = 0; i < markersArray.length; i++) {
                    markersArray[i].setMap(null);
                }
                markersArray.length = 0;
            }

        }
       
    </script>
    <script language="javascript" type="text/javascript">
        var currentUrl = document.location.href;
        var pageName = currentUrl.split('/');
        var currentPage = pageName[currentUrl.split('/').length - 1];
        function MessageForconfirm() {
            if (currentPage == 'BookingStep1.aspx' || currentPage == 'BookingStep2.aspx' || currentPage == 'BookingStep3.aspx') {
                return confirm("You lost your current booking data");
            }
        }
        jQuery("#<%=txtBookingDate.ClientID %>").prop("readonly", "readonly");


        function HideMapImage() {
            jQuery('#map_canvas').hide();
        }
    </script>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cntMediaLink" runat="Server">
    <uc2:LinkMedia ID="LinkMedia1" runat="server" />
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cntFotter" runat="Server">
    <uc1:BottomLink ID="BottomLink1" runat="server" />
</asp:Content>
