﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Data;
using LMMR.Entities;
using System.Configuration;
public partial class SlideShow : System.Web.UI.Page
{
    PictureVideoManagementManager pvm = new PictureVideoManagementManager();
    HotelManager hm = new HotelManager();
    SuperAdminTaskManager manager = new SuperAdminTaskManager();
    public string imagesCollection
    {
        get;
        set;
    }
    public string SiteRootPath
    {
        get
        {
            string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string appRootUrl = HttpContext.Current.Request.ApplicationPath;
            return host + appRootUrl;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] != null)
        {
            Hotel h = hm.GetHotelDetailsById(Convert.ToInt64(Request.QueryString["id"]));
            //imgHotellogo.ImageUrl = ConfigurationManager.AppSettings["FilePath"] + "HotelImage/" + h.Logo;
            //lblHotelName.Text = h.Name;
            TList<HotelPhotoVideoGallary> p = pvm.GetAllHotelPictureVideo(Convert.ToInt32(Request.QueryString["id"]));
            imagesCollection = "";
            foreach (HotelPhotoVideoGallary hp in p)
            {
                string imagepath = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Split('~')[1] + "HotelImage/" + hp.ImageName;
                imagesCollection += "{image: '" + imagepath + "', title: '" + (hp.AlterText == null ? "" : hp.AlterText) + "', thumb: '" + imagepath + "', url: '' },";
            }
            if (imagesCollection.Length > 0)
            {
                imagesCollection = imagesCollection.Substring(0, imagesCollection.Length - 1);
            }
            InsertStatistics(Convert.ToInt32(Request.QueryString["id"]));
        }
        else if (Request.QueryString["Mid"] != null)
        {
            TList<MeetingRoomPictureVideo> p = pvm.GetPictureVideoByMeetingRoomID(Convert.ToInt32(Request.QueryString["Mid"]));
            imagesCollection = "";
            foreach (MeetingRoomPictureVideo hp in p)
            {
                string imagepath = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Split('~')[1] + "MeetingRoomImage/" + hp.ImageName;
                imagesCollection += "{image: '" + imagepath + "', title: '" + (hp.AlterText == null ? "" : hp.AlterText) + "', thumb: '" + imagepath + "', url: '' },";
            }
            if (imagesCollection.Length > 0)
            {
                imagesCollection = imagesCollection.Substring(0, imagesCollection.Length - 1);
            }
        }
        else
        {
            /* imagesCollection = @"{image: 'http://lmmr.essindia.biz/uploads/FrontImages/LMMR_Loop_1.jpg', title: '', thumb: 'http://lmmr.essindia.biz/uploads/FrontImages/LMMR_Loop_1.jpg', url: '' },
                                                         { image: 'http://lmmr.essindia.biz/uploads/FrontImages/LMMR_Loop_2.jpg', title: '', thumb: 'http://lmmr.essindia.biz/uploads/FrontImages/LMMR_Loop_2.jpg', url: '' },
                                                         { image: 'http://lmmr.essindia.biz/uploads/FrontImages/LMMR_Loop_3.jpg', title: '', thumb: 'http://lmmr.essindia.biz/uploads/FrontImages/LMMR_Loop_3.jpg', url: '' },
                                                         { image: 'http://lmmr.essindia.biz/uploads/FrontImages/LMMR_Loop_4.jpg', title: '', thumb: 'http://lmmr.essindia.biz/uploads/FrontImages/LMMR_Loop_4.jpg', url: '' },
                                                         { image: 'http://lmmr.essindia.biz/uploads/FrontImages/LMMR_Loop_5.jpg', title: '', thumb: 'http://lmmr.essindia.biz/uploads/FrontImages/LMMR_Loop_5.jpg', url: '' },
                                                         { image: 'http://lmmr.essindia.biz/uploads/FrontImages/LMMR_Loop_6.jpg', title: '', thumb: 'http://lmmr.essindia.biz/uploads/FrontImages/LMMR_Loop_6.jpg', url: '' },
                                                         { image: 'http://lmmr.essindia.biz/uploads/FrontImages/LMMR_Loop_7.jpg', title: '', thumb: 'http://lmmr.essindia.biz/uploads/FrontImages/LMMR_Loop_7.jpg', url: '' },
                                                         { image: 'http://lmmr.essindia.biz/uploads/FrontImages/LMMR_Loop_8.jpg', title: '', thumb: 'http://lmmr.essindia.biz/uploads/FrontImages/LMMR_Loop_8.jpg', url: '' },
                                                         { image: 'http://lmmr.essindia.biz/uploads/FrontImages/LMMR_Loop_9.jpg', title: '', thumb: 'http://lmmr.essindia.biz/uploads/FrontImages/LMMR_Loop_9.jpg', url: '' }";
            
             imagesCollection = @"{image: '" + SiteRootPath + "uploads/FrontImages/LMMR_Loop_1.jpg'" + ", title: '', thumb: '" + SiteRootPath + "uploads/FrontImages/LMMR_Loop_1.jpg'" + ", url: '' }," +
                                                         "{ image: '" + SiteRootPath + "uploads/FrontImages/LMMR_Loop_2.jpg'" + ", title: '', thumb: '" + SiteRootPath + "uploads/FrontImages/LMMR_Loop_2.jpg'" + ", url: '' }," +
                                                         "{ image: '" + SiteRootPath + "uploads/FrontImages/LMMR_Loop_3.jpg'" + ", title: '', thumb: '" + SiteRootPath + "uploads/FrontImages/LMMR_Loop_3.jpg'" + ", url: '' }," +
                                                         "{ image: '" + SiteRootPath + "uploads/FrontImages/LMMR_Loop_4.jpg'" + ", title: '', thumb: '" + SiteRootPath + "uploads/FrontImages/LMMR_Loop_4.jpg'" + ", url: '' }," +
                                                         "{ image: '" + SiteRootPath + "uploads/FrontImages/LMMR_Loop_5.jpg'" + ", title: '', thumb: '" + SiteRootPath + "uploads/FrontImages/LMMR_Loop_5.jpg'" + ", url: '' }," +
                                                         "{ image: '" + SiteRootPath + "uploads/FrontImages/LMMR_Loop_6.jpg'" + ", title: '', thumb: '" + SiteRootPath + "uploads/FrontImages/LMMR_Loop_6.jpg'" + ", url: '' }," +
                                                         "{ image: '" + SiteRootPath + "uploads/FrontImages/LMMR_Loop_7.jpg'" + ", title: '', thumb: '" + SiteRootPath + "uploads/FrontImages/LMMR_Loop_7.jpg'" + ", url: '' }," +
                                                         "{ image: '" + SiteRootPath + "uploads/FrontImages/LMMR_Loop_8.jpg'" + ", title: '', thumb: '" + SiteRootPath + "uploads/FrontImages/LMMR_Loop_8.jpg'" + ", url: '' }," +
                                                         "{ image: '" + SiteRootPath + "uploads/FrontImages/LMMR_Loop_9.jpg'" + ", title: '', thumb: '" + SiteRootPath + "uploads/FrontImages/LMMR_Loop_9.jpg'" + ", url: '' }";

             */
            Cms obj = new Cms();
            if (Session["CMS"] != null)
            {
                obj = (Session["CMS"] as TList<Cms>).Find(a => a.CmsType.ToLower() == "imagegalleryfrontend");
            }
            else
            {
                obj = manager.getCmsEntityOnType("ImageGalleryFrontEnd");
            }
            if (obj == null)
            {
                obj = manager.getCmsEntityOnType("ImageGalleryFrontEnd");
            }
            TList<CmsDescription> p = new TList<CmsDescription>();
            if (obj != null)
            {
                if (Session["CMSDESCRIPTION"] != null)
                {
                    p = (Session["CMSDESCRIPTION"] as TList<CmsDescription>).FindAll(a => a.CmsId == obj.Id && a.IsActive==true);
                }
                else
                {
                    p = manager.getActiveCmsDescriptionOnCmsID(obj.Id);// language is not being considered currently
                }
                if (p == null)
                {
                    p = manager.getActiveCmsDescriptionOnCmsID(obj.Id);// language is not being considered currently
                }
            }
            imagesCollection = "";
            if (p != null)
            {
                foreach (CmsDescription hp in p)
                {
                    string imagepath = SiteRootPath + ConfigurationManager.AppSettings["FilePath"].ToString().Split('~')[1] + "FrontImages/" + hp.ContentImage;
                    imagesCollection += "{image: '" + imagepath + "', title: '" + (hp.ContentTitle == null ? "" : hp.ContentTitle) + "', thumb: '" + imagepath + "',url: '" + hp.PageUrl + "' },";
                }
                if (imagesCollection.Length > 0)
                {
                    imagesCollection = imagesCollection.Substring(0, imagesCollection.Length - 1);
                }
            }
            
														
        }
    }


    #region statistics
    public void InsertStatistics(int HotelId)
    {
        Viewstatistics objview = new Viewstatistics();
        Statistics ST = new Statistics();
        ST.HotelId = HotelId;
        ST.Views = 1;
        ST.StatDate = System.DateTime.Now;
        objview.InsertVisitwithBookingid(ST);
    }
    #endregion
}