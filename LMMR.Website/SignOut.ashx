﻿<%@ WebHandler Language="C#" Class="Ping" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public class Ping : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";

        context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        context.Response.Cache.SetExpires(DateTime.Now.AddMinutes(-1));

        context.Session.Abandon();
        context.Session.Clear();
        context.Response.Cookies.Clear();
        FormsAuthentication.SignOut();
        //context.Response.Redirect("~/UserLogin.aspx");
        //FormsAuthentication.RedirectToLoginPage();
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}