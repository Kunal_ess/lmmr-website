﻿#region Using Directives
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using LMMR.Business;
using LMMR.Entities;
using LMMR.Data;
using log4net;
using log4net.Config;
using System.IO;
using System.IO;
using System.Linq;
#endregion


public partial class Default2 : BasePage
{
    SendMails objSendmail = new SendMails();
    ManageCMSContent obj = new ManageCMSContent();
    EmailConfigManager em = new EmailConfigManager();

    #region PageLoad
    /// <summary>
    /// This method is call on the Page load and Initlize all the components of the page to its initial stage.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CurrentRestUserID"] != null)
        {
            DisabledbtnCompanyRegistrationDiv.Visible = true;
            disabledbtnUserRegistrationDiv.Visible = true;
            NormalbtnCompanyRegistrationDiv.Visible = false;
            normalbtnUserRegistrationDiv.Visible = false;
        }
        else
        {
            NormalbtnCompanyRegistrationDiv.Visible = true;
            normalbtnUserRegistrationDiv.Visible = true;
            DisabledbtnCompanyRegistrationDiv.Visible = false;
            disabledbtnUserRegistrationDiv.Visible = false;
        }
        if (Session["CurrentRestUser"] != null)
        {
            Users objUsers = (Users)Session["CurrentRestUser"];
            lblLoginUserName.Text = objUsers.FirstName + " " + objUsers.LastName;
            objUsers.LastLogin = DateTime.Now;
            lstLoginTime.Text = DateTime.Now.ToLongDateString();
            AfterLogin(objUsers.UserId, Convert.ToInt32(objUsers.Usertype));
        }

        if (Session["status"] != null)
        {
            Page.RegisterStartupScript("msg", "<script language='javascript'>alert(" + "'" + Session["status"] + "'" + ");</script>");
            Session["status"] = null;
        }
        if (Session["passwordChangeStatus"] != null)
        {
            Page.RegisterStartupScript("msg", "<script language='javascript'>alert(" + "'" + Session["passwordChangeStatus"] + "'" + ");</script>");
            Session["passwordChangeStatus"] = null;
        }

        if (!Page.IsPostBack)
        {
            GetJoinTodayData();
            GetJoinTodayDataforUsers();
            BindURLs();
        }

    }

    public void BindURLs()
    {
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {          
            joinToday.HRef = SiteRootPath + "join-today/" + l.Name.ToLower();
        }
        else
        {           
            joinToday.HRef = SiteRootPath + "join-today/english";
        }
    }

    #endregion

    #region event

    /// <summary>
    /// This event is used for open the registration page as Agency.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRegisterAsAgency_Click(object sender, EventArgs e)
    {

        Session["task"] = "register";
        Session["registerType"] = Convert.ToInt32(Usertype.Agency);
        //Response.Redirect("Registration.aspx");
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "registration-as-agency/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "registration-as-agency/english");
        }
    }
    /// <summary>
    /// This event is used for open the registration page as User.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnUserRegistration_Click(object sender, EventArgs e)
    {
        Session["task"] = "register";
        Session["registerType"] = Convert.ToInt32(Usertype.privateuser);
        //Response.Redirect("Registration.aspx");
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "registration/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "registration/english");
        }
    }
    /// <summary>
    /// This event is used for open the registration page as Company.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCompanyRegistration_Click(object sender, EventArgs e)
    {
        Session["task"] = "register";
        Session["registerType"] = Convert.ToInt32(Usertype.Company);
        //Response.Redirect("Registration.aspx");
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "registration-as-company/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "registration-as-company/english");
        }
    }
    /// <summary>
    /// This event is used for sending the mail to user and to super admin and operator for confirming.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSend_Click(object sender, EventArgs e)
    {
        EmailConfig eConfig = em.GetByName("Join us today");
        EmailValueCollection objEmailValues = new EmailValueCollection();
        if (eConfig.IsActive)
        {
            objSendmail.FromEmail = ConfigurationManager.AppSettings["HotelSupportEmailID"];

            //Mail to User
            objSendmail.ToEmail = txtEmail.Text;
            //string bodymsg = File.ReadAllText(Server.MapPath("~/EmailTemplets/JoinToday.html"));
            string bodymsg = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
            EmailConfigMapping emap2 = new EmailConfigMapping();
            emap2 = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
            bodymsg = bodymsg.Replace("@CONTENTS", emap2.EmailContents);
            objSendmail.Subject = "Request for joinToday!";
            foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForJoinToday(txtContactPerson.Text, txtTitle.Text.Trim()))
            {
                bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
            }
            objSendmail.Body = bodymsg;
            objSendmail.SendMail();
        }
        eConfig = em.GetByName("Join us today for operator");
        if (eConfig.IsActive)
        {
            //Send mail to the operator.
            string bodymsg = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
            EmailConfigMapping emap2 = new EmailConfigMapping();
            emap2 = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
            bodymsg = bodymsg.Replace("@CONTENTS", emap2.EmailContents);
            objSendmail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
            objSendmail.Bcc = ConfigurationManager.AppSettings["WatchEMailID"];
            Users objoperator = new HotelManager().GetFirstOperatorInDataBase();
            if (objoperator != null)
            {
                objSendmail.ToEmail = objoperator.EmailId;
            }
            //string bodymsgOperator = File.ReadAllText(Server.MapPath("~/EmailTemplets/JoinTodayForOperatorSuperAdmin.html"));
            EmailValueCollection objEmailValuesOperator = new EmailValueCollection();
            Users objUser = (Users)Session["CurrentUser"];
            objSendmail.Subject = "Request for join today";
            foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailToOperatorForJointoday("Operator", txtHotelName.Text, txtEmail.Text, txtContactPerson.Text, txtphoneNo.Text, txtNoMeetingRoom.Text))
            {
                bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
            }
            objSendmail.Body = bodymsg;
            objSendmail.SendMail();
        }
        //Mail to superadmin
        eConfig = em.GetByName("Join us today for operator");
        if (eConfig.IsActive)
        {
            Users objSuperAdmin = new HotelManager().GetFirstSuperAdminInDataBase();
            //string bodymsgSuperAdmin = File.ReadAllText(Server.MapPath("~/EmailTemplets/JoinTodayForOperatorSuperAdmin.html"));
            string bodymsg = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
            EmailConfigMapping emap2 = new EmailConfigMapping();
            emap2 = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
            bodymsg = bodymsg.Replace("@CONTENTS", emap2.EmailContents);
            objSendmail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
            objSendmail.ToEmail = objSendmail.ToEmail = objSuperAdmin.EmailId;
            objSendmail.Subject = "Request for join today";
            foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailToOperatorForJointoday("Super Admin", txtHotelName.Text, txtEmail.Text, txtTitle.Text + " " + txtContactPerson.Text, txtphoneNo.Text, txtNoMeetingRoom.Text))
            {
                bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
            }
            objSendmail.Body = bodymsg;
            objSendmail.SendMail();
        }
        //Page.RegisterStartupScript("aa", "<script language='javascript'>alert('"+ GetKeyResult("REGISTERVENUESUCCESS1")+"<br/>"+GetKeyResult("REGISTERVENUESUCCESS2")+"<br/><br/>"+GetKeyResult("REGISTERVENUESUCCESS3")+"');</script>");

        //Response.Redirect("RegisterOK-Venue.aspx");
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "registerok-venue/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "registerok-venue/english");
        }
        //Page.RegisterStartupScript("aa", "<script language='javascript'>alert('" + GetKeyResult("REGISTERVENUESUCCESS") + "');</script>");
        Page.RegisterStartupScript("aa", "<script language='javascript'>alert('" + GetKeyResultForJavaScript("REGISTERVENUESUCCESS") + "');</script>");
        clearPopUpForm();

    }
    #endregion

    #region function

    void clearPopUpForm()
    {
        txtHotelName.Text = "";
        txtTitle.Text = "";
        txtContactPerson.Text = "";
        txtEmail.Text = "";
        txtphoneNo.Text = "";
        txtNoMeetingRoom.Text = "";
    }

    public void AfterLogin(long userID, int userType)
    {
        beforeLogin.Style.Add("display", "none");
        afterLogin.Style.Add("display", "block");
        Session["registerType"] = userType;
        //hypManageProfile.NavigateUrl = "Registration.aspx";
    }


    /// <summary>
    /// This method return the value in Getresult function
    /// </summary>
    /// <param name="Key"></param>
    /// <returns></returns>
    public string GetKeyResult(string key)
    {
        return System.Net.WebUtility.HtmlDecode(ResultManager.GetResult(Convert.ToInt64(Session["LanguageID"]), key));
    }

    /// <summary>
    /// This method retreives the content description based on cms id=12 from CmsDescription table 
    /// </summary>
    public void GetJoinTodayData()
    {
        int type = 12;
        TList<CmsDescription> getDesc = obj.GetCMSContent(type, Convert.ToInt64(Session["LanguageID"]));
        if (getDesc.Count > 0)
        {
            lblpartener.Text = getDesc[0].ContentsDesc;

        }
    }

    /// <summary>
    /// This method retreives the content description based on cms id=18 from CmsDescription table 
    /// </summary>
    public void GetJoinTodayDataforUsers()
    {
        int type = 18;
        TList<CmsDescription> getDesc = obj.GetCMSContent(type, Convert.ToInt64(Session["LanguageID"]));
        if (getDesc.Count > 0)
        {
            lblUsers.Text = getDesc[0].ContentsDesc;

        }
    }

    protected void hypManageProfile_Click(object sender, EventArgs e)
    {
        Session["task"] = "Edit";
        //Response.Redirect("Registration.aspx");
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "manage-profile/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "manage-profile/english");
        }
    }
    protected void hypChangepassword_Click(object sender, EventArgs e)
    {
        //Response.Redirect("ChangePassword.aspx");
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "change-password/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "change-password/english");
        }
    }

    protected void hypListBookings_Click(object sender, EventArgs e)
    {
        //Response.Redirect("ListBookings.aspx");
        //Language l = new LanguageManager().GetLanguageByID(Convert.ToInt64(Session["LanguageID"]));
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "list-bookings/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "list-bookings/english");
        }
    }

    protected void hypListRequests_Click(object sender, EventArgs e)
    {
        if (l != null)
        {
            Response.Redirect(SiteRootPath + "list-requests/" + l.Name.ToLower());
        }
        else
        {
            Response.Redirect(SiteRootPath + "list-requests/english");
        }
    }

    #endregion
}