﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RegisterMaster.master" AutoEventWireup="true"
    CodeFile="Registration.aspx.cs" Inherits="Registration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<script type="text/javascript">
    function disableBackButton() {
        window.history.forward();
    }
    disableBackButton();
    window.onload = new function () { disableBackButton() }
    window.onpageshow = new function (evt) { if (evt != undefined) { if (evt.persisted) disableBackButton(); } }
    window.onunload = new function () { void (0) }
    </script>
    <div class="register-body">
       <h1>
            <asp:Label ID="lblHeading" runat="server"></asp:Label>
            </h1>
            <asp:Label ID="lblPrivateUser" runat="server" Visible="false"><%= GetKeyResult("PRIVATEUSERMESSAGE") %></asp:Label><br />
            <asp:Label ID="lblPrivateUserLine2" runat="server" Visible="false"><%= GetKeyResult("PRIVATEUSERMESSAGECONTINUE") %></asp:Label>
            <asp:Label ID="lblCompanyMessage" runat="server" Visible="false"><%= GetKeyResult("COMPANYMESSAGE") %></asp:Label>
        <asp:UpdateProgress ID="uprog" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
            <ProgressTemplate>
                <div id="Loding_overlay" style="display: block;">
                    <img src="Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
                    Loading...</div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
              
                <div id="divmessage" runat="server" class="error">
                </div>
                <div class="register-mainbody">
                    <div class="register-left">
                        <fieldset class="reg">
                            <legend>
                                <asp:Label ID="lblPersonalData" runat="server"><%= GetKeyResult("PERSONALDATA") %></asp:Label></legend>
                            <div id="agencySpecificSpan" runat="server">
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblCompany" runat="server"><%= GetKeyResult("COMPANY")%></asp:Label>
                                        <span style="color: Red">*</span></div>
                                    <div class="res-input">
                                        <asp:TextBox ID="companyName" class="inputregister" runat="server" MaxLength="50"></asp:TextBox>
                                                                           </div>
                                </div>
                               
                                
                            </div>
                            <div class="register-row clearfix">
                                <div class="res-level">
                                    <asp:Label ID="lblFirstName" runat="server"><%= GetKeyResult("FIRSTNAME")%></asp:Label>
                                    <span style="color: Red">*</span></div>
                                <div class="res-input">
                                    <asp:TextBox ID="firstName" class="inputregister" runat="server" MaxLength="50"></asp:TextBox>
                                                                   </div>
                            </div>
                            <div class="register-row clearfix">
                                <div class="res-level">
                                    <asp:Label ID="lblLastName" runat="server"><%= GetKeyResult("LASTNAME")%></asp:Label>
                                    <span style="color: Red">*</span></div>
                                <div class="res-input">
                                    <asp:TextBox ID="lastName" runat="server" class="inputregister" MaxLength="50"></asp:TextBox>
                                   
                                </div>
                            </div>
                            <div id="companySpecificSpan" runat="server">
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblCompany2" runat="server"><%= GetKeyResult("COMPANY")%></asp:Label>
                                        <span style="color: Red">*</span></div>
                                    <div class="res-input">
                                        <asp:TextBox ID="company" class="inputregister" runat="server" MaxLength="50"></asp:TextBox>                                        
                                    </div>
                                </div>
                            </div>
                            <div class="register-row clearfix">
                                <div class="res-level">
                                    <asp:Label ID="lblEmail" runat="server"><%= GetKeyResult("EMAILONLY")%></asp:Label>
                                    <span style="color: Red">*</span>
                                </div>
                                <div class="res-input">
                                    <asp:TextBox ID="email" runat="server" class="inputregister" MaxLength="50" onkeyup="return ChangeValue(this);"></asp:TextBox>                                    
                                </div>
                            </div>
                            <div id="reEnterEmailDiv" runat="server">
                            <div class="register-row clearfix">
                                <div class="res-level">
                                    <asp:Label ID="lblReEnterEmail" runat="server"><%= GetKeyResult("REENTEREMAIL")%></asp:Label>
                                    <span style="color: Red">*</span></div>
                                <div class="res-input">
                                    <asp:TextBox ID="emailAgain" runat="server" class="inputregister" MaxLength="50"></asp:TextBox>
                                   
                                </div>
                            </div>
                            </div>
                            <div class="register-row clearfix">
                                <div class="res-level">
                                    <asp:Label ID="lblCountry" runat="server"><%= GetKeyResult("COUNTRY")%></asp:Label>
                                    <span style="color: Red">*</span></div>
                                <div class="res-input" >
                                    <div id="countryListSpan">
                                        <asp:DropDownList ID="countryList" runat="server" OnSelectedIndexChanged="countryList_SelectedIndexChanged"
                                             Width="172px" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="register-row clearfix" id="generalCityDropDownDiv" runat="server" visible="false">
                                <div class="res-level">
                                    <asp:Label ID="lblCity" runat="server"><%= GetKeyResult("CITY")%></asp:Label>
                                    <span style="color: Red">*</span></div>
                                <div class="res-input">
                                    <div id="cityListSpan">
                                        <asp:DropDownList ID="cityList" runat="server" Width="172px">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <%--new markup for city textbox--%>
                            <div class="register-row clearfix">
                                <div class="res-level">
                                    <asp:Label ID="lblCityNew" runat="server"><%= GetKeyResult("CITY")%></asp:Label>
                                    <span style="color: Red">*</span></div>
                                <div class="res-input">
                                    <div id="Div2">
                                        <asp:TextBox ID="txtCity" runat="server" MaxLength="15" CssClass="inputregister">
                                        </asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="register-row clearfix">
                                <div class="res-level">
                                    <asp:Label ID="lblAddress" runat="server"><%= GetKeyResult("ADDRESS")%></asp:Label>
                                    <span style="color: Red">*</span></div>
                                <div class="res-input">
                                    <asp:TextBox ID="address" runat="server" onkeypress="checklength();" TextMode="MultiLine" class="textareareg"
                                        oncopy="return false" onpaste="return false"></asp:TextBox>
                                    
                                    <div class="sep">
                                    </div>
                                </div>
                            </div>
                            <div class="register-row1 clearfix">
                                <div class="res-level">
                                    <asp:Label ID="lblPhone" runat="server"><%= GetKeyResult("PHONE")%></asp:Label>
                                    <span style="color: Red">*</span></div>
                                <div class="res-input">
                                    <table width="100%" border="0">
                                        <tr>
                                            <td style="width: 80px">
                                                <div id="countryCodeList1Span">
                                                    <asp:DropDownList ID="countryCodeList1" runat="server">
                                                        <asp:ListItem>1</asp:ListItem>
                                                        <asp:ListItem>7</asp:ListItem>
                                                        <asp:ListItem>20</asp:ListItem>
                                                        <asp:ListItem>27</asp:ListItem>
                                                        <asp:ListItem>30</asp:ListItem>
                                                        <asp:ListItem>31</asp:ListItem>
                                                        <asp:ListItem>32</asp:ListItem>
                                                        <asp:ListItem>33</asp:ListItem>
                                                        <asp:ListItem>34</asp:ListItem>
                                                        <asp:ListItem>36</asp:ListItem>
                                                        <asp:ListItem>39</asp:ListItem>
                                                        <asp:ListItem>40</asp:ListItem>
                                                        <asp:ListItem>41</asp:ListItem>
                                                        <asp:ListItem>43</asp:ListItem>
                                                        <asp:ListItem>44</asp:ListItem>
                                                        <asp:ListItem>45</asp:ListItem>
                                                        <asp:ListItem>46</asp:ListItem>
                                                        <asp:ListItem>47</asp:ListItem>
                                                        <asp:ListItem>48</asp:ListItem>
                                                        <asp:ListItem>49</asp:ListItem>
                                                        <asp:ListItem>51</asp:ListItem>
                                                        <asp:ListItem>52</asp:ListItem>
                                                        <asp:ListItem>53</asp:ListItem>
                                                        <asp:ListItem>54</asp:ListItem>
                                                        <asp:ListItem>55</asp:ListItem>
                                                        <asp:ListItem>56</asp:ListItem>
                                                        <asp:ListItem>57</asp:ListItem>
                                                        <asp:ListItem>58</asp:ListItem>
                                                        <asp:ListItem>60</asp:ListItem>
                                                        <asp:ListItem>61</asp:ListItem>
                                                        <asp:ListItem>62</asp:ListItem>
                                                        <asp:ListItem>63</asp:ListItem>
                                                        <asp:ListItem>64</asp:ListItem>
                                                        <asp:ListItem>65</asp:ListItem>
                                                        <asp:ListItem>66</asp:ListItem>
                                                        <asp:ListItem>81</asp:ListItem>
                                                        <asp:ListItem>82</asp:ListItem>
                                                        <asp:ListItem>84</asp:ListItem>
                                                        <asp:ListItem>86</asp:ListItem>
                                                        <asp:ListItem>90</asp:ListItem>
                                                        <asp:ListItem>91</asp:ListItem>
                                                        <asp:ListItem>92</asp:ListItem>
                                                        <asp:ListItem>93</asp:ListItem>
                                                        <asp:ListItem>94</asp:ListItem>
                                                        <asp:ListItem>95</asp:ListItem>
                                                        <asp:ListItem>98</asp:ListItem>
                                                        <asp:ListItem>212</asp:ListItem>
                                                        <asp:ListItem>213</asp:ListItem>
                                                        <asp:ListItem>216</asp:ListItem>
                                                        <asp:ListItem>218</asp:ListItem>
                                                        <asp:ListItem>220</asp:ListItem>
                                                        <asp:ListItem>221</asp:ListItem>
                                                        <asp:ListItem>222</asp:ListItem>
                                                        <asp:ListItem>223</asp:ListItem>
                                                        <asp:ListItem>224</asp:ListItem>
                                                        <asp:ListItem>225</asp:ListItem>
                                                        <asp:ListItem>226</asp:ListItem>
                                                        <asp:ListItem>227</asp:ListItem>
                                                        <asp:ListItem>228</asp:ListItem>
                                                        <asp:ListItem>229</asp:ListItem>
                                                        <asp:ListItem>230</asp:ListItem>
                                                        <asp:ListItem>231</asp:ListItem>
                                                        <asp:ListItem>232</asp:ListItem>
                                                        <asp:ListItem>233</asp:ListItem>
                                                        <asp:ListItem>234</asp:ListItem>
                                                        <asp:ListItem>235</asp:ListItem>
                                                        <asp:ListItem>236</asp:ListItem>
                                                        <asp:ListItem>237</asp:ListItem>
                                                        <asp:ListItem>238</asp:ListItem>
                                                        <asp:ListItem>239</asp:ListItem>
                                                        <asp:ListItem>240</asp:ListItem>
                                                        <asp:ListItem>241</asp:ListItem>
                                                        <asp:ListItem>242</asp:ListItem>
                                                        <asp:ListItem>243</asp:ListItem>
                                                        <asp:ListItem>244</asp:ListItem>
                                                        <asp:ListItem>245</asp:ListItem>
                                                        <asp:ListItem>248</asp:ListItem>
                                                        <asp:ListItem>249</asp:ListItem>
                                                        <asp:ListItem>250</asp:ListItem>
                                                        <asp:ListItem>251</asp:ListItem>
                                                        <asp:ListItem>252</asp:ListItem>
                                                        <asp:ListItem>253</asp:ListItem>
                                                        <asp:ListItem>254</asp:ListItem>
                                                        <asp:ListItem>255</asp:ListItem>
                                                        <asp:ListItem>256</asp:ListItem>
                                                        <asp:ListItem>257</asp:ListItem>
                                                        <asp:ListItem>258</asp:ListItem>
                                                        <asp:ListItem>260</asp:ListItem>
                                                        <asp:ListItem>261</asp:ListItem>
                                                        <asp:ListItem>262</asp:ListItem>
                                                        <asp:ListItem>263</asp:ListItem>
                                                        <asp:ListItem>264</asp:ListItem>
                                                        <asp:ListItem>265</asp:ListItem>
                                                        <asp:ListItem>266</asp:ListItem>
                                                        <asp:ListItem>267</asp:ListItem>
                                                        <asp:ListItem>268</asp:ListItem>
                                                        <asp:ListItem>269</asp:ListItem>
                                                        <asp:ListItem>290</asp:ListItem>
                                                        <asp:ListItem>291</asp:ListItem>
                                                        <asp:ListItem>297</asp:ListItem>
                                                        <asp:ListItem>298</asp:ListItem>
                                                        <asp:ListItem>299</asp:ListItem>
                                                        <asp:ListItem>350</asp:ListItem>
                                                        <asp:ListItem>351</asp:ListItem>
                                                        <asp:ListItem>352</asp:ListItem>
                                                        <asp:ListItem>353</asp:ListItem>
                                                        <asp:ListItem>354</asp:ListItem>
                                                        <asp:ListItem>355</asp:ListItem>
                                                        <asp:ListItem>356</asp:ListItem>
                                                        <asp:ListItem>357</asp:ListItem>
                                                        <asp:ListItem>358</asp:ListItem>
                                                        <asp:ListItem>359</asp:ListItem>
                                                        <asp:ListItem>370</asp:ListItem>
                                                        <asp:ListItem>371</asp:ListItem>
                                                        <asp:ListItem>372</asp:ListItem>
                                                        <asp:ListItem>373</asp:ListItem>
                                                        <asp:ListItem>374</asp:ListItem>
                                                        <asp:ListItem>375</asp:ListItem>
                                                        <asp:ListItem>376</asp:ListItem>
                                                        <asp:ListItem>377</asp:ListItem>
                                                        <asp:ListItem>378</asp:ListItem>
                                                        <asp:ListItem>380</asp:ListItem>
                                                        <asp:ListItem>381</asp:ListItem>
                                                        <asp:ListItem>382</asp:ListItem>
                                                        <asp:ListItem>385</asp:ListItem>
                                                        <asp:ListItem>386</asp:ListItem>
                                                        <asp:ListItem>387</asp:ListItem>
                                                        <asp:ListItem>389</asp:ListItem>
                                                        <asp:ListItem>420</asp:ListItem>
                                                        <asp:ListItem>421</asp:ListItem>
                                                        <asp:ListItem>423</asp:ListItem>
                                                        <asp:ListItem>500</asp:ListItem>
                                                        <asp:ListItem>501</asp:ListItem>
                                                        <asp:ListItem>502</asp:ListItem>
                                                        <asp:ListItem>503</asp:ListItem>
                                                        <asp:ListItem>504</asp:ListItem>
                                                        <asp:ListItem>505</asp:ListItem>
                                                        <asp:ListItem>506</asp:ListItem>
                                                        <asp:ListItem>507</asp:ListItem>
                                                        <asp:ListItem>508</asp:ListItem>
                                                        <asp:ListItem>509</asp:ListItem>
                                                        <asp:ListItem>590</asp:ListItem>
                                                        <asp:ListItem>591</asp:ListItem>
                                                        <asp:ListItem>592</asp:ListItem>
                                                        <asp:ListItem>593</asp:ListItem>
                                                        <asp:ListItem>595</asp:ListItem>
                                                        <asp:ListItem>597</asp:ListItem>
                                                        <asp:ListItem>598</asp:ListItem>
                                                        <asp:ListItem>599</asp:ListItem>
                                                        <asp:ListItem>670</asp:ListItem>
                                                        <asp:ListItem>672</asp:ListItem>
                                                        <asp:ListItem>673</asp:ListItem>
                                                        <asp:ListItem>674</asp:ListItem>
                                                        <asp:ListItem>675</asp:ListItem>
                                                        <asp:ListItem>676</asp:ListItem>
                                                        <asp:ListItem>677</asp:ListItem>
                                                        <asp:ListItem>678</asp:ListItem>
                                                        <asp:ListItem>679</asp:ListItem>
                                                        <asp:ListItem>680</asp:ListItem>
                                                        <asp:ListItem>681</asp:ListItem>
                                                        <asp:ListItem>682</asp:ListItem>
                                                        <asp:ListItem>683</asp:ListItem>
                                                        <asp:ListItem>685</asp:ListItem>
                                                        <asp:ListItem>686</asp:ListItem>
                                                        <asp:ListItem>687</asp:ListItem>
                                                        <asp:ListItem>688</asp:ListItem>
                                                        <asp:ListItem>689</asp:ListItem>
                                                        <asp:ListItem>690</asp:ListItem>
                                                        <asp:ListItem>691</asp:ListItem>
                                                        <asp:ListItem>692</asp:ListItem>
                                                        <asp:ListItem>850</asp:ListItem>
                                                        <asp:ListItem>852</asp:ListItem>
                                                        <asp:ListItem>853</asp:ListItem>
                                                        <asp:ListItem>855</asp:ListItem>
                                                        <asp:ListItem>856</asp:ListItem>
                                                        <asp:ListItem>870</asp:ListItem>
                                                        <asp:ListItem>880</asp:ListItem>
                                                        <asp:ListItem>886</asp:ListItem>
                                                        <asp:ListItem>960</asp:ListItem>
                                                        <asp:ListItem>961</asp:ListItem>
                                                        <asp:ListItem>962</asp:ListItem>
                                                        <asp:ListItem>963</asp:ListItem>
                                                        <asp:ListItem>964</asp:ListItem>
                                                        <asp:ListItem>965</asp:ListItem>
                                                        <asp:ListItem>966</asp:ListItem>
                                                        <asp:ListItem>967</asp:ListItem>
                                                        <asp:ListItem>968</asp:ListItem>
                                                        <asp:ListItem>970</asp:ListItem>
                                                        <asp:ListItem>971</asp:ListItem>
                                                        <asp:ListItem>972</asp:ListItem>
                                                        <asp:ListItem>973</asp:ListItem>
                                                        <asp:ListItem>974</asp:ListItem>
                                                        <asp:ListItem>975</asp:ListItem>
                                                        <asp:ListItem>976</asp:ListItem>
                                                        <asp:ListItem>977</asp:ListItem>
                                                        <asp:ListItem>992</asp:ListItem>
                                                        <asp:ListItem>993</asp:ListItem>
                                                        <asp:ListItem>994</asp:ListItem>
                                                        <asp:ListItem>995</asp:ListItem>
                                                        <asp:ListItem>996</asp:ListItem>
                                                        <asp:ListItem>998</asp:ListItem>
                                                        <asp:ListItem>1242</asp:ListItem>
                                                        <asp:ListItem>1246</asp:ListItem>
                                                        <asp:ListItem>1264</asp:ListItem>
                                                        <asp:ListItem>1268</asp:ListItem>
                                                        <asp:ListItem>1284</asp:ListItem>
                                                        <asp:ListItem>1340</asp:ListItem>
                                                        <asp:ListItem>1345</asp:ListItem>
                                                        <asp:ListItem>1441</asp:ListItem>
                                                        <asp:ListItem>1473</asp:ListItem>
                                                        <asp:ListItem>1599</asp:ListItem>
                                                        <asp:ListItem>1649</asp:ListItem>
                                                        <asp:ListItem>1664</asp:ListItem>
                                                        <asp:ListItem>1670</asp:ListItem>
                                                        <asp:ListItem>1671</asp:ListItem>
                                                        <asp:ListItem>1684</asp:ListItem>
                                                        <asp:ListItem>1758</asp:ListItem>
                                                        <asp:ListItem>1767</asp:ListItem>
                                                        <asp:ListItem>1784</asp:ListItem>
                                                        <asp:ListItem>1809</asp:ListItem>
                                                        <asp:ListItem>1868</asp:ListItem>
                                                        <asp:ListItem>1869</asp:ListItem>
                                                        <asp:ListItem>1876</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </td>
                                            <td style="padding-left: 0px;">
                                                <asp:TextBox ID="phone" runat="server" class="inputregister2" MaxLength="15"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="phone"
                                                    FilterType="Numbers">
                                                </asp:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="register-row clearfix">
                                <div class="res-level">
                                    <asp:Label ID="lblPostalCode" runat="server"><%= GetKeyResult("POSTALCODE")%></asp:Label>
                                    <span style="color: Red">*</span></div>
                                <div class="res-input">
                                    <asp:TextBox ID="postalCode" runat="server" class="inputregister2" MaxLength="10"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="postalCode"
                                        FilterType="Numbers">
                                    </asp:FilteredTextBoxExtender>
                                </div>
                            </div>
                            <div id="agencySpecificSpan2" runat="server">
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblPreferredLang" runat="server"><%= GetKeyResult("PREFERREDLANG")%></asp:Label>
                                        <span style="color: Red"></span></div>
                                    <div class="res-input">
                                        <div id="languageListSpan">
                                            <asp:DropDownList ID="languageList" runat="server" Width="172px">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="loginDiv" runat="server">
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblLogin" runat="server"><%= GetKeyResult("LOGIN")%></asp:Label>
                                    </div>
                                    <div class="res-input" style="margin-top: 5px;">
                                        <asp:Label ID="Login" runat="server" Font-Bold="true"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div id="passwordDiv" runat="server">
                            <div class="register-row clearfix">
                                <div class="res-level">
                                    <asp:Label ID="lblPassword" runat="server"><%= GetKeyResult("PASSWORD")%></asp:Label>
                                    <span style="color: Red">*</span></div>
                                <div class="res-input">
                                    <asp:TextBox ID="password" runat="server" TextMode="Password" CssClass="inputregister"
                                        MaxLength="20"></asp:TextBox><br />
                                         <span  style="font-family: Arial; font-size: Smaller;">
                                         <%= GetKeyResult("PASSWORDMUSTHAVE")%></span>
                                </div>
                            </div>
                            <div class="register-row clearfix">
                                <div class="res-level">
                                    <asp:Label ID="lblReType" runat="server"><%= GetKeyResult("RETYPEPWD")%></asp:Label>
                                    <span style="color: Red">*</span></div>
                                <div class="res-input">
                                    <asp:TextBox ID="retype" runat="server" class="inputregister" MaxLength="20" TextMode="Password"></asp:TextBox>
                                </div>
                            </div>
                            </div>
                            <div class="register-row4 clearfix">
                                <div class="res-level">
                                    <asp:Label ID="lblHearingMode" runat="server"><%= GetKeyResult("HOWDIDYOUHEAR")%></asp:Label>
                                    </div>
                                <div class="res-input">
                                    <div>
                                        <asp:DropDownList ID="modeList" runat="server" Width="172px">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div id="agency_companySpan" class="hide" runat="server">
                        <div class="register-right" id="rightSection">
                            <fieldset class="reg">
                                <legend>
                                    <asp:Label ID="lblFinancialData" runat="server"><%= GetKeyResult("FINANCIALDATA")%></asp:Label></legend>
                                <div class="register-row clearfix">
                                    <div id="agencySpan" runat="server">
                                        <asp:CheckBox ID="sameDataCheckBox" runat="server" AutoPostBack="true" OnCheckedChanged="sameDataCheckBox_CheckedChanged" />
                                        <asp:Label ID="lblSameData" runat="server"><%= GetKeyResult("USESAMEDATA")%></asp:Label>
                                    </div>
                                </div>
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblVatNo" runat="server"><%= GetKeyResult("VATNO")%></asp:Label>
                                        <span style="color: Red">*</span></div>
                                    <div class="res-input">
                                        <asp:TextBox ID="vatNo" runat="server" class="inputregister" MaxLength="30"></asp:TextBox>
                                       <asp:FilteredTextBoxExtender
                                            ID="FilteredTextBoxExtender6" TargetControlID="vatNo" FilterType="Numbers,LowercaseLetters,UppercaseLetters"
                                            runat="server">
                                        </asp:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblName" runat="server"><%= GetKeyResult("NAME")%></asp:Label>
                                        <span style="color: Red"></span></div>
                                    <div class="res-input">
                                        <asp:TextBox ID="txtName" runat="server" class="inputregister" MaxLength="50"></asp:TextBox>
                                        
                                    </div>
                                </div>
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblDepartment" runat="server"><%= GetKeyResult("DEPARTMENT")%></asp:Label>
                                        <span style="color: Red"></span></div>
                                    <div class="res-input">
                                        <asp:TextBox ID="dept" runat="server" class="inputregister" MaxLength="50"></asp:TextBox>
                                       
                                    </div>
                                </div>
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblEmailForComm" runat="server"><%= GetKeyResult("EMAILFORCOMM")%></asp:Label>
                                        <span style="color: Red"></span></div>
                                    <div class="res-input">
                                        <asp:TextBox ID="txtEmail" runat="server" class="inputregister" MaxLength="50"></asp:TextBox>
                                        
                                    </div>
                                </div>
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblCountryFinancial" runat="server"><%= GetKeyResult("COUNTRY")%></asp:Label>
                                        <span style="color: Red"></span></div>
                                    <div class="res-input">
                                        <div id="selectCountryListSpan">
                                            <asp:DropDownList ID="selectCountryList" runat="server"  
                                                OnSelectedIndexChanged="selectCountryList_SelectedIndexChanged" Width="172px">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="register-row clearfix" id="financialCityDropDownList" runat="server" visible="false">
                                    <div class="res-level">
                                        <asp:Label ID="lblCityFinancial" runat="server"><%= GetKeyResult("CITY")%></asp:Label>
                                        <span style="color: Red"></span></div>
                                    <div class="res-input">
                                        <div id="cityDropListSpan">
                                            <asp:DropDownList ID="cityDropList" runat="server" Width="172px">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <%--new markup for finance section city textbox--%>
                                 <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblCityFinance" runat="server"><%= GetKeyResult("CITY")%></asp:Label>
                                        <span style="color: Red"></span></div>
                                    <div class="res-input">
                                        <div id="Div3">
                                            <asp:TextBox ID="txtFinanceCity" runat="server" MaxLength="15" CssClass="inputregister">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblAddressFinancial" runat="server"><%= GetKeyResult("ADDRESS")%></asp:Label>
                                        <span style="color: Red"></span></div>
                                    <div class="res-input">
                                        <asp:TextBox ID="txtAddress" runat="server" MaxLength="100" TextMode="MultiLine"
                                            class="textareareg" oncopy="return false" onpaste="return false"></asp:TextBox>
                                        
                                        <div class="sep">
                                        </div>
                                    </div>
                                </div>
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblPhoneFinancial" runat="server"><%= GetKeyResult("PHONE")%></asp:Label>
                                        <span style="color: Red"></span></div>
                                    <div class="res-input">
                                        <table width="100%" border="0px">
                                            <tr>
                                                <td style="width: 80px">
                                                    <div id="countryCodeList2Span">
                                                        <asp:DropDownList ID="countryCodeList2" runat="server">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>7</asp:ListItem>
                                                            <asp:ListItem>20</asp:ListItem>
                                                            <asp:ListItem>27</asp:ListItem>
                                                            <asp:ListItem>30</asp:ListItem>
                                                            <asp:ListItem>31</asp:ListItem>
                                                            <asp:ListItem>32</asp:ListItem>
                                                            <asp:ListItem>33</asp:ListItem>
                                                            <asp:ListItem>34</asp:ListItem>
                                                            <asp:ListItem>36</asp:ListItem>
                                                            <asp:ListItem>39</asp:ListItem>
                                                            <asp:ListItem>40</asp:ListItem>
                                                            <asp:ListItem>41</asp:ListItem>
                                                            <asp:ListItem>43</asp:ListItem>
                                                            <asp:ListItem>44</asp:ListItem>
                                                            <asp:ListItem>45</asp:ListItem>
                                                            <asp:ListItem>46</asp:ListItem>
                                                            <asp:ListItem>47</asp:ListItem>
                                                            <asp:ListItem>48</asp:ListItem>
                                                            <asp:ListItem>49</asp:ListItem>
                                                            <asp:ListItem>51</asp:ListItem>
                                                            <asp:ListItem>52</asp:ListItem>
                                                            <asp:ListItem>53</asp:ListItem>
                                                            <asp:ListItem>54</asp:ListItem>
                                                            <asp:ListItem>55</asp:ListItem>
                                                            <asp:ListItem>56</asp:ListItem>
                                                            <asp:ListItem>57</asp:ListItem>
                                                            <asp:ListItem>58</asp:ListItem>
                                                            <asp:ListItem>60</asp:ListItem>
                                                            <asp:ListItem>61</asp:ListItem>
                                                            <asp:ListItem>62</asp:ListItem>
                                                            <asp:ListItem>63</asp:ListItem>
                                                            <asp:ListItem>64</asp:ListItem>
                                                            <asp:ListItem>65</asp:ListItem>
                                                            <asp:ListItem>66</asp:ListItem>
                                                            <asp:ListItem>81</asp:ListItem>
                                                            <asp:ListItem>82</asp:ListItem>
                                                            <asp:ListItem>84</asp:ListItem>
                                                            <asp:ListItem>86</asp:ListItem>
                                                            <asp:ListItem>90</asp:ListItem>
                                                            <asp:ListItem>91</asp:ListItem>
                                                            <asp:ListItem>92</asp:ListItem>
                                                            <asp:ListItem>93</asp:ListItem>
                                                            <asp:ListItem>94</asp:ListItem>
                                                            <asp:ListItem>95</asp:ListItem>
                                                            <asp:ListItem>98</asp:ListItem>
                                                            <asp:ListItem>212</asp:ListItem>
                                                            <asp:ListItem>213</asp:ListItem>
                                                            <asp:ListItem>216</asp:ListItem>
                                                            <asp:ListItem>218</asp:ListItem>
                                                            <asp:ListItem>220</asp:ListItem>
                                                            <asp:ListItem>221</asp:ListItem>
                                                            <asp:ListItem>222</asp:ListItem>
                                                            <asp:ListItem>223</asp:ListItem>
                                                            <asp:ListItem>224</asp:ListItem>
                                                            <asp:ListItem>225</asp:ListItem>
                                                            <asp:ListItem>226</asp:ListItem>
                                                            <asp:ListItem>227</asp:ListItem>
                                                            <asp:ListItem>228</asp:ListItem>
                                                            <asp:ListItem>229</asp:ListItem>
                                                            <asp:ListItem>230</asp:ListItem>
                                                            <asp:ListItem>231</asp:ListItem>
                                                            <asp:ListItem>232</asp:ListItem>
                                                            <asp:ListItem>233</asp:ListItem>
                                                            <asp:ListItem>234</asp:ListItem>
                                                            <asp:ListItem>235</asp:ListItem>
                                                            <asp:ListItem>236</asp:ListItem>
                                                            <asp:ListItem>237</asp:ListItem>
                                                            <asp:ListItem>238</asp:ListItem>
                                                            <asp:ListItem>239</asp:ListItem>
                                                            <asp:ListItem>240</asp:ListItem>
                                                            <asp:ListItem>241</asp:ListItem>
                                                            <asp:ListItem>242</asp:ListItem>
                                                            <asp:ListItem>243</asp:ListItem>
                                                            <asp:ListItem>244</asp:ListItem>
                                                            <asp:ListItem>245</asp:ListItem>
                                                            <asp:ListItem>248</asp:ListItem>
                                                            <asp:ListItem>249</asp:ListItem>
                                                            <asp:ListItem>250</asp:ListItem>
                                                            <asp:ListItem>251</asp:ListItem>
                                                            <asp:ListItem>252</asp:ListItem>
                                                            <asp:ListItem>253</asp:ListItem>
                                                            <asp:ListItem>254</asp:ListItem>
                                                            <asp:ListItem>255</asp:ListItem>
                                                            <asp:ListItem>256</asp:ListItem>
                                                            <asp:ListItem>257</asp:ListItem>
                                                            <asp:ListItem>258</asp:ListItem>
                                                            <asp:ListItem>260</asp:ListItem>
                                                            <asp:ListItem>261</asp:ListItem>
                                                            <asp:ListItem>262</asp:ListItem>
                                                            <asp:ListItem>263</asp:ListItem>
                                                            <asp:ListItem>264</asp:ListItem>
                                                            <asp:ListItem>265</asp:ListItem>
                                                            <asp:ListItem>266</asp:ListItem>
                                                            <asp:ListItem>267</asp:ListItem>
                                                            <asp:ListItem>268</asp:ListItem>
                                                            <asp:ListItem>269</asp:ListItem>
                                                            <asp:ListItem>290</asp:ListItem>
                                                            <asp:ListItem>291</asp:ListItem>
                                                            <asp:ListItem>297</asp:ListItem>
                                                            <asp:ListItem>298</asp:ListItem>
                                                            <asp:ListItem>299</asp:ListItem>
                                                            <asp:ListItem>350</asp:ListItem>
                                                            <asp:ListItem>351</asp:ListItem>
                                                            <asp:ListItem>352</asp:ListItem>
                                                            <asp:ListItem>353</asp:ListItem>
                                                            <asp:ListItem>354</asp:ListItem>
                                                            <asp:ListItem>355</asp:ListItem>
                                                            <asp:ListItem>356</asp:ListItem>
                                                            <asp:ListItem>357</asp:ListItem>
                                                            <asp:ListItem>358</asp:ListItem>
                                                            <asp:ListItem>359</asp:ListItem>
                                                            <asp:ListItem>370</asp:ListItem>
                                                            <asp:ListItem>371</asp:ListItem>
                                                            <asp:ListItem>372</asp:ListItem>
                                                            <asp:ListItem>373</asp:ListItem>
                                                            <asp:ListItem>374</asp:ListItem>
                                                            <asp:ListItem>375</asp:ListItem>
                                                            <asp:ListItem>376</asp:ListItem>
                                                            <asp:ListItem>377</asp:ListItem>
                                                            <asp:ListItem>378</asp:ListItem>
                                                            <asp:ListItem>380</asp:ListItem>
                                                            <asp:ListItem>381</asp:ListItem>
                                                            <asp:ListItem>382</asp:ListItem>
                                                            <asp:ListItem>385</asp:ListItem>
                                                            <asp:ListItem>386</asp:ListItem>
                                                            <asp:ListItem>387</asp:ListItem>
                                                            <asp:ListItem>389</asp:ListItem>
                                                            <asp:ListItem>420</asp:ListItem>
                                                            <asp:ListItem>421</asp:ListItem>
                                                            <asp:ListItem>423</asp:ListItem>
                                                            <asp:ListItem>500</asp:ListItem>
                                                            <asp:ListItem>501</asp:ListItem>
                                                            <asp:ListItem>502</asp:ListItem>
                                                            <asp:ListItem>503</asp:ListItem>
                                                            <asp:ListItem>504</asp:ListItem>
                                                            <asp:ListItem>505</asp:ListItem>
                                                            <asp:ListItem>506</asp:ListItem>
                                                            <asp:ListItem>507</asp:ListItem>
                                                            <asp:ListItem>508</asp:ListItem>
                                                            <asp:ListItem>509</asp:ListItem>
                                                            <asp:ListItem>590</asp:ListItem>
                                                            <asp:ListItem>591</asp:ListItem>
                                                            <asp:ListItem>592</asp:ListItem>
                                                            <asp:ListItem>593</asp:ListItem>
                                                            <asp:ListItem>595</asp:ListItem>
                                                            <asp:ListItem>597</asp:ListItem>
                                                            <asp:ListItem>598</asp:ListItem>
                                                            <asp:ListItem>599</asp:ListItem>
                                                            <asp:ListItem>670</asp:ListItem>
                                                            <asp:ListItem>672</asp:ListItem>
                                                            <asp:ListItem>673</asp:ListItem>
                                                            <asp:ListItem>674</asp:ListItem>
                                                            <asp:ListItem>675</asp:ListItem>
                                                            <asp:ListItem>676</asp:ListItem>
                                                            <asp:ListItem>677</asp:ListItem>
                                                            <asp:ListItem>678</asp:ListItem>
                                                            <asp:ListItem>679</asp:ListItem>
                                                            <asp:ListItem>680</asp:ListItem>
                                                            <asp:ListItem>681</asp:ListItem>
                                                            <asp:ListItem>682</asp:ListItem>
                                                            <asp:ListItem>683</asp:ListItem>
                                                            <asp:ListItem>685</asp:ListItem>
                                                            <asp:ListItem>686</asp:ListItem>
                                                            <asp:ListItem>687</asp:ListItem>
                                                            <asp:ListItem>688</asp:ListItem>
                                                            <asp:ListItem>689</asp:ListItem>
                                                            <asp:ListItem>690</asp:ListItem>
                                                            <asp:ListItem>691</asp:ListItem>
                                                            <asp:ListItem>692</asp:ListItem>
                                                            <asp:ListItem>850</asp:ListItem>
                                                            <asp:ListItem>852</asp:ListItem>
                                                            <asp:ListItem>853</asp:ListItem>
                                                            <asp:ListItem>855</asp:ListItem>
                                                            <asp:ListItem>856</asp:ListItem>
                                                            <asp:ListItem>870</asp:ListItem>
                                                            <asp:ListItem>880</asp:ListItem>
                                                            <asp:ListItem>886</asp:ListItem>
                                                            <asp:ListItem>960</asp:ListItem>
                                                            <asp:ListItem>961</asp:ListItem>
                                                            <asp:ListItem>962</asp:ListItem>
                                                            <asp:ListItem>963</asp:ListItem>
                                                            <asp:ListItem>964</asp:ListItem>
                                                            <asp:ListItem>965</asp:ListItem>
                                                            <asp:ListItem>966</asp:ListItem>
                                                            <asp:ListItem>967</asp:ListItem>
                                                            <asp:ListItem>968</asp:ListItem>
                                                            <asp:ListItem>970</asp:ListItem>
                                                            <asp:ListItem>971</asp:ListItem>
                                                            <asp:ListItem>972</asp:ListItem>
                                                            <asp:ListItem>973</asp:ListItem>
                                                            <asp:ListItem>974</asp:ListItem>
                                                            <asp:ListItem>975</asp:ListItem>
                                                            <asp:ListItem>976</asp:ListItem>
                                                            <asp:ListItem>977</asp:ListItem>
                                                            <asp:ListItem>992</asp:ListItem>
                                                            <asp:ListItem>993</asp:ListItem>
                                                            <asp:ListItem>994</asp:ListItem>
                                                            <asp:ListItem>995</asp:ListItem>
                                                            <asp:ListItem>996</asp:ListItem>
                                                            <asp:ListItem>998</asp:ListItem>
                                                            <asp:ListItem>1242</asp:ListItem>
                                                            <asp:ListItem>1246</asp:ListItem>
                                                            <asp:ListItem>1264</asp:ListItem>
                                                            <asp:ListItem>1268</asp:ListItem>
                                                            <asp:ListItem>1284</asp:ListItem>
                                                            <asp:ListItem>1340</asp:ListItem>
                                                            <asp:ListItem>1345</asp:ListItem>
                                                            <asp:ListItem>1441</asp:ListItem>
                                                            <asp:ListItem>1473</asp:ListItem>
                                                            <asp:ListItem>1599</asp:ListItem>
                                                            <asp:ListItem>1649</asp:ListItem>
                                                            <asp:ListItem>1664</asp:ListItem>
                                                            <asp:ListItem>1670</asp:ListItem>
                                                            <asp:ListItem>1671</asp:ListItem>
                                                            <asp:ListItem>1684</asp:ListItem>
                                                            <asp:ListItem>1758</asp:ListItem>
                                                            <asp:ListItem>1767</asp:ListItem>
                                                            <asp:ListItem>1784</asp:ListItem>
                                                            <asp:ListItem>1809</asp:ListItem>
                                                            <asp:ListItem>1868</asp:ListItem>
                                                            <asp:ListItem>1869</asp:ListItem>
                                                            <asp:ListItem>1876</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </td>
                                                <td style="padding-left: 0px;">
                                                    <asp:TextBox ID="txtPhone" runat="server" Text="Phone" class="inputregister2" MaxLength="15"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtPhone"
                                                        FilterType="Numbers">
                                                    </asp:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="register-row clearfix">
                                    <div class="res-level">
                                        <asp:Label ID="lblPostalCodeFinancial" runat="server"><%= GetKeyResult("POSTALCODE") %></asp:Label>
                                        <span style="color: Red"></span></div>
                                    <div class="res-input">
                                        <asp:TextBox ID="txtPostalCode" runat="server" class="inputregister2" MaxLength="10"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtPostalCode"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="register1-form-btn" runat="server" id="btnRegisterCancel">
            <div class="save-cancel-btn1">
                <asp:LinkButton ID="registerBtn" runat="server" class="register-btn" OnClick="registerBtn_Click"
                     />&nbsp;&nbsp;
                <asp:LinkButton ID="cancelBtn" runat="server" class="cancel-btn" OnClick="cancelBtn_Click" />&nbsp;
            </div>
        </div>
    </div>
    <div id="Loding_overlaySec">
        <img src="Images/ajax-loader.gif" style="margin: 350px 0px 0px 0px; z-index: 1999" /><br />
        <span>Saving...</span></div>
    <script language="javascript" type="text/javascript">

        jQuery(document).ready(function () {

            if ('<%= Session["registerType"] %>' === 10) {
                jQuery("#<%= agency_companySpan.ClientID %>").addClass("hide").hide();
            }

            jQuery("#<%= registerBtn.ClientID %>").bind("click", function () {
                if (jQuery("#<%= divmessage.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= divmessage.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                }
                var isvalid = true;
                var errormessage = "";

                //Agency Specific Validation starts--------------------------------

                if ('<%= Session["registerType"] %>' == 7) {
                    var companyName = jQuery("#<%= companyName.ClientID %>").val();
                    if (companyName.length <= 0) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>"
                        }
                        errormessage += '<%=GetKeyResultForJavaScript("COMPANYERRORMESSAGE")%>';
                        isvalid = false;
                    }

                }

                //Agency Specific validation ends-------------------------------

                var firstName = jQuery("#<%= firstName.ClientID %>").val();
                if (firstName <= 0) {
                    
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%=GetKeyResultForJavaScript("FIRSTNAMEERRORMESSAGE") %>';
                    isvalid = false;
                }
                var lastName = jQuery("#<%= lastName.ClientID %>").val();
                if (lastName <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%=GetKeyResultForJavaScript("LASTNAMEERRORMESSAGE") %>';
                    isvalid = false;
                }



                //Company Specific Validation starts-----------------------------

                if ('<%= Session["registerType"] %>' == 9) {
                    var company = jQuery("#<%= company.ClientID %>").val();
                    if (company.length <= 0) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>"
                        }
                        errormessage += '<%=GetKeyResultForJavaScript("COMPANYNAMEERRORMESSAGE")%>';
                        isvalid = false;
                    }
                }

                //Company Specific validation Ends-----------------------------

                var email = jQuery("#<%= email.ClientID %>").val();
                if (email.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%=GetKeyResultForJavaScript("EMAILIDERRORMESSAGE")%>';
                    isvalid = false;
                }
                else {
                    if (!validateEmail(email)) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += '<%=GetKeyResultForJavaScript("EMAILVALADITYERRORMESSAGE")%>';
                        isvalid = false;
                    }
                }
                var emailAgain = jQuery("#<%= emailAgain.ClientID %>").val();
                if (emailAgain != null) {
                    if (emailAgain.length <= 0) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += '<%=GetKeyResultForJavaScript("REENTEREMAILERRORMESSAGE")%>';
                        isvalid = false;
                    }
                }
                //var countryList = jQuery("#countryListSpan div span").html();
                var countryList = jQuery("#<%= countryList.ClientID %>").val();
                if (countryList == '<%=GetKeyResultForJavaScript("SELECTCOUNTRY")%>' || countryList == "0") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%=GetKeyResultForJavaScript("COUNTRYERRORMESSAGE")%>';
                    isvalid = false;
                }
                //var cityList = jQuery("#cityListSpan div span").html();
                var cityList = jQuery("#<%= cityList.ClientID %>").val();
                if (cityList == '<%=GetKeyResultForJavaScript("SELECTCITY")%>' || cityList == "0") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%=GetKeyResultForJavaScript("CITYERRORMESSAGE")%>';
                    isvalid = false;
                }
                var cityName = jQuery("#<%= txtCity.ClientID %>").val();
                if (cityName <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%=GetKeyResultForJavaScript("CITYNAMEERRORMESSAGE") %>';
                    isvalid = false;
                }



                var address = jQuery("#<%= address.ClientID %>").val();
                if (address.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%=GetKeyResultForJavaScript("ADDRESSERRORMESSAGE")%>';
                    isvalid = false;
                }
                //var countryCodeList1 = jQuery("#countryCodeList1Span div span").html();
                var countryCodeList1 = jQuery("#<%= countryCodeList1.ClientID %>").val();
                if (countryCodeList1 == '<%=GetKeyResultForJavaScript("EXTENSION")%>' || countryCodeList1 == "0") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%=GetKeyResultForJavaScript("EXTENSIONCODEERRORMESSAGE")%>';
                    isvalid = false;
                }
                var phone = jQuery("#<%= phone.ClientID %>").val();
                if (phone.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%=GetKeyResultForJavaScript("PHONEERRORMESSAGE")%>';
                    isvalid = false;
                }
                var postalCode = jQuery("#<%= postalCode.ClientID %>").val();
                if (postalCode.length <= 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%=GetKeyResultForJavaScript("POSTALCODEERRORMESSAGE")%>';
                    isvalid = false;
                }

                //preferred language validation goes here--------------
                if ('<%= Session["registerType"] %>' == 10 || '<%= Session["registerType"] %>' == 9) {
                    var password = jQuery("#<%= password.ClientID %>").val();
                    if (password != null) {
                        if (password.length <= 0) {
                            if (errormessage.length > 0) {
                                errormessage += "<br/>";
                            }
                            errormessage += '<%=GetKeyResultForJavaScript("PASSWORDERRORMESSAGE")%>';
                            isvalid = false;
                        }
                        else {
                            if (password.length < 6) {
                                if (errormessage.length > 0) {
                                    errormessage += "<br/>";
                                }
                                errormessage += '<%=GetKeyResultForJavaScript("PASSWORDLENGTHERRORMESSAGE")%>';
                                isvalid = false;
                            }
                        }
                    }
                    var reTypePwd = jQuery("#<%= retype.ClientID %>").val();
                    if (password != null) {
                        if (reTypePwd.length <= 0) {
                            if (errormessage.length > 0) {
                                errormessage += "<br/>";
                            }
                            errormessage += '<%=GetKeyResultForJavaScript("REENTERPASSWORDMESSAGE")%>';
                            isvalid = false;
                        }
                    }

                }

                //-----------------------"how do you hear from us" validation goes here-----

                if ('<%= Session["registerType"] %>' == 7 || '<%= Session["registerType"] %>' == 9) {
                    var vatNo = jQuery("#<%= vatNo.ClientID %>").val();
                    if (vatNo.length <= 0) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += '<%=GetKeyResultForJavaScript("VATNOERRORMESSAGE")%>';
                        isvalid = false;
                    }

                }
                if (emailAgain != null) {
                    if (email != emailAgain) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += '<%=GetKeyResultForJavaScript("EMAILSNOTMATCHINGMESSAGE")%>';
                        isvalid = false;
                    }
                }
                if (password != null) {
                    if (password != reTypePwd) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += '<%=GetKeyResultForJavaScript("PWDNOTMATCHINGMESSAGE")%>';
                        isvalid = false;
                    }
                }

                if (!isvalid) {
                    jQuery(".error").show();
                    jQuery(".error").html(errormessage);
                    var offseterror = jQuery(".error").offset();
                    jQuery("body").scrollTop(offseterror.top);
                    jQuery("html").scrollTop(offseterror.top);
                    return false;
                }


                jQuery(".error").hide();
                jQuery(".error").html('');
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                jQuery("#Loding_overlaySec span").html("Saving...");
                jQuery("#Loding_overlaySec").show();
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';



            });
        });
        function validateEmail(email) {
            var a = email;
            var filter = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
            if (filter.test(a)) {
                return true;
            }
            else {
                return false;
            }

        }

        jQuery(document).ready(function () {
            var MaxLength = 100;
            jQuery('#<%=address.ClientID %>').keypress(function (e) {
                var keycode = e.keyCode ? e.keyCode : e.which;
                if (jQuery(this).val().length >= MaxLength && keycode != 8) {
                    e.preventDefault();
                }
            });
        });

        jQuery(document).ready(function () {
            var MaxLength = 100;
            jQuery('#<%=txtAddress.ClientID %>').keypress(function (e) {
                var keycode = e.keyCode ? e.keyCode : e.which;
                if (jQuery(this).val().length >= MaxLength && keycode != 8) {
                    e.preventDefault();
                }
            });

        });

        jQuery(document).ready(function () {
            if (document.getElementById('<%=email.ClientID %>').value.length <= 0) {
                document.getElementById('<%=loginDiv.ClientID %>').style.display = 'none';
            }
            else {
                document.getElementById('<%=loginDiv.ClientID %>').style.display = 'block';
            }

        });
        function ChangeValue(txt) {
            if (document.getElementById('<%=email.ClientID %>').value.length <= 0) {
                document.getElementById('<%=loginDiv.ClientID %>').style.display = 'none';
            }
            else {
                document.getElementById('<%=loginDiv.ClientID %>').style.display = 'block';
            }
            var at = txt.value;
            jQuery('#<%=Login.ClientID %>').html(at);
        }

        function copydata() {
            if (document.getElementById('<%=sameDataCheckBox.ClientID %>').checked == 1) {
                document.getElementById('<%=txtName.ClientID %>').value = document.getElementById('<%=firstName.ClientID %>').value;
                document.getElementById('<%=txtEmail.ClientID %>').value = document.getElementById('<%=email.ClientID %>').value;
                document.getElementById('<%=txtAddress.ClientID %>').value = document.getElementById('<%=address.ClientID %>').value;
                document.getElementById('<%=txtPhone.ClientID %>').value = document.getElementById('<%=phone.ClientID %>').value;
                document.getElementById('<%=txtPostalCode.ClientID %>').value = document.getElementById('<%=postalCode.ClientID %>').value;
                var t = document.getElementById('<%=countryCodeList1.ClientID %>').selectedIndex;
                document.getElementById('<%=countryCodeList2.ClientID%>').selectedIndex = t;
            }
            else {
                document.getElementById('<%=txtName.ClientID %>').value = "";
                document.getElementById('<%=txtEmail.ClientID %>').value = "";
            }
        }

        function checklength() {
            var MaxLength = 100;
            jQuery('#<%=address.ClientID %>').keypress(function (e) {
                
                var keycode = e.keyCode ? e.keyCode : e.which;
                if (jQuery(this).val().length >= MaxLength && keycode != 8) {
                    e.preventDefault();
                }
            });
        }

//        function CheckLength() {
//            var textbox = document.getElementById("<%=address.ClientID%>").value;
//            if (textbox.trim().length >= 100) {
//                return false;
//            }
//            else {
//                return true;
//            }
//        }
        
    </script>
    
  

    
</asp:Content>
