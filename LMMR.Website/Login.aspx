﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/RegisterMaster.master"
    CodeFile="Login.aspx.cs" Inherits="Login" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<!-- ClickTale Top part -->
<script type="text/javascript">
    var WRInitTime = (new Date()).getTime();
</script>
<!-- ClickTale end of Top part -->
    <!--Main register-body start -->
    <div class="register-body">
        <style type="text/css">
            .modalBackground
            {
                background-color: #CCCCFF;
                filter: alpha(opacity=40);
                opacity: 0.5;
            }
            .water
            {
                color: Gray;
            }
        </style>
        <div class="login-main1">
            <table width="100%" border="0" cellpadding="5" cellspacing="0">
                <tr>
                    <td width="60%" valign="top" align="center">
                        <asp:Panel ID="loginpnl" runat="server" DefaultButton="btnLogin">
                            <table width="60%" border="0" cellpadding="3" cellspacing="0" align="center">
                                <tr>
                                    <td align="center" colspan="2" height="35" valign="top">
                                        <h2 class="register">
                                            <%= GetKeyResult("EXISTINGUSER")%></h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="error" id="divError" runat="server">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="res-leve2" align="left">
                                        <%= GetKeyResult("LOGIN")%><span style="color: Red"> *</span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtUserName" runat="server" CssClass="inputregister" TabIndex="1"
                                            MaxLength="200"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="res-leve2" align="left">
                                        <%= GetKeyResult("PASSWORD")%>
                                        <span style="color: Red">*</span>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtPassword" runat="server" CssClass="inputregister" TextMode="Password"
                                            TabIndex="1" MaxLength="20"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="res-leve2">
                                    </td>
                                    <td align="left">
                                        <asp:CheckBox ID="chkRememberPassword" runat="server" /><%= GetKeyResult("REMEMBERME")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left">
                                        <%= GetKeyResult("FORGOTPASSWORD")%>
                                        <asp:HyperLink ID="hypForgotPassword" runat="server"><%= GetKeyResult("CLICKHERE")%></asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <recaptcha:RecaptchaControl ID="recaptcha" Theme="White" runat="server" PublicKey="6LfevNsSAAAAALzRdZilA5xfJORmpbqApN248ksJ"
                                            PrivateKey="6LfevNsSAAAAAD1w3qhJYWblWc57jzKpcQ6W9Xp6"></recaptcha:RecaptchaControl>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <div class="blue-button">
                                            <asp:LinkButton ID="btnLogin" CssClass="login-btn" runat="server" OnClick="btnLogin_Click"
                                                ValidationGroup="LOGIN" TabIndex="1"><%= GetKeyResult("LOGIN")%></asp:LinkButton></div>
                                        <div class="blue-button">
                                            <asp:LinkButton ID="btnReset" runat="server" CssClass="cancel-btn" OnClick="btnReset_Click"><%= GetKeyResult("CANCEL")%></asp:LinkButton></div>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                    <td width="40%" valign="top">
                        <table width="80%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" colspan="2" height="35" valign="top">
                                    <h2 class="register">
                                        <%= GetKeyResult("TOREGISTER")%></h2>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="register">
                                        <li>
                                                <asp:LinkButton ID="companyLink" runat="server" OnClick="companyLink_Click" Enabled="true"><%= GetKeyResult("REGISTERASCLIENT")%></asp:LinkButton></li>
                                                <li><asp:LinkButton ID="userLink" runat="server" OnClick="userLink_Click" Enabled="true"><%= GetKeyResult("REGISTERASCLIENTPRIVATE")%></asp:LinkButton></li>
                                                </ul>
                                                <br /><br />
                                </td>

                            </tr>
                            
                            <tr>
                                <td align="center">
                                    <asp:LinkButton ID="agencyLink" runat="server" Enabled="true" 
                                        onclick="agencyLink_Click"><%= GetKeyResult("REGISTERASAGENCY")%></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div id="divJoinToday-overlay">
        </div>
        <div id="divJoinToday">
            <div class="popup-top">
            </div>
            <div class="popup-mid">
                <div class="popup-mid-inner">
                    <div class="error" id="errorPopup" runat="server">
                    </div>
                    <table cellspacing="10">
                        <tr>
                            <td>
                                <%= GetKeyResult("FIRSTNAME")%>
                            </td>
                            <td>
                                <asp:TextBox ID="txtfirstname" runat="server" value="" class="inputregister" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= GetKeyResult("LASTNAME")%>
                            </td>
                            <td>
                                <asp:TextBox ID="txtlastname" runat="server" value="" class="inputregister" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= GetKeyResult("EMAIL")%>
                            </td>
                            <td>
                                <asp:TextBox ID="txtEmail" runat="server" value="" class="inputregister" MaxLength="250"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= GetKeyResult("PHONE")%>
                            </td>
                            <td>
                                <asp:TextBox ID="txtphoneNo" runat="server" value="" class="inputregister" MaxLength="15"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtphoneNo"
                                    ValidChars="-0123456789">
                                </asp:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Company Name:
                            </td>
                            <td>
                                <asp:TextBox ID="txtcompanyname" runat="server" value="" class="inputregister" MaxLength="250"></asp:TextBox>
                                <%--<asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" TargetControlID="txtNoMeetingRoom"
                                ValidChars="0123456789" runat="server">
                            </asp:FilteredTextBoxExtender>--%>
                            </td>
                        </tr>
                    </table>
                    <div class="subscribe-btn">
                        <div class="save-cancel-btn1 button_section">
                            <asp:LinkButton ID="btnSend" runat="server" CssClass="select" OnClick="btnSend_Click"><%=GetKeyResult("SEND") %></asp:LinkButton>
                            &nbsp;or&nbsp;
                            <asp:LinkButton ID="lnkCnacel" runat="server" CssClass="cancelpop" OnClientClick="return CloseJoinTodayPopUp();"><%=GetKeyResult("CANCEL") %></asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <div class="popup-bottom">
            </div>
        </div>
        <!-- end register-body-->
    </div>
    
    <script language="javascript" type="text/javascript">
        function registeredMessage() {
            alert('<%= GetKeyResultForJavaScript("ALREADYREGISTERED")%>');
            return false;
        }

        function CloseJoinTodayPopUp() {
            document.getElementsByTagName('html')[0].style.overflow = 'auto';
            document.getElementById('divJoinToday').style.display = 'none';
            document.getElementById('divJoinToday-overlay').style.display = 'none';
            jQuery('#<%=txtfirstname.ClientID %>').val('');
            jQuery('#<%=txtlastname.ClientID %>').val('');
            jQuery('#<%=txtEmail.ClientID %>').val('');
            jQuery('#<%=txtphoneNo.ClientID %>').val('');
            jQuery('#<%=txtcompanyname.ClientID %>').val('');
            return false;
        }
       
        jQuery(document).ready(function () {
            jQuery("#<%= agencyLink.ClientID %>").bind("click", function () {
                if (jQuery("#<%= errorPopup.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= errorPopup.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                }
                jQuery('#<%=txtfirstname.ClientID %>').val('');
                jQuery('#<%=txtlastname.ClientID %>').val('');
                jQuery('#<%=txtEmail.ClientID %>').val('');
                jQuery('#<%=txtphoneNo.ClientID %>').val('');
                jQuery('#<%=txtcompanyname.ClientID %>').val('');
                jQuery("#<%= errorPopup.ClientID %>").html("");
                jQuery("#<%= errorPopup.ClientID %>").hide();
                //jQuery("#Loding_overlay").show();
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                jQuery("#divJoinToday").show();
                jQuery("#divJoinToday-overlay").show();
                return false;
            });
            jQuery("#<%= errorPopup.ClientID %>").hide();
            jQuery("#errorPopup").hide();
            jQuery("#<%= btnSend.ClientID %>").bind("click", function () {
                if (jQuery("#<%= errorPopup.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= errorPopup.ClientID %>").removeClass("succesfuly").addClass("error").hide();
                }
                jQuery("#divJoinToday").show();
                jQuery("#divJoinToday-overlay").show();
                var isvalid = true;
                var errormessage = "";
                var Firstname = jQuery("#<%= txtfirstname.ClientID %>").val();
                if (Firstname.length <= 0 || Firstname == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>"
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("ERRORFIRSTNAME")%>';
                    isvalid = false;
                }

                var lastname = jQuery("#<%= txtlastname.ClientID %>").val();
                if (lastname.length <= 0 || lastname == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>"
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("ERRORLASTNAME")%>';
                    isvalid = false;
                }

                var emailp1 = jQuery("#<%= txtEmail.ClientID %>").val();
                if (emailp1.length <= 0 || emailp1 == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("ERROR")%>';
                    isvalid = false;
                }

                else {
                    if (!validateEmail(emailp1)) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += '<%= GetKeyResultForJavaScript("ERROR")%>';
                        isvalid = false;
                    }
                }

                var Phone = jQuery("#<%=txtphoneNo.ClientID %>").val();
                if (Phone.length <= 0 || Phone == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>"
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("ERRORPHONE")%>';
                    isvalid = false;
                }

                var Companyname = jQuery("#<%=txtcompanyname.ClientID %>").val();
                if (Companyname.length <= 0 || Companyname == "") {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>"
                    }
                     errormessage += '<%= GetKeyResultForJavaScript("COMPANYERRORMESSAGE")%>';
                    isvalid = false;
                }

                if (!isvalid) {
                    jQuery("#<%= errorPopup.ClientID %>").show();
                    jQuery("#<%= errorPopup.ClientID %>").html(errormessage);
                    return false;
                }
                jQuery("#<%= errorPopup.ClientID %>").html("");
                jQuery("#<%= errorPopup.ClientID %>").hide();
                jQuery("#Loding_overlay").show();
                jQuery("body").scrollTop(0);
                jQuery("html").scrollTop(0);
                document.getElementsByTagName('html')[0].style.overflow = 'hidden';
                jQuery("#divJoinToday").show();
                jQuery("#divJoinToday-overlay").show();
            });

        });
        function validateEmail(txtEmail) {
            var a = txtEmail;
            var filter = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
            if (filter.test(a)) {
                return true;
            }
            else {
                return false;
            }
        }

    </script>
    <!-- end register-body-->
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#<%= btnLogin.ClientID %>").bind("click", function () {
                var isvalid = true;
                var errormessage = "";
                var Email = jQuery("#<%=txtUserName.ClientID %>").val();
                if (Email.length == 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("EMAILISREQUIRED")%>';
                    isvalid = false;
                }
                else {
                    if (!validateEmail(Email)) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += '<%= GetKeyResultForJavaScript("ENTERVALIDEMAIL")%>';
                        isvalid = false;
                    }
                }

                var password = jQuery("#<%=txtPassword.ClientID %>").val();
                if (password.length == 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("PASSWORDISREQUIRED")%>';
                    isvalid = false;
                }
                if (!isvalid) {
                    jQuery(".error").show();
                    jQuery(".error").html(errormessage);
                    return false;
                }
                jQuery(".error").html("");
                jQuery(".error").hide();
            });
        });

        function validateEmail(txtEmail) {
            var a = txtEmail;
            var filter = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
            if (filter.test(a)) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    
<!-- ClickTale Bottom part -->
<div id="ClickTaleDiv" style="display: none;"></div>
<script type="text/javascript">
    if (document.location.protocol != 'https:')
        document.write(unescape("%3Cscript%20src='http://s.clicktale.net/WRd.js'%20type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
    if (typeof ClickTale == 'function') ClickTale(24322, 0.0586, "www02");
</script>
<!-- ClickTale end of Bottom part -->
</asp:Content>
