﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SurveyRequest.aspx.cs" Inherits="SurveyRequest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="css/Survey.css" rel="stylesheet" type="text/css" />
     <link href="css/style.css" rel="stylesheet" type="text/css" />
    <%-- <link href="css/jqtransform.css" rel="stylesheet" type="text/css" />--%>
    <title>Survey Request</title>
    <script type="text/javascript">
        //window.opener = window
        function close() {
            window.close();
        }
        
    </script>
    <style type="text/css">
        .style1
        {
            font-size: medium;
        }
    </style>
</head>
<body>
    <div>
        <form id="form1" runat="server">
        <div id="divsurvey" runat="server" style="vertical-align:top">
            <table class="table" style="width: 1000px;" cellpadding="0" cellspacing="0">
                <tbody>                   
                    <tr class="tableHeaderRow" align="center" valign="top">
                        <td>
                            <img src="Images/mail-logo.png" />
                        </td>
                       
                        <td>
                            <h1 >
                              Please take a moment to rate the following product and service elements.
                            </h1>
                        </td>
                    </tr>                 
                    <tr align="center">
                        <td colspan="2">
                            <div id="divmessage" align="center" runat="server" class="error">
                            </div>
                        </td>
                    </tr>
                     <tr>   
                        <td></td>                    
                        <td align="center">                        
                            <asp:Label ID="lblVenueName" runat="server" Text="" ForeColor="Black" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                     <tr>                       
                        <td></td>                    
                        <td align="center">                       
                            <asp:Label ID="lblDate" runat="server" Text="" ForeColor="Black" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="table"  style="width: 950px; padding-left: 150px;"
                cellspacing="1">
                <tbody>
                    <tr class="tableHeaderRow">
                        <td width="25%" class="tableHeaderCell">
                            Please tell us if the meeting was held
                        </td>
                        <td width="100px" class="tableHeaderCell" height="20px"  style="border-left: #F6F6F6 solid 1px;
                            padding-left: 20px;">
                            <asp:DropDownList ID="Dropmetingheld" runat="server" OnSelectedIndexChanged="Dropmetingheld_SelectedIndexChanged"
                                AutoPostBack="true">
                                <asp:ListItem Text="Yes" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="No" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <%-- <div id="divhoteldrop" runat="server">--%>
                        <td width="25%" class="tableHeaderCell" id="tblhotel" runat="server">
                            If,Yes, Please Specify the hotel
                        </td>
                        <td width="100px" class="tableHeaderCell" height="20px" id="tblhotel1" runat="server" style="border-left: #F6F6F6 solid 1px;
                            padding-left: 10px;">
                            <asp:DropDownList ID="drphotel" runat="server">
                            </asp:DropDownList>
                        </td>
                        <%--</div>--%>
                    </tr>
                </tbody>
            </table>
            <asp:Panel ID="pnlRequest" runat="server">
            <table class="table" style="width: 950px; padding-left: 150px;" cellspacing="1" border="5px">
            <tbody>
                <asp:Repeater ID="rptheadQuestion" runat="server" OnItemDataBound="rptheadQuestion_ItemDataBound">
                    <ItemTemplate>
                        <tr class="tableHeaderRow">
                            <td class="tableHeaderCell" colspan="2">
                                <%#Container.ItemIndex +1 %> .<asp:Label ID="lblHeader" runat="server"></asp:Label>
                                <asp:HiddenField ID="hdfsurveyId" runat="server" />
                            </td>
                        </tr>
                        <asp:Repeater ID="rptsubquestions" runat="server" OnItemDataBound="rptsubquestions_ItemDataBound" onitemcreated="rptsubquestionscreated">
                            <ItemTemplate>
                                <tr class="tableFormRowEditor Even">
                                    <td width="50%" class="tableFormCell Even">
                                        <%#Container.ItemIndex +1 %> ).<asp:Label ID="lblsubquestions" runat="server"></asp:Label><%--Text='<%# DataBinder.Eval(Container.DataItem, "Answer") %>'--%>
                                        <asp:HiddenField ID="hdfSubQsurveyId" runat="server"  Value='<%# DataBinder.Eval(Container.DataItem, "QuestionID") %>'/><%--Value='<%# DataBinder.Eval(Container.DataItem, "AnswerID") %>' --%>
                                        &nbsp;
                                    </td>
                                    <td width="50%" class="tableFormCell Even" style="border-left: #fff solid 1px;">
                                        <table style="padding: 4px; width: 400px;" class="formInput">
                                            <tbody>
                                                <tr style="height: 50%; vertical-align: bottom;">
                                                    <td style="padding-left: 8px; text-align: center; white-space: nowrap;">
                                                        <asp:RadioButtonList ID="rbtsurvey" runat="server" RepeatDirection="Horizontal"
                                                            CssClass="formRadio" Visible="true">
                                                            <asp:ListItem Value="1"></asp:ListItem>
                                                            <asp:ListItem Value="2"></asp:ListItem>
                                                            <asp:ListItem Value="3"></asp:ListItem>
                                                            <asp:ListItem Value="4"></asp:ListItem>
                                                            <asp:ListItem Value="5" Selected></asp:ListItem>
                                                            <asp:ListItem Value="6"></asp:ListItem>
                                                            <asp:ListItem Value="7"></asp:ListItem>
                                                            <asp:ListItem Value="8"></asp:ListItem>
                                                            <asp:ListItem Value="9"></asp:ListItem>
                                                            <asp:ListItem Value="10"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                        <asp:TextBox ID="commentsText" runat="server" TextMode="MultiLine" Width="447px" Visible="false" />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
        <table class="table" style="width: 950px; padding-left: 150px" cellspacing="1">
            <tbody>
                <tr class="tableFormRowEditor Odd">
                    <td class="tableFormCell Odd" width="50%" colspan="2">
                        <p>
                            <b>
                                <asp:Label ID="Label18" runat="server" Text="We highly appreciate your comments and feedback."
                                    Style="margin-left: 200px;" />
                            </b>
                        </p>
                        <br />
                        <p>
                            <b>
                                <asp:Label ID="Label19" runat="server" Text="Lastminutemeetingroom support team"
                                    Style="margin-left: 210px;" /></b></p>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Button ID="btn_submit" runat="server" Text="submit" 
                            Style="margin-left: 300px;" onclick="btn_submit_Click" />
                            <asp:HiddenField ID="hdfcommentstxt" runat="server" />
                        <%--<input name="btnValid" value="Submit"  class="formButton formButtonMiddle"  style="margin-left:450px;" type="submit">--%>
                    </td>
                </tr>
            </tbody>
        </table>
        </asp:Panel>
        </div>
        </form>
    </div>
</body>
</html>
