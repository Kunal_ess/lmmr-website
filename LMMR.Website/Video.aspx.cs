﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using System.Web.Services;
using System.Configuration;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Data;
using LMMR.Data;
using log4net;
using log4net.Config;
using System.Text;
#endregion

public partial class Video : System.Web.UI.Page
{
    public string VideoPath = "";
    BookingRequest objBookingRequest = new BookingRequest();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["HotelId"] != null)
            {
                TList<HotelPhotoVideoGallary> ObjHotelImageVideo = objBookingRequest.GetAllHotelPictureVideo(Convert.ToInt32(Request.QueryString.Get("HotelId").ToString()));
                if (ObjHotelImageVideo.Count != 0)
                {
                    //VideoPath = ConfigurationManager.AppSettings["FilePath"].ToString() + "HotelVideo/" + ObjHotelImageVideo[0].VideoName;
                    VideoPath = ObjHotelImageVideo[0].VideoName;
                    //litPlayVideo.Text = GetYouTubeScript(VideoPath);
                    litPlayVideo.Text = VideoPath;//"<iframe width='480' height='280' src='" + VideoPath + "' frameborder='0' allowfullscreen></iframe>";

                }
                else
                {
                    //noVedioDIV.Visible = true;
                }
            }
            else
            {
                if (Request.QueryString["fid"] != null)
                {

                    if (Convert.ToString(Request.QueryString["fid"]) == "GUESTS")
                    {
                        VideoPath = ConfigurationManager.AppSettings["FilePath"].ToString() + "ReadVideo/LMMR_Film_Clients.mp4";
                    }
                    if (Convert.ToString(Request.QueryString["fid"]) == "HOTEL")
                    {
                        VideoPath = ConfigurationManager.AppSettings["FilePath"].ToString() + "ReadVideo/LMMR_Film_Hotel&Meeting2.mp4";
                    }
                }
                //"Uploads/ReadVideo/video.mp4";

            }
        }

    }
    protected string GetYouTubeScript(string id)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<object width='425' height='344'>");
        sb.Append("<param name='movie' value='http://www.youtube.com/watch?v=SKRgktzRvZ0&feature=related'>");
        sb.Append("</param>");
        sb.Append("<param name='allowFullScreen' value='true'>");
        sb.Append("</param>");
        sb.Append("<param name='allowscriptaccess' value='always'></param>");
        sb.Append("<embed src='http://www.youtube.com/watch?v=SKRgktzRvZ0&feature=related' type='application/x-shockwave-flash' allowscriptaccess='always'                             allowfullscreen='true' width='425' height='344'></embed>");
        sb.Append("</object>");
        //string scr = @"<object width='320' height='240'> ";
        //scr = scr + @"<param name='movie' value='" + id + "'></param> ";
        //scr = scr + @"<param name='allowFullScreen' value='true'></param> ";
        //scr = scr + @"<param name='allowscriptaccess' value='always'></param> ";
        //scr = scr + @"<embed src='" + id + "' ";
        //scr = scr + @"type='application/x-shockwave-flash' allowscriptaccess='always' ";
        //scr = scr + @"allowfullscreen='true' width='320' height='240'> ";
        //scr = scr + @"</embed></object>";
        //return scr;
        return sb.ToString();
    }
}