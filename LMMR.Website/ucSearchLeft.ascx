﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucSearchLeft.ascx.cs" Inherits="ucSearchLeft" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<div class="left-inner-top">
    <asp:ScriptManager ID="script" runat="server" >
    </asp:ScriptManager>
    <div class="map">
        <a href="#">
            <img src="<%= SiteRootPath %>images/map.png" /></a>
    </div>
    <div class="logo">
        <a href="#">
            <img src="<%= SiteRootPath %>images/logo-front.png" /></a>
    </div>
    <!--left-form START HERE-->
    <div class="left-form">
        <div id="Country">
            <%--<select name="test">
                <option>Select Country</option>
                <option value="Country1">Country1</option>
                <option value="Country2">Country2</option>
                <option value="Country3">Country3</option>
            </select>--%>
            <asp:DropDownList ID="drpCountry" runat="server">
                <asp:ListItem Value="" Text="Select Country"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <div id="City">
            <asp:DropDownList ID="drpCity" runat="server">
                <asp:ListItem Value="" Text="Select City"></asp:ListItem>
            </asp:DropDownList>
            <%--<select name="test">
                <option>Select City</option>
                <option value="City1">City1</option>
                <option value="City2">City2</option>
                <option value="City3">City3</option>
            </select>--%>
        </div>
        <div id="Date" >
            <span style="float:left;margin-top:5px;"><asp:TextBox ID="txtDatePicker" runat="server" CssClass="dateinput" Text="dd/mm/yy" ></asp:TextBox><asp:CalendarExtender ID="CalendarExtender2" runat="server" 
                            TargetControlID="txtDatePicker" PopupButtonID="calFrom" Format="dd/MM/yy">
                        </asp:CalendarExtender></span><span style="float:left;margin-top:5px;"><input type="image" src="Images/cal-2.png" id="calFrom"/></span>
            <%--<input name="AnotherDate" class="dateinput">--%>
            <%--<input type="button" value="select" class="datebutton" onclick="displayDatePicker('AnotherDate', this);">--%>
        </div>
        <div id="Quantity-day">
            <div id="Quantity">
                <label>
                    Duration &nbsp;</label><asp:TextBox ID="txtDuration"  runat="server" Text="1" CssClass="inputbox"></asp:TextBox><%--<input type="text" value="1" name="quantity[]" id="quantity74"
                        class="inputbox">--%>
                <input type="button" onclick="var qty_el = document.getElementById('searchLeftTop1_txtDuration'); var qty = qty_el.value; if( !isNaN( qty )) qty_el.value++;return false;"
                    class="button_up">
                <input type="button" onclick="var qty_el = document.getElementById('searchLeftTop1_txtDuration'); var qty = qty_el.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) qty_el.value--;return false;"
                    class="button_down">
            </div>
            <div id="daysname">
                day(s)</div>
            <div id="Day">
                <asp:DropDownList ID="drpDayOption" runat="server" >
                    <asp:ListItem Value="" Text="Full Day"></asp:ListItem>
                </asp:DropDownList>
                <%--<select name="test">
                    <option>Full Day</option>
                    <option value="Day1">Day1</option>
                    <option value="Day2">Day2</option>
                    <option value="Day3">Day3</option>
                </select>--%>
            </div>
        </div>
        <div id="Participants">
            <label>
                Participants &nbsp;</label><asp:TextBox ID="txtParticipants" runat="server" value="1" CssClass="inputbox"></asp:TextBox><%--<input type="text" value="1" name="Participants[]" id="Participants74"
                    class="inputbox">--%>
            <input type="button" onclick="var qty_el = document.getElementById('searchLeftTop1_txtParticipants'); var qty = qty_el.value; if( !isNaN( qty )) qty_el.value++;return false;"
                class="button_up">
            <input type="button" onclick="var qty_el = document.getElementById('searchLeftTop1_txtParticipants'); var qty = qty_el.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) qty_el.value--;return false;"
                class="button_down">
        </div>
        <input type="button" value="Find your meeting room!" class="find-button" />
    </div>
    <!--left-form ENDS HERE-->
</div>
