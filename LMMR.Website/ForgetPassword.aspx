﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RegisterMaster.master" AutoEventWireup="true"
    CodeFile="ForgetPassword.aspx.cs" Inherits="ForgetPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="register-body">
        <div class="login-main1">
            <table width="100%" border="0" cellpadding="5" cellspacing="0">
                <tr>
                    <td width="60%" valign="top" align="center">
                        <asp:Panel ID="Panel1" if="loginpnl" runat="server" DefaultButton="btnLogin">
                            <table width="60%" border="0" cellpadding="3" cellspacing="0" align="center">
                                <tr>
                                    <td align="center" colspan="2" height="35" valign="top">
                                        <h2 class="register">
                                            <%= GetKeyResult("FORGOTPASSWORD")%></h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div id="divError" runat="server">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="res-leve2">
                                        <%= GetKeyResult("EMAILONLY")%><span style="color: Red"> *</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEmail" runat="server" CssClass="inputregister" TabIndex="1"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <div class="blue-button" style="float: left;">
                                            <asp:LinkButton ID="btnLogin" CssClass="login-btn" runat="server" OnClick="btnSubmit_Click"
                                                ValidationGroup="LOGIN" TabIndex="1"><%= GetKeyResult("SUBMIT")%></asp:LinkButton></div>
                                        <span style="float: left; font-weight: bolder; margin-top: 20px;">or</span><div class="blue-button"
                                            style="float: left;">
                                            <asp:LinkButton ID="btnReset" runat="server" CssClass="cancel-btn"><%= GetKeyResult("BACKTOLOGIN")%></asp:LinkButton></div>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                    <td width="40%" valign="top">
                        <table width="80%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" colspan="2" height="35" valign="top">
                                    <h2 class="register">
                                        <%= GetKeyResult("TOREGISTER")%></h2>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ul class="register">
                                        <li>
                                                <asp:LinkButton ID="companyLink" runat="server" OnClick="companyLink_Click" Enabled="true"><%= GetKeyResult("REGISTERASCLIENT")%></asp:LinkButton></li>
                                                <li><asp:LinkButton ID="userLink" runat="server" OnClick="userLink_Click" Enabled="true"><%= GetKeyResult("REGISTERASCLIENTPRIVATE")%></asp:LinkButton></li>
                                                </ul>
                                                <br /><br />
                                </td>

                            </tr>
                            
                            <tr>
                                <td align="center">
                                    <asp:LinkButton ID="agencyLink" runat="server" Enabled="true" 
                                        onclick="agencyLink_Click"><%= GetKeyResult("REGISTERASAGENCY")%></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <!-- end register-body-->
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#<%= btnLogin.ClientID %>").bind("click", function () {
                if (jQuery("#<%= divError.ClientID %>").hasClass("succesfuly")) {
                    jQuery("#<%= divError.ClientID %>").removeClass("succesfuly").addClass("error");
                }
                var isvalid = true;
                var errormessage = "";
                var Email = jQuery("#<%=txtEmail.ClientID %>").val();
                if (Email.length == 0) {
                    if (errormessage.length > 0) {
                        errormessage += "<br/>";
                    }
                    errormessage += '<%= GetKeyResultForJavaScript("EMAILIDERRORMESSAGE")%>';
                    isvalid = false;
                }
                else {
                    if (!validateEmail(Email)) {
                        if (errormessage.length > 0) {
                            errormessage += "<br/>";
                        }
                        errormessage += '<%= GetKeyResultForJavaScript("EMAILVALADITYERRORMESSAGE")%>';
                        isvalid = false;
                    }
                }
                if (!isvalid) {
                    jQuery("#<%= divError.ClientID %>").addClass("error").show();
                    jQuery("#<%= divError.ClientID %>").html(errormessage);
                    return false;
                }
                jQuery("#<%= divError.ClientID %>").html("");
                jQuery("#<%= divError.ClientID %>").hide();
            });
        });

        function validateEmail(txtEmail) {
            var a = txtEmail;
            var filter = /^[a-zA-Z0-9_.-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{1,4}$/;
            if (filter.test(a)) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
