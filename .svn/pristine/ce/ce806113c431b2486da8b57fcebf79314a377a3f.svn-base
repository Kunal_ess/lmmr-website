﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ServeyresultProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ServeyresultProviderBaseCore : EntityProviderBase<LMMR.Entities.Serveyresult, LMMR.Entities.ServeyresultKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.ServeyresultKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Serveyresult_ServeyAnswer key.
		///		FK_Serveyresult_ServeyAnswer Description: 
		/// </summary>
		/// <param name="_answerId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Serveyresult objects.</returns>
		public TList<Serveyresult> GetByAnswerId(System.Int32 _answerId)
		{
			int count = -1;
			return GetByAnswerId(_answerId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Serveyresult_ServeyAnswer key.
		///		FK_Serveyresult_ServeyAnswer Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_answerId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Serveyresult objects.</returns>
		/// <remarks></remarks>
		public TList<Serveyresult> GetByAnswerId(TransactionManager transactionManager, System.Int32 _answerId)
		{
			int count = -1;
			return GetByAnswerId(transactionManager, _answerId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Serveyresult_ServeyAnswer key.
		///		FK_Serveyresult_ServeyAnswer Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Serveyresult objects.</returns>
		public TList<Serveyresult> GetByAnswerId(TransactionManager transactionManager, System.Int32 _answerId, int start, int pageLength)
		{
			int count = -1;
			return GetByAnswerId(transactionManager, _answerId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Serveyresult_ServeyAnswer key.
		///		fkServeyresultServeyAnswer Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_answerId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Serveyresult objects.</returns>
		public TList<Serveyresult> GetByAnswerId(System.Int32 _answerId, int start, int pageLength)
		{
			int count =  -1;
			return GetByAnswerId(null, _answerId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Serveyresult_ServeyAnswer key.
		///		fkServeyresultServeyAnswer Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_answerId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Serveyresult objects.</returns>
		public TList<Serveyresult> GetByAnswerId(System.Int32 _answerId, int start, int pageLength,out int count)
		{
			return GetByAnswerId(null, _answerId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Serveyresult_ServeyAnswer key.
		///		FK_Serveyresult_ServeyAnswer Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.Serveyresult objects.</returns>
		public abstract TList<Serveyresult> GetByAnswerId(TransactionManager transactionManager, System.Int32 _answerId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Serveyresult_ServeyQuestion key.
		///		FK_Serveyresult_ServeyQuestion Description: 
		/// </summary>
		/// <param name="_questionId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Serveyresult objects.</returns>
		public TList<Serveyresult> GetByQuestionId(System.Int32 _questionId)
		{
			int count = -1;
			return GetByQuestionId(_questionId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Serveyresult_ServeyQuestion key.
		///		FK_Serveyresult_ServeyQuestion Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.Serveyresult objects.</returns>
		/// <remarks></remarks>
		public TList<Serveyresult> GetByQuestionId(TransactionManager transactionManager, System.Int32 _questionId)
		{
			int count = -1;
			return GetByQuestionId(transactionManager, _questionId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Serveyresult_ServeyQuestion key.
		///		FK_Serveyresult_ServeyQuestion Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Serveyresult objects.</returns>
		public TList<Serveyresult> GetByQuestionId(TransactionManager transactionManager, System.Int32 _questionId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionId(transactionManager, _questionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Serveyresult_ServeyQuestion key.
		///		fkServeyresultServeyQuestion Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Serveyresult objects.</returns>
		public TList<Serveyresult> GetByQuestionId(System.Int32 _questionId, int start, int pageLength)
		{
			int count =  -1;
			return GetByQuestionId(null, _questionId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Serveyresult_ServeyQuestion key.
		///		fkServeyresultServeyQuestion Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.Serveyresult objects.</returns>
		public TList<Serveyresult> GetByQuestionId(System.Int32 _questionId, int start, int pageLength,out int count)
		{
			return GetByQuestionId(null, _questionId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Serveyresult_ServeyQuestion key.
		///		FK_Serveyresult_ServeyQuestion Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.Serveyresult objects.</returns>
		public abstract TList<Serveyresult> GetByQuestionId(TransactionManager transactionManager, System.Int32 _questionId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.Serveyresult Get(TransactionManager transactionManager, LMMR.Entities.ServeyresultKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Serveyresult index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Serveyresult"/> class.</returns>
		public LMMR.Entities.Serveyresult GetById(System.Int32 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Serveyresult index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Serveyresult"/> class.</returns>
		public LMMR.Entities.Serveyresult GetById(System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Serveyresult index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Serveyresult"/> class.</returns>
		public LMMR.Entities.Serveyresult GetById(TransactionManager transactionManager, System.Int32 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Serveyresult index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Serveyresult"/> class.</returns>
		public LMMR.Entities.Serveyresult GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Serveyresult index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Serveyresult"/> class.</returns>
		public LMMR.Entities.Serveyresult GetById(System.Int32 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Serveyresult index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.Serveyresult"/> class.</returns>
		public abstract LMMR.Entities.Serveyresult GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Serveyresult&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Serveyresult&gt;"/></returns>
		public static TList<Serveyresult> Fill(IDataReader reader, TList<Serveyresult> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.Serveyresult c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Serveyresult")
					.Append("|").Append((System.Int32)reader[((int)ServeyresultColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Serveyresult>(
					key.ToString(), // EntityTrackingKey
					"Serveyresult",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.Serveyresult();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int32)reader[((int)ServeyresultColumn.Id - 1)];
					c.QuestionId = (System.Int32)reader[((int)ServeyresultColumn.QuestionId - 1)];
					c.AnswerId = (System.Int32)reader[((int)ServeyresultColumn.AnswerId - 1)];
					c.Rating = (reader.IsDBNull(((int)ServeyresultColumn.Rating - 1)))?null:(System.Int32?)reader[((int)ServeyresultColumn.Rating - 1)];
                    c.ServeyId = (reader.IsDBNull(((int)ServeyresultColumn.ServeyId - 1))) ? 0 : (System.Int32)reader[((int)ServeyresultColumn.ServeyId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Serveyresult"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Serveyresult"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.Serveyresult entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int32)reader[((int)ServeyresultColumn.Id - 1)];
			entity.QuestionId = (System.Int32)reader[((int)ServeyresultColumn.QuestionId - 1)];
			entity.AnswerId = (System.Int32)reader[((int)ServeyresultColumn.AnswerId - 1)];
			entity.Rating = (reader.IsDBNull(((int)ServeyresultColumn.Rating - 1)))?null:(System.Int32?)reader[((int)ServeyresultColumn.Rating - 1)];
			entity.ServeyId = (System.Int64)reader[((int)ServeyresultColumn.ServeyId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.Serveyresult"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Serveyresult"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.Serveyresult entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int32)dataRow["Id"];
			entity.QuestionId = (System.Int32)dataRow["QuestionID"];
			entity.AnswerId = (System.Int32)dataRow["AnswerID"];
			entity.Rating = Convert.IsDBNull(dataRow["Rating"]) ? null : (System.Int32?)dataRow["Rating"];
			entity.ServeyId = (System.Int64)dataRow["ServeyID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.Serveyresult"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.Serveyresult Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.Serveyresult entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region AnswerIdSource	
			if (CanDeepLoad(entity, "ServeyAnswer|AnswerIdSource", deepLoadType, innerList) 
				&& entity.AnswerIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.AnswerId;
				ServeyAnswer tmpEntity = EntityManager.LocateEntity<ServeyAnswer>(EntityLocator.ConstructKeyFromPkItems(typeof(ServeyAnswer), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AnswerIdSource = tmpEntity;
				else
					entity.AnswerIdSource = DataRepository.ServeyAnswerProvider.GetByAnswerId(transactionManager, entity.AnswerId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AnswerIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AnswerIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ServeyAnswerProvider.DeepLoad(transactionManager, entity.AnswerIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AnswerIdSource

			#region QuestionIdSource	
			if (CanDeepLoad(entity, "ServeyQuestion|QuestionIdSource", deepLoadType, innerList) 
				&& entity.QuestionIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.QuestionId;
				ServeyQuestion tmpEntity = EntityManager.LocateEntity<ServeyQuestion>(EntityLocator.ConstructKeyFromPkItems(typeof(ServeyQuestion), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.QuestionIdSource = tmpEntity;
				else
					entity.QuestionIdSource = DataRepository.ServeyQuestionProvider.GetByQuestionId(transactionManager, entity.QuestionId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.QuestionIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ServeyQuestionProvider.DeepLoad(transactionManager, entity.QuestionIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion QuestionIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.Serveyresult object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.Serveyresult instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.Serveyresult Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.Serveyresult entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region AnswerIdSource
			if (CanDeepSave(entity, "ServeyAnswer|AnswerIdSource", deepSaveType, innerList) 
				&& entity.AnswerIdSource != null)
			{
				DataRepository.ServeyAnswerProvider.Save(transactionManager, entity.AnswerIdSource);
				entity.AnswerId = entity.AnswerIdSource.AnswerId;
			}
			#endregion 
			
			#region QuestionIdSource
			if (CanDeepSave(entity, "ServeyQuestion|QuestionIdSource", deepSaveType, innerList) 
				&& entity.QuestionIdSource != null)
			{
				DataRepository.ServeyQuestionProvider.Save(transactionManager, entity.QuestionIdSource);
				entity.QuestionId = entity.QuestionIdSource.QuestionId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ServeyresultChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.Serveyresult</c>
	///</summary>
	public enum ServeyresultChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>ServeyAnswer</c> at AnswerIdSource
		///</summary>
		[ChildEntityType(typeof(ServeyAnswer))]
		ServeyAnswer,
			
		///<summary>
		/// Composite Property for <c>ServeyQuestion</c> at QuestionIdSource
		///</summary>
		[ChildEntityType(typeof(ServeyQuestion))]
		ServeyQuestion,
		}
	
	#endregion ServeyresultChildEntityTypes
	
	#region ServeyresultFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ServeyresultColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Serveyresult"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ServeyresultFilterBuilder : SqlFilterBuilder<ServeyresultColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ServeyresultFilterBuilder class.
		/// </summary>
		public ServeyresultFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ServeyresultFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ServeyresultFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ServeyresultFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ServeyresultFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ServeyresultFilterBuilder
	
	#region ServeyresultParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ServeyresultColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Serveyresult"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ServeyresultParameterBuilder : ParameterizedSqlFilterBuilder<ServeyresultColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ServeyresultParameterBuilder class.
		/// </summary>
		public ServeyresultParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ServeyresultParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ServeyresultParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ServeyresultParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ServeyresultParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ServeyresultParameterBuilder
	
	#region ServeyresultSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ServeyresultColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Serveyresult"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ServeyresultSortBuilder : SqlSortBuilder<ServeyresultColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ServeyresultSqlSortBuilder class.
		/// </summary>
		public ServeyresultSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ServeyresultSortBuilder
	
} // end namespace
