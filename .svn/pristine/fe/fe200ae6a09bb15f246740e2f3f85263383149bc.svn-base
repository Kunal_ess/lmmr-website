﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="PackageByHotelTrailProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class PackageByHotelTrailProviderBaseCore : EntityProviderBase<LMMR.Entities.PackageByHotelTrail, LMMR.Entities.PackageByHotelTrailKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.PackageByHotelTrailKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_Trail_PackageByHotel key.
		///		FK_PackageByHotel_Trail_PackageByHotel Description: 
		/// </summary>
		/// <param name="_packagebyHotelId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotelTrail objects.</returns>
		public TList<PackageByHotelTrail> GetByPackagebyHotelId(System.Int64? _packagebyHotelId)
		{
			int count = -1;
			return GetByPackagebyHotelId(_packagebyHotelId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_Trail_PackageByHotel key.
		///		FK_PackageByHotel_Trail_PackageByHotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_packagebyHotelId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotelTrail objects.</returns>
		/// <remarks></remarks>
		public TList<PackageByHotelTrail> GetByPackagebyHotelId(TransactionManager transactionManager, System.Int64? _packagebyHotelId)
		{
			int count = -1;
			return GetByPackagebyHotelId(transactionManager, _packagebyHotelId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_Trail_PackageByHotel key.
		///		FK_PackageByHotel_Trail_PackageByHotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_packagebyHotelId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotelTrail objects.</returns>
		public TList<PackageByHotelTrail> GetByPackagebyHotelId(TransactionManager transactionManager, System.Int64? _packagebyHotelId, int start, int pageLength)
		{
			int count = -1;
			return GetByPackagebyHotelId(transactionManager, _packagebyHotelId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_Trail_PackageByHotel key.
		///		fkPackageByHotelTrailPackageByHotel Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_packagebyHotelId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotelTrail objects.</returns>
		public TList<PackageByHotelTrail> GetByPackagebyHotelId(System.Int64? _packagebyHotelId, int start, int pageLength)
		{
			int count =  -1;
			return GetByPackagebyHotelId(null, _packagebyHotelId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_Trail_PackageByHotel key.
		///		fkPackageByHotelTrailPackageByHotel Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_packagebyHotelId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotelTrail objects.</returns>
		public TList<PackageByHotelTrail> GetByPackagebyHotelId(System.Int64? _packagebyHotelId, int start, int pageLength,out int count)
		{
			return GetByPackagebyHotelId(null, _packagebyHotelId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_PackageByHotel_Trail_PackageByHotel key.
		///		FK_PackageByHotel_Trail_PackageByHotel Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_packagebyHotelId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.PackageByHotelTrail objects.</returns>
		public abstract TList<PackageByHotelTrail> GetByPackagebyHotelId(TransactionManager transactionManager, System.Int64? _packagebyHotelId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.PackageByHotelTrail Get(TransactionManager transactionManager, LMMR.Entities.PackageByHotelTrailKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_PackageByHotel_Trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageByHotelTrail"/> class.</returns>
		public LMMR.Entities.PackageByHotelTrail GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_PackageByHotel_Trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageByHotelTrail"/> class.</returns>
		public LMMR.Entities.PackageByHotelTrail GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_PackageByHotel_Trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageByHotelTrail"/> class.</returns>
		public LMMR.Entities.PackageByHotelTrail GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_PackageByHotel_Trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageByHotelTrail"/> class.</returns>
		public LMMR.Entities.PackageByHotelTrail GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_PackageByHotel_Trail index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageByHotelTrail"/> class.</returns>
		public LMMR.Entities.PackageByHotelTrail GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_PackageByHotel_Trail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.PackageByHotelTrail"/> class.</returns>
		public abstract LMMR.Entities.PackageByHotelTrail GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;PackageByHotelTrail&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;PackageByHotelTrail&gt;"/></returns>
		public static TList<PackageByHotelTrail> Fill(IDataReader reader, TList<PackageByHotelTrail> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.PackageByHotelTrail c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("PackageByHotelTrail")
					.Append("|").Append((System.Int64)reader[((int)PackageByHotelTrailColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<PackageByHotelTrail>(
					key.ToString(), // EntityTrackingKey
					"PackageByHotelTrail",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.PackageByHotelTrail();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)PackageByHotelTrailColumn.Id - 1)];
					c.PackagebyHotelId = (reader.IsDBNull(((int)PackageByHotelTrailColumn.PackagebyHotelId - 1)))?null:(System.Int64?)reader[((int)PackageByHotelTrailColumn.PackagebyHotelId - 1)];
					c.PackageId = (System.Int64)reader[((int)PackageByHotelTrailColumn.PackageId - 1)];
					c.HotelId = (System.Int64)reader[((int)PackageByHotelTrailColumn.HotelId - 1)];
					c.HalfdayPrice = (reader.IsDBNull(((int)PackageByHotelTrailColumn.HalfdayPrice - 1)))?null:(System.Decimal?)reader[((int)PackageByHotelTrailColumn.HalfdayPrice - 1)];
					c.FulldayPrice = (reader.IsDBNull(((int)PackageByHotelTrailColumn.FulldayPrice - 1)))?null:(System.Decimal?)reader[((int)PackageByHotelTrailColumn.FulldayPrice - 1)];
					c.IsOnline = (System.Boolean)reader[((int)PackageByHotelTrailColumn.IsOnline - 1)];
					c.ItemId = (System.Int64)reader[((int)PackageByHotelTrailColumn.ItemId - 1)];
					c.IsComplementary = (System.Boolean)reader[((int)PackageByHotelTrailColumn.IsComplementary - 1)];
					c.UpdatedBy = (reader.IsDBNull(((int)PackageByHotelTrailColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)PackageByHotelTrailColumn.UpdatedBy - 1)];
					c.UpdateDate = (reader.IsDBNull(((int)PackageByHotelTrailColumn.UpdateDate - 1)))?null:(System.DateTime?)reader[((int)PackageByHotelTrailColumn.UpdateDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.PackageByHotelTrail"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.PackageByHotelTrail"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.PackageByHotelTrail entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)PackageByHotelTrailColumn.Id - 1)];
			entity.PackagebyHotelId = (reader.IsDBNull(((int)PackageByHotelTrailColumn.PackagebyHotelId - 1)))?null:(System.Int64?)reader[((int)PackageByHotelTrailColumn.PackagebyHotelId - 1)];
			entity.PackageId = (System.Int64)reader[((int)PackageByHotelTrailColumn.PackageId - 1)];
			entity.HotelId = (System.Int64)reader[((int)PackageByHotelTrailColumn.HotelId - 1)];
			entity.HalfdayPrice = (reader.IsDBNull(((int)PackageByHotelTrailColumn.HalfdayPrice - 1)))?null:(System.Decimal?)reader[((int)PackageByHotelTrailColumn.HalfdayPrice - 1)];
			entity.FulldayPrice = (reader.IsDBNull(((int)PackageByHotelTrailColumn.FulldayPrice - 1)))?null:(System.Decimal?)reader[((int)PackageByHotelTrailColumn.FulldayPrice - 1)];
			entity.IsOnline = (System.Boolean)reader[((int)PackageByHotelTrailColumn.IsOnline - 1)];
			entity.ItemId = (System.Int64)reader[((int)PackageByHotelTrailColumn.ItemId - 1)];
			entity.IsComplementary = (System.Boolean)reader[((int)PackageByHotelTrailColumn.IsComplementary - 1)];
			entity.UpdatedBy = (reader.IsDBNull(((int)PackageByHotelTrailColumn.UpdatedBy - 1)))?null:(System.Int64?)reader[((int)PackageByHotelTrailColumn.UpdatedBy - 1)];
			entity.UpdateDate = (reader.IsDBNull(((int)PackageByHotelTrailColumn.UpdateDate - 1)))?null:(System.DateTime?)reader[((int)PackageByHotelTrailColumn.UpdateDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.PackageByHotelTrail"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.PackageByHotelTrail"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.PackageByHotelTrail entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.PackagebyHotelId = Convert.IsDBNull(dataRow["PackagebyHotelId"]) ? null : (System.Int64?)dataRow["PackagebyHotelId"];
			entity.PackageId = (System.Int64)dataRow["PackageID"];
			entity.HotelId = (System.Int64)dataRow["HotelId"];
			entity.HalfdayPrice = Convert.IsDBNull(dataRow["HalfdayPrice"]) ? null : (System.Decimal?)dataRow["HalfdayPrice"];
			entity.FulldayPrice = Convert.IsDBNull(dataRow["FulldayPrice"]) ? null : (System.Decimal?)dataRow["FulldayPrice"];
			entity.IsOnline = (System.Boolean)dataRow["IsOnline"];
			entity.ItemId = (System.Int64)dataRow["ItemId"];
			entity.IsComplementary = (System.Boolean)dataRow["IsComplementary"];
			entity.UpdatedBy = Convert.IsDBNull(dataRow["UpdatedBy"]) ? null : (System.Int64?)dataRow["UpdatedBy"];
			entity.UpdateDate = Convert.IsDBNull(dataRow["UpdateDate"]) ? null : (System.DateTime?)dataRow["UpdateDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.PackageByHotelTrail"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.PackageByHotelTrail Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.PackageByHotelTrail entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region PackagebyHotelIdSource	
			if (CanDeepLoad(entity, "PackageByHotel|PackagebyHotelIdSource", deepLoadType, innerList) 
				&& entity.PackagebyHotelIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.PackagebyHotelId ?? (long)0);
				PackageByHotel tmpEntity = EntityManager.LocateEntity<PackageByHotel>(EntityLocator.ConstructKeyFromPkItems(typeof(PackageByHotel), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PackagebyHotelIdSource = tmpEntity;
				else
					entity.PackagebyHotelIdSource = DataRepository.PackageByHotelProvider.GetById(transactionManager, (entity.PackagebyHotelId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PackagebyHotelIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PackagebyHotelIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PackageByHotelProvider.DeepLoad(transactionManager, entity.PackagebyHotelIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PackagebyHotelIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.PackageByHotelTrail object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.PackageByHotelTrail instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.PackageByHotelTrail Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.PackageByHotelTrail entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region PackagebyHotelIdSource
			if (CanDeepSave(entity, "PackageByHotel|PackagebyHotelIdSource", deepSaveType, innerList) 
				&& entity.PackagebyHotelIdSource != null)
			{
				DataRepository.PackageByHotelProvider.Save(transactionManager, entity.PackagebyHotelIdSource);
				entity.PackagebyHotelId = entity.PackagebyHotelIdSource.Id;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region PackageByHotelTrailChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.PackageByHotelTrail</c>
	///</summary>
	public enum PackageByHotelTrailChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>PackageByHotel</c> at PackagebyHotelIdSource
		///</summary>
		[ChildEntityType(typeof(PackageByHotel))]
		PackageByHotel,
		}
	
	#endregion PackageByHotelTrailChildEntityTypes
	
	#region PackageByHotelTrailFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;PackageByHotelTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PackageByHotelTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PackageByHotelTrailFilterBuilder : SqlFilterBuilder<PackageByHotelTrailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageByHotelTrailFilterBuilder class.
		/// </summary>
		public PackageByHotelTrailFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PackageByHotelTrailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PackageByHotelTrailFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PackageByHotelTrailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PackageByHotelTrailFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PackageByHotelTrailFilterBuilder
	
	#region PackageByHotelTrailParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;PackageByHotelTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PackageByHotelTrail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PackageByHotelTrailParameterBuilder : ParameterizedSqlFilterBuilder<PackageByHotelTrailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageByHotelTrailParameterBuilder class.
		/// </summary>
		public PackageByHotelTrailParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PackageByHotelTrailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PackageByHotelTrailParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PackageByHotelTrailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PackageByHotelTrailParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PackageByHotelTrailParameterBuilder
	
	#region PackageByHotelTrailSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;PackageByHotelTrailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PackageByHotelTrail"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class PackageByHotelTrailSortBuilder : SqlSortBuilder<PackageByHotelTrailColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PackageByHotelTrailSqlSortBuilder class.
		/// </summary>
		public PackageByHotelTrailSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion PackageByHotelTrailSortBuilder
	
} // end namespace
