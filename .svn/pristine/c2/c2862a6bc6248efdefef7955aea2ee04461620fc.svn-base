using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// 
/// </summary>
public class GridViewExportUtil
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="fileName"></param>
    /// <param name="gv"></param>
    public static void Export(string fileName, GridView gv)
    {
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.AddHeader(
            "content-disposition", string.Format("attachment; filename={0}", fileName));
        HttpContext.Current.Response.ContentType = "application/ms-excel";

        using (StringWriter sw = new StringWriter())
        {
            using (HtmlTextWriter htw = new HtmlTextWriter(sw))
            {
                //  Create a form to contain the grid
                Table table = new Table();

                //  add the header row to the table
                if (gv.HeaderRow != null)
                {
                    GridViewExportUtil.PrepareControlForExport(gv.HeaderRow);
                    table.Rows.Add(gv.HeaderRow);
                }

                //  add each of the data rows to the table
                foreach (GridViewRow row in gv.Rows)
                {
                    GridViewExportUtil.PrepareControlForExport(row);
                    table.Rows.Add(row);
                }

                //  add the footer row to the table
                if (gv.FooterRow != null)
                {
                    GridViewExportUtil.PrepareControlForExport(gv.FooterRow);
                    table.Rows.Add(gv.FooterRow);
                }

                //  render the table into the htmlwriter
                table.RenderControl(htw);

                //  render the htmlwriter into the response
                HttpContext.Current.Response.Write(sw.ToString());
                HttpContext.Current.Response.End();
            }
        }
    }

    /// <summary>
    /// Replace any of the contained controls with literals
    /// </summary>
    /// <param name="control"></param>
    private static void PrepareControlForExport(Control control)
    {
        for (int i = 0; i < control.Controls.Count; i++)
        {
            Control current = control.Controls[i];
            if (current is LinkButton)
            {
                if ((current as LinkButton).Text != "Attach Credit Note")
                {
                    if ((current as LinkButton).Visible == false)
                    {
                        control.Controls.Remove(current);
                    }
                    else
                    {
                        control.Controls.Remove(current);
                        control.Controls.AddAt(i, new LiteralControl((current as LinkButton).Text));
                    }
                }
                else
                {
                    control.Controls.Remove(current);
                }
            }
            else if (current is ImageButton)
            {
                control.Controls.Remove(current);
                control.Controls.AddAt(i, new LiteralControl((current as ImageButton).AlternateText));
            }
            else if (current is HyperLink)
            {
                if ((current as HyperLink).Visible == true)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as HyperLink).Text = "Yes"));
                }
                else
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as HyperLink).Text = "No"));
                }
            }
            else if (current is DropDownList)
            {
                control.Controls.Remove(current);
                if ((current as DropDownList).ID == "drpPeriod")
                {
                    control.Controls.AddAt(i, new LiteralControl((current as DropDownList).SelectedItem.Text = "Period"));
                }
                else if((current as DropDownList).ID == "drpHotelName")
                {
                    control.Controls.AddAt(i, new LiteralControl((current as DropDownList).SelectedItem.Text = "Hotel Name"));
                }
            }
            else if (current is CheckBox)
            {
                control.Controls.Remove(current);
            }

            else if ((current is Image) || (current is Label))
            {
                if (current is Image)
                {
                    if ((current as Image).Visible == true)
                    {
                        control.Controls.Remove(current);
                        if ((current as Image).ID == "verifiedIMG")
                        {
                            control.Controls.AddAt(i, new LiteralControl((current as Image).AlternateText = "Yes"));
                        }
                        else
                        {
                            control.Controls.AddAt(i, new LiteralControl((current as Image).AlternateText = "Paid"));
                        }
                    }
                    else
                    {
                        control.Controls.Remove(current);
                        control.Controls.Remove(current);
                        if ((current as Image).ID == "verifiedIMG")
                        {
                            control.Controls.AddAt(i, new LiteralControl((current as Image).AlternateText = "No"));
                        }
                        else
                        {
                            control.Controls.AddAt(i, new LiteralControl((current as Image).AlternateText = ""));
                        }
                    }
                }
            }


            if (current.HasControls())
            {
                GridViewExportUtil.PrepareControlForExport(current);
            }
        }
    }
}
