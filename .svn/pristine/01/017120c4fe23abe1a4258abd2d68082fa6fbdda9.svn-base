﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ViewReportCancelationProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ViewReportCancelationProviderBaseCore : EntityViewProviderBase<ViewReportCancelation>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ViewReportCancelation&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ViewReportCancelation&gt;"/></returns>
		protected static VList&lt;ViewReportCancelation&gt; Fill(DataSet dataSet, VList<ViewReportCancelation> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ViewReportCancelation>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ViewReportCancelation&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ViewReportCancelation>"/></returns>
		protected static VList&lt;ViewReportCancelation&gt; Fill(DataTable dataTable, VList<ViewReportCancelation> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ViewReportCancelation c = new ViewReportCancelation();
					c.HotelName = (Convert.IsDBNull(row["HotelName"]))?string.Empty:(System.String)row["HotelName"];
					c.HotelId = (Convert.IsDBNull(row["HotelId"]))?(long)0:(System.Int64)row["HotelId"];
					c.CountryId = (Convert.IsDBNull(row["CountryID"]))?(long)0:(System.Int64)row["CountryID"];
					c.CountryName = (Convert.IsDBNull(row["CountryName"]))?string.Empty:(System.String)row["CountryName"];
					c.CityId = (Convert.IsDBNull(row["CityID"]))?(long)0:(System.Int64)row["CityID"];
					c.City = (Convert.IsDBNull(row["City"]))?string.Empty:(System.String)row["City"];
					c.BookingDate = (Convert.IsDBNull(row["BookingDate"]))?DateTime.MinValue:(System.DateTime?)row["BookingDate"];
					c.CancelBookByClient = (Convert.IsDBNull(row["CancelBookByClient"]))?(int)0:(System.Int32?)row["CancelBookByClient"];
					c.CancelBookByOperator = (Convert.IsDBNull(row["CancelBookByOperator"]))?(int)0:(System.Int32?)row["CancelBookByOperator"];
					c.CancelReqByOperator = (Convert.IsDBNull(row["CancelReqByOperator"]))?(int)0:(System.Int32?)row["CancelReqByOperator"];
					c.CancelReqByHotel = (Convert.IsDBNull(row["CancelReqByHotel"]))?(int)0:(System.Int32?)row["CancelReqByHotel"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ViewReportCancelation&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ViewReportCancelation&gt;"/></returns>
		protected VList<ViewReportCancelation> Fill(IDataReader reader, VList<ViewReportCancelation> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ViewReportCancelation entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ViewReportCancelation>("ViewReportCancelation",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ViewReportCancelation();
					}
					
					entity.SuppressEntityEvents = true;

					entity.HotelName = (reader.IsDBNull(((int)ViewReportCancelationColumn.HotelName)))?null:(System.String)reader[((int)ViewReportCancelationColumn.HotelName)];
					//entity.HotelName = (Convert.IsDBNull(reader["HotelName"]))?string.Empty:(System.String)reader["HotelName"];
					entity.HotelId = (System.Int64)reader[((int)ViewReportCancelationColumn.HotelId)];
					//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
					entity.CountryId = (System.Int64)reader[((int)ViewReportCancelationColumn.CountryId)];
					//entity.CountryId = (Convert.IsDBNull(reader["CountryID"]))?(long)0:(System.Int64)reader["CountryID"];
					entity.CountryName = (reader.IsDBNull(((int)ViewReportCancelationColumn.CountryName)))?null:(System.String)reader[((int)ViewReportCancelationColumn.CountryName)];
					//entity.CountryName = (Convert.IsDBNull(reader["CountryName"]))?string.Empty:(System.String)reader["CountryName"];
					entity.CityId = (System.Int64)reader[((int)ViewReportCancelationColumn.CityId)];
					//entity.CityId = (Convert.IsDBNull(reader["CityID"]))?(long)0:(System.Int64)reader["CityID"];
					entity.City = (reader.IsDBNull(((int)ViewReportCancelationColumn.City)))?null:(System.String)reader[((int)ViewReportCancelationColumn.City)];
					//entity.City = (Convert.IsDBNull(reader["City"]))?string.Empty:(System.String)reader["City"];
					entity.BookingDate = (reader.IsDBNull(((int)ViewReportCancelationColumn.BookingDate)))?null:(System.DateTime?)reader[((int)ViewReportCancelationColumn.BookingDate)];
					//entity.BookingDate = (Convert.IsDBNull(reader["BookingDate"]))?DateTime.MinValue:(System.DateTime?)reader["BookingDate"];
					entity.CancelBookByClient = (reader.IsDBNull(((int)ViewReportCancelationColumn.CancelBookByClient)))?null:(System.Int32?)reader[((int)ViewReportCancelationColumn.CancelBookByClient)];
					//entity.CancelBookByClient = (Convert.IsDBNull(reader["CancelBookByClient"]))?(int)0:(System.Int32?)reader["CancelBookByClient"];
					entity.CancelBookByOperator = (reader.IsDBNull(((int)ViewReportCancelationColumn.CancelBookByOperator)))?null:(System.Int32?)reader[((int)ViewReportCancelationColumn.CancelBookByOperator)];
					//entity.CancelBookByOperator = (Convert.IsDBNull(reader["CancelBookByOperator"]))?(int)0:(System.Int32?)reader["CancelBookByOperator"];
					entity.CancelReqByOperator = (reader.IsDBNull(((int)ViewReportCancelationColumn.CancelReqByOperator)))?null:(System.Int32?)reader[((int)ViewReportCancelationColumn.CancelReqByOperator)];
					//entity.CancelReqByOperator = (Convert.IsDBNull(reader["CancelReqByOperator"]))?(int)0:(System.Int32?)reader["CancelReqByOperator"];
					entity.CancelReqByHotel = (reader.IsDBNull(((int)ViewReportCancelationColumn.CancelReqByHotel)))?null:(System.Int32?)reader[((int)ViewReportCancelationColumn.CancelReqByHotel)];
					//entity.CancelReqByHotel = (Convert.IsDBNull(reader["CancelReqByHotel"]))?(int)0:(System.Int32?)reader["CancelReqByHotel"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ViewReportCancelation"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewReportCancelation"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ViewReportCancelation entity)
		{
			reader.Read();
			entity.HotelName = (reader.IsDBNull(((int)ViewReportCancelationColumn.HotelName)))?null:(System.String)reader[((int)ViewReportCancelationColumn.HotelName)];
			//entity.HotelName = (Convert.IsDBNull(reader["HotelName"]))?string.Empty:(System.String)reader["HotelName"];
			entity.HotelId = (System.Int64)reader[((int)ViewReportCancelationColumn.HotelId)];
			//entity.HotelId = (Convert.IsDBNull(reader["HotelId"]))?(long)0:(System.Int64)reader["HotelId"];
			entity.CountryId = (System.Int64)reader[((int)ViewReportCancelationColumn.CountryId)];
			//entity.CountryId = (Convert.IsDBNull(reader["CountryID"]))?(long)0:(System.Int64)reader["CountryID"];
			entity.CountryName = (reader.IsDBNull(((int)ViewReportCancelationColumn.CountryName)))?null:(System.String)reader[((int)ViewReportCancelationColumn.CountryName)];
			//entity.CountryName = (Convert.IsDBNull(reader["CountryName"]))?string.Empty:(System.String)reader["CountryName"];
			entity.CityId = (System.Int64)reader[((int)ViewReportCancelationColumn.CityId)];
			//entity.CityId = (Convert.IsDBNull(reader["CityID"]))?(long)0:(System.Int64)reader["CityID"];
			entity.City = (reader.IsDBNull(((int)ViewReportCancelationColumn.City)))?null:(System.String)reader[((int)ViewReportCancelationColumn.City)];
			//entity.City = (Convert.IsDBNull(reader["City"]))?string.Empty:(System.String)reader["City"];
			entity.BookingDate = (reader.IsDBNull(((int)ViewReportCancelationColumn.BookingDate)))?null:(System.DateTime?)reader[((int)ViewReportCancelationColumn.BookingDate)];
			//entity.BookingDate = (Convert.IsDBNull(reader["BookingDate"]))?DateTime.MinValue:(System.DateTime?)reader["BookingDate"];
			entity.CancelBookByClient = (reader.IsDBNull(((int)ViewReportCancelationColumn.CancelBookByClient)))?null:(System.Int32?)reader[((int)ViewReportCancelationColumn.CancelBookByClient)];
			//entity.CancelBookByClient = (Convert.IsDBNull(reader["CancelBookByClient"]))?(int)0:(System.Int32?)reader["CancelBookByClient"];
			entity.CancelBookByOperator = (reader.IsDBNull(((int)ViewReportCancelationColumn.CancelBookByOperator)))?null:(System.Int32?)reader[((int)ViewReportCancelationColumn.CancelBookByOperator)];
			//entity.CancelBookByOperator = (Convert.IsDBNull(reader["CancelBookByOperator"]))?(int)0:(System.Int32?)reader["CancelBookByOperator"];
			entity.CancelReqByOperator = (reader.IsDBNull(((int)ViewReportCancelationColumn.CancelReqByOperator)))?null:(System.Int32?)reader[((int)ViewReportCancelationColumn.CancelReqByOperator)];
			//entity.CancelReqByOperator = (Convert.IsDBNull(reader["CancelReqByOperator"]))?(int)0:(System.Int32?)reader["CancelReqByOperator"];
			entity.CancelReqByHotel = (reader.IsDBNull(((int)ViewReportCancelationColumn.CancelReqByHotel)))?null:(System.Int32?)reader[((int)ViewReportCancelationColumn.CancelReqByHotel)];
			//entity.CancelReqByHotel = (Convert.IsDBNull(reader["CancelReqByHotel"]))?(int)0:(System.Int32?)reader["CancelReqByHotel"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ViewReportCancelation"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ViewReportCancelation"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ViewReportCancelation entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.HotelName = (Convert.IsDBNull(dataRow["HotelName"]))?string.Empty:(System.String)dataRow["HotelName"];
			entity.HotelId = (Convert.IsDBNull(dataRow["HotelId"]))?(long)0:(System.Int64)dataRow["HotelId"];
			entity.CountryId = (Convert.IsDBNull(dataRow["CountryID"]))?(long)0:(System.Int64)dataRow["CountryID"];
			entity.CountryName = (Convert.IsDBNull(dataRow["CountryName"]))?string.Empty:(System.String)dataRow["CountryName"];
			entity.CityId = (Convert.IsDBNull(dataRow["CityID"]))?(long)0:(System.Int64)dataRow["CityID"];
			entity.City = (Convert.IsDBNull(dataRow["City"]))?string.Empty:(System.String)dataRow["City"];
			entity.BookingDate = (Convert.IsDBNull(dataRow["BookingDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["BookingDate"];
			entity.CancelBookByClient = (Convert.IsDBNull(dataRow["CancelBookByClient"]))?(int)0:(System.Int32?)dataRow["CancelBookByClient"];
			entity.CancelBookByOperator = (Convert.IsDBNull(dataRow["CancelBookByOperator"]))?(int)0:(System.Int32?)dataRow["CancelBookByOperator"];
			entity.CancelReqByOperator = (Convert.IsDBNull(dataRow["CancelReqByOperator"]))?(int)0:(System.Int32?)dataRow["CancelReqByOperator"];
			entity.CancelReqByHotel = (Convert.IsDBNull(dataRow["CancelReqByHotel"]))?(int)0:(System.Int32?)dataRow["CancelReqByHotel"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ViewReportCancelationFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportCancelation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportCancelationFilterBuilder : SqlFilterBuilder<ViewReportCancelationColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportCancelationFilterBuilder class.
		/// </summary>
		public ViewReportCancelationFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportCancelationFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportCancelationFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportCancelationFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportCancelationFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportCancelationFilterBuilder

	#region ViewReportCancelationParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportCancelation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ViewReportCancelationParameterBuilder : ParameterizedSqlFilterBuilder<ViewReportCancelationColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportCancelationParameterBuilder class.
		/// </summary>
		public ViewReportCancelationParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ViewReportCancelationParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ViewReportCancelationParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ViewReportCancelationParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ViewReportCancelationParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ViewReportCancelationParameterBuilder
	
	#region ViewReportCancelationSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ViewReportCancelation"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ViewReportCancelationSortBuilder : SqlSortBuilder<ViewReportCancelationColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ViewReportCancelationSqlSortBuilder class.
		/// </summary>
		public ViewReportCancelationSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ViewReportCancelationSortBuilder

} // end namespace
