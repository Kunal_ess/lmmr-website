﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SearchTracerProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SearchTracerProviderBaseCore : EntityProviderBase<LMMR.Entities.SearchTracer, LMMR.Entities.SearchTracerKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.SearchTracerKey key)
		{
			return Delete(transactionManager, key.SearchId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_searchId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.String _searchId)
		{
			return Delete(null, _searchId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_searchId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.String _searchId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SearchTracer_SearchTracer key.
		///		FK_SearchTracer_SearchTracer Description: 
		/// </summary>
		/// <param name="_userId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SearchTracer objects.</returns>
		public TList<SearchTracer> GetByUserId(System.Int64? _userId)
		{
			int count = -1;
			return GetByUserId(_userId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SearchTracer_SearchTracer key.
		///		FK_SearchTracer_SearchTracer Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <returns>Returns a typed collection of LMMR.Entities.SearchTracer objects.</returns>
		/// <remarks></remarks>
		public TList<SearchTracer> GetByUserId(TransactionManager transactionManager, System.Int64? _userId)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_SearchTracer_SearchTracer key.
		///		FK_SearchTracer_SearchTracer Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SearchTracer objects.</returns>
		public TList<SearchTracer> GetByUserId(TransactionManager transactionManager, System.Int64? _userId, int start, int pageLength)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SearchTracer_SearchTracer key.
		///		fkSearchTracerSearchTracer Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_userId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SearchTracer objects.</returns>
		public TList<SearchTracer> GetByUserId(System.Int64? _userId, int start, int pageLength)
		{
			int count =  -1;
			return GetByUserId(null, _userId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SearchTracer_SearchTracer key.
		///		fkSearchTracerSearchTracer Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_userId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.SearchTracer objects.</returns>
		public TList<SearchTracer> GetByUserId(System.Int64? _userId, int start, int pageLength,out int count)
		{
			return GetByUserId(null, _userId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SearchTracer_SearchTracer key.
		///		FK_SearchTracer_SearchTracer Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.SearchTracer objects.</returns>
		public abstract TList<SearchTracer> GetByUserId(TransactionManager transactionManager, System.Int64? _userId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.SearchTracer Get(TransactionManager transactionManager, LMMR.Entities.SearchTracerKey key, int start, int pageLength)
		{
			return GetBySearchId(transactionManager, key.SearchId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SearchTracer index.
		/// </summary>
		/// <param name="_searchId"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SearchTracer"/> class.</returns>
		public LMMR.Entities.SearchTracer GetBySearchId(System.String _searchId)
		{
			int count = -1;
			return GetBySearchId(null,_searchId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SearchTracer index.
		/// </summary>
		/// <param name="_searchId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SearchTracer"/> class.</returns>
		public LMMR.Entities.SearchTracer GetBySearchId(System.String _searchId, int start, int pageLength)
		{
			int count = -1;
			return GetBySearchId(null, _searchId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SearchTracer index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_searchId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SearchTracer"/> class.</returns>
		public LMMR.Entities.SearchTracer GetBySearchId(TransactionManager transactionManager, System.String _searchId)
		{
			int count = -1;
			return GetBySearchId(transactionManager, _searchId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SearchTracer index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_searchId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SearchTracer"/> class.</returns>
		public LMMR.Entities.SearchTracer GetBySearchId(TransactionManager transactionManager, System.String _searchId, int start, int pageLength)
		{
			int count = -1;
			return GetBySearchId(transactionManager, _searchId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SearchTracer index.
		/// </summary>
		/// <param name="_searchId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SearchTracer"/> class.</returns>
		public LMMR.Entities.SearchTracer GetBySearchId(System.String _searchId, int start, int pageLength, out int count)
		{
			return GetBySearchId(null, _searchId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SearchTracer index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_searchId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.SearchTracer"/> class.</returns>
		public abstract LMMR.Entities.SearchTracer GetBySearchId(TransactionManager transactionManager, System.String _searchId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SearchTracer&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SearchTracer&gt;"/></returns>
		public static TList<SearchTracer> Fill(IDataReader reader, TList<SearchTracer> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.SearchTracer c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SearchTracer")
					.Append("|").Append((System.String)reader[((int)SearchTracerColumn.SearchId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SearchTracer>(
					key.ToString(), // EntityTrackingKey
					"SearchTracer",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.SearchTracer();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.SearchId = (System.String)reader[((int)SearchTracerColumn.SearchId - 1)];
					c.OriginalSearchId = c.SearchId;
					c.SearchObject = (string)reader[((int)SearchTracerColumn.SearchObject - 1)];
					c.CurrentStep = (System.Int32)reader[((int)SearchTracerColumn.CurrentStep - 1)];
					c.CurrentDay = (System.Int32)reader[((int)SearchTracerColumn.CurrentDay - 1)];
					c.UserId = (reader.IsDBNull(((int)SearchTracerColumn.UserId - 1)))?null:(System.Int64?)reader[((int)SearchTracerColumn.UserId - 1)];
					c.IsBooking = (System.Boolean)reader[((int)SearchTracerColumn.IsBooking - 1)];
					c.ModifyDate = (System.DateTime)reader[((int)SearchTracerColumn.ModifyDate - 1)];
					c.CreationDate = (System.DateTime)reader[((int)SearchTracerColumn.CreationDate - 1)];
					c.IsProcessDone = (System.Boolean)reader[((int)SearchTracerColumn.IsProcessDone - 1)];
					c.IsSentAsRequest = (reader.IsDBNull(((int)SearchTracerColumn.IsSentAsRequest - 1)))?null:(System.Boolean?)reader[((int)SearchTracerColumn.IsSentAsRequest - 1)];
					c.ChannelBy = (reader.IsDBNull(((int)SearchTracerColumn.ChannelBy - 1)))?null:(System.String)reader[((int)SearchTracerColumn.ChannelBy - 1)];
					c.ChannelId = (reader.IsDBNull(((int)SearchTracerColumn.ChannelId - 1)))?null:(System.String)reader[((int)SearchTracerColumn.ChannelId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.SearchTracer"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SearchTracer"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.SearchTracer entity)
		{
			if (!reader.Read()) return;
			
			entity.SearchId = (System.String)reader[((int)SearchTracerColumn.SearchId - 1)];
			entity.OriginalSearchId = (System.String)reader["SearchID"];
			entity.SearchObject = (string)reader[((int)SearchTracerColumn.SearchObject - 1)];
			entity.CurrentStep = (System.Int32)reader[((int)SearchTracerColumn.CurrentStep - 1)];
			entity.CurrentDay = (System.Int32)reader[((int)SearchTracerColumn.CurrentDay - 1)];
			entity.UserId = (reader.IsDBNull(((int)SearchTracerColumn.UserId - 1)))?null:(System.Int64?)reader[((int)SearchTracerColumn.UserId - 1)];
			entity.IsBooking = (System.Boolean)reader[((int)SearchTracerColumn.IsBooking - 1)];
			entity.ModifyDate = (System.DateTime)reader[((int)SearchTracerColumn.ModifyDate - 1)];
			entity.CreationDate = (System.DateTime)reader[((int)SearchTracerColumn.CreationDate - 1)];
			entity.IsProcessDone = (System.Boolean)reader[((int)SearchTracerColumn.IsProcessDone - 1)];
			entity.IsSentAsRequest = (reader.IsDBNull(((int)SearchTracerColumn.IsSentAsRequest - 1)))?null:(System.Boolean?)reader[((int)SearchTracerColumn.IsSentAsRequest - 1)];
			entity.ChannelBy = (reader.IsDBNull(((int)SearchTracerColumn.ChannelBy - 1)))?null:(System.String)reader[((int)SearchTracerColumn.ChannelBy - 1)];
			entity.ChannelId = (reader.IsDBNull(((int)SearchTracerColumn.ChannelId - 1)))?null:(System.String)reader[((int)SearchTracerColumn.ChannelId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.SearchTracer"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SearchTracer"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.SearchTracer entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.SearchId = (System.String)dataRow["SearchID"];
			entity.OriginalSearchId = (System.String)dataRow["SearchID"];
			entity.SearchObject = (string)dataRow["SearchObject"];
			entity.CurrentStep = (System.Int32)dataRow["CurrentStep"];
			entity.CurrentDay = (System.Int32)dataRow["CurrentDay"];
			entity.UserId = Convert.IsDBNull(dataRow["UserID"]) ? null : (System.Int64?)dataRow["UserID"];
			entity.IsBooking = (System.Boolean)dataRow["IsBooking"];
			entity.ModifyDate = (System.DateTime)dataRow["ModifyDate"];
			entity.CreationDate = (System.DateTime)dataRow["CreationDate"];
			entity.IsProcessDone = (System.Boolean)dataRow["IsProcessDone"];
			entity.IsSentAsRequest = Convert.IsDBNull(dataRow["IsSentAsRequest"]) ? null : (System.Boolean?)dataRow["IsSentAsRequest"];
			entity.ChannelBy = Convert.IsDBNull(dataRow["ChannelBy"]) ? null : (System.String)dataRow["ChannelBy"];
			entity.ChannelId = Convert.IsDBNull(dataRow["ChannelID"]) ? null : (System.String)dataRow["ChannelID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.SearchTracer"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.SearchTracer Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.SearchTracer entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region UserIdSource	
			if (CanDeepLoad(entity, "Users|UserIdSource", deepLoadType, innerList) 
				&& entity.UserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.UserId ?? (long)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.UserIdSource = tmpEntity;
				else
					entity.UserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.UserId ?? (long)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.UserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.UserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion UserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.SearchTracer object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.SearchTracer instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.SearchTracer Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.SearchTracer entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region UserIdSource
			if (CanDeepSave(entity, "Users|UserIdSource", deepSaveType, innerList) 
				&& entity.UserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.UserIdSource);
				entity.UserId = entity.UserIdSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SearchTracerChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.SearchTracer</c>
	///</summary>
	public enum SearchTracerChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Users</c> at UserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
		}
	
	#endregion SearchTracerChildEntityTypes
	
	#region SearchTracerFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SearchTracerColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SearchTracer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SearchTracerFilterBuilder : SqlFilterBuilder<SearchTracerColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SearchTracerFilterBuilder class.
		/// </summary>
		public SearchTracerFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SearchTracerFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SearchTracerFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SearchTracerFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SearchTracerFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SearchTracerFilterBuilder
	
	#region SearchTracerParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SearchTracerColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SearchTracer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SearchTracerParameterBuilder : ParameterizedSqlFilterBuilder<SearchTracerColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SearchTracerParameterBuilder class.
		/// </summary>
		public SearchTracerParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SearchTracerParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SearchTracerParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SearchTracerParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SearchTracerParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SearchTracerParameterBuilder
	
	#region SearchTracerSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SearchTracerColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SearchTracer"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SearchTracerSortBuilder : SqlSortBuilder<SearchTracerColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SearchTracerSqlSortBuilder class.
		/// </summary>
		public SearchTracerSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SearchTracerSortBuilder
	
} // end namespace
