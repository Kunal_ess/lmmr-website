﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using LMMR.Entities;
using LMMR.Data;

#endregion

namespace LMMR.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="WhiteLabelProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class WhiteLabelProviderBaseCore : EntityProviderBase<LMMR.Entities.WhiteLabel, LMMR.Entities.WhiteLabelKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, LMMR.Entities.WhiteLabelKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override LMMR.Entities.WhiteLabel Get(TransactionManager transactionManager, LMMR.Entities.WhiteLabelKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_WhiteLabel index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.WhiteLabel"/> class.</returns>
		public LMMR.Entities.WhiteLabel GetById(System.Int64 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_WhiteLabel index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.WhiteLabel"/> class.</returns>
		public LMMR.Entities.WhiteLabel GetById(System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_WhiteLabel index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.WhiteLabel"/> class.</returns>
		public LMMR.Entities.WhiteLabel GetById(TransactionManager transactionManager, System.Int64 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_WhiteLabel index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.WhiteLabel"/> class.</returns>
		public LMMR.Entities.WhiteLabel GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_WhiteLabel index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.WhiteLabel"/> class.</returns>
		public LMMR.Entities.WhiteLabel GetById(System.Int64 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_WhiteLabel index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.WhiteLabel"/> class.</returns>
		public abstract LMMR.Entities.WhiteLabel GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;WhiteLabel&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;WhiteLabel&gt;"/></returns>
		public static TList<WhiteLabel> Fill(IDataReader reader, TList<WhiteLabel> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				LMMR.Entities.WhiteLabel c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("WhiteLabel")
					.Append("|").Append((System.Int64)reader[((int)WhiteLabelColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<WhiteLabel>(
					key.ToString(), // EntityTrackingKey
					"WhiteLabel",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new LMMR.Entities.WhiteLabel();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int64)reader[((int)WhiteLabelColumn.Id - 1)];
					c.Name = (reader.IsDBNull(((int)WhiteLabelColumn.Name - 1)))?null:(System.String)reader[((int)WhiteLabelColumn.Name - 1)];
					c.TargetUrl = (reader.IsDBNull(((int)WhiteLabelColumn.TargetUrl - 1)))?null:(System.String)reader[((int)WhiteLabelColumn.TargetUrl - 1)];
					c.Commission = (reader.IsDBNull(((int)WhiteLabelColumn.Commission - 1)))?null:(System.Decimal?)reader[((int)WhiteLabelColumn.Commission - 1)];
					c.TargetSiteLogo = (reader.IsDBNull(((int)WhiteLabelColumn.TargetSiteLogo - 1)))?null:(System.String)reader[((int)WhiteLabelColumn.TargetSiteLogo - 1)];
					c.WhiteLabelNumber = (reader.IsDBNull(((int)WhiteLabelColumn.WhiteLabelNumber - 1)))?null:(System.String)reader[((int)WhiteLabelColumn.WhiteLabelNumber - 1)];
					c.CodeFile = (reader.IsDBNull(((int)WhiteLabelColumn.CodeFile - 1)))?null:(System.String)reader[((int)WhiteLabelColumn.CodeFile - 1)];
					c.CreationDate = (reader.IsDBNull(((int)WhiteLabelColumn.CreationDate - 1)))?null:(System.DateTime?)reader[((int)WhiteLabelColumn.CreationDate - 1)];
					c.IsActive = (reader.IsDBNull(((int)WhiteLabelColumn.IsActive - 1)))?null:(System.Boolean?)reader[((int)WhiteLabelColumn.IsActive - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.WhiteLabel"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.WhiteLabel"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, LMMR.Entities.WhiteLabel entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int64)reader[((int)WhiteLabelColumn.Id - 1)];
			entity.Name = (reader.IsDBNull(((int)WhiteLabelColumn.Name - 1)))?null:(System.String)reader[((int)WhiteLabelColumn.Name - 1)];
			entity.TargetUrl = (reader.IsDBNull(((int)WhiteLabelColumn.TargetUrl - 1)))?null:(System.String)reader[((int)WhiteLabelColumn.TargetUrl - 1)];
			entity.Commission = (reader.IsDBNull(((int)WhiteLabelColumn.Commission - 1)))?null:(System.Decimal?)reader[((int)WhiteLabelColumn.Commission - 1)];
			entity.TargetSiteLogo = (reader.IsDBNull(((int)WhiteLabelColumn.TargetSiteLogo - 1)))?null:(System.String)reader[((int)WhiteLabelColumn.TargetSiteLogo - 1)];
			entity.WhiteLabelNumber = (reader.IsDBNull(((int)WhiteLabelColumn.WhiteLabelNumber - 1)))?null:(System.String)reader[((int)WhiteLabelColumn.WhiteLabelNumber - 1)];
			entity.CodeFile = (reader.IsDBNull(((int)WhiteLabelColumn.CodeFile - 1)))?null:(System.String)reader[((int)WhiteLabelColumn.CodeFile - 1)];
			entity.CreationDate = (reader.IsDBNull(((int)WhiteLabelColumn.CreationDate - 1)))?null:(System.DateTime?)reader[((int)WhiteLabelColumn.CreationDate - 1)];
			entity.IsActive = (reader.IsDBNull(((int)WhiteLabelColumn.IsActive - 1)))?null:(System.Boolean?)reader[((int)WhiteLabelColumn.IsActive - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="LMMR.Entities.WhiteLabel"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="LMMR.Entities.WhiteLabel"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, LMMR.Entities.WhiteLabel entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int64)dataRow["Id"];
			entity.Name = Convert.IsDBNull(dataRow["Name"]) ? null : (System.String)dataRow["Name"];
			entity.TargetUrl = Convert.IsDBNull(dataRow["TargetURL"]) ? null : (System.String)dataRow["TargetURL"];
			entity.Commission = Convert.IsDBNull(dataRow["Commission"]) ? null : (System.Decimal?)dataRow["Commission"];
			entity.TargetSiteLogo = Convert.IsDBNull(dataRow["TargetSiteLogo"]) ? null : (System.String)dataRow["TargetSiteLogo"];
			entity.WhiteLabelNumber = Convert.IsDBNull(dataRow["WhiteLabelNumber"]) ? null : (System.String)dataRow["WhiteLabelNumber"];
			entity.CodeFile = Convert.IsDBNull(dataRow["CodeFile"]) ? null : (System.String)dataRow["CodeFile"];
			entity.CreationDate = Convert.IsDBNull(dataRow["CreationDate"]) ? null : (System.DateTime?)dataRow["CreationDate"];
			entity.IsActive = Convert.IsDBNull(dataRow["IsActive"]) ? null : (System.Boolean?)dataRow["IsActive"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="LMMR.Entities.WhiteLabel"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">LMMR.Entities.WhiteLabel Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, LMMR.Entities.WhiteLabel entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region WhiteLabelMappingWithHotelCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<WhiteLabelMappingWithHotel>|WhiteLabelMappingWithHotelCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'WhiteLabelMappingWithHotelCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.WhiteLabelMappingWithHotelCollection = DataRepository.WhiteLabelMappingWithHotelProvider.GetByWhiteLabelId(transactionManager, entity.Id);

				if (deep && entity.WhiteLabelMappingWithHotelCollection.Count > 0)
				{
					deepHandles.Add("WhiteLabelMappingWithHotelCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<WhiteLabelMappingWithHotel>) DataRepository.WhiteLabelMappingWithHotelProvider.DeepLoad,
						new object[] { transactionManager, entity.WhiteLabelMappingWithHotelCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the LMMR.Entities.WhiteLabel object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">LMMR.Entities.WhiteLabel instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">LMMR.Entities.WhiteLabel Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, LMMR.Entities.WhiteLabel entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<WhiteLabelMappingWithHotel>
				if (CanDeepSave(entity.WhiteLabelMappingWithHotelCollection, "List<WhiteLabelMappingWithHotel>|WhiteLabelMappingWithHotelCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(WhiteLabelMappingWithHotel child in entity.WhiteLabelMappingWithHotelCollection)
					{
						if(child.WhiteLabelIdSource != null)
						{
							child.WhiteLabelId = child.WhiteLabelIdSource.Id;
						}
						else
						{
							child.WhiteLabelId = entity.Id;
						}

					}

					if (entity.WhiteLabelMappingWithHotelCollection.Count > 0 || entity.WhiteLabelMappingWithHotelCollection.DeletedItems.Count > 0)
					{
						//DataRepository.WhiteLabelMappingWithHotelProvider.Save(transactionManager, entity.WhiteLabelMappingWithHotelCollection);
						
						deepHandles.Add("WhiteLabelMappingWithHotelCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< WhiteLabelMappingWithHotel >) DataRepository.WhiteLabelMappingWithHotelProvider.DeepSave,
							new object[] { transactionManager, entity.WhiteLabelMappingWithHotelCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region WhiteLabelChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>LMMR.Entities.WhiteLabel</c>
	///</summary>
	public enum WhiteLabelChildEntityTypes
	{

		///<summary>
		/// Collection of <c>WhiteLabel</c> as OneToMany for WhiteLabelMappingWithHotelCollection
		///</summary>
		[ChildEntityType(typeof(TList<WhiteLabelMappingWithHotel>))]
		WhiteLabelMappingWithHotelCollection,
	}
	
	#endregion WhiteLabelChildEntityTypes
	
	#region WhiteLabelFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;WhiteLabelColumn&gt;"/> class
	/// that is used exclusively with a <see cref="WhiteLabel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class WhiteLabelFilterBuilder : SqlFilterBuilder<WhiteLabelColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the WhiteLabelFilterBuilder class.
		/// </summary>
		public WhiteLabelFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the WhiteLabelFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public WhiteLabelFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the WhiteLabelFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public WhiteLabelFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion WhiteLabelFilterBuilder
	
	#region WhiteLabelParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;WhiteLabelColumn&gt;"/> class
	/// that is used exclusively with a <see cref="WhiteLabel"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class WhiteLabelParameterBuilder : ParameterizedSqlFilterBuilder<WhiteLabelColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the WhiteLabelParameterBuilder class.
		/// </summary>
		public WhiteLabelParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the WhiteLabelParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public WhiteLabelParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the WhiteLabelParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public WhiteLabelParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion WhiteLabelParameterBuilder
	
	#region WhiteLabelSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;WhiteLabelColumn&gt;"/> class
	/// that is used exclusively with a <see cref="WhiteLabel"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class WhiteLabelSortBuilder : SqlSortBuilder<WhiteLabelColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the WhiteLabelSqlSortBuilder class.
		/// </summary>
		public WhiteLabelSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion WhiteLabelSortBuilder
	
} // end namespace
