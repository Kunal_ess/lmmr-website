﻿#region Using
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Linq;
using LMMR.Data;
using System.Configuration;
using log4net;
using log4net.Config;
#endregion

public partial class Operator_HotelFacilityAccess : System.Web.UI.Page
{
    #region variable declaration
    DashboardManager ObjDashBoard = new DashboardManager();
    WizardLinkSettingManager ObjWizardLinkSetting = new WizardLinkSettingManager();
    ViewBooking_Hotel objvewbooking = new ViewBooking_Hotel();
    HotelManager objHotelManager = new HotelManager();
    Hotel objHotel = new Hotel();
    HotelInfo ObjHotelinfo = new HotelInfo();
    ClientContract objClient = new ClientContract();
    ILog logger = log4net.LogManager.GetLogger(typeof(Operator_HotelFacilityAccess));
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindAllHotel();
            BindAllCountry();
        }
    }
    #endregion

    #region Bind All Hotel
    /// <summary>
    /// Bind the Hotel details to the drop down drpHotelList
    /// </summary>
    public void BindAllHotel()
    {
        try
        {
            string whereclauseforHotel = HotelColumn.IsActive + "=1" + " and " + HotelColumn.IsRemoved + "=0";            
            TList<Hotel> THotel = ObjHotelinfo.GetHotelbyCondition(whereclauseforHotel, string.Empty);
            drpHotelList.DataSource = THotel.OrderBy(a => a.Name);
            drpHotelList.DataTextField = "Name";
            drpHotelList.DataValueField = "Id";
            drpHotelList.DataBind();
            drpHotelList.Items.Insert(0, new ListItem("Select Hotel", "0"));
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    #region Bind All Country
    /// <summary>
    /// Bind the Hotel details to the drop down drpHotelList
    /// </summary>
    public void BindAllCountry()
    {
        TList<Country> objCountry = objClient.GetByAllCountry();
        drpCountry.DataValueField = "Id";
        drpCountry.DataTextField = "CountryName";
        drpCountry.DataSource = objCountry.OrderBy(a => a.CountryName);
        drpCountry.DataBind();
        drpCountry.Items.Insert(0, new ListItem("Select Country", "0"));
    }
    #endregion

    #region Get city by country id
    /// <summary>
    /// Add panel country dropdown bind city according to country
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void drpCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (drpCountry.SelectedIndex > 0)
            {
                //Get Currency id on the basis of countryid
                Country cunt = objHotelManager.GetByCountryID(Convert.ToInt32(drpCountry.SelectedValue));               
                drpCity.DataValueField = "Id";
                drpCity.DataTextField = "City";
                drpCity.DataSource = objClient.GetCityByCountry(Convert.ToInt32(drpCountry.SelectedItem.Value)).OrderBy(a => a.City);
                drpCity.DataBind();
                drpCity.Items.Insert(0, new ListItem("Select City", "0"));
                drpCity.Enabled = true;                               
            }
            else
            {                
                drpCity.Items.Clear();
                drpCity.Items.Insert(0, new ListItem("Select City", "0"));
                drpCity.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    #region Get hotel by city id
    /// <summary>
    /// Add panel hotel dropdown according to the city
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void drpCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (drpCity.SelectedIndex > 0)
            {                
                string whereclause = "CityId='" + drpCity.SelectedValue + "'" + " and " + HotelColumn.IsActive + "=1" + " and " + HotelColumn.IsRemoved + "=0";    
                TList <Hotel> htlList = ObjHotelinfo.GetHotelbyCondition(whereclause, String.Empty);                
                drpHotelList.DataValueField = "Id";
                drpHotelList.DataTextField = "Name";
                drpHotelList.DataSource = htlList.OrderBy(a => a.Name);
                drpHotelList.DataBind();
                drpHotelList.Items.Insert(0, new ListItem("Select Hotel", "0"));               
            }            
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    #endregion

    #region Events
    protected void lnbSearch_Click(object sender, EventArgs e)
    {       
        try
        {
            Hotel objhotel = objHotelManager.GetHotelDetailsById(Convert.ToInt64(drpHotelList.SelectedValue));
            Users objUser = (Users)Session["CurrentOperator"];
            Session["CurrentUserID"] = objUser.UserId;
            Session["CurrentHotelID"] = objhotel.Id;
            Session["CurrentUser"] = Session["CurrentOperator"];
            Session["Operator"] = "1";
            Session["OType"] = "Facility";
            Response.Redirect("Watch.aspx");
            //pnlFrame.Visible = true;
            //lblFacilityName.Text = objhotel.Name;
            //lblContractNumber.Text = objhotel.ContractId;
        }
        catch (Exception ex)
        {
            logger.Error(ex);
        }
    }
    protected void lnb_Click(object sender, EventArgs e)
    {
        pnlFrame.Visible = false;
    }    
    protected void lnkClear_Click(object sender, EventArgs e)
    {
        drpCountry.SelectedValue = "0";        
        if (drpCity.SelectedValue == "")
        {
            drpCity.Items.Clear();
            drpCity.Items.Insert(0, new ListItem("Select City", "0"));
            drpCity.Enabled = false;
        }
        else
        {
            drpCity.SelectedValue = "0";
            drpCity.Enabled = false;
        }
        BindAllHotel();
    }
    #endregion
}