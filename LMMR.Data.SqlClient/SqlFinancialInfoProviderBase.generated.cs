﻿
/*
	File Generated by NetTiers templates [www.nettiers.com]
	Important: Do not modify this file. Edit the file SqlFinancialInfoProvider.cs instead.
*/

#region using directives

using System;
using System.Data;
using System.Data.Common;
using System.Text;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

using System.Collections;
using System.Collections.Specialized;

using System.Diagnostics;
using LMMR.Entities;
using LMMR.Data;
using LMMR.Data.Bases;

#endregion

namespace LMMR.Data.SqlClient
{
	///<summary>
	/// This class is the SqlClient Data Access Logic Component implementation for the <see cref="FinancialInfo"/> entity.
	///</summary>
	public abstract partial class SqlFinancialInfoProviderBase : FinancialInfoProviderBase
	{
		#region Declarations
		
		string _connectionString;
	    bool _useStoredProcedure;
	    string _providerInvariantName;
			
		#endregion "Declarations"
			
		#region Constructors
		
		/// <summary>
		/// Creates a new <see cref="SqlFinancialInfoProviderBase"/> instance.
		/// </summary>
		public SqlFinancialInfoProviderBase()
		{
		}
	
	/// <summary>
	/// Creates a new <see cref="SqlFinancialInfoProviderBase"/> instance.
	/// Uses connection string to connect to datasource.
	/// </summary>
	/// <param name="connectionString">The connection string to the database.</param>
	/// <param name="useStoredProcedure">A boolean value that indicates if we should use stored procedures or embedded queries.</param>
	/// <param name="providerInvariantName">Name of the invariant provider use by the DbProviderFactory.</param>
	public SqlFinancialInfoProviderBase(string connectionString, bool useStoredProcedure, string providerInvariantName)
	{
		this._connectionString = connectionString;
		this._useStoredProcedure = useStoredProcedure;
		this._providerInvariantName = providerInvariantName;
	}
		
	#endregion "Constructors"
	
		#region Public properties
	/// <summary>
    /// Gets or sets the connection string.
    /// </summary>
    /// <value>The connection string.</value>
    public string ConnectionString
	{
		get {return this._connectionString;}
		set {this._connectionString = value;}
	}
	
	/// <summary>
    /// Gets or sets a value indicating whether to use stored procedures.
    /// </summary>
    /// <value><c>true</c> if we choose to use use stored procedures; otherwise, <c>false</c>.</value>
	public bool UseStoredProcedure
	{
		get {return this._useStoredProcedure;}
		set {this._useStoredProcedure = value;}
	}
	
	/// <summary>
    /// Gets or sets the invariant provider name listed in the DbProviderFactories machine.config section.
    /// </summary>
    /// <value>The name of the provider invariant.</value>
    public string ProviderInvariantName
    {
        get { return this._providerInvariantName; }
        set { this._providerInvariantName = value; }
    }
	#endregion
	
		#region Get Many To Many Relationship Functions
		#endregion
	
		#region Delete Functions
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>	
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
        /// <exception cref="System.Exception">The command could not be executed.</exception>
        /// <exception cref="System.Data.DataException">The <paramref name="transactionManager"/> is not open.</exception>
        /// <exception cref="System.Data.Common.DbException">The command could not be executed.</exception>
		public override bool Delete(TransactionManager transactionManager, System.Int64 _id)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			DbCommand commandWrapper = StoredProcedureProvider.GetCommandWrapper(database, "dbo.FinancialInfo_Delete", _useStoredProcedure);
			database.AddInParameter(commandWrapper, "@Id", DbType.Int64, _id);
			
			//Provider Data Requesting Command Event
			OnDataRequesting(new CommandEventArgs(commandWrapper, "Delete")); 

			int results = 0;
			
			if (transactionManager != null)
			{	
				results = Utility.ExecuteNonQuery(transactionManager, commandWrapper);
			}
			else
			{
				results = Utility.ExecuteNonQuery(database,commandWrapper);
			}
			
			//Stop Tracking Now that it has been updated and persisted.
			if (DataRepository.Provider.EnableEntityTracking)
			{
				string entityKey = EntityLocator.ConstructKeyFromPkItems(typeof(FinancialInfo)
					,_id);
                EntityManager.StopTracking(entityKey);
                
			}
			
			//Provider Data Requested Command Event
			OnDataRequested(new CommandEventArgs(commandWrapper, "Delete")); 

			commandWrapper = null;
			
			return Convert.ToBoolean(results);
		}//end Delete
		#endregion

		#region Find Functions

		#region Parsed Find Methods
		/// <summary>
		/// 	Returns rows meeting the whereClause condition from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="whereClause">Specifies the condition for the rows returned by a query (Name='John Doe', Name='John Doe' AND Id='1', Name='John Doe' OR Id='1').</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out. The number of rows that match this query.</param>
		/// <remarks>Operators must be capitalized (OR, AND).</remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.FinancialInfo objects.</returns>
		public override TList<FinancialInfo> Find(TransactionManager transactionManager, string whereClause, int start, int pageLength, out int count)
		{
			count = -1;
			if (whereClause.IndexOf(";") > -1)
				return new TList<FinancialInfo>();
	
			SqlDatabase database = new SqlDatabase(this._connectionString);
			DbCommand commandWrapper = StoredProcedureProvider.GetCommandWrapper(database, "dbo.FinancialInfo_Find", _useStoredProcedure);

		bool searchUsingOR = false;
		if (whereClause.IndexOf(" OR ") > 0) // did they want to do "a=b OR c=d OR..."?
			searchUsingOR = true;
		
		database.AddInParameter(commandWrapper, "@SearchUsingOR", DbType.Boolean, searchUsingOR);
		
		database.AddInParameter(commandWrapper, "@Id", DbType.Int64, DBNull.Value);
		database.AddInParameter(commandWrapper, "@Name", DbType.String, DBNull.Value);
		database.AddInParameter(commandWrapper, "@Department", DbType.String, DBNull.Value);
		database.AddInParameter(commandWrapper, "@CommunicationEmail", DbType.String, DBNull.Value);
		database.AddInParameter(commandWrapper, "@CountryId", DbType.Int32, DBNull.Value);
		database.AddInParameter(commandWrapper, "@CityId", DbType.Int32, DBNull.Value);
		database.AddInParameter(commandWrapper, "@Address", DbType.String, DBNull.Value);
		database.AddInParameter(commandWrapper, "@Phone", DbType.String, DBNull.Value);
		database.AddInParameter(commandWrapper, "@CountryCode", DbType.String, DBNull.Value);
		database.AddInParameter(commandWrapper, "@PostalCode", DbType.String, DBNull.Value);
		database.AddInParameter(commandWrapper, "@UserId", DbType.Int64, DBNull.Value);
		database.AddInParameter(commandWrapper, "@VatValue", DbType.Int64, DBNull.Value);
		database.AddInParameter(commandWrapper, "@IsDeleted", DbType.Boolean, DBNull.Value);
		database.AddInParameter(commandWrapper, "@DeletedDate", DbType.DateTime, DBNull.Value);
		database.AddInParameter(commandWrapper, "@VatNo", DbType.String, DBNull.Value);
		database.AddInParameter(commandWrapper, "@CityName", DbType.String, DBNull.Value);
	
			// replace all instances of 'AND' and 'OR' because we already set searchUsingOR
			whereClause = whereClause.Replace(" AND ", "|").Replace(" OR ", "|") ; 
			string[] clauses = whereClause.ToLower().Split('|');
		
			// Here's what's going on below: Find a field, then to get the value we
			// drop the field name from the front, trim spaces, drop the '=' sign,
			// trim more spaces, and drop any outer single quotes.
			// Now handles the case when two fields start off the same way - like "Friendly='Yes' AND Friend='john'"
				
			char[] equalSign = {'='};
			char[] singleQuote = {'\''};
	   		foreach (string clause in clauses)
			{
				if (clause.Trim().StartsWith("id ") || clause.Trim().StartsWith("id="))
				{
					database.SetParameterValue(commandWrapper, "@Id", 
						clause.Trim().Remove(0,2).Trim().TrimStart(equalSign).Trim().Trim(singleQuote));
					continue;
				}
				if (clause.Trim().StartsWith("name ") || clause.Trim().StartsWith("name="))
				{
					database.SetParameterValue(commandWrapper, "@Name", 
						clause.Trim().Remove(0,4).Trim().TrimStart(equalSign).Trim().Trim(singleQuote));
					continue;
				}
				if (clause.Trim().StartsWith("department ") || clause.Trim().StartsWith("department="))
				{
					database.SetParameterValue(commandWrapper, "@Department", 
						clause.Trim().Remove(0,10).Trim().TrimStart(equalSign).Trim().Trim(singleQuote));
					continue;
				}
				if (clause.Trim().StartsWith("communicationemail ") || clause.Trim().StartsWith("communicationemail="))
				{
					database.SetParameterValue(commandWrapper, "@CommunicationEmail", 
						clause.Trim().Remove(0,18).Trim().TrimStart(equalSign).Trim().Trim(singleQuote));
					continue;
				}
				if (clause.Trim().StartsWith("countryid ") || clause.Trim().StartsWith("countryid="))
				{
					database.SetParameterValue(commandWrapper, "@CountryId", 
						clause.Trim().Remove(0,9).Trim().TrimStart(equalSign).Trim().Trim(singleQuote));
					continue;
				}
				if (clause.Trim().StartsWith("cityid ") || clause.Trim().StartsWith("cityid="))
				{
					database.SetParameterValue(commandWrapper, "@CityId", 
						clause.Trim().Remove(0,6).Trim().TrimStart(equalSign).Trim().Trim(singleQuote));
					continue;
				}
				if (clause.Trim().StartsWith("address ") || clause.Trim().StartsWith("address="))
				{
					database.SetParameterValue(commandWrapper, "@Address", 
						clause.Trim().Remove(0,7).Trim().TrimStart(equalSign).Trim().Trim(singleQuote));
					continue;
				}
				if (clause.Trim().StartsWith("phone ") || clause.Trim().StartsWith("phone="))
				{
					database.SetParameterValue(commandWrapper, "@Phone", 
						clause.Trim().Remove(0,5).Trim().TrimStart(equalSign).Trim().Trim(singleQuote));
					continue;
				}
				if (clause.Trim().StartsWith("countrycode ") || clause.Trim().StartsWith("countrycode="))
				{
					database.SetParameterValue(commandWrapper, "@CountryCode", 
						clause.Trim().Remove(0,11).Trim().TrimStart(equalSign).Trim().Trim(singleQuote));
					continue;
				}
				if (clause.Trim().StartsWith("postalcode ") || clause.Trim().StartsWith("postalcode="))
				{
					database.SetParameterValue(commandWrapper, "@PostalCode", 
						clause.Trim().Remove(0,10).Trim().TrimStart(equalSign).Trim().Trim(singleQuote));
					continue;
				}
				if (clause.Trim().StartsWith("userid ") || clause.Trim().StartsWith("userid="))
				{
					database.SetParameterValue(commandWrapper, "@UserId", 
						clause.Trim().Remove(0,6).Trim().TrimStart(equalSign).Trim().Trim(singleQuote));
					continue;
				}
				if (clause.Trim().StartsWith("vatvalue ") || clause.Trim().StartsWith("vatvalue="))
				{
					database.SetParameterValue(commandWrapper, "@VatValue", 
						clause.Trim().Remove(0,8).Trim().TrimStart(equalSign).Trim().Trim(singleQuote));
					continue;
				}
				if (clause.Trim().StartsWith("isdeleted ") || clause.Trim().StartsWith("isdeleted="))
				{
					database.SetParameterValue(commandWrapper, "@IsDeleted", 
						clause.Trim().Remove(0,9).Trim().TrimStart(equalSign).Trim().Trim(singleQuote));
					continue;
				}
				if (clause.Trim().StartsWith("deleteddate ") || clause.Trim().StartsWith("deleteddate="))
				{
					database.SetParameterValue(commandWrapper, "@DeletedDate", 
						clause.Trim().Remove(0,11).Trim().TrimStart(equalSign).Trim().Trim(singleQuote));
					continue;
				}
				if (clause.Trim().StartsWith("vatno ") || clause.Trim().StartsWith("vatno="))
				{
					database.SetParameterValue(commandWrapper, "@VatNo", 
						clause.Trim().Remove(0,5).Trim().TrimStart(equalSign).Trim().Trim(singleQuote));
					continue;
				}
				if (clause.Trim().StartsWith("cityname ") || clause.Trim().StartsWith("cityname="))
				{
					database.SetParameterValue(commandWrapper, "@CityName", 
						clause.Trim().Remove(0,8).Trim().TrimStart(equalSign).Trim().Trim(singleQuote));
					continue;
				}
	
				throw new ArgumentException("Unable to use this part of the where clause in this version of Find: " + clause);
			}
					
			IDataReader reader = null;
			//Create Collection
			TList<FinancialInfo> rows = new TList<FinancialInfo>();
	
				
			try
			{
				//Provider Data Requesting Command Event
				OnDataRequesting(new CommandEventArgs(commandWrapper, "Find", rows)); 

				if (transactionManager != null)
				{
					reader = Utility.ExecuteReader(transactionManager, commandWrapper);
				}
				else
				{
					reader = Utility.ExecuteReader(database, commandWrapper);
				}		
				
				Fill(reader, rows, start, pageLength);
				
				if(reader.NextResult())
				{
					if(reader.Read())
					{
						count = reader.GetInt32(0);
					}
				}
				
				//Provider Data Requested Command Event
				OnDataRequested(new CommandEventArgs(commandWrapper, "Find", rows)); 
			}
			finally
			{
				if (reader != null) 
					reader.Close();	
					
				commandWrapper = null;
			}
			return rows;
		}

		#endregion Parsed Find Methods
		
		#region Parameterized Find Methods
		
		/// <summary>
		/// 	Returns rows from the DataSource that meet the parameter conditions.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="parameters">A collection of <see cref="SqlFilterParameter"/> objects.</param>
		/// <param name="orderBy">Specifies the sort criteria for the rows in the DataSource (Name ASC; BirthDay DESC, Name ASC);</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out. The number of rows that match this query.</param>
		/// <returns>Returns a typed collection of LMMR.Entities.FinancialInfo objects.</returns>
		public override TList<FinancialInfo> Find(TransactionManager transactionManager, IFilterParameterCollection parameters, string orderBy, int start, int pageLength, out int count)
		{
			SqlFilterParameterCollection filter = null;
			
			if (parameters == null)
				filter = new SqlFilterParameterCollection();
			else 
				filter = parameters.GetParameters();
				
			SqlDatabase database = new SqlDatabase(this._connectionString);
			DbCommand commandWrapper = StoredProcedureProvider.GetCommandWrapper(database, "dbo.FinancialInfo_Find_Dynamic", typeof(FinancialInfoColumn), filter, orderBy, start, pageLength);
		
			SqlFilterParameter param;

			for ( int i = 0; i < filter.Count; i++ )
			{
				param = filter[i];
				database.AddInParameter(commandWrapper, param.Name, param.DbType, param.GetValue());
			}

			TList<FinancialInfo> rows = new TList<FinancialInfo>();
			IDataReader reader = null;
			
			try
			{
				//Provider Data Requesting Command Event
				OnDataRequesting(new CommandEventArgs(commandWrapper, "Find", rows)); 

				if ( transactionManager != null )
				{
					reader = Utility.ExecuteReader(transactionManager, commandWrapper);
				}
				else
				{
					reader = Utility.ExecuteReader(database, commandWrapper);
				}
				
				Fill(reader, rows, 0, int.MaxValue);
				count = rows.Count;
				
				if ( reader.NextResult() )
				{
					if ( reader.Read() )
					{
						count = reader.GetInt32(0);
					}
				}
				
				//Provider Data Requested Command Event
				OnDataRequested(new CommandEventArgs(commandWrapper, "Find", rows)); 
			}
			finally
			{
				if ( reader != null )
					reader.Close();
					
				commandWrapper = null;
			}
			
			return rows;
		}
		
		#endregion Parameterized Find Methods
		
		#endregion Find Functions
	
		#region GetAll Methods
				
		/// <summary>
		/// 	Gets All rows from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out. The number of rows that match this query.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.FinancialInfo objects.</returns>
        /// <exception cref="System.Exception">The command could not be executed.</exception>
        /// <exception cref="System.Data.DataException">The <paramref name="transactionManager"/> is not open.</exception>
        /// <exception cref="System.Data.Common.DbException">The command could not be executed.</exception>
		public override TList<FinancialInfo> GetAll(TransactionManager transactionManager, int start, int pageLength, out int count)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			DbCommand commandWrapper = StoredProcedureProvider.GetCommandWrapper(database, "dbo.FinancialInfo_Get_List", _useStoredProcedure);
			
			IDataReader reader = null;
		
			//Create Collection
			TList<FinancialInfo> rows = new TList<FinancialInfo>();
			
			try
			{
				//Provider Data Requesting Command Event
				OnDataRequesting(new CommandEventArgs(commandWrapper, "GetAll", rows)); 
					
				if (transactionManager != null)
				{
					reader = Utility.ExecuteReader(transactionManager, commandWrapper);
				}
				else
				{
					reader = Utility.ExecuteReader(database, commandWrapper);
				}		
		
				Fill(reader, rows, start, pageLength);
				count = -1;
				if(reader.NextResult())
				{
					if(reader.Read())
					{
						count = reader.GetInt32(0);
					}
				}
				
				//Provider Data Requested Command Event
				OnDataRequested(new CommandEventArgs(commandWrapper, "GetAll", rows)); 
			}
			finally 
			{
				if (reader != null) 
					reader.Close();
					
				commandWrapper = null;	
			}
			return rows;
		}//end getall
		
		#endregion
				
		#region GetPaged Methods
				
		/// <summary>
		/// Gets a page of rows from the DataSource.
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">Number of rows in the DataSource.</param>
		/// <param name="whereClause">Specifies the condition for the rows returned by a query (Name='John Doe', Name='John Doe' AND Id='1', Name='John Doe' OR Id='1').</param>
		/// <param name="orderBy">Specifies the sort criteria for the rows in the DataSource (Name ASC; BirthDay DESC, Name ASC);</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.FinancialInfo objects.</returns>
		public override TList<FinancialInfo> GetPaged(TransactionManager transactionManager, string whereClause, string orderBy, int start, int pageLength, out int count)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			DbCommand commandWrapper = StoredProcedureProvider.GetCommandWrapper(database, "dbo.FinancialInfo_GetPaged", _useStoredProcedure);
		
			
            if (commandWrapper.CommandType == CommandType.Text
                && commandWrapper.CommandText != null)
            {
                commandWrapper.CommandText = commandWrapper.CommandText.Replace(SqlUtil.PAGE_INDEX, string.Concat(SqlUtil.PAGE_INDEX, Guid.NewGuid().ToString("N").Substring(0, 8)));
            }
			
			database.AddInParameter(commandWrapper, "@WhereClause", DbType.String, whereClause);
			database.AddInParameter(commandWrapper, "@OrderBy", DbType.String, orderBy);
			database.AddInParameter(commandWrapper, "@PageIndex", DbType.Int32, start);
			database.AddInParameter(commandWrapper, "@PageSize", DbType.Int32, pageLength);
		
			IDataReader reader = null;
			//Create Collection
			TList<FinancialInfo> rows = new TList<FinancialInfo>();
			
			try
			{
				//Provider Data Requesting Command Event
				OnDataRequesting(new CommandEventArgs(commandWrapper, "GetPaged", rows)); 

				if (transactionManager != null)
				{
					reader = Utility.ExecuteReader(transactionManager, commandWrapper);
				}
				else
				{
					reader = Utility.ExecuteReader(database, commandWrapper);
				}
				
				Fill(reader, rows, 0, int.MaxValue);
				count = rows.Count;

				if(reader.NextResult())
				{
					if(reader.Read())
					{
						count = reader.GetInt32(0);
					}
				}
				
				//Provider Data Requested Command Event
				OnDataRequested(new CommandEventArgs(commandWrapper, "GetPaged", rows)); 

			}
			catch(Exception)
			{			
				throw;
			}
			finally
			{
				if (reader != null) 
					reader.Close();
				
				commandWrapper = null;
			}
			
			return rows;
		}
		
		#endregion	
		
		#region Get By Foreign Key Functions

		#region GetByUserId
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__Financial__UserI__4FBCC72F key.
		///		FK__Financial__UserI__4FBCC72F Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of LMMR.Entities.FinancialInfo objects.</returns>
        /// <exception cref="System.Exception">The command could not be executed.</exception>
        /// <exception cref="System.Data.DataException">The <paramref name="transactionManager"/> is not open.</exception>
        /// <exception cref="System.Data.Common.DbException">The command could not be executed.</exception>
		public override TList<FinancialInfo> GetByUserId(TransactionManager transactionManager, System.Int64? _userId, int start, int pageLength, out int count)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			DbCommand commandWrapper = StoredProcedureProvider.GetCommandWrapper(database, "dbo.FinancialInfo_GetByUserId", _useStoredProcedure);
			
				database.AddInParameter(commandWrapper, "@UserId", DbType.Int64, _userId);
			
			IDataReader reader = null;
			TList<FinancialInfo> rows = new TList<FinancialInfo>();
			try
			{
				//Provider Data Requesting Command Event
				OnDataRequesting(new CommandEventArgs(commandWrapper, "GetByUserId", rows)); 

				if (transactionManager != null)
				{
					reader = Utility.ExecuteReader(transactionManager, commandWrapper);
				}
				else
				{
					reader = Utility.ExecuteReader(database, commandWrapper);
				}
			
				//Create Collection
				Fill(reader, rows, start, pageLength);
				count = -1;
				if(reader.NextResult())
				{
					if(reader.Read())
					{
						count = reader.GetInt32(0);
					}
				}
				
				//Provider Data Requested Command Event
				OnDataRequested(new CommandEventArgs(commandWrapper, "GetByUserId", rows)); 
			}
			finally
			{
				if (reader != null) 
					reader.Close();
					
				commandWrapper = null;
			}
			return rows;
		}	
		#endregion
	
	#endregion
	
		#region Get By Index Functions

		#region GetById
					
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FinancialInfo index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query.</param>
		/// <returns>Returns an instance of the <see cref="LMMR.Entities.FinancialInfo"/> class.</returns>
		/// <remarks></remarks>
        /// <exception cref="System.Exception">The command could not be executed.</exception>
        /// <exception cref="System.Data.DataException">The <paramref name="transactionManager"/> is not open.</exception>
        /// <exception cref="System.Data.Common.DbException">The command could not be executed.</exception>
		public override LMMR.Entities.FinancialInfo GetById(TransactionManager transactionManager, System.Int64 _id, int start, int pageLength, out int count)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			DbCommand commandWrapper = StoredProcedureProvider.GetCommandWrapper(database, "dbo.FinancialInfo_GetById", _useStoredProcedure);
			
				database.AddInParameter(commandWrapper, "@Id", DbType.Int64, _id);
			
			IDataReader reader = null;
			TList<FinancialInfo> tmp = new TList<FinancialInfo>();
			try
			{
				//Provider Data Requesting Command Event
				OnDataRequesting(new CommandEventArgs(commandWrapper, "GetById", tmp)); 

				if (transactionManager != null)
				{
					reader = Utility.ExecuteReader(transactionManager, commandWrapper);
				}
				else
				{
					reader = Utility.ExecuteReader(database, commandWrapper);
				}		
		
				//Create collection and fill
				Fill(reader, tmp, start, pageLength);
				count = -1;
				if(reader.NextResult())
				{
					if(reader.Read())
					{
						count = reader.GetInt32(0);
					}
				}
				
				//Provider Data Requested Command Event
				OnDataRequested(new CommandEventArgs(commandWrapper, "GetById", tmp));
			}
			finally 
			{
				if (reader != null) 
					reader.Close();
					
				commandWrapper = null;
			}
			
			if (tmp.Count == 1)
			{
				return tmp[0];
			}
			else if (tmp.Count == 0)
			{
				return null;
			}
			else
			{
				throw new DataException("Cannot find the unique instance of the class.");
			}
			
			//return rows;
		}
		
		#endregion

	#endregion Get By Index Functions

		#region Insert Methods
		/// <summary>
		/// Lets you efficiently bulk insert many entities to the database.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entities">The entities.</param>
		/// <remarks>
		///		After inserting into the datasource, the LMMR.Entities.FinancialInfo object will be updated
		/// 	to refelect any changes made by the datasource. (ie: identity or computed columns)
		/// </remarks>	
		public override void BulkInsert(TransactionManager transactionManager, TList<LMMR.Entities.FinancialInfo> entities)
		{
			//System.Data.SqlClient.SqlBulkCopy bulkCopy = new System.Data.SqlClient.SqlBulkCopy(this._connectionString, System.Data.SqlClient.SqlBulkCopyOptions.CheckConstraints); //, null);
			
			System.Data.SqlClient.SqlBulkCopy bulkCopy = null;
	
			if (transactionManager != null && transactionManager.IsOpen)
			{			
				System.Data.SqlClient.SqlConnection cnx = transactionManager.TransactionObject.Connection as System.Data.SqlClient.SqlConnection;
				System.Data.SqlClient.SqlTransaction transaction = transactionManager.TransactionObject as System.Data.SqlClient.SqlTransaction;
				bulkCopy = new System.Data.SqlClient.SqlBulkCopy(cnx, System.Data.SqlClient.SqlBulkCopyOptions.CheckConstraints, transaction); //, null);
			}
			else
			{
				bulkCopy = new System.Data.SqlClient.SqlBulkCopy(this._connectionString, System.Data.SqlClient.SqlBulkCopyOptions.CheckConstraints); //, null);
			}
			
			bulkCopy.BulkCopyTimeout = 360;
			bulkCopy.DestinationTableName = "FinancialInfo";
			
			DataTable dataTable = new DataTable();
			DataColumn col0 = dataTable.Columns.Add("Id", typeof(System.Int64));
			col0.AllowDBNull = false;		
			DataColumn col1 = dataTable.Columns.Add("Name", typeof(System.String));
			col1.AllowDBNull = true;		
			DataColumn col2 = dataTable.Columns.Add("Department", typeof(System.String));
			col2.AllowDBNull = true;		
			DataColumn col3 = dataTable.Columns.Add("CommunicationEmail", typeof(System.String));
			col3.AllowDBNull = true;		
			DataColumn col4 = dataTable.Columns.Add("CountryId", typeof(System.Int32));
			col4.AllowDBNull = true;		
			DataColumn col5 = dataTable.Columns.Add("CityId", typeof(System.Int32));
			col5.AllowDBNull = true;		
			DataColumn col6 = dataTable.Columns.Add("Address", typeof(System.String));
			col6.AllowDBNull = true;		
			DataColumn col7 = dataTable.Columns.Add("Phone", typeof(System.String));
			col7.AllowDBNull = true;		
			DataColumn col8 = dataTable.Columns.Add("CountryCode", typeof(System.String));
			col8.AllowDBNull = true;		
			DataColumn col9 = dataTable.Columns.Add("PostalCode", typeof(System.String));
			col9.AllowDBNull = true;		
			DataColumn col10 = dataTable.Columns.Add("UserID", typeof(System.Int64));
			col10.AllowDBNull = true;		
			DataColumn col11 = dataTable.Columns.Add("VatValue", typeof(System.Int64));
			col11.AllowDBNull = true;		
			DataColumn col12 = dataTable.Columns.Add("IsDeleted", typeof(System.Boolean));
			col12.AllowDBNull = false;		
			DataColumn col13 = dataTable.Columns.Add("DeletedDate", typeof(System.DateTime));
			col13.AllowDBNull = true;		
			DataColumn col14 = dataTable.Columns.Add("VatNo", typeof(System.String));
			col14.AllowDBNull = true;		
			DataColumn col15 = dataTable.Columns.Add("CityName", typeof(System.String));
			col15.AllowDBNull = true;		
			
			bulkCopy.ColumnMappings.Add("Id", "Id");
			bulkCopy.ColumnMappings.Add("Name", "Name");
			bulkCopy.ColumnMappings.Add("Department", "Department");
			bulkCopy.ColumnMappings.Add("CommunicationEmail", "CommunicationEmail");
			bulkCopy.ColumnMappings.Add("CountryId", "CountryId");
			bulkCopy.ColumnMappings.Add("CityId", "CityId");
			bulkCopy.ColumnMappings.Add("Address", "Address");
			bulkCopy.ColumnMappings.Add("Phone", "Phone");
			bulkCopy.ColumnMappings.Add("CountryCode", "CountryCode");
			bulkCopy.ColumnMappings.Add("PostalCode", "PostalCode");
			bulkCopy.ColumnMappings.Add("UserID", "UserID");
			bulkCopy.ColumnMappings.Add("VatValue", "VatValue");
			bulkCopy.ColumnMappings.Add("IsDeleted", "IsDeleted");
			bulkCopy.ColumnMappings.Add("DeletedDate", "DeletedDate");
			bulkCopy.ColumnMappings.Add("VatNo", "VatNo");
			bulkCopy.ColumnMappings.Add("CityName", "CityName");
			
			foreach(LMMR.Entities.FinancialInfo entity in entities)
			{
				if (entity.EntityState != EntityState.Added)
					continue;
					
				DataRow row = dataTable.NewRow();
				
					row["Id"] = entity.Id;
							
				
					row["Name"] = entity.Name;
							
				
					row["Department"] = entity.Department;
							
				
					row["CommunicationEmail"] = entity.CommunicationEmail;
							
				
					row["CountryId"] = entity.CountryId.HasValue ? (object) entity.CountryId  : System.DBNull.Value;
							
				
					row["CityId"] = entity.CityId.HasValue ? (object) entity.CityId  : System.DBNull.Value;
							
				
					row["Address"] = entity.Address;
							
				
					row["Phone"] = entity.Phone;
							
				
					row["CountryCode"] = entity.CountryCode;
							
				
					row["PostalCode"] = entity.PostalCode;
							
				
					row["UserID"] = entity.UserId.HasValue ? (object) entity.UserId  : System.DBNull.Value;
							
				
					row["VatValue"] = entity.VatValue.HasValue ? (object) entity.VatValue  : System.DBNull.Value;
							
				
					row["IsDeleted"] = entity.IsDeleted;
							
				
					row["DeletedDate"] = entity.DeletedDate.HasValue ? (object) entity.DeletedDate  : System.DBNull.Value;
							
				
					row["VatNo"] = entity.VatNo;
							
				
					row["CityName"] = entity.CityName;
							
				
				dataTable.Rows.Add(row);
			}		
			
			// send the data to the server		
			bulkCopy.WriteToServer(dataTable);		
			
			// update back the state
			foreach(LMMR.Entities.FinancialInfo entity in entities)
			{
				if (entity.EntityState != EntityState.Added)
					continue;
			
				entity.AcceptChanges();
			}
		}
				
		/// <summary>
		/// 	Inserts a LMMR.Entities.FinancialInfo object into the datasource using a transaction.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">LMMR.Entities.FinancialInfo object to insert.</param>
		/// <remarks>
		///		After inserting into the datasource, the LMMR.Entities.FinancialInfo object will be updated
		/// 	to refelect any changes made by the datasource. (ie: identity or computed columns)
		/// </remarks>	
		/// <returns>Returns true if operation is successful.</returns>
        /// <exception cref="System.Exception">The command could not be executed.</exception>
        /// <exception cref="System.Data.DataException">The <paramref name="transactionManager"/> is not open.</exception>
        /// <exception cref="System.Data.Common.DbException">The command could not be executed.</exception>
		public override bool Insert(TransactionManager transactionManager, LMMR.Entities.FinancialInfo entity)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			DbCommand commandWrapper = StoredProcedureProvider.GetCommandWrapper(database, "dbo.FinancialInfo_Insert", _useStoredProcedure);
			
			database.AddOutParameter(commandWrapper, "@Id", DbType.Int64, 8);
			database.AddInParameter(commandWrapper, "@Name", DbType.String, entity.Name );
			database.AddInParameter(commandWrapper, "@Department", DbType.String, entity.Department );
			database.AddInParameter(commandWrapper, "@CommunicationEmail", DbType.String, entity.CommunicationEmail );
			database.AddInParameter(commandWrapper, "@CountryId", DbType.Int32, (entity.CountryId.HasValue ? (object) entity.CountryId  : System.DBNull.Value));
			database.AddInParameter(commandWrapper, "@CityId", DbType.Int32, (entity.CityId.HasValue ? (object) entity.CityId  : System.DBNull.Value));
			database.AddInParameter(commandWrapper, "@Address", DbType.String, entity.Address );
			database.AddInParameter(commandWrapper, "@Phone", DbType.String, entity.Phone );
			database.AddInParameter(commandWrapper, "@CountryCode", DbType.String, entity.CountryCode );
			database.AddInParameter(commandWrapper, "@PostalCode", DbType.String, entity.PostalCode );
			database.AddInParameter(commandWrapper, "@UserId", DbType.Int64, (entity.UserId.HasValue ? (object) entity.UserId  : System.DBNull.Value));
			database.AddInParameter(commandWrapper, "@VatValue", DbType.Int64, (entity.VatValue.HasValue ? (object) entity.VatValue  : System.DBNull.Value));
			database.AddInParameter(commandWrapper, "@IsDeleted", DbType.Boolean, entity.IsDeleted );
			database.AddInParameter(commandWrapper, "@DeletedDate", DbType.DateTime, (entity.DeletedDate.HasValue ? (object) entity.DeletedDate  : System.DBNull.Value));
			database.AddInParameter(commandWrapper, "@VatNo", DbType.String, entity.VatNo );
			database.AddInParameter(commandWrapper, "@CityName", DbType.String, entity.CityName );
			
			int results = 0;
			
			//Provider Data Requesting Command Event
			OnDataRequesting(new CommandEventArgs(commandWrapper, "Insert", entity));
				
			if (transactionManager != null)
			{
				results = Utility.ExecuteNonQuery(transactionManager, commandWrapper);
			}
			else
			{
				results = Utility.ExecuteNonQuery(database,commandWrapper);
			}
					
			object _id = database.GetParameterValue(commandWrapper, "@Id");
			entity.Id = (System.Int64)_id;
			
			
			entity.AcceptChanges();
	
			//Provider Data Requested Command Event
			OnDataRequested(new CommandEventArgs(commandWrapper, "Insert", entity));

			return Convert.ToBoolean(results);
		}	
		#endregion

		#region Update Methods
				
		/// <summary>
		/// 	Update an existing row in the datasource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">LMMR.Entities.FinancialInfo object to update.</param>
		/// <remarks>
		///		After updating the datasource, the LMMR.Entities.FinancialInfo object will be updated
		/// 	to refelect any changes made by the datasource. (ie: identity or computed columns)
		/// </remarks>
		/// <returns>Returns true if operation is successful.</returns>
        /// <exception cref="System.Exception">The command could not be executed.</exception>
        /// <exception cref="System.Data.DataException">The <paramref name="transactionManager"/> is not open.</exception>
        /// <exception cref="System.Data.Common.DbException">The command could not be executed.</exception>
		public override bool Update(TransactionManager transactionManager, LMMR.Entities.FinancialInfo entity)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			DbCommand commandWrapper = StoredProcedureProvider.GetCommandWrapper(database, "dbo.FinancialInfo_Update", _useStoredProcedure);
			
			database.AddInParameter(commandWrapper, "@Id", DbType.Int64, entity.Id );
			database.AddInParameter(commandWrapper, "@Name", DbType.String, entity.Name );
			database.AddInParameter(commandWrapper, "@Department", DbType.String, entity.Department );
			database.AddInParameter(commandWrapper, "@CommunicationEmail", DbType.String, entity.CommunicationEmail );
			database.AddInParameter(commandWrapper, "@CountryId", DbType.Int32, (entity.CountryId.HasValue ? (object) entity.CountryId : System.DBNull.Value) );
			database.AddInParameter(commandWrapper, "@CityId", DbType.Int32, (entity.CityId.HasValue ? (object) entity.CityId : System.DBNull.Value) );
			database.AddInParameter(commandWrapper, "@Address", DbType.String, entity.Address );
			database.AddInParameter(commandWrapper, "@Phone", DbType.String, entity.Phone );
			database.AddInParameter(commandWrapper, "@CountryCode", DbType.String, entity.CountryCode );
			database.AddInParameter(commandWrapper, "@PostalCode", DbType.String, entity.PostalCode );
			database.AddInParameter(commandWrapper, "@UserId", DbType.Int64, (entity.UserId.HasValue ? (object) entity.UserId : System.DBNull.Value) );
			database.AddInParameter(commandWrapper, "@VatValue", DbType.Int64, (entity.VatValue.HasValue ? (object) entity.VatValue : System.DBNull.Value) );
			database.AddInParameter(commandWrapper, "@IsDeleted", DbType.Boolean, entity.IsDeleted );
			database.AddInParameter(commandWrapper, "@DeletedDate", DbType.DateTime, (entity.DeletedDate.HasValue ? (object) entity.DeletedDate : System.DBNull.Value) );
			database.AddInParameter(commandWrapper, "@VatNo", DbType.String, entity.VatNo );
			database.AddInParameter(commandWrapper, "@CityName", DbType.String, entity.CityName );
			
			int results = 0;
			
			//Provider Data Requesting Command Event
			OnDataRequesting(new CommandEventArgs(commandWrapper, "Update", entity));

			if (transactionManager != null)
			{
				results = Utility.ExecuteNonQuery(transactionManager, commandWrapper);
			}
			else
			{
				results = Utility.ExecuteNonQuery(database,commandWrapper);
			}
			
			//Stop Tracking Now that it has been updated and persisted.
			if (DataRepository.Provider.EnableEntityTracking)
            {
                EntityManager.StopTracking(entity.EntityTrackingKey);				
            }
			
			
			entity.AcceptChanges();
			
			//Provider Data Requested Command Event
			OnDataRequested(new CommandEventArgs(commandWrapper, "Update", entity));

			return Convert.ToBoolean(results);
		}
			
		#endregion
		
		#region Custom Methods
	
		#endregion
	}//end class
} // end namespace
