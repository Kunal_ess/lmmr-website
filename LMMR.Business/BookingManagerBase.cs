﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Data;
using LMMR.Entities;
using System.Configuration;
using System.IO;
using log4net;
using log4net.Config;
using System.Xml;

namespace LMMR.Business
{
    public abstract class BookingManagerBase
    {
        ILog logger = log4net.LogManager.GetLogger(typeof(BookingManagerBase));
        EmailConfigManager em = new EmailConfigManager();
        public BookingManagerBase()
        {
        }

        public void CheckBookingComplete()
        {
            string strWhere = SearchTracerColumn.IsProcessDone + "=0 and DATEDIFF(MINUTE," + SearchTracerColumn.CreationDate + ",getdate()) > 60 and " + SearchTracerColumn.IsBooking + "=1";
            int count = 0;
            TList<SearchTracer> st = DataRepository.SearchTracerProvider.GetPaged(strWhere, string.Empty, 0, int.MaxValue, out count);
            foreach (SearchTracer st1 in st)
            {
                if (st1 != null)
                {
                    if (st1.SearchObject != null)
                    {
                        Createbooking objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), st1.SearchObject);
                        ResetIsReserverFalse(objBooking);
                    }
                }
            }
        }

        public void ResetIsReserverFalse(Createbooking objbooking)
        {
            if (objbooking != null)
            {
                foreach (BookedMR mr in objbooking.MeetingroomList)
                {
                    foreach (BookedMrConfig bmrc in mr.MrDetails)
                    {
                        string whereClause = AvailabilityColumn.HotelId + "=" + objbooking.HotelID + " and " + AvailabilityColumn.AvailabilityDate + "='" + objbooking.ArivalDate.AddDays(bmrc.SelectedDay - 1) + "'";
                        int count = 0;
                        TList<Availability> objAvail = DataRepository.AvailabilityProvider.GetPaged(whereClause, string.Empty, 0, int.MaxValue, out count);
                        foreach (Availability a in objAvail)
                        {
                            string where2 = AvailibilityOfRoomsColumn.AvailibilityIdForBedRoom + "=" + a.Id + " and  " + AvailibilityOfRoomsColumn.RoomId + "=" + mr.MRId + " and " + AvailibilityOfRoomsColumn.RoomType + "=0";
                            int count2 = 0;
                            TList<AvailibilityOfRooms> objAvor = DataRepository.AvailibilityOfRoomsProvider.GetPaged(where2, string.Empty, 0, int.MaxValue, out count2);
                            foreach (AvailibilityOfRooms objA in objAvor)
                            {
                                objA.IsReserved = false;
                                DataRepository.AvailibilityOfRoomsProvider.Update(objA);
                            }
                        }
                    }
                }
                #region statistics
                Viewstatistics objview = new Viewstatistics();
                Statistics ST = new Statistics();
                ST.BookingId = null;
                ST.HotelId = objbooking.HotelID;
                ST.FailureTimeout = 1;//Booking Count
                ST.BookingType = 0;//Booking 0, Request 1
                ST.StatDate = System.DateTime.Now;
                objview.InsertVisitwithBookingid(ST);
                #endregion
            }
        }
        public void ResetIsReserverTrue(Createbooking objbooking)
        {
            if (objbooking != null)
            {
                foreach (BookedMR mr in objbooking.MeetingroomList)
                {
                    foreach (BookedMrConfig bmrc in mr.MrDetails)
                    {
                        string whereClause = AvailabilityColumn.HotelId + "=" + objbooking.HotelID + " and " + AvailabilityColumn.AvailabilityDate + "='" + objbooking.ArivalDate.AddDays(bmrc.SelectedDay - 1) + "'";
                        int count = 0;
                        TList<Availability> objAvail = DataRepository.AvailabilityProvider.GetPaged(whereClause, string.Empty, 0, int.MaxValue, out count);
                        foreach (Availability a in objAvail)
                        {
                            string where2 = AvailibilityOfRoomsColumn.AvailibilityIdForBedRoom + "=" + a.Id + " and  " + AvailibilityOfRoomsColumn.RoomId + "=" + mr.MRId + " and " + AvailibilityOfRoomsColumn.RoomType + "=0";
                            int count2 = 0;
                            TList<AvailibilityOfRooms> objAvor = DataRepository.AvailibilityOfRoomsProvider.GetPaged(where2, string.Empty, 0, int.MaxValue, out count2);
                            foreach (AvailibilityOfRooms objA in objAvor)
                            {
                                objA.IsReserved = true;
                                objA.LastModifyTime = DateTime.Now;
                                DataRepository.AvailibilityOfRoomsProvider.Update(objA);
                            }
                        }
                    }
                }
            }
        }
        public CancelationPolicy CheckCancelationPolicy(Hotel h)
        {
            CancelationPolicy c = null;
            PolicyHotelMapping phm = DataRepository.PolicyHotelMappingProvider.GetByHotelId(h.Id).FirstOrDefault();
            if (phm == null)
            {
                TList<CancelationPolicy> cpol = DataRepository.CancelationPolicyProvider.GetByCountryId(h.CountryId);
                if (cpol.Count > 1)
                {
                    c = cpol.Where(a => a.DefaultCountry == true).FirstOrDefault();
                }
                else
                {
                    c = cpol.FirstOrDefault();
                }
            }
            else
            {
                c = DataRepository.CancelationPolicyProvider.GetById(Convert.ToInt64(phm.PolicyId));
            }
            try
            {
                DataRepository.CancelationPolicyProvider.DeepLoad(c);
            }
            catch
            {
            }
            return c;
        }
        public List<Createbooking> StartSearchByHotelId(Int64 Hotelid)
        {
            List<Createbooking> objCreate = new List<Createbooking>();
            Createbooking objCreateBooking = new Createbooking();
            objCreateBooking.HotelID = Hotelid;
            objCreate.Add(objCreateBooking);
            return objCreate;
        }
        #region Create Booking by Agency
        public bool CreateBookingByAgency(Createbooking sb, ref Int64 bookingids, string Filename, decimal netto, long AgentID, long SelectedUserID, BookType b)
        {
            bool result = false;
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Booking objBooking = new Booking();
                objBooking.ArrivalDate = sb.ArivalDate;
                objBooking.DepartureDate = sb.DepartureDate;
                objBooking.CreatorId = AgentID;
                objBooking.Duration = sb.Duration;
                objBooking.HotelId = sb.HotelID;
                objBooking.MainMeetingRoomId = sb.MeetingroomList[0].MRId;
                objBooking.BookingDate = DateTime.Now;
                objBooking.SpecialRequest = sb.SpecialRequest;
                objBooking.IsBedroom = sb.ManageAccomodationLst.Count > 0 ? false : true;
                objBooking.BookType = 0;
                objBooking.IsUserBookingProcessDone = true;
                objBooking.BedRoomTotalPrice = sb.AccomodationPriceTotal;
                objBooking.AgencyClientId = SelectedUserID;
                objBooking.AgencyUserId = AgentID;
                objBooking.BookingXml = TrailManager.XmlSerialize(sb);
                objBooking.RequestStatus = (int)BookingRequestStatus.New;
                objBooking.BookType = (int)b;
                objBooking.FinalTotalPrice = sb.TotalBookingPrice;
                objBooking.RevenueAmount = netto;
                objBooking.ChannelBy = sb.BookType;
                objBooking.ChannelId = sb.ChannelID;
                if (DataRepository.BookingProvider.Insert(tm, objBooking))
                {
                    if (b != BookType.ConvertToRequest)
                    {
                        if (!sb.NoAccomodation)
                        {
                            string whereClause = AvailabilityColumn.AvailabilityDate + " between '" + sb.ArivalDate.AddDays(-2) + "' and '" + (sb.Duration == 1 ? sb.ArivalDate.AddDays(2) : sb.DepartureDate.AddDays(2)) + "' and " + AvailabilityColumn.HotelId + "=" + sb.HotelID;
                            int count = 0;
                            TList<Availability> avail = DataRepository.AvailabilityProvider.GetPaged(tm, whereClause, string.Empty, 0, int.MaxValue, out count);
                            foreach (Availability a in avail)
                            {
                                int count2 = 0;
                                foreach (long am in sb.ManageAccomodationLst.Select(p => p.BedroomId).Distinct())
                                {
                                    string where = AvailibilityOfRoomsColumn.AvailibilityIdForBedRoom + "=" + a.Id + " and " + AvailibilityOfRoomsColumn.RoomId + "=" + am + " and " + AvailibilityOfRoomsColumn.RoomType + "=1";
                                    TList<AvailibilityOfRooms> availRoom = DataRepository.AvailibilityOfRoomsProvider.GetPaged(tm, where, string.Empty, 0, int.MaxValue, out count2);
                                    foreach (AvailibilityOfRooms aor in availRoom)
                                    {
                                        Accomodation aselected = sb.ManageAccomodationLst.Where(n => n.BedroomId == am && n.DateRequest == a.AvailabilityDate).FirstOrDefault();
                                        if (aselected != null)
                                        {
                                            //aor.NumberOfRoomsAvailable = aor.NumberOfRoomsAvailable - (aselected.QuantityDouble + aselected.QuantitySingle);
                                            aor.NumberOfRoomBooked = Convert.ToInt64(aor.NumberOfRoomBooked) + (aselected.QuantityDouble + aselected.QuantitySingle);
                                            if (aor.NumberOfRoomsAvailable <= aor.NumberOfRoomBooked)// && aor.NumberOfRoomsAvailable!=0)
                                            {
                                                aor.MorningStatus = (int)AvailabilityStatus.BOOKED;
                                                //aor.NumberOfRoomsAvailable = 0;
                                            }
                                            DataRepository.AvailibilityOfRoomsProvider.Update(tm, aor);
                                        }
                                    }
                                }
                            }
                        }
                        //int counterMr = 0;
                        foreach (BookedMR bmr in sb.MeetingroomList)
                        {
                            //counterMr++;
                            foreach (BookedMrConfig bmrconfig in bmr.MrDetails)
                            {
                                BookedMeetingRoom objbmr = new BookedMeetingRoom();
                                objbmr.BookingId = objBooking.Id;
                                objbmr.MeetingRoomId = bmr.MRId;
                                objbmr.MeetingRoomConfigId = bmr.MrConfigId;
                                objbmr.NoofParticipants = bmrconfig.NoOfParticepant;
                                objbmr.StartTime = bmrconfig.FromTime;
                                objbmr.EndTime = bmrconfig.ToTime;
                                objbmr.MeetingDate = sb.ArivalDate.AddDays(bmrconfig.SelectedDay - 1);
                                objbmr.MeetingDay = bmrconfig.SelectedDay;
                                //Save Details of Packages according to meetingroom.

                                string whereClause = AvailabilityColumn.AvailabilityDate + "='" + sb.ArivalDate.AddDays(bmrconfig.SelectedDay - 1) + "' and " + AvailabilityColumn.HotelId + "=" + sb.HotelID;
                                int count = 0;
                                TList<Availability> avail = DataRepository.AvailabilityProvider.GetPaged(tm, whereClause, string.Empty, 0, int.MaxValue, out count);
                                foreach (Availability a in avail)
                                {
                                    int count2 = 0;
                                    string where = AvailibilityOfRoomsColumn.AvailibilityIdForBedRoom + "=" + a.Id + " and " + AvailibilityOfRoomsColumn.RoomId + "=" + bmr.MRId + " and " + AvailibilityOfRoomsColumn.RoomType + "=0";
                                    TList<AvailibilityOfRooms> availRoom = DataRepository.AvailibilityOfRoomsProvider.GetPaged(tm, where, string.Empty, 0, int.MaxValue, out count2);
                                    foreach (AvailibilityOfRooms aor in availRoom)
                                    {
                                        if (bmrconfig.SelectedTime == 0 || bmrconfig.SelectedTime == 1)
                                        {
                                            aor.MorningStatus = (int)AvailabilityStatus.BOOKED;
                                        }
                                        if (bmrconfig.SelectedTime == 0 || bmrconfig.SelectedTime == 2)
                                        {
                                            aor.AfternoonStatus = (int)AvailabilityStatus.BOOKED;
                                        }
                                        aor.IsReserved = false;
                                        DataRepository.AvailibilityOfRoomsProvider.Update(tm, aor);
                                    }
                                    string where2 = ViewSelectAvailabilityColumn.HotelId + "=" + sb.HotelID + " and " + ViewSelectAvailabilityColumn.RoomType + "=0 and " + ViewSelectAvailabilityColumn.AvailabilityDate + "='" + a.AvailabilityDate + "'";
                                    VList<ViewSelectAvailability> availRoom2 = DataRepository.ViewSelectAvailabilityProvider.GetPaged(tm, where2, string.Empty, 0, int.MaxValue, out count2);
                                    int Count2 = availRoom2.Where(c => c.AfternoonStatus == (int)AvailabilityStatus.BOOKED && c.MorningStatus == (int)AvailabilityStatus.BOOKED).Count();
                                    if (Count2 == availRoom2.Count && availRoom2.Count != 0)
                                    {
                                        foreach (ViewSelectAvailability vaor in availRoom2)
                                        {
                                            AvailibilityOfRooms aor = (AvailibilityOfRooms)DataRepository.AvailibilityOfRoomsProvider.GetById(tm, vaor.AvailRoomId);
                                            aor.MorningStatus = (int)AvailabilityStatus.SOLD;
                                            aor.AfternoonStatus = (int)AvailabilityStatus.SOLD;
                                            DataRepository.AvailibilityOfRoomsProvider.Update(tm, aor);
                                        }
                                        a.FullPropertyStatus = (int)AvailabilityStatus.SOLD;
                                        a.FullProrertyStatusBr = (int)AvailabilityStatus.SOLD;
                                        DataRepository.AvailabilityProvider.Update(tm, a);
                                    }
                                }
                                DataRepository.BookedMeetingRoomProvider.Insert(tm, objbmr);
                            }
                        }
                    }
                }

                result = true;
                bookingids = objBooking.Id;

                #region log
                BookingLogs bklog = new BookingLogs();
                bklog.BookingId = objBooking.Id;
                bklog.CurrentStatus = BookingRequestStatus.New.ToString().ToUpper();
                bklog.LastStatus = "";
                bklog.HotelId = objBooking.HotelId;
                bklog.UserId = Convert.ToInt32(objBooking.CreatorId);
                bklog.ModifyDate = System.DateTime.Now;
                DataRepository.BookingLogsProvider.Insert(tm, bklog);
                #endregion


                #region statistics
                Viewstatistics objview = new Viewstatistics();
                Statistics ST = new Statistics();
                ST.BookingId = objBooking.Id;
                ST.HotelId = objBooking.HotelId;
                if (b == BookType.Booking)
                {
                    ST.Booking = 1;//Booking Count
                }
                else
                {
                    ST.Request = 1;
                }
                ST.BookingType = (int)b;//Booking 0, Request 1
                ST.StatDate = System.DateTime.Now;
                objview.InsertVisitwithBookingid(ST);
                #endregion

                try
                {
                    #region SendMail
                    SendMails objSendmail = new SendMails();
                    objSendmail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
                    Users objUser = new HotelManager().GetFirstOperatorInDataBase();
                    Hotel objHotel = DataRepository.HotelProvider.GetById(tm, objBooking.HotelId);
                    Users objUserCreator = DataRepository.UsersProvider.GetByUserId(tm, objBooking.CreatorId);
                    UserDetails ud = DataRepository.UserDetailsProvider.GetByUserId(objUserCreator.UserId).FirstOrDefault();
                    TList<HotelContact> hc = DataRepository.HotelContactProvider.GetByHotelId(tm, objHotel.Id);
                    objSendmail.ToEmail = objUserCreator.EmailId;
                    objSendmail.cc = objUser.EmailId;
                    string bodymsgc = string.Empty;
                    objSendmail.Bcc = ConfigurationManager.AppSettings["WatchEMailID"];
                    if (false)
                    {
                        bodymsgc = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/WLMail.html");
                    }
                    else
                    {
                        bodymsgc = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
                    }
                    string bodymsg3 = Convert.ToString(bodymsgc);

                    if (b != BookType.ConvertToRequest)
                    {

                        #region START booking details for hotel
                        EmailConfig eConfig2 = em.GetByName("Booking details for hotel");
                        if (eConfig2.IsActive)
                        {
                            objSendmail.ToEmail = objHotel.Email;
                            foreach (HotelContact hcon in hc)
                            {
                                if (hcon.Email != objHotel.Email && hcon.ContactType == "PrimaryContact2" && hcon.UserType == "WeekEnd" && (DateTime.Now.DayOfWeek == DayOfWeek.Saturday || DateTime.Now.DayOfWeek == DayOfWeek.Sunday))
                                {
                                    if (!string.IsNullOrEmpty(hcon.Email))
                                    {
                                        objSendmail.ToEmail += "," + hcon.Email;
                                    }
                                }
                                else if (hcon.Email != objHotel.Email && hcon.ContactType == "PrimaryContact1")
                                {
                                    if (!string.IsNullOrEmpty(hcon.Email))
                                    {
                                        objSendmail.ToEmail += "," + hcon.Email;
                                    }
                                }
                                #region for secondary contact
                                if (hcon.Email != objHotel.Email && hcon.ContactType == "SecondaryContact2" && hcon.UserType == "WeekEnd" && (DateTime.Now.DayOfWeek == DayOfWeek.Saturday || DateTime.Now.DayOfWeek == DayOfWeek.Sunday))
                                {
                                    if (!string.IsNullOrEmpty(hcon.Email))
                                    {
                                        objSendmail.ToEmail += "," + hcon.Email;
                                    }
                                }
                                else if (hcon.Email != objHotel.Email && hcon.ContactType == "SecondaryContact1")
                                {
                                    if (!string.IsNullOrEmpty(hcon.Email))
                                    {
                                        objSendmail.ToEmail += "," + hcon.Email;
                                    }
                                }
                                #endregion
                            }
                            EmailConfigMapping emap2 = new EmailConfigMapping();
                            emap2 = eConfig2.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                            bodymsg3 = bodymsg3.Replace("@CONTENTS", emap2.EmailContents);

                            EmailValueCollection objEmailValues3 = new EmailValueCollection();
                            objSendmail.Subject = "New booking from lastminutemeetingroom.com ID:" + objBooking.Id.ToString();
                            //objSendmail.Attachment.Add(new System.Net.Mail.Attachment(Filename));
                            foreach (KeyValuePair<string, string> strKey in objEmailValues3.EmailForBookingDone("Supplier", objBooking.Id.ToString(), objHotel.Name, objUserCreator.FirstName + " " + objUserCreator.LastName, "Support team Lastminutemeetingroom"))
                            {
                                bodymsg3 = bodymsg3.Replace(strKey.Key, strKey.Value);
                            }
                            objSendmail.Body = bodymsg3;
                            objSendmail.SendMail();
                        }
                        #endregion
                        #region START booking details for user
                        EmailConfig eConfig = new EmailConfig();
                        if (false)
                        {
                            eConfig = em.GetByName("Send WL Booking Mail");
                        }
                        else
                        {
                            eConfig = em.GetByName("booking details for user");
                        }
                        if (eConfig.IsActive)
                        {
                            EmailConfigMapping emap = new EmailConfigMapping();
                            if (System.Web.HttpContext.Current.Session["LanguageID"] != null)
                            {
                                emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == Convert.ToInt64(System.Web.HttpContext.Current.Session["LanguageID"])).FirstOrDefault();
                            }
                            else
                            {
                                if (ud == null)
                                {
                                    emap = null;
                                }
                                else
                                {
                                    emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == ud.LanguageId).FirstOrDefault();
                                }
                            }
                            if (emap != null)
                            {
                                bodymsgc = bodymsgc.Replace("@CONTENTS", emap.EmailContents);
                            }
                            else
                            {
                                emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                                bodymsgc = bodymsgc.Replace("@CONTENTS", emap.EmailContents);
                            }
                            EmailValueCollection objEmailValuesc = new EmailValueCollection();
                            objSendmail.Subject = "Booking Confirmation ID:" + objBooking.Id.ToString();
                            objSendmail.Attachment.Add(new System.Net.Mail.Attachment(Filename));
                            if (false)
                            {
                                foreach (KeyValuePair<string, string> strKey in objEmailValuesc.EmailForBookingDoneByWL(objUserCreator.FirstName + " " + objUserCreator.LastName, objBooking.Id.ToString(), objHotel.Name, objUserCreator.FirstName + " " + objUserCreator.LastName, "Last Minute Meeting Room Admin", string.Empty))
                                {
                                    bodymsgc = bodymsgc.Replace(strKey.Key, strKey.Value);
                                }
                            }
                            else
                            {
                                foreach (KeyValuePair<string, string> strKey in objEmailValuesc.EmailForBookingDone(objUserCreator.FirstName + " " + objUserCreator.LastName, objBooking.Id.ToString(), objHotel.Name, objUserCreator.FirstName + " " + objUserCreator.LastName, "Last Minute Meeting Room Admin"))
                                {
                                    bodymsgc = bodymsgc.Replace(strKey.Key, strKey.Value);
                                }
                            }
                            objSendmail.Body = bodymsgc;
                            objSendmail.SendMail();
                        }
                        #endregion
                    }
                    else
                    {
                        #region START Request to the hotel
                        string HotelNames = "";
                        EmailConfig eConfig2 = em.GetByName("Request details for hotel");
                        if (eConfig2.IsActive)
                        {
                            objSendmail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
                            objSendmail.ToEmail = objHotel.Email;
                            foreach (HotelContact hcon in hc)
                            {
                                if (hcon.Email != objHotel.Email && hcon.ContactType == "PrimaryContact2" && hcon.UserType == "WeekEnd" && (DateTime.Now.DayOfWeek == DayOfWeek.Saturday || DateTime.Now.DayOfWeek == DayOfWeek.Sunday))
                                {
                                    if (!string.IsNullOrEmpty(hcon.Email))
                                    {
                                        objSendmail.ToEmail += "," + hcon.Email;
                                    }
                                }
                                else if (hcon.Email != objHotel.Email && hcon.ContactType == "PrimaryContact1")
                                {
                                    if (!string.IsNullOrEmpty(hcon.Email))
                                    {
                                        objSendmail.ToEmail += "," + hcon.Email;
                                    }
                                }
                                #region for secondary contact
                                if (hcon.Email != objHotel.Email && hcon.ContactType == "SecondaryContact2" && hcon.UserType == "WeekEnd" && (DateTime.Now.DayOfWeek == DayOfWeek.Saturday || DateTime.Now.DayOfWeek == DayOfWeek.Sunday))
                                {
                                    if (!string.IsNullOrEmpty(hcon.Email))
                                    {
                                        objSendmail.ToEmail += "," + hcon.Email;
                                    }
                                }
                                else if (hcon.Email != objHotel.Email && hcon.ContactType == "SecondaryContact1")
                                {
                                    if (!string.IsNullOrEmpty(hcon.Email))
                                    {
                                        objSendmail.ToEmail += "," + hcon.Email;
                                    }
                                }
                                #endregion
                            }
                            bodymsg3 = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
                            EmailConfigMapping emap2 = new EmailConfigMapping();
                            emap2 = eConfig2.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                            bodymsg3 = bodymsg3.Replace("@CONTENTS", emap2.EmailContents);
                            EmailValueCollection objEmailValues3 = new EmailValueCollection();
                            objSendmail.Subject = "New Request from lastminutemeetingroom.com ID:" + objBooking.Id.ToString();
                            if (objSendmail.Attachment != null)
                            {
                                if (objSendmail.Attachment.Count > 0)
                                {
                                    objSendmail.Attachment.RemoveAt(0);
                                }
                            }
                            foreach (KeyValuePair<string, string> strKey in objEmailValues3.EmailForRequestDone("Supplier", objBooking.Id.ToString(), objHotel.Name, objUserCreator.FirstName + " " + objUserCreator.LastName, "Support team Lastminutemeetingroom "))
                            {
                                bodymsg3 = bodymsg3.Replace(strKey.Key, strKey.Value);
                            }
                            objSendmail.Body = bodymsg3;
                            objSendmail.SendMail();
                        }
                        HotelNames += objHotel.Name;
                        #endregion
                        #region START Request To the User
                        EmailConfig eConfig = new EmailConfig();
                        if (false)
                        {
                            eConfig = em.GetByName("Send WL Request  Mail");
                        }
                        else
                        {
                            eConfig = em.GetByName("Request details for user");
                        }
                        if (eConfig.IsActive)
                        {
                            string bodymsg2 = string.Empty;
                            if (false)
                            {
                                bodymsg2 = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/WLMail.html");
                            }
                            else
                            {
                                bodymsg2 = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
                            }
                            SendMails objSendmail2 = new SendMails();
                            objSendmail2.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];

                            objSendmail2.ToEmail = objUserCreator.EmailId;
                            objSendmail2.cc = objUser.EmailId;
                            objSendmail2.Bcc = ConfigurationManager.AppSettings["WatchEMailID"];
                            EmailConfigMapping emap = new EmailConfigMapping();
                            if (System.Web.HttpContext.Current.Session["LanguageID"] != null)
                            {
                                emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == Convert.ToInt64(System.Web.HttpContext.Current.Session["LanguageID"])).FirstOrDefault();
                            }
                            else
                            {
                                if (ud == null)
                                {
                                    emap = null;
                                }
                                else
                                {
                                    emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == ud.LanguageId).FirstOrDefault();
                                }
                            }
                            if (emap != null)
                            {
                                bodymsg2 = bodymsg2.Replace("@CONTENTS", emap.EmailContents);
                            }
                            else
                            {
                                emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                                bodymsg2 = bodymsg2.Replace("@CONTENTS", emap.EmailContents);
                            }
                            EmailValueCollection objEmailValues = new EmailValueCollection();
                            objSendmail2.Subject = "Request ID:" + bookingids;
                            if (objSendmail2.Attachment.Count <= 0)
                            {
                                objSendmail2.Attachment.Add(new System.Net.Mail.Attachment(Filename));
                            }
                            if (false)
                            {
                                foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForRequestDoneByWL(objUserCreator.FirstName + " " + objUserCreator.LastName, bookingids.ToString(), HotelNames, objUserCreator.FirstName + " " + objUserCreator.LastName, "Support team Lastminutemeetingroom ", string.Empty))
                                {
                                    bodymsg2 = bodymsg2.Replace(strKey.Key, strKey.Value);
                                }
                            }
                            else
                            {
                                foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForRequestDone(objUserCreator.FirstName + " " + objUserCreator.LastName, bookingids.ToString(), HotelNames, objUserCreator.FirstName + " " + objUserCreator.LastName, "Support team Lastminutemeetingroom "))
                                {
                                    bodymsg2 = bodymsg2.Replace(strKey.Key, strKey.Value);
                                }
                            }
                            objSendmail2.Body = bodymsg2;
                            objSendmail2.SendMail();
                        }
                        #endregion
                    }

                    
                    #endregion
                }
                catch
                {
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
                result = false;
                logger.Error(ex);
                throw ex;
            }
            return result;
        }
        #endregion

        #region Create Booking for Normal User and With White lable
        public bool CreateBooking(Createbooking sb, ref Int64 bookingids, string Filename, decimal netto, bool isWl, string wlLogo,BookType b)
        {
            bool result = false;
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Booking objBooking = new Booking();
                objBooking.ArrivalDate = sb.ArivalDate;
                objBooking.DepartureDate = sb.DepartureDate;
                objBooking.CreatorId = sb.CurrentUserId;
                objBooking.Duration = sb.Duration;
                objBooking.HotelId = sb.HotelID;
                objBooking.MainMeetingRoomId = sb.MeetingroomList[0].MRId;
                objBooking.BookingDate = DateTime.Now;
                objBooking.SpecialRequest = sb.SpecialRequest;
                objBooking.IsBedroom = sb.ManageAccomodationLst.Count > 0 ? false : true;
                objBooking.BookType = 0;
                objBooking.IsUserBookingProcessDone = true;
                objBooking.BedRoomTotalPrice = sb.AccomodationPriceTotal;
                objBooking.BookingXml = TrailManager.XmlSerialize(sb);
                objBooking.RequestStatus = (int)BookingRequestStatus.New;
                objBooking.BookType = (int)b;
                objBooking.FinalTotalPrice = sb.TotalBookingPrice;
                objBooking.RevenueAmount = netto;
                objBooking.ChannelBy = sb.BookType;
                objBooking.ChannelId = sb.ChannelID;
                if (DataRepository.BookingProvider.Insert(tm, objBooking))
                {
                    if (b != BookType.ConvertToRequest)
                    {
                        if(!sb.NoAccomodation)
                        {
                            string whereClause = AvailabilityColumn.AvailabilityDate + " between '" + sb.ArivalDate.AddDays(-2) + "' and '" + (sb.Duration == 1 ? sb.ArivalDate.AddDays(2) : sb.DepartureDate.AddDays(2)) + "' and " + AvailabilityColumn.HotelId + "=" + sb.HotelID;
                            int count = 0;
                            TList<Availability> avail = DataRepository.AvailabilityProvider.GetPaged(tm, whereClause, string.Empty, 0, int.MaxValue, out count);
                            foreach (Availability a in avail)
                            {
                                int count2 = 0;
                                foreach (long am in sb.ManageAccomodationLst.Select(p=>p.BedroomId).Distinct())
                                {
                                    string where = AvailibilityOfRoomsColumn.AvailibilityIdForBedRoom + "=" + a.Id + " and " + AvailibilityOfRoomsColumn.RoomId + "=" + am + " and " + AvailibilityOfRoomsColumn.RoomType + "=1";
                                    TList<AvailibilityOfRooms> availRoom = DataRepository.AvailibilityOfRoomsProvider.GetPaged(tm, where, string.Empty, 0, int.MaxValue, out count2);
                                    foreach (AvailibilityOfRooms aor in availRoom)
                                    {
                                        Accomodation aselected = sb.ManageAccomodationLst.Where(n => n.BedroomId == am && n.DateRequest == a.AvailabilityDate).FirstOrDefault();
                                        if (aselected != null)
                                        {
                                            //aor.NumberOfRoomsAvailable = aor.NumberOfRoomsAvailable - (aselected.QuantityDouble + aselected.QuantitySingle);
                                            aor.NumberOfRoomBooked = Convert.ToInt64(aor.NumberOfRoomBooked) + (aselected.QuantityDouble + aselected.QuantitySingle);
                                            if (aor.NumberOfRoomsAvailable <= aor.NumberOfRoomBooked)// && aor.NumberOfRoomsAvailable != 0)
                                            {
                                                aor.MorningStatus = (int)AvailabilityStatus.BOOKED;
                                                //aor.NumberOfRoomsAvailable = 0;
                                            }
                                            DataRepository.AvailibilityOfRoomsProvider.Update(tm, aor);
                                        }
                                    }
                                }
                            }
                        }
                        //int counterMr = 0;
                        foreach (BookedMR bmr in sb.MeetingroomList)
                        {
                            //counterMr++;
                            foreach (BookedMrConfig bmrconfig in bmr.MrDetails)
                            {
                                BookedMeetingRoom objbmr = new BookedMeetingRoom();
                                objbmr.BookingId = objBooking.Id;
                                objbmr.MeetingRoomId = bmr.MRId;
                                objbmr.MeetingRoomConfigId = bmr.MrConfigId;
                                objbmr.NoofParticipants = bmrconfig.NoOfParticepant;
                                objbmr.StartTime = bmrconfig.FromTime;
                                objbmr.EndTime = bmrconfig.ToTime;
                                objbmr.MeetingDate = sb.ArivalDate.AddDays(bmrconfig.SelectedDay - 1);
                                objbmr.MeetingDay = bmrconfig.SelectedDay;
                                //Save Details of Packages according to meetingroom.

                                string whereClause = AvailabilityColumn.AvailabilityDate + "='" + sb.ArivalDate.AddDays(bmrconfig.SelectedDay - 1) + "' and " + AvailabilityColumn.HotelId + "=" + sb.HotelID;
                                //if (counterMr==2)
                                //{
                                //    if (bmrconfig.SelectedDay == 1)
                                //    {
                                //        whereClause = AvailabilityColumn.AvailabilityDate + "='" + sb.ArivalDate + "' and " + AvailabilityColumn.HotelId + "=" + sb.HotelID;
                                //    }
                                //    else
                                //    {
                                //        whereClause = AvailabilityColumn.AvailabilityDate + "='" + sb.DepartureDate + "' and " + AvailabilityColumn.HotelId + "=" + sb.HotelID;
                                //    }
                                //}
                                int count = 0;
                                TList<Availability> avail = DataRepository.AvailabilityProvider.GetPaged(tm, whereClause, string.Empty, 0, int.MaxValue, out count);
                                foreach (Availability a in avail)
                                {
                                    int count2 = 0;
                                    string where = AvailibilityOfRoomsColumn.AvailibilityIdForBedRoom + "=" + a.Id + " and " + AvailibilityOfRoomsColumn.RoomId + "=" + bmr.MRId + " and " + AvailibilityOfRoomsColumn.RoomType + "=0";
                                    TList<AvailibilityOfRooms> availRoom = DataRepository.AvailibilityOfRoomsProvider.GetPaged(tm, where, string.Empty, 0, int.MaxValue, out count2);
                                    foreach (AvailibilityOfRooms aor in availRoom)
                                    {
                                        if (bmrconfig.SelectedTime == 0 || bmrconfig.SelectedTime == 1)
                                        {
                                            aor.MorningStatus = (int)AvailabilityStatus.BOOKED;
                                        }
                                        if (bmrconfig.SelectedTime == 0 || bmrconfig.SelectedTime == 2)
                                        {
                                            aor.AfternoonStatus = (int)AvailabilityStatus.BOOKED;
                                        }
                                        aor.IsReserved = false;
                                        DataRepository.AvailibilityOfRoomsProvider.Update(tm, aor);
                                    }
                                    string where2 = ViewSelectAvailabilityColumn.HotelId + "=" + sb.HotelID + " and " + ViewSelectAvailabilityColumn.RoomType + "=0 and " + ViewSelectAvailabilityColumn.AvailabilityDate + "='" + a.AvailabilityDate + "'";
                                    VList<ViewSelectAvailability> availRoom2 = DataRepository.ViewSelectAvailabilityProvider.GetPaged(tm, where2, string.Empty, 0, int.MaxValue, out count2);
                                    int Count2 = availRoom2.Where(c => c.AfternoonStatus == (int)AvailabilityStatus.BOOKED && c.MorningStatus == (int)AvailabilityStatus.BOOKED).Count();
                                    if (Count2 == availRoom2.Count && availRoom2.Count != 0)
                                    {
                                        foreach (ViewSelectAvailability vaor in availRoom2)
                                        {
                                            AvailibilityOfRooms aor = (AvailibilityOfRooms)DataRepository.AvailibilityOfRoomsProvider.GetById(tm, vaor.AvailRoomId);
                                            aor.MorningStatus = (int)AvailabilityStatus.SOLD;
                                            aor.AfternoonStatus = (int)AvailabilityStatus.SOLD;
                                            DataRepository.AvailibilityOfRoomsProvider.Update(tm, aor);
                                        }
                                        a.FullPropertyStatus = (int)AvailabilityStatus.SOLD;
                                        a.FullProrertyStatusBr = (int)AvailabilityStatus.SOLD;
                                        DataRepository.AvailabilityProvider.Update(tm, a);
                                    }
                                }
                                //DataRepository.BookedMeetingRoomProvider.Insert(tm, objbmr);
                            }
                        }
                    }
                }

                result = true;
                bookingids = objBooking.Id;

                #region log
                BookingLogs bklog = new BookingLogs();
                bklog.BookingId = objBooking.Id;
                bklog.CurrentStatus = BookingRequestStatus.New.ToString().ToUpper();
                bklog.LastStatus = "";
                bklog.HotelId = objBooking.HotelId;
                bklog.UserId = Convert.ToInt32(objBooking.CreatorId);
                bklog.ModifyDate = System.DateTime.Now;
                DataRepository.BookingLogsProvider.Insert(tm, bklog);
                #endregion


                #region statistics
                Viewstatistics objview = new Viewstatistics();
                Statistics ST = new Statistics();
                ST.BookingId = objBooking.Id;
                ST.HotelId = objBooking.HotelId;
                if (b == BookType.Booking)
                {
                    ST.Booking = 1;//Booking Count
                }
                else
                {
                    ST.Request = 1;
                }
                ST.BookingType = (int)b;//Booking 0, Request 1
                ST.StatDate = System.DateTime.Now;
                objview.InsertVisitwithBookingid(ST);
                #endregion

                try
                {
                    #region SendMail
                    SendMails objSendmail = new SendMails();
                    objSendmail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
                    Users objUser = new HotelManager().GetFirstOperatorInDataBase();
                    Hotel objHotel = DataRepository.HotelProvider.GetById(tm, objBooking.HotelId);
                    Users objUserCreator = DataRepository.UsersProvider.GetByUserId(tm, objBooking.CreatorId);
                    UserDetails ud = DataRepository.UserDetailsProvider.GetByUserId(objUserCreator.UserId).FirstOrDefault();
                    TList<HotelContact> hc = DataRepository.HotelContactProvider.GetByHotelId(tm, objHotel.Id);
                    objSendmail.ToEmail = objUserCreator.EmailId;
                    string bodymsgc = string.Empty;
                    if (isWl)
                    {
                        objSendmail.Bcc = ConfigurationManager.AppSettings["WatchEMailID"] + "," + objUser.EmailId;
                    }
                    else
                    {
                        objSendmail.cc = objUser.EmailId;
                        objSendmail.Bcc = ConfigurationManager.AppSettings["WatchEMailID"];
                    }
                    if (isWl)
                    {
                        bodymsgc = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/WLMail.html");
                    }
                    else
                    {
                        bodymsgc = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
                    }
                    string bodymsg3 = Convert.ToString(bodymsgc);
                    if (b != BookType.ConvertToRequest)
                    {
                        #region START booking details for user
                        EmailConfig eConfig = new EmailConfig();
                        if (isWl)
                        {
                            eConfig = em.GetByName("Send WL Booking Mail");
                        }
                        else
                        {
                            eConfig = em.GetByName("booking details for user");
                        }
                        if (eConfig.IsActive)
                        {
                            EmailConfigMapping emap = new EmailConfigMapping();
                            if (System.Web.HttpContext.Current.Session["LanguageID"] != null)
                            {
                                emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == Convert.ToInt64(System.Web.HttpContext.Current.Session["LanguageID"])).FirstOrDefault();
                            }
                            else
                            {
                                if (ud == null)
                                {
                                    emap = null;
                                }
                                else
                                {
                                    emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == ud.LanguageId).FirstOrDefault();
                                }
                            }
                            if (emap != null)
                            {
                                bodymsgc = bodymsgc.Replace("@CONTENTS", emap.EmailContents);
                            }
                            else
                            {
                                emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                                bodymsgc = bodymsgc.Replace("@CONTENTS", emap.EmailContents);
                            }
                            EmailValueCollection objEmailValuesc = new EmailValueCollection();
                            objSendmail.Subject = "Booking Confirmation ID:" + objBooking.Id.ToString();
                            objSendmail.Attachment.Add(new System.Net.Mail.Attachment(Filename));
                            if (isWl)
                            {
                                foreach (KeyValuePair<string, string> strKey in objEmailValuesc.EmailForBookingDoneByWL(objUserCreator.FirstName + " " + objUserCreator.LastName, objBooking.Id.ToString(), objHotel.Name, objUserCreator.FirstName + " " + objUserCreator.LastName, "Last Minute Meeting Room Admin", wlLogo))
                                {
                                    bodymsgc = bodymsgc.Replace(strKey.Key, strKey.Value);
                                }
                            }
                            else
                            {
                                foreach (KeyValuePair<string, string> strKey in objEmailValuesc.EmailForBookingDone(objUserCreator.FirstName + " " + objUserCreator.LastName, objBooking.Id.ToString(), objHotel.Name, objUserCreator.FirstName + " " + objUserCreator.LastName, "Last Minute Meeting Room Admin"))
                                {
                                    bodymsgc = bodymsgc.Replace(strKey.Key, strKey.Value);
                                }
                            }
                            objSendmail.Body = bodymsgc;
                            objSendmail.SendMail();
                        }
                        #endregion
                        #region START booking details for hotel
                        EmailConfig eConfig2 = em.GetByName("Booking details for hotel");
                        if (eConfig2.IsActive)
                        {
                            objSendmail.ToEmail = objHotel.Email;
                            foreach (HotelContact hcon in hc)
                            {
                                if (hcon.Email != objHotel.Email && hcon.ContactType == "PrimaryContact2" && hcon.UserType == "WeekEnd" && (DateTime.Now.DayOfWeek == DayOfWeek.Saturday || DateTime.Now.DayOfWeek == DayOfWeek.Sunday))
                                {
                                    if (!string.IsNullOrEmpty(hcon.Email))
                                    {
                                        objSendmail.ToEmail += "," + hcon.Email;
                                    }
                                }
                                else if (hcon.Email != objHotel.Email && hcon.ContactType == "PrimaryContact1")
                                {
                                    if (!string.IsNullOrEmpty(hcon.Email))
                                    {
                                        objSendmail.ToEmail += "," + hcon.Email;
                                    }
                                }
                                #region for secondary contact
                                if (hcon.Email != objHotel.Email && hcon.ContactType == "SecondaryContact2" && hcon.UserType == "WeekEnd" && (DateTime.Now.DayOfWeek == DayOfWeek.Saturday || DateTime.Now.DayOfWeek == DayOfWeek.Sunday))
                                {
                                    if (!string.IsNullOrEmpty(hcon.Email))
                                    {
                                        objSendmail.ToEmail += "," + hcon.Email;
                                    }
                                }
                                else if (hcon.Email != objHotel.Email && hcon.ContactType == "SecondaryContact1")
                                {
                                    if (!string.IsNullOrEmpty(hcon.Email))
                                    {
                                        objSendmail.ToEmail += "," + hcon.Email;
                                    }
                                }
                                #endregion
                            }
                            EmailConfigMapping emap2 = new EmailConfigMapping();
                            emap2 = eConfig2.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                            bodymsg3 = bodymsg3.Replace("@CONTENTS", emap2.EmailContents);

                            EmailValueCollection objEmailValues3 = new EmailValueCollection();
                            objSendmail.Subject = "New booking from lastminutemeetingroom.com ID:" + objBooking.Id.ToString();
                            //objSendmail.Attachment.Add(new System.Net.Mail.Attachment(Filename));
                            foreach (KeyValuePair<string, string> strKey in objEmailValues3.EmailForBookingDone("Supplier", objBooking.Id.ToString(), objHotel.Name, objUserCreator.FirstName + " " + objUserCreator.LastName, "Support team Lastminutemeetingroom"))
                            {
                                bodymsg3 = bodymsg3.Replace(strKey.Key, strKey.Value);
                            }
                            objSendmail.Body = bodymsg3;
                            objSendmail.SendMail();
                        }
                        #endregion
                    }
                    else
                    {
                        #region START Request to the hotel
                        string HotelNames = "";
                        EmailConfig eConfig2 = em.GetByName("Request details for hotel");
                        if (eConfig2.IsActive)
                        {
                            objSendmail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
                            objSendmail.ToEmail = objHotel.Email;
                            foreach (HotelContact hcon in hc)
                            {
                                if (hcon.Email != objHotel.Email && hcon.ContactType == "PrimaryContact2" && hcon.UserType == "WeekEnd" && (DateTime.Now.DayOfWeek == DayOfWeek.Saturday || DateTime.Now.DayOfWeek == DayOfWeek.Sunday))
                                {
                                    if (!string.IsNullOrEmpty(hcon.Email))
                                    {
                                        objSendmail.ToEmail += "," + hcon.Email;
                                    }
                                }
                                else if (hcon.Email != objHotel.Email && hcon.ContactType == "PrimaryContact1")
                                {
                                    if (!string.IsNullOrEmpty(hcon.Email))
                                    {
                                        objSendmail.ToEmail += "," + hcon.Email;
                                    }
                                }
                                #region for secondary contact
                                if (hcon.Email != objHotel.Email && hcon.ContactType == "SecondaryContact2" && hcon.UserType == "WeekEnd" && (DateTime.Now.DayOfWeek == DayOfWeek.Saturday || DateTime.Now.DayOfWeek == DayOfWeek.Sunday))
                                {
                                    if (!string.IsNullOrEmpty(hcon.Email))
                                    {
                                        objSendmail.ToEmail += "," + hcon.Email;
                                    }
                                }
                                else if (hcon.Email != objHotel.Email && hcon.ContactType == "SecondaryContact1")
                                {
                                    if (!string.IsNullOrEmpty(hcon.Email))
                                    {
                                        objSendmail.ToEmail += "," + hcon.Email;
                                    }
                                }
                                #endregion
                            }
                            bodymsg3 = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
                            EmailConfigMapping emap2 = new EmailConfigMapping();
                            emap2 = eConfig2.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                            bodymsg3 = bodymsg3.Replace("@CONTENTS", emap2.EmailContents);
                            EmailValueCollection objEmailValues3 = new EmailValueCollection();
                            objSendmail.Subject = "New Request from lastminutemeetingroom.com ID:" + objBooking.Id.ToString();
                            if (objSendmail.Attachment != null)
                            {
                                if (objSendmail.Attachment.Count > 0)
                                {
                                    objSendmail.Attachment.RemoveAt(0);
                                }
                            }
                            foreach (KeyValuePair<string, string> strKey in objEmailValues3.EmailForRequestDone("Supplier", objBooking.Id.ToString(), objHotel.Name, objUserCreator.FirstName + " " + objUserCreator.LastName, "Support team Lastminutemeetingroom "))
                            {
                                bodymsg3 = bodymsg3.Replace(strKey.Key, strKey.Value);
                            }
                            objSendmail.Body = bodymsg3;
                            objSendmail.SendMail();
                        }
                        HotelNames += objHotel.Name;
                        #endregion
                        #region START Request To the User
                        EmailConfig eConfig = new EmailConfig();
                        if (false)
                        {
                            eConfig = em.GetByName("Send WL Request  Mail");
                        }
                        else
                        {
                            eConfig = em.GetByName("Request details for user");
                        }
                        if (eConfig.IsActive)
                        {
                            string bodymsg2 = string.Empty;
                            if (false)
                            {
                                bodymsg2 = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/WLMail.html");
                            }
                            else
                            {
                                bodymsg2 = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
                            }
                            SendMails objSendmail2 = new SendMails();
                            objSendmail2.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];

                            objSendmail2.ToEmail = objUserCreator.EmailId;
                            objSendmail2.cc = objUser.EmailId;
                            objSendmail2.Bcc = ConfigurationManager.AppSettings["WatchEMailID"];
                            EmailConfigMapping emap = new EmailConfigMapping();
                            if (System.Web.HttpContext.Current.Session["LanguageID"] != null)
                            {
                                emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == Convert.ToInt64(System.Web.HttpContext.Current.Session["LanguageID"])).FirstOrDefault();
                            }
                            else
                            {
                                if (ud == null)
                                {
                                    emap = null;
                                }
                                else
                                {
                                    emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == ud.LanguageId).FirstOrDefault();
                                }
                            }
                            if (emap != null)
                            {
                                bodymsg2 = bodymsg2.Replace("@CONTENTS", emap.EmailContents);
                            }
                            else
                            {
                                emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                                bodymsg2 = bodymsg2.Replace("@CONTENTS", emap.EmailContents);
                            }
                            EmailValueCollection objEmailValues = new EmailValueCollection();
                            objSendmail2.Subject = "Request ID:" + bookingids;
                            if (objSendmail2.Attachment.Count <= 0)
                            {
                                objSendmail2.Attachment.Add(new System.Net.Mail.Attachment(Filename));
                            }
                            if (false)
                            {
                                foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForRequestDoneByWL(objUserCreator.FirstName + " " + objUserCreator.LastName, bookingids.ToString(), HotelNames, objUserCreator.FirstName + " " + objUserCreator.LastName, "Support team Lastminutemeetingroom ", string.Empty))
                                {
                                    bodymsg2 = bodymsg2.Replace(strKey.Key, strKey.Value);
                                }
                            }
                            else
                            {
                                foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForRequestDone(objUserCreator.FirstName + " " + objUserCreator.LastName, bookingids.ToString(), HotelNames, objUserCreator.FirstName + " " + objUserCreator.LastName, "Support team Lastminutemeetingroom "))
                                {
                                    bodymsg2 = bodymsg2.Replace(strKey.Key, strKey.Value);
                                }
                            }
                            objSendmail2.Body = bodymsg2;
                            objSendmail2.SendMail();
                        }
                        #endregion
                    }                    
                    #endregion
                }
                catch
                {
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
                result = false;
                logger.Error(ex);
                throw ex;
            }
            return result;
        }
        #endregion

        public bool CheckMeetingroomReserved(Createbooking c)
        {
            bool b = false;
            foreach (BookedMR mr in c.MeetingroomList)
            {
                foreach (BookedMrConfig bmrc in mr.MrDetails)
                {
                    string whereClause = AvailabilityColumn.HotelId + "=" + c.HotelID + " and " + AvailabilityColumn.AvailabilityDate + "='" + c.ArivalDate.AddDays(bmrc.SelectedDay - 1) + "'";
                    int count = 0;
                    TList<Availability> objAvail = DataRepository.AvailabilityProvider.GetPaged(whereClause, string.Empty, 0, int.MaxValue, out count);
                    foreach (Availability a in objAvail)
                    {
                        string where2 = AvailibilityOfRoomsColumn.AvailibilityIdForBedRoom + "=" + a.Id + " and  " + AvailibilityOfRoomsColumn.RoomId + "=" + mr.MRId + " and " + AvailibilityOfRoomsColumn.RoomType + "=0";
                        int count2 = 0;
                        TList<AvailibilityOfRooms> objAvor = DataRepository.AvailibilityOfRoomsProvider.GetPaged(where2, string.Empty, 0, int.MaxValue, out count2);
                        foreach (AvailibilityOfRooms objA in objAvor)
                        {
                            if (objA.IsReserved)
                            {
                                b = true;
                            }
                        }
                    }
                }
            }
            return b;
        }

        #region Request By normal user and with white lable
        public bool CreateRequest(CreateRequest sb, ref string bookingids, string FilePath, bool isWL, string wllogo)
        {
            bool result = false;
            TransactionManager tm = null;
            string bookingid = "";
            string HotelNames = "";
            EmailConfig eConfig2 = em.GetByName("Request details for hotel");
            Users objUser = new HotelManager().GetFirstOperatorInDataBase();
            Users objUserCreator = DataRepository.UsersProvider.GetByUserId(sb.CurrentUserId);
            UserDetails ud = DataRepository.UserDetailsProvider.GetByUserId(objUserCreator.UserId).FirstOrDefault();
            Guid newRequest = Guid.NewGuid();
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                foreach (BuildHotelsRequest b in sb.HotelList.Where(a => a.MeetingroomList.Count > 0))
                {
                    Booking objBooking = new Booking();
                    objBooking.ArrivalDate = sb.ArivalDate;
                    objBooking.DepartureDate = sb.DepartureDate;
                    objBooking.CreatorId = sb.CurrentUserId;
                    objBooking.Duration = sb.Duration;
                    objBooking.HotelId = b.HotelID;
                    objBooking.MainMeetingRoomId = b.MeetingroomList[0].MeetingRoomID;
                    objBooking.BookingDate = DateTime.Now;
                    objBooking.SpecialRequest = sb.SpecialRequest;
                    //objBooking.IsBedroom = sb.ManageAccomodationLst.Count > 0 ? false : true;
                    objBooking.BookType = 0;
                    objBooking.IsUserBookingProcessDone = true;
                    //objBooking.BedRoomTotalPrice = sb.AccomodationPriceTotal;
                    //objBooking.AgencyClientId = 1;
                    //objBooking.AgencyUserId = 1;
                    objBooking.ChannelBy = sb.BookType;
                    objBooking.ChannelId = sb.ChannelID;
                    objBooking.BookingXml = TrailManager.XmlSerialize(sb);
                    objBooking.RequestStatus = (int)BookingRequestStatus.New;
                    objBooking.BookType = 1;
                    objBooking.RequestCollectionId = newRequest.ToString();
                    if (DataRepository.BookingProvider.Insert(tm, objBooking))
                    {
                        bookingid += objBooking.Id + ",";
                        Hotel objHotel = DataRepository.HotelProvider.GetById(tm, objBooking.HotelId);
                        TList<HotelContact> hc = DataRepository.HotelContactProvider.GetByHotelId(tm, objHotel.Id);
                        try
                        {
                            #region START Request to the hotel

                            if (eConfig2.IsActive)
                            {
                                SendMails objSendmail = new SendMails();
                                objSendmail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
                                objSendmail.ToEmail = objHotel.Email;
                                foreach (HotelContact hcon in hc)
                                {
                                    if (hcon.Email != objHotel.Email && hcon.ContactType == "PrimaryContact2" && hcon.UserType == "WeekEnd" && (DateTime.Now.DayOfWeek == DayOfWeek.Saturday || DateTime.Now.DayOfWeek == DayOfWeek.Sunday))
                                    {
                                        if (!string.IsNullOrEmpty(hcon.Email))
                                        {
                                            objSendmail.ToEmail += "," + hcon.Email;
                                        }
                                    }
                                    else if (hcon.Email != objHotel.Email && hcon.ContactType == "PrimaryContact1")
                                    {
                                        if (!string.IsNullOrEmpty(hcon.Email))
                                        {
                                            objSendmail.ToEmail += "," + hcon.Email;
                                        }
                                    }
                                    #region for secondary contact
                                    if (hcon.Email != objHotel.Email && hcon.ContactType == "SecondaryContact2" && hcon.UserType == "WeekEnd" && (DateTime.Now.DayOfWeek == DayOfWeek.Saturday || DateTime.Now.DayOfWeek == DayOfWeek.Sunday))
                                    {
                                        if (!string.IsNullOrEmpty(hcon.Email))
                                        {
                                            objSendmail.ToEmail += "," + hcon.Email;
                                        }
                                    }
                                    else if (hcon.Email != objHotel.Email && hcon.ContactType == "SecondaryContact1")
                                    {
                                        if (!string.IsNullOrEmpty(hcon.Email))
                                        {
                                            objSendmail.ToEmail += "," + hcon.Email;
                                        }
                                    }
                                    #endregion
                                }
                                string bodymsg3 = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
                                EmailConfigMapping emap2 = new EmailConfigMapping();
                                emap2 = eConfig2.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                                bodymsg3 = bodymsg3.Replace("@CONTENTS", emap2.EmailContents);
                                EmailValueCollection objEmailValues3 = new EmailValueCollection();
                                objSendmail.Subject = "New Request from lastminutemeetingroom.com ID:" + objBooking.Id.ToString();
                                if (objSendmail.Attachment != null)
                                {
                                    if (objSendmail.Attachment.Count > 0)
                                    {
                                        objSendmail.Attachment.RemoveAt(0);
                                    }
                                }
                                foreach (KeyValuePair<string, string> strKey in objEmailValues3.EmailForRequestDone("Supplier", objBooking.Id.ToString(), objHotel.Name, objUserCreator.FirstName + " " + objUserCreator.LastName, "Support team Lastminutemeetingroom "))
                                {
                                    bodymsg3 = bodymsg3.Replace(strKey.Key, strKey.Value);
                                }
                                objSendmail.Body = bodymsg3;
                                objSendmail.SendMail();
                            }
                            HotelNames += objHotel.Name + ",";
                            #endregion
                        }
                        catch
                        {
                        }
                    }
                    #region log
                    BookingLogs bklog = new BookingLogs();
                    bklog.BookingId = objBooking.Id;
                    bklog.CurrentStatus = BookingRequestStatus.New.ToString().ToUpper();
                    bklog.LastStatus = "";
                    bklog.HotelId = objBooking.HotelId;
                    bklog.UserId = Convert.ToInt32(objBooking.CreatorId);
                    bklog.ModifyDate = System.DateTime.Now;
                    DataRepository.BookingLogsProvider.Insert(tm, bklog);
                    #endregion


                    #region statistics
                    Viewstatistics objview = new Viewstatistics();
                    Statistics ST = new Statistics();
                    ST.BookingId = objBooking.Id;
                    ST.HotelId = objBooking.HotelId;
                    ST.Request = 1;//Booking Count
                    ST.BookingType = 1;//Booking 0, Request 1
                    ST.StatDate = System.DateTime.Now;
                    objview.InsertVisitwithBookingid(ST);
                    #endregion
                }
                if (bookingid.Length > 0)
                {
                    bookingid = bookingid.Substring(0, bookingid.Length - 1);
                }
                bookingids = bookingid;

                if (HotelNames.Length > 0)
                {
                    HotelNames = HotelNames.Substring(0, HotelNames.Length - 1);
                }
                try
                {
                    #region START Request To the User
                    EmailConfig eConfig = new EmailConfig();
                    if (isWL)
                    {
                        eConfig = em.GetByName("Send WL Request  Mail");
                    }
                    else
                    {
                        eConfig = em.GetByName("Request details for user");
                    }
                    if (eConfig.IsActive)
                    {
                        string bodymsg2 = string.Empty;
                        if (isWL)
                        {
                            bodymsg2 = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/WLMail.html");
                        }
                        else
                        {
                            bodymsg2 = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
                        }
                        SendMails objSendmail2 = new SendMails();
                        objSendmail2.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];

                        objSendmail2.ToEmail = objUserCreator.EmailId;
                        if (isWL)
                        {
                            objSendmail2.Bcc = ConfigurationManager.AppSettings["WatchEMailID"] + "," + objUser.EmailId;
                        }
                        else
                        {
                            objSendmail2.cc = objUser.EmailId;
                            objSendmail2.Bcc = ConfigurationManager.AppSettings["WatchEMailID"];
                        }

                        EmailConfigMapping emap = new EmailConfigMapping();
                        if (System.Web.HttpContext.Current.Session["LanguageID"] != null)
                        {
                            emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == Convert.ToInt64(System.Web.HttpContext.Current.Session["LanguageID"])).FirstOrDefault();
                        }
                        else
                        {
                            if (ud == null)
                            {
                                emap = null;
                            }
                            else
                            {
                                emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == ud.LanguageId).FirstOrDefault();
                            }
                        }
                        if (emap != null)
                        {
                            bodymsg2 = bodymsg2.Replace("@CONTENTS", emap.EmailContents);
                        }
                        else
                        {
                            emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                            bodymsg2 = bodymsg2.Replace("@CONTENTS", emap.EmailContents);
                        }
                        EmailValueCollection objEmailValues = new EmailValueCollection();
                        objSendmail2.Subject = "Request ID:" + bookingids;
                        if (objSendmail2.Attachment.Count <= 0)
                        {
                            objSendmail2.Attachment.Add(new System.Net.Mail.Attachment(FilePath));
                        }
                        if (isWL)
                        {
                            foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForRequestDoneByWL(objUserCreator.FirstName + " " + objUserCreator.LastName, bookingids, HotelNames, objUserCreator.FirstName + " " + objUserCreator.LastName, "Support team Lastminutemeetingroom ", wllogo))
                            {
                                bodymsg2 = bodymsg2.Replace(strKey.Key, strKey.Value);
                            }
                        }
                        else
                        {
                            foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForRequestDone(objUserCreator.FirstName + " " + objUserCreator.LastName, bookingids, HotelNames, objUserCreator.FirstName + " " + objUserCreator.LastName, "Support team Lastminutemeetingroom "))
                            {
                                bodymsg2 = bodymsg2.Replace(strKey.Key, strKey.Value);
                            }
                        }
                        objSendmail2.Body = bodymsg2;
                        objSendmail2.SendMail();
                    }
                    #endregion
                }
                catch
                {
                }
                result = true;


                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
                result = false;
            }
            return result;
        }
        #endregion

        #region Request By Agency
        public bool CreateRequestByAgency(CreateRequest sb, ref string bookingids, string FilePath, long AgencyID, long SelectedUserID)
        {
            bool result = false;
            TransactionManager tm = null;
            string bookingid = "";
            string HotelNames = "";
            EmailConfig eConfig2 = em.GetByName("Request details for hotel");
            Users objUser = new HotelManager().GetFirstOperatorInDataBase();
            Users objUserCreator = DataRepository.UsersProvider.GetByUserId(sb.CurrentUserId);
            UserDetails ud = DataRepository.UserDetailsProvider.GetByUserId(objUserCreator.UserId).FirstOrDefault();
            Guid newRequest = Guid.NewGuid();
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                foreach (BuildHotelsRequest b in sb.HotelList.Where(a => a.MeetingroomList.Count > 0))
                {
                    Booking objBooking = new Booking();
                    objBooking.ArrivalDate = sb.ArivalDate;
                    objBooking.DepartureDate = sb.DepartureDate;
                    objBooking.CreatorId = AgencyID;
                    objBooking.Duration = sb.Duration;
                    objBooking.HotelId = b.HotelID;
                    objBooking.MainMeetingRoomId = b.MeetingroomList[0].MeetingRoomID;
                    objBooking.BookingDate = DateTime.Now;
                    objBooking.SpecialRequest = sb.SpecialRequest;
                    //objBooking.IsBedroom = sb.ManageAccomodationLst.Count > 0 ? false : true;
                    objBooking.BookType = 0;
                    objBooking.IsUserBookingProcessDone = true;
                    //objBooking.BedRoomTotalPrice = sb.AccomodationPriceTotal;
                    objBooking.AgencyClientId = SelectedUserID;
                    objBooking.AgencyUserId = AgencyID;
                    objBooking.ChannelBy = sb.BookType;
                    objBooking.ChannelId = sb.ChannelID;
                    objBooking.BookingXml = TrailManager.XmlSerialize(sb);
                    objBooking.RequestStatus = (int)BookingRequestStatus.New;
                    objBooking.BookType = 1;
                    objBooking.RequestCollectionId = newRequest.ToString();
                    if (DataRepository.BookingProvider.Insert(tm, objBooking))
                    {
                        bookingid += objBooking.Id + ",";
                        Hotel objHotel = DataRepository.HotelProvider.GetById(tm, objBooking.HotelId);
                        TList<HotelContact> hc = DataRepository.HotelContactProvider.GetByHotelId(tm, objHotel.Id);
                        try
                        {
                            #region START Request to the hotel

                            if (eConfig2.IsActive)
                            {
                                SendMails objSendmail = new SendMails();
                                objSendmail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
                                objSendmail.ToEmail = objHotel.Email;
                                foreach (HotelContact hcon in hc)
                                {
                                    if (hcon.Email != objHotel.Email && hcon.ContactType == "PrimaryContact2" && hcon.UserType == "WeekEnd" && (DateTime.Now.DayOfWeek == DayOfWeek.Saturday || DateTime.Now.DayOfWeek == DayOfWeek.Sunday))
                                    {
                                        if (!string.IsNullOrEmpty(hcon.Email))
                                        {
                                            objSendmail.ToEmail += "," + hcon.Email;
                                        }
                                    }
                                    else if (hcon.Email != objHotel.Email && hcon.ContactType == "PrimaryContact1")
                                    {
                                        if (!string.IsNullOrEmpty(hcon.Email))
                                        {
                                            objSendmail.ToEmail += "," + hcon.Email;
                                        }
                                    }

                                    #region for secondary contact
                                    if (hcon.Email != objHotel.Email && hcon.ContactType == "SecondaryContact2" && hcon.UserType == "WeekEnd" && (DateTime.Now.DayOfWeek == DayOfWeek.Saturday || DateTime.Now.DayOfWeek == DayOfWeek.Sunday))
                                    {
                                        if (!string.IsNullOrEmpty(hcon.Email))
                                        {
                                            objSendmail.ToEmail += "," + hcon.Email;
                                        }
                                    }
                                    else if (hcon.Email != objHotel.Email && hcon.ContactType == "SecondaryContact1")
                                    {
                                        if (!string.IsNullOrEmpty(hcon.Email))
                                        {
                                            objSendmail.ToEmail += "," + hcon.Email;
                                        }
                                    }
                                    #endregion
                                }
                                string bodymsg3 = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
                                EmailConfigMapping emap2 = new EmailConfigMapping();
                                emap2 = eConfig2.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                                bodymsg3 = bodymsg3.Replace("@CONTENTS", emap2.EmailContents);
                                EmailValueCollection objEmailValues3 = new EmailValueCollection();
                                objSendmail.Subject = "New Request from lastminutemeetingroom.com ID:" + objBooking.Id.ToString();
                                if (objSendmail.Attachment != null)
                                {
                                    if (objSendmail.Attachment.Count > 0)
                                    {
                                        objSendmail.Attachment.RemoveAt(0);
                                    }
                                }
                                foreach (KeyValuePair<string, string> strKey in objEmailValues3.EmailForRequestDone("Supplier", objBooking.Id.ToString(), objHotel.Name, objUserCreator.FirstName + " " + objUserCreator.LastName, "Support team Lastminutemeetingroom "))
                                {
                                    bodymsg3 = bodymsg3.Replace(strKey.Key, strKey.Value);
                                }
                                objSendmail.Body = bodymsg3;
                                objSendmail.SendMail();
                            }
                            HotelNames += objHotel.Name + ",";
                            #endregion
                        }
                        catch
                        {
                        }
                    }
                    #region log
                    BookingLogs bklog = new BookingLogs();
                    bklog.BookingId = objBooking.Id;
                    bklog.CurrentStatus = BookingRequestStatus.New.ToString().ToUpper();
                    bklog.LastStatus = "";
                    bklog.HotelId = objBooking.HotelId;
                    bklog.UserId = Convert.ToInt32(objBooking.CreatorId);
                    bklog.ModifyDate = System.DateTime.Now;
                    DataRepository.BookingLogsProvider.Insert(tm, bklog);
                    #endregion


                    #region statistics
                    Viewstatistics objview = new Viewstatistics();
                    Statistics ST = new Statistics();
                    ST.BookingId = objBooking.Id;
                    ST.HotelId = objBooking.HotelId;
                    ST.Request = 1;//Booking Count
                    ST.BookingType = 1;//Booking 0, Request 1
                    ST.StatDate = System.DateTime.Now;
                    objview.InsertVisitwithBookingid(ST);
                    #endregion
                }
                if (bookingid.Length > 0)
                {
                    bookingid = bookingid.Substring(0, bookingid.Length - 1);
                }
                bookingids = bookingid;

                if (HotelNames.Length > 0)
                {
                    HotelNames = HotelNames.Substring(0, HotelNames.Length - 1);
                }
                try
                {
                    #region START Request To the User
                    EmailConfig eConfig = new EmailConfig();
                    if (false)
                    {
                        eConfig = em.GetByName("Send WL Request  Mail");
                    }
                    else
                    {
                        eConfig = em.GetByName("Request details for user");
                    }
                    if (eConfig.IsActive)
                    {
                        string bodymsg2 = string.Empty;
                        if (false)
                        {
                            bodymsg2 = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/WLMail.html");
                        }
                        else
                        {
                            bodymsg2 = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
                        }
                        SendMails objSendmail2 = new SendMails();
                        objSendmail2.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];

                        objSendmail2.ToEmail = objUserCreator.EmailId;
                        objSendmail2.cc = objUser.EmailId;
                        objSendmail2.Bcc = ConfigurationManager.AppSettings["WatchEMailID"];
                        EmailConfigMapping emap = new EmailConfigMapping();
                        if (System.Web.HttpContext.Current.Session["LanguageID"] != null)
                        {
                            emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == Convert.ToInt64(System.Web.HttpContext.Current.Session["LanguageID"])).FirstOrDefault();
                        }
                        else
                        {
                            if (ud == null)
                            {
                                emap = null;
                            }
                            else
                            {
                                emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == ud.LanguageId).FirstOrDefault();
                            }
                        }
                        if (emap != null)
                        {
                            bodymsg2 = bodymsg2.Replace("@CONTENTS", emap.EmailContents);
                        }
                        else
                        {
                            emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                            bodymsg2 = bodymsg2.Replace("@CONTENTS", emap.EmailContents);
                        }
                        EmailValueCollection objEmailValues = new EmailValueCollection();
                        objSendmail2.Subject = "Request ID:" + bookingids;
                        if (objSendmail2.Attachment.Count <= 0)
                        {
                            objSendmail2.Attachment.Add(new System.Net.Mail.Attachment(FilePath));
                        }
                        if (false)
                        {
                            foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForRequestDoneByWL(objUserCreator.FirstName + " " + objUserCreator.LastName, bookingids, HotelNames, objUserCreator.FirstName + " " + objUserCreator.LastName, "Support team Lastminutemeetingroom ", string.Empty))
                            {
                                bodymsg2 = bodymsg2.Replace(strKey.Key, strKey.Value);
                            }
                        }
                        else
                        {
                            foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForRequestDone(objUserCreator.FirstName + " " + objUserCreator.LastName, bookingids, HotelNames, objUserCreator.FirstName + " " + objUserCreator.LastName, "Support team Lastminutemeetingroom "))
                            {
                                bodymsg2 = bodymsg2.Replace(strKey.Key, strKey.Value);
                            }
                        }
                        objSendmail2.Body = bodymsg2;
                        objSendmail2.SendMail();
                    }
                    #endregion
                }
                catch
                {
                }
                result = true;


                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
                result = false;
            }
            return result;
        }
        #endregion
        /*----Store Search Data in Server----*/
        public bool SaveSearch(SearchTracer objSearch)
        {
            TransactionManager tm = null;
            SearchTracer objSearchClone = null;
            if (!objSearch.IsNew)
            {
                objSearchClone = objSearch.Copy();
            }
            else
            {
                objSearchClone = objSearch;
            }
            int Total = 0;
            string where = SearchTracerColumn.SearchId + "='" + objSearch.SearchId + "'";
            DataRepository.SearchTracerProvider.GetPaged(where, string.Empty, 0, int.MaxValue, out Total);
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                if (Total != 0)
                {
                    objSearchClone.ModifyDate = DateTime.Now;
                    DataRepository.SearchTracerProvider.Update(tm, objSearchClone);
                }
                else
                {
                    objSearchClone.ModifyDate = DateTime.Now;
                    objSearchClone.CreationDate = DateTime.Now;
                    DataRepository.SearchTracerProvider.Insert(tm, objSearchClone);
                }
                tm.Commit();
                objSearch = objSearchClone;
                return true;
            }
            catch (Exception ex)
            {
                tm.Rollback();
                objSearch = objSearchClone;
                return false;
            }
        }

        public SearchTracer GetSearchDetailsBySearchID(string searchID)
        {
            return DataRepository.SearchTracerProvider.GetBySearchId(searchID);
        }

        public TList<SearchTracer> GetSearchTracerByUserID(Int64 UserID)
        {
            return DataRepository.SearchTracerProvider.GetByUserId(UserID);
        }

        public bool DeleteSearchByID(SearchTracer objSearch)
        {
            return DataRepository.SearchTracerProvider.Delete(objSearch);
        }
    }

    public class VatCollection
    {
        public decimal VATPercent
        {
            get;
            set;
        }
        public decimal CalculatedPrice
        {
            get;
            set;
        }
    }

    //--Created By Gaurav Shrivastava--//
    //For creating booking including search result This'll hold the data and save its object to the database as it is.
    //1. Create the object of CreateBooking class.
    //2. Assign the values from the start of search.
    //3. Save this object in Database.with the refrence of GUID
    //4. So for searching all the time we'll Append this object and store in database(I think we'll create this object at the last step of search) After login of user.
    //5. We'll Have a option to maintain that data.
    public class Createbooking
    {
        public Int64 CurrentUserId
        {
            get;
            set;
        }
        public Int64 HotelID
        {
            get;
            set;
        }
        public List<BookedMR> MeetingroomList
        {
            get;
            set;
        }
        public DateTime ArivalDate
        {
            get;
            set;
        }
        public DateTime DepartureDate
        {
            get;
            set;
        }
        public int Duration
        {
            get;
            set;
        }
        //if AccomodationDiffCheckinChecoutTime = false then "Select Same checkin check out time for all bedroom".
        //if AccomodationDiffCheckinChecoutTime = true then "Select diffrent check in checkout for all days".
        public bool NoAccomodation
        {
            get;
            set;
        }
        public List<Accomodation> ManageAccomodationLst
        {
            get;
            set;
        }
        public string BedroomNote
        {
            get;
            set;
        }
        public decimal AccomodationPriceTotal
        {
            get;
            set;
        }
        public string SpecialRequest
        {
            get;
            set;
        }
        public decimal TotalBookingPrice
        {
            get;
            set;
        }
        public bool SecondMRNotSameAsRoom1
        {
            get;
            set;
        }
        public string BookType
        {
            get;
            set;
        }
        public string ChannelID
        {
            get;
            set;
        }
    }
    public class BookedMR
    {
        public Int64 MRId
        {
            get;
            set;
        }
        public Int64 MrConfigId
        {
            get;
            set;
        }
        public List<BookedMrConfig> MrDetails
        {
            get;
            set;
        }
        public decimal MeetingRoomPrice
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Build Meetingroom Configure Like No. of Particepent, SelectedDay, Selected Time(Full day, Morning and afternoon), Timings of Meetingroom (From Time - To Time)
    /// If Package Selected Then Package ID not equal to 0 and then check manage Package List and Manage Package Extra.
    /// If Package id is 0 then check build your meetingroom.
    /// </summary>
    public class BookedMrConfig
    {
        public int NoOfParticepant
        {
            get;
            set;
        }
        public int SelectedDay
        {
            get;
            set;
        }
        public int SelectedTime//If 0 then Fullday If 1 then Morning and If 2 then AfterNoon.
        {
            get;
            set;
        }
        public string FromTime
        {
            get;
            set;
        }
        public string ToTime
        {
            get;
            set;
        }
        public bool IsBreakdown
        {
            get;
            set;
        }
        public Int64 PackageID
        {
            get;
            set;
        }
        public decimal PackagePriceTotal
        {
            get;
            set;
        }
        public decimal PackagePricePerPerson
        {
            get;
            set;
        }
        public List<ManagePackageItem> ManagePackageLst
        {
            get;
            set;
        }
        public decimal ExtraPriceTotal
        {
            get;
            set;
        }
        public List<ManageExtras> ManageExtrasLst
        {
            get;
            set;
        }
        public decimal BuildPackagePriceTotal
        {
            get;
            set;
        }
        public List<BuildYourMR> BuildManageMRLst
        {
            get;
            set;
        }
        public decimal OtherPriceTotal
        {
            get;
            set;
        }
        public List<ManageOtherItems> BuildOthers
        {
            get;
            set;
        }
        public decimal EquipmentPriceTotal
        {
            get;
            set;
        }
        public List<ManageEquipment> EquipmentLst
        {
            get;
            set;
        }
        public decimal MeetingroomPrice
        {
            get;
            set;
        }
        public decimal MeetingroomDiscount
        {
            get;
            set;
        }
        public decimal MeetingroomVAT
        {
            get;
            set;
        }
    }




    /* commented By gaurav This class not is use any more */
    //////public class ManagePackage
    //////{
    //////    public Int64 PackageId
    //////    {
    //////        get;
    //////        set;
    //////    }
    //////    public List<ManagePackageItem> ManagePackageLst
    //////    {
    //////        get;
    //////        set;
    //////    }
    //////    public List<ManageExtras> ManageExtrasLst
    //////    {
    //////        get;
    //////        set;
    //////    }
    //////    public List<BuildYourMR> BuildManageMRLst
    //////    {
    //////        get;
    //////        set;
    //////    }
    //////}

    /// <summary>
    /// Manage Package Items Details when user select Package
    /// </summary>
    public class ManagePackageItem
    {
        public Int64 ItemId
        {
            get;
            set;
        }
        public string FromTime
        {
            get;
            set;
        }
        public string ToTime
        {
            get;
            set;
        }
        public Int64 Quantity
        {
            get;
            set;
        }
        public decimal ItemPrice
        {
            get;
            set;
        }
        public decimal vatpercent
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Manage Extra Items Available in details.
    /// </summary>
    public class ManageExtras
    {
        public Int64 ItemId
        {
            get;
            set;
        }
        public string FromTime
        {
            get;
            set;
        }
        public string ToTime
        {
            get;
            set;
        }
        public Int64 Quantity
        {
            get;
            set;
        }
        public decimal ItemPrice
        {
            get;
            set;
        }
        public decimal vatpercent
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Build your Meetingroom when user Not select any Package.
    /// </summary>
    public class BuildYourMR
    {
        public Int64 ItemId
        {
            get;
            set;
        }
        public string ServeTime
        {
            get;
            set;
        }
        public Int64 Quantity
        {
            get;
            set;
        }
        public decimal ItemPrice
        {
            get;
            set;
        }
        public decimal vatpercent
        {
            get;
            set;
        }
    }

    public class ManageOtherItems
    {
        public Int64 ItemId
        {
            get;
            set;
        }
        public Int64 Quantity
        {
            get;
            set;
        }
        public decimal ItemPrice
        {
            get;
            set;
        }
        public decimal vatpercent
        {
            get;
            set;
        }
    }
    /// <summary>
    /// Manage the equipment of the meetingroom.
    /// </summary>
    public class ManageEquipment
    {
        public Int64 ItemId
        {
            get;
            set;
        }
        public Int64 Quantity
        {
            get;
            set;
        }
        public decimal ItemPrice
        {
            get;
            set;
        }
        public decimal vatpercent
        {
            get;
            set;
        }
    }

    /// <summary>
    /// For accomodation According to Rooms. Accomodation is not bounded with the number of days.
    /// </summary>
    public class Accomodation
    {
        public Int64 BedroomId
        {
            get;
            set;
        }
        public int BedroomType
        {
            get;
            set;
        }
        public int QuantitySingle
        {
            get;
            set;
        }
        public int QuantityDouble
        {
            get;
            set;
        }
        public decimal RoomPriceSingle
        {
            get;
            set;
        }
        public decimal RoomPriceDouble
        {
            get;
            set;
        }
        public decimal Vat
        {
            get;
            set;
        }
        public DateTime DateRequest
        {
            get;
            set;
        }
        public long RoomAvailable
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Manage Room details so that Identify that which Bedroom assigned two whome.
    /// </summary>
    public class RoomManage
    {
        public string RoomAssignedTo
        {
            get;
            set;
        }
        public string CheckInTime
        {
            get;
            set;
        }
        public string checkoutTime
        {
            get;
            set;
        }
        public string Notes
        {
            get;
            set;
        }
    }

    public class BedroomManage
    {
        public Int64 BedroomID
        {
            get;
            set;
        }
        public int bedroomType
        {
            get;
            set;
        }
    }
}
public enum BookType
{
    Booking = 0,
    Request = 1,
    ConvertToRequest = 2
}