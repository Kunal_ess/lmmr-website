﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;
using System.Web;

namespace LMMR.Business
{


    public abstract class MeetingRoomConfigManagerBase
    {
        #region Variables
        //Declare Variable to store data
        private TList<MeetingRoomConfig> _varGetAllMeetingRoomConfig;

        //Declare Variable to message
        private string _varMessage;

        //Declare variable to get meetingroom cofig.
        private MeetingRoomConfig _varMeetingroomConfigByID;

        #endregion

        #region Properties
        //Property to store TList of Entities.MeetingRoomCofig class0
        public TList<MeetingRoomConfig> propGetMeetingRoomConfig
        {
            get { return _varGetAllMeetingRoomConfig; }
            set { _varGetAllMeetingRoomConfig = value; }
        }
        //Property to store message
        public string propMessage
        {
            get { return _varMessage; }
            set { _varMessage = value; }
        }

        //Property to store meetingroom config by Id.
        public MeetingRoomConfig propMeetingroomConfigByID
        {
            get { return _varMeetingroomConfigByID; }
            set { _varMeetingroomConfigByID = value; }
        }

        #endregion

        #region Function


        /// <summary>
        /// this function use for get min value of enum.
        /// </summary>
        /// <returns></returns>
        public int GetMinMeetingRoomShapeID()
        {

            int dashboardlinkMin = (int)Enum.GetValues(typeof(MeetingRoomShapeName)).Cast<MeetingRoomShapeName>().Min();

            return dashboardlinkMin;
        }


        /// <summary>
        /// this function use for get max value from enum. 
        /// </summary>
        /// <returns></returns>
        public int GetMaxMeetingRoomShapeID()
        {
            int dashboardlinkMax = (int)Enum.GetValues(typeof(MeetingRoomShapeName)).Cast<MeetingRoomShapeName>().Max();

            return dashboardlinkMax;
        }

        /// <summary>
        /// function to get all list of MeetingRoomConfig
        /// </summary>
        /// <param name="meetingRoomID"></param>
        /// <returns></returns>
        public TList<MeetingRoomConfig> GetConfigByMeetingRoomID(Int64 meetingRoomID)
        {
            propGetMeetingRoomConfig = DataRepository.MeetingRoomConfigProvider.GetByMeetingRoomId(meetingRoomID);

            return propGetMeetingRoomConfig;
        }

        public MeetingRoomConfig GetMeetingroomConfigByID(int meetingRoomConfigID)
        {
            propMeetingroomConfigByID = DataRepository.MeetingRoomConfigProvider.GetById(meetingRoomConfigID);

            return propMeetingroomConfigByID;
        }

        /// <summary>
        /// function to get all list of MeetingRoomConfig betweeen partipant mentioned
        /// </summary>
        /// <param name="meetingRoomID"></param>
        /// <returns></returns>
        public TList<MeetingRoomConfig> GetConfigByMeetingRoomIDParticipant(int meetingRoomID,int Participant)
        {
            int Total = 0;
            string whereclause = "Meetingroom_Id=" + meetingRoomID + " " + " and " + Participant + " between " + MeetingRoomConfigColumn.MinCapacity + " and " + MeetingRoomConfigColumn.MaxCapicity + "";
            propGetMeetingRoomConfig = DataRepository.MeetingRoomConfigProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);            
            return propGetMeetingRoomConfig;
        }

        /// <summary>
        /// function to update MeetingRoomCofig. 
        /// </summary>
        /// <param name="meetingRoomConfigID"></param>
        /// <param name="meetingRoomID"></param>
        /// <param name="shapeID"></param>
        /// <param name="minCapacity"></param>
        /// <param name="maxCapacity"></param>
        /// <returns></returns>
        public string UpdateMeetingRoomCofig(MeetingRoomConfig objMeetingroomConfig)
        {
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                MeetingRoom meetingId = DataRepository.MeetingRoomProvider.GetById(objMeetingroomConfig.MeetingRoomId);
                
                //Audit trail maintain
                Guid gid = Guid.NewGuid();
                MeetingRoomConfig old = objMeetingroomConfig.GetOriginalEntity();
                old.Id = objMeetingroomConfig.Id;
                TrailManager.LogAuditTrail<MeetingRoomConfig>(tm, objMeetingroomConfig, old, AuditAction.U, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.MeetingRoomConfiguration), objMeetingroomConfig.TableName, Convert.ToString(meetingId.HotelId));
                //Audit trail maintain

                if (DataRepository.MeetingRoomConfigProvider.Update(tm, objMeetingroomConfig))
                {
                    propMessage = "Meetingroom Configuration Updated successfully !";
                }
                else
                {
                    propMessage = "Please try again !";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return propMessage;
        }

        /// <summary>
        /// //function to add new MeetingRoomCofig 
        /// </summary>
        /// <param name="meetingRoomID"></param>
        /// <param name="shapeID"></param>
        /// <param name="minCapacity"></param>
        /// <param name="maxCapacity"></param>
        /// <returns></returns>
        public string AddNewMeetingRoomCofig(MeetingRoomConfig objMeeingroomConfig)
        {
            TransactionManager tm = null;
            try
            {

                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                MeetingRoom meetingId = DataRepository.MeetingRoomProvider.GetById(objMeeingroomConfig.MeetingRoomId);
                Guid gid = Guid.NewGuid();
                if (DataRepository.MeetingRoomConfigProvider.Insert(tm, objMeeingroomConfig))
                {
                    //Audit trail maintain
                    TrailManager.LogAuditTrail<MeetingRoomConfig>(tm, objMeeingroomConfig, null, AuditAction.I, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.ContactDetails), objMeeingroomConfig.TableName, Convert.ToString(meetingId.HotelId));
                    //Audit trail maintain
                    propMessage = "Added successfully !";

                }
                else
                {
                    propMessage = "Please try again !";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }

            return propMessage;
        }
        #endregion
    }

}
