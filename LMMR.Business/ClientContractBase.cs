﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;
using log4net;
using log4net.Config;
#endregion

namespace LMMR.Business
{
    public abstract class ClientContractBase
    {
        #region Properties and variables
        ILog logger = log4net.LogManager.GetLogger(typeof(ClientContractBase));
        private TList<Country> _varGetAllCountry;
        private TList<Hotel> _varGetAllHotel;
        private TList<City> _varGetAllCity;
        private TList<Currency> _varGetCurrency;
        private TList<Zone> _varGetAllZone;
        private TList<Users> _varGetAllUsers;
        private TList<HotelOwnerLink> _varGetownerLink;
        private TList<StaffAccount> _varGetAllStaffAccount;

        //Declare Variable to message
        private string _varMessage;

        //Property to store message
        public string propMessage
        {
            get { return _varMessage; }
            set { _varMessage = value; }
        }

        //Property to store TList of Entities.Country class
        public TList<Country> propGetCountry
        {
            get { return _varGetAllCountry; }
            set { _varGetAllCountry = value; }
        }



        //Property to store TList of Entities.FinanaceInvoice class
        public TList<Hotel> propGetHotelName
        {
            get { return _varGetAllHotel; }
            set { _varGetAllHotel = value; }
        }

        //Property to store TList of Entities.Currency class
        public TList<Currency> propGetCurrency
        {
            get { return _varGetCurrency; }
            set { _varGetCurrency = value; }
        }

        //Property to store TList of Entities.Currency class
        public TList<City> propGetCity
        {
            get { return _varGetAllCity; }
            set { _varGetAllCity = value; }
        }

        //Property to store TList of Entities.Currency class
        public TList<Zone> propGetZone
        {
            get { return _varGetAllZone; }
            set { _varGetAllZone = value; }
        }

        //Property to store TList of Entities.Currency class
        public TList<Users> propGetUsers
        {
            get { return _varGetAllUsers; }
            set { _varGetAllUsers = value; }
        }

        //Property to store TList of Entities.Staffaccount class
        public TList<StaffAccount> propGetStaffAccount
        {
            get { return _varGetAllStaffAccount; }
            set { _varGetAllStaffAccount = value; }
        }

        //Property to store TList of Entities.HotelOwnerlink class
        public TList<HotelOwnerLink> propGetHotelowner
        {
            get { return _varGetownerLink; }
            set { _varGetownerLink = value; }
        }

        //Declare variable to store latestID
        private int _varUserID;

        //Property to store user ID
        public int propUserId
        {
            get { return _varUserID; }
            set { _varUserID = value; }
        }


        public int getUserId()
        {
            return propUserId;
        }
        #endregion

        #region get Methods
        //For bind hotel name
        public TList<Hotel> GetInvoiceByAllHotel()
        {
            propGetHotelName = DataRepository.HotelProvider.GetAll();
            return propGetHotelName;
        }

        //Bind all hotel name as per client search
        public TList<Hotel> GetHotelByClientId(int ClientId)
        {
            int total = 0;
            string whereClause = HotelColumn.IsRemoved + "=0" + " and " + "IsActive=1" + " and " + "ClientId='" + ClientId + "'";
            string OrderBy = HotelColumn.Name + " ASC";
            propGetHotelName = DataRepository.HotelProvider.GetPaged(whereClause, OrderBy, 0, int.MaxValue, out total);
            return propGetHotelName;
        }

        //Bind all country name
        public TList<Country> GetByAllCountry()
        {
            int total = 0;
            string whereClause = CountryColumn.IsActive + "=1";
            string OrderBy = CountryColumn.CountryName + " ASC";
            propGetCountry = DataRepository.CountryProvider.GetPaged(whereClause, OrderBy, 0, int.MaxValue, out total);
            return propGetCountry;
        }
        public TList<City> Getallcity()
        {
            int total = 0;
            string whereClause = CityColumn.IsActive + "=1";
            string OrderBy = CityColumn.City + " ASC";
            propGetCity = DataRepository.CityProvider.GetPaged(whereClause, OrderBy, 0, int.MaxValue, out total);
            return propGetCity;
        }

        //Bind all currency by countryId               
        public TList<Currency> GetCurrency()
        {
            propGetCurrency = DataRepository.CurrencyProvider.GetAll();
            return propGetCurrency;
        }
        public TList<Currency> GetCurrencyByCountry(Int64 countryID)
        {
            //Country objCountry = DataRepository.CountryProvider.GetById(countryID);
            TList<Others> objCountry = DataRepository.OthersProvider.GetByCountryId(countryID);
            TList<Currency> objCurrency = null;
            if (objCountry.Count > 0)
            {
                int count = 0;
                objCurrency = DataRepository.CurrencyProvider.GetPaged(CurrencyColumn.Id + "=" + objCountry[0].CurrencyId, CurrencyColumn.Currency + " ASC", 0, int.MaxValue, out count);
                return objCurrency;
            }
            else
            {
                return objCurrency;
            }
        }


        //Bind all city bu country id
        public TList<City> GetCityByCountry(int countryId)
        {
            propGetCity = DataRepository.CityProvider.GetByCountryId(countryId);
            return propGetCity;
        }

        //Bind all zone by city id
        public TList<Zone> GetZoneByCityId(int cityId)
        {
            propGetZone = DataRepository.ZoneProvider.GetByCityId(cityId);
            return propGetZone;
        }

        //Bind all clientname from user
        public TList<Users> GetAllClients()
        {
            int totalout = 0;
            string whereclause = UsersColumn.Usertype + "=" + ((int)Usertype.HotelClient) + " and " + UsersColumn.IsActive + "=1";
            propGetUsers = DataRepository.UsersProvider.GetPaged(whereclause, String.Empty, 0, int.MaxValue, out totalout);
            return propGetUsers;
        }

        //Bind all group clientname from user
        public TList<Users> GetAllGroupClients()
        {
            int totalout = 0;
            string whereclause = UsersColumn.Usertype + "=" + ((int)Usertype.HotelGroupUser);
            string OrderBy = UsersColumn.UserId + " Desc";
            propGetUsers = DataRepository.UsersProvider.GetPaged(whereclause, OrderBy, 0, int.MaxValue, out totalout);
            DataRepository.UsersProvider.DeepLoad(propGetUsers);
            return propGetUsers;
        }

        //Bind all agency users of particulat agent
        public TList<Users> GetAllClientsOfAgencyUsers(string UserId)
        {
            int totalout = 0;
            string whereclause = UsersColumn.Usertype + "=" + ((int)Usertype.AgencyClient) + " and " + UsersColumn.IsActive + "=1" + " and " + "isnull(IsRemoved, 0)" + "!=1" + " and " + "ParentId IN (" + UserId + ")";
            string OrderBy = UsersColumn.CreatedDate + " Desc";
            propGetUsers = DataRepository.UsersProvider.GetPaged(whereclause, OrderBy, 0, int.MaxValue, out totalout);
            return propGetUsers;
        }
        public TList<Users> GetClientsOfAgencySearch(string name, string UserId)
        {
            int totalout = 0;
            string whereclause = UsersColumn.Usertype + "=" + ((int)Usertype.AgencyClient) + " and " + UsersColumn.IsActive + "=1" + " and " + "isnull(IsRemoved, 0)" + "!=1" + " and " + "ParentId IN (" + UserId + ")" + " and (" + UsersColumn.FirstName + " LIKE '" + name + "%'  or " + UsersColumn.LastName + " LIKE '" + name + "%')";
            string OrderBy = UsersColumn.CreatedDate + " Desc";
            propGetUsers = DataRepository.UsersProvider.GetPaged(whereclause, OrderBy, 0, int.MaxValue, out totalout);
            return propGetUsers;
        }
        public TList<Users> GetClientsCompanyOfAgencySearch(string where)
        {
            int totalout = 0;
            string OrderBy = UsersColumn.CreatedDate + " Desc";
            propGetUsers = DataRepository.UsersProvider.GetPaged(where, OrderBy, 0, int.MaxValue, out totalout);
            DataRepository.UsersProvider.DeepLoad(propGetUsers);
            return propGetUsers;
        }

        //Bind all clients from HotelownerLink
        public TList<HotelOwnerLink> GetHotelOwnerLink(int UserId)
        {
            int total = 0;
            string whereClause = "UserId='" + UserId + "'";
            propGetHotelowner = DataRepository.HotelOwnerLinkProvider.GetPaged(whereClause, String.Empty, 0, int.MaxValue, out total);
            DataRepository.HotelOwnerLinkProvider.DeepLoad(propGetHotelowner);
            return propGetHotelowner;
        }

        //Bind all staffaccount
        public TList<Users> GetAllStaffAccount()
        {
            int total = 0;
            string whereClause = UsersColumn.Usertype + "=" + ((int)Usertype.Salesperson) + " and " + UsersColumn.IsActive + "=1";
            string OrderBy = UsersColumn.FirstName + " ASC";
            TList<Users> objStaff = DataRepository.UsersProvider.GetPaged(whereClause, OrderBy, 0, int.MaxValue, out total);
            return objStaff;
        }

        //Bind all GroupName
        public TList<Group> GetAllGroupName()
        {
            TList<Group> objGroup = DataRepository.GroupProvider.GetAll();
            return objGroup;
        }
        #endregion

        #region Add New Client Contract
        //function to insert new  Contract details 
        public string AddNewClientContract(string HotelName, string HotelAddress, string ContractId, int countryId, int currencyId, int cityId, int zoneId, Int64 clientId, int staffId, string Latitude, string Longitude, string phoneext, string phone, decimal contractValue, string operatorChoice, string scan, int star, int groupId, string contactPerson, string logo, out Int64 IdofHotel, string email)
        {
            Hotel hotel = new Hotel();
            hotel.Name = HotelName;
            hotel.HotelAddress = HotelAddress;
            hotel.ContractId = ContractId;
            hotel.CountryId = countryId;
            hotel.CurrencyId = currencyId;
            hotel.CityId = cityId;
            hotel.ZoneId = zoneId;
            hotel.ClientId = clientId;
            hotel.StaffId = staffId;
            hotel.Latitude = Latitude;
            hotel.Longitude = Longitude;
            hotel.PhoneExt = phoneext;
            hotel.Phone = phone;
            hotel.ContractValue = contractValue;
            hotel.OperatorChoice = operatorChoice;
            hotel.HotelPlan = scan;
            hotel.Stars = star;
            hotel.GroupId = groupId;
            hotel.ContactPerson = contactPerson;
            hotel.Logo = logo;
            hotel.Email = email;
            hotel.IsActive = false;
            if (DataRepository.HotelProvider.Insert(hotel))
            {
                propUserId = Convert.ToInt32(hotel.Id);
                propMessage = "Add successfully!";
                //This code is for allotment Cr, 
                //if (hotel.GoOnline == true)
                //{
                HotelManager objHotelManager = new HotelManager();
                objHotelManager.AddHotelAvailability(hotel.Id);
                objHotelManager.AddSpecialPriceandPromo(hotel.Id);
                //}
            }
            else
            {
                propMessage = "Please try again!";
            }
            IdofHotel = hotel.Id;

            return propMessage;
        }
        //function to insert new  User 
        public string AddNewClientContractUser(string userFirstName, out Int64 IdofUser)
        {
            Users u = new Users();
            u.FirstName = userFirstName;
            if (DataRepository.UsersProvider.Insert(u))
            {
                propUserId = Convert.ToInt32(u.UserId);
                propMessage = "Add successfully!";

            }
            else
            {
                propMessage = "Please try again!";
            }
            IdofUser = u.UserId;
            return propMessage;
        }
        //function to insert new  UserDetails 
        public string AddNewClientContractUserDetails(string UserName, Int64 userId, string vatNumber)
        {
            UserDetails ud = new UserDetails();
            ud.Name = UserName;
            ud.UserId = userId;
            ud.VatNo = vatNumber;
            if (DataRepository.UserDetailsProvider.Insert(ud))
            {
                //propUserId = Convert.ToInt32(ud.UserId);
                propMessage = "Add successfully!";

            }
            else
            {
                propMessage = "Please try again!";
            }
            //IdofUser = u.UserId;
            return propMessage;
        }


        //function to insert new  Contract details 
        public int AddNewHotelClient(string Name, string EmailId, string Password, int Usertype, bool Active)
        {
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                Users cltUser = new Users();
                cltUser.FirstName = Name;
                cltUser.EmailId = EmailId;
                //cltUser.Password = Password;
                cltUser.Usertype = Usertype;
                cltUser.IsActive = Active;

                if (DataRepository.UsersProvider.Insert(cltUser))
                {
                    propUserId = Convert.ToInt32(cltUser.UserId);
                    //propMessage = "Add successfully!";
                }
                else
                {
                    //propMessage = "Please try again!";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return propUserId;
        }

        public int AddNewHotelUser(string Name, string EmailId, string Password, int Usertype, bool Active, Int64 ParentId)
        {
            Users cltUser = new Users();
            cltUser.FirstName = Name;
            cltUser.EmailId = EmailId;
            //cltUser.Password = Password;
            cltUser.Usertype = Usertype;
            cltUser.IsActive = Active;
            cltUser.ParentId = ParentId;
            if (DataRepository.UsersProvider.Insert(cltUser))
            {
                propUserId = Convert.ToInt32(cltUser.UserId);
            }
            return propUserId;
        }
        public string Addhotelpicturlog(int recentHotelId, string imageName)
        {

            HotelPhotoVideoGallary objHotalvidio = new HotelPhotoVideoGallary();
            objHotalvidio.FileType = 0;
            objHotalvidio.ImageName = imageName;
            objHotalvidio.IsMain = true;
            objHotalvidio.UpdatedBy = 1;
            if (DataRepository.HotelPhotoVideoGallaryProvider.Insert(objHotalvidio))
            {
                propUserId = Convert.ToInt32(objHotalvidio.Id);
                propMessage = "Add successfully!";

            }
            else
            {
                propMessage = "Please try again!";
            }
            // IdofHotel = objHotalvidio.Id;
            return propMessage;

        }
        //Check already email exist
        public bool CheckEmailExist(string email)
        {
            int count = 0;
            string whereClause = UsersColumn.EmailId + "='" + email + "' AND isnull(" + UsersColumn.IsRemoved + ",0) = 0";
            TList<Users> objUsers = DataRepository.UsersProvider.GetPaged(whereClause, string.Empty, 0, int.MaxValue, out count);
            if (objUsers.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Check already email exist for admin link
        public bool CheckEmailExistAdminLink(string email)
        {
            int count = 0;
            string whereClause = UsersColumn.EmailId + "='" + email + "' AND isnull(" + UsersColumn.IsRemoved + ",0) = 0";
            TList<Users> objUsers = DataRepository.UsersProvider.GetPaged(whereClause, string.Empty, 0, int.MaxValue, out count);

            //int count1 = 0;
            //TList<Users> objUserClient = DataRepository.UsersProvider.GetPaged(UsersColumn.EmailId + "='" + email + "' and " + UsersColumn.Usertype + "=6", string.Empty, 0, int.MaxValue, out count1);

            if (objUsers.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
    }
}
