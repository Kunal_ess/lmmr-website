﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Data;
using LMMR.Entities;
using System.Web;

namespace LMMR.Business
{
    public abstract class HotelManagerBase
    {
        AvalibalityManager objAvailabilityManager = new AvalibalityManager();
        public HotelManagerBase()
        {

        }


        /// <summary>
        /// Get Hotel details according to clientId.
        /// </summary>
        /// <param name="ClientId"></param>
        /// <returns></returns>
        public TList<Hotel> GetHotelByClientId(Int64 ClientId)
        {
            TList<HotelOwnerLink> objLink = DataRepository.HotelOwnerLinkProvider.GetByUserId(ClientId);
            string whereclauseforhotel = HotelColumn.Id + " in (";
            foreach(HotelOwnerLink hl in objLink)
            {
                whereclauseforhotel += hl.HotelId + ",";
            }
            if(objLink.Count >0)
            {
                whereclauseforhotel = whereclauseforhotel.Substring(0,whereclauseforhotel.Length -1);
            }
            whereclauseforhotel += ") and " + HotelColumn.IsRemoved + "=0" + " and " + "IsActive=1";
            int hotelcount = 0;
            TList<Hotel> objHotels = DataRepository.HotelProvider.GetPaged(whereclauseforhotel, string.Empty, 0, int.MaxValue, out hotelcount);
            return objHotels;
        }


        /// <summary>
        /// Get Hotel Details By HotelID.
        /// </summary>
        /// <param name="HotelId"></param>
        /// <returns></returns>
        public Hotel GetHotelDetailsById(Int64 HotelId)
        {
            Hotel objHotel = DataRepository.HotelProvider.GetById(HotelId);
            if (objHotel != null)
            {
                DataRepository.HotelProvider.DeepLoad(objHotel, true, DeepLoadType.IncludeChildren, typeof(HotelDesc));
            }
            return objHotel;
        }

        public Hotel GetHotelDetailsByIdandLanguage(Int64 HotelId, Int64 LanguageId)
        {
            Hotel objHotel = DataRepository.HotelProvider.GetById(HotelId);
            int count = 0;
            HotelDesc htlDesc = DataRepository.HotelDescProvider.GetPaged("Hotel_Id=" + HotelId + " and Language_Id=" + LanguageId, "", 0, int.MaxValue, out count).FirstOrDefault();
            if (htlDesc == null)
            {
                htlDesc = DataRepository.HotelDescProvider.GetPaged("Hotel_Id=" + HotelId + " and Language_Id=1", "", 0, int.MaxValue, out count).FirstOrDefault();
            }
            objHotel.HotelDescCollection.Add(htlDesc);
            //DataRepository.HotelProvider.DeepLoad(objHotel,true,DeepLoadType.IncludeChildren,typeof(HotelDesc));
            return objHotel;
        }

        public string GetSessionTimeoutValue()
        {
            Others obj = DataRepository.OthersProvider.GetAll().FirstOrDefault();
            if (obj != null)
            {
                return (obj.SessionTimeout).ToString();
            }
            else
            {
                return (5).ToString();
            }
        }

        /// <summary>
        /// Update Hotel by Hotel details.
        /// </summary>
        /// <param name="objHotel"></param>
        /// <returns></returns>
        public bool UpdateHotel(Hotel objHotel)
        {
            return DataRepository.HotelProvider.Update(objHotel);
        }
        /// <summary>
        /// Get All Meetingroom details by hotel id
        /// </summary>
        /// <param name="hotelid"></param>
        /// <returns></returns>
        public TList<MeetingRoom> GetMeetingRoomByHotelId(Int64 hotelid)
        {
            string whereclause = "Hotel_Id=" + hotelid + " and " + MeetingRoomColumn.IsDeleted + "=0 and " + MeetingRoomColumn.IsOnline + "=1";
            int count = 0;
            TList<MeetingRoom> objMeetingRoom = DataRepository.MeetingRoomProvider.GetPaged(whereclause,string.Empty,0,int.MaxValue,out count);
            return objMeetingRoom;
        }


        /// <summary>
        /// Get all bedroom details by hotel id.
        /// </summary>
        /// <param name="hotelid"></param>
        /// <returns></returns>
        public TList<BedRoom> GetBedRoomByhotelId(Int64 hotelid)
        {
            string whereclause = BedRoomColumn.HotelId + "=" + hotelid + " and " + BedRoomColumn.IsActive + "=1 and " + BedRoomColumn.IsDeleted + "=0";
            int count = 0;
            TList<BedRoom> objBedRoom = DataRepository.BedRoomProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out count);
            return objBedRoom;
        }


        /// <summary>
        /// Update Leed Time by Hotel ID.
        /// </summary>
        /// <param name="Hotelid"></param>
        /// <param name="startdate"></param>
        /// <param name="leadtimeList"></param>
        /// <returns></returns>
        public string UpdateLeadTimeByHotelId(Int64 Hotelid, DateTime startdate, string leadtimeList)
        {
            string[] LeadTiming = leadtimeList.Split('|');
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Guid gid = Guid.NewGuid();
                for (int i = 0; i < LeadTiming.Length; i++)
                {
                    string whereclause = AvailabilityColumn.HotelId + "=" + Hotelid + " and " + AvailabilityColumn.AvailabilityDate + "='" + startdate + "'";
                    int total = 0;
                    TList<Availability> lstAvailability = DataRepository.AvailabilityProvider.GetPaged(tm,whereclause, string.Empty, 0, int.MaxValue, out total);
                    foreach (Availability a in lstAvailability)
                    {
                        a.LeadTimeForMeetingRoom = Convert.ToInt32(LeadTiming[i]);
                        Availability old = a.GetOriginalEntity();
                        old.Id = a.Id;
                        TrailManager.LogAuditTrail<Availability>(tm, a, old, AuditAction.U, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.ContactDetails), a.TableName, Convert.ToString(Hotelid));
                        DataRepository.AvailabilityProvider.Update(tm, a);
                    }
                    startdate = startdate.AddDays(1);
                }
                tm.Commit();
            }
            catch(Exception ex)
            {
                tm.Rollback();
            }
            return "Update";
        }


        /// <summary>
        /// Update Special Price of the Bedroom By hotel Id for all days.
        /// </summary>
        /// <param name="Hotelid"></param>
        /// <param name="startdate"></param>
        /// <param name="singlebedroom"></param>
        /// <param name="doublebedroom"></param>
        /// <returns></returns>
        public string UpdateSpecialPriceofBedroomHotelId(Int64 Hotelid, DateTime startdate, string singlebedroom, string doublebedroom, Int64 BedroomId)
        {
            TransactionManager tm = null;
            string[] Single = singlebedroom.Split('|');
            string[] Double = doublebedroom.Split('|');
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Guid gid = Guid.NewGuid();
                for (int i = 0; i < Single.Length; i++)
                {
                    string whereclause = SpecialPriceForBedroomColumn.HotelId + "=" + Hotelid + " and " + SpecialPriceForBedroomColumn.PriceDate + "='" + startdate + "' and " + SpecialPriceForBedroomColumn.BedroomId + "=" + BedroomId;
                    int total = 0;
                    TList<SpecialPriceForBedroom> lstAvailability = DataRepository.SpecialPriceForBedroomProvider.GetPaged(tm, whereclause, string.Empty, 0, int.MaxValue, out total);
                    foreach (SpecialPriceForBedroom a in lstAvailability)
                    {
                        a.PriceOfTheDayDouble = Convert.ToDecimal(Double[i]);
                        a.PriceOfTheDaySingle = Convert.ToDecimal(Single[i]);

                        //Audit trail maintain
                        SpecialPriceForBedroom old = a.GetOriginalEntity();
                        old.Id = a.Id;
                        TrailManager.LogAuditTrail<SpecialPriceForBedroom>(tm, a, old, AuditAction.U, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.SpecialPromotionPkgDiscount), a.TableName, Convert.ToString(Hotelid));
                        //Audit trail maintain

                        DataRepository.SpecialPriceForBedroomProvider.Update(tm, a);
                    }
                    startdate = startdate.AddDays(1);
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return "Update";
        }


        /// <summary>
        /// Update Discount by Hotelid
        /// </summary>
        /// <param name="Hotelid"></param>
        /// <param name="startdate"></param>
        /// <param name="discountmeetingroom"></param>
        /// <param name="discountpackage"></param>
        /// <returns></returns>
        public string UpdateDiscountByHotelId(Int64 Hotelid, DateTime startdate, string discountmeetingroom, string discountpackage)
        {
            TransactionManager tm = null;
            string[] LeadTiming = discountmeetingroom.Split('|');
            string[] DiscountDDR = discountpackage.Split('|');
            string currentUser = "";
            try
            {
                currentUser = Convert.ToString(HttpContext.Current.Session["CurrentUserID"] == null ? "1" : HttpContext.Current.Session["CurrentUserID"]);
            }
            catch
            {
                currentUser = "1";
            }
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Guid gid = Guid.NewGuid();
                for (int i = 0; i < LeadTiming.Length; i++)
                {
                    string whereclause = SpecialPriceAndPromoColumn.HotelId + "=" + Hotelid + " and " + SpecialPriceAndPromoColumn.SpecialPriceDate + "='" + startdate + "'";
                    int total = 0;
                    TList<SpecialPriceAndPromo> lstAvailability = DataRepository.SpecialPriceAndPromoProvider.GetPaged(tm,whereclause, string.Empty, 0, int.MaxValue, out total);
                    foreach (SpecialPriceAndPromo a in lstAvailability)
                    {
                        
                        a.DdrPercent = Convert.ToInt32(DiscountDDR[i]);
                        a.MeetingRoomPercent = Convert.ToInt32(LeadTiming[i]);

                        //Audit trail maintain
                        SpecialPriceAndPromo old = a.GetOriginalEntity();
                        old.Id = a.Id;
                        TrailManager.LogAuditTrail<SpecialPriceAndPromo>(tm, a, old, AuditAction.U, Convert.ToInt64(currentUser), Convert.ToInt32(PageType.SpecialPromotionPkgDiscount), a.TableName, Convert.ToString(Hotelid));
                        //Audit trail maintain

                        DataRepository.SpecialPriceAndPromoProvider.Update(tm, a);                        
                    }
                    startdate = startdate.AddDays(1);
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return "Update";
        }


        /// <summary>
        /// Update Availability of bedroom by Hotel ID.
        /// </summary>
        /// <param name="Hotelid"></param>
        /// <param name="Roomid"></param>
        /// <param name="startdate"></param>
        /// <param name="availabilitylist"></param>
        /// <returns></returns>
        public string UpdateAvailabilityOfBedroomByHotelId(Int64 Hotelid, Int64 Roomid, DateTime startdate, string availabilitylist)
        {
            string[] LeadTiming = availabilitylist.Split('|');
            TransactionManager tm = null;
            string currentUser = "";
            try
            {
                currentUser = Convert.ToString(HttpContext.Current.Session["CurrentUserID"] == null ? "1" : HttpContext.Current.Session["CurrentUserID"]);
            }
            catch
            {
                currentUser = "1";
            }
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Guid gid = Guid.NewGuid();
                for (int i = 0; i < LeadTiming.Length; i++)
                {
                    string whereclause = AvailabilityColumn.HotelId + "=" + Hotelid + " and " + AvailabilityColumn.AvailabilityDate + "='" + startdate + "'";
                    int total = 0;
                    TList<Availability> lstAvailability = DataRepository.AvailabilityProvider.GetPaged(tm,whereclause, string.Empty, 0, int.MaxValue, out total);
                    foreach (Availability a in lstAvailability)
                    {
                        string whereclause2 = AvailibilityOfRoomsColumn.AvailibilityIdForBedRoom + "=" + a.Id + " and " + AvailibilityOfRoomsColumn.RoomId + "=" + Roomid + " and " + AvailibilityOfRoomsColumn.RoomType + "=" + 1;
                        TList<AvailibilityOfRooms> objAvailableRooms = DataRepository.AvailibilityOfRoomsProvider.GetPaged(tm, whereclause2, string.Empty, 0, int.MaxValue, out total);
                        foreach (AvailibilityOfRooms avR in objAvailableRooms)
                        {
                            if (!string.IsNullOrEmpty(LeadTiming[i].Trim()))
                            {
                                if (Convert.ToInt32(LeadTiming[i]) != avR.NumberOfRoomsAvailable)
                                {
                                    if (Convert.ToInt32(LeadTiming[i]) > avR.NumberOfRoomsAvailable)
                                    {
                                        avR.MorningStatus = (int)AvailabilityStatus.AVAILABLE;
                                    }
                                    //if (Convert.ToInt32(LeadTiming[i]) == 0)
                                    //{
                                    //    avR.MorningStatus = (int)AvailabilityStatus.CLOSED;
                                    //}
                                    avR.NumberOfRoomsAvailable = Convert.ToInt32(LeadTiming[i]);
                                    AvailibilityOfRooms old2 = avR.GetOriginalEntity();
                                    old2.Id = avR.Id;
                                    TrailManager.LogAuditTrail<AvailibilityOfRooms>(tm, avR, old2, AuditAction.U, Convert.ToInt64(currentUser), Convert.ToInt32(PageType.Availability), avR.TableName, Convert.ToString(Hotelid));
                                    DataRepository.AvailibilityOfRoomsProvider.Update(tm, avR);
                                }
                            }
                        }
                    }
                    startdate = startdate.AddDays(1);
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return "Update";
        }


        /// <summary>
        /// Adjust Meeting room according to the opening and closing status.
        /// </summary>
        /// <param name="MeetingRoomIds"></param>
        /// <param name="MeetingDays"></param>
        /// <param name="ClosingStatus"></param>
        /// <param name="OpeningStatus"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public string AdjustMeetingRoom(string MeetingRoomIds, string MeetingDays, string ClosingStatus, string OpeningStatus, string FromDate, string ToDate)
        {
            int totalCount = 0;
            TransactionManager tm = null;
            string currentUser = "";
            try
            {
                currentUser = Convert.ToString(HttpContext.Current.Session["CurrentUserID"] == null ? "1" : HttpContext.Current.Session["CurrentUserID"]);
            }
            catch
            {
                currentUser = "1";
            }
            string meetingroomiddd = MeetingRoomIds.Replace('|', ',');
            DateTime startdate = new DateTime(Convert.ToInt32("20" + FromDate.Split('/')[2]), Convert.ToInt32(FromDate.Split('/')[1]), Convert.ToInt32(FromDate.Split('/')[0]));
            DateTime enddate = new DateTime(Convert.ToInt32("20" + ToDate.Split('/')[2]), Convert.ToInt32(ToDate.Split('/')[1]), Convert.ToInt32(ToDate.Split('/')[0]));
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Guid gid = Guid.NewGuid();
                string WhereClaus = ViewAvailabilityOfRoomsColumn.RoomId + " in (" + meetingroomiddd + ") and " + ViewAvailabilityOfRoomsColumn.AvailabilityDate + " between '" + startdate + "' and '" + enddate + "'";
                VList<ViewAvailabilityOfRooms> objviewAvailability = DataRepository.ViewAvailabilityOfRoomsProvider.GetPaged(tm, WhereClaus, string.Empty, 0, int.MaxValue, out totalCount);
                TList<AvailibilityOfRooms> ObjFinal = new TList<AvailibilityOfRooms>();
                foreach (ViewAvailabilityOfRooms v in objviewAvailability)
                {
                    AvailibilityOfRooms objRoom = DataRepository.AvailibilityOfRoomsProvider.GetById(tm, v.Id);
                    if (objRoom.MorningStatus == 0 || objRoom.MorningStatus == 1)
                    {
                        objRoom.MorningStatus = Convert.ToInt32(ClosingStatus.Split('|')[(int)v.AvailabilityDate.Value.DayOfWeek]);
                    }
                    if (objRoom.AfternoonStatus == 0 || objRoom.AfternoonStatus == 1)
                    {
                        objRoom.AfternoonStatus = Convert.ToInt32(ClosingStatus.Split('|')[(int)v.AvailabilityDate.Value.DayOfWeek]);
                    }
                    //objRoom.MorningStatus = Convert.ToInt32(ClosingStatus.Split('|')[(int)v.AvailabilityDate.Value.DayOfWeek]);
                    //objRoom.AfternoonStatus = Convert.ToInt32(ClosingStatus.Split('|')[(int)v.AvailabilityDate.Value.DayOfWeek]);
                    AvailibilityOfRooms old = objRoom.GetOriginalEntity();
                    old.Id = objRoom.Id;
                    try
                    {
                        TrailManager.LogAuditTrail<AvailibilityOfRooms>(tm, objRoom, old, AuditAction.U, Convert.ToInt64(currentUser), Convert.ToInt32(PageType.Availability), objRoom.TableName, Convert.ToString(v.HotelId));
                    }
                    catch
                    {
                    }
                    ObjFinal.Add(objRoom);
                }
                DataRepository.AvailibilityOfRoomsProvider.Update(tm, ObjFinal);
                tm.Commit();
                return "Update";
            }
            catch (Exception ex)
            {
                tm.Rollback();
                return "Cancel";
                //logger.Error(ex);
            }

            //try
            //{
            //    foreach (string s in MeetingRoomIds.Split('|'))
            //    {
            //        for (int i = 0; i < MeetingDays.Split('|').Length; i++)
            //        {
            //            DateTime startdate = new DateTime(Convert.ToInt32("20" + FromDate.Split('/')[2]), Convert.ToInt32(FromDate.Split('/')[1]), Convert.ToInt32(FromDate.Split('/')[0]));
            //            DateTime enddate = new DateTime(Convert.ToInt32("20" + ToDate.Split('/')[2]), Convert.ToInt32(ToDate.Split('/')[1]), Convert.ToInt32(ToDate.Split('/')[0]));
            //            int j = 0;
            //            while (startdate <= enddate)
            //            {
            //                if (i == (int)startdate.DayOfWeek)
            //                {
            //                    if (Convert.ToInt32(MeetingDays.Split('|')[i]) != 0)
            //                    {
            //                        objAvailabilityManager.AdjustMeetingRoombyMRIDandDate(Convert.ToInt32(s), startdate, ClosingStatus.Split('|')[i], OpeningStatus.Split('|')[i]);

            //                    }
            //                }
            //                j++;
            //                startdate = startdate.AddDays(1);
            //            }
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{

            //}
            //return "";
        }


        /// <summary>
        /// Check availability for full property change.
        /// </summary>
        /// <param name="hotelid"></param>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        public void CheckAvailabilityForFullProp(Int64 hotelid, string StartDate, string EndDate)
        {
            TransactionManager tm = null;
            string currentUser = "";
            try
            {
                currentUser = Convert.ToString(HttpContext.Current.Session["CurrentUserID"] == null ? "1" : HttpContext.Current.Session["CurrentUserID"]);
            }
            catch
            {
                currentUser = "1";
            }
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Guid gid = Guid.NewGuid();
                DateTime startdate = new DateTime(Convert.ToInt32("20" + StartDate.Split('/')[2]), Convert.ToInt32(StartDate.Split('/')[1]), Convert.ToInt32(StartDate.Split('/')[0]));
                DateTime enddate = new DateTime(Convert.ToInt32("20" + EndDate.Split('/')[2]), Convert.ToInt32(EndDate.Split('/')[1]), Convert.ToInt32(EndDate.Split('/')[0]));

                int totalCount = 0;
                string WhereClaus = AvailabilityColumn.HotelId + "=" + hotelid + " and " + AvailabilityColumn.AvailabilityDate + " between '" + startdate + "' and '" + enddate + "'";
                TList<Availability> objAvailability = DataRepository.AvailabilityProvider.GetPaged(tm,WhereClaus, string.Empty, 0, int.MaxValue, out totalCount);
                string whereclauseMR = "Hotel_Id=" + hotelid + " and " + MeetingRoomColumn.IsDeleted + "=0";
                int outcountmr = 0;
                TList<MeetingRoom> objMeetingRoom = DataRepository.MeetingRoomProvider.GetPaged(tm, whereclauseMR, string.Empty, 0, int.MaxValue, out outcountmr);
                TList<Availability> lstAvailability = new TList<Availability>();
                foreach (Availability v in objAvailability)
                {
                    
                    int closecounter = 0;
                    int FinalCount = 0;
                    string whereclauseforroomlst = AvailibilityOfRoomsColumn.AvailibilityIdForBedRoom + "=" + v.Id + " and " + AvailibilityOfRoomsColumn.RoomType + "=0";
                    int p = 0;
                    TList<AvailibilityOfRooms> objRoomlst = DataRepository.AvailibilityOfRoomsProvider.GetPaged(tm, whereclauseforroomlst, string.Empty, 0, int.MaxValue, out p);
                    foreach (MeetingRoom m in objMeetingRoom)
                    {
                        
                        foreach (AvailibilityOfRooms r in objRoomlst.Where(u=>u.RoomId==m.Id))
                        {
                            if (r.MorningStatus == (int)AvailabilityStatus.CLOSED && r.AfternoonStatus == (int)AvailabilityStatus.CLOSED)
                            {
                                closecounter++;
                            }
                            FinalCount++;
                        }
                    }

                    if (FinalCount == closecounter)
                    {
                        string whereclauseforroom = AvailibilityOfRoomsColumn.AvailibilityIdForBedRoom + "=" + v.Id + " and " + AvailibilityOfRoomsColumn.RoomType + "=1";
                        int p1 = 0;
                        TList<AvailibilityOfRooms> objRoom = DataRepository.AvailibilityOfRoomsProvider.GetPaged(tm, whereclauseforroom, string.Empty, 0, int.MaxValue, out p1);
                        TList<AvailibilityOfRooms> objBulkRoom = new TList<AvailibilityOfRooms>();
                        foreach (AvailibilityOfRooms r in objRoom)
                        {
                            r.MorningStatus = (int)AvailabilityStatus.CLOSED;
                            AvailibilityOfRooms old = r.GetOriginalEntity();
                            old.Id = r.Id;
                            TrailManager.LogAuditTrail<AvailibilityOfRooms>(tm, r, old, AuditAction.U, Convert.ToInt64(currentUser), Convert.ToInt32(PageType.Availability), r.TableName, Convert.ToString(hotelid));
                            objBulkRoom.Add(r);
                        }
                        DataRepository.AvailibilityOfRoomsProvider.Update(tm, objBulkRoom);
                        v.FullPropertyStatus = (int)AvailabilityStatus.CLOSED;
                        Availability old2 = v.GetOriginalEntity();
                        old2.Id = v.Id;
                        TrailManager.LogAuditTrail<Availability>(tm, v, old2, AuditAction.U, Convert.ToInt64(currentUser), Convert.ToInt32(PageType.Availability), v.TableName, Convert.ToString(hotelid));
                        //DataRepository.AvailabilityProvider.Update(tm, v);
                        lstAvailability.Add(v);
                    }
                    else
                    {
                        if (v.FullPropertyStatus != (int)AvailabilityStatus.SOLD)
                        {
                            string whereclauseforroom = AvailibilityOfRoomsColumn.AvailibilityIdForBedRoom + "=" + v.Id + " and " + AvailibilityOfRoomsColumn.RoomType + "=1";
                            int p2 = 0;
                            TList<AvailibilityOfRooms> objRoom = DataRepository.AvailibilityOfRoomsProvider.GetPaged(tm, whereclauseforroom, string.Empty, 0, int.MaxValue, out p2);
                            TList<AvailibilityOfRooms> objBulk = new TList<AvailibilityOfRooms>();
                            foreach (AvailibilityOfRooms r in objRoom)
                            {
                                r.MorningStatus = (int)AvailabilityStatus.AVAILABLE;
                                AvailibilityOfRooms old = r.GetOriginalEntity();
                                old.Id = r.Id;
                                TrailManager.LogAuditTrail<AvailibilityOfRooms>(tm, r, old, AuditAction.U, Convert.ToInt64(currentUser), Convert.ToInt32(PageType.Availability), r.TableName, Convert.ToString(hotelid));

                                objBulk.Add(r);   
                            }
                            DataRepository.AvailibilityOfRoomsProvider.Update(tm, objBulk);
                            v.FullPropertyStatus = (int)AvailabilityStatus.AVAILABLE;
                            Availability old2 = v.GetOriginalEntity();
                            old2.Id = v.Id;
                            TrailManager.LogAuditTrail<Availability>(tm, v, old2, AuditAction.U, Convert.ToInt64(currentUser), Convert.ToInt32(PageType.Availability), v.TableName, Convert.ToString(hotelid));
                            //DataRepository.AvailabilityProvider.Update(tm, v);
                            lstAvailability.Add(v);
                        }
                    }
                }
                DataRepository.AvailabilityProvider.Update(tm, lstAvailability);
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
        }


        public string SpecialPriceAdjustBedroom(Int64 hotelid, string DaysChecked, string discountEntered, string fromDate, string toDate, Int64 BedroomId,string type)
        {
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                DateTime startdate = new DateTime(Convert.ToInt32("20" + fromDate.Split('/')[2]), Convert.ToInt32(fromDate.Split('/')[1]), Convert.ToInt32(fromDate.Split('/')[0]));
                DateTime enddate = new DateTime(Convert.ToInt32("20" + toDate.Split('/')[2]), Convert.ToInt32(toDate.Split('/')[1]), Convert.ToInt32(toDate.Split('/')[0]));
                string whereClause = SpecialPriceForBedroomColumn.HotelId + "=" + hotelid + " and " + SpecialPriceForBedroomColumn.PriceDate + " between '" + startdate + "' and '" + enddate + "'";
                if (BedroomId != 0)
                {
                    whereClause += " and " + SpecialPriceForBedroomColumn.BedroomId + "=" + BedroomId;
                }
                int count = 0;
                string[] objDays = DaysChecked.Split('|');
                string[] objDiscount = discountEntered.Split('|');
                TList<SpecialPriceForBedroom> objSpecialPriceAndPromo = DataRepository.SpecialPriceForBedroomProvider.GetPaged(tm, whereClause, string.Empty, 0, int.MaxValue, out count);
                foreach (SpecialPriceForBedroom s in objSpecialPriceAndPromo)
                {
                    if (type == "All" || type == "Single")
                    {
                        s.PriceOfTheDaySingle = (Convert.ToInt32(objDays[(int)s.PriceDate.Value.DayOfWeek]) == 0 ? s.PriceOfTheDaySingle : Convert.ToDecimal(objDiscount[(int)s.PriceDate.Value.DayOfWeek]));
                    }
                    if (type == "All" || type == "Double")
                    {
                        s.PriceOfTheDayDouble = (Convert.ToInt32(objDays[(int)s.PriceDate.Value.DayOfWeek]) == 0 ? s.PriceOfTheDayDouble : Convert.ToDecimal(objDiscount[(int)s.PriceDate.Value.DayOfWeek]));
                    }
                    DataRepository.SpecialPriceForBedroomProvider.Update(tm, s);
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return "";
        }
        /// <summary>
        /// Special price and promo adjustment.
        /// </summary>
        /// <param name="hotelid"></param>
        /// <param name="DaysChecked"></param>
        /// <param name="discountEntered"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public string SpecialPriceAdjust(Int64 hotelid, string DaysChecked, string discountEntered, string fromDate, string toDate, string type)
        {
            TransactionManager tm = null;
            string currentUser = "";
            try
            {
                currentUser = Convert.ToString(HttpContext.Current.Session["CurrentUserID"] == null ? "1" : HttpContext.Current.Session["CurrentUserID"]);
            }
            catch
            {
                currentUser = "1";
            }
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Guid gid = Guid.NewGuid();
                DateTime startdate = new DateTime(Convert.ToInt32("20" + fromDate.Split('/')[2]), Convert.ToInt32(fromDate.Split('/')[1]), Convert.ToInt32(fromDate.Split('/')[0]));
                DateTime enddate = new DateTime(Convert.ToInt32("20" + toDate.Split('/')[2]), Convert.ToInt32(toDate.Split('/')[1]), Convert.ToInt32(toDate.Split('/')[0]));
                string whereClause = SpecialPriceAndPromoColumn.HotelId + "=" + hotelid + " and " + SpecialPriceAndPromoColumn.SpecialPriceDate + " between '" + startdate + "' and '" + enddate + "'";
                int count = 0;
                string[] objDays = DaysChecked.Split('|');
                string[] objDiscount = discountEntered.Split('|');
                TList<SpecialPriceAndPromo> objSpecialPriceAndPromo = DataRepository.SpecialPriceAndPromoProvider.GetPaged(tm, whereClause, string.Empty, 0, int.MaxValue, out count);
                foreach (SpecialPriceAndPromo s in objSpecialPriceAndPromo)
                {
                    if (type == "ALL" || type == "DDRs")
                    {
                        s.DdrPercent = (Convert.ToInt32(objDays[(int)s.SpecialPriceDate.Value.DayOfWeek]) == 0 ? s.DdrPercent : Convert.ToDecimal(objDiscount[(int)s.SpecialPriceDate.Value.DayOfWeek]));
                    }
                    if (type == "ALL" || type == "MeetingRoom")
                    {
                        s.MeetingRoomPercent = (Convert.ToInt32(objDays[(int)s.SpecialPriceDate.Value.DayOfWeek]) == 0 ? s.MeetingRoomPercent : Convert.ToDecimal(objDiscount[(int)s.SpecialPriceDate.Value.DayOfWeek]));
                    }
                    SpecialPriceAndPromo old = s.GetOriginalEntity();
                    old.Id = s.Id;
                    try
                    {
                        TrailManager.LogAuditTrail<SpecialPriceAndPromo>(tm, s, old, AuditAction.U, Convert.ToInt64(currentUser), Convert.ToInt32(PageType.Availability), s.TableName, Convert.ToString(hotelid));
                    }
                    catch
                    {

                    }
                    DataRepository.SpecialPriceAndPromoProvider.Update(tm, s);
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return "";
        }


        /// <summary>
        /// adjust bedroom availability.
        /// </summary>
        /// <param name="BedroomIds"></param>
        /// <param name="AvailableDays"></param>
        /// <param name="ClosingStatus"></param>
        /// <param name="OpeningStatus"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public string AdjustBedRoom(string BedroomIds, string AvailableDays, string ClosingStatus, string OpeningStatus, string FromDate, string ToDate)
        {
            foreach (string s in BedroomIds.Split('|'))
            {
                for (int i = 0; i < AvailableDays.Split('|').Length; i++)
                {
                    DateTime startdate = new DateTime(Convert.ToInt32("20" + FromDate.Split('/')[2]), Convert.ToInt32(FromDate.Split('/')[1]), Convert.ToInt32(FromDate.Split('/')[0]));
                    DateTime enddate = new DateTime(Convert.ToInt32("20" + ToDate.Split('/')[2]), Convert.ToInt32(ToDate.Split('/')[1]), Convert.ToInt32(ToDate.Split('/')[0]));
                    int j = 0;
                    while (startdate <= enddate)
                    {
                        if (i == (int)startdate.DayOfWeek)
                        {
                            if (Convert.ToInt32(AvailableDays.Split('|')[i]) != 0)
                            {
                                objAvailabilityManager.AdjustBedroombyBRIDandDate(Convert.ToInt32(s), startdate, ClosingStatus.Split('|')[i], OpeningStatus.Split('|')[i]);
                            }
                        }
                        j++;
                        startdate = startdate.AddDays(1);
                    }
                }
            }
            return "";
        }


        /// <summary>
        /// Add availability of hotel.
        /// </summary>
        /// <param name="HotelId"></param>
        /// <returns></returns>
        public TList<Availability> AddHotelAvailability(Int64 HotelId)
        {
            TList<Availability> objHotelAvailability = new TList<Availability>();
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                for (int i = 0; i < 60; i++)
                {
                    Availability avail = new Availability();
                    avail.HotelId = HotelId;
                    avail.AvailabilityDate = (i == 0 ? DateTime.Now : DateTime.Now.AddDays(i));
                    avail.FullPropertyStatus = (int)AvailabilityStatus.CLOSED;
                    avail.FullProrertyStatusBr = (int)AvailabilityStatus.CLOSED; 
                    avail.LeadTimeForMeetingRoom = 2;
                    DataRepository.AvailabilityProvider.Insert(tm, avail);
                    objHotelAvailability.Add(avail);
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return objHotelAvailability;
        }
        public TList<Availability> AddHotelAvailability(TransactionManager tmgr, Int64 HotelId)
        {
            TList<Availability> objHotelAvailability = new TList<Availability>();
            TransactionManager tm = null;
            bool trouter = false;
            try
            {
                if (tmgr == null)
                {
                    tm = DataRepository.Provider.CreateTransaction();
                    tm.BeginTransaction();
                }
                else
                {
                    tm = tmgr;
                    trouter = true;
                }
                for (int i = 0; i < 60; i++)
                {
                    Availability avail = new Availability();
                    avail.HotelId = HotelId;
                    avail.AvailabilityDate = (i == 0 ? DateTime.Now : DateTime.Now.AddDays(i));
                    avail.FullPropertyStatus = (int)AvailabilityStatus.CLOSED;
                    avail.FullProrertyStatusBr = (int)AvailabilityStatus.CLOSED;
                    avail.LeadTimeForMeetingRoom = 2;
                    
                    objHotelAvailability.Add(avail);
                }
                if (objHotelAvailability.Count > 0)
                {
                    DataRepository.AvailabilityProvider.Insert(tm, objHotelAvailability);
                }
                if (!trouter)
                {
                    tm.Commit();
                }
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return objHotelAvailability;
        }
        public TList<SpecialPriceForBedroom> AddBedroomSpecialPrice(Int64 HotelId, Int64 RoomId)
        {
            return AddBedroomSpecialPrice(null, HotelId, RoomId);
        }
        public TList<SpecialPriceForBedroom> AddBedroomSpecialPrice(TransactionManager tmgr, Int64 HotelId)
        {
            TList<SpecialPriceForBedroom> lstSpecialPriceandPromo = new TList<SpecialPriceForBedroom>();
            TransactionManager tm = null;
            bool trouter = false;
            try
            {
                if (tmgr == null)
                {
                    tm = DataRepository.Provider.CreateTransaction();
                    tm.BeginTransaction();
                }
                else
                {
                    tm = tmgr;
                    trouter = true;
                }
                int count = 0;
                TList<LMMR.Entities.BedRoom> lstBedroom = DataRepository.BedRoomProvider.GetPaged(tm, BedRoomColumn.HotelId + "=" + HotelId + " AND " + BedRoomColumn.IsActive + "=1 AND " + BedRoomColumn.IsDeleted + "=0", string.Empty, 0, int.MaxValue, out count);
                foreach (LMMR.Entities.BedRoom bed in lstBedroom)
                {
                    for (int i = 0; i < 60; i++)
                    {
                        SpecialPriceForBedroom objSPandP = new SpecialPriceForBedroom();
                        objSPandP.HotelId = HotelId;
                        objSPandP.BedroomId = bed.Id;
                        objSPandP.PriceOfTheDaySingle = bed.PriceSingle;
                        objSPandP.PriceOfTheDayDouble = bed.PriceDouble;
                        objSPandP.PriceDate = (i == 0 ? DateTime.Now : DateTime.Now.AddDays(i));
                        
                        lstSpecialPriceandPromo.Add(objSPandP);
                    }
                }
                if (lstSpecialPriceandPromo.Count > 0)
                {
                    DataRepository.SpecialPriceForBedroomProvider.Insert(tm, lstSpecialPriceandPromo);
                }
                if (!trouter)
                {
                    tm.Commit();
                }
            }
            catch (Exception exx)
            {
                tm.Rollback();
            }
            return lstSpecialPriceandPromo;
        }
        public TList<SpecialPriceForBedroom> AddBedroomSpecialPrice(TransactionManager tmgr,Int64 HotelId, Int64 RoomId)
        {
            TList<SpecialPriceForBedroom> lstSpecialPriceandPromo = new TList<SpecialPriceForBedroom>();
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                for (int i = 0; i < 120; i++)
                {
                    SpecialPriceForBedroom objSPandP = new SpecialPriceForBedroom();
                    objSPandP.HotelId = HotelId;
                    objSPandP.BedroomId = RoomId;
                    objSPandP.PriceOfTheDaySingle = 0;
                    objSPandP.PriceOfTheDayDouble = 0;
                    objSPandP.PriceDate = (i == 0 ? DateTime.Now : DateTime.Now.AddDays(i));
                    
                    lstSpecialPriceandPromo.Add(objSPandP);
                }
                if (lstSpecialPriceandPromo.Count > 0)
                {
                    DataRepository.SpecialPriceForBedroomProvider.Insert(tm, lstSpecialPriceandPromo);
                }
                tm.Commit();
            }
            catch (Exception exx)
            {
                tm.Rollback();
            }
            return lstSpecialPriceandPromo;
        }
        public TList<SpecialPriceForBedroom> UpdateBedroomSpecialPrice(TransactionManager tmgr, Int64 HotelId, Int64 RoomId, decimal PriceSingle, decimal PriceDouble)
        {
            TList<SpecialPriceForBedroom> lstSpecialPriceandPromo = new TList<SpecialPriceForBedroom>();
            int count = 0;
            lstSpecialPriceandPromo = DataRepository.SpecialPriceForBedroomProvider.GetPaged(SpecialPriceForBedroomColumn.HotelId + "=" + HotelId + " and " + SpecialPriceForBedroomColumn.BedroomId + "=" + RoomId, SpecialPriceForBedroomColumn.PriceDate + " ASC", 0, int.MaxValue, out count);
            TransactionManager tm = tmgr;
            try
            {
                if (tm == null)
                {
                    tm = DataRepository.Provider.CreateTransaction();
                    tm.BeginTransaction();
                }
                else if (!tm.IsOpen)
                {
                    tm.BeginTransaction();
                }
                foreach (SpecialPriceForBedroom objSPandP in lstSpecialPriceandPromo)
                {
                    objSPandP.PriceOfTheDaySingle = PriceSingle;
                    objSPandP.PriceOfTheDayDouble = PriceDouble;
                    DataRepository.SpecialPriceForBedroomProvider.Update(tm, objSPandP);
                }
                if (tmgr == null)
                {
                    tm.Commit();
                }
            }
            catch (Exception exx)
            {
                if (tmgr == null)
                {
                    tm.Rollback();
                }
            }
            return lstSpecialPriceandPromo;
        }
        /// <summary>
        /// Add room aavailability
        /// </summary>
        /// <param name="HotelId"></param>
        /// <param name="RoomId"></param>
        /// <param name="roomtype"></param>
        /// <returns></returns>
        public TList<AvailibilityOfRooms> AddRoomAvailability(Int64 HotelId, Int64 RoomId, int roomtype, int numberofroom)
        {
            return AddRoomAvailability(null, HotelId, RoomId, roomtype, numberofroom);
        }

        public TList<AvailibilityOfRooms> AddRoomAvailability(TransactionManager tmgr, Int64 HotelId)
        {
            TransactionManager tm = null;
            bool trouter = false;
            TList<AvailibilityOfRooms> objListRoomsAvail = new TList<AvailibilityOfRooms>();
            try
            {
                if (tmgr == null)
                {
                    tm = DataRepository.Provider.CreateTransaction();
                    tm.BeginTransaction();
                }
                else
                {
                    tm = tmgr;
                    trouter = true;
                }
                TList<Availability> objHotelAvailability = DataRepository.AvailabilityProvider.GetByHotelId(tm, HotelId);
                //TList<AvailibilityOfRooms> lstAvailabilityRoom = new TList<AvailibilityOfRooms>();
                int count = 0;
                TList<LMMR.Entities.BedRoom> lstBedroom = DataRepository.BedRoomProvider.GetPaged(tm, BedRoomColumn.HotelId + "=" + HotelId + " AND " + BedRoomColumn.IsActive + "=1 AND " + BedRoomColumn.IsDeleted + "=0", string.Empty, 0, int.MaxValue, out count);
                foreach(LMMR.Entities.BedRoom bed in lstBedroom)
                {
                    foreach (Availability avail in objHotelAvailability)
                    {
                        AvailibilityOfRooms objAvailBedRoom = new AvailibilityOfRooms();
                        objAvailBedRoom.AvailibilityIdForBedRoom = avail.Id;
                        if (Convert.ToInt32(avail.FullPropertyStatus) == (int)AvailabilityStatus.CLOSED)
                        {
                            objAvailBedRoom.MorningStatus = (int)AvailabilityStatus.CLOSED;
                            objAvailBedRoom.AfternoonStatus = (int)AvailabilityStatus.CLOSED;
                        }
                        else
                        {
                            objAvailBedRoom.MorningStatus = (int)AvailabilityStatus.CLOSED;
                            objAvailBedRoom.AfternoonStatus = (int)AvailabilityStatus.CLOSED;
                        }
                        objAvailBedRoom.NumberOfRoomsAvailable = bed.Allotment;
                        objAvailBedRoom.RoomId = bed.Id;
                        objAvailBedRoom.RoomType = 0;
                        //lstAvailabilityRoom.Add(objAvailBedRoom);
                        //DataRepository.AvailibilityOfRoomsProvider.Insert(tm, objAvailBedRoom);
                        objListRoomsAvail.Add(objAvailBedRoom);
                        if (Convert.ToInt32(avail.FullPropertyStatus) == (int)AvailabilityStatus.SOLD)
                        {
                            avail.FullPropertyStatus = (int)AvailabilityStatus.AVAILABLE;
                            DataRepository.AvailabilityProvider.Update(tm, avail);
                        }
                    }
                }
                if (objListRoomsAvail.Count > 0)
                {
                    DataRepository.AvailibilityOfRoomsProvider.Insert(tm, objListRoomsAvail);
                }
                if (!trouter)
                {
                    tm.Commit();
                }
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return objListRoomsAvail;
        }

        public TList<AvailibilityOfRooms> AddRoomAvailability(TransactionManager tmgr, Int64 HotelId, Int64 RoomId, int roomtype, int numberofroom)
        {
            TransactionManager tm = null;
            bool trouter = false;
            TList<AvailibilityOfRooms> objListRoomsAvail = new TList<AvailibilityOfRooms>();
            try
            {
                if (tmgr == null)
                {
                    tm = DataRepository.Provider.CreateTransaction();
                    tm.BeginTransaction();
                }
                else
                {
                    tm = tmgr;
                    trouter = true;
                }
                TList<Availability> objHotelAvailability = DataRepository.AvailabilityProvider.GetByHotelId(tm, HotelId);
                //TList<AvailibilityOfRooms> lstAvailabilityRoom = new TList<AvailibilityOfRooms>();
                foreach (Availability avail in objHotelAvailability)
                {
                    AvailibilityOfRooms objAvailBedRoom = new AvailibilityOfRooms();
                    objAvailBedRoom.AvailibilityIdForBedRoom = avail.Id;
                    if (Convert.ToInt32(avail.FullPropertyStatus) == (int)AvailabilityStatus.CLOSED)
                    {
                        objAvailBedRoom.MorningStatus = (int)AvailabilityStatus.CLOSED;
                        objAvailBedRoom.AfternoonStatus = (int)AvailabilityStatus.CLOSED;
                    }
                    else
                    {
                        objAvailBedRoom.MorningStatus = (int)AvailabilityStatus.CLOSED;
                        objAvailBedRoom.AfternoonStatus = (int)AvailabilityStatus.CLOSED;
                    }
                    objAvailBedRoom.NumberOfRoomsAvailable = numberofroom;
                    objAvailBedRoom.RoomId = RoomId;
                    objAvailBedRoom.RoomType = roomtype;
                    //lstAvailabilityRoom.Add(objAvailBedRoom);
                    //DataRepository.AvailibilityOfRoomsProvider.Insert(tm, objAvailBedRoom);
                    objListRoomsAvail.Add(objAvailBedRoom);
                    if (Convert.ToInt32(avail.FullPropertyStatus) == (int)AvailabilityStatus.SOLD)
                    {
                        avail.FullPropertyStatus = (int)AvailabilityStatus.AVAILABLE;
                        DataRepository.AvailabilityProvider.Update(tm, avail);
                    }
                }
                if (objListRoomsAvail.Count > 0)
                {
                    DataRepository.AvailibilityOfRoomsProvider.Insert(tm, objListRoomsAvail);
                }
                if (!trouter)
                {
                    tm.Commit();
                }
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return objListRoomsAvail;
        }


        /// <summary>
        /// Add Special Price and Promo.
        /// </summary>
        /// <param name="HotelId"></param>
        /// <returns></returns>
        public TList<SpecialPriceAndPromo> AddSpecialPriceandPromo(Int64 HotelId)
        {
            TList<SpecialPriceAndPromo> lstSpecialPriceandPromo = new TList<SpecialPriceAndPromo>();
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                for (int i = -1; i < 60; i++)
                {
                    SpecialPriceAndPromo objSPandP = new SpecialPriceAndPromo();
                    objSPandP.DdrPercent = 0;
                    objSPandP.HotelId = HotelId;
                    objSPandP.MeetingRoomPercent = 0;
                    objSPandP.SpecialPriceDate = (i == 0 ? DateTime.Now : DateTime.Now.AddDays(i));
                    DataRepository.SpecialPriceAndPromoProvider.Insert(tm,objSPandP);
                    lstSpecialPriceandPromo.Add(objSPandP);
                }
                tm.Commit();
            }
            catch (Exception exx)
            {
                tm.Rollback();
            }
            return lstSpecialPriceandPromo;
        }
        public TList<SpecialPriceAndPromo> AddSpecialPriceandPromo(TransactionManager tmgr, Int64 HotelId)
        {
            TList<SpecialPriceAndPromo> lstSpecialPriceandPromo = new TList<SpecialPriceAndPromo>();
            TransactionManager tm = null;
            bool trouter = false;
            try
            {
                if (tmgr == null)
                {
                    tm = DataRepository.Provider.CreateTransaction();
                    tm.BeginTransaction();
                }
                else
                {
                    tm = tmgr;
                    trouter = true;
                }
                for (int i = -1; i < 60; i++)
                {
                    SpecialPriceAndPromo objSPandP = new SpecialPriceAndPromo();
                    objSPandP.DdrPercent = 0;
                    objSPandP.HotelId = HotelId;
                    objSPandP.MeetingRoomPercent = 0;
                    objSPandP.SpecialPriceDate = (i == 0 ? DateTime.Now : DateTime.Now.AddDays(i));
                    
                    lstSpecialPriceandPromo.Add(objSPandP);
                }
                if (lstSpecialPriceandPromo.Count > 0)
                {
                    DataRepository.SpecialPriceAndPromoProvider.Insert(tm, lstSpecialPriceandPromo);
                }
                if (!trouter)
                {
                    tm.Commit();
                }
            }
            catch (Exception exx)
            {
                tm.Rollback();
            }
            return lstSpecialPriceandPromo;
        }
        public bool AddUpdateNewSpandP()
        {
            TransactionManager tm = null;
            bool myval = false;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                //TList<Hotel> objHotels = new TList<Hotel>();
                int counter = 0;
                using (TList<Hotel> objHotels = DataRepository.HotelProvider.GetPaged(tm, HotelColumn.IsRemoved + "=0", string.Empty, 0, int.MaxValue, out counter))
                {
                    foreach (Hotel h in objHotels)
                    {
                        if (!tm.IsOpen)
                        {
                            tm.BeginTransaction();
                        }
                        #region Insert Special Price and Promo
                        int counterS = 0;
                        using (TList<SpecialPriceAndPromo> lstSpecialPriceandPromo = DataRepository.SpecialPriceAndPromoProvider.GetPaged(tm, SpecialPriceAndPromoColumn.HotelId + "=" + h.Id , SpecialPriceAndPromoColumn.SpecialPriceDate + " DESC", 0, int.MaxValue, out counterS))
                        {
                            SpecialPriceAndPromo splSpecialPriceandPromo = lstSpecialPriceandPromo.OrderByDescending(u => u.SpecialPriceDate).FirstOrDefault();
                            TimeSpan diff;
                            if (splSpecialPriceandPromo == null)
                            {
                                diff = Convert.ToDateTime(DateTime.Now).Subtract(DateTime.Now);
                            }
                            else
                            {
                                diff = Convert.ToDateTime(splSpecialPriceandPromo.SpecialPriceDate == null ? DateTime.Now : splSpecialPriceandPromo.SpecialPriceDate).Subtract(DateTime.Now);
                            }
                            int daysLeft = diff.Days <= 0 ? 0 : diff.Days; ;
                            TList<SpecialPriceAndPromo> objlstSpandP = new TList<SpecialPriceAndPromo>();
                            for (int i = daysLeft; i < 60; i++)
                            {
                                if (lstSpecialPriceandPromo.FirstOrDefault(u => u.SpecialPriceDate == new DateTime(DateTime.Now.AddDays(i).Year, DateTime.Now.AddDays(i).Month, DateTime.Now.AddDays(i).Day)) == null)
                                {
                                    SpecialPriceAndPromo objSPandP = new SpecialPriceAndPromo();
                                    objSPandP.DdrPercent = 0;
                                    objSPandP.HotelId = h.Id;
                                    objSPandP.MeetingRoomPercent = 0;
                                    objSPandP.SpecialPriceDate = new DateTime(DateTime.Now.AddDays(i).Year, DateTime.Now.AddDays(i).Month, DateTime.Now.AddDays(i).Day);
                                    objlstSpandP.Add(objSPandP);
                                }
                            }
                            if (objlstSpandP.Count > 0)
                            {
                                DataRepository.SpecialPriceAndPromoProvider.Insert(tm, objlstSpandP);
                            }
                        }
                        #endregion

                        #region Bedroom Special Price for bedroom
                        int countbCount = 0;
                        int countbCount2 = 0;

                        string wherebmr = BedRoomColumn.HotelId + "=" + h.Id + " and  " + BedRoomColumn.IsDeleted + "=0";
                        using (TList<BedRoom> bmr = DataRepository.BedRoomProvider.GetPaged(tm, wherebmr, string.Empty, 0, int.MaxValue, out countbCount2))
                        {
                            foreach (BedRoom b in bmr)
                            {
                                using (TList<SpecialPriceForBedroom> lstSpecialPriceForBedroom = DataRepository.SpecialPriceForBedroomProvider.GetPaged(tm, SpecialPriceForBedroomColumn.HotelId + "=" + h.Id + " and " + SpecialPriceForBedroomColumn.BedroomId + "=" + b.Id, SpecialPriceForBedroomColumn.PriceDate + " DESC", 0, int.MaxValue, out countbCount))
                                {
                                    SpecialPriceForBedroom splSpecialPriceForBedroom = lstSpecialPriceForBedroom.OrderByDescending(u => u.PriceDate).FirstOrDefault();
                                    TimeSpan diffBedRoom;
                                    if (splSpecialPriceForBedroom == null)
                                    {
                                        diffBedRoom = Convert.ToDateTime(DateTime.Now).Subtract(DateTime.Now);
                                    }
                                    else
                                    {
                                        diffBedRoom = Convert.ToDateTime(splSpecialPriceForBedroom.PriceDate == null ? DateTime.Now : splSpecialPriceForBedroom.PriceDate).Subtract(DateTime.Now);
                                    }

                                    int daysLeftBedRoom = diffBedRoom.Days <= 0 ? 0 : diffBedRoom.Days;
                                    TList<SpecialPriceForBedroom> objlstSpandp = new TList<SpecialPriceForBedroom>();
                                    for (int i = daysLeftBedRoom; i < 60; i++)
                                    {
                                        if (lstSpecialPriceForBedroom.FirstOrDefault(u => u.PriceDate == new DateTime(DateTime.Now.AddDays(i).Year, DateTime.Now.AddDays(i).Month, DateTime.Now.AddDays(i).Day)) == null)
                                        {
                                            SpecialPriceForBedroom objSPandP = new SpecialPriceForBedroom();
                                            objSPandP.HotelId = h.Id;
                                            objSPandP.BedroomId = b.Id;
                                            objSPandP.PriceOfTheDaySingle = b.PriceSingle == null ? 0 : b.PriceSingle.Value;
                                            objSPandP.PriceOfTheDayDouble = b.PriceDouble == null ? 0 : b.PriceDouble.Value;                                            
                                            objSPandP.PriceDate = new DateTime(DateTime.Now.AddDays(i).Year, DateTime.Now.AddDays(i).Month, DateTime.Now.AddDays(i).Day);
                                            objlstSpandp.Add(objSPandP);
                                        }
                                    }
                                    if (objlstSpandp.Count > 0)
                                    {
                                        DataRepository.SpecialPriceForBedroomProvider.Insert(tm, objlstSpandp);
                                    }
                                }
                            }
                        }
                        #endregion
                        int counterA = 0;
                        TList<Availability> lstHotelAvailability = DataRepository.AvailabilityProvider.GetPaged(tm, "HotelID=" + h.Id, AvailabilityColumn.AvailabilityDate + " DESC", 0, int.MaxValue, out counterA);
                        Availability objHotelAvailability = lstHotelAvailability.OrderByDescending(u=>u.AvailabilityDate).FirstOrDefault();
                        TimeSpan diff2;
                        if (objHotelAvailability == null)
                        {
                            diff2 = Convert.ToDateTime(DateTime.Now).Subtract(DateTime.Now);
                        }
                        else
                        {
                            diff2 = Convert.ToDateTime(objHotelAvailability.AvailabilityDate == null ? DateTime.Now : objHotelAvailability.AvailabilityDate).Subtract(DateTime.Now);
                        }
                        int daysLeft2 = diff2.Days <= 0 ? 0 : diff2.Days;
                        TList<AvailibilityOfRooms> lstAvailabilityOfRoom = new TList<AvailibilityOfRooms>();
                        int countbr = 0;
                        string wherebr = BedRoomColumn.HotelId + "=" + h.Id + " and  " + BedRoomColumn.IsDeleted + "=0";
                        using (TList<BedRoom> br = DataRepository.BedRoomProvider.GetPaged(tm, wherebr, string.Empty, 0, 0, out countbr))
                        {
                            int countmr = 0;
                            string wheremr = "Hotel_Id=" + h.Id + " and " + MeetingRoomColumn.IsDeleted + "=0";
                            using (TList<MeetingRoom> mr = DataRepository.MeetingRoomProvider.GetPaged(tm, wheremr, string.Empty, 0, int.MaxValue, out countmr))
                            {
                                for (int i = daysLeft2; i < 60; i++)
                                {
                                    if (lstHotelAvailability.FirstOrDefault(u => u.AvailabilityDate == new DateTime(DateTime.Now.AddDays(i).Year, DateTime.Now.AddDays(i).Month, DateTime.Now.AddDays(i).Day)) == null)
                                    {
                                        Availability avail = new Availability();
                                        avail.HotelId = h.Id;
                                        avail.AvailabilityDate = new DateTime(DateTime.Now.AddDays(i).Year, DateTime.Now.AddDays(i).Month, DateTime.Now.AddDays(i).Day);
                                        avail.FullPropertyStatus = (int)AvailabilityStatus.CLOSED;
                                        avail.FullProrertyStatusBr = (int)AvailabilityStatus.CLOSED;
                                        avail.LeadTimeForMeetingRoom = 2;
                                        DataRepository.AvailabilityProvider.Insert(tm, avail);


                                        foreach (MeetingRoom m in mr)
                                        {
                                            AvailibilityOfRooms objAvailBedRoom = new AvailibilityOfRooms();
                                            objAvailBedRoom.AvailibilityIdForBedRoom = avail.Id;
                                            if (Convert.ToInt32(avail.FullPropertyStatus) == (int)AvailabilityStatus.CLOSED)
                                            {
                                                objAvailBedRoom.MorningStatus = (int)AvailabilityStatus.CLOSED;
                                                objAvailBedRoom.AfternoonStatus = (int)AvailabilityStatus.CLOSED;
                                            }
                                            else
                                            {
                                                objAvailBedRoom.MorningStatus = (int)AvailabilityStatus.CLOSED;
                                                objAvailBedRoom.AfternoonStatus = (int)AvailabilityStatus.CLOSED;
                                            }
                                            objAvailBedRoom.NumberOfRoomsAvailable = 0;
                                            objAvailBedRoom.RoomId = m.Id;
                                            objAvailBedRoom.RoomType = 0;
                                            lstAvailabilityOfRoom.Add(objAvailBedRoom);

                                        }


                                        foreach (BedRoom b in br)
                                        {
                                            AvailibilityOfRooms objAvailBedRoom = new AvailibilityOfRooms();
                                            objAvailBedRoom.AvailibilityIdForBedRoom = avail.Id;
                                            if (Convert.ToInt32(avail.FullPropertyStatus) == (int)AvailabilityStatus.CLOSED)
                                            {
                                                objAvailBedRoom.MorningStatus = (int)AvailabilityStatus.CLOSED;
                                                objAvailBedRoom.AfternoonStatus = (int)AvailabilityStatus.CLOSED;
                                            }
                                            else
                                            {
                                                objAvailBedRoom.MorningStatus = (int)AvailabilityStatus.CLOSED;
                                                objAvailBedRoom.AfternoonStatus = (int)AvailabilityStatus.CLOSED;
                                            }
                                            objAvailBedRoom.NumberOfRoomsAvailable = b.Allotment;
                                            //objAvailBedRoom.NumberOfRoomsAvailable = 0;
                                            objAvailBedRoom.RoomId = b.Id;
                                            objAvailBedRoom.RoomType = 1;
                                            lstAvailabilityOfRoom.Add(objAvailBedRoom);
                                            //DataRepository.AvailibilityOfRoomsProvider.Insert(tm, objAvailBedRoom);
                                        }
                                    }
                                }
                                DataRepository.AvailibilityOfRoomsProvider.Insert(tm, lstAvailabilityOfRoom);
                            }
                        }
                        tm.Commit();
                    }
                    myval = true;
                }
            }
            catch (Exception ex)
            {
                tm.Rollback();
                myval = false;
            }
            return myval;
        }

        public void SetOnlineCheck(TransactionManager tm, long HotelId, long meetingroomId)
        {
            //TransactionManager tm = null;
            bool myval = false;
            bool newTrans = false;
            try
            {
                if (tm == null)
                {
                    tm = DataRepository.Provider.CreateTransaction();
                    tm.BeginTransaction();
                    newTrans = true;
                }
                
                Hotel h = DataRepository.HotelProvider.GetById(tm, HotelId);
                if (!tm.IsOpen)
                {
                    tm.BeginTransaction();
                }
                int counterA = 0;
                TList<AvailibilityOfRooms> availabilityofrooms = new TList<AvailibilityOfRooms>();
                TList<Availability> lstAvailability = DataRepository.AvailabilityProvider.GetPaged(tm, "HotelID=" + h.Id + " And " + AvailabilityColumn.AvailabilityDate + ">='" + (new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day)) + "'", AvailabilityColumn.AvailabilityDate + " ASC", 0, int.MaxValue, out counterA);
                if (lstAvailability.Count <= 0)
                {
                    AddHotelAvailability(tm, h.Id);
                    AddSpecialPriceandPromo(tm, h.Id);
                    AddRoomAvailability(tm,h.Id);
                    AddBedroomSpecialPrice(tm, h.Id);
                    lstAvailability = DataRepository.AvailabilityProvider.GetPaged(tm, "HotelID=" + h.Id + " And " + AvailabilityColumn.AvailabilityDate + ">='" + (new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day)) + "'", AvailabilityColumn.AvailabilityDate + " ASC", 0, int.MaxValue, out counterA);
                }
                string whereclause = " Hotel_Id=" + HotelId;
                string orderbyclause = string.Empty;
                int counttotal = 0;
                VList<ViewAvailabilityOfRooms> availroom = DataRepository.ViewAvailabilityOfRoomsProvider.GetPaged(tm, whereclause, orderbyclause, 0, int.MaxValue, out counttotal);
                if (lstAvailability != null)
                {
                    foreach (Availability avail in lstAvailability)
                    {
                        int countmr = 0;
                        //string wheremr = "Hotel_Id=" + h.Id + " and " + MeetingRoomColumn.IsDeleted + "=0 and " + MeetingRoomColumn.Id + "=" + meetingroomId;

                        //TList<MeetingRoom> mr = DataRepository.MeetingRoomProvider.GetPaged(tm, wheremr, string.Empty, 0, int.MaxValue, out countmr);
                        //foreach (MeetingRoom m in mr)
                        //{
                            //MeetingRoom m = DataRepository.MeetingRoomProvider.GetById(tm, meetingroomId);
                            //if (m.IsOnline)
                            //{


                                ViewAvailabilityOfRooms objAvailabilitywithspandp = availroom.FirstOrDefault(u => u.AvailabilityDate == avail.AvailabilityDate && u.RoomId == meetingroomId);// DataRepository.ViewAvailabilityOfRoomsProvider.GetPaged(tm, whereclause, orderbyclause, 0, int.MaxValue, out counttotal).FirstOrDefault();
                                if (objAvailabilitywithspandp == null)
                                {
                                    AvailibilityOfRooms objAvailBedRoom = new AvailibilityOfRooms();
                                    objAvailBedRoom.AvailibilityIdForBedRoom = avail.Id;
                                    if (Convert.ToInt32(avail.FullPropertyStatus) == (int)AvailabilityStatus.CLOSED)
                                    {
                                        objAvailBedRoom.MorningStatus = (int)AvailabilityStatus.CLOSED;
                                        objAvailBedRoom.AfternoonStatus = (int)AvailabilityStatus.CLOSED;
                                    }
                                    else
                                    {
                                        objAvailBedRoom.MorningStatus = (int)AvailabilityStatus.CLOSED;
                                        objAvailBedRoom.AfternoonStatus = (int)AvailabilityStatus.CLOSED;
                                    }
                                    objAvailBedRoom.NumberOfRoomsAvailable = 0;
                                    objAvailBedRoom.RoomId = meetingroomId;
                                    objAvailBedRoom.RoomType = 0;
                                    availabilityofrooms.Add(objAvailBedRoom);
                                    //DataRepository.AvailibilityOfRoomsProvider.Insert(tm, objAvailBedRoom);
                                }
                            //}
                        //}
                    }
                    DataRepository.AvailibilityOfRoomsProvider.Insert(tm, availabilityofrooms);
                }
                if (newTrans)
                {
                    tm.Commit();
                }
            }
            catch (Exception ex)
            {
                tm.Rollback();
                myval = false;
            }
            return;
        }
        /// <summary>
        /// Get All packages by Hotel Id.
        /// </summary>
        /// <param name="Hotelid"></param>
        /// <returns></returns>
        public TList<PackageMaster> GetPackageByHotel(Int64 Hotelid)
        {
            TList<PackageByHotel> objPackage = DataRepository.PackageByHotelProvider.GetByHotelId(Hotelid);
            TList<PackageMaster> objPackageMaster = new TList<PackageMaster>();
            foreach (PackageByHotel p in objPackage)
            {
                if (p.PackageId != null)
                {
                    PackageMaster objPMaster = DataRepository.PackageMasterProvider.GetById(Convert.ToInt64(p.PackageId));
                    objPackageMaster.Add(objPMaster);
                }
            }
            return objPackageMaster;
        }

        /// <summary>
        /// Get Active and Standard Package from the details
        /// </summary>
        /// <param name="Hotelid"></param>
        /// <returns></returns>
        public TList<PackageMaster> GetActiveStandardPackageByHotel(Int64 Hotelid)
        {
            TList<PackageByHotel> objPackage = DataRepository.PackageByHotelProvider.GetByHotelId(Hotelid);
            TList<PackageMaster> objPackageMaster = new TList<PackageMaster>();
            foreach (PackageByHotel p in objPackage.OrderBy(a=>a.Id))
            {
                if (p.PackageId != null)
                {
                    PackageMaster objPMaster = DataRepository.PackageMasterProvider.GetById(Convert.ToInt64(p.PackageId));
                    if (objPMaster.PackageName.ToLower().Contains("standard"))
                    {
                        objPackageMaster.Add(objPMaster);
                        break;
                    }
                }
            }
            return objPackageMaster;
        }

        /// <summary>
        /// Get Details of Other Section values.
        /// </summary>
        /// <param name="OtherType"></param>
        /// <returns></returns>
        public string GetOtherSectionValues(string OtherType)
        {
            TList<Others> objOthers = DataRepository.OthersProvider.GetAll();
            string OthersValue = string.Empty;
            foreach (Others o in objOthers)
            {
                OthersValue = o.MinConsiderSpl == null ? string.Empty : o.MinConsiderSpl.ToString();
            }
            return OthersValue;
        }
        public Others GetOtherSectionValuesByHotelID(long HotelId)
        {
            Hotel h = DataRepository.HotelProvider.GetById(HotelId);
            TList<Others> objOthers = null;
            if (h != null)
            {
                objOthers = DataRepository.OthersProvider.GetByCountryId(h.CountryId);
                if (objOthers.Count <= 0)
                {
                    objOthers = DataRepository.OthersProvider.GetByCountryId(null);
                }
            }
            else
            {
                objOthers = DataRepository.OthersProvider.GetByCountryId(null);
            }
            return objOthers == null ? null : objOthers.FirstOrDefault();
        }

        /// <summary>
        /// Get all hotel details.
        /// </summary>
        /// <returns></returns>
        public TList<Hotel> GetAllHotel()
        {
            string wherClause = HotelColumn.IsRemoved + "=0 ";
            string Orderby = HotelColumn.Name + " ASC";
            int total = 0;
            return DataRepository.HotelProvider.GetPaged(wherClause, Orderby,0,int.MaxValue,out total);
        }


        /// <summary>
        /// Get Hotel details by Country id and City Id.
        /// </summary>
        /// <param name="countryid"></param>
        /// <param name="cityid"></param>
        /// <returns></returns>
        public TList<Hotel> GetHotelByCountryAndCity(Int64 countryid, Int64 cityid)
        {
            string whereclause = string.Empty;
            if (countryid != 0)
            {
                if (whereclause.Length > 0)
                {
                    whereclause += " and ";
                }
                whereclause = HotelColumn.CountryId + "=" + countryid;
            }
            if (cityid  != 0)
            {
                if (whereclause.Length > 0)
                {
                    whereclause += " and ";
                }
                whereclause = HotelColumn.CityId + "=" + cityid;
            }
            if (whereclause.Length > 0)
            {
                whereclause += " and ";
            }
            whereclause += HotelColumn.IsRemoved + "=0 ";
            string orderby = HotelColumn.CreationDate + " DESC";
            int count = 0;
            TList<Hotel> objHotel = DataRepository.HotelProvider.GetPaged(whereclause, orderby, 0, int.MaxValue, out count);
            return objHotel;
        }


        /// <summary>
        /// Get hotel details according to the where condition and where clause and orderby clause.
        /// </summary>
        /// <param name="whereclause"></param>
        /// <param name="orderbyclause"></param>
        /// <returns></returns>
        public TList<Hotel> GetHotelByWhereAndOrderby(string whereclause, string orderbyclause, string ClientNameStart)
        {
            if (!string.IsNullOrEmpty(ClientNameStart.Trim()))
            {
                int tcount = 0;
                string Makecondition = string.Empty;
                string whereclauseforClient = UsersColumn.FirstName + " LIKE '" + ClientNameStart + "%' and " + UsersColumn.Usertype + "=" + (int)Usertype.HotelClient;
                TList<Users> userClicnt = DataRepository.UsersProvider.GetPaged(whereclauseforClient,string.Empty,0,int.MaxValue,out tcount);
                Makecondition += HotelColumn.ClientId + " in(";
                if (userClicnt.Count > 0)
                {
                    for (int i = 0; i < userClicnt.Count; i++)
                    {
                        if (i == userClicnt.Count - 1)
                        {
                            Makecondition += userClicnt[i].UserId;
                        }
                        else
                        {
                            Makecondition += userClicnt[i].UserId + ",";
                        }
                    }
                    Makecondition += ") ";
                    whereclause += " and " + Makecondition;
                }
                else
                {
                    whereclause += " and 1=-1";
                }
            }
            int count = 0;
            string orderby = string.Empty;
            if (string.IsNullOrEmpty(orderbyclause))
            {
                orderby = HotelColumn.CreationDate + " DESC";
            }
            else
            {
                orderby = orderbyclause;
            }
            TList<Hotel> objHotel = DataRepository.HotelProvider.GetPaged(whereclause, orderby, 0, int.MaxValue, out count);
            return objHotel;
        }


        /// <summary>
        /// Add Userdetails and Hotel link.
        /// </summary>
        /// <param name="HotelId"></param>
        /// <param name="UserId"></param>
        public void AddUserandHotellink(Int64 HotelId, Int64 UserId)
        {
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                HotelOwnerLink objHotelOwnerLink = new HotelOwnerLink();
                objHotelOwnerLink.HotelId = HotelId;
                objHotelOwnerLink.UserId = UserId;
                DataRepository.HotelOwnerLinkProvider.Insert(objHotelOwnerLink);
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
        }

        
        /// <summary>
        /// Add New Client Contract related to user and hotel.
        /// </summary>
        /// <param name="objUsers"></param>
        /// <param name="objHotel"></param>
        public string[] AddNewContract(Users objUsers, Hotel objHotel, UserDetails objUserDetails, HotelPhotoVideoGallary objHotelPicture, bool AddOther, Users objOwnerUsers, UserDetails objOwnerUserDetails)
        {
            TransactionManager tm = null;
            string[] result = new string[2];
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                //If Hotel Client is added (i.e.) Other selected in Client dropdown
                if (AddOther)
                {
                    //User Created of Type Hotel Client Without any login details like No email, no password.
                    objOwnerUsers.CreatedDate = DateTime.Now;
                    DataRepository.UsersProvider.Save(tm, objOwnerUsers);
                    //Only VAT number is saved for this(above) user
                    objOwnerUserDetails.UserId = objOwnerUsers.UserId;
                    DataRepository.UserDetailsProvider.Insert(tm, objOwnerUserDetails);
                    //Set the above user ID as the Client ID for Hotel and Parent ID for Hotel User
                    objUsers.ParentId = objOwnerUsers.UserId;
                    objHotel.ClientId = objOwnerUsers.UserId;
                }
                //Create a User of type Hotel user and this user having email or password to login the Current Hotel.
                DataRepository.UsersProvider.Save(tm, objUsers);
                UserDetails ud = new UserDetails();
                //Add the user details for this(Hotel User Type User above)
                objUserDetails.UserId = objUsers.UserId;
                DataRepository.UserDetailsProvider.Insert(tm, objUserDetails);
                //Finally add the Hotel in the hotel table.
                objHotel.HotelUserId = objUsers.UserId;
                objHotel.CreationDate = DateTime.Now;
                DataRepository.HotelProvider.Save(tm, objHotel);
                //Then Create the link for Hotel User of Hotel User type and Hotel.
                HotelOwnerLink objHotelOwnerLinkHU = new HotelOwnerLink();
                objHotelOwnerLinkHU.HotelId = objHotel.Id;
                objHotelOwnerLinkHU.UserId = objUsers.UserId;
                DataRepository.HotelOwnerLinkProvider.Insert(tm, objHotelOwnerLinkHU);

                //Get SuperMan UserAccount Details and Add Hotel to Super Man User.
                int countSuperman = 0;
                TList<Users> onjSuperMan = DataRepository.UsersProvider.GetPaged(tm, UsersColumn.Usertype+"="+(int)Usertype.SuperMan, string.Empty , 0, int.MaxValue, out countSuperman);
                if (onjSuperMan.Count > 0)
                {
                    HotelOwnerLink objHotelOwnerLink = new HotelOwnerLink();
                    objHotelOwnerLink.HotelId = objHotel.Id;
                    objHotelOwnerLink.UserId = onjSuperMan[0].UserId;
                    DataRepository.HotelOwnerLinkProvider.Insert(tm, objHotelOwnerLink);
                }
                
                //Add photo and Videos table 1st record of this hotel. Logo is the main table.
                objHotelPicture.HotelId = objHotel.Id;
                objHotelPicture.UpdatedBy = objUsers.UserId;
                DataRepository.HotelPhotoVideoGallaryProvider.Insert(tm, objHotelPicture);
                
                tm.Commit();
                result[0] = "Contract added successfully.";
                result[1] = "succesfuly";
                
            }
            catch (Exception ex)
            {
                tm.Rollback();
                result[0] = "Error occured while contract added.";
                result[1] = "error";
            }
            return result;
        }


        /// <summary>
        /// Update clicnt contract related to Hotel.
        /// </summary>
        /// <param name="objHotel"></param>
        /// <param name="objHotelPhoto"></param>
        /// <returns></returns>
        public string[] UpdateContract(Hotel objHotel, HotelPhotoVideoGallary objHotelPhoto)
        {
            TransactionManager tm = null;
            string[] result = new string[2];
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                DataRepository.HotelProvider.Update(tm, objHotel);
                TList<HotelDesc> objHoteldesc = DataRepository.HotelDescProvider.GetByHotelId(tm,objHotel.Id);
                foreach (HotelDesc d in objHoteldesc)
                {
                    d.Address = objHotel.HotelAddress;
                    DataRepository.HotelDescProvider.Update(tm,d);
                }
                DataRepository.HotelPhotoVideoGallaryProvider.Update(tm, objHotelPhoto);
                tm.Commit();
                result[0] = "Contract updated successfully.";
                result[1] = "succesfuly";
            }
            catch (Exception ex)
            {
                tm.Rollback();
                result[0] = "Error occured while contract updated.";
                result[1] = "error";
            }
            return result;
        }


        /// <summary>
        /// Get Photo and Video by whereclause.
        /// </summary>
        /// <param name="whereClause"></param>
        /// <returns></returns>
        public TList<HotelPhotoVideoGallary> GetByPhotoVideoByCondition(string whereClause)
        {
            int count = 0;
            return DataRepository.HotelPhotoVideoGallaryProvider.GetPaged(whereClause, string.Empty, 0, int.MaxValue, out count);
        }


        /// <summary>
        /// Get photo video by photo id.
        /// </summary>
        /// <param name="photoid"></param>
        /// <returns></returns>
        public HotelPhotoVideoGallary GetPhotoVideoByID(Int64 photoid)
        {
            return DataRepository.HotelPhotoVideoGallaryProvider.GetById(photoid);
        }


        /// <summary>
        /// Change hotel Active status by hotel id.
        /// </summary>
        /// <param name="Hotelid"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool ChangeHotelActiveStatus(Int32 Hotelid, bool status)
        {
            TransactionManager tm = null;
            bool result = false;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Hotel objHotal = DataRepository.HotelProvider.GetById(Hotelid);
                objHotal.IsActive = status;

               
                //Audit trail maintain
                Guid gid = Guid.NewGuid();
                Hotel old = objHotal.GetOriginalEntity();
                old.Id = objHotal.Id;
                if (HttpContext.Current.Session["CurrentSalesPersonID"] != null)
                {
                    TrailManager.LogAuditTrail<Hotel>(tm, objHotal, old, AuditAction.U, Convert.ToInt64(HttpContext.Current.Session["CurrentSalesPersonID"]), Convert.ToInt32(PageType.ClientContract), "ClientContract", Convert.ToString(Hotelid));
                }
                else
                {
                    TrailManager.LogAuditTrail<Hotel>(tm, objHotal, old, AuditAction.U, Convert.ToInt64(HttpContext.Current.Session["CurrentOperatorID"]), Convert.ToInt32(PageType.ClientContract), "ClientContract", Convert.ToString(Hotelid));
                }
                //Audit trail maintain 


                DataRepository.HotelProvider.Update(tm, objHotal);
                tm.Commit();
                result = true;
            }
            catch (Exception ex)
            {
                tm.Rollback();
                result = false;
            }
            return result;
        }

        /// <summary>
        /// Change user Active status for operator add admin section
        /// </summary>
        /// <param name="Hotelid"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool ChangeHotelAdminActiveStatus(Int32 UserId, bool status)
        {
            TransactionManager tm = null;
            bool result = false;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Users objUser = DataRepository.UsersProvider.GetByUserId(UserId);
                objUser.IsActive = status;
                DataRepository.UsersProvider.Update(tm, objUser);
                tm.Commit();
                result = true;
            }
            catch (Exception ex)
            {
                tm.Rollback();
                result = false;
            }
            return result;
        }


        /// <summary>
        /// Change hotel Active status by hotel id.
        /// </summary>
        /// <param name="Hotelid"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool ChangeUserActiveStatus(Int32 UserId, bool status)
        {
            TransactionManager tm = null;
            bool result = false;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Users objUser = DataRepository.UsersProvider.GetByUserId(UserId);
                objUser.IsActive = status;
                DataRepository.UsersProvider.Update(tm, objUser);
                tm.Commit();
                result = true;
            }
            catch (Exception ex)
            {
                tm.Rollback();
                result = false;
            }
            return result;
        }


        /// <summary>
        /// Get details of First Operator in the database.
        /// </summary>
        /// <returns></returns>
        public Users GetFirstOperatorInDataBase()
        {
            int count = 0;
            TList<Users> objUsers = DataRepository.UsersProvider.GetPaged(UsersColumn.Usertype + "=" + (int)Usertype.Operator, string.Empty, 0, int.MaxValue, out count);
            if (objUsers.Count > 0)
            {
                return objUsers[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Get details of First super admin in the database.
        /// </summary>
        /// <returns></returns>
        public Users GetFirstSuperAdminInDataBase()
        {
            int count = 0;
            TList<Users> objUsers = DataRepository.UsersProvider.GetPaged(UsersColumn.Usertype + "=" + (int)Usertype.Superadmin, string.Empty, 0, int.MaxValue, out count);
            if (objUsers.Count > 0)
            {
                return objUsers[0];
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// Get Country details by Country ID.
        /// </summary>
        /// <param name="countryid"></param>
        /// <returns></returns>
        public Country GetByCountryID(Int64 countryid)
        {
            return DataRepository.CountryProvider.GetById(countryid);
        }

        /// <summary>
        /// Get City Details by City Id.
        /// </summary>
        /// <param name="cityid"></param>
        /// <returns></returns>
        public City GetByCityID(Int64 cityid)
        {
            return DataRepository.CityProvider.GetById(cityid);
        }

        /// <summary>
        /// Get User details By User ID.
        /// </summary>
        /// <param name="Userid"></param>
        /// <returns></returns>
        public Users GetByUserID(Int64 Userid)
        {
            Users u = DataRepository.UsersProvider.GetByUserId(Userid);
            DataRepository.UsersProvider.DeepLoad(u);
            return u;
        }

        /// <summary>
        /// Get User details By User ID and parent id.
        /// </summary>
        /// <param name="Userid"></param>
        /// <returns></returns>
        public TList<Users> GetUserByParentid(int UserId)
        {
            int count = 0;
            TList<Users> objUsers = DataRepository.UsersProvider.GetPaged(UsersColumn.Usertype + "=" + (int)Usertype.HotelClient + " and ParentID is null" + " and UserID=" + UserId, string.Empty, 0, int.MaxValue, out count);                
            return objUsers;
        }


        /// <summary>
        /// Get User details For VAT number
        /// </summary>
        /// <param name="Userid"></param>
        /// <returns></returns>
        public TList<UserDetails> GetVATnumberUserID(Int64 Userid)
        {
            int count = 0;
            TList<UserDetails> objUsers = DataRepository.UserDetailsProvider.GetPaged("UserID=" + Userid, string.Empty, 0, int.MaxValue, out count);
            return objUsers;
        }

        /// <summary>
        /// delete the Client contract 
        /// </summary>
        /// <param name="htlid"></param>
        /// <returns></returns>
        public bool DeleteContract(Int64 htlid)
        {
            TransactionManager tm = null;
            bool result = false;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();

                Hotel objHotal = DataRepository.HotelProvider.GetById(tm,htlid);
                TList<HotelOwnerLink> objHotelOwner = DataRepository.HotelOwnerLinkProvider.GetByHotelId(tm, htlid);
                foreach (HotelOwnerLink h in objHotelOwner)
                {
                    Users objuser = DataRepository.UsersProvider.GetByUserId(tm, h.UserId,0,int.MaxValue);
                    if (objuser.Usertype == (int)Usertype.HotelUser)
                    {
                        objuser.IsActive = false;
                        objuser.IsRemoved = true;
                        DataRepository.UsersProvider.Update(tm,objuser);
                    }
                }
                objHotal.IsActive = false;
                objHotal.IsRemoved = true;
                if (DataRepository.HotelProvider.Update(tm,objHotal))
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return result;
        }

        //Get Hotel description by Hotel id and Language Id.
        public HotelDesc GetHotelDescriptionByHotelAndLanguageId(Int64 hotelid, Int64 languageId)
        {
            string where = "Hotel_Id=" + hotelid;
            if (languageId > 0)
            {
                where += " and Language_Id=" + languageId;
            }
            int count = 0;
            TList<HotelDesc> objHotelDesc = DataRepository.HotelDescProvider.GetPaged(where, string.Empty, 0, int.MaxValue, out count);
            if (objHotelDesc.Count > 0)
            {
                return objHotelDesc[0];
            }
            else
            {
                return (new HotelDesc());
            }
        }

        //Get Meeting room details by meetingroom Id
        public MeetingRoom GetMeetingRoomDetailsById(Int64 meetingroomid)
        {
            MeetingRoom objMeeting = DataRepository.MeetingRoomProvider.GetById(meetingroomid);
            DataRepository.MeetingRoomProvider.DeepLoad(objMeeting);
            return objMeeting;
        }

        //Get all language from database wich is active and ready to use.
        public TList<Language> GetAllLanguage()
        {
            string where = LanguageColumn.IsActive + "=1";
            int count = 0;
            return DataRepository.LanguageProvider.GetPaged(where,string.Empty,0,int.MaxValue,out count);
        }

        /// <summary>
        /// Get Package details by hotel with deepload
        /// </summary>
        /// <param name="Hotelid"></param>
        /// <returns></returns>
        public TList<PackageByHotel> GetPackageDetailsByHotel(Int64 Hotelid)
        {
            DataRepository.Providers.Clear();
            TList<PackageByHotel> lstPackageByHotel = DataRepository.PackageByHotelProvider.GetByHotelId(Hotelid);
            DataRepository.PackageByHotelProvider.DeepLoad(lstPackageByHotel);
            return lstPackageByHotel.FindAll(a => a.PackageIdSource != null && (a.IsOnline == false || a.PackageIdSource.PackageName.ToLower() == "standard"));
        }
        public PackageMaster getPackageById(long packageid)
        {
            return DataRepository.PackageMasterProvider.GetById(packageid);
        }
        public void GetDescriptionofPackageByPackage(PackageMaster p)
        {
            DataRepository.PackageMasterProvider.DeepLoad(p);
        }
        /// <summary>
        /// Create By: Gaurav Shrivastava
        /// Get Package by hotel id in once and then pay with that element
        /// </summary>
        /// <param name="packageid"></param>
        /// <returns></returns>
        public TList<PackageByHotel> GetPackageDetailsByHotelID(Int64 HotelID)
        {
            TList<PackageByHotel> objPac = DataRepository.PackageByHotelProvider.GetByHotelId(HotelID);
            DataRepository.PackageByHotelProvider.DeepLoad(objPac);
            return objPac;
        }


        public TList<PackageItems> GetPackageItemDetailsByPackageID(Int64 packageid)
        {
            TList<PackageItemMapping> objPackageItemMap = new TList<PackageItemMapping>();
            objPackageItemMap = DataRepository.PackageItemMappingProvider.GetByPackageId(packageid);
            TList<PackageItems> objPackageItem = new TList<PackageItems>();
            foreach (PackageItemMapping p in objPackageItemMap)
            {
                //PackageItems pnew = DataRepository.PackageItemsProvider.GetById(p.ItemId);
                //if (pnew.IsActive == true)
                //{
                objPackageItem.Add(DataRepository.PackageItemsProvider.GetById(p.ItemId));
                //}
            }
            DataRepository.PackageItemsProvider.DeepLoad(objPackageItem);
            return objPackageItem;
        }

        public void GetActualPriceByHotelPackageID(PackageMaster packageMaster)
        {
            DataRepository.PackageMasterProvider.DeepLoad(packageMaster);
        }

        public TList<PackageItems> GetItemDetailsByHotelandType(Int64 Hotelid,ItemType objItemType)
        {
            TList<PackageItems> objPackageItems = new TList<PackageItems>();
            TList<PackageByHotel> objHotelPackage = DataRepository.PackageByHotelProvider.GetByHotelId(Hotelid);

            foreach (PackageByHotel p in objHotelPackage.Where(a => a.IsOnline == true && a.PackageId == null))
            {
                if (p.PackageId != null || p.PackageId != 0)
                {
                    PackageItems objPackageItms = DataRepository.PackageItemsProvider.GetById(p.ItemId);
                    if (Convert.ToInt32(objPackageItms.ItemType) == (int)objItemType && objPackageItms.IsActive == true)
                    {
                        objPackageItems.Add(objPackageItms);
                    }
                }
            }
            DataRepository.PackageItemsProvider.DeepLoad(objPackageItems);
            return objPackageItems;
        }

        public TList<PackageItems> GetIsExtraItemDetailsByHotel(Int64 HotelId)
        {
            TList<PackageItems> objPackageItems = new TList<PackageItems>();
            TList<PackageByHotel> objHotelPackage = DataRepository.PackageByHotelProvider.GetByHotelId(HotelId);

            foreach (PackageByHotel p in objHotelPackage.Where(a => a.IsOnline == true))
            {
                if (p.PackageId != null || p.PackageId != 0)
                {
                    PackageItems objPackageItms = DataRepository.PackageItemsProvider.GetById(p.ItemId);
                    if (objPackageItms.IsExtra == true && objPackageItms.IsActive == true)
                    {
                        objPackageItems.Add(objPackageItms);
                    }
                }
            }
            DataRepository.PackageItemsProvider.DeepLoad(objPackageItems);
            return objPackageItems;
        }

        #region Get Discount of Meeting room/Package according to selected date
        public decimal GetMeetingRoomDiscountByMeetingroomIDandDate(Int64 HotelID, DateTime CurrentDate)
        {
            string where = SpecialPriceAndPromoColumn.HotelId + "=" + HotelID + " and " + SpecialPriceAndPromoColumn.SpecialPriceDate + "='" + CurrentDate + "'";
            int count = 0;
            TList<SpecialPriceAndPromo> objSpecialPrice = DataRepository.SpecialPriceAndPromoProvider.GetPaged(where, string.Empty, 0, int.MaxValue, out count);
            if (objSpecialPrice.Count > 0)
            {
                return Convert.ToDecimal(objSpecialPrice[0].MeetingRoomPercent);
            }
            else
            {
                return 0;
            }
        }

        public decimal GetPackageDiscountByPackageIDandDate(Int64 HotelID, DateTime CurrentDate)
        {
            string where = SpecialPriceAndPromoColumn.HotelId + "=" + HotelID + " and " + SpecialPriceAndPromoColumn.SpecialPriceDate + "='" + CurrentDate + "'";
            int count = 0;
            TList<SpecialPriceAndPromo> objSpecialPrice = DataRepository.SpecialPriceAndPromoProvider.GetPaged(where, string.Empty, 0, int.MaxValue, out count);
            if (objSpecialPrice.Count > 0)
            {
                return Convert.ToDecimal(objSpecialPrice[0].DdrPercent);
            }
            else
            {
                return 0;
            }
        }
        #endregion

        #region Get Bedroom Details
        public BedRoom GetBedRoomDetailsByBedroomID(Int64 bedroomid)
        {
            BedRoom objBedroom = DataRepository.BedRoomProvider.GetById(bedroomid);
            DataRepository.BedRoomProvider.DeepLoad(objBedroom);
            return objBedroom;
        }
        public VList<ViewFindAvailableBedroom> GetAvailabilityBedroomListByHotelID(Int64 hotelid, DateTime DateFrom, DateTime DateTo, int Duration)
        {
            int count = 0;
            string where = ViewFindAvailableBedroomColumn.HotelId + "=" + hotelid + " and " + ViewFindAvailableBedroomColumn.AvailabilityDate + " between '" + DateFrom + "' and '" + DateTo + "'";
            //if (Duration == 1)
            //{
            //    where += " and (" + ViewFindAvailableBedroomColumn.MorningStatus + "=1 AND " + ViewFindAvailableBedroomColumn.AfternoonStatus + "=1)";
            //}
            //else
            //{
            //    where += " and (" + ViewFindAvailableBedroomColumn.MorningStatus + "=1 OR " + ViewFindAvailableBedroomColumn.AfternoonStatus + "=1)";
            //}
            VList<ViewFindAvailableBedroom> objAvailabilityBedroom = DataRepository.ViewFindAvailableBedroomProvider.GetPaged(where, string.Empty, 0, int.MaxValue, out count);
            return objAvailabilityBedroom;
        }
        #endregion

        //public decimal GetHotelVatPercentageByCountryID(Int64 Hotelid)
        //{

        //}
    }
}
