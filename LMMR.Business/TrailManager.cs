﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Data;
using LMMR.Entities;
using System.Xml;
using System.Xml.Serialization;
using System.Data;

namespace LMMR.Business
{

    public enum AuditAction
    {
        //Inserted
        I,
        //Updated
        U,
        //Deleted
        D,
        //Status Deleted
        SD
    }
    public enum PageType
    {
        HotelConferenceInfo = 1,
        ContactDetails = 2,
        Facilities = 3,
        MeetingRoomDescription = 4,
        MeetingRoomConfiguration = 5,
        BedroomDescription = 6,
        PricingMeetingRoom = 7,
        PricingBedRoom = 8,
        Availability = 9,
        OnlineBookingStatus = 10,
        SpecialPromotionPkgDiscount = 11,
        SpecialPromotionMeetingroomDiscount = 12,
        PendingRequestStatus = 13,
        ClientContract=15
    }
    public class TrailManager
    {
        public void HotelTrailInsert(Hotel htl)
        {

        }

        //Declare Variable to message
        private string _varMessage;

        //Property to store message
        public string propMessage
        {
            get { return _varMessage; }
            set { _varMessage = value; }
        }

        public void AvailabilityTrailInsert(Availability avail)
        {
            AvailabilityTrail objAvailTrail = new AvailabilityTrail();
            objAvailTrail.HotelId = avail.HotelId;
            objAvailTrail.AvailabilityId = avail.Id;
            objAvailTrail.AvailabilityDate = avail.AvailabilityDate;
            DataRepository.AvailabilityTrailProvider.Insert(objAvailTrail);
        }

        public static bool LogAuditTrail<T>(T newEntity, T oldEntity, AuditAction action, Int64 UserId, int PageName, string tablename,string RefID)
        {
            return LogAuditTrail<T>(null, newEntity, oldEntity, action, UserId, PageName, tablename, RefID);
        }

        public static bool LogAuditTrail<T>(TransactionManager tmgr, T newEntity, T oldEntity, AuditAction action, Int64 UserId, int PageName, string tablename, string RefID)
        {
            bool b = false;
            bool OuterTransaction = false;
            try
            {
                if (tmgr == null)
                {
                    tmgr = DataRepository.Provider.CreateTransaction();
                    tmgr.BeginTransaction();
                }
                else
                {
                    OuterTransaction = true;
                }
                DbAudit objAudit = new DbAudit();
                objAudit.PageName = PageName;
                objAudit.NewData = XmlSerialize(newEntity);
                if (action != AuditAction.I && action != AuditAction.D)
                {
                    objAudit.OldData = XmlSerialize(oldEntity);
                }
                else
                {
                    objAudit.OldData = null;
                }
                objAudit.RevisionStamp = DateTime.Now;
                objAudit.EntityTblName = tablename;
                objAudit.UserName = UserId;
                objAudit.Actions = Convert.ToString(action);
                objAudit.ReferenceId = RefID;
                if (DataRepository.DbAuditProvider.Insert(tmgr, objAudit))
                {
                    b = true;
                }
                if (!OuterTransaction)
                {
                    tmgr.Commit();
                }
                
            }
            catch (Exception ex)
            {
                tmgr.Rollback();
            }
            return b;            
        }


        #region
        /// <summary>
        //This method is used for insert new ContactDetail
        /// </summary>        
        public string AddNewAuditDetails(DbAudit objAudit)
        {
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                if (DataRepository.DbAuditProvider.Insert(tm, objAudit))
                {
                    propMessage = "Added successfully";
                }
                else
                {
                    propMessage = "Please try again !";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return propMessage;
        }
        #endregion


        /// <summary>
        /// Seriallize an object to XML
        /// object should be an EntityObject, class, struct
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string XmlSerialize(object obj)
        {
            if (null != obj)
            {
                // Assuming obj is an instance of an object
                XmlSerializer ser = new XmlSerializer(obj.GetType());
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.IO.StringWriter writer = new System.IO.StringWriter(sb);
                ser.Serialize(writer, obj);
                return sb.ToString();
            }
            return string.Empty;
        }

        /// <summary>
        /// Deseriallize an xml to corressponding EntityObject, class, struct
        /// </summary>
        /// <param name="objType"></param>
        /// <param name="xmlDoc"></param>
        /// <returns></returns>
        public static object XmlDeserialize(Type objType, string xmlDoc)
        {
            if (xmlDoc != null && objType != null)
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlDoc);
                //Assuming doc is an XML document containing a serialized object and objType is a System.Type set to the type of the object.
                XmlNodeReader reader = new XmlNodeReader(doc.DocumentElement);
                XmlSerializer ser = new XmlSerializer(objType);
                if (reader != null)
                {
                    return ser.Deserialize(reader);
                }
                else
                {
                    return null;
                }
            }
            return null;
        }        
    }

    

}
