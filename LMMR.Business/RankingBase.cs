﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;
using System.Data;
using System.IO;
using System.Configuration;
namespace LMMR.Business
{
    
    public class MainHoteldetail
    {
        public Int64 HotelID { get; set; }
        public int Bookingrank { get; set; }
        public int RequestRank { get; set; }
     
    }
    public enum operatorchoice
    { 
        excellent=20,
        good=15,
        average=10,
        bad=5
    
    }
    public abstract class RankingBase
    {
        TList<Hotel> tlisthotel;
            VList<Viewbookingrequest> Vlistreq = new VList<Viewbookingrequest>();
        

        #region Called function
        /// <summary>
        /// This funcation contains all the function which is used to calculate ranking
        /// </summary>
        public void Final()
        {
            try
            {


                HotelInfo objhotelinfo = new HotelInfo();
                tlisthotel = objhotelinfo.GetAllHotel();
                foreach (Hotel h in tlisthotel)
                {

                    h.BookingAlgo = 0;
                    h.RequestAlgo = 0;
                    DataRepository.HotelProvider.Update(h);

                }

            #region Hotelbackend
                movetoprocess();
                movetoexpire();
            #endregion

            #region  Ranking Rules for Booking
            try
            {
                operatorrule(0);
                totalavailability();
                TotalDiscount();
                numberofBookingReq(0);
            }
            catch (Exception ex)
            {
                FileStream fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + "Log/ErrorRankingbooking.txt", FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter m_streamWriter = new StreamWriter(fs);
                m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
                m_streamWriter.WriteLine(ex.Message + "-- target :" + ex.TargetSite +"-- innnerexception:"+ex.InnerException+ " -- source: " + ex.Source + "--- time :" + System.DateTime.Now);
                m_streamWriter.Flush();
                m_streamWriter.Close(); 
            
            }
            #endregion


            #region  Ranking Rules for Request
                try
                {
                    numberofBookingReq(1);
                    operatorrule(1);
                    totalonlineInventory();
                    TotalconversionREQtoTEN();
                    TotalconversionTENDEF();
                }
                catch (Exception ex)
                {
                    FileStream fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + "Log/ErrorRankingRequest.txt", FileMode.OpenOrCreate, FileAccess.Write);
                    StreamWriter m_streamWriter = new StreamWriter(fs);
                    m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
                    m_streamWriter.WriteLine(ex.Message + "-- target :" + ex.TargetSite + "-- innnerexception:" + ex.InnerException + " -- source: " + ex.Source + "--- time :" + System.DateTime.Now);
                    m_streamWriter.Flush();
                    m_streamWriter.Close();

                }
            #endregion

            }
            catch (Exception ex)
            {
                FileStream fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + "Log/Error-Final.txt", FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter m_streamWriter = new StreamWriter(fs);
                m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
                m_streamWriter.WriteLine(ex.Message + "-- target :" + ex.TargetSite + "-- innnerexception:" + ex.InnerException + " -- source: " + ex.Source + "--- time :" + System.DateTime.Now);
                m_streamWriter.Flush();
                m_streamWriter.Close();

            }
             
        }
        #endregion

        #region OperatorChoice - rule 4-book,11 -re
        /// <summary>
        /// Operator choice
        /// </summary>
        /// <param name="type">0- booking, 1 -request</param>
        public void operatorrule(int type)
        {
            
           // List<MainHoteldetail> objmain = new List<MainHoteldetail>();         
            HotelInfo objhotelinfo = new HotelInfo();
            tlisthotel = objhotelinfo.GetAllHotel();
            int weighttage = 0;
            foreach (Hotel h in tlisthotel)
            {
                MainHoteldetail mainhotel = new MainHoteldetail();
                mainhotel.HotelID = h.Id;
                mainhotel.Bookingrank = operatorchoice.excellent.ToString() == h.OperatorChoice.ToLower() ? (int)operatorchoice.excellent : operatorchoice.good.ToString() == h.OperatorChoice.ToLower() ? (int)operatorchoice.good : operatorchoice.average.ToString() == h.OperatorChoice.ToLower() ? (int)operatorchoice.average : operatorchoice.bad.ToString() == h.OperatorChoice.ToLower() ? (int)operatorchoice.bad : 0;

                decimal cnvalu = 0;

                if (type == 0)
                {
                  weighttage=  DataRepository.RankingAlgoMasterProvider.GetById(4).RulePercentage;
                  cnvalu = Convert.ToDecimal(h.BookingAlgo) + Convert.ToDecimal(mainhotel.Bookingrank * weighttage * .01);
                  h.BookingAlgo = cnvalu;
                }
                else
                {
                    weighttage = DataRepository.RankingAlgoMasterProvider.GetById(11).RulePercentage;
                    cnvalu = cnvalu = Convert.ToDecimal(h.RequestAlgo) + Convert.ToDecimal(mainhotel.Bookingrank * weighttage * .01);
                    h.RequestAlgo = cnvalu;
                }

                DataRepository.HotelProvider.Update(h);
            }
          

        }
        #endregion     


        #region rule 3for booking, 8- for request - numberofBookingReq
        /// <summary>
        /// Ranking rule for booking/request
        /// Ranking rule 3 (booking), 8 (request)
        /// </summary>
        /// <param name="type">0- booking, 1-request</param>
        public void numberofBookingReq(int type)
        {
           
            int weighttage =0;
            List<MainHoteldetail> objmain = new List<MainHoteldetail>();    
            if(type==0)
             weighttage = DataRepository.RankingAlgoMasterProvider.GetById(3).RulePercentage;
            else
                weighttage = DataRepository.RankingAlgoMasterProvider.GetById(8).RulePercentage;
            HotelInfo objhotelinfo = new HotelInfo();
            tlisthotel = objhotelinfo.GetAllHotel();
            foreach (Hotel h in tlisthotel)
            {
                int intCount = 0;
                int Total = 0;
               // int bookingcount = 0;
                string whereclause = " HotelId=" + h.Id + " and BookingDate  between getdate()-60 and getdate() and booktype= "+ Convert.ToString( type);
                TList<Booking> ObjBooking = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
                intCount = ObjBooking.Count;
                MainHoteldetail mainhotel = new MainHoteldetail();
                mainhotel.HotelID = h.Id;
                mainhotel.Bookingrank = intCount;
                objmain.Add(mainhotel);                
            }
            objmain = objmain.OrderByDescending(a => a.Bookingrank).ToList();
            #region loop for range

          for(int bookingcount=0;bookingcount<=4;bookingcount++)
            {
                Hotel n = DataRepository.HotelProvider.GetById(objmain[bookingcount].HotelID);
                decimal cn = 0;
                if (type == 0)
                {
                    cn = (Convert.ToDecimal(n.BookingAlgo) + Convert.ToDecimal(10 * weighttage * .01));
                    n.BookingAlgo = cn;
                }
                else 
                {
                    cn = (Convert.ToDecimal(n.RequestAlgo) + Convert.ToDecimal(20 * weighttage * .01));
                    n.RequestAlgo = cn;
              
                }

                DataRepository.HotelProvider.Update(n);
            }
              for (int bookingcount = 5 ; bookingcount <=9; bookingcount++)
              {
                  Hotel n = DataRepository.HotelProvider.GetById(objmain[bookingcount].HotelID);
                  decimal cn = 0;
                  if (type == 0)
                  {
                      cn = (Convert.ToDecimal(n.BookingAlgo) + Convert.ToDecimal(7 * weighttage * .01));
                      n.BookingAlgo = cn;
                  }
                  else
                  {
                      cn = (Convert.ToDecimal(n.RequestAlgo) + Convert.ToDecimal(15* weighttage * .01));
                      n.RequestAlgo = cn;

                  }
                  DataRepository.HotelProvider.Update(n);
              }

          for (int bookingcount = 10; bookingcount <= 29 ; bookingcount++)
          {
              Hotel n = DataRepository.HotelProvider.GetById(objmain[bookingcount].HotelID);
              decimal cn = 0;
              if (type == 0)
              {
                  cn = (Convert.ToDecimal(n.BookingAlgo) + Convert.ToDecimal(4 * weighttage * .01));
                  n.BookingAlgo = cn;
              }
              else
              {
                  cn = (Convert.ToDecimal(n.RequestAlgo) + Convert.ToDecimal(10 * weighttage * .01));
                  n.RequestAlgo = cn;

              }
              DataRepository.HotelProvider.Update(n);
          }
          for (int bookingcount = 30; bookingcount < objmain.Count; bookingcount++)
          {
              Hotel n = DataRepository.HotelProvider.GetById(objmain[bookingcount].HotelID);
              decimal cn = 0;
              if (type == 0)
              {
                  cn = (Convert.ToDecimal(n.BookingAlgo) + 0);
                  n.BookingAlgo = cn;
              }
              else
              {
                  cn = (Convert.ToDecimal(n.RequestAlgo) +0);
                  n.RequestAlgo = cn;

              }
              DataRepository.HotelProvider.Update(n);
          }

            #endregion
         
                         
        
        }
        #endregion

        #region rule6request-totalonlineInventory
        /// <summary>
        /// Ranking rule for request ,
        /// Rule id 6
        /// </summary>
        
        public void totalonlineInventory()
        {
           
            List<MainHoteldetail> objmain = new List<MainHoteldetail>();
            int weighttage = DataRepository.RankingAlgoMasterProvider.GetById(6).RulePercentage;
            HotelInfo objhotelinfo = new HotelInfo();
            tlisthotel = objhotelinfo.GetAllHotel();
            foreach (Hotel h in tlisthotel)
            {
                int intCount = 0;
                int Total = 0;
                // int bookingcount = 0;
                string whereclause = " Hotel_Id=" + h.Id + " and  isDeleted=0 ";
                TList<MeetingRoom> ObjBooking = DataRepository.MeetingRoomProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
                intCount = ObjBooking.Count;
                MainHoteldetail mainhotel = new MainHoteldetail();
                mainhotel.HotelID = h.Id;
                mainhotel.Bookingrank = intCount;
                objmain.Add(mainhotel);
            }
            objmain = objmain.OrderByDescending(a => a.Bookingrank).ToList();
            #region loop for range

            for (int bookingcount = 0; bookingcount <= 4; bookingcount++)
            {
                Hotel n = DataRepository.HotelProvider.GetById(objmain[bookingcount].HotelID);
                decimal cn = (Convert.ToDecimal(n.RequestAlgo) + Convert.ToDecimal(20 * weighttage * .01));
                n.RequestAlgo = cn;
                DataRepository.HotelProvider.Update(n);
            }
            for (int bookingcount = 5; bookingcount <= 9; bookingcount++)
            {
                Hotel n = DataRepository.HotelProvider.GetById(objmain[bookingcount].HotelID);
                n.RequestAlgo = Convert.ToDecimal(n.RequestAlgo) + Convert.ToDecimal(15 * weighttage * .01);
                DataRepository.HotelProvider.Update(n);
            }

            for (int bookingcount = 10; bookingcount <= 29; bookingcount++)
            {
                Hotel n = DataRepository.HotelProvider.GetById(objmain[bookingcount].HotelID);
                n.RequestAlgo = Convert.ToDecimal(n.RequestAlgo) + Convert.ToDecimal(5 * weighttage * .01);
                DataRepository.HotelProvider.Update(n);
            }
            for (int bookingcount = 30; bookingcount < objmain.Count; bookingcount++)
            {
                Hotel n = DataRepository.HotelProvider.GetById(objmain[bookingcount].HotelID);

                n.RequestAlgo = Convert.ToDecimal(n.RequestAlgo) + Convert.ToDecimal(0 * weighttage * .01);
                DataRepository.HotelProvider.Update(n);
            }

            #endregion
           
            
        }
        #endregion

        #region rule 1 for booking -totalavailability
        /// <summary>
        /// Rule for booking 1
        /// </summary>
        public void totalavailability()
        {
          
            
            int weighttage = DataRepository.RankingAlgoMasterProvider.GetById(1).RulePercentage;
            HotelInfo objhotelinfo = new HotelInfo();
            tlisthotel = objhotelinfo.GetAllHotel();
            foreach (Hotel h in tlisthotel)
            {
                int intCount = 0;
                int Total = 0;
                // int bookingcount = 0;
                string whereclause = " HotelId=" + h.Id + " and AvailabilityDate between getdate() and getdate()+60";
                TList<Availability> ObjBooking = DataRepository.AvailabilityProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
                decimal avail = 0;
                foreach (Availability avl in ObjBooking)
                {
                    if (avl.FullPropertyStatus == 0)
                    {
                        avail = avail + Convert.ToDecimal( 0.5);
                    }
                
                }

                decimal cnvalu = Convert.ToDecimal( h.BookingAlgo) +( (Convert.ToDecimal( avail) * Convert.ToDecimal( weighttage) * Convert .ToDecimal( .01)));
                h.BookingAlgo = cnvalu;
                DataRepository.HotelProvider.Update(h);
          
            }

            



        }
        #endregion


        public void TotalconversionREQtoTEN()
        {
                int weighttage = DataRepository.RankingAlgoMasterProvider.GetById(9).RulePercentage;

                HotelInfo objhotelinfo = new HotelInfo();
                tlisthotel = objhotelinfo.GetAllHotel();
                foreach (Hotel h in tlisthotel)
                {
                    int intCount = 0;
                    int Total = 0;

                    // int bookingcount = 0;

                    string whereclause = " HotelID=" + h.Id + " and ModifyDate between getdate()-60 and getdate()";

                    TList<BookingLogs> ObjBooking = DataRepository.BookingLogsProvider.GetPaged(whereclause, " ModifyDate ", 0, int.MaxValue, out Total);
                    int cntnewten = 0;

                    foreach (BookingLogs b in ObjBooking)
                    {
                        if (b.CurrentStatus == BookingRequestStatus.Tentative.ToString().ToUpper() && b.LastStatus == BookingRequestStatus.New.ToString().ToUpper())
                        {
                            cntnewten++;
                        }
                        //if (b.CurrentStatus == BookingRequestStatus.Definative.ToString().ToUpper() && b.LastStatus == BookingRequestStatus.Tentative.ToString().ToUpper())
                        //{
                        //    cnttendef++;
                        //}

                    }

                    string whereclause1 = " HotelId=" + h.Id + " and BookingDate  between getdate()-60 and getdate() and booktype=1 ";
                    TList<Booking> ObjBooking1 = DataRepository.BookingProvider.GetPaged(whereclause1, string.Empty, 0, int.MaxValue, out Total);
                    intCount = ObjBooking1.Count;
                    int percentage = 0;
                    if (intCount == 0)
                    {
                         percentage = 0;
                    }
                    else
                    {
                        percentage = (cntnewten / intCount) * 100;
                    }
                    decimal cnvalu = 0;
                    if (percentage >= 80 && percentage <= 100)
                    {
                        int val = 20;
                        cnvalu = Convert.ToDecimal(h.RequestAlgo) + (Convert.ToDecimal(val) * Convert.ToDecimal(weighttage) * Convert.ToDecimal(.01));
                        h.RequestAlgo = cnvalu;
                        DataRepository.HotelProvider.Update(h);
                    }
                    if (percentage >= 70 && percentage <= 79)
                    {

                        int val = 15;
                        cnvalu = Convert.ToDecimal(h.RequestAlgo) + (Convert.ToDecimal(val) * Convert.ToDecimal(weighttage) * Convert.ToDecimal(.01));
                        h.RequestAlgo = cnvalu;
                        DataRepository.HotelProvider.Update(h);


                    }
                    if (percentage >= 50 && percentage <= 69)
                    {

                        int val = 10;
                        cnvalu = Convert.ToDecimal(h.RequestAlgo) + (Convert.ToDecimal(val) * Convert.ToDecimal(weighttage) * Convert.ToDecimal(.01));
                        h.RequestAlgo = cnvalu;
                        DataRepository.HotelProvider.Update(h);

                    }
                    if (percentage >= 0 && percentage <= 49)
                    {

                        int val = 0;
                        cnvalu = Convert.ToDecimal(h.RequestAlgo) + (Convert.ToDecimal(val) * Convert.ToDecimal(weighttage) * Convert.ToDecimal(.01));
                        h.RequestAlgo = cnvalu;
                        DataRepository.HotelProvider.Update(h);

                    }



                }

        }

        public void TotalconversionTENDEF()
        {
           
            int weighttage =  DataRepository.RankingAlgoMasterProvider.GetById(10).RulePercentage;
            HotelInfo objhotelinfo = new HotelInfo();
            tlisthotel = objhotelinfo.GetAllHotel();
            foreach (Hotel h in tlisthotel)
            {
                int intCount = 0;
                int Total = 0;
                // int bookingcount = 0;

                string whereclause = " HotelID=" + h.Id + " and ModifyDate between getdate()-60 and getdate()";

                TList<BookingLogs> ObjBooking = DataRepository.BookingLogsProvider.GetPaged(whereclause, " ModifyDate ", 0, int.MaxValue, out Total);
               
                int cnttendef = 0;
                foreach (BookingLogs b in ObjBooking)
                {

                    if (b.CurrentStatus == BookingRequestStatus.Definite.ToString().ToUpper() && b.LastStatus == BookingRequestStatus.Tentative.ToString().ToUpper())
                    {
                        cnttendef++;
                    }

                }


                string whereclause1 = " HotelId=" + h.Id + " and BookingDate  between getdate()-60 and getdate() and booktype=1 " ;
                TList<Booking> ObjBooking1 = DataRepository.BookingProvider.GetPaged(whereclause1, string.Empty, 0, int.MaxValue, out Total);
                intCount = ObjBooking1.Count;


                int percentage = 0;
                if (intCount == 0)
                {
                    percentage = 0;
                }
                else
                {
                    percentage = (cnttendef / intCount) * 100;
                }
                decimal cnvalu = 0;
                if (percentage >= 70 && percentage <= 100)
                {
                    int val = 20;
                    cnvalu = Convert.ToDecimal(h.RequestAlgo) + (Convert.ToDecimal(val) * Convert.ToDecimal(weighttage) * Convert.ToDecimal(.01));
                    h.RequestAlgo = cnvalu;
                    DataRepository.HotelProvider.Update(h);
                }
                if (percentage >= 50 && percentage <= 69)
                {

                    int val = 15;
                    cnvalu = Convert.ToDecimal(h.RequestAlgo) + (Convert.ToDecimal(val) * Convert.ToDecimal(weighttage) * Convert.ToDecimal(.01));
                    h.RequestAlgo = cnvalu;
                    DataRepository.HotelProvider.Update(h);


                }
                if (percentage >= 30 && percentage <= 49)
                {

                    int val = 10;
                    cnvalu = Convert.ToDecimal(h.RequestAlgo) + (Convert.ToDecimal(val) * Convert.ToDecimal(weighttage) * Convert.ToDecimal(.01));
                    h.RequestAlgo = cnvalu;
                    DataRepository.HotelProvider.Update(h);

                }
                if (percentage >= 0 && percentage <= 29)
                {

                    int val = 0;
                    cnvalu = Convert.ToDecimal(h.RequestAlgo) + (Convert.ToDecimal(val) * Convert.ToDecimal(weighttage) * Convert.ToDecimal(.01));
                    h.RequestAlgo = cnvalu;
                    DataRepository.HotelProvider.Update(h);

                }
               
           
            }

          



        }

        #region discountvalue
        /// <summary>
        /// Total discount in past 60 days for booking
        /// </summary>
        public void TotalDiscount()
        {
          

            int weighttage = DataRepository.RankingAlgoMasterProvider.GetById(2).RulePercentage;
            HotelInfo objhotelinfo = new HotelInfo();
            tlisthotel = objhotelinfo.GetAllHotel();
            foreach (Hotel h in tlisthotel)
            {
                int intCount = 0;
                int Total = 0;
                int bookingcount = 0;
                string whereclause = " HotelId=" + h.Id + " and SpecialPriceDate between getdate() and getdate()+60";
                TList<SpecialPriceAndPromo> ObjBooking = DataRepository.SpecialPriceAndPromoProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
                decimal avail = 0;
                foreach (SpecialPriceAndPromo avl in ObjBooking)
                {
                    avail = Convert.ToDecimal( avail )+   Convert.ToDecimal(avl.DdrPercent);
                }

                avail = avail / 60;

                #region loopfor range

                if (-5 >= avail  && avail >= -10)
                {
                    bookingcount = 5;
                }

                if (-11 >= avail && avail >= -15)
                {
                    bookingcount = 10;
                }

                if (-16 >= avail  && avail >= -25)
                {
                    bookingcount = 15;
                }

                if (avail <= -26)
                {
                    bookingcount = 20;
                }
                #endregion
                decimal cnvalu = Convert.ToDecimal(h.BookingAlgo) + (Convert.ToDecimal(bookingcount) * Convert.ToDecimal(weighttage) * Convert.ToDecimal(.01));
                h.BookingAlgo = cnvalu;
                DataRepository.HotelProvider.Update(h);

            }


          


        }
        #endregion

        #region Hotel Backend

        #region Movetoprocessed
        public void movetoprocess()
        {
            string where = " requeststatus=1 and booktype = 0 and ArrivalDate between getdate()-1 and getdate()  ";
            string orderby = "";
            int totalcount = 0;
            VList<Viewbookingrequest> Vlistreq = new VList<Viewbookingrequest>();
            Vlistreq = DataRepository.ViewbookingrequestProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount);

            ViewBooking_Hotel objview = new ViewBooking_Hotel ();

            foreach (Viewbookingrequest vl in Vlistreq)
            {
                TransactionManager tm = null;
                bool Result = false;
                Booking objbooking = DataRepository.BookingProvider.GetById(vl.Id);
                try
                {
                    tm = DataRepository.Provider.CreateTransaction();
                    tm.BeginTransaction();
                    objbooking.RequestStatus = (int)BookingRequestStatus.Processed;
                    //Audit trail maintain
                    Guid gid = Guid.NewGuid();
                    Booking old = objbooking.GetOriginalEntity();
                    old.Id = objbooking.Id;

                    #region log
                    BookingLogs bklog = new BookingLogs();
                    bklog.BookingId = objbooking.Id;
                    bklog.CurrentStatus = Enum.GetName(typeof(BookingRequestStatus), 6).ToUpper();
                    bklog.LastStatus = "NEW";
                    bklog.HotelId = objbooking.HotelId;
                    bklog.UserId = Convert.ToInt32(objbooking.CreatorId);
                    bklog.ModifyDate = System.DateTime.Now;
                   
                    #endregion
                //  DataRepository.BookingLogsProvider.Insert(tm,bklog);
                    TrailManager.LogAuditTrail<Booking>(tm, objbooking, old, AuditAction.U, Convert.ToInt64(1), Convert.ToInt32(PageType.OnlineBookingStatus), objbooking.TableName, Convert.ToString(objbooking.HotelId));
                    //Audit trail maintain
                    if (DataRepository.BookingProvider.Update(tm, objbooking))
                    {
                        Result = true;
                        FileStream fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + "Log/SuccessHotelbackend.txt", FileMode.OpenOrCreate, FileAccess.Write);
                        StreamWriter m_streamWriter = new StreamWriter(fs);
                        m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
                        m_streamWriter.WriteLine(" Move to processed for booking # :" + objbooking.Id + " is done, time :" + System.DateTime.Now);
                        m_streamWriter.Flush();
                        m_streamWriter.Close();

                        DataRepository.BookingLogsProvider.Insert(tm, bklog);
                       
                    }
                    else
                    {
                        Result = false;
                    }
                    tm.Commit();
                }
                catch (Exception ex)
                {
                      
                FileStream fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + "Log/Hotelbackend-movetoprocesst.txt", FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter m_streamWriter = new StreamWriter(fs);
                m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
                 m_streamWriter.WriteLine(ex.Message + "-- target :" + ex.TargetSite +"-- innnerexception:"+ex.InnerException+ " -- source: " + ex.Source + "--- time :" + System.DateTime.Now);
                m_streamWriter.Flush();
                m_streamWriter.Close();            
                    tm.Rollback();
                }
            }

        }
        #endregion

        #region Move To Expire
        public void movetoexpire()
        {
            try
            {
                string where = " requeststatus=1 and booktype in (1,2) ";
                string orderby = "";
                int totalcount = 0;
                VList<Viewbookingrequest> Vlistreq = new VList<Viewbookingrequest>();
                Vlistreq = DataRepository.ViewbookingrequestProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount);

                ViewBooking_Hotel objview = new ViewBooking_Hotel();

                foreach (Viewbookingrequest vl in Vlistreq)
                {

                    
                    #region ExpiryDate

                    DateTime exptime;
                    DateTime dtmeetingdt = new DateTime(vl.BookingDate.Value.Year, vl.BookingDate.Value.Month, vl.BookingDate.Value.Day);
                    DateTime arrDate = new DateTime(vl.ArrivalDate.Value.Year, vl.ArrivalDate.Value.Month, vl.ArrivalDate.Value.Day);
                    //string day = Convert.ToString(dtmeetingdt.DayOfWeek); // as asked by martin to remove in MOM of 2609
                    //if (day == "Friday")
                    //{
                    //    lblExpiryDt.Text = dtmeetingdt.AddDays(3).ToString("dd/MM/yyyy");
                    //    exptime = dtmeetingdt.AddDays(3);
                    //}
                    //else
                    //{
                    //    lblExpiryDt.Text = dtmeetingdt.AddDays(2).ToString("dd/MM/yyyy");
                    //    exptime = dtmeetingdt.AddDays(2);
                    //}
                    exptime = dtmeetingdt.AddDays(5); // as asked by client
                    DateTime currentdate = new DateTime(Convert.ToInt32(System.DateTime.Now.Year), Convert.ToInt32(System.DateTime.Now.Month), Convert.ToInt32(System.DateTime.Now.Day));
                    TimeSpan ts = exptime.Subtract(currentdate);

                    if (ts.Days <= 0 || arrDate <= DateTime.Now)
                    {
                        TransactionManager tm = null;
                        bool Result = false;
                        Booking objbooking = DataRepository.BookingProvider.GetById(vl.Id);
                        try
                        {
                            tm = DataRepository.Provider.CreateTransaction();
                            tm.BeginTransaction();
                            objbooking.RequestStatus = (int)BookingRequestStatus.Expired;
                            //Audit trail maintain
                            Guid gid = Guid.NewGuid();
                            Booking old = objbooking.GetOriginalEntity();
                            old.Id = objbooking.Id;
                            TrailManager.LogAuditTrail<Booking>(tm, objbooking, old, AuditAction.U, Convert.ToInt64(1), Convert.ToInt32(PageType.OnlineBookingStatus), objbooking.TableName, Convert.ToString(objbooking.HotelId));
                            //Audit trail maintain
                            if (DataRepository.BookingProvider.Update(tm, objbooking))
                            {
                                FileStream fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + "Log/SuccessHotelbackend.txt", FileMode.OpenOrCreate, FileAccess.Write);
                                StreamWriter m_streamWriter = new StreamWriter(fs);
                                m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
                                m_streamWriter.WriteLine(" Move to expire for request # :" + objbooking.Id + " is done, time :" + System.DateTime.Now);
                                m_streamWriter.Flush();
                                m_streamWriter.Close();
                                Result = true;
                                #region log
                                BookingLogs bklog = new BookingLogs();
                                bklog.BookingId = objbooking.Id;
                                bklog.CurrentStatus = Enum.GetName(typeof(BookingRequestStatus), 8).ToUpper();
                                bklog.LastStatus = "NEW";
                                bklog.HotelId = objbooking.HotelId;
                                bklog.UserId = Convert.ToInt32(objbooking.CreatorId);
                                bklog.ModifyDate = System.DateTime.Now;
                                DataRepository.BookingLogsProvider.Insert(tm,bklog);
                              
                                #endregion

                            }
                            else
                            {
                                Result = false;
                            }
                            tm.Commit();
                        }
                        catch (Exception ex)
                        {
                            tm.Rollback();
                        }
                    }


                    #endregion

                }
            }
            catch                (Exception ex)
            {
                FileStream fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + "Log/Hotelbackend-movetoexpire.txt", FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter m_streamWriter = new StreamWriter(fs);
                m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
                m_streamWriter.WriteLine(ex.Message + "-- target :" + ex.TargetSite +"-- innnerexception:"+ex.InnerException+ " -- source: " + ex.Source + "--- time :" + System.DateTime.Now);
                m_streamWriter.Flush();
                m_streamWriter.Close();
            }

        }
        #endregion

      
        #region Move To Frozen - Booking
        public void movetofrozenbooking()
        {

            try
            {
                string where = " requeststatus=6 and booktype = 0  ";
                string orderby = "";
                int totalcount = 0;
                VList<Viewbookingrequest> Vlistreq = new VList<Viewbookingrequest>();
                Vlistreq = DataRepository.ViewbookingrequestProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount);



                ViewBooking_Hotel objview = new ViewBooking_Hotel();

                foreach (Viewbookingrequest vl in Vlistreq)
                {

                    #region Freeze row

                    DateTime currentdate = new DateTime(Convert.ToInt32(System.DateTime.Now.Year), Convert.ToInt32(System.DateTime.Now.Month), Convert.ToInt32(System.DateTime.Now.Day));

                    if (Convert.ToInt32(currentdate.Day) >= 8)
                    {

                        DateTime dtmeetingdt = new DateTime(Convert.ToInt32(vl.DepartureDate.Value.ToShortDateString().ToString().Split('/')[2]), Convert.ToInt32(vl.DepartureDate.Value.ToString().Split('/')[0]), Convert.ToInt32(vl.DepartureDate.Value.ToString().Split('/')[1]));

                        if (dtmeetingdt.Month < currentdate.Month)
                        {
                            TransactionManager tm = null;
                            bool Result = false;
                            Booking objbooking = DataRepository.BookingProvider.GetById(vl.Id);
                            try
                            {
                                tm = DataRepository.Provider.CreateTransaction();
                                tm.BeginTransaction();
                                objbooking.RequestStatus = (int)BookingRequestStatus.Frozen;
                                //Audit trail maintain
                                Guid gid = Guid.NewGuid();
                                Booking old = objbooking.GetOriginalEntity();
                                old.Id = objbooking.Id;



                                TrailManager.LogAuditTrail<Booking>(tm, objbooking, old, AuditAction.U, Convert.ToInt64(1), Convert.ToInt32(PageType.OnlineBookingStatus), objbooking.TableName, Convert.ToString(objbooking.HotelId));
                                //Audit trail maintain
                                if (DataRepository.BookingProvider.Update(tm, objbooking))
                                {
                                    Result = true;
                                    #region log
                                    BookingLogs bklog = new BookingLogs();
                                    bklog.BookingId = objbooking.Id;
                                    bklog.CurrentStatus = Enum.GetName(typeof(BookingRequestStatus), 4).ToUpper();
                                    bklog.LastStatus = Enum.GetName(typeof(BookingRequestStatus), 6).ToUpper();
                                    bklog.HotelId = objbooking.HotelId;
                                    bklog.UserId = Convert.ToInt32(objbooking.CreatorId);
                                    bklog.ModifyDate = System.DateTime.Now;
                                
                                    #endregion

                                    if (DataRepository.BookingLogsProvider.Insert(tm, bklog))
                                    {
                                        FileStream fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + "Log/SuccessHotelbackend.txt", FileMode.OpenOrCreate, FileAccess.Write);
                                        StreamWriter m_streamWriter = new StreamWriter(fs);
                                        m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
                                        m_streamWriter.WriteLine(" Move to frozen for booking # :" + objbooking.Id + " is done, time :" + System.DateTime.Now);
                                        m_streamWriter.Flush();
                                        m_streamWriter.Close();
                                    }
                                    else
                                    {
                                      //  tm.Rollback();
                                    }
                                }
                                else
                                {
                                    Result = false;
                                }
                                tm.Commit();
                            }
                            catch (Exception ex)
                            {
                                tm.Rollback();
                            }
                        }


                    }




                    #endregion

                }
            }
            catch (Exception ex)
            {
                FileStream fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + "Log/Hotelbackend-movetofrozenbooking.txt", FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter m_streamWriter = new StreamWriter(fs);
                m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
                m_streamWriter.WriteLine(ex.Message + "-- target :" + ex.TargetSite +"-- innnerexception:"+ex.InnerException+ " -- source: " + ex.Source + "--- time :" + System.DateTime.Now);
                m_streamWriter.Flush();
                m_streamWriter.Close();
            }

        }
        #endregion
        #region Move To Frozen - Request
        public void movetofrozenrequest()
        {

            try
            {
                string where = " (requeststatus=7 and booktype = 1 and IsComissionDone=1) or (requeststatus=2 and booktype = 1 )   ";
                string orderby = "";
                int totalcount = 0;
                VList<Viewbookingrequest> Vlistreq = new VList<Viewbookingrequest>();
                Vlistreq = DataRepository.ViewbookingrequestProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount);



                ViewBooking_Hotel objview = new ViewBooking_Hotel();

                foreach (Viewbookingrequest vl in Vlistreq)
                {



                    #region Freeze row

                    DateTime currentdate = new DateTime(Convert.ToInt32(System.DateTime.Now.Year), Convert.ToInt32(System.DateTime.Now.Month), Convert.ToInt32(System.DateTime.Now.Day));

                    if (Convert.ToInt32(currentdate.Day) >= 8)
                    {

                        DateTime dtmeetingdt = new DateTime(Convert.ToInt32(vl.DepartureDate.Value.ToShortDateString().ToString().Split('/')[2]), Convert.ToInt32(vl.DepartureDate.Value.ToString().Split('/')[0]), Convert.ToInt32(vl.DepartureDate.Value.ToString().Split('/')[1]));

                        if (dtmeetingdt.Month < currentdate.Month)
                        {
                            TransactionManager tm = null;
                            bool Result = false;
                            Booking objbooking = DataRepository.BookingProvider.GetById(vl.Id);
                            try
                            {
                                tm = DataRepository.Provider.CreateTransaction();
                                tm.BeginTransaction();
                                objbooking.RequestStatus = (int)BookingRequestStatus.Frozen;
                                //Audit trail maintain
                                Guid gid = Guid.NewGuid();
                                Booking old = objbooking.GetOriginalEntity();
                                old.Id = objbooking.Id;



                                TrailManager.LogAuditTrail<Booking>(tm, objbooking, old, AuditAction.U, Convert.ToInt64(1), Convert.ToInt32(PageType.OnlineBookingStatus), objbooking.TableName, Convert.ToString(objbooking.HotelId));
                                //Audit trail maintain
                                if (DataRepository.BookingProvider.Update(tm, objbooking))
                                {
                                    Result = true;
                                    #region log
                                    BookingLogs bklog = new BookingLogs();
                                    bklog.BookingId = objbooking.Id;
                                    bklog.CurrentStatus = Enum.GetName(typeof(BookingRequestStatus), 4).ToUpper();
                                    bklog.LastStatus = Enum.GetName(typeof(BookingRequestStatus), 7).ToUpper();
                                    bklog.HotelId = objbooking.HotelId;
                                    bklog.UserId = Convert.ToInt32(objbooking.CreatorId);
                                    bklog.ModifyDate = System.DateTime.Now;
                                   //DataRepository.BookingLogsProvider.Insert(bklog);
                                    #endregion

                                    if (DataRepository.BookingLogsProvider.Insert(tm, bklog))
                                    {
                                        FileStream fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + "Log/SuccessHotelbackend.txt", FileMode.OpenOrCreate, FileAccess.Write);
                                        StreamWriter m_streamWriter = new StreamWriter(fs);
                                        m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
                                        m_streamWriter.WriteLine(" Move to frozen for request # :" + objbooking.Id + " is done, time :" + System.DateTime.Now);
                                        m_streamWriter.Flush();
                                        m_streamWriter.Close();
                                    }
                                    else
                                    {
                                       // tm.Rollback();
                                    }
                                }
                                else
                                {
                                    Result = false;
                                }
                                tm.Commit();
                            }
                            catch (Exception ex)
                            {
                                tm.Rollback();
                            }
                        }


                    }




                    #endregion

                }
            }
            catch (Exception ex)
            {
                FileStream fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + "Log/Hotelbackend-movetofrozenrequest.txt", FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter m_streamWriter = new StreamWriter(fs);
                m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
                 m_streamWriter.WriteLine(ex.Message + "-- target :" + ex.TargetSite +"-- innnerexception:"+ex.InnerException+ " -- source: " + ex.Source + "--- time :" + System.DateTime.Now);
                m_streamWriter.Flush();
                m_streamWriter.Close();
            }

        }
        #endregion
        #endregion


    }
}
