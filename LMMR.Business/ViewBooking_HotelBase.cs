﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;
using System.Data;
using System.IO;
using System.Configuration;
using System.Web;

namespace LMMR.Business
{


    public class Viewdetails
    {
        public string Itemname { get; set; }
        public decimal totalprice { get; set; }


    }


    public class ViewMeetingroom
    {
        public string MeetingroomName { get; set; }
        public string MeetingRoomType { get; set; }
        public string MeetingRoomDesc { get; set; }
        public string MeetingRoomConfig { get; set; }
        public decimal MeetingRoomPrice { get; set; }
        public string MeetingRoomST { get; set; }
        public string MeetingRoomET { get; set; }
        public string MeetingRoomQTY { get; set; }
        public decimal MeetingRoomTotal { get; set; }


    }
    public abstract class ViewBooking_HotelBase
    {
        #region Variables

        Hotel objHotel;
        TList<Booking> objBooking;
        TList<BookedMeetingRoom> objBookingMR;
        TList<BookedBedRoom> objBookingBR;
        decimal Finalprice;
        EmailConfigManager em = new EmailConfigManager();
        #endregion
        #region Properties
        public decimal _finalprice
        {
            get { return Finalprice; }
            set { Finalprice = value; }

        }
        public Hotel _objHotel
        {
            get;
            set;
        }

        public Users _user
        {
            get;
            set;
        }
        public MeetingRoom _meetingroom
        {
            get;
            set;
        }

        public TList<Booking> _objBooking
        {
            get;
            set;
        }

        public TList<BookedMeetingRoom> _objBookingMR
        {
            get;
            set;
        }
        public TList<BookedBedRoom> _objBookingBR
        {
            get;
            set;
        }
        public TList<BuildMeetingConfigure> _objBuildMeetingConfig
        {
            get;
            set;
        }


        public TList<BuildPackageConfigure> _objBuildPackageConfig
        {
            get;
            set;
        }
        #endregion


        public ViewBooking_HotelBase()
        {


        }


        public Hotel GetbyHotelID(int hotelID)
        {
            _objHotel = DataRepository.HotelProvider.GetById(hotelID);
            return _objHotel;
        }
        //- to fetch the meetingroom for a particular bookingid
        public MeetingRoom Getmeetingrooms(Int64 bookingid)
        {

            _meetingroom = DataRepository.MeetingRoomProvider.GetById(bookingid);
            return _meetingroom;


        }


        //--- update request status
        public bool updaterequestStatus(Int64 bookingID)
        {
            TransactionManager tm = null;
            bool Result = false;
            Booking objbooking = DataRepository.BookingProvider.GetById(bookingID);
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                objbooking.RequestStatus = (int)BookingRequestStatus.Processed;
                //Audit trail maintain
                Guid gid = Guid.NewGuid();
                Booking old = objbooking.GetOriginalEntity();
                old.Id = objbooking.Id;

                #region log
                BookingLogs bklog = new BookingLogs();
                bklog.BookingId = objbooking.Id;
                bklog.CurrentStatus = Enum.GetName(typeof(BookingRequestStatus), 6).ToUpper();
                bklog.LastStatus = "NEW";
                bklog.HotelId = objbooking.HotelId;
                bklog.UserId = Convert.ToInt32(objbooking.CreatorId);
                bklog.ModifyDate = System.DateTime.Now;
                #endregion
                DataRepository.BookingLogsProvider.Insert(tm, bklog);
                TrailManager.LogAuditTrail<Booking>(tm, objbooking, old, AuditAction.U, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.OnlineBookingStatus), objbooking.TableName, Convert.ToString(objbooking.HotelId));
                //Audit trail maintain
                if (DataRepository.BookingProvider.Update(tm, objbooking))
                {
                    Result = true;
                }
                else
                {
                    Result = false;
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            if (Result)
            {
                if (objbooking.BookType == 0)
                {
                    EmailConfig eConfig = em.GetByName("Booking status change");
                    if (eConfig.IsActive)
                    {
                        string bodymsg = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
                        SendMails objSendmail = new SendMails();
                        objSendmail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
                        Users objUser = GetUser(objbooking.Id);
                        objSendmail.ToEmail = objUser.EmailId;
                        Hotel objhotel = DataRepository.HotelProvider.GetById(objbooking.HotelId);
                        objSendmail.Bcc = ConfigurationManager.AppSettings["WatchEMailID"];
                        if (objbooking.RequestStatus == (int)BookingRequestStatus.Processed)
                        {
                            //string bodymsg = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/BookingStatusChangeTemplet.html");
                            EmailConfigMapping emap2 = new EmailConfigMapping();
                            emap2 = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                            bodymsg = bodymsg.Replace("@CONTENTS", emap2.EmailContents);
                            EmailValueCollection objEmailValues = new EmailValueCollection();
                            objSendmail.Subject = "Booking Processed ID:" + objbooking.Id.ToString();
                            foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForUpdateRequestStatus(objUser.EmailId, objbooking.Id.ToString(), BookingRequestStatus.Processed.ToString(), "Last Minute Meeting Room Admin", objUser.FirstName + " " + objUser.LastName, objhotel.Name, Convert.ToString(objbooking.ArrivalDate)))
                            {
                                bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                            }
                            objSendmail.Body = bodymsg;
                            objSendmail.SendMail();
                        }
                    }
                }


            }
            return Result;
        }
        //--- update checkcoomsionm
        public bool updatecheckComm(Booking objbooking)
        {
            TransactionManager tm = null;
            bool Result = false;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                //Audit trail maintain
                Guid gid = Guid.NewGuid();
                Booking old = objbooking.GetOriginalEntity();
                old.Id = objbooking.Id;
                TrailManager.LogAuditTrail<Booking>(tm, objbooking, old, AuditAction.U, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.PendingRequestStatus), objbooking.TableName, Convert.ToString(objbooking.HotelId));
                //Audit trail maintain
                if (DataRepository.BookingProvider.Update(tm, objbooking))
                {
                    #region log
                    BookingLogs bklog = new BookingLogs();
                    bklog.BookingId = objbooking.Id;
                    bklog.CurrentStatus = Enum.GetName(typeof(BookingRequestStatus), objbooking.RequestStatus).ToUpper();
                    bklog.LastStatus = Enum.GetName(typeof(BookingRequestStatus), old.RequestStatus).ToUpper(); ;
                    bklog.HotelId = objbooking.HotelId;
                    bklog.UserId = Convert.ToInt32(objbooking.CreatorId);
                    bklog.ModifyDate = System.DateTime.Now;
                    DataRepository.BookingLogsProvider.Insert(tm, bklog);
                    #endregion

                    if (objbooking.RequestStatus == (int)BookingRequestStatus.Definite)
                    {
                        #region statistics
                        Viewstatistics objview = new Viewstatistics();

                        Statistics ST = new Statistics();
                        ST.BookingId = objbooking.Id;
                        ST.HotelId = objbooking.HotelId;
                        ST.SensitiveRequest = 1;
                        ST.StatDate = System.DateTime.Now;

                        objview.InsertVisitwithBookingid(ST);
                        #endregion
                    }
                    Result = true;
                }
                else
                {
                    Result = false;
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            if (Result)
            {
                SendMails objSendmail = new SendMails();
                objSendmail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
                Users objUser = GetUser(objbooking.Id);
                objSendmail.ToEmail = objUser.EmailId;
                Hotel objhotel = DataRepository.HotelProvider.GetById(objbooking.HotelId);
                objSendmail.Bcc = ConfigurationManager.AppSettings["WatchEMailID"];


                if (objbooking.BookType == 1 || objbooking.BookType == 2) //  if (objbooking.BookType == 1)
                {
                    string bodymsg = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
                    //---- for decline
                    if (objbooking.RequestStatus == (int)BookingRequestStatus.Cancel)
                    {
                        EmailConfig eConfig = em.GetByName("Request status change (cancel)");
                        if (eConfig.IsActive)
                        {
                            EmailConfigMapping emap2 = new EmailConfigMapping();
                            emap2 = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                            bodymsg = bodymsg.Replace("@CONTENTS", emap2.EmailContents);
                            //string bodymsg = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/RequestStatusChangeTemplet.html");
                            EmailValueCollection objEmailValues = new EmailValueCollection();
                            objSendmail.Subject = " Request-ID:" + objbooking.Id.ToString() + " " + BookingRequestStatus.Cancel.ToString();
                            foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForUpdateRequestStatus(objUser.EmailId, objbooking.Id.ToString(), BookingRequestStatus.Cancel.ToString(), "Last Minute Meeting Room Admin", objUser.FirstName + " " + objUser.LastName, objhotel.Name, Convert.ToString(objbooking.ArrivalDate)))
                            {
                                bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                            }
                            objSendmail.Body = bodymsg;
                            objSendmail.SendMail();
                        }
                    }
                    //---- for tentative
                    if (objbooking.RequestStatus == (int)BookingRequestStatus.Tentative)
                    {
                        EmailConfig eConfig = em.GetByName("Request status change (tentative)");
                        if (eConfig.IsActive)
                        {
                            EmailConfigMapping emap2 = new EmailConfigMapping();
                            emap2 = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                            bodymsg = bodymsg.Replace("@CONTENTS", emap2.EmailContents);
                            //string bodymsg = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/RequestStatusChangeTENTemplet.html");
                            EmailValueCollection objEmailValues = new EmailValueCollection();
                            objSendmail.Subject = " Request-ID:" + objbooking.Id.ToString() + " " + BookingRequestStatus.Tentative.ToString();
                            foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForUpdateRequestStatus(objUser.EmailId, objbooking.Id.ToString(), BookingRequestStatus.Tentative.ToString(), "Last Minute Meeting Room Admin", objUser.FirstName + " " + objUser.LastName, objhotel.Name, Convert.ToString(objbooking.ArrivalDate)))
                            {
                                bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                            }
                            objSendmail.Body = bodymsg;
                            objSendmail.SendMail();
                        }
                    }

                }
            }
            return Result;
        }

        public bool updatecheckComm(Booking objbooking,long changebyuserid)
        {
            TransactionManager tm = null;
            bool Result = false;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                //Audit trail maintain
                Guid gid = Guid.NewGuid();
                Booking old = objbooking.GetOriginalEntity();
                old.Id = objbooking.Id;
                TrailManager.LogAuditTrail<Booking>(tm, objbooking, old, AuditAction.U, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.PendingRequestStatus), objbooking.TableName, Convert.ToString(objbooking.HotelId));
                //Audit trail maintain
                if (DataRepository.BookingProvider.Update(tm, objbooking))
                {
                    #region log
                    BookingLogs bklog = new BookingLogs();
                    bklog.BookingId = objbooking.Id;
                    bklog.CurrentStatus = Enum.GetName(typeof(BookingRequestStatus), objbooking.RequestStatus).ToUpper();
                    bklog.LastStatus = Enum.GetName(typeof(BookingRequestStatus), old.RequestStatus).ToUpper(); ;
                    bklog.HotelId = objbooking.HotelId;
                    bklog.UserId = Convert.ToInt32(changebyuserid);
                    bklog.ModifyDate = System.DateTime.Now;
                    DataRepository.BookingLogsProvider.Insert(tm, bklog);
                    #endregion

                    if (objbooking.RequestStatus == (int)BookingRequestStatus.Definite)
                    {
                        #region statistics
                        Viewstatistics objview = new Viewstatistics();

                        Statistics ST = new Statistics();
                        ST.BookingId = objbooking.Id;
                        ST.HotelId = objbooking.HotelId;
                        ST.SensitiveRequest = 1;
                        ST.StatDate = System.DateTime.Now;

                        objview.InsertVisitwithBookingid(ST);
                        #endregion
                    }
                    Result = true;
                }
                else
                {
                    Result = false;
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            if (Result)
            {
                SendMails objSendmail = new SendMails();
                objSendmail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
                Users objUser = GetUser(objbooking.Id);
                objSendmail.ToEmail = objUser.EmailId;
                Hotel objhotel = DataRepository.HotelProvider.GetById(objbooking.HotelId);
                objSendmail.Bcc = ConfigurationManager.AppSettings["WatchEMailID"];


                if (objbooking.BookType == 1 || objbooking.BookType == 2) //  if (objbooking.BookType == 1)
                {
                    string bodymsg = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
                    //---- for decline
                    if (objbooking.RequestStatus == (int)BookingRequestStatus.Cancel)
                    {
                        EmailConfig eConfig = em.GetByName("Request status change (cancel)");
                        if (eConfig.IsActive)
                        {
                            EmailConfigMapping emap2 = new EmailConfigMapping();
                            emap2 = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                            bodymsg = bodymsg.Replace("@CONTENTS", emap2.EmailContents);
                            //string bodymsg = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/RequestStatusChangeTemplet.html");
                            EmailValueCollection objEmailValues = new EmailValueCollection();
                            objSendmail.Subject = " Request-ID:" + objbooking.Id.ToString() + " " + BookingRequestStatus.Cancel.ToString();
                            foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForUpdateRequestStatus(objUser.EmailId, objbooking.Id.ToString(), BookingRequestStatus.Cancel.ToString(), "Last Minute Meeting Room Admin", objUser.FirstName + " " + objUser.LastName, objhotel.Name, Convert.ToString(objbooking.ArrivalDate)))
                            {
                                bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                            }
                            objSendmail.Body = bodymsg;
                            objSendmail.SendMail();
                        }
                    }
                    //---- for tentative
                    if (objbooking.RequestStatus == (int)BookingRequestStatus.Tentative)
                    {
                        EmailConfig eConfig = em.GetByName("Request status change (tentative)");
                        if (eConfig.IsActive)
                        {
                            EmailConfigMapping emap2 = new EmailConfigMapping();
                            emap2 = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                            bodymsg = bodymsg.Replace("@CONTENTS", emap2.EmailContents);
                            //string bodymsg = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/RequestStatusChangeTENTemplet.html");
                            EmailValueCollection objEmailValues = new EmailValueCollection();
                            objSendmail.Subject = " Request-ID:" + objbooking.Id.ToString() + " " + BookingRequestStatus.Tentative.ToString();
                            foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForUpdateRequestStatus(objUser.EmailId, objbooking.Id.ToString(), BookingRequestStatus.Tentative.ToString(), "Last Minute Meeting Room Admin", objUser.FirstName + " " + objUser.LastName, objhotel.Name, Convert.ToString(objbooking.ArrivalDate)))
                            {
                                bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                            }
                            objSendmail.Body = bodymsg;
                            objSendmail.SendMail();
                        }
                    }

                }
            }
            return Result;
        }

        //--- Update Booking details while transfer booking
        public bool UpdateTransferBooking(Booking objbooking)
        {
            TransactionManager tm = null;
            bool Result = false;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                //Audit trail maintain
                Guid gid = Guid.NewGuid();
                Booking old = objbooking.GetOriginalEntity();
                old.Id = objbooking.Id;
                TrailManager.LogAuditTrail<Booking>(tm, objbooking, old, AuditAction.U, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.PendingRequestStatus), objbooking.TableName, Convert.ToString(objbooking.HotelId));
                //Audit trail maintain
                if (DataRepository.BookingProvider.Update(tm, objbooking))
                {
                    Result = true;
                }
                else
                {
                    Result = false;
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return Result;
        }


        //-- fetch the final price
        public decimal gefinalprice(Int64 bookingid)
        {
            Booking blist = DataRepository.BookingProvider.GetById(bookingid);
            _finalprice = (decimal)blist.FinalTotalPrice;
            return _finalprice;


        }

        //- to fetch the user details for a particular bookingid
        public Users GetUser(Int64 bookingid)
        {

            Booking objbook = DataRepository.BookingProvider.GetById(bookingid);
            _user = DataRepository.UsersProvider.GetByUserId(objbook.CreatorId);

            DataRepository.UsersProvider.DeepLoad(_user, true, DeepLoadType.IncludeChildren, typeof(UserDetails));


            return _user;

        }

        //- to fetch the Booking details for a particular hotel
        public TList<Booking> Getbooking()
        {
            _objBooking = DataRepository.BookingProvider.GetByHotelId(_objHotel.Id);
            return _objBooking;

        }

        //---- to bind the grid
        public VList<Viewbookingrequest> Bindgrid(string where, string orderby)
        {
            int totalcount = 0;
            VList<Viewbookingrequest> Vlistreq = new VList<Viewbookingrequest>();
            Vlistreq = DataRepository.ViewbookingrequestProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount);

            return Vlistreq;


        }

        //Get Data of hotel list For check availability we service
        public VList<ViewWsCheckAvailability> GetWsCheckAvailability(string where, string orderby)
        {
            int totalcount = 0;
            VList<ViewWsCheckAvailability> vCheck = new VList<ViewWsCheckAvailability>();
            vCheck = DataRepository.ViewWsCheckAvailabilityProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount);
            return vCheck;
        }

        //---- to details of particular booking
        public VList<Viewbookingrequest> getdetails(string where, string orderby)
        {
            int totalcount = 0;
            VList<Viewbookingrequest> Vlistreq = new VList<Viewbookingrequest>();
            Vlistreq = DataRepository.ViewbookingrequestProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount);

            return Vlistreq;


        }


        public TList<BookedMeetingRoom> getbookedmeetingroom(Int64 bookingid)
        {
            _objBookingMR = DataRepository.BookedMeetingRoomProvider.GetByBookingId(bookingid);
            return _objBookingMR;
        }

        //--- get user details
        public TList<UserDetails> getuserdetails(Int64 userid)
        {
            TList<UserDetails> objUserDetails = DataRepository.UserDetailsProvider.GetByUserId(userid);
            return objUserDetails;
        }
        public TList<BookedBedRoom> getbookedBedroom(Int64 bookingid)
        {
            _objBookingBR = DataRepository.BookedBedRoomProvider.GetByBookingId(bookingid);
            DataRepository.BookedBedRoomProvider.DeepLoad(_objBookingBR, true, DeepLoadType.IncludeChildren, typeof(BedRoom));

            return _objBookingBR;

        }


        //------ fetch the list of extras for a booking id
        public TList<BuildMeetingConfigure> getMeetingroomConfig(Int64 bookingid)
        {

            _objBuildMeetingConfig = DataRepository.BuildMeetingConfigureProvider.GetByBookingId(bookingid);


            return _objBuildMeetingConfig;

        }

        public TList<BuildPackageConfigure> getPackageConfig(Int64 bookingid)
        {

            _objBuildPackageConfig = DataRepository.BuildPackageConfigureProvider.GetByBookingId(bookingid);


            return _objBuildPackageConfig;

        }
        public int countbooking(int hotelid)
        {
            int intCount = 0;
            int Total = 0;
            string whereclause = "HotelId=" + hotelid + " and requeststatus=1 and booktype = 0 ";
            TList<Booking> ObjBooking = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
            intCount = ObjBooking.Count;
            return intCount;
        }
        public int countProcessedbooking(int hotelid)
        {
            int intCount = 0;
            int Total = 0;
            string whereclause = "HotelId=" + hotelid + " and requeststatus=6 and booktype = 0 ";
            TList<Booking> ObjBooking = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
            intCount = ObjBooking.Count;
            return intCount;
        }
        public int countrequest(int hotelid)
        {
            int intCount = 0;
            int Total = 0;
            string whereclause = "HotelId=" + hotelid + " and requeststatus=1 and booktype in ( 1,2) "; // booktype=1
            TList<Booking> ObjBooking = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
            intCount = ObjBooking.Count;
            return intCount;
        }
        //--- get booking

        public Booking getparticularbookingdetails(Int64 bookingid)
        {
            Booking objbooking = DataRepository.BookingProvider.GetById(bookingid);
            return objbooking;


        }
        public List<ViewMeetingroom> viewMR(Int64 bookingid)
        {
            List<ViewMeetingroom> vmr = new List<ViewMeetingroom>();
            TList<BookedMeetingRoom> obj_bookedmr = getbookedmeetingroom(bookingid);
            Type[] ty = new Type[] { typeof(MeetingRoom), typeof(MeetingRoomConfig) };
            DataRepository.BookedMeetingRoomProvider.DeepLoad(obj_bookedmr, true, DeepLoadType.IncludeChildren, ty);
            getPackageDetails(1);
            ViewMeetingroom objvmr = new ViewMeetingroom();
            foreach (BookedMeetingRoom bmr in obj_bookedmr)
            {
                objvmr.MeetingRoomST = bmr.StartTime;
                objvmr.MeetingRoomET = bmr.EndTime;
                objvmr.MeetingRoomQTY = Convert.ToString(bmr.NoofParticipants);
                objvmr.MeetingRoomTotal = Convert.ToDecimal(bmr.TotalPrice);
                objvmr.MeetingRoomDesc = Convert.ToString(bmr.Id);//bmr.MeetingRoomIdSource.
                objvmr.MeetingRoomConfig = bmr.MeetingRoomConfigIdSource.MinCapacity + "-" + bmr.MeetingRoomConfigIdSource.MaxCapicity;
                objvmr.MeetingRoomType = Enum.GetName(typeof(RoomShape), bmr.MeetingRoomConfigIdSource.RoomShapeId);
                objvmr.MeetingRoomPrice = Convert.ToDecimal(bmr.MeetingRoomIdSource.FulldayPrice);
                objvmr.MeetingroomName = bmr.MeetingRoomIdSource.Name;
                vmr.Add(objvmr);
            }


            return vmr;

        }

        public TList<BuildPackageConfigure> getPackageDetails(Int64 MeetingroomID)
        {
            TList<BuildPackageConfigure> objBuildPackageConfigure = DataRepository.BuildPackageConfigureProvider.GetByBookedMeetingRoomId(MeetingroomID);

            DataRepository.BuildPackageConfigureProvider.DeepLoad(objBuildPackageConfigure, true, DeepLoadType.IncludeChildren, typeof(PackageMaster));

            // TList<BuildPackageConfigureDesc> objDescription = DataRepository.BuildPackageConfigureDescProvider.GetByBuildPackageConfigId(objBuildPackageConfigure[0].Id);
            //Type[] ty = new Type[] { typeof(PackageMaster), typeof(BuildPackageConfigureDesc), typeof(PackageItems) };

            //DataRepository.BuildPackageConfigureProvider.DeepLoad(objBuildPackageConfigure, true, DeepLoadType.IncludeChildren, ty);

            return objBuildPackageConfigure;
        }

        public TList<BuildMeetingConfigure> getextra(Int64 MeetinroomID)
        {

            TList<BuildMeetingConfigure> ob = DataRepository.BuildMeetingConfigureProvider.GetByBookedMeetingRoomId(MeetinroomID);

            DataRepository.BuildMeetingConfigureProvider.DeepLoad(ob, true, DeepLoadType.IncludeChildren, typeof(PackageItems));


            return ob;

        }

        public TList<BuildPackageConfigureDesc> getpackagedetails(Int64 Packageid)
        {
            TList<BuildPackageConfigureDesc> obBuildpackageConfigDesc = DataRepository.BuildPackageConfigureDescProvider.GetByBuildPackageConfigId(Packageid);

            DataRepository.BuildPackageConfigureDescProvider.DeepLoad(obBuildpackageConfigDesc, true, DeepLoadType.IncludeChildren, typeof(PackageItems));


            return obBuildpackageConfigDesc;

        }

        public List<Viewdetails> ViewDetails(Int64 bookingid)
        {

            Booking objbook = DataRepository.BookingProvider.GetById(bookingid);
            List<Viewdetails> objViewDetails = new List<Viewdetails>();


            //if Is Package true
            if (objbook.IsPackageSelected)
            {
                Viewdetails v = new Viewdetails();

                TList<BuildPackageConfigure> objpackage = getPackageConfig(bookingid);
                DataRepository.BuildPackageConfigureProvider.DeepLoad(objpackage, true, DeepLoadType.IncludeChildren, typeof(PackageMaster));
                foreach (BuildPackageConfigure bpc in objpackage)
                {
                    v.Itemname = Convert.ToString(bpc.PackageItemIdSource.PackageName);
                    v.totalprice = Convert.ToDecimal(bpc.TotalPrice);
                    objViewDetails.Add(v);
                }
                //--- for extras
                TList<BuildMeetingConfigure> objmeeting = getMeetingroomConfig(bookingid);
                DataRepository.BuildMeetingConfigureProvider.DeepLoad(objmeeting, true, DeepLoadType.IncludeChildren, typeof(PackageItems));
                foreach (BuildMeetingConfigure bmc in objmeeting)
                {
                    v.Itemname = Convert.ToString(bmc.PackageIdSource.ItemName);
                    v.totalprice = Convert.ToDecimal(bmc.TotalPrice);
                    objViewDetails.Add(v);
                }
            }
            else
            {
                Viewdetails v = new Viewdetails();

                TList<BuildMeetingConfigure> objmeeting = getMeetingroomConfig(bookingid);
                DataRepository.BuildMeetingConfigureProvider.DeepLoad(objmeeting, true, DeepLoadType.IncludeChildren, typeof(PackageItems));
                foreach (BuildMeetingConfigure bmc in objmeeting)
                {
                    v.Itemname = Convert.ToString(bmc.PackageIdSource.ItemName);
                    v.totalprice = Convert.ToDecimal(bmc.TotalPrice);
                    objViewDetails.Add(v);
                }


            }

            return objViewDetails;


        }

        public Createbooking getxml(Int64 bookingid)
        {
            Createbooking objBooking = null;
            Booking obj = DataRepository.BookingProvider.GetById(bookingid);
            objBooking = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), obj.BookingXml);
            return objBooking;

        }
        public CreateRequest Reqgetxml(Int64 bookingid)
        {
            CreateRequest objBooking = null;
            Booking obj = DataRepository.BookingProvider.GetById(bookingid);
            objBooking = (CreateRequest)TrailManager.XmlDeserialize(typeof(CreateRequest), obj.BookingXml);
            return objBooking;

        }

        //-------------------------------------new(24/5/12)----------------------------------
        public TList<Hotel> getAllHotels(VList<Viewbookingrequest> temp)
        {
            TList<Hotel> hotels = new TList<Hotel>();
            foreach (Viewbookingrequest record in temp)
            {
                hotels.Add(DataRepository.HotelProvider.GetById(record.HotelId));
            }
            return hotels;
        }

        public TList<City> getAllCities(VList<Viewbookingrequest> temp)
        {
            List<long> allCityIDs = new List<long>();
            TList<City> cities = new TList<City>();
            long cityId;
            foreach (Viewbookingrequest hotel in temp)
            {
                cityId = DataRepository.HotelProvider.GetById(hotel.HotelId).CityId;
                cities.Add(DataRepository.CityProvider.GetById(cityId));
            }
            return cities.FindAllDistinct("City");
        }

        public TList<MeetingRoom> getAllMeetingRooms()
        {
            TList<Hotel> allHotels = DataRepository.HotelProvider.GetAll();
            TList<MeetingRoom> meetingRooms = new TList<MeetingRoom>();
            foreach (Hotel hotel in allHotels)
            {
                TList<MeetingRoom> temp = DataRepository.MeetingRoomProvider.GetByHotelId(hotel.Id);
                meetingRooms.AddRange(temp);
            }
            return meetingRooms;
        }

        public string getCityName(string hoteId)
        {
            string cityName = string.Empty;
            Hotel objH = DataRepository.HotelProvider.GetById(Convert.ToInt64(hoteId));
            if (objH != null)
            {
                City ObjC = DataRepository.CityProvider.GetById(objH.CityId);
                if (ObjC != null)
                {
                    cityName = ObjC.City;
                }
            }

            return cityName;
        }

        public long getCityId(long hoteId)
        {
            long hotelId = Convert.ToInt64(hoteId);
            long cityId = DataRepository.HotelProvider.GetById(hotelId).CityId;
            return cityId;
        }

        public long getHotelCountryId(int hotelId)
        {
            return (DataRepository.HotelProvider.GetById(hotelId).CountryId);
        }

        public long getSpecialPolicyId(int hotelId)
        {
            foreach (PolicyHotelMapping temp in DataRepository.PolicyHotelMappingProvider.GetByHotelId(hotelId))
            {
                return Convert.ToInt64(temp.PolicyId);
            }
            return 0;
        }

        public CancelationPolicy getPolicy(long specialPolicyId)
        {
            return (DataRepository.CancelationPolicyProvider.GetById(specialPolicyId));
        }

        public CancelationPolicy getDefaultPolicy(long countryId)
        {
            foreach (CancelationPolicy policy in DataRepository.CancelationPolicyProvider.GetByCountryId(countryId))
            {
                if (policy.DefaultCountry == true)
                {
                    return policy;
                }
            }
            return null;
        }
        #region Normal Cancel
        public void CancelBooking(long bookingId)
        {
            Booking bk = DataRepository.BookingProvider.GetById(bookingId);
            bk.RequestStatus = (int)BookingRequestStatus.Cancel;
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                DataRepository.BookingProvider.Update(bk);
                Createbooking sb = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), bk.BookingXml);
                Booking objBooking = new Booking();
                objBooking.ArrivalDate = sb.ArivalDate;
                objBooking.DepartureDate = sb.DepartureDate;
                objBooking.CreatorId = sb.CurrentUserId;
                objBooking.Duration = sb.Duration;
                objBooking.HotelId = sb.HotelID;
                objBooking.MainMeetingRoomId = sb.MeetingroomList[0].MRId;
                objBooking.BookingDate = DateTime.Now;
                objBooking.SpecialRequest = sb.SpecialRequest;
                objBooking.IsBedroom = sb.ManageAccomodationLst.Count > 0 ? false : true;
                objBooking.BookType = 0;
                objBooking.IsUserBookingProcessDone = true;
                objBooking.BedRoomTotalPrice = sb.AccomodationPriceTotal;
                objBooking.AgencyClientId = bk.AgencyClientId;
                objBooking.AgencyUserId = bk.AgencyUserId;
                objBooking.BookingXml = TrailManager.XmlSerialize(sb);
                objBooking.RequestStatus = (int)BookingRequestStatus.New;
                objBooking.BookType = 0;
                objBooking.FinalTotalPrice = sb.TotalBookingPrice;
                #region log
                BookingLogs bklog = new BookingLogs();
                bklog.BookingId = bk.Id;
                bklog.CurrentStatus = Enum.GetName(typeof(BookingRequestStatus), 2).ToUpper();
                bklog.LastStatus = "NEW";
                bklog.HotelId = bk.HotelId;
                bklog.UserId = Convert.ToInt32(bk.CreatorId);
                bklog.ModifyDate = System.DateTime.Now;
                DataRepository.BookingLogsProvider.Insert(tm, bklog);
                #endregion

                #region START Booking cancel by user
                try
                {
                    EmailConfig eConfig = em.GetByName("Booking cancel by user");
                    if (eConfig.IsActive)
                    {
                        string bodymsg = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
                        EmailConfigMapping emap2 = new EmailConfigMapping();
                        emap2 = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                        bodymsg = bodymsg.Replace("@CONTENTS", emap2.EmailContents);

                        SendMails objSendmail = new SendMails();
                        objSendmail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
                        Users objUser = GetUser(bk.Id);
                        objSendmail.ToEmail = objUser.EmailId;
                        Hotel objhotel = DataRepository.HotelProvider.GetById(bk.HotelId);
                        objSendmail.Bcc = ConfigurationManager.AppSettings["WatchEMailID"];
                        //Added by Manas : whhile operator changes status
                        if (HttpContext.Current.Session["CurrentOperator"] != null)
                        {
                            objSendmail.Bcc = objhotel.Email;
                        }
                        //Added by Manas : whhile operator changes status
                        EmailValueCollection objEmailValues = new EmailValueCollection();
                        objSendmail.Subject = "Booking cancelled by User, Booking id " + bk.Id.ToString();
                        foreach (KeyValuePair<string, string> strKey in objEmailValues.CancelBookingToHotelUSer(Convert.ToString(bk.Id), objUser.FirstName + " " + objUser.LastName, Convert.ToString(System.DateTime.Now.ToString("dd/MM/yyyy"))))
                        {
                            bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                        }
                        objSendmail.Body = bodymsg;
                        objSendmail.SendMail();
                    }
                }
                catch
                {
                }
                #endregion

                foreach (BookedMR bmr in sb.MeetingroomList)
                {
                    foreach (BookedMrConfig bmrconfig in bmr.MrDetails)
                    {
                        BookedMeetingRoom objbmr = new BookedMeetingRoom();
                        objbmr.BookingId = objBooking.Id;
                        objbmr.MeetingRoomId = bmr.MRId;
                        objbmr.MeetingRoomConfigId = bmr.MrConfigId;
                        objbmr.NoofParticipants = bmrconfig.NoOfParticepant;
                        objbmr.StartTime = bmrconfig.FromTime;
                        objbmr.EndTime = bmrconfig.ToTime;
                        objbmr.MeetingDate = sb.ArivalDate.AddDays(bmrconfig.SelectedDay - 1);
                        objbmr.MeetingDay = bmrconfig.SelectedDay;
                        //Save Details of Packages according to meetingroom.

                        string whereClause = AvailabilityColumn.AvailabilityDate + " between '" + sb.ArivalDate + "' and '" + sb.DepartureDate + "' and " + AvailabilityColumn.HotelId + "=" + sb.HotelID;
                        int count = 0;
                        TList<Availability> avail = DataRepository.AvailabilityProvider.GetPaged(tm, whereClause, string.Empty, 0, int.MaxValue, out count);
                        foreach (Availability a in avail)
                        {
                            int count2 = 0;
                            string where2 = ViewSelectAvailabilityColumn.HotelId + "=" + sb.HotelID + " and " + ViewSelectAvailabilityColumn.RoomType + "=0 and " + ViewSelectAvailabilityColumn.AvailabilityDate + "='" + a.AvailabilityDate + "'";
                            VList<ViewSelectAvailability> availRoom2 = DataRepository.ViewSelectAvailabilityProvider.GetPaged(tm, where2, string.Empty, 0, int.MaxValue, out count2);
                            int Count2 = availRoom2.Where(c => (c.AfternoonStatus == (int)AvailabilityStatus.BOOKED || c.AfternoonStatus == (int)AvailabilityStatus.SOLD) && (c.MorningStatus == (int)AvailabilityStatus.BOOKED || c.MorningStatus == (int)AvailabilityStatus.SOLD)).Count();
                            if (Count2 == availRoom2.Count && availRoom2.Count != 0)
                            {
                                foreach (ViewSelectAvailability vaor in availRoom2)
                                {
                                    AvailibilityOfRooms aor = (AvailibilityOfRooms)DataRepository.AvailibilityOfRoomsProvider.GetById(tm, vaor.AvailRoomId);
                                    if (bmrconfig.SelectedTime == 0 || bmrconfig.SelectedTime == 1)
                                    {
                                        aor.MorningStatus = (int)AvailabilityStatus.AVAILABLE;
                                    }
                                    if (bmrconfig.SelectedTime == 0 || bmrconfig.SelectedTime == 2)
                                    {
                                        aor.AfternoonStatus = (int)AvailabilityStatus.AVAILABLE;
                                    }
                                    DataRepository.AvailibilityOfRoomsProvider.Update(tm, aor);
                                }
                                a.FullPropertyStatus = (int)AvailabilityStatus.AVAILABLE;
                                a.FullProrertyStatusBr = (int)AvailabilityStatus.AVAILABLE;
                                DataRepository.AvailabilityProvider.Update(tm, a);
                            }
                            else
                            {
                                ListBase<ViewSelectAvailability> newLst = availRoom2.FindAll(c => ((c.AfternoonStatus == (int)AvailabilityStatus.BOOKED || c.AfternoonStatus == (int)AvailabilityStatus.SOLD) || (c.MorningStatus == (int)AvailabilityStatus.BOOKED || c.MorningStatus == (int)AvailabilityStatus.SOLD)) && c.RoomId == bmr.MRId);
                                foreach (ViewSelectAvailability vaor in newLst)
                                {
                                    AvailibilityOfRooms aor = (AvailibilityOfRooms)DataRepository.AvailibilityOfRoomsProvider.GetById(tm, vaor.AvailRoomId);
                                    if (bmrconfig.SelectedTime == 0 || bmrconfig.SelectedTime == 1)
                                    {
                                        aor.MorningStatus = (int)AvailabilityStatus.AVAILABLE;
                                    }
                                    if (bmrconfig.SelectedTime == 0 || bmrconfig.SelectedTime == 2)
                                    {
                                        aor.AfternoonStatus = (int)AvailabilityStatus.AVAILABLE;
                                    }
                                    DataRepository.AvailibilityOfRoomsProvider.Update(tm, aor);
                                }
                                //a.FullPropertyStatus = (int)AvailabilityStatus.AVAILABLE;
                                //a.FullProrertyStatusBr = (int)AvailabilityStatus.AVAILABLE;
                                //DataRepository.AvailabilityProvider.Update(tm, a);
                            }
                            //string where = AvailibilityOfRoomsColumn.AvailibilityIdForBedRoom + "=" + a.Id + " and " + AvailibilityOfRoomsColumn.RoomId + "=" + bmr.MRId + " and " + AvailibilityOfRoomsColumn.RoomType + "=0";
                            //TList<AvailibilityOfRooms> availRoom = DataRepository.AvailibilityOfRoomsProvider.GetPaged(tm, where, string.Empty, 0, int.MaxValue, out count2);
                            //foreach (AvailibilityOfRooms aor in availRoom)
                            //{
                            //    if (bmrconfig.SelectedTime == 0 || bmrconfig.SelectedTime == 1)
                            //    {
                            //        aor.MorningStatus = (int)AvailabilityStatus.AVAILABLE;
                            //    }
                            //    if (bmrconfig.SelectedTime == 0 || bmrconfig.SelectedTime == 2)
                            //    {
                            //        aor.AfternoonStatus = (int)AvailabilityStatus.AVAILABLE;
                            //    }
                            //    aor.IsReserved = false;
                            //    DataRepository.AvailibilityOfRoomsProvider.Update(tm, aor);
                            //}

                        }

                    }
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
        }
        #endregion

        #region Cancel By User
        public void CancelBooking(long bookingId,long UserId)
        {
            Booking bk = DataRepository.BookingProvider.GetById(bookingId);
            bk.RequestStatus = (int)BookingRequestStatus.Cancel;
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                DataRepository.BookingProvider.Update(bk);
                Createbooking sb = (Createbooking)TrailManager.XmlDeserialize(typeof(Createbooking), bk.BookingXml);
                Booking objBooking = new Booking();
                objBooking.ArrivalDate = sb.ArivalDate;
                objBooking.DepartureDate = sb.DepartureDate;
                objBooking.CreatorId = sb.CurrentUserId;
                objBooking.Duration = sb.Duration;
                objBooking.HotelId = sb.HotelID;
                objBooking.MainMeetingRoomId = sb.MeetingroomList[0].MRId;
                objBooking.BookingDate = DateTime.Now;
                objBooking.SpecialRequest = sb.SpecialRequest;
                objBooking.IsBedroom = sb.ManageAccomodationLst.Count > 0 ? false : true;
                objBooking.BookType = 0;
                objBooking.IsUserBookingProcessDone = true;
                objBooking.BedRoomTotalPrice = sb.AccomodationPriceTotal;
                objBooking.AgencyClientId = bk.AgencyClientId;
                objBooking.AgencyUserId = bk.AgencyUserId;
                objBooking.BookingXml = TrailManager.XmlSerialize(sb);
                objBooking.RequestStatus = (int)BookingRequestStatus.New;
                objBooking.BookType = 0;
                objBooking.FinalTotalPrice = sb.TotalBookingPrice;
                #region log
                BookingLogs bklog = new BookingLogs();
                bklog.BookingId = bk.Id;
                bklog.CurrentStatus = Enum.GetName(typeof(BookingRequestStatus), 2).ToUpper();
                bklog.LastStatus = "NEW";
                bklog.HotelId = bk.HotelId;
                bklog.UserId = Convert.ToInt32(UserId);
                bklog.ModifyDate = System.DateTime.Now;
                DataRepository.BookingLogsProvider.Insert(tm, bklog);
                #endregion

                foreach (BookedMR bmr in sb.MeetingroomList)
                {
                    foreach (BookedMrConfig bmrconfig in bmr.MrDetails)
                    {
                        BookedMeetingRoom objbmr = new BookedMeetingRoom();
                        objbmr.BookingId = objBooking.Id;
                        objbmr.MeetingRoomId = bmr.MRId;
                        objbmr.MeetingRoomConfigId = bmr.MrConfigId;
                        objbmr.NoofParticipants = bmrconfig.NoOfParticepant;
                        objbmr.StartTime = bmrconfig.FromTime;
                        objbmr.EndTime = bmrconfig.ToTime;
                        objbmr.MeetingDate = sb.ArivalDate.AddDays(bmrconfig.SelectedDay - 1);
                        objbmr.MeetingDay = bmrconfig.SelectedDay;
                        //Save Details of Packages according to meetingroom.

                        string whereClause = AvailabilityColumn.AvailabilityDate + " between '" + sb.ArivalDate + "' and '" + sb.DepartureDate + "' and " + AvailabilityColumn.HotelId + "=" + sb.HotelID;
                        int count = 0;
                        TList<Availability> avail = DataRepository.AvailabilityProvider.GetPaged(tm, whereClause, string.Empty, 0, int.MaxValue, out count);
                        foreach (Availability a in avail)
                        {
                            int count2 = 0;
                            string where2 = ViewSelectAvailabilityColumn.HotelId + "=" + sb.HotelID + " and " + ViewSelectAvailabilityColumn.RoomType + "=0 and " + ViewSelectAvailabilityColumn.AvailabilityDate + "='" + a.AvailabilityDate + "'";
                            VList<ViewSelectAvailability> availRoom2 = DataRepository.ViewSelectAvailabilityProvider.GetPaged(tm, where2, string.Empty, 0, int.MaxValue, out count2);
                            int Count2 = availRoom2.Where(c => (c.AfternoonStatus == (int)AvailabilityStatus.BOOKED || c.AfternoonStatus == (int)AvailabilityStatus.SOLD) && (c.MorningStatus == (int)AvailabilityStatus.BOOKED || c.MorningStatus == (int)AvailabilityStatus.SOLD)).Count();
                            if (Count2 == availRoom2.Count && availRoom2.Count != 0)
                            {
                                foreach (ViewSelectAvailability vaor in availRoom2)
                                {
                                    AvailibilityOfRooms aor = (AvailibilityOfRooms)DataRepository.AvailibilityOfRoomsProvider.GetById(tm, vaor.AvailRoomId);
                                    if (bmrconfig.SelectedTime == 0 || bmrconfig.SelectedTime == 1)
                                    {
                                        aor.MorningStatus = (int)AvailabilityStatus.AVAILABLE;
                                    }
                                    if (bmrconfig.SelectedTime == 0 || bmrconfig.SelectedTime == 2)
                                    {
                                        aor.AfternoonStatus = (int)AvailabilityStatus.AVAILABLE;
                                    }
                                    DataRepository.AvailibilityOfRoomsProvider.Update(tm, aor);
                                }
                                a.FullPropertyStatus = (int)AvailabilityStatus.AVAILABLE;
                                a.FullProrertyStatusBr = (int)AvailabilityStatus.AVAILABLE;
                                DataRepository.AvailabilityProvider.Update(tm, a);
                            }
                            else
                            {
                                ListBase<ViewSelectAvailability> newLst = availRoom2.FindAll(c => ((c.AfternoonStatus == (int)AvailabilityStatus.BOOKED || c.AfternoonStatus == (int)AvailabilityStatus.SOLD) || (c.MorningStatus == (int)AvailabilityStatus.BOOKED || c.MorningStatus == (int)AvailabilityStatus.SOLD)) && c.RoomId == bmr.MRId);
                                foreach (ViewSelectAvailability vaor in newLst)
                                {
                                    AvailibilityOfRooms aor = (AvailibilityOfRooms)DataRepository.AvailibilityOfRoomsProvider.GetById(tm, vaor.AvailRoomId);
                                    if (bmrconfig.SelectedTime == 0 || bmrconfig.SelectedTime == 1)
                                    {
                                        aor.MorningStatus = (int)AvailabilityStatus.AVAILABLE;
                                    }
                                    if (bmrconfig.SelectedTime == 0 || bmrconfig.SelectedTime == 2)
                                    {
                                        aor.AfternoonStatus = (int)AvailabilityStatus.AVAILABLE;
                                    }
                                    DataRepository.AvailibilityOfRoomsProvider.Update(tm, aor);
                                }
                            }
                        }

                    }
                }
                #region START Booking cancel by user
                try
                {
                    EmailConfig eConfig = em.GetByName("Booking cancel by user");
                    if (eConfig.IsActive)
                    {
                        string bodymsg = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
                        EmailConfigMapping emap2 = new EmailConfigMapping();
                        emap2 = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                        bodymsg = bodymsg.Replace("@CONTENTS", emap2.EmailContents);

                        SendMails objSendmail = new SendMails();
                        objSendmail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
                        Users objUser = GetUser(bk.Id);
                        objSendmail.ToEmail = objUser.EmailId;
                        Hotel objhotel = DataRepository.HotelProvider.GetById(bk.HotelId);
                        objSendmail.Bcc = ConfigurationManager.AppSettings["WatchEMailID"];
                        //Added by Manas : whhile operator changes status
                        if (HttpContext.Current.Session["CurrentOperator"] != null)
                        {
                            objSendmail.Bcc = objhotel.Email;
                        }
                        //Added by Manas : whhile operator changes status
                        EmailValueCollection objEmailValues = new EmailValueCollection();
                        objSendmail.Subject = "Booking cancelled by User, Booking id " + bk.Id.ToString();
                        foreach (KeyValuePair<string, string> strKey in objEmailValues.CancelBookingToHotelUSer(Convert.ToString(bk.Id), objUser.FirstName + " " + objUser.LastName, Convert.ToString(System.DateTime.Now.ToString("dd/MM/yyyy"))))
                        {
                            bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                        }
                        objSendmail.Body = bodymsg;
                        objSendmail.SendMail();
                    }
                }
                catch
                {
                }
                #endregion
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
        }
        #endregion
        public int getRequestStatus(long bookingId)
        {
            return (Convert.ToInt32(DataRepository.BookingProvider.GetById(bookingId).RequestStatus));
        }
        //-------------------------------------new ends--------------------------------------


        /// <summary>
        //This function to get all booking details for hotelid
        /// </summary>  
        public TList<Booking> CheckBookingInvoice(string whereclause)
        {
            int Total = 0;
            _objBooking = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
            return _objBooking;
        }


        public string GetHotelName(long intHotelID)
        {
            Hotel getHotelName = DataRepository.HotelProvider.GetById(intHotelID);
            return getHotelName.Name;
        }

        /// <summary>
        /// Insert Booking after transfer
        /// </summary>
        /// <param name="newUser"></param>
        /// <param name="details"></param>
        /// <returns>string</returns>        
        public void InsertBookingAfterTransfer(Booking objBook)
        {
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                DataRepository.BookingProvider.Insert(objBook);
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
        }



        /// FOR GETINGTNELINK 
        /// 
        public TList<Booking> fetchrequestlink(Int64 reqid )
        {
            string where = "";
            string orderby = "";
            int totalcount;
            TList<Booking> alreq = new TList<Booking>();
            string requeslink = DataRepository.BookingProvider.GetById(reqid).RequestCollectionId;
            if (string.IsNullOrEmpty(requeslink))
            {

                where = " id=" + reqid ;
                alreq = DataRepository.BookingProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount);
            }
            else
            {
                where = " RequestCollectionID='" + requeslink + "'";
                alreq = DataRepository.BookingProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount);               
            }
            return alreq;
        }
        public VList<Viewbookingrequest> fetchrequestlinkforDropdown(string where)
        {
            string whereclause ="";

            //if (cont == 0)
            //{
             whereclause += where;
            //}
            //else
            //{
            //    whereclause = "Requeststatus=7 and ";
              
            //    whereclause += where;
            //}
            
          
            string orderby = "";
            int totalcount;
            VList<Viewbookingrequest> alreq = new VList<Viewbookingrequest>();
            alreq = DataRepository.ViewbookingrequestProvider.GetPaged(whereclause, orderby, 0, int.MaxValue, out totalcount);

            return alreq;
        }
    }
}