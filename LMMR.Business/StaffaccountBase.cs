﻿#region NameSpaces

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;
#endregion
namespace LMMR.Business
{
    public class StaffaccountBase
    {
        public TList<Roles> propGetRoles
        {
            get;
            set;
        }
        #region InsertStaffData
        public string insertstaffdata(Users stfuser)
        {
            TransactionManager transaction = null;

            //bool uservalid = checkUser(stfuser.FirstName, Convert.ToInt64(stfuser.Usertype));
            bool validemail = checkEmail(stfuser.EmailId);
            if (validemail) { return "Email Already Exist."; }
            //if (!uservalid) { return "Staff Name With The Role Already Exist."; };


            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();

                stfuser.IsActive = true;
                if (DataRepository.UsersProvider.Insert(stfuser))
                {
                    UserDetails userdetailstf = new UserDetails();
                    userdetailstf.UserId = stfuser.UserId;
                    DataRepository.UserDetailsProvider.Insert(userdetailstf);
                    // Show proper message
                }
                else
                {
                    return "Information could not be saved.";
                }
                transaction.Commit();
                return "Information Insert successfully.";
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return "Information could not be saved.Please contact Administrator.";
            }
        }
        #endregion

        #region Get StaffUsers

        public TList<Users> GetStaffUsers(string where, string orderBy)
        {
            where += " AND isnull(" + UsersColumn.IsRemoved + ",0) =0";
            TList<Users> Stafflist = new TList<Users>();
            int Total = 0;
            Stafflist = DataRepository.UsersProvider.GetPaged(where, orderBy, 0, int.MaxValue, out Total);
            DataRepository.UsersProvider.DeepLoad(Stafflist, true);
            return Stafflist;

        }

        public TList<City> Getcityname(string where, string orderBy)
        {
            TList<City> citylist = new TList<City>();
            int Total = 0;
            citylist = DataRepository.CityProvider.GetPaged(where, orderBy, 0, int.MaxValue, out Total);
            DataRepository.CityProvider.DeepLoad(citylist, true);
            return citylist;

        }


        #endregion

        public TList<MainPoint> Getmainpoint(string where, string orderBy)
        {
            TList<MainPoint> mainpointlists = new TList<MainPoint>();
            int Total = 0;
            if (where == "")
            {
                where = MainPointColumn.CityId + " in (Select " + CityColumn.Id + " from City where " + CityColumn.IsActive + "=1)";
            }
            else
            {

                where += " and ";

                where += MainPointColumn.CityId + " in (Select " + CityColumn.Id + " from City where " + CityColumn.IsActive + "=1)";
            }

            mainpointlists = DataRepository.MainPointProvider.GetPaged(where, orderBy, 0, int.MaxValue, out Total);
            //DataRepository.MainPointProvider.DeepLoad(mainpointlists, true);
            return mainpointlists;

        }

        public TList<Zone> GetZonelist(string where, string orderBy)
        {
            TList<Zone> zonelist = new TList<Zone>();
            int Total = 0;
            if (where == "")
            {
                where = ZoneColumn.IsActive + "= 1 and " + ZoneColumn.CityId + " in (Select " + CityColumn.Id + " from City where " + CityColumn.IsActive + "=1)";
            }
            else
            {

                where += " and ";

                where += ZoneColumn.IsActive + "= 1 and " + ZoneColumn.CityId + " in (Select " + CityColumn.Id + " from City where " + CityColumn.IsActive + "=1)";
            }

            zonelist = DataRepository.ZoneProvider.GetPaged(where, orderBy, 0, int.MaxValue, out Total);
            //DataRepository.ZoneProvider.DeepLoad(mainpointlists, true);
            return zonelist;

        }




        public TList<Country> Getcountryname(string where, string orderBy)
        {
            TList<Country> citylist = new TList<Country>();
            int Total = 0;
            citylist = DataRepository.CountryProvider.GetPaged(where, orderBy, 0, int.MaxValue, out Total);
            DataRepository.CountryProvider.DeepLoad(citylist, true);
            return citylist;

        }

        public TList<Zone> GetZone()
        {
            int total = 0;
            string whereClause = "IsActive=1";
            string orderby = ZoneColumn.Zone + " ASC";
            TList<Zone> objcountry = DataRepository.ZoneProvider.GetPaged(whereClause, orderby, 0, int.MaxValue, out total);
            return objcountry;
        }
        public TList<Currency> Getcurrency()
        {
            int total = 0;
            string whereClause = string.Empty;
            string orderby = string.Empty;
            TList<Currency> objcurrency = DataRepository.CurrencyProvider.GetPaged(whereClause, orderby, 0, int.MaxValue, out total);
            return objcurrency;
        }
        public TList<City> Getcity()
        {
            int total = 0;
            string whereClause = CityColumn.IsActive + "= 1 and " + CityColumn.CountryId + " in (Select " + CountryColumn.Id + " from Country where " + CountryColumn.IsActive + "=1)";
            string orderby = CityColumn.City + " ASC";
            TList<City> objcityalllist = DataRepository.CityProvider.GetPaged(whereClause, orderby, 0, int.MaxValue, out total);
            DataRepository.CityProvider.DeepLoad(objcityalllist, true);
            return objcityalllist;
        }
        //Bind all country name
        public TList<Country> GetByAllCountry()
        {
            int total = 0;
            string whereClause = CountryColumn.IsActive + "=1 and " + CountryColumn.IsForAll + "=1";
            string OrderBy = CountryColumn.CountryName + " ASC";
            TList<Country> objcityalllist = DataRepository.CountryProvider.GetPaged(whereClause, OrderBy, 0, int.MaxValue, out total);
            return objcityalllist;
        }
        public TList<Country> Getcountry()
        {
            int total = 0;
            string whereClause = "IsActive=1";
            string orderby = CountryColumn.CountryName + " ASC";
            TList<Country> objcityalllist = DataRepository.CountryProvider.GetPaged(whereClause, orderby, 0, int.MaxValue, out total);
            return objcityalllist;
        }

        #region GetStaffUserById
        public Users GetStaffUserByID(string id)
        {
            string where = "UserId='" + id + "'";
            int Total = 0;
            string orderby = "CreatedDate desc";
            TList<Users> getstaff = new TList<Users>();
            getstaff = DataRepository.UsersProvider.GetPaged(where, orderby, 0, int.MaxValue, out Total);
            DataRepository.UsersProvider.DeepLoad(getstaff, true);
            foreach (Users temp in getstaff)
            {
                return temp;
            }
            return null;

        }
        #endregion

        public Zone GetzoneByID(string id)
        {
            string where = "id='" + id + "'";
            int Total = 0;
            string orderby = string.Empty;
            TList<Zone> zoneusers = new TList<Zone>();
            zoneusers = DataRepository.ZoneProvider.GetPaged(where, orderby, 0, int.MaxValue, out Total);
            DataRepository.ZoneProvider.DeepLoad(zoneusers, true);
            foreach (Zone temp in zoneusers)
            {
                return temp;
            }
            return null;

        }
        public MainPoint Getmainpointbyid(Int64 id)
        {
            return DataRepository.MainPointProvider.GetByMainPointId(id);

        }



        public Zone GetzoneByID(Int64 id)
        {
            return DataRepository.ZoneProvider.GetById(id);

        }
        public Currency GetcurrencyByID(Int64 id)
        {
            return DataRepository.CurrencyProvider.GetById(id);

        }

        public City GetcityByID(string id)
        {
            string where = "CountryId='" + id + "'";
            int Total = 0;
            string orderby = string.Empty;
            TList<City> cityusers = new TList<City>();
            cityusers = DataRepository.CityProvider.GetPaged(where, orderby, 0, int.MaxValue, out Total);
            DataRepository.CityProvider.DeepLoad(cityusers, true);
            foreach (City temp in cityusers)
            {
                return temp;
            }
            return null;

        }
        public City GetcityByCityID(string id)
        {
            string where = "id='" + id + "'";
            int Total = 0;
            string orderby = string.Empty;
            TList<City> cityusers = new TList<City>();
            cityusers = DataRepository.CityProvider.GetPaged(where, orderby, 0, int.MaxValue, out Total);
            DataRepository.CityProvider.DeepLoad(cityusers, true);
            foreach (City temp in cityusers)
            {
                return temp;
            }
            return null;

        }

        public MainPoint getmainpointbyid(string id)
        {
            string where = "MainPointID='" + id + "'";
            int Total = 0;
            string orderby = string.Empty;
            TList<MainPoint> mainpoint = new TList<MainPoint>();
            mainpoint = DataRepository.MainPointProvider.GetPaged(where, orderby, 0, int.MaxValue, out Total);
            DataRepository.MainPointProvider.DeepLoad(mainpoint, true);
            foreach (MainPoint temp in mainpoint)
            {
                return temp;
            }
            return null;

        }

        #region StaffUserUpdate
        public string staffupdate(Users staffuserupdate, string staffemail)
        {
            TransactionManager transaction = null;
            //bool uservalid = checkUser(staffuserupdate.FirstName, Convert.ToInt64(staffuserupdate.Usertype));
            if (staffuserupdate.EmailId == staffemail)
            {

            }
            else
            {
                bool validemail = checkEmail(staffuserupdate.EmailId);
                if (!validemail) { return "Email Already Exist."; }
            }

            //if (!uservalid) { return "Staff Name With The Role Already Exist."; };

            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();
                //if (staffuserupdate.IsActive) 
                //{

                //    staffuserupdate.IsActive = false;
                //}
                //else
                //{
                //    staffuserupdate.IsActive = true;
                //}
                if ((DataRepository.UsersProvider.Update(staffuserupdate)))
                {

                    // Show proper message
                }
                else
                {
                    return "Information could not be saved.";
                }
                transaction.Commit();
                return "Information updated successfully.";
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return "Information could not be saved.Please contact Administrator.";
            }
        }
        #endregion

        #region Delete StaffUsers
        public string DeleteStaffUser(Users deletestaff)
        {
            TransactionManager transaction = null;

            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();
                deletestaff.IsActive = false;
                deletestaff.IsRemoved = true;
                if ((DataRepository.UsersProvider.Update(deletestaff)))
                {
                    TList<UserDetails> userdel = DataRepository.UserDetailsProvider.GetByUserId(deletestaff.UserId);
                    if (userdel.Count > 0)
                    {
                        foreach (UserDetails user in userdel)
                        {
                            user.IsDeleted = true;
                            DataRepository.UserDetailsProvider.Update(user);
                        }
                    }
                }
                else
                {
                    return "Information could not be Delete.";
                }
                transaction.Commit();
                return "Information Deleted successfully.";
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return "Information could not be Delete.Please contact Administrator.";
            }

        }
        #endregion

        public string insertcountrydata(Currency cur, Country country)
        {
            TransactionManager transaction = null;

            //bool uservalid = checkUser(staff.StaffName, staff.Role);
            //if (!uservalid) { return "Staff Name With The Role Already Exist."; };


            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();


                if ((DataRepository.CurrencyProvider.Insert(transaction, cur)))
                {
                    country.CurrencyId = cur.Id;
                    DataRepository.CountryProvider.Insert(transaction, country);
                    // Show proper message
                }
                else
                {
                    return "Information could not be saved.";
                }
                transaction.Commit();
                return "Information Insert successfully.";
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return "Information could not be saved.Please contact Administrator.";
            }
        }

        public string deletezone(Zone zone)
        {
            TransactionManager transaction = null;

            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();

                if ((DataRepository.ZoneProvider.Delete(zone)))
                {

                    // Show proper message
                }
                else
                {
                    return "Information could not be Delete.";
                }
                transaction.Commit();
                return "Information Deleted successfully.";
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return "Information could not be Delete.Please contact Administrator.";
            }
        }

        public string deletezone(Zone zone, City city)
        {
            TransactionManager transaction = null;

            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();
                zone.IsActive = false;
                if ((DataRepository.ZoneProvider.Update(zone)))
                {
                    city.IsActive = false;
                    DataRepository.CityProvider.Update(city);
                }
                else
                {
                    return "Information could not be Delete.";
                }
                transaction.Commit();
                return "Information Deleted successfully.";
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return "Information could not be Delete.Please contact Administrator.";
            }
        }

        public string deletezone(City city)
        {
            TransactionManager transaction = null;

            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();
                city.IsActive = false;
                if ((DataRepository.CityProvider.Update(city)))
                {


                }
                else
                {
                    return "Information could not be Delete.";
                }
                transaction.Commit();
                return "Information Deleted successfully.";
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return "Information could not be Delete.Please contact Administrator.";
            }
        }

        public string deletepointpermanent(Int64 id)
        {
            TransactionManager transaction = null;

            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();

                if ((DataRepository.MainPointProvider.Delete(id)))
                {


                }
                else
                {
                    return "Information could not be Delete.";
                }
                transaction.Commit();
                return "Information Deleted successfully.";
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return "Information could not be Delete.Please contact Administrator.";
            }
        }

        public string deletecity(City Cityname)
        {
            TransactionManager transaction = null;

            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();

                if ((DataRepository.CityProvider.Update(Cityname)))
                {

                    // Show proper message
                }
                else
                {
                    return "Information could not be Delete.";
                }
                transaction.Commit();
                return "Information Deleted successfully.";
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return "Information could not be Delete.Please contact Administrator.";
            }
        }

        public string Updatecity(City Citynameupdate)
        {
            TransactionManager transaction = null;

            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();

                if ((DataRepository.CityProvider.Update(Citynameupdate)))
                {

                    // Show proper message
                }
                else
                {
                    return "Information could not be Updated.";
                }
                transaction.Commit();
                return "Information Updated successfully.";
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return "Information could not be Delete.Please contact Administrator.";
            }
        }

        public string Updatezone(Zone Zonenameupdate)
        {
            TransactionManager transaction = null;

            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();

                if ((DataRepository.ZoneProvider.Update(Zonenameupdate)))
                {

                    // Show proper message
                }
                else
                {
                    return "Information could not be Updated.";
                }
                transaction.Commit();
                return "Information Updated successfully.";
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return "Information could not be Delete.Please contact Administrator.";
            }
        }

        #region CheckUser Exist
        public bool checkUser(string staffname, long role)
        {
            int count = 0;

            TList<Users> objstaffuser = DataRepository.UsersProvider.GetPaged(UsersColumn.FirstName + "='" + staffname + "' and " + UsersColumn.Usertype + "='" + role + "'", string.Empty, 0, int.MaxValue, out count);
            if (objstaffuser.Count > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion

        #region checkemail Exist
        public bool checkEmail(string email)
        {
            bool result = false;
            int total = 0;
            string whereClause = UsersColumn.EmailId + "='" + email + "' AND isnull(" + UsersColumn.IsRemoved + ",0) = 0";
            string OrderBy = UsersColumn.FirstName + " ASC";
            TList<Users> objusers = DataRepository.UsersProvider.GetPaged(whereClause, OrderBy, 0, int.MaxValue, out total);
            if (objusers.Count > 0)
            {
                result = true;
            }

            //bool val = true;
            //TList<Users> objstaffuser = DataRepository.UsersProvider.GetPaged(UsersColumn.EmailId + "='" + email + "'", string.Empty, 0, int.MaxValue, out count);
            //if (objstaffuser.Count > 0)
            //{
            //    val = false;
            //    foreach (Users objuser in objstaffuser)
            //    {

            //        //TList<UserDetails> objuseremail = DataRepository.UserDetailsProvider.GetByUserId(objuser.UserId);
            //        string orderby = "IsDeleted desc";
            //        TList<UserDetails> objuseremail = DataRepository.UserDetailsProvider.GetPaged(UserDetailsColumn.UserId + "='" + objuser.UserId + "'", orderby, 0, int.MaxValue, out count);
            //        if (objuseremail.Count > 0)
            //        {
            //            foreach (UserDetails objuserdetail in objuseremail)
            //            {

            //                if (objuserdetail.IsDeleted == true)
            //                {
            //                    val = true;
            //                }
            //                else
            //                {
            //                    val = false;
            //                    return val;
            //                }
            //            }
            //        }

            //    }

            //}
            return result;
        }
        #endregion

        public TList<Roles> GetByAllRole()
        {
            int total = 0;
            string whereClause = string.Empty;
            string orderby = "";
            propGetRoles = DataRepository.RolesProvider.GetPaged(whereClause, orderby, 0, int.MaxValue, out total);
            return propGetRoles;
        }

        public Country GetCountrybyid(Int64 Countryid)
        {
            return DataRepository.CountryProvider.GetById(Countryid);
        }
        public City GetCityById(Int64 cityid)
        {
            return DataRepository.CityProvider.GetById(cityid);
        }
        public TList<City> GetCitybyCountryid(Int64 Countryid)
        {
            TList<City> allcity = new TList<City>();
            allcity = DataRepository.CityProvider.GetByCountryId(Countryid);
            return allcity;
        }
        public TList<Zone> GetZonebycityid(Int64 Cityid)
        {
            TList<Zone> allzone = new TList<Zone>();
            allzone = DataRepository.ZoneProvider.GetByCityId(Cityid);
            return allzone;
        }

        public TList<MainPoint> getmainpointdel(Int64 Cityid)
        {
            TList<MainPoint> allpoint = new TList<MainPoint>();
            allpoint = DataRepository.MainPointProvider.GetByCityId(Cityid);
            return allpoint;
        }
        public bool Deletecountryall(Zone zonedel, City citydel, Country countrydel)
        {
            TransactionManager transaction = null;

            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();
                zonedel.IsActive = false;
                if ((DataRepository.ZoneProvider.Update(zonedel)))
                {
                    citydel.IsActive = false;
                    if ((DataRepository.CityProvider.Update(citydel)))
                    {
                        countrydel.IsActive = false;
                        DataRepository.CountryProvider.Update(countrydel);
                    }
                    else
                    {
                        return false;
                    }
                    // Show proper message
                }
                else
                {
                    return false;
                }
                transaction.Commit();
                return true;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return false;
            }
        }

        public bool Deletecountrycity(City citydel, Country countrydel)
        {
            TransactionManager transaction = null;

            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();

                citydel.IsActive = false;
                if ((DataRepository.CityProvider.Update(citydel)))
                {
                    countrydel.IsActive = false;
                    DataRepository.CountryProvider.Update(countrydel);
                }

                // Show proper message
                else
                {
                    return false;
                }
                transaction.Commit();
                return true;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return false;
            }
        }

        public bool Deletecountry(Country countrydel)
        {
            TransactionManager transaction = null;

            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();

                countrydel.IsActive = false;
                if ((DataRepository.CountryProvider.Update(countrydel)))
                {

                }

                // Show proper message
                else
                {
                    return false;
                }
                transaction.Commit();
                return true;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return false;
            }
        }
        public bool Updatecountry(Country countryupdate, string oldname)
        {
            if (DataRepository.CountryProvider.Update(countryupdate))
            {

                return true;
            }
            else
            {
                return false;
            }
        }
        public bool UpdateCurrency(Currency Currencyname)
        {
            if (DataRepository.CurrencyProvider.Update(Currencyname))
            {

                return true;
            }
            else
            {
                return false;
            }
        }
        public void AddLanguageByCountry(long countryID)
        {
            TList<CountryLanguage> objLanguage = DataRepository.CountryLanguageProvider.GetByCountryId(countryID);
            if (objLanguage.Count <= 0)
            {
                CountryLanguage objAddNew = new CountryLanguage();
                TList<Language> objGetLanguage = DataRepository.LanguageProvider.GetAll();
                if (objGetLanguage.Find(a => a.Name == "English") != null)
                {
                    long languageId = Convert.ToInt64(objGetLanguage.Find(a => a.Name == "English").Id);
                    objAddNew.CountryId = countryID;
                    objAddNew.LanguageId = languageId;
                }

                DataRepository.CountryLanguageProvider.Insert(objAddNew);
            }
        }

        public bool Zonedelete(Zone zoneupdate)
        {
            TransactionManager transaction = null;

            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();

                zoneupdate.IsActive = false;
                if ((DataRepository.ZoneProvider.Update(zoneupdate)))
                {

                }

                // Show proper message
                else
                {
                    return false;
                }
                transaction.Commit();
                return true;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return false;
            }
        }

        public bool CurrencyDelete(Currency Currencyupdate)
        {
            TransactionManager transaction = null;

            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();


                if ((DataRepository.CurrencyProvider.Delete(Currencyupdate)))
                {

                }

                // Show proper message
                else
                {
                    return false;
                }
                transaction.Commit();
                return true;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return false;
            }
        }

        public bool countryinsert(Country insertcountry)
        {
            TransactionManager transaction = null;

            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();


                if ((DataRepository.CountryProvider.Insert(insertcountry)))
                {

                }

                // Show proper message
                else
                {
                    return false;
                }
                transaction.Commit();
                return true;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return false;
            }
        }

        public bool Cityinsert(City insertcity)
        {
            TransactionManager transaction = null;

            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();


                if ((DataRepository.CityProvider.Insert(insertcity)))
                {
                    Zone newzoneinsert = new Zone();
                    newzoneinsert.IsActive = true;
                    newzoneinsert.Zone = "Center";
                    newzoneinsert.CityId = insertcity.Id;

                    DataRepository.ZoneProvider.Insert(newzoneinsert);


                }

                // Show proper message
                else
                {
                    return false;
                }
                transaction.Commit();
                return true;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return false;
            }
        }

        public bool pointdelete(MainPoint pointdelete)
        {
            TransactionManager transaction = null;

            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();


                if ((DataRepository.MainPointProvider.Delete(pointdelete)))
                {

                }

                // Show proper message
                else
                {
                    return false;
                }
                transaction.Commit();
                return true;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return false;
            }
        }

        public bool Zoneinsert(Zone zoneinsert)
        {
            TransactionManager transaction = null;

            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();


                if ((DataRepository.ZoneProvider.Insert(zoneinsert)))
                {

                }

                // Show proper message
                else
                {
                    return false;
                }
                transaction.Commit();
                return true;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return false;
            }
        }

        public bool Currencyinsert(Currency Currencyinsert)
        {
            TransactionManager transaction = null;

            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();


                if ((DataRepository.CurrencyProvider.Insert(Currencyinsert)))
                {

                }

                // Show proper message
                else
                {
                    return false;
                }
                transaction.Commit();
                return true;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return false;
            }
        }

        public bool Pointinsert(MainPoint insertpoint)
        {
            TransactionManager transaction = null;

            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();


                if ((DataRepository.MainPointProvider.Insert(insertpoint)))
                {

                }

                // Show proper message
                else
                {
                    return false;
                }
                transaction.Commit();
                return true;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return false;
            }
        }

        public bool Pointupdate(MainPoint updatepoint)
        {
            TransactionManager transaction = null;

            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();


                if ((DataRepository.MainPointProvider.Update(updatepoint)))
                {

                }

                // Show proper message
                else
                {
                    return false;
                }
                transaction.Commit();
                return true;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return false;
            }
        }




    }


}
