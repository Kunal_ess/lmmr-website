﻿#region NameSpaces

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Data;
using LMMR.Entities;
using System.Configuration;
using System.IO;
using log4net;
using log4net.Config;
using System.Xml;
#endregion

namespace LMMR.Business
{
    public class SurveyBase
    {
        #region Methods

        public string insertSurvey(Serveyresult serveyresult)
        {
            TransactionManager transaction = null;
            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();
                if (DataRepository.ServeyresultProvider.Insert(serveyresult))
                {
                    
                    // Show proper message
                }
                else
                {
                    return "Survey could not be saved.";
                }
                transaction.Commit();
                return "Thank You For The Survey";
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return "Survey could not be saved.";
            }
        }


        public string insertSurveybookingrequest(ServeyQuestion surveyquestion)
        {
            TransactionManager transaction = null;
            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();
                if (DataRepository.ServeyQuestionProvider.Insert(surveyquestion))
                {

                    // Show proper message
                }
                else
                {
                    return "Survey could not be saved.";
                }
                transaction.Commit();
                return "Thank You For The Survey";
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return "Survey could not be saved.";
            }
        }


        public string insertSurvey(ServeyResponse serveyresponse)
        {
            TransactionManager transaction = null;
            try
            {
               
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();
                if (DataRepository.ServeyResponseProvider.Insert(serveyresponse))
                {

                    // Show proper message
                }
                else
                {
                    return "Survey could not be saved.";
                }
                transaction.Commit();
                return "Thank You For The Survey";
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return "Survey could not be saved.";
            }
 
        }

        
                
        //
        //public string insertSurvey(ServeyResponse serveyresponse)
        //{
        //    TransactionManager transaction = null;
        //    try
        //    {
        //        transaction = DataRepository.Provider.CreateTransaction();
        //        transaction.BeginTransaction();
        //        if (DataRepository.ServeyResponseProvider.Insert(serveyresponse))
        //        {
        //            // Show proper message
        //        }
        //        else
        //        {
        //            return "Survey could not be saved.";
        //        }
        //        transaction.Commit();
        //        return "Thank You For The Survey";
        //    }
        //    catch (Exception e)
        //    {
        //        transaction.Rollback();
        //        return "Survey could not be saved.";
        //    }
 
        //}

        #endregion
    }
}
