﻿#region NameSpaces

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LMMR.Business;
using LMMR.Entities;
using log4net;
using log4net.Config;
using System.IO;
using System.Configuration;
using LMMR.Data;
using System.Globalization;



#endregion

namespace LMMR.Business
{

    public class periodicmailbase
    {
        Ranking rank = new Ranking(); 
        public void callmail()
        {
            #region Ranking
            try
            {
                rank.Final();
            }
            catch (Exception ex)
            {

            }
            #endregion

            #region Periodic Mails
            string where = "EmailConfigType=1";
            string orderby1 = "";
            int totalcount = 0;

            using(TList<EmailConfig> econfig = DataRepository.EmailConfigProvider.GetPaged(where, orderby1, 0, int.MaxValue, out totalcount))//.FindAllDistinct(BookingColumn.HotelId);
            {
                try
                {
                    DataRepository.EmailConfigProvider.DeepLoad(econfig);
                }
                catch
                {
                }
                foreach (EmailConfig emc in econfig)
                {
                    DateTime emc1 = (DateTime)(emc.LastCallDuration);


                    DateTime dtmeetingdt = new DateTime(emc1.Year, emc1.Month, emc1.Day);
                    TimeSpan span = dtmeetingdt.Subtract(System.DateTime.Now);
                    int spn = span.Days;
                    switch (emc.EmailName)
                    {
                        case "Survey (booking)":
                            if (spn < 0 && emc.IsActive == true && spn * -1 >= emc.PeriodOfCall)
                            {
                                periodicmailbase.durationbase.surveybooking(emc);
                                emc.LastCallDuration = System.DateTime.Now;
                                DataRepository.EmailConfigProvider.Update(emc);
                            }
                            else
                            {
                                if (spn * -1 >= emc.PeriodOfCall)
                                {
                                    emc.LastCallDuration = System.DateTime.Now;
                                    DataRepository.EmailConfigProvider.Update(emc);
                                }
                            }
                            break;
                        case "Survey (request)":
                            if (spn < 0 && emc.IsActive == true && spn * -1 >= emc.PeriodOfCall)
                            {
                                periodicmailbase.durationbase.surveyrequest(emc);
                                emc.LastCallDuration = System.DateTime.Now;
                                DataRepository.EmailConfigProvider.Update(emc);
                            }
                            else
                            {
                                if (spn * -1 >= emc.PeriodOfCall)
                                {
                                    emc.LastCallDuration = System.DateTime.Now;
                                    DataRepository.EmailConfigProvider.Update(emc);
                                }
                            }
                            break;
                        case "Newsletter":
                            if (spn < 0 && emc.IsActive == true && spn * -1 >= emc.PeriodOfCall)
                            {
                                periodicmailbase.durationbase.newsletter(emc);
                                emc.LastCallDuration = System.DateTime.Now;
                                DataRepository.EmailConfigProvider.Update(emc);
                            }
                            else
                            {
                                if (spn * -1 >= emc.PeriodOfCall)
                                {
                                    emc.LastCallDuration = System.DateTime.Now;
                                    DataRepository.EmailConfigProvider.Update(emc);
                                }
                            }
                            break;
                        case "Reminder availability":
                            if (spn < 0 && emc.IsActive == true && spn * -1 >= emc.PeriodOfCall)
                            {
                                periodicmailbase.durationbase.reminderavailability(emc);
                                emc.LastCallDuration = System.DateTime.Now;
                                DataRepository.EmailConfigProvider.Update(emc);
                            }
                            else
                            {
                                if (spn * -1 >= emc.PeriodOfCall)
                                {
                                    emc.LastCallDuration = System.DateTime.Now;
                                    DataRepository.EmailConfigProvider.Update(emc);
                                }
                            }
                            break;
                        case "Reminder comission":
                            //if (spn < 0 && emc.IsActive == true && spn * -1 >= emc.PeriodOfCall)
                            //{
                                periodicmailbase.datebase.reminderCommission(emc);
                                emc.LastCallDuration = System.DateTime.Now;
                                DataRepository.EmailConfigProvider.Update(emc);

                            //}
                            //else
                            //{
                            //    if (spn * -1 >= emc.PeriodOfCall)
                            //    {
                            //        emc.LastCallDuration = System.DateTime.Now;
                            //        DataRepository.EmailConfigProvider.Update(emc);
                            //    }
                            //}
                            break;
                        case "Reminder request":
                            if (spn < 0 && emc.IsActive == true && spn * -1 >= emc.PeriodOfCall)
                            {
                                periodicmailbase.durationbase.reminderrequest(emc);
                                emc.LastCallDuration = System.DateTime.Now;
                                DataRepository.EmailConfigProvider.Update(emc);

                            }
                            else
                            {
                                if (spn * -1 >= emc.PeriodOfCall)
                                {
                                    emc.LastCallDuration = System.DateTime.Now;
                                    DataRepository.EmailConfigProvider.Update(emc);
                                }
                            }
                            break;
                        case "Reminder content":
                            if (spn < 0 && emc.IsActive == true && spn * -1 >= emc.PeriodOfCall)
                            {
                                periodicmailbase.durationbase.reminderContent(emc);
                                emc.LastCallDuration = System.DateTime.Now;
                                DataRepository.EmailConfigProvider.Update(emc);

                            }
                            else
                            {
                                if (spn * -1 >= emc.PeriodOfCall)
                                {
                                    emc.LastCallDuration = System.DateTime.Now;
                                    DataRepository.EmailConfigProvider.Update(emc);
                                }
                            }
                            break;
                        case "Reminder invoice":
                            //if (spn < 0 && emc.IsActive == true && spn * -1 >= emc.PeriodOfCall)
                            //{
                                periodicmailbase.datebase.reminderInvoice(emc);
                                emc.LastCallDuration = System.DateTime.Now;
                                DataRepository.EmailConfigProvider.Update(emc);
                            //}
                            //else
                            //{
                            //    if (spn * -1 >= emc.PeriodOfCall)
                            //    {
                            //        emc.LastCallDuration = System.DateTime.Now;
                            //        DataRepository.EmailConfigProvider.Update(emc);
                            //    }
                            //}
                            break;
                        case "Commissions check mail":
                            if (spn < 0 && emc.IsActive == true && spn * -1 >= emc.PeriodOfCall)
                            {
                                periodicmailbase.durationbase.Commissioncheckmail(emc);
                                emc.LastCallDuration = System.DateTime.Now;
                                DataRepository.EmailConfigProvider.Update(emc);

                            }
                            else
                            {
                                if (spn * -1 >= emc.PeriodOfCall)
                                {
                                    emc.LastCallDuration = System.DateTime.Now;
                                    DataRepository.EmailConfigProvider.Update(emc);
                                }
                            }
                            break;
                    }
                }
            }
            #endregion

            
        }

        public void mailtype(int a)
        {
           
        }
        public static class durationbase
        {

            public static void surveybooking(EmailConfig eConfig)
            {
                #region survey Booking
                //string firstDate;
                //string lastDate;
                string where;
                //EmailConfigManager em = new EmailConfigManager();
                //EmailConfig eConfig = em.GetByName("Survey (booking)");
                //firstDate = DateTime.Now.ToString("yyyy-MM-dd");
                //lastDate = DateTime.Now.AddDays(-2).ToString("yyyy-MM-dd");
                where = "datediff(day,Departuredate,GetDate())='" + eConfig.PeriodOfCall + "' and requeststatus=6 and booktype=0";
                //where = "DepartureDate" + " between '" + lastDate + "' and '" + firstDate + "' and requeststatus=6";
                string orderby = "";
                int totalcount = 0;
                //int intCount = 1;
                //VList<Viewbookingrequest> Vlistreq = new VList<Viewbookingrequest>();
                using (VList<Viewbookingrequest> Vlistreq = DataRepository.ViewbookingrequestProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount))//.FindAllDistinct(ViewbookingrequestColumn.HotelId);
                {
                    if (Vlistreq.Count > 0)
                    {
                        foreach (Viewbookingrequest vl in Vlistreq)
                        {
                            Booking b = new Booking();
                            b = DataRepository.BookingProvider.GetById(vl.Id);
                            if (b.IsSurveyDone == false)
                            {
                                SendMails mail = new SendMails();
                                mail.FromEmail = ConfigurationManager.AppSettings["ClientSupportEmailID"];
                                mail.ToEmail = vl.EmailId;
                                mail.Bcc = ConfigurationManager.AppSettings["WatchPeriodicEMailID"];
                                string bodymsg = File.ReadAllText(HttpContext.Current.Server.MapPath("~/EmailTemplets/EmailTempletSample.html"));
                                EmailValueCollection objEmailValues = new EmailValueCollection();
                                int languageid = 1;
                                UserDetails udetails = DataRepository.UserDetailsProvider.GetByUserId(vl.CreatorId).FirstOrDefault();
                                if (udetails != null)
                                {
                                    if (udetails.LanguageId != null)
                                    {
                                        languageid = Convert.ToInt32(udetails.LanguageId);
                                    }
                                }
                                EmailConfigMapping emap = new EmailConfigMapping();
                                emap = eConfig.EmailConfigMappingCollection.FirstOrDefault(a => a.LanguageId == languageid);
                                if (emap == null)
                                {
                                    emap = eConfig.EmailConfigMappingCollection.FirstOrDefault(a => a.LanguageId == 1);
                                }
                                bodymsg = bodymsg.Replace("@CONTENTS", emap.EmailContents);
                                mail.Subject = "LMMR User Survey Booking";


                                //string strActivationLink = "activation.aspx?activate=" + newUser.UserId;
                                string user = vl.Contact + vl.LastName;
                                string hotelname = vl.HotelName;
                                string creatorid = vl.CreatorId.ToString();
                                string venueid = vl.HotelId.ToString();
                                string bookingID = vl.Id.ToString();
                                string link = "SurveyBooking.aspx";
                                foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForSurveybooking(user, hotelname, ConfigurationManager.AppSettings["SenderPeriodic"], creatorid, venueid, bookingID, link))
                                {
                                    bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                                }
                                mail.Body = bodymsg;
                                mail.SendMail();

                                //b.IsSurveyDone = true;
                                //DataRepository.BookingProvider.Update(b);

                            }


                        }

                    }
                    else
                    {
                        //lbl1.Text = "There is no Booking for the current date";
                    }
                }
                #endregion
            }

            public static void surveyrequest(EmailConfig eConfig)
            {
                #region survey Request
                //EmailConfigManager em = new EmailConfigManager();
                //EmailConfig eConfig = em.GetByName("Survey (request)");
                //string firstDate = DateTime.Now.ToString("yyyy-MM-dd");
                //string lastDate = DateTime.Now.AddDays(-2).ToString("yyyy-MM-dd");

                string where = "datediff(day,Departuredate,GetDate())='" + eConfig.PeriodOfCall + "' and requeststatus=5 and booktype in (1,2)";
                //string where = "DepartureDate" + " between '" + lastDate + "' and '" + firstDate + "' and requeststatus=7";
                string orderby = "";
                int totalcount = 0;
                int intCount = 1;
                //VList<Viewbookingrequest> Vlistreq = new VList<Viewbookingrequest>();
                using (VList<Viewbookingrequest> Vlistreq = DataRepository.ViewbookingrequestProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount))//.FindAllDistinct(ViewbookingrequestColumn.HotelId);
                {
                    if (Vlistreq.Count > 0)
                    {
                        foreach (Viewbookingrequest vl in Vlistreq)
                        {
                            Booking b = new Booking();
                            b = DataRepository.BookingProvider.GetById(vl.Id);
                            if (b.IsSurveyDone == false)
                            {
                                SendMails mail = new SendMails();
                                mail.FromEmail = ConfigurationManager.AppSettings["ClientSupportEmailID"];
                                mail.ToEmail = vl.EmailId;
                                mail.Bcc = ConfigurationManager.AppSettings["WatchPeriodicEMailID"];
                                string bodymsg = File.ReadAllText(HttpContext.Current.Server.MapPath("~/EmailTemplets/EmailTempletSample.html"));
                                EmailValueCollection objEmailValues = new EmailValueCollection();
                                int languageid = 1;
                                UserDetails udetails = DataRepository.UserDetailsProvider.GetByUserId(vl.CreatorId).FirstOrDefault();
                                if (udetails != null)
                                {
                                    if (udetails.LanguageId != null)
                                    {
                                        languageid = Convert.ToInt32(udetails.LanguageId);
                                    }
                                }
                                EmailConfigMapping emap = new EmailConfigMapping();
                                emap = eConfig.EmailConfigMappingCollection.FirstOrDefault(a => a.LanguageId == languageid);
                                if (emap == null)
                                {
                                    emap = eConfig.EmailConfigMappingCollection.FirstOrDefault(a => a.LanguageId == 1);
                                }
                                bodymsg = bodymsg.Replace("@CONTENTS", emap.EmailContents);
                                mail.Subject = "LMMR User Survey Request";
                                //string strActivationLink = "activation.aspx?activate=" + newUser.UserId;
                                string user = vl.Contact + vl.LastName;
                                //string hotelname = vl.HotelName;
                                string creatorid = vl.CreatorId.ToString();
                                string venueid = vl.HotelId.ToString();
                                string bookingID = vl.Id.ToString();
                                string link = "SurveyRequest.aspx";
                                foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForSurveyrequest(user, ConfigurationManager.AppSettings["SenderPeriodic"], creatorid, venueid, bookingID, link))
                                {
                                    bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                                }
                                mail.Body = bodymsg;
                                mail.SendMail();
                            }
                        }
                    }
                }
                #endregion
            }
            public static void reminderrequest(EmailConfig eConfig)
            {
                #region Reminder Request
                //EmailConfigManager em = new EmailConfigManager();
                //EmailConfig eConfig = em.GetByName("Reminder request");
                //string firstDate = DateTime.Now.ToString("yyyy-MM-dd");
                //string where = "datediff(day,BookingDate,GetDate())='-" + eConfig.PeriodOfCall + "' and requeststatus=1 and booktype in (1,2)";
                string where = "requeststatus=1 and booktype in (1,2)";
                string orderby1 = "";
                int totalcount = 0;
                //TList<Booking> request = new TList<Booking>();
                using (TList<Booking> request = DataRepository.BookingProvider.GetPaged(where, orderby1, 0, int.MaxValue, out totalcount))
                {
                    if (request.Count > 0)
                    {
                        foreach (Booking bokreq in request)
                        {
                            Hotel hotelname = DataRepository.HotelProvider.GetById(bokreq.HotelId);
                            TList<HotelContact> hc = DataRepository.HotelContactProvider.GetByHotelId(bokreq.HotelId);
                            if (hc.Count > 0)
                            {
                                SendMails mail = new SendMails();
                                mail.FromEmail = ConfigurationManager.AppSettings["ClientSupportEmailID"];
                                foreach (HotelContact hcon in hc)
                                {
                                    if (hcon.ContactType == "PrimaryContact2" && hcon.UserType == "WeekEnd" && (DateTime.Now.DayOfWeek == DayOfWeek.Saturday || DateTime.Now.DayOfWeek == DayOfWeek.Sunday))
                                    {
                                        if (!string.IsNullOrEmpty(hcon.Email))
                                        {
                                            mail.ToEmail = hcon.Email;
                                        }
                                        else
                                        {
                                            if (hcon.ContactType == "PrimaryContact1")
                                            {
                                                if (!string.IsNullOrEmpty(hcon.Email))
                                                {
                                                    mail.ToEmail = hcon.Email;
                                                }
                                            }
                                        }
                                    }
                                    else if (hcon.ContactType == "PrimaryContact1")
                                    {
                                        if (!string.IsNullOrEmpty(hcon.Email))
                                        {
                                            mail.ToEmail = hcon.Email;
                                        }
                                    }
                                }
                                //mail.ToEmail = hotelname.Email;
                                mail.Bcc = ConfigurationManager.AppSettings["WatchPeriodicEMailID"];
                                string bodymsg = File.ReadAllText(HttpContext.Current.Server.MapPath("~/EmailTemplets/EmailTempletSample.html"));
                                EmailValueCollection objEmailValues = new EmailValueCollection();
                                EmailConfigMapping emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                                bodymsg = bodymsg.Replace("@CONTENTS", emap.EmailContents);
                                mail.Subject = "Reminder pending request";

                                foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForPendingRequest(ConfigurationManager.AppSettings["SenderPeriodic"]))
                                {
                                    bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                                }

                                mail.Body = bodymsg;
                                mail.SendMail();
                            }
                        }
                    }
                }
                #endregion
            }

            public static void newsletter(EmailConfig eConfig)
            {
                #region newsletter
                //EmailConfigManager em = new EmailConfigManager();
                //EmailConfig eConfig = em.GetByName("Newsletter");
                //string firstDate = DateTime.Now.ToString("yyyy-MM-dd");
                //DateTime emc1 = (DateTime) (eConfig.LastCallDuration);
                //DateTime dtmeetingdt = new DateTime(Convert.ToInt32(emc1.ToString("dd/MM/yyyy").Split('/')[2]), Convert.ToInt32(emc1.ToString("dd/MM/yyyy").Split('/')[1]), Convert.ToInt32(emc1.ToString("dd/MM/yyyy").Split('/')[0]));

                //TimeSpan span = dtmeetingdt.Subtract(System.DateTime.Now);
                //int spn = span.Days;
                //if (spn == eConfig.PeriodOfCall * -1)
                //{
                    string where = "";
                    string orderby1 = "";
                    int totalcount = 0;
                    //TList<NewsLetterSubscriber> request = new TList<NewsLetterSubscriber>();
                    using (TList<NewsLetterSubscriber> request = DataRepository.NewsLetterSubscriberProvider.GetPaged(where, orderby1, 0, int.MaxValue, out totalcount))
                    {
                        foreach (NewsLetterSubscriber ns in request)
                        {
                            SendMails mail = new SendMails();
                            mail.FromEmail = ConfigurationManager.AppSettings["ClientSupportEmailID"];
                            mail.ToEmail = ns.SubscriberEmailid;
                            mail.Bcc = ConfigurationManager.AppSettings["WatchPeriodicEMailID"];
                            string bodymsg = File.ReadAllText(HttpContext.Current.Server.MapPath("~/EmailTemplets/EmailTempletSample.html"));
                            EmailValueCollection objEmailValues = new EmailValueCollection();
                            EmailConfigMapping emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                            bodymsg = bodymsg.Replace("@CONTENTS", emap.EmailContents);
                            mail.Subject = " Newsletter";
                            string SiteRootPath = (string.IsNullOrEmpty(HttpContext.Current.Request.ApplicationPath) || HttpContext.Current.Request.ApplicationPath == "/") ? HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" : HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath + "/";
                            string Url = SiteRootPath + "login/english"; //ConfigurationManager.AppSettings["SiteRootPath"] + "login/english";
                            foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForNewsletter(ConfigurationManager.AppSettings["SenderPeriodic"], Url, ns.FirstName + " " + ns.LastName))
                            {
                                bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                            }
                            mail.Body = bodymsg;
                            mail.SendMail();
                        }
                    }
                #endregion
            }

            public static void Commissioncheckmail(EmailConfig eConfig)
            {
                #region Reminder commission Check Mail
                //EmailConfigManager em = new EmailConfigManager();
                //EmailConfig eConfig = em.GetByName("Commissions check mail");
                //string firstDate = DateTime.Now.ToString("yyyy-MM-dd");
                //string lastDate = DateTime.Now.AddDays(-5).ToString("yyyy-MM-dd");
                string where = " IsComissionDone=0 and booktype=0 and  datediff(day,BookingDate,GetDate())='" + eConfig.PeriodOfCall + "' ";
                string orderby1 = "";
                int totalcount = 0;
                //TList<Booking> bookingCommission = new TList<Booking>();
                //bookingCommission = DataRepository.BookingProvider.GetPaged(where, orderby1, 0, int.MaxValue, out totalcount);//.FindAllDistinct(BookingColumn.HotelId);
                 //VList<Viewbookingrequest> bookingCommission = new VList<Viewbookingrequest>();
                using (VList<Viewbookingrequest> bookingCommission = DataRepository.ViewbookingrequestProvider.GetPaged(where, orderby1, 0, int.MaxValue, out totalcount))
                {
                    if (bookingCommission.Count > 0)
                    {
                        foreach (Viewbookingrequest booking in bookingCommission)
                        {
                            Hotel hotelname = DataRepository.HotelProvider.GetById(booking.HotelId);
                            TList<HotelContact> hc = DataRepository.HotelContactProvider.GetByHotelId(booking.HotelId);
                            if (hc.Count > 0)
                            {
                                if (booking.IsComissionDone == false)
                                {
                                    SendMails mail = new SendMails();
                                    mail.FromEmail = ConfigurationManager.AppSettings["ClientSupportEmailID"];
                                    //mail.ToEmail = hotelname.Email;
                                    foreach (HotelContact hcon in hc)
                                    {
                                        if (hcon.ContactType == "PrimaryContact2" && hcon.UserType == "WeekEnd" && (DateTime.Now.DayOfWeek == DayOfWeek.Saturday || DateTime.Now.DayOfWeek == DayOfWeek.Sunday))
                                        {
                                            if (!string.IsNullOrEmpty(hcon.Email))
                                            {
                                                mail.ToEmail = hcon.Email;
                                            }
                                            else
                                            {
                                                if (hcon.ContactType == "PrimaryContact1")
                                                {
                                                    if (!string.IsNullOrEmpty(hcon.Email))
                                                    {
                                                        mail.ToEmail = hcon.Email;
                                                    }
                                                }
                                            }
                                        }
                                        else if (hcon.ContactType == "PrimaryContact1")
                                        {
                                            if (!string.IsNullOrEmpty(hcon.Email))
                                            {
                                                mail.ToEmail = hcon.Email;
                                            }
                                        }
                                    }
                                    mail.Bcc = ConfigurationManager.AppSettings["WatchPeriodicEMailID"];
                                    string bodymsg = File.ReadAllText(HttpContext.Current.Server.MapPath("~/EmailTemplets/EmailTempletSample.html"));
                                    EmailValueCollection objEmailValues = new EmailValueCollection();
                                    EmailConfigMapping emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                                    bodymsg = bodymsg.Replace("@CONTENTS", emap.EmailContents);
                                    mail.Subject = " Check commission";
                                    string NumberOfBooking = Convert.ToString(booking.Id);
                                    string SiteRootPath = (string.IsNullOrEmpty(HttpContext.Current.Request.ApplicationPath) || HttpContext.Current.Request.ApplicationPath == "/") ? HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" : HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath + "/";
                                    string Url = SiteRootPath + "login/english";

                                    StringBuilder sb = new StringBuilder();
                                    sb.Append("<table border='1px' cellpadding='5' cellspacing='0'  style='border: solid 1px Black; font-size: small;font-family: Arial Narrow;'> <tr> <td> Booking # </td> <td> Company </td><td> Contact person </td> <td> Arrival date </td> <td> Departure date </td> </tr>");
                                    sb.Append("<tr> <td> " + booking.Id + " </td> <td> " + booking.Usertype + " </td><td> " + booking.Contact + " </td> <td> " + Convert.ToString(booking.ArrivalDate) + " </td> <td> " + Convert.ToString(booking.DepartureDate) + "  </td> </tr> </table>");
                                    string List = sb.ToString();
                                    foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForCheckCommission(ConfigurationManager.AppSettings["SenderPeriodic"], NumberOfBooking, List, Url))
                                    {
                                        bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                                    }
                                    mail.Body = bodymsg;
                                    mail.SendMail();
                                }
                            }
                        }
                    }
                }
                #endregion
            }

            public static void reminderavailability(EmailConfig eConfig)
            {
                #region Reminder Availability
                //string firstDate;
                //string lastDate;
                string where;
                //EmailConfigManager em = new EmailConfigManager();
                //EmailConfig eConfig = em.GetByName("Reminder availability");
                //firstDate = DateTime.Now.ToString("yyyy-MM-dd");
                where = HotelColumn.GoOnline + "=1" + "and " + HotelColumn.IsRemoved + "=0" + " and " + "IsActive=1";           //"";//datediff(day,AvailabilityDate,'" + firstDate + "')='" + eConfig.PeriodOfCall + "'";
                string orderby = "";
                int totalcount = 0;
                //int intCount = 1;
                //TList<Hotel> remindavail = new TList<Hotel>();
                using (TList<Hotel> remindavail = DataRepository.HotelProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount))
                {
                    foreach (Hotel availability in remindavail)
                    {
                        Hotel hotelname = DataRepository.HotelProvider.GetById(availability.Id);
                        TList<HotelContact> hc = DataRepository.HotelContactProvider.GetByHotelId(availability.Id);
                        if (hc.Count > 0)
                        {
                            SendMails mail = new SendMails();
                            mail.FromEmail = ConfigurationManager.AppSettings["ClientSupportEmailID"];
                            foreach (HotelContact hcon in hc)
                            {
                                if (hcon.ContactType == "PrimaryContact2" && hcon.UserType == "WeekEnd" && (DateTime.Now.DayOfWeek == DayOfWeek.Saturday || DateTime.Now.DayOfWeek == DayOfWeek.Sunday))
                                {
                                    if (!string.IsNullOrEmpty(hcon.Email))
                                    {
                                        mail.ToEmail = hcon.Email;
                                    }
                                    else
                                    {
                                        if (hcon.ContactType == "PrimaryContact1")
                                        {
                                            if (!string.IsNullOrEmpty(hcon.Email))
                                            {
                                                mail.ToEmail = hcon.Email;
                                            }
                                        }
                                    }
                                }
                                else if (hcon.ContactType == "PrimaryContact1")
                                {
                                    if (!string.IsNullOrEmpty(hcon.Email))
                                    {
                                        mail.ToEmail = hcon.Email;
                                    }
                                }
                            }
                            //mail.ToEmail = hotelname.Email;
                            mail.Bcc = ConfigurationManager.AppSettings["WatchPeriodicEMailID"];
                            string bodymsg = File.ReadAllText(HttpContext.Current.Server.MapPath("~/EmailTemplets/EmailTempletSample.html"));
                            EmailValueCollection objEmailValues = new EmailValueCollection();
                            EmailConfigMapping emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                            bodymsg = bodymsg.Replace("@CONTENTS", emap.EmailContents);
                            mail.Subject = " Lastminutemeetingoom Availability Status";
                            string user = hotelname.Name;
                            //string hotelname = vl.HotelName;
                            string creatorid = hotelname.CreationDate.ToString();
                            string venueid = hotelname.Id.ToString();
                            string bookingID = hotelname.Id.ToString();
                            string link = "SurveyDetailsRequest.aspx";
                            string SiteRootPath = (string.IsNullOrEmpty(HttpContext.Current.Request.ApplicationPath) || HttpContext.Current.Request.ApplicationPath == "/") ? HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" : HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath + "/";
                            string logInUrl = SiteRootPath + "login/english";
                            foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForReminderAvailability(ConfigurationManager.AppSettings["SenderPeriodic"], logInUrl, new AvalibalityManager().GetAvailabilityForNextsixtyDaysByHotelID(hotelname.Id), user))
                            {
                                bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                            }
                            mail.Body = bodymsg;
                            mail.SendMail();
                        }
                    }
                }
                #endregion
            }
            public static void reminderContent(EmailConfig eConfig)
            {
                #region Reminder Content
                //string firstDate;
                //string lastDate;
                string where;
                //EmailConfigManager em = new EmailConfigManager();
                //EmailConfig eConfig = em.GetByName("Reminder content");
                //firstDate = DateTime.Now.ToString("yyyy-MM-dd");
                where = "  isnull( RequestGoOnline,0)=0  and IsRemoved=0 and datediff(day,creationDate,Getdate())='" + eConfig.PeriodOfCall + "'";
                string orderby = "";
                int totalcount = 0;
                int intCount = 1;
                //TList<Hotel> remindcontent = new TList<Hotel>();
                using (TList<Hotel> remindcontent = DataRepository.HotelProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount))
                {
                    if (remindcontent.Count > 0)
                    {
                        {
                            foreach (Hotel hotel in remindcontent)
                            {
                                TList<HotelContact> hc = DataRepository.HotelContactProvider.GetByHotelId(hotel.Id);
                                if (hc.Count > 0)
                                {
                                    SendMails mail = new SendMails();
                                    mail.FromEmail = ConfigurationManager.AppSettings["ClientSupportEmailID"];
                                    foreach (HotelContact hcon in hc)
                                    {
                                        if (hcon.ContactType == "PrimaryContact2" && hcon.UserType == "WeekEnd" && (DateTime.Now.DayOfWeek == DayOfWeek.Saturday || DateTime.Now.DayOfWeek == DayOfWeek.Sunday))
                                        {
                                            if (!string.IsNullOrEmpty(hcon.Email))
                                            {
                                                mail.ToEmail = hcon.Email;
                                            }
                                            else
                                            {
                                                if (hcon.ContactType == "PrimaryContact1")
                                                {
                                                    if (!string.IsNullOrEmpty(hcon.Email))
                                                    {
                                                        mail.ToEmail = hcon.Email;
                                                    }
                                                }
                                            }
                                        }
                                        else if (hcon.ContactType == "PrimaryContact1")
                                        {
                                            if (!string.IsNullOrEmpty(hcon.Email))
                                            {
                                                mail.ToEmail = hcon.Email;
                                            }
                                        }
                                    }
                                    //mail.ToEmail = hotel.Email;
                                    mail.Bcc = ConfigurationManager.AppSettings["WatchPeriodicEMailID"];
                                    string bodymsg = File.ReadAllText(HttpContext.Current.Server.MapPath("~/EmailTemplets/EmailTempletSample.html"));
                                    EmailValueCollection objEmailValues = new EmailValueCollection();
                                    EmailConfigMapping emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                                    bodymsg = bodymsg.Replace("@CONTENTS", emap.EmailContents);
                                    mail.Subject = "Content Lastminutemeetingroom";
                                    foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForReminderContent(ConfigurationManager.AppSettings["SenderPeriodic"]))
                                    {
                                        bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                                    }
                                    mail.Body = bodymsg;
                                    mail.SendMail();
                                }
                            }
                        }
                    }
                }
                #endregion
            }
        }
        public static class datebase
        {
            public static void reminderInvoice(EmailConfig eConfig)
            {
                #region Reminder Invoice
                //EmailConfigManager em = new EmailConfigManager();
                //EmailConfig eConfig = em.GetByName("Reminder invoice");
                DateTime currentdate = new DateTime(Convert.ToInt32(System.DateTime.Now.Year), Convert.ToInt32(System.DateTime.Now.Month), Convert.ToInt32(System.DateTime.Now.Day));
                if (Convert.ToInt32(currentdate.Day) == eConfig.PeriodOfCall && DateTime.Now.Hour < 12 && eConfig.IsActive == true)
                {
                    string firstDate = DateTime.Now.ToString("yyyy-MM-dd");
                    string where = " IsPaid=0 and DueDate <'" + currentdate + "'";
                    string orderby1 = "";
                    int totalcount = 0;

                    //TList<Invoice> invoicemail = new TList<Invoice>();

                    using (TList<Invoice> invoicemail = DataRepository.InvoiceProvider.GetPaged(where, orderby1, 0, int.MaxValue, out totalcount))
                    {
                        if (invoicemail.Count > 0)
                        {
                            foreach (Invoice inv in invoicemail)
                            {
                                Hotel hotelname = DataRepository.HotelProvider.GetById(inv.HotelId);
                                TList<HotelContact> hc = DataRepository.HotelContactProvider.GetByHotelId(inv.HotelId);
                                if (hc.Count > 0)
                                {
                                    if (inv.IsPaid == false)
                                    {
                                        SendMails mail = new SendMails();
                                        mail.FromEmail = ConfigurationManager.AppSettings["ClientSupportEmailID"];
                                        //mail.ToEmail = hotelname.Email;
                                        foreach (HotelContact hcon in hc)
                                        {
                                            if (hcon.ContactType == "PrimaryContact2" && hcon.UserType == "WeekEnd" && (DateTime.Now.DayOfWeek == DayOfWeek.Saturday || DateTime.Now.DayOfWeek == DayOfWeek.Sunday))
                                            {
                                                if (!string.IsNullOrEmpty(hcon.Email))
                                                {
                                                    mail.ToEmail = hcon.Email;
                                                }
                                                else
                                                {
                                                    if (hcon.ContactType == "PrimaryContact1")
                                                    {
                                                        if (!string.IsNullOrEmpty(hcon.Email))
                                                        {
                                                            mail.ToEmail = hcon.Email;
                                                        }
                                                    }
                                                }
                                            }
                                            else if (hcon.ContactType == "PrimaryContact1")
                                            {
                                                if (!string.IsNullOrEmpty(hcon.Email))
                                                {
                                                    mail.ToEmail = hcon.Email;
                                                }
                                            }
                                        }
                                        mail.Bcc = ConfigurationManager.AppSettings["WatchPeriodicEMailID"];
                                        string bodymsg = File.ReadAllText(HttpContext.Current.Server.MapPath("~/EmailTemplets/EmailTempletSample.html"));
                                        EmailValueCollection objEmailValues = new EmailValueCollection();
                                        EmailConfigMapping emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                                        bodymsg = bodymsg.Replace("@CONTENTS", emap.EmailContents);
                                        mail.Subject = "Invoice Lastminutemeetingroom";
                                        string SiteRootPath = (string.IsNullOrEmpty(HttpContext.Current.Request.ApplicationPath) || HttpContext.Current.Request.ApplicationPath == "/") ? HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" : HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath + "/";
                                        string Url = SiteRootPath + "login/english";
                                        string List = "";
                                        foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForReminderInvoice(ConfigurationManager.AppSettings["SenderPeriodic"], Url, List))
                                        {
                                            bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                                        }
                                        mail.Body = bodymsg;
                                        mail.SendMail();

                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
            }

            public static void reminderCommission(EmailConfig eConfig)
            {
                #region Reminder commission
                //TList<Hotel> tlisthotel;
                //EmailConfigManager em = new EmailConfigManager();
                //EmailConfig eConfig = em.GetByName("Reminder comission");
                DateTime currentdate = new DateTime(Convert.ToInt32(System.DateTime.Now.Year), Convert.ToInt32(System.DateTime.Now.Month), Convert.ToInt32(System.DateTime.Now.Day));
                if (Convert.ToInt32(currentdate.Day) == eConfig.PeriodOfCall && DateTime.Now.Hour < 12 && eConfig.IsActive == true)
                {


                    HotelInfo objhotelinfo = new HotelInfo();
                    using (TList<Hotel> tlisthotel = objhotelinfo.GetAllHotel())
                    {
                        foreach (Hotel h in tlisthotel)
                        {

                            string firstDate = DateTime.Now.ToString("yyyy-MM-dd");
                            string where = "IsComissionDone=0 and requeststatus in (7,6) and DepartureDate <'" + firstDate + "' and hotelId ='" + h.Id + "'";
                            string orderby1 = "";
                            int totalcount = 0;

                            VList<Viewbookingrequest> bookingCommission = new VList<Viewbookingrequest>();
                            bookingCommission = DataRepository.ViewbookingrequestProvider.GetPaged(where, orderby1, 0, int.MaxValue, out totalcount);
                            if (bookingCommission.Count > 0)
                            {
                                StringBuilder sb = new StringBuilder();
                                sb.Append("<table border='1px' cellpadding='5' cellspacing='0'  style='border: solid 1px Black; font-size: small;font-family: Arial Narrow;'> <tr> <td> Booking/Request # </td> <td> Company </td><td> Contact person </td> <td> Arrival date </td> <td> Departure date </td> </tr>");
                                foreach (Viewbookingrequest booking in bookingCommission)
                                {

                                    sb.Append("<tr> <td> " + booking.Id + " </td> <td> " + booking.Usertype + " </td><td> " + booking.Contact + " </td> <td> " + Convert.ToString(booking.ArrivalDate) + " </td> <td> " + Convert.ToString(booking.DepartureDate) + "  </td> </tr> ");
                                }
                                sb.Append("</table>");
                                string List = sb.ToString();
                                Hotel hotelname = DataRepository.HotelProvider.GetById(h.Id);
                                TList<HotelContact> hc = DataRepository.HotelContactProvider.GetByHotelId(h.Id);
                                if (hc.Count > 0)
                                {
                                    SendMails mail = new SendMails();
                                    mail.FromEmail = ConfigurationManager.AppSettings["ClientSupportEmailID"];
                                    foreach (HotelContact hcon in hc)
                                    {
                                        if (hcon.ContactType == "PrimaryContact2" && hcon.UserType == "WeekEnd" && (DateTime.Now.DayOfWeek == DayOfWeek.Saturday || DateTime.Now.DayOfWeek == DayOfWeek.Sunday))
                                        {
                                            if (!string.IsNullOrEmpty(hcon.Email))
                                            {
                                                mail.ToEmail = hcon.Email;
                                            }
                                            else
                                            {
                                                if (hcon.ContactType == "PrimaryContact1")
                                                {
                                                    if (!string.IsNullOrEmpty(hcon.Email))
                                                    {
                                                        mail.ToEmail = hcon.Email;
                                                    }
                                                }
                                            }
                                        }
                                        else if (hcon.ContactType == "PrimaryContact1")
                                        {
                                            if (!string.IsNullOrEmpty(hcon.Email))
                                            {
                                                mail.ToEmail = hcon.Email;
                                            }
                                        }
                                    }
                                    //mail.ToEmail = hotelname.Email;
                                    mail.Bcc = ConfigurationManager.AppSettings["WatchPeriodicEMailID"];
                                    string bodymsg = File.ReadAllText(HttpContext.Current.Server.MapPath("~/EmailTemplets/EmailTempletSample.html"));
                                    EmailValueCollection objEmailValues = new EmailValueCollection();
                                    EmailConfigMapping emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                                    bodymsg = bodymsg.Replace("@CONTENTS", emap.EmailContents);
                                    mail.Subject = "Outstanding commissions";
                                    string CurrentMonth = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(System.DateTime.Now.Month);
                                    foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForOustandingCommissions(ConfigurationManager.AppSettings["SenderPeriodic"], CurrentMonth, List))
                                    {
                                        bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                                    }
                                    mail.Body = bodymsg;
                                    mail.SendMail();
                                }
                            }
                        }
                    }
                }
                #endregion
            }
        }

    }
}
