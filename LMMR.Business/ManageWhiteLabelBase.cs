﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using LMMR.Entities;
using LMMR.Data;

namespace LMMR.Business
{
    public abstract class ManageWhiteLabelBase
    {
        /// <summary>
        /// This function usedto get all white label.
        /// </summary>
        /// <returns></returns>
        public TList<WhiteLabel> GetAllWhiteLabel()
        {
            TList<WhiteLabel> ObjWhiteLabel = DataRepository.WhiteLabelProvider.GetAll();
            return ObjWhiteLabel;
        }

        /// <summary>
        /// This function used for get white label by Id.
        /// </summary>
        /// <param name="WLId"></param>
        /// <returns></returns>
        public WhiteLabel GetWhiteLabelByID(long WLId)
        {
            WhiteLabel ObjWhiteLabel = DataRepository.WhiteLabelProvider.GetById(WLId);

            return ObjWhiteLabel;
        }



        public TList<WhiteLabelMappingWithHotel> GetHotelMappingByWhiteLabelId(long WLId)
        {
            TList<WhiteLabelMappingWithHotel> objMapping = DataRepository.WhiteLabelMappingWithHotelProvider.GetByWhiteLabelId(WLId);

            return objMapping;
        }

        public long GetWhiteLabelLastID()
        {
            WhiteLabel ObjWhiteLabel = DataRepository.WhiteLabelProvider.GetAll().LastOrDefault();
            if (ObjWhiteLabel != null)
            {
                return ObjWhiteLabel.Id;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// This function used for delete.
        /// </summary>
        /// <returns></returns>
        public bool DeleteWhiteLabel(long WLId)
        {
            bool isResult = false;

            //TList<WhiteLabelMappingWithGroup> objGetWLGrounp = DataRepository.WhiteLabelMappingWithGroupProvider.GetByWhiteLabelId(WLId);
            //if (objGetWLGrounp.Count > 0)
            //{
            //    int index = 0;
            //    for (index = 0; index <= objGetWLGrounp.Count - 1; index++)
            //    {
            //        DataRepository.WhiteLabelMappingWithGroupProvider.Delete(objGetWLGrounp[index].Id);
            //    }
            //}
            TList<WhiteLabelMappingWithHotel> ObjGetWLHotel = DataRepository.WhiteLabelMappingWithHotelProvider.GetByHotelId(WLId);
            if (ObjGetWLHotel.Count > 0)
            {
                int index1 = 0;
                for (index1 = 0; index1 <= ObjGetWLHotel.Count - 1; index1++)
                {
                    DataRepository.WhiteLabelMappingWithHotelProvider.Delete(ObjGetWLHotel[index1].Id);
                }
            }

            if (DataRepository.WhiteLabelProvider.Delete(WLId))
            {
                isResult = true;
            }
            else
            {
                isResult = false;
            }

            return isResult;
        }

        /// <summary>
        /// This function used to insert new white label record.
        /// </summary>
        /// <param name="objWL"></param>
        /// <returns></returns>
        public bool InsertWhiteLabel(WhiteLabel objWL)
        {
            bool isResult = false;

            if (DataRepository.WhiteLabelProvider.Insert(objWL))
            {
                isResult = true;
            }

            return isResult;
        }

        /// <summary>
        /// This function used for update white label record.
        /// </summary>
        /// <param name="objWL"></param>
        /// <returns></returns>
        public bool UpdateWhiteLabel(WhiteLabel objWL)
        {
            bool isResult = false;
            if (DataRepository.WhiteLabelProvider.Update(objWL))
            {
                isResult = true;
            }
            return isResult;
        }



        public bool InsertMappingHotel(WhiteLabelMappingWithHotel objWLHotel)
        {
            bool isResult = false;

            if (DataRepository.WhiteLabelMappingWithHotelProvider.Insert(objWLHotel))
            {
                isResult = true;
            }
            else
            {
                isResult = false;
            }

            return isResult;
        }



        public void ClearMappingHotel(long WLId)
        {
            TList<WhiteLabelMappingWithHotel> ObjGetWLHotel = DataRepository.WhiteLabelMappingWithHotelProvider.GetByWhiteLabelId(WLId);
            if (ObjGetWLHotel.Count > 0)
            {
                int index1 = 0;
                for (index1 = 0; index1 <= ObjGetWLHotel.Count - 1; index1++)
                {
                    DataRepository.WhiteLabelMappingWithHotelProvider.Delete(ObjGetWLHotel[index1].Id);
                }
            }
        }

        public string GetHotelNameById(long Id)
        {
            string HotelName = string.Empty;
            Hotel objHotel = DataRepository.HotelProvider.GetById(Id);
            if (objHotel != null)
            {
                HotelName = objHotel.Name;
            }
            return HotelName;
        }

        public string GetGroupNameById(long Id)
        {
            string GroupName = string.Empty;
            Users objGroup = DataRepository.UsersProvider.GetByUserId(Id);
            if (objGroup != null)
            {
                GroupName = objGroup.FirstName;
            }
            return GroupName;
        }

        public TList<Hotel> GetHotelInfo()
        {
            TList<Hotel> objHotel = DataRepository.HotelProvider.GetAll();
            return objHotel;
        }

        public TList<Country> GetCountry()
        {
            int total = 0;
            string whereClause = "IsForAll=1";
            string OrderBy = string.Empty;
            TList<Country> objCountry = DataRepository.CountryProvider.GetPaged(whereClause, OrderBy, 0, int.MaxValue, out total);
            if (objCountry.Count > 0)
            {
                DataRepository.CountryProvider.DeepLoad(objCountry, false);
            }
            return objCountry;
        }

        public TList<City> GetCity()
        {
            TList<City> objCity = DataRepository.CityProvider.GetAll();
            return objCity;
        }


        public bool Deactivate(long WLID)
        {
            bool result = false;
            WhiteLabel objWL = DataRepository.WhiteLabelProvider.GetById(WLID);
            if (objWL != null)
            {
                objWL.IsActive = false;
                if (DataRepository.WhiteLabelProvider.Update(objWL))
                {
                    result = true;
                }
            }
            return result;
        }

    }
}
