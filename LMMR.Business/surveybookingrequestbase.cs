﻿#region NameSpaces

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;
#endregion

namespace LMMR.Business
{
   public class surveybookingrequestbase
    {

        //Declare Variable to store data
        private TList<Serveyresult> _varGetAllServeyresult;

        //Property to Store the TList of Entities.SurveyResponse class
        public TList<Serveyresult> propGetAllServeyresult
        {
            get { return _varGetAllServeyresult; }
            set { _varGetAllServeyresult = value; }

        }

       public TList<ServeyAnswer> Getsurveybooking()
       {
           string where = "AnswerType=1";
           int Total = 0;
           string orderby = string.Empty;
           TList<ServeyAnswer> surveyanswer = new TList<ServeyAnswer>();
           surveyanswer = DataRepository.ServeyAnswerProvider.GetPaged(where, orderby, 0, int.MaxValue, out Total);
           //DataRepository.ZoneProvider.DeepLoad(zoneusers, true);
           return surveyanswer;

       }
       public TList<ServeyAnswer> Getsurveyrequest()
       {
           string where = "AnswerType=0";
           int Total = 0;
           string orderby = string.Empty;
           TList<ServeyAnswer> surveyanswer = new TList<ServeyAnswer>();
           surveyanswer = DataRepository.ServeyAnswerProvider.GetPaged(where, orderby, 0, int.MaxValue, out Total);
           //DataRepository.ZoneProvider.DeepLoad(zoneusers, true);
           return surveyanswer;

       }
       /// <summary>
       /// Get All Survey Head Question
       /// </summary>
       /// <param name="surveyanswer"></param>
       /// <returns></returns>
       public TList<ServeyQuestion> GetAllsurveyDescription()
       {
           string where = string.Empty;
           int Total = 0;
           string orderby = string.Empty;
           TList<ServeyQuestion> surveydescription = new TList<ServeyQuestion>();
           surveydescription = DataRepository.ServeyQuestionProvider.GetPaged(where, orderby, 0, int.MaxValue, out Total);
           //DataRepository.ZoneProvider.DeepLoad(zoneusers, true);
           return surveydescription;

       }
       public string UpdateSurvey(ServeyAnswer surveyanswer)
       {
           TransactionManager transaction = null;

           try
           {
               transaction = DataRepository.Provider.CreateTransaction();
               transaction.BeginTransaction();

               if ((DataRepository.ServeyAnswerProvider.Update(surveyanswer)))
               {

                   // Show proper message
               }
               else
               {
                   return "Information could not be Update.";
               }
               transaction.Commit();
               return "Information Updated successfully.";
           }
           catch (Exception e)
           {
               transaction.Rollback();
               return "Information could not be Update.Please contact Administrator.";
           }
       }

       public ServeyAnswer GetbySurveyid(int id)
       {
           string where = "AnswerID='" + id + "'";
           int Total = 0;
           string orderby = string.Empty;
           TList<ServeyAnswer> Survey = new TList<ServeyAnswer>();
           Survey = DataRepository.ServeyAnswerProvider.GetPaged(where, orderby, 0, int.MaxValue, out Total);
           //DataRepository.ServeyAnswerProvider.DeepLoad(Survey, true);
           foreach (ServeyAnswer temp in Survey)
           {
               return temp;
           }
           return null;

       }
       public TList<ServeyAnswer> GetbyQuestionIdBooking(int id)
       {
           string where = "AnswerID='" + id + "' and AnswerType=1";
           int Total = 0;
           string orderby = string.Empty;
           TList<ServeyAnswer> Survey = new TList<ServeyAnswer>();
           Survey = DataRepository.ServeyAnswerProvider.GetPaged(where, orderby, 0, int.MaxValue, out Total);
           //DataRepository.ServeyAnswerProvider.DeepLoad(Survey, true);

           return Survey;

       }
       public TList<ServeyAnswer> GetbyQuestionIdRequest(int id)
       {
           string where = "AnswerID='" + id + "' and AnswerType=0";
           int Total = 0;
           string orderby = string.Empty;
           TList<ServeyAnswer> Survey = new TList<ServeyAnswer>();
           Survey = DataRepository.ServeyAnswerProvider.GetPaged(where, orderby, 0, int.MaxValue, out Total);
           //DataRepository.ServeyAnswerProvider.DeepLoad(Survey, true);

           return Survey;

       }
       public string GetSurveyNameByid(int id)
       {
           string where = "AnswerID='" + id + "'";
           int Total = 0;
           string orderby = string.Empty;
           TList<ServeyAnswer> Survey = new TList<ServeyAnswer>();
           Survey = DataRepository.ServeyAnswerProvider.GetPaged(where, orderby, 0, int.MaxValue, out Total);
           //DataRepository.ServeyAnswerProvider.DeepLoad(Survey, true);
           foreach (ServeyAnswer temp in Survey)
           {
               string answer = temp.Answer;
               
               return answer;
           }
           return null;

       }

       //Declare Variable to store data
       private TList<ServeyResponse> _varGetAllServeyResponse;

       //Property to Store the TList of Entities.SurveyResponse class
       public TList<ServeyResponse> propGetAllServeyResponse
       {
           get { return _varGetAllServeyResponse; }
           set { _varGetAllServeyResponse = value; }

       }
       /// <summary>
       /// Function to get survey response with all hotels
       /// </summary>
       /// <param name="hotelID"></param>
       /// <returns></returns>
       /// 
       public TList<ServeyResponse> GetSurveyHotelName(string whereclause)
       {
           int Total = 0;           
           propGetAllServeyResponse = DataRepository.ServeyResponseProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
           DataRepository.ServeyResponseProvider.DeepLoad(propGetAllServeyResponse);
           return propGetAllServeyResponse;
       }


       /// <summary>
       /// This function used to delete survey.
       /// </summary>
       /// <param name="agentID"></param>
       /// <returns></returns>
       public bool DeleteServey(int serveyID)
       {
           bool result = false;
           ServeyResponse obj = DataRepository.ServeyResponseProvider.GetByServeyId(serveyID);
           if (obj != null)
           {
               if (DataRepository.ServeyResponseProvider.Delete(obj))
               {
                   result = true;
               }
           }
           return result;
       }

       /// <summary>
       /// This function used to update comment in surveyresponse table.
       /// </summary>
       /// <param name="agentID"></param>
       /// <returns></returns>
       public bool UpdateSurvey(int serveyID)
       {
           bool result = false;
           ServeyResponse obj = DataRepository.ServeyResponseProvider.GetByServeyId(serveyID);
           if (obj != null)
           {
               if (DataRepository.ServeyResponseProvider.Update(obj))
               {
                   result = true;
               }
           }
           return result;
       }

       /// <summary>
       /// Function to get survey result as per servey id
       /// </summary>
       /// <param name="hotelID"></param>
       /// <returns></returns>
       public TList<Serveyresult> GetServeyresult(string where)
       {
           int Total = 0;
           propGetAllServeyresult = DataRepository.ServeyresultProvider.GetPaged(where, string.Empty, 0, int.MaxValue, out Total);
           return propGetAllServeyresult;
       }

       /// <summary>
       //This function to get survey details by unique id
       /// </summary>  
       public ServeyResponse GeyDetailsBySurveyId(int surveyId)
       {
           ServeyResponse propSurvey = DataRepository.ServeyResponseProvider.GetByServeyId(surveyId);
           return propSurvey;
       }

       public bool UpdateServeyResponse(ServeyResponse objServeyResponse)
       {
           if (DataRepository.ServeyResponseProvider.Update(objServeyResponse))
           {
               return true;
           }
           else
           {
               return false;
           }
       }

    }
}
