﻿#region NameSpaces

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;
#endregion

namespace LMMR.Business
{

    public abstract class NewUserBase
    {
        #region Variable declaration

        //public string FirstName
        //{
        //    get;
        //    set;
        //}

        //public string LastName
        //{
        //    get;
        //    set;
        //}

        //public string Email
        //{
        //    get;
        //    set;
        //}

        //public string Password
        //{
        //    get;
        //    set;
        //}

        //public string VATNo
        //{
        //    get;
        //    set;
        //}

        //public string Name
        //{
        //    get;
        //    set;
        //}

        //public string Department
        //{
        //    get;
        //    set;
        //}

        //public string CommunicationEmail
        //{
        //    get;
        //    set;
        //}

        //public string Address
        //{
        //    get;
        //    set;
        //}
        //public string Phone
        //{
        //    get;
        //    set;
        //}
        //public string PostalCode
        //{
        //    get;
        //    set;
        //}
        //public string IATANo
        //{
        //    get;
        //    set;
        //}
        //public string CompanyName
        //{
        //    get;
        //    set;
        //}
        //public int UserType
        //{
        //    get;
        //    set;
        //}
        public TList<Country> propGetCountry
        {
            get;
            set;
        }
        public TList<City> propGetCity
        {
            get;
            set;
        }
        //Property to store meeting room ID
        private int _varHotelID;
        public int propHotelID
        {
            get { return _varHotelID; }
            set { _varHotelID = value; }
        }
        //public string Country
        //{
        //    get;
        //    set;
        //}
        //public string City
        //{
        //    get;
        //    set;
        //}
        //public int CountryId
        //{
        //    get;
        //    set;
        //}

        #endregion


        #region methods

        /// <summary>
        /// Registers the Financial info in case of Agency and company user.
        /// </summary>
        /// <param name="financeInfo"></param>
        /// <returns>string</returns>
        public string register(FinancialInfo financeInfo)
        {
            TransactionManager transaction = null;   
            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();
                if (DataRepository.FinancialInfoProvider.Insert(financeInfo))
                {
                    // Show proper message
                }
                else
                {
                    return "Information could not be saved.";
                }
                transaction.Commit();
                return "Please activate you account by clicking on the link from the email message.";
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return "Information could not be saved.Please contact Administrator.";
            }
        }

        /// <summary>
        /// Overload of the register method.It registers the private user.
        /// </summary>
        /// <param name="newUser"></param>
        /// <param name="details"></param>
        /// <returns>string</returns>
        public string register(Users newUser, UserDetails details)
        {
            TransactionManager transaction = null;
            bool validemail = checkEmail(newUser.EmailId);
            if (validemail) { return "Email Already Exist."; }
            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();
                if ((DataRepository.UsersProvider.Insert(transaction,newUser)))
                {
                    details.UserId = newUser.UserId;
                    
                    
                    DataRepository.UserDetailsProvider.Insert(transaction,details);
                    // Show proper message
                }
                else
                {
                    return "Information could not be saved.";
                }
                transaction.Commit();
                return "Please activate you account by clicking on the link from the email message.";
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return "Information could not be saved.Please contact Administrator.";
            }
        }

        /// <summary>
        /// This method updates the users financial information.
        /// </summary>
        /// <param name="financeInfo"></param>
        /// <returns></returns>
        public string update(FinancialInfo financeInfo, long userId)
        {
            TransactionManager transaction = null;
            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();
                //financeInfo.UserId = userId;
                TList<FinancialInfo> all = DataRepository.FinancialInfoProvider.GetByUserId(userId);
                if (all.Count > 0)
                {
                    foreach (FinancialInfo temp in all)
                    {
                        if (temp.UserId == financeInfo.UserId)
                        {
                            financeInfo.Id = temp.Id;
                            break;
                        }
                    }
                    if (DataRepository.FinancialInfoProvider.Update(financeInfo))
                    {
                        // Show proper message
                    }
                    else
                    {
                        return "Information could not be saved.";
                    }
                    transaction.Commit();
                    return "Information updated successfully.";
                }
                else
                {
                    return register(financeInfo);
                }
                
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return "Information could not be saved.Please contact Administrator.";
            }
        }

        /// <summary>
        /// This is the overload method for updating the user details.
        /// </summary>
        /// <param name="newUser"></param>
        /// <param name="details"></param>
        /// <returns></returns>
        public string update(Users newUser, UserDetails details)
        {
            TransactionManager transaction = null;
            
            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();
                
                if ((DataRepository.UsersProvider.Update(newUser)))
                {
                    details.UserId = newUser.UserId;
                    TList<UserDetails> all = DataRepository.UserDetailsProvider.GetByUserId(details.UserId);
                    foreach (UserDetails temp in all)
                    {
                        if (temp.UserId == details.UserId)
                        { details.Id = temp.Id; break; }
                    }
                    DataRepository.UserDetailsProvider.Update(details);
                    // Show proper message
                }
                else
                {
                    return "Information could not be saved.";
                }
                transaction.Commit();
                return "Information updated successfully.";
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return "Information could not be saved.Please contact Administrator.";
            }
        }


        /// <summary>
        /// Register admin owner link
        /// </summary>
        /// <param name="newUser"></param>
        /// <param name="details"></param>
        /// <returns>string</returns>
        public string RegisterAdminOwner(Users newUser, UserDetails details)
        {
            TransactionManager transaction = null;            
            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();
                if ((DataRepository.UsersProvider.Insert(transaction, newUser)))
                {
                    details.UserId = newUser.UserId;
                    DataRepository.UserDetailsProvider.Insert(transaction, details);                   
                }
                else
                {
                    return "Information could not be saved.";
                }
                transaction.Commit();
                return "Hotel owner link has been successfully created";
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return "Information could not be saved.Please contact Administrator.";
            }
        }
        public string InsertHotelOwner(int Userid, int HotelId)
        {
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                HotelOwnerLink hotel = new HotelOwnerLink();
                hotel.UserId = Userid;
                hotel.HotelId = HotelId;
                if (DataRepository.HotelOwnerLinkProvider.Insert(tm, hotel))
                {
                    propHotelID = Convert.ToInt32(hotel.Id);                   
                }
                else
                {                    
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return "Added";
        }


        /// <summary>
        /// Returns the details of the user on the basis of the userId.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>UserDetails</returns>
        public UserDetails getDetailsByID(long id)
        {
            TList<UserDetails> all = DataRepository.UserDetailsProvider.GetByUserId(id);
            foreach(UserDetails temp in all)
            {
                return temp;
            }
            return null;
        }

        /// <summary>
        /// This method returns the financial info on the basis of the userId.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public FinancialInfo getFinanceInfoByUserID(long id)
        {
            int count;
            string whereClause="UserId="+id;
            TList<FinancialInfo> all = DataRepository.FinancialInfoProvider.GetPaged(whereClause,string.Empty,0,int.MaxValue,out count);
            foreach (FinancialInfo temp in all)
            {
                return temp;
            }
            return null;
        }

        /// <summary>
        /// Returns a list of Countries.
        /// </summary>
        /// <returns>TList<Country></returns>
        public TList<Country> GetByAllCountry()
        {
            int total = 0;
            string whereClause = string.Empty;
            string orderby = CountryColumn.CountryName + " ASC";
            propGetCountry = DataRepository.CountryProvider.GetPaged(whereClause, orderby, 0, int.MaxValue, out total);
            return propGetCountry;
        }

        public TList<Country> GetByAllCountrycms()
        {
            int total = 0;
            string whereClause = CountryColumn.IsActive + "=1 and " + CountryColumn.IsForAll + "=1";
            string OrderBy = CountryColumn.CountryName + " ASC";
            TList<Country> objcityalllist = DataRepository.CountryProvider.GetPaged(whereClause, OrderBy, 0, int.MaxValue, out total);
            return objcityalllist;
        }
        
        /// <summary>
        /// Returns the list of cities on the bases of country code.
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns>TList<City></returns>
        public TList<City> GetCityByCountry(int countryId)
        {
            int Total = 0;
            string whereClause = CityColumn.IsActive + "= 1 and " + CityColumn.CountryId + " in (Select " + CountryColumn.Id + " from Country where " + CountryColumn.IsActive + "=1 and " + CountryColumn.Id + "='"+ countryId +"')";
            string orderby = CityColumn.City + " ASC";
            propGetCity = DataRepository.CityProvider.GetPaged(whereClause, orderby, 0, int.MaxValue, out Total);
            return propGetCity;
        }

        /// <summary>
        /// Validates the duplicacy of email.
        /// </summary>
        /// <param name="email"></param>
        /// <returns>true/false</returns>
       

        #region checkemail Exist
        public bool checkEmail(string email)
        {
            int count = 0;
            string whereClause = UsersColumn.EmailId + "='" + email + "' AND isnull(" + UsersColumn.IsRemoved + ",0) = 0";
            TList<Users> objUsers = DataRepository.UsersProvider.GetPaged(whereClause, string.Empty, 0, int.MaxValue, out count);
            if (objUsers.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
            //int count = 0;
            //bool val = true;
            //TList<Users> objstaffuser = DataRepository.UsersProvider.GetPaged(UsersColumn.EmailId + "='" + email + "'", string.Empty, 0, int.MaxValue, out count);
            //if (objstaffuser.Count > 0)
            //{
            //    val = false;
            //    foreach (Users objuser in objstaffuser)
            //    {
                    
            //        //TList<UserDetails> objuseremail = DataRepository.UserDetailsProvider.GetByUserId(objuser.UserId);
            //        string orderby = "IsDeleted desc";
            //        TList<UserDetails> objuseremail = DataRepository.UserDetailsProvider.GetPaged(UserDetailsColumn.UserId + "='" + objuser.UserId + "'",orderby, 0, int.MaxValue, out count);
            //        if (objuseremail.Count > 0)
            //        {
            //            foreach (UserDetails objuserdetail in objuseremail)
            //            {

            //                if (objuserdetail.IsDeleted == true)
            //                {
            //                    val = true;
            //                }
            //                else
            //                {
            //                    val = false;
            //                    return val;
            //                }
            //            }
            //        }
                    
            //    }
                 
            //}

            //return val;

            
        }
        #endregion

        

        /// <summary>
        /// This method returns the list of all users from the database
        /// </summary>
        /// <returns>TList<Users></returns>
        public Users GetUserByID(long userID)
        {
            Users getUser = DataRepository.UsersProvider.GetByUserId(userID);
            return getUser;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>TList<Users></returns>
        public void  UpdateUser(Users objUser)
        {
            DataRepository.UsersProvider.Update(objUser);
        }

        /// <summary>
        /// returns a list of all the countries
        /// </summary>
        /// <returns>TList<Language></returns>
        public TList<Language> GetAllLanguages()
        {
            TList<Language> Obj = DataRepository.LanguageProvider.GetAll();
            return Obj;
        }

        /// <summary>
        /// This method updates the user details.
        /// </summary>
        /// <param name="details"></param>
        public void updateDetails(UserDetails details)
        {
            DataRepository.UserDetailsProvider.Update(details);
        }

        /// <summary>
        /// This method returns the welcome message.
        /// </summary>
        /// <param name="CmsType"></param>
        /// <param name="languageID"></param>
        /// <returns>string</returns>
        public string getWelcomeMessage(string CmsType, int languageID)
        {
            int count;
            Cms c = DataRepository.CmsProvider.GetPaged("CmsType='FrontEndWelcomeMessage'", string.Empty, 0, int.MaxValue, out count).FirstOrDefault();
            long cmsId = 0;
            string Result = "";
            if(c!=null)
            {
                cmsId = c.Id;
                CmsDescription cd = DataRepository.CmsDescriptionProvider.GetPaged("CmsId=" + cmsId + " and LanguageId=" + languageID, string.Empty, 0, int.MaxValue, out count).FirstOrDefault();
                if(cd!=null)
                {
                    Result = cd.ContentsDesc;
                }
                else{
                    Result = "";
                }
            }
            else
            {
                Result = "";
            }

            return Result;
        }

        /// <summary>
        /// This function for get question by countryID.
        /// </summary>
        /// <param name="intCountryID"></param>
        /// <returns></returns>
        public TList<HearUsQuestion> GetQuestionByCountryID(int intCountryID)
        {
            TList<HearUsQuestion> objHear = DataRepository.HearUsQuestionProvider.GetByCountryId(intCountryID);
            return objHear;
        }

        /// <summary>
        /// This method returns all the zones of a particular city by cityID.
        /// </summary>
        /// <param name="intCountryID"></param>
        /// <returns></returns>

        public TList<Zone> GetZonesByCityID(long CityId)
        {
            TList<Zone> list = DataRepository.ZoneProvider.GetByCityId(CityId);
            return list;
        }

        public string InsertRPF(Rpf objrpf)
        {
            TransactionManager transaction = null;
            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();
                if (DataRepository.RpfProvider.Insert(objrpf))
                {
                    // Show proper message
                }
                else
                {
                    return "RPF could not be saved.";
                }
                transaction.Commit();
                return "RPF successfully Saved";
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return "Information could not be saved.Please contact Administrator.";
            }
        }
       
        #endregion
    }
}
