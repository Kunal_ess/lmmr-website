﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Data;
using LMMR.Entities;

namespace LMMR.Business
{
    public enum DashboardLinks
    {
        lnkGeneralInfo = 1,
        lnkMeetingRooms = 2,
        lnkBedRooms = 3,
        lnkPricing = 4,
        lnkPictureManagement = 5,
        lnkGoOnline =6,
        lnkUpdateAvailability = 7,
        lnkNewBooking = 8,
        lnkOldbooking = 9,
        lnkDetailedSearch = 10,
        lnkInformation = 11,
        lnkInvoices = 12,
        lnkInvoicingDetails = 13,
        lnkRoomRate = 14,
        lnkPromotion = 15,
        lnkAddPromotion = 16,
        lnkNewRequest = 17,
        lnkSummary = 18,
        lnkDetailsearch = 19,
        lnkNews = 20,
        lnkGdt = 21,
        lnkVisitors = 22,
        lnkConversion = 23,
        lnkLeadtime = 24,
        lnkFuturemarketconditions = 25,
        lnkchangePassword = 26
    }



    public abstract class DashboardManagerBase
    {

        #region Variables
        // Variables to store data
        private DashboardLink _varGetDashbordLink;

        //Variable to store TList of Hotal Class
        private TList<DashboardLink> _varGetAllDashboardLink;

        #endregion

        #region Properties

        // Property to store the data of Entities.Hotal class
        public DashboardLink _propDashboardLink
        {
            get { return _varGetDashbordLink; }
            set { _varGetDashbordLink = value; }
        }

        // Property To Store the TList of Entities.Hotal class
        public TList<DashboardLink> _listDashboardLink
        {
            get { return _varGetAllDashboardLink; }
            set { _varGetAllDashboardLink = value; }
        }

        #endregion

        #region Functions
        /// <summary>
        /// Get list of dashboard link by hotelID
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public TList<DashboardLink> GetLinkByHotelID(int hotelID)
        {
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                _listDashboardLink = DataRepository.DashboardLinkProvider.GetByHotelId(tm,hotelID);
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return _listDashboardLink;
        }
       
        /// <summary>
        /// this function use for get min value from enum.
        /// </summary>
        /// <returns>int</returns>
        public int GetMinDashboardID()
        {

            int dashboardlinkMin = (int)Enum.GetValues(typeof(DashboardLinks)).Cast<DashboardLinks>().Min();

            return dashboardlinkMin;
        }

        /// <summary>
        /// this function use for get max value from enum.
        /// </summary>
        /// <returns></returns>
        public int GetMaxDashboardID()
        {

            int dashboardlinkMax = (int)Enum.GetValues(typeof(DashboardLinks)).Cast<DashboardLinks>().Max();

            return dashboardlinkMax;

        }

        /// <summary>
        /// this function use for get min value from enum.
        /// </summary>
        /// <returns></returns>
        public int GetMinPropertyLevelSectionID()
        {

            int PropertyLevelSectionMin = (int)Enum.GetValues(typeof(PropertyLevelSection)).Cast<PropertyLevelSection>().Min();

            return PropertyLevelSectionMin;

        }

        /// <summary>
        /// this function use for get max value from enum.
        /// </summary>
        /// <returns></returns>
        public int GetMaxPropertyLevelSectionID()
        {

            int PropertyLevelSectionMax = (int)Enum.GetValues(typeof(PropertyLevelSection)).Cast<PropertyLevelSection>().Max();

            return PropertyLevelSectionMax;

        }

        /// <summary>
        /// this fiuntion use for insert pageID into table. 
        /// </summary>
        /// <param name="hotelID"></param>
        /// <param name="dashboardLinkID"></param>
        /// <returns></returns>
        public string DoneThisStep(int hotelID, int dashboardLinkID)
        {
            TransactionManager tm = null;
            string Result = string.Empty;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                TList<DashboardLink> ObjGetDashboardMenu = DataRepository.DashboardLinkProvider.GetByHotelId(hotelID);
                if (ObjGetDashboardMenu.Count > 0)
                {
                    var row = ObjGetDashboardMenu.Find(a => a.DashBoardLinkId == dashboardLinkID);

                    if (row == null)
                    {
                        DashboardLink ObjDashboard = new DashboardLink();
                        ObjDashboard.HotelId = hotelID;
                        ObjDashboard.DashBoardLinkId = dashboardLinkID;

                        if (DataRepository.DashboardLinkProvider.Insert(tm,ObjDashboard))
                        {
                            Result = "Insert Successfully";
                        }
                        else
                        {

                            Result = "Try Again";

                        }
                    }

                }
                else
                {
                    DashboardLink ObjDashboard = new DashboardLink();
                    ObjDashboard.HotelId = hotelID;
                    ObjDashboard.DashBoardLinkId = dashboardLinkID;

                    if (DataRepository.DashboardLinkProvider.Insert(tm,ObjDashboard))
                    {
                        Result = "Insert Successfully";
                    }
                    else
                    {
                        Result = "Try Again";
                    }
                }

                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }

            return Result;
        }

        #endregion
    }
}
