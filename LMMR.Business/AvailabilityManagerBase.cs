﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;
using log4net;
using log4net.Config;
using System.Web;
#endregion
namespace LMMR.Business
{
    public abstract class AvailabilityManagerBase
    {
        #region variables
        ILog logger = log4net.LogManager.GetLogger(typeof(AvailabilityManagerBase));
        #endregion

        #region properties

        #endregion

        #region Constractor
        public AvailabilityManagerBase()
        {

        }
        #endregion

        #region Methods

        #region add new
            
        #endregion

        #region get methods
        /// <summary>
        /// This methos is used to get the result of all the properties by hotel and for specif time period.
        /// </summary>
        /// <param name="HotelId">Hotel id is integior value.</param>
        /// <param name="AvailabilityDate">Availability of date is string value.</param>
        /// <param name="StartDate">Start date is string value.</param>
        /// <param name="EndDate">End date is string value.</param>
        /// <returns>Return value is a VList of AvailabilityWithSpandp.</returns>
        public VList<AvailabilityWithSpandp> GetFullPropertDetails(int HotelId, string AvailabilityDate, string StartDate, string EndDate)
        {
            VList<AvailabilityWithSpandp> objAvailabilitywithspandp = new VList<AvailabilityWithSpandp>();
            int counttotal = 0;
            try
            {
                string whereclause = AvailabilityWithSpandpColumn.HotelId + "=" + HotelId + " and " + AvailabilityWithSpandpColumn.AvailabilityDate + " like '" + AvailabilityDate + "%' and " + AvailabilityWithSpandpColumn.AvailabilityDate + " between '" + StartDate + "' and '" + EndDate + "'";
                string orderbyclause = string.Empty;
                objAvailabilitywithspandp = DataRepository.AvailabilityWithSpandpProvider.GetPaged(whereclause, orderbyclause, 0, int.MaxValue, out counttotal);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return objAvailabilitywithspandp;
        }

        /// <summary>
        /// Get all Meeting room details by hotel id.
        /// </summary>
        /// <param name="HotelId">Hotel id is a integer value.</param>
        /// <param name="AvailabilityDate">Availability date is a string value.</param>
        /// <param name="StartDate">Start date is a string value.</param>
        /// <param name="EndDate">End date is a string value.</param>
        /// <returns>Return value is a VList of ViewAvailabilityOfRooms.</returns>
        public VList<ViewAvailabilityOfRooms> GetAllMeetingRoomDetailsByHotelID(int HotelId, string AvailabilityDate, string StartDate, string EndDate, Int64 meetingroomid)
        {
            VList<ViewAvailabilityOfRooms> objAvailabilitywithspandp = new VList<ViewAvailabilityOfRooms>();
            int counttotal = 0;
            try
            {
                string whereclause = " Hotel_Id=" + HotelId + " and " + ViewAvailabilityOfBedRoomColumn.RoomId + "=" + meetingroomid + " and " + ViewAvailabilityOfRoomsColumn.AvailabilityDate + " like '" + AvailabilityDate + "%' and " + ViewAvailabilityOfRoomsColumn.AvailabilityDate + " between '" + StartDate + "' and '" + EndDate + "'";
                string orderbyclause = string.Empty;
                objAvailabilitywithspandp = DataRepository.ViewAvailabilityOfRoomsProvider.GetPaged(whereclause, orderbyclause, 0, int.MaxValue, out counttotal);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return objAvailabilitywithspandp;
        }

        /// <summary>
        /// Get all beadroom details by hotel id and for perticular time period.
        /// </summary>
        /// <param name="HotelId">Hotel id is a integer value.</param>
        /// <param name="AvailabilityDate">Availability date is a string value</param>
        /// <param name="StartDate">Start date is a string value.</param>
        /// <param name="EndDate">End date is a string value.</param>
        /// <returns>Return value is a VList of ViewAvailabilityOfBedRoom.</returns>
        public VList<ViewAvailabilityOfBedRoom> GetAllBedRoomDetailsByHotelID(int HotelId, string AvailabilityDate, string StartDate, string EndDate)
        {
            VList<ViewAvailabilityOfBedRoom> objAvailabilitywithspandp = new VList<ViewAvailabilityOfBedRoom>();
            int counttotal = 0;
            try
            {
                string whereclause = " HotelId=" + HotelId + " and " + ViewAvailabilityOfBedRoomColumn.AvailabilityDate + " like '" + AvailabilityDate + "%' and " + ViewAvailabilityOfBedRoomColumn.AvailabilityDate + " between '" + StartDate + "' and '" + EndDate + "'";
                string orderbyclause = string.Empty;
                objAvailabilitywithspandp = DataRepository.ViewAvailabilityOfBedRoomProvider.GetPaged(whereclause, orderbyclause, 0, int.MaxValue, out counttotal);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return objAvailabilitywithspandp;
        }

        /// <summary>
        /// Get availability of Room by hotel id, Availability id and Availability date.(this function not is use now we need this functionality on next phase)
        /// </summary>
        /// <param name="HotelId">Hotel id is a integer value.</param>
        /// <param name="AvailabilityId">Availability id is a integer value.</param>
        /// <param name="availabilityDate">Availability date is a string value.</param>
        /// <returns>Return value is a List of Availability of rooms.</returns>
        public List<AvailibilityOfRooms> GetAvailabilityOfRoom(int HotelId, int AvailabilityId, string availabilityDate)
        {
            List<AvailibilityOfRooms> objAvailabilityofRooms = new List<AvailibilityOfRooms>();
            return objAvailabilityofRooms;
        }

        /// <summary>
        /// Get all Meeting room details by hotel id.
        /// </summary>
        /// <param name="HotelId">Hotel id is a integer value.</param>
        /// <param name="AvailabilityDate">Availability date is a string value.</param>
        /// <param name="StartDate">Start date is a string value.</param>
        /// <param name="EndDate">End date is a string value.</param>
        /// <returns>Return value is a VList of ViewAvailabilityOfRooms.</returns>
        public VList<ViewAvailabilityOfRooms> GetMeetingRoomByHotelIDForTransfer(int HotelId, string AvailabilityDate, Int64 meetingroomid)
        {
            VList<ViewAvailabilityOfRooms> objAvailabilitywithspandp = new VList<ViewAvailabilityOfRooms>();
            int counttotal = 0;
            try
            {
                string whereclause = " Hotel_Id=" + HotelId + " and " + ViewAvailabilityOfBedRoomColumn.RoomId + "=" + meetingroomid + " and " + ViewAvailabilityOfRoomsColumn.AvailabilityDate + "='" + AvailabilityDate + "'";
                string orderbyclause = string.Empty;
                objAvailabilitywithspandp = DataRepository.ViewAvailabilityOfRoomsProvider.GetPaged(whereclause, orderbyclause, 0, int.MaxValue, out counttotal);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return objAvailabilitywithspandp;
        }
        #endregion

        #region Update
        /// <summary>
        /// Update full property details of beadroom by Property Id, date of availability and status.
        /// </summary>
        /// <param name="Propid">Property id is a long value.</param>
        /// <param name="dtavailability">Date of availability is a datetime value.</param>
        /// <param name="status">Status is a string value.</param>
        /// <returns>Return value is type of string.</returns>
        public string UpdateFullPropertyBedRoom(long Propid, DateTime dtavailability, string status)
        {
            Availability avail = DataRepository.AvailabilityProvider.GetById(Propid);
            string result = "";
            Guid gid = Guid.NewGuid();
            TransactionManager tm = null;
            string currentUser = "";
            try
            {
                currentUser = Convert.ToString(HttpContext.Current.Session["CurrentUserID"] == null ? "1" : HttpContext.Current.Session["CurrentUserID"]);
            }
            catch
            {
                currentUser = "1";
            }
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                string whereclause = AvailibilityOfRoomsColumn.AvailibilityIdForBedRoom + "=" + Propid + " and " + AvailibilityOfRoomsColumn.RoomType + "=1";
                int count = 0;
                TList<AvailibilityOfRooms> availRooms = DataRepository.AvailibilityOfRoomsProvider.GetPaged(tm, whereclause, string.Empty, 0, int.MaxValue, out count);
                int TotalCount = availRooms.Count;
                int counter = 0;
                int intStatus = 0;
                switch (status)
                {
                    case "closed":
                        intStatus = (int)AvailabilityStatus.CLOSED;
                        break;
                    case "opened":
                        intStatus = (int)AvailabilityStatus.AVAILABLE;
                        break;
                    case "booked":
                        intStatus = (int)AvailabilityStatus.BOOKED;
                        break;
                    case "sold":
                        intStatus = (int)AvailabilityStatus.SOLD;
                        break;
                    default:
                        intStatus = (int)AvailabilityStatus.AVAILABLE;
                        break;
                }
                foreach (AvailibilityOfRooms avMR in availRooms)
                {
                    avMR.MorningStatus = intStatus;
                    AvailibilityOfRooms old = avMR.GetOriginalEntity();
                    old.Id = avMR.Id;
                    try
                    {
                        TrailManager.LogAuditTrail<AvailibilityOfRooms>(tm, avMR, old, AuditAction.U, Convert.ToInt64(currentUser), Convert.ToInt32(PageType.Availability), avMR.TableName, Convert.ToString(avail.HotelId));
                    }
                    catch
                    {

                    }
                    if (DataRepository.AvailibilityOfRoomsProvider.Update(tm,avMR))
                    {
                        counter++;
                    }
                }
                if (counter == TotalCount)
                {
                    avail.FullProrertyStatusBr = intStatus;
                    Availability old = avail.GetOriginalEntity();
                    old.Id = avail.Id;
                    try
                    {
                        TrailManager.LogAuditTrail<Availability>(tm, avail, old, AuditAction.U, Convert.ToInt64(currentUser), Convert.ToInt32(PageType.Availability), avail.TableName, Convert.ToString(avail.HotelId));
                    }
                    catch
                    {
                    }
                    if (DataRepository.AvailabilityProvider.Update(tm,avail))
                    {
                        result = "Update";
                    }
                    else
                    {
                        result = "Error";
                    }
                }
                else
                {
                    result = "Error";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
                logger.Error(ex);
            }
            return result;
        }

        /// <summary>
        /// Update Full property meeting room only if any room status changes
        /// </summary>
        /// <param name="Propid"></param>
        /// <param name="dtavailability"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public string UpdateFullPropertyMeetingRoomOnly(long Propid, DateTime dtavailability, string status)
        {
            Availability avail = DataRepository.AvailabilityProvider.GetById(Propid);
            string result = "";
            TransactionManager tm = null;
            Guid gid = Guid.NewGuid();
            string currentUser = "";
            try
            {
                currentUser = Convert.ToString(HttpContext.Current.Session["CurrentUserID"] == null ? "1" : HttpContext.Current.Session["CurrentUserID"]);
            }
            catch
            {
                currentUser = "1";
            }
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                int intStatus = 0;
                switch (status)
                {
                    case "closed":
                        intStatus = (int)AvailabilityStatus.CLOSED;
                        break;
                    case "opened":
                        intStatus = (int)AvailabilityStatus.AVAILABLE;
                        break;
                    case "booked":
                        intStatus = (int)AvailabilityStatus.BOOKED;
                        break;
                    case "sold":
                        intStatus = (int)AvailabilityStatus.SOLD;
                        break;
                    default:
                        intStatus = (int)AvailabilityStatus.AVAILABLE;
                        break;
                }
                avail.FullPropertyStatus = intStatus;
                Availability old = avail.GetOriginalEntity();
                old.Id = avail.Id;
                try
                {
                    TrailManager.LogAuditTrail<Availability>(tm, avail, old, AuditAction.U, Convert.ToInt64(currentUser), Convert.ToInt32(PageType.Availability), avail.TableName, Convert.ToString(avail.HotelId));
                }
                catch
                {
                }
                if (DataRepository.AvailabilityProvider.Update(tm,avail))
                {
                    result = "Update";
                }
                else
                {
                    result = "Error";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
                logger.Error(ex);
            }
            return result;
        }
        /// <summary>
        /// Update full property meeting room by property id, date of availability and status.
        /// </summary>
        /// <param name="Propid">Property Id is a long value.</param>
        /// <param name="dtavailability">Date of availability is a datetime value.</param>
        /// <param name="status">Status is a string value.</param>
        /// <returns>Return value is type of string.</returns>
        public string UpdateFullPropertyMeetingRoom(long Propid, DateTime dtavailability, string status)
        {
            Availability avail = DataRepository.AvailabilityProvider.GetById(Propid);
            string result = "";
            TransactionManager tm = null;
            Guid gid = Guid.NewGuid();
            string currentUser = "";
            try
            {
                currentUser = Convert.ToString(HttpContext.Current.Session["CurrentUserID"] == null ? "1" : HttpContext.Current.Session["CurrentUserID"]);
            }
            catch
            {
                currentUser = "1";
            }
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                string whereclause = AvailibilityOfRoomsColumn.AvailibilityIdForBedRoom + "=" + Propid + " and " + AvailibilityOfRoomsColumn.RoomType + "=0";
                int count = 0;
                TList<AvailibilityOfRooms> availRooms = DataRepository.AvailibilityOfRoomsProvider.GetPaged(tm, whereclause, string.Empty, 0, int.MaxValue, out count);
                string whereclauseBR = AvailibilityOfRoomsColumn.AvailibilityIdForBedRoom + "=" + Propid + " and " + AvailibilityOfRoomsColumn.RoomType + "=1";
                TList<AvailibilityOfRooms> availRoomsBR = DataRepository.AvailibilityOfRoomsProvider.GetPaged(tm, whereclauseBR, string.Empty, 0, int.MaxValue, out count);
                int TotalCount = availRooms.Count;
                int counter = 0;
                int intStatus = 0;
                switch (status)
                {
                    case "closed":
                        intStatus = (int)AvailabilityStatus.CLOSED;
                        break;
                    case "opened":
                        intStatus = (int)AvailabilityStatus.AVAILABLE;
                        break;
                    case "booked":
                        intStatus = (int)AvailabilityStatus.BOOKED;
                        break;
                    case "sold":
                        intStatus = (int)AvailabilityStatus.SOLD;
                        break;
                    default:
                        intStatus = (int)AvailabilityStatus.AVAILABLE;
                        break;
                }
                foreach (AvailibilityOfRooms avMR in availRooms)
                {
                    avMR.MorningStatus = intStatus;
                    avMR.AfternoonStatus = intStatus;
                    AvailibilityOfRooms old = avMR.GetOriginalEntity();
                    old.Id = avMR.Id;
                    try
                    {
                        TrailManager.LogAuditTrail<AvailibilityOfRooms>(tm, avMR, old, AuditAction.U, Convert.ToInt64(currentUser), Convert.ToInt32(PageType.Availability), avMR.TableName, Convert.ToString(avail.HotelId));
                    }
                    catch
                    {
                    }
                    if (DataRepository.AvailibilityOfRoomsProvider.Update(tm, avMR))
                    {
                        counter++;
                    }
                }
                foreach (AvailibilityOfRooms avBr in availRoomsBR)
                {
                    avBr.MorningStatus = intStatus;
                    avBr.AfternoonStatus = intStatus;
                    AvailibilityOfRooms old = avBr.GetOriginalEntity();
                    old.Id = avBr.Id;
                    try
                    {
                        TrailManager.LogAuditTrail<AvailibilityOfRooms>(tm, avBr, old, AuditAction.U, Convert.ToInt64(currentUser), Convert.ToInt32(PageType.Availability), avBr.TableName, Convert.ToString(avail.HotelId));
                    }
                    catch
                    {
                    }
                    DataRepository.AvailibilityOfRoomsProvider.Update(tm, avBr);
                }
                if (counter == TotalCount)
                {
                    avail.FullPropertyStatus = intStatus;
                    Availability  old = avail.GetOriginalEntity();
                    old.Id = avail.Id;
                    try
                    {
                        TrailManager.LogAuditTrail<Availability>(tm, avail, old, AuditAction.U, Convert.ToInt64(currentUser), Convert.ToInt32(PageType.Availability), avail.TableName, Convert.ToString(avail.HotelId));
                    }
                    catch
                    {
                    }
                    if (DataRepository.AvailabilityProvider.Update(tm, avail))
                    {
                        result = "Update";
                    }
                    else
                    {
                        result = "Error";
                    }
                }
                else
                {
                    result = "Error";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
                logger.Error(ex);
            }
            return result;
        }

        /// <summary>
        /// Update Current beadroom availability by availability of room id, datetime, type of status and status.
        /// </summary>
        /// <param name="availid">availability id is a long value.</param>
        /// <param name="dtavailability">Date of availability is a datetime value.</param>
        /// <param name="typestatus">type of status is a string value.</param>
        /// <param name="status">Status is a string value.</param>
        /// <returns>Return value is a string value.</returns>
        public string UpdateCurrentBRAvailability(long availid, DateTime dtavailability, string typestatus, string status)
        {            
            AvailibilityOfRooms availroom = DataRepository.AvailibilityOfRoomsProvider.GetById(availid);
            Availability availId = DataRepository.AvailabilityProvider.GetById(availroom.AvailibilityIdForBedRoom);
            TransactionManager tm = null;
            string result = "";
            string currentUser = "";
            try
            {
                currentUser = Convert.ToString(HttpContext.Current.Session["CurrentUserID"] == null ? "1" : HttpContext.Current.Session["CurrentUserID"]);
            }
            catch
            {
                currentUser = "1";
            }
            Guid gid = Guid.NewGuid();
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                int intStatus = 0;
                switch (status)
                {
                    case "closed":
                        intStatus = (int)AvailabilityStatus.CLOSED;
                        break;
                    case "opened":
                        intStatus = (int)AvailabilityStatus.AVAILABLE;
                        break;
                    case "booked":
                        intStatus = (int)AvailabilityStatus.BOOKED;
                        break;
                    case "sold":
                        intStatus = (int)AvailabilityStatus.SOLD;
                        break;
                    default:
                        intStatus = (int)AvailabilityStatus.AVAILABLE;
                        break;
                }
                availroom.MorningStatus = intStatus;
                AvailibilityOfRooms old = availroom.GetOriginalEntity();
                old.Id = availroom.Id;
                try
                {
                    TrailManager.LogAuditTrail<AvailibilityOfRooms>(tm, availroom, old, AuditAction.U, Convert.ToInt64(currentUser), Convert.ToInt32(PageType.Availability), availroom.TableName, Convert.ToString(availId.HotelId));
                }
                catch
                {
                }
                if (DataRepository.AvailibilityOfRoomsProvider.Update(tm, availroom))
                {
                    result = "Update";
                }
                else
                {
                    result = "Error";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
                logger.Error(ex);
            }
            return result;
        }


        /// <summary>
        /// Update the current availability according to availability details.
        /// </summary>
        /// <param name="availid"></param>
        /// <param name="dtavailability"></param>
        /// <param name="typestatus"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public string UpdateCurrentAvailability(long availid, DateTime dtavailability, string typestatus, string status)
        {
            AvailibilityOfRooms availroom = DataRepository.AvailibilityOfRoomsProvider.GetById(availid);
            Availability availId = DataRepository.AvailabilityProvider.GetById(availroom.AvailibilityIdForBedRoom);
            string result = "";
            string currentUser = "";
            try
            {
                currentUser = Convert.ToString(HttpContext.Current.Session["CurrentUserID"] == null ? "1" : HttpContext.Current.Session["CurrentUserID"]);
            }
            catch
            {
                currentUser = "1";
            }
            Guid gid = Guid.NewGuid();
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                int intStatus = 0;
                switch (status)
                {
                    case "closed":
                        intStatus = (int)AvailabilityStatus.CLOSED;
                        break;
                    case "opened":
                        intStatus = (int)AvailabilityStatus.AVAILABLE;
                        break;
                    case "booked":
                        intStatus = (int)AvailabilityStatus.BOOKED;
                        break;
                    case "sold":
                        intStatus = (int)AvailabilityStatus.SOLD;
                        break;
                    default:
                        intStatus = (int)AvailabilityStatus.AVAILABLE;
                        break;
                }
                if (typestatus == "Morning")
                {
                    availroom.MorningStatus = intStatus;
                }
                else if(typestatus == "Fullday")
                {
                    availroom.AfternoonStatus = intStatus;
                    availroom.MorningStatus = intStatus;
                }
                else
                {
                    availroom.AfternoonStatus = intStatus;
                }
                AvailibilityOfRooms old = availroom.GetOriginalEntity();
                old.Id = availroom.Id;
                try
                {
                    TrailManager.LogAuditTrail<AvailibilityOfRooms>(tm, availroom, old, AuditAction.U, Convert.ToInt64(currentUser), Convert.ToInt32(PageType.Availability), availroom.TableName, Convert.ToString(availId.HotelId));
                }
                catch
                {
                }
                if (DataRepository.AvailibilityOfRoomsProvider.Update(tm,availroom))
                {
                    result = "Update";
                }
                else
                {
                    result = "Error";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
                logger.Error(ex);
            }
            return result;
        }

        /// <summary>
        /// Adjust meetingroom by Meetingroom id and Date.
        /// </summary>
        /// <param name="MRId"></param>
        /// <param name="dtAvailDate"></param>
        /// <param name="closingstatus"></param>
        /// <param name="openingstatus"></param>
        /// <returns></returns>
        public string AdjustMeetingRoombyMRIDandDate(int MRId, DateTime dtAvailDate, string closingstatus, string openingstatus)
        {
            int totalCount = 0;
            TransactionManager tm = null;
            string currentUser = "";
            try
            {
                currentUser = Convert.ToString(HttpContext.Current.Session["CurrentUserID"] == null ? "1" : HttpContext.Current.Session["CurrentUserID"]);
            }
            catch
            {
                currentUser = "1";
            }

            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Guid gid = Guid.NewGuid();
                string WhereClaus = ViewAvailabilityOfRoomsColumn.RoomId + "=" + MRId + " and " + ViewAvailabilityOfRoomsColumn.AvailabilityDate + "='" + dtAvailDate + "'";
                VList<ViewAvailabilityOfRooms> objviewAvailability = DataRepository.ViewAvailabilityOfRoomsProvider.GetPaged(tm, WhereClaus, string.Empty, 0, int.MaxValue, out totalCount);
                TList<AvailibilityOfRooms> ObjFinal = new TList<AvailibilityOfRooms>();
                foreach (ViewAvailabilityOfRooms v in objviewAvailability)
                {
                    AvailibilityOfRooms objRoom = DataRepository.AvailibilityOfRoomsProvider.GetById(tm, v.Id);
                    if (objRoom.MorningStatus == 0 || objRoom.MorningStatus == 1)
                    {
                        objRoom.MorningStatus = Convert.ToInt32(closingstatus);
                    }
                    if (objRoom.AfternoonStatus == 0 || objRoom.AfternoonStatus == 1)
                    {
                        objRoom.AfternoonStatus = Convert.ToInt32(closingstatus);
                    }
                    
                    AvailibilityOfRooms old = objRoom.GetOriginalEntity();
                    old.Id = objRoom.Id;
                    try
                    {
                        TrailManager.LogAuditTrail<AvailibilityOfRooms>(tm, objRoom, old, AuditAction.U, Convert.ToInt64(currentUser), Convert.ToInt32(PageType.Availability), objRoom.TableName, Convert.ToString(v.HotelId));
                    }
                    catch
                    {
                    }
                    ObjFinal.Add(objRoom);
                }
                DataRepository.AvailibilityOfRoomsProvider.Update(tm, ObjFinal);
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
                logger.Error(ex);
            }
            return "Update";
        }

        /// <summary>
        /// Adjust of availability according to bedroom id and date.
        /// </summary>
        /// <param name="BRId"></param>
        /// <param name="dtAvailDate"></param>
        /// <param name="closingstatus"></param>
        /// <param name="openingstatus"></param>
        /// <returns></returns>
        public string AdjustBedroombyBRIDandDate(int BRId, DateTime dtAvailDate, string closingstatus, string openingstatus)
        {
            int totalCount = 0;
            TransactionManager tm = null;
            string currentUser = "";
            try
            {
                currentUser = Convert.ToString(HttpContext.Current.Session["CurrentUserID"] == null ? "1" : HttpContext.Current.Session["CurrentUserID"]);
            }
            catch
            {
                currentUser = "1";
            }
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Guid gid = Guid.NewGuid();
                string WhereClaus = ViewAvailabilityOfBedRoomColumn.RoomId + "=" + BRId + " and " + ViewAvailabilityOfBedRoomColumn.AvailabilityDate + "='" + dtAvailDate + "'";
                VList<ViewAvailabilityOfBedRoom> objviewAvailability = DataRepository.ViewAvailabilityOfBedRoomProvider.GetPaged(tm, WhereClaus, string.Empty, 0, int.MaxValue, out totalCount);
                foreach (ViewAvailabilityOfBedRoom v in objviewAvailability)
                {
                    AvailibilityOfRooms objRoom = DataRepository.AvailibilityOfRoomsProvider.GetById(tm, v.Id);
                    if (objRoom.MorningStatus == 0 || objRoom.MorningStatus == 1)
                    {
                        objRoom.MorningStatus = Convert.ToInt32(closingstatus);
                    }
                    AvailibilityOfRooms old = objRoom.GetOriginalEntity();
                    old.Id = objRoom.Id;
                    try
                    {
                        TrailManager.LogAuditTrail<AvailibilityOfRooms>(tm, objRoom, old, AuditAction.U, Convert.ToInt64(currentUser), Convert.ToInt32(PageType.ContactDetails), objRoom.TableName, Convert.ToString(v.HotelId));
                    }
                    catch
                    {
                    }
                    DataRepository.AvailibilityOfRoomsProvider.Update(tm, objRoom);
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
                logger.Error(ex);
            }
            return "Update";
        }
        #endregion

        public void UpdateForFullDay(int SelectedTime, DateTime ArivalDate, DateTime DepartureDate, long HotelID, long MRId)
        {
            TransactionManager tm = null;
            string currentUser = "";
            try
            {
                currentUser = Convert.ToString(HttpContext.Current.Session["CurrentUserID"] == null ? "1" : HttpContext.Current.Session["CurrentUserID"]);
            }
            catch
            {
                currentUser = "1";
            }
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Guid gid = Guid.NewGuid();
                string whereClause = AvailabilityColumn.AvailabilityDate + " between '" + ArivalDate + "' and '" + DepartureDate + "' and " + AvailabilityColumn.HotelId + "=" + HotelID;
                int count = 0;
                TList<Availability> avail = DataRepository.AvailabilityProvider.GetPaged(tm, whereClause, string.Empty, 0, int.MaxValue, out count);
                foreach (Availability a in avail)
                {
                    int count2 = 0;
                    string where = AvailibilityOfRoomsColumn.AvailibilityIdForBedRoom + "=" + a.Id + " and " + AvailibilityOfRoomsColumn.RoomId + "=" + MRId + " and " + AvailibilityOfRoomsColumn.RoomType + "=0";
                    TList<AvailibilityOfRooms> availRoom = DataRepository.AvailibilityOfRoomsProvider.GetPaged(tm, where, string.Empty, 0, int.MaxValue, out count2);
                    foreach (AvailibilityOfRooms aor in availRoom)
                    {
                        if (SelectedTime == 0 || SelectedTime == 1)
                        {
                            aor.MorningStatus = (int)AvailabilityStatus.AVAILABLE;
                        }
                        if (SelectedTime == 0 || SelectedTime == 2)
                        {
                            aor.AfternoonStatus = (int)AvailabilityStatus.AVAILABLE;
                        }
                        aor.IsReserved = false;
                        AvailibilityOfRooms old = aor.GetOriginalEntity();
                        old.Id = aor.Id;
                        try
                        {
                            TrailManager.LogAuditTrail<AvailibilityOfRooms>(tm, aor, old, AuditAction.U, Convert.ToInt64(currentUser), Convert.ToInt32(PageType.ContactDetails), aor.TableName, Convert.ToString(HotelID));
                        }
                        catch
                        {
                        }
                        DataRepository.AvailibilityOfRoomsProvider.Update(tm, aor);
                    }
                    //string where2 = ViewSelectAvailabilityColumn.HotelId + "=" + HotelID + " and " + ViewSelectAvailabilityColumn.RoomType + "=0 and " + ViewSelectAvailabilityColumn.AvailabilityDate + "='" + a.AvailabilityDate + "'";
                    //VList<ViewSelectAvailability> availRoom2 = DataRepository.ViewSelectAvailabilityProvider.GetPaged(tm, where2, string.Empty, 0, int.MaxValue, out count2);
                    //int Count2 = availRoom2.Where(c => c.AfternoonStatus == (int)AvailabilityStatus.BOOKED && c.MorningStatus == (int)AvailabilityStatus.BOOKED).Count();
                    //if (Count2 == availRoom2.Count && availRoom2.Count != 0)
                    //{
                    //    foreach (ViewSelectAvailability vaor in availRoom2)
                    //    {
                    //        AvailibilityOfRooms aor = (AvailibilityOfRooms)DataRepository.AvailibilityOfRoomsProvider.GetById(tm, vaor.AvailRoomId);
                    //        aor.MorningStatus = (int)AvailabilityStatus.AVAILABLE;
                    //        aor.AfternoonStatus = (int)AvailabilityStatus.AVAILABLE;
                    //        DataRepository.AvailibilityOfRoomsProvider.Update(tm, aor);
                    //    }
                    //    a.FullPropertyStatus = (int)AvailabilityStatus.AVAILABLE;
                    //    a.FullProrertyStatusBr = (int)AvailabilityStatus.AVAILABLE;
                    //    DataRepository.AvailabilityProvider.Update(tm, a);
                    //}
                }
                tm.Commit();
            }
            catch
            {
                tm.Rollback();
            }
        }
        #endregion

        #region Get Table string of Availability calander for next 60 days
        public string GetAvailabilityForNextsixtyDaysByHotelID(long hotelid)
        {
            int CurrentDay = DateTime.Now.Day;
            int CurrentYear = DateTime.Now.Year;
            int CurrentMonth = DateTime.Now.Month;
            int LastMonth = DateTime.Now.AddDays(59).Month;
            int LastDay = DateTime.Now.AddDays(59).Day;
            int LastYear = DateTime.Now.AddDays(59).Year;
            int SelectedDay = 0;
            int SelectedYear = 0;
            int SelectedMonth = 0;
            string table = "<table border='1'><tr>";
            string header = "<tr>";
            string Bodycal = "";
            string BodycalEmpty = "";
            int total = 0;
            bool checkme = false;
            string whereClase = AvailabilityColumn.HotelId + "=" + hotelid  + " AND " + AvailabilityColumn.AvailabilityDate + ">='" + (new DateTime(CurrentYear, CurrentMonth, CurrentDay)) + "' AND " + AvailabilityColumn.AvailabilityDate + "<='" + (new DateTime(LastYear, LastMonth, LastDay)) + "'";
            TList<Availability> avail = DataRepository.AvailabilityProvider.GetPaged(whereClase,string.Empty,0,int.MaxValue,out total);
            for (int i = 0; DateTime.Now.AddDays(i) <= DateTime.Now.AddDays(59); i++)
            {
                if (SelectedMonth != DateTime.Now.AddDays(i).Month)
                {
                    table += "<td align='center' ><b>" + Enum.GetName(typeof(MonthName), DateTime.Now.AddDays(i).Month) + "-" + DateTime.Now.AddDays(i).Year + "</b></td>";
                    if (SelectedMonth != 0)
                    {
                        header = header.Replace("@DAYCAL", Bodycal);
                        Bodycal = "";
                    }
                    header += "<td valign='top' ><table><tr><td>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td></tr>@DAYCAL</table></td>";
                    SelectedMonth = DateTime.Now.AddDays(i).Month;
                    checkme = true;
                }
                Availability a = avail.Where(c => c.AvailabilityDate.Value == new DateTime(DateTime.Now.AddDays(i).Year, DateTime.Now.AddDays(i).Month, DateTime.Now.AddDays(i).Day)).FirstOrDefault();
                #region FirstDay
                if (DateTime.Now.AddDays(i).Day != 1 && i==0)
                {
                    for (int j = 1; j < DateTime.Now.AddDays(i).Day; j++)
                    {
                        DateTime d = new DateTime(DateTime.Now.AddDays(i).Year, DateTime.Now.AddDays(i).Month, j);
                        if (j == 1)
                        {
                            if (d.DayOfWeek != DayOfWeek.Sunday)
                            {
                                Bodycal = "<tr>";
                                for (int k = (int)DayOfWeek.Sunday; k < (int)d.DayOfWeek; k++)
                                {
                                    Bodycal += "<td></td>";
                                }
                            }
                        }
                        if (d.DayOfWeek == DayOfWeek.Sunday)
                        {
                            Bodycal += "<tr>";
                        }
                        Bodycal += "<td bgcolor='silver'>" + d.Day + "</td>";
                        if (d.DayOfWeek == DayOfWeek.Saturday)
                        {
                            Bodycal += "</tr>";
                        }
                    }
                    checkme = false;
                }
                else if (DateTime.Now.AddDays(i).Day == 1)
                {
                    if (DateTime.Now.AddDays(i).DayOfWeek != DayOfWeek.Sunday)
                    {
                        Bodycal += "<tr>";
                        for (int k = (int)DayOfWeek.Sunday; k < (int)DateTime.Now.AddDays(i).DayOfWeek; k++)
                        {
                            Bodycal += "<td></td>";
                        }
                    }
                }
                #endregion
                #region NormalDay
                if (DateTime.Now.AddDays(i).DayOfWeek == DayOfWeek.Sunday)
                {
                    Bodycal += "<tr>";
                }
                if (a != null)
                {
                    if (a.FullPropertyStatus == (int)AvailabilityStatus.AVAILABLE)
                    {
                        Bodycal += "<td bgcolor='#00E500' style='color:#FFFFFF;'>" + DateTime.Now.AddDays(i).Day + "</td>";
                    }
                    else
                    {
                        Bodycal += "<td bgcolor='#DF4649' style='color:#FFFFFF;'>" + DateTime.Now.AddDays(i).Day + "</td>";
                    }
                }
                else
                {
                    Bodycal += "<td>" + DateTime.Now.AddDays(i).Day + "</td>";
                }
                if (DateTime.Now.AddDays(i).DayOfWeek == DayOfWeek.Saturday)
                {
                    Bodycal += "</tr>";
                }
                #endregion
                #region LastDay
                if (DateTime.Now.AddDays(i).Day == DateTime.DaysInMonth(DateTime.Now.AddDays(i).Year, DateTime.Now.AddDays(i).Month))
                {
                    if (DateTime.Now.AddDays(i).Day == DateTime.DaysInMonth(DateTime.Now.AddDays(i).Year, DateTime.Now.AddDays(i).Month))
                    {
                        if (DateTime.Now.AddDays(i).DayOfWeek != DayOfWeek.Saturday)
                        {
                            for (int k = (int)DayOfWeek.Saturday; k > (int)DateTime.Now.AddDays(i).DayOfWeek; k--)
                            {
                                Bodycal += "<td></td>";
                            }
                            Bodycal += "</tr>";
                        }
                    }
                }
                else if (i == 59 && DateTime.Now.AddDays(i).Day != DateTime.DaysInMonth(DateTime.Now.AddDays(i).Year, DateTime.Now.AddDays(i).Month))
                {
                    for (int l = DateTime.Now.AddDays(i).Day + 1; l <= DateTime.DaysInMonth(DateTime.Now.AddDays(i).Year, DateTime.Now.AddDays(i).Month); l++)
                    {
                        DateTime d = new DateTime(DateTime.Now.AddDays(i).Year, DateTime.Now.AddDays(i).Month, l);
                        if (d.DayOfWeek == DayOfWeek.Sunday)
                        {
                            Bodycal += "<tr>";
                        }
                        Bodycal += "<td bgcolor='silver'>" + d.Day + "</td>";
                        if (d.DayOfWeek == DayOfWeek.Saturday)
                        {
                            Bodycal += "</tr>";
                        }
                        if (l == DateTime.DaysInMonth(DateTime.Now.AddDays(i).Year, DateTime.Now.AddDays(i).Month))
                        {
                            if (d.DayOfWeek != DayOfWeek.Saturday)
                            {
                                for (int k = (int)DayOfWeek.Saturday; k > (int)d.DayOfWeek; k--)
                                {
                                    Bodycal += "<td></td>";
                                }
                                Bodycal += "</tr>";
                            }
                        }
                    }
                }
                #endregion
            }
            header = header.Replace("@DAYCAL", Bodycal);
            header += "</tr>";
            table += "</tr>" + header + "</table>";
            return table;
        }
        #endregion
    }

    public  enum  AvailabilityStatus
    {
        AVAILABLE=0,
        CLOSED=1,
        BOOKED=2,
        SOLD = 3,
    }
    public enum MonthName
    {
        Jan = 1,
        Feb = 2,
        Mar = 3,
        Apr = 4,
        May = 5,
        Jun = 6,
        Jul = 7,
        Aug = 8,
        Sep = 9,
        Oct = 10,
        Nov = 11,
        Dec = 12
    }
}
