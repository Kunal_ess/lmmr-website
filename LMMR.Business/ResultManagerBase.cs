﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using LMMR.Data;
using LMMR.Business;
using LMMR.Entities;
#endregion

namespace LMMR.Business
{
    public abstract class ResultManagerBase
    {
        #region constructor
        public ResultManagerBase()
        {

        }
        #endregion

        #region function
        /// <summary>
        /// This method takes the data from xml document
        /// </summary>
        /// <param name="LanguageId"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static string GetResult(Int64 LanguageId, string msg)
        {          
            string result = string.Empty;
            string key = msg.ToString();
            string FilePath = "";
            if (LanguageId == 0)
            {
                FilePath = AppDomain.CurrentDomain.BaseDirectory + "Resource\\english\\ResultMessage.xml";
            }
            else
            {
                FilePath = AppDomain.CurrentDomain.BaseDirectory + "Resource\\" + DataRepository.LanguageProvider.GetById(LanguageId).Name + "\\ResultMessage.xml";
            }
            XmlDocument doc = new XmlDocument();
            doc.Load(FilePath);
            XmlNode nodes = doc.SelectSingleNode("items/item[@key='" + key + "']");        
            result = nodes.InnerText;
            return result;
        }

        public static XmlDocument GetLanguageXMLFile(Int64 LanguageId)
        {
            string FilePath = AppDomain.CurrentDomain.BaseDirectory + "Resource\\" + DataRepository.LanguageProvider.GetById(LanguageId).Name + "\\ResultMessage.xml";
            XmlDocument doc = new XmlDocument();
            doc.Load(FilePath);
            return doc;
        }
        #endregion 

    }

    #region enum
    public enum ResultMessage
    {
        HOME = 1,
        ABOUTUS = 2,
        JOINTODAY = 3,
        CONTACTUS = 4,
        LOGIN = 5,

        MEETINGSPECIAL = 6,
        NEWS = 7,
        HOTELRECENTLYJOINED = 8,
        HOWTOBOOKONLINE = 9,
        HOWTOSENDREQUEST = 10,

        DURATION = 11,
        PARTICIPANTS = 12,
        DAYS = 13,
        READ = 14,
        G = 15,
        H = 16,
        J = 17,
        FORHOTELS = 18,
        NEED = 19
    }
#endregion
}

