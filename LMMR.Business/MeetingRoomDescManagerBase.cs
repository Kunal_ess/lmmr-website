﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Data;
using LMMR.Entities;
using System.Web;

namespace LMMR.Business
{
    public abstract class MeetingRoomDescManagerBase
    {
        #region Variables
        //Declare Variable to store data
        private TList<MeetingRoomDesc> _varGetAllMeetingRoomDesc;

        //Declare Variable to store data
        private TList<MeetingRoom> _varGetAllMeetingRoom;

        //Declare Variable to store data
        private TList<MeetingRoomDesc> _varGetMeetingRoomDesc;


        //Declare Variable to store meetingroomDesc
        private MeetingRoomDesc _varGetMeetingRoomDescByID;

        //Declare Variable to store data
        private MeetingRoom _varGetMeetingRoom;


        //Declare variable to get all language 
        private TList<Language> _varGetAllLanguage;

        //Declare Variable to message
        private string _varMessage;

        //Declare variable to store latestID
        private int _varMeetingRoomID;
        #endregion

        #region Properties
        //Property to Store the TList of Entities.MeetingRoomDesc class
        public TList<MeetingRoomDesc> propGetAllMeetingRoomDesc
        {
            get { return _varGetAllMeetingRoomDesc; }
            set { _varGetAllMeetingRoomDesc = value; }
        }

        //Property to Store meetingroom Desc by ID
        public MeetingRoomDesc propMeetingRoomDescByID
        {
            get { return _varGetMeetingRoomDescByID; }
            set { _varGetMeetingRoomDescByID = value; }
        }

        //Property to Store the TList of Entities.MeetingRoom class
        public TList<MeetingRoom> propGetAllMeetingRoom
        {
            get { return _varGetAllMeetingRoom; }
            set { _varGetAllMeetingRoom = value; }
        }

        //Property to store TList of Entities.MeetingRoomDesc class
        public TList<MeetingRoomDesc> propGetMeetingRoomDesc
        {
            get { return _varGetMeetingRoomDesc; }
            set { _varGetMeetingRoomDesc = value; }
        }

        //Property to store row of Entities.MeetingRoom class
        public MeetingRoom propGetMeetingRoom
        {
            get { return _varGetMeetingRoom; }
            set { _varGetMeetingRoom = value; }
        }

        //Property to store the TList of Entities.Language class
        public TList<Language> propGetAllLanguage
        {
            get { return _varGetAllLanguage; }
            set { _varGetAllLanguage = value; }
        }

        //Property to store message
        public string propMessage
        {
            get { return _varMessage; }
            set { _varMessage = value; }
        }

        //Property to store meeting room ID
        public int propMeetingRoomID
        {
            get { return _varMeetingRoomID; }
            set { _varMeetingRoomID = value; }
        }

        #endregion

        #region Functions
        /// <summary>
        /// Function to get all list of MeetingRoomDesc
        /// </summary>
        /// <returns></returns>
        public TList<MeetingRoomDesc> GetAllMeetingRoomDesc()
        {
            propGetAllMeetingRoomDesc = DataRepository.MeetingRoomDescProvider.GetAll();

            return propGetAllMeetingRoomDesc;

        }

        /// <summary>
        /// Function to get  MeetingRoomDesc by ID
        /// </summary>
        /// <param name="meetingRoomID"></param>
        /// <returns></returns>
        public TList<MeetingRoomDesc> GetMeetingRoomDescByMeetingRoomID(int meetingRoomID)
        {
            propGetMeetingRoomDesc = DataRepository.MeetingRoomDescProvider.GetByMeetingRoomId(meetingRoomID);

            return propGetMeetingRoomDesc;

        }

        /// <summary>
        //Function to get  MeetingRoomDesc by ID and language id
        /// </summary> 
        public TList<MeetingRoomDesc> GetMeetingRoomDescByMeetingRoomLanguageId(string where)
        {
            int totalcount = 0;
            TList<MeetingRoomDesc> Meetingdesc = DataRepository.MeetingRoomDescProvider.GetPaged(where, string.Empty, 0, int.MaxValue, out totalcount);
            return Meetingdesc;
        }

        /// <summary>
        /// function to get all meetingRooms 
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public TList<MeetingRoom> GetAllMeetingRoom(int hotelID)
        {
            int Total = 0;
            string whereclause = "Hotel_Id=" + hotelID + " and IsDeleted='" + false + "' ";
            string orderby = "OrderNumber";
            propGetAllMeetingRoom = DataRepository.MeetingRoomProvider.GetPaged(whereclause, orderby, 0, int.MaxValue, out Total);
            return propGetAllMeetingRoom;
        }

        /// <summary>
        /// function to get all online meetingRooms 
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public TList<MeetingRoom> GetAllOnlineMeetingRoom(int hotelID)
        {
            int Total = 0;
            string whereclause = "Hotel_Id=" + hotelID + " and IsDeleted='" + false + "' and IsOnline ='" + true + "' ";
            string orderby = "OrderNumber";
            propGetAllMeetingRoom = DataRepository.MeetingRoomProvider.GetPaged(whereclause, orderby, 0, int.MaxValue, out Total);
            return propGetAllMeetingRoom;
        }        

        public TList<MeetingRoom> GetAllOnlineMeetingRoomWithoutDuration(int hotelID, int meetingroom)
        {
            int Total = 0;
            string whereclause = "Hotel_Id=" + hotelID + " and IsDeleted='" + false + "' and IsOnline ='" + true + "' and id not in('" + meetingroom + "')";
            string orderby = "OrderNumber";
            propGetAllMeetingRoom = DataRepository.MeetingRoomProvider.GetPaged(whereclause, orderby, 0, int.MaxValue, out Total);
            return propGetAllMeetingRoom;
        }
      
        /// <summary>
        /// Function to insert new MeetingRoom
        /// </summary>
        /// <param name="hotelID"></param>
        /// <param name="name"></param>
        /// <param name="dayLight"></param>
        /// <param name="surface"></param>
        /// <param name="height"></param>
        /// <param name="image"></param>
        /// <param name="plan"></param>
        /// <param name="updatedBy"></param>
        /// <returns></returns>
        public string AddNewMeetingRoom(MeetingRoom objMeetingroom,MeetingRoomDesc objMeetingroomDesc,MeetingRoomPictureVideo objMeetingroomPictureVideo)
        {
            TransactionManager tm = null;
            bool Insert = false;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Guid gid = Guid.NewGuid();
                objMeetingroom.IsActive = true;
                if (DataRepository.MeetingRoomProvider.Insert(tm, objMeetingroom))
                {
                    objMeetingroomDesc.MeetingRoomId = objMeetingroom.Id;
                    objMeetingroomDesc.UpdatedBy = objMeetingroom.UpdatedBy;
                    DataRepository.MeetingRoomDescProvider.Insert(tm, objMeetingroomDesc);
                    objMeetingroomPictureVideo.MeetingRoomId = objMeetingroom.Id;
                    objMeetingroomPictureVideo.UpdatedBy = objMeetingroom.UpdatedBy;
                    DataRepository.MeetingRoomPictureVideoProvider.Insert(tm, objMeetingroomPictureVideo);
                    Insert = true;

                    //Audit trail maintain
                    TrailManager.LogAuditTrail<MeetingRoom>(tm, objMeetingroom, null, AuditAction.I, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.MeetingRoomDescription), objMeetingroom.TableName, Convert.ToString(objMeetingroom.HotelId));
                    TrailManager.LogAuditTrail<MeetingRoomDesc>(tm, objMeetingroomDesc, null, AuditAction.I, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.MeetingRoomDescription), objMeetingroomDesc.TableName, Convert.ToString(objMeetingroom.HotelId));
                    TrailManager.LogAuditTrail<MeetingRoomPictureVideo>(tm, objMeetingroomPictureVideo, null, AuditAction.I, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.MeetingRoomDescription), objMeetingroomPictureVideo.TableName, Convert.ToString(objMeetingroom.HotelId));                    
                    //Audit trail maintain

                    propMessage = "Meetingroom Added successfully";
                }
                else
                {
                    propMessage = "Please try again !";
                }
                tm.Commit();
                if (Insert)
                {
                    new HotelManager().AddRoomAvailability(null, objMeetingroom.HotelId, objMeetingroom.Id, 0,0);
                }
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }

            return propMessage;
        }

        /// <summary>
        /// Get meetingroomOrderNumber of meetingroom
        /// </summary>
        /// <returns></returns>
        public int? GetOrderNumberOfMeetingroom(int hotelID)
        {
            int Total = 0;
            string whereclause = "IsDeleted='" + false + "' AND Hotel_Id =" + hotelID + "";
            int? OrderNumber = DataRepository.MeetingRoomProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total).Max(a => a.OrderNumber);
            return OrderNumber;
        }


        
        /// <summary>
        /// Function to get meetingroom by ID.
        /// </summary>
        /// <param name="meetingRoomID"></param>
        /// <returns></returns>
        public MeetingRoom GetMeetingRoomByID(int meetingRoomID)
        {
            propGetMeetingRoom = DataRepository.MeetingRoomProvider.GetById(meetingRoomID);

            return propGetMeetingRoom;

        }
        /// <summary>
        /// Function to get meetingroom desc by ID
        /// </summary>
        /// <returns></returns>
        public MeetingRoomDesc GetMeetingRoomDescByID(int meetingroomDescID)
        {
            propMeetingRoomDescByID = DataRepository.MeetingRoomDescProvider.GetById(meetingroomDescID);
            return propMeetingRoomDescByID;
        }

        /// <summary>
        /// Function to update meetingRoom 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="hotelID"></param>
        /// <param name="name"></param>
        /// <param name="dayLight"></param>
        /// <param name="surface"></param>
        /// <param name="height"></param>
        /// <param name="image"></param>
        /// <param name="plan"></param>
        /// <param name="updatedBy"></param>
        /// <returns></returns>
        public string UpdateMeetingRoom(MeetingRoom objMeetingroom, MeetingRoomDesc objMeetingroomDesc)
        {
            TransactionManager tm = null;
            try
            {

                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();    
            
                //Audit trail maintain
                Guid gid = Guid.NewGuid();
                MeetingRoom old = objMeetingroom.GetOriginalEntity();
                old.Id = objMeetingroom.Id;
                TrailManager.LogAuditTrail<MeetingRoom>(tm, objMeetingroom, old, AuditAction.U, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.MeetingRoomDescription), objMeetingroom.TableName, Convert.ToString(objMeetingroom.HotelId));
                MeetingRoomDesc olddesc = objMeetingroomDesc.GetOriginalEntity();
                olddesc.Id = objMeetingroomDesc.Id;
                TrailManager.LogAuditTrail<MeetingRoomDesc>(tm, objMeetingroomDesc, olddesc, AuditAction.U, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.MeetingRoomDescription), objMeetingroomDesc.TableName, Convert.ToString(objMeetingroom.HotelId));
                //Audit trail maintain

                if (DataRepository.MeetingRoomProvider.Update(tm, objMeetingroom))
                {
                    objMeetingroomDesc.MeetingRoomId = objMeetingroom.Id;
                    objMeetingroomDesc.UpdatedBy = objMeetingroom.UpdatedBy;
                    DataRepository.MeetingRoomDescProvider.Update(tm, objMeetingroomDesc);
                    TList<MeetingRoomPictureVideo> ObjPic = DataRepository.MeetingRoomPictureVideoProvider.GetByMeetingRoomId(tm, objMeetingroom.Id);
                    if (ObjPic != null)
                    {
                        MeetingRoomPictureVideo getPic = ObjPic.Find(a => a.IsMain == true);
                        if (getPic != null)
                        {
                            getPic.ImageName = objMeetingroom.Picture;
                            getPic.UpdatedBy = objMeetingroom.UpdatedBy;
                            DataRepository.MeetingRoomPictureVideoProvider.Update(tm, getPic);
                        }
                    }
                    propMessage = "Meetingroom detail updated successfully !";

                }
                else
                {
                    propMessage = "Please try again !";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {

                tm.Rollback();
            }
            return propMessage;
        }
      
        /// <summary>
        /// Function to get all required language
        /// </summary>
        /// <returns></returns>
        public TList<Language> GetAllRequiredLanguage()
        {
            propGetAllLanguage = DataRepository.LanguageProvider.GetAll();

            return propGetAllLanguage;
        }

        public string AddNewMeetingroomDesc(MeetingRoom objMeetingroom, MeetingRoomDesc objMeetingroomDesc)
        {
            TransactionManager tm = null;
            try
            {

                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Guid gid = Guid.NewGuid();
                if (DataRepository.MeetingRoomProvider.Update(tm, objMeetingroom))
                {
                    objMeetingroomDesc.MeetingRoomId = objMeetingroom.Id;
                    objMeetingroomDesc.UpdatedBy = objMeetingroom.UpdatedBy;
                    DataRepository.MeetingRoomDescProvider.Insert(tm, objMeetingroomDesc);
                    TList<MeetingRoomPictureVideo> ObjPic = DataRepository.MeetingRoomPictureVideoProvider.GetByMeetingRoomId(tm, objMeetingroom.Id);
                    if (ObjPic != null)
                    {
                        MeetingRoomPictureVideo getPic = ObjPic.Find(a => a.IsMain == true);
                        if (getPic != null)
                        {
                            getPic.ImageName = objMeetingroom.Picture;
                            getPic.UpdatedBy = objMeetingroom.UpdatedBy;
                            DataRepository.MeetingRoomPictureVideoProvider.Update(tm, getPic);
                        }
                    }
                    //Audit trail maintain
                    TrailManager.LogAuditTrail<MeetingRoomDesc>(tm, objMeetingroomDesc, null, AuditAction.I, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.MeetingRoomDescription), objMeetingroomDesc.TableName, Convert.ToString(objMeetingroom.HotelId));                    
                    //Audit trail maintain
                    propMessage = "Meetingroom detail updated successfully !";

                }
                else
                {
                    propMessage = "Please try again !";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {

                tm.Rollback();
            }
            return propMessage;
        
        }


        /// <summary>
        /// This function use to UpdateCurrent Order of meetingroom.
        /// </summary>
        /// <param name="objCurrentItem"></param>
        public void UpdateCurrentOrder(MeetingRoom objCurrentItem)
        {
            DataRepository.MeetingRoomProvider.Update(objCurrentItem);
        }
        /// <summary>
        /// This function use to update next Order of meetingroom.
        /// </summary>
        /// <param name="objNextClient"></param>
        public void UpdateNextOrder(MeetingRoom objNextClient)
        {
            DataRepository.MeetingRoomProvider.Update(objNextClient);

        }

        /// <summary>
        /// function to delete meetingroom By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string DeleteMeetingRoom(int id)
        {
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();

                MeetingRoom Obj = DataRepository.MeetingRoomProvider.GetById(tm,id);
                Obj.IsDeleted = true;

                //Audit trail maintain
                Guid gid = Guid.NewGuid();
                MeetingRoom old = Obj.GetOriginalEntity();
                old.Id = Obj.Id;
                TrailManager.LogAuditTrail<MeetingRoom>(tm, Obj, old, AuditAction.SD, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.MeetingRoomDescription), Obj.TableName, Convert.ToString(Obj.HotelId));
                //Audit trail maintain
                

                if (DataRepository.MeetingRoomProvider.Update(tm,Obj))
                {
                    int myAvailCount = 0;
                    TList<AvailibilityOfRooms> myAvailability = DataRepository.AvailibilityOfRoomsProvider.GetPaged(tm, AvailibilityOfRoomsColumn.RoomId + "=" + id + " and " + AvailibilityOfRoomsColumn.RoomType + "=1", string.Empty, 0, int.MaxValue, out myAvailCount);
                    DataRepository.AvailibilityOfRoomsProvider.Delete(tm, myAvailability);
                    propMessage = "Meetingroom delete successfully";
                }
                else
                {
                    propMessage = "Please try again !";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }

            return propMessage;
        }

        #endregion
    }

}
