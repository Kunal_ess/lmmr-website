﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;

namespace LMMR.Business
{
    public abstract class FinanceInvoiceBase
    {
        //Declare Variable to store data
        private TList<Invoice> _varGetAllFinanceInvoice;
        private TList<Hotel> _varGetAllHotel;
        private Invoice _varGetInvoice;
        private TList<Others> _varGetInvoiceVerify;

        //Declare Variable to message
        private string _varMessage;

        //Property to store message
        public string propMessage
        {
            get { return _varMessage; }
            set { _varMessage = value; }
        }

        //Property to store TList of Entities.FinanaceInvoice class
        public TList<Invoice> propGetFinanceInvoice
        {
            get { return _varGetAllFinanceInvoice; }
            set { _varGetAllFinanceInvoice = value; }
        }

        //Property to store TList of Entities.Others class
        public TList<Others> propGetOthers
        {
            get { return _varGetInvoiceVerify; }
            set { _varGetInvoiceVerify = value; }
        }

        //Property to store TList of Entities.FinanaceInvoice class
        public TList<Hotel> propGetHotelName
        {
            get { return _varGetAllHotel; }
            set { _varGetAllHotel = value; }
        }

        //Property to store row of Entities.Invoice class
        public Invoice propGetInvoice
        {
            get { return _varGetInvoice; }
            set { _varGetInvoice = value; }
        }


        #region
        /// <summary>
        //This method is used to get all list of FinanaceInvoice
        /// </summary>            
        public TList<Invoice> GetInvoiceByHotelid(int hotelId)
        {
            propGetFinanceInvoice = DataRepository.InvoiceProvider.GetByHotelId(hotelId);
            DataRepository.InvoiceProvider.DeepLoad(propGetFinanceInvoice,true,DeepLoadType.IncludeChildren, typeof(Hotel));
            return propGetFinanceInvoice;
        }
        #endregion

        #region
        /// <summary>
        //This method is used to get all invoice by hotelid
        /// </summary>  
        public TList<Invoice> GetInvoiceByHotelid(int hotelId, string OrderBy)
        {
            string whereclause = InvoiceColumn.HotelId + "=" + hotelId;
            int count = 0;
            propGetFinanceInvoice = DataRepository.InvoiceProvider.GetPaged(whereclause,OrderBy,0,int.MaxValue,out count);
            DataRepository.InvoiceProvider.DeepLoad(propGetFinanceInvoice, true, DeepLoadType.IncludeChildren, typeof(Hotel));
            return propGetFinanceInvoice;
        }
        #endregion
        
        #region
        /// <summary>
        //This function to get all list of FinanaceInvoice
        /// </summary>  
        //
        public TList<Invoice> GetInvoiceByHotelName()
        {
            propGetFinanceInvoice = DataRepository.InvoiceProvider.GetAll();
            DataRepository.InvoiceProvider.DeepLoad(propGetFinanceInvoice, true, DeepLoadType.IncludeChildren, typeof(Hotel));
            return propGetFinanceInvoice;
        }
        #endregion

        #region
        /// <summary>
        //This function to get all hotel name
        /// </summary>  
        //        
        public TList<Hotel> GetInvoiceByAllHotel()
        {
            propGetHotelName = DataRepository.HotelProvider.GetAll();
            return propGetHotelName;
        }
        #endregion

        #region
        /// <summary>
        //This function is to get invoice details by condition  
        /// </summary> 
        public TList<Invoice> GetInvoicebyCondition(string where, string orderby)
        {
            int totalcount = 0;
            TList<Invoice> invoiceDtls = DataRepository.InvoiceProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount);
            DataRepository.InvoiceProvider.DeepLoad(invoiceDtls, true, DeepLoadType.IncludeChildren, typeof(Hotel));
            return invoiceDtls;
        }
        #endregion

        #region
        /// <summary>
        //This function to get invoice details by unique id
        /// </summary>  
        public Invoice GetInvoiceByID(int id)
        {
            propGetInvoice = DataRepository.InvoiceProvider.GetById(id);
            return propGetInvoice;
        }
        #endregion

        #region
        /// <summary>
        //This function to get invoice details by client name
        /// </summary> 
        public TList<Invoice> GetinvoicebyClient(string whereclause)
        {
            string orderby = "DueDate DESC";
            TList<Invoice> lstInvoice = GetInvoicebyCondition(whereclause, orderby);
            DataRepository.InvoiceProvider.DeepLoad(lstInvoice, true, DeepLoadType.IncludeChildren, typeof(Hotel));
            return lstInvoice;
        }

        /// <summary>
        /// Get Hotel details by Country id
        /// </summary>
        /// <param name="countryid"></param>
        /// <param name="cityid"></param>
        /// <returns></returns>
        public TList<Hotel> GetHotelByCountryAndCity(Int64 countryid)
        {
            string whereclause = string.Empty;
            if (countryid != 0)
            {
                if (whereclause.Length > 0)
                {
                    whereclause += " and ";
                }
                whereclause = HotelColumn.CountryId + "=" + countryid;
            }                                
            string orderby = HotelColumn.CreationDate + " DESC";
            int count = 0;
            TList<Hotel> objHotel = DataRepository.HotelProvider.GetPaged(whereclause, orderby, 0, int.MaxValue, out count);
            return objHotel;
        }
        #endregion      
  
        #region
        /// <summary>
        //This function to get result for search from-to date
        /// </summary> 
        public TList<Invoice> GetDateSearch(int hotelId, string Datetime)
        {
            string[] DateArray = Datetime.Split('-');
            DateTime fromDate = new DateTime(Convert.ToInt32(DateArray[0].Split('/')[2]), Convert.ToInt32(DateArray[0].Split('/')[1]), Convert.ToInt32(DateArray[0].Split('/')[0]));
            DateTime todate = new DateTime(Convert.ToInt32(DateArray[1].Split('/')[2]), Convert.ToInt32(DateArray[1].Split('/')[1]), Convert.ToInt32(DateArray[1].Split('/')[0]));            
            string whereclause = "HotelId=" + hotelId + " and " + InvoiceColumn.DateFrom + "='" + fromDate + "' and " + InvoiceColumn.DateTo + "='" + todate + "'";
            int count = 0;
            propGetFinanceInvoice = DataRepository.InvoiceProvider.GetPaged(whereclause, String.Empty, 0, int.MaxValue, out count);
            DataRepository.InvoiceProvider.DeepLoad(propGetFinanceInvoice, true, DeepLoadType.IncludeChildren, typeof(Hotel));
            return propGetFinanceInvoice;
        }

        public TList<Invoice> GetDateSearchForInvoice(string Datetime)
        {
            string[] DateArray = Datetime.Split('-');
            DateTime fromDate = new DateTime(Convert.ToInt32(DateArray[0].Split('/')[2]), Convert.ToInt32(DateArray[0].Split('/')[1]), Convert.ToInt32(DateArray[0].Split('/')[0]));
            DateTime todate = new DateTime(Convert.ToInt32(DateArray[1].Split('/')[2]), Convert.ToInt32(DateArray[1].Split('/')[1]), Convert.ToInt32(DateArray[1].Split('/')[0]));
            string whereclause = InvoiceColumn.DateFrom + "='" + fromDate + "' and " + InvoiceColumn.DateTo + "='" + todate + "'";
            int count = 0;
            propGetFinanceInvoice = DataRepository.InvoiceProvider.GetPaged(whereclause, String.Empty, 0, int.MaxValue, out count);
            DataRepository.InvoiceProvider.DeepLoad(propGetFinanceInvoice, true, DeepLoadType.IncludeChildren, typeof(Hotel));
            return propGetFinanceInvoice;
        }
        #endregion
        public TList<Invoice> getAllInvoices()
        {
            return (DataRepository.InvoiceProvider.GetAll());
        }

        ////function to get all list of FinanaceInvoice by search date
        //public TList<Invoice> GetInvoiceBySearch(string ClientName)
        //{
        //    int totalout = 0;
        //    string whereclause = "CountryId='" +  + "'";
        //    TList<Invoice> lsthInvoice = DataRepository.InvoiceProvider.GetPaged(whereclause, String.Empty, 0, int.MaxValue, out totalout);            
        //    return propGetFinanceInvoice;
        //}

        #region
        /// <summary>
        //This function to update credit note on basis of Invoice id
        /// </summary> 
        public string[] UpdateInvoice(Invoice inv)
        {
            TransactionManager tm = null;
            string[] Result = new string[2];
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                if (DataRepository.InvoiceProvider.Update(tm, inv))
                {
                    Result[0] = "Updated Succesfully.";                    
                }
                else
                {
                    Result[0] = "No record Update";                    
                }
                tm.Commit();
                Invoice objInvoice = new Invoice();

            }
            catch (Exception ex)
            {
                tm.Rollback();
                Result[0] = "Error occured wile updating.";                
            }
            return Result;
        }
        #endregion

        #region
        /// <summary>
        //This function to get InvoiceVerify check from other table
        /// </summary>                 
        public TList<Others> GetInvoiceVerify()
        {
            propGetOthers = DataRepository.OthersProvider.GetAll();
            return propGetOthers;
        }
        #endregion
    }
}
