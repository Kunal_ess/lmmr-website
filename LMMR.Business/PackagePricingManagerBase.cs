﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;
using System.Data;
using System.Web;

namespace LMMR.Business
{
    public abstract class PackagePricingManagerBase
    {
        #region Variables
        //Declare Variable to store data
        private TList<PackageDescription> _varGetPackageDescription;

        //Declare Variable to store data
        private TList<PackageItemMapping> _varGetPackageItems;

        //Declare Variable to store data
        private TList<PackageItems> _varGetAllPackageItems;

        //Declare Variable to store data
        private TList<PackageItems> _varFoodBeveragesPackageItems;

        //Declare Variable to store data
        private TList<PackageItems> _varMeetingRoomVat;


        //Declare Variable to store data
        private TList<PackageItems> _varEquipmentPackageItems;

        //Declare Variable to store data
        private TList<PackageMaster> _varGetAllPackageName;

        //Declare Variable to store data
        private TList<PackageByHotel> _varGetAllPackagePrice;


        //Declare Variable to store data
        private PackageByHotel _varGetPriceByID;

        //Declare Variable to store data
        private string _varMessage;
        #endregion

        #region Properties
        //Property to Store the TList of Entities.PackageItems class
        public TList<PackageItems> propGetAllPackageItems
        {
            get { return _varGetAllPackageItems; }
            set { _varGetAllPackageItems = value; }
        }

        //Property to Store the TList of Entities.PackageItems class
        public TList<PackageItemMapping> propGetPackageItemsByPackageID
        {
            get { return _varGetPackageItems; }
            set { _varGetPackageItems = value; }
        }

        //Property to Store the TList of Entities.PackageItems class
        public TList<PackageItems> propFoodBeveragesPackageItems
        {
            get { return _varFoodBeveragesPackageItems; }
            set { _varFoodBeveragesPackageItems = value; }
        }

        //Property to Store the TList of Entities.PackageItems class
        public TList<PackageItems> propEquipmentPackageItems
        {
            get { return _varEquipmentPackageItems; }
            set { _varEquipmentPackageItems = value; }
        }

        //Property to Store the TList of Entities.PackageItems class
        public TList<PackageItems> propGetMeetingRoomVat
        {
            get { return _varMeetingRoomVat; }
            set { _varMeetingRoomVat = value; }
        }


        //Property to Store the TList of Entities.PackageDescription class
        public TList<PackageDescription> propGetPackageDescription
        {
            get { return _varGetPackageDescription; }
            set { _varGetPackageDescription = value; }
        }


        //Property to Store the TList of Entities.PackageMaster class
        public TList<PackageMaster> propGetAllPackageName
        {
            get { return _varGetAllPackageName; }
            set { _varGetAllPackageName = value; }
        }

        //Property to Store the TList of Entities.PackageByHotel class
        public TList<PackageByHotel> propGetAllPackagePrice
        {
            get { return _varGetAllPackagePrice; }
            set { _varGetAllPackagePrice = value; }
        }

        //Property to Store the Price.
        public PackageByHotel propGetPriceByID
        {
            get { return _varGetPriceByID; }
            set { _varGetPriceByID = value; }
        }

        //Property to Store Message
        public string propMassage
        {
            get { return _varMessage; }
            set { _varMessage = value; }
        }
        #endregion

        #region Function
        /// <summary>
        /// Function to get all package item.
        /// </summary>
        /// <returns></returns>
        public TList<PackageItems> GetAllPackageItems()
        {
            propGetAllPackageItems = DataRepository.PackageItemsProvider.GetAll();
            DataRepository.PackageItemsProvider.DeepLoad(propGetAllPackageItems);
            return propGetAllPackageItems;
        }

        public TList<PackageItems> GetAllPackageItembyCountryid(int countryID)
        {
            TList<PackageItems> obj = DataRepository.PackageItemsProvider.GetByCountryId(countryID);
            DataRepository.PackageItemsProvider.DeepLoad(obj);
            return obj;
        }

        /// <summary>
        /// Function to get all package item.
        /// </summary>
        /// <param name="packageID"></param>
        /// <returns></returns>
        public TList<PackageItemMapping> GetPackageItemsByPackageID(Int64 packageID)
        {
            propGetPackageItemsByPackageID = DataRepository.PackageItemMappingProvider.GetByPackageId(packageID);

            return propGetPackageItemsByPackageID;
        }

        /// <summary>
        /// Function to get actual price by packageID
        /// </summary>
        /// <param name="hotelID"></param>
        /// <param name="PackageId"></param>
        /// <returns></returns>
        public TList<ActualPackagePrice> GetActualPackagePriceByPackage(Int64 hotelID, Int64 PackageId)
        {
            string whereclause = ActualPackagePriceColumn.HotelId + "=" + hotelID + " and " + ActualPackagePriceColumn.PackageId + "=" + PackageId;
            int count = 0;
            return DataRepository.ActualPackagePriceProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out count);
        }

        /// <summary>
        /// Function to get all package master.
        /// </summary>
        /// <returns></returns>
        public TList<PackageMaster> GetAllPackageName()
        {
            int Total = 0;
            string whereclause = "IsActive=1";
            propGetAllPackageName = DataRepository.PackageMasterProvider.GetPaged(whereclause, String.Empty, 0, int.MaxValue, out Total);
            return propGetAllPackageName;
        }

        public TList<PackageMaster> GetAllPackageName(long intCountryId)
        {
            int Total = 0;
            string whereclause = "IsActive=1 AND CountryId='" + intCountryId + "'";
            propGetAllPackageName = DataRepository.PackageMasterProvider.GetPaged(whereclause, String.Empty, 0, int.MaxValue, out Total);
            return propGetAllPackageName;
        }

        public TList<PackageMaster> GetAllPackageNameSuperAdmin(long intCountryId)
        {
            int Total = 0;
            string whereclause = "CountryId='" + intCountryId + "'";
            propGetAllPackageName = DataRepository.PackageMasterProvider.GetPaged(whereclause, String.Empty, 0, int.MaxValue, out Total);
            return propGetAllPackageName;
        }

        public TList<PackageMaster> GetStandardName(int CountryId)
        {
            int Total = 0;
            string whereclause = "IsActive=1 AND CountryId='" + CountryId + "' AND PackageName='standard'";
            propGetAllPackageName = DataRepository.PackageMasterProvider.GetPaged(whereclause, String.Empty, 0, int.MaxValue, out Total);
            return propGetAllPackageName;
        }

        /// <summary>
        /// This function used for get package by ID
        /// </summary>
        /// <param name="intPackageID"></param>
        /// <returns></returns>
        public PackageMaster GetPackageByID(long intPackageID)
        {
            PackageMaster obj = DataRepository.PackageMasterProvider.GetById(intPackageID);
            return obj;
        }

        /// <summary>
        /// Function to get all package price.
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public TList<PackageByHotel> GetAllPackagePrice(int hotelID)
        {
            propGetAllPackagePrice = DataRepository.PackageByHotelProvider.GetByHotelId(hotelID);

            return propGetAllPackagePrice;
        }

        /// <summary>
        /// Function to get all package desc.
        /// </summary>
        /// <param name="itemID"></param>
        /// <returns></returns>
        public TList<PackageDescription> GetPackageDescriptionByItemID(int itemID)
        {
            propGetPackageDescription = DataRepository.PackageDescriptionProvider.GetByItemId(itemID);

            return propGetPackageDescription;
        }

        /// <summary>
        /// Function to Update package price by ID
        /// </summary>
        /// <param name="packageByHotelID"></param>
        /// <param name="halfDayPackagePrice"></param>
        /// <param name="fullDayPackagePrice"></param>
        /// <param name="isOnline"></param>
        /// <param name="isComEmentary"></param>
        /// <returns></returns>
        public string UpdatePackagrPraice(PackageByHotel objPackagePrice)
        {
            TransactionManager tm = null;
            try
            {

                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                //Audit trail maintain
                Guid gid = Guid.NewGuid();
                PackageByHotel old = objPackagePrice.GetOriginalEntity();
                old.Id = objPackagePrice.Id;
                TrailManager.LogAuditTrail<PackageByHotel>(tm, objPackagePrice, old, AuditAction.U, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.PricingMeetingRoom), objPackagePrice.TableName, Convert.ToString(objPackagePrice.HotelId));
                //Audit trail maintain
                if (DataRepository.PackageByHotelProvider.Update(tm, objPackagePrice))
                {

                    propMassage = "Price Updated Successfully";
                }
                else
                {
                    propMassage = "Please try again";

                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return propMassage;

        }


        public PackageByHotel GetPriceByID(long packageByHotelID)
        {
            propGetPriceByID = DataRepository.PackageByHotelProvider.GetById(packageByHotelID);

            return propGetPriceByID;
        }

        public TList<PackageByHotel> IsPriceExists(long packageID, long itemId, long hotelId)
        {
            int Total = 0;
            string whereclause = "PackageID =" + packageID + " AND ItemId=" + itemId + " AND HotelId='" + hotelId + "'";
            TList<PackageByHotel> objgetNow = DataRepository.PackageByHotelProvider.GetPaged(whereclause, String.Empty, 0, int.MaxValue, out Total);
            return objgetNow;
        }

        public TList<PackageByHotel> IsPriceExists(long itemId, long hotelId)
        {
            int Total = 0;
            string whereclause = "PackageID IS NULL AND ItemId=" + itemId + " AND HotelId='" + hotelId + "'";
            TList<PackageByHotel> objgetNow = DataRepository.PackageByHotelProvider.GetPaged(whereclause, String.Empty, 0, int.MaxValue, out Total);
            return objgetNow;
        }

        /// <summary>
        /// function to insert package price
        /// </summary>
        /// <param name="packageId"></param>
        /// <param name="itemId"></param>
        /// <param name="hotelID"></param>
        /// <param name="halfDayPackagePrice"></param>
        /// <param name="fullDayPackagePrice"></param>
        /// <param name="isOnline"></param>
        /// <param name="isComEmentary"></param>
        /// <returns></returns>
        public string AddPackagrPraice(PackageByHotel objPackagePrice)
        {
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Guid gid = Guid.NewGuid();
                if (DataRepository.PackageByHotelProvider.Insert(tm, objPackagePrice))
                {
                    //Audit trail maintain
                    TrailManager.LogAuditTrail<PackageByHotel>(tm, objPackagePrice, null, AuditAction.I, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.PricingMeetingRoom), objPackagePrice.TableName, Convert.ToString(objPackagePrice.HotelId));
                    //Audit trail maintain
                    propMassage = "Price added Successfully";
                }
                else
                {
                    propMassage = "Please try again";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return propMassage;
        }

        /// <summary>
        /// Function to get all Food Beverages items
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public TList<PackageItems> GetAllFoodBeveragesItems(Int64 hotelID)
        {
            long countryID = DataRepository.HotelProvider.GetById(hotelID).CountryId;
            int Total = 0;
            string whereclause = "IsActive =1 AND ItemType=1 AND CountryId='" + countryID + "'";
            propFoodBeveragesPackageItems = DataRepository.PackageItemsProvider.GetPaged(whereclause, String.Empty, 0, int.MaxValue, out Total);
            DataRepository.PackageItemsProvider.DeepLoad(propFoodBeveragesPackageItems);
            return propFoodBeveragesPackageItems;
        }

        /// <summary>
        /// Function to check foodBeverages Items price exists or not.
        /// </summary>
        /// <param name="hotelId"></param>
        /// <returns></returns>
        public bool IsExistFoodBeveragesItems(int hotelId)
        {
            bool isExist = false;
            int index = 0;
            TList<PackageItems> ObjItems = GetAllFoodBeveragesItems(hotelId);
            TList<PackageByHotel> ObjItemPrice = GetAllPackagePrice(hotelId);
            for (index = 0; index <= ObjItems.Count - 1; index++)
            {
                var getRow = ObjItemPrice.Find(a => a.ItemId == ObjItems[index].Id && a.PackageId == null);
                if (getRow != null)
                {
                    isExist = true;
                    break;
                }

            }

            return isExist;
        }

        /// <summary>
        /// Function to get online FoodBeverages Items.
        /// </summary>
        /// <param name="hotelId"></param>
        /// <returns></returns>
        public TList<PackageItems> OnlineFoodBeveragesItems(int hotelId)
        {
            TList<PackageItems> ObjOnlineItems = GetAllFoodBeveragesItems(hotelId);
            TList<PackageByHotel> ObjItemPrice = GetAllPackagePrice(hotelId);

            int index = 0;
            for (index = 0; index <= ObjOnlineItems.Count - 1; index++)
            {
                //var getRow = ObjItemPrice.Find(a => a.ItemId == ObjOnlineItems[index].Id && a.PackageId == null && a.IsOnline == true);
                var getRow = ObjItemPrice.Find(a => a.ItemId == ObjOnlineItems[index].Id && a.PackageId == null);
                if (getRow == null)
                {
                    PackageItems d = ObjOnlineItems.Find(a => a.Id == ObjOnlineItems[index].Id);
                    ObjOnlineItems.Remove(d);
                }

            }


            return ObjOnlineItems;
        }

        /// <summary>
        /// Function to get online Equipment Items.
        /// </summary>
        /// <param name="hotelId"></param>
        /// <returns></returns>
        public TList<PackageItems> OnlineEquipmentItems(int hotelId)
        {
            TList<PackageItems> ObjOnlineItems = GetAllEquipmentItems(hotelId);
            TList<PackageByHotel> ObjItemPrice = GetAllPackagePrice(hotelId);

            int index = 0;
            for (index = 0; index <= ObjOnlineItems.Count - 1; index++)
            {
                //var getRow = ObjItemPrice.Find(a => a.ItemId == ObjOnlineItems[index].Id && a.PackageId == null && a.IsOnline == true);
                var getRow = ObjItemPrice.Find(a => a.ItemId == ObjOnlineItems[index].Id && a.PackageId == null);
                if (getRow == null)
                {
                    PackageItems d = ObjOnlineItems.Find(a => a.Id == ObjOnlineItems[index].Id);
                    ObjOnlineItems.Remove(d);
                }

            }


            return ObjOnlineItems;
        }

        /// <summary>
        /// Function to check Equipment Items price exists or not.
        /// </summary>
        /// <param name="hotelId"></param>
        /// <returns></returns>
        public bool IsExistEquipmentItems(int hotelId)
        {
            bool isExist = false;
            int index = 0;
            TList<PackageItems> ObjItems = GetAllEquipmentItems(hotelId);
            TList<PackageByHotel> ObjItemPrice = GetAllPackagePrice(hotelId);
            for (index = 0; index <= ObjItems.Count - 1; index++)
            {
                var getRow = ObjItemPrice.Find(a => a.ItemId == ObjItems[index].Id && a.PackageId == null);
                if (getRow != null)
                {
                    isExist = true;
                    break;
                }

            }

            return isExist;
        }



        /// <summary>
        /// Function to check Equipment Items price exists or not.
        /// </summary>
        /// <param name="hotelId"></param>
        /// <returns></returns>
        public bool IsExistOthersItems(int hotelId)
        {
            bool isExist = false;
            int index = 0;
            TList<PackageItems> ObjItems = GetAllOtherItems(hotelId);
            TList<PackageByHotel> ObjItemPrice = GetAllPackagePrice(hotelId);
            for (index = 0; index <= ObjItems.Count - 1; index++)
            {
                var getRow = ObjItemPrice.Find(a => a.ItemId == ObjItems[index].Id && a.PackageId == null);
                if (getRow != null)
                {
                    isExist = true;
                    break;
                }

            }

            return isExist;
        }

        /// <summary>
        /// Function to check meeting room price exist or not.
        /// </summary>
        /// <param name="hotelId"></param>
        /// <returns></returns>
        public bool IsExistMeetingRoomPrice(int hotelId)
        {
            MeetingRoomDescManager objMeetingRoom = new MeetingRoomDescManager();
            bool isExist = false;
            int index = 0;
            TList<MeetingRoom> Objrooms = objMeetingRoom.GetAllMeetingRoom(hotelId);
            for (index = 0; index <= Objrooms.Count - 1; index++)
            {
                var getRow = Objrooms.Find(a => a.HalfdayPrice != null || a.FulldayPrice != null);
                if (getRow != null)
                {
                    isExist = true;
                    break;
                }

            }

            return isExist;
        }

        /// <summary>
        /// Function to get all Equipment items.
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public TList<PackageItems> GetAllEquipmentItems(Int64 hotelID)
        {
            long countryID = DataRepository.HotelProvider.GetById(hotelID).CountryId;
            int Total = 0;
            string whereclause = "IsActive = 1 AND ItemType=2 AND CountryId='" + countryID + "'";
            propEquipmentPackageItems = DataRepository.PackageItemsProvider.GetPaged(whereclause, String.Empty, 0, int.MaxValue, out Total);
            DataRepository.PackageItemsProvider.DeepLoad(propEquipmentPackageItems);
            return propEquipmentPackageItems;
        }

        /// <summary>
        /// Function to get all Equipment items.
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public TList<PackageItems> GetAllOtherItems(Int64 hotelID)
        {
            long countryID = DataRepository.HotelProvider.GetById(hotelID).CountryId;
            int Total = 0;
            string whereclause = "IsActive = 1 AND ItemType=4 AND CountryId='" + countryID + "'";
            propEquipmentPackageItems = DataRepository.PackageItemsProvider.GetPaged(whereclause, String.Empty, 0, int.MaxValue, out Total);
            DataRepository.PackageItemsProvider.DeepLoad(propEquipmentPackageItems);
            return propEquipmentPackageItems;
        }

        /// <summary>
        /// Function to meeting room vat.
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public TList<PackageItems> GetMeetingRoomVat(int hotelID)
        {
            long countryID = DataRepository.HotelProvider.GetById(hotelID).CountryId;
            int Total = 0;
            string whereclause = "ItemType=3 AND CountryId='" + countryID + "'";
            propGetMeetingRoomVat = DataRepository.PackageItemsProvider.GetPaged(whereclause, String.Empty, 0, int.MaxValue, out Total);

            return propGetMeetingRoomVat;
        }

        /// <summary>
        /// Update meeting room price by meeting roomID.
        /// </summary>
        /// <param name="meetingRoomId"></param>
        /// <param name="halfDayPrice"></param>
        /// <param name="fullDayPrice"></param>
        /// <param name="isOnline"></param>
        /// <returns></returns>
        public string UpdateMeetingRoomPrice(int meetingRoomId, decimal? halfDayPrice, decimal? fullDayPrice, bool isOnline)
        {
            TransactionManager tm = null;
            try
            {

                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Guid gid = Guid.NewGuid();

                MeetingRoom Obj = DataRepository.MeetingRoomProvider.GetById(meetingRoomId);
                Obj.Id = meetingRoomId;
                Obj.HalfdayPrice = halfDayPrice;
                Obj.FulldayPrice = fullDayPrice;
                Obj.IsOnline = isOnline;

                //Audit trail maintain                
                MeetingRoom old = Obj.GetOriginalEntity();
                old.Id = Obj.Id;
                TrailManager.LogAuditTrail<MeetingRoom>(tm, Obj, old, AuditAction.U, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.PricingMeetingRoom), Obj.TableName, Convert.ToString(Obj.HotelId));
                //Audit trail maintain

                if (DataRepository.MeetingRoomProvider.Update(tm, Obj))
                {
                    propMassage = "Update Successfully";
                }
                else
                {
                    propMassage = "Please try again";
                }
                if (Obj.IsOnline)
                {
                   new HotelManager().SetOnlineCheck(tm, Obj.HotelId, meetingRoomId);
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return propMassage;

        }

        /// <summary>
        /// Function to addupdate package actual price by hotel id.
        /// </summary>
        /// <param name="hotelId"></param>
        /// <param name="packageId"></param>
        /// <param name="halfDayActualPrice"></param>
        /// <param name="fullDayActualPrice"></param>
        public void AddUpdatePackageActualPrice(int hotelId, int packageId, decimal? halfDayActualPrice, decimal? fullDayActualPrice)
        {
            TransactionManager tm = null;
            try
            {

                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Guid gid = Guid.NewGuid();
                TList<ActualPackagePrice> Obj = DataRepository.ActualPackagePriceProvider.GetByHotelId(hotelId);
                if (Obj.Count > 0)
                {

                    ActualPackagePrice getRow = Obj.Find(a => a.PackageId == packageId);

                    if (getRow != null)
                    {
                        getRow.ActualHalfDayPrice = halfDayActualPrice;
                        getRow.ActualFullDayPrice = fullDayActualPrice;
                        //Audit trail maintain                         
                        ActualPackagePrice old = getRow.GetOriginalEntity();
                        old.Id = getRow.Id;
                        TrailManager.LogAuditTrail<ActualPackagePrice>(tm, getRow, old, AuditAction.U, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.PricingMeetingRoom), getRow.TableName, Convert.ToString(hotelId));
                        //Audit trail maintain 
                        DataRepository.ActualPackagePriceProvider.Update(tm, getRow);
                    }
                    else
                    {
                        ActualPackagePrice objActualprice = new ActualPackagePrice();
                        objActualprice.HotelId = hotelId;
                        objActualprice.PackageId = packageId;
                        objActualprice.ActualFullDayPrice = fullDayActualPrice;
                        objActualprice.ActualHalfDayPrice = halfDayActualPrice;
                        DataRepository.ActualPackagePriceProvider.Insert(tm, objActualprice);
                        //Audit trail maintain
                        TrailManager.LogAuditTrail<ActualPackagePrice>(tm, objActualprice, null, AuditAction.I, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.PricingMeetingRoom), objActualprice.TableName, Convert.ToString(hotelId));
                        //Audit trail maintain
                    }
                }
                else
                {
                    ActualPackagePrice objActualprice = new ActualPackagePrice();
                    objActualprice.HotelId = hotelId;
                    objActualprice.PackageId = packageId;
                    objActualprice.ActualFullDayPrice = fullDayActualPrice;
                    objActualprice.ActualHalfDayPrice = halfDayActualPrice;
                    DataRepository.ActualPackagePriceProvider.Insert(tm, objActualprice);
                    //Audit trail maintain
                    TrailManager.LogAuditTrail<ActualPackagePrice>(tm, objActualprice, null, AuditAction.I, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.PricingMeetingRoom), objActualprice.TableName, Convert.ToString(hotelId));
                    //Audit trail maintain
                }

                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }

        }

        /// <summary>
        /// Function to addupdate package actual price by hotelId.
        /// </summary>
        /// <returns></returns>
        public decimal? GetRoomRentalVat()
        {
            decimal? intgetRoomRentalVat = DataRepository.PackageItemsProvider.GetAll().Find(a => a.ItemType == "3").Vat;

            return intgetRoomRentalVat;
        }

        /// <summary>
        /// Function to addupdate package actual price by hotelId. CREATED BY GAURAV
        /// </summary>
        /// <returns></returns>
        public decimal? GetRoomRentalVatByCountry(Int64 countryid)
        {
            decimal? intgetRoomRentalVat = DataRepository.PackageItemsProvider.GetAll().Find(a => a.ItemType == "3" && a.CountryId == countryid).Vat;

            return intgetRoomRentalVat;
        }

        public void DeletePackageByHotel(int hotelID, int packageID)
        {
            int Total = 0;
            string whereclause = "HotelId = '" + hotelID + "' AND PackageID='" + packageID + "'";
            string orderby = CancelationPolicyColumn.Id + " ASC";
            TList<PackageByHotel> objPackage = DataRepository.PackageByHotelProvider.GetPaged(whereclause, orderby, 0, int.MaxValue, out Total);
            if (objPackage.Count > 0)
            {
                int i = 0;
                for (i = 0; i <= objPackage.Count - 1; i++)
                {
                    DataRepository.PackageByHotelProvider.Delete(objPackage[i].Id);
                }
            }
        }

        /// <summary>
        /// This function used for Insert Items.
        /// </summary>
        /// <param name="objItems"></param>
        /// <param name="objDescription"></param>
        /// <returns></returns>
        public bool InsertItem(PackageItems objItems, PackageDescription objDescription)
        {
            bool IsSuccessFull = false;
            if (DataRepository.PackageItemsProvider.Insert(objItems))
            {
                objDescription.ItemId = objItems.Id;
                DataRepository.PackageDescriptionProvider.Insert(objDescription);
                IsSuccessFull = true;
            }
            else
            {
                IsSuccessFull = false;
            }

            return IsSuccessFull;
        }

        /// <summary>
        /// This function used for Update Item
        /// </summary>
        /// <param name="objItems"></param>
        /// <param name="objDescription"></param>
        /// <returns></returns>
        public bool UpdateItem(PackageItems objItems, PackageDescription objDescription)
        {
            bool IsSuccessFull = false;
            if (DataRepository.PackageItemsProvider.Update(objItems))
            {
                objDescription.ItemId = objItems.Id;
                DataRepository.PackageDescriptionProvider.Update(objDescription);
                IsSuccessFull = true;
            }
            else
            {
                IsSuccessFull = false;
            }

            return IsSuccessFull;
        }

        /// <summary>
        /// This function used for insert Item description 
        /// </summary>
        /// <param name="objItems"></param>
        /// <param name="objDescription"></param>
        /// <returns></returns>
        public bool InsertItemDescription(PackageItems objItems, PackageDescription objDescription)
        {
            bool IsSuccessFull = false;

            if (DataRepository.PackageItemsProvider.Update(objItems))
            {
                objDescription.ItemId = objItems.Id;
                DataRepository.PackageDescriptionProvider.Insert(objDescription);
                IsSuccessFull = true;
            }
            else
            {
                IsSuccessFull = false;
            }

            return IsSuccessFull;
        }

        /// <summary>
        /// Function to get all required language
        /// </summary>
        /// <returns></returns>
        public TList<Language> GetAllRequiredLanguage()
        {
            TList<Language> Objpackage = DataRepository.LanguageProvider.GetAll();

            return Objpackage;
        }

        /// <summary>
        /// This function used for get item by ID.
        /// </summary>
        /// <param name="intItmeID"></param>
        /// <returns></returns>
        public PackageItems GetItemByID(long intItmeID)
        {
            PackageItems objItem = DataRepository.PackageItemsProvider.GetById(intItmeID);

            return objItem;
        }

        /// <summary>
        /// This function used for Insert Items.
        /// </summary>
        /// <param name="objItems"></param>
        /// <param name="objDescription"></param>
        /// <returns></returns>
        public bool InsertPackage(PackageMaster objPackage, PackageMasterDescription objDescription)
        {
            bool IsSuccessFull = false;
            if (DataRepository.PackageMasterProvider.Insert(objPackage))
            {
                objDescription.PackageMasterId = objPackage.Id;
                DataRepository.PackageMasterDescriptionProvider.Insert(objDescription);
                IsSuccessFull = true;
            }
            else
            {
                IsSuccessFull = false;
            }

            return IsSuccessFull;
        }

        /// <summary>
        /// This function used for Update Item
        /// </summary>
        /// <param name="objItems"></param>
        /// <param name="objDescription"></param>
        /// <returns></returns>
        public bool UpdatePackage(PackageMaster objPackage, PackageMasterDescription objDescription)
        {
            bool IsSuccessFull = false;
            if (DataRepository.PackageMasterProvider.Update(objPackage))
            {
                objDescription.PackageMasterId = objPackage.Id;
                DataRepository.PackageMasterDescriptionProvider.Update(objDescription);
                IsSuccessFull = true;
            }
            else
            {
                IsSuccessFull = false;
            }

            return IsSuccessFull;
        }

        /// <summary>
        /// This function used for insert Item description 
        /// </summary>
        /// <param name="objItems"></param>
        /// <param name="objDescription"></param>
        /// <returns></returns>
        public bool InsertPackageDescription(PackageMaster objPackage, PackageMasterDescription objDescription)
        {
            bool IsSuccessFull = false;

            if (DataRepository.PackageMasterProvider.Update(objPackage))
            {
                objDescription.PackageMasterId = objPackage.Id;
                DataRepository.PackageMasterDescriptionProvider.Insert(objDescription);
                IsSuccessFull = true;
            }
            else
            {
                IsSuccessFull = false;
            }

            return IsSuccessFull;
        }

        /// <summary>
        /// This function  used for delete item for package mapping table.
        /// </summary>
        /// <param name="packageID"></param>
        /// <returns></returns>
        public bool DeletePackageItemMapping(int packageID)
        {
            bool isDeleted = false;
            TList<PackageItemMapping> obj = DataRepository.PackageItemMappingProvider.GetByPackageId(packageID);
            if (obj.Count > 0)
            {
                int i = 0;
                for (i = 0; i <= obj.Count - 1; i++)
                {
                    if (DataRepository.PackageItemMappingProvider.Delete(obj[i].Id))
                    {
                        isDeleted = true;
                    }
                }

                TList<PackageByHotel> objPackagePrice = DataRepository.PackageByHotelProvider.GetByPackageId(packageID);
                if (objPackagePrice.Count > 0)
                {
                    int j = 0;
                    for (j = 0; j <= objPackagePrice.Count - 1; j++)
                    {
                        DataRepository.PackageByHotelProvider.Delete(Convert.ToInt64(objPackagePrice[j].Id));
                    }
                }
            }
            else
            {
                isDeleted = true;
            }

            return isDeleted;
        }

        /// <summary>
        /// This function used for mapped item with package.
        /// </summary>
        /// <param name="objPackageItemMapping"></param>
        /// <returns></returns>
        public bool MappingPackgeWithItem(PackageItemMapping objPackageItemMapping)
        {
            bool IsSuccessfull = false;
            if (DataRepository.PackageItemMappingProvider.Insert(objPackageItemMapping))
            {
                IsSuccessfull = true;
            }
            else
            {
                IsSuccessfull = false;
            }
            return IsSuccessfull;
        }

        /// <summary>
        /// This function used to get description by packageID.
        /// </summary>
        /// <param name="intPackageID"></param>
        /// <returns></returns>
        public TList<PackageMasterDescription> GetPackageMasterDescription(int intPackageID)
        {
            TList<PackageMasterDescription> objDescription = DataRepository.PackageMasterDescriptionProvider.GetByPackageMasterId(intPackageID);
            return objDescription;
        }

        public TList<PackageItemMapping> GetPackgeItemByPackgeID(long intPackageID)
        {
            TList<PackageItemMapping> obj = DataRepository.PackageItemMappingProvider.GetByPackageId(intPackageID);

            return obj;
        }

        public bool DeleteItem(int intItemId)
        {
            bool isdelete = false;

            TList<PackageItemMapping> IsMapped = DataRepository.PackageItemMappingProvider.GetByItemId(intItemId);
            if (IsMapped.Count > 0)
            {
                isdelete = false;
            }
            else
            {
                TList<PackageDescription> obj = DataRepository.PackageDescriptionProvider.GetByItemId(intItemId);
                if (obj.Count > 0)
                {
                    int i = 0;
                    for (i = 0; i <= obj.Count - 1; i++)
                    {
                        DataRepository.PackageDescriptionProvider.Delete(obj[i].Id);
                    }
                }

                TList<PackageByHotel> ObjPackagePrice = DataRepository.PackageByHotelProvider.GetByItemId(intItemId);
                if (ObjPackagePrice.Count > 0)
                {
                    int i = 0;
                    for (i = 0; i <= ObjPackagePrice.Count - 1; i++)
                    {
                        DataRepository.PackageByHotelProvider.Delete(ObjPackagePrice[i].Id);

                    }
                }

                if (DataRepository.PackageItemsProvider.Delete(intItemId))
                {
                    isdelete = true;
                }

            }
            return isdelete;
        }

        public bool DeletePackage(int intPackageID)
        {
            bool isdelete = false;
            if (DeletePackageItemMapping(intPackageID))
            {
                TList<PackageByHotel> objPackagePrice = DataRepository.PackageByHotelProvider.GetByPackageId(intPackageID);
                if (objPackagePrice.Count > 0)
                {
                    int i = 0;
                    for (i = 0; i <= objPackagePrice.Count - 1; i++)
                    {
                        DataRepository.PackageByHotelProvider.Delete(Convert.ToInt64(objPackagePrice[i].Id));
                    }
                }

                TList<PackageMasterDescription> obj = DataRepository.PackageMasterDescriptionProvider.GetByPackageMasterId(intPackageID);
                if (obj.Count > 0)
                {
                    int i = 0;
                    for (i = 0; i <= obj.Count - 1; i++)
                    {
                        DataRepository.PackageMasterDescriptionProvider.Delete(Convert.ToInt64(obj[i].Id));
                    }
                }
                if (DataRepository.PackageMasterProvider.Delete(intPackageID))
                {
                    isdelete = true;
                }
            }
            return isdelete;
        }

        /// <summary>
        /// This function used for get english language ID.
        /// </summary>
        /// <returns></returns>
        public long EglishLanguageID()
        {
            long languageID = 0;
            TList<Language> objLanguage = DataRepository.LanguageProvider.GetAll();
            if (objLanguage.Count > 0)
            {
                languageID = objLanguage.Find(a => a.Name == "English").Id;
            }
            return languageID;
        }
        #endregion



    }
}
