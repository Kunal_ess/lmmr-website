﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Data;
using LMMR.Entities;
using log4net;
using log4net.Config;
#endregion

namespace LMMR.Business
{
    public abstract class CancellationPolicyBase
    {
        #region Variables and Properties
        ILog logger = log4net.LogManager.GetLogger(typeof(CancellationPolicyBase));
        //Declare Variable to store data
        private TList<CancelationPolicyLanguage> _varGetPolicyLang;

        //Property to Store the TList of Entities.HotelDesc class
        public TList<CancelationPolicyLanguage> propGetAllPolicyLang
        {
            get { return _varGetPolicyLang; }
            set { _varGetPolicyLang = value; }
        }
        #endregion

        #region
        /// <summary>
        //This method is used to get Cancellation policy for CountryId
        /// </summary>            
        public TList<CancelationPolicy> GetCancellationPolicy(int CountryId)
        {
            int totalout = 0;
            TList<CancelationPolicy> lsthPolicy = new TList<CancelationPolicy>();
            try
            {
                string whereclause = "CountryId='" + CountryId + "' and " + " IsDeleted='0' and " + " CountryDefaultPolicy='1' ";
                lsthPolicy = DataRepository.CancelationPolicyProvider.GetPaged(whereclause, String.Empty, 0, int.MaxValue, out totalout);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return lsthPolicy;
        }
        #endregion

        #region
        /// <summary>
        //This function to get policy by language id
        /// </summary>         
        public TList<CancelationPolicyLanguage> GetCancellationPolicylangByPolicyId(int Policyid)
        {
            propGetAllPolicyLang = DataRepository.CancelationPolicyLanguageProvider.GetByCancelationPolicyId(Policyid);
            return propGetAllPolicyLang;
        }
        #endregion

        /// <summary>
        /// This function to get all policy.
        /// </summary>
        /// <returns></returns>
        public TList<CancelationPolicy> GetAllPolicy()
        {
            int Total = 0;
            string whereclause = "IsDeleted = 0";
            string orderby = CancelationPolicyColumn.Id + " ASC";
            TList<CancelationPolicy> objPolicy = DataRepository.CancelationPolicyProvider.GetPaged(whereclause, orderby, 0, int.MaxValue, out Total);
            return objPolicy;
        }

        /// <summary>
        /// This function used for get language by country.
        /// </summary>
        /// <param name="countryID"></param>
        /// <returns></returns>
        public TList<CountryLanguage> GetLanguageByCountry(int countryID)
        {
            return DataRepository.CountryLanguageProvider.GetByCountryId(countryID);
        }


        /// <summary>
        /// This function used for get all language.
        /// </summary>
        /// <param name="countryID"></param>
        /// <returns></returns>
        public TList<Language> GetAllLanguage()
        {
            return DataRepository.LanguageProvider.GetAll();
        }

        /// <summary>
        /// This function used for get language Name.
        /// </summary>
        /// <param name="languageID"></param>
        /// <returns></returns>
        public Language GetLanguageByID(int languageID)
        {
            return DataRepository.LanguageProvider.GetById(languageID);
        }

        /// <summary>
        /// This function used for get all country.
        /// </summary>
        /// <returns></returns>
        public TList<Country> GetCountry()
        {
            int total = 0;
            string whereClause = CountryColumn.IsForAll + "=1 AND IsActive = 1";
            string orderby = CountryColumn.CountryName + " ASC";
            TList<Country> objcountry = DataRepository.CountryProvider.GetPaged(whereClause, orderby, 0, int.MaxValue, out total);
            return objcountry;
        }



        public bool AddNewPolicy(CancelationPolicy objCancellationPolicy, CancelationPolicyLanguage objCancellationPolicyLanguage)
        {
            TransactionManager tm = null;
            bool inserted = false;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                if (DataRepository.CancelationPolicyProvider.Insert(tm, objCancellationPolicy))
                {
                    objCancellationPolicyLanguage.CancelationPolicyId = objCancellationPolicy.Id;
                    DataRepository.CancelationPolicyLanguageProvider.Insert(tm, objCancellationPolicyLanguage);

                    inserted = true;
                }
                else
                {
                    inserted = false;
                }
                tm.Commit();
            }
            catch (Exception ex)
            {

                tm.Rollback();

            }
            return inserted;
        }


        public TList<Language> GetLanguage()
        {
            TList<Language> objLanguage = DataRepository.LanguageProvider.GetAll();

            return objLanguage;
        }

        /// <summary>
        /// This function used for get policy by ID.
        /// </summary>
        /// <param name="policyID"></param>
        /// <returns></returns>
        public CancelationPolicy GetPolicyByID(int policyID)
        {
            CancelationPolicy objCancellationPolicy = DataRepository.CancelationPolicyProvider.GetById(policyID);
            return objCancellationPolicy;
        }

        /// <summary>
        /// This section used for get policysection by policyID.
        /// </summary>
        /// <param name="policyID"></param>
        /// <returns></returns>
        public TList<CancelationPolicyLanguage> GetPolicySectionsByPolicyID(int policyID)
        {
            TList<CancelationPolicyLanguage> objCancellationpolicySection = DataRepository.CancelationPolicyLanguageProvider.GetByCancelationPolicyId(policyID);
            return objCancellationpolicySection;
        }

        /// <summary>
        /// This function used to add new section in new language.
        /// </summary>
        /// <param name="objcancellation"></param>
        /// <returns></returns>
        public bool AddSectionInNewlanguage(CancelationPolicy objcancellation, CancelationPolicyLanguage objcancellationlanguage)
        {
            bool inserted = false;
            if (DataRepository.CancelationPolicyProvider.Update(objcancellation))
            {
                DataRepository.CancelationPolicyLanguageProvider.Insert(objcancellationlanguage);
                inserted = true;
            }
            else
            {
                inserted = false;
            }
            return inserted;
        }

        /// <summary>
        /// This function used fof 
        /// </summary>
        /// <param name="objcancellation"></param>
        /// <param name="objcancellationLanguage"></param>
        /// <returns></returns>
        public bool UpdateCancellationPolicy(CancelationPolicy objcancellation, CancelationPolicyLanguage objcancellationLanguage)
        {
            bool updated = false;
            if (DataRepository.CancelationPolicyProvider.Update(objcancellation))
            {
                DataRepository.CancelationPolicyLanguageProvider.Update(objcancellationLanguage);
                updated = true;
            }
            else
            {
                updated = false;
            }
            return updated;
        }


        /// <summary>
        /// This function used for delete cancellation policy.
        /// </summary>
        /// <param name="objCancellationpolicy"></param>
        /// <returns></returns>
        public bool DeletePolicy(CancelationPolicy objCancellationpolicy)
        {
            bool deleted = false;
            if (DataRepository.CancelationPolicyProvider.Update(objCancellationpolicy))
            {
                deleted = true;
            }
            else
            {
                deleted = false;
            }

            return deleted;
        }

        /// <summary>
        /// This function used for chcek default policy
        /// </summary>
        /// <param name="countryID"></param>
        /// <returns></returns>
        public bool IsDefaultpolicyExist(int countryID)
        {
            int Total = 0;
            string whereclause = HotelColumn.CountryId + "=" + countryID + "AND IsDeleted = 0 AND DefaultCountry = 1";
            string orderby = CancelationPolicyColumn.Id + " ASC";
            TList<CancelationPolicy> objPolicy = DataRepository.CancelationPolicyProvider.GetPaged(whereclause, orderby, 0, int.MaxValue, out Total);
            if (objPolicy.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// This function used for get hotel by countryID.
        /// </summary>
        /// <param name="countryID"></param>
        /// <returns></returns>
        public TList<Hotel> GetHotelByCountryID(int countryID)
        {
            int Total = 0;
            string whereclause = HotelColumn.CountryId + "=" + countryID + " AND " + HotelColumn.IsRemoved + "= 0 AND  " + HotelColumn.IsActive + "=1";
            string orderby = HotelColumn.Name + " ASC";
            TList<Hotel> objHotel = DataRepository.HotelProvider.GetPaged(whereclause, orderby, 0, int.MaxValue, out Total);
            return objHotel;
        }

        /// <summary>
        /// This function used for get policy assigned hotel.
        /// </summary>
        /// <param name="policyID"></param>
        /// <returns></returns>
        public TList<PolicyHotelMapping> GetHotelMapping(int policyID)
        {
            TList<PolicyHotelMapping> objHotelMapping = DataRepository.PolicyHotelMappingProvider.GetByPolicyId(policyID);
            return objHotelMapping;
        }

        /// <summary>
        /// This function used for get policy by CountryID.
        /// </summary>
        /// <param name="countryID"></param>
        /// <returns></returns>
        public TList<CancelationPolicy> GetPolicyByCountry(int countryID)
        {
            int Total = 0;
            string whereclause = CancelationPolicyColumn.IsDeleted + "= 0 AND " + CancelationPolicyColumn.CountryId + "=" + countryID + "";
            string orderby = CancelationPolicyColumn.Id + " ASC";
            TList<CancelationPolicy> objCancellationpolicy = DataRepository.CancelationPolicyProvider.GetPaged(whereclause, orderby, 0, int.MaxValue, out Total);
            return objCancellationpolicy;
        }

        /// <summary>
        /// This function used to delete hotel from mapping table.
        /// </summary>
        /// <param name="policyID"></param>
        public void DeleteMapping(int policyID)
        {
            TList<PolicyHotelMapping> objmapping = DataRepository.PolicyHotelMappingProvider.GetByPolicyId(policyID);
            DataRepository.PolicyHotelMappingProvider.Delete(objmapping);
        }

        /// <summary>
        /// This function used to insert hotel mapping.
        /// </summary>
        public void InsertMapping(PolicyHotelMapping objpolicyMapping)
        {
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                DataRepository.PolicyHotelMappingProvider.Insert(objpolicyMapping);
                tm.Commit();
            }
            catch
            {
                tm.Rollback();
            }
        }

        /// <summary>
        /// This function used for get hotel name by ID
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public Hotel GetHotelById(int hotelID)
        {
            Hotel objhotel = DataRepository.HotelProvider.GetById(hotelID);
            return objhotel;
        }

        /// <summary>
        /// This function used for set default country
        /// </summary>
        /// <param name="cancellationpolicy"></param>
        /// <returns></returns>
        public bool UpdateIsDefault(CancelationPolicy cancellationpolicy)
        {
            bool updated = false;

            if (DataRepository.CancelationPolicyProvider.Update(cancellationpolicy))
            {
                updated = true;
            }
            else
            {
                updated = false;

            }
            return updated;
        }

        public bool SetDefaultPolicyOfCountry(int policyID)
        {
            bool result = false;
            try
            {
                int countryId = Convert.ToInt32(DataRepository.CancelationPolicyProvider.GetById(policyID).CountryId);
                int Total = 0;
                string whereclause = CancelationPolicyColumn.IsDeleted + "= 0 AND " + CancelationPolicyColumn.CountryId + "=" + countryId + "";
                string orderby = CancelationPolicyColumn.Id + " ASC";
                TList<CancelationPolicy> objCancellationpolicy = DataRepository.CancelationPolicyProvider.GetPaged(whereclause, orderby, 0, int.MaxValue, out Total);
                int i = 0;
                for (i = 0; i <= objCancellationpolicy.Count - 1; i++)
                {
                    CancelationPolicy objCancellation = objCancellationpolicy[i];
                    objCancellation.DefaultCountry = false;
                    DataRepository.CancelationPolicyProvider.Update(objCancellation);
                }

                CancelationPolicy obj = DataRepository.CancelationPolicyProvider.GetById(policyID);
                obj.DefaultCountry = true;
                DataRepository.CancelationPolicyProvider.Update(obj);
                result = true;
            }
            catch
            {
                result = false;
            }
            return result;
        }

        /// <summary>
        /// This fucntion used for get policy by hotel ID.
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public TList<PolicyHotelMapping> GetPolicyByHotelID(long hotelID)
        {
            TList<PolicyHotelMapping> obj = DataRepository.PolicyHotelMappingProvider.GetByHotelId(hotelID);
            return obj;
        }

    }

}
