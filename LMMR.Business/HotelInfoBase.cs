﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Data;
using LMMR.Entities;
using System.Web;

namespace LMMR.Business  
{
    public abstract class HotelInfoBase
    {
        //Declare Variable to store data
        private TList<HotelDesc> _varGetAllHotelDesc;

        //Declare Variable to store data
        private TList<Hotel> _varGetAllHotel;


        //Declare Variable to store data
        private TList<HotelDesc> _varGetHotelDesc;

        //Declare Variables to store data
        private Hotel _varGetHotel;

        //Declare Variables to store data
        private TList<UserDetails> _varUserDetails;   


        //Declare variable to get all language 
        private TList<Language> _varGetAllLanguage;

        //Declare Variable to message
        private string _varMessage;        

        //Declare variable to store latestID
        private int _varHotelID;

        //Declare variable to get all language by countryid 
        private TList<CountryLanguage> _varGetAllLanguageBycountryid;


        //Property to Store the TList of Entities.HotelDesc class
        public TList<HotelDesc> propGetAllHotelDesc
        {
            get { return _varGetAllHotelDesc; }
            set { _varGetAllHotelDesc = value; }
        }

        //Property to Store the TList of Entities.Hotel class
        public TList<Hotel> propGetAllHotel
        {
            get { return _varGetAllHotel; }
            set { _varGetAllHotel = value; }
        }

        //Property to Store the TList of Entities.CountryLanguage class
        public TList<CountryLanguage> propGetAlllanguagebyCountryId
        {
            get { return _varGetAllLanguageBycountryid; }
            set { _varGetAllLanguageBycountryid = value; }
        }

        //Property to store TList of Entities.HotelDesc class
        public TList<HotelDesc> propGetHotelDesc
        {
            get { return _varGetHotelDesc; }
            set { _varGetHotelDesc = value; }
        }

        //Property to store TList of Entities.UserDetails class
        public TList<UserDetails> propGetUserDetails
        {
            get { return _varUserDetails; }
            set { _varUserDetails = value; }
        }


        //Property to store row of Entities.Hotel class
        public Hotel propGetHotel
        {
            get { return _varGetHotel; }
            set { _varGetHotel = value; }
        }       

        //Property to store the TList of Entities.Language class
        public TList<Language> propGetAllLanguage
        {
            get { return _varGetAllLanguage; }
            set { _varGetAllLanguage = value; }
        }


        //Property to store message
        public string propMessage
        {
            get { return _varMessage; }
            set { _varMessage = value; }
        }

        //Property to store meeting room ID
        public int propHotelID
        {
            get { return _varHotelID; }
            set { _varHotelID = value; }
        }


        //function to get all MeetingRoom 
        public TList<Hotel> GetAllHotel()
        {
            propGetAllHotel = DataRepository.HotelProvider.GetAll();

            return propGetAllHotel;

        }

        //function to get all required language
        public TList<Language> GetAllRequiredLanguage()
        {
            propGetAllLanguage = DataRepository.LanguageProvider.GetAll();

            return propGetAllLanguage;
        }

        //function to get  HotelDesc by ID
        public TList<HotelDesc> GetHotelDescByHotelID(int hotelId)
        {
            propGetHotelDesc = DataRepository.HotelDescProvider.GetByHotelId(hotelId);

            return propGetHotelDesc;

        }
        
        #region
        /// <summary>
        //This function to get hotelid by unique id
        /// </summary>  
        public Hotel GetHotelByHotelID(int hotelId)
        {
            propGetHotel = DataRepository.HotelProvider.GetById(hotelId);
            return propGetHotel;
        }

        /// <summary>
        //This function to get booking details by booking id
        /// </summary>  
        public Booking GetBookingByBookingID(int bookingId)
        {
            Booking booking = DataRepository.BookingProvider.GetById(bookingId);
            return booking;
        }
        #endregion

        #region
        /// <summary>
        //This function to get Country by unique id
        /// </summary>  
        public string GetCountryByID(int id)
        {
            propMessage = DataRepository.CountryProvider.GetById(id).CountryName;
            return propMessage;
        }
        #endregion

        #region
        /// <summary>
        //This function to get City by unique id
        /// </summary>  
        public string GetCityByID(int id)
        {
            propMessage = DataRepository.CityProvider.GetById(id).City;
            return propMessage;
        }
        #endregion

        #region
        /// <summary>
        //This function to get City by unique id
        /// </summary>  
        public string GetZoneByID(int id)
        {
            propMessage = DataRepository.ZoneProvider.GetById(id).Zone;
            return propMessage;
        }
        #endregion

        #region
        /// <summary>
        //This function to get all required language on the basis of HotelDesc by ID and by country
        /// </summary>  
        public TList<CountryLanguage> GetAllRequiredLanguageByCountryId(int CountryId)
        {
            propGetAlllanguagebyCountryId = DataRepository.CountryLanguageProvider.GetByCountryId(CountryId);
            return propGetAlllanguagebyCountryId;
        }
        #endregion

        #region
        /// <summary>
        //This function to update hotel address and description by language id
        /// </summary>          
        public string UpdateHotelDesc(int id, int HotelID, int languageID, string description, string address)
        {
             TransactionManager tm = null;
             try
             {
                 tm = DataRepository.Provider.CreateTransaction();
                 tm.BeginTransaction();
                 Guid gid = Guid.NewGuid();
                 HotelDesc hoteldesc = DataRepository.HotelDescProvider.GetById(tm, id);
                 hoteldesc.Id = id;
                 hoteldesc.HotelId = Convert.ToInt64(HotelID);
                 hoteldesc.LanguageId = Convert.ToInt64(languageID);
                 hoteldesc.Description = description;
                 hoteldesc.Address = address;
                 //Audit trail maintain
                 HotelDesc old = hoteldesc.GetOriginalEntity();
                 old.Id = hoteldesc.Id;
                 TrailManager.LogAuditTrail<HotelDesc>(tm, hoteldesc, old, AuditAction.U, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.HotelConferenceInfo), hoteldesc.TableName, Convert.ToString(HotelID));
                 //Audit trail maintain
                 if (DataRepository.HotelDescProvider.Update(tm,hoteldesc))
                 {
                     propMessage = "Update successfully!";

                 }
                 else
                 {
                     propMessage = "Please try again!";
                 }
                 tm.Commit();
             }
             catch (Exception ex)
             {
                 tm.Rollback();
             }

            return propMessage;
        }
        #endregion

        #region
        /// <summary>
        //This function to insert hotel address and description by language id
        /// </summary>  
        public string InsertHotelDesc(int hotelId,int languageID, string description, string address)
        {
             TransactionManager tm = null;
             try
             {
                    tm = DataRepository.Provider.CreateTransaction();
                    tm.BeginTransaction();
                    Guid gid = Guid.NewGuid();
                    HotelDesc hotel = new HotelDesc();
                    hotel.HotelId = hotelId;
                    hotel.LanguageId = languageID;
                    hotel.Description = description;
                    hotel.Address = address;            
                    if (DataRepository.HotelDescProvider.Insert(tm, hotel))
                    {
                        propHotelID = Convert.ToInt32(hotel.Id);
                        //Audit trail maintain
                        TrailManager.LogAuditTrail<HotelDesc>(tm, hotel, hotel.GetOriginalEntity(), AuditAction.I, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.HotelConferenceInfo), hotel.TableName, Convert.ToString(hotelId));
                        //Audit trail maintain
                        propMessage = "Added successfully!";               
                    }
                    else
                    {
                        propMessage = "Please try again!";
                    }
                    tm.Commit();
             }
             catch (Exception ex)
             {
                 tm.Rollback();
             }
             return propMessage;
        }
        #endregion

        #region
        /// <summary>
        //This function to update all hotel information into Hotel table
        /// </summary> 
        public string[] UpdateHotelInfo(Hotel hotel)
        {
            TransactionManager tm = null;
            string[] Result = new string[2];
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Guid gid = Guid.NewGuid();
                //Audit trail maintain
                Hotel old = hotel.GetOriginalEntity();
                old.Id = hotel.Id;
                TrailManager.LogAuditTrail<Hotel>(tm, hotel, old, AuditAction.U, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.HotelConferenceInfo), hotel.TableName, Convert.ToString(hotel.Id));
                //Audit trail maintain
                if (DataRepository.HotelProvider.Update(tm, hotel))
                {
                    Result[0] = "Updated Succesfully.";
                    //Result[1] = "sucessfuly";
                }
                else
                {
                    Result[0] = "No record Update";
                    //Result[1] = "error";
                }
                tm.Commit();
                Hotel objHotel = new Hotel();
                
            }
            catch (Exception ex)
            {
                tm.Rollback();
                Result[0] = "Error occured wile updating.";
                //Result[1] = "error";
            }
            return Result;
        }
        #endregion


        #region
        /// <summary>
        //This function to check, whether hotel is goonline or not
        /// </summary>                  
        public TList<Hotel> GetHotelGoOnline(int hotelId)
        {
            int Totallnk = 0;
            string whereclauseTotallnk = "Id=" + hotelId + " and " + "GoOnline=1";
            TList<Hotel> lsthLink = DataRepository.HotelProvider.GetPaged(whereclauseTotallnk, String.Empty, 0, int.MaxValue, out Totallnk);
            return lsthLink;
        }
        #endregion



        #region
        /// <summary>
        //This function to check for Request Go online check 
        /// </summary>  
        public TList<Hotel> GetHotelRequestGoOnline(int hotelId)
        {
            int Totallnk = 0;
            string whereclauseTotallnk = "Id=" + hotelId + " and " + "RequestGoOnline=1";
            TList<Hotel> lsthLink = DataRepository.HotelProvider.GetPaged(whereclauseTotallnk, String.Empty, 0, int.MaxValue, out Totallnk);
            return lsthLink;
        }
        #endregion
         
        #region
        /// <summary>
        //This function to check language id on basis of hotel  
        /// </summary> 
        public TList<HotelDesc> CheckLanguageId(int hotelId, int LangId)
        {
            int Totallnk = 0;
            string whereclauseTotaldesc = "Hotel_Id=" + hotelId + " and " + "Language_Id=" + LangId + "";
            TList<HotelDesc> lsthDesc = DataRepository.HotelDescProvider.GetPaged(whereclauseTotaldesc, String.Empty, 0, int.MaxValue, out Totallnk);
            return lsthDesc;
        }
        #endregion

        #region
        /// <summary>
        //This function is to update hotel information  
        /// </summary> 
        public string UpdateHotelInformation(int id, string CountryCodePhone, string PhoneNumber, string CountryCodeFax, string FaxNumber, string TimeZone, string RtMFrom, string RtMTo, string RtAFrom, string RtATo, string RtFFrom, string RtFTo, string ZipCode, string Theme, int NumberOfBedroom, bool IsBedroomAvailable, string CreditCardType)
        {
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Hotel hotelIns = DataRepository.HotelProvider.GetById(tm, id);
                hotelIns.Id = id;
                hotelIns.CountryCodePhone = CountryCodePhone;
                hotelIns.PhoneNumber = PhoneNumber;
                hotelIns.CountryCodeFax = CountryCodeFax;
                hotelIns.FaxNumber = FaxNumber;
                hotelIns.TimeZone = TimeZone;
                hotelIns.RtMFrom = RtMFrom;
                hotelIns.RtMTo = RtMTo;
                hotelIns.RtAFrom = RtAFrom;
                hotelIns.RtATo = RtATo;
                hotelIns.RtFFrom = RtFFrom;
                hotelIns.RtFTo = RtFTo;
                hotelIns.ZipCode = ZipCode;
                hotelIns.Theme = Theme;
                hotelIns.NumberOfBedroom = NumberOfBedroom;
                hotelIns.IsBedroomAvailable = IsBedroomAvailable;
                hotelIns.CreditCardType = CreditCardType;

                if (DataRepository.HotelProvider.Update(tm, hotelIns))
                {
                    propMessage = "Update successfully!";
                }
                else
                {
                    propMessage = "Please try again!";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }

            return propMessage;
        }
        #endregion

        #region
        /// <summary>
        //This function is to get hotel description by cindition  
        /// </summary> 
        public TList<HotelDesc> GetHotelDesc(string where, string orderby)
        {
            int totalcount = 0;           
            TList<HotelDesc> hoteldesc = DataRepository.HotelDescProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount);
            return hoteldesc;
        }
        #endregion

        #region
        /// <summary>
        //This function is to get hotel details by condition  
        /// </summary> 
        public TList<Hotel> GetHotelbyCondition(string where, string orderby)
        {
            int totalcount = 0;
            TList<Hotel> hoteldtls = DataRepository.HotelProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount);
            return hoteldtls;
        }
        #endregion

        #region
        /// <summary>
        //This function is to get language by condition  
        /// </summary> 
        public TList<Language> GetLanguage(string where, string orderby)
        {
            int totalcount = 0;
            TList<Language> Language = DataRepository.LanguageProvider.GetPaged(where, orderby, 0, int.MaxValue, out totalcount);
            return Language;
        }
        #endregion

        #region
        /// <summary>
        //This function is to get hotel description by cindition  
        /// </summary> 
        public TList<UserDetails> GetUserDetails(int userId)
        {
            propGetUserDetails = DataRepository.UserDetailsProvider.GetByUserId(userId);
            return propGetUserDetails;
        }
        #endregion

        //--------------------------------------------------new-------------------------------------

        public string getHotelEmail(long hotelID)
        {
            return (DataRepository.HotelProvider.GetById(hotelID).Email);
        }

        public string getSecondaryEmail(long hotelID)
        {
            return (DataRepository.HotelProvider.GetById(hotelID).AccountingOfficer);
        }

        //--------------------------------------------------end-------------------------------------

        //----------------------------new by tariq---------------------------------
        public TList<MainPoint> getAreasByCityId(int id)
        {
            return (DataRepository.MainPointProvider.GetByCityId(id));
        }

        public MainPoint getById(int id)
        {
            return (DataRepository.MainPointProvider.GetByMainPointId(id));
        }

        public Zone getZoneById(long id)
        {
            return (DataRepository.ZoneProvider.GetById(id));
        }

        /// <summary>
        /// Bind all active hotel
        /// </summary>
        /// <param name="ClientId"></param>
        /// <returns></returns>
        public TList<Hotel> GetAllActiveHotel()
        {
            string whereclause = HotelColumn.IsRemoved + "=0" + " and " + "IsActive=1" + " and " + "GoOnline=1";
            string Orderby = HotelColumn.Name + " DESC";
            int hotelcount = 0;
            TList<Hotel> objHotels = DataRepository.HotelProvider.GetPaged(whereclause, Orderby, 0, int.MaxValue, out hotelcount);
            return objHotels;
        }

    }
}
