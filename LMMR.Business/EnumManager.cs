﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LMMR.Business
{
    public enum Usertype
    {
        Superadmin = 1,
        Operator = 2,
        Salesperson = 3,
        HotelClient = 4,
        HotelUser = 5,
        HotelGroupUser = 6,
        Agency = 7,
        agencyuser = 8,
        AgencyClient=12,
        Company = 9,
        privateuser = 10,
        SuperMan = 11
    }
    public enum RoomShape
    {
        Theatre = 1,
        School = 2,
        UShape = 3,
        Boardroom = 4,
        Cocktail = 5,
        Classroom = 6
    }

    public enum BookingRequestStatus
    {
        New = 1,
        Cancel = 2,
        Transfer = 3,
        Frozen = 4,
        Tentative = 5,
        Processed = 6,
        Definite = 7,
        Expired = 8,
        Deleted =9
    }

    // enum for bedroom type.
    public enum BedRoomType
    {
        Standard = 1,
        Classic = 2,
        Deluxe = 3,
        Superior = 4,
        Business = 5,
        Executive = 6

    }

    // enum for item type.
    public enum ItemType
    {
        FoodBeverages = 1,
        Equipment = 2,
        MeetingRoom = 3,
        Others = 4
    }


    //enum for property section left menu.
    public enum PropertyLevelSectionLeftMenu
    {
        lnkConferenceInfo = 1,
        lnkContactDetails = 2,
        lnkFacilities = 3,
        lnkCancelationPolicy = 4,
        lnkDescriptionMeetingRoom = 5,
        lnkConfiguration = 6,
        lnkPrintFactSheet = 7,
        lnkPreviewOnWeb = 8,
        lnkDescriptionBedRoom = 9,
        lnkMeetingRooms = 10,
        lnkBedrooms = 11,
        lnkPictureManagement = 12,
        lnkGoOnline = 13
    }

    public enum PropertyLevelSection
    {
        lnkGeneralInfo = 1,
        lnkMeetingRooms = 2,
        lnkBedRooms = 3,
        lnkPricing = 4,
        lnkPictureManagement = 5,
        lnkGoOnline = 6
    }

    public enum MeetingRoomShapeName
    {
        TheaterStyle = 1,
        School = 2,
        UShape = 3,
        Boardroom = 4,
        Cocktail = 5
    }

    public enum UploadFileType
    {
        Image = 0,
        Video = 1
    }

    
}
