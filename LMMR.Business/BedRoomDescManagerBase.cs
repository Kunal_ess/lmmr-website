﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;
using log4net;
using log4net.Config;
using System.Web;
#endregion


namespace LMMR.Business
{


    public abstract class BedRoomDescManagerBase
    {
        #region Variables
        //Declare Variable to store data
        private TList<BedRoomDesc> _varGetAllBedRoomDesc;

        //Declare Variable to store data
        private TList<BedRoom> _varGetAllBedRoom;

        //Declare Variable to store data
        private BedRoom _varGetBedRoom;

        //Declare variable to get all language 
        private TList<Language> _varGetAllLanguage;

        //Declare Variable to message
        private string _varMessage;

        //Declare variable to store latestID
        private int _varBedRoomID;
        ILog logger = log4net.LogManager.GetLogger(typeof(BedRoomDescManagerBase));
        #endregion

        #region Properties
        //Property to Store the TList of Entities.BedRoomDesc class
        public TList<BedRoomDesc> propGetAllBedRoomDesc
        {
            get { return _varGetAllBedRoomDesc; }
            set { _varGetAllBedRoomDesc = value; }
        }

        //Property to Store the TList of Entities.BedRoom class
        public TList<BedRoom> propGetAllBedRoom
        {
            get { return _varGetAllBedRoom; }
            set { _varGetAllBedRoom = value; }
        }

        //Property to store row of Entities.BedRoom class
        public BedRoom propGetBedRoom
        {
            get { return _varGetBedRoom; }
            set { _varGetBedRoom = value; }
        }


        //Property to store the TList of Entities.Language class
        public TList<Language> propGetAllLanguage
        {
            get { return _varGetAllLanguage; }
            set { _varGetAllLanguage = value; }
        }


        //Property to store message
        public string propMessage
        {
            get { return _varMessage; }
            set { _varMessage = value; }
        }

        //Property to store meeting room ID
        public int propBedRoomID
        {
            get { return _varBedRoomID; }
            set { _varBedRoomID = value; }
        }

        #endregion

        #region Function
        /// <summary>
        /// Function to get all list of MeetingRoomDesc
        /// </summary>
        /// <returns></returns>
        public TList<BedRoomDesc> GetAllBedRoomDesc()
        {
            propGetAllBedRoomDesc = DataRepository.BedRoomDescProvider.GetAll();

            return propGetAllBedRoomDesc;

        }

        /// <summary>
        /// Function to get BedRoomDesc by bedroomID
        /// </summary>
        /// <param name="bedRoomID"></param>
        /// <returns></returns>
        public TList<BedRoomDesc> GetBedRoomDescByBedRoomID(int bedRoomID)
        {
            propGetAllBedRoomDesc = DataRepository.BedRoomDescProvider.GetByBedRoomId(bedRoomID);

            return propGetAllBedRoomDesc;

        }


        /// <summary>
        /// Function to get all BedRooms of current hotel 
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public TList<BedRoom> GetAllBedRoom(int hotelID)
        {
            int Total = 0;
            try
            {
                string whereclause = "HotelId=" + hotelID + " and IsDeleted='" + false + "' ";
                propGetAllBedRoom = DataRepository.BedRoomProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return propGetAllBedRoom;
        }

        /// <summary>
        /// Function to get BedRoom by bedroomID
        /// </summary>
        /// <param name="bedRoomID"></param>
        /// <returns></returns>
        public BedRoom GetBedRoomByID(int bedRoomID)
        {
            propGetBedRoom = DataRepository.BedRoomProvider.GetById(bedRoomID);

            return propGetBedRoom;

        }

        /// <summary>
        /// This function used for get bedroom desc by ID
        /// </summary>
        /// <param name="bedRoomDescID"></param>
        /// <returns></returns>
        public BedRoomDesc GetBedroomDescByID(int bedRoomDescID)
        {
            return DataRepository.BedRoomDescProvider.GetById(bedRoomDescID);
        }

        /// <summary>
        /// Function to get list of all required language name
        /// </summary>
        /// <returns></returns>
        public TList<Language> GetAllRequiredLanguage()
        {
            propGetAllLanguage = DataRepository.LanguageProvider.GetAll();

            return propGetAllLanguage;
        }

        /// <summary>
        /// Function to insert new bedroom of hotel
        /// </summary>
        /// <param name="hotelID"></param>
        /// <param name="typeID"></param>
        /// <param name="allotment"></param>
        /// <param name="image"></param>
        /// <param name="updatedBy"></param>
        /// <returns></returns>
        public string AddNewBedRoom(BedRoom bedRoom, BedRoomDesc objBedroomDesc, BedRoomPictureImage ObjPic)
        {
            TransactionManager tm = null;
            bool insert = false;
            try
            {
                Hotel h = null;
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Guid gid = Guid.NewGuid();
                bedRoom.IsActive = true;
                h = DataRepository.HotelProvider.GetById(tm, bedRoom.HotelId);
                if (DataRepository.BedRoomProvider.Insert(tm, bedRoom))
                {
                    objBedroomDesc.BedRoomId = bedRoom.Id;
                    objBedroomDesc.UpdatedBy = bedRoom.UpdatedBy;
                    DataRepository.BedRoomDescProvider.Insert(tm, objBedroomDesc);
                    ObjPic.BedRoomId = bedRoom.Id;
                    ObjPic.UpdatedBy = bedRoom.UpdatedBy;
                    DataRepository.BedRoomPictureImageProvider.Insert(tm, ObjPic);
                    insert = true;

                    //Audit trail maintain
                    TrailManager.LogAuditTrail<BedRoom>(tm, bedRoom, null, AuditAction.I, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.BedroomDescription), bedRoom.TableName, Convert.ToString(h.Id));
                    TrailManager.LogAuditTrail<BedRoomDesc>(tm, objBedroomDesc, null, AuditAction.I, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.BedroomDescription), objBedroomDesc.TableName, Convert.ToString(h.Id));
                    TrailManager.LogAuditTrail<BedRoomPictureImage>(tm, ObjPic, null, AuditAction.I, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.BedroomDescription), ObjPic.TableName, Convert.ToString(h.Id));
                    //Audit trail maintain
                    int counterA = 0;
                    TList<Availability> lstAvailability = DataRepository.AvailabilityProvider.GetPaged(tm, "HotelID=" + bedRoom.HotelId + " And " + AvailabilityColumn.AvailabilityDate + ">='" + (new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day)) + "'", AvailabilityColumn.AvailabilityDate + " ASC", 0, int.MaxValue, out counterA);
                    if (lstAvailability.Count <= 0)
                    {
                        HotelManager objHM = new HotelManager();
                        objHM.AddHotelAvailability(tm, bedRoom.HotelId);
                        objHM.AddSpecialPriceandPromo(tm, bedRoom.HotelId);
                        objHM.AddRoomAvailability(tm, bedRoom.HotelId);
                        objHM.AddBedroomSpecialPrice(tm, bedRoom.HotelId);
                    }
                    propMessage = "Bedroom added successfully";
                }
                else
                {
                    propMessage = "Please try again";
                }
                tm.Commit();
                if (insert)
                {
                    int allotment = Convert.ToInt32(bedRoom.Allotment == null ? 0 : bedRoom.Allotment);
                    new HotelManager().AddRoomAvailability(null, bedRoom.HotelId, bedRoom.Id, 1, allotment);
                    new HotelManager().AddBedroomSpecialPrice(null, bedRoom.HotelId, bedRoom.Id);
                }
            }
            catch (Exception ex)
            {
                tm.Rollback();
                logger.Error(ex);
            }
            return propMessage;
        }

        /// <summary>
        /// Function to update bedroom by bedroomID of hotel 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="hotelID"></param>
        /// <param name="typeID"></param>
        /// <param name="allotment"></param>
        /// <param name="image"></param>
        /// <param name="updatedBy"></param>
        /// <returns></returns>
        public string UpdateBedRoom(BedRoom objBedroom, BedRoomDesc objBedroomDesc)
        {
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();

                //Audit trail maintain
                Guid gid = Guid.NewGuid();
                BedRoom old = objBedroom.GetOriginalEntity();
                old.Id = objBedroom.Id;
                TrailManager.LogAuditTrail<BedRoom>(tm, objBedroom, old, AuditAction.U, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.BedroomDescription), objBedroom.TableName, Convert.ToString(old.HotelId));
                BedRoomDesc olddesc = objBedroomDesc.GetOriginalEntity();
                olddesc.Id = objBedroomDesc.Id;
                TrailManager.LogAuditTrail<BedRoomDesc>(tm, objBedroomDesc, olddesc, AuditAction.U, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.BedroomDescription), objBedroomDesc.TableName, Convert.ToString(old.HotelId));
                //Audit trail maintain

                if (DataRepository.BedRoomProvider.Update(tm, objBedroom))
                {
                    objBedroomDesc.BedRoomId = objBedroom.Id;
                    objBedroomDesc.UpdatedBy = objBedroom.UpdatedBy;
                    DataRepository.BedRoomDescProvider.Update(tm, objBedroomDesc);
                    TList<BedRoomPictureImage> ObjPic = DataRepository.BedRoomPictureImageProvider.GetByBedRoomId(objBedroom.Id);
                    if (ObjPic != null)
                    {
                        BedRoomPictureImage getPic = ObjPic.Find(a => a.IsMain == true);
                        if (getPic != null)
                        {
                            getPic.ImageName = objBedroom.Picture;
                            getPic.UpdatedBy = objBedroom.UpdatedBy;
                            DataRepository.BedRoomPictureImageProvider.Update(tm, getPic);
                        }
                    }
                    propMessage = "Bedroom detail updated successfully";
                }
                else
                {
                    propMessage = "Please try again.";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
                logger.Error(ex);
            }
            return propMessage;
        }

        /// <summary>
        /// Function to delete bedroom by bedroomID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string DeleteBedRoom(int id)
        {
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                BedRoom Obj = DataRepository.BedRoomProvider.GetById(id);
                Obj.IsDeleted = true;

                //Audit trail maintain
                Guid gid = Guid.NewGuid();
                BedRoom old = Obj.GetOriginalEntity();
                old.Id = Obj.Id;
                TrailManager.LogAuditTrail<BedRoom>(tm, Obj, old, AuditAction.SD, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.BedroomDescription), Obj.TableName, Convert.ToString(Obj.HotelId));
                //Audit trail maintain

                if (DataRepository.BedRoomProvider.Update(tm, Obj))
                {
                    int myAvailCount = 0;
                    TList<AvailibilityOfRooms> myAvailability = DataRepository.AvailibilityOfRoomsProvider.GetPaged(tm, AvailibilityOfRoomsColumn.RoomId + "=" + id + " and " + AvailibilityOfRoomsColumn.RoomType + "=0", string.Empty, 0, int.MaxValue, out myAvailCount);
                    DataRepository.AvailibilityOfRoomsProvider.Delete(tm, myAvailability);
                    propMessage = "Delete successfully !";

                }
                else
                {
                    propMessage = "Please try again !";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
                logger.Error(ex);
            }

            return propMessage;

        }

        public string AddBedroomDesc(BedRoom objBedroom, BedRoomDesc objBedroomDesc)
        {

            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Guid gid = Guid.NewGuid();
                if (DataRepository.BedRoomProvider.Update(tm, objBedroom))
                {
                    objBedroomDesc.BedRoomId = objBedroom.Id;
                    objBedroomDesc.UpdatedBy = objBedroom.UpdatedBy;
                    DataRepository.BedRoomDescProvider.Insert(tm, objBedroomDesc);
                    TList<BedRoomPictureImage> ObjPic = DataRepository.BedRoomPictureImageProvider.GetByBedRoomId(objBedroom.Id);
                    if (ObjPic != null)
                    {
                        BedRoomPictureImage getPic = ObjPic.Find(a => a.IsMain == true);
                        if (getPic != null)
                        {
                            getPic.ImageName = objBedroom.Picture;
                            getPic.UpdatedBy = objBedroom.UpdatedBy;
                            DataRepository.BedRoomPictureImageProvider.Update(tm, getPic);
                        }
                    }
                    //Audit trail maintain
                    TrailManager.LogAuditTrail<BedRoomDesc>(tm, objBedroomDesc, null, AuditAction.I, Convert.ToInt64(HttpContext.Current.Session["CurrentUserID"]), Convert.ToInt32(PageType.BedroomDescription), objBedroomDesc.TableName, Convert.ToString(objBedroom.HotelId));
                    //Audit trail maintain
                    propMessage = "Bedroom detail updated successfully";
                }
                else
                {
                    propMessage = "Please Try again";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
                logger.Error(ex);
            }
            return propMessage;
        }

        /// <summary>
        /// This function use for create language name by ID
        /// </summary>
        /// <param name="languageID"></param>
        /// <returns></returns>
        public string GetLanguageNameById(int languageID)
        {
            string strLanguageName = DataRepository.LanguageProvider.GetById(languageID).Name;

            return strLanguageName;
        }


        #endregion
    }
}
