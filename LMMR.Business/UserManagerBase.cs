﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;

namespace LMMR.Business
{
    public abstract class UserManagerBase
    {
        public UserManagerBase()
        {

        }
        //login user details
        public Users LoginUser(string UserName, string Password)
        {
            int total = 0;
            string whereclause = UsersColumn.EmailId + "='" + UserName + "' and " + UsersColumn.Password + "='" + PasswordManager.Encrypt(Password, true) + "' and " + UsersColumn.IsActive + "=1 AND isnull(" + UsersColumn.IsRemoved + ",0) =0";//
            TList<Users> lstUser = DataRepository.UsersProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out total);
            if (lstUser.Count > 0)
            {
                lstUser[0].LastLogin = DateTime.Now;
                DataRepository.UsersProvider.Update(lstUser[0]);
                DataRepository.UsersProvider.DeepLoad(lstUser[0]);
                return lstUser[0];
            }
            else
            {
                return null;
            }
        }
        //Check User activation 
        public Users CheckUseractivation(string UserName)
        {
            int total = 0;
            string whereclause = UsersColumn.EmailId + "='" + UserName + "' and " + UsersColumn.IsActive + "=1";
            TList<Users> lstUser = DataRepository.UsersProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out total);
            if (lstUser.Count > 0)
            {
                return lstUser[0];
            }
            else
            {
                return null;
            }
        }
        public TList<UserDetails> getUserbyid(Int64 userid)
        {
            return DataRepository.UserDetailsProvider.GetByUserId(userid);
        }
        public Users GetUserByID(Int64 userid)
        {
            return DataRepository.UsersProvider.GetByUserId(userid);
        }

        public TList<Users> GetUserDetails(string where, string orderBy)
        {
            //TList<Users> ulist = new TList<Users>();
            int Total = 0;
            
            //ulist = DataRepository.UsersProvider.GetPaged(where, orderBy, 0, int.MaxValue, out Total);
            //DataRepository.UsersProvider.DeepLoad(ulist, true);

            //TList<Users> test = new TList<Users>();
            //test = ulist.FindAll(a => a.UserDetailsCollection.FirstOrDefault() == null ? false : (a.UserDetailsCollection.FirstOrDefault().IsDeleted == null ? false : a.UserDetailsCollection.FirstOrDefault().IsDeleted == false));

            //Where(a => a.UserDetailsCollection.FirstOrDefault() != null ? a.UserDetailsCollection.FirstOrDefault().IsDeleted == "0" : false); 
            where = where +" AND isnull("+ UsersColumn.IsRemoved +",0) =0";
            TList<Users> objUsers = DataRepository.UsersProvider.GetPaged(where, orderBy, 0, int.MaxValue, out Total);

            return objUsers;
        }

        public bool ChangeuserActiveStatus(Int32 userId, bool status)
        {
            TransactionManager tm = null;
            bool result = false;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Users objuser = DataRepository.UsersProvider.GetByUserId(userId);
                objuser.IsActive = status;
                if (objuser.Password == null)
                {
                    objuser.Password = null;
                    DataRepository.UsersProvider.Update(tm, objuser);
                }
                else
                {
                    DataRepository.UsersProvider.Update(tm, objuser);
                }

                tm.Commit();
                result = true;
            }
            catch (Exception ex)
            {
                tm.Rollback();
                result = false;
            }
            return result;
        }
    }
}
