﻿#region Using 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;
using System.Web;
#endregion

namespace LMMR.Business
{

    public abstract class WizardLinkSettingManagerBase
    {
        #region Variables
        StringBuilder ObjSB = new StringBuilder();
        FinanceInvoice obj = new FinanceInvoice();
        #endregion 

        #region Properties
        public int HotelIDLink { get; set; }

        public int hotelid {  get;set;}
        #endregion

        #region Functions
        /// <summary>
        /// This function use for get min value of propertylevel enum.
        /// </summary>
        /// <returns></returns>
        public int GetMinPropertyLevelSectionID()
        {
            int PropertyLevelSectionMin = (int)Enum.GetValues(typeof(PropertyLevelSectionLeftMenu)).Cast<PropertyLevelSectionLeftMenu>().Min();
            return PropertyLevelSectionMin;
        }

        /// <summary>
        /// This function use for get max value of enum.
        /// </summary>
        /// <returns></returns>
        public int GetMaxPropertyLevelSectionID()
        {
            int PropertyLevelSectionMax = (int)Enum.GetValues(typeof(PropertyLevelSectionLeftMenu)).Cast<PropertyLevelSectionLeftMenu>().Max();
            return PropertyLevelSectionMax;
        }

        /// <summary>
        ///  Get list of left menu link by hotelId.
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public TList<LeftMenuLink> GetLinkByHotelID(int hotelID)
        {
            TransactionManager tm = null;
            TList<LeftMenuLink> ObjList = new TList<LeftMenuLink>();
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                HotelIDLink = hotelID;                
                ObjList = DataRepository.LeftMenuLinkProvider.GetByHotelId(tm,hotelID);
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return ObjList;
        }

        /// <summary>
        /// This function to check leftmenulink for current hotel
        /// </summary>
        /// <param name="hotelId"></param>
        /// <param name="leftLink"></param>
        /// <returns>object</returns>
        public TList<LeftMenuLink> CheckLeftmenu(int hotelId,int leftLink)
        {            
            int totalout1 = 0;
            string whereclause1 = "HotelId=" + hotelId + " and " + " LeftMenuLinkId=" + leftLink + "";
            TList<LeftMenuLink> leflink = DataRepository.LeftMenuLinkProvider.GetPaged(whereclause1, String.Empty, 0, int.MaxValue, out totalout1);
            return leflink;

        }

        /// <summary>
        /// This function to get linkID of current hotel
        /// </summary>
        /// <param name="hotelId"></param>
        /// <returns></returns>
        public TList<LeftMenuLink> GetLastlinkId(int hotelId)
        {
            int totalout1 = 0;
            string whereclause1 = "HotelId=" + hotelId + "";
            string Orderby = "LeftMenuLinkId desc";
            TList<LeftMenuLink> leflink = DataRepository.LeftMenuLinkProvider.GetPaged(whereclause1, Orderby, 0, int.MaxValue, out totalout1);
            return leflink;
        }
        
        /// <summary>
        /// Thisfunction to get isOnline status by hotelID.
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns>bool</returns>
        public bool IsGoOnline(int hotelID)
        {
            Hotel ObjHotel = new Hotel();
            ObjHotel = DataRepository.HotelProvider.GetById(hotelID);
            bool flag = ObjHotel.GoOnline == null ? false : ObjHotel.GoOnline.Value;
            return flag;
        }

        /// <summary>
        /// This Function use for enable links of left menu.
        /// </summary>
        /// <param name="linkID"></param>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public string LoadLeftMenu(string linkID, int hotelID)
        {
            bool flag = IsGoOnline(hotelID);
            if (flag == true)
            {
                return LoadEnableLeftMenu(linkID);
            }
            else
            {
                return LoadDisableLeftMenu(linkID);
            }

        }

        /// <summary>
        /// This function to return disabled left menu string.
        /// </summary>
        /// <param name="linkID"></param>
        /// <returns></returns>
        private string LoadDisableLeftMenu(string linkID)
        {
            
            //---- Below three link should always be enabled.
            string currenturl = HttpContext.Current.Request.Url.ToString();
            if (linkID == "lnkNews")
            {

                // start blue white box-->
                ObjSB.Append(@"<div class=""blue_white_box"">");
                ObjSB.Append(@"<h2>Last minute meeting</h2>");
                ObjSB.Append(@"<div class=""content"">");
                ObjSB.Append(@"<ul class=""innerlist"">");
                if (currenturl.ToLower().Contains("/news.aspx"))
                {
                    ObjSB.Append(@"<li><a href=""News.aspx"" class=""seltd selected"" runat=""server"" id=""lnklnkNews"">News</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""News.aspx"" class=""seltd"" runat=""server"" id=""lnklnkNews"">News</a></li>");
                }

                if (currenturl.ToLower().Contains("/generaldeliveryterms.aspx"))
                {
                    ObjSB.Append(@"<li><a href=""GeneralDeliveryTerms.aspx"" class=""seltd selected"" runat=""server"" id=""lnklnkGdt"">GDT (General Delivery Terms)</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""GeneralDeliveryTerms.aspx"" class=""seltd"" runat=""server"" id=""lnklnkGdt"">GDT (General Delivery Terms)</a></li>");
                }

                if (currenturl.ToLower().Contains("/changepassword.aspx"))
                {
                    ObjSB.Append(@"<li><a href=""ChangePassword.aspx"" runat=""server"" class=""seltd selected""  id=""lnkchangePassword"">Change Password</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""ChangePassword.aspx"" runat=""server"" class=""seltd"" id=""lnkchangePassword"">Change Password</a></li>");
                }

                ObjSB.Append(@"</ul>");
                ObjSB.Append(@"</div>");
                ObjSB.Append(@"</div>");


            }
            else if (linkID == "lnkGdt")
            {

                // start blue white box-->
                ObjSB.Append(@"<div class=""blue_white_box"">");
                ObjSB.Append(@"<h2>Last minute meeting</h2>");
                ObjSB.Append(@"<div class=""content"">");
                ObjSB.Append(@"<ul class=""innerlist"">");
                if (currenturl.ToLower().Contains("/news.aspx"))
                {
                    ObjSB.Append(@"<li><a href=""News.aspx"" runat=""server"" class=""seltd selected"" id=""lnklnkNews"">News</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""News.aspx"" runat=""server"" class=""seltd"" id=""lnklnkNews"">News</a></li>");
                }
                if (currenturl.ToLower().Contains("/generaldeliveryterms.aspx"))
                {
                    ObjSB.Append(@"<li><a href=""GeneralDeliveryTerms.aspx"" class=""seltd selected"" runat=""server"" id=""lnklnkGdt"">GDT (General Delivery Terms)</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""GeneralDeliveryTerms.aspx"" class=""seltd"" runat=""server"" id=""lnklnkGdt"">GDT (General Delivery Terms)</a></li>");
                }
                if (currenturl.ToLower().Contains("/changepassword.aspx"))
                {
                    ObjSB.Append(@"<li><a href=""ChangePassword.aspx"" class=""seltd selected"" runat=""server"" id=""lnkchangePassword"">Change Password</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""ChangePassword.aspx"" class=""seltd"" runat=""server"" id=""lnkchangePassword"">Change Password</a></li>");
                }
                ObjSB.Append(@"</ul>");
                ObjSB.Append(@"</div>");
                ObjSB.Append(@"</div>");


            }
            else if (linkID == "lnkchangePassword")
            {

                // start blue white box-->
                ObjSB.Append(@"<div class=""blue_white_box"">");
                ObjSB.Append(@"<h2>Last minute meeting</h2>");
                ObjSB.Append(@"<div class=""content"">");
                ObjSB.Append(@"<ul class=""innerlist"">");
                if (currenturl.ToLower().Contains("/news.aspx"))
                {
                    ObjSB.Append(@"<li><a href=""News.aspx"" class=""seltd selected"" runat=""server"" id=""lnklnkNews"">News</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""News.aspx"" class=""seltd"" runat=""server"" id=""lnklnkNews"">News</a></li>");
                }
                if (currenturl.ToLower().Contains("/generaldeliveryterms.aspx"))
                {
                    ObjSB.Append(@"<li><a href=""GeneralDeliveryTerms.aspx"" class=""seltd selected"" runat=""server"" id=""lnklnkGdt"">GDT (General Delivery Terms)</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""GeneralDeliveryTerms.aspx"" class=""seltd"" runat=""server"" id=""lnklnkGdt"">GDT (General Delivery Terms)</a></li>");
                }
                if (currenturl.ToLower().Contains("/changepassword.aspx"))
                {
                    ObjSB.Append(@"<li><a href=""ChangePassword.aspx"" runat=""server"" class=""seltd selected"" id=""lnkchangePassword"">Change Password</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""ChangePassword.aspx"" runat=""server"" class=""seltd"" id=""lnkchangePassword"">Change Password</a></li>");
                }
                ObjSB.Append(@"</ul>");
                ObjSB.Append(@"</div>");
                ObjSB.Append(@"</div>");


            }
           
           else if (linkID == "lnkAvailability")
            {

                // start blue white box-->
                ObjSB.Append(@"<div class=""blue_white_box"">");
                ObjSB.Append(@"<h2>Availability</h2>");
                ObjSB.Append(@"<div class=""content"">");
                ObjSB.Append(@"<ul class=""innerlist"">");
                ObjSB.Append(@"<li><a style=""color:#999999"" disabled=""disabled"" runat=""server"" id=""lnkConferenceInfo"">Change / Update availability</a></li>");
                ObjSB.Append(@"</ul>");
                ObjSB.Append(@"</div>");
                ObjSB.Append(@"</div>");
                // start blue white box-->
                ObjSB.Append(@"<div class=""blue_white_box"">");
                ObjSB.Append(@"<h2>Legend</h2>");
                ObjSB.Append(@"<div class=""content"">");
                ObjSB.Append(@"<ul class=""innerlist"">");
                ObjSB.Append(@"<li><img src=""../images/closed.jpg"" alt=""Closed"" align=""left"" />Closed</li>");
                ObjSB.Append(@"<li><img src=""../images/availability.PNG"" alt=""Available"" align=""left"" style=""margin-right: 5px;"" />Available</li>");
                ObjSB.Append(@"<li><img src=""../images/sold.jpg"" alt=""Sold Out"" align=""left"" />Sold Out</li>");
                ObjSB.Append(@"<li><img src=""../images/booked.jpg"" alt=""Booked Online"" align=""left"" />Booked Online</li>");
                ObjSB.Append(@"</ul>");
                ObjSB.Append(@"</div>");
                ObjSB.Append(@"</div>");

            }

            else if (linkID == "lnkBookings")
            {

                // start blue white box-->
                ViewBooking_Hotel objRequest = new ViewBooking_Hotel();


                int CNT = objRequest.countbooking(Convert.ToInt32(hotelid));

                // start blue white box-->
                ObjSB.Append(@"<div class=""blue_white_box"">");
                ObjSB.Append(@"<h2>Booking</h2>");
                ObjSB.Append(@"<div class=""content"">");
                ObjSB.Append(@"<ul class=""innerlist"">");

                ObjSB.Append(@"<li><a style=""color:#999999"" runat=""server"" disabled=""disabled"" id=""lnkViewNewBooking"">View new (" + CNT + ")</a></li>");
                ObjSB.Append(@"<li><a style=""color:#999999"" runat=""server"" disabled=""disabled"" id=""lnkViewProcessed"">See old bookings(" + Convert.ToString(objRequest.countProcessedbooking(hotelid)) + ")</a></li>");
                ObjSB.Append(@"<li><a style=""color:#999999"" runat=""server"" disabled=""disabled"" id=""lnkDetailedSearch"">Detailed search</a></li>");

                ObjSB.Append(@"</ul>");
                ObjSB.Append(@"</div>");
                ObjSB.Append(@"</div>");

            }
            else if (linkID == "lnkFinance")
            {

                // start blue white box-->
                ObjSB.Append(@"<div class=""blue_white_box"">");
                ObjSB.Append(@"<h2>Finance</h2>");
                ObjSB.Append(@"<div class=""content"">");
                ObjSB.Append(@"<ul class=""innerlist"">");
                ObjSB.Append(@"<li><a style=""color:#999999"" runat=""server"" id=""lnkConferenceInfo"">Information</a></li>");
                ObjSB.Append(@"<li><a style=""color:#999999"" runat=""server"" id=""lnkContactDetails"">Statement</a></li>");
                ObjSB.Append(@"</ul>");
                ObjSB.Append(@"</div>");
                ObjSB.Append(@"</div>");


            }
            else if (linkID == "lnkStatistics")
            {
                // start blue white box-->
                ObjSB.Append(@"<div class=""blue_white_box"">");
                ObjSB.Append(@"<h2>Statistics</h2>");
                ObjSB.Append(@"<div class=""content"">");
                ObjSB.Append(@"<ul class=""innerlist"">");
                ObjSB.Append(@"<li><a style=""color:#999999"" runat=""server""  id=""lnkStatVisitors"">Visitors</a></li>");
                ObjSB.Append(@"<li><a style=""color:#999999"" runat=""server"" id=""lnkStatBooking"">Bookings</a></li>");
                ObjSB.Append(@"<li><a style=""color:#999999"" runat=""server"" id=""lnkStatRequest"">Requests</a></li>");
                ObjSB.Append(@"</ul>");
                ObjSB.Append(@"</div>");
                ObjSB.Append(@"</div>");
            }
            else if (linkID == "lnkPromotions")
            {

                // start blue white box-->
                ObjSB.Append(@"<div class=""blue_white_box"">");
                ObjSB.Append(@"<h2>Pricing / Hotel Promo</h2>");
                ObjSB.Append(@"<div class=""content"">");
                ObjSB.Append(@"<ul class=""innerlist"">");
                ObjSB.Append(@"<li><a style=""color:#999999"" disabled=""disabled"" runat=""server"" id=""lnkConferenceInfo"">Set special prices</a></li>");
                ObjSB.Append(@"<li><a style=""color:#999999"" disabled=""disabled"" runat=""server"" id=""lnkConferenceInfo"">Add Promotion</a></li>");
                ObjSB.Append(@"</ul>");
                ObjSB.Append(@"</div>");
                ObjSB.Append(@"</div>");


            }

            else if (linkID == "lnkRequests")
            {

                ViewBooking_Hotel objRequest = new ViewBooking_Hotel();


                int CNT = objRequest.countrequest(Convert.ToInt32(hotelid));

                // start blue white box-->
                ObjSB.Append(@"<div class=""blue_white_box"">");
                ObjSB.Append(@"<h2>Request</h2>");
                ObjSB.Append(@"<div class=""content"">");
                ObjSB.Append(@"<ul class=""innerlist"">");

                ObjSB.Append(@"<li><a style=""color:#999999"" runat=""server"" disabled=""disabled"" id=""lnkViewNewBooking"">(" + CNT + ")New Request(s) </a></li>");
                ObjSB.Append(@"<li><a style=""color:#999999"" runat=""server"" disabled=""disabled"" id=""lnkViewProcessed"">Active requests</a></li>");
                ObjSB.Append(@"<li><a style=""color:#999999"" runat=""server"" disabled=""disabled"" id=""lnkDetailedSearch"">Detailed search</a></li>");

                ObjSB.Append(@"</ul>");
                ObjSB.Append(@"</div>");
                ObjSB.Append(@"</div>");
            }

            

            return ObjSB.ToString();

        }

        /// <summary>
        /// This function to return enabled left menu string.
        /// </summary>
        /// <param name="linkID"></param>
        /// <returns></returns>
        private string LoadEnableLeftMenu(string linkID)
        {
            string currenturl = HttpContext.Current.Request.Url.ToString();
            if (linkID == "lnkNews")
            {

                // start blue white box-->
                ObjSB.Append(@"<div class=""blue_white_box"">");
                ObjSB.Append(@"<h2>Last minute meeting</h2>");
                ObjSB.Append(@"<div class=""content"">");
                ObjSB.Append(@"<ul class=""innerlist"">");
                if (currenturl.ToLower().Contains("/news.aspx"))
                {
                    ObjSB.Append(@"<li><a href=""News.aspx"" class=""seltd selected"" runat=""server"" id=""lnklnkNews"">News</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""News.aspx"" class=""seltd"" runat=""server"" id=""lnklnkNews"">News</a></li>");
                }

                if (currenturl.ToLower().Contains("/generaldeliveryterms.aspx"))
                {
                    ObjSB.Append(@"<li><a href=""GeneralDeliveryTerms.aspx"" class=""seltd selected"" runat=""server"" id=""lnklnkGdt"">GDT (General Delivery Terms)</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""GeneralDeliveryTerms.aspx"" class=""seltd"" runat=""server"" id=""lnklnkGdt"">GDT (General Delivery Terms)</a></li>");
                }

                if (currenturl.ToLower().Contains("/changepassword.aspx"))
                {
                    ObjSB.Append(@"<li><a href=""ChangePassword.aspx"" runat=""server"" class=""seltd selected""  id=""lnkchangePassword"">Change Password</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""ChangePassword.aspx"" runat=""server"" class=""seltd"" id=""lnkchangePassword"">Change Password</a></li>");
                }
                
                ObjSB.Append(@"</ul>");
                ObjSB.Append(@"</div>");
                ObjSB.Append(@"</div>");


            }
            else if (linkID == "lnkGdt")
            {

                // start blue white box-->
                ObjSB.Append(@"<div class=""blue_white_box"">");
                ObjSB.Append(@"<h2>Last minute meeting</h2>");
                ObjSB.Append(@"<div class=""content"">");
                ObjSB.Append(@"<ul class=""innerlist"">");
                if (currenturl.ToLower().Contains("/news.aspx"))
                {
                    ObjSB.Append(@"<li><a href=""News.aspx"" runat=""server"" class=""seltd selected"" id=""lnklnkNews"">News</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""News.aspx"" runat=""server"" class=""seltd"" id=""lnklnkNews"">News</a></li>");
                }
                if (currenturl.ToLower().Contains("/generaldeliveryterms.aspx"))
                {
                    ObjSB.Append(@"<li><a href=""GeneralDeliveryTerms.aspx"" class=""seltd selected"" runat=""server"" id=""lnklnkGdt"">GDT (General Delivery Terms)</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""GeneralDeliveryTerms.aspx"" class=""seltd"" runat=""server"" id=""lnklnkGdt"">GDT (General Delivery Terms)</a></li>");
                }
                if (currenturl.ToLower().Contains("/changepassword.aspx"))
                {
                    ObjSB.Append(@"<li><a href=""ChangePassword.aspx"" class=""seltd selected"" runat=""server"" id=""lnkchangePassword"">Change Password</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""ChangePassword.aspx"" class=""seltd"" runat=""server"" id=""lnkchangePassword"">Change Password</a></li>");
                }
                ObjSB.Append(@"</ul>");
                ObjSB.Append(@"</div>");
                ObjSB.Append(@"</div>");


            }
            else if (linkID == "lnkchangePassword")
            {

                // start blue white box-->
                ObjSB.Append(@"<div class=""blue_white_box"">");
                ObjSB.Append(@"<h2>Last minute meeting</h2>");
                ObjSB.Append(@"<div class=""content"">");
                ObjSB.Append(@"<ul class=""innerlist"">");
                if (currenturl.ToLower().Contains("/news.aspx"))
                {
                    ObjSB.Append(@"<li><a href=""News.aspx"" class=""seltd selected"" runat=""server"" id=""lnklnkNews"">News</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""News.aspx"" class=""seltd"" runat=""server"" id=""lnklnkNews"">News</a></li>");
                }
                if (currenturl.ToLower().Contains("/generaldeliveryterms.aspx"))
                {
                    ObjSB.Append(@"<li><a href=""GeneralDeliveryTerms.aspx"" class=""seltd selected"" runat=""server"" id=""lnklnkGdt"">GDT (General Delivery Terms)</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""GeneralDeliveryTerms.aspx"" class=""seltd"" runat=""server"" id=""lnklnkGdt"">GDT (General Delivery Terms)</a></li>");
                }
                if (currenturl.ToLower().Contains("/changepassword.aspx"))
                {
                    ObjSB.Append(@"<li><a href=""ChangePassword.aspx"" runat=""server"" class=""seltd selected"" id=""lnkchangePassword"">Change Password</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""ChangePassword.aspx"" runat=""server"" class=""seltd"" id=""lnkchangePassword"">Change Password</a></li>");
                }
                ObjSB.Append(@"</ul>");
                ObjSB.Append(@"</div>");
                ObjSB.Append(@"</div>");


            }
           else if (linkID == "lnkAvailability")
            {
                // start blue white box-->
                ObjSB.Append(@"<div class=""blue_white_box1"">");
                ObjSB.Append(@"<h2>Availability</h2>");
                ObjSB.Append(@"<div class=""content1"">");
                ObjSB.Append(@"<ul class=""innerlist1"">");
                if (currenturl.ToLower().Contains("/manageavailability.aspx"))
                {
                    ObjSB.Append(@"<li><a href=""ManageAvailability.aspx"" runat=""server"" class=""seltd selected"" id=""lnkConferenceInfo"">Change / Update  availability</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""ManageAvailability.aspx"" runat=""server"" class=""seltd"" id=""lnkConferenceInfo"">Change / Update  availability</a></li>");
                }
                ObjSB.Append(@"</ul>");
                ObjSB.Append(@"</div>");
                ObjSB.Append(@"</div>");

                // start blue white box-->
                ObjSB.Append(@"<div class=""blue_white_box"">");
                ObjSB.Append(@"<h2>Legend</h2>");
                ObjSB.Append(@"<div class=""content"">");
                ObjSB.Append(@"<ul class=""innerlist"">");
                ObjSB.Append(@"<li><img src=""../images/closed.jpg"" alt=""Closed"" align=""left"" />Closed</li>");
                ObjSB.Append(@"<li><img src=""../images/availability.PNG"" alt=""Available"" align=""left"" style=""margin-right: 5px;"" />Available</li>");
                ObjSB.Append(@"<li><img src=""../images/sold.jpg"" alt=""Sold Out"" align=""left"" />Sold Out</li>");
                ObjSB.Append(@"<li><img src=""../images/booked.jpg"" alt=""Booked Online"" align=""left"" />Booked Online</li>");
                ObjSB.Append(@"</ul>");
                ObjSB.Append(@"</div>");
                ObjSB.Append(@"</div>");

            }

            else if (linkID == "lnkBookings")
            {


                ViewBooking_Hotel objRequest = new ViewBooking_Hotel();


                int CNT = objRequest.countbooking(Convert.ToInt32(hotelid));

                // start blue white box-->
                ObjSB.Append(@"<div class=""blue_white_box1"">");

                ObjSB.Append(@"<h2>Booking</h2>");
                ObjSB.Append(@"<div class=""content1"">");
                ObjSB.Append(@"<ul class=""innerlist1"">");
                if (currenturl.ToLower().Contains("/viewbookings.aspx?type=1"))
                {
                    ObjSB.Append(@"<li><a href=""ViewBookings.aspx?type=1"" runat=""server"" class=""seltd selected"" id=""lnkViewNewBooking"">View new (" + CNT + ")</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""ViewBookings.aspx?type=1"" runat=""server"" class=""seltd"" id=""lnkViewNewBooking"">View new (" + CNT + ")</a></li>");
                }
                if (currenturl.ToLower().Contains("/viewbookings.aspx?type=6"))
                {
                    ObjSB.Append(@"<li><a href=""ViewBookings.aspx?type=6"" runat=""server"" class=""seltd selected"" id=""lnkViewProcessed"">Active bookings (" + Convert.ToString(objRequest.countProcessedbooking(hotelid)) + ")</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""ViewBookings.aspx?type=6"" runat=""server"" class=""seltd"" id=""lnkViewProcessed"">Active bookings (" + Convert.ToString(objRequest.countProcessedbooking(hotelid)) + ")</a></li>");
                }
                if (currenturl.ToLower().Contains("/detailbooking.aspx?type=0"))
                {
                    ObjSB.Append(@"<li><a href=""Detailbooking.aspx?type=0"" runat=""server"" class=""seltd selected""  id=""lnkDetailedSearch"">Detailed search</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""Detailbooking.aspx?type=0"" runat=""server"" class=""seltd""  id=""lnkDetailedSearch"">Detailed search</a></li>");
                }

                ObjSB.Append(@"</ul>");
                ObjSB.Append(@"</ul>");
                ObjSB.Append(@"</div>");
                ObjSB.Append(@"</div>");

            }
            else if (linkID == "lnkFinance")
            {
                string whereClause = "HotelId=" + Convert.ToInt32(hotelid) + " and " + "InvoiceSent=1" + " and " + "Ispaid=0";                
                TList<Invoice> lstInvoice = obj.GetinvoicebyClient(whereClause);
                int CntFin = lstInvoice.Count;

                // start blue white box-->
                ObjSB.Append(@"<div class=""blue_white_box1"">");
                ObjSB.Append(@"<h2>Finance</h2>");
                ObjSB.Append(@"<div class=""content1"">");
                ObjSB.Append(@"<ul class=""innerlist1"">");
                if (currenturl.ToLower().Contains("/financeinformation.aspx"))
                {
                    ObjSB.Append(@"<li><a href=""FinanceInformation.aspx"" runat=""server"" class=""seltd selected"" id=""lnkConferenceInfo"">Information</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""FinanceInformation.aspx"" runat=""server"" class=""seltd"" id=""lnkConferenceInfo"">Information</a></li>");
                }
                if (currenturl.ToLower().Contains("/financeinvoice.aspx"))
                {
                    ObjSB.Append(@"<li><a href=""FinanceInvoice.aspx"" runat=""server"" class=""seltd selected"" id=""lnkContactDetails"">Statement(" + CntFin + ")</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""FinanceInvoice.aspx"" runat=""server"" class=""seltd"" id=""lnkContactDetails"">Statement(" + CntFin + ")</a></li>");
                }
                ObjSB.Append(@"</ul>");
                ObjSB.Append(@"</div>");
                ObjSB.Append(@"</div>");

            }
            else if (linkID == "lnkStatistics")
            {

                // start blue white box-->
                ObjSB.Append(@"<div class=""blue_white_box1"">");
                ObjSB.Append(@"<h2>Statistics</h2>");
                ObjSB.Append(@"<div class=""content1"">");
                ObjSB.Append(@"<ul class=""innerlist1"">");
                if (currenturl.ToLower().Contains("/statistics.aspx?type=1"))
                {

                    ObjSB.Append(@"<li><a  href=""Statistics.aspx?type=1""  class=""seltd selected"" runat=""server"" id=""lnkStatVisitors"">Visitors</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a  href=""Statistics.aspx?type=1""  class=""seltd"" runat=""server"" id=""lnkStatVisitors"">Visitors</a></li>");
                }
                if (currenturl.ToLower().Contains("/statistics.aspx?type=2"))
                {
                    ObjSB.Append(@"<li><a  href=""Statistics.aspx?type=2"" runat=""server""  class=""seltd selected"" id=""lnkStatBooking"">Bookings</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a  href=""Statistics.aspx?type=2"" runat=""server""  class=""seltd"" id=""lnkStatBooking"">Bookings</a></li>");
                }
                if (currenturl.ToLower().Contains("/statistics.aspx?type=3"))
                {
                    ObjSB.Append(@"<li><a  href=""statistics.aspx?type=3"" runat=""server""  class=""seltd selected"" id=""lnkStatRequest"">Requests</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a  href=""statistics.aspx?type=3"" runat=""server""  class=""seltd"" id=""lnkStatRequest"">Requests</a></li>");
                }
                ObjSB.Append(@"</ul>");
                ObjSB.Append(@"</div>");
                ObjSB.Append(@"</div>");

            }
            else if (linkID == "lnkPromotions")
            {
                
                // start blue white box-->
                ObjSB.Append(@"<div class=""blue_white_box1"">");
                ObjSB.Append(@"<h2>Pricing / Hotel Promo</h2>");
                ObjSB.Append(@"<div class=""content1"">");
                ObjSB.Append(@"<ul class=""innerlist1"">");
                if (currenturl.ToLower().Contains("/salesandpromo.aspx"))
                {
                    ObjSB.Append(@"<li><a href=""Salesandpromo.aspx""  runat=""server"" class=""seltd selected"" id=""lnkConferenceInfo"">Set special prices</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""Salesandpromo.aspx""  runat=""server"" class=""seltd"" id=""lnkConferenceInfo"">Set special prices</a></li>");
                }
                if (currenturl.ToLower().Contains("/addpromotion.aspx"))
                {
                    ObjSB.Append(@"<li><a href=""AddPromotion.aspx"" runat=""server"" class=""seltd selected"" id=""lnkConferenceInfo"">Add Promotion</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""AddPromotion.aspx"" runat=""server"" class=""seltd"" id=""lnkConferenceInfo"">Add Promotion</a></li>");
                }
                ObjSB.Append(@"</ul>");
                ObjSB.Append(@"</div>");
                ObjSB.Append(@"</div>");
            }

            else if (linkID == "lnkRequests")
            {
                ViewBooking_Hotel objRequest = new ViewBooking_Hotel();


                int CNT = objRequest.countrequest(Convert.ToInt32(hotelid));

                // start blue white box-->
                ObjSB.Append(@"<div class=""blue_white_box1"">");
                ObjSB.Append(@"<h2>Request</h2>");
                ObjSB.Append(@"<div class=""content1"">");
                ObjSB.Append(@"<ul class=""innerlist1"">");
                if (currenturl.ToLower().Contains("/viewrequest.aspx?type=1"))
                {
                    ObjSB.Append(@"<li><a href=""ViewRequest.aspx?type=1"" runat=""server"" class=""seltd selected""   id=""lnkViewNewBooking"">(" + CNT + ") New Request(s) </a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""ViewRequest.aspx?type=1"" runat=""server"" class=""seltd""   id=""lnkViewNewBooking"">(" + CNT + ") New Request(s)</a></li>");
                }
                if (currenturl.ToLower().Contains("/viewrequest.aspx?type=5"))
                {
                    ObjSB.Append(@"<li><a href=""ViewRequest.aspx?type=5"" runat=""server"" class=""seltd selected"" id=""lnkViewProcessed"">Active requests</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""ViewRequest.aspx?type=5"" runat=""server"" class=""seltd"" id=""lnkViewProcessed"">Active requests</a></li>");
                }
                if (currenturl.ToLower().Contains("/viewdetailrequest.aspx?type=1"))
                {
                    ObjSB.Append(@"<li><a href=""Viewdetailrequest.aspx?type=1"" runat=""server"" class=""seltd selected""  id=""lnkDetailedSearch"">Detailed search</a></li>");
                }
                else
                {
                    ObjSB.Append(@"<li><a href=""Viewdetailrequest.aspx?type=1"" runat=""server"" class=""seltd""  id=""lnkDetailedSearch"">Detailed search</a></li>");
                }

                ObjSB.Append(@"</ul>");
                ObjSB.Append(@"</div>");
                ObjSB.Append(@"</div>");
            }


            return ObjSB.ToString();

        }

        /// <summary>
        /// This function use to insert pagelinkID into table.
        /// </summary>
        /// <param name="hotelID"></param>
        /// <param name="pageLinkID"></param>
        /// <returns></returns>
        public string ThisPageIsDone(int hotelID, int pageLinkID)
        {
            TransactionManager tm = null;
            string Result = string.Empty;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                TList<LeftMenuLink> ObjGetLeftMenu = DataRepository.LeftMenuLinkProvider.GetByHotelId(hotelID);
                if (ObjGetLeftMenu.Count > 0)
                {
                    var row = ObjGetLeftMenu.Find(a => a.LeftMenuLinkId == pageLinkID);

                    if (row == null)
                    {

                        LeftMenuLink ObjLeftMenu = new LeftMenuLink();
                        ObjLeftMenu.HotelId = hotelID;
                        ObjLeftMenu.LeftMenuLinkId = pageLinkID;

                        if (DataRepository.LeftMenuLinkProvider.Insert(tm,ObjLeftMenu))
                        {
                            Result = "Insert Successfully";
                        }
                        else
                        {
                            Result = "Try Again";
                        }
                    }

                }
                else
                {
                    LeftMenuLink ObjLeftMenu = new LeftMenuLink();
                    ObjLeftMenu.HotelId = hotelID;
                    ObjLeftMenu.LeftMenuLinkId = pageLinkID;

                    if (DataRepository.LeftMenuLinkProvider.Insert(tm,ObjLeftMenu))
                    {
                        Result = "Insert Successfully";
                    }
                    else
                    {
                        Result = "Try Again";
                    }
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return Result;
        }
        #endregion
    }

}



