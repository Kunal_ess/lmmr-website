﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;

namespace LMMR.Business
{
    public abstract class CreditCardManagerBase
    {
        /// <summary>
        /// This function used for get all country.
        /// </summary>
        /// <returns></returns>
        public TList<Country> GetCountry()
        {
            return DataRepository.CountryProvider.GetAll();
        }

        /// <summary>
        /// This function used to get accepted credit card for specific user.
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public Hotel GetCreditCard(Int64 hotelID)
        {
            return DataRepository.HotelProvider.GetById(hotelID);
        }

        /// <summary>
        /// This function used for save credit card detail in database.
        /// </summary>
        /// <param name="objCreditCardDetail"></param>
        /// <returns></returns>
        public string SaveCreditCardDeatil(CreditCardDetail objCreditCardDetail)
        {
            string msg = string.Empty;
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                if (DataRepository.CreditCardDetailProvider.Insert(tm, objCreditCardDetail))
                {
                    msg = "Detail send successfully.";
                }
                else
                {
                    msg = "Please try again.";
                }
                tm.Commit();
            }
            catch
            {
                tm.Rollback();
            }

            return msg;
        }
    }
}
