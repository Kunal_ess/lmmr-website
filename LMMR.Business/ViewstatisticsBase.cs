﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;
using System.Data;
using System.Timers;


namespace LMMR.Business
{
    #region user defined class
    public class Fetchdata
   {
       public string currentdate { get; set; }
       public int statisticvalue { get; set; }
       public int confirmvalue { get; set; }
       public decimal conversion { get; set; }
       public int Booking { get; set; }
       public int FailureCancel { get; set; }
       public int FailureTimeout { get; set; }
       public int Sensitive { get; set; }
       public decimal ConversionBooking { get; set; }
       public decimal ConversionReq { get; set; }
       public decimal ConversionConfirmReq { get; set; }
      

   }
   public class Fetchlabel
   {
       public string visit { get; set; }
       public string Booking { get; set; }
       public string FailureCancel { get; set; }
       public string FailureTimeout { get; set; }
       public string Sensitive { get; set; }
   }
    #endregion
   public abstract class ViewstatisticsBase
    {
      
        #region Porperties
        public string HotelId { get; set; }
        public Int64 Views { get; set; }
        public Int64 Visitor { get; set; }
        public Int64 Booking { get; set; }
        public Int64 Request { get; set; }
        public Int64 FailureCancel { get; set; }
        public Int64 FailureTimeout { get; set; }
        public Int64 DeclineByOperator { get; set; }
        public Int64 DeclineByHotel { get; set; }
        public Int64 DeclineByHotelTentative { get; set; }
        public Int64 NoActionTaken { get; set; }
        public Int64 SensitiveBooking { get; set; }
        public Int64 SensitiveRequest { get; set; }
        public Int64 BookingId { get; set; }


        #endregion
        #region Insert Vistit
        public void insertVisits( Statistics ob)
        
        {
            TransactionManager tm = null;
           
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                 
                
               
                if (DataRepository.StatisticsProvider.Insert(ob))
                {
                    tm.Commit();
                }
            }
            catch (Exception ex)
            {
                tm.Rollback();
            
            }
                

        }
#endregion
        #region Insert Vistit with BookingID
        public void InsertVisitwithBookingid(Statistics ob)
        {
            TransactionManager tm = null;

            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                if (DataRepository.StatisticsProvider.Insert(ob))
                {
                    tm.Commit();
                }
            }
            catch (Exception ex)
            {
                tm.Rollback();

            }
                
        
        }
        #endregion
        #region Get by Date
        /// <summary>
        /// Get by date
        /// </summary>
        /// <param name="Fromdate"> start date</param>
        /// <param name="toDate">end date</param>
        /// <param name="type">type -booking/request</param>
        /// <returns></returns>
        public List<Fetchdata> getbydate(DateTime Fromdate, DateTime toDate, string type)
        {
            List<Fetchdata> objfetch = new List<Fetchdata>();
            TimeSpan diff = toDate.Subtract(Fromdate);
           
            for (int cnt = 0; cnt <= diff.Days; cnt++)
            {
                Fetchdata objdata = new Fetchdata();
              objdata.currentdate = Fromdate.ToString("dd/MM/yy");
              int[] count = getstatisticscount(Fromdate,Fromdate);

              objdata.statisticvalue = count[0];
              objdata.confirmvalue = count[1];

                //calculating the conversion
                  if (count[0] != 0)
                      objdata.conversion = Math.Round((Convert.ToDecimal( Convert.ToDecimal( count[1]) / Convert.ToDecimal( count[0])) * 100), 2);
                  else
                      objdata.conversion = 0;


                
                  if (type == "2") // for booking
                  {
                      objdata.Booking = count[1];
                   //   objdata.Sensitive = count[3];
                      objdata.FailureTimeout = count[5];
                      objdata.FailureCancel = count[6];

                    
                  }
                  if (type == "3") // for request
                  {
                      objdata.Booking = count[2];
                      objdata.Sensitive= count[4];
                      objdata.FailureTimeout = count[7];
                      objdata.FailureCancel = count[8];
                  }

              objfetch.Add(objdata);
              Fromdate=  Fromdate.AddDays(1);
            }
            return objfetch;
        }
        #endregion
        #region get by week
        /// <summary>
        /// Get by week
        /// </summary>
        /// <param name="Fromdate"> start date</param>
        /// <param name="toDate">end date</param>
        /// <param name="type">type -booking/request</param>
        /// <returns></returns>
        public List<Fetchdata> getbyweek(DateTime Fromdate, DateTime toDate, string type)
        {
            List<Fetchdata> objfetch = new List<Fetchdata>();
           
            TimeSpan diff = toDate.Subtract(Fromdate);
            int week = Convert.ToInt32( diff.Days / 7);

            for (int cnt = 0; cnt <= week; cnt++)
            {
               
                Fetchdata objdata = new Fetchdata();
 
                int[] count = getstatisticscount(Fromdate, Fromdate.AddDays(6));

                objdata.currentdate = Fromdate.ToString("dd/MM/yy") + " - " + Fromdate.AddDays(6).ToString("dd/MM/yy");
                objdata.statisticvalue = count[0];
                objdata.confirmvalue = count[1];
                if (count[0] != 0)
                    objdata.conversion = Math.Round((Convert.ToDecimal(Convert.ToDecimal(count[1]) / Convert.ToDecimal(count[0])) * 100), 2);
                else
                    objdata.conversion = 0;

                if (type == "2") // for booking
                {
                    objdata.Booking = count[1];
                    //   objdata.Sensitive = count[3];
                    objdata.FailureTimeout = count[5];
                    objdata.FailureCancel = count[6];


                }
                if (type == "3") // for request
                {
                    objdata.Booking = count[2];
                    objdata.Sensitive = count[4];
                    objdata.FailureTimeout = count[7];
                    objdata.FailureCancel = count[8];
                }
                objfetch.Add(objdata);
                Fromdate = Fromdate.AddDays(7);
            }
            return objfetch;
        }
        #endregion
        #region Get by Month
        /// <summary>
        /// Get by Month
        /// </summary>
        /// <param name="Fromdate"> start date</param>
        /// <param name="toDate">end date</param>
        /// <param name="type">type -booking/request</param>
        /// <returns></returns>
        public List<Fetchdata> getbyMonth(DateTime Fromdate, DateTime toDate ,string type)
        {
            List<Fetchdata> objfetch = new List<Fetchdata>();
            TimeSpan diff = toDate.Subtract(Fromdate);
            int fromYear = Fromdate.Year;
            int toyear = toDate.Year;
            int Frommonth = Fromdate.Month;
            int Tomonth = toDate.Month;

            if (fromYear != toyear)
            {

                for (int i = Frommonth; i <= 12; i++)
                {

                    #region fetch
                    Fetchdata objdata = new Fetchdata();
                    int[] count = getstatisticscountMonth(Fromdate, Fromdate.AddMonths(1));
                    objdata.currentdate = Fromdate.ToString("MMM yyyy");
                    objdata.statisticvalue = count[0];
                    objdata.confirmvalue = count[1];
                    if (count[0] != 0)
                        objdata.conversion = Math.Round((Convert.ToDecimal(Convert.ToDecimal(count[1]) / Convert.ToDecimal(count[0])) * 100), 2);
                    else
                        objdata.conversion = 0;

                    if (type == "2") // for booking
                    {
                        objdata.Booking = count[1];
                        //   objdata.Sensitive = count[3];
                        objdata.FailureTimeout = count[5];
                        objdata.FailureCancel = count[6];


                    }
                    if (type == "3") // for request
                    {
                        objdata.Booking = count[2];

                        objdata.Sensitive = count[4];
                        objdata.FailureTimeout = count[7];
                        objdata.FailureCancel = count[8];
                    }


                    objfetch.Add(objdata);
                    Fromdate = Fromdate.AddMonths(1);

                    
                    #endregion
                }
                for (int i = 1; i <= Tomonth; i++)
                {
                    #region fetch
                    Fetchdata objdata = new Fetchdata();
                    int[] count = getstatisticscountMonth(Fromdate, Fromdate.AddMonths(1));
                    objdata.currentdate = Fromdate.ToString("MMM yyyy");
                    objdata.statisticvalue = count[0];
                    objdata.confirmvalue = count[1];
                    if (count[0] != 0)
                        objdata.conversion = Math.Round((Convert.ToDecimal(Convert.ToDecimal(count[1]) / Convert.ToDecimal(count[0])) * 100), 2);
                    else
                        objdata.conversion = 0;

                    if (type == "2") // for booking
                    {
                        objdata.Booking = count[1];
                        //   objdata.Sensitive = count[3];
                        objdata.FailureTimeout = count[5];
                        objdata.FailureCancel = count[6];


                    }
                    if (type == "3") // for request
                    {
                        objdata.Booking = count[2];

                        objdata.Sensitive = count[4];
                        objdata.FailureTimeout = count[7];
                        objdata.FailureCancel = count[8];
                    }


                    objfetch.Add(objdata);
                    Fromdate = Fromdate.AddMonths(1);

                   
                    #endregion
                }

            }
            else
            {
                for (int i = Frommonth; i <= Tomonth; i++)
                {
                    #region fetch
                    Fetchdata objdata = new Fetchdata();
                    int[] count = getstatisticscountMonth(Fromdate, Fromdate.AddMonths(1));
                    objdata.currentdate = Fromdate.ToString("MMM yyyy");
                    objdata.statisticvalue = count[0];
                    objdata.confirmvalue = count[1];
                    if (count[0] != 0)
                        objdata.conversion = Math.Round((Convert.ToDecimal(Convert.ToDecimal(count[1]) / Convert.ToDecimal(count[0])) * 100), 2);
                    else
                        objdata.conversion = 0;

                    if (type == "2") // for booking
                    {
                        objdata.Booking = count[1];
                        //   objdata.Sensitive = count[3];
                        objdata.FailureTimeout = count[5];
                        objdata.FailureCancel = count[6];


                    }
                    if (type == "3") // for request
                    {
                        objdata.Booking = count[2];

                        objdata.Sensitive = count[4];
                        objdata.FailureTimeout = count[7];
                        objdata.FailureCancel = count[8];
                    }


                    objfetch.Add(objdata);
                    Fromdate = Fromdate.AddMonths(1);

                    //week++;
                    #endregion
                }

            }
            int week = Convert.ToInt32(diff.Days / 30);//Convert.ToInt32( toDate.Subtract(Fromdate).Days / (30.45));//
           
                for (int cnt = 0; cnt <= week; cnt++)
                {

                    
                }
                       return objfetch;
        }
        #endregion
        #region Get by Year
        /// <summary>
        /// Get by Year
        /// </summary>
        /// <param name="Fromdate"> start date</param>
        /// <param name="toDate">end date</param>
        /// <param name="type">type -booking/request</param>
        /// <returns></returns>
        public List<Fetchdata> getbyYear(DateTime Fromdate, DateTime toDate, string type)
        {
            List<Fetchdata> objfetch = new List<Fetchdata>();
            TimeSpan diff = toDate.Subtract(Fromdate);
            int week = Convert.ToInt32(diff.Days / 365);
            for (int cnt = Fromdate.Year ;cnt <= toDate.Year; cnt++)
            {

                Fetchdata objdata = new Fetchdata();

                int[] count = getstatisticscount(Fromdate, Fromdate.AddYears(1));

                objdata.currentdate = Fromdate.ToString("yyyy") + " - " + Fromdate.AddYears(1).ToString("yyyy");
                objdata.statisticvalue = count[0];
                objdata.confirmvalue = count[1];
                if (count[0] != 0)
                    objdata.conversion = Math.Round((Convert.ToDecimal(Convert.ToDecimal(count[1]) / Convert.ToDecimal(count[0])) * 100), 2);
                else
                    objdata.conversion = 0;



                if (type == "2") // for booking
                {
                    objdata.Booking = count[1];
                    //   objdata.Sensitive = count[3];
                    objdata.FailureTimeout = count[5];
                    objdata.FailureCancel = count[6];


                }
                if (type == "3") // for request
                {
                    objdata.Booking = count[2];
                    objdata.Sensitive = count[4];
                    objdata.FailureTimeout = count[7];
                    objdata.FailureCancel = count[8];
                }

                objfetch.Add(objdata);
                Fromdate = Fromdate.AddYears(1);
            }
            return objfetch;
        }
        #endregion
        #region Get by Year
        /// <summary>
        /// Get by Year
        /// </summary>
        /// <param name="Fromdate"> start date</param>
        /// <param name="toDate">end date</param>
        /// <param name="type">type -booking/request</param>
        /// <returns></returns>
        public List<Fetchdata> getbyYearSum(DateTime Fromdate, DateTime toDate, string type)
        {
            List<Fetchdata> objfetch = new List<Fetchdata>();
            TimeSpan diff = toDate.Subtract(Fromdate);
            int week = Convert.ToInt32(diff.Days / 365);
            

                Fetchdata objdata = new Fetchdata();

                int[] count = getstatisticscount(Fromdate, toDate);

                objdata.currentdate = Fromdate.ToString("yyyy") + " - " + Fromdate.AddYears(1).ToString("yyyy");
                objdata.statisticvalue = count[0];
                objdata.confirmvalue = count[1];
                if (count[0] != 0)
                    objdata.conversion = Math.Round((Convert.ToDecimal(Convert.ToDecimal(count[1]) / Convert.ToDecimal(count[0])) * 100), 2);
                else
                    objdata.conversion = 0;



                if (type == "2") // for booking
                {
                    objdata.Booking = count[1];
                    //   objdata.Sensitive = count[3];
                    objdata.FailureTimeout = count[5];
                    objdata.FailureCancel = count[6];


                }
                if (type == "3") // for request
                {
                    objdata.Booking = count[2];
                    objdata.Sensitive = count[4];
                    objdata.FailureTimeout = count[7];
                    objdata.FailureCancel = count[8];
                }

                objfetch.Add(objdata);
                Fromdate = Fromdate.AddYears(1);
            
            return objfetch;
        }
        #endregion
        #region Get Count
        /// <summary>
        /// Get count of the particular duration
        /// </summary>
        /// <param name="fromdate"></param>
        /// <param name="enddt"></param>
        /// <returns></returns>
        public int[] getstatisticscount(DateTime fromdate , DateTime enddt)
        {
           
            string strfromdate = Convert.ToString(fromdate);
            int[] intCount = new int[9];
            int Total = 0;
            DateTime startdate = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day);
            DateTime enddate = new DateTime(enddt.Year, enddt.Month, enddt.Day);
            //string whereclause = "HotelId=" + HotelId + " and statdate ="+fromdate.ToString();
            string whereclause = "HotelId  in (" + HotelId + ") and statdate  between '" + startdate + "' and '" + enddate.AddDays(1).AddSeconds(-1) + "'";
            TList<Statistics> tlistcnt = DataRepository.StatisticsProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
            //-- count visitors
            Int64? countView = tlistcnt.Sum(a => a.Views);
            if( string.IsNullOrEmpty( Convert.ToString( countView.Value)))
                intCount[0]=0;
            else
                intCount[0]= Convert.ToInt32(countView);

            //---- count conversion Booking
            Int64? countConversion  = tlistcnt.Sum(a => a.Booking);
            if (string.IsNullOrEmpty(Convert.ToString(countConversion.Value)))
                intCount[1] = 0;
            else
                intCount[1] = Convert.ToInt32(countConversion);

            //---- count conversion Request
            Int64? countConversionReq = tlistcnt.Sum(a => a.Request);
            if (string.IsNullOrEmpty(Convert.ToString(countConversionReq.Value)))
                intCount[2] = 0;
            else
                intCount[2] = Convert.ToInt32(countConversionReq);


            //---- count sensitive Booking
            Int64? countSenBooking = tlistcnt.Sum(a => a.SensitiveBooking);
            if (string.IsNullOrEmpty(Convert.ToString(countSenBooking.Value)))
                intCount[3] = 0;
            else
                intCount[3] = Convert.ToInt32(countSenBooking);

            //---- count sensitive Request
            Int64? countSenReq = tlistcnt.Sum(a => a.SensitiveRequest);
            if (string.IsNullOrEmpty(Convert.ToString(countSenReq.Value)))
                intCount[4] = 0;
            else
                intCount[4] = Convert.ToInt32(countSenReq);


            //---- find booking type
            TList<Statistics> tlistType = tlistcnt.FindAll(a => Convert.ToInt32(a.BookingType) == 0);

            //---- count Failure timeout -Booking
            Int64? countFTBooking = tlistType.Sum(a => a.FailureTimeout);
            if (string.IsNullOrEmpty(Convert.ToString(countFTBooking.Value)))
                intCount[5] = 0;
            else
                intCount[5] = Convert.ToInt32(countFTBooking);

            //---- count Failure Cancel -Booking
            Int64? countFCBooking = tlistType.Sum(a => a.FailureCancel);
            if (string.IsNullOrEmpty(Convert.ToString(countFCBooking.Value)))
                intCount[6] = 0;
            else
                intCount[6] = Convert.ToInt32(countFCBooking);

            //---- find request type
            TList<Statistics> tlistTypeR = tlistcnt.FindAll(a => Convert.ToInt32(a.BookingType) == 1);

            //---- count Failure timeout -Request
            Int64? countFTRequest = tlistTypeR.Sum(a => a.FailureTimeout);
            if (string.IsNullOrEmpty(Convert.ToString(countFTRequest.Value)))
                intCount[7] = 0;
            else
                intCount[7] = Convert.ToInt32(countFTRequest);

            //---- count Failure Cancel -Request
            Int64? countFCRequest = tlistTypeR.Sum(a => a.FailureCancel);
            if (string.IsNullOrEmpty(Convert.ToString(countFCRequest.Value)))
                intCount[8] = 0;
            else
                intCount[8] = Convert.ToInt32(countFCRequest);



          
            return intCount;

        
        }
        public int[] getstatisticscountMonth(DateTime fromdate, DateTime enddt)
        {

            string strfromdate = Convert.ToString(fromdate);
            int[] intCount = new int[9];
            int Total = 0;
            DateTime startdate = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day);
            DateTime enddate = new DateTime(enddt.Year, enddt.Month, enddt.Day);
            //string whereclause = "HotelId=" + HotelId + " and statdate ="+fromdate.ToString();
            string whereclause = "HotelId  in (" + HotelId + ") and month(statdate)= '" + startdate.Month + "' and  year(statdate)='" + startdate.Year + "'";
            TList<Statistics> tlistcnt = DataRepository.StatisticsProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
            //-- count visitors
            Int64? countView = tlistcnt.Sum(a => a.Views);
            if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                intCount[0] = 0;
            else
                intCount[0] = Convert.ToInt32(countView);

            //---- count conversion Booking
            Int64? countConversion = tlistcnt.Sum(a => a.Booking);
            if (string.IsNullOrEmpty(Convert.ToString(countConversion.Value)))
                intCount[1] = 0;
            else
                intCount[1] = Convert.ToInt32(countConversion);

            //---- count conversion Request
            Int64? countConversionReq = tlistcnt.Sum(a => a.Request);
            if (string.IsNullOrEmpty(Convert.ToString(countConversionReq.Value)))
                intCount[2] = 0;
            else
                intCount[2] = Convert.ToInt32(countConversionReq);


            //---- count sensitive Booking
            Int64? countSenBooking = tlistcnt.Sum(a => a.SensitiveBooking);
            if (string.IsNullOrEmpty(Convert.ToString(countSenBooking.Value)))
                intCount[3] = 0;
            else
                intCount[3] = Convert.ToInt32(countSenBooking);

            //---- count sensitive Request
            Int64? countSenReq = tlistcnt.Sum(a => a.SensitiveRequest);
            if (string.IsNullOrEmpty(Convert.ToString(countSenReq.Value)))
                intCount[4] = 0;
            else
                intCount[4] = Convert.ToInt32(countSenReq);


            //---- find booking type
            TList<Statistics> tlistType = tlistcnt.FindAll(a => Convert.ToInt32(a.BookingType) == 0);

            //---- count Failure timeout -Booking
            Int64? countFTBooking = tlistType.Sum(a => a.FailureTimeout);
            if (string.IsNullOrEmpty(Convert.ToString(countFTBooking.Value)))
                intCount[5] = 0;
            else
                intCount[5] = Convert.ToInt32(countFTBooking);

            //---- count Failure Cancel -Booking
            Int64? countFCBooking = tlistType.Sum(a => a.FailureCancel);
            if (string.IsNullOrEmpty(Convert.ToString(countFCBooking.Value)))
                intCount[6] = 0;
            else
                intCount[6] = Convert.ToInt32(countFCBooking);

            //---- find request type
            TList<Statistics> tlistTypeR = tlistcnt.FindAll(a => Convert.ToInt32(a.BookingType) == 1);

            //---- count Failure timeout -Request
            Int64? countFTRequest = tlistTypeR.Sum(a => a.FailureTimeout);
            if (string.IsNullOrEmpty(Convert.ToString(countFTRequest.Value)))
                intCount[7] = 0;
            else
                intCount[7] = Convert.ToInt32(countFTRequest);

            //---- count Failure Cancel -Request
            Int64? countFCRequest = tlistTypeR.Sum(a => a.FailureCancel);
            if (string.IsNullOrEmpty(Convert.ToString(countFCRequest.Value)))
                intCount[8] = 0;
            else
                intCount[8] = Convert.ToInt32(countFCRequest);




            return intCount;


        }
        #endregion
        #region Get Label
        /// <summary>
        /// get label for statistics-Booking/request
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<Fetchlabel> getlabel(string type)
        {
            List<Fetchlabel> objfetchlbl = new List<Fetchlabel>();
            Fetchlabel objfetchlabel = new Fetchlabel();
            if (type == "2") //-- for booking
            {
                
                objfetchlabel.Booking = "Total Booked";
              //  objfetchlabel.Sensitive = "Sensitive Booking";
                objfetchlabel.FailureTimeout = "Failure Timeout";
                objfetchlabel.FailureCancel = "Failure Cancel";
                
            }
            else
            {                   //--- request
                
                objfetchlabel.Booking = "Requests Confirmed";
                objfetchlabel.Sensitive = "Total Requests";
                objfetchlabel.FailureTimeout = "Failure Timeout";
                objfetchlabel.FailureCancel = "Failure Cancel";
               

            }
            objfetchlbl.Add(objfetchlabel);

            return objfetchlbl;

        }
        #endregion
        #region Get by Tentative
        /// <summary>
        /// Get by Year
        /// </summary>
        /// <param name="Fromdate"> start date</param>
        /// <param name="toDate">end date</param>
        /// <param name="type">type -booking/request</param>
        /// <returns></returns>
        public List<Fetchdata> Getreq(DateTime Fromdate, DateTime toDate, string type)
        {
            List<Fetchdata> objfetch = new List<Fetchdata>();
            TimeSpan diff = toDate.Subtract(Fromdate);

            for (int cnt = 0; cnt <= diff.Days; cnt++)
            {
                Fetchdata objdata = new Fetchdata();
                objdata.currentdate = Fromdate.ToString("dd/MM/yy");
                int[] count = getstatisticscountreq(Fromdate, Fromdate, type);

                objdata.Booking = count[0];
                
                objfetch.Add(objdata);
                Fromdate = Fromdate.AddDays(1);
            }
            return objfetch;
        }
        #endregion
        #region Get Count Request
        /// <summary>
        /// Get count of the particular duration
        /// </summary>
        /// <param name="fromdate"></param>
        /// <param name="enddt"></param>
        /// <returns></returns>
        public int[] getstatisticscountreq(DateTime fromdate, DateTime enddt, string type)
        {

            string strfromdate = Convert.ToString(fromdate);
            int[] intCount = new int[9];
            int Total = 0;
            DateTime startdate = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day);
            DateTime enddate = new DateTime(enddt.Year, enddt.Month, enddt.Day);
            //string whereclause = " Booktype in (1,2) and  Bookingdate  between '" + startdate + "' and '" + enddate.AddDays(1).AddSeconds(-1) + "'";
            string whereclause = " Booktype in (1,2) and  Bookingdate  between '" + startdate + "' and '" + enddate.AddDays(1).AddSeconds(-1) + "'";
            TList<Booking> tlistcnt = new TList<Entities.Booking>();// DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
            //-- count Ten req
            if (type == "1")
            {
                whereclause = " HotelId  in (" + HotelId + ")  and Booktype in (1,2) and RequestStatus=5 and  Bookingdate  between '" + startdate + "' and '" + enddate.AddDays(1).AddSeconds(-1) + "'";
             tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
             Int64? countView = tlistcnt.Count;// tlistcnt.Sum(a => a.RequestStatus = (int)BookingRequestStatus.Tentative);
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[0] = 0;
                else
                    intCount[0] = Convert.ToInt32(countView);
            }

            //---- count New request
            if (type == "2")
            {
                whereclause = " HotelId  in (" + HotelId + ")  and Booktype in (1,2) and RequestStatus=1 and  Bookingdate  between '" + startdate + "' and '" + enddate.AddDays(1).AddSeconds(-1) + "'";
                tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
                Int64? countView = tlistcnt.Count;// tlistcnt.Sum(a => a.RequestStatus = (int)BookingRequestStatus.New);
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[0] = 0;
                else
                    intCount[0] = Convert.ToInt32(countView);
            }
          

            //---- count Online request
            if (type == "3")
            {
                whereclause = "HotelId  in (" + HotelId + ") and  Booktype in (1,2) and  Bookingdate  between '" + startdate + "' and '" + enddate.AddDays(1).AddSeconds(-1) + "'";
                tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
                Int64? countView = tlistcnt.Count;
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[0] = 0;
                else
                    intCount[0] = Convert.ToInt32(countView);
            }


            if (type == "4")
            {

                string whereclause1 = "HotelId  in (" + HotelId + ") and Booktype=0 and  Bookingdate  between '" + startdate + "' and '" + enddate.AddDays(1).AddSeconds(-1) + "'";
                TList<Booking> tlistcnt1 = DataRepository.BookingProvider.GetPaged(whereclause1, string.Empty, 0, int.MaxValue, out Total);
                Int64? countView = Convert.ToInt64( tlistcnt1.Sum(a=>a.RevenueAmount));
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[0] = 0;
                else
                    intCount[0] = Convert.ToInt32(countView);
           
            }
            return intCount;


        }
        #endregion
        #region Get by Tentative
        /// <summary>
        /// Get by Year
        /// </summary>
        /// <param name="Fromdate"> start date</param>
        /// <param name="toDate">end date</param>
        /// <param name="type">type -booking/request</param>
        /// <returns></returns>
        public List<Fetchdata> GetUncheckComm(DateTime Fromdate, DateTime toDate, string type)
        {
            List<Fetchdata> objfetch = new List<Fetchdata>();
            TimeSpan diff = toDate.Subtract(Fromdate);

            for (int cnt = 0; cnt <= diff.Days; cnt++)
            {
                Fetchdata objdata = new Fetchdata();
                objdata.currentdate = Fromdate.ToString("dd/MM/yy");
                int[] count = getstatisticscountUncomm(Fromdate, Fromdate, type);

                objdata.Booking = count[0];

                objfetch.Add(objdata);
                Fromdate = Fromdate.AddDays(1);
            }
            return objfetch;
        }
        #endregion
        #region Get Count Request
        /// <summary>
        /// Get count of the particular duration
        /// </summary>
        /// <param name="fromdate"></param>
        /// <param name="enddt"></param>
        /// <returns></returns>
        public int[] getstatisticscountUncomm(DateTime fromdate, DateTime enddt, string type)
        {

            string strfromdate = Convert.ToString(fromdate);
            int[] intCount = new int[9];
            int Total = 0;
            DateTime startdate = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day);
            DateTime enddate = new DateTime(enddt.Year, enddt.Month, enddt.Day);
            //string whereclause = "HotelId=" + HotelId + " and statdate ="+fromdate.ToString();

            string whereclause = string.Empty;
            if (type == "1")
            {
                whereclause = "hotelid in (" + HotelId + ") and Booktype=0 and   ( IsComissionDone=0 or  IsComissionDone is null)  and InvoiceId= null and   Bookingdate  between '" + startdate + "' and '" + enddate.AddDays(1).AddSeconds(-1) + "'";
            }
            if (type == "2")
            {
                whereclause = "hotelid in (" + HotelId + ") and Booktype in (1,2) and  ( IsComissionDone=0 or  IsComissionDone is null) and requeststatus in (7)  and  Bookingdate  between '" + startdate + "' and '" + enddate.AddDays(1).AddSeconds(-1) + "'";
            }

            TList<Booking> tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);


            Int64? countView = tlistcnt.Count();
            if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                intCount[0] = 0;
            else
                intCount[0] = Convert.ToInt32(countView);
          

            return intCount;


        }
        #endregion

       /// for report
       /// 

        #region Get for report
        /// <summary>
        /// Get by date
        /// </summary>
        /// <param name="Fromdate"> start date</param>
        /// <param name="toDate">end date</param>
        /// <param name="type">type -booking/request</param>
        /// <returns></returns>
        public decimal[] getreport(int month, string hotel)
        {

            decimal[] arrcnt = new decimal[3];
               // Fetchdata objdata = new Fetchdata();
                int Total;
                string whereclause = " MONTH(StatDate)="+month+" and HotelId='"+hotel +"'";
                TList<Statistics> tlistcnt = DataRepository.StatisticsProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);

            Int64? countView = tlistcnt.Sum(a => a.Views);
            if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
               arrcnt[0] =   0;
            else

                arrcnt[0] = Convert.ToInt32(countView);

            //---- count conversion Booking
            Int64? countConversion = tlistcnt.Sum(a => a.Booking);
            if (string.IsNullOrEmpty(Convert.ToString(countConversion.Value)))
            {
                arrcnt[1] = 0;
                arrcnt[2] = 0;
            }
            else
            {
                arrcnt[2] = Convert.ToDecimal(countConversion);
                if (arrcnt[0] != 0)
                    arrcnt[1] = (Convert.ToDecimal(countConversion) / Convert.ToDecimal(countView)) * 100;
                else
                    arrcnt[1] = 0;
            }
                return arrcnt;
        }
        #endregion
       //--------- statistics


        #region Get statistics
        /// <summary>
        /// Get by day
        /// </summary>
        /// <param name="Fromdate"> start date</param>
        /// <param name="toDate">end date</param>
        /// <param name="type">type -booking/request</param>
        /// <returns></returns>
        public List<Fetchdata> Getagencyuser(DateTime Fromdate, DateTime toDate, string type, int user)
        {
            List<Fetchdata> objfetch = new List<Fetchdata>();
            if (type == "1")
            {
            
            TimeSpan diff = toDate.Subtract(Fromdate);
            
            // Agency user -> link with agency using partentid
            // select * from booking where creatorid=@1 / select * from booking where creatorid in (@2)
           
                for (int cnt = 0; cnt <= diff.Days; cnt++)
                {
                    Fetchdata objdata = new Fetchdata();
                    objdata.currentdate = Fromdate.ToString("dd/MM/yy");
                    int[] count = getstatisticscountagencyuser(Fromdate, Fromdate, type, user);

                    //-----  for booking
                    objdata.Booking = count[6]; // no of booking of agent user
                    //calculating the conversion /// no of booking for that agency
                    if (count[5] != 0)
                        objdata.ConversionBooking = Math.Round((Convert.ToDecimal(Convert.ToDecimal(count[6]) / Convert.ToDecimal(count[5])) * 100), 2);
                    else
                        objdata.ConversionBooking = 0;
                    objdata.statisticvalue = count[5];
                    objdata.conversion = count[4];



                    //--- for request 
                    objdata.Sensitive = count[0]; // no of req of the agent
                    //calculating the conversion // no of req  of agency
                    if (count[1] != 0)
                        objdata.ConversionReq = Math.Round((Convert.ToDecimal(Convert.ToDecimal(count[0]) / Convert.ToDecimal(count[1])) * 100), 2);
                    else
                        objdata.ConversionReq = 0;
                    objdata.FailureTimeout = count[1];

                    //--- for confimred request 
                    objdata.confirmvalue = count[2]; // no of confirmed req of the agent
                    //calculating the conversion// no of confirmed req  of agency
                    if (count[3] != 0)
                        objdata.ConversionConfirmReq = Math.Round((Convert.ToDecimal(Convert.ToDecimal(count[2]) / Convert.ToDecimal(count[3])) * 100), 2);
                    else
                        objdata.ConversionConfirmReq = 0;
                    objdata.FailureCancel = count[3];




                    objfetch.Add(objdata);
                    Fromdate = Fromdate.AddDays(1);
                }
            }
            if (type == "2")
            {
                int fromYear = Fromdate.Year;
                int toyear = toDate.Year;
                int Frommonth = Fromdate.Month;
                int Tomonth = toDate.Month;

                if (fromYear != toyear)
                {

                    for (int i = Frommonth; i <= 12; i++)
                    {

                        #region fetch
                        Fetchdata objdata = new Fetchdata();
                        objdata.currentdate = Fromdate.ToString("MMM yyyy");
                        int[] count = getstatisticscountagencyuser(Fromdate, Fromdate, type, user);

                        //-----  for booking
                        objdata.Booking = count[6]; // no of booking of agent user
                        //calculating the conversion /// no of booking for that agency
                        if (count[5] != 0)
                            objdata.ConversionBooking = Math.Round((Convert.ToDecimal(Convert.ToDecimal(count[6]) / Convert.ToDecimal(count[5])) * 100), 2);
                        else
                            objdata.ConversionBooking = 0;
                        objdata.statisticvalue = count[5];
                        objdata.conversion = count[4];


                        //--- for request 
                        objdata.Sensitive = count[0]; // no of req of the agent
                        //calculating the conversion // no of req  of agency
                        if (count[1] != 0)
                            objdata.ConversionReq = Math.Round((Convert.ToDecimal(Convert.ToDecimal(count[0]) / Convert.ToDecimal(count[1])) * 100), 2);
                        else
                            objdata.ConversionReq = 0;
                        objdata.FailureTimeout = count[1];

                        //--- for confimred request 
                        objdata.confirmvalue = count[2]; // no of confirmed req of the agent
                        //calculating the conversion// no of confirmed req  of agency
                        if (count[3] != 0)
                            objdata.ConversionConfirmReq = Math.Round((Convert.ToDecimal(Convert.ToDecimal(count[2]) / Convert.ToDecimal(count[3])) * 100), 2);
                        else
                            objdata.ConversionConfirmReq = 0;
                        objdata.FailureCancel = count[3];




                        objfetch.Add(objdata);
                        Fromdate = Fromdate.AddMonths(1);
                        #endregion
                    }
                    for (int i = 1; i <= Tomonth; i++)
                    {
                        #region fetch
                        Fetchdata objdata = new Fetchdata();
                        objdata.currentdate = Fromdate.ToString("MMM yyyy");
                        int[] count = getstatisticscountagencyuser(Fromdate, Fromdate, type, user);

                        //-----  for booking
                        objdata.Booking = count[6]; // no of booking of agent user
                        //calculating the conversion /// no of booking for that agency
                        if (count[5] != 0)
                            objdata.ConversionBooking = Math.Round((Convert.ToDecimal(Convert.ToDecimal(count[6]) / Convert.ToDecimal(count[5])) * 100), 2);
                        else
                            objdata.ConversionBooking = 0;
                        objdata.statisticvalue = count[5];
                        objdata.conversion = count[4];



                        //--- for request 
                        objdata.Sensitive = count[0]; // no of req of the agent
                        //calculating the conversion // no of req  of agency
                        if (count[1] != 0)
                            objdata.ConversionReq = Math.Round((Convert.ToDecimal(Convert.ToDecimal(count[0]) / Convert.ToDecimal(count[1])) * 100), 2);
                        else
                            objdata.ConversionReq = 0;
                        objdata.FailureTimeout = count[1];

                        //--- for confimred request 
                        objdata.confirmvalue = count[2]; // no of confirmed req of the agent
                        //calculating the conversion// no of confirmed req  of agency
                        if (count[3] != 0)
                            objdata.ConversionConfirmReq = Math.Round((Convert.ToDecimal(Convert.ToDecimal(count[2]) / Convert.ToDecimal(count[3])) * 100), 2);
                        else
                            objdata.ConversionConfirmReq = 0;
                        objdata.FailureCancel = count[3];




                        objfetch.Add(objdata);
                        Fromdate = Fromdate.AddMonths(1);
                        #endregion
                    }

                }
                else
                {
                    for (int i = Frommonth; i <= Tomonth; i++)
                    {
                        #region fetch
                        Fetchdata objdata = new Fetchdata();
                        objdata.currentdate = Fromdate.ToString("MMM yyyy");
                        int[] count = getstatisticscountagencyuser(Fromdate, Fromdate, type, user);

                        //-----  for booking
                        objdata.Booking = count[6]; // no of booking of agent user
                        //calculating the conversion /// no of booking for that agency
                        if (count[5] != 0)
                            objdata.ConversionBooking = Math.Round((Convert.ToDecimal(Convert.ToDecimal(count[6]) / Convert.ToDecimal(count[5])) * 100), 2);
                        else
                            objdata.ConversionBooking = 0;
                        objdata.statisticvalue = count[5];
                        objdata.conversion = count[4];



                        //--- for request 
                        objdata.Sensitive = count[0]; // no of req of the agent
                        //calculating the conversion // no of req  of agency
                        if (count[1] != 0)
                            objdata.ConversionReq = Math.Round((Convert.ToDecimal(Convert.ToDecimal(count[0]) / Convert.ToDecimal(count[1])) * 100), 2);
                        else
                            objdata.ConversionReq = 0;
                        objdata.FailureTimeout = count[1];

                        //--- for confimred request 
                        objdata.confirmvalue = count[2]; // no of confirmed req of the agent
                        //calculating the conversion// no of confirmed req  of agency
                        if (count[3] != 0)
                            objdata.ConversionConfirmReq = Math.Round((Convert.ToDecimal(Convert.ToDecimal(count[2]) / Convert.ToDecimal(count[3])) * 100), 2);
                        else
                            objdata.ConversionConfirmReq = 0;
                        objdata.FailureCancel = count[3];




                        objfetch.Add(objdata);
                        Fromdate = Fromdate.AddMonths(1);
                        #endregion
                    }
                
                }
            }
            if (type == "3")
            {
                int fromYear = Fromdate.Year;
                int toyear = toDate.Year;

                for (int cnt = fromYear; cnt <= toyear; cnt++)
                {

                    #region fetch
                    Fetchdata objdata = new Fetchdata();
                    objdata.currentdate = Fromdate.ToString("MMM yyyy");
                    int[] count = getstatisticscountagencyuser(Fromdate, Fromdate, type, user);

                    //-----  for booking
                    objdata.Booking = count[6]; // no of booking of agent user
                    //calculating the conversion /// no of booking for that agency
                    if (count[5] != 0)
                        objdata.ConversionBooking = Math.Round((Convert.ToDecimal(Convert.ToDecimal(count[6]) / Convert.ToDecimal(count[5])) * 100), 2);
                    else
                        objdata.ConversionBooking = 0;
                    objdata.statisticvalue = count[5];
                    objdata.conversion = count[4];



                    //--- for request 
                    objdata.Sensitive = count[0]; // no of req of the agent
                    //calculating the conversion // no of req  of agency
                    if (count[1] != 0)
                        objdata.ConversionReq = Math.Round((Convert.ToDecimal(Convert.ToDecimal(count[0]) / Convert.ToDecimal(count[1])) * 100), 2);
                    else
                        objdata.ConversionReq = 0;
                    objdata.FailureTimeout = count[1];

                    //--- for confimred request 
                    objdata.confirmvalue = count[2]; // no of confirmed req of the agent
                    //calculating the conversion// no of confirmed req  of agency
                    if (count[3] != 0)
                        objdata.ConversionConfirmReq = Math.Round((Convert.ToDecimal(Convert.ToDecimal(count[2]) / Convert.ToDecimal(count[3])) * 100), 2);
                    else
                        objdata.ConversionConfirmReq = 0;
                    objdata.FailureCancel = count[3];




                    objfetch.Add(objdata);
                    Fromdate = Fromdate.AddYears(1);
                    #endregion
                
                }


            }



            return objfetch;
        }
        #endregion


        #region Get Count Request
        /// <summary>
        /// Get count of the particular duration
        /// </summary>
        /// <param name="fromdate"></param>
        /// <param name="enddt"></param>
        /// <returns></returns>
        public int[] getstatisticscountagencyuser(DateTime fromdate, DateTime enddt, string type, int userid)
        {

            string strfromdate = Convert.ToString(fromdate);
            int[] intCount = new int[9];
            int Total = 0;
            DateTime startdate = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day);
            DateTime enddate = new DateTime(enddt.Year, enddt.Month, enddt.Day);
           // string whereclause = "HotelId=" + HotelId + " and statdate ="+fromdate.ToString();
           string whereclause = " Booktype in (1,2) and CreatorId='"+ userid+"' and  Bookingdate  between '" + startdate + "' and '" + enddate.AddDays(1).AddSeconds(-1) + "'";
            TList<Booking> tlistcnt = new TList<Entities.Booking>();// DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
            //-- count Ten req
            if (type == "1")
            {
                //-- FOR request for the particlar agent user
                whereclause = " Booktype in (1,2) and CreatorId='" + userid + "' and  Bookingdate  between '" + startdate + "' and '" + enddate.AddDays(1).AddSeconds(-1) + "'";
                tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
                Int64? countView = tlistcnt.Count;
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[0] = 0;
                else
                    intCount[0] = Convert.ToInt32(countView);

                //-- FOR request for all the agencies user of that particlar user

                string getcurrentagencyuser = GetAllAgencyUsers(userid);
                whereclause = " Booktype in (1,2) and CreatorId IN (" + getcurrentagencyuser + ") and  Bookingdate  between '" + startdate + "' and '" + enddate.AddDays(1).AddSeconds(-1) + "'";
                tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);

                countView = tlistcnt.Count;
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[1] = 0;
                else
                    intCount[1] = Convert.ToInt32(countView);

               

                //-- FOR  Conversion request for the particlar agent user
                whereclause = " Booktype in (1,2) and CreatorId='" + userid + "' and requeststatus=5 and  Bookingdate  between '" + startdate + "' and '" + enddate.AddDays(1).AddSeconds(-1) + "'";
                tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
                countView = tlistcnt.Count;
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[2] = 0;
                else
                    intCount[2] = Convert.ToInt32(countView);

                //-- FOR request for all the agencies user of that particlar user
                     whereclause = " Booktype=1 and CreatorId IN (" + getcurrentagencyuser + ") and requeststatus=5 and  Bookingdate  between '" + startdate + "' and '" + enddate.AddDays(1).AddSeconds(-1) + "'";
                tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);

                countView = tlistcnt.Count;
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[3] = 0;
                else
                    intCount[3] = Convert.ToInt32(countView);

                //-- FOR  Booking for the particlar agent user
                whereclause = " Booktype=0 and CreatorId='" + userid + "' and  Bookingdate  between '" + startdate + "' and '" + enddate.AddDays(1).AddSeconds(-1) + "'";
                tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
                countView = Convert.ToInt64(tlistcnt.Sum(a => a.RevenueAmount));
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[4] = 0;
                else
                    intCount[4] = Convert.ToInt32(countView);

                countView = tlistcnt.Count;
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[6] = 0;
                else
                    intCount[6] = Convert.ToInt32(countView);


                //-- FOR booking for all the agencies user of that particlar user
                whereclause = " Booktype=0 and CreatorId IN (" + getcurrentagencyuser + ") and  Bookingdate  between '" + startdate + "' and '" + enddate.AddDays(1).AddSeconds(-1) + "'";
                tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);

                countView = tlistcnt.Count;
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[5] = 0;
                else
                    intCount[5] = Convert.ToInt32(countView);



            }
            if (type == "2")
            {
                int fromYear = fromdate.Year;
                int Frommonth = fromdate.Month;

                //-- FOR request for the particlar agent user
                whereclause = " Booktype=1 and CreatorId='" + userid + "' and  Month (Bookingdate)= '" + Frommonth + "' and  Year(bookingdate)= '" +fromYear + "'";
                tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
                Int64? countView = tlistcnt.Count;
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[0] = 0;
                else
                    intCount[0] = Convert.ToInt32(countView);

                //-- FOR request for all the agencies user of that particlar user

                string getcurrentagencyuser = GetAllAgencyUsers(userid);
                whereclause = " Booktype=1 and CreatorId IN (" + getcurrentagencyuser + ") and  Month (Bookingdate)= '" + Frommonth + "' and  Year(bookingdate)= '" + fromYear + "'";
                tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);

                countView = tlistcnt.Count;
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[1] = 0;
                else
                    intCount[1] = Convert.ToInt32(countView);



                //-- FOR  Conversion request for the particlar agent user
                whereclause = " Booktype=1 and CreatorId='" + userid + "' and requeststatus=5 and   Month (Bookingdate)= '" + Frommonth + "' and  Year(bookingdate)= '" + fromYear + "'";
                tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
                countView = tlistcnt.Count;
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[2] = 0;
                else
                    intCount[2] = Convert.ToInt32(countView);

                //-- FOR request for all the agencies user of that particlar user
                whereclause = " Booktype in (1,2) and CreatorId IN (" + getcurrentagencyuser + ") and requeststatus=5 and   Month (Bookingdate)= '" + Frommonth + "' and  Year(bookingdate)= '" + fromYear + "'";
                tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);

                countView = tlistcnt.Count;
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[3] = 0;
                else
                    intCount[3] = Convert.ToInt32(countView);

                //-- FOR  Booking for the particlar agent user
                whereclause = " Booktype=0 and CreatorId='" + userid + "' and   Month (Bookingdate)= '" + Frommonth + "' and  Year(bookingdate)= '" + fromYear + "'";
                tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
                countView = Convert.ToInt64(tlistcnt.Sum(a => a.RevenueAmount));
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[4] = 0;
                else
                    intCount[4] = Convert.ToInt32(countView);


                countView = tlistcnt.Count;
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[6] = 0;
                else
                    intCount[6] = Convert.ToInt32(countView);
                //-- FOR request for all the agencies user of that particlar user
                whereclause = " Booktype=0 and CreatorId IN (" + getcurrentagencyuser + ") and   Month (Bookingdate)= '" + Frommonth + "' and  Year(bookingdate)= '" + fromYear + "'";
                tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);

                countView = tlistcnt.Count;
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[5] = 0;
                else
                    intCount[5] = Convert.ToInt32(countView);



            }

            if (type == "3")
            {
                int fromYear = fromdate.Year;
                int Frommonth = fromdate.Month;

                //-- FOR request for the particlar agent user
                whereclause = " Booktype in (1,2) and CreatorId='" + userid + "' and  Year(bookingdate)= '" + fromYear + "'";
                tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
                Int64? countView = tlistcnt.Count;
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[0] = 0;
                else
                    intCount[0] = Convert.ToInt32(countView);

                //-- FOR request for all the agencies user of that particlar user

                string getcurrentagencyuser = GetAllAgencyUsers(userid);
                whereclause = " Booktype in (1,2) and CreatorId IN (" + getcurrentagencyuser + ")  and  Year(bookingdate)= '" + fromYear + "'";
                tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);

                countView = tlistcnt.Count;
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[1] = 0;
                else
                    intCount[1] = Convert.ToInt32(countView);



                //-- FOR  Conversion request for the particlar agent user
                whereclause = " Booktype in (1,2) and CreatorId='" + userid + "' and requeststatus=5 and     Year(bookingdate)= '" + fromYear + "'";
                tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
                countView = tlistcnt.Count;
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[2] = 0;
                else
                    intCount[2] = Convert.ToInt32(countView);

                //-- FOR request for all the agencies user of that particlar user
                whereclause = " Booktype in (1,2) and CreatorId IN (" + getcurrentagencyuser + ") and requeststatus=5 and  Year(bookingdate)= '" + fromYear + "'";
                tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);

                countView = tlistcnt.Count;
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[3] = 0;
                else
                    intCount[3] = Convert.ToInt32(countView);

                //-- FOR  Booking for the particlar agent user
                whereclause = " Booktype=0 and CreatorId='" + userid + "' and    Year(bookingdate)= '" + fromYear + "'";
                tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
                countView = Convert.ToInt64(tlistcnt.Sum(a => a.RevenueAmount));
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[4] = 0;
                else
                    intCount[4] = Convert.ToInt32(countView);

                countView = tlistcnt.Count;
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[6] = 0;
                else
                    intCount[6] = Convert.ToInt32(countView);
                //-- FOR request for all the agencies user of that particlar user
                whereclause = " Booktype=0 and CreatorId IN (" + getcurrentagencyuser + ") and    Year(bookingdate)= '" + fromYear + "'";
                tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);

                countView = tlistcnt.Count;
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[5] = 0;
                else
                    intCount[5] = Convert.ToInt32(countView);



            }


            ////---- count New request
            //if (type == "2")
            //{
            //    whereclause = " HotelId  in (" + HotelId + ")  and Booktype in (1,2) and RequestStatus=1 and  Bookingdate  between '" + startdate + "' and '" + enddate.AddDays(1).AddSeconds(-1) + "'";
            //    tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
            //    Int64? countView = tlistcnt.Sum(a => a.RequestStatus = (int)BookingRequestStatus.New);
            //    if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
            //        intCount[0] = 0;
            //    else
            //        intCount[0] = Convert.ToInt32(countView);
            //}


            ////---- count Online request
            //if (type == "3")
            //{
            //    whereclause = "HotelId  in (" + HotelId + ") and  Booktype in (1,2) and  Bookingdate  between '" + startdate + "' and '" + enddate.AddDays(1).AddSeconds(-1) + "'";
            //    tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
            //    Int64? countView = tlistcnt.Count;
            //    if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
            //        intCount[0] = 0;
            //    else
            //        intCount[0] = Convert.ToInt32(countView);
            //}


            //if (type == "4")
            //{

            //    string whereclause1 = "HotelId  in (" + HotelId + ") and Booktype=0 and  Bookingdate  between '" + startdate + "' and '" + enddate.AddDays(1).AddSeconds(-1) + "'";
            //    TList<Booking> tlistcnt1 = DataRepository.BookingProvider.GetPaged(whereclause1, string.Empty, 0, int.MaxValue, out Total);
            //    Int64? countView = Convert.ToInt64(tlistcnt1.Sum(a => a.RevenueAmount));
            //    if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
            //        intCount[0] = 0;
            //    else
            //        intCount[0] = Convert.ToInt32(countView);

            //}
            return intCount;


        }
        #endregion

        public string GetAllAgencyUsers(int userid)
        {

            Users getparentid = DataRepository.UsersProvider.GetByUserId(Convert.ToInt64(userid));

            int Total = 0;
            string whereclause="";
            if( getparentid.Usertype == (int) Usertype.Agency)
                whereclause = "ParentID =" + getparentid.UserId;
            else
             whereclause= "UserId ="+getparentid.ParentId;
            TList<Users> getallagents = DataRepository.UsersProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);

                //whereclause = "HotelId  in (" + HotelId + ") and  Booktype in (1,2) and  Bookingdate  between '" + startdate + "' and '" + enddate.AddDays(1).AddSeconds(-1) + "'";
                //tlistcnt = 
            string useridforagency = userid.ToString();

            foreach (Users us in getallagents)
            {
                if (useridforagency.Length > 0)
                {
                    useridforagency += "," + us.UserId;
                }
                else
                {
                    useridforagency =""+ us.UserId;
                }
            
            
            }

            return useridforagency;
        }


        /// <summary>
        /// This function used to get all agent by agencyID.
        /// </summary>
        /// <returns></returns>
        public TList<Users> GetAllAgencyUser(int  agencyID)
        {
            int total = 0;
            string whereClause = UsersColumn.IsActive + "=1 and (" + UsersColumn.ParentId + "=" + agencyID + " OR " + UsersColumn.UserId + "=" + agencyID + ")";
            string OrderBy = UsersColumn.FirstName + " ASC";
            TList<Users> objUser = DataRepository.UsersProvider.GetPaged(whereClause, OrderBy, 0, int.MaxValue, out total);
            return objUser;
        }


        public TList<Users> GetAllAgencyUser(string userid)
        {


            int total = 0;
            string whereClause = UsersColumn.IsActive + "=1 and " + UsersColumn.UserId + " in (" + userid + ")";
            string OrderBy = UsersColumn.FirstName + " ASC";
            TList<Users> objUser = DataRepository.UsersProvider.GetPaged(whereClause, OrderBy, 0, int.MaxValue, out total);
            return objUser;
        }




       //-------  Control panel


        #region Get statistics for control panel
        /// <summary>
        /// Get by day
        /// </summary>
        /// <param name="Fromdate"> start date</param>
        /// <param name="toDate">end date</param>
        /// <param name="type">type -booking/request</param>
        /// <returns></returns>
        public DataTable Getcontrolpaneluser(DateTime Fromdate, DateTime toDate, string type, int user)
        {
            List<Fetchdata> objfetch = new List<Fetchdata>();
          
                int fromYear = Fromdate.Year;
                int toyear = toDate.Year;
                int Frommonth = Fromdate.Month;
                int Tomonth = toDate.Month;

                DateTime dtfrmdtcol = Fromdate;
          

             TList<Users> tlstagent= GetAllAgencyUser(user);

             DataTable dtmain = new DataTable();
             dtmain.Columns.Add("Username");

             #region colformonth
             if (fromYear != toyear)
             {

                 for (int i = Frommonth; i <= 12; i++)
                 {
                     dtmain.Columns.Add(dtfrmdtcol.ToString("MMM yyyy"));
                     dtfrmdtcol = dtfrmdtcol.AddMonths(1);
                 }

                 for (int i = 1; i <= Tomonth; i++)
                 {
                     dtmain.Columns.Add(dtfrmdtcol.ToString("MMM yyyy"));
                     dtfrmdtcol = dtfrmdtcol.AddMonths(1);
                 }
             
             }
             else
             {
                 for (int i = Frommonth; i <= Tomonth; i++)
                 {
                     dtmain.Columns.Add(dtfrmdtcol.ToString("MMM yyyy"));
                     dtfrmdtcol = dtfrmdtcol.AddMonths(1);
                 }
             }
             #endregion

             foreach (Users u in tlstagent)
             {
                 DateTime dtfromrow = Fromdate;

             //Users u = DataRepository.UsersProvider.GetByUserId(856);
                 if (fromYear != toyear)
                 {
                     DataRow druser = dtmain.NewRow();
                     druser["Username"] = u.FirstName; 
                     for (int i = Frommonth; i <= 12; i++)
                     {
                         #region fetch
                         int[] count = getstatisticscountroluser(dtfromrow, dtfromrow, Convert.ToInt32(u.UserId));
                         //-----  for booking     
                         if (type == "1")
                             druser[dtfromrow.ToString("MMM yyyy")] = count[1];
                         else ///--- for request
                             druser[dtfromrow.ToString("MMM yyyy")] = count[0];


                         dtfromrow = dtfromrow.AddMonths(1);
                         #endregion
                     }
                     for (int i = 1; i <= Tomonth; i++)
                     {
                         #region fetch


                         int[] count = getstatisticscountroluser(dtfromrow, dtfromrow, Convert.ToInt32(u.UserId));


                         //-----  for booking     
                         if (type == "1")
                             druser[dtfromrow.ToString("MMM yyyy")] = count[1];
                         else
                             druser[dtfromrow.ToString("MMM yyyy")] = count[0];


                         dtfromrow = dtfromrow.AddMonths(1);
                         #endregion
                     }
                     dtmain.Rows.Add(druser);

                 }
                 else
                 {

                    
                     DataRow druser = dtmain.NewRow();
                     for (int i = Frommonth; i <= Tomonth; i++)
                     {
                         druser["Username"] = u.FirstName;                     
                         #region fetch
                        

                         int[] count = getstatisticscountroluser(dtfromrow, dtfromrow,  Convert.ToInt32(u.UserId));

                         
                         //-----  for booking     
                         if(type=="1")
                         druser[dtfromrow.ToString("MMM yyyy")] = count[1];
                         else
                             druser[dtfromrow.ToString("MMM yyyy")] = count[0];


                       dtfromrow = dtfromrow.AddMonths(1);
                         #endregion
                     }
                     dtmain.Rows.Add(druser);

                 }


             }


            return dtmain;
        }
        #endregion


        #region Get Count 
        /// <summary>
        /// Get count of the particular duration
        /// </summary>
        /// <param name="fromdate"></param>
        /// <param name="enddt"></param>
        /// <returns></returns>
        public int[] getstatisticscountroluser(DateTime fromdate, DateTime enddt,  int userid)
        {

            string strfromdate = Convert.ToString(fromdate);
            int[] intCount = new int[9];
            int Total = 0;
            DateTime startdate = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day);
            DateTime enddate = new DateTime(enddt.Year, enddt.Month, enddt.Day);
            // string whereclause = "HotelId=" + HotelId + " and statdate ="+fromdate.ToString();
            string whereclause = " Booktype in (1,2) and CreatorId='" + userid + "' and  Bookingdate  between '" + startdate + "' and '" + enddate.AddDays(1).AddSeconds(-1) + "'";
            TList<Booking> tlistcnt = new TList<Entities.Booking>();// DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
            //-- count Ten req
            
           


                int fromYear = fromdate.Year;
                int Frommonth = fromdate.Month;


                //-- FOR request- def , is comm  cehcked for the particlar agent user Booktype in (1,2) and  ( IsComissionDone=0 or  IsComissionDone is null) and requeststatus in (7)
                //whereclause = " Booktype in (1,2) and CreatorId='" + userid + "' and requeststatus in (5,7) and  Month (Bookingdate)= '" + Frommonth + "' and  Year(bookingdate)= '" + fromYear + "'";
                whereclause = " Booktype in (1,2) and CreatorId='" + userid + "' and  Month (Bookingdate)= '" + Frommonth + "' and  Year(bookingdate)= '" + fromYear + "'";
                tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
                Int64? countView = Convert.ToInt64(tlistcnt.Count);
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[0] = 0;
                else
                    intCount[0] = Convert.ToInt32(countView);

                //-- FOR  Booking for the particlar agent user
                whereclause = " Booktype=0 and CreatorId='" + userid + "' and   Month (Bookingdate)= '" + Frommonth + "' and  Year(bookingdate)= '" + fromYear + "'";
                tlistcnt = DataRepository.BookingProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
                countView = Convert.ToInt64(tlistcnt.Sum(a => a.RevenueAmount));
                if (string.IsNullOrEmpty(Convert.ToString(countView.Value)))
                    intCount[1] = 0;
                else
                    intCount[1] = Convert.ToInt32(countView);

            //----------- userid

                intCount[2] = userid;
            

            return intCount;


        }
        #endregion

    }
}
