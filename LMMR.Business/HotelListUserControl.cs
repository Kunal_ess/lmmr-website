﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using LMMR.Entities;
using LMMR.Business;
using LMMR.Data;

namespace LMMR.Business
{
   public class HotelListUserControl
    {
       
       public HotelListUserControl()
       {
           TList<Hotel> htl = new TList<Hotel>();
           htl = DataRepository.HotelProvider.GetByClientId(1);
       }
       #region Variables
       private int HotelID;
       private string image;
       #endregion

       #region Properties
       public string _HotelID { get; set; }
       public string _image { get; set; }
       #endregion 

       #region Methods
       public void getImage(int clientID)
       {
           TList<Hotel> htl = new TList<Hotel>();
           htl = DataRepository.HotelProvider.GetByClientId(clientID); // need to dynamically replace 1 with client ID

           foreach (Hotel h in htl)
           {

               _image = h.Logo;
           }

       }
       #endregion 
    }
}
