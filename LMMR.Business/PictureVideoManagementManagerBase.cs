﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;

namespace LMMR.Business
{
    public abstract class PictureVideoManagementManagerBase
    {
        #region Variables
        //Declare Variable to store data
        private TList<MeetingRoomPictureVideo> _varGetAllMeetingRoomPictureVideo;

        //Declare Variable to store data
        private TList<BedRoomPictureImage> _varGetAllBedRoomPicture;

        //Declare Variable to store data
        private TList<HotelPhotoVideoGallary> _varGetAllHotelPictureVideo;

        //Declare Variable to store data
        private TList<MeetingRoom> _varGetAllMeetingRoom;

        //Declare Variable to store data
        private TList<BedRoom> _varGetAllBedRoom;

        //Declare Variable to store data
        private string _varMessage;

        #endregion

        #region Properties
        //Property to Store the TList of Entities.HotelPictureVideo class
        public TList<HotelPhotoVideoGallary> propGetAllHotelPictureVideo
        {
            get { return _varGetAllHotelPictureVideo; }
            set { _varGetAllHotelPictureVideo = value; }

        }

        //Property to Store the TList of Entities.MeetingRoomPictureVideo class
        public TList<MeetingRoomPictureVideo> propGetVideoPictureByMeetingRoomID
        {
            get { return _varGetAllMeetingRoomPictureVideo; }
            set { _varGetAllMeetingRoomPictureVideo = value; }

        }

        //Property to Store the TList of Entities.MeetingRoom class
        public TList<MeetingRoom> propGetAllMeetingRoomByHotelID
        {
            get { return _varGetAllMeetingRoom; }
            set { _varGetAllMeetingRoom = value; }
        }

        //Property to Store the TList of Entities.BedRoomPictureVideo class
        public TList<BedRoomPictureImage> propGetVideoPictureByBedRoomID
        {
            get { return _varGetAllBedRoomPicture; }
            set { _varGetAllBedRoomPicture = value; }
        }

        //Property to Store the TList of Entities.BedRoom class
        public TList<BedRoom> propGetAllBedByHotelID
        {
            get { return _varGetAllBedRoom; }
            set { _varGetAllBedRoom = value; }
        }

        //Property to Store Message
        public string propMessage
        {
            get { return _varMessage; }
            set { _varMessage = value; }
        }

        #endregion

        #region Functions
        /// <summary>
        /// Function to get all list of MeetingRoomPictureVideo
        /// </summary>
        /// <param name="meetingRoomID"></param>
        /// <returns></returns>
        public TList<MeetingRoomPictureVideo> GetPictureVideoByMeetingRoomID(int meetingRoomID)
        {
            propGetVideoPictureByMeetingRoomID = DataRepository.MeetingRoomPictureVideoProvider.GetByMeetingRoomId(meetingRoomID);

            return propGetVideoPictureByMeetingRoomID;
        }

        /// <summary>
        /// Function to get all MeetingRoom
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public TList<MeetingRoom> GetAllMeetingRoomByHotelID(int hotelID)
        {
            int Total = 0;
            string whereclause = "Hotel_Id=" + hotelID + " and IsDeleted='" + false + "' ";
            string orderby = "OrderNumber";
            propGetAllMeetingRoomByHotelID = DataRepository.MeetingRoomProvider.GetPaged(whereclause, orderby, 0, int.MaxValue, out Total);
            return propGetAllMeetingRoomByHotelID;
        }

        /// <summary>
        /// function to insert new pictures into table
        /// </summary>
        /// <param name="meetingRoomID"></param>
        /// <param name="fileType"></param>
        /// <param name="imageName"></param>
        /// <param name="fileName"></param>
        /// <param name="alterText"></param>
        /// <param name="isMain"></param>
        /// <param name="orderNumber"></param>
        /// <param name="updatedBy"></param>
        /// <returns></returns>
        public string AddNewMeetingRoomPicturs(int meetingRoomID, int fileType, string imageName, string fileName, string alterText, bool isMain, int orderNumber, int updatedBy)
        {
            TransactionManager tm = null;
            try
            {

                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                MeetingRoomPictureVideo objMeetingRoomPicture = new MeetingRoomPictureVideo();
                objMeetingRoomPicture.MeetingRoomId = meetingRoomID;
                objMeetingRoomPicture.FileType = fileType;
                objMeetingRoomPicture.ImageName = imageName;
                objMeetingRoomPicture.FileName = fileName;
                objMeetingRoomPicture.AlterText = alterText;
                objMeetingRoomPicture.IsMain = isMain;
                objMeetingRoomPicture.OrderNumber = orderNumber;
                objMeetingRoomPicture.UpdatedBy = updatedBy;
                if (DataRepository.MeetingRoomPictureVideoProvider.Insert(tm, objMeetingRoomPicture))
                {
                    propMessage = "Added Successfully!";
                }
                else
                {
                    propMessage = "Please try again!";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return propMessage;
        }


        /// <summary>
        /// functing to get bedroom pictures by bedroomID.
        /// </summary>
        /// <param name="bedRoomID"></param>
        /// <returns></returns>
        public TList<BedRoomPictureImage> GetPictureVideoByBedRoomID(int bedRoomID)
        {
            propGetVideoPictureByBedRoomID = DataRepository.BedRoomPictureImageProvider.GetByBedRoomId(bedRoomID);

            return propGetVideoPictureByBedRoomID;
        }

        /// <summary>
        /// Function to get all BedRoom list by hotelID 
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public TList<BedRoom> GetAllBedRoomByHotelID(int hotelID)
        {
            int Total = 0;
            string whereclause = "HotelId=" + hotelID + " and IsDeleted='" + false + "' ";
            //string orderby = "OrderNumber";
            propGetAllBedByHotelID = DataRepository.BedRoomProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
            return propGetAllBedByHotelID;
        }

        /// <summary>
        /// Function to add new bedroom picture.
        /// </summary>
        /// <param name="bedRoomID"></param>
        /// <param name="fileType"></param>
        /// <param name="imageName"></param>
        /// <param name="fileName"></param>
        /// <param name="alterText"></param>
        /// <param name="isMain"></param>
        /// <param name="orderNumber"></param>
        /// <param name="updatedBy"></param>
        /// <returns></returns>
        public string AddNewBedroomPicturs(int bedRoomID, int fileType, string imageName, string fileName, string alterText, bool isMain, int orderNumber, int updatedBy)
        {
            TransactionManager tm = null;
            try
            {

                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                BedRoomPictureImage objMeetingRoomPicture = new BedRoomPictureImage();
                objMeetingRoomPicture.BedRoomId = bedRoomID;
                objMeetingRoomPicture.FileType = fileType;
                objMeetingRoomPicture.ImageName = imageName;
                objMeetingRoomPicture.FileName = fileName;
                objMeetingRoomPicture.AlterText = alterText;
                objMeetingRoomPicture.IsMain = isMain;
                objMeetingRoomPicture.OrderNumber = orderNumber;
                objMeetingRoomPicture.UpdatedBy = updatedBy;
                if (DataRepository.BedRoomPictureImageProvider.Insert(tm, objMeetingRoomPicture))
                {
                    propMessage = "Added Successfully!";
                }
                else
                {
                    propMessage = "Please try again!";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return propMessage;
        }


        /// <summary>
        /// Function to add new hotel picture video.
        /// </summary>
        /// <param name="hotelID"></param>
        /// <param name="fileType"></param>
        /// <param name="imageName"></param>
        /// <param name="fileName"></param>
        /// <param name="alterText"></param>
        /// <param name="isMain"></param>
        /// <param name="orderNumber"></param>
        /// <param name="updatedBy"></param>
        /// <returns></returns>
        public string AddNewHotelPictursVideo(int hotelID, int fileType, string imageName, string fileName, string alterText, bool isMain, int orderNumber, int updatedBy)
        {
            TransactionManager tm = null;
            try
            {

                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                HotelPhotoVideoGallary objHotelPicture = new HotelPhotoVideoGallary();
                objHotelPicture.HotelId = hotelID;
                objHotelPicture.FileType = fileType;
                objHotelPicture.ImageName = imageName;
                objHotelPicture.FileName = fileName;
                objHotelPicture.AlterText = alterText;
                objHotelPicture.IsMain = isMain;
                objHotelPicture.OrderNumber = orderNumber;
                objHotelPicture.UpdatedBy = updatedBy;
                if (DataRepository.HotelPhotoVideoGallaryProvider.Insert(tm, objHotelPicture))
                {
                    propMessage = "Added Successfully!";
                }
                else
                {
                    propMessage = "Please try again!";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
            }
            return propMessage;
        }


        /// <summary>
        /// Function to get all list of HotelPictureVideo
        /// </summary>
        /// <param name="hotelID"></param>
        /// <returns></returns>
        public TList<HotelPhotoVideoGallary> GetAllHotelPictureVideo(int hotelID)
        {
            propGetAllHotelPictureVideo = DataRepository.HotelPhotoVideoGallaryProvider.GetByHotelId(hotelID);

            return propGetAllHotelPictureVideo;
        }

        /// <summary>
        /// Save pictures data in database
        /// </summary>
        /// <param name="element"></param>
        /// <param name="type"></param>
        /// <param name="typeID"></param>
        /// <returns></returns>
        public string SavePicturesVideo(object element, string type, int typeID)
        {
            TransactionManager tm = null;
            try
            {

                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                switch (type)
                {

                    case "MeetingRoom":
                        TList<MeetingRoomPictureVideo> ObjMeetingRoomNewImage = ((TList<MeetingRoomPictureVideo>)element).Copy();
                        TList<MeetingRoomPictureVideo> ObjMeetingRoomOldImage = DataRepository.MeetingRoomPictureVideoProvider.GetByMeetingRoomId(typeID);
                        foreach (MeetingRoomPictureVideo d in ObjMeetingRoomOldImage)
                        {
                            DataRepository.MeetingRoomPictureVideoProvider.Delete(tm, d.Id);

                        }

                        foreach (MeetingRoomPictureVideo a in ObjMeetingRoomNewImage)
                        {
                            if (a.IsMain == true)
                            {
                                MeetingRoom objMeetingRoom = DataRepository.MeetingRoomProvider.GetById(tm, a.MeetingRoomId);
                                if (objMeetingRoom != null)
                                {
                                    objMeetingRoom.Picture = a.ImageName;
                                    DataRepository.MeetingRoomProvider.Update(tm, objMeetingRoom);
                                }
                            }

                            DataRepository.MeetingRoomPictureVideoProvider.Insert(tm, a);

                        }

                        break;
                    case "BedRoom":
                        TList<BedRoomPictureImage> ObjBedRoomNewImage = ((TList<BedRoomPictureImage>)element).Copy();
                        TList<BedRoomPictureImage> ObjBedRoomOldImage = DataRepository.BedRoomPictureImageProvider.GetByBedRoomId(typeID);
                        foreach (BedRoomPictureImage d in ObjBedRoomOldImage)
                        {
                            DataRepository.BedRoomPictureImageProvider.Delete(tm, d.Id);

                        }

                        foreach (BedRoomPictureImage a in ObjBedRoomNewImage)
                        {
                            if (a.IsMain == true)
                            {
                                BedRoom objBedRoom = DataRepository.BedRoomProvider.GetById(tm, a.BedRoomId);
                                if (objBedRoom != null)
                                {
                                    objBedRoom.Picture = a.ImageName;
                                    DataRepository.BedRoomProvider.Update(tm, objBedRoom);
                                }
                            }
                            DataRepository.BedRoomPictureImageProvider.Insert(tm, a);
                        }
                        break;

                    case "Hotel":
                        TList<HotelPhotoVideoGallary> ObjHotelNewImage = ((TList<HotelPhotoVideoGallary>)element).Copy();
                        TList<HotelPhotoVideoGallary> ObjHotelOldImage = DataRepository.HotelPhotoVideoGallaryProvider.GetByHotelId(typeID);
                        foreach (HotelPhotoVideoGallary d in ObjHotelOldImage)
                        {
                            DataRepository.HotelPhotoVideoGallaryProvider.Delete(tm, d.Id);

                        }

                        foreach (HotelPhotoVideoGallary a in ObjHotelNewImage)
                        {
                            if (a.IsMain == true)
                            {
                                Hotel objHotel = DataRepository.HotelProvider.GetById(tm, a.HotelId);
                                if (objHotel != null)
                                {
                                    objHotel.Logo = a.ImageName;
                                    DataRepository.HotelProvider.Update(tm, objHotel);
                                }
                            }
                            DataRepository.HotelPhotoVideoGallaryProvider.Insert(tm, a);
                        }
                        break;
                }
                tm.Commit();
            }
            catch (Exception ex)
            {

                tm.Rollback();
            }
            return "ok";
        }
        #endregion
    }

}
