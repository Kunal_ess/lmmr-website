﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
namespace LMMR.Business
{
    public class EmailValueCollection
    {
        public string SiteRootPath
        {
            get
            {
                string host = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                string appRootUrl = HttpContext.Current.Request.ApplicationPath;
                return host + appRootUrl + "/";
            }
        }
        public Dictionary<string, string> EmailForAddPromotions(string HotelName, string DateFrom, string DateTo, string Description, string UserName)
        {
            Dictionary<string, string> Promotion = new Dictionary<string, string>();
            Promotion.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            Promotion.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            Promotion.Add("@EMAILTO", "Operator");
            Promotion.Add("@HOTELNAME", HotelName);
            Promotion.Add("@USERNAME", UserName);
            Promotion.Add("@DESCRIPTION", Description);
            Promotion.Add("@DATETO", DateTo);
            Promotion.Add("@DATEFROM", DateFrom);
            Promotion.Add("@SENDERUSER", UserName);
            return Promotion;
        }
        public Dictionary<string, string> EmailForGoOnline(string HotelName, string UserName, string SendTo)
        {
            Dictionary<string, string> goOnline = new Dictionary<string, string>();
            goOnline.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            goOnline.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            goOnline.Add("@EMAILTO", SendTo);
            goOnline.Add("@HOTELNAME", HotelName);
            goOnline.Add("@USERNAME", UserName);
            goOnline.Add("@SENDERUSER", HotelName);
            return goOnline;
        }

        public Dictionary<string, string> EmailForGoOnlineHotelUser(string HotelName, string UserName)
        {
            Dictionary<string, string> goOnline = new Dictionary<string, string>();
            goOnline.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            goOnline.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            goOnline.Add("@HOTELNAME", HotelName);
            goOnline.Add("@USERNAME", UserName);
            goOnline.Add("@LMMR", "Last Minute Meeting Room Admin");
            return goOnline;
        }
        public Dictionary<string, string> EmailForLoginDetailsOfClientContract(string useremailid, string password, string SenderName)
        {
            Dictionary<string, string> Promotion = new Dictionary<string, string>();
            Promotion.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            Promotion.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            Promotion.Add("@LOGINURL", SiteRootPath + "login.aspx");
            Promotion.Add("@EMAILTO", "User");
            Promotion.Add("@USEREMAILID", useremailid);
            Promotion.Add("@PASSWORD", password);
            Promotion.Add("@SENDERUSER", SenderName);
            return Promotion;
        }

        public Dictionary<string, string> EmailForLoginDetailsOfAgencyUser(string useremailid, string password, string SenderName)
        {
            Dictionary<string, string> Promotion = new Dictionary<string, string>();
            Promotion.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            Promotion.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            Promotion.Add("@LOGINURL", SiteRootPath + "login.aspx");
            Promotion.Add("@EMAILTO", "User");
            Promotion.Add("@USEREMAILID", useremailid);
            Promotion.Add("@PASSWORD", password);
            Promotion.Add("@SENDERUSER", SenderName);
            return Promotion;
        }

        public Dictionary<string, string> EmailForChangePasswordDetails(string useremailid, string password, string SenderName)
        {
            Dictionary<string, string> Promotion = new Dictionary<string, string>();
            Promotion.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            Promotion.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            Promotion.Add("@EMAILTO", "User");
            Promotion.Add("@USEREMAILID", useremailid);
            Promotion.Add("@PASSWORD", password);
            Promotion.Add("@LOGINURL", SiteRootPath + "Login.aspx");
            Promotion.Add("@SENDERUSER", SenderName);
            return Promotion;
        }

        public Dictionary<string, string> EmailForUpdateRequestStatus(string useremailid, string BookingId, string Status, string SenderName, string ContactPerson, string Nameofthevenue, string arrivaldate)
        {
            Dictionary<string, string> Promotion = new Dictionary<string, string>();
            Promotion.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            Promotion.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            Promotion.Add("@EMAILTO", ContactPerson);
            Promotion.Add("@USEREMAILID", useremailid);
            Promotion.Add("@REQUESTID", BookingId);
            Promotion.Add("@STATUS", Status);
            Promotion.Add("@SENDERUSER", SenderName);
            Promotion.Add("@CONTACTPERSON", ContactPerson);
            Promotion.Add("@NAMEOFTHEVENUE", Nameofthevenue);
            Promotion.Add("@ARRIVALDATE", arrivaldate);
            return Promotion;
        }

        public Dictionary<string, string> EmailForBookingDone(string user, string BookingId, string HotelName, string UserBy, string SenderName)
        {
            Dictionary<string, string> Promotion = new Dictionary<string, string>();
            Promotion.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            Promotion.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            Promotion.Add("@EMAILTO", user);
            Promotion.Add("@HOTELNAME", HotelName);
            Promotion.Add("@USERNAME", UserBy);
            Promotion.Add("@BOOKINGID", BookingId);
            Promotion.Add("@SENDERUSER", SenderName);
            return Promotion;
        }

        public Dictionary<string, string> EmailForBookingDoneByWL(string user, string BookingId, string HotelName, string UserBy, string SenderName, string logo)
        {
            Dictionary<string, string> Promotion = new Dictionary<string, string>();
            Promotion.Add("@EMAILHEADER", SiteRootPath + "Images/bg_WL_Mail.jpg");
            Promotion.Add("@LMMRLOGO", logo);
            Promotion.Add("@EMAILTO", user);
            Promotion.Add("@HOTELNAME", HotelName);
            Promotion.Add("@USERNAME", UserBy);
            Promotion.Add("@BOOKINGID", BookingId);
            Promotion.Add("@SENDERUSER", SenderName);
            return Promotion;
        }


        public Dictionary<string, string> EmailForRequestDone(string user, string BookingId, string HotelName, string UserBy, string SenderName)
        {
            Dictionary<string, string> Promotion = new Dictionary<string, string>();
            Promotion.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            Promotion.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            Promotion.Add("@EMAILTO", user);
            Promotion.Add("@HOTELNAME", HotelName);
            Promotion.Add("@USERNAME", UserBy);
            Promotion.Add("@REQUESTID", BookingId);
            Promotion.Add("@SENDERUSER", SenderName);
            return Promotion;
        }

        public Dictionary<string, string> EmailForRequestDoneByWL(string user, string BookingId, string HotelName, string UserBy, string SenderName, string wlLogo)
        {
            Dictionary<string, string> Promotion = new Dictionary<string, string>();
            Promotion.Add("@EMAILHEADER", SiteRootPath + "Images/bg_WL_Mail.jpg");
            Promotion.Add("@LMMRLOGO", wlLogo);
            Promotion.Add("@EMAILTO", user);
            Promotion.Add("@HOTELNAME", HotelName);
            Promotion.Add("@USERNAME", UserBy);
            Promotion.Add("@REQUESTID", BookingId);
            Promotion.Add("@SENDERUSER", SenderName);
            return Promotion;
        }


        public Dictionary<string, string> EmailForSurveybooking(string user, string HotelName, string SenderName, string userid, string venuid, string bookingID, string link)
        {
            Dictionary<string, string> Promotion = new Dictionary<string, string>();
            Promotion.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            Promotion.Add("@emaillink", SiteRootPath + link);

            Promotion.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            Promotion.Add("@EMAILTO", user);
            Promotion.Add("@HOTELNAME", HotelName);
            Promotion.Add("@userid", userid);
            Promotion.Add("@venuid", venuid);
            //Promotion.Add("@USERNAME", UserBy);
            Promotion.Add("@bookingID", bookingID);
            Promotion.Add("@SENDERUSER", SenderName);
            return Promotion;
        }

        public Dictionary<string, string> EmailForSurveyrequest(string user, string SenderName, string userid, string venuid, string bookingID, string link)
        {
            Dictionary<string, string> Promotion = new Dictionary<string, string>();
            Promotion.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            Promotion.Add("@emaillink", SiteRootPath + link);
            Promotion.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            Promotion.Add("@EMAILTO", user);
            Promotion.Add("@userid", userid);
            //Promotion.Add("@HOTELNAME", HotelName);
            Promotion.Add("@venuid", venuid);
            //Promotion.Add("@USERNAME", UserBy);
            Promotion.Add("@BOOKINGID", bookingID);
            Promotion.Add("@SENDERUSER", SenderName);
            return Promotion;
        }

        public Dictionary<string, string> EmailForUserRegistration(string newuser, string email, string password, string senderName, string link)
        {
            Dictionary<string, string> Promotion = new Dictionary<string, string>();
            Promotion.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            Promotion.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            Promotion.Add("@EMAILTO", newuser);
            Promotion.Add("@USEREMAILID", email);
            Promotion.Add("@PASSWORD", password);
            Promotion.Add("@SENDERUSER", senderName);
            Promotion.Add("@ACTIVATIONLINK", SiteRootPath + link);
            return Promotion;
        }
        public Dictionary<string, string> EmailForAgencyRegistration(string newuser, string email, string password, string senderName)
        {
            Dictionary<string, string> Promotion = new Dictionary<string, string>();
            Promotion.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            Promotion.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            Promotion.Add("@EMAILTO", newuser);
            Promotion.Add("@USEREMAILID", email);
            Promotion.Add("@PASSWORD", password);
            Promotion.Add("@SENDERUSER", senderName);
            return Promotion;
        }
        public Dictionary<string, string> EmailFornewssubscribe(string FirstName, string LastName)
        {
            Dictionary<string, string> Promotion = new Dictionary<string, string>();
            Promotion.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            Promotion.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            Promotion.Add("@FIRSTNAME", FirstName);
            Promotion.Add("@LMMR", "Lastminutemeetingroom team");
            return Promotion;
        }

        public Dictionary<string, string> EmailForJoinToday(string ContactPerson, string Title)
        {
            Dictionary<string, string> joinToday = new Dictionary<string, string>();
            joinToday.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            joinToday.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            joinToday.Add("@CONTACTPERSON", ContactPerson);
            joinToday.Add("@Title", Title);
            joinToday.Add("@LMMR", "Last Minute Meeting Room Admin");
            return joinToday;
        }

        public Dictionary<string, string> EmailForSendRFP(string hotelusername ,string EventRFP, string responcedate, string decisiondate, string country, string city, string typeofvenue, string nameofvenue, string eventtype, string TotalAttendes, string eventstartdate, string eventenddate, string dateflexible, string requirebedrooms, string noofbedrooms,string additionalinfo)
        {
            Dictionary<string, string> Promotion = new Dictionary<string, string>();
            Promotion.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            Promotion.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            Promotion.Add("@EMAILTO", hotelusername);
            Promotion.Add("@EVENTRPF",EventRFP);
            Promotion.Add("@RESPONCEDUEDATE", responcedate);
            Promotion.Add("@DECISIONDATE", decisiondate);
            Promotion.Add("@COUNTRY", country);
            Promotion.Add("@CITY", city);
            Promotion.Add("@TYPEOFVENUE", typeofvenue);
            Promotion.Add("@NAMEOFVENUE", nameofvenue);
            Promotion.Add("@EVENTTYPE", eventtype);
            Promotion.Add("@TOTALATTENDES", TotalAttendes);
            Promotion.Add("@EVENTSTARTDATE", eventstartdate);
            Promotion.Add("@EVENTENDDATE", eventenddate);
            Promotion.Add("@DATEFLEXIBLE", dateflexible);
            Promotion.Add("@REQUIREBEDROOMS", requirebedrooms);
            Promotion.Add("@NOOFBEDROOMS", noofbedrooms);
            Promotion.Add("@ADDITIONALINFORMATION", additionalinfo);
            
            return Promotion;
        }

        public Dictionary<string, string> EmailForSendWLCode(string Email)
        {
            Dictionary<string, string> WLCode = new Dictionary<string, string>();
            WLCode.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            WLCode.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            WLCode.Add("@EMAILTO", Email);
            WLCode.Add("@LMMR", "Last Minute Meeting Room Admin");
            return WLCode;
        }


        public Dictionary<string, string> EmailForRequestCompletionToHotelUser(string emailTo, string hotelName, string requestingUserName)
        {
            Dictionary<string, string> joinToday = new Dictionary<string, string>();
            joinToday.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            joinToday.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            joinToday.Add("@EMAILTO", emailTo);
            joinToday.Add("@HOTELNAME", hotelName);
            joinToday.Add("@USERNAME", requestingUserName);
            joinToday.Add("@LMMR", "Last Minute Meeting Room Admin");
            return joinToday;
        }

        public Dictionary<string, string> EmailForRequestCompletionToUser(string contactPerson, List<string> hotelList)
        {
            Dictionary<string, string> request = new Dictionary<string, string>();
            request.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            request.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            request.Add("@CONTACTPERSON", contactPerson);
            string s = "";
            foreach (var hotel in hotelList)
            {
                s += hotel + ", ";
            }
            if (s.Length > 2)
            {
                s = s.Remove(s.Length - 2);
            }
            request.Add("@HOTELNAME", s);
            request.Add("@LMMR", "Last Minute Meeting Room Admin");
            return request;
        }

        public Dictionary<string, string> EmailForBookingCompletionToHotelUser(string emailTo, string hotelName, string bookingUserName)
        {
            Dictionary<string, string> joinToday = new Dictionary<string, string>();
            joinToday.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            joinToday.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            joinToday.Add("@EMAILTO", emailTo);
            joinToday.Add("@HOTELNAME", hotelName);
            joinToday.Add("@USERNAME", bookingUserName);
            joinToday.Add("@LMMR", "Last Minute Meeting Room Admin");
            return joinToday;
        }

        public Dictionary<string, string> EmailForBookingCompletionToUser(string contactPerson, string hotelName)
        {
            Dictionary<string, string> request = new Dictionary<string, string>();
            request.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            request.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            request.Add("@CONTACTPERSON", contactPerson);
            request.Add("@HOTELNAME", hotelName);
            request.Add("@LMMR", "Last Minute Meeting Room Admin");
            return request;
        }

        public Dictionary<string, string> EmailToOperatorForJointoday(string OperatorOrSuperAdmin, string HotelName, string Email, string ContactPerson, string Phone, string NumberOfMeetingRoom)
        {
            Dictionary<string, string> joinToday = new Dictionary<string, string>();
            joinToday.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            joinToday.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            joinToday.Add("@OperatorOrSuperAdmin", OperatorOrSuperAdmin);
            joinToday.Add("@HOTELNAME", HotelName);
            joinToday.Add("@EMAIL", Email);
            joinToday.Add("@CONTACTPERSON", ContactPerson);
            joinToday.Add("@PHONE", Phone);
            joinToday.Add("@NUMBEROFMEETINGROOM", NumberOfMeetingRoom);
            return joinToday;
        }

        public Dictionary<string, string> EmailToCoporate(string OperatorOrSuperAdmin, string companyname, string Email,string Phone, string contactname)
        {
            Dictionary<string, string> joinToday = new Dictionary<string, string>();
            joinToday.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            joinToday.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            joinToday.Add("@OperatorOrSuperAdmin", OperatorOrSuperAdmin);
            joinToday.Add("@COMPANYNAME", companyname);
            joinToday.Add("@EMAIL", Email);
            joinToday.Add("@PHONE", Phone);
            joinToday.Add("@CONTACTNAME", contactname);
            return joinToday;
        }
        

        public Dictionary<string, string> InvoiceEmail(string hotelName, string email, string senderName, string previousmonth)
        {
            Dictionary<string, string> Promotion = new Dictionary<string, string>();
            Promotion.Add("@SENDERUSER", senderName);
            Promotion.Add("@USEREMAILID", email);
            Promotion.Add("@HOTELNAME", hotelName);
            Promotion.Add("@PREVIOUSMONTH", previousmonth);
            return Promotion;
        }

        public Dictionary<string, string> CancelBookingToHotelUSer(string bookingid, string username, string canceldate)
        {
            Dictionary<string, string> Promotion = new Dictionary<string, string>();
            Promotion.Add("@SENDERUSER", "Lastminutemeetingroom support team");
            Promotion.Add("@BOOKINGID", bookingid);
            Promotion.Add("@USERNAME", username);
            Promotion.Add("@CANCELDATE", canceldate);
            return Promotion;
        }



        /// <summary>
        /// Work for Periodic mail
        /// </summary>
        /// <param name="SenderName"></param>
        /// <returns></returns>
        public Dictionary<string, string> EmailForPendingRequest(string SenderName)
        {
            Dictionary<string, string> mail = new Dictionary<string, string>();
            mail.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            mail.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            mail.Add("@SENDERUSER", SenderName);
            return mail;
        }

        public Dictionary<string, string> EmailForOustandingCommissions(string SenderName, string CurrentMonth, string List)
        {
            Dictionary<string, string> mail = new Dictionary<string, string>();
            mail.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            mail.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            mail.Add("@CURRENTMONTH", CurrentMonth);
            mail.Add("@LIST", List);
            mail.Add("@SENDERUSER", SenderName);
            return mail;
        }

        public Dictionary<string, string> EmailForReminderContent(string SenderName)
        {
            Dictionary<string, string> mail = new Dictionary<string, string>();
            mail.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            mail.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            mail.Add("@SENDERUSER", SenderName);
            return mail;
        }
        public Dictionary<string, string> EmailForReminderAvailability(string SenderName, string LoginURL, string Calender, string HotelName)
        {
            Dictionary<string, string> mail = new Dictionary<string, string>();
            mail.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            mail.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            mail.Add("@LOGINURL", LoginURL);
            mail.Add("@CALENDAR", Calender);
            mail.Add("@SENDER", SenderName);
            mail.Add("@HOTELNAME", HotelName);
            return mail;
        }
        public Dictionary<string, string> EmailForReminderInvoice(string SenderName, string url, string List)
        {
            Dictionary<string, string> mail = new Dictionary<string, string>();
            mail.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            mail.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            mail.Add("@URL", SiteRootPath + url);
            mail.Add("@SENDERUSER", SenderName);
            return mail;
        }
    
        public Dictionary<string, string> EmailForCheckCommission(string SenderName, string numberOfBooking, string List, string Url)
        {
            Dictionary<string, string> mail = new Dictionary<string, string>();
            mail.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            mail.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            mail.Add("@NUMBEROFBOOKING", numberOfBooking);
            mail.Add("@LIST", List);
            mail.Add("@Url", SiteRootPath + Url);
            mail.Add("@SENDERUSER", SenderName);
            return mail;
        }

        public Dictionary<string, string> EmailForNewsletter(string SenderName, string url, string FirstName)
        {
            Dictionary<string, string> mail = new Dictionary<string, string>();
            mail.Add("@EMAILHEADER", SiteRootPath + "Images/mail-headerbg.jpg");
            mail.Add("@LMMRLOGO", SiteRootPath + "Images/mail-logo.png");
            mail.Add("@FIRSTNAME", FirstName);
            mail.Add("@URL", SiteRootPath + url);
            mail.Add("@SENDERUSER", SenderName);
            return mail;
        }
    }
}
