﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;
#endregion

namespace LMMR.Business
{
    public abstract class ManageMapSearchBase
    {
        #region Constructor
        public ManageMapSearchBase() { }
        #endregion

        #region Variables

        private TList<Country> _varCountry;
        private TList<City> _varCity;
        private TList<Hotel> _varHotel;
        private TList<MainPoint> _varCenterPoint;
        #endregion

        #region Properties
        public TList<Country> propCountry
        {
            get
            {
                return _varCountry;
            }
            set { _varCountry = value; }
        }

        public TList<City> propCity
        {
            get { return _varCity; }
            set { _varCity = value; }
        }

        public TList<Hotel> propHotel
        {
            get { return _varHotel; }
            set { _varHotel = value; }
        }


        public TList<MainPoint> propCenterPoint
        {
            get { return _varCenterPoint; }
            set { _varCenterPoint = value; }
        }

        #endregion

        #region function
        /// <summary>
        /// This method is taking all the data of Country table
        /// </summary>
        /// <returns></returns>
        public TList<Country> GetCountryData()
        {
            int Total = 0;
            string whereclause = "IsForAll = 1 AND IsActive=1";
            string orderby = CountryColumn.CountryName + " ASC";
            propCountry = DataRepository.CountryProvider.GetPaged(whereclause, orderby, 0, int.MaxValue, out Total);
            return propCountry;

        }
        /// <summary>
        /// This method is taking all the data of City table
        /// </summary>
        /// <returns></returns>
        public TList<City> GetCityData(int CountryId)
        {
            int Total = 0;
            int flag = 0;
            string whereclause = "CountryId = " + CountryId + "";
            string orderby = CityColumn.City + " ASC";
            propCity = DataRepository.CityProvider.GetPaged(whereclause, orderby, 0, int.MaxValue, out Total);
            whereclause += " and IsActive = " + "1" + " and GoOnline = " + "1" + " and IsRemoved = " + "0";
            TList<Hotel> allHotel = DataRepository.HotelProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
            foreach (City city in propCity.ToArray())
            {
                flag = 0;
                foreach (Hotel hotel in allHotel)
                {
                    if (city.Id == hotel.CityId)
                    {
                        flag = 1;
                        break;
                    }
                }
                if (flag == 0)
                    propCity.Remove(city);
            }
            return propCity;
        }

        //public TList<City> GetCitiesData(int CountryId)
        //{
        //    int Total = 0;
        //    int flag = 0;
        //    string whereclause = "CountryId = " + CountryId + "";
        //    string orderby = CityColumn.City + " ASC";
        //    propCity = DataRepository.CityProvider.GetPaged(whereclause, orderby, 0, int.MaxValue, out Total);
        //    TList<Hotel> allHotel = DataRepository.HotelProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out Total);
        //    foreach (City city in propCity.ToArray())
        //    {
        //        flag = 0;
        //        foreach (Hotel hotel in allHotel)
        //        {
        //            if (city.Id == hotel.CityId)
        //            {
        //                flag = 1;
        //                break;
        //            }
        //        }
        //        if (flag == 0)
        //            propCity.Remove(city);
        //    }
        //    return propCity;
        //}
        /// <summary>
        /// This function used for get list of hotel By city and country.
        /// </summary>
        /// <param name="cityID"></param>
        /// <param name="countryID"></param>
        /// <returns>object</returns>
        public TList<Hotel> GetHotelByCity(int cityID, int countryID)
        {
            int Total = 0;
            string whereclause = "CityId=" + cityID + " and CountryId=" + countryID + " and IsActive=1 and GoOnline=1 and IsRemoved=0";
            string orderby = HotelColumn.Id + " ASC";
            propHotel = DataRepository.HotelProvider.GetPaged(whereclause, orderby, 0, int.MaxValue, out Total);
            return propHotel;
        }
        /// <summary>
        /// This function used for get center point of the city.
        /// </summary>
        /// <param name="cityID"></param>
        /// <returns></returns>
        public TList<MainPoint> GetCenterPoint(int cityID)
        {
            propCenterPoint = DataRepository.MainPointProvider.GetByCityId(cityID);

            return propCenterPoint;
        }
        #endregion
    }
}
