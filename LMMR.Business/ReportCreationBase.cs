﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Data;
using LMMR.Entities;

namespace LMMR.Business
{
    public abstract class ReportCreationBase
    {
        public ReportCreationBase()
        {

        }

        #region Variables

        #endregion

        #region Properties

        #endregion

        #region Methods
        public VList<ViewReportLeadTime> GetLeadTimeReport(string where)
        {
            int count = 0;
            string orderby = ViewReportLeadTimeColumn.CountryName + " ASC, " + ViewReportLeadTimeColumn.City + " ASC, " + ViewReportLeadTimeColumn.Name + " ASC, " + ViewReportLeadTimeColumn.AvailabilityDate + " ASC";
            return DataRepository.ViewReportLeadTimeProvider.GetPaged(where, orderby, 0, int.MaxValue, out count);
        }

        public VList<ViewReportOnLineInventoryReport> GetOnLineInventoryReport(string where)
        {
            int count = 0;
            string orderby = ViewReportOnLineInventoryReportColumn.CountryName + " ASC, " + ViewReportOnLineInventoryReportColumn.City + " ASC, " + ViewReportOnLineInventoryReportColumn.Name + " ASC, " + ViewReportLeadTimeColumn.AvailabilityDate + " ASC";
            return DataRepository.ViewReportOnLineInventoryReportProvider.GetPaged(where, orderby, 0, int.MaxValue, out count);
        }

        public VList<ViewReportAvailability> GetAvailabilityReport(string where)
        {
            int count = 0;
            string orderby = ViewReportAvailabilityColumn.CountryName + " ASC, " + ViewReportAvailabilityColumn.City + " ASC, " + ViewReportAvailabilityColumn.HotelName + " ASC, " + ViewReportAvailabilityColumn.AvailabilityDate + " ASC";
            return DataRepository.ViewReportAvailabilityProvider.GetPaged(where, orderby, 0, int.MaxValue, out count);
        }

        public VList<ViewReportCommissionControl> GetCommissionControlReport(string where)
        {
            int count = 0;
            string orderby = ViewReportCommissionControlColumn.CountryName + " ASC, " + ViewReportCommissionControlColumn.City + " ASC, " + ViewReportCommissionControlColumn.HotelName + " ASC, " + ViewReportCommissionControlColumn.BookingDate + " ASC";
            return DataRepository.ViewReportCommissionControlProvider.GetPaged(where, orderby, 0, int.MaxValue, out count);
        }


        public VList<ViewReportBookingRequestHistory> GetBookingRequestHistoryReport(string where)
        {
            int count = 0;

            string orderby = ViewReportBookingRequestHistoryColumn.CountryName + " ASC, " + ViewReportBookingRequestHistoryColumn.City + " ASC, " + ViewReportBookingRequestHistoryColumn.HotelName + " ASC, " + ViewReportBookingRequestHistoryColumn.BookingDate + " ASC";
            return DataRepository.ViewReportBookingRequestHistoryProvider.GetPaged(where, orderby, 0, int.MaxValue, out count);
        }

        public VList<ViewReportCancelation> GetCancelationReport(string where)
        {
            int count = 0;
            string orderby = ViewReportCancelationColumn.HotelName + " ASC ";
            return DataRepository.ViewReportCancelationProvider.GetPaged(where, orderby, 0, int.MaxValue, out count);
        }

        public VList<ViewReportOnLineInventoryReport> GetInvExistinghotelReport(string where)
        {
            int count = 0;
            string orderby = ViewReportOnLineInventoryReportColumn.CountryName + " ASC, " + ViewReportOnLineInventoryReportColumn.City + " ASC, " + ViewReportOnLineInventoryReportColumn.Name + " ASC, " + ViewReportOnLineInventoryReportColumn.AvailabilityDate + " ASC";
            return DataRepository.ViewReportOnLineInventoryReportProvider.GetPaged(where, orderby, 0, int.MaxValue, out count);
        }
        public VList<ViewreportInventoryReportofExistingHotel> GetInvExistinghotelReportmeeting(string where)
        {
            int count = 0;
            string orderby = ViewreportInventoryReportofExistingHotelColumn.Countryname + " ASC, " + ViewreportInventoryReportofExistingHotelColumn.City + " ASC, " + ViewreportInventoryReportofExistingHotelColumn.HotelName + " ASC";
            return DataRepository.ViewreportInventoryReportofExistingHotelProvider.GetPaged(where, orderby, 0, int.MaxValue, out count);
        }
        public TList<Hotel> GetInvAddedhotelReport(string where)
        {
            int count = 0;
            string orderby = HotelColumn.CountryId + " ASC, " + HotelColumn.CityId + " ASC, " + HotelColumn.Name + " ASC, " + HotelColumn.CreationDate + " ASC";
            return DataRepository.HotelProvider.GetPaged(where, orderby, 0, int.MaxValue, out count);
        }

        public VList<ViewReportProductionCompanyAgency> GetProductionReport(string where)
        {
            int count = 0;
            string orderby = ViewReportProductionCompanyAgencyColumn.CountryName + " ASC, " + ViewReportProductionCompanyAgencyColumn.CityName + " ASC, " + ViewReportProductionCompanyAgencyColumn.CompanyName + " ASC, " + ViewReportProductionCompanyAgencyColumn.Months + " ASC";
            return DataRepository.ViewReportProductionCompanyAgencyProvider.GetPaged(where, orderby, 0, int.MaxValue, out count);
        }

        public VList<ViewReportProductionReportHotel> GetProductionReportHotel(string where)
        {
            int count = 0;
            string orderby = ViewReportProductionReportHotelColumn.CountryName + " ASC, " + ViewReportProductionReportHotelColumn.City + " ASC, " + ViewReportProductionReportHotelColumn.HotelName + " ASC, " + ViewReportProductionReportHotelColumn.Months + " ASC";
            return DataRepository.ViewReportProductionReportHotelProvider.GetPaged(where, orderby, 0, int.MaxValue, out count);
        }
        public VList<ViewReportforProfileCompanyAgency> GetProfileCompanyAgencyReport(string where)
        {
            int count = 0;
            //string orderby = ViewReportforProfileCompanyAgencyColumn.CountryName + " ASC, " + ViewReportforProfileCompanyAgencyColumn.City + " ASC";
            return DataRepository.ViewReportforProfileCompanyAgencyProvider.GetPaged(where, string.Empty, 0, int.MaxValue, out count);
        }
        public VList<ViewReportRanking> GetRanking(string where)
        {
            int count = 0;
            //string orderby = ViewReportforProfileCompanyAgencyColumn.CountryName + " ASC, " + ViewReportforProfileCompanyAgencyColumn.City + " ASC";
            return DataRepository.ViewReportRankingProvider.GetPaged(where, string.Empty, 0, int.MaxValue, out count);
        }
        public TList<NewsLetterSubscriber> GetNewsletter(string where)
        {
            int count = 0;
            string orderby = NewsLetterSubscriberColumn.FirstName + " ASC, ";
            return DataRepository.NewsLetterSubscriberProvider.GetPaged(where, string.Empty, 0, int.MaxValue, out count);
        }
        #endregion
    }
}
