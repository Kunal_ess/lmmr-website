﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Collections;
using System.Net;
using LMMR.Entities;
using System.IO;
using LMMR.Data;
using System.Collections.ObjectModel;
using System.Xml;

namespace LMMR.Business
{
    public class SendMails
    {
        public string FromEmail { get; set; }
        public string ToEmail { get; set; }
        public string Bcc { get; set; }
        public string cc { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        private Dictionary<string, string> _dTemplateKeys;
        private List<Attachment> attachment = new List<Attachment>();
        public List<Attachment> Attachment
        {
            get
            {
                return attachment;
            }
        }
        public SendMails()
        {
            this._dTemplateKeys = new Dictionary<string, string>();
        }
        public SendMails(string subject, string Body, string attachment, string fromemail, string toemail)
        {
            FromEmail = fromemail;
            ToEmail = toemail;
            Subject = subject;
            // Attachment = attachment;
            this._dTemplateKeys = new Dictionary<string, string>();
        }
        public void Add(string strKey, string strValue)
        {
            this._dTemplateKeys.Add(strKey, strValue);
        }
        public string SendMailToCompany()
        {
            string strOutPut = "";
            if (!IsDomainExist())
            {
                if (SendingWelcomeMail() != "Not Valid")
                {
                    // Instantiate a new instance of MailMessage
                    MailMessage mMailMessage = new MailMessage();
                    if (FromEmail.Contains(","))
                    {
                        foreach (string sf in FromEmail.Split(','))
                        {
                            mMailMessage.From = new MailAddress(sf);
                        }
                    }
                    else
                    {
                        // Set the sender address of the mail message
                        mMailMessage.From = new MailAddress(FromEmail);
                    }
                    if (ToEmail.Contains(","))
                    {
                        foreach (string st in ToEmail.Split(','))
                        {
                            mMailMessage.To.Add(new MailAddress(st));
                        }
                    }
                    else
                    {
                        // Set the recepient address of the mail message
                        mMailMessage.To.Add(new MailAddress(ToEmail));
                    }
                    // Check if the bcc value is null or an empty string
                    if ((Bcc != null) && (Bcc != string.Empty))
                    {
                        // Set the Bcc address of the mail message
                        //mMailMessage.Bcc.Add(new MailAddress(Bcc));
                        if (Bcc.Contains(","))
                        {
                            foreach (string st in Bcc.Split(','))
                            {
                                mMailMessage.Bcc.Add(new MailAddress(st));
                            }
                        }
                        else
                        {
                            // Set the recepient address of the mail message
                            mMailMessage.Bcc.Add(new MailAddress(Bcc));
                        }
                    }
                    //mMailMessage.Bcc.Add(new MailAddress("gaurav-shrivastava@essindia.co.in"));
                    // Check if the cc value is null or an empty value
                    if ((cc != null) && (cc != string.Empty))
                    {
                        // Set the CC address of the mail message
                        //mMailMessage.CC.Add(new MailAddress(cc));
                        if (cc.Contains(","))
                        {
                            foreach (string st in cc.Split(','))
                            {
                                mMailMessage.CC.Add(new MailAddress(st));
                            }
                        }
                        else
                        {
                            // Set the recepient address of the mail message
                            mMailMessage.CC.Add(new MailAddress(cc));
                        }
                    }
                    // Set the subject of the mail message
                    mMailMessage.Subject = Subject;
                    //Check Attachment Provided
                    //if ((Attachment != null) && (Attachment != string.Empty))
                    //{
                    //    // Set attachment in mail
                    //    mMailMessage.Attachments.Add(new Attachment(Attachment));
                    //}
                    foreach (var data in attachment)
                    {
                        mMailMessage.Attachments.Add(data);
                    }
                    // Set the body of the mail message
                    foreach (KeyValuePair<string, string> strKey in this._dTemplateKeys)
                    {

                        Body = Body.Replace(strKey.Key, strKey.Value);

                    }
                    mMailMessage.Body = Body;

                    // Set the format of the mail message body as HTML
                    mMailMessage.IsBodyHtml = true;
                    // Set the priority of the mail message to normal
                    mMailMessage.Priority = MailPriority.High;

                    // Instantiate a new instance of SmtpClient
                    SmtpClient mSmtpClient = new SmtpClient();
                    // Send the mail message
                    mSmtpClient.Send(mMailMessage);
                    strOutPut = "Send Mail";
                }
                else
                {
                    SnedInfoToOperator();
                    strOutPut = "Email is not Valid";

                }
            }
            else
            {
                SnedInfoToOperator();
                strOutPut = "Email is not Valid";
            }

            return strOutPut;
        }
        public void SnedInfoToOperator()
        {
            //Send Mail to operator for this mail not exist.
            string strMailToOperator = string.Empty;
            //Send mail to the operator.
            Users objoperator = new HotelManager().GetFirstOperatorInDataBase();
            if (objoperator != null)
            {
                strMailToOperator = objoperator.EmailId;
                //strMailToOperator = "gaurav-shrivastava@essindia.co.in";
            }
            else
            {
                strMailToOperator = "gaurav-shrivastava@essindia.co.in";
            }

            // Instantiate a new instance of MailMessage
            MailMessage mMailMessage = new MailMessage();

            // Set the sender address of the mail message
            mMailMessage.From = new MailAddress(FromEmail);
            // Set the recepient address of the mail message
            mMailMessage.To.Add(new MailAddress(strMailToOperator));

            // Check if the bcc value is null or an empty string
            if ((Bcc != null) && (Bcc != string.Empty))
            {
                // Set the Bcc address of the mail message
                //mMailMessage.Bcc.Add(new MailAddress(Bcc));
                if (Bcc.Contains(","))
                {
                    foreach (string st in Bcc.Split(','))
                    {
                        mMailMessage.Bcc.Add(new MailAddress(st));
                    }
                }
                else
                {
                    // Set the recepient address of the mail message
                    mMailMessage.Bcc.Add(new MailAddress(Bcc));
                }
            }
            mMailMessage.Bcc.Add(new MailAddress("pranayesh-pathak@essindia.co.in"));
            // Check if the cc value is null or an empty value
            if ((cc != null) && (cc != string.Empty))
            {
                // Set the CC address of the mail message
                //mMailMessage.CC.Add(new MailAddress(cc));
                if (cc.Contains(","))
                {
                    foreach (string st in cc.Split(','))
                    {
                        mMailMessage.CC.Add(new MailAddress(st));
                    }
                }
                else
                {
                    // Set the recepient address of the mail message
                    mMailMessage.CC.Add(new MailAddress(cc));
                }
            }
            // Set the subject of the mail message
            mMailMessage.Subject = "email not valid";

            mMailMessage.Body = "This email is not valid " + ToEmail + "";

            // Set the format of the mail message body as HTML
            mMailMessage.IsBodyHtml = true;
            // Set the priority of the mail message to normal
            mMailMessage.Priority = MailPriority.High;

            // Instantiate a new instance of SmtpClient
            SmtpClient mSmtpClient = new SmtpClient();
            // Send the mail message
            mSmtpClient.Send(mMailMessage);
        }
        public string SendingWelcomeMail()
        {
            try
            {
                int total = 0;
                string whereClause = "EmailId=" + ToEmail + "IsActive=1";
                string orderby = string.Empty;
                TList<Users> obj = DataRepository.UsersProvider.GetPaged(whereClause, orderby, 0, int.MaxValue, out total);
                if (obj.Count < 0)
                {
                    // Instantiate a new instance of MailMessage
                    MailMessage mMailMessage = new MailMessage();

                    // Set the sender address of the mail message
                    mMailMessage.From = new MailAddress(FromEmail);
                    // Set the recepient address of the mail message
                    mMailMessage.To.Add(new MailAddress(ToEmail));

                    // Check if the bcc value is null or an empty string
                    if ((Bcc != null) && (Bcc != string.Empty))
                    {
                        if (Bcc.Contains(","))
                        {
                            foreach (string st in Bcc.Split(','))
                            {
                                mMailMessage.Bcc.Add(new MailAddress(st));
                            }
                        }
                        else
                        {
                            // Set the recepient address of the mail message
                            mMailMessage.Bcc.Add(new MailAddress(Bcc));
                        }
                        // Set the Bcc address of the mail message
                        //mMailMessage.Bcc.Add(new MailAddress(Bcc));
                    }
                    //mMailMessage.Bcc.Add(new MailAddress("gaurav-shrivastava@essindia.co.in"));
                    // Check if the cc value is null or an empty value
                    if ((cc != null) && (cc != string.Empty))
                    {
                        if (cc.Contains(","))
                        {
                            foreach (string st in cc.Split(','))
                            {
                                mMailMessage.CC.Add(new MailAddress(st));
                            }
                        }
                        else
                        {
                            // Set the recepient address of the mail message
                            mMailMessage.CC.Add(new MailAddress(cc));
                        }
                        // Set the CC address of the mail message
                        //mMailMessage.CC.Add(new MailAddress(cc));
                    }
                    // Set the subject of the mail message
                    mMailMessage.Subject = "Welcome";
                    //string body = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "EmailTemplets/JoinTodayForOperatorSuperAdmin.html");
                    mMailMessage.Body = "Last Minute Meetingroom";

                    // Set the format of the mail message body as HTML
                    mMailMessage.IsBodyHtml = true;
                    // Set the priority of the mail message to normal
                    mMailMessage.Priority = MailPriority.High;

                    // Instantiate a new instance of SmtpClient
                    SmtpClient mSmtpClient = new SmtpClient();
                    // Send the mail message
                    mSmtpClient.Send(mMailMessage);

                    return "Send Mail";
                }
                else
                {
                    return "Not valid";
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Mailbox unavailable"))
                {
                    return "Not Valid";
                }
                else
                {
                    return "Valid";
                }

            }
        }
        public bool IsDomainExist()
        {
            bool isEx = false;
            string[] strMail = ToEmail.Split('@');
            bool IsValid = false;
            string domain = strMail[1];
            string[] get = domain.Split('.');
            if (get[0].Contains("gmail"))
                isEx = true;
            if (get[0].Contains("yahoo"))
                isEx = true;
            if (get[0].Contains("live"))
                isEx = true;
            if (get[0].Contains("hotmail"))
                isEx = true;
            if (get[0].Contains("rediffmail"))
                isEx = true;
            if (get[0].Contains("ymail"))
                isEx = true;
            if (get[0].Contains("rockmail"))
                isEx = true;
            if (!isEx)
            {
                try
                {
                    string key = ToEmail.Split('@')[1];
                    string FilePath = AppDomain.CurrentDomain.BaseDirectory + "unwanteddomain.xml";
                    XmlDocument doc = new XmlDocument();
                    doc.Load(FilePath);
                    XmlNode nodes = doc.SelectSingleNode("domains/domain[@key='" + key + "']");
                    if (nodes != null)
                    {
                        //Dns.GetHostEntry(domain);
                        IsValid = true;
                    }
                    else
                    {
                        IsValid = false;
                    }
                }

                catch
                {
                    IsValid = false;
                }

            }
            else
            {
                IsValid = true;
            }
            return IsValid;
        }
        public string SendMail()
        {
            string strOutPut = "";

            if (SendingWelcomeMail() != "Not Valid")
            {
                // Instantiate a new instance of MailMessage
                MailMessage mMailMessage = new MailMessage();

                if (FromEmail.Contains(","))
                {
                    foreach (string sf in FromEmail.Split(','))
                    {
                        mMailMessage.From = new MailAddress(sf);
                    }
                }
                else
                {
                    // Set the sender address of the mail message
                    mMailMessage.From = new MailAddress(FromEmail);
                }
                if (ToEmail.Contains(","))
                {
                    foreach (string st in ToEmail.Split(','))
                    {
                        mMailMessage.To.Add(new MailAddress(st));
                    }
                }
                else
                {
                    // Set the recepient address of the mail message
                    mMailMessage.To.Add(new MailAddress(ToEmail));
                }

                // Check if the bcc value is null or an empty string
                if ((Bcc != null) && (Bcc != string.Empty))
                {
                    if (Bcc.Contains(","))
                    {
                        foreach (string st in Bcc.Split(','))
                        {
                            mMailMessage.Bcc.Add(new MailAddress(st));
                        }
                    }
                    else
                    {
                        // Set the recepient address of the mail message
                        mMailMessage.Bcc.Add(new MailAddress(Bcc));
                    }
                    // Set the Bcc address of the mail message
                    //mMailMessage.Bcc.Add(new MailAddress(Bcc));
                }
                //mMailMessage.Bcc.Add(new MailAddress("gaurav-shrivastava@essindia.co.in"));
                // Check if the cc value is null or an empty value
                if ((cc != null) && (cc != string.Empty))
                {
                    if (cc.Contains(","))
                    {
                        foreach (string st in cc.Split(','))
                        {
                            mMailMessage.CC.Add(new MailAddress(st));
                        }
                    }
                    else
                    {
                        // Set the recepient address of the mail message
                        mMailMessage.CC.Add(new MailAddress(cc));
                    }
                    // Set the CC address of the mail message
                    //mMailMessage.CC.Add(new MailAddress(cc));
                }
                // Set the subject of the mail message
                mMailMessage.Subject = Subject;
                //Check Attachment Provided
                //if ((Attachment != null) && (Attachment != string.Empty))
                //{
                //    // Set attachment in mail
                //    mMailMessage.Attachments.Add(new Attachment(Attachment));
                //}
                foreach (var data in attachment)
                {
                    mMailMessage.Attachments.Add(data);
                }
                // Set the body of the mail message
                foreach (KeyValuePair<string, string> strKey in this._dTemplateKeys)
                {
                    Body = Body.Replace(strKey.Key, strKey.Value);
                }
                mMailMessage.Body = Body;

                // Set the format of the mail message body as HTML
                mMailMessage.IsBodyHtml = true;
                // Set the priority of the mail message to normal
                mMailMessage.Priority = MailPriority.High;

                // Instantiate a new instance of SmtpClient
                SmtpClient mSmtpClient = new SmtpClient();
                // Send the mail message
                mSmtpClient.Send(mMailMessage);
                strOutPut = "Send Mail";
            }
            else
            {
                SnedInfoToOperator();
                strOutPut = "Email is not Valid";
            }

            return strOutPut;
        }
    }
}
