﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using LMMR.Entities;
using LMMR.Data;
using LMMR.Business;

namespace LMMR.Business
{   
    public abstract class CurrencyManagerBase
    {
        public CurrencyManagerBase()
        {
        }

        /// <summary>
        /// Get details of Currenct conversion from yahoo finance. Set to and From Currenct and Get the result of latest current running exchange.
        /// </summary>
        /// <param name="tocur"></param>
        /// <param name="fromCur"></param>
        /// <returns></returns>
        public static string Currency(string tocur, string fromCur)
        {
            try
            {
                string result;
                string url;
                StreamReader inStream;

                //url = "http://finance.yahoo.com/currency-converter/"+fromCur+"/"+tocur+"/"+amount+"";

                // url = "http://finance.yahoo.com/currency-converter/#from="+fromCur+";to="+tocur+";amt="+amount+"";

                url = "http://download.finance.yahoo.com/d/quotes.csv?s=" + tocur + fromCur + "=X&f=snl1d1t1ab";


                // prepare the web page we will be asking for
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

                // execute the request
                HttpWebResponse response = (HttpWebResponse)
                    request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    inStream = new StreamReader(response.GetResponseStream());
                    result = inStream.ReadToEnd();
                    result = result.Replace(Environment.NewLine, string.Empty);
                    result = result.Replace("\r", string.Empty);
                    string[] arrayResult = result.Split(',');
                    var required = arrayResult.Last();
                    return required;
                    //following code commented by vaibhav
                    //string strline = "";
                    //int x = 0;
                    //string str1 = null;
                    //string[] values = null;
                    //while (!inStream.EndOfStream)
                    //{
                    //    x++;
                    //    strline = inStream.ReadLine();
                    //    values = strline.Split(',');
                    //    foreach (string str in values)
                    //    {
                    //        str1 = str + ",";
                    //    }
                    //    str1 = str1.Trim(',');

                    //}

                    //return str1;
                }
                else
                {
                    return null;
                }
                //string[] return_array = result.Split('|');

                // double converted_amount =result[0];
                // string symbol_unicode =Convert.ToString( result[1]);
                // string currency_code =Convert.ToString( result[2]);


                // return converted_amount.ToString();
            }
            catch (Exception ex)
            {
                return "1";
            }
        }

        public Currency GetCurrencyDetailsByID(Int64 currencyID)
        {
            return DataRepository.CurrencyProvider.GetById(currencyID);
        }

        public Currency GetCurrencyDetailsByCountryID(Int64 countryID)
        {
            Country c = DataRepository.CountryProvider.GetById(countryID);
            return DataRepository.CurrencyProvider.GetById(c.CurrencyId);
        }

        public TList<Currency> GetAllCurrency()
        {
            return DataRepository.CurrencyProvider.GetAll();
        }


    }
}
