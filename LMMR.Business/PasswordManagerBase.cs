﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;
using System.Security.Cryptography;
using System.Configuration;
using System.IO;


namespace LMMR.Business
{
    public abstract class PasswordManagerBase
    {
        #region Variables
        /// <summary>
        /// Variables to store Message
        /// </summary>
        private string _varUpdateMessage;
        /// <summary>
        /// Variables to store Data
        /// </summary>
        private Users _varUserData;

        EmailConfigManager em = new EmailConfigManager();
        #endregion

        #region Properties

        /// <summary>
        /// Property To Store the TList of Entities.Hotal class
        /// </summary>
        public string propStrUpdateMessage
        {
            get { return _varUpdateMessage; }
            set { _varUpdateMessage = value; }
        }
        /// <summary>
        /// Property To Store the row of Entities.Hotal class
        /// </summary>
        public Users propUserData
        {
            get { return _varUserData; }
            set { _varUserData = value; }
        }

        #endregion

        #region Method
        /// <summary>
        /// Method for change password of the user.
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        public string UpdatePassword(int userID, string oldPassword, string newPassword)
        {
            TransactionManager tm = null;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                propUserData = DataRepository.UsersProvider.GetByUserId(userID);
                // verify user here.
                if (PasswordManager.Encrypt(oldPassword,true) == propUserData.Password)
                //if (oldPassword == propUserData.Password)
                {
                    propUserData.Password = PasswordManager.Encrypt(newPassword,true);
                    DataRepository.UsersProvider.Update(tm, propUserData);
                    propStrUpdateMessage = "Password update Successfully";
                }
                else
                {
                    propStrUpdateMessage = "Please enter correct old password";
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
                propStrUpdateMessage = "Error occured while changing password";
            }
            return propStrUpdateMessage;
        }

        /// <summary>
        /// Update the password at the time of activation of client. It is call only once.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static bool UpdateClientPasswordByEmail(string email, string password)
        {
            string whereclause = UsersColumn.EmailId + "='" + email + "' and " + UsersColumn.IsActive + "=1";
            int count = 0;
            
            TransactionManager tm = null;
            bool result = false;
            try
            {
                tm = DataRepository.Provider.CreateTransaction();
                tm.BeginTransaction();
                Users objUser = DataRepository.UsersProvider.GetPaged(tm, whereclause, string.Empty, 0, int.MaxValue, out count).FirstOrDefault();
                if (string.IsNullOrEmpty(objUser.Password))
                {
                    objUser.Password = PasswordManager.Encrypt(password, true);
                    if (DataRepository.UsersProvider.Update(tm, objUser))
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
                tm.Commit();
            }
            catch (Exception ex)
            {
                tm.Rollback();
                result = false;
            }
            return result;
        }

        public string[] ForgetPassword(string email)
        {
            int count = 0;
            string[] Message = new string[2];
            TList<Users> objUsers = DataRepository.UsersProvider.GetPaged(UsersColumn.EmailId + "='" + email+"'", string.Empty, 0, int.MaxValue, out count);
            DataRepository.UsersProvider.DeepLoad(objUsers);
            if (objUsers.Count > 0)
            {
                TransactionManager tm = null;
                try
                {
                    tm = DataRepository.Provider.CreateTransaction();
                    tm.BeginTransaction();
                    string genereatedPassword;
                    genereatedPassword = blRandomPassword.Generate(8, 10);
                    objUsers[0].Password = PasswordManager.Encrypt(genereatedPassword, true); ;
                    DataRepository.UsersProvider.Update(tm, objUsers[0]);
                    EmailConfig eConfig = em.GetByName("Forget password");
                    if (eConfig.IsActive)
                    {
                        string bodymsg = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/EmailTempletSample.html");
                        EmailConfigMapping emap = new EmailConfigMapping();
                        if (objUsers[0].UserDetailsCollection.Count <= 0)
                        {
                            emap = null;
                        }
                        else
                        {
                            emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == objUsers[0].UserDetailsCollection.FirstOrDefault().LanguageId).FirstOrDefault();
                        }
                        if (emap != null)
                        {
                            bodymsg = bodymsg.Replace("@CONTENTS", emap.EmailContents);
                        }
                        else
                        {
                            emap = eConfig.EmailConfigMappingCollection.Where(a => a.LanguageId == 1).FirstOrDefault();
                            bodymsg = bodymsg.Replace("@CONTENTS", emap.EmailContents);
                        }
                        SendMails objSendmail = new SendMails();
                        objSendmail.FromEmail = ConfigurationManager.AppSettings["AdminEMailID"];
                        objSendmail.ToEmail = objUsers[0].EmailId;
                        objSendmail.Bcc = ConfigurationManager.AppSettings["WatchEMailID"];
                        //objSendmail.Bcc = "gaurav-shrivastava@essindia.co.in";
                        //string bodymsg = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplets/ForgetPasswordTemplet.html");
                        EmailValueCollection objEmailValues = new EmailValueCollection();
                        objSendmail.Subject = "Last minute meeting room Password Recovery";
                        foreach (KeyValuePair<string, string> strKey in objEmailValues.EmailForChangePasswordDetails(objUsers[0].EmailId, genereatedPassword, "Last Minute Meeting Room Admin"))
                        {
                            bodymsg = bodymsg.Replace(strKey.Key, strKey.Value);
                        }
                        objSendmail.Body = bodymsg;
                        objSendmail.SendMail();
                    }
                    Message[0] = "Password successfully sent to your email";
                    Message[1] = "succesfuly";
                    tm.Commit();
                }
                catch
                {
                    Message[0] = "Error occured while generating password";
                    Message[1] = "error";
                    tm.Rollback();
                }
            }
            else
            {
                Message[0] = "This email address does not exist in database";
                Message[1] = "error";
            }
            return Message;
        }
        #endregion

        #region Encryption and decryption
        /// <summary>
        /// Created by Gaurav Shrivastava for Password Encryption
        /// </summary>
        /// <param name="toEncrypt"></param>
        /// <param name="useHashing"></param>
        /// <returns></returns>
        public static string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            System.Configuration.AppSettingsReader settingsReader = new System.Configuration.AppSettingsReader();
            // Get the key from config file

            string key = (string)settingsReader.GetValue("SecurityKey", typeof(String));

            //If hashing use get hashcode regards to your key
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                //Always release the resources and flush data of the Cryptographic service provide. Best Practice

                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there are other 4 modes.
            //We choose ECB(Electronic code Book)
            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)

            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            //transform the specified region of bytes array to resultArray
            byte[] resultArray =
              cTransform.TransformFinalBlock(toEncryptArray, 0,
              toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor
            tdes.Clear();
            //Return the encrypted data into unreadable string format
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        /// <summary>
        /// Created by Gaurav Shrivastava for Password decreption
        /// </summary>
        /// <param name="cipherString"></param>
        /// <param name="useHashing"></param>
        /// <returns></returns>
        public static string Decrypt(string cipherString, bool useHashing)
        {
            byte[] keyArray;
            //get the byte code of the string

            byte[] toEncryptArray = Convert.FromBase64String(cipherString);

            System.Configuration.AppSettingsReader settingsReader = new System.Configuration.AppSettingsReader();
            //Get your key from config file to open the lock!
            string key = (string)settingsReader.GetValue("SecurityKey", typeof(String));

            if (useHashing)
            {
                //if hashing was used get the hash code with regards to your key
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                //release any resource held by the MD5CryptoServiceProvider

                hashmd5.Clear();
            }
            else
            {
                //if hashing was not implemented get the byte code of the key
                keyArray = UTF8Encoding.UTF8.GetBytes(key);
            }

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there are other 4 modes. 
            //We choose ECB(Electronic code Book)

            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(
                                 toEncryptArray, 0, toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor                
            tdes.Clear();
            //return the Clear decrypted TEXT
            return UTF8Encoding.UTF8.GetString(resultArray);
        }
        #endregion
    }
}



/// <summary>
/// Summary description for RandomPassword
/// </summary>
public class blRandomPassword
{

    // Define default min and max password lengths.
    private static int DEFAULT_MIN_PASSWORD_LENGTH = 6;
    private static int DEFAULT_MAX_PASSWORD_LENGTH = 8;

    // Define supported password characters divided into groups.
    // You can add (or remove) characters to (from) these groups.
    private static string PASSWORD_CHARS_LCASE = "abcdefgijkmnopqrstwxyz";
    private static string PASSWORD_CHARS_UCASE = "ABCDEFGHJKLMNPQRSTWXYZ";
    private static string PASSWORD_CHARS_NUMERIC = "23456789";
    private static string PASSWORD_CHARS_SPECIAL = "-_";

    /// <summary>
    /// Generates a random password.
    /// </summary>
    /// <returns>
    /// Randomly generated password.
    /// </returns>
    /// <remarks>
    /// The length of the generated password will be determined at
    /// random. It will be no shorter than the minimum default and
    /// no longer than maximum default.
    /// </remarks>
    public static string Generate()
    {
        return Generate(DEFAULT_MIN_PASSWORD_LENGTH,
                        DEFAULT_MAX_PASSWORD_LENGTH);
    }

    /// <summary>
    /// Generates a random password of the exact length.
    /// </summary>
    /// <param name="length">
    /// Exact password length.
    /// </param>
    /// <returns>
    /// Randomly generated password.
    /// </returns>
    public static string Generate(int length)
    {
        return Generate(length, length);
    }

    /// <summary>
    /// Generates a random password.
    /// </summary>
    /// <param name="minLength">
    /// Minimum password length.
    /// </param>
    /// <param name="maxLength">
    /// Maximum password length.
    /// </param>
    /// <returns>
    /// Randomly generated password.
    /// </returns>
    /// <remarks>
    /// The length of the generated password will be determined at
    /// random and it will fall with the range determined by the
    /// function parameters.
    /// </remarks>
    public static string Generate(int minLength,
                                  int maxLength)
    {
        // Make sure that input parameters are valid.
        if (minLength <= 0 || maxLength <= 0 || minLength > maxLength)
            return null;

        // Create a local array containing supported password characters
        // grouped by types. You can remove character groups from this
        // array, but doing so will weaken the password strength.
        char[][] charGroups = new char[][] 
        {
            PASSWORD_CHARS_LCASE.ToCharArray(),
            PASSWORD_CHARS_UCASE.ToCharArray(),
            PASSWORD_CHARS_NUMERIC.ToCharArray(),
            PASSWORD_CHARS_SPECIAL.ToCharArray()
        };

        // Use this array to track the number of unused characters in each
        // character group.
        int[] charsLeftInGroup = new int[charGroups.Length];

        // Initially, all characters in each group are not used.
        for (int i = 0; i < charsLeftInGroup.Length; i++)
            charsLeftInGroup[i] = charGroups[i].Length;

        // Use this array to track (iterate through) unused character groups.
        int[] leftGroupsOrder = new int[charGroups.Length];

        // Initially, all character groups are not used.
        for (int i = 0; i < leftGroupsOrder.Length; i++)
            leftGroupsOrder[i] = i;

        // Because we cannot use the default randomizer, which is based on the
        // current time (it will produce the same "random" number within a
        // second), we will use a random number generator to seed the
        // randomizer.

        // Use a 4-byte array to fill it with random bytes and convert it then
        // to an integer value.
        byte[] randomBytes = new byte[4];

        // Generate 4 random bytes.
        RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
        rng.GetBytes(randomBytes);

        // Convert 4 bytes into a 32-bit integer value.
        int seed = (randomBytes[0] & 0x7f) << 24 |
                    randomBytes[1] << 16 |
                    randomBytes[2] << 8 |
                    randomBytes[3];

        // Now, this is real randomization.
        Random random = new Random(seed);

        // This array will hold password characters.
        char[] password = null;

        // Allocate appropriate memory for the password.
        if (minLength < maxLength)
            password = new char[random.Next(minLength, maxLength + 1)];
        else
            password = new char[minLength];

        // Index of the next character to be added to password.
        int nextCharIdx;

        // Index of the next character group to be processed.
        int nextGroupIdx;

        // Index which will be used to track not processed character groups.
        int nextLeftGroupsOrderIdx;

        // Index of the last non-processed character in a group.
        int lastCharIdx;

        // Index of the last non-processed group.
        int lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1;

        // Generate password characters one at a time.
        for (int i = 0; i < password.Length; i++)
        {
            // If only one character group remained unprocessed, process it;
            // otherwise, pick a random character group from the unprocessed
            // group list. To allow a special character to appear in the
            // first position, increment the second parameter of the Next
            // function call by one, i.e. lastLeftGroupsOrderIdx + 1.
            if (lastLeftGroupsOrderIdx == 0)
                nextLeftGroupsOrderIdx = 0;
            else
                nextLeftGroupsOrderIdx = random.Next(0,
                                                     lastLeftGroupsOrderIdx);

            // Get the actual index of the character group, from which we will
            // pick the next character.
            nextGroupIdx = leftGroupsOrder[nextLeftGroupsOrderIdx];

            // Get the index of the last unprocessed characters in this group.
            lastCharIdx = charsLeftInGroup[nextGroupIdx] - 1;

            // If only one unprocessed character is left, pick it; otherwise,
            // get a random character from the unused character list.
            if (lastCharIdx == 0)
                nextCharIdx = 0;
            else
                nextCharIdx = random.Next(0, lastCharIdx + 1);

            // Add this character to the password.
            password[i] = charGroups[nextGroupIdx][nextCharIdx];

            // If we processed the last character in this group, start over.
            if (lastCharIdx == 0)
                charsLeftInGroup[nextGroupIdx] =
                                          charGroups[nextGroupIdx].Length;
            // There are more unprocessed characters left.
            else
            {
                // Swap processed character with the last unprocessed character
                // so that we don't pick it until we process all characters in
                // this group.
                if (lastCharIdx != nextCharIdx)
                {
                    char temp = charGroups[nextGroupIdx][lastCharIdx];
                    charGroups[nextGroupIdx][lastCharIdx] =
                                charGroups[nextGroupIdx][nextCharIdx];
                    charGroups[nextGroupIdx][nextCharIdx] = temp;
                }
                // Decrement the number of unprocessed characters in
                // this group.
                charsLeftInGroup[nextGroupIdx]--;
            }

            // If we processed the last group, start all over.
            if (lastLeftGroupsOrderIdx == 0)
                lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1;
            // There are more unprocessed groups left.
            else
            {
                // Swap processed group with the last unprocessed group
                // so that we don't pick it until we process all groups.
                if (lastLeftGroupsOrderIdx != nextLeftGroupsOrderIdx)
                {
                    int temp = leftGroupsOrder[lastLeftGroupsOrderIdx];
                    leftGroupsOrder[lastLeftGroupsOrderIdx] =
                                leftGroupsOrder[nextLeftGroupsOrderIdx];
                    leftGroupsOrder[nextLeftGroupsOrderIdx] = temp;
                }
                // Decrement the number of unprocessed groups.
                lastLeftGroupsOrderIdx--;
            }
        }

        // Convert password characters into a string and return the result.
        return new string(password);
    }
}