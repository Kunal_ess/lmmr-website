﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LMMR.Entities;
using LMMR.Data;

namespace LMMR.Business
{
    public abstract class ManageOthersBase
    {
        #region Functions

        /// <summary>
        /// This function used to get data by country ID.
        /// </summary>
        /// <param name="intCountry"></param>
        /// <returns></returns>
        public TList<Others> GetInfoByCountryID(int intCountry)
        {
            TList<Others> obj = DataRepository.OthersProvider.GetByCountryId(intCountry);
            return obj;
        }

        /// <summary>
        /// This function used to get all data.
        /// </summary>
        /// <returns></returns>
        public TList<Others> GetAll()
        {
            TList<Others> obj = DataRepository.OthersProvider.GetAll();
            return obj;
        }

        /// <summary>
        /// This function used for get data by ID
        /// </summary>
        /// <param name="intId"></param>
        /// <returns></returns>
        public Others GetByID(int intId)
        {
            Others obj = DataRepository.OthersProvider.GetById(intId);
            return obj;
        }

        /// <summary>
        /// This function used to insert others info.
        /// </summary>
        /// <param name="objOthers"></param>
        /// <returns></returns>
        public bool InserOtherInfo(Others objOthers)
        {
            bool result = false;
            if (DataRepository.OthersProvider.Insert(objOthers))
            {
                result = true;
            }
            return result;
        }

        /// <summary>
        /// This function used to get all language.
        /// </summary>
        /// <returns></returns>
        public TList<Language> GetAllLanguage()
        {
            int Total = 0;
            string whereclause = "IsActive=1";
            TList<Language> obj = DataRepository.LanguageProvider.GetPaged(whereclause, String.Empty, 0, int.MaxValue, out Total);
            return obj;
        }
        public TList<Language> GetLanguages()
        {
            TList<Language> obj = DataRepository.LanguageProvider.GetAll();
            return obj;
        }

        /// <summary>
        /// This function used to get all language.
        /// </summary>
        /// <returns></returns>
        public TList<Currency> GetAllCurrency()
        {
            int Total = 0;
            string whereclause = string.Empty;
            TList<Currency> obj = DataRepository.CurrencyProvider.GetPaged(whereclause, String.Empty, 0, int.MaxValue, out Total);
            return obj;
        }

        /// <summary>
        /// This Function used to get currency by ID.
        /// </summary>
        /// <param name="CurrencyID"></param>
        /// <returns></returns>
        public string GetCurrencyByID(int CurrencyID)
        {
            string currency = Convert.ToString(DataRepository.CurrencyProvider.GetById(CurrencyID).Currency);
            return currency;
        }

        /// <summary>
        /// This function used to get county languages.
        /// </summary>
        /// <param name="CurrencyID"></param>
        /// <returns></returns>
        public TList<CountryLanguage> GetLanguageByCountryID(int CurrencyID)
        {
            TList<CountryLanguage> Obj = DataRepository.CountryLanguageProvider.GetByCountryId(CurrencyID);
            return Obj;
        }

        public bool UpdateOthersInfo(Others objothers)
        {
            bool isResult = false;
            if (DataRepository.OthersProvider.Update(objothers))
            {
                isResult = true;
            }

            return isResult;
        }


        /// <summary>
        /// This function used for delete question by countryID.
        /// </summary>
        /// <param name="CountryID"></param>
        /// <returns></returns>
        public bool ClearQuestion(int CountryID)
        {
            bool isSuccess = false;
            TList<HearUsQuestion> objHear = DataRepository.HearUsQuestionProvider.GetByCountryId(CountryID);
            if (objHear.Count > 0)
            {
                int i = 0;
                for (i = 0; i <= objHear.Count - 1; i++)
                {
                    DataRepository.HearUsQuestionProvider.Delete(objHear[i].Id);
                }
                isSuccess = true;
            }
            else
            {
                isSuccess = true;
            }
            return isSuccess;
        }

        /// <summary>
        /// This function used inser question.
        /// </summary>
        /// <param name="objHearFromUs"></param>
        /// <returns></returns>
        public bool InsertQuestion(HearUsQuestion objHearFromUs)
        {
            bool isSuccess = false;

            if (DataRepository.HearUsQuestionProvider.Insert(objHearFromUs))
            {
                isSuccess = true;
            }
            return isSuccess;
        }
        /// <summary>
        /// This function used to get all quetion by country.
        /// </summary>
        /// <param name="countryID"></param>
        /// <returns></returns>
        public TList<HearUsQuestion> GetAllQuestion(int countryID)
        {
            TList<HearUsQuestion> obj = DataRepository.HearUsQuestionProvider.GetByCountryId(countryID);

            return obj;
        }

        /// <summary>
        /// This function used to clean old maping.
        /// </summary>
        /// <param name="countryID"></param>
        /// <returns></returns>
        public bool ClearOldLanguageMapping(int countryID)
        {
            bool isSuccess = false;
            TList<CountryLanguage> objLanguage = DataRepository.CountryLanguageProvider.GetByCountryId(countryID);
            if (objLanguage.Count > 0)
            {
                int i = 0;
                for (i = 0; i <= objLanguage.Count - 1; i++)
                {
                    DataRepository.CountryLanguageProvider.Delete(objLanguage[i].Id);
                }
                isSuccess = true;
            }
            else
            {
                isSuccess = true;
            }
            return isSuccess;
        }

        /// <summary>
        /// This function used to insert new maping.
        /// </summary>
        /// <param name="ObjcountryLanguage"></param>
        /// <returns></returns>
        public bool InsertLanguageMapping(CountryLanguage ObjcountryLanguage)
        {
            bool isSuccess = false;
            if (DataRepository.CountryLanguageProvider.Insert(ObjcountryLanguage))
            {
                isSuccess = true;
            }
            return isSuccess;
        }

        public Others GetApplicationOthers()
        {
            Others obj = DataRepository.OthersProvider.GetAll().Find(a => a.CountryId == null);

            return obj;
        }

        /// <summary>
        /// Bind all active hotel for set invoice section
        /// </summary>
        /// <param name="ClientId"></param>
        /// <returns></returns>
        public TList<Hotel> GetAllActiveHotel(int CountryId)
        {
            string whereclause = HotelColumn.IsRemoved + "=0" + " and " + "IsActive=1" + " and " + "GoOnline=1" + " and " + "CountryId=" + CountryId;
            string Orderby = HotelColumn.Name + " DESC";
            int hotelcount = 0;
            TList<Hotel> objHotels = DataRepository.HotelProvider.GetPaged(whereclause, Orderby, 0, int.MaxValue, out hotelcount);
            return objHotels;
        }

        /// <summary>
        /// Bind all active hotel for set invoice section
        /// </summary>
        /// <param name="ClientId"></param>
        /// <returns></returns>
        public TList<InvoiceCommission> GetInvoiceCommission()
        {
            TList<InvoiceCommission> objCommission = DataRepository.InvoiceCommissionProvider.GetAll();
            return objCommission;
        }

        /// <summary>
        /// Check validation for not enter duplicate hotel in invoice commission table
        /// </summary>
        /// <param name="HotelId"></param>
        /// <returns></returns>
        public TList<InvoiceCommPercentage> CheckInvoiceCommissionByHotelId(int HotelId)
        {
            string whereclause = "HotelId=" + HotelId;            
            int hotelcount = 0;
            TList<InvoiceCommPercentage> objCommission = DataRepository.InvoiceCommPercentageProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out hotelcount);
            return objCommission;
        }


        /// <summary>
        /// Get all invoice commission details
        /// </summary>
        /// <param name="ClientId"></param>
        /// <returns></returns>
        public VList<ViewForSetInvoice> GetCommissionDetails()
        {
            VList<ViewForSetInvoice> Vlist = new VList<ViewForSetInvoice>();
            string Orderby = ViewForSetInvoiceColumn.CommPernId + " DESC";
            int hotelcount = 0;
            Vlist = DataRepository.ViewForSetInvoiceProvider.GetPaged(string.Empty, Orderby, 0, int.MaxValue, out hotelcount);
            return Vlist;
        }

        /// <summary>
        /// Get invoice commission details by Hotel id
        /// </summary>
        /// <param name="HotelId"></param>
        /// <returns></returns>
        public VList<ViewForSetInvoice> GetCommissionDetailsForHotelId(int Hotelid)
        {
            string whereclause = "HotelId=" + Hotelid;
            VList<ViewForSetInvoice> Vlist = new VList<ViewForSetInvoice>();
            int hotelcount = 0;
            Vlist = DataRepository.ViewForSetInvoiceProvider.GetPaged(whereclause, string.Empty, 0, int.MaxValue, out hotelcount);
            return Vlist;
        }



        /// <summary>
        /// Set new commission percentage for hotel id
        /// </summary>        
        public string RegisterNewCommission(InvoiceCommPercentage newComm)
        {
            TransactionManager transaction = null;
            try
            {
                transaction = DataRepository.Provider.CreateTransaction();
                transaction.BeginTransaction();
                if ((DataRepository.InvoiceCommPercentageProvider.Insert(transaction, newComm)))
                {
                    
                }
                else
                {
                    return "Information could not be saved.";
                }
                transaction.Commit();
                return "New commission has been created";
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return "Information could not be saved.Please contact Administrator.";
            }
        }

        /// <summary>
        /// Delete commission details as per hotel id
        /// </summary>                
        public bool DeleteCommission(int Hotelid)
        {
            bool status = false;
            status = DataRepository.InvoiceCommPercentageProvider.Delete(Hotelid);
            return status;
        }
        #endregion
    }
}
