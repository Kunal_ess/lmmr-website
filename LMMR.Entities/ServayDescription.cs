﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'ServayDescription' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class ServayDescription : ServayDescriptionBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="ServayDescription"/> instance.
		///</summary>
		public ServayDescription():base(){}	
		
		#endregion
	}
}
