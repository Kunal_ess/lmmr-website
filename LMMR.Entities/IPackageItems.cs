﻿using System;
using System.ComponentModel;

namespace LMMR.Entities
{
	/// <summary>
	///		The data structure representation of the 'PackageItems' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IPackageItems 
	{
		/// <summary>			
		/// Id : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "PackageItems"</remarks>
		System.Int64 Id { get; set; }
				
		
		
		/// <summary>
		/// ItemType : 0 for Foodbev, 1 for other, 2 for Equipment
		/// </summary>
		System.String  ItemType  { get; set; }
		
		/// <summary>
		/// ItemName : 
		/// </summary>
		System.String  ItemName  { get; set; }
		
		/// <summary>
		/// VAT : 
		/// </summary>
		System.Decimal?  Vat  { get; set; }
		
		/// <summary>
		/// IsActive : 
		/// </summary>
		System.Boolean  IsActive  { get; set; }
		
		/// <summary>
		/// CreatedDate : 
		/// </summary>
		System.DateTime?  CreatedDate  { get; set; }
		
		/// <summary>
		/// CountryId : 
		/// </summary>
		System.Int64  CountryId  { get; set; }
		
		/// <summary>
		/// IsExtra : 
		/// </summary>
		System.Boolean  IsExtra  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _packageByHotelItemId
		/// </summary>	
		TList<PackageByHotel> PackageByHotelCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _packageItemMappingItemId
		/// </summary>	
		TList<PackageItemMapping> PackageItemMappingCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _packageDescriptionItemId
		/// </summary>	
		TList<PackageDescription> PackageDescriptionCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _buildMeetingConfigurePackageId
		/// </summary>	
		TList<BuildMeetingConfigure> BuildMeetingConfigureCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _buildPackageConfigureDescSectionDescId
		/// </summary>	
		TList<BuildPackageConfigureDesc> BuildPackageConfigureDescCollection {  get;  set;}	

		#endregion Data Properties

	}
}


