﻿using System;
using System.ComponentModel;

namespace LMMR.Entities
{
	/// <summary>
	///		The data structure representation of the 'MeetingRoom' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IMeetingRoom 
	{
		/// <summary>			
		/// Id : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "MeetingRoom"</remarks>
		System.Int64 Id { get; set; }
				
		
		
		/// <summary>
		/// Hotel_Id : 
		/// </summary>
		System.Int64  HotelId  { get; set; }
		
		/// <summary>
		/// Name : 
		/// </summary>
		System.String  Name  { get; set; }
		
		/// <summary>
		/// Picture : 
		/// </summary>
		System.String  Picture  { get; set; }
		
		/// <summary>
		/// DayLight : 
		/// </summary>
		System.Int32  DayLight  { get; set; }
		
		/// <summary>
		/// Surface : 
		/// </summary>
		System.Int64?  Surface  { get; set; }
		
		/// <summary>
		/// Height : 
		/// </summary>
		System.Decimal?  Height  { get; set; }
		
		/// <summary>
		/// IsActive : 
		/// </summary>
		System.Boolean  IsActive  { get; set; }
		
		/// <summary>
		/// MR_Plan : 
		/// </summary>
		System.String  MrPlan  { get; set; }
		
		/// <summary>
		/// OrderNumber : 
		/// </summary>
		System.Int32?  OrderNumber  { get; set; }
		
		/// <summary>
		/// HalfdayPrice : 
		/// </summary>
		System.Decimal?  HalfdayPrice  { get; set; }
		
		/// <summary>
		/// FulldayPrice : 
		/// </summary>
		System.Decimal?  FulldayPrice  { get; set; }
		
		/// <summary>
		/// IsOnline : 
		/// </summary>
		System.Boolean  IsOnline  { get; set; }
		
		/// <summary>
		/// IsDeleted : 1 for deleted
		/// </summary>
		System.Boolean  IsDeleted  { get; set; }
		
		/// <summary>
		/// UpdatedBy : 
		/// </summary>
		System.Int64?  UpdatedBy  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _meetingRoomDescMeetingRoomId
		/// </summary>	
		TList<MeetingRoomDesc> MeetingRoomDescCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _meetingRoomConfigMeetingRoomId
		/// </summary>	
		TList<MeetingRoomConfig> MeetingRoomConfigCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _spandPmeetingRoomMeetingRoomId
		/// </summary>	
		TList<SpandPmeetingRoom> SpandPmeetingRoomCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _bookingMainMeetingRoomId
		/// </summary>	
		TList<Booking> BookingCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _bookedMeetingRoomMeetingRoomId
		/// </summary>	
		TList<BookedMeetingRoom> BookedMeetingRoomCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _meetingRoomPictureVideoMeetingRoomId
		/// </summary>	
		TList<MeetingRoomPictureVideo> MeetingRoomPictureVideoCollection {  get;  set;}	

		#endregion Data Properties

	}
}


