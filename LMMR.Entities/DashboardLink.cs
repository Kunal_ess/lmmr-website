﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'DashboardLink' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class DashboardLink : DashboardLinkBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="DashboardLink"/> instance.
		///</summary>
		public DashboardLink():base(){}	
		
		#endregion
	}
}
