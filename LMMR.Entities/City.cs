﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'City' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class City : CityBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="City"/> instance.
		///</summary>
		public City():base(){}	
		
		#endregion
	}
}
