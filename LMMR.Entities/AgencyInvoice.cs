﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'AgencyInvoice' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class AgencyInvoice : AgencyInvoiceBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="AgencyInvoice"/> instance.
		///</summary>
		public AgencyInvoice():base(){}	
		
		#endregion
	}
}
