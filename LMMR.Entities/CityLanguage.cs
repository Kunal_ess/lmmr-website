﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'CityLanguage' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class CityLanguage : CityLanguageBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="CityLanguage"/> instance.
		///</summary>
		public CityLanguage():base(){}	
		
		#endregion
	}
}
