﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'NewsLetterSubscriber' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class NewsLetterSubscriber : NewsLetterSubscriberBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="NewsLetterSubscriber"/> instance.
		///</summary>
		public NewsLetterSubscriber():base(){}	
		
		#endregion
	}
}
