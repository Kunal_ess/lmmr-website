﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'HotelFacilities_trail' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class HotelFacilitiesTrail : HotelFacilitiesTrailBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="HotelFacilitiesTrail"/> instance.
		///</summary>
		public HotelFacilitiesTrail():base(){}	
		
		#endregion
	}
}
