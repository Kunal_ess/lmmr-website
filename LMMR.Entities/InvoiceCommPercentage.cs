﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'InvoiceCommPercentage' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class InvoiceCommPercentage : InvoiceCommPercentageBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="InvoiceCommPercentage"/> instance.
		///</summary>
		public InvoiceCommPercentage():base(){}	
		
		#endregion
	}
}
