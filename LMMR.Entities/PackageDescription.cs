﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'PackageDescription' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class PackageDescription : PackageDescriptionBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="PackageDescription"/> instance.
		///</summary>
		public PackageDescription():base(){}	
		
		#endregion
	}
}
