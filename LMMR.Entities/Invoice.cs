﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'Invoice' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class Invoice : InvoiceBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="Invoice"/> instance.
		///</summary>
		public Invoice():base(){}	
		
		#endregion
	}
}
