﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'DBAudit' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class DbAudit : DbAuditBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="DbAudit"/> instance.
		///</summary>
		public DbAudit():base(){}	
		
		#endregion
	}
}
