﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'Availability_trail' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class AvailabilityTrail : AvailabilityTrailBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="AvailabilityTrail"/> instance.
		///</summary>
		public AvailabilityTrail():base(){}	
		
		#endregion
	}
}
