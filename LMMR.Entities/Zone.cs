﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'Zone' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class Zone : ZoneBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="Zone"/> instance.
		///</summary>
		public Zone():base(){}	
		
		#endregion
	}
}
