﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'RankingAlgo_Master' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class RankingAlgoMaster : RankingAlgoMasterBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="RankingAlgoMaster"/> instance.
		///</summary>
		public RankingAlgoMaster():base(){}	
		
		#endregion
	}
}
