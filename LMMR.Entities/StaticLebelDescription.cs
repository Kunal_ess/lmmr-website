﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'StaticLebelDescription' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class StaticLebelDescription : StaticLebelDescriptionBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="StaticLebelDescription"/> instance.
		///</summary>
		public StaticLebelDescription():base(){}	
		
		#endregion
	}
}
