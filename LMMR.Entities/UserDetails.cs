﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'UserDetails' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class UserDetails : UserDetailsBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="UserDetails"/> instance.
		///</summary>
		public UserDetails():base(){}	
		
		#endregion
	}
}
