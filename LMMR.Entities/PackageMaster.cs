﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'PackageMaster' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class PackageMaster : PackageMasterBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="PackageMaster"/> instance.
		///</summary>
		public PackageMaster():base(){}	
		
		#endregion
	}
}
