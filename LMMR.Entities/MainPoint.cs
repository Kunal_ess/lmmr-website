﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'MainPoint' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class MainPoint : MainPointBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="MainPoint"/> instance.
		///</summary>
		public MainPoint():base(){}	
		
		#endregion
	}
}
