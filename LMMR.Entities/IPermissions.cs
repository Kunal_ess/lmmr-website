﻿using System;
using System.ComponentModel;

namespace LMMR.Entities
{
	/// <summary>
	///		The data structure representation of the 'Permissions' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IPermissions 
	{
		/// <summary>			
		/// Id : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "Permissions"</remarks>
		System.Int64 Id { get; set; }
				
		
		
		/// <summary>
		/// RoleId : 
		/// </summary>
		System.Int64?  RoleId  { get; set; }
		
		/// <summary>
		/// IsAdd : 
		/// </summary>
		System.Boolean  IsAdd  { get; set; }
		
		/// <summary>
		/// IsUpdate : 
		/// </summary>
		System.Boolean  IsUpdate  { get; set; }
		
		/// <summary>
		/// IsDelete : 
		/// </summary>
		System.Boolean  IsDelete  { get; set; }
		
		/// <summary>
		/// Condition : Set permissions using Enum from code.
		/// </summary>
		System.Int64?  Condition  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}


