﻿using System;
using System.ComponentModel;

namespace LMMR.Entities
{
	/// <summary>
	///		The data structure representation of the 'WhiteLabelMappingWithHotel' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IWhiteLabelMappingWithHotel 
	{
		/// <summary>			
		/// Id : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "WhiteLabelMappingWithHotel"</remarks>
		System.Int64 Id { get; set; }
				
		
		
		/// <summary>
		/// WhiteLabelId : 
		/// </summary>
		System.Int64  WhiteLabelId  { get; set; }
		
		/// <summary>
		/// HotelId : 
		/// </summary>
		System.Int64  HotelId  { get; set; }
		
		/// <summary>
		/// GroupId : 
		/// </summary>
		System.Int64  GroupId  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}


