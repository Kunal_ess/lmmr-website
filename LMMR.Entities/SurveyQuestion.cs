﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'survey_Question' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class SurveyQuestion : SurveyQuestionBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="SurveyQuestion"/> instance.
		///</summary>
		public SurveyQuestion():base(){}	
		
		#endregion
	}
}
