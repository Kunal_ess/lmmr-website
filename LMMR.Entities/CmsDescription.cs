﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'CMSDescription' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class CmsDescription : CmsDescriptionBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="CmsDescription"/> instance.
		///</summary>
		public CmsDescription():base(){}	
		
		#endregion
	}
}
