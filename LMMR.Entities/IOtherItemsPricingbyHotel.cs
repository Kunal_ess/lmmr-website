﻿using System;
using System.ComponentModel;

namespace LMMR.Entities
{
	/// <summary>
	///		The data structure representation of the 'OtherItemsPricingbyHotel' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IOtherItemsPricingbyHotel 
	{
		/// <summary>			
		/// Id : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "OtherItemsPricingbyHotel"</remarks>
		System.Int64 Id { get; set; }
				
		
		
		/// <summary>
		/// SectionDescId : 
		/// </summary>
		System.Int64  SectionDescId  { get; set; }
		
		/// <summary>
		/// HotelId : 
		/// </summary>
		System.Int64  HotelId  { get; set; }
		
		/// <summary>
		/// ItemId : 
		/// </summary>
		System.Int64  ItemId  { get; set; }
		
		/// <summary>
		/// HalfdayPrice : 
		/// </summary>
		System.Decimal?  HalfdayPrice  { get; set; }
		
		/// <summary>
		/// FulldayPrice : 
		/// </summary>
		System.Decimal?  FulldayPrice  { get; set; }
		
		/// <summary>
		/// IsComplementry : 
		/// </summary>
		System.Boolean  IsComplementry  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}


