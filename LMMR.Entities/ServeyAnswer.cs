﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'ServeyAnswer' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class ServeyAnswer : ServeyAnswerBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="ServeyAnswer"/> instance.
		///</summary>
		public ServeyAnswer():base(){}	
		
		#endregion
	}
}
