﻿using System;
using System.ComponentModel;

namespace LMMR.Entities
{
	/// <summary>
	///		The data structure representation of the 'OtherDescriptionLanguage' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IOtherDescriptionLanguage 
	{
		/// <summary>			
		/// Id : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "OtherDescriptionLanguage"</remarks>
		System.Int64 Id { get; set; }
				
		
		
		/// <summary>
		/// OthersId : 
		/// </summary>
		System.Int64  OthersId  { get; set; }
		
		/// <summary>
		/// LanguageId : 
		/// </summary>
		System.Int64  LanguageId  { get; set; }
		
		/// <summary>
		/// Description : 
		/// </summary>
		System.String  Description  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}


