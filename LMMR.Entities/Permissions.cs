﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'Permissions' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class Permissions : PermissionsBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="Permissions"/> instance.
		///</summary>
		public Permissions():base(){}	
		
		#endregion
	}
}
