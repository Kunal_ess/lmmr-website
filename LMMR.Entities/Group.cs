﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'Group' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class Group : GroupBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="Group"/> instance.
		///</summary>
		public Group():base(){}	
		
		#endregion
	}
}
