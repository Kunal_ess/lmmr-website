﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'Booking' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class Booking : BookingBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="Booking"/> instance.
		///</summary>
		public Booking():base(){}	
		
		#endregion
	}
}
