﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'BookedBedRoom' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class BookedBedRoom : BookedBedRoomBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="BookedBedRoom"/> instance.
		///</summary>
		public BookedBedRoom():base(){}	
		
		#endregion
	}
}
