﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'BuildPackageConfigure' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class BuildPackageConfigure : BuildPackageConfigureBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="BuildPackageConfigure"/> instance.
		///</summary>
		public BuildPackageConfigure():base(){}	
		
		#endregion
	}
}
