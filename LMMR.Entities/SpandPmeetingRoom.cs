﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'SPandPmeetingRoom' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class SpandPmeetingRoom : SpandPmeetingRoomBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="SpandPmeetingRoom"/> instance.
		///</summary>
		public SpandPmeetingRoom():base(){}	
		
		#endregion
	}
}
