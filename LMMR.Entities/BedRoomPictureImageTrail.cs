﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'BedRoomPictureImage_Trail' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class BedRoomPictureImageTrail : BedRoomPictureImageTrailBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="BedRoomPictureImageTrail"/> instance.
		///</summary>
		public BedRoomPictureImageTrail():base(){}	
		
		#endregion
	}
}
