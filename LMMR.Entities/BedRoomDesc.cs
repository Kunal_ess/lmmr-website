﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'BedRoomDesc' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class BedRoomDesc : BedRoomDescBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="BedRoomDesc"/> instance.
		///</summary>
		public BedRoomDesc():base(){}	
		
		#endregion
	}
}
