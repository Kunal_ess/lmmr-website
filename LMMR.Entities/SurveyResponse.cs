﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'SurveyResponse' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class SurveyResponse : SurveyResponseBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="SurveyResponse"/> instance.
		///</summary>
		public SurveyResponse():base(){}	
		
		#endregion
	}
}
