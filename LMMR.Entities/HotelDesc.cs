﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'HotelDesc' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class HotelDesc : HotelDescBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="HotelDesc"/> instance.
		///</summary>
		public HotelDesc():base(){}	
		
		#endregion
	}
}
