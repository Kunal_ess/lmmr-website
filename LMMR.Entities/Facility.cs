﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'Facility' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class Facility : FacilityBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="Facility"/> instance.
		///</summary>
		public Facility():base(){}	
		
		#endregion
	}
}
