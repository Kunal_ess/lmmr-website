﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'EmailConfigMapping' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class EmailConfigMapping : EmailConfigMappingBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="EmailConfigMapping"/> instance.
		///</summary>
		public EmailConfigMapping():base(){}	
		
		#endregion
	}
}
