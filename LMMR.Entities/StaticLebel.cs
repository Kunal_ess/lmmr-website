﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'StaticLebel' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class StaticLebel : StaticLebelBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="StaticLebel"/> instance.
		///</summary>
		public StaticLebel():base(){}	
		
		#endregion
	}
}
