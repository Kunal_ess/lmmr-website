﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'Hotel_Trail' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class HotelTrail : HotelTrailBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="HotelTrail"/> instance.
		///</summary>
		public HotelTrail():base(){}	
		
		#endregion
	}
}
