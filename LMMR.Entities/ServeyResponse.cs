﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'ServeyResponse' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class ServeyResponse : ServeyResponseBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="ServeyResponse"/> instance.
		///</summary>
		public ServeyResponse():base(){}	
		
		#endregion
	}
}
