﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'HotelContact' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class HotelContact : HotelContactBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="HotelContact"/> instance.
		///</summary>
		public HotelContact():base(){}	
		
		#endregion
	}
}
