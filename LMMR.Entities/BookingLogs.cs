﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'BookingLogs' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class BookingLogs : BookingLogsBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="BookingLogs"/> instance.
		///</summary>
		public BookingLogs():base(){}	
		
		#endregion
	}
}
