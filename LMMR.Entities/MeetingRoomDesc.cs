﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'MeetingRoomDesc' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class MeetingRoomDesc : MeetingRoomDescBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="MeetingRoomDesc"/> instance.
		///</summary>
		public MeetingRoomDesc():base(){}	
		
		#endregion
	}
}
