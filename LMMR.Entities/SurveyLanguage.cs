﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'SurveyLanguage' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class SurveyLanguage : SurveyLanguageBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="SurveyLanguage"/> instance.
		///</summary>
		public SurveyLanguage():base(){}	
		
		#endregion
	}
}
