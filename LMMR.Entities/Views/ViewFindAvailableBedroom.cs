﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'ViewFindAvailableBedroom' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class ViewFindAvailableBedroom : ViewFindAvailableBedroomBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="ViewFindAvailableBedroom"/> instance.
		///</summary>
		public ViewFindAvailableBedroom():base(){}	
		
		#endregion
	}
}
