﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'ViewReportLeadTime' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class ViewReportLeadTime : ViewReportLeadTimeBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="ViewReportLeadTime"/> instance.
		///</summary>
		public ViewReportLeadTime():base(){}	
		
		#endregion
	}
}
