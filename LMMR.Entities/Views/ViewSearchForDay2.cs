﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'ViewSearchForDay2' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class ViewSearchForDay2 : ViewSearchForDay2Base
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="ViewSearchForDay2"/> instance.
		///</summary>
		public ViewSearchForDay2():base(){}	
		
		#endregion
	}
}
