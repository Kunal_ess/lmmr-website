﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'ViewAvailabilityAndSpecialManager' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class ViewAvailabilityAndSpecialManager : ViewAvailabilityAndSpecialManagerBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="ViewAvailabilityAndSpecialManager"/> instance.
		///</summary>
		public ViewAvailabilityAndSpecialManager():base(){}	
		
		#endregion
	}
}
