﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'BookingRequestViewList' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class BookingRequestViewList : BookingRequestViewListBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="BookingRequestViewList"/> instance.
		///</summary>
		public BookingRequestViewList():base(){}	
		
		#endregion
	}
}
