﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'ViewReportCancelation' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class ViewReportCancelation : ViewReportCancelationBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="ViewReportCancelation"/> instance.
		///</summary>
		public ViewReportCancelation():base(){}	
		
		#endregion
	}
}
