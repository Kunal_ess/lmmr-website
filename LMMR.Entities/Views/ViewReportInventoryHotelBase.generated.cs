﻿/*
	File generated by NetTiers templates [www.nettiers.com]
	Important: Do not modify this file. Edit the file ViewReportInventoryHotel.cs instead.
*/
#region Using Directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;
using System.Xml.Serialization;
#endregion

namespace LMMR.Entities
{
	///<summary>
	/// An object representation of the 'ViewReportInventoryHotel' view. [No description found in the database]	
	///</summary>
	[Serializable]
	[CLSCompliant(true)]
	[ToolboxItem("ViewReportInventoryHotelBase")]
	public abstract partial class ViewReportInventoryHotelBase : System.IComparable, System.ICloneable, INotifyPropertyChanged
	{
		
		#region Variable Declarations
		
		/// <summary>
		/// Id : 
		/// </summary>
		private System.Int64		  _id = (long)0;
		
		/// <summary>
		/// Name : 
		/// </summary>
		private System.String		  _name = null;
		
		/// <summary>
		/// TotalMeetintRoom : 
		/// </summary>
		private System.Int32?		  _totalMeetintRoom = null;
		
		/// <summary>
		/// OnlineMeetingRoom : 
		/// </summary>
		private System.Int32?		  _onlineMeetingRoom = null;
		
		/// <summary>
		/// Object that contains data to associate with this object
		/// </summary>
		private object _tag;
		
		/// <summary>
		/// Suppresses Entity Events from Firing, 
		/// useful when loading the entities from the database.
		/// </summary>
	    [NonSerialized] 
		private bool suppressEntityEvents = false;
		
		#endregion Variable Declarations
		
		#region Constructors
		///<summary>
		/// Creates a new <see cref="ViewReportInventoryHotelBase"/> instance.
		///</summary>
		public ViewReportInventoryHotelBase()
		{
		}		
		
		///<summary>
		/// Creates a new <see cref="ViewReportInventoryHotelBase"/> instance.
		///</summary>
		///<param name="_id"></param>
		///<param name="_name"></param>
		///<param name="_totalMeetintRoom"></param>
		///<param name="_onlineMeetingRoom"></param>
		public ViewReportInventoryHotelBase(System.Int64 _id, System.String _name, System.Int32? _totalMeetintRoom, System.Int32? _onlineMeetingRoom)
		{
			this._id = _id;
			this._name = _name;
			this._totalMeetintRoom = _totalMeetintRoom;
			this._onlineMeetingRoom = _onlineMeetingRoom;
		}
		
		///<summary>
		/// A simple factory method to create a new <see cref="ViewReportInventoryHotel"/> instance.
		///</summary>
		///<param name="_id"></param>
		///<param name="_name"></param>
		///<param name="_totalMeetintRoom"></param>
		///<param name="_onlineMeetingRoom"></param>
		public static ViewReportInventoryHotel CreateViewReportInventoryHotel(System.Int64 _id, System.String _name, System.Int32? _totalMeetintRoom, System.Int32? _onlineMeetingRoom)
		{
			ViewReportInventoryHotel newViewReportInventoryHotel = new ViewReportInventoryHotel();
			newViewReportInventoryHotel.Id = _id;
			newViewReportInventoryHotel.Name = _name;
			newViewReportInventoryHotel.TotalMeetintRoom = _totalMeetintRoom;
			newViewReportInventoryHotel.OnlineMeetingRoom = _onlineMeetingRoom;
			return newViewReportInventoryHotel;
		}
				
		#endregion Constructors
		
		#region Properties	
		/// <summary>
		/// 	Gets or Sets the Id property. 
		///		
		/// </summary>
		/// <value>This type is bigint</value>
		/// <remarks>
		/// This property can not be set to null. 
		/// </remarks>
		[DescriptionAttribute(""), System.ComponentModel.Bindable( System.ComponentModel.BindableSupport.Yes)]
		public virtual System.Int64 Id
		{
			get
			{
				return this._id; 
			}
			set
			{
				if (_id == value)
					return;
					
				this._id = value;
				this._isDirty = true;
				
				OnPropertyChanged("Id");
			}
		}
		
		/// <summary>
		/// 	Gets or Sets the Name property. 
		///		
		/// </summary>
		/// <value>This type is nvarchar</value>
		/// <remarks>
		/// This property can be set to null. 
		/// </remarks>
		[DescriptionAttribute(""), System.ComponentModel.Bindable( System.ComponentModel.BindableSupport.Yes)]
		public virtual System.String Name
		{
			get
			{
				return this._name; 
			}
			set
			{
				if (_name == value)
					return;
					
				this._name = value;
				this._isDirty = true;
				
				OnPropertyChanged("Name");
			}
		}
		
		/// <summary>
		/// 	Gets or Sets the TotalMeetintRoom property. 
		///		
		/// </summary>
		/// <value>This type is int</value>
		/// <remarks>
		/// This property can be set to null. 
		/// If this column is null, this property will return (int)0. It is up to the developer
		/// to check the value of IsTotalMeetintRoomNull() and perform business logic appropriately.
		/// </remarks>
		[DescriptionAttribute(""), System.ComponentModel.Bindable( System.ComponentModel.BindableSupport.Yes)]
		public virtual System.Int32? TotalMeetintRoom
		{
			get
			{
				return this._totalMeetintRoom; 
			}
			set
			{
				if (_totalMeetintRoom == value && TotalMeetintRoom != null )
					return;
					
				this._totalMeetintRoom = value;
				this._isDirty = true;
				
				OnPropertyChanged("TotalMeetintRoom");
			}
		}
		
		/// <summary>
		/// 	Gets or Sets the OnlineMeetingRoom property. 
		///		
		/// </summary>
		/// <value>This type is int</value>
		/// <remarks>
		/// This property can be set to null. 
		/// If this column is null, this property will return (int)0. It is up to the developer
		/// to check the value of IsOnlineMeetingRoomNull() and perform business logic appropriately.
		/// </remarks>
		[DescriptionAttribute(""), System.ComponentModel.Bindable( System.ComponentModel.BindableSupport.Yes)]
		public virtual System.Int32? OnlineMeetingRoom
		{
			get
			{
				return this._onlineMeetingRoom; 
			}
			set
			{
				if (_onlineMeetingRoom == value && OnlineMeetingRoom != null )
					return;
					
				this._onlineMeetingRoom = value;
				this._isDirty = true;
				
				OnPropertyChanged("OnlineMeetingRoom");
			}
		}
		
		
		/// <summary>
		///     Gets or sets the object that contains supplemental data about this object.
		/// </summary>
		/// <value>Object</value>
		[System.ComponentModel.Bindable(false)]
		[LocalizableAttribute(false)]
		[DescriptionAttribute("Object containing data to be associated with this object")]
		public virtual object Tag
		{
			get
			{
				return this._tag;
			}
			set
			{
				if (this._tag == value)
					return;
		
				this._tag = value;
			}
		}
	
		/// <summary>
		/// Determines whether this entity is to suppress events while set to true.
		/// </summary>
		[System.ComponentModel.Bindable(false)]
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public bool SuppressEntityEvents
		{	
			get
			{
				return suppressEntityEvents;
			}
			set
			{
				suppressEntityEvents = value;
			}	
		}

		private bool _isDeleted = false;
		/// <summary>
		/// Gets a value indicating if object has been <see cref="MarkToDelete"/>. ReadOnly.
		/// </summary>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public virtual bool IsDeleted
		{
			get { return this._isDeleted; }
		}


		private bool _isDirty = false;
		/// <summary>
		///	Gets a value indicating  if the object has been modified from its original state.
		/// </summary>
		///<value>True if object has been modified from its original state; otherwise False;</value>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public virtual bool IsDirty
		{
			get { return this._isDirty; }
		}
		

		private bool _isNew = true;
		/// <summary>
		///	Gets a value indicating if the object is new.
		/// </summary>
		///<value>True if objectis new; otherwise False;</value>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public virtual bool IsNew
		{
			get { return this._isNew; }
			set { this._isNew = value; }
		}

		/// <summary>
		///		The name of the underlying database table.
		/// </summary>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public string ViewName
		{
			get { return "ViewReportInventoryHotel"; }
		}

		
		#endregion
		
		#region Methods	
		
		/// <summary>
		/// Accepts the changes made to this object by setting each flags to false.
		/// </summary>
		public virtual void AcceptChanges()
		{
			this._isDeleted = false;
			this._isDirty = false;
			this._isNew = false;
			OnPropertyChanged(string.Empty);
		}
		
		
		///<summary>
		///  Revert all changes and restore original values.
		///  Currently not supported.
		///</summary>
		/// <exception cref="NotSupportedException">This method is not currently supported and always throws this exception.</exception>
		public virtual void CancelChanges()
		{
			throw new NotSupportedException("Method currently not Supported.");
		}
		
		///<summary>
		///   Marks entity to be deleted.
		///</summary>
		public virtual void MarkToDelete()
		{
			this._isDeleted = true;
		}
		
		#region ICloneable Members
		///<summary>
		///  Returns a Typed ViewReportInventoryHotelBase Entity 
		///</summary>
		public virtual ViewReportInventoryHotelBase Copy()
		{
			//shallow copy entity
			ViewReportInventoryHotel copy = new ViewReportInventoryHotel();
				copy.Id = this.Id;
				copy.Name = this.Name;
				copy.TotalMeetintRoom = this.TotalMeetintRoom;
				copy.OnlineMeetingRoom = this.OnlineMeetingRoom;
			copy.AcceptChanges();
			return (ViewReportInventoryHotel)copy;
		}
		
		///<summary>
		/// ICloneable.Clone() Member, returns the Deep Copy of this entity.
		///</summary>
		public object Clone(){
			return this.Copy();
		}
		
		///<summary>
		/// Returns a deep copy of the child collection object passed in.
		///</summary>
		public static object MakeCopyOf(object x)
		{
			if (x is ICloneable)
			{
				// Return a deep copy of the object
				return ((ICloneable)x).Clone();
			}
			else
				throw new System.NotSupportedException("Object Does Not Implement the ICloneable Interface.");
		}
		#endregion
		
		
		///<summary>
		/// Returns a value indicating whether this instance is equal to a specified object.
		///</summary>
		///<param name="toObject">An object to compare to this instance.</param>
		///<returns>true if toObject is a <see cref="ViewReportInventoryHotelBase"/> and has the same value as this instance; otherwise, false.</returns>
		public virtual bool Equals(ViewReportInventoryHotelBase toObject)
		{
			if (toObject == null)
				return false;
			return Equals(this, toObject);
		}
		
		
		///<summary>
		/// Determines whether the specified <see cref="ViewReportInventoryHotelBase"/> instances are considered equal.
		///</summary>
		///<param name="Object1">The first <see cref="ViewReportInventoryHotelBase"/> to compare.</param>
		///<param name="Object2">The second <see cref="ViewReportInventoryHotelBase"/> to compare. </param>
		///<returns>true if Object1 is the same instance as Object2 or if both are null references or if objA.Equals(objB) returns true; otherwise, false.</returns>
		public static bool Equals(ViewReportInventoryHotelBase Object1, ViewReportInventoryHotelBase Object2)
		{
			// both are null
			if (Object1 == null && Object2 == null)
				return true;

			// one or the other is null, but not both
			if (Object1 == null ^ Object2 == null)
				return false;

			bool equal = true;
			if (Object1.Id != Object2.Id)
				equal = false;
			if (Object1.Name != null && Object2.Name != null )
			{
				if (Object1.Name != Object2.Name)
					equal = false;
			}
			else if (Object1.Name == null ^ Object1.Name == null )
			{
				equal = false;
			}
			if (Object1.TotalMeetintRoom != null && Object2.TotalMeetintRoom != null )
			{
				if (Object1.TotalMeetintRoom != Object2.TotalMeetintRoom)
					equal = false;
			}
			else if (Object1.TotalMeetintRoom == null ^ Object1.TotalMeetintRoom == null )
			{
				equal = false;
			}
			if (Object1.OnlineMeetingRoom != null && Object2.OnlineMeetingRoom != null )
			{
				if (Object1.OnlineMeetingRoom != Object2.OnlineMeetingRoom)
					equal = false;
			}
			else if (Object1.OnlineMeetingRoom == null ^ Object1.OnlineMeetingRoom == null )
			{
				equal = false;
			}
			return equal;
		}
		
		#endregion
		
		#region IComparable Members
		///<summary>
		/// Compares this instance to a specified object and returns an indication of their relative values.
		///<param name="obj">An object to compare to this instance, or a null reference (Nothing in Visual Basic).</param>
		///</summary>
		///<returns>A signed integer that indicates the relative order of this instance and obj.</returns>
		public virtual int CompareTo(object obj)
		{
			throw new NotImplementedException();
		}
	
		#endregion
		
		#region INotifyPropertyChanged Members
		
		/// <summary>
      /// Event to indicate that a property has changed.
      /// </summary>
		[field:NonSerialized]
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
      /// Called when a property is changed
      /// </summary>
      /// <param name="propertyName">The name of the property that has changed.</param>
		protected virtual void OnPropertyChanged(string propertyName)
		{ 
			OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
		}
		
		/// <summary>
      /// Called when a property is changed
      /// </summary>
      /// <param name="e">PropertyChangedEventArgs</param>
		protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
		{
			if (!SuppressEntityEvents)
			{
				if (null != PropertyChanged)
				{
					PropertyChanged(this, e);
				}
			}
		}
		
		#endregion
				
		/// <summary>
		/// Gets the property value by name.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="propertyName">Name of the property.</param>
		/// <returns></returns>
		public static object GetPropertyValueByName(ViewReportInventoryHotel entity, string propertyName)
		{
			switch (propertyName)
			{
				case "Id":
					return entity.Id;
				case "Name":
					return entity.Name;
				case "TotalMeetintRoom":
					return entity.TotalMeetintRoom;
				case "OnlineMeetingRoom":
					return entity.OnlineMeetingRoom;
			}
			return null;
		}
				
		/// <summary>
		/// Gets the property value by name.
		/// </summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <returns></returns>
		public object GetPropertyValueByName(string propertyName)
		{			
			return GetPropertyValueByName(this as ViewReportInventoryHotel, propertyName);
		}
		
		///<summary>
		/// Returns a String that represents the current object.
		///</summary>
		public override string ToString()
		{
			return string.Format(System.Globalization.CultureInfo.InvariantCulture,
				"{5}{4}- Id: {0}{4}- Name: {1}{4}- TotalMeetintRoom: {2}{4}- OnlineMeetingRoom: {3}{4}", 
				this.Id,
				(this.Name == null) ? string.Empty : this.Name.ToString(),
			     
				(this.TotalMeetintRoom == null) ? string.Empty : this.TotalMeetintRoom.ToString(),
			     
				(this.OnlineMeetingRoom == null) ? string.Empty : this.OnlineMeetingRoom.ToString(),
			     
				System.Environment.NewLine, 
				this.GetType());
		}
	
	}//End Class
	
	
	/// <summary>
	/// Enumerate the ViewReportInventoryHotel columns.
	/// </summary>
	[Serializable]
	public enum ViewReportInventoryHotelColumn
	{
		/// <summary>
		/// Id : 
		/// </summary>
		[EnumTextValue("Id")]
		[ColumnEnum("Id", typeof(System.Int64), System.Data.DbType.Int64, false, false, false)]
		Id,
		/// <summary>
		/// Name : 
		/// </summary>
		[EnumTextValue("Name")]
		[ColumnEnum("Name", typeof(System.String), System.Data.DbType.String, false, false, true, 100)]
		Name,
		/// <summary>
		/// TotalMeetintRoom : 
		/// </summary>
		[EnumTextValue("TotalMeetintRoom")]
		[ColumnEnum("TotalMeetintRoom", typeof(System.Int32), System.Data.DbType.Int32, false, false, true)]
		TotalMeetintRoom,
		/// <summary>
		/// OnlineMeetingRoom : 
		/// </summary>
		[EnumTextValue("OnlineMeetingRoom")]
		[ColumnEnum("OnlineMeetingRoom", typeof(System.Int32), System.Data.DbType.Int32, false, false, true)]
		OnlineMeetingRoom
	}//End enum

} // end namespace
