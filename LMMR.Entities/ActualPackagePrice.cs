﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'ActualPackagePrice' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class ActualPackagePrice : ActualPackagePriceBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="ActualPackagePrice"/> instance.
		///</summary>
		public ActualPackagePrice():base(){}	
		
		#endregion
	}
}
