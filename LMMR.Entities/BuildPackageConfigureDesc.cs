﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'BuildPackageConfigureDesc' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class BuildPackageConfigureDesc : BuildPackageConfigureDescBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="BuildPackageConfigureDesc"/> instance.
		///</summary>
		public BuildPackageConfigureDesc():base(){}	
		
		#endregion
	}
}
