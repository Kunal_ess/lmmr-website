﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'ZoneLanguage' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class ZoneLanguage : ZoneLanguageBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="ZoneLanguage"/> instance.
		///</summary>
		public ZoneLanguage():base(){}	
		
		#endregion
	}
}
