﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'MeetingRoomConfig' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class MeetingRoomConfig : MeetingRoomConfigBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="MeetingRoomConfig"/> instance.
		///</summary>
		public MeetingRoomConfig():base(){}	
		
		#endregion
	}
}
