﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'Statistics' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class Statistics : StatisticsBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="Statistics"/> instance.
		///</summary>
		public Statistics():base(){}	
		
		#endregion
	}
}
