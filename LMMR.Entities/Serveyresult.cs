﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'Serveyresult' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class Serveyresult : ServeyresultBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="Serveyresult"/> instance.
		///</summary>
		public Serveyresult():base(){}	
		
		#endregion
	}
}
