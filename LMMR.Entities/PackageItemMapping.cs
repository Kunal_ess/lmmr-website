﻿#region Using directives

using System;

#endregion

namespace LMMR.Entities
{	
	///<summary>
	/// An object representation of the 'PackageItemMapping' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class PackageItemMapping : PackageItemMappingBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="PackageItemMapping"/> instance.
		///</summary>
		public PackageItemMapping():base(){}	
		
		#endregion
	}
}
